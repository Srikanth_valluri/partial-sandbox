trigger ContentVersionTrigger on ContentVersion (before insert, 
                                                 before update, 
                                                 before delete, 
                                                 after insert, 
                                                 after update, 
                                                 after delete, 
                                                 after undelete) 
{
    TriggerFactoryCls.createHandler(ContentVersion.sObjectType);
}