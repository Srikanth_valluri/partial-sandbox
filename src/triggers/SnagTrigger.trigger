trigger SnagTrigger on SNAGs__c (after insert, after update, after delete) {
    if(trigger.isAfter){
        if(trigger.isInsert){
            SnagTriggerHandler.afterInsert(trigger.newMap);
        }
        if(trigger.isUpdate){
            SnagTriggerHandler.afterUpdate(trigger.newMap, trigger.oldMap);
        }
        if(trigger.isDelete){
            SnagTriggerHandler.afterDelete(trigger.oldMap);
        }
    }
}