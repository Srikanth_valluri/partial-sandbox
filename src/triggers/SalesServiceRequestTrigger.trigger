Trigger SalesServiceRequestTrigger on Sales_Service_Request__c (after update) {

    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            new SSRTriggerHandler().sendForApprovalOnUpdate(Trigger.newMap, Trigger.oldMap);
            new SSRTriggerHandler().createFinanceTask(Trigger.newMap, Trigger.oldMap);
        }
    }
}