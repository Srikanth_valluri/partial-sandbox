trigger InventoryAddOnTrg on Inventory_AddOn__c (before insert, before update, after insert, after update, after delete, after undelete) {
    if(trigger.isBefore){
        if(trigger.isInsert || trigger.isUpdate){
            for(Inventory_AddOn__c ia : trigger.new){
                if(ia.Costing__c > 0){
                    ia.Costing_in_Words__c = NumberToWord.english_number(integer.valueOf(ia.Costing__c));
                }
                if(trigger.isUpdate && ia.Inventory__c != null && ia.Option__c != trigger.oldmap.get(ia.Id).Option__c){
                    ia.addError('AddOns cannot be changed or deleted for DP OK or DOCS OK Inventory.');
                }
            }
        }
        if(trigger.isDelete){
            for(Inventory_AddOn__c ia : trigger.old){
                if(ia.inventory__c != null)
                    ia.addError('AddOns cannot be changed or deleted for DP OK or DOCS OK Inventory.');
            }
        }
    }



    if(trigger.isafter){
        set<Id> bookingUnitIds = new set<Id>();
        if(trigger.isInsert || trigger.isUpdate || trigger.isUndelete){
            for(Inventory_AddOn__c ia : trigger.new){
                if(ia.Booking_unit__c != null)
                    bookingUnitIds.add(ia.Booking_unit__c);
            }
        }

        if(trigger.isUpdate || trigger.isDelete){
            for(Inventory_AddOn__c ia : trigger.old){
                if(ia.Booking_unit__c != null && ia.Booking_unit__c != trigger.oldmap.get(ia.Id).Booking_unit__c)
                    bookingUnitIds.add(ia.Booking_unit__c);
            }
        }

        if(!bookingUnitIds.isEmpty()){
            list<Booking_unit__c> lstUnits = new list<Booking_unit__c>();
            for(AggregateResult ar : [Select sum(Costing__c) cost, Booking_unit__c bId from Inventory_AddOn__c where Booking_Unit__c != null and costing__c > 0 group by Booking_unit__c]){
                lstUnits.add(new Booking_Unit__c(Id = (string)ar.get('bId'), Total_AddOns_Price__c = (decimal)ar.get('cost')));
            }

            if(!lstUnits.IsEmpty())
                update lstUnits;
        }
    }
}