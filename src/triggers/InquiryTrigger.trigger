/****************************************************************************************************
* Name               : InquiryTrigger                                                               *
* Description        : This is a trigger on the inquiry object.                                     *
* Created Date       : 06/01/2017                                                                   *
* Created By         : NSI                                                                          *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE            COMMENTS                                                *
* 1.0   NSI - Vineet        12/01/2017      Initial Draft                                           *
* 2.0   Lochana Rajput      23/04/2018      Added encryption logic for Promoter Community Profile   *
* 3.0   Monali Nagpure      08/05/2018      Called TeleSalesConfirmation calss                      *
**************************************************************************************************/
trigger InquiryTrigger on Inquiry__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if (DAMAC_Constants.skip_InquiryTrigger == FALSE) {
        TriggerFactoryCls.createHandler(Inquiry__c.sObjectType);
        /*List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String MyProflieName = PROFILE[0].Name;
        if (MyProflieName !='Promoter Community Profile') {
            TriggerFactoryCls.createHandler(Inquiry__c.sObjectType);
            //InquiryTriggerHandler.callhandlers();
        }*/
    
        //V3.0 Monali 8-May-2018 START
        if(trigger.isBefore) {
            if(Trigger.isInsert || Trigger.isUpdate){
                TeleSalesConfirmation instance = new TeleSalesConfirmation();
                instance.UpdateLocation(trigger.new);
                system.debug('===InquiryTrigger is before Insert==');
            }
        }

        if(Trigger.isAfter && Trigger.isUpdate){
            TeleSalesConfirmation obj = new TeleSalesConfirmation();
            if(TeleSalesConfirmation.iscalled == null){
                TeleSalesConfirmation.iscalled = true; 
                system.debug('isUpdate >>>>>>>>>' + trigger.newMap.keySet());
                TeleSalesConfirmation.SendTeleSalesConfirmationEmail(trigger.newMap.keySet());
            } 
        } 
        //V3.0 Monali 8-May-2018 END  
    }

}// End of class.