trigger DesignChangeRequestTrg on Design_Change_Request__c (after update) {
    if(trigger.isAfter)
        InventoryDesignChangeUtility.afterHandler(trigger.new, trigger.oldmap);
}