/****************************************************************************************************
* Name          : UserActionPlanTrigger                                                             *
* Description   :                                                                                   *
* Created Date  : 27/03/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    27/03/2018      Initial Draft.                                          *
****************************************************************************************************/
trigger UserActionPlanTrigger on User_Action_Plan__c (
    before insert, 
    before update, 
    before delete, 
    after insert, 
    after update, 
    after delete, 
    after undelete
) {
    TriggerFactoryCls.createHandler(User_Action_Plan__c.sObjectType);
}