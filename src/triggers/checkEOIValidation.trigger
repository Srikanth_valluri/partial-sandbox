trigger checkEOIValidation on EOI_Process__c (before insert, after Update) {
    for (EOI_Process__c record :Trigger.NEW) {
        if (Trigger.isBefore) {
            record.status__c = 'Draft';
        }
    }
    
    Set <ID> eoiId = new Set <ID> ();
    if (Trigger.isUpdate) {
        for (EOI_Process__c record :Trigger.NEW) {
            eoiId.add (record.Id);
        }
        Map <Id, List <Attachment>> attMap = new Map <Id, List <Attachment>> ();
        for (Attachment att :[SELECT Name, ParentID FROM Attachment where ParentId IN: eoiId ]) {
            if (attMap.containsKey (att.ParentId))
                attMap.get (att.ParentId).add (att);        
            else
                attMap.put (att.ParentID, new List <Attachment> {att});
                    
        }
        for (EOI_Process__c record :Trigger.NEW) {
            
            if (record.status__c == 'Approved' 
                && Trigger.oldMap.get (record.Id).status__c != record.Status__c
                && Trigger.oldMap.get (record.Id).status__c == 'Submitted') {
                
                if (record.Receipt_Number__c == NULL) {
                    record.addError (Label.EOI_Receipt_Number_Missing);
                }    
            }
            
            if (record.status__c == 'Submitted'){
            /*
                if (attmap.containsKey (record.ID)) {
                    if (attMap.get (record.ID).size () == 0) {
                        record.addError (Label.EOI_Receipt_Missing);
                    }    
                }
                else {
                    record.addError (Label.EOI_Receipt_Missing);
                }
                */
            } 
            
        }
    }
}