/**************************************************************************************************
* Name               : TasTrigger                                                                 *
* Description        : This is a trigger on task object.                                          *
* Created Date       : 12/01/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         NSI - Vineet      12/01/2017      Initial Draft                                     *
* 1.2         NSI - Sivasankar  30/03/2017      Adding the events of Trigger                      *
**************************************************************************************************/
trigger TaskTrigger on Task (before insert, after insert,before update, after update,before delete,after delete) {
    Skip_Task_Trigger_for_Informatica__c settings = Skip_Task_Trigger_for_Informatica__c.getInstance (UserInfo.getUserID ());
    system.debug('settings=='+settings);
    if (settings.Enable__c) {
        system.debug('in trigger==');
        TriggerFactoryCls.createHandler(Task.sObjectType);
    
    
        /*if( Trigger.isUpdate && Trigger.isBefore){
          for( Task t : Trigger.new){
              if( t.Subject == 'Verify COCD Fee' && t.status == 'Closed' && ( String.isEmpty( t.Description ) || String.isEmpty( t.COCD_Fee_Invoice_Number__c ) ) ){
                t.addError( 'Cannot close task without adding Comments and COCD Fee Invoice Number' );
              }
          }
        }*/
        
        if(Trigger.isInsert && Trigger.isBefore ) { 
            list<Task> lstTask = new list<Task>();
             
             for (Task objTask : Trigger.new) {
                if( String.isNotBlank( objTask.Process_Name__c )
                    && objTask.Process_Name__c.equalsIgnoreCase( 'POP' ) ) {
                        lstTask.add(objTask);
                    }
             }
             if (lstTask.size() > 0) {
                 RoundRobinAssignment.changeOwner(lstTask); 
                 //RoundRobinAssignment.changeOwner(Trigger.new); 
             }  
        }
        if(Trigger.isInsert && Trigger.isAfter) {
            system.debug('after insert*****************');
            TaskTriggerHandlerAssignment taskObj = new TaskTriggerHandlerAssignment();
            TaskTriggerHandlerRPAgreement taskRPObj = new TaskTriggerHandlerRPAgreement();
            taskObj.checkTaskAssignedUser(trigger.new);
            taskRPObj.checkRPTaskAssignedUser(trigger.new);
    
            list<Task> lstTask = new list<Task>();
            list<Task> lstHOTask = new list<Task>();
            list<Task> lstLHOTask = new list<Task>();
            for (Task objTask : Trigger.new) {
                if (objTask.Process_Name__c == 'Early Handover') {
                    lstTask.add(objTask);
                 } else if (objTask.Process_Name__c == 'Handover'){
                   lstHOTask.add(objTask);
                 } else if (objTask.Process_Name__c == 'Lease Handover') {
                     system.debug('!!!!!inside lease Handover');
                     lstLHOTask.add(objTask);
                 }             
            }
    
            if (lstTask.size() > 0) {
                TaskTriggerHandlerEarlyHandover objHandler = new TaskTriggerHandlerEarlyHandover();
                objHandler.taskForExternalUser(lstTask);
            }
            if (lstHOTask.size() > 0 && Label.HO_Task_Push_To_IPMS.equalsIgnoreCase('Y')){
                TaskTriggerHandlerHandover objHandler = new TaskTriggerHandlerHandover();
                objHandler.taskForExternalUser(lstHOTask);  // Commented to stop IPMS task creation for Handover process
            }
            if (lstLHOTask.size() > 0) {
                TaskTriggerHandlerLeaseHandover objHandler = new TaskTriggerHandlerLeaseHandover();
                objHandler.taskForExternalUser(lstLHOTask);
            }
        }
    
        if(Trigger.isAfter && Trigger.isUpdate){
        system.debug('after Update*****************');
          //if(TaskTriggerHandlerIPMSTasks.firstRun) {
             //TaskTriggerHandlerIPMSTasks.firstRun = false;
             System.debug('Calling After Update PushToIPMS');
             TaskTriggerHandlerIPMSTasks.pushToIPMSOnAfterUpdate(trigger.newMap, trigger.oldMap);  
          //} 
        TaskTriggerHandlerAssignment taskObj = new TaskTriggerHandlerAssignment();
        TaskTriggerHandlerRPAgreement taskRPObj = new TaskTriggerHandlerRPAgreement();
        taskObj.checkTaskCompletion(trigger.new, trigger.oldMap);
        taskRPObj.checkRPTaskCompletion(trigger.new, trigger.oldMap);
    
        TaskTriggerHandlerLeaseHandover taskLHOObj = new TaskTriggerHandlerLeaseHandover();
        taskLHOObj.checkTaskCompletion(trigger.new, trigger.oldMap);
    
            list<Task> lstHOTask = new list<Task>();
            for (Task objtask : Trigger.new) {
                if (objTask.Process_Name__c == 'Handover'){
                    lstHOTask.add(objTask);
                }
            }
    
            if (lstHOTask.size() > 0){
                TaskTriggerHandlerHandover objHandler = new TaskTriggerHandlerHandover();
                if( Label.HO_Task_Push_To_IPMS.equalsIgnoreCase('Y')) {// Added to stop IPMS task creation for Handover process
                    objHandler.afterUpdate(trigger.newMap, trigger.oldMap);   
                }
            }
          
        }

        // Below added for validation as part of Manual Handover process
        if(Trigger.isBefore && Trigger.isUpdate){
            System.debug('Inside before update Handover trigger');
            
                TaskTriggerHandlerHandover triggerHandler = new TaskTriggerHandlerHandover();
                triggerHandler.checkESignatureVerification(trigger.new);
        }


         Set<Id> TaskId  = new Set<Id>();
         Set<Id> TaskIdUpdate  = new Set<Id>();
         if(Trigger.new != null ){
          for(Task t : Trigger.new){
          if(t.Subject == 'Verify Payment Received') {
           TaskId.add(t.Id);
           }
           if(t.Subject == 'Inform Customer on Approval' && t.Status == 'Completed'){
           TaskIdUpdate.add(t.Id);
           }
           }
    
         }
        
    
           if(Trigger.isupdate && Trigger.isBefore){
                TaskTriggerHandlerMoveIn_Out.executeBeforeUpdateTrigger(Trigger.new);
                TaskTriggerHandlerHandover.checkValidDocuments(trigger.newMap, trigger.oldMap);
                TaskTriggerHandlerSuggestion.checkValidInfoOnCase(trigger.newMap, trigger.oldMap);
                TaskTriggerHandlerMoveIn_Out.checkEmailPhoneOnCallingList(Trigger.new);
           }
    }

}// End of trigger