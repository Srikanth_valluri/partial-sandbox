/**************************************************************************************************
* Name               : AccountTrigger                                                             *
* Description        : This is a trigger on account object.                                       *
* Created Date       : 12/01/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR      DATE                                                                    *
* 1.0         Vineet      12/01/2017                                                              *
**************************************************************************************************/
trigger UnitAssignmentTrigger on Unit_Assignment__c (before insert, before update, before delete, 
    after insert, after update, after delete, after undelete) {

    TriggerFactoryCls.createHandler(Unit_Assignment__c.sObjectType);

}