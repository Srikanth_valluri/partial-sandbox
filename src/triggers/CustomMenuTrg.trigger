/* This trigger will execute on wechat messages when the first record in a day appears, it will send the custom menu as per the custom settings.*/
trigger CustomMenuTrg on Wechat_Deliverability__c (after insert) {

    List <Wechat_Deliverability__c> todayMessages = new List <Wechat_Deliverability__c> ();
    try {
        todayMessages = [SELECT Name FROM Wechat_Deliverability__c WHERE CreatedDate = TODAY LIMIT 10];
    } catch (Exception e) {}
    if (todayMessages.size () == 1) {
        Damac_WeChatServices.createCustomWeChatMenu ();
    }
}