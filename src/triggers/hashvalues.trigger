/*
Trigger to generate the SHA256 HASH Values for Mobile and Email Id and Name or any String
Name or any string added on Nov 20 2018
*/

trigger hashvalues on ROI_Hash__c (before insert) {

    for(ROI_Hash__c r:trigger.new){
        if(r.mobile__c != null){
            Blob targetBlob = Blob.valueOf(r.mobile__c);
            Blob hashm = Crypto.generateDigest('SHA256', targetBlob);    
            r.mobile_Hash__c = EncodingUtil.convertToHex(hashm);
        }
        
        if(r.email__c != null){
            Blob targetBlobem = Blob.valueOf(r.email__c);
            Blob hashe = Crypto.generateDigest('SHA256', targetBlobem);    
            r.email_Hash__c = EncodingUtil.convertToHex(hashe);
        }
        
        if(r.Name_or_Any_Value__c!= null){
            Blob targetBlobString = Blob.valueOf(r.Name_or_Any_Value__c);
            Blob hashName = Crypto.generateDigest('SHA256', targetBlobString);    
            r.Name_or_Any_Value_Hash__c = EncodingUtil.convertToHex(hashName);
        }
        if(r.Country__c != null){
            Blob targetBlobString = Blob.valueOf(r.Country__c);
            Blob hashName = Crypto.generateDigest('SHA256', targetBlobString);    
            r.Country_Hash__c = EncodingUtil.convertToHex(hashName);
        }
        if(r.City__c != null){
            Blob targetBlobString = Blob.valueOf(r.City__c);
            Blob hashName = Crypto.generateDigest('SHA256', targetBlobString);    
            r.City_Hash__c = EncodingUtil.convertToHex(hashName);
        }

        if(r.First_Name__c != null){
            Blob targetBlobString = Blob.valueOf(r.First_Name__c);
            Blob hashName = Crypto.generateDigest('SHA256', targetBlobString);    
            r.First_Name_Hash__c = EncodingUtil.convertToHex(hashName);
        }
        
        if(r.Last_Name__c != null){
            Blob targetBlobString = Blob.valueOf(r.Last_Name__c);
            Blob hashName = Crypto.generateDigest('SHA256', targetBlobString);    
            r.Last_Name_Hash__c = EncodingUtil.convertToHex(hashName);
        }
        
    
    }
    
}