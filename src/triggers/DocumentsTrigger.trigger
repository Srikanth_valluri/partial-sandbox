trigger DocumentsTrigger on SR_Attachments__c (after update) {
  DocumentsTriggerHandler objClass = new DocumentsTriggerHandler();
    objClass.afterUpdate(trigger.newMap, trigger.oldMap);
}