trigger DLDAgreement on DLD_Agreement__c (before insert, after insert, after update) {
    if (DAMAC_Constants.skip_InquiryTrigger == FALSE) {
        if (Trigger.isBefore && Trigger.isInsert)
            DLDAgreementHandler.beforeInsertEvent (Trigger.NEW);
        if (Trigger.isAfter && Trigger.isInsert)
            DLDAgreementHandler.afterInsertEvent (Trigger.NEW);
           
        if (Trigger.IsAfter && trigger.isUpdate) {
            for (DLD_Agreement__c agg :TRIGGER.NEW) {
                if (agg.Agreement_Status__c != 'OQOOD Registration Success' 
                    && agg.Agreement_Status__c == 'Reference Id Generated'
                    && agg.Agreement_Status__c != 'OQOOD Registration Failed')
                    System.enqueueJob(new Async_DLD_Rest (agg.Id, agg.Request_Reference_Id__c));
            }
        }
    }
}