/**************************************************************************************************
* Name               : InventoryTrigger                                                           *
* Description        : This is a trigger on inventory object.                                     *
* Created Date       : 04/01/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR          DATE          COMMENTS                                              *
* 1.0         NSI - Ravindra  04/01/2017    Initial Draft                                         *
* 1.1         NSI - Ravindra  26/04/2017    Added logic to update associated booking units.       *
* 1.2         bw -  srikanth  24/04/2018    Added logic to call attachments for plans             *
**************************************************************************************************/
trigger InventoryTrigger on Inventory__c (before insert, before update,after insert,after update) {
    TriggerFactoryCls.createHandler(Inventory__c.sObjectType);
    if(Trigger.isAfter && trigger.isUpdate){
        DAMAC_IPMS_INVENTORY_UPDATE.updateInvStatusToIPMS(trigger.New, trigger.oldMap);
    }
    if(trigger.isafter && !DAMAC_Constants.skip_InventoryTrigger && (trigger.isinsert || trigger.isupdate)){
        List<Inventory__c> lstInv = new List<Inventory__c>();
        Set <ID> inventoryIdsForPlans = new Set <ID> ();
        for(Inventory__c inv : trigger.new){
            if(inv.Status__c != null && inv.Status__c == 'Released'){
                lstInv.add(inv);
            }
        }
        if(lstInv != null && !lstInv.isempty())
            InventoryHelper.createRec(lstInv);
        if(trigger.isupdate){
            // Added by Srikanth to call createAttachmentsForPlanBatch class to Update the Plan Links on Inventory record 
            for (Inventory__c inv : Trigger.NEW) {
                /*
                if (inv.Unit_Plan__c != Trigger.OldMap.get (inv.ID).Unit_Plan__c) {
                    inventoryIdsForPlans.add (inv.ID); 
                }
                if (inv.plot_Plan__c != Trigger.OldMap.get (inv.ID).plot_Plan__c ) {
                    inventoryIdsForPlans.add (inv.ID); 
                }
                if (inv.floor_Plan__c != Trigger.OldMap.get (inv.ID).floor_Plan__c) {
                    inventoryIdsForPlans.add (inv.ID); 
                }
                */
                if ( (inv.status__c != Trigger.OldMap.get(inv.ID).status__c) && (inv.status__c == 'Released') ) {
                    inventoryIdsForPlans.add (inv.ID); 
                }
                
            }
            if (inventoryIdsForPlans.size () > 0) {
                if(System.Isbatch() || Test.isRunningTest()){
                    
                    Integer hourInt = Datetime.now().hour();
                    Integer minInt = Datetime.now().minute() ;
                    Integer secInt = Datetime.now().second() + 5;
                    if(secInt >= 60){
                        secInt = secInt - 60;
                        minInt++;
                        if(minInt >= 60){
                            minInt = minInt - 60;
                            hourInt++;
                        }                    
                    }
                    String hour = String.valueOf(hourInt);
                    String min = String.valueOf(minInt);
                    String ss = String.valueOf(secInt);
                    //parse to cron expression
                    String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';

                    System.schedule('ScheduledJob ' + String.valueOf(Math.random()), nextFireTime, 
                                        new createAttachmentsForPlanScheduler(inventoryIdsForPlans));
                } else{
                    Database.executeBatch (new createAttachmentsForPlanBatch (inventoryIdsForPlans), 1);
                }
            }    
        
            /* Calling the method to update the associated booking unit status, only on update of the invetory. */
            InventoryHelper.updateAssociatedBookingUnits(trigger.newMap, trigger.oldMap);
        }
    }
}// End of trigger.