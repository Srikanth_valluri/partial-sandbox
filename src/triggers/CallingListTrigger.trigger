/***************************************************************************************************    
 * @Description :This Trigger is used for Calling List.                                            *    
 *                                                                                                 *    
 *                                                                                                 *    
 *  Version     Author           Date         Description                                          *    
 *  1.0                                                                                            *    
****************************************************************************************************/   
trigger CallingListTrigger on Calling_List__c ( before Insert, before update, after update,after insert ) { 
    system.debug('=== Trigger of calling list called===');  
    /*check whether calling list trigger is enable*/    
        Map<String,TriggerOnOffCustomSetting__c> triggerSettingMap = TriggerOnOffCustomSetting__c.getAll(); 
        TriggerOnOffCustomSetting__c callingTriggerInst = triggerSettingMap.get('CallingListTrigger');  
        List<TriggerOnOffCustomSetting__c>updateTriggerSetting = new List<TriggerOnOffCustomSetting__c>();  
        Boolean isOn = callingTriggerInst.OnOffCheck__c;    
        if( trigger.isBefore ) {    
                    /*This is for Walk-In Calling List*/    
                    /*if( trigger.isUpdate ){   
                         WalkInCallingListHandler.onBeforeUpdateForWalkInCallingLst(trigger.oldMap,trigger.new);    
                      }*/   
            if( trigger.isUpdate && isOn) { 
                system.debug('=== Inside onBeforeUpdate ===');  
                //FetchRoleOFUserForCallingList.getAllOwnersList(trigger.new);  
                CallingListHandler.onBeforeUpdate( trigger.newMap, trigger.oldMap );    
                UpdateCallingListHandler.updateCallingListLookups(trigger.new); 
                UpdateCallingListHandler.onBeforeUpdateCallingLstForOutcomes(trigger.oldMap,trigger.new);   
                UpdateCallingListHandler.onBeforeUpdateForWalkInCallingLst(trigger.oldMap,trigger.new); 
                BouncedChequeCallingHandler.onBeforeUpdate(trigger.oldMap,trigger.new); 
                AppointmentHandler.onCREUpdate(trigger.newMap, trigger.oldMap);

                /******* Added by Aishwarya Todkar on 24-09-2020 ***********************/
                OwnerReassignmentHandler.onBeforeUpdateCL( trigger.oldMap, trigger.newMap );
            } 
            else if( trigger.isInsert && isOn){   
                system.debug('=== Inside onBeforeInsert ===');  
                //FetchRoleOFUserForCallingList.getAllOwnersList(trigger.new);  
                UpdateCallingListHandler.onBeforeInsert(trigger.new);   
                UpdateCallingListHandler.onBeforeInsertForWelcome(trigger.new); 
                UpdateCallingListHandler.onBeforeInsertForCallback(trigger.new);    
                CallBackRequestCallingHandler.onBeforeInsert(trigger.new);  
            } 
        }   
        
        else if( trigger.isAfter ) {
            //Todo addede to validate Cl if in future validation Come Plz Use the same Class
            if( trigger.isInsert && isOn) {
                ValidationCallingList.Validate(trigger.new);

               //Below added for Damac living app Email Communitcation part (Handover Document- approved)
               HDApp_handoverEmailCommunicationHandler.sendHandoverDocumentApprovedEmail(Trigger.New);
            }
            if(trigger.isUpdate){   
                //WalkInCallingListHandler.onAfterUpdateForWalkInCallingList(trigger.oldMap,trigger.new);   
            
            }   
            
            if( trigger.isUpdate && isOn) { 
                system.debug('=== Inside onAfter Update ===');  
                ValidationCallingList.ValidateCBR(trigger.new);
                UpdateCallingListHandler.onAfterUpdateForWalkInCallingList(trigger.oldMap,trigger.new); 
                //new CallingListTriggerHandlerReAssignOwner().afterUpdate(trigger.newMap, trigger.oldMap); 
                UpdateCallingListHandler.onAfterUpdate(trigger.oldMap,trigger.new); 
                BouncedChequeCallingHandler.onAfterUpdate(trigger.oldMap,trigger.new);  
                new CallingListHandler().onAfterUpdate(trigger.oldMap, trigger.new);    
                CreateNoteService.createNoteBasedOnCEComments(trigger.new,trigger.oldMap);  
                FMNotesCreation.createFMNoteBasedOnCEComments(trigger.new,trigger.oldMap);  
                InitiatePreparationsPageController.createCallingList(trigger.new, trigger.oldMap);  
                HO_UnitViewing.createCallingList(trigger.new, trigger.oldMap);  
                HO_KeyHandover.createCallingList(trigger.new, trigger.oldMap);  
               //caseRoundRobinAssignment.assignTicketsRoundRobin(Trigger.NewMap.keySet());  
               
               //Below added for Damac living app Email Communitcation part
               HDApp_handoverEmailCommunicationHandler.sendHandoverEmail(Trigger.New); 
            }
        }
}