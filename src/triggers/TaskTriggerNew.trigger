/**
 * @File Name          : TaskTriggerNew.trigger
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 11/18/2019, 2:35:24 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    11/11/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
trigger TaskTriggerNew on Task (before insert, before update, after update) {
    if(trigger.isBefore){
        if(trigger.isInsert){
            new TaskTriggerHandlerAssignOwner().beforeInsert(trigger.new);
        }
        if(trigger.isUpdate){
        
         /*for(Task t : trigger.new)
            {
             if(t.Status == 'Assign To Me' && System.Label.TaskAssign=='Task')
             t.OwnerId= UserInfo.getUserid();
             }*/
            TaskTriggerHandlerMoveIn_Out.executeBeforeUpdateTrigger(Trigger.new);
            new TaskTriggerHandlerClosureValidation().beforeUpdate(trigger.newMap, trigger.oldMap);
            new MortgageTaskClosureValidation().beforeUpdate(trigger.newMap, trigger.oldMap);
            ComplaintTaskTriggerHandler.performValidationOnCase(Trigger.new);
            TaskAddErrorhandler.validationBeforeUpdate(trigger.newMap, trigger.oldMap);
            TaskAddErrorHandler.validationBeforeClosingTask(trigger.newMap,trigger.oldMap);
            TaskTriggerHandlerDewaRefund triggerHandlerDewa = new TaskTriggerHandlerDewaRefund();
            triggerHandlerDewa.validationForRefundAmount(Trigger.new);
            triggerHandlerDewa.uploadDocsValidation(Trigger.new);
        }
    }
    if(trigger.isAfter){
        if(trigger.isUpdate){
            

        
    for(Task objTask: Trigger.New){
                /*code added for follow up validation*/
        // Commented on 19/11/19 as requested by Aditya
                /*if ( objTask.Status == 'Completed' 
                  && objTask.subject == 'Follow up with customer'
                  && objTask.Process_Name__c == 'AOPT'){
                    ValidateTask.validateFollowUpTask(objTask);
                }*/
        
         TaskTriggerHandlerDewaRefund triggerHandlerDewa = new TaskTriggerHandlerDewaRefund();
         if(TaskTriggerHandlerDewaRefund.firstRun) {
             if(!Test.isRunningTest()){TaskTriggerHandlerDewaRefund.firstRun = false;}
             triggerHandlerDewa.checkDEWATaskCompletion(trigger.newMap, trigger.oldMap);
         } 
                
        /*code added for POP ReviewTheRejected TasK validation*/
        if ( objTask.Status == 'Completed' && objTask.subject == 'Review the rejected finance task' && objTask.Process_Name__c == 'POP'){
            ValidateReviewTheRejectedTasK.methodValidateReviewTheRejectedTask(objTask);
            system.debug('In trigger ');
         }
      
        /*code added for POP Upload Required Document TasK validation*/
        if ( objTask.Status == 'Completed' && objTask.subject == 'Upload Required Document' && objTask.Process_Name__c == 'POP'){
            ValidateUploadRequiredDocumentTasK.methodValidateUploadRequiredDocumentTasK(objTask);
            system.debug('In trigger ');
        }
        
        if(objTask.Status == 'Completed' && objTask.Process_Name__c == 'General Inquiry' && objTask.Self_Employed__c == TRUE){
            SendTaskClosureEmailCustomerSRProcess.getTaskAndAccountData(objTask);
        }
    }
            new TaskTriggerHandlerClosureValidation().afterUpdate(trigger.newMap, trigger.oldMap);
            TaskTriggerHandlerManager.submitFMCaseForApproval(trigger.newMap, trigger.oldMap);
            //FM_TaskTriggerHandler.afterTaskUpdate( trigger.newMap, trigger.oldMap );
            TaskTriggerHandlerManager.createtaskForManager( trigger.newMap);
           
        }
    }
}