/**************************************************************************************************
 Name               : AttachmentTrigger                                                          
 Description        : This is a trigger on Attachment object.                                    
 Created Date       : 24/04/2017                                                                 
 Created By         : NSI                                                                        
 ----------------------------------------------------------------------------------------------- 
 VERSION        AUTHOR            DATE        COMMENTS                                                  
 1.0            NSI - Vineet      24/04/2017  Initial Draft
 1.1            QBurst            21/01/2020  Updating Sys Id in Unit Documents on Attachment Insert
 1.3            Aishwarya Todkar  08/02/2020  Added check for VAT Addendum                                           
**************************************************************************************************/
trigger AttachmentTrigger on Attachment (before insert, before update,before delete, after insert) {
    //System.debug('Inside Attachment Trigger '+DAMAC_Constants.Skip_AttachmentTrigger_SMS);
    // IF condition added by Srikanth, to skip the Attachment trigger, as it is effecting the SMS batch class.
    system.debug('Inside Attachment Trigger');
    if (DAMAC_Constants.Skip_AttachmentTrigger_SMS == false) {
        //TriggerFactoryCls.createHandler(Attachment.sObjectType);
        system.debug('Inside Skip');
        if(Trigger.isAfter && Trigger.isInsert){
            system.debug('Inside After Insert');
            AttachmentHandlerUploadDocument.createSRAttachment(trigger.new);
            ProcessKeyReleaseResponse.ProcessResponse(trigger.new);
            ProcessClientAckFormResposne.createSrAttach(trigger.new); 
            /******* 1.1 starts ***********/
            List<Attachment> unitDocAttachList = new List<Attachment>();
            for(Attachment attch: [SELECT Id, ParentId, Parent.Type FROM Attachment WHERE Id IN: Trigger.new]){
                
                if(attch.Parent.Type == 'Unit_Documents__c'){
                    unitDocAttachList.add(attch);
                }
            }
            system.debug('unitDocAttachList: ' + unitDocAttachList);
            if(unitDocAttachList.size() > 0){
                AttachmentTriggerHandler.updateUnitDocAfterAttachInsert(unitDocAttachList);
            }
            /*****1.1 ends********/ 
            // If attachment is related to DP Invoice.
            DPInvoicePaymentCallCoverLetterResponse.getAllAttachmentRecordsToPopulateCoverLetter(trigger.new);
            // SendKYCDrawloopDocument objSendKYCDrawloopDocument = new SendKYCDrawloopDocument();
            Map<Id,Attachment> mapParentToAtt = new Map<Id,Attachment>();
            Map<Id,Attachment> mapBuToAtt = new Map<Id,Attachment>();
            List<Id> listAttachId = new List<Id>();
            for(Attachment objAttachment : trigger.new){
                System.debug('-->> objAttachment : ' + objAttachment + ' \n Name : ' + objAttachment.Name);
                if(objAttachment.Name == 'Account KYC Form.pdf'){
              //      SendKYCDrawloopDocument.getBUandTemplate(objAttachment);
                }
                else if( objAttachment.Name.contains( 'VAT Letter' ) ) {
                    AttachmentHandler.sendVATLetter( objAttachment.Id );
                }
                else if( objAttachment.Name.contains( 'Refused to sign letter' ) 
                && objAttachment.ParentId != null 
                && String.valueOf( objAttachment.ParentId ).startsWith('500') ) {
                    //RefusedEmailNotifier.prepareSendGridEmail( objAttachment.Id );
                }
                else if( ( objAttachment.Name.contains( 'HO VAT Addendum' ) 
                || objAttachment.Name.contains( 'Handed Over Cover Letter' ) )
                && objAttachment.ParentId != null 
                &&  String.valueOf( objAttachment.ParentId ).startsWith('a0x') ) {
                    mapParentToAtt.put( objAttachment.ParentId, objAttachment );
                }
                else if( objAttachment.Name.contains( 'VAT Addendum' )
                && objAttachment.ParentId != null 
                && String.valueOf( objAttachment.ParentId ).startsWith('500') ) {
                    listAttachId.add( objAttachment.Id );
                }

            }
            
            System.debug('mapParentToAtt: ' +  mapParentToAtt );
            if( !mapParentToAtt.isEmpty() ) {
                VAT_AddendumDocGenerator.updateBu( mapParentToAtt );
            }
            if( !listAttachId.isEmpty() )
                VAT_AddendumDocGenCase.uploadVAT( listAttachId );
            system.debug('After Insert Complete');
        }
        system.debug('Trigger Complete');
    }
    if(Trigger.isAfter && Trigger.isInsert){
        
    }
}// End of trigger