trigger InvRestrictionReqTrigger on Inventory_Restriction_Request__c (before update, after Update) {
    if(Trigger.isAfter && Trigger.isUpdate){
        InvRestrictionReqTriggerHandler.afterUpdateHandler(trigger.new, trigger.oldmap);  
    }
    if(Trigger.isBefore && Trigger.isUpdate){
        InvRestrictionReqTriggerHandler.beforeUpdateHandler(trigger.new, trigger.oldmap);  
    }
}