trigger AmendmentTrigger on Amendment__c (before insert, before update, after insert, after update) {
	TriggerFactoryCls.createHandler(Amendment__c.sObjectType);
}