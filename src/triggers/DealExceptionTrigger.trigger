trigger DealExceptionTrigger on Deal_Exception_Request__c (after update, before update) {
    DealExceptionTriggerHandler handler = new DealExceptionTriggerHandler();
    if(Trigger.isBefore && Trigger.isUpdate){
        handler.checkSalesMargins(Trigger.newMap);
        List<Deal_Exception_Request__c> approvedDERList = new List<Deal_Exception_Request__c>();
        Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Deal_Exception_Request__c.getRecordTypeInfosById();
        for( Deal_Exception_Request__c der: Trigger.New){
            system.debug('rtMap: ' + rtMap);
            String recType = 'Deal Exception';
            if(der.RecordTypeId != null && rtMap.containsKey(der.RecordTypeId)){
                recType = rtMap.get(der.RecordTypeId).getName();
            }
            if(recType == 'Deal Exception' && der.Status__c != Trigger.oldMap.get(der.Id).Status__c && der.Status__c == 'Approved'){
                approvedDERList.add(der);
            }
            if(recType == 'Deal Exception' && der.Status__c != Trigger.oldMap.get(der.Id).Status__c
                    && der.Status__c == 'Awaiting Management Approval' 
                    && Trigger.oldMap.get(der.Id).Status__c == 'Awaiting MIS HOD Review'
                    && der.Sales_Margin_Approver__c == 'Chairman'){
                //der.status__c = 'Awaiting Chairman Approval';
            }
            if(recType == 'Request Inventory' && der.Status__c != Trigger.oldMap.get(der.Id).Status__c
                    && der.Status__c == 'Waiting for RM Confirmation'
                    && Trigger.oldMap.get(der.Id).Status__c == 'Awaiting Sales Admin Manager Approval'
                    && der.Contains_Restricted_Inventories__c == TRUE){
                der.Status__c = 'Awaiting MIS HOD Approval';
            }
        }
        system.debug('approvedDERList: ' + approvedDERList);
        if(approvedDERList.size() > 0){
            handler.updateDERStatus(approvedDERList);
        }
        
    }
    if(Trigger.isAfter && Trigger.isUpdate){
        List<Deal_Exception_Request__c> derList = new List<Deal_Exception_Request__c>();
        List<Deal_Exception_Request__c> derChairmanApprovalList = new List<Deal_Exception_Request__c>();
        List<Id> approvedIdList = new List<Id>();
        Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Deal_Exception_Request__c.getRecordTypeInfosById();
        for( Deal_Exception_Request__c der: Trigger.New){
            system.debug('rtMap: ' + rtMap);
            String recType = 'Deal Exception';
            if(der.RecordTypeId != null && rtMap.containsKey(der.RecordTypeId)){
                recType = rtMap.get(der.RecordTypeId).getName();
            }
            if(recType == 'Deal Exception' && der.Status__c != Trigger.oldMap.get(der.Id).Status__c){
                derList.add(der);
                if(der.Status__c == 'Approved' || der.Status__c == 'Approved Conditionally'){
                    approvedIdList.add(der.Id);
                }    
            }
        }
        if(derList.size() > 0){
            handler.updateRelatedAccInq(derList);
        }
        if(derChairmanApprovalList.size() > 0){
            handler.updateRelatedAccInq(derChairmanApprovalList);
        }
        system.debug('approvedIdList: ' + approvedIdList);
        if(approvedIdList.size() > 0){
            DealExceptionTriggerHandler.sendDERApprovalNotification(approvedIdList);
        }
    }
}