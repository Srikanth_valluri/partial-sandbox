trigger Damac_ResetInquiry on Inquiry_Assignment_Rules__c (after Insert) {

    
       
    
    if (Trigger.isInsert) {
        try {
            Id reassignParentRecordTypeId = Schema.SObjectType.Inquiry_Assignment_Rules__c.getRecordTypeInfosByDeveloperName().get('Reassign_Parent').getRecordTypeId();
            List <String> hodNames = new List <String> ();
            Map <String, List <String>> hodCountryCodes = new Map <String, List <String>> ();
            for (Inquiry_Assignment_Rules__c rule :Trigger.NEW) {

                if (rule.recordTypeId == reassignParentRecordTypeId && rule.Rule_for_HOD__c != NULL) {
                    hodNames.add (rule.Rule_for_HOD__c);
                }
            }
            if (hodNames.size () > 0) {
                for (Inquiry_Country_Code_Categories__c code :[SELECT Team_Name__c, Name 
                                                                FROM Inquiry_Country_Code_Categories__c 
                                                                WHERE Team_Name__c != NULL 
                                                                AND Team_Name__c IN: hodNames]) 
                {
                    if (!hodCountryCodes.containsKey (code.Team_Name__c))
                        hodCountryCodes.put (code.Team_Name__c, new List <String> {code.Name});
                    else
                        hodCountryCodes.get (code.Team_Name__c).add (code.Name);
                }
            }
            if (hodCountryCodes.values().size () > 0) {
                Id reAssignChildrecordTypeId = Schema.SObjectType.Inquiry_Assignment_Rules__c.getRecordTypeInfosByDeveloperName().get('Reassign_Child').getRecordTypeId();
                List <Inquiry_Assignment_Rules__c> childRules = new List <Inquiry_Assignment_Rules__c>();
                List <Inquiry_Assignment_Rules__c> parentRules = new List <Inquiry_Assignment_Rules__c>();
                for (Inquiry_Assignment_Rules__c rule :[SELECT Filter_Logic__c, RecordTypeId, Rule_for_HOD__c, HOD_Filter_Logic__c  
                                                            FROM Inquiry_Assignment_Rules__c 
                                                            WHERE ID IN:Trigger.NEW]) {
                    Integer i = 1;
                    String filterLogic = '';
                    if (rule.recordTypeId == reassignParentRecordTypeId && rule.Rule_for_HOD__c != NULL) {
                        for (String key :hodCountryCodes.get (rule.Rule_for_HOD__c)) {
                            Inquiry_Assignment_Rules__c obj = new Inquiry_Assignment_Rules__c();
                            obj.Parent_Reassign_Rule__c = rule.id;
                            obj.Parameter__c = 'Mobile_CountryCode__c';
                            obj.Condition__c = '=';
                            obj.Value__c = key;
                            obj.Active__c = TRUE;
                            obj.RecordTypeId = reAssignChildrecordTypeId;
                            obj.Value_Type__c = 'Text';
                            obj.Order__c = i;
                            filterLogic += i+' OR ';
                            i++;
                            childRules.add(obj);
                        }
                        filterLogic = filterLogic.removeEnd (' OR ');
                        if (filterLogic != '' && filterLogic != NULL)
                            rule.Rule_for_HOD_Filter_Logic__c = '('+filterLogic+')';
                        rule.HOD_Filter_Logic__c = true;
                        parentRules.add(rule);
                    }
                }
                if (childRules.size () > 0) {
                    insert childRules;
                }
                if (parentRules.size () > 0) {
                    update parentRules;
                }
            }
            
            
            if (hodNames.size () == 0) {
                Id recordTypeId = Schema.SObjectType.Inquiry_Assignment_Rules__c.getRecordTypeInfosByDeveloperName().get('Reset_Inquiry_Fields').getRecordTypeId();
                Id reAssignChildrecordTypeId = Schema.SObjectType.Inquiry_Assignment_Rules__c.getRecordTypeInfosByDeveloperName().get('Reassign_Child').getRecordTypeId();
                Map<String, String> fieldsFromMetaData = new Map<String, String>();
                
                for (Inquiry_Field_Reset__mdt metaData: [SELECT Label, Value__c FROM Inquiry_Field_Reset__mdt WHERE DeveloperName != NULL]) {
                    fieldsFromMetaData.put(metaData.Label, metaData.Value__c);
                }
                List<Inquiry_Assignment_Rules__c> assignmentRulesToInsert = new List<Inquiry_Assignment_Rules__c> ();
                for (Inquiry_Assignment_Rules__c rule :Trigger.NEW) {
                    if (rule.RecordTypeId == recordTypeId) {
                        for (String key: fieldsFromMetaData.keySet()) {
                            Inquiry_Assignment_Rules__c obj = new Inquiry_Assignment_Rules__c();
                            obj.Parameter__c = key;
                            obj.RecordTypeId = reAssignChildrecordTypeId;
                            obj.Parent_Reassign_Rule__c = rule.Id;
                            obj.Value__c = fieldsFromMetadata.get(key);
                            assignmentRulesToInsert.add(obj);
                        }           
                    }
                }
                if (assignmentRulesToInsert.size() > 0)
                    insert assignmentRulesToInsert;
            }
        } catch (Exception e) {}
    }
}