/*
 * Description - Trigger for populating account id on booking record
 *
 * Version        Date            Author            Description
 * 1.0            24/12/17        Vivek Shinde      Initial Draft
 */
trigger BuyerTriggerEternus on Buyer__c (after insert, after update) {
    if( trigger.isAfter && trigger.isInsert ) {
        new BuyerTriggerHandler().callInsertHelper(trigger.new);  
    }
  
    if(trigger.isAfter && trigger.isUpdate) {
        new BuyerTriggerHandler().callUpdateHelper(trigger.oldMap, trigger.new);
    } 
}