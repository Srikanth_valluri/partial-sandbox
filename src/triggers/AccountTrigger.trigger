/**************************************************************************************************
* Name               : AccountTrigger                                                             *
* Description        : This is a trigger on account object.                                       *
* Created Date       : 12/01/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR      DATE                                                                    *
* 1.0         Vineet      12/01/2017                                                              *
**************************************************************************************************/
trigger AccountTrigger on Account (before insert, before update, after insert, after update) {
    TriggerFactoryCls.createHandler(Account.sObjectType);
}// End of trigger