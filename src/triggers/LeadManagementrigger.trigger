/***************************************************************************************************
* Description - The apex trigger is developed to update Lead Management records after approval
*
* Version   Date            Author            Description
* 1.0       22/03/18        Monali            Initial Draft
***************************************************************************************************/

trigger LeadManagementrigger on Lead_Management__c (after insert, after update) {

    if(trigger.isafter && trigger.isInsert){
        LeadMangementApproval instanceHandler = new LeadMangementApproval();
        instanceHandler.SendForApproval(trigger.new);
        System.debug('=====LeadManagementrigger after insert called');
    }

    if(trigger.isafter && trigger.isupdate){
        System.debug('=====LeadManagementrigger after update called');
        LeadManagementriggerHandler instanceHandler = new LeadManagementriggerHandler();
        instanceHandler.UpdateLeadManagement(trigger.new);
    }
}