/**************************************************************************************************
* Name               : TargetTrigger                                                              *
* Description        : This is a trigger on Target object.                                        *
* Created Date       : 12/06/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR      DATE                                                                    *
* 1.0         Rahul       12/06/2017                                                              *
**************************************************************************************************/
trigger TargetTrigger on Target__c (before insert, before update, after insert, after update) {
    TriggerFactoryCls.createHandler(Target__c.sObjectType);
}// End of trigger