/***************************************************************************************************
* Name                  : AgencyPCTrigger                                                          *    
* Test Class            : AgencyPCTriggerHandlerTest                                               *
* Description           : This is a trigger on Agency_PC__c object.                                *
* Created By            : NSI                                                                      *                                                                        
* Created Date          : 12/01/2017                                                               * 
* -------------------------------------------------------------------------------------------------* 
* VERSION     AUTHOR			DATE			COMMENT                                            *                    
* 1.0         Sivasankar		12/01/2017      Initial Development                                *
**************************************************************************************************/
trigger AgencyPCTrigger on Agency_PC__c (after insert, after update, after delete, before delete) { 
    TriggerFactoryCls.createHandler(Agency_PC__c.sObjectType); 
}// End of trigger