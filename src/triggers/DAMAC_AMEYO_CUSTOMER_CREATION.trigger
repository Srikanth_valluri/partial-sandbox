trigger DAMAC_AMEYO_CUSTOMER_CREATION on Ameyo_Log__c (after insert) {

    List <ID> inquiryIds = new List <ID> ();
    for (Ameyo_Log__c log :TRIGGER.NEW) {
        if (log.Inquiry__c != NULL) {
            inquiryIds.add (log.inquiry__c);
        }
    }
    if (inquiryIds.size () > 0) {
        DAMAC_AMEYO_CUSTOMER_CREATION.setCustomerManagerAsync (inquiryIds);
    }
        

}