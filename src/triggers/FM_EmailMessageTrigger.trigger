trigger FM_EmailMessageTrigger on EmailMessage ( before insert, after insert ) {
    if( trigger.isBefore ) {
    	if( trigger.isInsert ) {
    		FM_EmailMessageTriggerHandler.beforeInsert( trigger.new ); 
    	}    	
    }
    else if( trigger.isAfter ) {
    	if( trigger.isInsert ) {
    		FM_EmailMessageTriggerHandler.afterInsert(trigger.newMap);
    	}
    }
}