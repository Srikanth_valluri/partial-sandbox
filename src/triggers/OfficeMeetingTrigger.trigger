trigger OfficeMeetingTrigger on Office_Meeting__c (before insert, after insert,
    before update, after update,before delete,after delete) {
    
    if (Trigger.isBefore && Trigger.isInsert) {
        Set <ID> inqMeetings = new Set <ID> ();
        for (Office_Meeting__c meeting :Trigger.NEW) {
            if (meeting.Inquiry__c != NULL) {
                inqMeetings.add (meeting.Inquiry__c);
            }
        }
        if (inqMeetings.size () > 0) {
            Map <ID, Inquiry__c > salesOfficemap = new Map <ID, Inquiry__c > ();
            Set <String> salesOfficeSet = new Set <String> ();
            
            for (Inquiry__c inq : [SELECT Email__c, Sales_Office__c FROM Inquiry__c WHERE ID IN :inqMeetings AND Sales_Office__c != NULL]) {
                salesOfficeMap.put (inq.Id, inq);
                salesOfficeSet.add (inq.Sales_Office__c);
            }
            Map <String, Sales_Office_Locations__c> salesOfficeCampaings = new Map <String, Sales_Office_Locations__c> ();
            if (salesOfficeSet.size () > 0) {
                for (Sales_Office_Locations__c campaign: [SELECT Name, Assign_To__c, Campaign_ID__c FROM Sales_Office_Locations__c 
                                                            WHERE Name IN : salesOfficeSet]) 
                {
                    salesOfficeCampaings.put (campaign.Name, campaign);
                }
            }
            
            for (Office_Meeting__c meeting :Trigger.NEW) {
                if (meeting.Inquiry__c != NULL) {
                    meeting.Inquiry_Email__c = salesOfficeMap.get (meeting.Inquiry__c).Email__c;
                    String salesOffice = '';
                    if (salesOfficeMap.containsKey (meeting.Inquiry__c)) {
                        salesOffice = salesOfficeMap.get (meeting.Inquiry__c).Sales_Office__c;
                        if (salesOffice != '' && salesOffice != NULL) {
                            if (salesOfficeCampaings.containsKey (salesOffice )) {
                                String campaignName = salesOfficeCampaings.get (salesOffice).Campaign_ID__c;
                                if (campaignName != '' && campaignName != NULL) {
                                    meeting.Roadshow_Campaign__c = TRUE;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    if (Trigger.isAfter && Trigger.isInsert) {
        Database.executeBatch(new Damac_AssignRecommendedRmBatch (), 1);
    }
    if(Trigger.isAfter && Trigger.isUpdate){
        
        Map <ID, ID> preInqToConvertIds = new Map <ID, ID> ();
        for (Office_Meeting__c meeting:Trigger.NEW) {
            System.Debug (meeting);
            System.Debug (Trigger.OldMap.get (meeting.ID));
            if (Trigger.OldMap.get (meeting.ID).Convert_Inquiry__c != meeting.Convert_Inquiry__c 
                && meeting.Convert_Inquiry__c) {
                System.Debug ('===========');
                preInqToConvertIds.put (meeting.Inquiry__c, meeting.Recommended_RM__c);
            }
        }
        System.Debug ('===='+preInqToConvertIds);
        if (preInqToConvertIds.size () > 0) {
            List <Inquiry__c> inqToUpdate = new List <Inquiry__c> ();
            
            for (Inquiry__c inq : [SELECT Is_Meeting_Confirmed__c FROM Inquiry__c 
                    WHERE ID IN: preInqToConvertIds.keySet ()
                    AND RecordType.name = 'Pre Inquiry']) {
                inq.Assigned_PC__c = preInqToConvertIds.get (inq.ID);
                inq.Inquiry_Status__c = 'Meeting Scheduled';
                inqToUpdate.add (inq);
            }
            if (!Test.isRunningTest ())
                Database.Update (inqToUpdate);
        }
        OfficeMeetingTriggerHandler obj = new OfficeMeetingTriggerHandler();
        obj.UpdateRMMeetingTask(Trigger.new); 
    }

    if(Trigger.isBefore && Trigger.isUpdate){
        System.Debug ('isBefore Trigger.Old====' + Trigger.OldMap);
        System.Debug ('isBefore Trigger.New====' + Trigger.NewMap);
        Map<Id, List<Office_Meeting__c>> rmMeetingListMap = new Map<Id, List<Office_Meeting__c>>();
        for (Id offmId : Trigger.NewMap.keySet()) {
            Office_Meeting__c newOFFM = (Office_Meeting__c) Trigger.NewMap.get(offmId);
            Office_Meeting__c oldOFFM = (Office_Meeting__c) Trigger.OldMap.get(offmId);
            System.Debug ('newOFFM.Recommended_RM__c  >>>> ' + newOFFM.Recommended_RM__c );
            if (newOFFM.Recommended_RM__c != null) {
                System.Debug ('newOFFM.Recommended_RM__c  >>>> ' + newOFFM.Recommended_RM__c );
                System.Debug ('oldOFFM.Recommended_RM__c  >>>> ' + oldOFFM.Recommended_RM__c );
                if (oldOFFM.Recommended_RM__c != newOFFM.Recommended_RM__c) {
                    System.Debug ('Different >>> ');
                    if (rmMeetingListMap.containsKey(newOFFM.Recommended_RM__c)) {
                        rmMeetingListMap.get(newOFFM.Recommended_RM__c).add(newOFFM);
                    } else {
                        List<Office_Meeting__c> offmList = new List<Office_Meeting__c>();
                        offmList.add(newOFFM);
                        rmMeetingListMap.put(newOFFM.Recommended_RM__c, offmList);
                    }
                    System.Debug ('rmMeetingListMap >>> ' + rmMeetingListMap);
                }
            }
        }
        System.Debug ('rmMeetingListMap >>>> ' + rmMeetingListMap);
        if (!rmMeetingListMap.isEmpty()) {
            
            System.Debug ('rmMeetingListMap >>> ' + rmMeetingListMap);
            for (User userObj : [ SELECT Id, Name, Sales_Office__c, 
                                         DOS_Email__c, HOS_Email__c, HOD_Email__c
                                    FROM User 
                                   WHERE Id IN :rmMeetingListMap.keySet()
            ]) {
                if (rmMeetingListMap.containsKey(userObj.Id) 
                    && rmMeetingListMap.get(userObj.Id) != null
                ) {
                    for (Office_Meeting__c ofm : rmMeetingListMap.get(userObj.Id)) {
                        ofm.RM_DOS_Email__c = userObj.DOS_Email__c;
                        if (userObj.HOS_Email__c != Label.Exclude_From_Office_Meeting_Email) {
                            ofm.RM_HOS_Email__c = userObj.HOS_Email__c;
                        } else {
                            ofm.RM_HOS_Email__c = null;
                        }
                        ofm.RM_HOD_Email__c = userObj.HOD_Email__c;
                        if (userObj.Sales_Office__c == 'OCEAN HEIGHTS') {
                            ofm.RM_Additional_Email__c = Label.Ocean_Heights_Additional_Email;
                        } else {
                            ofm.RM_Additional_Email__c = null; 
                        } 
                    }
                }
            }
        }
    }

}