trigger CallLogTrigger  on Call_Log__c (before insert, after insert, 
                                                          before update, after update, 
                                                          before delete, after delete, 
                                                          after undelete) {
     CallLogTriggerHandler.handleTrigger(Trigger.new, Trigger.operationType);
     
     }