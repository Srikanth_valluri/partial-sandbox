// Create By : Naresh
// Created for Testing after final code this code need to move in CaseTrigger

trigger Mortgage_Trigger on Case (after update) {
  
 Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Mortgage').getRecordTypeId();
 Set<String> CaseId =  new Set<String>(); 
  
  for(Case c :  Trigger.new){
      System.debug('Mortgage RecordTypeId '+c.RecordTypeId);
      System.debug('Mortgage Developer '+devRecordTypeId);
      System.debug('c.Type '+c.Type);
      System.debug('c.AccountId '+c.AccountId);
      System.debug('c.Booking_Unit__c '+c.Booking_Unit__c);
      System.debug('c.status '+c.status);
      
      
     if(c.RecordTypeId == devRecordTypeId && c.Type.EqualsIgnoreCase('Mortgage')&&string.isNotBlank(c.AccountId)&&string.isNotBlank(c.Booking_Unit__c)&&c.status.EqualsIgnoreCase('Mortgage Off Plan / Ready')){
        CaseId.add(c.Id);
        System.debug('-- Inside Loop--'+CaseId);
      }
       
  }  
   
   if(!CaseId.isEmpty()){
   System.debug('--Mortgage---Callout '+CaseId);
   MortageTaskCreation.MortgageTask(CaseId);
   }

}