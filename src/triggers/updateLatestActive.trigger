/************************************************************************************************
 * @Name              : updateLatestActive
 * @Description       : Trigger to set the recently created record's Active field to true
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         QBurst         04/05/2020       Created
***********************************************************************************************/

Trigger updateLatestActive on Partnership_Program__c(after insert) {
    
    List<Partnership_Program__c> partnershipProgramList = new List<Partnership_Program__c>();  
    Set<Id> accountIds = new Set<Id>();
    Set<Id> partnershipProgramIds = new Set<Id>();
    
    for(Partnership_Program__c newPartnerProgram : trigger.new){
        if(newPartnerProgram.Active__c  == true){
               
            accountIds.add(newPartnerProgram.Account__c);
            partnershipProgramIds.add(newPartnerProgram.Id);
            
        }
    }
    
    partnershipProgramList = [SELECT Id,Active__c FROM Partnership_Program__c
          WHERE Active__c  = true AND Account__c IN: accountIds AND Id NOT IN: partnershipProgramIds];
          
    for(Partnership_Program__c oldPartnerPrograms :partnershipProgramList){
        oldPartnerPrograms.Active__c  =false;
    }
    update partnershipProgramList;
}