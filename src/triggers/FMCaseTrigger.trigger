/**
 * FM_Case__c Trigger
 *
 * Revision History
 * 28-05-2018   Lochana Rajput      Added logic for after insert and after update events
 * 31-07-2018   Lochana Rajput      Added logic for after insert and after update events
 * 27-09-2020   Shruti Nikam        Added FMCaseTriggerProcessFromPB methods
 */
trigger FMCaseTrigger on FM_Case__c (
    before insert, after insert, before update, after update, before delete, after delete
) {
    System.debug('FMCaseTrigger');
    if (Trigger.isBefore) {
        if (Trigger.isInsert) {
            FMCaseTriggerHandler.beforeInsert(trigger.newMap, trigger.oldMap);
            FMCaseTriggerHelper.addErrrorIfNoFMContractForP1Priority(trigger.new);
            FMCaseTriggerHelper.addErrrorForValidCAFMLocationsBeforeInsert(trigger.new);
            //FMCaseTriggerHelper.validationForContractSelection(trigger.new);
            //FMCaseTriggerProcessFromPB.beforeTriggerFieldUpdates(trigger.new);
        }
        if (Trigger.isUpdate) {
            FMCaseTriggerHandler.beforeUpdate(trigger.newMap, trigger.oldMap);
            FMCaseTriggerHelper.addErrrorForValidCAFMLocationsBeforeUpdate(trigger.newMap, trigger.oldMap);
            //FMCaseTriggerHelper.validationForContractSelection(trigger.new);
            //FMCaseTriggerProcessFromPB.beforeTriggerFieldUpdates(trigger.new); 
        }
    }
    if (Trigger.isAfter ) {
        if (Trigger.isUpdate) {
            FMCaseTriggerHandler.afterUpdate(trigger.newMap, trigger.oldMap);
            //FMCaseTriggerProcessFromPB.updateEnrichCustomerAccountForClosedCase(trigger.new);
            FMCaseTriggerHelper.createTaskINCAFMAfterUpdate(trigger.newMap, trigger.oldMap);
            //FMCaseTriggerProcessFromPB.fmCaseTaskRegMoveInCreation(trigger.new);
            //FMCaseTriggerProcessFromPB.fmMoveOutUpdate(trigger.new, trigger.oldMap);
            //FMCaseTriggerProcessFromPB.createSMSHistoryForFMCase(trigger.new);
            //FMCaseTriggerProcessFromPB.fmCasePOPTaskCreation(trigger.new);
            //FMCaseTriggerProcessFromPB.updateTaskStatus(trigger.new);
            //FMCaseTriggerProcessFromPB.accessCardRequest(trigger.new);
            //FMCaseTriggerProcessFromPB.createPenaltyWaiverTasks(trigger.new);
        }
        if (Trigger.isInsert) {
            FMCaseTriggerHandler.afterInsert(trigger.new);
            //FMCaseTriggerProcessFromPB.updateEnrichCustomerAccountForClosedCase(trigger.new);
            FMCaseTriggerHelper objFm = new FMCaseTriggerHelper();
            FMCaseTriggerHelper.createTaskInCAFM(trigger.newMap, trigger.oldMap);
            FMCaseTriggerHelper.createAndPushToCAFMForP1Priority(trigger.new);
            //FMCaseTriggerProcessFromPB.fmCaseTaskRegMoveInCreation(trigger.new);
            //FMCaseTriggerProcessFromPB.fmMoveOutUpdate(trigger.new, trigger.oldMap);
            //FMCaseTriggerProcessFromPB.createSMSHistoryForFMCase(trigger.new);
            //FMCaseTriggerProcessFromPB.fmCasePOPTaskCreation(trigger.new);
            //FMCaseTriggerProcessFromPB.updateTaskStatus(trigger.new);
            //FMCaseTriggerProcessFromPB.accessCardRequest(trigger.new);
            ////FMCaseTriggerProcessFromPB.createPenaltyWaiverTasks(trigger.new);
        }
    }
}