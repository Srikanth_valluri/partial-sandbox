/*
 * Description - Apex trigger on Email Message object
 *
 * Version        Date            Author            Description
 * 1.0            01/03/18        Vivek Shinde      Initial Draft
 */
trigger EmailMessageTrigger on EmailMessage (after insert) {
    if(Trigger.isAfter) {
        new EmailMessageTriggerHandler().onAfterInsert(Trigger.new);
        EmailCaseTriggerHandler.onAfterInsert(Trigger.new);
    }
}