trigger TravelDetailsTrigger on Travel_Details__c (before insert, before update, before delete, 
    after insert, after update, after delete, after undelete) {

    TriggerFactoryCls.createHandler(Travel_Details__c.sObjectType);

}