trigger DealExceptionUnitTrg on Deal_Exception_Unit__c (after insert, after update) {
    
    if(trigger.isAfter){
        DealExceptionUnitTriggerHandler.afterHandler(trigger.new, trigger.oldmap, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isUndelete);
    }
    
}