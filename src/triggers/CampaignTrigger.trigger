/**************************************************************************************************
* Name               : CampaignTrigger                                                            *
* Description        : This is a trigger on marketing campaign object.                            *
* Created Date       : 20/06/2017                                                                 *
* Created By         : PWC                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR      DATE                                                                    *
* 1.0         Vineet      20/06/2017                                                              *
**************************************************************************************************/
trigger CampaignTrigger on Campaign__c (before insert, before update,after update) {
    TriggerFactoryCls.createHandler(Campaign__c.sObjectType);
}// End of trigger