/****************************************************************************************
 * Trigger : ShareFilesWithCommunityUsers
 * Created By : ES
 ----------------------------------------------------------------------------------------
 * Description : Trigger is for allowing users to view Content Documents
 *
 * Test Data : 
 ----------------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
   1.0         ES                   08/08/2018      Initial Development
 ***************************************************************************************/
 
trigger ShareFilesWithCommunityUsers on ContentDocumentLink (before insert) {
    ShareFilesWithCommunityUsersHandler.shareFiles(trigger.new);
}