/**************************************************************************************************
* Name               : ProofOfPaymentTrigger *
* Description        : This is a trigger on account object.                                       *
* Created Date       : June 2017                                                                *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR      DATE                                                                    *
* 1.0         Vineet      12/01/2017                                                              *
**************************************************************************************************/
trigger ProofOfPaymentTrigger on Proof_of_Payment__c (before insert, before update, after insert, after update) {
  TriggerFactoryCls.createHandler(Proof_of_Payment__c.sObjectType);
}// End of trigger