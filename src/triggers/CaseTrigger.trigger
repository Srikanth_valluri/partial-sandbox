trigger CaseTrigger on Case(before insert, after insert, before update, after update ) {
    // Trigger Enable or Disable using custom settings
    Boolean CheckCustomSettings = false;
    if(!Test.isRunningTest()){
       TriggerOnOffCustomSetting__c TS = TriggerOnOffCustomSetting__c.getValues('CaseTrigger');
       if(TS.OnOffCheck__c != null){
           CheckCustomSettings = TRUE;
       }
    }
    
    IF (CheckCustomSettings ||  Test.isRunningTest()){
    // Trigger
    system.debug('case trigger initiate');
    if(trigger.isAfter && trigger.isUpdate) {
      // new CaseTriggerHandler().onAfterUpdate(trigger.oldMap, trigger.new);
       new CaseTriggerHandler().restructureCSR(trigger.new);
    }
    if(trigger.isAfter)
    {
    if(PushNotificationToMobileApp.run)
    {
        PushNotificationToMobileApp.run=false;
        if(trigger.isInsert)
        {
           //For Insersing notification record
           if(trigger.new[0].Origin=='Mobile App')
           {
            
            CaseTriggerHandler.CreateNotificationRecord(trigger.new);
            
            PushNotificationToMobileApp.PushNotificationSend(trigger.new[0].caseNumber,trigger.new[0].createdbyid,trigger.new[0].Status);
           }
        }
        
        if(trigger.isUpdate)
        {
        
          if(trigger.new[0].Origin=='Mobile App')
           {
           CaseTriggerHandler.CreateNotificationRecord(trigger.new);
          
            PushNotificationToMobileApp.PushNotificationSend(trigger.new[0].caseNumber,trigger.new[0].createdbyid,trigger.new[0].Status);
           }
        }
    }
    
    }

    if( trigger.isBefore ) {
        if (trigger.isInsert) {
            CaseTriggerHandler.onBeforeInsert(trigger.old, trigger.new);
            new CallingListGenerator().onBeforeInsert( trigger.new );
            CaseTriggerHandler.populateContactSprinkler(trigger.new);
            CaseTriggerHandler.dewaRefundOpenSRValidation(trigger.new);
        }
        if (trigger.isUpdate) {
            CaseTriggerHandler.onBeforeUpdate(trigger.old, trigger.new);
            CaseTriggerHandlerAssignment.beforeUpdate(trigger.newMap, trigger.oldMap);
            CaseTriggerHandler.populateValuesOnBeforeUpdate(trigger.newMap, trigger.oldMap);
            //CaseAssignmentAddErrorHandler.assignmentValidationBeforeUpdate(trigger.newMap, trigger.oldMap);
            
            /******* Added by Aishwarya Todkar on 24-09-2020 ***********************/
            OwnerReassignmentHandler.onBeforeUpdateCase( trigger.oldMap, trigger.newMap );
        }
    }
    else if( trigger.isAfter ) {
        if( trigger.isInsert ) {
            system.debug('case trigger inside insert');
            new CaseTriggerHandler().onAfterInsert( trigger.new );
            new CaseTriggerHandlerHO().afterInsert(trigger.newMap);
            new CallingListGenerator().onAfterInsert( trigger.new );
            //new CaseTriggerHandlerAssignment().afterInsert(trigger.new);
            
        }
        else if( trigger.isUpdate ) {
        
         
          
            system.debug('*****AFTER UPDATE*****');
            new ComplaintCaseTriggerHandler().changeOwnerOfTasks(Trigger.oldMap, Trigger.newMap);
            new CaseTriggerHandlerHO().afterUpdate(trigger.newMap, trigger.oldMap);
            
            new CaseTriggerHandlerRPAgreement().afterUpdate(trigger.newMap, trigger.oldMap);
            // TEMPORARILY COMMENTED BELOW 2 LINES
            RentalPoolTerminationTriggerHandler.afterCaseUpdate( trigger.newMap, trigger.oldMap );
            new CaseTriggerHandlerRPAssignment().afterUpdate(trigger.newMap, trigger.oldMap);
            new CaseTriggerHanderReAssignOwner().afterUpdate(trigger.newMap, trigger.oldMap);
            new CaseTriggerHandler().onAfterUpdate(trigger.oldMap, trigger.new);
            new CaseTriggerHandler().onAfterUpdateForDEWARefund(trigger.oldMap, trigger.new);
            //new CallingListGenerator().onAfterUpdate( trigger.new, trigger.oldMap );
            new CaseTriggerHandlerAssignment().afterUpdate(trigger.newMap, trigger.oldMap);
            caseTriggerHandlerOTPVerification.afterUpdate(trigger.newMap, trigger.oldMap);
            new CODCaseTriggerHandler().UpdateCODCase(trigger.new);
            new SubstractNumberOfNOC().OnAfterUpdate(trigger.newMap, trigger.oldMap);

            //new UpdateHoEhoAndRelatedClOwnerHandler().updateOwnerOfCallingList(trigger.newMap, trigger.oldMap);
        }
    }
  }
}