/**************************************************************************************************
* Name               : StepTrigger                                                                *
* Description        : This is a trigger on Target object.                                        *
* Created Date       : 30/07/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR      DATE                                                                    *
* 1.0         Rahul       30/07/2017                                                              *
**************************************************************************************************/
trigger StepEmailTrigger on NSIBPM__Step__c (before insert, before update, after insert, after update) {
    TriggerFactoryCls.createHandler(NSIBPM__Step__c.sObjectType);
}// End of trigger

/*trigger StepEmailTrigger on NSIBPM__Step__c (before insert) {
    Set<Id> srIds = new Set<Id>();
    Map<Id, NSIBPM__Service_Request__c> srMap = new Map<Id, NSIBPM__Service_Request__c>();
    List<NSIBPM__Step__c> stepList = trigger.new;
    if(!stepList.isEmpty()){
        for(NSIBPM__Step__c stp : stepList){
            if(stp.NSIBPM__SR__c != null){
                srIds.add(stp.NSIBPM__SR__c);
            }
        }
    }
    if(!srIds.isEmpty()){
        for(NSIBPM__Service_Request__c sr : [SELECT Id, Manager_Email__c, DOS_Email__c, HOS_Email__c, HOD_Email__c, Name
                                             FROM NSIBPM__Service_Request__c WHERE Id IN :srIds])
        {
            srMap.put(sr.Id, sr);
        }
    }
    
    if(!srMap.isEmpty() && !stepList.isEmpty()){
        for(NSIBPM__Step__c stp : stepList){
            stp.Manager_Email__c = srMap.get(stp.NSIBPM__SR__c).Manager_Email__c;
            stp.DOS_Email__c = srMap.get(stp.NSIBPM__SR__c).DOS_Email__c ;
            stp.HOS_Email__c = srMap.get(stp.NSIBPM__SR__c).HOS_Email__c ;
            stp.HOD_Email__c = srMap.get(stp.NSIBPM__SR__c).HOD_Email__c ;
        }
    }
    system.debug(stepList);
               
}*/