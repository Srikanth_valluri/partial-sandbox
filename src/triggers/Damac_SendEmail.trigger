trigger Damac_SendEmail on Email_Metrics__c (after insert, after Update) {

    if (Damac_Constants.skip_EmailMetricsTrigger == FALSE) {
        List <ID> emailMetricsIds = new List <ID> ();
        for (Email_Metrics__c metrics :Trigger.NEW) {
            if (Trigger.isInsert && Trigger.isAfter)
                emailMetricsIds.add (metrics.id);
            if (Trigger.isUpdate && Trigger.isAfter) {
                if (Trigger.OldMap.get (metrics.id).Email_Send__c != metrics.Email_Send__c && metrics.Email_Send__c == TRUE) {
                    emailMetricsIds.add (metrics.id);    
                }
            }
        }
        if (emailMetricsIds.size () > 0) {
            Damac_SendgridEmailController.sendEmailAsync (emailmetricsIds);
            //Damac_SendEmailTriggerHandler.sendEmailAsync (emailMetricsIds);
        }
    }
}