trigger BookingUnitTrigger on Booking_Unit__c (before update,after Update) {
    if(trigger.isBefore && trigger.isUpdate){
        BookingUnitTriggerHandlerHO objClass = new BookingUnitTriggerHandlerHO();
        objClass.beforeUpdate(trigger.newMap,trigger.oldMap);
    }
    if( trigger.isAfter && trigger.isUpdate ) {
        BookingUnitHONoticeSentTriggerHandler.onAfterUpdate(trigger.new,trigger.oldMap);
    }
}