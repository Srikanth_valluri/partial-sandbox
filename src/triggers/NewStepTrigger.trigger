trigger NewStepTrigger on New_Step__c (before insert, after insert, before update, after update) {
    
    TriggerFactoryCls.createHandler(New_Step__c.sObjectType);
    
}