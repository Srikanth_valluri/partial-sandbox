trigger SalesTour_SubmitForHODApproval on Sales_Tours__c (before update, after update) {
    
    Map<Id, Sales_Tours__c> rejectedStatements = new Map<Id, Sales_Tours__c >{};
  
    if (Trigger.isUpdate && Trigger.isbefore) {
        List <sales_tours__Share> tourShares = new List <sales_tours__Share> ();
        
        for(Sales_Tours__c inv: trigger.new)
        {    
            /*if (Trigger.oldMap.get (inv.Id).HOS_User__c != inv.HOS_User__c 
                && inv.HOS_User__c != null) {
                
                sales_tours__Share shareTour = new sales_tours__Share ();
                shareTour.ParentId = inv.Id;
                shareTour.UserOrGroupId = inv.HOS_User__c;
                shareTour.AccessLevel = 'Read';
                shareTour.RowCause = Schema.sales_tours__Share.RowCause.Manual;
                tourShares.add (shareTour);
            }*/
            
            
            
        
            if (inv.Approval_Comment_Check__c == 'Requested' && Trigger.oldMap.get (inv.Id).Approval_Comment_Check__c != inv.Approval_Comment_Check__c)
            { 
                rejectedStatements .put(inv.Id, inv);
                inv.Approval_Comment_Check__c = null; 
            }
        }  
        /*if (tourShares.size () > 0) {
            Database.insert(tourShares, false);
        }*/
        if (!rejectedStatements.isEmpty())  
        {
            List<Id> processInstanceIds = new List<Id>{};
            for (Sales_Tours__c invs : [SELECT (SELECT ID
                                                      FROM ProcessInstances
                                                      ORDER BY CreatedDate DESC
                                                      LIMIT 1)
                                              FROM Sales_Tours__c
                                              WHERE ID IN :rejectedStatements.keySet()])
            {
                if (!Test.isRunningTest ())
                    processInstanceIds.add(invs.ProcessInstances[0].Id);
            }
              
            for (ProcessInstance pi : [SELECT TargetObjectId,
                                          (SELECT Id, StepStatus, Comments FROM Steps ORDER BY CreatedDate DESC LIMIT 1)
                                            
                                       FROM ProcessInstance
                                       WHERE Id IN :processInstanceIds
                                       ORDER BY CreatedDate DESC])   
            {                   
                
                if ((pi.Steps[0].Comments == null || pi.Steps[0].Comments.trim().length() == 0))
                {
                    rejectedStatements.get(pi.TargetObjectId).addError('Operation Cancelled: Please provide a rejection reason!');
                }
            }
        } 
  
    }
    
    if (Trigger.isUpdate && Trigger.isAfter) {
        List <ID> tourIds = new List <ID> ();
        for (Sales_Tours__c tour :Trigger.New) {
            
            if (Trigger.oldMap.get (Tour.Id).Disqualify_Status__c != tour.Disqualify_Status__c 
                && tour.Disqualify_Status__c == 'HT Rejected' && tour.Approver__c != null) {
                
                tourIds.add (tour.Id); 
            }
        }
        if (tourIds.size () > 0)
            Damac_SubmitForApproval.submitApproval (tourIds);
    }
    
    
}