Trigger LiveChatTranscriptTrigger on LiveChatTranscript (after insert) {

    if (Trigger.isAfter && Trigger.isInsert) {
        new LiveChatTranscriptHandler().sendEmail(Trigger.new);
    }
}