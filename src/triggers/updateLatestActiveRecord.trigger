/************************************************************************************************
 * @Name              : updateLatestActiveRecord 
 * @Description       : Trigger to set the recently created record's Active field to true
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         QBurst         14/05/2020       Created
***********************************************************************************************/

Trigger updateLatestActiveRecord on New_Partnership_Program__c(after insert) {
    
    List<New_Partnership_Program__c> partnershipProgramList = new List<New_Partnership_Program__c>();  
    Set<Id> accountIds = new Set<Id>();
    Set<Id> partnershipProgramIds = new Set<Id>();
    
    for(New_Partnership_Program__c newPartnerProgram : trigger.new){
        if(newPartnerProgram.Active__c  == true){
               
            accountIds.add(newPartnerProgram.Account__c);
            partnershipProgramIds.add(newPartnerProgram.Id);
            
        }
    }
    
    partnershipProgramList = [SELECT Id,Active__c FROM New_Partnership_Program__c
          WHERE Active__c  = true AND Account__c IN: accountIds AND Id NOT IN: partnershipProgramIds];
          
    for(New_Partnership_Program__c oldPartnerPrograms :partnershipProgramList){
        oldPartnerPrograms.Active__c  =false;
    }
    update partnershipProgramList;
}