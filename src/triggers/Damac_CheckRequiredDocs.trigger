trigger Damac_CheckRequiredDocs on Unit_Documents__c (after insert, after update) {

    Set <ID> srIds = new Set <ID> ();
    Map <ID, Boolean> docsUploaded = new Map <ID, Boolean> ();
    Id recordTypeId = Schema.SObjectType.Unit_Documents__c.getRecordTypeInfosByName().get('Registration').getRecordTypeId();
    set<id> requestIds = new set<Id>(); 
    for (Unit_Documents__c doc :Trigger.NEW) {        
        if (doc.RecordTypeId ==  recordTypeId) {
            srIds.add (doc.Service_Request__c);   
            docsUploaded.put (doc.Service_Request__c, false);
        }
        if(trigger.isInsert && doc.Service_Request__c != null){
            requestIds.add(doc.service_Request__c);
        }
    }
    if(!requestIds.isEmpty()){
        Map <ID, NSIBPM__Service_Request__c> srMap = new Map <ID, NSIBPM__Service_Request__c> ([SELECT ownerId, NSIBPM__Required_Docs_not_Uploaded__c 
                                                                                                FROM NSIBPM__Service_Request__c
                                                                                                WHERE ID IN :requestIds]);
        
        list<Unit_Documents__Share> unitShares = new list<Unit_Documents__Share>();
        for(Unit_Documents__c doc :Trigger.NEW){
            if(trigger.isInsert && srMap.containsKey(doc.Service_Request__c)){
                Unit_Documents__Share share = new Unit_Documents__Share();
                share.ParentId = doc.Id;
                share.UserOrGroupId = srMap.get(doc.Service_Request__c).OwnerId;
                share.AccessLevel = 'edit';
                share.RowCause = 'Manual';
                unitShares.add(share);
            }
        }
        Database.SaveResult[] unitSharesResult = Database.insert(unitShares,false);

    }
    if (srIds.size () > 0) {
        Map <ID, NSIBPM__Service_Request__c> srMap = new Map <ID, NSIBPM__Service_Request__c> ([SELECT NSIBPM__Required_Docs_not_Uploaded__c 
                                                                                                FROM NSIBPM__Service_Request__c
                                                                                                WHERE ID IN :srIds]);
        for (Unit_Documents__c doc :[SELECT Optional__c, Status__c, Service_Request__c 
                                    FROM Unit_Documents__c 
                                    WHERE Service_Request__c IN :srIds 
                                        AND Optional__c = FALSE 
                                        AND Status__c = 'Pending Upload' 
                                        AND RecordType.Name = 'Registration']){
            docsUploaded.put (doc.Service_Request__c, true);
        }
        List <NSIBPM__Service_Request__c> srToUpdate = new List <NSIBPM__Service_Request__c> ();
        
        for (ID key :docsUploaded.keySet ()) {
            System.Debug (srMap.get (key).NSIBPM__Required_Docs_not_Uploaded__c);
            System.Debug (docsUploaded.get (key));
            if (srMap.get (key).NSIBPM__Required_Docs_not_Uploaded__c != docsUploaded.get (key)) {
                NSIBPM__Service_Request__c req = new NSIBPM__Service_Request__c (id = key);
                req.NSIBPM__Required_Docs_not_Uploaded__c = docsUploaded.get (key);
                srToUpdate.add (req);
            }
        }
        if (srToUpdate.size () > 0) {
            update srToUpdate;
        }
    }

}