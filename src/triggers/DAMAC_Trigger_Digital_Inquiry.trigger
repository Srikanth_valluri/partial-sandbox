trigger DAMAC_Trigger_Digital_Inquiry on Digital_Inquiry__c (after insert, after Update) {
    String enableTrigger = System.Label.Damac_SendSmsTrigger;
    if (Test.isRunningTest ()) {
        enableTrigger = 'enable';
    }
    if (enableTrigger  == 'enable') {
        set<ID> inqRecords = new set<ID> ();
        
        for (Digital_Inquiry__c inq :Trigger.NEW) {
            if (Trigger.isUpdate) {
                if (inq.Mobile_Phone_Encrypt__c != NULL 
                    && inq.SMS_Skipped__c == FALSE && inq.Send_SMS__c
                    && Trigger.oldMap.get (inq.Id).SMS_Skipped__c != inq.SMS_Skipped__c) {
                        inqRecords.add(inq.Id);
                }
            } 
            if (Trigger.isInsert) {
                if (inq.Mobile_Phone_Encrypt__c != NULL && inq.Send_SMS__c) {
                    inqRecords.add(inq.Id);
                } 
            }
        }
        if (inqRecords.size () > 0) {
            Integer total = 0;
            try {
                Integer nonBatchAsyncApexJobs = [SELECT count() FROM AsyncApexJob 
                                                 WHERE JobType != 'BatchApexWorker' 
                                                 AND JobType != 'BatchApex' 
                                                 AND CreatedDate >= :DateTime.now().addDays(-1)];
                
                AggregateResult[] ar = [SELECT SUM(JobItemsProcessed) FROM AsyncApexJob 
                                        WHERE JobType = 'BatchApex' 
                                        AND CreatedDate >= :DateTime.now().addDays(-1)];
                
                Integer batchJobItemsProcessed = Integer.valueOf(ar[0].get('expr0'));
                
                
                total = nonBatchAsyncApexJobs + batchJobItemsProcessed;
            } catch (Exception e) {}
            System.debug('total: ' + total);            
            //Apex method executions (batch Apex, future methods, Queueable Apex, and scheduled Apex) per a 24-hour period: 250,000 
            // or the number of user licenses in your organization multiplied by 200
            if ( total < 200000) {
                Damac_sendSMS.sendSecureMessage (inqRecords,'Digital_Inquiry__c');
            }
            else {
                List <Digital_Inquiry__c> inqToUpdate = new List <Digital_Inquiry__c> ();
                for (ID key :inqRecords) {
                    Digital_Inquiry__c inq = new Digital_Inquiry__c (id = key);
                    inq.SMS_Skipped__c = true;
                    inqToUpdate.add (inq);
                }
                Update inqToUpdate;
            }
        }
    }

}