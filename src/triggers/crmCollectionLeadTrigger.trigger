//Test class : crmCollectionLeadTriggerHandlerTest
trigger crmCollectionLeadTrigger on Collection__c (before insert) {
    
    crmCollectionLeadTriggerHandler obj = new crmCollectionLeadTriggerHandler ();
    obj.beforeInsert (Trigger.New);
}