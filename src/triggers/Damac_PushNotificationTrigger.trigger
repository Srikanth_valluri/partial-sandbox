trigger Damac_PushNotificationTrigger on Push_Notifications__c (after insert) {
    Set <ID> recordIds = new Set <ID> ();
    List <ID> firebaseRecordIds = new List <ID> ();
    for (Push_Notifications__c rec :Trigger.New) {
        if(rec.Firebase__c)           
            firebaseRecordIds.add(rec.Id);     
        else
            recordIds.add(rec.Id);
    }
    if(!firebaseRecordIds.isEmpty())
        DAMAC_PUSH_NOTIFICATION.sendNotificationaAsync(firebaseRecordIds);
    if(!recordIds.isEmpty())
        Damac_PushNotifications.insertPushMessage (recordIds);
}