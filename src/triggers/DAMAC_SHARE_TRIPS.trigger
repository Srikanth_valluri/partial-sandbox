trigger DAMAC_SHARE_TRIPS on Trip__c (after insert,after update) {

    list<trip__share> shareRecordstoInsert = new list<trip__share>();
    for(trip__c i:[SELECT Driver_id__c, Coordinator_id__c FROM Trip__c WHERE ID IN :trigger.new]){
        System.Debug(i);
        // Share to Driver
        if(i.Driver_id__c != NULL){
            trip__share shareTripDriver = new trip__share();        
            shareTripDriver.ParentId = i.id; 
            shareTripDriver.UserOrGroupId = i.Driver_id__c;
            shareTripDriver.AccessLevel = 'edit';
            shareTripDriver.RowCause = Schema.Trip__share.RowCause.Apex_Sharing__c;
            shareRecordstoInsert.add(shareTripDriver);
        }
        // Share to Coordinator
        if(i.Coordinator_id__c != NULL){
            trip__share shareTripCoordinator = new trip__share();        
            shareTripCoordinator.ParentId = i.id; 
            shareTripCoordinator.UserOrGroupId = i.Coordinator_id__c ;
            shareTripCoordinator.AccessLevel = 'edit';
            shareTripCoordinator.RowCause = Schema.Trip__share.RowCause.Apex_Sharing__c;
            shareRecordstoInsert.add(shareTripCoordinator);
        }
    }
    if(shareRecordstoInsert.size()>0){
        try {
            insert shareRecordstoInsert;
        } catch (Exception e) {}
    }
}