trigger CheckNexmoValidation on Nexmo_ZeroBounce_Validity__c (after insert) {

    Set <ID> recIds = new Set <ID> ();
    for (Nexmo_ZeroBounce_Validity__c rec :Trigger.NEW) {
        recIds.add (rec.ID);
    }
    ValidateEmailAndNumber.validateData (recIds);
}