<apex:page standardController="New_Step__c" extensions="MidOfficeApprovalController" >
    <style>
        /* This is for the full screen DIV */
        .overlay {
            background-color: black;
            /*cursor: wait;*/
            opacity: 0.6;
            /* Generic for Major browsers */ 
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)"; 
            /* IE 5-7 */
            filter: alpha(opacity = 60);
            /* Netscape */
            -moz-opacity: 0.6;
            /* Safari 1.x */
            -khtml-opacity: 0.6;
            position: fixed;
            width: 100%; 
            height: 100%;
            top: 0;
            left: 0;
            z-index: 101;
            zIndex: 101;
        }

        /* This is for the message DIV */
        .contentDiv{
            position: absolute;
            z-index: 102; 
            top: 0px; 
            margin-left: 8%;
            margin-top:5%;
            min-wdth: 500px !important;
        }

        td {
            margin-top: 6px  !important;
            margin-left: 10px  !important;
            margin-right: 10px  !important;
            padding: 3px ;
        }

        tr {
            margin-top: 30px  !important;
        }

        .pageTitleIcon {
            background-image: url(/img/sprites/master.png);
            background-position: 0 -1202px;
            width: 32px;
            height: 32px;
        }

        .bPageBlock .labelCol{
            text-align: left !important;
        }

        #pbdiv .bPageBlock {
            min-height:250px !important;
            margin-bottom:0 !important;
            max-width:610px;
            text-align: left !important;
        }

        .Step_pageTitleIcon {
            background-image: url(/img/icon/custom51_100/presenter32.png);
            background-position: 0 0;
            width: 32px;
            height: 32px;
        }

        .ActnHeaderCol{
            text-align:center;
        }

        .bEditBlock .pbHeader > table > tbody > tr > td, .bPageBlock .pbHeader > table > tbody > tr > td, 
        .bLayoutBlock .pbHeader > table > tbody > tr > td, 
        .bEditBlock .pbBottomButtons > table > tbody > tr > td, 
        .bPageBlock .pbBottomButtons > table > tbody > tr > td, 
        .bLayoutBlock .pbBottomButtons > table > tbody > tr > td{
            padding: 15px 12px;
        }

        .message .messageText h4 {
            font-weight: bold;
            display: block;
            font-size: 14px;
            line-height: 0;
            margin: 5px 0;
        }
    </style>
    <style type="text/css">
        * {
            font-family:Verdana, Arial, Helvetica, sans-serif;
            font-size:11px;
            opacity: 1; 
        }

        a:hover {
            background:#ffffff; 
            text-decoration:none;
            opacity: 1;
        } /*BG color is a must for IE6*/

        a.tooltip span {
            display:none; 
            padding:2px 3px; 
            margin-left:8px; 
            width:130px;
            opacity: 1;
        }

        a.tooltip:hover span{
            display:inline;
            position:absolute;
            border:1px solid #cccccc;
            background:#ffffff;
            color:#6c6c6c;
            opacity: 1;
        }
    </style>
    <apex:form id="frm">
        <apex:outputField value="{!New_Step__c.Service_Request__r.Is_VAT_SR__c}" rendered="false" />
        <apex:detail subject="{!New_Step__c.Service_Request__c}"/>
        <div class="overlay"></div>
        <apex:outputPanel styleClass="contentDiv" rendered="{! !canEditRecord}">
            <div id="noEditpbdiv">
                <apex:pageBlock >
                    <apex:pageBlockButtons >
                        <apex:commandButton value="Cancel" action="{!cancelUpdate}" immediate="true" />
                    </apex:pageBlockButtons>

                    <apex:pageBlockSection >
                        <apex:pageBlockSectionItem >
                            <b>You dont have rights to edit this Step.</b>
                        </apex:pageBlockSectionItem>
                    </apex:pageBlockSection>
                </apex:pageBlock>
            </div>
        </apex:outputPanel>

        <apex:outputPanel styleClass="contentDiv" rendered="{! canEditRecord}">
            <div id="pbdiv">
                <apex:pageMessages />
                <apex:pageBlock >
                    <apex:pageBlockButtons location="bottom">
                        <apex:commandButton value="Cancel" action="{!cancelUpdate}" immediate="true" />
                        <apex:commandButton value="Update Checklist" action="{!updateStatus}" rendered="{!NOT(New_Step__c.Is_Closed__c)}" />
                    </apex:pageBlockButtons>
                    <br/> 
                    <table>
                        <tr>
                            <td style="min-width: 30px !important;"> </td>
                            <td style="min-width: 360px !important;"> </td>
                            <td style="min-width: 80px !important;"> </td>
                        </tr>
                        <tr>
                            <td> <h2>Sl No. </h2></td>
                            <td> <h2>Reservation Form Verification Check-List </h2></td>
                            <td> <h2>Details Match </h2></td>
                        </tr>
                        <tr>
                        <td> </td><td> </td><td> </td>
                        </tr>
                        <!---------- normal check list items---------------->
                        <apex:repeat value="{!prioritySet}" var="priority">
                            <apex:outputpanel rendered="{!IF(prioritySubheadingMap[priority] ='Others', true, false)}"> 
                                <apex:repeat value="{!checkListPriorityMap[priority]}" var="item">
                                    <tr>
                                        <td> &nbsp;&nbsp;{!priority} </td>
                                        <td>
                                            <apex:outputText value=" {!item}"/>
                                        </td>
                                        <td> 
                                            <apex:selectList value="{!checkListMap[item]}" size="1" required="true" >
                                                <apex:selectOptions value="{!checklistOption}" />
                                            </apex:selectList>
                                         </td>
                                    </tr>
                                </apex:repeat>
                            </apex:outputpanel> 
                            <apex:outputpanel rendered="{!IF(prioritySubheadingMap[priority] ='Others', false, true)}"> 
                                <!---------- subheading---------------->
                                <tr>
                                    <td> &nbsp;&nbsp;{!priority} </td>
                                    <td> <apex:outputtext value="{!prioritySubheadingMap[priority]}"/></td>
                                    <td>  </td>
                                </tr>
                                <tr/>
                                <!---------- check list items having subheading---------------->
                                <apex:repeat value="{!checkListPriorityMap[priority]}" var="item">
                                    <tr>
                                        <td></td>
                                        <td>&nbsp;&nbsp;{!priority}.{!prioritySubheadingMap[item]}&nbsp;&nbsp; <apex:outputText value=" {!item}"/> </td>
                                        <td> 
                                            <apex:selectList value="{!checkListMap[item]}" size="1" required="true" >
                                                <apex:selectOptions value="{!checklistOption}" />
                                            </apex:selectList>
                                         </td>
                                    </tr>
                                </apex:repeat>
                            </apex:outputpanel> 
                         </apex:repeat>
                         <tr/>
                     </table> <br/>
                     <table>
                         <tr>
                            <td> Comments  &nbsp; &nbsp; &nbsp;</td>
                            <td > 
                            <apex:inputField value="{!New_Step__c.Comments__c}" style="min-width: 250px !important;"  /> </td>
                        </tr>
                    </table>
                </apex:pageBlock>
            </div>
        </apex:outputPanel>
    </apex:form>
</apex:page>