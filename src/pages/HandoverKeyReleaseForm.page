<!--
  Ver       Date            Author      		    Modification
  1.0    10/3/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version 
-->
<apex:page id="pageId" standardController="Booking_Unit__c" extensions="HandoverKeyReleaseFormExtension"
    sidebar="false" showHeader="false" standardStylesheets="false">
    
    <title>Key Release Form</title>
    
    <link rel='shortcut icon' href='favicon.ico' type='image/x-icon'/ >
    <apex:stylesheet value="{!URLFOR($Resource.Bulk_SOA, 'css/bootstrap.min.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.Bulk_SOA, 'css/style.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.assets, 'assets/css/bootstrap.min.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.assets, 'assets/css/style.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.assets, 'assets/css/bootstrap-datepicker.min.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.PortalAssets, 'assets/css/bootstrap-multiselect.css')}"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>      
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/>
    <script src="https://use.fontawesome.com/0d38fcfa41.js"></script>
    <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css"/>

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900"/>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900"/>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900"/>

    <style>
        #filter {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: #ffffff;
            z-index: 10;
            opacity: 0.6;
            filter: alpha(opacity=50);
        }

        .sideBarFixed {
            position: absolute!important;
        }

        .fontColor {
            color: black;
        }

        .main-content-section {
            padding: 10px 0 10px 10px;
            height: 100vh;
        }
        
        .formatNumber {
            text-align: right !important;
        }
        
        .titleCell {
            font-weight: 700;
            text-align: right;
        }
        .keyCell{
            font-weight: 700;
        }
        input[type=checkbox], input[type=radio] {
            width: 10%;
        }
        .autoPopulateVal{
            height: 60px;margin-top: 2%;box-shadow: 10px 10px 20px -15px;
        }
        .rowMarginShadow{
            margin-top: 1%;box-shadow: 10px 10px 20px -15px;
        }
        .dateFormat{display: none;}
    </style>
    
    
    <apex:form id="formId">
    <div id="filter"></div>
    <apex:actionStatus id="blockUI" onStart="startProcess();" onStop="endProcess();"/>
    <apex:image url="{!URLFOR($Resource.Spinner,'')}" id="loadingdetail" style="width:50px;height:50px;display:none;position:fixed;top:50%;left:50%;z-index:10;"/>
    <apex:outputpanel id="hiddenConferencePanel">
        <apex:inputHidden value="{!keyDocName}" id="KeyReleaseName"/>
        <apex:inputHidden value="{!keyDocBody}" id="KeyReleaseBody"/>

        <apex:inputHidden value="{!keyDocNamePassport}" id="KeyReleaseNamePassport"/>
        <apex:inputHidden value="{!keyDocBodyPassport}" id="KeyReleaseBodyPassport"/>
    </apex:outputpanel>

    <apex:actionFunction name="saveDraftActionFunction" action="{!saveDraft}" rerender="savePanel" status="blockUI"/>
    <apex:actionFunction name="saveActionFunction" action="{!save}" rerender="pgMsg, pgMsgFooter" status="blockUI"/>
    
    <apex:actionFunction name="uploadFileToServerActionFunction" action="{!uploadFileToServer}" rerender="hiddenConferencePanel, savePanel, pgMsg, pgMsgFooter" status="blockUI" />


    <apex:pageMessages id="pgMsg"/>
    
    <section class="mainBox container"> 
        <div class="logoBox">
            <img src="{!URLFOR($Resource.Bulk_SOA,'img/Damac_logo.png')}" class="logoImg" />
        </div>
        <hr/>
        <!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
        <apex:outputpanel styleClass="mainContentBox" id="transDetails">
            <div class="logoBox rowMarginShadow">
                <h4> Client Acknowledgment Form </h4>
            </div>
            
        
            <div class="row autoPopulateVal">
                <div class="col-lg-1"></div>
                <div class="col-lg-5">
                    <label>Unit Name : {!selectedUnit.Unit_Name__c}</label>
                </div>
                
                <div class="col-lg-6">
                    <label>Customer Name : {!selectedUnit.Primary_Buyer_s_Name__c}</label>
                </div>
            </div>
            
            <div class="row rowMarginShadow well" >
                <div class="col-md-10">
                    <label class="headerLabel">Authorized Personnel Details : </label>
                    
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td class="titleCell" >
                                        <apex:outputText value="Name"/>
                                    </td>
                                    <td> 
                                        <apex:inputField value="{!selectedUnit.Authorized_Personnel_Name__c}" styleClass="form-control"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="titleCell" >Phone Number</td>
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Authorized_Personnel_Phone_Number__c}" styleClass="form-control"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="titleCell" >Email</td>
                                    <td>
                                        <apex:inputField styleClass="form-control" value="{!selectedUnit.Authorized_Personnel_Email__c}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="titleCell" >Nationality</td>
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Authorized_Personnel_Nationality__c}" styleClass="form-control"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="titleCell" >Passport Number</td>
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Authorized_Personnel_Passport_Number__c}" styleClass="form-control"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="titleCell" >Key Release Date</td>
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Key_Release_Date__c}" styleClass="form-control datepickerelement"/>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
            
            <!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
            
            <div class="row rowMarginShadow" >

                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th  >
                                    <apex:outputText value="Particulars"/>
                                </th>
                                <th >
                                    <apex:outputText value="Quantity"/>
                                </th>
                                <th >
                                    <apex:outputText value="Received"/>
                                </th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="keyCell">
                                        <apex:outputText value="Main door keys" />
                                    </td>                           
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Main_door_keys_Quantity__c}" styleClass="form-control"/>
                                    </td>
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Main_door_keys_Received__c}" styleClass="form-control"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="keyCell">
                                        <apex:outputText value="Main door card number (IA)" />
                                    </td>                           
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Main_door_card_number_Quantity__c}" styleClass="form-control"/>
                                    </td>
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Main_door_card_number_Recieved__c}" styleClass="form-control"/>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="keyCell">
                                        <apex:outputText value="Internal door keys" />
                                    </td>                           
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Internal_door_keys_Quantity__c}" styleClass="form-control"/>
                                    </td>
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Internal_door_keys_Received__c}" styleClass="form-control"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="keyCell">
                                        <apex:outputText value="Appliances manuals" />
                                    </td>                           
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Appliances_manuals_Quantity__c}" styleClass="form-control"/>
                                    </td>
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Appliances_manuals_Received__c}" styleClass="form-control"/>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="keyCell">
                                        <apex:outputText value="Parking Access Cards" />
                                    </td>                           
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Parking_Access_Cards_Quantity__c}" styleClass="form-control"/>
                                    </td>
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Parking_Access_Cards_Recieved__c}" styleClass="form-control"/>
                                    </td>
                                </tr>                           

                                <tr>
                                    <td class="keyCell">
                                        <apex:outputText value="Parking access card no. (IA)" />
                                    </td>                           
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Parking_access_card_no_Quantity__c}" styleClass="form-control"/>
                                    </td>
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Parking_access_card_no_Recevied__c}" styleClass="form-control"/>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="keyCell">
                                        <apex:outputText value="Building Access (IA)" />
                                    </td>                           
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Building_Access_Quantity__c}" styleClass="form-control"/>
                                    </td>
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Building_Access_Recieved__c}" styleClass="form-control"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="keyCell">
                                        <apex:outputText value="Building access card no. (IA)" />
                                    </td>                           
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Building_access_card_no_Quantity__c}" styleClass="form-control"/>
                                    </td>
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Building_access_card_no_Recieved__c}" styleClass="form-control"/>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="keyCell">
                                        <apex:outputText value="Handover Pack with Home Owner’s Manuals" />
                                    </td>                           
                                    <td>
                                        <apex:inputField value="{!selectedUnit.HO_Pack_with_Home_Owner_s_Manual_Quantiy__c}" styleClass="form-control"/>
                                    </td>
                                    <td>
                                        <apex:inputField value="{!selectedUnit.HO_Pack_with_Home_Owners_Manual_Received__c}" styleClass="form-control"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="keyCell">
                                        <apex:outputText value="SRF Signed off" />
                                    </td>                           
                                    <td>
                                        <apex:inputField value="{!selectedUnit.SRF_Signed_off_Quantity__c}" styleClass="form-control"/>
                                    </td>
                                    <td>
                                        <apex:inputField value="{!selectedUnit.SRF_Signed_off_Received__c}" styleClass="form-control"/>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="keyCell">
                                        <span style="color:red;">*</span>
                                        <apex:outputText value="Handover Executive Name" />
                                    </td>                           
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Handover_Executive_Name__c}" styleClass="form-control mandatory" onChange="validateSubmission(false, true);"/>
                                    </td>
                                </tr>                                
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
            
            
            <!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
            
            <div class="row rowMarginShadow" >

                <div class="col-md-10" style="margin-top: 2% !important; margin-left: 3% !important;">
                    
                    <label class="headerLabel"> Authorization To Access Property For Snags Rectification : </label>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td class="titleCell" >Snag Rectification</td> 
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Snag_Rectification__c}" styleClass=""/>
                                    </td> 
                                </tr>
                                <tr>
                                    <td class="titleCell" >Painting/ Cleaning</td>                          
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Painting_Cleaning__c}" styleClass=""/>                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td class="titleCell" >Number of Keys Provided</td> 
                                    <td>
                                        <apex:inputField value="{!selectedUnit.Number_of_Keys_Provided__c}" styleClass="form-control"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
            
            
            <!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
            
            
            <apex:outputPanel layout="block" id="KeyReleaseBlock">
                <section id="" style="Margin-top:1%;box-shadow: 10px 10px 20px -15px;">
                    <div class="well">

                        <div class="row">
                            <div class="col-md-6" style="text-align:left;">
                                <p style="font-weight:bold; font-color:black;">Please ensure that the file size is less than 2 MB.</p>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-3" style="text-align:left;">
                            <apex:commandButton value="Generate Client Ack Form" action="{!generateDoc}"
                                styleClass="btn btn-sm btn-success" reRender="pgMsg,transDetails" 
                                status="blockUI"/>
                            </div>
                            <div class="col-md-6" style="text-align:left;">
                                <h4>Upload Signed Client Acknowledgment Document</h4>
                            </div>
                        </div>
                        
                        
                        <apex:outputText rendered="{!isClientAckFormGen}">
                            <div class="row">
                                <div class="col-md-12" style="text-align:left;">
                                    <b> Success: Please wait for 5 minutes, system is generating Client Acknowledgment Letter.</b> <br /></div>
                                </div>
                        </apex:outputText>
                        
                        <apex:outputText rendered="{!isError}">
                            <div class="row">
                                <div class="col-md-12" style="text-align:left;">
                                    <b> Warning: You can not generate Client Acknowledgment Letter, as this unit is not marked as okay to key release.</b> <br /></div>
                                </div>
                        </apex:outputText>
                        
                        <apex:outputText rendered="{!selectedUnit.Is_Client_Acknowledgment_Form_Generated__c}">
                            <apex:outputlink value="{!selectedUnit.Value__c}" target="_Blank" >
                                {!selectedUnit.Value__c}
                            </apex:outputlink>
                        </apex:outputText>
                    
                    
                    
                    
                        <div class="row">
                            <div class="col-lg-12 col-md-6 col-xs-12 text-center">
                                <apex:outputPanel layout="block" id="KeyBlock">
                                    <apex:outputPanel layout="block" rendered="{!NOT(ISNULL(objKeyAttach))}">
                                        <table class="paymentDue" cellpadding="10" width="100%">
                                            <col width="20%"/>
                                            <col width="20%"/>
                                            <col width="20%"/>
                                            <col width="20%"/>
                                            <thead>
                                                <tr>
                                                    <!--<th style="text-align: center;" class="temp">Action</th>-->
                                                    <th style="text-align: center;">Document Name</th>
                                                    <th style="text-align: center;">Type</th>
                                                    <th style="text-align: center;">Verified</th>
                                                    <th style="text-align: center;">View</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <!--<td class="temp"> 
                                                        <apex:commandLink onclick="if(!confirm('Are you sure?')) return false;" reRender="tempBlock,CRFAttachmentBlock" onComplete="notificationFunction();" status="blockUI"> Del
                                                            <apex:param value="{!objKeyAttach.Id}" name="recToDel2" assignTo="{!deleteAttRecId}"/>
                                                        </apex:commandLink>
                                                    </td>-->
                                                    <td><apex:outputField value="{!objKeyAttach.Name}"/></td>
                                                    <td><apex:outputField value="{!objKeyAttach.Type__c}"/></td>
                                                    <td><apex:outputField value="{!objKeyAttach.isValid__c}"/></td>
                                                    <td><apex:outputLink value="{!objKeyAttach.Attachment_URL__c}" target="_blank">VIEW</apex:outputLink></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </apex:outputPanel>
                                    <apex:outputPanel layout="block" rendered="{!ISNULL(objKeyAttach)}">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn btn-primary btn-file">
                                                <span class="fileupload-new"><i class="fa fa-address-card-o" aria-hidden="true"></i><br/>Upload Signed Client Acknowledgment Document<span style="color:red;font-size: initial;">*</span></span>
                                                <span class="fileupload-exists"><i class="fa fa-file-pdf-o" aria-hidden="true"></i><br/>Change</span>
                                                <input type="file" id="KeyAttach" onchange="attachKeyReleaseFile();"/>
                                            </span>
                                            <div>
                                                <span class="fileupload-preview"></span>
                                                <a href="#" class="close fileupload-exists required" data-dismiss="fileupload" style="float: none">×</a>
                                            </div>
                                        </div>
                                    </apex:outputPanel>
                                </apex:outputPanel>
                            </div>
                        </div>
                    </div>
                </section>
            </apex:outputPanel>
            
            <apex:outputPanel layout="block" id="KeyReleaseIdDocumentBlock">
                <section id="" style="Margin-top:1%;box-shadow: 10px 10px 20px -15px;">
                    <div class="well">
                    
                        <div class="row">
                            <div class="col-lg-12 col-md-6 col-xs-12 text-center">
                                <div class="col-md-3" style="text-align:left;">
                                <!--<apex:commandButton value="Generate Client Ack Form" action="{!generateDoc}"
                                    styleClass="btn btn-sm btn-success" reRender="pgMsg,transDetails" 
                                    status="blockUI"/>-->
                                </div>
                                <div class="col-md-6" style="text-align:left;">
                                    <h4>Upload Passport/ID</h4>
                                </div>
                                 
                        
                                <apex:outputPanel layout="block">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn btn-primary btn-file">
                                            <span class="fileupload-new"><i class="fa fa-address-card-o" aria-hidden="true"></i><br/>Upload Passport/ID<span style="color:red;font-size: initial;">*</span></span>
                                            <span class="fileupload-exists"><i class="fa fa-file-pdf-o" aria-hidden="true"></i><br/>Change</span>
                                            <input type="file" id="KeyAttachPassport" onchange="attachKeyReleaseFilePassport();"/>
                                        </span>
                                        <div>
                                            <span class="fileupload-preview"></span>
                                            <a href="#" class="close fileupload-exists required" data-dismiss="fileupload" style="float: none">×</a>
                                        </div>
                                        
                                    </div>
                                </apex:outputPanel>
                            </div>
                        </div>
                    </div>
                </section>
            </apex:outputPanel>
            
            
            
            <apex:outputPanel id="savePanel">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4" style="padding-left: 8%; margin-bottom: 2%; text-align:center; color: red; font-weight: bold;">
                        <apex:pageMessages id="pgMsgFooter" />
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <div class="row">
                    <div class="col-md-4" style="text-align:center;" >
                        <apex:commandButton value="Save as Draft" onclick="saveDraftActionFunction();" 
                            status="blockUI" styleClass="btn btn-sm btn-success saveAsDraftBtn" 
                            reRender="pgMsg, pgMsgFooter" style="margin-left: 100px;margin-right:2px; " />
                    </div>
                    <div class="col-md-4" style="text-align:center;">
                        <apex:commandButton value="Save" 
                            styleClass="btn btn-sm btn-success submitBtnOne" reRender="pgMs" 
                            status="blockUI" rendered="{!isSaveAsDraft == false}" disabled="true"/>
                        <apex:commandButton value="Save" onclick="validateSubmission(false, true); addpicker();"
                            styleClass="btn btn-sm btn-success submitBtn" reRender="pgMsg, validateMandatoryPanel"
                            status="blockUI" rendered="{!isSaveAsDraft == true}" />
                    </div>
                    <div class="col-md-4" style="text-align:center;">
                    <apex:commandButton value="Back" action="{!back}"
                        styleClass="btn btn-sm btn-success" reRender="transDetails" 
                        status="blockUI"/>
                    </div>
                    
                </div>
            </apex:outputPanel>
            <br/>
        </apex:outputpanel>
    </section>

    </apex:form>
    
    <apex:stylesheet value="{!URLFOR($Resource.FundTransferResource,'All_Processes/assets/css/bootstrap-chosen.css')}"/>
    
    <apex:includeScript value="{!URLFOR($Resource.PortalAssets, 'assets/js/jquery.js')}"/>
    <apex:includeScript value="https://code.jquery.com/ui/1.11.2/jquery-ui.js"/>

    <apex:includeScript value="{!URLFOR($Resource.PortalAssets, 'assets/js/bootstrap.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.PortalAssets, '/assets/js/bootstrap-datepicker.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.PortalAssets, 'assets/js/main.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.PortalAssets, 'assets/js/bootstrap-multiselect.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.PortalAssets, 'assets/js/upload.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.PortalAssets, 'assets/js/ease.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.NotificationStyles, 'NotificationStyles/js/modernizr.custom.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.NotificationStyles, 'NotificationStyles/js/classie.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.NotificationStyles, 'NotificationStyles/js/notificationFx.js')}"/>
    
    <script>
        $(document).ready(function() {
            $('.well input.datepickerelement').datepicker({format: 'dd/mm/yyyy', autoclose: true});
            $('.submitBtnOne').attr('disabled', 'disabled');
        });

        function validateSubmission(dontCall, disableMessages) {
            if (dontCall) {
                return false;
            }
            
            if(validateMandatoryFields(disableMessages)) {
                console.log('returned true');
                $('.submitBtn').attr('disabled', false);
                return true;
            }
            else {
                console.log('returned false');
                $('.submitBtn').attr('disabled', true);
                return false;
            }
        }

        function validateMandatoryFields(disableMessages) {
            var counter = 0;
            console.log('..counter 1 ',counter);
            $('.mandatory').each(function(){
                if( $(this).val() ) {
                    console.log('Inside this');
                    counter++ ;
                    console.log('..counter 2 ',counter);
                } else {
                    if (!disableMessages) {
                        var fieldName = ($(this).closest('.form-group').find('label.formLabel').text() || '').replace(' *', '');
                        toastr.error('Please fill in the ' + fieldName);
                        return false;
                    }
                }
            });
            console.log('..counter 3 ',counter);
            console.log('..mandatory len ',$('.mandatory').length);
            if( counter == $('.mandatory').length ) {
                return true
            }
        }

        function addpicker(){
            saveActionFunction();
            $('.well input.datepickerelement').datepicker({ format: 'dd/mm/yyyy', autoclose: true });    
        }
        
        function startProcess() {
            document.getElementById('{!$Component.pageId.formId.loadingdetail}').style.display = 'block';
            document.getElementById('filter').style.display = 'block';
        }

        function endProcess() {
            document.getElementById('{!$Component.pageId.formId.loadingdetail}').style.display = 'none';
            document.getElementById('filter').style.display = 'none';
        }
        
        function attachKeyReleaseFile() {
            console.log( '...attachKeyReleaseFile: ', document.getElementById("KeyAttach").files[0] );
            var filename = document.getElementById("KeyAttach").value;
            if( filename != '' ) {
                $('[id$=KeyReleaseName]').val(filename);
                var fbody = document.getElementById("KeyAttach").files[0];
                var reader = new FileReader();
                reader.readAsDataURL(fbody);
                reader.onload = function(e) {
                    var blobfile = window.btoa(this.result);
                    $('[id$=KeyReleaseBody]').val(blobfile);
                }
            }
            callUploadAction();
        }
        function attachKeyReleaseFilePassport() {
            console.log('...attachKeyReleaseFile: ', document.getElementById("KeyAttachPassport").files[0]);
            var filename = document.getElementById("KeyAttachPassport").value;
            console.log('...filename: ', filename);
            if (filename != '') {
                $('[id$=KeyReleaseNamePassport]').val(filename);
                var fbody = document.getElementById("KeyAttachPassport").files[0];
                var reader = new FileReader();
                reader.readAsDataURL(fbody);
                reader.onload = function (e) {
                    var blobfile = window.btoa(this.result);
                    $('[id$=KeyReleaseBodyPassport]').val(blobfile);
                }
            }

            callUploadAction();
        }

        function callUploadAction() {
            window.setTimeout(function () {
                uploadFileToServerActionFunction();
            }, 1000);
            
        }
    </script>
    
</apex:page>