<apex:page showHeader="false" cache="true" standardStylesheets="false" controller="DAMAC_INVENTORY_MOBILE" readOnly="true" action="{!initProjects}">
    
    <html lang="en">
        
        <head>
            <title>Damac Projects</title>
            <meta charset="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            <meta name="theme-color" content="#b4a483"/>
            
            <!-- SEO -->
            <meta name="author" content="Damac App"/>
            <meta name="description" content="Damac App"/>
            <meta name="keywords" content="Damac App"/>
            
            <link rel="shortcut icon" href="{!URLFOR($Resource.Damac_MobileApp, '/assets/images/favicon.png')}" type="image/png"/>
            <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700,800&display=swap" rel="stylesheet" />
            <link rel="stylesheet" type="text/css" href="{!URLFOR($Resource.Damac_MobileApp, 'assets/icons/damac_icons.css')}" />
            <link href="{!URLFOR($Resource.Damac_MobileApp, 'assets/css/select2.min.css')}" rel="stylesheet" />
            <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
            <link rel="stylesheet" href="{!URLFOR($Resource.Damac_MobileApp, 'assets/css/jquery.scrolling-tabs.css')}"/>
            <link rel="stylesheet" href="{!URLFOR($Resource.Damac_MobileApp, 'assets/css/damac.css')}"/>
            <style>
                #loading-img {
                    background: url({!URLFOR($Resource.Damac_MobileApp, '/loading.gif')}) center center no-repeat;
                    height: 105%;
                    z-index: 20;
                }
                .blurBackground {
                    background :#d3d3d329 !important;
                }
                
                .overlay {
                background: #e9e9e9;
                display: none;
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                opacity: 0.5;
                }
            </style>
        </head>
        
        <body>
            
            <header class="main_navbar">
                <ul class="d_flex justify_content_between align_items_center">
                   <!-- <li>
                        <a href="/apex/Inventory_Mobile">
                            <i class="home"></i>
                            <span>Home</span>
                        </a>
                    </li>-->
                    <li class="active">
                        <a href="/apex/Inventory_Projects">
                            <i class="project"></i>
                            <span>Projects</span>
                        </a>
                    </li>
                    <li >
                        <a href="/apex/Inventory_Mobile">
                            <i class="units"></i>
                            <span>Units</span>
                        </a>
                    </li>
                    <li>
                        <a href="/apex/Inventory_Mobile_Favorites">
                            <i class="save"></i>
                            <span>Saved</span>
                        </a>
                    </li>
                 <!--   <li>
                        <a href="#">
                            <i class="menu"></i>
                            <span>Menu</span>
                        </a>
                    </li>-->
                </ul>
            </header>
            
            <div class="overlay">
                <div id="loading-img"></div>
            </div>
            <div class="top_navigation d_flex justify_content_between align_items_center">
                <div class="main_search_icon"><i class="icon-search"></i></div>
                <h1 class="main_title">Projects</h1>
                <div class="top_filter"></div>
                <div class="main_search_wrp">
                    <input type="text" class="form_control searchbox" id="searchBox" />
                    <div class="btn icon_search">
                        <i class="icon-search"></i>
                    </div>
                    <div class="btn main_search_wrp_close" onclick="searchProjects('');return false;">
                        <i class="icon-close"></i>
                    </div>
                </div>
            </div>
            <apex:form >
                <main>
                    <div class="main_wrapper">
                        <!--<div class="select_wrapper">
                            <select name="" class="select-location" id="selectedLocation" onchange="showSpinner();projectLocations(this.value); return false;">
                                <apex:repeat value="{!marketingLocations}" var="loc">
                                    <option value="{!loc}" data-description="{!marketingLocations[loc]} projects" 
                                            data-icon="{!URLFOR($Resource.Damac_MobileApp, '/assets/images/location_icon.svg')}">
                                        {!loc}
                                    </option>
                                </apex:repeat>
                                
                            </select>
                        </div>-->
                        <div class="unit_list">
                            <apex:outputPanel id="projectsPanel">
                                <apex:repeat value="{!projectsList}" var="proj">
                                    
                                    <div class="white_box unit_item" onclick="navigate('/apex/Inventory_Projects_details?projId={!proj.projId}')">
                                        
                                        <div class="units_thumb">
                                            <apex:image value="{!proj.marketingImage}"/>
                                        </div>
                                        <div class="unit_item_spec_list d_flex justify_content_between align_items_center">
                                            
                                            <div>
                                                
                                                <p class="main_title">{!proj.marketingName}</p>
                                                <p class="item_title unit_location d_flex align_items_center">
                                                    <span>{!proj.country}</span>
                                                    <span>{!proj.City}</span>
                                                </p>
                                                <p class="gold_color item_title">
                                                    {!proj.avaliableUnitsCount} units available
                                                </p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </apex:repeat>
                            </apex:outputPanel>
                        </div>
                    </div>
                </main>
                <apex:actionFunction name="projectLocations" action="{!getProjects}" reRender="projectsPanel" oncomplete="hideSpinner();">
                    <apex:param name="locName" value=""/>
                </apex:actionFunction>
                <apex:actionFunction name="searchProjectsActn" action="{!getProjects}" reRender="projectsPanel" oncomplete="hideSpinner();">
                    <apex:param name="locName" value=""/>
                    <apex:param name="docName" value=""/>
                </apex:actionFunction>
                <a href="" id="anchor" style="display:none"/>
            </apex:form>
            <script src="{!URLFOR($Resource.Damac_MobileApp, '/assets/js/jquery.min.js')}"></script>
            <script src="{!URLFOR($Resource.Damac_MobileApp, '/assets/js/bootstrap.min.js')}"></script>
            <script src="{!URLFOR($Resource.Damac_MobileApp, '/assets/js/jquery.scrolling-tabs.js')}"></script>
            <script src="{!URLFOR($Resource.Damac_MobileApp, '/assets/js/select2.min.js')}"></script>
            <script src="{!URLFOR($Resource.Damac_MobileApp, '/assets/js/custom.js')}"></script>
            
            <script>
            //Scrolling tab function
            (function () {
                'use strict';
                $(activate);
                function activate() {
                    $('.nav-tabs').scrollingTabs({
                        enableSwiping: true,
                        bootstrapVersion: 4,
                        scrollToTabEdge: true,
                        disableScrollArrowsOnFullyScrolled: true
                        
                    }).on('ready.scrtabs', function () {
                        $('.nav-tabs').click(function () {
                            setTimeout(function () {
                                $('.nav-tabs').scrollingTabs('scrollToActiveTab');
                            }, 110);
                        });
                    });
                }
            }());
            function showSpinner () {
                $(".overlay").show();
                $('.white_box').each(function() {
                    $(this).addClass ('blurBackground');
                });
            }
            function hideSpinner () {
                $(".overlay").hide();
                $('.white_box').each(function() {
                    $(this).removeClass ('blurBackground');
                });
            }
            function formatText (icon) {
                var result = $('<span class="select_item main_title">'
                               +'<img src="'+$(icon.element).data("icon")+'"/>'
                         + icon.text 
                         +'<span class="select_description">(' + $(icon.element).data('description') + ')</span>'
                         +'</span>');
                return result;
            }
            $('.select-location').select2({
                width: "100%",
                templateSelection: formatText,
                templateResult: formatText,
                minimumResultsForSearch: -1
            }); 
            
            $('.searchbox').keyup(function(event) {
                if (event.keyCode === 13) {
                    return false;
                } else {
                    searchProjects ('');
                }
            });           
            function searchProjects(val) {
                showSpinner ();
                var loc = $('#selectedLocation').val();
                setTimeout(function() {
                    var value = document.getElementById ('searchBox').value;
                    searchProjectsActn (loc, value);
                }, 1000);
            }
            function navigate (url) {
                console.log(url);
                $('#anchor').attr ('href', url);
                document.getElementById('anchor').click();
            }
            </script>
        </body>
        
    </html>
</apex:page>