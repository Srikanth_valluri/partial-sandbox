<!--***********************************************************************************************
* Name               : ContactInformation                                                         *
* Description        : This page is to contain contact information.                               *
* Created Date       : 08/03/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR        DATE          DESCRIPTION                                             *
* 1.0         NSI - Vineet  08/03/2017    Initial Draft                                           *
************************************************************************************************-->
<apex:page standardController="Inquiry__c" >  
    <style>
        .labelMargin{
            margin-left: 78px;
        }
        .labelText{
            margin-left: 10px;
        }
    </style>
    <apex:actionStatus id="pleasewait" stopText="">
        <apex:facet name="start">
            <div>
                <div class="popupBackground"></div>
                <div class="PopupPanel">
                    <img src="{!$Resource.LoadingImage}"/>
                </div>
            </div>
        </apex:facet>
    </apex:actionStatus>
    <apex:form >
        <apex:pageMessages />
        <apex:pageBlock mode="maindetail">
            <apex:pageBlockSection columns="1" >
                <apex:outputPanel rendered="{!Inquiry__c.Mobile_Phone__c != null}">
                    <apex:outputLabel value="Mobile Phone 1" for="mobilePhone1" styleClass="labelCol labelMargin"/>
                    <apex:outputLink value="hjavascript:void(0);" onclick="window.open('https://www.damacproperties.com/en');">
                        <apex:outputText value="{!Inquiry__c.Mobile_CountryCode__c}-XXXXX-{!RIGHT(Inquiry__c.Mobile_Phone__c, 2)}" id="mobilePhone1"/>
                        <apex:image value="/img/btn_dial_inline.gif" styleClass="labelText" height="11px"/>
                    </apex:outputLink>
                </apex:outputPanel>
                <apex:outputPanel rendered="{!Inquiry__c.Mobile_Phone__c == null}">
                    <apex:outputLabel value="Mobile Phone 1" for="mobilePhone1_ip" styleClass="labelCol labelMargin"/>
                    <apex:inputField value="{!Inquiry__c.Mobile_CountryCode__c}" id="mobilePhone1_ip"/> 
                    <apex:inputField value="{!Inquiry__c.Mobile_Phone__c}" styleClass="labelText"/> 
                </apex:outputPanel>
                
                <apex:outputPanel rendered="{!Inquiry__c.Mobile_Phone_2__c != null}" >
                    <apex:outputLabel value="Mobile Phone 2" for="mobilePhone2" styleClass="labelCol labelMargin"/>
                    <apex:outputLink value="hjavascript:void(0);" onclick="window.open('https://www.damacproperties.com/en');">
                        <apex:outputText value="{!Inquiry__c.Mobile_Country_Code_2__c}-XXXXX-{!RIGHT(Inquiry__c.Mobile_Phone_2__c, 2)}" id="mobilePhone2"/>
                        <apex:image value="/img/btn_dial_inline.gif" styleClass="labelText" height="11px"/>
                    </apex:outputLink>
                </apex:outputPanel>
                 <apex:outputPanel rendered="{!Inquiry__c.Mobile_Phone_2__c == null}">
                    <apex:outputLabel value="Mobile Phone 2" for="mobilePhone2_ip" styleClass="labelCol labelMargin"/>
                    <apex:inputField value="{!Inquiry__c.Mobile_Country_Code_2__c}"/>   
                    <apex:inputField value="{!Inquiry__c.Mobile_Phone_2__c}" styleClass="labelText"/>   
                </apex:outputPanel>
                
                <apex:outputPanel rendered="{!Inquiry__c.Mobile_Phone_3__c != null}" >
                    <apex:outputLabel value="Mobile Phone 3" for="mobilePhone3" styleClass="labelCol labelMargin"/>
                    <apex:outputLink value="hjavascript:void(0);" onclick="window.open('https://www.damacproperties.com/en');">
                        <apex:outputText value="{!Inquiry__c.Mobile_Country_Code_3__c}-XXXXX-{!RIGHT(Inquiry__c.Mobile_Phone_3__c, 2)}" id="mobilePhone3"/>
                        <apex:image value="/img/btn_dial_inline.gif" styleClass="labelText" height="11px"/>
                    </apex:outputLink>
                </apex:outputPanel>
                <apex:outputPanel rendered="{!Inquiry__c.Mobile_Phone_3__c == null}">
                    <apex:outputLabel value="Mobile Phone 3" for="mobilePhone3_ip" styleClass="labelCol labelMargin"/>
                    <apex:inputField value="{!Inquiry__c.Mobile_Country_Code_3__c}" />  
                    <apex:inputField value="{!Inquiry__c.Mobile_Phone_3__c}" styleClass="labelText"/>   
                </apex:outputPanel>
                
                <apex:outputPanel rendered="{!Inquiry__c.Mobile_Phone_4__c != null}">
                    <apex:outputLabel value="Mobile Phone 4" for="mobilePhone4"  styleClass="labelCol labelMargin"/>
                    <apex:outputLink value="hjavascript:void(0);" onclick="window.open('https://www.damacproperties.com/en');">
                        <apex:outputText value="{!Inquiry__c.Mobile_Country_Code_4__c}-XXXXX-{!RIGHT(Inquiry__c.Mobile_Phone_4__c, 2)}" id="mobilePhone4"/>
                        <apex:image value="/img/btn_dial_inline.gif" styleClass="labelText" height="11px"/>
                    </apex:outputLink>
                </apex:outputPanel>
                 <apex:outputPanel rendered="{!Inquiry__c.Mobile_Phone_4__c == null}">
                    <apex:outputLabel value="Mobile Phone 4" for="mobilePhone4_ip" styleClass="labelCol labelMargin"/>
                    <apex:inputField value="{!Inquiry__c.Mobile_Country_Code_4__c}" />  
                    <apex:inputField value="{!Inquiry__c.Mobile_Phone_4__c}" styleClass="labelText"/>
                </apex:outputPanel>
                
                <apex:outputPanel rendered="{!Inquiry__c.Mobile_Phone_5__c != null}">
                    <apex:outputLabel value="Mobile Phone 5" for="mobilePhone5" styleClass="labelCol labelMargin"/>
                    <apex:outputLink value="hjavascript:void(0);" onclick="window.open('https://www.damacproperties.com/en');">
                        <apex:outputText value="{!Inquiry__c.Mobile_Country_Code_5__c}-XXXXX-{!RIGHT(Inquiry__c.Mobile_Phone_5__c, 2)}" id="mobilePhone5"/>
                        <apex:image value="/img/btn_dial_inline.gif" styleClass="labelText" height="11px"/>
                    </apex:outputLink>
                </apex:outputPanel>
                 <apex:outputPanel rendered="{!Inquiry__c.Mobile_Phone_5__c == null}">
                     <apex:outputLabel value="Mobile Phone 5" for="mobilePhone5_ip" styleClass="labelCol labelMargin"/>
                    <apex:inputField value="{!Inquiry__c.Mobile_Country_Code_5__c}" />  
                    <apex:inputField value="{!Inquiry__c.Mobile_Phone_5__c}" styleClass="labelText"/>   
                </apex:outputPanel>
            </apex:pageBlockSection>
            <apex:pageBlockButtons location="bottom">
                 <apex:commandLink action="{!save}" value="Save" target="_parent" styleClass="btn" style="text-decoration:none;padding:4px;" status="status"/>
            </apex:pageBlockButtons>
        </apex:pageBlock>
    </apex:form>
</apex:page>