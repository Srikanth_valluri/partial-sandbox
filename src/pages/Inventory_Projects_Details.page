<apex:page cache="true" showHeader="false" standardStylesheets="false" controller="DAMAC_INVENTORY_MOBILE" readOnly="true" action="{!initProjectDetails}">
    <html lang="en">
        
        <head>
            <title>Damac Projects Details</title>
            <meta charset="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            <meta name="theme-color" content="#b4a483"/>
            
            <!-- SEO -->
            <meta name="author" content="Damac App" />
            <meta name="description" content="Damac App" />
            <meta name="keywords" content="Damac App" />
            
            <link rel="shortcut icon" href="{!URLFOR($Resource.Damac_MobileApp, 'assets/images/favicon.png')}" type="image/png"/>
            <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700,800&display=swap" rel="stylesheet" />
            <link rel="stylesheet" type="text/css" href="{!URLFOR($Resource.Damac_MobileApp, 'assets/icons/damac_icons.css')}"/>
            <link rel="stylesheet" href="{!URLFOR($Resource.Damac_MobileApp, 'assets/css/jquery.scrolling-tabs.css')}"/>
            <link rel="stylesheet" href="{!URLFOR($Resource.Damac_MobileApp, 'assets/css/damac.css')}"/>
            <style>
                .unit_item_img_wrp img {
                    height: 6rem;
                }
                
                
            </style>
        </head>
        
        <body class="p_0">
            <header class="main_navbar">
                <ul class="d_flex justify_content_between align_items_center">
    
                    <li class="active">
                        <a href="/apex/Inventory_Projects">
                            <i class="project"></i>
                            <span>Projects</span>
                        </a> 
                    </li>
                    <li >
                        <a href="/apex/Inventory_Mobile">
                            <i class="units"></i>
                            <span>Units</span>
                        </a> 
                    </li>
                    <li>
                        <a href="/apex/Inventory_Mobile_Favorites">
                            <i class="save"></i>
                            <span>Saved</span>
                        </a> 
                    </li>
                </ul>
            </header>
            <main>
                <apex:repeat value="{!projectsList}" var="proj">
                    <div class="project_banner" style="background-image: url({!proj.marketingImage});">
                        <a href="/apex/Inventory_projects" class="btn_icon">
                            <img src="{!URLFOR($Resource.Damac_MobileApp, '/assets/images/back.svg')}" alt="" />
                        </a>
                    </div>
                </apex:repeat>
                <div class="model_view_box">
                    <apex:repeat value="{!projectsList}" var="proj">
                        <div class="project_thumb">
                            <p class="main_title">{!proj.marketingName}</p>
                            <p class="item_title unit_location d_flex align_items_center">
                                <span>{!proj.City}</span>
                                <span>{!proj.country}</span>
                            </p>
                        </div>
                    </apex:repeat>
                    <div class="white_rds_box">
                        <ul class="nav nav-tabs d_flex align_items_center" role="tablist">
                            <li role="presentation"><a href="#tab1" class="active" role="tab" data-toggle="tab">Units</a></li>
                            <li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Project info</a></li>
                            <li role="presentation"><a href="#tab3" role="tab" data-toggle="tab">Photos &amp; plans</a></li>
                            <li role="presentation"><a href="#tab4" role="tab" data-toggle="tab">Location</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab1">
                                <div class="unit_list">
                                    <apex:variable var="favorites" value="{!''}{!favoriteInventories}" />
                                    <apex:repeat value="{!inventories}" var="inv">
                                        <a href="/apex/Inventory_Unit_details?invId={!inv.Id}&projId={!$CurrentPage.parameters.projId}">
                                            <div class="white_box unit_item">
                                                <div class="unit_item_header d_flex">
                                                    <div class="unit_item_img whishlist_wrp">
                                                        <div class="unit_item_img_wrp">
                                                            <apex:outputPanel rendered="{!inv.Marketing_Plan__c != null}">
                                                                <img src="{!inv.Marketing_Plan__c}"/>
                                                            </apex:outputPanel>
                                                            <apex:outputPanel rendered="{!inv.Marketing_Plan__c == null}">
                                                                <img src="{!URLFOR($Resource.Damac_MobileApp, '/assets/images/building_1.png')}" 
                                                                     alt="Unit"/>
                                                            </apex:outputPanel>
                                                        </div>
                                                        <div class="addto_whishlist {!if (CONTAINS(favorites, inv.Id), ' active', '')}" id="favorite" onclick="event.stopPropagation();addToFavorites('{!inv.Id}');return false"><i class="icon-wishlist"></i></div>
                                                    </div>
                                                    <div class="unit_item_details d_flex flex_column align_items_baseline">
                                                        <div class="country_label">{!inv.Property_City__c} | {!inv.Property_Country__c}</div>
                                                        <h2 class="main_title">{!inv.Property_Name__c}</h2>
                                                        <h5 class="unit_no">({!inv.Unit_Name__c})</h5>
                                                        <h5 class="unit_price"> 
                                                            <span>AED&nbsp;
                                                            
                                                                <apex:outputText value="{0,number,#,##0}">    
                                                                    <apex:param value="{!inv.Total_Price_inc_VAT__c}"/>
                                                                </apex:outputText>
                                                            </span>
                                                        </h5>
                                                    </div>
                                                    <!--<div class="unit_menu_box">
<div class="unit_menu_icon"><i class="icon-vertical_menu"></i></div>
<ul class="unit_menu">
<li><a href="#">Option 1</a></li>
<li><a href="#">Option 2</a></li>
</ul>
</div>-->
                                                    
                                                </div>
                                                <div class="unit_item_spec_list d_flex justify_content_between align_items_center">
                                                    <div class="unit_item_spec">
                                                        <h2 class="item_title">Price per SQF</h2>
                                                        <div class="unit_item_spec_box">
                                                            <div class="currency">aed</div>
                                                            <h4>
                                                                <apex:outputText value="{0,number,#,##0}">    
                                                                    <apex:param value="{!inv.Price_Per_Sqft__c}"/>
                                                                </apex:outputText>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                    <div class="unit_item_spec">
                                                        <h2 class="item_title">Area</h2>
                                                        <div class="unit_item_spec_box">
                                                            <div class="unit_item_spec_box_img"><i class="icon-area"></i></div>
                                                            <h4>
                                                                <apex:outputText value="{0,number,#,##0}">    
                                                                    <apex:param value="{!inv.Area_sft__c}"/>
                                                                </apex:outputText> SQF
                                                            </h4>
                                                        </div>
                                                    </div>
                                                    <div class="unit_item_spec">
                                                        <h2 class="item_title">BR</h2>
                                                        <div class="unit_item_spec_box">
                                                            <div class="unit_item_spec_box_img"><i class="icon-bed"></i></div>
                                                            <h4>{!inv.Bedroom_Type__c}</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </apex:repeat>
                                </div>
                                <br/><br/><br/>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="tab2">
                                <div class="white_box "  style="padding:1rem;margin-bottom:1rem;">
                                    <h2 class="main_title" style="font-size:0.9rem">Facilities</h2>
                                    <div class="icon_listings">
                                        <ul>
                                            <apex:repeat value="{!facilites}" var="facilite">
                                                <li>
                                                    <apex:variable value="{!featureURLS[facilite].URL__C}" var="feature" />

                                                    <img src="{!URLFOR($Resource.AdditionalFeatures, feature)}" alt=""/>
                                                    <p class="item_title_lg">{!facilite}</p>
                            
                                                </li>
                                            </apex:repeat>
                                        </ul> 
                                    </div>
                                </div>
                                
                            
                                <div class="white_box download_box">
                                    <h2 class="main_title">Downloads</h2>
                                    <a onclick="openURL ('{!inspectionReportURL}', 'inspectionURL');" download="agarwal_1" 
                                       class="download_field d_flex justify_content_between align_items_center">
                                        <span class="download_file_name">Inspection Report</span>
                                        <span class="download_field_icon">
                                            <img src="{!URLFOR($Resource.Damac_MobileApp, 'assets/images/download.svg')}" alt=""/>
                                        </span>
                                    </a>
                                </div>
                                
                                <div class="btn_wrp_colmn">
                                    <!--<a href="#" class="btn btn_primary">Mortgage calculator</a>-->
                                    <!--<a href="#" class="btn btn_secondary">Sales offer</a>-->
                                </div>
                                <br/><br/><br/>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="tab3">
                                <p class="main_title">Photos</p>
                                <div class="space_remove_box">
                                    <ul class="slider_wrapper">
                                        <apex:repeat value="{!projInformation}" var="gallery">
                                            <apex:outputPanel rendered="{!gallery.URL__c != null}">
                                                <li class="slider_item">
                                                    <img src="{!gallery.URL__c}" alt=""/>
                                                </li>
                                            </apex:outputPanel>
                                        </apex:repeat>
                                        
                                    </ul>
                                </div>
                                <p class="main_title">Floor plans</p>
                                <div class="space_remove_box">
                                    <ul class="slider_wrapper">
                                        <apex:repeat value="{!inventories}" var="inv">
                                            <apex:outputPanel rendered="{!inv.Floor_Plan_SF_Link__c != null}">
                                                <li class="slider_item">
                                                    <img src="{!inv.Floor_Plan_SF_Link__c}" alt=""/>
                                                </li>
                                            </apex:outputPanel>
                                        </apex:repeat>
                                        
                                    </ul>
                                </div>
                                <p class="main_title">Unit plans</p>
                                <div class="space_remove_box">
                                    <ul class="slider_wrapper mb_0">
                                        <apex:repeat value="{!inventories}" var="inv">
                                            <apex:outputPanel rendered="{!inv.Unit_Plan_SF_Link__c != null}">
                                                <li class="slider_item">
                                                    <img src="{!inv.Unit_Plan_SF_Link__c}" alt=""/>
                                                </li>
                                            </apex:outputPanel>
                                        </apex:repeat>
                                    </ul>
                                </div>
                                <br/><br/><br/>
                            </div>
                            <div  role="tabpanel" class="tab-pane" id="tab4">
                                <div class="space_remove_box">
                                    
                                    <apex:outputPanel rendered="{!latitude != null && longitude != null}">
                                        <div class="location_box">
                                            <div class="location_map">
                                                <iframe class="map_item" 
                                                        src="https://www.google.com/maps/embed/v1/place?q={!latitude},{!longitude}&key=AIzaSyCz0ZqZ7Bh13JonRSF1_UzM8zCq4nduBm8"
                                                        frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                            </div>
                                            <div class="show_in_map">
                                                <a onclick="openURL('https://www.google.com/maps/place/{!latitude},{!longitude}', 'Map')" class="btn btn_secondary" target="_blank">Show in map</a>
                                            </div>
                                        </div>
                                    </apex:outputPanel>
                                </div>
                                <br/><br/><br/>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </main>
            <script src="{!URLFOR($Resource.Damac_MobileApp, 'assets/js/jquery.min.js')}"></script>
            <script src="{!URLFOR($Resource.Damac_MobileApp, 'assets/js/bootstrap.min.js')}"></script>
            <script src="{!URLFOR($Resource.Damac_MobileApp, 'assets/js/jquery.scrolling-tabs.js')}"></script>
            <script src="{!URLFOR($Resource.Damac_MobileApp, 'assets/js/custom.js')}"></script>
            <script>
            function openURL (e, type){
                
                if (e != '') {
                    sforce.one.navigateToURL(e);
                } else {
                    if (type == 'inspectionURL') {
                        alert ('There is no Inspection URL for this Unit.');
                    }
                }
            }
            jQuery(document).ready(function($) {
                updateContainer();
                
            });
            $(window).resize(function() {
                updateContainer();
            });
            function updateContainer() {
                var bannerHeight = $(".project_banner").outerHeight();
                var thumbHeight  = $(".project_thumb").outerHeight();
                $('.project_thumb').css("margin-top", bannerHeight - thumbHeight);
            }
            function addToFavorites(invId) {
                var isFavorite = $('#favorite').hasClass ('active');
                console.log (isFavorite);
                DAMAC_INVENTORY_MOBILE.addOrRemoveFavorite (invId, !isFavorite, function (result, event) {
                    if (event.status) {
                        
                    }
                });
            }
            
            </script>
            <script>
            //Scrolling tab function
            (function () {
                'use strict';
                $(activate);
                function activate() {
                    $('.nav-tabs').scrollingTabs({
                        enableSwiping: true,
                        bootstrapVersion: 4,
                        scrollToTabEdge: true,
                        disableScrollArrowsOnFullyScrolled: true
                        
                    }).on('ready.scrtabs', function () {
                        $('.nav-tabs').click(function () {
                            setTimeout(function () {
                                $('.nav-tabs').scrollingTabs('scrollToActiveTab');
                            }, 110);
                        });
                    });
                }
            }());
            
            </script>
        </body>
        
    </html>
</apex:page>