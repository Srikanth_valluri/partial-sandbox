<apex:page controller="InventoryWarehouseController" standardStyleSheets="false" lightningStylesheets="false" 
                                           showHeader="false" title="Bulk Updates" docType="html-5.0" >
    <html lang="en">
        <head>
            <apex:slds />
            <meta charset="UTF-8"/>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
            <title>Bulk Updates</title>
            <apex:styleSheet value="{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/css/font-awesome.min.css')}"/>
            <apex:styleSheet value="{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/css/style.css')}"/>
            <apex:styleSheet value="{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/css/jquery.dataTables.min.css')}"/>
            <apex:styleSheet value="{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/css/jquery-ui.min.css')}"/>
            <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
            <script src="/soap/ajax/30.0/connection.js" type="text/javascript"></script>
            <script src="/soap/ajax/33.0/apex.js" type="text/javascript"></script>
            

            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment.min.js"></script>
            <script src="{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/js/jquery.min.js')}"></script>
            <script src="{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/js/jquery-ui.min.js')}"></script>
            <script src="{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/js/jquery.dataTables.min.js')}"></script>
            <script src="{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/js/jquery.datatable.ColReorderWithResize.js')}"></script>
            <script src="https://cdn.datatables.net/fixedcolumns/3.3.2/js/dataTables.fixedColumns.min.js"></script>
            <style>
                @font-face{
                    font-family: "fontawesome-webfont";
                    src: url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fontawesome-webfont.woff2')}") format("woff2"),
                    url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fontawesome-webfont.woff')}") format("woff"),
                    url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fontawesome-webfont.eot')}") format('embedded-opentype'),
                    url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fontawesome-webfont.ttf')}") format("truetype");
                }

                @font-face{
                    font-family: "OpenSans-Bold";
                    src: url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fonts/OpenSans-Bold/OpenSans-Bold.woff2')}") format("woff2"),
                        url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fonts/OpenSans-Bold/OpenSans-Bold.woff')}") format("woff"),
                        url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fonts/OpenSans-Bold/OpenSans-Bold.eot')}") format('embedded-opentype'), 
                        url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fonts/OpenSans-Bold/OpenSans-Bold.ttf')}") format("truetype");
                }

                @font-face{
                    font-family: "OpenSans-Regular";
                    src: url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fonts/OpenSans-Regular/OpenSans-Regular.woff2')}") format("woff2"),
                        url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fonts/OpenSans-Regular/OpenSans-Regular.woff')}") format("woff"),
                        url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fonts/OpenSans-Regular/OpenSans-Regular.eot')}") format('embedded-opentype'),
                        url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fonts/OpenSans-Regular/OpenSans-Regular.ttf')}") format("truetype");
                }

                @font-face{
                    font-family: "OpenSans-Light";
                    src: url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fonts/OpenSans-Light/OpenSans-Light.woff2')}") format("woff2"),
                        url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fonts/OpenSans-Light/OpenSans-Light.woff')}") format("woff"),
                        url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fonts/OpenSans-Light/OpenSans-Light.eot')}") format('embedded-opentype'),
                        url("{!URLFOR($Resource.InventoryHTML, '/InventoryHTML/fonts/OpenSans-Light/OpenSans-Light.ttf')}") format("truetype");
                }

            </style>
        </head>
        <body>
            <!-- Spinner Div -->
            <div class="slds-scope">
                <div id="spinner" class="slds-hide">
                    <div class="slds-spinner_container" style="position: fixed;">
                        <div role="status" class="slds-spinner slds-spinner--large slds-spinner--brand" >
                            <span class="slds-assistive-text">Loading</span>
                            <div class="slds-spinner__dot-a"></div>
                            <div class="slds-spinner__dot-b"></div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                function showSpinner () {
                    $ ('#spinner').removeClass ('slds-hide');
                }

                function hideSpinner () {
                    $ ('#spinner').addClass ('slds-hide');
                }
            </script>
            <header>
                <ul class="breadcrumb">
                    <li><a href="/apex/InventorySetup">Setup</a></li>
                </ul>
                        <div class="header-wrap">
            <p>Bulk Updates</p>
        </div>
    </header>
    <div class="content-wrap bulk-updates">
        <div class="menu-wrap">
            <p class="menu-type">Bulk Updates Unit [Screen]</p>
            <ul>
                <li>
                    <a href="/apex/BulkUpdateAttributes">
                        <i class="fa fa-edit blue" aria-hidden="true"></i>
                        <div>
                            <p class="menu-title">Update Attributes</p>
                            <p class="menu-title-sub">Update unit attributes by line level.</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="/apex/BulkUpdateFocusUnits">
                        <i class="fa fa-check-circle green" aria-hidden="true"></i>
                        <div>
                            <p class="menu-title">Focus Units(s)</p>
                            <p class="menu-title-sub">Mark and update Inventory units(s) as Focusing Product.</p>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="menu-wrap">
            <p class="menu-type">Bulk Updates Unit [CSV]</p>
            <ul>
                <li>
                    <a href="/apex/BulkUpdate">
                        <i class="fa fa-upload blue" aria-hidden="true"></i>
                        <div>
                            <p class="menu-title">Update Details</p>
                            <p class="menu-title-sub">Bulk Unit updates by csv upload.</p>
                        </div>
                    </a>
                </li>
                <!--
                <li>
                    <a href="/apex/BulkUpdateStoreRoom">
                        <i class="fa fa-upload green" aria-hidden="true"></i>
                        <div>
                            <p class="menu-title">Update Store Room</p>
                            <p class="menu-title-sub">Bulk Unit updates by csv upload.</p>
                        </div>
                    </a>
                </li>
                -->
                <li>
                    <a href="/apex/BulkUpdatePrice">
                        <i class="fa fa-upload green-light" aria-hidden="true"></i>
                        <div>
                            <p class="menu-title">Update Price</p>
                            <p class="menu-title-sub">Bulk Unit updates by csv upload.</p>
                        </div>
                    </a>
                </li>
                <!--
                <li>
                    <a href="/apex/BulkUpdateMarketingName">
                        <i class="fa fa-upload dark-green" aria-hidden="true"></i>
                        <div>
                            <p class="menu-title">Update Marketing Name</p>
                            <p class="menu-title-sub">Bulk Unit updates by csv upload.</p>
                        </div>
                    </a>
                </li>
                -->
                <li>
                    <a href="/apex/BulkUpdateBedroomType">
                        <i class="fa fa-upload yellow-green" aria-hidden="true"></i>
                        <div>
                            <p class="menu-title">Update Bedroom Type</p>
                            <p class="menu-title-sub">Bulk Unit updates by csv upload.</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="/apex/BulkUpdateViewType">
                        <i class="fa fa-upload green" aria-hidden="true"></i>
                        <div>
                            <p class="menu-title">Update View Type</p>
                            <p class="menu-title-sub">Bulk Unit updates by csv upload.</p>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>

                

        </body>
    </html>

    <apex:outputPanel id="scriptTag">
        <script>
        
        </script>
    </apex:outputPanel>

    <script type="text/Javascript">
    </script>
</apex:page>