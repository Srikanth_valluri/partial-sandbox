<!--
  @Page Name          : CustomerFeedbackPageForCallingList.page
  @Description        : 
  @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
  @Group              : 
  @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
  @Last Modified On   : 9/4/2019, 7:21:14 PM
  @Modification Log   : 
  ==============================================================================
  Ver         Date                     Author      		      Modification
  ==============================================================================
  1.0    9/4/2019, 7:21:14 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
-->
<apex:page id="pageId" standardController="Calling_List__c" 
        sidebar="false" showHeader="false" 
        applyHtmlTag="false" 
        applyBodyTag="false" 
        extensions="CustomerFeedbackForCallingListCntrl" > 
    <head>
        <apex:slds /> 
    </head>

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link type="text/css" rel="stylesheet" href="https://www.lightningdesignsystem.com/assets/styles/slds.css"/>
    <style>
        .msgIcon {
            display: none!important
        }
        .customMessage * {
            color: #fff!important
        }
        .customMessage {
            margin: 5px 0!important;
            max-width: 1400px;
            opacity: 1!important;
            width: 100%;
            font-size: 12px;
            border: 0px;
            padding-left: 10px;
        }
    </style>
    
    <body class="slds-scope">
    <apex:form id="frmId">
        <apex:pagemessages id="pgMsgs"/>
        <apex:outputpanel id="op">
            <apex:inputHidden value="{!isSubmitted}" id="theHiddenInput"/>
        </apex:outputpanel>
        <!--modal-->
            <div id="modalId" class="demo-only demo-only_viewport" style="height:640px;display:none;">
                <section role="dialog" tabindex="-1" aria-labelledby="modal-heading-01" 
                            aria-modal="true" 
                            aria-describedby="modal-content-id-1" 
                            class="slds-modal slds-fade-in-open">
                    <div class="slds-modal__container">
                        <header class="slds-modal__header">
                            <button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" 
                                    title="Close">
                                <svg class="slds-button__icon slds-button__icon_large" aria-hidden="true">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" 
                                        xlink:href="/apexpages/slds/latest/assets/icons/standard-sprite/svg/symbols.svg#close">
                                    </use>
                                </svg>
                                <span class="slds-assistive-text">Close</span>
                            </button>
                            <h2 id="modal-heading-01" class="slds-text-heading_medium slds-hyphenate">Thank You</h2>
                        </header>
                        <div class="slds-modal__content slds-p-around_medium" id="modal-content-id-1">
                            <p>Thanks for providing your valuable feedback. We appreciate your valuable time spend in writing.</p>
                        </div>
                    </div>
                </section>
            <div class="slds-backdrop slds-backdrop_open"></div>
            </div>
        <!--modal end-->

        
        <div class="slds-panel slds-grid slds-grid--vertical slds-nowrap" style="margin-left: 15em;margin-right: 15em;">
            <div class="slds-form--stacked slds-grow slds-scrollable--y">

                <div class="slds-panel__section">
                   
                    <div class="slds-form-element slds-hint-parent">
                        <span class="slds-form-element__label">Please rate your experience</span>
                        <div class="slds-form-element__control">
                            <div class="slds-grid slds-gutters">
                                <div class="slds-col">
                                    <span class="slds-avatar slds-avatar--large">
                                        <img src="{!URLFOR($Resource.Emojis, 'Emojis/Happy-Smiley.jpg')}" alt="Happy" style="border: 1px solid #d8dde6;border-radius: 5px;"/>
                                    </span>
                                    <span class="slds-form-element__label" style="display: block;"> Happy</span>
                                    <label class="slds-checkbox">
                                        <apex:inputcheckbox id="isHappy" styleclass="slds-input" value="{!isHappy}" />
                                        <span class="slds-checkbox--faux"></span>
                                    </label>
                                </div>
                                <div class="slds-col">
                                    <span class="slds-avatar slds-avatar--large">
                                        <img src="{!URLFOR($Resource.Emojis, 'Emojis/relieved-face.png')}" alt="Satisfied" style="border: 1px solid #d8dde6;border-radius: 5px;"/>
                                    </span>
                                    <span class="slds-form-element__label" style="display: block;"> Satisfied</span>
                                    <label class="slds-checkbox">
                                        <apex:inputcheckbox id="isSatisfied" styleclass="slds-input" value="{!isSatisfied}" />
                                        <span class="slds-checkbox--faux"></span>
                                    </label>
                                </div>
                                <div class="slds-col">
                                    <span class="slds-avatar slds-avatar--large">
                                        <img src="{!URLFOR($Resource.Emojis, 'Emojis/sad-emoji.png')}" alt="Not Satisfied" style="border: 1px solid #d8dde6;border-radius: 5px;"/>
                                    </span>
                                    <span class="slds-form-element__label" style="display: block;">Not Satisfied</span>
                                    <label class="slds-checkbox">
                                        <apex:inputcheckbox id="isNotSatisfied" styleclass="slds-input" value="{!isNotSatisfied}" />
                                        <span class="slds-checkbox--faux"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div><br/><br/>
                    <!--<apex:outputpanel rendered="{!IF(objFMCase.Request_Type__c != 'Suggestion',true,false)}"> -->
                    <apex:outputpanel >
                        <div class="slds-form-element slds-hint-parent">
                            <span class="slds-form-element__label"> Tell us more about your experience</span>
                            <div class="slds-form-element__control">
                            <apex:inputTextarea styleclass="slds-input" value="{!customerComments}" rows="3" id="theTextInput1" style="max-width: 600px;"/>
                            
                            </div>
                        </div>
                    </apex:outputpanel>

                </div>
                <div class="slds-panel__section slds-has-divider--bottom">
                    <div class="slds-media">
                        <div class="slds-media__body">
                            <div class="slds-button-group slds-m-top--small" role="group">
                                <apex:commandButton value="Submit Feedback" styleclass="slds-button slds-button_brand slds-align_absolute-center" action="{!submitFeedback}" oncomplete="showModal();return false;" reRender="pgMsgs,op"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </apex:form>
    </body>
    
    <script>
        $(document).ready(function(){
            overridePageMessages();   
        });
        
        function overridePageMessages(){   
            var textureEffect = '';
            //Uncomment below line for texture effect on page messages
            //textureEffect = 'slds-theme--alert-texture';
                        
            $('.warningM3').addClass('slds-notify slds-notify--toast slds-theme--warning customMessage '+textureEffect);         
            $('.confirmM3').addClass('slds-notify slds-notify--alert slds-theme--success  customMessage '+textureEffect);   
            $('.errorM3').addClass('slds-notify slds-notify--alert slds-theme--error customMessage '+textureEffect);                 
            $('.infoM3').addClass('slds-notify slds-notify--toast customMessage '+textureEffect);   
                            
            $('.errorM3').removeClass('errorM3');
            $('.confirmM3').removeClass('confirmM3');
            $('.infoM3').removeClass('infoM3');  
            $('.warningM3').removeClass('warningM3'); 
        }

        function showModal(){
            console.log('ala popup la',"{!isSubmitted}");
            var submitted = document.getElementById("pageId:frmId:theHiddenInput").value;
            console.log('submitted===',submitted);
            //var sub = "{!isSubmitted}";
            //console.log('ala sub la',sub);
            if(submitted != 'false'){
                console.log('ka ala aat ');
                document.getElementById("modalId").style.display = "block";
            }
            
        }
    </script>

</apex:page>