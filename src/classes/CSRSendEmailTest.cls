@IsTest
public class CSRSendEmailTest{
 @IsTest
    static void positiveTest1(){
        // User Insert 
         User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABC@test.com');
         //pcUser.IPMS_Employee_ID__c = '1111';
         //pcUser.Sales_Office__c = 'DAMACHO';
         //pcUser.Manager = pcUser1;
         insert pcUser;
        // Create common test accounts
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Approving_User_Id__c=pcUser.id;
        objCase.Approving_User_Role__c='Manager';
        //objCase.Parking_Details_JSON__c ='[{"AggrementStatus":"Agreement executed by DAMAC","Area":"518.00","PaidAmmount":"1200693","PaidPersentage":"","Price":"1295000.00","PropertyName":"","PSF":"2500.00","RegDate":"2013-09-29","UnitNumber":"DTPC/32/3213"}]';
        objCase.Parking_Details_JSON__c = '{"Penalties waived AMT":["0"],"Penalties charged AMT":["20"],"Total O/S. Incl. Penalties":["20"],"Balance To Pay":["3220160.00"],"Termination - Refund Value":["a0x0Y000001jABnQAM"],"Termination - Retained Value":["a0x0Y000001jABnQAM"],"Paid %":["40.00%"],"Paid Amount":["AED 73840.00"],"Price Drop by Damac on AC Area":["a0x0Y000001jABnQAM"],"Current PSF on AC Area":["0"],"Current PSF on Saleable Area":["0"],"Sold PSF on Saleable Area":["2,400.37"],"JOPD Area":["1372.29"],"AC Area":["0"],"Saleable Area":["1,372.29"],"Unit Value":["AED 3,294,000.00"],"Inventory Sale Classification":[null],"Handover Status":["Handover in Progress – Notice < 30 days"],"Current ACD":["28-FEB-2019"],"Construction Status":["00"],"Unit Typology":["2 BR , Two Bedroom"],"Agreement Date":["2017-07-18"],"Unit Registration ID":["88395"],"Unit Number":["DRZ/79/7903"],"Unit Details":["a0x0Y000001jABnQAM"],"BU SOA":["88395"]}';
        objCase.NewPaymentTermJSON__c = '["BU SOA","Unit Details","Unit Number","Unit Registration ID","Agreement Date","Unit Typology","Construction Status","Current ACD","Handover Status","Inventory Sale Classification","Unit Value","Saleable Area","AC Area","JOPD Area","Sold PSF on Saleable Area","Current PSF on Saleable Area","Current PSF on AC Area","Price Drop by Damac on AC Area","Paid Amount","Paid %","Termination - Retained Value","Termination - Refund Value","Balance To Pay","Total O/S. Incl. Penalties","Penalties charged AMT","Penalties waived AMT"]';
        insert objCase;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        objSR.Agency__c = objAcc.Id ;
        insert objSR ;

        Id dealId = objSR.Deal_ID__c;
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
        insert lstBookings ; 

        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
        lstBookingUnits[0].Registration_ID__c = '1520';
        insert lstBookingUnits;
        //Approving_User_Id__c
        String str ='{"CRE_Comments__c":"Test","Booking_Unit__c":"a0x25000000BDbiAAG","CRE_Request_History__c":"% of Area Variation to be waived=>550,Amount=>,","Case__c":"5002500000Bg6xMAAR","RequestType__c":"Area Variation Waiver","Per_Area_Variation_to_be_waived__c":"550","Approving_User_Id__c":"0050Y000001SVkiQAG","Approving_User_Name__c":"Waleed Elkady","Approving_User_Role__c":"HOD"}}';
        
        CaseSummaryRequest__c objcsr = (CaseSummaryRequest__c)JSON.deserialize(str, CaseSummaryRequest__c.class);
                                system.debug('obj :'+objcsr);
                                objcsr.case__c = objCase.Id;
                                objcsr.Booking_Unit__c = lstBookingUnits[0].Id;
                                objcsr.Manager_Request_History__c ='% of Area Variation to be waived=>550';
                                objcsr.Director_Request_History__c ='% of Area Variation to be waived=>550';
                                objcsr.HOD_Request_History__c ='% of Area Variation to be waived=>550';
                                objcsr.Committee_Comments__c = 'Test';
                                objcsr.Director_Comments__c = 'Test';
                                objcsr.HOD_Comments__c= 'Test';
                                objcsr.Manager_Comments__c = 'Test';
                                
        insert objcsr;

        Test.startTest();
        PageReference myVfPage = Page.CSRApprovalPage;
        Test.setCurrentPage(myVfPage);

        // Put Id into the current page Parameters
        //ApexPages.currentPage().getParameters().put('caseId',objCase.Id);
        //ApexPages.currentPage().getParameters().put('approver','Manager');
        //ApexPages.currentPage().getParameters().put('Name','Test');
        //ApexPages.currentPage().getParameters().put('Status','Approved');

        //new
        myVfPage.getParameters().put('caseId',objCase.Id);
        /*myVfPage.getParameters().put('approver','Manager');
        myVfPage.getParameters().put('Name','Test');
        myVfPage.getParameters().put('Status','Approved');*/
        myVfPage.getParameters().put('id',objCase.Id);
        //new end
        //Test.startTest();
        CSRApprovalController obj = new CSRApprovalController();
        obj.submitCSR();
        List<Id> lstId = new List<Id>() ;
        lstId.add(objCase.Id);

        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        CSRSendEmail objSendEmail = new CSRSendEmail(sc);
        objSendEmail.init();
        
        CSRSendEmail.SendMailCSR(lstId);

        
        Test.stopTest();

        
    }
     @IsTest
    static void positiveTest2(){
        // User Insert 
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABC@test.com');
        //pcUser.IPMS_Employee_ID__c = '1111';
        //pcUser.Sales_Office__c = 'DAMACHO';
        //pcUser.Manager = pcUser1;
        insert pcUser;
        // Create common test accounts
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        pcUser.IPMS_Employee_ID__c = '1111';
        pcUser.Sales_Office__c = 'DAMACHO';
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Approving_User_Id__c=pcUser.id;
        objCase.Approving_User_Role__c='Manager';
        //objCase.Parking_Details_JSON__c ='[{"AggrementStatus":"Agreement executed by DAMAC","Area":"518.00","PaidAmmount":"1200693","PaidPersentage":"","Price":"1295000.00","PropertyName":"","PSF":"2500.00","RegDate":"2013-09-29","UnitNumber":"DTPC/32/3213"}]';
        objCase.Parking_Details_JSON__c = '{"Penalties waived AMT":["0"],"Penalties charged AMT":["20"],"Total O/S. Incl. Penalties":["20"],"Balance To Pay":["3220160.00"],"Termination - Refund Value":["a0x0Y000001jABnQAM"],"Termination - Retained Value":["a0x0Y000001jABnQAM"],"Paid %":["40.00%"],"Paid Amount":["AED 73840.00"],"Price Drop by Damac on AC Area":["a0x0Y000001jABnQAM"],"Current PSF on AC Area":["0"],"Current PSF on Saleable Area":["0"],"Sold PSF on Saleable Area":["2,400.37"],"JOPD Area":["1372.29"],"AC Area":["0"],"Saleable Area":["1,372.29"],"Unit Value":["AED 3,294,000.00"],"Inventory Sale Classification":[null],"Handover Status":["Handover in Progress – Notice < 30 days"],"Current ACD":["28-FEB-2019"],"Construction Status":["00"],"Unit Typology":["2 BR , Two Bedroom"],"Agreement Date":["2017-07-18"],"Unit Registration ID":["88395"],"Unit Number":["DRZ/79/7903"],"Unit Details":["a0x0Y000001jABnQAM"],"BU SOA":["88395"]}';
        objCase.NewPaymentTermJSON__c = '["BU SOA","Unit Details","Unit Number","Unit Registration ID","Agreement Date","Unit Typology","Construction Status","Current ACD","Handover Status","Inventory Sale Classification","Unit Value","Saleable Area","AC Area","JOPD Area","Sold PSF on Saleable Area","Current PSF on Saleable Area","Current PSF on AC Area","Price Drop by Damac on AC Area","Paid Amount","Paid %","Termination - Retained Value","Termination - Refund Value","Balance To Pay","Total O/S. Incl. Penalties","Penalties charged AMT","Penalties waived AMT"]';
        insert objCase;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        objSR.Agency__c = objAcc.Id ;
        insert objSR ;

        Id dealId = objSR.Deal_ID__c;
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
        insert lstBookings ; 

        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
        lstBookingUnits[0].Registration_ID__c = '1520';
        insert lstBookingUnits;
        //Approving_User_Id__c
        String str ='{"CRE_Comments__c":"Test","Booking_Unit__c":"a0x25000000BDbiAAG","CRE_Request_History__c":"% of Area Variation to be waived=>550,Amount=>,","Case__c":"5002500000Bg6xMAAR","RequestType__c":"Area Variation Waiver","Per_Area_Variation_to_be_waived__c":"550","Approving_User_Id__c":"0050Y000001SVkiQAG","Approving_User_Name__c":"Waleed Elkady","Approving_User_Role__c":"HOD"}}';
        
        CaseSummaryRequest__c objcsr = (CaseSummaryRequest__c)JSON.deserialize(str, CaseSummaryRequest__c.class);
                                system.debug('obj :'+objcsr);
                                objcsr.case__c = objCase.Id;
                                objcsr.Booking_Unit__c = lstBookingUnits[0].Id;
                                objcsr.Manager_Request_History__c ='% of Area Variation to be waived=>550';
                                objcsr.Director_Request_History__c ='% of Area Variation to be waived=>550';
                                objcsr.HOD_Request_History__c ='% of Area Variation to be waived=>550';
                                objcsr.Committee_Comments__c = 'Test';
                                objcsr.Director_Comments__c = 'Test';
                                objcsr.HOD_Comments__c= 'Test';
                                objcsr.Manager_Comments__c = 'Test';
                                
        insert objcsr;

        Test.startTest();
        PageReference myVfPage = Page.CSRApprovalPage;
        Test.setCurrentPage(myVfPage);

        // Put Id into the current page Parameters
        //ApexPages.currentPage().getParameters().put('caseId',objCase.Id);
        //ApexPages.currentPage().getParameters().put('approver','Manager');
        //ApexPages.currentPage().getParameters().put('Name','Test');
        //ApexPages.currentPage().getParameters().put('Status','Approved');

        //new
        myVfPage.getParameters().put('caseId',objCase.Id);
        /*myVfPage.getParameters().put('approver','Manager');
        myVfPage.getParameters().put('Name','Test');
        myVfPage.getParameters().put('Status','Approved');*/
        myVfPage.getParameters().put('id',objCase.Id);
        //new end
        //Test.startTest();
        CSRApprovalController obj = new CSRApprovalController();
        obj.submitCSR();
        List<Id> lstId = new List<Id>() ;
        lstId.add(objCase.Id);

        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        CSRSendEmail objSendEmail = new CSRSendEmail(sc);
        objSendEmail.init();
        
        CSRSendEmail.SendMailCSR(lstId);
        Test.stopTest();

        
    }
}