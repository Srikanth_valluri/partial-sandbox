/*
* Test class for RecordCallAfterWorkingHoursWeb Class
*/

@isTest
public class RecordCallAfterWorkingHoursWebTest{

    @testSetup static void setup() {
        TriggerOnOffCustomSetting__c obj = new TriggerOnOffCustomSetting__c();
        obj.Name = 'CallingListTrigger';
        obj.OnOffCheck__c = true;
        insert obj;
    }

    public static final String INVALID_DATE = '28/11/2017';
    public static final String VALID_DATE = '11/6/2014 12:10:20';
    public static final String CALLED_NUMBER = '1234567890';
    public static final String CALLING_NUMBER = '9876543210';
    public static final String POST = 'POST';
    public static final String QUEUE_MEMBERS = '/recordCallAfterWorkingHours';
    /*
    * Test method to test response with sample data
    */
    @isTest
    public static void doPostTest(){
        RecordCallAfterWorkingHoursLogic.RecordCallAfterWorkingHoursRequestBody reqst =
            new RecordCallAfterWorkingHoursLogic.RecordCallAfterWorkingHoursRequestBody();
        reqst.calledDateTime = INVALID_DATE;
        reqst.calledNumber = CALLED_NUMBER;
        reqst.callingNumber = CALLING_NUMBER;
        reqst.callback = true;

        String JsonMsg=JSON.serialize(reqst);

        RestRequest req = new RestRequest();
        req.httpMethod = POST;
        req.requestUri = QUEUE_MEMBERS;
        req.requestBody = Blob.valueOf(JSON.serializePretty(reqst));
        RestContext.request = req;
        RestContext.response = new RestResponse();
        Test.startTest();
        RecordCallAfterWorkingHoursWebService.doPost();
        Test.stopTest();
        System.assertEquals(400,RestContext.response.statusCode);

        String recordCallResponse= RestContext.response.responseBody.toString();
        RecordCallAfterWorkingHoursLogic.RecordCallAfterWorkingHoursResponse response =
            (RecordCallAfterWorkingHoursLogic.RecordCallAfterWorkingHoursResponse)JSON.deserializeStrict(
                recordCallResponse,
                RecordCallAfterWorkingHoursLogic.RecordCallAfterWorkingHoursResponse.Class);
         System.assertEquals(response.errorMessage, 'Please enter valid date format (MM/DD/YYYY HH:MM:SS)');

    }

    /*
    * Test method to test functionality of processRecordCallAfterWorkingHoursRequest method with Callback
    */
    @isTest
    public static void processRecordCallAfterWorkingHoursRequest_WithCallback_Test(){
        CreRoutingTest.createRegisteredNonVipAccount(CALLING_NUMBER);
        RecordCallAfterWorkingHoursLogic.RecordCallAfterWorkingHoursRequestBody reqst =
            new RecordCallAfterWorkingHoursLogic.RecordCallAfterWorkingHoursRequestBody();
        reqst.calledDateTime = VALID_DATE;
        reqst.calledNumber = CALLED_NUMBER;
        reqst.callingNumber = CALLING_NUMBER;
        reqst.callback = true;

        String JsonMsg=JSON.serialize(reqst);

        RestRequest req = new RestRequest();
        req.httpMethod = POST;
        req.requestUri = QUEUE_MEMBERS;
        req.requestBody = Blob.valueOf(JSON.serializePretty(reqst));
        RestContext.request = req;
        RestContext.response = new RestResponse();
        Test.startTest();
        RecordCallAfterWorkingHoursLogic.processRecordCallAfterWorkingHoursRequest();
        Test.stopTest();
        System.assertEquals(200,RestContext.response.statusCode);

        String recordCallResponse= RestContext.response.responseBody.toString();
        RecordCallAfterWorkingHoursLogic.RecordCallAfterWorkingHoursResponse response =
            (RecordCallAfterWorkingHoursLogic.RecordCallAfterWorkingHoursResponse)JSON.deserializeStrict(
                recordCallResponse,
                RecordCallAfterWorkingHoursLogic.RecordCallAfterWorkingHoursResponse.Class);
        System.assertEquals('Call Record Saved', response.status);

    }

    /*
    * Test method to test functionality of processRecordCallAfterWorkingHoursRequest method without Callback
    */
    @isTest
    public static void processRecordCallAfterWorkingHoursRequest_WithoutCallback_Test(){
        Account acc = CreRoutingTest.createRegisteredNonVipAccount(CALLING_NUMBER);
        System.assert(acc != null);
        RecordCallAfterWorkingHoursLogic.RecordCallAfterWorkingHoursRequestBody reqst =
            new RecordCallAfterWorkingHoursLogic.RecordCallAfterWorkingHoursRequestBody();
        reqst.calledDateTime = VALID_DATE;
        reqst.calledNumber = CALLED_NUMBER;
        reqst.callingNumber = CALLING_NUMBER;
        reqst.callback = false;

        String JsonMsg=JSON.serialize(reqst);

        RestRequest req = new RestRequest();
        req.httpMethod = POST;
        req.requestUri = QUEUE_MEMBERS;
        req.requestBody = Blob.valueOf(JSON.serializePretty(reqst));
        RestContext.request = req;
        RestContext.response = new RestResponse();
        Test.startTest();
        RecordCallAfterWorkingHoursLogic.processRecordCallAfterWorkingHoursRequest();
        Test.stopTest();
        System.assertEquals(200,RestContext.response.statusCode);

        String recordCallResponse= RestContext.response.responseBody.toString();
        RecordCallAfterWorkingHoursLogic.RecordCallAfterWorkingHoursResponse response =
            (RecordCallAfterWorkingHoursLogic.RecordCallAfterWorkingHoursResponse)JSON.deserializeStrict(
                recordCallResponse,
                RecordCallAfterWorkingHoursLogic.RecordCallAfterWorkingHoursResponse.Class);
        System.assertEquals('Call Record Saved', response.status);

    }
}