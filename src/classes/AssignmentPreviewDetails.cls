public without sharing class AssignmentPreviewDetails{
    Id caseId;
    public Case objCase{get;set;}
    public list<Account> lstSellers {get;set;}
    public list<Buyer__c> lstBuyers {get;set;}
    public string personRectypeId {get;set;}
    
    public AssignmentPreviewDetails(){
        caseId = ApexPages.currentPage().getParameters().get('id');
        system.debug('*****caseId*****'+caseId);
        personRectypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        fetchAllDetails();
    }
    
    public void fetchAllDetails(){
         list<Case> lstC = [select Id,
                            Booking_Unit__r.Inventory__r.Seller_Name__c, Booking_Unit__r.Inventory__r.Seller_Name_Arabic__c,
                            Booking_Unit__r.Inventory__r.Unit_Location_EN__c, Booking_Unit__r.Inventory__r.Unit_Location_AR__c,
                            Booking_Unit__r.Inventory__r.Unit__c, Booking_Unit__r.Inventory__r.Plot_Number__c,
                            Booking_Unit__r.Inventory__r.Building_Name__c,  New_Booking__c, New_Booking_Unit__c,
                            New_Booking_Unit__r.Booking__c,
                            Booking_Unit__r.Inventory__r.Master_Community_EN__c, Booking_Unit__r.Inventory__r.Master_Community_AR__c,
                            Booking_Unit__r.Requested_Price__c, Assignment_Fee__c, CurrencyISOCode,
                            createdDate, Seller__r.Name, Seller__r.FirstName, Seller__r.LastName,
                            Seller__r.MiddleName,Seller__r.Nationality__c, Seller__r.Passport_Number__c, Seller__r.CR_Number__c,
                            Seller__r.CR_Registration_Place__c, Seller__r.CR_Registration_Expiry_Date__c,
                            Seller__r.Address_Line_1__c, Seller__r.Address_Line_2__c, Seller__r.Address_Line_3__c,
                            Seller__r.Address_Line_4__c, Seller__r.City__c, Seller__r.Zip_Postal_Code__c,
                            Seller__r.Country__c, Seller__r.Telephone__c, Seller__r.Fax__c, Seller__c,
                            Seller__r.Mobile__c, Seller__r.Email__c, Seller__r.City__pc, Seller__r.Country__pc,
                            Seller__r.Nationality__pc, Seller__r.Passport_Number__pc, Seller__r.CR_Registration_Place__pc,
                            Seller__r.CR_Number__pc, Seller__r.CR_Registration_Date__pc, Seller__r.Address_Line_1__pc,
                            Seller__r.Address_Line_2__pc, Seller__r.Address_Line_3__pc, Seller__r.Address_Line_4__pc,
                            Seller__r.PersonMobilePhone, Seller__r.PersonEmail, Seller__r.First_Name__c, Seller__r.Middle_Name__c,
                            Seller__r.Last_Name__c, Booking_Unit__r.Inventory__r.Project_Category__c, 
                            Booking_Unit__r.Name, Booking_Unit__c, Booking_Unit__r.Amount_Paid__c, 
                            Booking_Unit__r.Booking__c, Booking_Unit__r.Inventory__r.Property_Name_2__c,
                            Booking_Unit__r.Agreement_Date__c, Buyer__r.Booking__c, Buyer__r.Organisation_Name_Arabic__c,
                            Buyer__r.First_Name__c, Buyer__r.Last_Name__c, Buyer__r.Organisation_Name__c,
                            Buyer__r.Nationality__c, Buyer__r.Passport_Number__c, Buyer__r.CR_Number__c,
                            Buyer__r.CR_Registration_Place__c, Buyer__r.CR_Registration_Expiry_Date__c,
                            Buyer__r.Address_Line_1__c, Buyer__r.Address_Line_2__c, Buyer__r.Address_Line_3__c,
                            Buyer__r.Address_Line_4__c, Buyer__r.City__c, Buyer__r.Country__c, 
                            Booking_Unit__r.Inventory__r.Property_Country__c, Security_Cheque_Amount__c,
                            Bounced_Cheque_Date_of_Expiry__c, Booking_Unit__r.Inventory__r.Bedroom_Type__c,
                            Booking_Unit__r.Selling_Price__c, Booking_Unit__r.Parking_Bay_No__c,
                            Booking_Unit__r.Inventory__r.Property_Status__c, Booking_Unit__r.Inventory__r.Property_City__c,
                            Booking_Unit__r.Inventory__r.Parking__c, Booking_Unit__r.Building_Name__c,
                            
                            Booking_Unit__r.District__c, Booking_Unit__r.Inventory__r.Master_Developer_EN__c,
                            Booking_Unit__r.Inventory__r.Area_Sqft_2__c, Seller__r.Mobile_Country_Code__pc,
                            
                            
                            Seller__r.Mobile_Area_Code__pc, Booking_Unit__r.Inventory__r.Property_Name_Arabic__c,
                            Seller__r.First_Name_Arabic__pc, Seller__r.Middle_Name_Arabic__pc,Seller__r.Last_Name_Arabic__pc,
                            Seller__r.Passport_Number_Arabic__pc,Seller__r.Title_Arabic__pc, Seller__r.IsPersonAccount,
                            Seller__r.Address_Line_1_Arabic__pc, Seller__r.Address_Line_2_Arabic__pc,
                            Seller__r.Address_Line_3_Arabic__pc, Seller__r.Address_Line_4_Arabic__pc,
                            Seller__r.City_Arabic__pc, Seller__r.Nationality_Arabic__pc, Buyer__r.Email__c,
                            Seller__r.Mobile_Country_Code_Arabic__pc, Seller__r.Mobile_Area_Code_Arabic__pc,
                            Seller__r.Mobile_Number_Arabic__pc, Seller__r.Fax_Country_Code_Arabic__pc,
                            Seller__r.Fax_Area_Code_Arabic__pc, Seller__r.Fax_Number_Arabic__pc, Seller__r.Mobile_Encrypt__c,
                            Seller__r.Home_Phone_Country_Code__c, Seller__r.Home_Phone__c, Booking_Unit__r.Registration_ID__c,
                            Seller__r.Email_Address_Arabic__pc, Buyer__r.First_Name_Arabic__c, Seller__r.Name_Arabic__c,
                            Seller__r.Title_Arabic__c, Seller__r.Address_Line_1_Arabic__c, Seller__r.Address_Line_2_Arabic__c,
                            Seller__r.Address_Line_3_Arabic__c, Seller__r.Address_Line_4_Arabic__c, Seller__r.City_Arabic__c,
                            Seller__r.Nationality_Arabic__c, Seller__r.Mobile_Country_Code__c,Seller__r.Mobile_Area_Code__c,
                            Buyer__r.Last_Name_Arabic__c, Buyer__r.Title_Arabic__c, Buyer__r.Address_Line_1_Arabic__c,
                            Buyer__r.Address_Line_2_Arabic__c, Buyer__r.Address_Line_3_Arabic__c,
                            Buyer__r.Address_Line_4_Arabic__c, Buyer__r.City_Arabic__c, Buyer__r.Nationality_Arabic__c,
                            Buyer__r.Phone_Country_Code__c,Buyer__r.Phone__c, Booking_Unit__r.Unit_Type__c,
                            Payment_Verified__c, Document_Verified__c, Booking_Unit__r.Handover_Flag__c
                            from Case where Id =: caseId];
        if(lstC != null && !lstC.isEmpty()){
            objCase = lstC[0];
            if(objCase.Booking_Unit__r.Booking__c != null && objCase.Booking_Unit__c != null){
                lstSellers = returnAccounts(objCase.Booking_Unit__r.Booking__c);
            }
            system.debug('objCase.New_Booking__c*****'+objCase.New_Booking__c);
            system.debug('objCase.New_Booking_Unit__c*****'+objCase.New_Booking_Unit__c);
            if(objCase.Buyer__c != null && objCase.Buyer__r.Booking__c != null){
                system.debug('came in*************');
                lstBuyers = returnBuyers(objCase.Buyer__r.Booking__c);
                system.debug('lstBuyers*************'+lstBuyers);
            }
        }
    }
    
    public list<Account> returnAccounts(Id bookingId){
        list<Account> lstA = new list<Account>();
        set<String> setNationality = new set<String>();
        set<String> setPassport = new set<String>();
        set<String> setCRNumber = new set<String>();
        list<Buyer__c> lstBuyers = new list<Buyer__c>();
        for(Buyer__c objB : [Select Id
                                    , Account__c
                                    , Booking__c
                                    , Primary_Buyer__c
                                    , Buyer_Type__c
                                    , Title__c
                                    , Title_Arabic__c
                                    , First_Name__c
                                    , First_Name_Arabic__c
                                    , Last_Name__c
                                    , Last_Name_Arabic__c
                                    , Nationality__c
                                    , Nationality_Arabic__c
                                    , Passport_Number__c
                                    , Address_Line_1__c
                                    , Address_Line_1_Arabic__c
                                    , Address_Line_2__c
                                    , Address_Line_2_Arabic__c
                                    , Address_Line_3__c
                                    , Address_Line_3_Arabic__c
                                    , Address_Line_4__c
                                    , Address_Line_4_Arabic__c
                                    , Country__c
                                    , Country_Arabic__c
                                    , City__c
                                    , City_Arabic__c
                                    , Phone__c
                                    , Phone_Country_Code__c
                                    , Email__c
                                    , Organisation_Name__c
                                    , Organisation_Name_Arabic__c
                                    , CR_Number__c
                                    , CR_Registration_Place__c
                                    , CR_Registration_Place_Arabic__c
                                    , Account__r.RecordTypeId
                                    , Account__r.Party_ID__c
                                    , Account__r.Party_Type__c
                                    , Account__r.FirstName
                                    , Account__r.Salutation
                                    , Account__r.MiddleName
                                    , Account__r.LastName
                                    , Account__r.Name
                                    , Account__r.Name_Arabic__c
                                    , Account__r.IsPersonAccount
                                    , Account__r.Nationality__pc
                                    , Account__r.Passport_Number__pc
                                    , Account__r.Address_Line_1__pc
                                    , Account__r.Address_Line_2__pc
                                    , Account__r.Address_Line_3__pc
                                    , Account__r.Address_Line_4__pc
                                    , Account__r.Address_Line_1__c
                                    , Account__r.Address_Line_2__c
                                    , Account__r.Address_Line_3__c
                                    , Account__r.Address_Line_4__c
                                    , Account__r.Country__pc
                                    , Account__r.Country__c
                                    , Account__r.City__pc
                                    , Account__r.City__c
                                    , Account__r.Zip_Postal_Code__c
                                    , Account__r.Telephone__c
                                    , Account__r.Fax__c
                                    , Account__r.PersonMobilePhone
                                    , Account__r.PersonEmail
                                    , Account__r.CR_Number__c
                                    , Account__r.CR_Registration_Place__c
                                    , Account__r.CR_Registration_Expiry_Date__c
                                    , Account__r.Email__c
                                    , Account__r.First_Name_Arabic__pc
                                    , Account__r.Middle_Name_Arabic__pc
                                    , Account__r.Last_Name_Arabic__pc
                                    , Account__r.Passport_Number_Arabic__pc
                                    , Account__r.Title_Arabic__pc
                                    , Account__r.Address_Line_1_Arabic__pc
                                    , Account__r.Address_Line_2_Arabic__pc
                                    , Account__r.Address_Line_3_Arabic__pc
                                    , Account__r.Address_Line_4_Arabic__pc
                                    , Account__r.City_Arabic__pc
                                    , Account__r.Nationality_Arabic__pc
                                 from Buyer__c 
                                 where Booking__c =:bookingId
                                 order by Primary_Buyer__c desc]){
            if(objB.Account__c != null){
                lstA.add(objB.Account__r);
            }
        }
        return lstA;
    }
    
    public list<Buyer__c> returnBuyers(Id bookingId){
        list<Buyer__c> lstBuyers;
        lstBuyers = [Select b.Title__c
                          , b.CreatedDate
                          , b.LastModifiedDate
                          , b.Title_Arabic__c
                          , b.Place_of_Issue__c
                          , b.Place_of_Issue_Arabic__c
                          , b.Phone__c
                          , b.Phone_Country_Code__c
                          , b.Passport_Number__c
                          , b.Passport_Expiry_Date__c
                          , b.Party_ID__c
                          , b.Organisation_Name__c
                          , b.Organisation_Name_Arabic__c
                          , b.Nationality__c
                          , b.Nationality_Arabic__c
                          , b.Name
                          , b.Last_Name__c
                          , b.Last_Name_Arabic__c
                          , b.Id
                          , b.IPMS_Status__c
                          , b.IPMS_Buyer_ID__c
                          , b.First_Name__c
                          , b.First_Name_Arabic__c
                          , b.Email__c
                          , b.Date_of_Birth__c
                          , b.Country__c
                          , b.Country_Arabic__c
                          , b.City__c
                          , b.City_Arabic__c
                          , b.CR_Registration_Place__c
                          , b.CR_Registration_Place_Arabic__c
                          , b.CR_Registration_Expiry_Date__c
                          , b.CR_Number__c
                          , b.Buyer_Type__c
                          , b.Buyer_ID__c
                          , b.Booking__c
                          , b.Address_Line_4__c
                          , b.Address_Line_4_Arabic__c
                          , b.Address_Line_3__c
                          , b.Address_Line_3_Arabic__c
                          , b.Address_Line_2__c
                          , b.Address_Line_2_Arabic__c
                          , b.Address_Line_1__c
                          , b.Address_Line_1_Arabic__c
                    From Buyer__c b
                    where b.Booking__c = :bookingId
                    order by Primary_Buyer__c desc];
                    return lstBuyers;
    }
}