/*
* Description - Test class developed for CalculateNumberOfUnitsForCallingLstBatch 
*
* Version            Date            Author            Description
* 1.0              03/01/2018                           Initial Draft
*/
@isTest
public class CalculateNoOfUnitsForCallingLstBatchTest {
    
    static testMethod void test_CalculateNoOfUnitsForCallingLstBatch () {
        CalculateNumberOfUnitsForCallingLstBatch batchCls = new CalculateNumberOfUnitsForCallingLstBatch ();
         List<TriggerOnOffCustomSetting__c>settingLst = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst.add(newSetting);
        insert settingLst;
        Id RecordTypeIdCollection = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Collections_Calling_List'
            AND IsActive = TRUE LIMIT 1
        ].Id;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        List<Calling_List__c> lstCallingLists = createCallingList( RecordTypeIdCollection , 5 ,objAcc.Id );
        insert lstCallingLists;
        
        Test.startTest();
            Database.executeBatch(batchCls);
        Test.stopTest();
    }

    /*
     @ Description : To create calling list reocord
     @ Return      : list of calling list to be created
    */
    public static List<Calling_List__c> createCallingList( Id RecordTypeIdCollection, Integer counter , Id acctId ) {
        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        for( Integer i=0; i<counter; i++ ) {
            lstCallingLists.add(new Calling_List__c(IsHideFromUI__c = false, Account__c = acctId  , Customer_Flag__c  = false ,RecordTypeId = RecordTypeIdCollection ) );
        }
        return lstCallingLists;
    }
}