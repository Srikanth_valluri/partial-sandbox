public without sharing class RentalPoolHOCheckController {
    public Id caseId;

    public RentalPoolHOCheckController(ApexPages.StandardController controller) {
        caseId = ApexPages.currentPage().getParameters().get('id');
    }

    public Pagereference checkHOStatus() {
        List<Case> listCase = new List<Case>();
        List<Task> listUpdateTask = new List<Task>();
        listCase = [select Id, Booking_Unit__c, Booking_Unit__r.Handover_Flag__c, Handover__c
                        from Case where Id =: caseId];
        if(listCase[0].Booking_Unit__c != null) {
            if(!listCase[0].Handover__c) {
                if(String.isNotBlank(listCase[0].Booking_Unit__r.Handover_Flag__c) && listCase[0].Booking_Unit__r.Handover_Flag__c == 'Y') {
                    listCase[0].Handover__c = true;
                    update listCase;
    
                    listUpdateTask = [select Id, Subject, Status from Task where Subject =: Label.TaskHOCompletion 
                                        and Process_Name__c =: 'Rental Pool Agreement' 
                                        and WhatId =: caseId and Status != 'Completed'];
                    if(!listUpdateTask.isEmpty()) {
                        listUpdateTask[0].Status = 'Completed';
                        update listUpdateTask;
                    }
                    system.debug('listUpdateTask=='+listUpdateTask);
                }
                else {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error : Awaiting Handover Completion');
                    ApexPages.addMessage(myMsg);
                    return null;
                }
            }
            else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Already Handovered');
                ApexPages.addMessage(myMsg);
                return null;
            }
        }
        else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error : No Booking Unit Exist');
            ApexPages.addMessage(myMsg);
            return null;
        }
        PageReference  pg = new PageReference ('/'+caseId);
        pg.setRedirect(true);
        return pg;
    }
}