/**********************************************************************************************************************
Class Name : API_BookingUnits 
Description : To get the Booking unit details and collections, commissions of an agent based on SRID
Test class : API_BookingUnits_Test
========================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.1     |   15-10-2020      | Subin C Antony	  | Added HTTP callout to IPMS to fetch payment/payment-qualification statuses for BU. Added the additional info to API response.
1.1     |   28-10-2020      | Subin C Antony	  | Changes in remote URL for fetching invoice details from IPMS.
***********************************************************************************************************************/

@RestResource(urlMapping='/API_BookingUnits')
global without sharing class API_BookingUnits {
    public static Boolean IsSandbox;
    static {
        IsSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    
    @HttpGET
    global static void doGET() {
        RestRequest req = RestContext.request;
        String srId = req.params.get('srid');
        System.debug('==='+srId);
        User U = [SELECT ContactID FROM User WHERE ID =: Userinfo.getUserId ()];
        /*
        Contact con = new Contact ();
        try {
            con = [SELECT Name FROM Contact WHERE ID =: u.contactId];
        } catch (Exception e) {}
        */
        Map <ID, Double> bookingUnitCommission = new Map <ID, Double> ();
        // To get the commission amount for the agent
        for (Agent_Commission__c commission : [SELECT Amount__c, Booking_Unit__c 
                                                FROM Agent_Commission__c 
                                                WHERE Booking__r.Deal_SR__c =: srId
                                                AND Amount__c != null])
        {
            
            if (bookingUnitCommission.containsKey (commission.Booking_Unit__c)) {
                bookingUnitCommission.put (commission.Booking_Unit__c, bookingUnitCommission.get (commission.Booking_Unit__c)+commission.Amount__c);
            }
            else {
                bookingUnitCommission.put (commission.Booking_Unit__c, commission.Amount__c);
            }
            
        }
        
        Map <ID, String> buyerNames = new Map <ID, String> ();
        // To get the buyer names based on the booking
        for (Buyer__c buyer : [SELECT Booking__r.Deal_Sr__c, Title__c, First_Name__c, Last_Name__c 
                                FROM Buyer__c 
                                WHERE Primary_Buyer__c = true 
                                AND 
                                Booking__r.Deal_Sr__c =: srId])
        {
            String buyerName = '';
            if (buyer.Title__c != NULL)
                buyerName = buyer.Title__c+' ';
            
            if (buyer.First_Name__c != NULL)
                buyerName += buyer.First_Name__c +' ';

            if (buyer.Last_Name__c != NULL)
                buyerName += buyer.Last_Name__c ;
                
            buyerNames.put (buyer.Booking__r.Deal_sr__c, buyerName);
        } 
        
        // TO prepare Unit details JSON
        List <UnitDetails> unitDetails = new List <UnitDetails> ();
        Set <String> regIdSet = new Set<String>(); /* Added on 15 Oct 2020 */
        for (Booking_Unit__c bUnit : [SELECT Unit_Location__c, Booking__r.Deal_SR__r.App_Booking_Status__c, 
                                        Booking__r.Deal_SR__r.Name, Booking__r.Deal_SR__r.Total_Booking_Amount__c,
                                        Registration_Date__c, Registration_ID__c, Booking__r.Deal_Sr__r.IPMS_Registration_ID__c,
                                        Token_Amount__c, Property_Name_Inventory__c,
                                        Requested_Price__c,
                                        Total_Collections__c,
                                        Booking__r.Deal_sr__r.NSIBPM__Internal_SR_Status__r.Name,
                                        Booking__r.Deal_Sr__r.Agency__c,
                                        Booking__r.Deal_Sr__r.Agency__r.Name,
                                        Booking__r.Deal_Sr__r.Agent_Name__r.contact.id,
                                        Booking__r.Deal_Sr__r.Agent_Name__r.contact.name,
                                        Inventory__r.Marketing_Plan__c // Added on April 2
                                    FROM Booking_Unit__c
                                    WHERE 
                                    Booking__r.Deal_Sr__c =: srId 
                                    AND Booking__r.Deal_Sr__c != null])
        {
            UnitDetails unit = new UnitDetails ();
            unit.bookingStatus = bUnit.Booking__r.Deal_SR__r.App_Booking_Status__c;
            unit.srNumber = bUnit.Booking__r.Deal_sr__r.Name;
            unit.srId = bUnit.Booking__r.Deal_Sr__c;
            unit.bookingUnitId = bUnit.Id;
            
            if (buyerNames.containsKey (bUnit.Booking__r.Deal_Sr__c))
                unit.buyerName = buyerNames.get (bUnit.Booking__r.Deal_Sr__c);
            else
                unit.buyerName = '';
            
            unit.unitName = bUnit.Unit_Location__c;
            unit.registrationDate = bUnit.Registration_Date__c;
            unit.bookingAmount = bUnit.Requested_Price__c;
            unit.collectionAmount = bUnit.Total_Collections__c;
            if (bookingUnitCommission.containsKey (bUnit.Id))
                unit.commissionAmount = bookingUnitCommission.get (bUnit.Id);
            else
                unit.commissionAmount = 0;
                
            unit.agencyName = bUnit.Booking__r.Deal_Sr__r.Agency__r.Name;
            unit.agencyId = bUnit.Booking__r.Deal_Sr__r.Agency__c;
            unit.projectName = bUnit.Property_Name_Inventory__c;
            //unit.contactName = con.Name;
            //unit.contactId = u.ContactId;
            unit.contactName = bUnit.Booking__r.Deal_Sr__r.Agent_Name__r.contact.name;
            unit.contactId = bUnit.Booking__r.Deal_Sr__r.Agent_Name__r.contact.id;
            unit.marketingName = bUnit.Inventory__r.Marketing_Plan__c;
            unit.internalSrStatus = bUnit.Booking__r.Deal_sr__r.NSIBPM__Internal_SR_Status__r.Name;
            unit.registrationId = bUnit.Registration_ID__c;
            unitDetails.add (unit);
            if(NULL != bUnit.Registration_ID__c) {
                regIdSet.add(bUnit.Registration_ID__c); /* Added on 15 Oct 2020 */
            }
        }
        
        /* Section Modified on 15 Oct 2020 : to fetch RegID for BU and get payment Status from IPMS ......................BEGIN */
        IPMS_ResponseWraper remotePaymentStatusResponse = getPaymentStatusFromIPMS(regIdSet);
        Map <String, IPMS_ResponseDataItem> paymentStatusMapByRegId = new Map<String, IPMS_ResponseDataItem>();
        if(remotePaymentStatusResponse.meta_data.status_code == 1 && NULL != remotePaymentStatusResponse.data && remotePaymentStatusResponse.data.size() > 0) {
            for(IPMS_ResponseDataItem item : remotePaymentStatusResponse.data){
                paymentStatusMapByRegId.put(item.registration_id, item);
            }
        }
        
        for(UnitDetails unit : unitDetails) {
            if(paymentStatusMapByRegId.containsKey(unit.registrationId)) {
                IPMS_ResponseDataItem remotePaymentStatusData = paymentStatusMapByRegId.get(unit.registrationId);
                unit.invoice_types = new Set<String>();
                if(NULL != remotePaymentStatusData.invoice_types && remotePaymentStatusData.invoice_types.size() > 0) {
                	unit.invoice_types.addAll(remotePaymentStatusData.invoice_types);
                }
                unit.policy_run_qualification = remotePaymentStatusData.policy_run_qualification;
                unit.payment_status = remotePaymentStatusData.payment_status;
                
                /* COMMENT LATER ####################################################################### */
                for(String invTyp : unit.invoice_types) {
                    if(invTyp.equalsIgnoreCase('invoice')) {
                        unit.invoice_types.remove(invTyp);
                        unit.invoice_types.add('Tax_Invoice');
                    }
                } /* COMMENT LATER ##################################################################### */
            }
        }
        /* Section Modified on 15 Oct 2020 : to fetch RegID for BU and get payment Status from IPMS ......................END */
        
        // To send response back to API
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(unitDetails));
    } 
    
    /*
     * Method to perform HTTP callout to IPMS for payment status */  /* Added on 15 Oct 2020 */
    private static IPMS_ResponseWraper getPaymentStatusFromIPMS(Set<String> regIds) {
        IPMS_ResponseWraper responseWraper = new IPMS_ResponseWraper();
        IPMS_ResponseMetaData metaData = new IPMS_ResponseMetaData();
        List <IPMS_ResponseDataItem> responseDataList = new List<IPMS_ResponseDataItem>();
        
        if(regIds.size() < 1) {
            responseWraper.meta_data = getErrorMetaDataForResponse(3,'Bad Request', 'Empty list of Arguments', NULL);
        }
        else {
            String regIdStringQuery = '';
            String seperator = '';
            for(String regId : regIds){
                regIdStringQuery += (seperator + regId);
                seperator = ',';
            }
            
            HttpRequest paymentStatusRequest = new HttpRequest();
            String baseUrl = IsSandbox ? 'https://ptctest.damacgroup.com/agent-app/api/v1/' : 'https://ptc.damacgroup.com/agentapp-prod/api/v1/';
            paymentStatusRequest.setEndpoint(baseUrl + 'customers/registrations?reg-id=' + regIdStringQuery);
            paymentStatusRequest.setMethod('GET');
            paymentStatusRequest.setTimeout(120000);
            
            HttpResponse paymentStatusResponse;
            String exceptionMsg;
            try{
                if(!Test.isRunningTest()) {
                    paymentStatusResponse = new Http().send(paymentStatusRequest);
                }
                else {
                    // for test excecution context
                    paymentStatusResponse  = new HttpResponse();
                    paymentStatusResponse.setStatus('OK');
                    paymentStatusResponse.setStatusCode(200);
                    paymentStatusResponse.setBody('{"data":[{"invoice_types":["Invoice"],"policy_run_qualification":"Audit Completed,Doc Ok,Deposit Received,Collection Received","payment_status":null,"registration_id":1234,"agent_id":null}],"meta_data":{"status_code":1,"message":"Success","title":"Success","developer_message":null}}');
                }
                
                system.debug(paymentStatusResponse.getBody());
                if(paymentStatusResponse.getStatusCode() == 200 && NULL != paymentStatusResponse.getBody() && 
                   !(paymentStatusResponse.getBody().equals(''))){
                    responseWraper = (IPMS_ResponseWraper)JSON.deserialize(paymentStatusResponse.getBody(), IPMS_ResponseWraper.class);
                }
            }
            catch(Exception ex){
                system.debug(ex.getMessage());
                system.debug(ex.getStackTraceString());
                responseWraper = NULL;
                exceptionMsg = ex.getMessage() + ex.getStackTraceString();
            }
            
            if(NULL == responseWraper || NULL == responseWraper.data || responseWraper.data.size() < 1) {
                responseWraper = new IPMS_ResponseWraper();
                responseWraper.meta_data = getErrorMetaDataForResponse(2,'Failure', 'Error while fetching data', exceptionMsg);
            }
        }
        
        return responseWraper;
    }
    
    private static IPMS_ResponseMetaData getErrorMetaDataForResponse(Integer code, String title, String message, String devMsg) {
        IPMS_ResponseMetaData metaData = new IPMS_ResponseMetaData();
        metaData.status_code = code;
        metaData.title = title;
        metaData.message = message;
        metaData.developer_message = devMsg;
        return metaData;
    }
    
    public class IPMS_ResponseWraper {
        public IPMS_ResponseMetaData meta_data;
        public List <IPMS_ResponseDataItem> data;
    }
    
    public class IPMS_ResponseMetaData {
        public Integer status_code;
        public String message;
        public String title;
        public String developer_message;
    }
    
    public class IPMS_ResponseDataItem {
        public String registration_id;
        public String agent_id;
        public List<String> invoice_types;
        public String policy_run_qualification;
        public String payment_status;
    }
    
    // To prepare the Unit details JSON as response
    public class unitDetails {
        public String bookingStatus;
        public String srNumber;
        public ID srId;
        public ID bookingUnitId;
        public String registrationId;
        public String buyerName;
        public String unitName;
        public Date registrationDate;
        public Double bookingAmount;
        public Double collectionAmount;
        public Double commissionAmount ;
        public String agencyName;
        public Id agencyId;  
        public String projectName ;
        public String contactName;
        public String contactId; 
        public string marketingName;
        public String internalSrStatus;
        public Set<String> invoice_types; /* Added on 15 Oct 2020 */
        public String policy_run_qualification; /* Added on 15 Oct 2020 */
        public String payment_status; /* Added on 15 Oct 2020 */
    }   
}