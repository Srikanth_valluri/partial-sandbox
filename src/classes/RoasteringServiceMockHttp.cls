/***************************************************************************************************************
Description: Mock class for RoasteringSerivce.
________________________________________________________________________________________________________________
Version Date(DD-MM-YYYY)    Author              Description
----------------------------------------------------------------------------------------------------------------
1.0     16-07-2020          Komal Shitole       Initial Draft
****************************************************************************************************************/
@isTest
global class RoasteringServiceMockHttp implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req){
        String temp = '';
        HttpResponse res = new HttpResponse();
        System.debug(req.getEndpoint());
        res.setStatusCode( 200 );
        
        if(req.getEndpoint().containsIgnoreCase('attendance-daily')) {
            temp = '{"attendance-daily": [{"userid": "1341","workingshift": "GS","firsthalf":"AB","secondhalf": "AB","username": "Dax Karackal Philip","processdate": "01/07/2020","punch1": "01/07/2020 13:03:07","punch2": "01/07/2020 13:04:23","latein": "0","earlyout": "0","overtime": "0","worktime": "1"}]}';
        }
        else if(req.getEndpoint().containsIgnoreCase('holiday')) {
            temp = '{"holiday": [{"user-id": "1341","user-name": "Mohammed Ather Aqil","short-name": "Mohammed Ather","schedule-id": 1,"schedule-name": "Corporate Off","year": 2020,"holiday-name": "New Year 2020","holiday-date": "01/01/2020-01/01/2020"},{"user-id": "1341","user-name": "Mohammed Ather Aqil","short-name": "Mohammed Ather","schedule-id": 1,"schedule-name": "Corporate Off","year": 2020,"holiday-name": "Eid Holidays","holiday-date": "05/24/2020-05/26/2020"}]}';
        }
        else {
            temp = '{"shift-details": [{"shift-id": "GS","shift-name": "General0830to1800","shift-start": "08:30","shift-end": "18:00","break-start": "00:00","break-end": "00:00","late-in-grace": "00:15","early-out-grace": "00:00","type": 0}]}';
        }
        res.setBody( temp );
        return res;
        
    }
    
}