/*-------------------------------------------------------------------------------------------------
Description: Class to generate Notice Document from drawloop
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 07-04-2018       | Deepika Rajput   | 1. Added logic to generate Notice Document from drawloop
=============================================================================================================================
*/
public class FMGenerateNoticeDocument {
    public String fmCaseId;
    public fm_case__c objFMCase;
    public FMGenerateNoticeDocument(ApexPages.StandardController stdCon){
        fmCaseId = stdCon.getId();
        objFMCase = [SELECT Id
            , Booking_Unit__c
            , Notice_Type__c
           FROM fm_case__c 
           WHERE Id =: fmCaseId];
    }
    
    public void callDrawloop(){
        executeBatch();
    }
    
    public pageReference returnToCase(){
        PageReference Pg = new PageReference('/'+fmCaseId);
        return pg;
    }
    
    public void executeBatch() {
        system.debug('objFMCase====='+objFMCase);
        if(objFMCase.Notice_Type__c != NULL){
            FM_GenerateDrawloopDocumentBatch  objInstance;
            if(objFMCase.Notice_Type__c == 'First Notice'){
                objFMCase.First_Notice_Generation_Date__c = System.today();
                objInstance = new FM_GenerateDrawloopDocumentBatch (fmCaseId
                                                        , System.Label.First_Notice_DDPID
                                                        , System.Label.First_Notice_Template_ID);
            } else {
                objInstance = new FM_GenerateDrawloopDocumentBatch (fmCaseId
                                                        , System.Label.Immediate_Notice_DDPID
                                                        , System.Label.Immediate_Notice_Template_ID);
            }
            // Update FM case with First Notice Generation date
            update objFMCase;
            
            Id batchId = Database.ExecuteBatch(objInstance);
            if(String.valueOf(batchId) != '000000000000000'){      
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Your request for Notice document was successfully submitted. Please check the documents section for the document in a while.');
              ApexPages.addMessage(myMsg);
              /*list<task> lstTask = new list<task>();
              lstTask = [SELECT id, whatId, subject, status
                         FROM Task
                         WHERE whatId =: fmCaseId
                         AND subject =: label.Violation_PM_Task limit 1];
              if(lstTask[0].status != 'Closed' || lstTask[0].status !='Completed'){
                 lstTask[0].status = 'Closed'; 
              }
              update lstTask[0];
              objFMCase.status__c = 'Closed';              
              update objFMCase;*/
              
            }else{
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Your request for Notice document could not be completed. Please try again later.');
              ApexPages.addMessage(myMsg);
            }
        } else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select the Notice Type to proceed.');
            ApexPages.addMessage(myMsg);
        }        
    }
} // end of class