/**
 * Simple service to make http callout to
 * Bitly url shortener service.
 */
public class BitlyService {
    
    // reusable access token for oauth,
    // required when making API requests
    private String accessToken;
    
    public BitlyService() {
        //this.accessToken = getAccessToken();
    }
    
    /**
     * Given a long URL will return the shortened version.
     * http://dev.bitly.com/links.html#v3_shorten
     */
    public String shorten( String url ) {

        HttpRequest req = new HttpRequest();
        req.setEndpoint(
            'callout:Bitly/v3/shorten' +
            '?access_token=' + Label.Bitly_Access_Token +
            '&longUrl=' + EncodingUtil.urlEncode( url, 'UTF-8' ) +
            '&format=txt'
        );
        req.setMethod('GET');

        Http http = new Http();
        HttpResponse res = http.send(req);
        return res.getBody();
    }
    
    /*private String getAccessToken() {

        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:Bitly/oauth/access_token');
        req.setMethod('POST');
        req.setHeader('Content-Length','200');
        req.setTimeout(20000);
        Http http = new Http();
        HttpResponse res = http.send(req);
        system.debug( 'access token-' + res.getBody());
        return res.getBody();
    }*/
    
}