@isTest
public class CAFMBatchTest {
    /*@isTest static void test_method_one() {
        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Helpdesk').getRecordTypeId();
        FM_Case__c caseObj = new FM_Case__c();
        caseObj.Task_Id__c = 10;
        caseObj.RecordTypeId = devRecordTypeId;
        caseObj.Status__c = 'Submitted';
        insert caseObj;
        Test.setMock( WebServiceMock.class, new MaintenanceRequestMock() );
        Test.StartTest();
		CAFMBatchScheduler cafmSch = new CAFMBatchScheduler();
		String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, cafmSch); 
        Test.stopTest();
        
    }*/
     @isTest static void test_method_two() {
        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Helpdesk').getRecordTypeId();
        FM_Case__c caseObj = new FM_Case__c();
        caseObj.Task_Id__c = 10;
        caseObj.RecordTypeId = devRecordTypeId;
        caseObj.Status__c = 'Submitted';
        insert caseObj;
         Test.setMock( WebServiceMock.class, new MaintenanceRequestMock() );
        //Test.setMock( WebServiceMock.class, new MaintenanceRequestTaskByIDMock() );
        Test.StartTest();
        CAFMBatchScheduler cafmSch = new CAFMBatchScheduler();
        CAFMBatchScheduler.cronExp = '0 0 0 ? * * *';
        CAFMBatchScheduler.jobName = 'CAFMBatchScheduler12345';
        cafmSch.scheduleMyJob();    
        Test.stopTest();        
    }
   
}