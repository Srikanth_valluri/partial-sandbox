global class TagUserNameonCallLogBatch implements Database.Batchable<sObject>{
//**************AKISHOR: stamp cre ref*******************//
    
   
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query;
        
        query = 'SELECT Id,Ext2__c,TL__c,Call_Recording_URL__c'+
            ' FROM Call_Log__c where Ext2__c != NULL AND TL__c = NULL  AND Ext2__c != \'\'';
         System.debug('query'+query);
       // List<Call_Log__c> BUList = [Select  from Call_Log__c where Ext2__c!=''];
        return Database.getQueryLocator(query);
    }
    /**
    
            String strTest = 'https://damacipt.damacgroup.com/recordings/6501/[Rebin Rajan]_6501-00971529250447_20200418125901(126057).wav';
        strTest =strTest.substringAfter('_');
        strTest =strTest.substringBefore('_');
        String strEX1 ;
        String strEX2 ;
        IF(strTest.length()== 9){
            strEX1 = strTest.substringBefore('-');
            strEX2 = strTest.substringBefore('-');
        }
        System.debug('strEX1'+strEX1);
        System.debug('strEX2'+strEX2);
        System.debug('strTest'+strTest);

        String strTest = 'https://damacipt.damacgroup.com/recordings/6501/[Rebin Rajan]_6501-6502_20200416162122(121760).wav';
        strTest =strTest.substringAfter('_');//6501-6502_20200416162122(121760).wav
        strTest =strTest.substringBefore('_');//6501-6502
        String strEX1 ;
        String strEX2 ;
        IF(strTest.length()== 9){
            strEX1 = strTest.substringBefore('-');
            strEX2 = strTest.substringAfter('-');
        }
        System.debug('strEX1'+strEX1);
        System.debug('strEX2'+strEX2);
        System.debug('strTest'+strTest);
    */
    
    global void execute(Database.BatchableContext BC, List<Call_Log__c> CollList){

        System.debug('CollList:'+CollList);
        //
        Map<String, List<Call_log__c>> mapExtnCalllog = new Map <String, List<Call_log__c> > ();
        
        Map<String, User>  mapExtnUser = new Map<String, User>();
        
        List <Call_Log__c> CLList = new List <Call_Log__c> ();
        for(Call_Log__c callLog : CollList){
            String strTest = callLog.Call_Recording_URL__c;
            strTest =strTest.substringAfter('_');
            strTest =strTest.substringBefore('_');
            String strEX1 ;
            String strEX2 ;
            
            if(strTest.length()== 9){
                strEX1 = strTest.substringBefore('-');
                strEX2 = strTest.substringAfter('-');
            }
            
            if(String.isNotBlank(strEX2)){
                if(mapExtnCalllog.containsKey(strEX2)) {
                    mapExtnCalllog.get(strEX2).add(callLog);
                } else {
                    mapExtnCalllog.put(strEX2, new List<Call_log__c> { callLog });
                }
                
           }
            
                    
        }
        System.debug('mapExtnCalllog:'+mapExtnCalllog);
       
       
       
       
         List<User> UList = [Select Id,Extension,
                                      IsActive
                                      from User where Extension IN: mapExtnCalllog.keySet() AND IsActive =true];
                                      
        for(User objUser : UList){
            mapExtnUser.put(objUser.Extension,objUser);
        }
        
        System.debug('mapExtnUser'+mapExtnUser);
        for(String strExtention : mapExtnCalllog.keyset()) {
        System.debug('strExtention'+strExtention );
            for(Call_log__c callLogObj :mapExtnCalllog.get(strExtention)) {
                
                if(mapExtnUser.containsKey(strExtention)){
                    System.debug('MapUserGet'+mapExtnUser.get(strExtention).Id);
                    callLogObj.TL__c = mapExtnUser.get(strExtention).Id;
                }
                CLList.add(callLogObj);
            }
        }
        
        if(CLList.size() > 0){Update CLList;}//update call log record
        
        
    }
    global void finish(Database.BatchableContext BC){
    }
 }