global class BatchToSendHospitalityScheduler implements Schedulable {
   global void execute(SchedulableContext sc) {
	  BatchToSendHospitalityStatments objHospitality = new BatchToSendHospitalityStatments();
	  DataBase.executeBatch(objHospitality,1); 
   }
}