/************************To generate SOA for call letters*****************************
//AKISHOR
**************************************************************************************/
global Class GenerateSOAForPCC {
   
    static String endurl;
    Static List<Error_Log__c>  listErrorLogs ;//for logs
    
    public static String generateSOA(String regId, String RecordId) {//, String dpClId
        listErrorLogs  = new List<Error_Log__c>();
        String responseMsg = '';//regId='94278';RecordId='a52250000004fEv';
        endurl = Label.DP_Soa_Rest_Endpoint + regId;
        system.debug('generateDPSoa endurl === ' + endurl);
        if(String.isNotBlank(endurl)) {  
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endurl);
            req.setMethod('GET');
            req.setHeader('ACCEPT','application/json');
            req.setHeader('Content-Type', 'application/json');
            req.setTimeout(120000);
            System.debug('generateDPSoa request === ' + req);
            Http http = new Http();
            HTTPResponse res;
            res = http.send(req);
            system.debug('generateDPSoa response === ' + res);
            system.debug('generateDPSoa response body === ' + res.getBody());            
            
            if(res.getStatusCode() == 200) {
                DPSoaResponseWrapper soaResWrapObj = new DPSoaResponseWrapper();
                soaResWrapObj = (DPSoaResponseWrapper)JSON.deserialize(
                res.getBody(), DPSoaResponseWrapper.class);
                system.debug('generateDPSoa soaResWrapObj === ' + soaResWrapObj);
                if(soaResWrapObj.actions != NULL && soaResWrapObj.actions.size() > 0) {
                    responseMsg = soaResWrapObj.Actions[0].url;
                    if(responseMsg !='')//update SOA URL in DP Inv object
                    {
                        List<DP_Invoices__c> UpdInv= new List<DP_Invoices__c>();
                        List <DP_Invoices__c> DPInv=[Select Id from DP_Invoices__c where Id=: RecordId];
                        if(DPInv.size() > 0){
                            DP_Invoices__c DPI = new DP_Invoices__c();
                            DPI.Id=DPInv[0].Id;
                            DPI.SOA__c=responseMsg;
                           UpdInv.add(DPI); 
                        }
                        Update UpdInv;
                    }
                }
                else {
                    responseMsg = 'Error : actions does not recieved.';
                    //Create Error Log
                    //createErrorLog(responseMsg, accountId, buId);
                }
            }
            else {
                responseMsg = 'Error : ' + res.getBody();
                 //Create Error Log
                //createErrorLog(responseMsg, accountId, buId);
            }
        }
        else {
            responseMsg = 'Error : Blank endpoint URL.';
            //createErrorLog(responseMsg, accountId, buId);
        }
        
        if(!listErrorLogs.isEmpty() && listErrorLogs.size() > 0) {
            insert listErrorLogs;
        }
        return responseMsg;
}
 public Class DPSoaResponseWrapper {
        String responseId, responseTime;
        List<ActionsWrapper> actions;
        public DPSoaResponseWrapper() {
            responseId = '';
            responseTime = '';
            actions =  new List<ActionsWrapper>();
        }
    }
    
    public Class ActionsWrapper {
        String action, method,url;
    }
}