//  Emails list – show email on hover + multiselect and close with close reason (e.g. spam) in one shot without reading. Give access to Contact Center Director and TL only.
public class CloseEmailSRController {
 
    //Our collection of the class/wrapper objects wrapSR
    public List<wrapSR> wrapSRList {get; set;}
    public List<Case> selectedSRs{get;set;}
    public Id EmailRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
    public set<String> setStatusVal = new set<String>{'Closed','Rejected','Cancelled'};
    String filterId;
    public Case objCaseInitial {get;set;}
    
    public CloseEmailSRController( ApexPages.StandardSetController controller ) {
        //Id EmailRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        objCaseInitial = new Case();
        filterId = controller.getFilterId();
    }
    
    public void init() {
        system.debug('<<< in init');
        if( wrapSRList == null ) {
            wrapSRList = new List<wrapSR>();
             Id Contact_Center_QueueId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName( 'Contact_Center_Queue','Queue' );
             Id Unregister_Email_Service_RequestQueueId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName( 'Unregister_Email_Service_Request','Queue' );
            for( Case objCase: [SELECT Id, CaseNumber,Subject, Origin ,Status,CreatedDate
                                     , Reason,Comments,RecordTypeId,OwnerId,Owner.Name,SuppliedEmail
                                  FROM Case 
                                 WHERE RecordTypeId =: EmailRecTypeId AND Origin = 'Email' 
                                   AND Status != 'Closed' AND Status != 'Rejected' AND Status != 'Cancelled'
                                   AND ( OwnerId =: Contact_Center_QueueId OR OwnerId =: Unregister_Email_Service_RequestQueueId ) ] ) {
                // As each Account is processed we create a new wrapAccount object and add it to the wrapSRList
                
                wrapSRList.add(new wrapSR(objCase));
            }
         
        }
    }
    
    @RemoteAction
    public static String setDescription( String strId ) {
        String setDesc;
        if( String.isNotBlank(strId) ) {
            List<Case> objCase = [ SELECT Description 
                                     FROM Case 
                                    WHERE Id =: strId limit 1];
            setDesc = objCase[0].Description;
        }
        return setDesc;
    }

    public pagereference processSelected() {
        selectedSRs = new List<Case>();
        String strString = Apexpages.currentPage().getParameters().get('node');
        if( String.isNotBlank(strString) ) {
            system.debug('strString : ' + strString);
            
            List<String> lstCaseNumber = (List<String>)System.JSON.deserialize(strString, List<String>.class);
            system.debug(lstCaseNumber);
            //system.assert( false,' objCaseInitial :' +objCaseInitial );
            for(Case objCase : [SELECT Id,Reason_to_Close_Email_Case__c, CaseNumber,Subject, Description ,Origin ,Status ,Reason,Comments,                      RecordTypeId,OwnerId,Owner.Name,SuppliedEmail,CreatedDate
                                  FROM Case 
                                 WHERE RecordTypeId =: EmailRecTypeId AND Origin = 'Email' AND Status NOT IN: setStatusVal
                                   AND CaseNumber IN: lstCaseNumber]) {
                if( objCase.Status != 'Closed' ) {
                    objCase.Status = 'Closed';
                    objCase.Reason = objCaseInitial.Reason_to_Close_Email_Case__c;
                    objCase.Comments = 'Closed';
                    objCase.Reason_to_Close_Email_Case__c = objCaseInitial.Reason_to_Close_Email_Case__c;
                    selectedSRs.add( objCase );
                }
            }
            if( selectedSRs != null && selectedSRs.size() > 0 ) {
                update selectedSRs;
            }
            PageReference reRend = new PageReference('/apex/CloseEmailSR');
            reRend.setRedirect(true);
            return reRend;
            //return new Pagereference(String.format(Label.Case_Lightning_List_View_URL, new List<String>{filterId}));
            //return new Pagereference('/500?fcf='+filterId);
            //return new ApexPages.Action('{!List}').invoke();
        }
        
        return null;
    }
 
    public pagereference processSelectedForOwnerChange() {
        selectedSRs = new List<Case>();
        String strString = Apexpages.currentPage().getParameters().get('node1');
        if( String.isNotBlank(strString) ) {
            system.debug('strString : ' + strString);
            
            List<String> lstCaseNumber = (List<String>)System.JSON.deserialize(strString, List<String>.class);
            system.debug(lstCaseNumber);
            //system.assert( false,' objCaseInitial :' +objCaseInitial.OwnerId );
            
            for(Case objCase : [SELECT Id,Reason_to_Close_Email_Case__c, CaseNumber,Subject, Description ,Origin ,Status ,Reason,Comments,                          RecordTypeId,OwnerId
                                  FROM Case 
                                 WHERE RecordTypeId =: EmailRecTypeId AND Origin = 'Email' AND Status NOT IN: setStatusVal
                                   AND CaseNumber IN: lstCaseNumber]) {
                if( objCase.Status != 'Closed' ) {
                    objCase.OwnerId = objCaseInitial.OwnerId;
                    selectedSRs.add( objCase );
                }
            }
            if( selectedSRs != null && selectedSRs.size() > 0 ) {
                update selectedSRs;
            }
            PageReference reRend = new PageReference('/apex/CloseEmailSR');
            reRend.setRedirect(true);
            return reRend;
            //return new Pagereference(String.format(Label.Case_Lightning_List_View_URL, new List<String>{filterId}));
            //return new Pagereference('/500?fcf='+filterId);
            //return new ApexPages.Action('{!List}').invoke();
        }
        
        return null;
    }
 
 
 
    // This is our wrapper/container class. In this example a wrapper class contains both the standard salesforce object Account and a Boolean value
    public class wrapSR {
        public Case objCase {get; set;}
        public Boolean selected {get; set;}
 
        public wrapSR( Case objCase ) {
            this.objCase = objCase;
            this.selected = false;
        }
    }
}