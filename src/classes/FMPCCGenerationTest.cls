@isTest
private class FMPCCGenerationTest {

    static testMethod void testFMPCC() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        test.startTest();
          SOAPCalloutServiceMock.returnToMe = new Map<String, FMPCC1.fmPccResponse_element>();
            FMPCC1.fmPccResponse_element response1 = new FMPCC1.fmPccResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
          Case objCase = new Case();
          objCase.Status = 'New';
          objCase.Priority = 'High';
          objCase.Origin = 'Email';
          insert objCase;
          FMPCCGeneration.GetFMPCC( 'PCC-DFM', lstBookingUnits[0].Registration_Id__c, objCase.Id);
        test.stopTest();
        
    }
}