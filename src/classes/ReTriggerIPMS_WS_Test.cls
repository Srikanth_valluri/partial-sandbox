/******************************************************************
Methods for Retriggering IPMS webservice
Version   Author        Comments
1.0     Alok Chauhan   Added Method to Retrigger Vendor Creation WS

*****************************************************************/
@isTest
global class ReTriggerIPMS_WS_Test{
    static testMethod void testVendorRetry()
    {
           Test.startTest(); 
           String sResponse= 'Fail';
           Id accRecordTypeId   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

            Account nuAccn= InitialiseTestData.getCorporateAccount('AgencyTest312');
            insert nuAccn;
            sResponse = ReTriggerIPMS_WS.retriggerVendorCreation(nuAccn.Id);
            Test.stopTest(); 
  // System.assertEquals('Success',sResponse);
 }
 }