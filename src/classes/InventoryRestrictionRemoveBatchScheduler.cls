/**************************************************************************************************
* Name               : InventoryRestrictionRemoveBatchScheduler
* Test Class         : InventoryRestrictionRemoveBatchSchedulerTest
* Description        : Scheduler class for InventoryRestrictionRemoveBatch class.
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst        06/10/2020      Initial Draft.
**************************************************************************************************/
public with sharing class InventoryRestrictionRemoveBatchScheduler implements Schedulable{

    public void execute(SchedulableContext SC) {
        InventoryRestrictionRemoveBatch batchObject = new InventoryRestrictionRemoveBatch();
        Database.executeBatch(batchObject, 10); 
    }
}