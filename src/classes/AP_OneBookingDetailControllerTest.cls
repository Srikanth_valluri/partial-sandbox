@istest
public class AP_OneBookingDetailControllerTest {
    @testSetup static void setupData() {
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        loc.Property_ID__c = '123';
        insert loc; 

        Payment_Plan__c pp = new Payment_Plan__c();        
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today(); 
        pp.Building_Location__c =  loc.id;  
        pp.term_id__c = '1234';   
        insert pp;   

        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c  = 1;
        newProperty.Property_Code__c    = 'VIR' ;
        newProperty.Property_Name__c    = 'VIRIDIS @ AKOYA OXYGEN' ;
        newProperty.District__c = 'AL YUFRAH 2' ;
        newProperty.AR_Transaction_Type__c  = 'INV VIR' ;
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR' ;
        newProperty.Brokerage_Distribution_Set__c   = '11600' ;
        newProperty.Sales_Commission_Dist_Set__c    = '11601' ;
        newProperty.Currency_Of_Sale__c = 'AED' ;
        newProperty.Signature_Col_Customer_Stmt__c  = 'Front Line Investment Management Co. LLC' ;
        newProperty.EOI_Enabled__c = true;
        insert newProperty; 

        Inventory__c invent = new Inventory__c();
        invent.CM_Price_Per_Sqft__c = 20;
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1' ;
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.ID;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Released';
        invent.special_price__c = 1000000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234'; 
        invent.Floor_Package_ID__c = ''; 
        invent.building_location__c = loc.id;
        invent.property_id__c = newProperty.id;
        insert invent;

        List<NSIBPM__Status__c> createStatus = new List<NSIBPM__Status__c>();
        createStatus = InitialiseTestData.createStatusRecords(
            new List<NSIBPM__Status__c>{
                new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_ADDITIONAL_INFO', Name = 'AWAITING_ADDITIONAL_INFO'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'MORE_INFO_UPDATED', Name = 'MORE_INFO_UPDATED'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_FFA_AA', Name = 'AWAITING_FFA_AA'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'FFA_AA_UPLOADED', Name = 'FFA_AA_UPLOADED')
                }
            );

        NSIBPM__SR_Steps__c srStep= new NSIBPM__SR_Steps__c();
        insert srStep;

        List<NSIBPM__SR_Template__c> SRTemplateList =  
            InitialiseTestData.createTestTemplateRecords(
                new List<NSIBPM__SR_Template__c>{
                    new NSIBPM__SR_Template__c()
                }
            );

        List<NSIBPM__Service_Request__c> SRList  = InitialiseTestData.createTestServiceRequestRecords(
            new List<NSIBPM__Service_Request__c>{
                new NSIBPM__Service_Request__c(
                    recordTypeId = 
                        InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration'),
                        NSIBPM__SR_Template__c = SRTemplateList[0].Id
                )
            }
        );

        List<NSIBPM__Step__c> createStepList = InitialiseTestData.createTestStepRecords(
            new List<NSIBPM__Step__c>{
                new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id, 
                                    NSIBPM__Status__c = createStatus[0].Id, 
                                    NSIBPM__SR_Step__c = srStep.id
                    ),
                new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id, 
                                    NSIBPM__Status__c = createStatus[1].Id, 
                                    NSIBPM__SR_Step__c = srStep.id
                )
            }
        );

        NSIBPM__Document_Master__c Dm = new NSIBPM__Document_Master__c();
        Dm.Name = 'Test DM';
        Dm.NSIBPM__Code__c = 'Bank Statement';
        Dm.NSIBPM__Document_Type_Code__c = 'Doc Type';
        insert Dm;

        //List<NSIBPM__SR_Doc__c> SRDoc_List = new List<NSIBPM__SR_Doc__c>();
        NSIBPM__SR_Doc__c srDoc = new NSIBPM__SR_Doc__c();
        srDoc = new NSIBPM__SR_Doc__c();
        srDoc.Name = 'Test SR DOC';
        srDoc.NSIBPM__Is_Not_Required__c = true;
        srDoc.NSIBPM__Status__c = 'Re-upload';
        srDoc.NSIBPM__Document_Master__c = Dm.Id;
        srDoc.NSIBPM__Service_Request__c = SRList[0].id;
        
        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc = new Account();
        acc.LastName= 'Test Account';
        acc.RecordTypeId = accountRTId;        
        insert acc;

        List<Booking__c> lstBookings = new List<Booking__c>();
        lstBookings.add(InitializeSRDataTest.createBooking(SRList[0].id));
        //lstBookings.add(InitializeSRDataTest.createBooking(SRList[0].id));
        lstBookings[0].Deal_SR__c = SRList[0].id;
        lstBookings[0].Inquiry_Account_Id__c = acc.id;       
        insert lstBookings;

        List<Booking_Unit__c> BUList = new List<Booking_Unit__c>();
        Booking_Unit__c bu1 = new Booking_Unit__c();
        bu1.Booking__c = lstBookings[0].id;
        bu1.Payment_Method__c = 'Cash';
        bu1.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu1.Primary_Buyer_s_Name__c = 'testNSI';
        bu1.Primary_Buyer_s_Nationality__c = 'Russia';
        bu1.Inventory__c = invent.id;
        bu1.Registration_ID__c = '1234'; 
        bu1.Payment_Plan_Id__c = pp.id;     
        bu1.Requested_Price_AED__c = 500;     
        BUList.add(bu1);
        insert BUList;

        buyer__c b = new buyer__c();
        b.Buyer_Type__c =  'Individual';
        b.Address_Line_1__c =  'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.City__c = 'Dubai' ;       
        b.Account__c = acc.id;
        b.dob__c = system.today().addyears(-30);
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname' ;
        b.Last_Name__c =  'lastname';
        b.Nationality__c = 'Indian' ;
        b.Passport_Expiry__c = system.today().addyears(20) ;
        b.Passport_Number__c = 'J0565556' ;
        b.Phone__c = '569098767' ;
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b.Place_of_Issue__c =  'India';
        b.Title__c = 'Mr';
        b.booking__c = lstBookings[0].id;
        b.Primary_Buyer__c =true;
        b.Unique_Key__c = lstBookings[0].id+''+acc.id;
              
        insert b;

        Agent_Commission__c agentCommission = InitialiseTestData.createAgentCommission(acc.Id,System.now().Date(),System.now().Date());
        agentCommission.Booking_Unit__c = BUList[0].Id;
        insert agentCommission;

        Unit_Documents__c unitDoc = new Unit_Documents__c();
        unitDoc.Booking_Unit__c = BUList[0].Id;
        insert unitDoc;

        proof_of_payment__c objPOP = new proof_of_payment__c();
        objPOP.deal_sr__c = SRList[0].id;
        objPOP.booking_unit__c = BUList[0].id;
        objPOP.payment_amount__c = 1432332;
        objPOP.payment_mode__c = 'Cash';
        objPOP.payment_remarks__c = 'AED';
        objPOP.currency__c = 'AED';
        objPOP.payment_date__c = system.today();
        insert objPOP;

        Receipt__c receipt = new Receipt__c();
        receipt.Amount__c = 100;
        receipt.Booking_Unit__c = BUList[0].id;
        insert receipt;

        Payment_Plan__c paymentPlanPrent = new Payment_Plan__c();
        paymentPlanPrent.TERM_ID__c='4546';
        paymentPlanPrent.Booking_Unit__c = BUList[0].id;
        insert paymentPlanPrent;

        Payment_Plan__c paymentPlan = new Payment_Plan__c();
        paymentPlan.Parent_Payment_Plan__c = paymentPlanPrent.id;
        paymentPlan.Booking_Unit__c = BUList[0].id;
        paymentPlan.Status__c = 'Active';
        insert paymentPlan;

        Payment_Terms__c pymTerms = new Payment_Terms__c();
        pymTerms.Payment_Plan__c = paymentPlan.id;
        insert pymTerms;
    }
    static testmethod void testMethod1(){
        List<NSIBPM__Service_Request__c> SRList  = [SELECT Id FROM NSIBPM__Service_Request__c];
        List<Booking__c> lstBookings  = [SELECT Id FROM Booking__c];
        List<Booking_Unit__c> BUList  = [SELECT Id FROM Booking_Unit__c];
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.AP_OneBookingDetail'));
        System.currentPageReference().getParameters().put('Id', SRList[0].Id);
        AP_OneBookingDetailController objController = new AP_OneBookingDetailController();
        objController.listUnitDocsForBU();
        AP_OneBookingDetailController.getlistUnitDocsForBU(BUList[0].Id);
        AP_OneBookingDetailController.getlistAgentCommsForBU(BUList[0].Id);
        AP_OneBookingDetailController.getlistPOPForBU(BUList[0].Id);
        AP_OneBookingDetailController.getlistReceiptForBU(BUList[0].Id);
        AP_OneBookingDetailController.getlistPaymentTermsForBU(BUList[0].Id);
        AP_OneBookingDetailController.getlistPaymentPlansForBU(BUList[0].Id);
        AP_OneBookingDetailController.GenerateURL('99763');
        objController.callSOAGeneration();
        objController.redirectToSRPOPPage();
        AP_OneBookingDetailController.SaveSOAResponse('"http://94.200.40.200:8080/IPMSREPORT/process/1075022"',lstBookings[0].id);
        objController.redirectToPOPPage();
        Test.stopTest();
    }
}