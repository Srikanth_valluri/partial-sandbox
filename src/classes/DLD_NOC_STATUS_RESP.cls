public class DLD_NOC_STATUS_RESP {

    public class Parties {
        //public String id;
        public Integer role;
        public Integer shareArea;
    }

    public Integer amount;
    public String createdAt;
    public String createdBy;
    public String creatorMSP;
    public List<Documents> documents;
    public List<FeeList> feeList;
    public String nocKey;
    public List<Parties> parties;
    public String piKey;
    public String propertyId;
    public String requestRefID;
    public String status;
    public List<ValidationErrors> validationErrors;
    public string remarks;
    
    public class FeeList {
        public Integer amount;
        public String feeId;
        public String feeName;
        public String feeNameAr;
        public String payee;
        public String paymentType;
        public String status;
    }
    public class docs {
        public String docId;    //a412ad6c-7de2-4372-955a-4c8c5fba7b65
        public String hash; //2961ca918a7f2f4f7c34a136bf00d041
    }
    public class Documents {
        public String docName;
        public String docType;
        public List <docs> docs;
        public Boolean isRequired;
        public Integer maxPages;
        public Integer maxSize;
    }
    public class ValidationErrors {
        public String errorMessage;
        public String errorMessageAr;
        public Integer errorNumber;
        public Integer validationType;
    }

    
    public static DLD_NOC_STATUS_RESP parse(String json) {
        return (DLD_NOC_STATUS_RESP) System.JSON.deserialize(json, DLD_NOC_STATUS_RESP.class);
    }
}