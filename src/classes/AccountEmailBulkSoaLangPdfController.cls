public class AccountEmailBulkSoaLangPdfController {

    public Id recordId {get; set;}
    public String responseUrl {get; set;}
    public String accessToken {get; set;}
    public String pdfFile {get; set;}
    public Blob pdfBlob {get; set;}
    public String emailId {get; set;}

    public AccountEmailBulkSoaLangPdfController(ApexPages.StandardController controller) {
        recordId = controller.getId();
        System.debug('Account Id in consturctor: '+recordId);
    }

    public PageReference sendBulkSoaLangPdfEmail() {
        FmIpmsRestCoffeeServices.updateCustomSetting = false;

        Account account = [SELECT Id, Name, Party_ID__c, RecordType.Name, Email__c, Email__pc
                           FROM Account
                           WHERE Id =: recordId];
        String partyId = account.Party_ID__c;
        System.debug('partyId is: '+partyId);

        //Fetch Email field value as per the Type of Account i.e. Business/Person Account
        if(account.RecordType.Name == 'Business Account') {
            System.debug('Its Business Account');
            emailId = account.Email__c;
        }
        else if(account.RecordType.Name == 'Person Account') {
            System.debug('Its Person Account');
            emailId = account.Email__pc;
        }

        System.debug('Email Id of Account: '+emailId);

        responseUrl = FmIpmsRestCoffeeServices.getBulkSoaInLanguage(partyId);
        System.debug('responseUrl is: '+responseUrl);

        if(String.isBlank(responseUrl) || responseUrl == null || responseUrl == 'Null') {
            System.debug('In ApexMessage Method');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.SEVERITY.WARNING, 'There was an error processing your request. Please try again...');
            ApexPages.addMessage(msg);
            return null;
        }

        //Http callout

        accessToken = FmIpmsRestCoffeeServices.accessToken;
        System.debug('AccessToken is' + accessToken);

        HttpRequest req = new HttpRequest();
        req.setEndpoint(responseUrl);
        req.setHeader('Accept', 'application/json');
        req.setMethod('GET');
        req.setHeader('Authorization','Bearer' + accessToken);
        req.setTimeout(120000);
        HttpResponse response = new Http().send(req);
        System.debug('Status Code = ' + response.getStatusCode());
        System.debug('Pdf Body = ' + response.getBodyAsBlob());

        ApexPages.Message pageMessage;

        Map<String, String> languageCodeMap = new Map<String, String> {
            'ab' => 'Abkhazian',
            'aa' => 'Afar',
            'af' => 'Afrikaans',
            'ak' => 'Akan',
            'sq' => 'Albanian',
            'am' => 'Amharic',
            'ar' => 'Arabic',
            'an' => 'Aragonese',
            'hy' => 'Armenian',
            'as' => 'Assamese',
            'av' => 'Avaric',
            'ae' => 'Avestan',
            'ay' => 'Aymara',
            'az' => 'Azerbaijani',
            'bm' => 'Bambara',
            'ba' => 'Bashkir',
            'eu' => 'Basque',
            'be' => 'Belarusian',
            'bn' => 'Bengali',
            'bh' => 'Bihari',
            'bi' => 'Bislama',
            'bs' => 'Bosnian',
            'br' => 'Breton',
            'bg' => 'Bulgarian',
            'my' => 'Burmese',
            'ca' => 'Catalan',
            'ch' => 'Chamorro',
            'ce' => 'Chechen',
            'ny' => 'Chichewa',
            'zh' => 'Chinese',
            'cv' => 'Chuvash',
            'kw' => 'Cornish',
            'co' => 'Corsican',
            'cr' => 'Cree',
            'hr' => 'Croatian',
            'cs' => 'Czech',
            'da' => 'Danish',
            'dv' => 'Divehi',
            'nl' => 'Dutch',
            'dz' => 'Dzongkha',
            'en' => 'English',
            'eo' => 'Esperanto',
            'et' => 'Estonian',
            'ee' => 'Ewe',
            'fo' => 'Faroese',
            'fj' => 'Fijian',
            'fi' => 'Finnish',
            'fr' => 'French',
            'ff' => 'Fula',
            'gl' => 'Galician',
            'gd' => 'Gaelic',
            'gv' => 'Gaelic',
            'ka' => 'Georgian',
            'de' => 'German',
            'el' => 'Greek',
            'kl' => 'Greenlandic',
            'gn' => 'Guarani',
            'gu' => 'Gujarati',
            'ht' => 'Haitian Creole',
            'ha' => 'Hausa',
            'he' => 'Hebrew',
            'hz' => 'Herero',
            'hi' => 'Hindi',
            'ho' => 'Hiri Motu',
            'hu' => 'Hungarian',
            'is' => 'Icelandic',
            'io' => 'Ido',
            'ig' => 'Igbo',
            'id' => 'Indonesian',
            'in' => 'Indonesian',
            'ia' => 'Interlingua',
            'ie' => 'Interlingue',
            'iu' => 'Inuktitut',
            'ik' => 'Inupiak',
            'ga' => 'Irish',
            'it' => 'Italian',
            'ja' => 'Japanese',
            'jv' => 'Javanese',
            'kl' => 'Kalaallisut',
            'kn' => 'Kannada',
            'kr' => 'Kanuri',
            'ks' => 'Kashmiri',
            'kk' => 'Kazakh',
            'km' => 'Khmer',
            'ki' => 'Kikuyu',
            'rw' => 'Kinyarwanda',
            'rn' => 'Kirundi',
            'ky' => 'Kyrgyz',
            'kv' => 'Komi',
            'kg' => 'Kongo',
            'ko' => 'Korean',
            'ku' => 'Kurdish',
            'kj' => 'Kwanyama',
            'lo' => 'Lao',
            'la' => 'Latin',
            'lv' => 'Latvian',
            'li' => 'Limburgish',
            'ln' => 'Lingala',
            'lt' => 'Lithuanian',
            'lu' => 'Luga-Katanga',
            'lg' => 'Luganda,Ganda',
            'lb' => 'Luxembourgish',
            'gv' => 'Manx',
            'mk' => 'Macedonian',
            'mg' => 'Malagasy',
            'ms' => 'Malay',
            'ml' => 'Malayalam',
            'mt' => 'Maltese',
            'mi' => 'Maori',
            'mr' => 'Marathi',
            'mh' => 'Marshallese',
            'mo' => 'Moldavian',
            'mn' => 'Mongolian',
            'na' => 'Nauru',
            'nv' => 'Navajo',
            'ng' => 'Ndonga',
            'nd' => 'Northern Ndebele',
            'ne' => 'Nepali',
            'no' => 'Norwegian',
            'nb' => 'Norwegian bokmål',
            'nn' => 'Norwegian nynorsk',
            'ii' => 'Nuosu',
            'oc' => 'Occitan',
            'oj' => 'Ojibwe',
            'cu' => 'Old Church Slavonic',
            'or' => 'Oriya',
            'om' => 'Oromo (Afaan Oromo)',
            'os' => 'Ossetian',
            'pi' => 'Pāli',
            'ps' => 'Pashto',
            'fa' => 'Persian (Farsi)',
            'pl' => 'Polish',
            'pt' => 'Portuguese',
            'pa' => 'Punjabi',
            'qu' => 'Quechua',
            'rm' => 'Romansh',
            'ro' => 'Romanian',
            'ru' => 'Russian',
            'se' => 'Sami',
            'sm' => 'Samoan',
            'sg' => 'Sango',
            'sa' => 'Sanskrit',
            'sr' => 'Serbian',
            'sh' => 'Serbo-Croatian',
            'st' => 'Sesotho',
            'tn' => 'Setswana',
            'sn' => 'Shona',
            'ii' => 'Sichuan Yi',
            'sd' => 'Sindhi',
            'si' => 'Sinhalese',
            'ss' => 'Siswati',
            'sk' => 'Slovak',
            'sl' => 'Slovenian',
            'so' => 'Somali',
            'nr' => 'Southern Ndebele',
            'es' => 'Spanish',
            'su' => 'Sundanese',
            'sw' => 'Swahili (Kiswahili)',
            'ss' => 'Swati',
            'sv' => 'Swedish',
            'tl' => 'Tagalog',
            'ty' => 'Tahitian',
            'tg' => 'Tajik',
            'ta' => 'Tamil',
            'tt' => 'Tatar',
            'te' => 'Telugu',
            'th' => 'Thai',
            'bo' => 'Tibetan',
            'ti' => 'Tigrinya',
            'to' => 'Tonga',
            'ts' => 'Tsonga',
            'tr' => 'Turkish',
            'tk' => 'Turkmen',
            'tw' => 'Twi',
            'ug' => 'Uyghur',
            'uk' => 'Ukrainian',
            'ur' => 'Urdu',
            'uz' => 'Uzbek',
            've' => 'Venda',
            'vi' => 'Vietnamese',
            'vo' => 'Volapük',
            'wa' => 'Wallon',
            'cy' => 'Welsh',
            'wo' => 'Wolof',
            'fy' => 'Western Frisian',
            'xh' => 'Xhosa',
            'yi' => 'Yiddish',
            'ji' => 'Yiddish',
            'yo' => 'Yoruba',
            'za' => 'Zhuang',
            'zu' => 'Zulu'
        };

        if(response.getStatusCode() == 200) {
            pdfBlob =  response.getBodyAsBlob();
            pdfFile = EncodingUtil.base64Encode(pdfBlob);

            //SendGrid
            //Convert name to Capitalized Case
            String str = account.Name;
            String name = str.toLowerCase();
            List<String> names = name.split(' ');
            for (Integer i = 0; i < names.size(); i++)
                names[i] = names[i].capitalize();
            name = String.join(names, ' ');
            System.debug(name);

            String emailSubject = 'Statement of Account for your units';
            String emailBody = 'Dear ' + name + ',';
            emailBody += '<br/><br/> As requested, please find attached the Statement of Account for your units.';
            emailBody += '<br/><br/><br/> Regards,';
            emailBody += '<br/><br/>' + UserInfo.getName();
            emailBody += '<br/> Officer-Client Relations';
            emailBody += '<br/> DAMAC PROPERTIES Co. LLC.';
            emailBody += '<br/> P.O. Box: 2195, Dubai, United Arab Emirates';
            emailBody += '<br/> Telephone: +971 4 237 5000';
            emailBody += '<br/> Fax: +971 4 373 1373';
            emailBody += '<br/> E-mail: <a href="mailto:atyourservice@damacproperties.com"> atyourservice@damacproperties.com</a>';
            emailBody += '<br/> <a href="http://www.damacproperties.com/">http://www.damacproperties.com/</a>';

            String languageName;
            System.debug('Language Code in Email controller is: '+FmIpmsRestCoffeeServices.languageCode);
            if(String.isEmpty(FmIpmsRestCoffeeServices.languageCode) || FmIpmsRestCoffeeServices.languageCode == null || FmIpmsRestCoffeeServices.languageCode == 'Null') {
                languageName = 'English';
            }
            else if(!String.isEmpty(FmIpmsRestCoffeeServices.languageCode)) {
                languageName = languageCodeMap.get(FmIpmsRestCoffeeServices.languageCode);
            }

            Attachment attach = new Attachment();
            attach.contentType = 'application/pdf';
            attach.name = 'Bulk SOA in '+languageName+'.pdf';
            attach.body = pdfBlob;

            List<Attachment> attachlst = new List<Attachment>();
            attachlst.add(attach);

            List<OrgWideEmailAddress> orgWideAddressHelloDamac = [
                SELECT  Id
                        , Address
                        , DisplayName
                FROM    OrgWideEmailAddress
                WHERE   DisplayName = :Label.DAMAC_no_replysf_damacgroup_com
            ];
            System.debug('@@@Email Address: ' +  orgWideAddressHelloDamac[0].Address);

            List<OrgWideEmailAddress> bccMailAddress = [
                SELECT Id,
                       Address,
                       DisplayName
                FROM   OrgWideEmailAddress
                WHERE  DisplayName = :Label.Sf_Copy_Bcc_Mail_Id
            ];
            System.debug('@@@Bcc mail Id: ' + bccMailAddress[0].Address);

            Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;
            System.debug('IsSandbox is: '+isSandbox);

            if ((isSandbox
                && (emailId.containsIgnoreCase('davidvarghese1989@gmail.com')
                    || emailId.containsIgnoreCase('@damacgroup.com')
                    || emailId.containsIgnoreCase('@eternussolutions.com')
                    || emailId.containsIgnoreCase('@mailinator.com')
                )
            ) || !isSandbox) {
                SendGridEmailService.SendGridResponse sendGridResponse = SendGridEmailService.sendEmailService(
                    emailId, '', '', '', bccMailAddress[0].Address, '', emailSubject, '', orgWideAddressHelloDamac[0].Address, '', '', '',
                    'text/html', emailBody, '', attachlst
                );

                if(sendGridResponse.ResponseStatus == 'Accepted') {
                    EmailMessage mail = new EmailMessage();
                    mail.Subject = emailSubject;
                    mail.MessageDate = System.Today();
                    mail.Status = '3';
                    mail.RelatedToId = recordId;//Put FM case id
                    mail.ToAddress = emailId;
                    mail.FromAddress = orgWideAddressHelloDamac[0].Address;
                    mail.TextBody = emailBody;
                    mail.CcAddress = '';
                    mail.BccAddress = bccMailAddress[0].Address;
                    mail.Sent_By_Sendgrid__c = true;
                    mail.SentGrid_MessageId__c = sendGridResponse.messageId;
                    mail.Account__c = recordId;
                    mail.SendGrid_Status__c = sendGridResponse.ResponseStatus;
                    insert mail;
                    pageMessage = new ApexPages.Message(
                        ApexPages.SEVERITY.CONFIRM, 'An email has been successfully sent to ' + emailId
                    );
                }
                else {
                    Error_Log__c objError = new Error_Log__c();
                    objError.Account__c = recordId;
                    objError.Error_Details__c = 'Bulk SOA Email from SendGrid for Party Id ' + partyId;
                    objError.Process_Name__c = 'Generic Email';
                    insert objError;
                    pageMessage = new ApexPages.Message(
                        ApexPages.SEVERITY.ERROR, 'There was a problem while sending an email to ' + emailId
                    );
                }

            }

             else {
                pageMessage = new ApexPages.Message(
                    ApexPages.SEVERITY.ERROR, 'There was an error while generating Bulk SOA for Party Id ' + partyId
                );
            }
        }

        ApexPages.addMessage(pageMessage);

        FmIpmsRestCoffeeServices.updateCustomSetting = true;
        FmIpmsRestCoffeeServices.updateBearerToken();//calling updateMethod of Custom Setting after all callouts execution
        return null;

    }

}