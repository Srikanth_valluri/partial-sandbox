global class Damac_InquiryAssignmentCloneController {
    public static string getAllfields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ())
            fields += f+ ', ';
        return fields.removeEnd(', '); 
        
    }
    webservice static Id cloneRecord (id ruleId, String executeOn) {
        
        Inquiry_Assignment_Rules__c latestRuleRank = new Inquiry_Assignment_Rules__c ();
        latestRuleRank = [SELECT Priority__c from Inquiry_Assignment_Rules__c WHERE Execute_On__c =: executeOn AND Parent_Reassign_Rule__c = NULL Order by Priority__c Desc LIMIT 1];
        Inquiry_Assignment_Rules__c rule = new Inquiry_Assignment_Rules__c ();
        String query = 'SELECT '+getAllFields ('Inquiry_Assignment_Rules__c')+' FROM Inquiry_Assignment_Rules__c WHERE ID =: ruleId';
        rule = Database.query (query);
        rule.Rule_Running_Status__c = NULL;
        rule.Rule_Executed_Date__c = NULL;
        if (latestRuleRank.Priority__c != null)
            rule.Priority__c = latestRuleRank.Priority__c + 1;
        rule.ID = NULL;
        insert rule;
        
        
        String childQuery = 'SELECT '+getAllFields ('Inquiry_Assignment_Rules__c')+' FROM Inquiry_Assignment_Rules__c WHERE Parent_Reassign_Rule__c =: ruleId';
        List <Inquiry_Assignment_Rules__c> childRules = new List <Inquiry_Assignment_Rules__c> ();
        for (Inquiry_Assignment_Rules__c childRule :database.query (childQuery)) {
            childRule.ID = NULL;
            childRule.Parent_Reassign_Rule__c = rule.ID;
            childRules.add (childRule);
        }
        insert childRules;
        return rule.ID;
    }
}