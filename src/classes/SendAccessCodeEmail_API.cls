@RestResource(urlMapping='/sendAccessCodeEmail/*')
global class SendAccessCodeEmail_API {

    public static String responseMessage;
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        //200 => 'OK',
        //400 => 'Bad Request',
        //401 => 'Unauthorized',
        //500 => 'Internal Server Error' 
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    @HttpPost 
    global static FinalReturnWrapper ProcessCase() {

        System.debug('inside apiMethod');

        FinalReturnWrapper retunResponse = new FinalReturnWrapper();
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();

        List<Booking_Unit__c> lstBUs = new List<Booking_Unit__c>();
        //FM_Case__c objFMCase = new FM_Case__c();

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        if(!r.params.containsKey('bookingUnitId')) {

            objMeta.message = 'Missing parameter : bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            retunResponse.meta_data = objMeta;
            return retunResponse;

        }
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) {
            objMeta.message = 'Missing parameter value: bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            //objMeta.is_success = false;

            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        

        if(String.isNotBlank(r.params.get('bookingUnitId'))) {

            String bookingUnitId = r.params.get('bookingUnitId');
            System.debug('bookingUnitId :' + bookingUnitId);

            try {
                //lstBUs = [SELECT  Id
                //                , Unit_Name__c
                //                , Registration_Status__c
                //                , Resident_Type__c
                //                , (SELECT id, Status__c, Booking_Unit__c, Tenant__c, Tenant_Email__c
                //                   FROM FM_Cases__r 
                //                   WHERE Origin__c = 'Portal - Guest'       //Will need to update as per APP
                //                   AND Request_Type__c = 'Tenant Registration'
                //                   AND Status__c IN ('Draft Request' , 'In Progress' , 'New' , 'Awaiting Correction')
                //                   ORDER BY CreatedDate DESC
                //                   LIMIT 1 )
                //         FROM Booking_Unit__c
                //         WHERE id =: bookingUnitId];

                lstBUs = GetBookingUnitFromId(bookingUnitId);

                System.debug('lstBUs: ' + lstBUs);
            }
            catch(System.QueryException e){
                System.debug('exception :' + e.getMessage());
                //objMeta.is_success = false;
                objMeta.message = e.getMessage();
                objMeta.status_code = 6;

            }

            System.debug('lstBUs: ' + lstBUs);

            if( lstBUs != null && lstBUs.size() > 0) {

                if(lstBUs[0].FM_Cases__r.size() > 0) {
                    System.debug('lstBUs[0].FM_Cases__r:: ' + lstBUs[0].FM_Cases__r);
                    String isSent = SendAccessCodeEmail(lstBUs[0].FM_Cases__r);

                    objMeta.message = isSent == 'Accepted' ? 'Successful' : isSent;
                    objMeta.status_code = isSent == 'Accepted' ? 1 : 2;
                }
                else {
                    objMeta.message = 'No Draft FM Case found for given BookingUnitId';
                    objMeta.status_code = 3;
                }
            }
            else {
                objMeta.message = 'No Booking unit found for given BookingUnitId';
                objMeta.status_code = 3;
            }
           
            //objMeta.is_success = objMeta.status_code == 1 ? true : false;
            //objMeta.message = responseMessage;
            //objMeta.status_code = 1;
            objMeta.title = mapStatusCode.get(objMeta.status_code);
            objMeta.developer_message = null;

            //objData.fm_case = objFMCase;

            //retunResponse.data = objData;
            retunResponse.meta_data = objMeta;

            System.debug('retunResponse:: ' + retunResponse);
            
        }

        return retunResponse;

    }

    public static List<Booking_Unit__c> GetBookingUnitFromId(String buId) {

        List<Booking_Unit__c> lstBU = [SELECT  Id
                                             , Unit_Name__c
                                             , Registration_Status__c
                                             , Resident_Type__c
                                             , (SELECT id, Status__c, Booking_Unit__c, Tenant__c, Tenant_Email__c
                                                FROM FM_Cases__r 
                                                WHERE Origin__c = 'Portal - Guest'       //Will need to update as per APP
                                                AND Request_Type__c = 'Tenant Registration'
                                                AND Status__c IN ('Draft Request' , 'In Progress' , 'New' , 'Awaiting Correction')
                                                //ORDER BY CreatedDate DESC
                                                ORDER BY LastModifiedDate DESC
                                                LIMIT 1 )
                                       FROM Booking_Unit__c
                                       WHERE id =: buId];
        System.debug(':: ' + lstBU);                               
        return !lstBU.isEmpty() ? lstBU : null;
    }

    public static String SendAccessCodeEmail(list<FM_Case__c> lstFmCase) {

        String sendGridRes;

        String emailBody =  'Dear Tenant,'; 
        emailBody+= '<br/><br/>Your request for Tenant Registration has been saved as DRAFT.'  ; 
        emailBody+= '<br/>Please note that the request is NOT submitted yet. You can access the draft request, add missing information/ documents and submit the request later using the Access Code: '+lstFmCase[0].id+'\n\n';
        emailBody+= '<br/>For further support on registration you can contact our helpline number: 04 7049090. \n'  ;  
        emailBody+= '<br/><br/>Yours Truly, \n '  ; 
        emailBody+= '<br/>LOAMS (Luxury Owners Association Management Services)\n\n'; 
        //emailBody+= '<br/>This is test disclaimer....  ';

        String subject = 'Access Code for your Draft Request for Tenant Registration';

        List<OrgWideEmailAddress> orgWideAddressHelloDamac = [
            SELECT  Id
                    , Address
                    , DisplayName
            FROM    OrgWideEmailAddress
            WHERE   DisplayName = :Label.DAMAC_no_replysf_damacgroup_com
        ];
        System.debug('@@@Email Address: ' + orgWideAddressHelloDamac[0].Address);


         if(String.isNotBlank(lstFmCase[0].Tenant_Email__c)) {

            SendGridEmailService.SendGridResponse sendGridResponse = SendGridEmailService.sendEmailService(
                lstFmCase[0].Tenant_Email__c, '', '', '','', '', subject, '', orgWideAddressHelloDamac[0].Address, 'DAMAC', '', '',
                'text/html', emailBody, '', new List<Attachment>()
            );

            sendGridRes = Test.isRunningTest() ? 'Accepted' : sendGridResponse.ResponseStatus;

            if(sendGridRes == 'Accepted') {
                EmailMessage mail = new EmailMessage();
                mail.Subject = subject;
                mail.MessageDate = System.Today();
                mail.Status = '3';
                mail.RelatedToId = lstFmCase[0].Id;//Put FM case id
                mail.ToAddress = lstFmCase[0].Tenant_Email__c;
                mail.FromAddress = orgWideAddressHelloDamac[0].Address;
                mail.TextBody = emailBody;
                mail.CcAddress = '';
                mail.BccAddress = '';
                mail.Sent_By_Sendgrid__c = true;
                mail.SentGrid_MessageId__c = sendGridResponse.messageId;
                mail.Booking_Unit__c = lstFmCase[0].Booking_Unit__c;
                mail.Account__c = lstFmCase[0].Tenant__c;
                insert mail;
            }
            else {
                responseMessage = 'Tenant Registration Resend Access Code email failed';

                Error_Log__c objError = new Error_Log__c();
                objError.Account__c = lstFmCase[0].Tenant__c;
                objError.FM_Case__c = lstFmCase[0].Id;
                objError.Error_Details__c = 'Tenant Registration Resend Access Code email failed';
                objError.Process_Name__c = 'Generic Email';
                insert objError;
            }

         }
         else {
            sendGridRes = 'Tenant Email not found on FM Case';
         }

         System.debug('sendGridRes:: ' + sendGridRes);

         return sendGridRes;
    }


    /*For only getting Email Address - to be shown on Pop-up in mobile App*/
    @httpGet
    global static FinalReturnWrapper SendTenantEmailAddress() {

        System.debug('inside SendTenantEmailAddress method');

        List<Booking_Unit__c> lstBUs2 = new List<Booking_Unit__c>();
        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_meta_data objMeta = new cls_meta_data();

         if(!r.params.containsKey('bookingUnitId')) {

            objMeta.message = 'Missing parameter : bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;

        }
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) {
            objMeta.message = 'Missing parameter value: bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        

        if(String.isNotBlank(r.params.get('bookingUnitId'))) {

            String bookingUnitId = r.params.get('bookingUnitId');
            System.debug('bookingUnitId :' + bookingUnitId);

            lstBUs2 = GetBookingUnitFromId(bookingUnitId);

            System.debug('lstBUs2: ' + lstBUs2);

            if(lstBUs2!= null && !lstBUs2.isEmpty()) {

                if(lstBUs2[0].FM_Cases__r.size() > 0) {
                    System.debug('lstBUs2[0].FM_Cases__r:: ' + lstBUs2[0].FM_Cases__r);

                    String EmailAddress = lstBUs2[0].FM_Cases__r[0].Tenant_Email__c;
                    System.debug('EmailAddress: ' + EmailAddress);

                    if(String.isNotBlank(EmailAddress)) {
                        System.debug('maked emailId:' + EmailAddress.replaceAll('(^[^@]{2}|(?!^)\\G)[^@]', '$1*'));
                        objMeta.message = 'We will be sending you the Access Code on '+EmailAddress.replaceAll('(^[^@]{2}|(?!^)\\G)[^@]', '$1*');
                        objMeta.status_code = 1;
                    }
                    else {
                        objMeta.message = 'No email addres present on fm case';
                        objMeta.status_code = 2;
                    }
                    
                }
                else {
                    objMeta.message = 'No Draft FM Case found for given BookingUnitId';
                    objMeta.status_code = 3;
                }
            }
            else {
                objMeta.message = 'No Booking unit found for given buildingId';
                objMeta.status_code = 3;
            }
        }

        objMeta.title = objMeta.status_code == 1 ? 'Resend Access Code' : mapStatusCode.get(objMeta.status_code);
        objMeta.developer_message = null;
        returnResponse.meta_data = objMeta;

        System.debug('returnResponse:: ' + returnResponse);
        return returnResponse;
    }



    
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        //public FMCaseWrapper fm_case ;
    }

    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }

}