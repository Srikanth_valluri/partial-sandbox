@isTest
Public Class DAMAC_WAYBEO_REST_SERVICE_TEST {
     public static task data() {
         Task t = new Task ();
            t.Subject='Donni';
            t.Status='New';
            t.SQL_id__c = '1234';
            t.Priority='Normal';
            t.CallType='Outbound';
            t.Call_Start_Time__c = system.now();
            t.Call_End_Time__c = system.now();
        insert t;
        return t;
     }
     public  static testmethod void init(){
         RestRequest req = new RestRequest();
         Inquiry_Country_Code_Categories__c code = new Inquiry_Country_Code_Categories__c ();
         code.Short_Code__c = '0009';
         code.name=  'India';
         insert code; 
         
         Task t = new Task();
         t = data();
        String jsonBody = '{"answered_time":"'+t.Call_Start_Time__c +'","completed_time":"'+t.Call_End_Time__c+'","callId":"1234","customer_clid":"false","extension":"false"}';
        
            RestResponse res = new RestResponse();
            req.requestURI = '/api/mobile';  
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueof(jsonBody);
            Blob body1 = req.requestBody;
            req.params.put('op', '9876543210');
            req.params.put('taskid', t.id);
            req.params.put('mobile', '9876541230');
            String bodyString = body1.toString(); 
            RestContext.request = req;
            RestContext.response = res;            
     }
     static testMethod void method1 () {  
         Inquiry_Country_Code_Categories__c code = new Inquiry_Country_Code_Categories__c ();
         code.Short_Code__c = '0009';
         code.name=  'India';
         insert code;
           Task t = new Task();
           t = data();
           RestRequest req = new RestRequest();
           String jsonBody = '{"answered_time":"'+t.Call_Start_Time__c +'","completed_time":"'+t.Call_End_Time__c+'","callId":"1234","customer_clid":"false","extension":"false"}';
        
            RestResponse res = new RestResponse();
            req.requestURI = '/api/read';  
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueof(jsonBody);
            Blob body1 = req.requestBody;
            req.params.put('op', '9876543210');
            req.params.put('callId', '1234');
            req.params.put('mobile', '9876541230');
            String bodyString = body1.toString(); 
            RestContext.request = req;
            RestContext.response = res;  
        test.setMock(HttpCalloutMock.class, new MockHttpCalloutClass());  
        DAMAC_WAYBEO_REST_SERVICE obj = New DAMAC_WAYBEO_REST_SERVICE ();
    }
    static testMethod void method2 () {
    Inquiry_Country_Code_Categories__c code = new Inquiry_Country_Code_Categories__c ();
         code.Short_Code__c = '0009';
         code.name=  'India';
         insert code;
      Task t = new Task();
      t = data();
      RestRequest req = new RestRequest();
        
            RestResponse res = new RestResponse();
            req.requestURI = '/api/extensions/PC';  
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueof('{}');
            Blob body1 = req.requestBody;
            req.params.put('op', '9876543210');
            req.params.put('taskid', t.id);
            req.params.put('mobile', '9876541230');
            String bodyString = body1.toString(); 
            RestContext.request = req;
            RestContext.response = res;  
            
           
        List<User> usersList = New List<User> ();
           User u1 = [SELECT ID, Extension FROM User where ID =: USERInfo.getUserID ()];
        if (u1.Extension == NULL) {
            u1.Extension = '0715';
            update u1;
        }
        usersList.add(u1);
        test.setMock(HttpCalloutMock.class, new MockHttpCalloutClass());        
        try {
        DAMAC_WAYBEO_REST_SERVICE obj = New DAMAC_WAYBEO_REST_SERVICE ();
        DAMAC_WAYBEO_REST_SERVICE.doPost();
        }
        catch (Exception e) {}
    }
    static testMethod void method3() {
    Inquiry_Country_Code_Categories__c code = new Inquiry_Country_Code_Categories__c ();
         code.Short_Code__c = '0009';
         code.name=  'India';
         insert code;
    RestRequest req = new RestRequest();
         Task t = new Task();
         t = data();
        String jsonBody = '{"answered_time":"'+t.Call_Start_Time__c +'","completed_time":"'+t.Call_End_Time__c+'","callId":"1234","customer_clid":"false","extension":"false"}';
        
            RestResponse res = new RestResponse();
            req.requestURI = '/api/update';  
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueof(jsonBody);
            Blob body1 = req.requestBody;
            req.params.put('op', '9876543210');
            req.params.put('taskid', t.id);
            req.params.put('mobile', '9876541230');
            String bodyString = body1.toString(); 
            RestContext.request = req;
            RestContext.response = res; 
     
        test.setMock(HttpCalloutMock.class, new MockHttpCalloutClass());
         DAMAC_WAYBEO_REST_SERVICE obj = New DAMAC_WAYBEO_REST_SERVICE ();
        DAMAC_WAYBEO_REST_SERVICE.doPost();
    }
     static testMethod void method4() {
     Inquiry_Country_Code_Categories__c code = new Inquiry_Country_Code_Categories__c ();
         code.Short_Code__c = '0009';
         code.name=  'India';
         insert code;
     RestRequest req = new RestRequest();
         Task t = new Task();
         t = data();
        String jsonBody = '{"answered_time":"'+t.Call_Start_Time__c +'","completed_time":"'+t.Call_End_Time__c+'","callId":"1234","customer_clid":"false","extension":"false","recording_url":"test"}';
        
            RestResponse res = new RestResponse();
            req.requestURI = '/api/create';  
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueof(jsonBody);
            Blob body1 = req.requestBody;
            req.params.put('op', '9876543210');
            req.params.put('taskid', t.id);
            req.params.put('mobile', '9876541230');
            String bodyString = body1.toString(); 
            RestContext.request = req;
            RestContext.response = res; 
            
         
        test.setMock(HttpCalloutMock.class, new MockHttpCalloutClass());
         DAMAC_WAYBEO_REST_SERVICE obj = New DAMAC_WAYBEO_REST_SERVICE ();
       // DAMAC_WAYBEO_REST_SERVICE.doGet ();        
        DAMAC_WAYBEO_REST_SERVICE.doPost();
    }
    static testMethod void method5() {
    Inquiry_Country_Code_Categories__c code = new Inquiry_Country_Code_Categories__c ();
         code.Short_Code__c = '0009';
         code.name=  'India';
         insert code;
      RestRequest req = new RestRequest();
         Task t = new Task();
         t = data();
        String jsonBody = '{"answered_time":"'+t.Call_Start_Time__c +'","completed_time":"'+t.Call_End_Time__c+'","callId":"1234","customer_clid":"false","extension":"false"}';
        
            RestResponse res = new RestResponse();
            req.requestURI = '/api/mobile';  
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueof(jsonBody);
            Blob body1 = req.requestBody;
            req.params.put('op', '9876543210');
            req.params.put('taskid', t.id);
            req.params.put('mobile', '9876541230');
            String bodyString = body1.toString(); 
            RestContext.request = req;
            RestContext.response = res; 
        test.setMock(HttpCalloutMock.class, new MockHttpCalloutClass());
         DAMAC_WAYBEO_REST_SERVICE obj = New DAMAC_WAYBEO_REST_SERVICE ();
        DAMAC_WAYBEO_REST_SERVICE.doPost();
        DAMAC_WAYBEO_REST_SERVICE.createLog(NULL, 'test');
    }
     static testMethod void method6 () {
    Inquiry_Country_Code_Categories__c code = new Inquiry_Country_Code_Categories__c ();
         code.Short_Code__c = '0009';
         code.name=  'India';
         insert code;
      
      Task t = new Task();
      t = data();
      RestRequest req = new RestRequest();
        
            RestResponse res = new RestResponse();
            req.requestURI = '/api/extensions/TSA';  
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueof('{}');
            Blob body1 = req.requestBody;
            req.params.put('op', '9876543210');
            req.params.put('taskid', t.id);
            req.params.put('mobile', '9876541230');
            req.params.put('callingNumber', '9876543210');
            req.params.put('callingNumber', '9876543210');
            String bodyString = body1.toString(); 
            RestContext.request = req;
            RestContext.response = res;  
            
           
        List<User> usersList = New List<User> ();
           User u1 = [SELECT ID, Extension FROM User where ID =: USERInfo.getUserID ()];
        if (u1.Extension == NULL) {
            u1.Extension = '0715';
            update u1;
        }
        usersList.add(u1);
        test.setMock(HttpCalloutMock.class, new MockHttpCalloutClass());        
        try {
        DAMAC_WAYBEO_REST_SERVICE obj = New DAMAC_WAYBEO_REST_SERVICE ();
        DAMAC_WAYBEO_REST_SERVICE.doPost();
        }
        catch (Exception e) {}
    }
     static testMethod void method7() {
    Inquiry_Country_Code_Categories__c code = new Inquiry_Country_Code_Categories__c ();
         code.Short_Code__c = '0009';
         code.name=  'India';
         insert code;
      
      Task t = new Task();
      t = data();
      RestRequest req = new RestRequest();
        
            RestResponse res = new RestResponse();
            req.requestURI = '/api/extensions/TSA';  
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueof('{}');
            Blob body1 = req.requestBody;
            req.params.put('op', '9876543210');
            req.params.put('taskid', t.id);
            req.params.put('mobile', '9876541230');
            String bodyString = body1.toString(); 
            RestContext.request = req;
            RestContext.response = res;  
            
           
        List<User> usersList = New List<User> ();
           User u1 = [SELECT ID, Extension FROM User where ID =: USERInfo.getUserID ()];
        if (u1.Extension == NULL) {
            u1.Extension = '0715';
            update u1;
        }
        usersList.add(u1);
        test.setMock(HttpCalloutMock.class, new MockHttpCalloutClass());        
        try {
        DAMAC_WAYBEO_REST_SERVICE obj = New DAMAC_WAYBEO_REST_SERVICE ();
        DAMAC_WAYBEO_REST_SERVICE.doPost();
        }
        catch (Exception e) {}
    }
}