public without sharing class EscalateCLExtention {

    ID recordId;
    public EscalateCLExtention(ApexPages.StandardController controller) {
        recordId = controller.getId();
        System.debug('strAccountId'+recordId);
    }

    public PageReference escalate(){
        
        
        System.debug('strCLId'+recordId);
       
        
        if(recordId != null) {
            Calling_List__c objCL = [ Select Id,CRE_Manager__c,OwnerId,
                                     Escalated_Mgr__c,Escalated_Director__c,Escalated_SVP__c,Current_Escl_with__c
                                    ,RTP_Escl_Date__c,Director_Escl_Date__c,SVP_Escl_Date__c
                            FROM Calling_List__c WHERE ID = : recordId ];
            
            String CurrentUserId = UserInfo.getUserId();   
            
            User currentUser = [ SELECT Id
                                    ,FirstName
                                    ,LastName
                                    ,ManagerId
                                    ,Manager.FirstName
                                    ,Manager.LastName
                                FROM User
                                WHERE Id = :UserInfo.getUserId() Limit 1];
                                
            System.debug('objUser='+currentUser);
            System.debug('objUser.ManagerId='+currentUser.ManagerId);
            System.debug('objUser.Manager.Name='+currentUser.Manager.FirstName+' '+currentUser.Manager.LastName);
                                
            User OwnerUser = [ SELECT Id
                                    ,FirstName
                                    ,LastName
                                    ,ManagerId
                                    ,Manager.ManagerId
                                FROM User
                                WHERE Id = : objCL.OwnerId Limit 1];
                                
            System.debug('OwnerUser='+OwnerUser);
            System.debug('objUser.ManagerId='+OwnerUser.ManagerId);
            System.debug('objUser.Manager.ManagerId='+OwnerUser.Manager.ManagerId);
                                
            if( objCL.OwnerId != NULL && currentUser.Id == OwnerUser.Id){
                System.debug('in First if');
                objCL.Current_Escl_with__c = currentUser.ManagerId;
                objCL.RTP_Escl_Date__c = System.Now();
                objCL.Escalated_Mgr__c = currentUser.Manager.FirstName+''+currentUser.Manager.LastName;
                objCL.Current_Escl_User_Role__c = 'Manager';
            }
            if( OwnerUser.ManagerId != NULL && currentUser.Id == OwnerUser.ManagerId){
                System.debug('in second if');
                objCL.Current_Escl_with__c = currentUser.ManagerId;
                objCL.Director_Escl_Date__c = System.Now();
                objCL.Escalated_Director__c = currentUser.Manager.FirstName+''+currentUser.Manager.LastName;
                objCL.Current_Escl_User_Role__c = 'Director';
            }
            if( OwnerUser.Manager.ManagerId != NULL && currentUser.Id == OwnerUser.Manager.ManagerId){
                System.debug('in third if');
                objCL.Current_Escl_with__c = currentUser.ManagerId;
                objCL.SVP_Escl_Date__c = System.Now();
                objCL.Escalated_SVP__c = currentUser.Manager.FirstName+''+currentUser.Manager.LastName;
            } 
            update objCL;
        }
        
        PageReference demoPage = new pageReference('/'+recordId);
        demoPage.setRedirect(true);
        return demoPage;
        
    }
    
    
}