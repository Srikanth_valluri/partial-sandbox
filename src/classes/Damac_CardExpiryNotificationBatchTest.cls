/*
Created Date: 17/06/2020
Batch Class: Damac_CardExpiryNotification_Batch_Acc, Damac_CardExpiryNotification_Batch_Con
Schedule Class: Damac_CardExpiryNotificationSch_Con, Damac_CardExpiryNotificationSch_Acc
*/
@isTest
private class Damac_CardExpiryNotificationBatchTest {
    @isTest
    static void cardExpiryCorporate() {
        
        Account acc = createAccount();
        Contact con = createContact(acc.Id, 30, 30, 30);
        Contact con1 = createContact(acc.Id, 30, 10, 23);
        
        String cronExp = '0 0 0 15 3 ? *';
        
        Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest',  cronExp, new Damac_CardExpiryNotificationSch_Acc());
        List<CronTrigger> ct = [SELECT Id, CronExpression, TimesTriggered FROM CronTrigger WHERE Id = :jobId];
        System.assertEquals(1, ct.size());
        Test.stopTest();
        
    }
    
    @isTest
    static void cardExpiryIndividual() {
        
       Account acc = createAccount();
        Contact con = createContact(acc.Id, 20, 35, 35);     
        System.debug ([SELECT Expired_Cards__c FROM Contact WHERE Id =: con.Id]);   
        String cronExp = '0 0 0 16 5 ? *';
        
        Test.startTest();
        
        String jobId = System.schedule('ScheduleApexClassTest1',  cronExp, new Damac_CardExpiryNotificationSch_Con());
        Damac_CardExpiryNotificationBatch_Con obj = new Damac_CardExpiryNotificationBatch_Con();
        obj.sendMailToUsers(acc.Id, acc.Agency_Email__c);
        List<CronTrigger> ct = [SELECT Id, CronExpression, TimesTriggered FROM CronTrigger WHERE Id = :jobId];
        System.assertEquals(1, ct.size());
       
        
        Test.stopTest();
        
        
    }
    
    private static Account createAccount() {
        Id accRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Agency_Type__c = 'Corporate';
        acc.Agency_Email__c = 'testmail@tmail.com';
        acc.RecordTypeId = accRecTypeId;
        acc.Trade_License_Expiry_Date__c = Date.today().addDays(20);
        acc.RERA_Expiry_Date__c = Date.today().addDays(20);
        insert acc;
        return acc;
    }
        
    private static Contact createContact(Id accId, Integer idExpiry, Integer passportExpiry, Integer brokerExpiry) {
        Id conRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Agency Contact').getRecordTypeId();
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Contact';
        con.AccountId = accId;
        con.RecordTypeId = conRecTypeId;
        con.ID_Type__c = 'Passport';
        con.ID_Expiry_Date__c = Date.today().addDays(idExpiry);
        con.Passport_Expiry_Date__c = Date.today().addDays(passportExpiry);
        con.Broker_Card_Expiry_Date__c = Date.today().addDays(brokerExpiry);
        insert con;
        return con;
    }   
}