/**
 *  Author      : Shashank Maind
 *  Date        : 2017-12-26
 *  Description : This class is to update the Customer payment details obtained from payment gateway
 */
global class CustomerPaymentStatusUpdate extends CLS_PaymentStatusUpdate {

    public Boolean paymentIsSuccessful {get; set;}
    public String EncRespStr;
    public String refNo, orderStatus, cardType, errorCode, errorMsg;
    public Map<String, String> paramMap = new Map<String, String>();
    Receipt__c receipt;


    public CustomerPaymentStatusUpdate() {
        super();
        paramMap = ApexPages.currentPage().getParameters();
        EncRespStr = paramMap.get('encResp');
        initializeVariables();
    }

    public CustomerPaymentStatusUpdate (ApexPages.StandardController controller) {
        EncRespStr = ApexPages.currentPage().getParameters().get('encResp');
        initializeVariables();
        receipt = (Receipt__c) controller.getRecord();
    }

    public void initializeVariables() {
        paymentIsSuccessful = false;
    }

    public void checkReceiptPaymentStatus() {
        if (receipt != NULL && 'FAILURE'.equalsIgnoreCase(receipt.Payment_Status__c)) {
            delete receipt;
        } else {
            paymentIsSuccessful = true;
        }
    }

    global PageReference updateReceiptMethod(){

        String decryptedStr = decryptStr(EncResp);

        decryptedStr = decryptedStr.replace('|','--SEPARATOR--');
        System.debug('decryptedStr = ' + decryptedStr);

        /****Get response parameters from the decrypted message***/
        List<String> respParams = decryptedStr.split('--SEPARATOR--');
        system.debug('Response Params = '+ respParams);
        if ('query'.equalsIgnoreCase(mode)) {
            RNo = respParams[1];
        } else {
            RNo = respParams[0];
        }
        system.debug('RNo = '+RNo);

        //Get the transation receipt record for the payment done on the portal sidebar
        Receipt__c receipt =[  SELECT  Id,
                                    Name,
                                    Amount__c,
                                    Card_Type__c,
                                    Payment_Status__c,
                                    Payment_Gateway_Ref_No__c,
                                    PG_Error_Code__c,
                                    PG_Error_Message__c
                            FROM    Receipt__c
                            WHERE   Name = :RNo];

        if (mode=='query') {
            //Update the values from the response parameter onto the rceipt record
            receipt.Payment_Gateway_Ref_No__c=respParams[2];
            receipt.Payment_Status__c=respParams[5];
            receipt.Card_Type__c=respParams[7];
            receipt.PG_Error_Code__c=respParams[8];
            receipt.PG_Error_Message__c=respParams[9];

            update receipt;

            /*
            receipt.Processing_Charges__c=Integer.valueof(processcharge);
            receipt.Amount__c=Integer.valueof(payamt);
            */
            return null;
        } else{
            //Update the values from the response parameter onto the rceipt record
            receipt.Payment_Gateway_Ref_No__c=respParams[1];
            receipt.Payment_Status__c=respParams[4];
            receipt.Bank_Ref_No__c=respParams[5];
            receipt.Card_Type__c=respParams[7];
            receipt.PG_Error_Code__c=respParams[8];
            receipt.PG_Error_Message__c=respParams[9];

            update receipt;

            List<id> lstRecieptId= new List<id>();
            if(receipt.Payment_Status__c=='SUCCESS'){
                lstRecieptId.add(receipt.id);
                //system.enqueueJob(new AsyncReceiptWebservice (lstRecieptId,'Receipt Creation'));
                createReceipt(lstRecieptId);
            }
            /*
            receipt.Processing_Charges__c=Integer.valueof(processcharge);
            receipt.Amount__c=Integer.valueof(payamt);
            */

            //Return to the Portal Home Page
            //PageReference pageRef = new PageReference(Label.Community_URL+'/'+receipt.id);
            PageReference pageRef = new PageReference('/CustomerPaymentStatus?id='+receipt.id);
            return pageRef;
        }

    }

    global override PageReference updateReceipt() {
        Map<String, String> responseParams = CCAvenuePaymentGateway.getInstance('Customer Portal')
                .getDecryptedResponse(paramMap);
        System.debug('responseParams = ' + responseParams);
        if (responseParams != NULL && !responseParams.isEmpty()) {
            refNo = responseParams.get('bank_ref_no');
            orderStatus = responseParams.get('order_status');
            cardType = responseParams.get('payment_mode');
            errorCode = responseParams.get('status_code');
            errorMsg = responseParams.get('status_message');
        }
        system.debug('refNo = '+ refNo);
        system.debug('orderStatus = '+ orderStatus);
        system.debug('cardType = '+ cardType);
        system.debug('errorCode = '+ errorCode);
        system.debug('errorMsg = '+ errorMsg);
        if (paramMap != NULL && !paramMap.isEmpty()) {
            RNo = paramMap.get('orderNo');
        }
        system.debug('RNo = '+RNo);
        String Uno = Rno.split('/')[0];
        Receipt__c receipt =[  SELECT  Id,
                                    Name,
                                    Amount__c,
                                    Card_Type__c,
                                    Payment_Status__c,
                                    Payment_Gateway_Ref_No__c,
                                    PG_Error_Code__c,
                                    PG_Error_Message__c
                            FROM    Receipt__c
                            WHERE   Name LIKE :('%' + Uno)];

        if (responseParams != NULL && !responseParams.isEmpty()) {
            //Update the values from the response parameter onto the rceipt record
            receipt.Payment_Gateway_Ref_No__c=refNo;
            receipt.Payment_Status__c=orderStatus;
            receipt.Card_Type__c=cardType;
            receipt.PG_Error_Code__c=errorCode;
            receipt.PG_Error_Message__c=errorMsg;

            update receipt;
            PageReference pageRef = new PageReference('/CustomerPaymentStatus?id='+receipt.id);
            return pageRef;
        } else{
            receipt.Payment_Gateway_Ref_No__c=refNo;
            receipt.Payment_Status__c=orderStatus;
            receipt.Bank_Ref_No__c=refNo;
            receipt.Card_Type__c=cardType;
            receipt.PG_Error_Code__c=errorCode;
            receipt.PG_Error_Message__c=errorMsg;

            update receipt;

            List<id> lstRecieptId= new List<id>();
            if(receipt.Payment_Status__c=='SUCCESS'){
                lstRecieptId.add(receipt.id);
                createReceipt(lstRecieptId);
            }
            PageReference pageRef = new PageReference('/CustomerPaymentStatus?id='+receipt.id);
            return pageRef;
        }

    }

    @future(callout=true)
    private static void createReceipt(List<Id> lstRecieptId) {
        if (lstRecieptId == NULL) {
            return;
        }

        IpmsSettings settings = getIPMSsetting('IPMS_webservice');
        HTTPRequest req = new HTTPRequest();
        req.setMethod('POST');

        String reqXML = prepareReceipt(lstRecieptId, settings);
        System.debug('>>>>>Request Body>>>>>>'+ reqXML);
        reqXML = reqXML.replaceAll('null', '');
        reqXML = reqXML.trim();
        req.setbody(reqXML);
        req.setEndpoint(settings.endurl);
        req.setHeader('Content-Type','text/xml');
        req.setTimeout(120 * 1000);
        HTTP http = new HTTP();
        //if(!Test.isrunningTest()){
            try{
                HTTPResponse res = http.send(req);
                System.debug('Response = ' + res.getbody());

                parseReceiptResponse(res.getBody());
            }
            catch(Exception ex) {
                System.debug('Callout error: '+ ex);
                Log__c objLog = new Log__c();
                objLog.Description__c ='Ids=='+lstRecieptId+'-Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage();
                objLog.Type__c = 'Webservice Callout For Receipt Creation';
                insert objLog;
            }
        //}

    }

    /*global override String DecryptStr(String EncStr){
        //Convert the encrypted string to Blob
        list<PaymentGateway__c> lstCustmSettng = new list<PaymentGateway__c>();
        lstCustmSettng = [SELECT AccessCode__c, EncryptionKey__c, MerchantId__c,Url__c
                          FROM PaymentGateway__c
                          WHERE Name = 'Customer Portal'];
        system.debug('>>>>lstCustmSettng>>>>'+lstCustmSettng);
        Blob encryptedData= EncodingUtil.convertFromHex(EncStr);

        //Get the encryption key from custom setting
        String keyval= lstCustmSettng[0].EncryptionKey__c;
        Blob cryptoKey = Blob.valueOf(keyval);
        Blob hash = Crypto.generateDigest('MD5', cryptoKey );
        //Get the initialization vector from custom setting
        Blob iv = Blob.valueof('0123456789abcdef');

        //Decrypt the data using Salesforce Crypto class
        //Blob decryptedData = Crypto.decrypt('AES256', cryptoKey,iv,encryptedData);
        Blob decryptedData = Crypto.decryptWithManagedIV('AES128', hash , encryptedData);
        system.debug('DDDDD==>'+decryptedData);

        // Convert the decrypted data to string
        String decryptedDataString = decryptedData.toString();
        system.debug('DSSSSS==>'+decryptedDataString);

        return decryptedDataString;
    }*/

    public static String prepareReceipt(List<id> lstRecieptId, IpmsSettings settings) {
        String recQuery = 'SELECT ';
        for (Schema.SObjectField fieldToken : Schema.SObjectType.Receipt__c.fields.getMap().values()) {
            Schema.DescribeFieldResult field = fieldToken.getDescribe();
            if (field.isAccessible()) {
                recQuery += field.getName() + ',';
            }
        }
        recQuery = recQuery.removeEnd(',') + ' FROM Receipt__c WHERE Id IN :lstRecieptId';

        List<Receipt__c> rcptlist= Database.query(recQuery);

        String reqno='123456';
        if(rcptlist.size()>0)
            reqno =rcptlist[0].name+'-'+GetFormattedDateTime(system.now());
        system.debug('REQ NO==='+reqno);
        // String currdate= system.now
        String body = '';
        body += preparesoapHeader(settings);
        body +='<soapenv:Body>';
        body +='<proc:InputParameters>';
        //body+='<!--Optional:-->';
        body +='<proc:P_REQUEST_NUMBER>'+reqno+'</proc:P_REQUEST_NUMBER>';
        //body+='<!--Optional:-->';
        body +='<proc:P_SOURCE_SYSTEM>SFDC</proc:P_SOURCE_SYSTEM>';
        //body+='<!--Optional:-->';
        body +='<proc:P_REQUEST_NAME>CREATE RECEIPT</proc:P_REQUEST_NAME>';
        //body+='<!--Optional:-->';
        body +='<proc:P_REQUEST_MESSAGE>';
        //body+='<!--Zero or more repetitions:-->';
        for (Receipt__c rec: rcptlist) {
            body+='<proc:P_REQUEST_MESSAGE_ITEM>';
            //body+='<!--Optional:SFDC ID-->';
            body+='<proc:PARAM_ID>'+rec.Id+'</proc:PARAM_ID>';
            //body+='<!--Optional:RECEIPT_NUMBER-->';
            body+='<proc:ATTRIBUTE1>'+rec.Name+'</proc:ATTRIBUTE1>';
            //body+='<!--Optional:REG_ID-->';
            body+='<proc:ATTRIBUTE2>'+rec.Registration_Id__c+'</proc:ATTRIBUTE2>';
            //body+='<!--Optional:CURRENCY_CODE-->';
            body+='<proc:ATTRIBUTE3>'+rec.BU_Currency__c+'</proc:ATTRIBUTE3>'; //AED
            //body+='<!--Optional:AMOUNT-->';
            body+='<proc:ATTRIBUTE4>'+rec.IPMS_Token_Amount__c+'</proc:ATTRIBUTE4>';
            //body+='<!--Optional:RECEIPT_METHOD-->';
            body+='<proc:ATTRIBUTE5>Direct Receipt in the Bank</proc:ATTRIBUTE5>';
            //body+='<!--Optional:BANK_ACCOUNT_NUM-->';
            String banknum ='';
            if(rec.Unit_Country__c == 'Saudi Arabia')
                banknum=Label.Saudi_Bank_Account_No;
            else
                banknum='Master/Visa/Amex/Diners -CC';
            body+='<proc:ATTRIBUTE6>'+banknum+'</proc:ATTRIBUTE6>';
            body+='<proc:ATTRIBUTE7>TOKEN</proc:ATTRIBUTE7>';
            body+='</proc:P_REQUEST_MESSAGE_ITEM>';
        }
        body+='</proc:P_REQUEST_MESSAGE>';
        body+='</proc:InputParameters>';
        body+='</soapenv:Body>';
        body+='</soapenv:Envelope>';


        return body.trim();
    }

        public static void parseReceiptResponse(string body){

            string ipmsrcptid,status,statusmsg,recId = '';

            DOM.Document xmlDOC = new DOM.Document();
            xmlDOC.load(body);
            DOM.XMLNode rootElement = xmlDOC.getRootElement();

            Map<Id, String> ReceiptStatusMap = new Map<Id, String>();
            Map<Id, String> ReceiptStatusMsgMap = new Map<Id, String>();
            Map<Id, String> ReceiptMap = new Map<Id, String>();
            for(Dom.XMLNode child1: rootElement.getChildElements()){
                for(Dom.XMLNode child2: child1.getChildElements()){
                    for(Dom.XMLNode child3: child2.getChildElements()){
                        for(Dom.XMLNode child4: child3.getChildElements()){
                            System.debug('>>>>Child4>>>>>>>>'+child4.getName());
                            for(Dom.XMLNode child5: child4.getChildElements()){
                                System.debug('>>>>Child5>>>>>>>>'+child5.getName());
                                System.debug('>>>>Child5 Text>>>>>>>>'+child5.getText());
                                if(child5.getName()=='PROC_STATUS')
                                    status = child5.getText();
                                if(child5.getName()=='PROC_MESSAGE')
                                    statusmsg = child5.getText();
                                if(child5.getName()=='PARAM_ID')
                                    recId= child5.getText();
                                if(child5.getName()=='ATTRIBUTE1')
                                    ipmsrcptid = child5.getText();
                            }
                            ReceiptMap.put(recId,ipmsrcptid);
                            ReceiptStatusMap.put(recId,status);
                            ReceiptStatusMsgMap.put(recId,statusmsg);
                        }
                    }
                }
            }
            //List<id> BUids= new List<id>();
            List<Receipt__c> reclist= [select id,Booking_Unit__c,IPMS_Receipt_ID__c,IPMS_Status__c from Receipt__c where id in :ReceiptMap.keyset()];
            try{
                if(reclist.size()>0){
                    for(Receipt__c rec :reclist){
                        String ipmsstatus=ReceiptStatusMsgMap.get(rec.id);
                        if(ipmsstatus.length()>255)
                            rec.IPMS_Status__c=ipmsstatus.left(255);
                        else
                            rec.IPMS_Status__c=ipmsstatus;
                        if(ReceiptStatusMap.get(rec.id)=='S'){
                            rec.IPMS_Receipt_ID__c=ReceiptMap.get(rec.id);
                            //if(rec.IPMS_Receipt_ID__c!=null)
                            //    BUids.add(rec.Booking_Unit__c);
                        }
                    }
                    update reclist;
                    //List<Id> SRIds = new List<id>();
                    //if(BUids.size()>0){
                    //    List<Booking_Unit__c>BUlist = [select id,Token_paid__c,Token_Paid_Time__c,Status__c from Booking_Unit__c where Status__c!='Removed' and id in:BUids];
                    //    for(Booking_Unit__c BU:BUlist){
                    //        BU.Token_Paid__c=true;
                    //        BU.Token_Paid_Time__c= system.now();
                    //        SRIds.add(BU.SR_Id__c);
                    //    }
                    //    update BUlist;
                    //}
                    /*
    //Converting the inquiry into account
    Integer tokenCount=0;
    integer bucount=0;
    for(Booking_Unit__c BU : [select id, Booking__c,Booking__r.Deal_SR__c,Token_Paid__c from Booking_Unit__c where SR_Id__c in:SRIds]){
    //AllBuids.add(BU.id);
    //BookingIds.add(BU.Booking__c);
    //system.debug('BU'+BU.id+'---SPACount==='+BU.SR_Docs__r.size());
    bucount++;
    if(BU.Token_Paid__c)
    tokenCount++;
    }
    Set<Id> inquiryIdsSet= new Set<Id>();
    if(bucount==tokencount){
    for(Buyer__c b:[select id,Inquiry__c from Buyer__c where Inquiry__c!=null AND Booking__r.Deal_SR__c in :SRIds]){
    inquiryIdsSet.add(b.Inquiry__c);
    }
    system.debug('II==== '+inquiryIdsSet);
    if(!inquiryIdsSet.isEmpty()){
    InquiryConversionProcess.convertInquiry(inquiryIdsSet);
    }
    }*/
                }
            }
            catch(Exception ex) {
                System.debug('Callout error: '+ ex);
                Log__c objLog = new Log__c();
                objLog.Description__c ='Ids=='+ReceiptMap.keyset()+'-Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage();
                objLog.Type__c = 'Error updating response For Receipt Creation';
                insert objLog;
            }

        }

    public static String preparesoapHeader(IpmsSettings settings){
        String body = '';
        body+='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xxdc="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/" xmlns:proc="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/process/">';
        body+='<soapenv:Header>';
        body+='<xxdc:SOAHeader>';
        //body+='<!--Optional:-->';
        body+='<xxdc:Responsibility>ONT_ICP_SUPER_USER</xxdc:Responsibility>';
        //body+='<!--Optional:-->';
        body+='<xxdc:RespApplication>ONT</xxdc:RespApplication>';
        //body+='<!--Optional:-->';
        body+='<xxdc:SecurityGroup>standard</xxdc:SecurityGroup>';
        //body+='<!--Optional:-->';
        body+='<xxdc:NLSLanguage>american</xxdc:NLSLanguage>';
        //body+='<!--Optional:-->';
        body+='<xxdc:Org_Id/>';
        body+='</xxdc:SOAHeader>';
        body+='<wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">';
        body+='<wsse:UsernameToken>';
        body+='<wsse:Username>' + settings.usrname + '</wsse:Username>';
        body+='<wsse:Password>' + settings.pwd + '</wsse:Password>';
        body+='</wsse:UsernameToken>';
        body+='</wsse:Security>';
        body+='</soapenv:Header>';
        return body;
    }

    public static string GetFormattedDateTime(DateTime dt){
        String yyyy=string.valueof(dt.year());
        String mm=string.valueof(dt.month());
        String dd=string.valueof(dt.day());
        String hh=string.valueof(dt.hour());
        String mi=string.valueof(dt.minute());
        String ss=string.valueof(dt.second());
        String ms=string.valueof(dt.millisecond());

        String formatdate=yyyy+mm+dd+hh+mi+ss+ms;
        return formatdate;
    }

    public static IpmsSettings getIPMSsetting(String servicename) {
        IPMS_Integration_Settings__mdt ipms= [select id,Endpoint_URL__c,Password__c,  Username__c from IPMS_Integration_Settings__mdt where DeveloperName = :servicename LIMIT 1];
        return new IpmsSettings(ipms.Endpoint_URL__c, ipms.Username__c, PasswordCryptoGraphy.DecryptPassword(ipms.Password__c));

    }

    public class IpmsSettings {
        public String endurl;
        public String usrname;
        public String pwd;

        public IpmsSettings(String endurl, String usrname, String pwd) {
            this.endurl     = endurl;
            this.usrname    = usrname;
            this.pwd        = pwd;
        }
    }

}