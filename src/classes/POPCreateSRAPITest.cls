@isTest
public class POPCreateSRAPITest {
    @TestSetup
    static void TestData() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
        
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;
        
        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
        
        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Villa';
        insert invObj;
        
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        Booking_Unit__c bookingUnit1 = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'N',
                                      Inventory__c = invObj.id,
                                      Dummy_Booking_Unit__c = true);
        bookingUnitList.add(bookingUnit1);
        Booking_Unit__c bookingUnit2 = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1205',
                                      Registration_ID__c = '3902',
                                      Handover_Flag__c = 'Y',
                                      Inventory__c = invObj.id,
                                      Dummy_Booking_Unit__c = true);
        bookingUnitList.add(bookingUnit2);
        
        insert bookingUnitList;        
    }
    
    @isTest
    static void TestRenewalDraftInstallment() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        String jsonString =  
                '   {                              '  + 
                '      "action": "draft",                      '  + 
                '      "id": "",                            '  + 
              '      "account_id": "' + account.Id + '",    '  +
                '      "pop_type": "installment",        '  +
                '      "payment_date": "2020-12-21",      '  +
                '      "payment_mode": "Credit Card",      '  +
                '      "cash_deposit_slip": "",          '  +
                '      "cheque_from_account": "",        '  +
                '      "source_of_funds": "test source",    '  +
                '      "payment_remarks": "test remarks",    '  +
                '      "payment_currency": "AED",        '  +
                '      "total_amount": 10,            '  +
                '      "sender_name": "test sender",      '  +
                '      "sender_bank_name": "test bank",      '  +
                '      "swift_code": "1234",          '  +
                '      "unit_details": [            '  +
                '          {                  '  +
                '         "unit_id": "' + bookingUnit.Id + '"  '  +
                '          }                  '  +
                '      ]                    '  +
                '  }  ' ;
      
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void TestRenewalDraftServiceCharge() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        String jsonString =  
                '   {                              '  + 
                '      "action": "draft",                      '  + 
                '      "id": "",                            '  + 
              '      "account_id": "' + account.Id + '",    '  +
                '      "pop_type": "service_charge",      '  +
                '      "payment_date": "2020-12-21",      '  +
                '      "payment_mode": "Credit Card",      '  +
                '      "cash_deposit_slip": "",          '  +
                '      "cheque_from_account": "",        '  +
                '      "source_of_funds": "test source",    '  +
                '      "payment_remarks": "test remarks",    '  +
                '      "payment_currency": "AED",        '  +
                '      "total_amount": 10,            '  +
                '      "sender_name": "test sender",      '  +
                '      "sender_bank_name": "test bank",      '  +
                '      "swift_code": "1234",          '  +
                '      "unit_details": [            '  +
                '          {                  '  +
                '         "unit_id": "' + bookingUnit.Id + '"  '  +
                '          }                  '  +
                '      ]                    '  +
                '  }  ' ;
      
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void TestRenewalSubmitInstallment() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        Case caseRecord = new Case(Type='Proof of Payment SR',RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId(),
                                   SR_Type__c='Proof of Payment SR',AccountId=account.Id,Payment_Date__c=System.today());
        insert caseRecord;
        
        String jsonString =  
                '   {                              '  + 
                '      "action": "submit",                    '  + 
                '      "id": "' + caseRecord.Id + '",           '  + 
              '      "account_id": "' + account.Id + '",    '  +
                '      "pop_type": "installment",        '  +
                '      "payment_date": "2020-12-21",      '  +
                '      "payment_mode": "Credit Card",      '  +
                '      "cash_deposit_slip": "",          '  +
                '      "cheque_from_account": "",        '  +
                '      "source_of_funds": "test source",    '  +
                '      "payment_remarks": "test remarks",    '  +
                '      "payment_currency": "AED",        '  +
                '      "total_amount": 10,            '  +
                '      "sender_name": "test sender",      '  +
                '      "sender_bank_name": "test bank",      '  +
                '      "swift_code": "1234",          '  +
                '      "unit_details": [            '  +
                '          {                  '  +
                '         "unit_id": "' + bookingUnit.Id + '"  '  +
                '          }                  '  +
                '      ]                    '  +
                '  }  ' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void TestRenewalSubmitServiceCharge() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c='Proof_of_Payment',RecordTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Proof Of Payment').getRecordTypeId(),
                                           Request_Type__c='Proof Of Payment',Account__c=account.Id,Payment_Date__c=System.today());
        insert fmCase;
        
        String jsonString =  
                '   {                              '  + 
                '      "action": "submit",                    '  + 
                '      "id": "' + fmCase.Id + '",               '  + 
              '      "account_id": "' + account.Id + '",    '  +
                '      "pop_type": "service_charge",      '  +
                '      "payment_date": "2020-12-21",      '  +
                '      "payment_mode": "Credit Card",      '  +
                '      "cash_deposit_slip": "",          '  +
                '      "cheque_from_account": "",        '  +
                '      "source_of_funds": "test source",    '  +
                '      "payment_remarks": "test remarks",    '  +
                '      "payment_currency": "AED",        '  +
                '      "total_amount": 10,            '  +
                '      "sender_name": "test sender",      '  +
                '      "sender_bank_name": "test bank",      '  +
                '      "swift_code": "1234",          '  +
                '      "unit_details": [            '  +
                '          {                  '  +
                '         "unit_id": "' + bookingUnit.Id + '"  '  +
                '          }                  '  +
                '      ]                    '  +
                '  }  ' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void apiMainMethodTestNoJSON() {
        String jsonString = ''; 
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void apiMainMethodTestNoAction() {
        String jsonString =    
                '   {              '  + 
                '       "action": "",      '  + 
                '       "id": "",        '  + 
                '       "account_id" : ""    '  + 
                '  }  ' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
  
    @isTest
    static void apiMainMethodTestNoAccount() {
        String jsonString =  
                '   {              '  + 
                '       "action": "draft",  '  + 
                '       "id": "",        '  + 
                '       "account_id" : ""    '  + 
                '  }  ' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void apiMainMethodTestNoPopType() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        String jsonString =  
                '   {                        '  + 
                '       "action": "draft",            '  + 
                '       "id": "",                  '  + 
                '       "account_id" : "' + account.Id + '",    '  + 
              '        "pop_type": ""              '  +
                '  }  ' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void apiMainMethodTestInvalidPopType() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        String jsonString =  
                '   {                        '  + 
                '       "action": "draft",            '  + 
                '       "id": "",                  '  + 
                '       "account_id" : "' + account.Id + '",    '  + 
              '        "pop_type": "pop"            '  +
                '  }  ' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void apiMainMethodTestNoPaymentMode() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        String jsonString =  
                '   {                              '  + 
                '      "action": "draft",                      '  + 
                '      "id": "",                            '  + 
              '      "account_id": "' + account.Id + '",    '  +
                '      "pop_type": "installment",        '  +
                '      "payment_date": "2020-12-21",      '  +
                '      "payment_mode": "",            '  +
                '      "cash_deposit_slip": "",          '  +
                '      "cheque_from_account": "",        '  +
                '      "source_of_funds": "test source",    '  +
                '      "payment_remarks": "test remarks",    '  +
                '      "payment_currency": "AED",        '  +
                '      "total_amount": 10,            '  +
                '      "sender_name": "test sender",      '  +
                '      "sender_bank_name": "test bank",      '  +
                '      "swift_code": "1234",          '  +
                '      "unit_details": [            '  +
                '          {                  '  +
                '         "unit_id": "' + bookingUnit.Id + '"  '  +
                '          }                  '  +
                '      ]                    '  +
                '  }  ' ;
      
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void apiMainMethodTestNoPaymentDate() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        String jsonString =  
                '   {                              '  + 
                '      "action": "draft",                      '  + 
                '      "id": "",                            '  + 
              '      "account_id": "' + account.Id + '",    '  +
                '      "pop_type": "installment",        '  +
                '      "payment_date": "",            '  +
                '      "payment_mode": "Credit Card",      '  +
                '      "cash_deposit_slip": "",          '  +
                '      "cheque_from_account": "",        '  +
                '      "source_of_funds": "test source",    '  +
                '      "payment_remarks": "test remarks",    '  +
                '      "payment_currency": "AED",        '  +
                '      "total_amount": 10,            '  +
                '      "sender_name": "test sender",      '  +
                '      "sender_bank_name": "test bank",      '  +
                '      "swift_code": "1234",          '  +
                '      "unit_details": [            '  +
                '          {                  '  +
                '         "unit_id": "' + bookingUnit.Id + '"  '  +
                '          }                  '  +
                '      ]                    '  +
                '  }  ' ;
      
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void apiMainMethodTestNoAmount() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        String jsonString =  
                '   {                              '  + 
                '      "action": "draft",                      '  + 
                '      "id": "",                            '  + 
              '      "account_id": "' + account.Id + '",    '  +
                '      "pop_type": "installment",        '  +
                '      "payment_date": "2020-12-21",      '  +
                '      "payment_mode": "Credit Card",      '  +
                '      "cash_deposit_slip": "",          '  +
                '      "cheque_from_account": "",        '  +
                '      "source_of_funds": "test source",    '  +
                '      "payment_remarks": "test remarks",    '  +
                '      "payment_currency": "AED",        '  +
                '      "sender_name": "test sender",      '  +
                '      "sender_bank_name": "test bank",      '  +
                '      "swift_code": "1234",          '  +
                '      "unit_details": [            '  +
                '          {                  '  +
                '         "unit_id": "' + bookingUnit.Id + '"  '  +
                '          }                  '  +
                '      ]                    '  +
                '  }  ' ;
      
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void apiMainMethodTestNoUnitDetails() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        String jsonString =  
                '   {                              '  + 
                '      "action": "draft",                      '  + 
                '      "id": "",                            '  + 
              '      "account_id": "' + account.Id + '",    '  +
                '      "pop_type": "installment",        '  +
                '      "payment_date": "2020-12-21",      '  +
                '      "payment_mode": "Credit Card",      '  +
                '      "cash_deposit_slip": "",          '  +
                '      "cheque_from_account": "",        '  +
                '      "source_of_funds": "test source",    '  +
                '      "payment_remarks": "test remarks",    '  +
                '      "payment_currency": "AED",        '  +
              '      "total_amount": 10,            '  +
                '      "sender_name": "test sender",      '  +
                '      "sender_bank_name": "test bank",      '  +
                '      "swift_code": "1234"            '  +
                '  }  ' ;
      
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void apiMainMethodTestNoSourceOfFunds() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        String jsonString =  
                '   {                              '  + 
                '      "action": "draft",                      '  + 
                '      "id": "",                            '  + 
              '      "account_id": "' + account.Id + '",    '  +
                '      "pop_type": "installment",        '  +
                '      "payment_date": "2020-12-21",      '  +
                '      "payment_mode": "Credit Card",      '  +
                '      "cash_deposit_slip": "",          '  +
                '      "cheque_from_account": "",        '  +
                '      "payment_remarks": "test remarks",    '  +
                '      "payment_currency": "AED",        '  +
                '      "total_amount": 10,            '  +
                '      "sender_name": "test sender",      '  +
                '      "sender_bank_name": "test bank",      '  +
                '      "swift_code": "1234",          '  +
                '      "unit_details": [            '  +
                '          {                  '  +
                '         "unit_id": "' + bookingUnit.Id + '"  '  +
                '          }                  '  +
                '      ]                    '  +
                '  }  ' ;
      
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void apiMainMethodTestNoChequeOptions() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        String jsonString =  
                '   {                              '  + 
                '      "action": "draft",                      '  + 
                '      "id": "",                            '  + 
              '      "account_id": "' + account.Id + '",    '  +
                '      "pop_type": "installment",        '  +
                '      "payment_date": "2020-12-21",      '  +
                '      "payment_mode": "Cheque",        '  +
                '      "cash_deposit_slip": "",          '  +
                '      "source_of_funds": "test source",    '  +
                '      "payment_remarks": "test remarks",    '  +
                '      "payment_currency": "AED",        '  +
                '      "total_amount": 10,            '  +
                '      "sender_name": "test sender",      '  +
                '      "sender_bank_name": "test bank",      '  +
                '      "swift_code": "1234",          '  +
                '      "unit_details": [            '  +
                '          {                  '  +
                '         "unit_id": "' + bookingUnit.Id + '"  '  +
                '          }                  '  +
                '      ]                    '  +
                '  }  ' ;
      
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void apiMainMethodTestNoFmCaseOnSubmit() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        String jsonString =  
                '   {                              '  + 
                '      "action": "submit",                    '  + 
                '      "id": "",                            '  + 
              '      "account_id": "' + account.Id + '",    '  +
                '      "pop_type": "installment",        '  +
                '      "payment_date": "2020-12-21",      '  +
                '      "payment_mode": "Credit Card",      '  +
                '      "cash_deposit_slip": "",          '  +
                '      "cheque_from_account": "",        '  +
                '      "source_of_funds": "test source",    '  +
                '      "payment_remarks": "test remarks",    '  +
                '      "payment_currency": "AED",        '  +
                '      "total_amount": 10,            '  +
                '      "sender_name": "test sender",      '  +
                '      "sender_bank_name": "test bank",      '  +
                '      "swift_code": "1234",          '  +
                '      "unit_details": [            '  +
                '          {                  '  +
                '         "unit_id": "' + bookingUnit.Id + '"  '  +
                '          }                  '  +
                '      ]                    '  +
                '  }  ' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popCreateSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        POPCreateSRAPI.apiMainMethod();
        
        Test.stopTest();
    }
}