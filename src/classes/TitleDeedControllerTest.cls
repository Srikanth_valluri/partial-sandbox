/*-------------------------------------------------------------------------------------------------
Description: Test class for TitleDeedController

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 21-12-2017       | Lochana Rajput    | 1. Initial draft
   =============================================================================================================================
*/
@isTest
private class TitleDeedControllerTest {

    @testSetup
    static void createSetupDate() {
        insert TestDataFactory_CRM.createPersonAccount();
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'CreateHoCLOnUnitNoticeSent',OnOffCheck__c = true));
        insert settingLst2;
        
        List<Process_Validations__c>validationLst = new List<Process_Validations__c>();
        validationLst.add( new Process_Validations__c(Name = 'PCC Check', Process_Name__c = 'TitleDeed', Allow_Validation__c = true));
        insert validationLst;
    }

    @isTest static void test_init() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,IsPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 5);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 20);
        insert lstBookingUnits;
        Test.startTest();
        TitleDeedController controller = new TitleDeedController();
        controller.strAccountID = objAcc.Id;
        controller.isPersonAccount = objAcc.IsPersonAccount;
        controller.strSRType = 'TitleDeed';
        controller.getName();
        controller.init(false);
        Test.stopTest();
    }

    @isTest static void test_saveDraft() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,party_ID__c,IsPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 5);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 20);
        lstBookingUnits[0].Registration_ID__c = '74712';
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Title Deed').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;

        Case objCase1 = [SELECT ID, OwnerId FROM Case where Id =: objCase.ID];
        Task taskObj = TestDataFactory_CRM.createTask(objCase1,'Test Subject', 'CRE', 'Title Deed', System.today());
        insert taskObj;
        Test.startTest();
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        pageRef.getParameters().put('CaseId', String.valueOf(objCase.Id));
        pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('SRType', 'TitleDeed');
        Test.setCurrentPage(pageRef);
        TitleDeedController controller = new TitleDeedController();
        controller.PCCFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.PCCFileName = 'Test1.pdf';
        controller.nationalIdFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.nationalIdFileName = 'Test1.pdf';
        controller.passportFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.passportFileName = 'Test1.pdf';
        controller.IDFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.IDFileName = 'Test1.pdf';
        controller.familyBookFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.familyBookFileName = 'Test1.pdf';
        controller.cocdFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.cocdFileName = 'Test1.pdf';
        controller.isPersonAccount = objAcc.IsPersonAccount;
        controller.saveDraft();
        Test.stopTest();
    }
    
    @isTest static void test_submitCase() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,IsPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 5);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 20);
        lstBookingUnits[0].Registration_ID__c = '74712';
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        Test.startTest();
        TitleDeedController controller = new TitleDeedController();
        controller.isPersonAccount = objAcc.isPersonAccount;
        controller.strAccountID = objAcc.Id;
        controller.strSRType = 'TitleDeed';
        controller.getName();
        controller.PCCFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.PCCFileName = 'Test1.pdf';
        controller.nationalIdFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.nationalIdFileName = 'Test1.pdf';
        controller.passportFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.passportFileName = 'Test1.pdf';
        controller.IDFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.IDFileName = 'Test1.pdf';
        controller.selectedBookingUnit = lstBookingUnits[0].Id;
        controller.isPersonAccount = objAcc.IsPersonAccount;
        controller.getSelectedUnitDetails();
        controller.saveDraft();

        Test.stopTest();
        controller.submitCase();

    }

    @isTest static void test_removeAttachment() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,party_ID__c,isPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 5);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 20);
        lstBookingUnits[0].Registration_ID__c = '74712';
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Title Deed').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Status = 'Submitted';
        insert objCase;
        SR_Attachments__c pccDoc = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'PCC');
        insert pccDoc;
        Case objCase1 = [SELECT ID, OwnerId FROM Case where Id =: objCase.ID];
        Task taskObj = TestDataFactory_CRM.createTask(objCase1,'Test Subject', 'CRE', 'Title Deed', System.today());
        insert taskObj;
        Test.startTest();
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        pageRef.getParameters().put('CaseId', String.valueOf(objCase.Id));
        pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('SRType', 'TitleDeed');
        Test.setCurrentPage(pageRef);
        TitleDeedController controller = new TitleDeedController();
        controller.isPersonAccount = objAcc.isPersonAccount;
        Test.stopTest();
        controller.SRAttachmentToRemove = [SELECT Id FROM SR_Attachments__c LIMIT 1].ID;
        controller.removeAttachment();
    }
    
    @isTest static void test_removeAttachmentNegative() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        insert objAcc;
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 5);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 20);
        lstBookingUnits[0].Registration_ID__c = '74712';
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Title Deed').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Status = 'Submitted';
        insert objCase;
        SR_Attachments__c pccDoc = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'PCC');
        insert pccDoc;
        Case objCase1 = [SELECT ID, OwnerId FROM Case where Id =: objCase.ID];
        Task taskObj = TestDataFactory_CRM.createTask(objCase1,'Test Subject', 'CRE', 'Title Deed', System.today());
        insert taskObj;
        Test.startTest();
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        pageRef.getParameters().put('CaseId', String.valueOf(objCase.Id));
        pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('SRType', 'TitleDeed');
        Test.setCurrentPage(pageRef);
        TitleDeedController controller = new TitleDeedController();
        Test.stopTest();
        controller.SRAttachmentToRemove = [SELECT Id FROM SR_Attachments__c LIMIT 1].ID;
        controller.removeAttachment();
    }

    @isTest static void test_submitTitleDeedCopy() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,party_ID__c,isPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 5);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 20);
        lstBookingUnits[0].Registration_ID__c = '74712';
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Title Deed').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Status = 'Submitted';
        insert objCase;
        SR_Attachments__c pccDoc = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'PCC');
        insert pccDoc;
        Case objCase1 = [SELECT ID, OwnerId FROM Case where Id =: objCase.ID];
        /*Task taskObj = TestDataFactory_CRM.createTask(objCase1,'Test Subject', 'CRE', 'Title Deed', System.today());
        insert taskObj;*/
        Test.startTest();
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        pageRef.getParameters().put('CaseId', String.valueOf(objCase.Id));
        pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('SRType', 'TitleDeed');
        Test.setCurrentPage(pageRef);
        TitleDeedController controller = new TitleDeedController();
        controller.titleDeedFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.titleDeedFileName = 'Test1.pdf';
        controller.isPersonAccount = objAcc.isPersonAccount;
        controller.selectedBookingUnit = lstBookingUnits[0].Id;
        //controller.lstTask.add(taskObj);
        controller.submitTitleDeedCopy();
        controller.SRAttachmentToRemove = [SELECT Id FROM SR_Attachments__c LIMIT 1].ID;
        controller.removeAttachment();
        Test.stopTest();
    }

    @isTest static void test_existingCase() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id, Party_ID__c,isPersonAccount from Account LIMIT 1];
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Title Deed').getRecordTypeId();

        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 5);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 20);
        lstBookingUnits[0].Registration_ID__c = '74712';
        insert lstBookingUnits;
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;
        SR_Attachments__c attPCC = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'PCC');
        insert attPCC;
        //Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        pageRef.getParameters().put('CaseId', String.valueOf(objCase.Id));
        pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('SRType', 'TitleDeed');
        Test.setCurrentPage(pageRef);
        TitleDeedController controller = new TitleDeedController();
        controller.accountId = objAcc.Id;
        controller.SRType = 'TitleDeed';
        controller.isPersonAccount = objAcc.isPersonAccount;
        controller.getName();
        controller.titleDeedFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.titleDeedFileName = 'Test1.pdf';
        controller.selectedBookingUnit = lstBookingUnits[0].Id;
        controller.insertStatementOfAccount();
        /*controller.getSelectedUnitDetails();
        controller.saveDraft();
        controller.submitCase();
        //Case objCase1 = [SELECT ID, OwnerId FROM Case where Id =: controller.objCase.ID];
        //Task taskObj = TestDataFactory_CRM.createTask(objCase1,'Test Subject', 'CRE', 'Title Deed', System.today());
        //insert taskObj;
        //controller.lstTask.add(taskObj);
        // controller.submitTitleDeedCopy();
        //controller.insertStatementOfAccount();*/
        Test.stopTest();
    }

    @isTest static void test_saveDraftForCorporateAccount() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        insert objAcc;
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 5);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 20);
        lstBookingUnits[0].Registration_ID__c = '74712';
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Title Deed').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;

        Case objCase1 = [SELECT ID, OwnerId FROM Case where Id =: objCase.ID];
        Task taskObj = TestDataFactory_CRM.createTask(objCase1,'Test Subject', 'CRE', 'Title Deed', System.today());
        insert taskObj;
        Test.startTest();
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        pageRef.getParameters().put('CaseId', String.valueOf(objCase.Id));
        pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('SRType', 'TitleDeed');
        Test.setCurrentPage(pageRef);
        TitleDeedController controller = new TitleDeedController();
        controller.PCCFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.PCCFileName = 'Test1.pdf';
        controller.nationalIdFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.nationalIdFileName = 'Test1.pdf';
        controller.passportFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.passportFileName = 'Test1.pdf';
        controller.IDFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.IDFileName = 'Test1.pdf';
        controller.moaFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.moaFileName = 'Test1.pdf';
        controller.tradeLicenseFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.tradeLicenseFileName = 'Test1.pdf';
        controller.familyBookFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.familyBookFileName = 'Test1.pdf';
        controller.cocdFileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.cocdFileName = 'Test1.pdf';
        controller.isPersonAccount = objAcc.IsPersonAccount;
        controller.saveDraft();
        controller.checkDocumentsForSubmition();
        Test.stopTest();
    }

    @isTest static void test_removeAttachmentForGCCId() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,party_ID__c,isPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 5);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 20);
        lstBookingUnits[0].Registration_ID__c = '74712';
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Title Deed').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Status = 'Submitted';
        insert objCase;
        SR_Attachments__c pccDoc = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'GCC Id');
        insert pccDoc;
        Case objCase1 = [SELECT ID, OwnerId FROM Case where Id =: objCase.ID];
        Task taskObj = TestDataFactory_CRM.createTask(objCase1,'Test Subject', 'CRE', 'Title Deed', System.today());
        insert taskObj;
        Test.startTest();
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        pageRef.getParameters().put('CaseId', String.valueOf(objCase.Id));
        pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('SRType', 'TitleDeed');
        Test.setCurrentPage(pageRef);
        TitleDeedController controller = new TitleDeedController();
        controller.isPersonAccount = objAcc.isPersonAccount;
        Test.stopTest();
        controller.SRAttachmentToRemove = [SELECT Id FROM SR_Attachments__c LIMIT 1].ID;
        controller.removeAttachment();
    }

    @isTest static void test_removeAttachmentForPassport() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,party_ID__c,isPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 5);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 20);
        lstBookingUnits[0].Registration_ID__c = '74712';
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Title Deed').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Status = 'Submitted';
        insert objCase;
        SR_Attachments__c pccDoc = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'Passport');
        insert pccDoc;
        Case objCase1 = [SELECT ID, OwnerId FROM Case where Id =: objCase.ID];
        Task taskObj = TestDataFactory_CRM.createTask(objCase1,'Test Subject', 'CRE', 'Title Deed', System.today());
        insert taskObj;
        Test.startTest();
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        pageRef.getParameters().put('CaseId', String.valueOf(objCase.Id));
        pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('SRType', 'TitleDeed');
        Test.setCurrentPage(pageRef);
        TitleDeedController controller = new TitleDeedController();
        controller.isPersonAccount = objAcc.isPersonAccount;
        Test.stopTest();
        controller.SRAttachmentToRemove = [SELECT Id FROM SR_Attachments__c LIMIT 1].ID;
        controller.removeAttachment();
    }
}