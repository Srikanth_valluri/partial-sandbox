/**
 * @File Name          : SendBirthdayWishToCustomerBatchTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 3/16/2020, 6:59:58 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    3/16/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
public class SendBirthdayWishToCustomerBatchTest {
    
    @isTest
    private static void positiveTest() {
         Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount2 = TestDataFactory_CRM.createPersonAccount();
        objAccount2.Nationality__c = 'Saudi';
        objAccount2.Email__pc = 'gsauf2@hjfd.com';
        objAccount2.Active_Customer__c = 'Active';
        Date tody= system.today();
        objAccount2.Date_Of_Birth__pc = date.newInstance(1994, tody.month(),tody.day());
        insert objAccount2;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount2.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;

        Test.StartTest();
            DataBase.executeBatch(new SendBirthdayWishToCustomerBatch(), 1);
        Test.StopTest();
    }
}