/*
 * Description: Class used to create 'Non-SR' type of Case from Task detail page
 */

public class GenerateNonSRCaseExtension {
    public Task objTask {get;set;}
    public Case objCase {get;set;}
    public String strAccountName {get; set;}

    public GenerateNonSRCaseExtension (ApexPages.StandardController controller) {
        objTask = (Task)controller.getrecord();
        objCase = new Case();
        
        // Fetch the Task details
        objTask = [SELECT Id, AccountId,WhoId,Who.Name, Description FROM Task WHERE Id =: objTask.Id];
        
        //system.assert(false,'Who.Name : '+objTask.Who.Name);
        String strWhoId = objTask.WhoId;
        
        // Check if the WhoId is Contact then fetch its related Account
        if (String.isNotBlank(strWhoId) && strWhoId.startsWith('003')) {
            List<Contact> lstContact = [SELECT AccountId, Account.Name FROM Contact WHERE Id =: strWhoId];
            if (lstContact != null && !lstContact.isEmpty()) {
                strWhoId = lstContact[0].AccountId;
                strAccountName = lstContact[0].Account.Name;
            }
        }
        objCase.AccountId = strWhoId;
        objCase.Description = objTask.Description;
    }
    
    /**
     * Method used to create 'Non-SR' type of Case
     */
    public pageReference createNonSRCase() {
        objCase.recordTypeId = getRecordTypeIdForNonSRCase();
        objCase.Status = 'Closed';
        insert objCase;
        
        // Update the task with newly created case
        if (objCase.Id != null) {
            objTask.WhatId = objCase.Id;
            objTask.status = 'Completed';
            update objTask;
        }
    
        // Redirect to Task detail page
        PageReference taskDetailPage = new PageReference ('/'+objTask.Id);
        return taskDetailPage ;
    }

    /**
     * Method to get the "Non-SR" record type
     */
    private static Id getRecordTypeIdForNonSRCase() {
        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id nonSRRecordTypeID = caseRecordTypes.get('Non SR case').getRecordTypeId();
        return nonSRRecordTypeID ;
    }
}