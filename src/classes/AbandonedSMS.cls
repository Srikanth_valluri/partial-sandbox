public with sharing class AbandonedSMS{//AKISHOR
  public Set<String> nsPhones                 {Set;get;}
    public Set<String> sPhones                 {set;get;}
    public List<selectoption> soptions        {set;get;}
    public List<selectoption> nsoptions         {set;get;}
    public List<string> selected              {set;get;}
    public List<string> removed             {set;get;}
    public String msgContent            {get;set;}
    public String callingId              {get;set;}
    public String selectedTemplateId         {get;set;}
    public String templateBody             {get;set;}
    public Boolean isComplete             {get;set;}
    public  Map<String,String>valToLabelFieldMap   {get;set;}
    public  List<String>selectedLabelsLst      {get;set;}
    Public Map<String,String> mapMaskedNoUnmasked {get;set;}
    
    String accntId;
    public AbandonedSMS(ApexPages.StandardController controller){
        isComplete = false;
        mapMaskedNoUnmasked = new Map<String,String>();
        valToLabelFieldMap = new Map<String,String>();
      callingId = ApexPages.currentPage().getParameters().get('Id');
      callingId = String.valueOf(callingId).substring(0, 15);
      selectedLabelsLst = new List<String>();
      nsPhones = new set<String>();
      List<String> countryCodestr = new List<String>();  
      List<String>phoneNumberLst = new List<String>();
      Map<String,String>phoneValueToPhoneLabelMap = new Map<String,String>();
        List<Abandoned_Calls__c>callingList = [SELECT Id,
                            Mobile_No__c,Account__c
                       FROM Abandoned_Calls__c
                      WHERE Id = :callingId 
                       ];
          
       if( callingList[0].Mobile_No__c != null) {
            //nsPhones.add(callingList[0].Mobile_No__c);
            nsPhones.add(maskNo(callingList[0].Mobile_No__c));
            mapMaskedNoUnmasked.put(maskNo(callingList[0].Mobile_No__c),callingList[0].Mobile_No__c);
                }
       String labelForPhone;
            accntId=callingList[0].Account__c;
        sPhones=new Set<string>();      
        soptions=new List<selectoption>();
        nsoptions=new List<selectoption>();
        selected=new List<string>();
        removed=new List<string>();
        fetchPhones();
    
  }  
    public void fetchPhones(){
      nsoptions.clear();
        soptions.clear();
        if(sPhones.size() ==0){
            SelectOption p=new SelectOption('none','--None--');
            soptions.add(p);
        }else{
            for(String firstSelectOpt : sPhones){
              if(firstSelectOpt != null){
                   SelectOption firstOpt = new SelectOption(firstSelectOpt,firstSelectOpt);
                   soptions.add(firstOpt);
                }
            }
        }
         if(nsPhones.size() == 0){
           SelectOption firstNotSelectOpt = new SelectOption('none','--None--');
            nsoptions.add(firstNotSelectOpt);
        }else{
          for(String selectOpt : nsPhones){
            if(selectOpt != null){
              SelectOption notSelectOptions = new SelectOption(selectOpt,selectOpt);
                  nsoptions.add(notSelectOptions);
            }
                
            }
        }
    }
    public void addElements(){
      List<String>selectedValLst = new  List<String>();
        for(String selectedInst : selected){
          if(selectedInst != null){
            selectedValLst.add(selectedInst);
          }
        }
        sPhones.addAll(selectedValLst);
        nsPhones.removeAll(selected);
        fetchPhones();
    }
    public void removeElements(){
      List<string>removedValToLabelLst = new List<string>();
        for(String removedInst : removed){
          if(removedInst != null){
            removedValToLabelLst.add(removedInst);
          }
        }
        sPhones.removeAll(removed);
        nsPhones.addAll(removedValToLabelLst);
        fetchPhones();
    }
    public List<SelectOption> getMyPersonalTemplateOptions(){
      List<SelectOption> options = new List<SelectOption>();
      if(options.size() == 0)
        options.add(new SelectOption('--None--','--None--'));
      for(EmailTemplate emailObj :[SELECT Id,
                         Name,
                         Body,
                         TemplateType 
                      FROM EmailTemplate
                       WHERE Folder.Name = 'SMS Templates'
                       AND TemplateType = 'Visualforce'
                       ]){
         
        options.add(new SelectOption(emailObj.Id,emailObj.Name));
      }
      return options;
    }
    public void showContent(){        
        if( selectedTemplateId == '--None--' || selectedTemplateId == '' ) {
            templatebody = '';
            return ;
        }
        
      EmailTemplate selectedTempLate = [SELECT Subject, 
                           Body, 
                           HtmlValue, 
                           Markup, 
                           TemplateType FROM EmailTemplate
                        WHERE  Id = :selectedTemplateId];
       templatebody = String.valueOf((selectedTempLate.body));
       
    Messaging.reserveSingleEmailCapacity(1);
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    String[] toAddresses = new String[]{'invalid@emailaddr.es'};
    mail.setToAddresses(toAddresses);
    mail.setUseSignature(false);
    mail.setSaveAsActivity(false);
    mail.setSenderDisplayName('DAMAC');
    
    mail.setTargetObjectId(UserInfo.getUserId());
    mail.setWhatId(callingId);
    mail.setTemplateId(selectedTempLate.Id);
    Savepoint sp = Database.setSavepoint();
    if(!Test.isRunningTest())
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    Database.rollback(sp);
    templatebody = mail.getPlainTextBody();
    String mailHtmlBody = mail.getHTMLBody();
    String mailSubject = mail.getSubject();
    }
    
    
    public pageReference callSMSService(){
      List<String> listStrings = new List<String>(sPhones);
      
      List<String> toSendStrings = new List<String>();
          for(string str : listStrings){
              toSendStrings.add(mapMaskedNoUnmasked.get(str));
          }
      
      if ( toSendStrings.isEmpty() == true ) {
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please choose atleast 1 mobile number'));
   
        }
        else if( String.isBlank( templateBody ) ){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter some text'));
        }
        else {
           SendSMSAccountService.Sendtextmessage(toSendStrings,templateBody,accntId,false,callingId);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.confirm, 'Message Successfully Sent !!! '));
       isComplete = true;
       return new Pagereference(String.format(Label.Lightning_Calling_Detail_Page_Url, new List<String>{callingId}));  
        }
       return null;
    }
    
    Public String maskNo (String tempMob){
       return tempMob.Substring(0, 3) + 'xxxx'+tempMob.Substring(tempMob.length()-3,tempMob.length());
   }
    
}