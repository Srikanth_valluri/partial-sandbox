/********************************************************************************************************************************
 * Description : Batch class to re-assign owner of calling lists in bulk
 *===============================================================================================================================
 * Ver      Date-DD/MM/YYYY     Author              Modification
 *===============================================================================================================================
 * 1.0      06/05/2020          Aishwarya Todkar    Initial Draft
*********************************************************************************************************************************/
global class OwnerReAssignmentBatch implements Database.Batchable<SObject>, Database.Stateful {
    
    global Id changeOwnerToId;
    global Set<String> stringSet;
    String objectName;

    global OwnerReAssignmentBatch(Set<String> stringSet, Id changeOwnerToId, String objectName){
        this.changeOwnerToId = changeOwnerToId;
        this.stringSet = stringSet;
        this.objectName = objectName;
    }
    global Database.QueryLocator start( Database.BatchableContext bc ) {
        String query = 'SELECT Id, OwnerId FROM '+ objectName+' WHERE Id In : stringSet AND OwnerId !=: changeOwnerToId';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        List<sObject> listToReAssignOwner = new List<sObject>();
        for( sObject sObj : scope ) {
            sObj.put('ownerId', changeOwnerToId);
            
            listToReAssignOwner.add( sObj ); 
        }
        
        if( listToReAssignOwner != null && listToReAssignOwner.size() > 0 ) {
            Database.update( listToReAssignOwner,false );
        }
    }
    global void finish(Database.BatchableContext bc){
        
    }
}