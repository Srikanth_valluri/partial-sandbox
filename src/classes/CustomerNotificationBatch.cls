public with sharing class CustomerNotificationBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
     
     private list<Error_Log__c> lstLogsToBeInserted ;
     private list<SMS_History__c> lstSMSHistory ;
     private CustomerNotificationInfoWrapper objInfoWrap ;
     
     public CustomerNotificationBatch() {
     	system.debug('== Inside the CustomerNotificationBatch constructor ==');
     	lstLogsToBeInserted = new list<Error_Log__c>();
     	lstSMSHistory = new list<SMS_History__c>();
     	
     	objInfoWrap = new CustomerNotificationInfoWrapper(); 
     	objInfoWrap = CustomerNotificationBatchHelper.getMapDaysEmailTemplate( CustomerNotificationBatchUtility.getListOfNotificationSettings(), objInfoWrap );
     	system.debug('== objInfoWrap after populating the custom metadata and set of email templates \n == '+objInfoWrap);
     	if( objInfoWrap.mapDaysNotifications != null && !objInfoWrap.mapDaysNotifications.isEmpty() &&
     		objInfoWrap.setEmailTemplateNames != null && !objInfoWrap.setEmailTemplateNames.isEmpty() ) { 
     		objInfoWrap = CustomerNotificationBatchHelper.getEmailTemplateMap( CustomerNotificationBatchUtility.getEmailTemplates( objInfoWrap.setEmailTemplateNames ), objInfoWrap );
     	}
     	system.debug('== objInfoWrap after populating the email templates \n == '+objInfoWrap);
     }
     
     public Database.Querylocator start( Database.BatchableContext bc ) {
     	system.debug('== Inside the CustomerNotificationBatch start ==');
     	return Database.getQueryLocator( CustomerNotificationBatchHelper.makeQueryString() );
     }
     
     public void execute( Database.BatchableContext bc, list<Calling_List__c> lstCalling ) {
     	system.debug('== Inside the CustomerNotificationBatch execute ==');
     	system.debug('== lstCalling =='+lstCalling);
     	list<Messaging.SingleEmailMessage> lstEmailToBeSent = new list<Messaging.SingleEmailMessage>();
     	
     	if( lstCalling != null && !lstCalling.isEmpty() ) {
     		
     		for( Calling_List__c objCall : lstCalling ) {
     			system.debug('== objCall =='+objCall);
     			
     			if( objCall.Account__c != null && objCall.Due_Date__c != null && objCall.Mobile_Phone__c != null && objInfoWrap != null &&
     				String.isNotBlank( objCall.Email__c ) && String.isNotBlank( objCall.Registration_ID__c ) ) {
	     			
	     			Integer intDaysDiff = calculateDateDifference( objCall.Due_Date__c ) ; 
	     			system.debug('== intDaysDiff =='+intDaysDiff );
	     			
	     			if( objInfoWrap.mapDaysNotifications != null && objInfoWrap.mapDaysNotifications.containsKey( intDaysDiff ) && 
	     				objInfoWrap.mapDNEmailTemplate != null ) {
	     				
	     				system.debug('== list of notifications =='+objInfoWrap.mapDaysNotifications.get( intDaysDiff ) );
	     				
	     				for( Collection_Customer_Notification_Setting__mdt objNotifi : 
	     														objInfoWrap.mapDaysNotifications.get( intDaysDiff ) ) {
	     					system.debug('== objNotifi =='+objNotifi );
	     					
	     					if( objInfoWrap.mapDNEmailTemplate.containsKey( objNotifi.Email_Template_API_Name__c ) ) {
	     						Id templateId = objInfoWrap.mapDNEmailTemplate.get( objNotifi.Email_Template_API_Name__c ).Id ;
	     						system.debug('== templateId =='+templateId );
	     						// Define the email
		        				//Messaging.reserveSingleEmailCapacity(1);
		        				Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
								email.setToAddresses( new list<String> { 'invalid@emailaddr.es' } );
		     					email.setUseSignature(false);
								email.setSaveAsActivity(false);
								email.setSenderDisplayName('DAMAC');
		     					email.setTargetObjectId( UserInfo.getUserId() );
		     					email.setWhatId( String.valueOf( objCall.Id ) );
		     					email.setTemplateID( templateId );
		     					
		     					if( String.isNotBlank( objNotifi.Notification_Type__c ) && objNotifi.Notification_Type__c.equalsIgnoreCase('Email') ) {
		     						//Generate SOA for this account
	     							try {
	     								GenerateSOAController.soaResponse objSOA = GenerateSOAController.getSOADocument( objCall.Registration_ID__c );
	     								system.debug('== objSOA =='+objSOA );
				     					if( objSOA != null && String.isNotBlank( objSOA.status ) && objSOA.status.equalsIgnoreCase('s') &&
					     					String.isNotBlank( objSOA.PROC_STATUS ) && objSOA.PROC_STATUS.equalsIgnoreCase('s') &&
					     					String.isNotBlank( objSOA.url ) ) {
					     					
					     					// Create the email attachment
						        			HttpRequest req = new HttpRequest();
								            req.setEndpoint( objSOA.url );
								            req.setMethod('GET');
						        			Http http = new Http();
								            HTTPResponse res = http.send(req);
								            Blob objBody = res.getBodyAsBlob();
						        			
						        			email.setToAddresses( new list<String> { objCall.Email__c } );
									        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
									        efa.setFileName('Statement Of Account.pdf');
									        efa.setBody( objBody );
									        email.setFileAttachments(new list<Messaging.EmailFileAttachment> { efa } );
									        lstEmailToBeSent.add( email );
					     				}
					     				else if( objSOA != null && String.isNotBlank( objSOA.status ) && String.isNotBlank( objSOA.PROC_STATUS ) &&
					     						( !objSOA.status.equalsIgnoreCase('s') || !objSOA.PROC_STATUS.equalsIgnoreCase('s') || 
					     						  String.isBlank( objSOA.url ) ) ) {
					     					lstLogsToBeInserted.add( createErrorLogRecord( objCall, objSOA.PROC_MESSAGE ) );
					     				}  
	     							}
				     				catch( Exception e ) {
				     					lstLogsToBeInserted.add( createErrorLogRecord( objCall, e.getMessage() ) );
				     				} 
		     					}
		     					else if( String.isNotBlank( objNotifi.Notification_Type__c ) && objNotifi.Notification_Type__c.equalsIgnoreCase('SMS') ) {
		     						Savepoint sp = Database.setSavepoint();
									Messaging.sendEmail( new Messaging.SingleEmailMessage[]{ email } );
									Database.rollback( sp );
									lstSMSHistory.add( createSMSLogRecord( objCall, email.getPlainTextBody() ) );
									/*SMS_History__c objSMSHist = CustomerNotificationBatchHelper.sendSMSToCustomer( objCall.Mobile_Phone__c , email.getPlainTextBody(), objCall );
		     						if( objSMSHist != null ){
		     							lstSMSHistory.add( objSMSHist );
		     						}*/
		     					}
	     					}
	     				}
	     			}
	     		}
     		}
     		
     		system.debug('== lstEmailToBeSent =='+lstEmailToBeSent);
     		if( lstEmailToBeSent != null && !lstEmailToBeSent.isEmpty() ) {
	     		list<Messaging.SendEmailResult> lstResult = Messaging.sendEmail( lstEmailToBeSent ) ;
	     	}
     	}
     }
     
     public void finish( Database.BatchableContext bc ) {
     	system.debug('== Inside the finish method ==');
     	system.debug('== lstLogsToBeInserted =='+lstLogsToBeInserted);
     	if( lstLogsToBeInserted != null && !lstLogsToBeInserted.isEmpty() ) {
     		insert lstLogsToBeInserted ;
     	}
     	system.debug('== lstSMSHistory =='+lstSMSHistory);
     	if( lstSMSHistory != null && !lstSMSHistory.isEmpty() ) {
     		insert lstSMSHistory ;
     	}
     	Database.executeBatch( new CustomerNotificationSendSMSBatch(), 100 );
     }
     
     @testVisible 
     private Integer calculateDateDifference( Date dateDue ) {
     	return dateDue.daysBetween( system.today() ) ;
     }
     
     @testVisible 
     private Error_Log__c createErrorLogRecord( Calling_List__c objCall, String strMessage ) {
     	Error_Log__c objLog = new Error_Log__c();
 		objLog.Calling_List__c = objCall.Id ;
 		objLog.Account__c = objCall.Account__c;
 		objLog.Booking_Unit__c = objCall.Booking_Unit__c;
 		objLog.Error_Details__c = 'Error Generated while sending the notifications to customer \n Error : '+strMessage ;
 		return objLog ;
     }
     
     @testVisible 
     private SMS_History__c createSMSLogRecord( Calling_List__c objCall, String strMessageBody ) {
     	SMS_History__c objSMSHist = new SMS_History__c();
		objSMSHist.Message__c = strMessageBody;
		objSMSHist.Phone_Number__c = objCall.Mobile_Phone__c ;
		objSMSHist.Customer__c = objCall.Account__c;
		objSMSHist.Calling_List__c = objCall.Id;
		objSMSHist.Is_SMS_Sent__c = false ;
		return objSMSHist ;
     }
     
     public class CustomerNotificationInfoWrapper {
	     public map<Integer, list<Collection_Customer_Notification_Setting__mdt>> mapDaysNotifications ;
	     public map<String, EmailTemplate> mapDNEmailTemplate ;
	     public set<String> setEmailTemplateNames ;
	     //public list<Messaging.SingleEmailMessage> lstEmailToBeSent ;
	     
	     public CustomerNotificationInfoWrapper() {
	     	mapDaysNotifications = new map<Integer, list<Collection_Customer_Notification_Setting__mdt>>();
	     	mapDNEmailTemplate = new map<String, EmailTemplate>();
	     	setEmailTemplateNames = new set<String>();
	     	//lstEmailToBeSent = new list<Messaging.SingleEmailMessage>();
	     }
     }
}