/***************************************************************************************************
 * @Name              : DAMAC_UnitDocumentAttachmentDetails_API
 * @Test Class Name   : UnitDocumentAttachmentDelete_API_Test
 * @Description       : Delete Attachments from Unit Documents based on given Id
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         27/07/2020       Created
****************************************************************************************************/
@RestResource(urlMapping='/deleteAttachmentDetails')
global class DAMAC_UnitDocumentAttachmentDetails_API{
    @HttpPOST
    global static deleteBuyerResponse getResults(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String reqBody = req.requestBody.toString();
        System.debug (reqBody);
        String statusMessage;
        String statusCode;
        deleteBuyerResponse resp = new deleteBuyerResponse ();
        //String buyerId = req.params.get('buyerId');
        String jsonBody = req.requestBody.toString();
        system.debug('jsonBody: ' + jsonBody);
        deleteBuyerRequest reqst = (deleteBuyerRequest) JSON.deserialize(jsonBody, deleteBuyerRequest.class);               
        system.debug('reqst: ' + reqst);
        
        List<Attachment> attachmentList = new List<Attachment>();
        List<Unit_Documents__c> unitDocList = new List<Unit_Documents__c>();
        List<Unit_Documents__c> updatedUnitDocList = new List<Unit_Documents__c>();
        
        list<String> requestAttachmentIds = reqst.attachmentIds;
        
        if(requestAttachmentIds != NULL && !requestAttachmentIds.isEmpty()){
            
            attachmentList = [SELECT Id,ParentId FROM Attachment WHERE Id in: requestAttachmentIds];
            set<Id> parentIds = new set<Id>();
            for(Attachment att: attachmentList){
                parentIds.add(att.parentId);
            }
            unitDocList = [SELECT Sys_Doc_Id__c,Status__c FROM Unit_Documents__c WHERE Id in: parentIds];
            for(Unit_Documents__c unit : unitDocList ){
                unit.Sys_Doc_Id__c = '';
                unit.Status__c = 'Pending Upload';
                updatedUnitDocList.add(unit);
            }
        }
        else{
            statusMessage = 'There Is No Id Given!!';
            statusCode = '400';
        }
       
        if(attachmentList.size() > 0 ){            
            delete attachmentList;
            update updatedUnitDocList;
            statusCode = '200';
            statusMessage = 'Success';
        }
        else{
            statusCode = '400';
            statusMessage = 'Please send valid request!!';
        }
        resp.statusCode = statusCode;
        resp.statusMessage = statusMessage; 
        return resp;
    
    
    }    
    global class deleteBuyerRequest {
        public list<string> attachmentIds {get; set;}               
    }
    
    global class deleteBuyerResponse {        
        public String StatusCode{get; set;}
        public String StatusMessage{get; set;}
    }    

}