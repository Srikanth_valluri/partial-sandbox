/**************************************************************************************************
 * @Name              : InventorySchedulable
 * @Test Class Name   : InventorySchedulableTest
 * @Description       : Schedulable class to invoke Queueable class InventoryQueueable
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst          29/09/2020       Created
**************************************************************************************************/
public class InventorySchedulable implements Schedulable {
    public list<Inventory__c> invList {get;set;}
    
    public InventorySchedulable(List<Inventory__c> invList){
        this.invList = invList;
    }
    public void execute(SchedulableContext sc) {
        System.enqueueJob(new InventoryQueueable(invList));
        // Abort the job once the job is queued
        System.abortJob(sc.getTriggerId());
    }
}