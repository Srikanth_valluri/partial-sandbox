/*-------------------------------------------------------------------------------------------------
Description: Test class for PromotorRegistrationController
====================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------
1.0     | 20-02-2018       | Lochana Rajput   | 1. Added test methods to test the functionality
----------------------------------------------------------------------------------------------------
2.0     | 15-03-2018       | Lochana Rajput   | 1. Updated to cover the updated code in main controller
====================================================================================================
*/
@isTest
public class PromotorRegistrationControllerTest {
    @testsetup
    static void setup() {
        List<Campaign__c> lstCampaign = new List<Campaign__c>();
        for (Integer i=0;i<10;i++) {
            Campaign__c objCampaign = InitialiseTestData.getCampaignDetails();
            objCampaign.Campaign_Type_New__c = 'Stands';
            objCampaign.Marketing_Active__c= true;
            objCampaign.Credit_Control_Active__c= true;
            objCampaign.Sales_Admin_Active__c = true;
            lstCampaign.add(objCampaign);
        }
        insert lstCampaign;
    }

    @istest
    private static void submitInquiry_errprMsg() {
        PromotorRegistrationController controller = new PromotorRegistrationController();
        Campaign__c objCampaign = [SELECT ID from Campaign__c limit 1];
        controller.selectedStand = objCampaign.Id;
        controller.objInquiry.Tour_Date_Time__c = System.now().addDays(4);
        controller.objInquiry.Meeting_Type__c = 'Scheduled Tour';
        controller.userId = UserInfo.getUserId();
        // controller.meetingOnStand = true;
        Test.startTest();
        controller.cancel();
        controller.selectStand();
        controller.submitInquiry();
        PromotorRegistrationController.queryInquiries();
        Test.stopTest();
    }
    @istest
    private static void submitInquiry() {
        PromotorRegistrationController controller = new PromotorRegistrationController();
        Campaign__c objCampaign = [SELECT ID from Campaign__c limit 1];
        controller.selectedStand = objCampaign.Id;
        controller.objInquiry.Tour_Date_Time__c = System.now();
        controller.userId = UserInfo.getUserId();
        // controller.meetingOnStand = true;
        Test.startTest();
        controller.cancel();
        controller.selectStand();
        controller.submitInquiry();
        PromotorRegistrationController.queryInquiries();
        Test.stopTest();
    }
}