@isTest
public class HD_SendNotificationByEmail_APITest {
    private static String baseUrl = URL.getOrgDomainUrl().toExternalForm();  /* URL.getSalesforceBaseUrl() */
    
    @testSetup
    static void setup(){
        Id personAcc_rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc1 = new Account( LastName = 'Test Account1',
                                       Party_ID__c = '63061',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'success@mailinator.com');
        insert acc1;
        
        Account acc2 = new Account( LastName = 'Test Account2',
                                       Party_ID__c = '63062',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'fail@mailinator.com');
        insert acc2;
        
        Account acc3 = new Account( LastName = 'Test Account3',
                                       Party_ID__c = '63063',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = '');
        insert acc3;
    }
    
    @isTest
    static void test_SendMailFail(){
        HD_SendNotificationByEmail_API.TemplateMergeData mergeValueData;
        mergeValueData = new HD_SendNotificationByEmail_API.TemplateMergeData();
        mergeValueData.customer_name = 'TEST OWNER';
        mergeValueData.unit = 'XX/YYYY';
        
        Map<String, Object> requestMap_post = new Map<String, Object>();
        requestMap_post.put('party_id', '63062');
        requestMap_post.put('template_id', 'DH-SUB-DOC-SR');
        requestMap_post.put('send_to_list', NULL); 
        requestMap_post.put('cc_list', NULL); 
        requestMap_post.put('bcc_list', 'v-subin@damacgroup.com');
        requestMap_post.put('merge_values', mergeValueData);
        
        String reqJSON = JSON.serialize(requestMap_post);
        getNewRestContext_SendEmailPOST(reqJSON);
        
        Test.startTest();
        
        invokeEmailNotificationPost(requestMap_post); /* 1 */
        
        Test.stopTest();
    }
    
    @isTest
    static void test_AD_ScheduleSetupMails(){
        List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem> adSchedules = 
            new List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem>();
        HD_SendNotificationByEmail_API.AutoDebitScheduleItem adSched = 
            new HD_SendNotificationByEmail_API.AutoDebitScheduleItem();
        adSched.unit_number = '';
        adSched.invoice_number = '';
        adSched.due_date = '';
        adSched.amount = '';
        adSchedules.add(adSched);
        
        HD_SendNotificationByEmail_API.TemplateMergeData mergeValueData;
        mergeValueData = new HD_SendNotificationByEmail_API.TemplateMergeData();
        mergeValueData.customer_name = 'TEST OWNER';
        mergeValueData.unit = '';
        mergeValueData.auto_debit_schedules = NULL;
        
        Map<String, Object> requestMap_post = new Map<String, Object>();
        requestMap_post.put('party_id', NULL);
        requestMap_post.put('template_id', NULL);
        requestMap_post.put('send_to_list', NULL); 
        requestMap_post.put('cc_list', NULL); 
        requestMap_post.put('bcc_list', 'v-subin@damacgroup.com'); 
        requestMap_post.put('merge_values', NULL);
        
        String reqJSON = JSON.serialize(requestMap_post);
        getNewRestContext_SendEmailPOST(reqJSON);
        
        Test.startTest();
        
        invokeEmailNotificationPost(requestMap_post); /* 1 no party */
        
        requestMap_post.put('party_id', '63060');
        invokeEmailNotificationPost(requestMap_post); /* 2 invalid party */
        
        requestMap_post.put('party_id', '63063');
        invokeEmailNotificationPost(requestMap_post); /* 3 valid party(no email), no template */
        
        requestMap_post.put('party_id', '63061');
        invokeEmailNotificationPost(requestMap_post); /* 3i valid party(with email), no template */
        
        requestMap_post.put('template_id', 'INVALID');
        invokeEmailNotificationPost(requestMap_post); /* 4 invalid template */
        
        requestMap_post.put('template_id', 'AD-SCH-SET');
        invokeEmailNotificationPost(requestMap_post); /* 5 valid template, mergeValues is NULL */
        
        requestMap_post.put('merge_values', mergeValueData);
        invokeEmailNotificationPost(requestMap_post); /* 6 mergeValue.unit field empty */
        
        mergeValueData.unit = 'XX/YYYY';
        invokeEmailNotificationPost(requestMap_post); /* 7 mergeValue.auto_debit_schedules NULL */
        
        mergeValueData.auto_debit_schedules = new List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem>();
        invokeEmailNotificationPost(requestMap_post); /* 8 mergeValue.auto_debit_schedules is empty list */
        
        mergeValueData.auto_debit_schedules = adSchedules;
        invokeEmailNotificationPost(requestMap_post); /* 9 mergeValue.auto_debit_schedule is having empty fields */
        
        adSchedules = new List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem>();
        adSched = new HD_SendNotificationByEmail_API.AutoDebitScheduleItem();
        adSched.unit_number = 'XX/YYYY';
        adSched.invoice_number = 'inv_202010_1234_1';
        adSched.due_date = '30 Sep 2020';
        adSched.amount = 'AED 64,000.00';
        adSchedules.add(adSched);
        mergeValueData.auto_debit_schedules = adSchedules;
        invokeEmailNotificationPost(requestMap_post); /* 10 valid for schedule create */
        
        requestMap_post.put('template_id', 'AD-SCH-MOD');
        invokeEmailNotificationPost(requestMap_post); /*11 valid for schedule modify */
        
        Test.stopTest();
    }
    
    @isTest
    static void test_AD_ScheduleAllInvSetupMails(){
        List<HD_SendNotificationByEmail_API.PaymentOption> adPayments = 
            new List<HD_SendNotificationByEmail_API.PaymentOption>();
        List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem> adSchedules = 
            new List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem>();
        HD_SendNotificationByEmail_API.PaymentOption adPayment = 
            new HD_SendNotificationByEmail_API.PaymentOption();
        adPayment.unit_number = 'xx/yyyy';
        adPayment.payment_option = '';
        adPayment.auto_debit_schedules = NULL;
        adPayments.add(adPayment);
        
        HD_SendNotificationByEmail_API.AutoDebitScheduleItem adSched;
        
        HD_SendNotificationByEmail_API.TemplateMergeData mergeValueData;
        mergeValueData = new HD_SendNotificationByEmail_API.TemplateMergeData();
        mergeValueData.customer_name = 'TEST OWNER';
        mergeValueData.unit = 'xx/yyyy';
        mergeValueData.auto_debit_payments = NULL;
        
        Map<String, Object> requestMap_post = new Map<String, Object>();
        requestMap_post.put('party_id', '63061');
        requestMap_post.put('template_id', 'AD-SCH-SET-ALL');
        requestMap_post.put('send_to_list', NULL); 
        requestMap_post.put('cc_list', NULL); 
        requestMap_post.put('bcc_list', 'v-subin@damacgroup.com'); 
        requestMap_post.put('merge_values', NULL);
        
        String reqJSON = JSON.serialize(requestMap_post);
        getNewRestContext_SendEmailPOST(reqJSON);
        
        Test.startTest();
        
        invokeEmailNotificationPost(requestMap_post); /* 1 no mergeValues */
        
        requestMap_post.put('merge_values', mergeValueData);
        invokeEmailNotificationPost(requestMap_post); /* 2 mergeValue.auto_debit_payments NULL */
        
        mergeValueData.auto_debit_payments = new List<HD_SendNotificationByEmail_API.PaymentOption>();
        invokeEmailNotificationPost(requestMap_post); /* 3 mergeValue.auto_debit_payments is empty list */
        
        adPayments = new List<HD_SendNotificationByEmail_API.PaymentOption>();
        adPayment = new HD_SendNotificationByEmail_API.PaymentOption();
        adPayment.unit_number = 'XX/YYYY';
        adPayment.payment_option = '';
        adPayment.auto_debit_schedules = NULL;
        adPayments.add(adPayment);
        mergeValueData.auto_debit_payments = adPayments;
        invokeEmailNotificationPost(requestMap_post); /* 4 mergeValue.auto_debit_payments is having empty fields */
        
        adPayment.payment_option = 'Installment';
        invokeEmailNotificationPost(requestMap_post); /* 15 valid for schedule create_all */
        
        requestMap_post.put('template_id', 'AD-SCH-MOD-ALL');
        invokeEmailNotificationPost(requestMap_post); /*11 valid for schedule modify_all */
        
        adSchedules = new List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem>();
        adSched = new HD_SendNotificationByEmail_API.AutoDebitScheduleItem();
        adSched.unit_number = 'XX/YYYY';
        adSched.invoice_number = 'inv_202010_1234_1';
        adSched.due_date = '30 Sep 2020';
        adSched.amount = 'AED 64,000.00';
        adSchedules.add(adSched);
        adPayment.auto_debit_schedules = adSchedules;
        adPayment.payment_option = 'Custom';
        invokeEmailNotificationPost(requestMap_post); /* 4 column layout for All_Invoice case */
        
        adPayment = new HD_SendNotificationByEmail_API.PaymentOption();
        adPayment.unit_number = 'XX/YYYY';
        adPayment.payment_option = 'Installments';
        adPayment.auto_debit_schedules = NULL;
        adPayments.add(adPayment);
        invokeEmailNotificationPost(requestMap_post); /* 2 column layout for All_Invoice case */
        
        requestMap_post.put('template_id', 'AD-SCH-FAIL');
        invokeEmailNotificationPost(requestMap_post); /* AD setup fail */
        
        Test.stopTest();
    }
    
    @isTest
    static void test_AD_CardUpdateMails(){
        HD_SendNotificationByEmail_API.TemplateMergeData mergeValueData;
        mergeValueData = new HD_SendNotificationByEmail_API.TemplateMergeData();
        mergeValueData.customer_name = 'TEST OWNER';
        mergeValueData.card_old_num = '';
        mergeValueData.card_new_num = '';
        mergeValueData.card_new_upd_dt = '';
        mergeValueData.card_old_exp_dt = '';
        
        Map<String, Object> requestMap_post = new Map<String, Object>();
        requestMap_post.put('party_id', '63061');
        requestMap_post.put('template_id', '');
        requestMap_post.put('send_to_list', NULL); 
        requestMap_post.put('cc_list', NULL); 
        requestMap_post.put('bcc_list', 'v-subin@damacgroup.com');
        requestMap_post.put('merge_values', mergeValueData);
        
        String reqJSON = JSON.serialize(requestMap_post);
        getNewRestContext_SendEmailPOST(reqJSON);
        
        Test.startTest();
        
        requestMap_post.put('template_id', 'AD-CRD-UPD');
        invokeEmailNotificationPost(requestMap_post); /* 1 */
        
        mergeValueData.card_old_num = '123xxxx456';
        mergeValueData.card_new_num = '456xxxx789';
        mergeValueData.card_new_upd_dt = '31 Aug 2020';
        invokeEmailNotificationPost(requestMap_post); /* 2 */
        
        requestMap_post.put('template_id', 'AD-CRD-EXP');
        invokeEmailNotificationPost(requestMap_post); /* 3 */
        
        mergeValueData.card_old_exp_dt = '31 May 2020';
        invokeEmailNotificationPost(requestMap_post); /* 4 */
        
        Test.stopTest();
    }
    
    @isTest
    static void test_AD_TransactionMails(){
        List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem> adSchedules = 
            new List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem>();
        HD_SendNotificationByEmail_API.AutoDebitScheduleItem adSched = 
            new HD_SendNotificationByEmail_API.AutoDebitScheduleItem();
        adSched.unit_number = '';
        adSched.invoice_number = '';
        adSched.due_date = '';
        adSched.amount = '';
        adSched.receipt_url = '';
        
        HD_SendNotificationByEmail_API.TemplateMergeData mergeValueData;
        mergeValueData = new HD_SendNotificationByEmail_API.TemplateMergeData();
        mergeValueData.customer_name = 'TEST OWNER';
        mergeValueData.unit = '';
        mergeValueData.auto_debit_schedules = NULL;
        
        Map<String, Object> requestMap_post = new Map<String, Object>();
        requestMap_post.put('party_id', '63061');
        requestMap_post.put('template_id', '');
        requestMap_post.put('send_to_list', NULL); 
        requestMap_post.put('cc_list', NULL); 
        requestMap_post.put('bcc_list', 'v-subin@damacgroup.com');
        requestMap_post.put('merge_values', mergeValueData);
        
        String reqJSON = JSON.serialize(requestMap_post);
        getNewRestContext_SendEmailPOST(reqJSON);
        
        Test.startTest();
        
        requestMap_post.put('template_id', 'AD-TRX-DUE');
        invokeEmailNotificationPost(requestMap_post); /* 1 */
        
        mergeValueData.unit = 'XX/YYYY';
        invokeEmailNotificationPost(requestMap_post); /* 2 */
        
        mergeValueData.auto_debit_schedules = adSchedules;
        invokeEmailNotificationPost(requestMap_post); /* 3 */
        
        adSchedules.add(adSched);
        invokeEmailNotificationPost(requestMap_post); /* 4 */
        
        adSched.unit_number = 'XX/YYYY';
        adSched.invoice_number = 'inv_202010_1234_1';
        adSched.due_date = '30 Sep 2020';
        adSched.amount = 'AED 64,000.00';
        adSchedules = new List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem>();
        adSchedules.add(adSched);
        mergeValueData.auto_debit_schedules = adSchedules;
        invokeEmailNotificationPost(requestMap_post); /* 4i */
        
        requestMap_post.put('template_id', 'AD-TRX-CHGD');
        invokeEmailNotificationPost(requestMap_post); /* 5 */
        
        requestMap_post.put('template_id', 'AD-TRX-FAIL');
        invokeEmailNotificationPost(requestMap_post); /* 6 */
        
        requestMap_post.put('template_id', 'AD-TRX-SUCC');
        invokeEmailNotificationPost(requestMap_post); /* 7 */
        
        /* set Mock Response for fetch receipt service callouts */
        Test.setMock(HttpCalloutMock.class, new FetchReceiptMock());
        
        adSched.receipt_url = 'https://ptctest.damacgroup.com/hellodamac-dev/api/v1/payments/service-fees/receipts/499128/doc';
        adSchedules = new List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem>();
        adSchedules.add(adSched);
        mergeValueData.auto_debit_schedules = adSchedules;
        invokeEmailNotificationPost(requestMap_post); /* 8 */
        
        /*adSched.receipt_url = 'https://ptctest.damacgroup.com/hellodamac-dev/api/v1/payments/service-fees/receipts/499128/pdf';
        adSchedules = new List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem>();
        adSchedules.add(adSched);
        mergeValueData.auto_debit_schedule = adSched;
        mergeValueData.auto_debit_schedules = adSchedules;
        invokeEmailNotificationPost(requestMap_post);*/ /* 9 */
        
        adSched.receipt_url = 'https://ptctest.damacgroup.com/COFFEE/apex/document/view/db0673faab03546ffe3ddff6da91b06a';
        adSchedules = new List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem>();
        adSchedules.add(adSched);
        mergeValueData.auto_debit_schedules = adSchedules;
        invokeEmailNotificationPost(requestMap_post); /* 9 */
        
        requestMap_post.put('template_id', 'AD-ADHOC-TRX-SUCC');
        invokeEmailNotificationPost(requestMap_post); /* 10 */
        
        Test.stopTest();
    }
    
    @isTest
    static void test_DH_ServiceRequestMails(){
        HD_SendNotificationByEmail_API.TemplateMergeData mergeValueData;
        mergeValueData = new HD_SendNotificationByEmail_API.TemplateMergeData();
        mergeValueData.customer_name = 'TEST OWNER';
        mergeValueData.unit = '';
        mergeValueData.appointment_date = '';
        mergeValueData.appointment_time = '';
        mergeValueData.appointment_rejected_reason = '';
        
        Map<String, Object> requestMap_post = new Map<String, Object>();
        requestMap_post.put('party_id', '63061');
        requestMap_post.put('template_id', '');
        requestMap_post.put('send_to_list', NULL); 
        requestMap_post.put('cc_list', NULL); 
        requestMap_post.put('bcc_list', 'v-subin@damacgroup.com');
        requestMap_post.put('merge_values', mergeValueData);
        
        String reqJSON = JSON.serialize(requestMap_post);
        getNewRestContext_SendEmailPOST(reqJSON);
        
        Test.startTest();
        
        requestMap_post.put('template_id', 'DH-SUB-DOC-SR');
        invokeEmailNotificationPost(requestMap_post); /* 1 */
        
        mergeValueData.unit = 'XX/YYYY';
        invokeEmailNotificationPost(requestMap_post); /* 2 */
        
        requestMap_post.put('template_id', 'DH-CANC-UNT-INS-APPNT');
        invokeEmailNotificationPost(requestMap_post); /* 3 */
        
        requestMap_post.put('template_id', 'DH-UNT-INS-SNG-REPRTD');
        invokeEmailNotificationPost(requestMap_post); /* 4 */
        
        requestMap_post.put('template_id', 'DH-UNT-INS-SNG-RESLVD');
        invokeEmailNotificationPost(requestMap_post); /* 5 */
        
        requestMap_post.put('template_id', 'DH-REQ-HNDOVR-DOCS-SR');
        invokeEmailNotificationPost(requestMap_post); /* 6 */
        
        requestMap_post.put('template_id', 'DH-CANC-SGNF-HNDVR-DOC-APPNT');
        invokeEmailNotificationPost(requestMap_post); /* 7 */
        
        requestMap_post.put('template_id', 'DH-BUK-KEY-HNDOVR-SR');
        invokeEmailNotificationPost(requestMap_post); /* 8 */
        
        requestMap_post.put('template_id', 'DH-CANC-KEY-HNDOVR-APPNT');
        invokeEmailNotificationPost(requestMap_post); /* 9 */
        
        requestMap_post.put('template_id', 'DH-BUK-UNT-INS-APR');
        invokeEmailNotificationPost(requestMap_post); /* 10 */
        
        mergeValueData.appointment_date = '31 May 2020';
        mergeValueData.appointment_time = '11.30 AM';
        invokeEmailNotificationPost(requestMap_post); /* 11 */
        
        requestMap_post.put('template_id', 'DH-REQ-SGNF-HNDVR-DOC-SR-APR');
        invokeEmailNotificationPost(requestMap_post); /* 12 */
        
        requestMap_post.put('template_id', 'DH-BUK-KEY-HNDOVR-SR-APR');
        invokeEmailNotificationPost(requestMap_post); /* 13 */
        
        requestMap_post.put('template_id', 'DH-BUK-UNT-INS-REJ');
        invokeEmailNotificationPost(requestMap_post); /* 14 */
        
        mergeValueData.appointment_rejected_reason = 'of some reason';
        invokeEmailNotificationPost(requestMap_post); /* 15 */
        
        requestMap_post.put('template_id', 'DH-REQ-SGNF-HNDVR-DOC-SR-REJ');
        invokeEmailNotificationPost(requestMap_post); /* 16 */
        
        requestMap_post.put('template_id', 'DH-BUK-KEY-HNDOVR-SR-REJ');
        invokeEmailNotificationPost(requestMap_post); /* 17 */
        
        Test.stopTest();
    }
    
    @isTest
    static void test_DH_TransactionMails(){
        List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem> adSchedules = 
            new List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem>();
        HD_SendNotificationByEmail_API.AutoDebitScheduleItem adSched = 
            new HD_SendNotificationByEmail_API.AutoDebitScheduleItem();
        adSched.unit_number = '';
        adSched.invoice_number = '';
        adSched.due_date = '';
        adSched.amount = '';
        adSched.receipt_url = '';
        
        HD_SendNotificationByEmail_API.TemplateMergeData mergeValueData;
        mergeValueData = new HD_SendNotificationByEmail_API.TemplateMergeData();
        mergeValueData.customer_name = 'TEST OWNER';
        mergeValueData.unit = 'XX/YYYY';
        mergeValueData.auto_debit_schedules = NULL;
        
        Map<String, Object> requestMap_post = new Map<String, Object>();
        requestMap_post.put('party_id', '63061');
        requestMap_post.put('template_id', '');
        requestMap_post.put('send_to_list', NULL); 
        requestMap_post.put('cc_list', NULL); 
        requestMap_post.put('bcc_list', 'v-subin@damacgroup.com');
        requestMap_post.put('merge_values', mergeValueData);
        
        String reqJSON = JSON.serialize(requestMap_post);
        getNewRestContext_SendEmailPOST(reqJSON);
        
        Test.startTest();
        
        requestMap_post.put('template_id', 'DH-FIN-PAY-DUE-2W-REC');
        invokeEmailNotificationPost(requestMap_post); /* 1 */
        
        mergeValueData.auto_debit_schedules = adSchedules;
        invokeEmailNotificationPost(requestMap_post); /* 2 */
        
        adSchedules.add(adSched);
        mergeValueData.auto_debit_schedules = adSchedules;
        invokeEmailNotificationPost(requestMap_post); /* 3 */
        
        adSched.unit_number = 'XX/YYYY';
        adSched.invoice_number = 'inv_202010_1234_1';
        adSched.due_date = '30 Sep 2020';
        adSched.amount = 'AED 64,000.00';
        adSchedules = new List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem>();
        adSchedules.add(adSched);
        mergeValueData.auto_debit_schedules = adSchedules;
        invokeEmailNotificationPost(requestMap_post); /* 3i */
        
        requestMap_post.put('template_id', 'DH-FIN-PAY-DUE-7D-PRE');
        invokeEmailNotificationPost(requestMap_post); /* 4 */
        
        requestMap_post.put('template_id', 'DH-FIN-PAY-TRANS-FAIL');
        invokeEmailNotificationPost(requestMap_post); /* 5 */
        
        requestMap_post.put('template_id', 'DH-FIN-PAY-TRANS-SUCC');
        invokeEmailNotificationPost(requestMap_post); /* 6 */
        
        /* set Mock Response for fetch receipt service callouts */
        Test.setMock(HttpCalloutMock.class, new FetchReceiptMock());
        
        adSched.receipt_url = 'https://ptctest.damacgroup.com/hellodamac-dev/api/v1/payments/service-fees/receipts/499128/doc';
        adSchedules = new List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem>();
        adSchedules.add(adSched);
        mergeValueData.auto_debit_schedules = adSchedules;
        invokeEmailNotificationPost(requestMap_post); /* 7 */
        
        /*adSched.receipt_url = 'https://ptctest.damacgroup.com/hellodamac-dev/api/v1/payments/service-fees/receipts/499128/pdf';
        adSchedules = new List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem>();
        adSchedules.add(adSched);
        mergeValueData.auto_debit_schedule = adSched;
        mergeValueData.auto_debit_schedules = adSchedules;
        invokeEmailNotificationPost(requestMap_post);*/ /* 8 */
        
        adSched.receipt_url = 'https://ptctest.damacgroup.com/COFFEE/apex/document/view/db0673faab03546ffe3ddff6da91b06a';
        adSchedules = new List<HD_SendNotificationByEmail_API.AutoDebitScheduleItem>();
        adSchedules.add(adSched);
        mergeValueData.auto_debit_schedules = adSchedules;
        invokeEmailNotificationPost(requestMap_post); /* 8 */
        
        Test.stopTest();
    }
    
    private static void invokeEmailNotificationPost(Map<String, Object> requestMap_post){
        HD_SendNotificationByEmail_API.doPost(
            (String)requestMap_post.get('party_id'), 
            (String)requestMap_post.get('template_id'), 
            (String)requestMap_post.get('from_address'), 
            (String)requestMap_post.get('send_to_list'),  
            (String)requestMap_post.get('cc_list'),  
            (String)requestMap_post.get('bcc_list'),  
            (HD_SendNotificationByEmail_API.TemplateMergeData)requestMap_post.get('merge_values'));
    }
    
    private static void getNewRestContext_SendEmailPOST(String reqJSON){
        RestRequest request = new RestRequest();
        request.requestUri = baseUrl + '/services/apexrest/sendNotification/email';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(reqJSON);
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
    }
    
    public class SendGridResponseMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req){
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            
            if(req.getBody().containsIgnoreCase('success@mailinator')){
                response.setStatus('Accepted');
                response.setStatusCode(202);
            }
            else {
                response.setStatus('Bad Request');
                response.setStatusCode(400);
                response.setBody('{'+ 
                            '"errors": [{'+ 
                                '"error_id": "3600",'+ 
                                '"field": "to",'+ 
                                '"message": "invalid address",'+ 
                                '"parameter": "invalid@address",'+ 
                            '}]'+ 
                        '}');
            }
            return response;
        }
    } /* class SendGridResponseMock implements HttpCalloutMock */
    
    public class FetchReceiptMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req){
            HttpResponse response = new HttpResponse();
            String endPath = req.getEndPoint();
            
            if(endpath.endsWithIgnoreCase('doc')){
            	response.setHeader('Content-Type', 'application/json');
                response.setStatus('OK');
                response.setStatusCode(200);
                response.setBody('{"data":null,"meta_data":{"title":"Some Exception occurred","message":"JSONObject[\"url\"] not found.","status_code":10,"developer_message":null}}');
            }
            else if(endpath.endsWithIgnoreCase('pdf')) {
            	response.setHeader('Content-Type', 'application/json');
                response.setStatus('OK');
                response.setStatusCode(200);
                response.setBody('{"data":{"url":"https://ptctest.damacgroup.com/COFFEE/apex/document/view/e560b620840838ca69dad7867f1a2fdb"},"meta_data":{"title":"Successful","message":"Success","status_code":1,"developer_message":null}}');
            }
            else {
            	response.setHeader('Content-Type', 'application/pdf');
            	response.setHeader('Content-Disposition', 'inline; filename="sample.pdf"');
                response.setStatus('OK');
                response.setStatusCode(200);
                response.setBodyAsBlob(Blob.toPdf('Hello World !!!'));
            }
            return response;
        }
    } /* class FetchReceiptMock implements HttpCalloutMock */
}