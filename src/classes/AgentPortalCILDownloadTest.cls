/**************************************************************************************************
* Name               : AgentPortalCILDownloadTest
* Description        : Test Class for AgentPortalCILDownload
* Created Date       : 4/09/2017                                                                     
* Created By         : Naresh Kaneriya                                                             
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         Naresh Kaneriya      04/09/2017                                                               
**************************************************************************************************/

@isTest(SeeAlldata=true) 
public class AgentPortalCILDownloadTest{

 public static Contact adminContact;
    public static User portalUser;
    public static Account adminAccount;
    public static User portalOnlyAgent;
    
    static void init(){
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole111');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't6', email='x1@email.com',
                emailencodingkey='UTF-8', lastname='U56', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='x1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {

        adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        insert adminAccount;
        
        adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        adminContact.Owner__c = true;
        adminContact.Agent_Representative__c= true;
        adminContact.Portal_Administrator__c= true;
        insert adminContact;
        
        Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
        insert agentContact;
        
        portalUser = InitialiseTestData.getPortalUser('t311@test.com', adminContact.Id, 'Admin');
        portalOnlyAgent = InitialiseTestData.getPortalUser('t3112@test.com', agentContact.Id, 'Agent');
        
        System.runAs(portalUser){
        Property__c property = InitialiseTestData.insertProperties();
        InitialiseTestData.createInventoryUser(property);
        }
        
         System.runAs(portalUser){
            Inquiry__c CIL = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT,1);
            insert CIL;
        }
        
        System.runAs(portalOnlyAgent){
            Inquiry__c CIL = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT,2);
            insert CIL;
        } 
        
        ApexPages.currentPage().getParameters().put('sfdc.tabName','aX982364');
        }
        
    }
    
    public static testmethod void CILDownloadTest(){
        init();
      
        AgentPortalCILDownload obj = new AgentPortalCILDownload();
        obj.loadCILData();
        obj.customListViewId = 'customListViewId';
          
        // Inner Class
        AgentPortalCILDownload.CustomerDetails objCustomerDetails = new AgentPortalCILDownload.CustomerDetails(); 
        objCustomerDetails.FirstName = 'Test';
        objCustomerDetails.LastName = 'Test';
        objCustomerDetails.OwnerID = 'Test';
        objCustomerDetails.InquiryNo = 'Test';
        objCustomerDetails.CreatedDate = '2018-09-09T00:00:00Z';  
        objCustomerDetails.Comments = 'Test';
        objCustomerDetails.DPOK= true;
        objCustomerDetails.DOCOK= true;
    }
    
       @isTest static void showCILForAdmin(){
        Test.startTest();
        init();
        
        System.runAs(portalUser){
            AgentPortalCILDownload inquiry = new AgentPortalCILDownload();
            inquiry.getloadCILData();
           // system.assert(inquiry.CILLists.size()==2);
        }
        
        Test.stopTest();
    }
    
    @isTest static void showCILForAgent(){
        Test.startTest();
        init();
        
        System.runAs(portalOnlyAgent){
             AgentPortalCILDownload inquiry = new AgentPortalCILDownload();
            inquiry.getloadCILData();
            //system.assert(inquiry.CILLists.size()==1);
        }
        
        Test.stopTest();
    }
        @isTest static void showCILForAgent2(){
        
        
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole12121');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't121', email='x11@email.com',
                emailencodingkey='UTF-8', lastname='U16', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='x1199@mail.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
        
        Test.startTest();
            adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;
            
            adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            adminContact.Owner__c = false;
            adminContact.Agent_Representative__c= true;
            adminContact.Portal_Administrator__c= false;
            insert adminContact;
            
            Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            
            portalUser = InitialiseTestData.getPortalUser('t786@test.com', adminContact.Id, 'Admin');
            portalOnlyAgent = InitialiseTestData.getPortalUser('t78@test.com', agentContact.Id, 'Agent');
            
            System.runAs(portalUser){
            Property__c property = InitialiseTestData.insertProperties();
            InitialiseTestData.createInventoryUser(property);
            }
            
            System.runAs(portalUser){
                Inquiry__c CIL = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT,1);
                insert CIL;
            }
            
            System.runAs(portalOnlyAgent){
                Inquiry__c CIL = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT,2);
                insert CIL;
            } 
            
            ApexPages.currentPage().getParameters().put('sfdc.tabName','aX982364');
            
            System.runAs(portalOnlyAgent){
                AgentPortalCILDownload inquiry = new AgentPortalCILDownload();
                inquiry.getloadCILData();
               // system.assert(inquiry.CILLists.size()==1);
            }
            
            Test.stopTest();
        }
    }
    
}