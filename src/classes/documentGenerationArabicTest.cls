/****************************************************************************************
* Description - Test  Class for documentGenerationArabic
*
* Version            Date            Author                              Description
* 1.0                23/11/17        Naresh Kaneriya (Accely)           Initial Draft
****************************************************************************************/

@isTest
private class documentGenerationArabicTest
{
	@isTest
	static void itShould()
	{   
	    Test.startTest();
	    documentGenerationArabic.SFDCDocumentGenerationHttpSoap11Endpoint  objDocGeneration = new documentGenerationArabic.SFDCDocumentGenerationHttpSoap11Endpoint();
	    docgenerationDtoComArabicXsd.DocGenDTO objDoc ;
	  	SOAPCalloutServiceMock.returnToMe = new Map<String, documentGenerationArabic.DocGenerationResponse_element>();
		documentGenerationArabic.DocGenerationResponse_element obj =  new documentGenerationArabic.DocGenerationResponse_element();
		obj.return_x = 'S';
		SOAPCalloutServiceMock.returnToMe.put('response_x',obj);
		Test.setMock(WebServiceMock .Class, new SOAPCalloutServiceMock());
		
		objDocGeneration.DocGeneration('TemplateName',objDoc);
		Test.stopTest();
	}
}