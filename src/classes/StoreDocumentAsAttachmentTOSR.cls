Global class StoreDocumentAsAttachmentTOSR {
    Global StoreDocumentAsAttachmentTOSR() {
    
    }
    webservice static String uploadAttachment(String docIds,string srRecordId) {
        List<Document> docList = [select AuthorId,Name,ContentType,Body,DeveloperName,FolderId,type,Description from Document where isDeleted = False and id =: docIds];
        if ( docList.size() > 0 ) {
            List<Attachment> attachList = new List<Attachment>();
            for ( Document doc : docList ) {
                Attachment attach = new Attachment();
                attach.body = doc.body;
                attach.ContentType = doc.ContentType;
                attach.Description = doc.Description;
                attach.Name = doc.Name;
                attach.OwnerId = doc.AuthorId;
                attach.parentId = srRecordId;
                attachList.add(attach);
            }
            try {
                insert attachList;
                return 'Success';
            } catch ( Exception e ) {
                return 'fail';
            }
        } else {
            return 'fail';
        }
        
    }
    
}