public with sharing class PaymentResponseController {
    public static final String LINK_NAME = 'Click Here';
    public Boolean showReceipt      { get; set; }
    public Boolean isFlyin          { get; set; }
    public Receipt__c receipt       { get; set; }
    public Boolean error            { get; set; }
    public String errorMessage      { get; set; }
    public String iqNumber          { get; set; }
    public String detailsId         { get; set; }
    public String receiptNo         { get; set; }
    public Map <String, String> parameters;

    public PaymentResponseController () {
        parameters = apexpages.currentpage().getparameters();
        System.Debug ('PaymentResponseController>>>>>> ' + parameters);
        /*generateReciept();*/
    }

    public void generateReciept () {
        
        System.Debug ('generateReciept>>>>>> ' + parameters);
        String responseText, order_Number = '';
        if (parameters != NULL) {
            if (parameters.containsKey ('receiptNo')) {
                receiptNo = parameters.get ('receiptNo');
            }
            if (parameters.containsKey ('orderNo')) {
                order_Number = parameters.get ('orderNo');
            }
            if (parameters.containsKey ('encResp')) {
                responseText = decryptResponse (parameters.get ('encResp'));
            }

            if (Test.isRunningTest ()) {
                responseText = '{"6":"5123452346","merchant_param5":"","merchant_param4":"","merchant_param3":"","billing_name":"MR. Faiz Subsih","merchant_param2":"","status_message":"","merchant_param1":"","response_type":"JSON","billing_city":"test","amount":"20000.0","order_status":"Error","billing_country":"United Arab Emirates","billing_address":"test","discount_value":"0.0","billing_zip":"23142342","delivery_country":"","billing_tel":"369852","failure_message":"test","order_id":"RECP-1204","bank_ref_no":"346853","delivery_address":"","status_code":"null","billing_state":"test","payment_mode":"Credit Card","vault":"N","delivery_state":"","card_holder_name":"test","offer_type":"null","delivery_name":"","offer_code":"null","bank_receipt_no":"810001346853","tracking_id":"107004886797","delivery_city":"","delivery_zip":"","delivery_tel":"","currency":"AED","eci_value":"05","card_name":"MasterCard","billing_email":"test@test.com","mer_amount":"20000.0"}';
                System.Debug ('responseText TEST>>>>>> ' + responseText);
            }
        }

        System.Debug ('order_Number>>>>>> ' + order_Number);
        if (String.isNotBlank(order_Number)) {
            receipt = new Receipt__c ();
            receipt = [ SELECT Name, Amount__c, Bank_Account_Number__c, Bank_Ref_No__c, Card_Amount__c, 
                        Card_Type__c, Payment_Gateway_Ref_No__c, Payment_Status__c, Service_Request__c, 
                        Service_Request__r.Name, Receipt_Type__c, Registration_Number__c, Request_Number__c, 
                        Status__c, Transaction_Date__c, Payment_Response__c, PG_Error_Message__c, 
                        Travel_Details__c, Travel_Details__r.Inquiry_Number__c FROM Receipt__c 
                        WHERE Name = :order_Number 
            ];
            System.Debug ('receipt>>>>>> ' + receipt);
            if (receipt.Travel_Details__c != null) {
                isFlyin = true;
                iqNumber = receipt.Travel_Details__r.Inquiry_Number__c;
                detailsId = receipt.Travel_Details__c;
                System.Debug ('isFlyin>>>>>> ' + isFlyin);
            } else {
                isFlyin = false;
            }

            if (receipt.Payment_Response__c == NULL) {
                if (responseText != NULL && responseText != '') {
                    system.Debug (responseText);
                    if (!responseText.startsWith ('{"')) {
                        responseText = '{"'+responseText;
                    }
                    Map <String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(responseText);
                    Receipt.Payment_Response__c = responseText;
                    try {
                        receipt.PG_Error_Message__c = (String) m.get ('failure_message');
                        receipt.Payment_Status__c = (String) m.get ('status_message');
                        receipt.Status__c = (String) m.get ('order_status');
                        receipt.Bank_Ref_No__c = (String) m.get ('bank_ref_no');
                        receipt.Card_Type__c = (String) m.get ('card_name');
                        receipt.Registration_Number__c = (String) m.get ('tracking_id');
                        receipt.Request_Number__c = (String) m.get ('bank_receipt_no');

                        String headerdata = ApexPages.currentPage().getHeaders().get('Host');
                        System.debug('headerdata>>>>>>>  ' + headerdata);
                        receipt.Payment_Confirmation__c = '<a href = \'https://' + headerdata + '/PaymentStatusUpdate/CC_ThankyouPage?receiptNo='+order_Number+'\''+'>'+LINK_NAME+'</a>';
                        errorMessage = 'Receipt generated Successfully.';

                        if (receipt.Status__c != 'Success') {
                            error = true;
                            errorMessage = (String) m.get ('status_message');
                            if (errorMessage != '' && errorMessage != NULL) {

                            } else { 
                                errorMessage = (String) m.get ('failure_message');
                            }
                        }
                    } catch (Exception e) {
                        error = true;
                        errorMessage = e.getMessage ();+e.getLineNumber ();
                    }
                    System.Debug ('errorMessage>>>>>> ' + errorMessage);
                    System.Debug ('receipt>>>>>> ' + receipt);
                    Update receipt;
                    
                } else {
                    error = true;
                    errorMessage = 'Failed to Update the Receipt details.';
                }
            } else {
                error = true;
                errorMessage = 'Receipt already generated with this Order : '+receipt.Name;
            }
            
            showReceipt = true;
            System.Debug ('showReceipt>>>>>> ' + showReceipt);
        }else if(String.isNotBlank(receiptNo)){
            receipt = new Receipt__c ();

            isFlyin = true;
            showReceipt = true;
            receipt = [ SELECT Name, Amount__c, Bank_Account_Number__c, Bank_Ref_No__c, Card_Amount__c, 
                        Card_Type__c, Payment_Gateway_Ref_No__c, Payment_Status__c, Service_Request__c, 
                        Service_Request__r.Name, Receipt_Type__c, Registration_Number__c, Request_Number__c, 
                        Status__c, Transaction_Date__c, Payment_Response__c, PG_Error_Message__c, 
                        Travel_Details__c, Travel_Details__r.Inquiry_Number__c FROM Receipt__c 
                        WHERE Name = :receiptNo 
            ];
            System.Debug ('receipt>>>>>> ' + receipt);
            iqNumber = receipt.Travel_Details__r.Inquiry_Number__c;
            System.Debug ('iqNumber>>>>>> ' + iqNumber);
            if (receipt.Status__c != 'Success') {
                error = true;
                errorMessage = receipt.Payment_Status__c;
                if (errorMessage != '' && errorMessage != NULL) {

                } else { 
                    errorMessage = receipt.Payment_Status__c;
                }
            }
        }
        
    }

    public String decryptResponse (String response) {
        List<Payment_Gateway_Credentials__mdt> credentialsList = new List<Payment_Gateway_Credentials__mdt>();
        credentialsList = [SELECT Developername, Redirect_URL__c, Access_Code__c, ENC_Key__c, Merchant_ID__c, Label 
                        FROM Payment_Gateway_Credentials__mdt 
                        WHERE Label = 'CCAvenue' LIMIT 1];
        if (!credentialsList.isEmpty()) {
            Blob cryptoKey = Blob.valueOf(credentialsList[0].ENC_Key__c);
            Blob hash = Crypto.generateDigest('MD5', cryptoKey);        
            if (!Test.isRunningTest ()) {
                Blob data = EncodingUtil.convertFromHex(response);
                Blob decryptedText = Crypto.decryptWithManagedIV('AES128', hash, data);
                String responseText = decryptedText.toString();
                return responseText;
            } 
        } 
        return 'text';
    }

}