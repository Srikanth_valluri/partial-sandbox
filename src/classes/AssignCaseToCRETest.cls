@isTest
private class AssignCaseToCRETest {

    /**
     * Method to create registered  VIP account
     */
    public static Account createRegisteredVipAccount(
            String phoneNumber,
            Id primaryCre,
            Id sceondryCre,
            Id tertiaryCre
    ) {
        Account account = new Account(
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId(),
            LastName = 'Test Account',
            Phone = phoneNumber,
            Phone_Country_Code__c = 'United Arab Emirates: 00971',
            Phone_Encrypt__c = phoneNumber,
            Primary_CRE__c = primaryCre,
            Secondary_CRE__c = sceondryCre,
            Tertiary_CRE__c = tertiaryCre,
            AnnualRevenue = 999999999,
            Primary_Language__c = 'English'
        );
        insert account;
        Account acc = [Select Id, Phone FROM Account WHERE Id =: account.Id];
        return account;
    }

    /**
     * Method to create User
     */
    public static User createPropertyConsultantUsers(String creUserName, String extension) {
         User creUser;
        System.runAs(new User(Id = UserInfo.getUserID())) {
         creUser = new User(
            Username = creUserName,
            Extension = extension,
            Alias = 'TestCRE',
            Email = creUserName,
            Emailencodingkey = 'UTF-8',
            Lastname = 'Test CRE',
            Languagelocalekey = 'en_US',
            Localesidkey = 'en_US',
            Profileid = [Select Id from Profile Where Name='Contact Center - CRE'].Id,
            Country = 'United Arab Emirates',
            IsActive = true,
            Timezonesidkey='America/Los_Angeles'
        );
        insert creUser;
        }
        return creUser;
    }

    /**
     * Method to test Primary User is owner of Case
     */
    @isTest static void test_AccountPrimaryCRE() {
        // Implement test code
        User primaryUser = createPropertyConsultantUsers('creTest@primaryUser.com', '7000');
        Account acccountObj = createRegisteredVipAccount('9898989898', primaryUser.Id, null, null);
        System.assert(acccountObj != null);
        System.assert(acccountObj.Id != null);
        System.assert(acccountObj.Phone != null);



        Case caseObj = new Case(Subject = 'Test Case', AccountId = acccountObj.Id, Origin = 'Email', Priority = 'Medium',
                                RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Email').getRecordTypeId());
        insert caseObj;

        Case queryCase = [SELECT Account.Primary_CRE__c,
                                 Account.Secondary_CRE__c,
                                 Account.Tertiary_CRE__c,
                                 Account.Primary_Language__c,
                                 Account.AnnualRevenue,
                                 OwnerId,
                                 Preferred_Language__c ,
                                 Subject
                        FROM Case
                        WHERE Id =: caseObj.Id];

        //System.assertEquals(queryCase.OwnerId, primaryUser.Id);
        //System.assertEquals(queryCase.Preferred_Language__c , 'English');
    }

    /**
     * Method to test Secondary User is owner of Case
     */
    @isTest static void test_AccountSecondaryCRE() {
        // Implement test code
        User secondaryUser = createPropertyConsultantUsers('creTest@primaryUser.com', '7000');
        Account acccountObj = createRegisteredVipAccount('9898989898', null, secondaryUser.Id, null);
        System.assert(acccountObj != null);
        System.assert(acccountObj.Id != null);
        System.assert(acccountObj.Phone != null);



        Case caseObj = new Case(Subject = 'Test Case', AccountId = acccountObj.Id, Origin = 'Email', Priority = 'Medium',
                                RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Email').getRecordTypeId());
        insert caseObj;

        Case queryCase = [SELECT Account.Primary_CRE__c,
                                 Account.Secondary_CRE__c,
                                 Account.Tertiary_CRE__c,
                                 Account.Primary_Language__c,
                                 Account.AnnualRevenue,
                                 OwnerId,
                                 Preferred_Language__c,
                                 Subject
                        FROM Case
                        WHERE Id =: caseObj.Id];

        //System.assertEquals(queryCase.OwnerId, secondaryUser.Id);
        //System.assertEquals(queryCase.Preferred_Language__c , 'English');
    }

    /**
     * Method to test Tertiary User is owner of Case
     */
    @isTest static void test_AccountTertiaryCRE() {
        // Implement test code
        User tertiaryUser = createPropertyConsultantUsers('creTest@primaryUser.com', '7000');
        Account acccountObj = createRegisteredVipAccount('9898989898', null, null, tertiaryUser.Id);
        System.assert(acccountObj != null);
        System.assert(acccountObj.Id != null);
        System.assert(acccountObj.Phone != null);
        


        Case caseObj = new Case(Subject = 'Test Case', AccountId = acccountObj.Id, Origin = 'Email', Priority = 'Medium',
                                RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Email').getRecordTypeId());
        insert caseObj;

        Case queryCase = [SELECT Account.Primary_CRE__c,
                                 Account.Secondary_CRE__c,
                                 Account.Tertiary_CRE__c,
                                 Account.Primary_Language__c,
                                 Account.AnnualRevenue,
                                 OwnerId,
                                 Preferred_Language__c,
                                 Subject
                        FROM Case
                        WHERE Id =: caseObj.Id];

        //System.assertEquals(queryCase.OwnerId, tertiaryUser.Id);
        //System.assertEquals(queryCase.Preferred_Language__c , 'English');
    }


    /**
     * Method to test Queue is owner of Case
     */
    @isTest static void test_AccountQueueCRE() {
        // Implement test code
        Account acccountObj = createRegisteredVipAccount('9898989898', null, null, null);
        System.assert(acccountObj != null);
        System.assert(acccountObj.Id != null);
        System.assert(acccountObj.Phone != null);

        Group testGroup = new Group(Name='Email - Service Request', Type='Queue');
        insert testGroup;

        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
        }

        Case caseObj = new Case(Subject = 'Test Case', AccountId = acccountObj.Id, Origin = 'Email', Priority = 'Medium',
                                RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Email').getRecordTypeId());
        insert caseObj;

        Case queryCase = [SELECT Account.Primary_CRE__c,
                                 Account.Secondary_CRE__c,
                                 Account.Tertiary_CRE__c,
                                 Account.Primary_Language__c,
                                 Account.AnnualRevenue,
                                 OwnerId,
                                 Preferred_Language__c,
                                 Subject
                        FROM Case
                        WHERE Id =: caseObj.Id];

        //System.assertEquals(String.valueOf(queryCase.OwnerId).startsWith('00G'), true);
        //System.assertEquals(queryCase.Preferred_Language__c , 'English');
    }

    @isTest static void test_isVipAccount() {
        User primaryUser = createPropertyConsultantUsers('creTest@primaryUser.com', '7000');
        Account acccountObj = createRegisteredVipAccount('9898989898', primaryUser.Id, null, null);
        Test.startTest();
        AssignCaseToCRE.isVipAccount(acccountObj);
        Test.stopTest();
    }

    @isTest static void test_updateCases() {
        User primaryUser = createPropertyConsultantUsers('creTest@primaryUser.com', '7000');
        Account acccountObj = createRegisteredVipAccount('9898989898', null, null, primaryUser.Id);
        Case caseObj = new Case(Subject = 'Test Case', AccountId = acccountObj.Id, Origin = 'Email', Priority = 'Medium',
                                RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Email').getRecordTypeId());
        insert caseObj;
        Test.startTest();
        AssignCaseToCRE.updateCases(new List<ID> {caseObj.Id});
        Test.stopTest();
    }
}