/*-------------------------------------------------------------------------------------------------
Description: Test class for FM_AGASendNoticeController
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 19-02-2019       | Lochana Rajput   | 1. Added functionality to test FM_AGASendNoticeController
=============================================================================================================================
*/
@isTest
private class FM_AGASendNoticeControllerTest
{
	private static testMethod void testRedirect(){
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        Account objAcc = new Account();
		objAcc = TestDataFactory_CRM.createPersonAccount();
		insert objAcc;
		
		NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;

        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;

        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;
		       
        Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('AGA Process').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Booking_Unit__c=lstBookingUnit[0].id;
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
        fmCaseObj.Status__c='Submitted';
        fmCaseObj.Origin__c='Portal';
        fmCaseObj.Approval_Status__c = 'Pending';
        fmCaseObj.Submit_for_Approval__c = true;
        fmCaseObj.Account__c=objAcc.id;
        fmCaseObj.Current_Approver__c = 'FM Manager__';
        fmCaseObj.Tenant_Email__c = 'test@gmail.com';
		fmCaseObj.Property__c = objProperty.id;
		fmCaseObj.Meeting_Type__c = 'AGA meeting';
        insert fmCaseObj;
		
        Test.startTest();  
		PageReference myVfPage = Page.FM_AGASendNotice;
		Test.setCurrentPage(myVfPage);
		ApexPages.currentPage().getParameters().put('propertyId',fmCaseObj.Property__c);
		ApexPages.currentPage().getParameters().put('retURL','/'+fmCaseObj.Id);
        ApexPages.currentPage().getParameters().put('docType','Notice');
		ApexPages.currentPage().getParameters().put('docLevel','building');
		ApexPages.currentPage().getParameters().put('recipient','Owners');
        ApexPages.StandardController sc = new ApexPages.StandardController(fmCaseObj);
		FM_AGASendNoticeController obj=new FM_AGASendNoticeController(sc);		
		obj.redirect();
        Test.stopTest();
    }

	private static testMethod void testredirectToSendMOM(){
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        Account objAcc = new Account();
		objAcc = TestDataFactory_CRM.createPersonAccount();
		insert objAcc;
		
		NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;

        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;

        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;
		       
        Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('AGA Process').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Booking_Unit__c=lstBookingUnit[0].id;
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
        fmCaseObj.Status__c='Submitted';
        fmCaseObj.Origin__c='Portal';
        fmCaseObj.Approval_Status__c = 'Pending';
        fmCaseObj.Submit_for_Approval__c = true;
        fmCaseObj.Account__c=objAcc.id;
        fmCaseObj.Current_Approver__c = 'FM Manager__';
        fmCaseObj.Tenant_Email__c = 'test@gmail.com';
		fmCaseObj.Property__c = objProperty.id;
		fmCaseObj.Meeting_Type__c = 'AGA meeting';
        insert fmCaseObj;
		
        Test.startTest();  
		PageReference myVfPage = Page.FM_AGASendNotice;
		Test.setCurrentPage(myVfPage);
		ApexPages.currentPage().getParameters().put('propertyId',fmCaseObj.Property__c);
		ApexPages.currentPage().getParameters().put('retURL','/'+fmCaseObj.Id);
        ApexPages.currentPage().getParameters().put('docType','MOM');
		ApexPages.currentPage().getParameters().put('docLevel','building');
		ApexPages.currentPage().getParameters().put('recipient','Owners');
        ApexPages.StandardController sc = new ApexPages.StandardController(fmCaseObj);
		FM_AGASendNoticeController obj=new FM_AGASendNoticeController(sc);		
		obj.redirectToSendMOM();
        Test.stopTest();
    }
}