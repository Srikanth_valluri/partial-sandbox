/**************************************************************************************************
* Name               : InventoryShareRemoveBatchScheduler
* Test Class         : InventoryShareRemoveBatchSchedulerTest 
* Description        : Scheduler class for InventoryShareRemoveBatch class.
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst        11/08/2020      Initial Draft.
**************************************************************************************************/
public with sharing class InventoryShareRemoveBatchScheduler implements Schedulable{

    public void execute(SchedulableContext SC) {
        InventoryShareRemoveBatch batchObject = new InventoryShareRemoveBatch();
        Database.executeBatch(batchObject, 10); 
    }
}