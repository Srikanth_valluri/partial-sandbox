/*
Description: Rest Response Class for KISOSK IPMS to read data from Salesforce             
Developed By: DAMAC IT Team
*/


@RestResource(urlMapping='/payservices/*')
global without sharing class DAMAC_KIOSK_REST_SERVICES{   


    @HttpGet
    global static void doGet(){  
        DAMAC_REST_RESP_UNIT postResp = new DAMAC_REST_RESP_UNIT();
        RestRequest req = RestContext.request;  
        String bookingRef = req.params.get('bref');
        if(!String.isBlank(bookingRef) && bookingRef!= ''){
            RestContext.response.responseBody = Blob.valueOf(bookingDetails(bookingRef));
        }else{
            //RestContext.response.responseBody = Blob.valueOf('Booking Reference Cannot be blank');
            postResp.id = bookingRef;
            postResp.STATUS = 'error';
            postResp.MESSAGE = 'Parameter Missing';
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(postResp));
        }
    
    }
    
    public static string bookingDetails(string srName){ 
        DAMAC_REST_RESP_UNIT postResp = new DAMAC_REST_RESP_UNIT();
        string responseAsJsonStr;    
        list<NSIBPM__Service_Request__c> sr = new list<NSIBPM__Service_Request__c>();  
        list<booking__c> book = new list<booking__c>();
        book = [select id,
                    Name,
                    Total_Token_Amount_AED__c,
                    //Account_Party_Id__c,
                    Account__c,
                    //Account_Name__c,
                    Total_Booking_Amount__c,                    
                        (select id,
                            registration_id__c,
                            Unit_Location__c,
                            Requested_Token_Amount__c,
                            Requested_Price_AED__c,
                            Requested_Price__c,
                            Selling_Price__c 
                            from booking_units__r where Token_Paid__c = false) 
                        from booking__c where Deal_SR__r.Name=:srName and Rejected__c=false ];
        if(book.size() > 0){
            responseAsJsonStr = JSON.serialize(book[0]);        
        }else{
            //responseAsJsonStr = 'Booking reference not found';
            postResp.id = srName;
            postResp.STATUS = 'error';
            postResp.MESSAGE = 'Booking reference not found';
            responseAsJsonStr = JSON.serialize(postResp);
        } 
        return responseAsJsonStr;       
    }


}