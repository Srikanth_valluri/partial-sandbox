global class Async_DLD_Rest implements Queueable, Database.AllowsCallouts {

    public string refId;
    public string agId;
    public Async_DLD_Rest (string agreementRecId, string id){
        agId = agreementRecId;
        refId = id;
    }
    public void execute(QueueableContext context) {        
        POLLForSPAAsync (agId, refId); // Do Callout
    }
        
    public void POLLForSPAAsync (String agreementId, String refId){
        System.Debug (agreementId);
        dld_agreement__c record = [SELECT Request_Reference_Id__c, Agreement_Status__c, Status_Poll_Count__c from dld_agreement__c  where id=:agreementId LIMIT 1];
        System.Debug (record.Request_Reference_Id__c);
        Integration_Settings__mdt dldcreds = [select id,DeveloperName,Endpoint_URL__c,user_name__c,Password__c from Integration_Settings__mdt where DeveloperName='DLD_Settings'];        
        if (record.Agreement_Status__c == 'Reference Id Generated') {
            String key = '';
            if (!Test.isRunningTest ())
                key = DLD_DAMAC_REQUESTS.accessKey(agreementId);
            HttpRequest req = new HttpRequest();
            req.setEndpoint(dldcreds.Endpoint_URL__c+'/requests/'+refId);        
            req.setMethod('GET');        
            req.setheader('Authorization','Bearer '+key);
            req.setheader('channelName','mychannel');
            req.setheader('chaincodeid','RequestManagerChainCode');        
            req.setheader('content-type','application/json');        
            req.setTimeout(120 * 1000);
            
            Http http = new Http();
            HTTPResponse res = new HTTPResponse ();
            if (!Test.isRunningTest ())
                res = http.send(req);
            if (Test.isRunningTest ()) {
                res.setStatusCode (200);
                res.setBody ('');
            }
            System.debug(res);
            System.debug(res.getBody());
            if( res.getstatuscode() == 200){  
                DLD_DAMAC_RESP_OBJ1 respCls = new DLD_DAMAC_RESP_OBJ1();
                if (!test.isRunningTest ()) {
                    DLD_DAMAC_RESP_OBJ dataObj = DLD_DAMAC_RESP_OBJ.parse(string.valueof(res.getBody())); 
                    respCls = DLD_DAMAC_RESP_OBJ1.parse(dataObj.data);                
                }
                if (record.Status_Poll_Count__c == NULL) {
                    record.Status_Poll_Count__c = 1;
                }            
                else
                    record.Status_Poll_Count__c = record.Status_Poll_Count__c + 1;
                DAMAC_Constants.skip_InquiryTrigger = TRUE;
                update record;
                if (!Test.isRunningTest())
                DLD_DAMAC_REQUESTS.saveReqData('OQOOD_STATUS_CHECK',agreementId,null,string.valueof(res.getBody()),refId,respCls.OqoodResponse.issuccess,respCls);
                if (Test.isRunningTest())
                    respcls.status = 'Pending';
                if( respcls.status == 'Pending' || respcls.status == 'PENDING' ) {
                    if (record.Status_Poll_Count__c <= Integer.ValueOf (Label.Maximum_Poll_Count)) {
                        if (!Test.isRunningTest())
                            System.enqueueJob(new Async_DLD_Rest (agreementId, refId));
                    }
                }
            }
        }
    }
}