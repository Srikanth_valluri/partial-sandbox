/*
 * Controller for multi attachment component
 */
 
public with sharing class MultiAttachmentController 
{
    public String contentType {get; set;}
    // the parent object it
    public Id sobjId {get; set;}
    
    public Id parentId {get;set;}
    
    public Id attachmentid{get; set;}
    
    // list of existing attachments - populated on demand
    public List<Attachment> attachments;
    
    // list of new attachments to add
    public List<Attachment> newAttachments {get; set;}
    
    // the number of new attachments to add to the list when the user clicks 'Add More'
    public static final Integer NUM_ATTACHMENTS_TO_ADD=5;

    // constructor
    public MultiAttachmentController() {
        // instantiate the list with a single attachment
        newAttachments=new List<Attachment>{new Attachment()};
    }
    
    // retrieve the existing attachments
    public List<Attachment> getAttachments() {
        // only execute the SOQL if the list hasn't been initialised
        if (null == attachments) {
            attachments=[select Id, ParentId, Name, Description from Attachment where parentId=:sobjId];
        }
        return attachments;
    }


    public PageReference save() {
        List<Attachment> toInsert=new List<Attachment>();
        for (Attachment newAtt : newAttachments) {
            if (newAtt.Body != null) {
                newAtt.parentId=sobjId;
                newAtt.ContentType=contentType;
                toInsert.add(newAtt);
            }
        }
        insert toInsert;
        newAttachments.clear();
        newAttachments.add(new Attachment());
        
        // null the list of existing attachments - this will be rebuilt when the page is refreshed
        attachments=null;
        /*PageReference pageRef = new PageReference('/apex/FlyinDetails?recid='+parentId);
        pageRef.setRedirect(true);
        return pageRef;*/
        PageReference pageRef = Page.FlyinDetails;
        pageref.getParameters().put('recid', parentId);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public Pagereference closeUploadPage() {
        PageReference pageRef = new PageReference('/apex/FlyinDetails?recid='+parentId);
        pageRef.setRedirect(true);
        return pageRef;
    
    
    }



    // Add more attachments action method
    /*public void addMore()
    {
        // append NUM_ATTACHMENTS_TO_ADD to the new attachments list
        for (Integer idx=0; idx<NUM_ATTACHMENTS_TO_ADD; idx++)
        {
            newAttachments.add(new Attachment());
        }
    } */    
    
    // Save action method
    /*public void save()
    {
        List<Attachment> toInsert=new List<Attachment>();
        for (Attachment newAtt : newAttachments)
        {
            if (newAtt.Body!=null)
            {
                newAtt.parentId=sobjId;
                newAtt.ContentType=contentType;
                toInsert.add(newAtt);
            }
        }
        insert toInsert;
        newAttachments.clear();
        newAttachments.add(new Attachment());
        
        // null the list of existing attachments - this will be rebuilt when the page is refreshed
        attachments=null;
    }*/

    // Action method when the user is done
    /*public PageReference done()
    {
        // send the user to the detail page for the sobject
        return new PageReference('/' + sobjId);
    } */
    
    /*public Pagereference deleteatt(){
        //String attid = System.currentPageReference().getParameters().get('attidparam');
        
        if(String.isnotBlank(attachmentid)){
           Attachment att = new Attachment ();
           att.id= attachmentid;
           delete att; 
        }
        PageReference pageRef = new PageReference('/apex/FlyinDetails?recid='+parentId);
        pageRef.setRedirect(true);
 
        return pageRef;
       
    
    } */
    
    //Action to delete the attachment
    /*@RemoteAction
    public static void deleteAttachment(String attaid){
        System.debug('@@@ att id @@@@  '+ attaid);
        Attachment att = new Attachment ();
        att.id= attaid;
        delete att;
    
    } */
    
}