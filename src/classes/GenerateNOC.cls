public with sharing class GenerateNOC {
    
    public GenerateNOC(ApexPages.standardController controller){
      
        
    }//End constructor
    
    public pagereference init(){
        PageReference pr = new PageReference('www.test.com');
        return pr;
    }

    public pagereference mortgageNOC(){
        string strPageID = ApexPages.currentPage().getParameters().get('id');
        list<Attachment> lstAttachment = new list<Attachment>();
        Case currentCase = [Select Id
                                , Mortgage_Status__c
                                , Booking_Unit__c 
                                , Mortgage_Bank_Name__c
                                , Mortgage_Value__c
                                , Mortgage_Start_Date__c
                                , Mortgage_End_Date__c
                            FROM Case Where Id = :strPageID ];
        if((currentCase.Mortgage_Status__c).equalsIgnoreCase('Mortgage Generate NOC') && String.isNotBlank( currentCase.Booking_Unit__c ) ) {
           /* 
           List<Booking_Unit__c> buList = [ SELECT Unit_Location__c 
                                            , Seller_Name__c
                                            , Unit_Location_AR__c
                                            , Plot_Number__c
                                            , Property_Name__c
                                            , Property_Name_Arabic__c
                                            , Unit_Name__c
                                            , Seller_Name_AR__c
                                            , Requested_Price__c
                                            , Registration_ID__c
                                            , Booking__c
                                            FROM Booking_Unit__c 
                                            WHERE Id =: currentCase.Booking_Unit__c ];

            List<Buyer__c> buyerList = [ SELECT Account__r.Org_Account_Name__c 
                                            , Account__r.Person_Account_Name__c
                                            , Account__r.First_Name_Arabic__pc
                                            , Account__r.Middle_Name_Arabic__c 
                                            , Account__r.Last_Name_Arabic__pc
                                            , Account__r.Nationality__pc
                                            , Account__r.Nationality_Arabic__pc
                                            , Account__r.CR_Number__c
                                            , Account__r.Passport_Number__pc
                                            , Account__r.Passport_Number_Arabic__c
                                            , Account__r.Name_Arabic__c
                                            , Account__r.IsPersonAccount
                                            , Primary_Buyer__c
                                        FROM Buyer__c
                                        WHERE Booking__c =: buList[0].Booking__c ];

            Buyer__c primaryBuyer = new Buyer__c();
            Buyer__c jointBuyer = new Buyer__c();
            for( Buyer__c buyerObj : buyerList ){
                if ( buyerObj.Primary_Buyer__c ){
                    primaryBuyer = buyerObj;
                }
                if ( !buyerObj.Primary_Buyer__c ){
                    jointBuyer = buyerObj;
                }
            }

            Integer primaryOwnership = ( buyerList.size() > 1 ) ? 50 : 100;
                                                   
            docgenerationDtoComXsd.DocGenDTO objDocGenDTO = new docgenerationDtoComXsd.DocGenDTO();
            String docUrl ='';
            
            objDocGenDTO.regId = buList[0].Registration_ID__c;
            objDocGenDTO.ATTRIBUTE1 = String.isNotBlank( buList[0].Seller_Name__c ) ? buList[0].Seller_Name__c : '';
            objDocGenDTO.ATTRIBUTE2 = DateTime.newInstance(System.today().year(),System.today().month(),System.today().day()).format('dd-MMM-YYYY');
            objDocGenDTO.ATTRIBUTE3 = '';
            objDocGenDTO.ATTRIBUTE4 = String.isNotBlank( buList[0].Unit_Location__c ) ? buList[0].Unit_Location__c : '';
            objDocGenDTO.ATTRIBUTE5 = String.isNotBlank( buList[0].Unit_Location_AR__c ) ? buList[0].Unit_Location_AR__c : '';
            objDocGenDTO.ATTRIBUTE6 = String.isNotBlank( buList[0].Plot_Number__c ) ? buList[0].Plot_Number__c : '';
            objDocGenDTO.ATTRIBUTE7 = String.isNotBlank( buList[0].Plot_Number__c ) ? buList[0].Plot_Number__c : '';
            objDocGenDTO.ATTRIBUTE8 = String.isNotBlank( buList[0].Property_Name__c ) ? buList[0].Property_Name__c : '';
            objDocGenDTO.ATTRIBUTE9 = String.isNotBlank( buList[0].Property_Name_Arabic__c ) ? buList[0].Property_Name_Arabic__c : '';
            objDocGenDTO.ATTRIBUTE10 =String.isNotBlank(  buList[0].Unit_Name__c ) ? buList[0].Unit_Name__c : '';
            objDocGenDTO.ATTRIBUTE11 = String.isNotBlank( buList[0].Seller_Name_AR__c ) ? buList[0].Seller_Name_AR__c : '';
            objDocGenDTO.ATTRIBUTE12 = buList[0].Requested_Price__c != null ? String.valueOf( buList[0].Requested_Price__c ) : '';
            objDocGenDTO.ATTRIBUTE13 = buList[0].Requested_Price__c != null ? String.valueOf( buList[0].Requested_Price__c ) : '';
            objDocGenDTO.ATTRIBUTE14 = String.isNotBlank( currentCase.Mortgage_Bank_Name__c ) ? currentCase.Mortgage_Bank_Name__c : '';
            objDocGenDTO.ATTRIBUTE15 = String.isNotBlank( currentCase.Mortgage_Bank_Name__c ) ? currentCase.Mortgage_Bank_Name__c : '';
            objDocGenDTO.ATTRIBUTE16 = currentCase.Mortgage_Value__c != null ? String.valueOf( currentCase.Mortgage_Value__c ) : '';
            objDocGenDTO.ATTRIBUTE17 = currentCase.Mortgage_Value__c != null ? String.valueOf( currentCase.Mortgage_Value__c ) : '';
            objDocGenDTO.ATTRIBUTE18 = currentCase.Mortgage_Start_Date__c != null ? DateTime.newInstance(currentCase.Mortgage_Start_Date__c.year(),currentCase.Mortgage_Start_Date__c.month(),currentCase.Mortgage_Start_Date__c.day()).format('dd-MMM-YYYY') : '';
            objDocGenDTO.ATTRIBUTE19 = currentCase.Mortgage_End_Date__c != null ? DateTime.newInstance(currentCase.Mortgage_End_Date__c.year(),currentCase.Mortgage_End_Date__c.month(),currentCase.Mortgage_End_Date__c.day()).format('dd-MMM-YYYY') : '';
            objDocGenDTO.ATTRIBUTE20 = ( !primaryBuyer.Account__r.IsPersonAccount ) 
                                            ? String.isNotBlank( primaryBuyer.Account__r.Org_Account_Name__c ) ? primaryBuyer.Account__r.Org_Account_Name__c : ''
                                            : String.isNotBlank( primaryBuyer.Account__r.Person_Account_Name__c ) ? primaryBuyer.Account__r.Person_Account_Name__c : '';
            objDocGenDTO.ATTRIBUTE21 = ( !primaryBuyer.Account__r.IsPersonAccount ) 
                                            ? String.isNotBlank( primaryBuyer.Account__r.Name_Arabic__c ) ? primaryBuyer.Account__r.Name_Arabic__c : '' 
                                            : String.isNotBlank( primaryBuyer.Account__r.First_Name_Arabic__pc + ' ' + primaryBuyer.Account__r.Middle_Name_Arabic__c + ' ' + primaryBuyer.Account__r.Last_Name_Arabic__pc ) ? primaryBuyer.Account__r.First_Name_Arabic__pc + ' ' + primaryBuyer.Account__r.Middle_Name_Arabic__c + ' ' + primaryBuyer.Account__r.Last_Name_Arabic__pc : '';
            objDocGenDTO.ATTRIBUTE22 = String.isNotBlank( primaryBuyer.Account__r.Nationality__pc ) ? primaryBuyer.Account__r.Nationality__pc : '';
            objDocGenDTO.ATTRIBUTE23 = String.isNotBlank( primaryBuyer.Account__r.Nationality_Arabic__pc ) ? primaryBuyer.Account__r.Nationality_Arabic__pc : '';
            objDocGenDTO.ATTRIBUTE24 = ( !primaryBuyer.Account__r.IsPersonAccount && String.isNotBlank( primaryBuyer.Account__r.CR_Number__c )) ? primaryBuyer.Account__r.CR_Number__c : '';
            objDocGenDTO.ATTRIBUTE25 = ( !primaryBuyer.Account__r.IsPersonAccount && String.isNotBlank( primaryBuyer.Account__r.CR_Number__c )) ? primaryBuyer.Account__r.CR_Number__c : '';
            objDocGenDTO.ATTRIBUTE26 = ( primaryBuyer.Account__r.IsPersonAccount && String.isNotBlank(  primaryBuyer.Account__r.Passport_Number__pc )) ? primaryBuyer.Account__r.Passport_Number__pc : '';
            objDocGenDTO.ATTRIBUTE27 = ( primaryBuyer.Account__r.IsPersonAccount && String.isNotBlank( primaryBuyer.Account__r.Passport_Number_Arabic__c )) ? primaryBuyer.Account__r.Passport_Number_Arabic__c : '';
            objDocGenDTO.ATTRIBUTE28 = String.valueOf( primaryOwnership ) ;
            objDocGenDTO.ATTRIBUTE29 = String.valueOf( primaryOwnership ) ;
            objDocGenDTO.ATTRIBUTE30 = ( !jointBuyer.Account__r.IsPersonAccount ) 
                                        ? String.isNotBlank( jointBuyer.Account__r.Org_Account_Name__c ) ? jointBuyer.Account__r.Org_Account_Name__c : ''
                                        : String.isNotBlank( jointBuyer.Account__r.Person_Account_Name__c ) ? jointBuyer.Account__r.Person_Account_Name__c : '';
            objDocGenDTO.ATTRIBUTE31 = ( !jointBuyer.Account__r.IsPersonAccount ) 
                                        ? String.isNotBlank( jointBuyer.Account__r.Name_Arabic__c ) ? jointBuyer.Account__r.Name_Arabic__c : ''
                                        : String.isNotBlank( jointBuyer.Account__r.First_Name_Arabic__pc + ' ' + jointBuyer.Account__r.Middle_Name_Arabic__c + ' ' + jointBuyer.Account__r.Last_Name_Arabic__pc ) ? jointBuyer.Account__r.First_Name_Arabic__pc + ' ' + jointBuyer.Account__r.Middle_Name_Arabic__c + ' ' + jointBuyer.Account__r.Last_Name_Arabic__pc : '';
            objDocGenDTO.ATTRIBUTE32 = String.isNotBlank( jointBuyer.Account__r.Nationality__pc  )  ? jointBuyer.Account__r.Nationality__pc : '';
            objDocGenDTO.ATTRIBUTE33 = String.isNotBlank( jointBuyer.Account__r.Nationality_Arabic__pc ) ? jointBuyer.Account__r.Nationality_Arabic__pc : '';
            objDocGenDTO.ATTRIBUTE34 = ( !jointBuyer.Account__r.IsPersonAccount && String.isNotBlank( jointBuyer.Account__r.CR_Number__c )) ? jointBuyer.Account__r.CR_Number__c : '';
            objDocGenDTO.ATTRIBUTE35 = ( !jointBuyer.Account__r.IsPersonAccount && String.isNotBlank( jointBuyer.Account__r.CR_Number__c )) ? jointBuyer.Account__r.CR_Number__c : '';
            objDocGenDTO.ATTRIBUTE36 = ( jointBuyer.Account__r.IsPersonAccount &&  String.isNotBlank( jointBuyer.Account__r.Passport_Number__pc )) ? jointBuyer.Account__r.Passport_Number__pc : '';
            objDocGenDTO.ATTRIBUTE37 = ( jointBuyer.Account__r.IsPersonAccount &&  String.isNotBlank( jointBuyer.Account__r.Passport_Number_Arabic__c )) ? jointBuyer.Account__r.Passport_Number_Arabic__c : '';
            objDocGenDTO.ATTRIBUTE38 = String.isNotBlank( jointBuyer.Account__c ) ? String.valueOf( 100 - primaryOwnership ) : '';
            objDocGenDTO.ATTRIBUTE39 = String.isNotBlank( jointBuyer.Account__c ) ? String.valueOf( 100 - primaryOwnership ) : '';
            objDocGenDTO.ATTRIBUTE40 = '';
            objDocGenDTO.ATTRIBUTE41 = '';
            objDocGenDTO.ATTRIBUTE42 = '';
            objDocGenDTO.ATTRIBUTE43 = '';
            objDocGenDTO.ATTRIBUTE44 = '';
            objDocGenDTO.ATTRIBUTE45 = '';
            objDocGenDTO.ATTRIBUTE46 = '';
            objDocGenDTO.ATTRIBUTE47 = '';
            objDocGenDTO.ATTRIBUTE48 = '';
            objDocGenDTO.ATTRIBUTE49 = '';
            objDocGenDTO.ATTRIBUTE50 = '';
            objDocGenDTO.ATTRIBUTE51 = '';
            objDocGenDTO.ATTRIBUTE52 = '';
            objDocGenDTO.ATTRIBUTE53 = '';
            objDocGenDTO.ATTRIBUTE54 = '';
            objDocGenDTO.ATTRIBUTE55 = '';
            objDocGenDTO.ATTRIBUTE56 = '';
            objDocGenDTO.ATTRIBUTE57 = '';
            objDocGenDTO.ATTRIBUTE58 = '';
            objDocGenDTO.ATTRIBUTE59 = '';
            objDocGenDTO.ATTRIBUTE60 = '';
            objDocGenDTO.ATTRIBUTE61 = '';
            objDocGenDTO.ATTRIBUTE62 = '';
            objDocGenDTO.ATTRIBUTE63 = '';
            objDocGenDTO.ATTRIBUTE64 = '';
            objDocGenDTO.ATTRIBUTE65 = '';
            objDocGenDTO.ATTRIBUTE66 = '';
            objDocGenDTO.ATTRIBUTE67 = '';
            objDocGenDTO.ATTRIBUTE68 = '';
            objDocGenDTO.ATTRIBUTE69 = '';
            objDocGenDTO.ATTRIBUTE70 = '';
            objDocGenDTO.ATTRIBUTE71 = '';
            objDocGenDTO.ATTRIBUTE72 = '';
            objDocGenDTO.ATTRIBUTE73 = '';
            objDocGenDTO.ATTRIBUTE74 = '';
            objDocGenDTO.ATTRIBUTE75 = '';
            objDocGenDTO.ATTRIBUTE76 = '';
            objDocGenDTO.ATTRIBUTE77 = '';
            objDocGenDTO.ATTRIBUTE78 = '';
            objDocGenDTO.ATTRIBUTE79 = '';
            objDocGenDTO.ATTRIBUTE80 = '';
            objDocGenDTO.ATTRIBUTE81 = '';
            objDocGenDTO.ATTRIBUTE82 = '';
            objDocGenDTO.ATTRIBUTE83 = '';
            objDocGenDTO.ATTRIBUTE84 = '';
            objDocGenDTO.ATTRIBUTE85 = '';
            objDocGenDTO.ATTRIBUTE86 = '';
            objDocGenDTO.ATTRIBUTE87 = '';
            objDocGenDTO.ATTRIBUTE88 = '';
            objDocGenDTO.ATTRIBUTE89 = '';
            objDocGenDTO.ATTRIBUTE90 = '';
            objDocGenDTO.ATTRIBUTE91 = '';
            objDocGenDTO.ATTRIBUTE92 = '';
            objDocGenDTO.ATTRIBUTE93 = '';
            objDocGenDTO.ATTRIBUTE94 = '';
            objDocGenDTO.ATTRIBUTE95 = '';
            objDocGenDTO.ATTRIBUTE96 = '';
            objDocGenDTO.ATTRIBUTE97 = '';
            objDocGenDTO.ATTRIBUTE98 = '';
            objDocGenDTO.ATTRIBUTE99 = '';
            objDocGenDTO.ATTRIBUTE100 = '';
            objDocGenDTO.ATTRIBUTE101 = '';
            objDocGenDTO.ATTRIBUTE102 = '';
            objDocGenDTO.ATTRIBUTE103 = '';
            objDocGenDTO.ATTRIBUTE104 = '';
            objDocGenDTO.ATTRIBUTE105 = '';
            objDocGenDTO.ATTRIBUTE106 = '';
            objDocGenDTO.ATTRIBUTE107 = '';
            objDocGenDTO.ATTRIBUTE108 = '';
            objDocGenDTO.ATTRIBUTE109 = '';
            objDocGenDTO.ATTRIBUTE110 = '';
            objDocGenDTO.ATTRIBUTE111 = '';
            objDocGenDTO.ATTRIBUTE112 = '';
            objDocGenDTO.ATTRIBUTE113 = '';
            objDocGenDTO.ATTRIBUTE114 = '';
            objDocGenDTO.ATTRIBUTE115 = '';
            objDocGenDTO.ATTRIBUTE116 = '';
            objDocGenDTO.ATTRIBUTE117 = '';
            objDocGenDTO.ATTRIBUTE118 = '';
            objDocGenDTO.ATTRIBUTE119 = '';
            objDocGenDTO.ATTRIBUTE120 = '';

            documentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint docGen = new documentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint();
            docGen.timeout_x = 120000;

            try{
                docUrl = docGen.DocGeneration('Mortgage_NOC_New', objDocGenDTO);
                System.debug('...docUrl.----------------------'+docUrl); 
                PageReference pageRef = new PageReference(docUrl);
                pageRef.setRedirect(true);
                return pageRef;
            } catch(Exception e) {
                system.debug('=====docUrl : ' +e);
                Error_Log__c errorLogObj = new Error_Log__c(Error_Details__c=e.getMessage(),Case__c=currentCase.Id, Process_Name__c='Mortgage');
                insert errorLogObj;
                //docUrl = 'No URL found from IPMS';
                PageReference pageRef = new PageReference('/'+currentCase.Id);
                pageRef.setRedirect(true);
                return pageRef;
            } */

            // Creation of Mortgage NOC Letter through Drawloop
            if(!Test.isRunningTest()) {
                Database.executeBatch(new GenerateDrawloopDocumentBatch(String.valueOf(currentCase.Id),Label.Mortgage_NOC_Letter_DDP_Template_Id,Label.Mortgage_NOC_Letter_DDP_Template_Delivery_Id));
            }
            PageReference objPageReference = new PageReference ('/'+currentCase.Id);
            return objPageReference;
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'You can not Generate NOC at this stage'));
            PageReference pageRef = new PageReference('/apex/Generate_NOC');
            pageRef.setRedirect(true);
            return pageRef;
        }
    }
}