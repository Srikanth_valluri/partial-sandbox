@istest
private class GenerateAreaVariationLetterTest{
    @istest static void areaVariationError(){
       Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;  
        System.assert(Acc != null);
        
         NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
     
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.Total_Snags_Closed__c = 3;
        insert pCas;
        
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.Document_Verified__c = true;
        Cas.Total_Snags_Closed__c = 3;
        Cas.ParentId = pCas.Id;
        insert Cas;
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverMQServices.LetterofDischargeResponse_element>();
        HandoverMQServices.LetterofDischargeResponse_element response1 = new HandoverMQServices.LetterofDischargeResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
 
        PageReference pageRef = Page.GenerateAreaVariation;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateAreaVariationLetter controller = new GenerateAreaVariationLetter(sc);  
       
        pageRef = controller.init();
        Test.stopTest();
    }
    
    @istest static void areaVariationSuccess(){
       Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;  
        System.assert(Acc != null);
        
         NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
     
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.Total_Snags_Closed__c = 3;
        insert pCas;
        
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.Document_Verified__c = true;
        Cas.Total_Snags_Closed__c = 3;
        Cas.ParentId = pCas.Id;
        insert Cas;
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverMQServices.LetterofDischargeResponse_element>();
        HandoverMQServices.LetterofDischargeResponse_element response1 = new HandoverMQServices.LetterofDischargeResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
 
        PageReference pageRef = Page.GenerateAreaVariation;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateAreaVariationLetter controller = new GenerateAreaVariationLetter(sc);  
        
        pageRef = controller.init();
        CallHandoverMQServices.HandoverMQServicesResponse objLOD = new CallHandoverMQServices.HandoverMQServicesResponse();
        objLOD.url = 'www.google.com';
        pageRef = controller.processData(objLOD);
        
        Test.stopTest();
    }
}