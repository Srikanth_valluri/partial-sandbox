public without sharing class FM_NotifyGuestUserController{
    public string recordId;
    public FM_Case__c objFMCase {get;set;}
    
    public FM_NotifyGuestUserController(ApexPages.StandardController sc){
        recordId = sc.getId();
    }
    
    /*public pageReference createSMSHistoryRecord(){
        list<FM_Case__c> lstC = [Select Id
                                      , Name
                                      , Person_To_Collect__c
                                      , Mobile_no_contractor__c
                                      , Mobile_no__c
                                      , Mobile_Country_Code__c
                                 From FM_Case__c
                                 Where Id =: recordId];
        if(!lstC.isEmpty()){
            objFMCase = lstC[0];
            system.debug('*****objFMCase.Person_To_Collect__c*****'+objFMCase.Person_To_Collect__c);
            if(objFMCase.Person_To_Collect__c != null){
                SMS_History__c objSMSHistory = new SMS_History__c();
                
                system.debug('*****objFMCase.Mobile_no_contractor__c*****'+objFMCase.Mobile_no_contractor__c);
                system.debug('*****objFMCase.Mobile_Country_Code__c*****'+objFMCase.Mobile_Country_Code__c);
                system.debug('*****objFMCase.Mobile_no__c*****'+objFMCase.Mobile_no__c);
                
                if(objFMCase.Person_To_Collect__c.equalsIgnoreCase('Contractor')
                && objFMCase.Mobile_no_contractor__c != null && objFMCase.Mobile_Country_Code__c != null){
                    objSMSHistory.Phone_Number__c = concatCountryCode(splitCountryCode(objFMCase.Mobile_Country_Code__c) , objFMCase.Mobile_no_contractor__c);
                }else if(objFMCase.Person_To_Collect__c.equalsIgnoreCase('Consultant')
                && objFMCase.Mobile_no__c != null && objFMCase.Mobile_Country_Code__c != null){
                    objSMSHistory.Phone_Number__c = concatCountryCode(splitCountryCode(objFMCase.Mobile_Country_Code__c) , objFMCase.Mobile_no__c);
                }
                system.debug('*****objSMSHistory.Phone_Number__c*****'+objSMSHistory.Phone_Number__c);
                if(objSMSHistory.Phone_Number__c != null){
                    objFMCase.OTP__c = String.valueOf(Math.random()).substring(8,12);
                    objSMSHistory.Name = 'SMS - '+system.now();
                    objSMSHistory.FM_Case__c = objFMCase.Id;
                    objSMSHistory.Is_SMS_Sent__c = false;
                    objSMSHistory.Message__c = Label.Contractor_Consultant_OTP_Message+' '+objFMCase.OTP__c;
                    update objFMCase;
                    insert objSMSHistory;
                    
                    Pagereference pgRef = new PageReference('/'+recordId);
                    return pgRef;
                }else{
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Phone or Country Code has not been set. You will be unable to proceed.');
                    ApexPages.addMessage(myMsg);
                }
            }else{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Person to collect has not been set. You will be unable to proceed.');
                ApexPages.addMessage(myMsg);
            }
        }
        return null;
    }
    
    public string splitCountryCode(String strCountryWithCode){
        // eg. United Arab Emirates: 00971
        String countryCode = strCountryWithCode.substringAfter(': 00');
        system.debug('*****countryCode*****'+countryCode);
        return countryCode;
    }
    
    public string concatCountryCode(String strCountryCode, String phNo){
        return strCountryCode + phNo;
    }
    
    */
    public void createSMSHistoryRecord(){
        try{
            ApexPages.Message myMsg;
            FM_Case__c insFMCase = FM_Utility.getCaseDetails(recordId);
            System.debug('insFMCase------'+insFMCase);
            if((new Set<String>{'Contractor','Consultant'}).contains(insFMCase.Person_to_collect__c)){
                insFMCase.Is_Contractor_Consultant_Notified__c=true;
                System.debug('insFMCase-----'+insFMCase);
                update insFMCase;
                myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Succesfully notified');
            }else{
                myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Person To Collect Should be Contractor or Consultant');
            }
            ApexPages.addMessage(myMsg);
        }
        catch(DMLException e){
            ApexPages.addMessages(e);
        }
    }
    
    public PageReference redirect(){
        Pagereference pgRef = new PageReference('/'+recordId);
        return pgRef;
    }
}