@isTest
public without sharing class GenerateVATLetterTest  {
    
    @isTest
    public static void testGenerateVAT() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
       
        Location__c loc = new Location__c();
        loc.New_Ho_Doc_Type__c = 'Villa';
        loc.Location_ID__c = '123';
        insert loc;
        Inventory__c inv = new Inventory__c();
        inv.Building_Location__c = loc.id;
        insert inv;
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.Account_Number__c = '85999994';
        insert sr;
            
        Booking__c booking= new Booking__c();
        booking.Account__c = acc.Id;
        booking.Deal_SR__c = sr.Id;
        insert booking;
        
        
        Booking_Unit__c  bookingUnit = new Booking_Unit__c();
        //bookingUnit.Account_Id__c =  acc.Id;
        bookingUnit.Registration_ID__c = '80512';
        bookingUnit.Property_Name__c = 'Damac Hills';
        bookingUnit.Inventory__c = inv.Id;
        bookingUnit.Booking__c = booking.Id;
        insert bookingUnit ;
        
        Test.StartTest(); 

            ApexPages.currentPage().getParameters().put('id', String.valueOf(bookingUnit.Id));
            GenerateVATLetter  vat = new GenerateVATLetter();
            vat.init();
            loc.New_Ho_Doc_Type__c = 'Unit';
            update loc;
            vat.init();
            loc.New_Ho_Doc_Type__c = 'Apartment';
            update loc;
            vat.init();

        Test.StopTest();
        
        
    }
    
}