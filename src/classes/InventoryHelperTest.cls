@istest
public class InventoryHelperTest{



    static testmethod void  InventoryHelper_methods(){
        Test.startTest();
        Inv_Fld_Mapping__c fldmap = new Inv_Fld_Mapping__c();
        fldmap.name = '01';
        fldmap.Is_Property__c = true;
        fldmap.Is_Number_Conversion_Req__c = true;
        fldmap.Field_API_Name__c = 'Property_ID__c';
        fldmap.Inv_Field_API_Name__c = 'Property_ID__c';
        //fldmap
        insert fldmap;        
        
        Property__c  propertyDetail = InitialiseTestData.getPropertyDetails(7650);
        insert propertyDetail ; 
        
        Inventory__c inventory = InitialiseTestData.getInventoryDetails('3456','1234','2345',9086,7650); 
        insert inventory ; 
        
        inventory.Status__c = 'Blocked';
        update inventory ; 
        
        inventory.Status__c = 'Released';
        update inventory ; 
        
        try{
            InventoryHelper.createRec(null);
        }catch(exception ex){
        }
        
        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(inventory);
        Set<Id> invIdSet = new  Set<Id>();
        for(Inventory__c inv: lstInv){
            invIdSet.add(inv.Id);
        }
        Map<Id, String> invOldStatusMap = new Map<Id, String>();
        for(Inventory__c inv: lstInv){
            invOldStatusMap.put(inv.Id, 'Inventory');
        }
        InventoryUpdateSchedulable ascsObject2 = new InventoryUpdateSchedulable(lstInv,invOldStatusMap);
        String sch2 = '0 0 23 * * ?'; 
        system.schedule('Test Scheduler2', sch2, ascsObject2); 
        
        createAttachmentsForPlanScheduler ascsObject3 = new createAttachmentsForPlanScheduler(invIdSet);
        String sch3 = '0 0 23 * * ?'; 
        system.schedule('Test Scheduler3', sch3, ascsObject3); 
            
        Test.stopTest();
    }
}