@isTest 
private Class DeletePreviousTSASharingBatch_test{

    @isTest static void test_method_1() {
        
        
    
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        List<User> userlst = new List<User>();
        

         
        User u = new User(Alias = 'standt', Email='deleteprevioustsa1@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='deleteprevioustsa1@testorg.com');
        userlst.add(u);
        //insert u;
        
        User utwo = new User(Alias = 'standt', Email='deleteprevioustsa2@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='deleteprevioustsa2@testorg.com');
        
        userlst.add(utwo);
        
        insert userlst;    
        
        Inquiry__c inquiryInst = new Inquiry__c(
            First_Name__c = 'Test',
            Last_Name__c = 'Last Test',
            Inquiry_Source__c = 'Call Center',
            Inquiry_Status__c = 'Active',
            Email__c = 'test@gmail.com',
            OwnerId = UserInfo.getuserid(),
            Telesales_Executive__c= u.id,
            Recordtypeid = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId() 
        );
        insert inquiryInst;
        
        Inquiry__Share shareRecord = new Inquiry__Share(ParentId=inquiryInst.id,UserOrGroupId=utwo.id,RowCause='Assign_TS__c',AccessLevel='Edit');
        insert shareRecord;
        
        System.test.startTest();
            DeletePreviousTSASharingBatch batchdelete = new DeletePreviousTSASharingBatch();
            ID batchprocessid = Database.executeBatch(batchdelete,1);
            
    
        System.test.stopTest();



    }


}