@isTest
private class TaskTriggerHandlerSuggestionTest{
    @isTest
    static void itShould(){
        Skip_Task_Trigger_for_Informatica__c objCS =new Skip_Task_Trigger_for_Informatica__c();
        objCS.SetupOwnerId=Userinfo.getUserId();
        objCS.Enable__c = true;
        insert objCS;
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Id LocRecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        Location__c objLocation = new Location__c();
        objLocation.Name = 'ABC';
        objLocation.Location_ID__c = '12345';
        objLocation.RecordTypeId = LocRecordTypeId;
        insert objLocation;
        
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;
         
        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,objLocation);
        fmUser.fm_role__c ='Property Manager';
        insert fmUser;

        FM_User__c fmUser1= TestDataFactoryFM.createFMUser(u,objLocation);
        fmUser1.fm_role__c ='Property Director';
        insert fmUser1;

        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id; 
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
        
        recordType suggestionRT = [SELECT Id,
                      developername
                   FROM RecordType 
                   WHERE SObjectType = 'FM_Case__c' 
                   AND developername ='Suggestion'
                   LIMIT 1];
        system.debug('suggestionRT'+suggestionRT);

        FM_Case__c fmCaseObj=new FM_Case__c();        
        fmCaseObj.Description__c='test';
        fmCaseObj.Suggestion_Type__c = 'Common Area';
        fmCaseObj.Suggestion_Sub_Type__c = 'Reception'; 
        fmCaseObj.recordTypeId = suggestionRT.id;
        fmCaseObj.Booking_Unit__c = BU.id;
        fmCaseObj.Account_Email__c = 'test@t.com';
        fmCaseObj.Status__c = 'Draft Request';
        fmCaseObj.Feedback_to_customer__c ='test';
        fmCaseObj.Request_Type__c = 'Suggestion';
        fmCaseObj.Submission_Datetime__c = system.today();
        fmCaseObj.Internal_comments__c = 'Suggestion';
        fmCaseObj.Feedback_to_customer__c = 'Suggestion';
        //fmCaseObj.Customer_Experience__c = 'Satisfied';
        insert fmCaseObj;
        
        map<Id,Task> newMap = new map<Id,Task>();
        map<Id,Task> oldMap = new map<Id,Task>();
        Task tsk = new Task();
        tsk.WhatId = fmCaseObj.Id;
        tsk.ActivityDate = System.today()+2;
        tsk.Subject = Label.FM_Suggestion_Task_Subject;
        tsk.Status = 'Not Started';
        tsk.Assigned_User__c = 'Property Manager'; 
        tsk.Process_Name__c = 'Suggestion';
        insert tsk ;  
        oldMap.put(tsk.Id,tsk);
        tsk.Status = 'Completed';
        update tsk;
        newMap.put(tsk.Id,tsk);
        TaskTriggerHandlerSuggestion.checkValidInfoOnCase(oldMap,newMap);
    }

    @isTest
    public static void testUMATask() {
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', RecordTypeID=caseRecTypeId, Bank_Name_dtpc__c='test');
        insert caseObj;
        
        insert new Skip_Task_Trigger_for_Informatica__c( SetupOwnerId = UserInfo.getUserID (), Enable__c = true);
        map<Id,Task> newMap = new map<Id,Task>();
        map<Id,Task> oldMap = new map<Id,Task>();
        Task tsk = new Task();
        tsk.WhatId = caseObj.Id;
        tsk.ActivityDate = System.today()+2;
        tsk.Subject = 'Upload, Verify and Execute UMA and HO documents';
        tsk.Status = 'Not Started';
        tsk.Assigned_User__c = 'CRE'; 
        tsk.Process_Name__c = 'Lease Handover';
        insert tsk ;  
        oldMap.put(tsk.Id,tsk);
        tsk.Status = 'Completed';
        Test.startTest();
        update tsk;
        
        newMap.put(tsk.Id,tsk);
        
        System.debug('oldMap=='+ oldMap);
        System.debug('newMap=='+ newMap);
        //TaskTriggerHandlerSuggestion.checkValidInfoOnCase(newMap,oldMap);
        Test.stopTest();
    }
    @isTest
    public static void testConditionalSendEmailTask() {
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', RecordTypeID=caseRecTypeId, Discount_Type__c ='Conditional');
        insert caseObj;
        
        insert new SR_Attachments__c( Case__c = caseObj.Id, Name = 'Signed Advance Payment Rebate Letter');

        insert new Skip_Task_Trigger_for_Informatica__c( SetupOwnerId = UserInfo.getUserID (), Enable__c = true);

        Task tsk = new Task();
        tsk.WhatId = caseObj.Id;
        tsk.ActivityDate = System.today()+2;
        tsk.Subject = 'Send Email';
        tsk.Status = 'Not Started';
        tsk.Assigned_User__c = 'CRE'; 
        tsk.Process_Name__c = 'Rebate On Advance';
        insert tsk ;  

        tsk.Status = 'Completed';
        Test.startTest();
        try {
            update tsk;
        }
        catch( Exception ex) {
            system.assert( ex.getMessage().contains( 'Please upload Signed Advance Payment Letter.' ) );
        }
        Test.stopTest();
    }

    @isTest
    public static void testUnconditionalSendEmailTask() {
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', RecordTypeID=caseRecTypeId, Discount_Type__c ='Unconditional');
        insert caseObj;
        
        insert new SR_Attachments__c( Case__c = caseObj.Id, Name = 'Signed Advance Payment Rebate Letter');

        insert new Skip_Task_Trigger_for_Informatica__c( SetupOwnerId = UserInfo.getUserID (), Enable__c = true);

        Task tsk = new Task();
        tsk.WhatId = caseObj.Id;
        tsk.ActivityDate = System.today()+2;
        tsk.Subject = 'Send Email';
        tsk.Status = 'Not Started';
        tsk.Assigned_User__c = 'CRE'; 
        tsk.Process_Name__c = 'Rebate On Advance';
        insert tsk ;  

        tsk.Status = 'Completed';
        Test.startTest();
        try {
            update tsk;
        }
        catch( Exception ex) {
            system.assert( ex.getMessage().contains( 'Please send email before closing task!') );
        }
        
        Test.stopTest();
    }

    @isTest
    public static void testMortgageask1() {
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', RecordTypeID=caseRecTypeId, Bank_Name_dtpc__c='test');
        insert caseObj;
        
        insert new Skip_Task_Trigger_for_Informatica__c( SetupOwnerId = UserInfo.getUserID (), Enable__c = true);
        
        Task tsk = new Task();
        tsk.WhatId = caseObj.Id;
        tsk.ActivityDate = System.today()+2;
        tsk.Subject = label.Mortgage_10;
        tsk.Status = 'Not Started';
        tsk.Assigned_User__c = 'CRE'; 
        tsk.Process_Name__c = 'Mortgage';
        insert tsk ; 

        tsk.Status = 'Completed';
        Test.startTest();
        try {
            update tsk;
        }
        catch( Exception ex) {}
        Test.stopTest();
    }
    @isTest
    public static void testMortgageask2() {
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Mortgage').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', RecordTypeID=caseRecTypeId, Bank_Name_dtpc__c='test');
        insert caseObj;
        
        insert new Skip_Task_Trigger_for_Informatica__c( SetupOwnerId = UserInfo.getUserID (), Enable__c = true);
        
        Task tsk = new Task();
        tsk.WhatId = caseObj.Id;
        tsk.ActivityDate = System.today()+2;
        tsk.Subject = label.Mortgage_10_1;
        tsk.Status = 'Not Started';
        tsk.Assigned_User__c = 'CRE'; 
        tsk.Process_Name__c = 'Mortgage';
        insert tsk ; 

        tsk.Status = 'Completed';
        Test.startTest();
        try {
            update tsk;
        }
        catch( Exception ex) {}
        Test.stopTest();
    }
}