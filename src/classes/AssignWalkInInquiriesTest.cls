/****************************************************************************************************
* Name          : AssignWalkInInquiriesTest                                                         *
* Description   :  Test class for AssignWalkInInquiries class                                       *
* Created Date  : 13/05/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER     AUTHOR            DATE            COMMENTS                                                *
* 1.0     Craig Lobo        13/05/2018      Initial Draft.                                          *
****************************************************************************************************/
@isTest 
public class AssignWalkInInquiriesTest {
    
    @isTest static void testMethod1 (){

        Profile receptionistProfileObj = [SELECT Id FROM Profile WHERE Name = 'Receptionist' LIMIT 1]; 
        User receptionistUser = new User(
            Alias = 'stand1', 
            Email = 'receptionistsf@testorg1.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = receptionistProfileObj.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'receptionistsf@testorg1.com',
            Sales_Office__c = 'AKOYA'
        );
        insert receptionistUser;

        Profile profileObj = [SELECT Id FROM Profile WHERE Name = 'Property Consultant' LIMIT 1]; 
        User userObj1 = new User(
            Alias = 'stand', 
            Email = 'user@testorg1.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObj.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfd@testorg1.com',
            Sales_Office__c = 'AKOYA'
        );
        insert userObj1;

        User userObj2 = new User(
            Alias = 'stand', 
            Email = 'user@testorg2.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObj.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfd@testorg2.com',
            Sales_Office__c = 'AKOYA'
        );
        insert userObj2;

        Walk_In_Schedule__c instance1 = new Walk_In_Schedule__c(
            User__c = userObj1.id,
            Order__c = 1,
            Week_End_Day__c = System.today().addDays(2),
            Previous_RM__c = true,
            Week_Start_Day__c = System.now().addDays(-2),
            Available_On_Friday__c = true,
            Available_On_Saturday__c = true
        );
        insert instance1;

        Walk_In_Schedule__c instance2 = new Walk_In_Schedule__c(
            User__c = userObj2.id,
            Order__c = 2,
            Week_End_Day__c = System.today().addDays(2),
            Previous_RM__c = false,
            Week_Start_Day__c = System.now().addDays(-2),
            Available_On_Friday__c = true,
            Available_On_Saturday__c = true
        );
        insert instance2;

        Inquiry__c newInquiry = new Inquiry__c(
            Inquiry_Source__c = 'Walk in'
        );
        
        List<Inquiry__c> inqList = new List<Inquiry__c>();
        inqList.add(newInquiry);

        System.runAs(receptionistUser) {
            System.Test.startTest();
            AssignWalkInInquiries q = new AssignWalkInInquiries();
            q.assignWalkInInquiriesToRM(inqList);
            System.Test.stopTest();
        }

    }
}