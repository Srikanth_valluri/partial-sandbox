/**
 * @File Name          : SendSMSAccountService.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 1/30/2020, 12:25:39 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/20/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public with sharing class SendSMSAccountService {
    
    @future (callout=true)
    public static void Sendtextmessage(list<String> lstPhoneNumbers, String MsgContent, String accountId,Boolean isFMSMS, String strAbndId ){
        System.debug('callingId:::::::::::^&%^&%$&^'+accountId);
        
        /*Accoi callObj =  [SELECT Id,
                          Name,
                          Account__c,
                          Account__r.Id,
                          Customer_Name__c 
                          FROM Calling_List__c
                          WHERE Id = :Id.valueOf(callingId)];
        System.debug('callObj:::&&&&&'+callObj);*/
        Id profileId = userinfo.getProfileId();
        String profileName = [Select Id,Name from Profile where Id=:profileId].Name;
        System.debug('profileName == ' + profileName);


    List<SMS_History__c>smsLst = new List<SMS_History__c>();
        for(String PhoneNumber : lstPhoneNumbers){
          System.debug('PhoneNumber::'+PhoneNumber);
          if(string.isBlank(PhoneNumber))
              continue;
          Id caseId = null;
          if(PhoneNumber.contains(':')){
             list<String> lstValues = PhoneNumber.split(':');
             PhoneNumber = lstValues[1];
             caseId = lstValues[0];
          }
          if(PhoneNumber.startsWith('00')) {
              PhoneNumber = PhoneNumber.removeStart('00');
          }
          else if(PhoneNumber.startsWith('0')) {
              PhoneNumber = PhoneNumber.removeStart('0');
          }
          system.debug('PhoneNumber : '+PhoneNumber);
          HttpRequest req = new HttpRequest();
          HttpResponse res = new HttpResponse();
          Http http = new Http();
          
          //String user= 'salesforce';
          //String passwd= 'D@$al3sF0rc387!';
          
            String user = '';
            String passwd = '';
            String strSID = '';

            if(profileName.contains('loams') || isFMSMS == true ) {
                user = Label.FM_SMS_Service_Username; //'loams.';
                passwd =  Label.FM_SMS_Service_Password; //'1@#$%qwert';
                //strSID =  Label.FM_SMS_Service_SenderId; //'LOAMS';
                strSID = SMSClass.getSenderName(user, PhoneNumber, false);
            }
            else if(profileName.contains('Collection') ) {
                user = Label.SF_SMS_Service_User_name;  //'salesforce';
                passwd = Label.SF_SMS_Service_Password; //'D@$al3sF0rc387!';
                //strSID = Label.SF_SMS_Service_Sender_Id; //'DAMAC GRP';
                strSID = SMSClass.getSenderName(user, PhoneNumber, false);
            }
            else {
                user = Label.Damac_CRM_SMS_Service_User_name;//'damaccrm';
                passwd = Label.Damac_CRM_SMS_Service_Password;//'D@$al3sF0rc387!';
                //strSID = Label.Damac_CRM_SMS_Service_Sender_Id;//'DAMAC GRP';
                strSID = SMSClass.getSenderName(user, PhoneNumber, false);
            }
            /*
            if( isFMSMS == true ) {
                user = 'loams.';
                passwd = '1@#$%qwert';
                strSID = 'LOAMS';
            }else {
                user = 'salesforce';
                passwd = 'D@$al3sF0rc387!';
                strSID = 'DAMAC GRP';               
            }  */        
          
          req.setMethod('POST' ); // Method Type
          req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx'); 
          //string msgCont = MsgContent;
          string msgCont = GenericUtility.encodeChar(MsgContent);
          System.debug('msgCont:'+msgCont);
           req.setBody('user='+ user + '&passwd=' + passwd +'&message=' + msgCont + '&mobilenumber=' + PhoneNumber + '&sid='+strSID+ '&MTYPE=LNG&DR=Y'); // Request Parameters
          try {
              system.debug('Request Body---'+req.getBody());
              system.debug('Request Header---'+req.getHeader('Authorization'));
              system.debug('Request Header---'+req.getHeader('Content-Type'));
              res = http.send(req);
              system.debug('Response---'+res.getBody());
              if(res.getBody() != null){
                  // Parse Response
                  //if('SMS message(s) sent' == res.getBody()){
                    if(String.valueOf(res.getBody()).contains('OK:')){
                        system.debug('Message Send Sucessfully');
                        SMS_History__c smsObj = new SMS_History__c();
                        smsObj.Message__c = MsgContent;
                        smsObj.Phone_Number__c = PhoneNumber;
                        if(accountId!=''){
                            smsObj.Customer__c = accountId;
                        }                       
                        if(strAbndId !='' && strAbndId.startsWith('a40')){
                            smsObj.FM_Case__c = strAbndId;
                        }
                        else if(strAbndId !=''){
                             smsObj.Abandoned_Calls__c = strAbndId;
                        }
                        smsObj.Is_SMS_Sent__c = true;
                        if( isFMSMS == true ) {
                            smsObj.isFMSMS__c = true;
                          }else {
                            smsObj.isFMSMS__c = false;
                          }
                        smsObj.sms_Id__c = res.getBody().substringAfter('OK:');
                        smsObj.Description__c = res.getBody();
                        System.debug('smsObj::::if:'+smsObj);
                        smsLst.add(smsObj);
                  }
                  else{
                    //if failure
                      SMS_History__c smsObj = new SMS_History__c();
                        smsObj.Message__c = MsgContent;
                        smsObj.Phone_Number__c = PhoneNumber;
                        smsObj.Customer__c = accountId;
                        if( isFMSMS == true ) {
                            smsObj.isFMSMS__c = true;
                          }else {
                            smsObj.isFMSMS__c = false;
                          }
                        //smsObj.Calling_List__c = callObj.Id;
                        smsObj.Description__c = res.getBody();
                        System.debug('smsObj::::else:'+smsObj);
                        smsLst.add(smsObj);
                  }
              }
          } catch(Exception e) {
              System.debug('Error***** '+ e);
          }
        }
        if(!smsLst.isEmpty()){
            try{ 
                upsert smsLst;
            }catch(exception ex){
                System.debug('DML Exception*****: '+ ex);
            }
        }
    }
    
}