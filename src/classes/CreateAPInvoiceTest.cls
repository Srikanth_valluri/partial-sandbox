@isTest
public class CreateAPInvoiceTest {

    private class Mock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            
            HTTPResponse res = new HTTPResponse();
            res.setBody('{ "OutputParameters": { "@xmlns": " http://xmlns.oracle.com/apps/ont/rest/XXDC_PROCESS_SERVICE_WS/process/", "@xmlns:xsi": " http://www.w3.org/2001/XMLSchema-instance", "X_RESPONSE_MESSAGE": { "X_RESPONSE_MESSAGE_ITEM": [ { "PARAM_ID": "58355", "PROC_STATUS": "S", "PROC_MESSAGE": "Invoice Created Successfully with Voucher Number : 10306052", "ATTRIBUTE1": "3619052", "ATTRIBUTE2": null, "ATTRIBUTE3": null, "ATTRIBUTE4": null } ] }, "X_RETURN_STATUS": "S", "X_RETURN_MESSAGE": "Process Completed Returning 1 Response Message(s)..." } }');
            res.setStatusCode(200);
            return res;
            
        }
    }
    
    @isTest
    static void  testSendTask(){
        
        
        Account objAccount = new Account();
        objAccount.Name ='test';
        objAccount.Party_ID__c = '12345';
        insert objAccount;
        
        Case objCase = new Case();
        objCase.Waiver_Amount_AED__c = 100;
        objCase.Excess_Amount__c = 100;
        insert objCase;
        
        SR_Booking_Unit__c objSR = new SR_Booking_Unit__c();
        objSR.Case__c = objCase.Id;
        insert objSR;
        
        Task objTask = new Task();
        objTask.Subject = 'Validate Service Charge Waiver To Auto Apply Invoice';
        objTask.WhatId = objCase.Id;
        objTask.Type = 'Case';
        objTask.Process_Name__c = System.Label.Service_Charge_Waiver;
        insert objTask;
        
        Task objTask1 = new Task();
        objTask1.Subject = 'Validate Service Charge Reset To Auto Apply Invoice';
        objTask1.WhatId = objCase.Id;
        objTask1.Type = 'Case';
        objTask.Process_Name__c = System.Label.Service_Charge_Reset;
        insert objTask1;

        Task objTaskHandover = new Task();
        objTaskHandover.Subject = System.Label.Validate_Excess_Fund_Transfer;
        objTaskHandover.WhatId = objCase.Id;
        objTaskHandover.Type = 'Case';
        objTaskHandover.Process_Name__c = 'Handover';
        insert objTaskHandover;

        Task objTaskFundTransfer = new Task();
        objTaskFundTransfer.Subject = System.Label.Validate_Excess_Fund_Transfer;
        objTaskFundTransfer.WhatId = objCase.Id;
        objTaskFundTransfer.Type = 'Case';
        objTaskFundTransfer.Process_Name__c = 'Fund Transfer Active Units';
        insert objTaskFundTransfer;
        
        List<Task> taskList = new List<Task>();
        taskList.add(objTask);
        taskList.add(objTask1);
        taskList.add(objTaskHandover);
        taskList.add(objTaskFundTransfer);
        
        Credentials_Details__c objCred = new Credentials_Details__c();
        objCred.Name = 'AP Invoice API';
        objCred.User_Name__c = 'oracle_user';
        objCred.Password__c = 'crp1user';
        objCred.Endpoint__c = 'http://83.111.194.181:8033/webservices/rest/XXDC_PROCESS_SERVICE_WS/process/';
        insert objCred;
        
        Test.setMock(HttpCalloutMock.class, new Mock());
        Test.startTest();
        	CreateAPInvoice.sendTasks(taskList);
        Test.stopTest();
    }
    
    
}