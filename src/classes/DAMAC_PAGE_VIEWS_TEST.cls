@istest
public class DAMAC_PAGE_VIEWS_TEST{


    static testmethod void testPg(){
        
        log__c l = new log__c();
        insert l;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(l);
        DAMAC_PAGE_VIEWS cls = new DAMAC_PAGE_VIEWS(sc);
        
        PageReference pageRef = Page.Buyer_c_Tracker;
        pageRef.getParameters().put('id', String.valueOf(l.Id));
        Test.setCurrentPage(pageRef);
        cls.updateCount();
        
        Page_View__c p = new Page_View__c();
        p.Unique_Key__c = userinfo.getuserid()+'-'+l.id+'-'+system.today().day()+'/'+system.today().Month()+'/'+system.today().year();
        upsert p Unique_Key__c ;
        cls.updateCount();
    
    }


}