Global without sharing class Damac_ResetInquiry implements Database.Batchable<sObject> {

    Set<ID> inquiryIds = new Set<ID>();
    Boolean createNewCSV = false;
    ID ruleId;
    global Damac_ResetInquiry (Set<ID> inqIds, ID assignmentRuleId, Boolean csvCreation) {
        inquiryIds = inqIds;
        createNewCSV = csvCreation;
        ruleId = assignmentRuleId;
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.Debug(inquiryIds);
        Id reAssignChildRecordTypeId = Schema.SObjectType.Inquiry_Assignment_Rules__c.getRecordTypeInfosByDeveloperName().get('Reassign_Child').getRecordTypeId();
        String query = 'SELECT name, mobile_phone__c, full_name__c, inquiry_status__c, ameyo_dialing_priority_attribute__c, campaign_name_text__c, '
                    +'inquiry_source__c, mobile_countrycode__c, encrypted_mobile__c, ownerid, owner.name, ';
        
        for (Inquiry_Assignment_Rules__c metaData: [SELECT Parameter__c, Value__c FROM Inquiry_Assignment_Rules__c WHERE Parent_Reassign_Rule__c =: ruleId AND RecordTypeId =: reAssignChildRecordTypeId ]) {
            if (!query.contains(metaData.Parameter__c.toLowerCase()))
                query += metaData.Parameter__c+',';
        }
        query = query.removeEnd(',').removeEnd(', ')+' FROM Inquiry__c WHERE ID IN: inquiryIds ';
        
        System.Debug(query);
        return Database.getQueryLocator(query);
    
    }
    global void execute(Database.BatchableContext BC, List<Inquiry__c> inqList) {

        Map<String, Object> valuesToReset = new Map<String, Object>();
        Id reAssignChildRecordTypeId = Schema.SObjectType.Inquiry_Assignment_Rules__c.getRecordTypeInfosByDeveloperName().get('Reassign_Child').getRecordTypeId();
        for (Inquiry_Assignment_Rules__c metaData: [SELECT Parameter__c, Value__c FROM Inquiry_Assignment_Rules__c WHERE Parent_Reassign_Rule__c =: ruleId AND RecordTypeId =: reAssignChildRecordTypeId ]) {
            valuesToReset.put(metaData.Parameter__c, metaData.Value__c);            
        }
        List<Inquiry__c> inqToUpdate = new List<Inquiry__c> ();
        for (Inquiry__c inq: inqList) {
            
            for (String field : valuesToReset.keySet()) {
                System.Debug(field+'==='+valuesToReset.get(field));
                if (valuesToReset.get(field) != NULL && valuesToReset.get(field) != '') {
                    String value = String.valueOf(valuesToReset.get(field));
                    if (value.toLowerCase() == 'true')
                        valuesToReset.put (field, TRUE);
                    
                    if (value.toLowerCase() == 'false')
                        valuesToReset.put (field, FALSE);
                }
                inq.put(field, valuesToReset.get(field));
            }
            if (inq.Mobile_Phone__c != NULL)
                inq.Encrypted_Mobile__c = Damac_PhoneEncrypt.encryptPhoneNumber(inq.Mobile_Phone__c);
                inq.Inquiry_Assignment_Rules__c= ruleId;
            inqToUpdate.add(inq);
        }
        DAMAC_Constants.skip_InquiryTrigger = true;
        Update inqToUpdate;
        
        if(createNewCSV) {
            //Create CSV File
            
            Attachment att = new Attachment ();
            try {
                att = [SELECT Body,Name,ParentId FROM Attachment WHERE ParentId =: ruleId AND Name = 'InquiryFields.csv' LIMIT 1];           
            } catch (Exception e) {}
            
            
            String body = '';
            if (att.Id == NULL) {            
                body += 'Id,Name,attribute1, attribute2,campaign_name_text,inquiry_source,mobilecountrycode1, mobilecountrycode2, phone1, OwnerId, OwnerName, wrapupurl';
            } else {
                body = att.Body.toString();
            }
            
            for (Inquiry__c inq: [SELECT Name, Full_Name__c, Inquiry_Status__c, Ameyo_Dialing_Priority_Attribute__c, campaign_name_text__c,
            Inquiry_Source__c, Mobile_countrycode__c, Encrypted_mobile__c, OwnerId, Owner.Name FROm Inquiry__c WHERE ID IN: inqToUpdate]) {
                String mobileCountryCode2 = inq.Mobile_countrycode__c != NULL ? inq.Mobile_countrycode__c.split (': 00')[1] : '';

                body += '\n'+inq.Id+','+inq.Name+'-'+inq.Full_Name__c+','+inq.Inquiry_Status__c+','+inq.Ameyo_Dialing_Priority_Attribute__c
                            +','+inq.campaign_name_text__c+','+inq.Inquiry_Source__c+','+inq.Mobile_countrycode__c+','+mobileCountryCode2 
                            +','+inq.Encrypted_mobile__c+','+inq.OwnerId+','+inq.Owner.Name+','+inq.Id;
                
                body = body.replace('null', '');
            }   
            
            att.Name = 'InquiryFields.csv';
            att.Body = Blob.valueOf(body);  
            if (att.id == NULL) {
                att.parentId = ruleId;
                insert att;
            } else {
                update att;
            }
        }  
        
        Inquiry_Assignment_Rules__c rule = new Inquiry_Assignment_Rules__c();
        rule.Id = ruleId;
        rule.Rule_Running_Status__c = 'Processing';
        update rule;      
        
    }
    global void finish(Database.BatchableContext BC) {
    
        AsyncApexJob jobInfo = [SELECT Status, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE Id = :bc.getJobID()];
        
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] { UserInfo.getUserID() };
        message.optOutPolicy = 'FILTER';
        message.subject = 'Inquiry Reset Job Status';
        message.SetHtmlBody('Hi,<br/>Inquiry fields reset job is completed with '+(jobInfo.TotalJobItems-jobInfo.NumberOfErrors)
                                +' Success and '+jobInfo.NumberOfErrors+' Failures.<br/><br/>Thanks,<br/>Damac Group.');
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
        Inquiry_Assignment_Rules__c rule = new Inquiry_Assignment_Rules__c();
        rule.Id = ruleId;
        rule.Rule_Running_Status__c = 'Completed';
        rule.Success__c = jobInfo.TotalJobItems-jobInfo.NumberOfErrors;
        rule.Errors__c = jobInfo.NumberOfErrors;
        rule.Rule_Executed_Date__c = System.Today();
        update rule;
    }
}