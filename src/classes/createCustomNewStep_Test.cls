@isTest
public class createCustomNewStep_Test {
    
    @isTest
    static void testCase1(){
        test.startTest();
        
        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c serviceReq = InitializeSRDataTest.getSerReq('Deal', true, null);
        serviceReq.RecordTypeId = DealRT;
        insert serviceReq;
        
        Booking__c booking = new Booking__c();
        booking.Deal_SR__c = serviceReq.Id;
        insert booking;
        
        Booking_Unit__c bookingUnit = new Booking_Unit__c();
        bookingUnit.booking__c = booking.id;
        insert bookingUnit;
        
        List<Id> serviceReqIds = new list<id>();
        serviceReqIds.add(serviceReq.Id);
        createCustomNewStep.createNewStep(serviceReqIds);
        
        test.stopTest();
    }
    
}