public without sharing class FmcMaintenanceRequestController extends MaintenanceRequestController {
    public list<SelectOption> lstUnit {get; set;}
    //String caseId {get;set;}

    public FmcMaintenanceRequestController() {
        super(false);
        if (FmcUtils.isCurrentView('HelpdeskRequest')) {
            //super();
            showBuildings = false;
            mapId_Code = new map<String,String>();
            lstUnit = new list<SelectOption>();
            Map<String, String> params = ApexPages.currentPage().getParameters();
            strAccountId = CustomerCommunityUtils.customerAccountId;
            objFMCase = new FM_Case__c();
            objFMCase.Origin__c = 'Portal';
            objFMCase.Raised_Date__c = DateTime.now();
            objLoc = new CAFM_Location__c();
            mapType_Building = new map<String,CAFM_Building__c>();
            selectedLocation = selectedOption = 'None';
            strSRType='Helpdesk';

            Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Helpdesk').getRecordTypeId();
            objFMCase.RecordTypeId = devRecordTypeId;

            System.debug('== strAccountId ==' + strAccountId );
            System.debug('== strSelectedUnit ==' + strUnitId );
            if(String.isNotBlank(strAccountId)) {
                List<Account> lstAcc = new List<Account>();
                lstAcc = [SELECT Name from Account where id=:strAccountId];
                if(lstAcc.size() > 0) {
                    reporterName = lstAcc[0].Name;
                    objFMCase.Reporter_Name__c = reporterName;
                }
            }
            lstUnit = getUnitOptions();
        }
    }

    public override PageReference submit() {
      lstErrLogs = new list<Error_Log__c>();
      assignDetailsBuildingAndContract();
      //createTaskINCAFM();
      saveAsDraft();
      for(Error_Log__c obj: lstErrLogs) {
          obj.FM_Case__c = objFMCase.Id;
      }
      insert lstErrLogs;
     if(objFMCase.Id != Null) {
      PageReference objPage = new PageReference('/' + objFMCase.Id );
          if (objPage != NULL) {
              String unitId = objPage.getUrl().substringAfterLast('/');
              objPage = new PageReference(ApexPages.currentPage().getUrl());
              objPage.getParameters().clear();
              objPage.getParameters().put('view', 'CaseDetails');
              objPage.getParameters().put('id', unitId);
              objPage.setRedirect(true);
               
          }
          return objPage ;
      }
      else {
          return null;
      }
     
  }

    private static List<SelectOption> getUnitOptions() {
        List<SelectOption> lstOptions = new List<SelectOption>{new SelectOption('', 'Select', false)};
        for (Booking_Unit__c unit : FmcUtils.queryUnitsForAccount(
            CustomerCommunityUtils.customerAccountId, 'Id, Name, Unit_Name__c', ''
        )) {
            lstOptions.add(new SelectOption(unit.Id, unit.Unit_Name__c));
        }
        return lstOptions;
    }

    public void selectOptionAndGetCafmLocations() {
        System.debug('objFMCase.Helpdesk_Request_Area__c = ' + objFMCase.Helpdesk_Request_Area__c);
        System.debug('selectedOption = ' + selectedOption);
        selectedOption = objFMCase.Helpdesk_Request_Area__c;
        System.debug('objFMCase.Helpdesk_Request_Area__c = ' + objFMCase.Helpdesk_Request_Area__c);
        System.debug('selectedOption = ' + selectedOption);
        getCAFMLocations();
    }

    public void selectUnit() {
        System.debug('strUnitId = ' + strUnitId);
        if( String.isNotBlank( strUnitId ) ) {
            List<Booking_Unit__c> lstBU = new List<Booking_Unit__c>();
            lstBU = [SELECT Id, Unit_Name__c, DLP_End_Date__c,DLP_Start_date__c
                    FROM Booking_Unit__c WHERE Id =: strUnitId];
            System.debug('==lstBU===' + lstBU);
            if(lstBU.size() > 0) {
              objBU = lstBU[0];
            }
            System.debug('==objBU===' + objBU);
            //Get Location(Building) using unit name
            if(objBU != NULL && String.isNotBlank(objBU.Unit_Name__c)) {
                lstSelectOptions = new List<SelectOption>();
                lstSelectOptions.add(new SelectOption('None','None'));
                System.debug('==DLP_End_Date__c===' + String.valueOf(objBU.DLP_End_Date__c < Date.Today()));
                System.debug('==DLP_End_Date__c===' + objBU.DLP_End_Date__c);
                System.debug('==Date.Today()===' + Date.Today());
                if(objBU.DLP_End_Date__c == null || objBU.DLP_End_Date__c < Date.Today()) {
                    lstSelectOptions.add(new SelectOption('Common area','Common area'));
                }
                else {
                    lstSelectOptions.add(new SelectOption('Inside unit','Inside unit'));
                    lstSelectOptions.add(new SelectOption('Common area','Common area'));
                }
                List<Location__c> lstLocations = new List<Location__c>();
                String buildingCode = objBU.Unit_Name__c.substringBefore('/');
                System.debug('==buildingCode===' + buildingCode);
                lstLocations = [SELECT Name, ID,CAFM_Common_area__c,CAFM_Unit__c,
                                  (SELECT FM_User__c,FM_Role__c FROM FM_Users__r
                                  WHERE FM_Role__c = 'FM Admin'
                                   OR FM_Role__c='Master Community Admin' LIMIT 1)
                            FROM Location__c
                            WHERE RecordType.Name = 'Building'
                            AND Name =: buildingCode LIMIT 1];
                //Get CAFM buildings
                if(lstLocations.size() >0) {
                    Set<Id> setCAFMBuildingIds = new Set<Id>();
                    setCAFMBuildingIds.add(lstLocations[0].CAFM_Unit__c);
                    setCAFMBuildingIds.add(lstLocations[0].CAFM_Common_area__c);
                    system.debug('>>lstLocations>>'+lstLocations);
                    system.debug('>>setCAFMBuildingIds>>'+setCAFMBuildingIds);
                    getCAFMBuildings(setCAFMBuildingIds);
                    if(lstLocations[0].FM_Users__r.size() > 0) {
                        objFMCase.OwnerId = lstLocations[0].FM_Users__r[0].FM_User__c;
                    }
                }
                if(mapType_Building != Null && mapType_Building.size() == 0) {
                    showBuildings = false;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'NO CAFM BUILDINGS'));
                }
                else{
                    showBuildings = true;
                }
            }
        }
    }
}