/************************************************************************************************************************
* Name               : AP_FileUploadController 
* Test Class         : 
* Description        : Class for sending Email with Attachment selected
* Author             : QBurst          
*************************************************************************************************************************
* VERSION   AUTHOR          DATE            COMMENTS                
                                                                          
                                             
* 1.0                  
*************************************************************************************************************************/



public with sharing class AP_FileUploadController {

    public String filename { get; set; }
    public String conType { get; set; }
    public blob file { get; set; }
    public string fileBlobStr { get; set; }
    public String filenameDrop { get; set; }
    
    public String strSelectedLanguage  {set; get;}
    public string marketingCampaign {get; set;}
    public String RMEmailId { get; set; }
    public String comments { get; set; }
    
    Public String campaignName;
   
    Public  String agentName;
    Public  String agencyName;
    Public Boolean isError {get; set;}
    
    //Public String body ; 
    
    /* for the page AgentPortalDownloadExcel*/
    public String xlsHeader {
            get {
                String strHeader = '';
                strHeader += '<?xml version="1.0"?>';
                strHeader += '<?mso-application progid="Excel.Sheet"?>';
                return strHeader;
            }
      set;
    }
/* ends here*/
    
     
    Public AP_FileUploadController(){
        agencyName = '';
        agentName = '';
        campaignName= '';
      

        if( apexpages.currentPage().getParameters().get('langCode') != null){
            strSelectedLanguage = apexpages.currentPage().getParameters().get('langCode');
        } 
        
           

    }
    
    //Method to send Email
    public PageReference sendDocAttach() {
        system.debug('entered>>>>');
        if(String.isNotBlank(marketingCampaign)){
            campaignName = [SELECT Campaign_Name__c FROM Campaign__c WHERE Id =: marketingCampaign ].Campaign_Name__c;
        }
        else{
           
            campaignName = '';
        
        }
       // Creating a new Bulk Upload Request record 
       Bulk_Upload_Request__c bu = new Bulk_Upload_Request__c();
       
       bu.Status__c = 'Submitted';       
       bu.Comments__c = comments;
              
       if(String.isNotBlank(agencyName )){
       
            bu.Agency__c = [SELECT Id FROM Account WHERE Name =: agencyName ].Id;
       }
       else{
           
           bu.Agency__c = '';
        
       }
       
        
       if(String.isNotBlank(agentName )){
       
           bu.Agent__c = [SELECT Id FROM Contact WHERE Name =: agentName ].Id;
       }
       else{
           
           bu.Agent__c = '';
        
       }
       
       
       if(String.isNotBlank(marketingCampaign)){
       
            bu.Marketing_Campaign__c = [SELECT Campaign_Name__c FROM Campaign__c WHERE Id =: marketingCampaign ].Id;
        }
        else{
           
            bu.Marketing_Campaign__c = '';
        
        }

       bu.RM_email__c = RMEmailId;            
       Database.Insert(bu,false);
       
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
         
        //String[] sendTo = new String[]{'jitheshcv90@gmail.com','Swathy.jayaraj@damacgroup.com'}; 
        
        //String[] sendTo = System.Label.TR_AP_Bulk_Upload_Summary_Send_To.split(',');
        String[] sendTo;
        if(System.Label.TR_AP_Bulk_Upload_Summary_Send_To != '' && System.Label.TR_AP_Bulk_Upload_Summary_Send_To != NULL){
            if(System.Label.TR_AP_Bulk_Upload_Summary_Send_To.contains(',')){
                sendTo = System.Label.TR_AP_Bulk_Upload_Summary_Send_To.split(',');
                system.debug('sendEmailsTo >>>'+sendTo  );
            }
            else{
                sendTo = new String[]{System.Label.TR_AP_Bulk_Upload_Summary_Send_To};
                system.debug('sendEmailsTo >>>'+sendTo  );
            }
        }
        else{
        
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'There Is No Email ID Given..!'));

        }
        
 
       // String[] sendTo = new String[]{sendEmailsTo};      
        String body;
        String salutation;
        salutation = 'Dear Team,';        
        String emailBody;
        String bulkUploadRequestNumber;
        if(bu.Id !=NULL){
            bulkUploadRequestNumber = [SELECT Id, Name FROM Bulk_Upload_Request__c WHERE Id =: bu.Id ].Name;
        }
        else{
            bulkUploadRequestNumber = '';
        
        }
        String linkToBulkUploadRequest = 'Link to the Bulk Upload Request : ';
          
        emailBody = 'A new upload request has been submitted through Agent Portal. Please take the necessary action.';
        body ='';
        
        body = '<html><body>'
                +salutation+'<br/>'+'<br/>'+emailBody +'<br/><br/>'
                +'<b>Request Number</b>'+ '&emsp;&emsp;&emsp;&emsp;&emsp;&ensp; : &ensp;'+bulkUploadRequestNumber+'<br/>'
                +'<b>Agency</b>'+'&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp; : &ensp;'+agencyName +'<br/>'
                +'<b>Agent</b>'+'&emsp;&emsp;&emsp;&emsp;&ensp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;&ensp;:&ensp;'+agentName+'<br/>' 
                +'<b>Marketing Campaign Name</b>'+'&ensp; :&ensp;'+campaignName+'<br/>'
                + '<b>RM</b>'+' &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;  :&ensp;'+RMEmailId + '<br/>'
                +'<b>Comments</b>'+' &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;  :&ensp;'+comments+ '<br/><br/>'
                +linkToBulkUploadRequest + URL.getSalesforceBaseUrl().getHost() + '/' + bu.Id+'<br/><br/>'
                +'</body></html>';
                
        mail.setToAddresses(sendTo); 
        mail.setHtmlBody(body);
        //mail.setPlainTextBody(body); 
        String subject='Bulk Lead Upload Request '+bulkUploadRequestNumber +' Submitted';
        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
        if(file != null){
            attachment.setBody(file);
        } else {
            file =  Blob.valueOf(fileBlobStr); // EncodingUtil.base64Decode(fileBlobStr);
            attachment.setBody(file);
        }
        
        attachment.setContentType(conType);
        system.debug('filenameDrop>>>'+filenameDrop);
        system.debug('filename>>>'+filename);
        if(filenameDrop == '' || filenameDrop == null){
            attachment.setFileName(filename);
        } else {
            attachment.setFileName(filenameDrop);
        }
        attachment.setinline(false);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attachment } );
        mail.setSubject(subject);
      
        
        //Sends the email and checking if success
       
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {mail};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email was sent successfully..');
             ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Upload Request '+bulkUploadRequestNumber + ' Submitted.'));
        } else {
            System.debug('The email failed to send: '+ results[0].errors[0].message);
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please check input!'));
             
        }
        
        return null;
    }   
     // Method to get the Marketing Campaigns of the logged in agent
     public List<SelectOption> getMarketingCampaigns() {
        List<Assigned_Agent__c> assignedAgentList = new List<Assigned_Agent__c>();
        assignedAgentList = [SELECT 
                                id,Campaign__c,Campaign__r.Campaign_Name__c,User__c,Agency__r.Name,Contact__r.Name, Agency_Name__c 
                            From 
                                Assigned_Agent__c
                            WHERE 
                                User__c =:UserInfo.getUserId()
                            AND 
                                Campaign__r.Eligible_for_Agent_Portal__c = true
                           
                            AND
                               Campaign__r.Active__c = true ]; 
                               
        List<SelectOption> marketinCampaignNameList = new List<SelectOption>();
        if(assignedAgentList.size()>0){
            agencyName = assignedAgentList.get(0).Agency_Name__c;
            agentName = assignedAgentList.get(0).Contact__r.Name;
        }
         else{
            agencyName  ='';
            agentName = '';
        }
        for( Assigned_Agent__c assignedObj : assignedAgentList)
        {                
            marketinCampaignNameList.add(new SelectOption(assignedObj.Campaign__c, assignedObj.Campaign__r.Campaign_Name__c)); 
        }       
        return marketinCampaignNameList;
    }

    //Validating RM Email if it is entered
    public String validateRM (String rmEmail) {
        String error = '';
        if (rmEmail != '' && rmEmail != NULL) {
            try {
                User userRMObj = [SELECT Id FROM user WHERE Email =:rmEmail
                                            AND IsActive = TRUE 
                                            AND ( Profile.Name LIKE '%Property Consultant%' OR Profile.Name LIKE '%Agent Executive Team%')
                                            LIMIT 1];
            } catch (Exception e) {
                error = Label.Validate_RM_Email;                 
            }
        } 
        return error;
    }
        
    public void validateRMEmail() {
        isError = false;
        string customPageMessage ='';
        String emailId = apexpages.currentpage().getparameters().get('emailid');
        system.debug('emailId-------'+emailId);
        customPageMessage = validateRM (emailId);
        if (customPageMessage != '') {            
            isError = true;
            system.debug('customPageMessage in validate email'+customPageMessage);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,customPageMessage);
            ApexPages.addMessage(myMsg);
            //Apexpages.addmessages(customPageMessage);
        }        
    }
    


}