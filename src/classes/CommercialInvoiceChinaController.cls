/****************************************************************************************************
* Name          : CommercialInvoiceChinaController                                                  *
* Description   : CommercialInvoiceChina Page Controller                                            *
* Created Date  : 28-08-2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE            COMMENTS                                                *
* 1.0   Craig Lobo          28-08-2018      Initial Draft.                                          *
****************************************************************************************************/
public with sharing class CommercialInvoiceChinaController {

    public CommercialInvoiceChinaController.InvoiceData ivData                          {get; set;}
    public String AED_RMB = Label.AED_To_RMB;
    public Boolean isInitialised;
    public String dealSRId;
    public String dealSRName;

    /**
     * Constructor 
     */
    public CommercialInvoiceChinaController() {
        String bookingId = '';
        ivData = new CommercialInvoiceChinaController.InvoiceData();
        

        
        dealSRId = apexpages.currentpage().getparameters().get('srid');
        ivData.address = Apexpages.currentpage().getparameters().get('add');
        ivData.remarks = Apexpages.currentpage().getparameters().get('rmrk');
        ivData.chequeDate = Apexpages.currentpage().getparameters().get('date');
        ivData.totalAmount = Decimal.valueOf(Apexpages.currentpage().getparameters().get('amount'));
        system.debug('dealSRId-------------------------'+dealSRId);
        
        if (String.isNotBlank(dealSRId)) {
            List<NSIBPM__Service_Request__c> dealSRList = [ SELECT Id, Name, Token_Amount_AED__c 
                                                              FROM NSIBPM__Service_Request__c 
                                                             WHERE Id = :dealSRId 
                                                             LIMIT 1
            ];

            if (dealSRList != null && !dealSRList.isEmpty()) {
                String invNum = dealSRList[0].Name;
                invNum = invNum.replace('-', '');
                invNum = invNum.replace('SR', '');
                String dateTimeStr = String.valueOf(System.now());
                dateTimeStr = dateTimeStr.replace(':', '');
                dateTimeStr = dateTimeStr.replace(' ', '');
                dateTimeStr = dateTimeStr.replace('-', '');

                // Service Request Details
                ivData.invoiceNumber = invNum + dateTimeStr;
                ivData.invoiceDate = System.Today();
                ivData.invoiceDueDate = System.Today().addDays(7);
                /*if (String.isNotBlank(AED_RMB)) {
                    //ivData.amount = dealSRList[0].Token_Amount_AED__c * Decimal.valueOf(AED_RMB) ;
                    //ivData.totalAmount = ivData.totalAmount * Decimal.valueOf(AED_RMB) ;
                }*/
                System.debug('ivData.amount >>>>> ' + ivData.amount);
                
                ivData.vatAmount = ivData.totalAmount * 0.06;
                ivData.amount = ivData.totalAmount -ivData.vatAmount;
                ivData.amountInWords = NumberToWord.english_number(Integer.valueof(ivData.totalAmount)); 

                // Booking Details
                for (Booking__c bookObj :  [ SELECT Id, Registration_ID__c
                                               FROM Booking__c 
                                              WHERE Deal_SR__c = :dealSRId
                                                AND Rejected__c = false
                ]) {
                    bookingId = bookObj.Id;
                }

                // Buyer Details
                for (Buyer__c buyerkObj :  [ SELECT Id, Primary_Buyer__c, First_Name__c, Last_Name__c, 
                                                    Title__c 
                                               FROM Buyer__c 
                                              WHERE Booking__c = :bookingId
                                                AND Primary_Buyer__c = true
                ]) {

                    String fName = String.isNotBlank(buyerkObj.First_Name__c) ? buyerkObj.First_Name__c : '';
                    String lName = String.isNotBlank(buyerkObj.Last_Name__c) ? buyerkObj.Last_Name__c : '';
                    ivData.buyerName = buyerkObj.Title__c + ' ' + fName + ' ' + lName;
                }

                // Booking Unit Details
                for (Booking_Unit__c buObj : [ SELECT Id, Registration_ID__c, Payment_Method__c
                                                 FROM Booking_Unit__c
                                                WHERE Booking__c = :bookingId
                                                  AND Registration_ID__c != ''
                ]) {
                    if (String.isNotBlank(ivData.buRegIds)) {
                        ivData.buRegIds = ivData.buRegIds + ', ' + buObj.Registration_ID__c;
                    } else {
                        ivData.buRegIds = buObj.Registration_ID__c;
                    }
                    ivData.modeOfPayment = buObj.Payment_Method__c;
                }
                System.debug('FINAL ivData >>>>> ' + ivData);
                isInitialised = false;
            }
        } 
    }

    /**
     * Wrapper Class
     */
    public class InvoiceData {
        public String buyerName                                                         {get; set;}
        public String address                                                           {get; set;}
        public String remarks                                                           {get; set;}
        public String buRegIds                                                          {get; set;}
        public String modeOfPayment                                                     {get; set;}
        public String invoiceNumber                                                     {get; set;}
        public Date invoiceDueDate                                                      {get; set;}
        public Date invoiceDate                                                         {get; set;}
        public Decimal totalAmount                                                      {get; set;}
        public Decimal amount                                                           {get; set;}
        public Decimal vatAmount                                                        {get; set;}
        public String amountInWords                                                     {get; set;}
        public String chequeDate                                                        {get; set;}

    }


}