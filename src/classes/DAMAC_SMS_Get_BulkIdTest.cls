@isTest
public class DAMAC_SMS_Get_BulkIdTest {
    public static testMethod void method1() {
        String json=        '{'+
        '    "Status": "OK",'+
        '    "Data": {'+
        '        "BulkId": 3032693'+
        '    }'+
        '}';
        DAMAC_SMS_Get_BulkId obj = DAMAC_SMS_Get_BulkId.parse(json);
        System.assert(obj != null);
    }
}