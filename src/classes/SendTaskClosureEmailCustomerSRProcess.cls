/**
 * Ver       Date            Author                 Modification
 * 1.0    9/19/2019        Arsh Dave               Initial Version 
**/
public with sharing class SendTaskClosureEmailCustomerSRProcess {

    public Static String STR_CLOSED_NOTIFICATION_TEMPLATE  = 'NotifyCustomerSRProcessTaskClosure';
    public Static EmailTemplate objEmailTemplate;
    
    public SendTaskClosureEmailCustomerSRProcess() {
        objEmailTemplate = [
               SELECT Id
                    , Subject
                    , Body
                    , Name
                    , HtmlValue
                    , TemplateType
                    , BrandTemplateId
                 FROM EmailTemplate 
                WHERE Name =: STR_CLOSED_NOTIFICATION_TEMPLATE

        ];
        System.debug('-->> objEmailTemplate in const: ' + objEmailTemplate);
    }


    public static void getTaskAndAccountData(Task objTaskClosed){
        System.debug('-->> Inside getBUandTemplate: ' );
        Task getTaskDetails = [
            SELECT Id
                 , IsClosed
                 , OwnerId
                 , Parent_Task_Id__c
                 , Status
                 , WhatId
                 , Description
              FROM Task
             WHERE Id = :objTaskClosed.Id    
        ];
        FM_Case__c objFMCaseTask = [
            SELECT Id
                 , Account__r.Email__c
                 , Account__r.Email__pc
                 , Account__r.isPersonAccount
                 , Account__r.Name
                 , OwnerId
                 , Owner.Name
                 , Owner.Email
                 , Name
              FROM FM_Case__c
             WHERE Id = :getTaskDetails.WhatId
        ];
        
        
        System.debug('-->> objFMCaseTask: ' + objFMCaseTask);
        if(objFMCaseTask != NULL){
            System.debug('-->> Insdie objFMCaseTask Not null: ');

            sendEmail(getTaskDetails , objEmailTemplate , objFMCaseTask);
           
        } else {
            //String strAttachId = objAttachment.Id;
        
            
        }
       

    }    
    
    public static void sendEmail( Task objTaskClosed , EmailTemplate emailTemplateObj , FM_Case__c objFMCaseTask ) {
       
        System.debug('-->> Send Email: ' );

        String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue = '',contentBody = '';
        String subject, strAccountId, strRelatedToId, strDPIId, bccAddress ='', strCCAddress;
        
        strAccountId = objFMCaseTask.Account__c;
        strRelatedToId = objFMCaseTask.Account__c;

        //toAddress = objFMCaseTask.Owner.Email;
        //If Person Acount and Else If not person
        if(objFMCaseTask.Account__r.isPersonAccount
        && objFMCaseTask.Account__r.Email__pc != null) {
            toAddress = objFMCaseTask.Account__r.Email__pc;
        } else if(objFMCaseTask.Account__r.Email__c != null){
            toAddress = objFMCaseTask.Account__r.Email__c;
        }
        System.debug('toAddress = ' + toAddress);
        
        fromAddress = 'no-replysf@damacgroup.com';
        bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
        contentType = 'text/html';
        strCCAddress = '';
        system.debug(' emailTemplateObj : '+ emailTemplateObj );
        if( toAddress != '' ) {
            System.debug(' -->> objFMCaseTask.Account__r.Name: '+ objFMCaseTask.Account__r.Name);
             //contentBody = '<p> Dear Mr/Ms. ' + objFMCaseTask.Account__r.Name + ',</p><Br/>';
            // if(emailTemplateObj.body != NULL){
            //     contentBody +=  emailTemplateObj.body;
            // }
            // if(emailTemplateObj.htmlValue != NULL){
            //     contentValue += emailTemplateObj.htmlValue;
            // }
            // if(string.isblank(contentValue)){
            //     contentValue='No HTML Body';
            // }
            contentValue = '<html><body style="font-family: Arial; font-size:16px;"><p style="font-family: Arial; font-size:16px;"> Dear Mr/Ms. ' + objFMCaseTask.Account__r.Name + ',';
            contentValue += 'The service request with referrence number ' + objFMCaseTask.Name + ' has been closed. Please find the comments below:<br/>'+ objTaskClosed.Description + '<br/>Regards,  <br/>LOAMS</p></body></html>';
            subject = 'Task Closure Notification';
            System.debug('-->> subject: ' + subject);
            System.debug('-->> before callSendGrid : ');
            callSendGrid(toAddress, strCCAddress, bccAddress, subject, fromAddress, replyToAddress, contentType, contentValue, strAccountId, contentBody, strRelatedToId);
            System.debug('-->> After callSendGrid : ');
        }
    }

    // @Future(callout=true)
    public static void callSendGridFromBatch(String toAddress, String strCCAddress, String bccAddress, String subject, String fromAddress, String replyToAddress, String contentType, String contentValue, String strAccountId, String contentBody, String strRelatedToId){
        
        List<EmailMessage> lstEmails = new List<EmailMessage>();
        List<Attachment> lstAttach = new List<Attachment>();
        // List<Attachment> lstAttach = [
        //        SELECT Id
        //             , Body
        //             , Name
        //             , ParentId
        //             , Parent.Name 
        //         FROM Attachment 
        //         WHERE Id =: strAttachId 
        // ] ;
        System.debug('-->> lstAttach: ' + lstAttach);
        // Callout to sendgrid to send an email
        SendGridEmailService.SendGridResponse objSendGridResponse =
            SendGridEmailService.sendEmailService(
                toAddress
                , ''
                , strCCAddress
                , ''
                , bccAddress
                , ''
                , subject
                , ''
                , fromAddress
                , ''
                , replyToAddress
                , ''
                , contentType
                , contentValue
                , ''
                , lstAttach
            );

            System.debug('-->> objSendGridResponse: ' + objSendGridResponse);
            if(Test.isRunningTest()){
                objSendGridResponse.ResponseStatus = 'Accepted';
            }
            if (objSendGridResponse.ResponseStatus == 'Accepted') {
                EmailMessage mail = new EmailMessage();
                mail.Subject = subject;
                mail.MessageDate = System.Today();
                mail.Status = '3';//'Sent';
                mail.RelatedToId = strRelatedToId;
                mail.Account__c  = strAccountId;
                mail.Type__c = 'KYC Account Form';
                mail.ToAddress = toAddress;
                mail.FromAddress = fromAddress;
                mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                mail.Sent_By_Sendgrid__c = true;
                mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                mail.CcAddress = strCCAddress;
                mail.BccAddress = bccAddress;
                lstEmails.add(mail);
                system.debug('Mail obj == ' + mail);
            }
        system.debug('lstEmails = '+ lstEmails );
        if( lstEmails != null && lstEmails.size() > 0 ) {
            insert lstEmails;
        }
    }

    @Future(callout=true)
    public static void callSendGrid(String toAddress, String strCCAddress, String bccAddress, String subject, String fromAddress, String replyToAddress, String contentType, String contentValue, String strAccountId, String contentBody, String strRelatedToId){
        System.debug('-->> callSendGrid : ');
        List<EmailMessage> lstEmails = new List<EmailMessage>();
        List<Attachment> lstAttach = new List<Attachment>();
        // List<Attachment> lstAttach = [
        //        SELECT Id
        //             , Body
        //             , Name
        //             , ParentId
        //             , Parent.Name 
        //         FROM Attachment 
        //         WHERE Id =: strAttachId 
        // ] ;
        System.debug('-->> lstAttach: ' + lstAttach);
        // Callout to sendgrid to send an email
        SendGridEmailService.SendGridResponse objSendGridResponse =
            SendGridEmailService.sendEmailService(
                toAddress
                , ''
                , strCCAddress
                , ''
                , bccAddress
                , ''
                , subject
                , ''
                , fromAddress
                , ''
                , replyToAddress
                , ''
                , contentType
                , contentValue
                , ''
                , lstAttach
            );
            if(Test.isRunningTest()){
                objSendGridResponse.ResponseStatus = 'Accepted';
            }
            System.debug('-->> objSendGridResponse: ' + objSendGridResponse);
            if (objSendGridResponse.ResponseStatus == 'Accepted') {
                EmailMessage mail = new EmailMessage();
                mail.Subject = subject;
                mail.MessageDate = System.Today();
                mail.Status = '3';//'Sent';
                mail.RelatedToId = strRelatedToId;
                mail.Account__c  = strAccountId;
                mail.Type__c = 'KYC Account Form';
                mail.ToAddress = toAddress;
                mail.FromAddress = fromAddress;
                mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                mail.Sent_By_Sendgrid__c = true;
                mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                mail.CcAddress = strCCAddress;
                mail.BccAddress = bccAddress;
                lstEmails.add(mail);
                system.debug('Mail obj == ' + mail);
            }   
        system.debug('lstEmails = '+ lstEmails );
        if( lstEmails != null && lstEmails.size() > 0 ) {
            insert lstEmails;
        }
    }

}