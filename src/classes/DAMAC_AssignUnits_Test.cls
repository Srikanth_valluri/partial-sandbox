@isTest
public class DAMAC_AssignUnits_Test {
    public static testMethod void method1 () {
        Unit_Allocation__c unitAlloc = New Unit_Allocation__c();
            unitAlloc.Start_Date__c = system.today();
            unitAlloc.End_Date__c = system.today();
        insert unitAlloc;
        Inventory__c inv = New Inventory__c ();
            inv.Status__c = 'Released';
            inv.Unit_Type__c = 'test';
            inv.Unit__c = 'test';
            inv.property_name__c = 'test';
            inv.Bedroom_Type__c = '1 BR';
            inv.Floor_Package_Name__c = 'test';
            inv.property_city__c = 'test';
            inv.Property_Status__c = 'test';
            inv.View_Type__c = 'test';
            inv.District__c = 'test';
            
        insert inv;
        DAMAC_AssignUnits obj = New DAMAC_AssignUnits(new ApexPages.StandardController(unitAlloc));
        
        ApexPages.currentPage().getParameters().put('startdateday1','1');
        ApexPages.currentPage().getParameters().put('startdatemonth1','1');
        ApexPages.currentPage().getParameters().put('startdateyear1','1994');
        ApexPages.currentPage().getParameters().put('endDateyear1','2019');
        ApexPages.currentPage().getParameters().put('endDateday1','1');
        ApexPages.currentPage().getParameters().put('endDatemonth1','1');
        ApexPages.currentPage().getParameters().put('searchText','test');
        obj.sortColVal = 'Price';

        obj.filterData();
        //obj.dataPagination();
        obj.getdateYear();
        obj.getdateMonth();
        obj.getdateDay();
        DAMAC_AssignUnits.saveInventoriesMethod(unitAlloc.id,inv.id);
        //obj.sendemailfun();
        DAMAC_AssignUnits.getInquiries('test');
        DAMAC_AssignUnits.searchUnits('test');
        //obj.changecurrency();
        obj.getnxt();
        obj.getprv();
       // obj.searchRecs();
        obj.changeSortOrder();
        obj.prvbtn();
        obj.nextbtn();
        obj.getPkgNames();
        obj.getProjctFtrs();
        obj.getViewTypes();
        obj.getBedRooms();
        obj.getAcd();
        obj.getPropertyNames();
        obj.getUnits();
        obj.getUnitTypes();
        obj.getLocation();
        obj.getCities();
        obj.getUsers();
        obj.getPropStatus();
        obj.getItems();
        
        Apexpages.currentPage().getParameters().put ('limitVal', '10');
        obj.changeRecCountFunMethod();
        obj.searchRecs();
        double val = obj.offset;
        
    }
}