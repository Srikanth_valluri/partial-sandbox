@isTest
public class MollakInvoiceSearchControllerTest {
    @IsTest
    static void positiveTestCase1(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        objAccount.Old_Email__c = 'Test2@gmail.com';
        insert objAccount;
        
        Id caseRecordId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('FM Collection Email').getRecordTypeId();
        Case objCase = new Case();
        objCase.AccountId = objCase.Id;
        objCase.Subject = 'SERVICE CHARGE INVOICE';
        objCase.SuppliedEmail = 'noreply@dubailandmail.gov.ae';
        objCase.RecordTypeId = caseRecordId;
        insert objCase;

        EmailMessage objEmail = new EmailMessage();
        objEmail.ParentId = objCase.Id;
        objEmail.Subject  = 'SERVICE CHARGE INVOICE';
        objEmail.FromAddress = 'Test1@gmail.com';
        objEmail.ToAddress = 'Test2@gmail.com;Test3@gmail.com';
        objEmail.HtmlBody = 'Test1234';
        insert objEmail;
        MollakInvoiceSearchLWCController.searchMollakInvoice(objAccount.Id);
    }
    
    @IsTest
    static void positiveTestCase2(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        objAccount.Old_Email__c = 'Test2@gmail.com';
        insert objAccount;
        
         NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;

        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAccount.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;

        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Account_Id__c = objAccount.Id;
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC';
        objBookingUnit.Registration_Status_Code__c = 'ZX';
        insert objBookingUnit;
        
        Id caseRecordId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('FM Collection Email').getRecordTypeId();
        Case objCase = new Case();
        objCase.AccountId = objCase.Id;
        objCase.Subject = 'SERVICE CHARGE INVOICE';
        objCase.SuppliedEmail = 'noreply@dubailandmail.gov.ae';
        objCase.RecordTypeId = caseRecordId;
        insert objCase;

        EmailMessage objEmail = new EmailMessage();
        objEmail.ParentId = objCase.Id;
        objEmail.Subject  = 'SERVICE CHARGE INVOICE';
        objEmail.FromAddress = 'Test1@gmail.com';
        objEmail.ToAddress = 'Test2@gmail.com;Test3@gmail.com';
        objEmail.HtmlBody = 'Test1234';
        insert objEmail;
        MollakInvoiceSearchLWCController.searchMollakInvoice(objBookingUnit.Id);
    }
    
    @IsTest
    static void positiveTestCase3(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        objAccount.Old_Email__c = 'Test2@gmail.com';
        insert objAccount;
        
         NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;

        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAccount.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;

        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Account_Id__c = objAccount.Id;
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC';
        objBookingUnit.Registration_Status_Code__c = 'ZX';
        insert objBookingUnit;
        
        Id caseRecordId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('FM Collection Email').getRecordTypeId();
        Case objCase = new Case();
        objCase.AccountId = objCase.Id;
        objCase.Subject = 'SERVICE CHARGE INVOICE';
        objCase.SuppliedEmail = 'noreply@dubailandmail.gov.ae';
        objCase.RecordTypeId = caseRecordId;
        insert objCase;

        EmailMessage objEmail = new EmailMessage();
        objEmail.ParentId = objCase.Id;
        objEmail.Subject  = 'SERVICE CHARGE INVOICE';
        objEmail.FromAddress = 'Test1@gmail.com';
        objEmail.ToAddress = 'Test2@gmail.com;Test3@gmail.com';
        objEmail.HtmlBody = 'Test1234';
        insert objEmail;
        MollakInvoiceSearchLWCController.reSendEmail(objEmail.Id);
    }
}