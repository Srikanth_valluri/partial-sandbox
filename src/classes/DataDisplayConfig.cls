public class DataDisplayConfig {
    public String recordId                                                  {set;get;}
    public String parentField                                               {set;get;}
    public String configJSON                                                {set;get;}

    //table/list/recordDetail/htmltable
    public String displayAs                                                 {set;get;}
    //data fetch strategy : fieldlist, fieldset, class
    public String strategy                                                  {set;get;}

    public String objectName                                                {set;get;}
    public String fieldset                                                  {set;get;}
    public String dataProviderClass                                         {set;get;}
    public String recordLimit                                               {set;get;}

    //column wise fields and columns options
    public List<Map<String,String>> fieldList                               {set;get;}
    // column wise fields and columns options to be shown in detail view on record hover
    public List<Map<String,String>> detailFieldList                         {set;get;}
    // maps RecordType To FieldList
    public Map<String, List<Map<String, String>>> mapRecordTypeToFieldList  {set;get;}

    public List<Map<String, Object>> dataList                               {set;get;}

    public String href                                                      {set;get;}
    public String target                                                    {set;get;}

    // filter clause for the record query
    public String filter                                                    {set;get;}
    // additional parameters
    public Map<String, String> params                                       {set;get;}
}