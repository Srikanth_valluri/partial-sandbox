@RestResource(urlMapping='/surveyResponse/')
global class SurveyResponseService{ 
    @HttpPost
    Global static SurveyResponseWrapper submitResults(List<SurveyQuestionWrapper> allQuestions) {
        System.debug('inside submit::::::::##');
        SurveyResponseWrapper responseWrapper=new SurveyResponseWrapper();
        
        try {
        
            
            List <Survey_Question_Response_CRM__c> sqrList = new List<Survey_Question_Response_CRM__c>();
            List<SurveyQuestionWrapper> allCombinedQuestions = new List<SurveyQuestionWrapper>();
            allCombinedQuestions.addAll(allQuestions);
            System.debug('allQuestions::::'+allQuestions);
            for (SurveyQuestionWrapper question : allQuestions) {
               System.debug('question.isSubQuestion:::'+question.isSubQuestion);
                if(question.isSubQuestion){
                    allCombinedQuestions.addAll(question.subQuestions);
                }
            }
            //isError = false;
            System.debug('allCombinedQuestions:::'+allCombinedQuestions);
            for (SurveyQuestionWrapper q : allCombinedQuestions) {
                System.debug('q.surveyQuestion.Type__c:::'+q.surveyQuestion.Type__c);
                if(q.surveyQuestion.Type__c != 'Section'){
                    Survey_Question_Response_CRM__c sqr = new Survey_Question_Response_CRM__c();
                    if(q.choiceForAdditionalTextbox != null && q.choiceForAdditionalTextbox != ''
                        && q.additionalResponse != ''){
                            sqr.Additional_Response__c = q.additionalResponse;
                    }
                    if (q.renderSelectRadio) {
                        if (q.required && (q.selectedOption == null || q.selectedOption == '')) {
                            responseWrapper.message='Please fill out all required fields marked with asterisk';
                            responseWrapper.isError = true;
                            return responseWrapper;
                        }
                        if (q.selectedOption == null || q.selectedOption == '') {
                            sqr.Response__c = '';
                        }
                        else {
                            sqr.Response__c = q.singleOptions.get(q.selectedOption);
                        }
                        sqr.Survey_Question__c = q.Id;
                        System.debug('sqr@@@@@@'+sqr);
                        sqrList.add(sqr);
                    }
                    else if (q.renderRatings) {
                        system.debug('>>>>>>Inner q.selectedOption : ' + q.selectedOption);
                        if (q.required && (q.selectedOption == null || q.selectedOption == '')) {
                            responseWrapper.message='Please fill out all required fields marked with asterisk';
                            responseWrapper.isError = true;
                            return responseWrapper;
                        } else {
                            if (q.selectedOption == null || q.selectedOption == '') {
                                sqr.Response__c = '';
                            }
                            else {
                                sqr.Response__c = q.singleOptions.get(String.valueOf(Integer.valueOf(q.selectedOption)+1));
                            }
                        }
                        sqr.Survey_Question__c = q.Id;
                        sqrList.add(sqr);
                    } else if (q.renderPicklist) {

                        if (q.required && (q.selectedOption == null || q.selectedOption == '')) {
                            responseWrapper.message='Please fill out all required fields marked with asterisk';
                            responseWrapper.isError = true;
                            return responseWrapper;
                        }

                        if (q.selectedOption == null || q.selectedOption == '') {
                            sqr.Response__c = '';
                        } else {
                            sqr.Response__c = q.singleOptions.get(String.valueOf(Integer.valueOf(q.selectedOption)+1));
                              System.debug(' sqr.  sqr.Response__c::renderPicklist:jhol:'+sqr.Response__c);
                        
                         }
                        sqr.Survey_Question__c = q.Id;
                        sqrList.add(sqr);
                    } else if (q.renderFreeText) {
                        if (q.required && q.choices == '') {
                            responseWrapper.message='Please fill out all required fields marked with asterisk';
                            responseWrapper.isError = true;
                            return responseWrapper;
                        }

                        sqr.Response__c = q.choices;
                        sqr.Survey_Question__c = q.Id;
                        sqrList.add(sqr);
                    } else if (q.renderSelectCheckboxes) {
                        if (q.required && (q.selectedOptions == null || q.selectedOptions.size() == 0)) {
                            responseWrapper.message='Please fill out all required fields marked with asterisk';
                            responseWrapper.isError = true;
                            return responseWrapper;
                        }
                        List<String> checkBoxLst = new List<String>();
                        for (String opt : q.selectedOptions) {
                            sqr = new Survey_Question_Response_CRM__c();
                            if (opt == '' || opt == null) {
                                sqr.Response__c = '';
                            } 
                            else {
                                sqr.Response__c = q.multiOptions.get(opt);
                               // String optionInst =  q.multiOptions.get(Integer.valueOf(opt)).getLabel();
                                checkBoxLst.add(sqr.Response__c);
                              }
                            sqr.Response__c  = '';
                            for(String checkboxInst : checkBoxLst){
                                
                                sqr.Response__c += checkboxInst + ',';
                            }
                            sqr.Response__c = sqr.Response__c.removeEnd(',');
                            sqr.Survey_Question__c = q.Id;
                        }
                        sqrList.add(sqr);
                    }
                    else if (q.renderSelectRow) {
                        if (q.required && (q.selectedOption == null || q.selectedOption == '')) {
                            responseWrapper.message='Please fill out all required fields marked with asterisk';
                            responseWrapper.isError = true;
                            return responseWrapper;
                        }

                        if (q.selectedOption == null || q.selectedOption == '') {
                            sqr.Response__c = '';
                        }
                        else {
                            sqr.Response__c = q.rowOptions.get(q.selectedOption);
                        }

                        sqr.Survey_Question__c = q.Id;
                        sqrList.add(sqr);
                    }
                    system.debug('>>>>>>^************^sqr : '+sqr);
                    system.debug('>>>>>>&&&&&&&&&&&&&&sqr.Response__c : '+sqr.Response__c);
                    system.debug('>>>>>^^^^^^^^^^^^^^^>q.Id : '+q.Id);

                    if (q.choiceForAdditionalTextbox != NULL) {
                    }

                    if (q.choiceForAdditionalTextbox != NULL
                        && String.isBlank(sqr.Additional_Response__c)) {
                        List<String> optionList = q.choiceForAdditionalTextbox.split(';');
                        for (String option : optionList) {
                            if (option.equalsIgnoreCase(sqr.Response__c)) {
                                responseWrapper.message='Please fill out all required fields marked with asterisk';
                                responseWrapper.isError = true;
                                return responseWrapper;
                            }
                        }
                    }
                }
            }
            Survey_Taken_CRM__c surveyTaken = new Survey_Taken_CRM__c();
            if (CustomerCommunityUtils.customerAccountId != NULL) {
                surveyTaken.Customer_taking_Survey__c = CustomerCommunityUtils.customerAccountId;
            }
            //if(isPortalSurvey) {
                surveyTaken.User_taking_Survey__c = UserInfo.getUserId();
            /*}
            else {
                system.debug(' isError : ' + isError );
                system.debug(' surveyTaken.Booking_Unit__c : ' + surveyTaken.Booking_Unit__c );

                surveyTaken.Customer_taking_Survey__c = listBookingUnit[0].Booking__r.Account__c;
                surveyTaken.Property__c = listBookingUnit[0].Inventory__r.Property__c;

                if ( surveyTaken.Booking_Unit__c == null  ) {
                    message='Please fill out all required fields marked with asterisk';
                    isError = true;
                    return;
                }

                if ( String.isBlank( surveyTaken.Customer_Name__c )  ) {
                    message='Please fill out all required fields marked with asterisk';
                    isError = true;
                    return;
                }

                if ( String.isBlank( surveyTaken.Property_Name__c )   ) {
                    message='Please fill out all required fields marked with asterisk';
                    isError = true;
                    return;
                }

                if ( surveyTaken.Phone__c == null  ) {
                    message='Please fill out all required fields marked with asterisk';
                    isError = true;
                    return;
                }
                if( surveyTaken.Purpose_of_Visit__c == null ) {
                    message='Please fill out all required fields marked with asterisk';
                    isError = true;
                    return;
                }
            }*/
            List<Survey_CRM__c> surveyList = [Select Id,Name,Hide_Survey_Name__c,Survey_Header__c,Thank_You_Text__c,Description__c from Survey_CRM__c
                                          where Is_Active__c = True and Type__c ='Portal' and End_Date__c > :System.Today() and Start_Date__c < :System.Today()];
    
    
            //surveyTaken = new Survey_Taken_CRM__c();
    
            if(surveyList != null && surveyList.size()>0){   
                surveyTaken.Survey__c = surveyList[0].Id;
            }
            
            insert surveyTaken;

            for (Survey_Question_Response_CRM__c sqr : sqrList)
            {
                sqr.Survey_Taker__c = surveyTaken.Id;
            }
            insert sqrList;
            responseWrapper.isError = false;
            responseWrapper.message='Success';
            //thankYouRendered=true;


        }catch(Exception e){
            System.debug('Exception: ' + e.getMessage() + e.getStackTraceString() );
            responseWrapper.isError = true;
            responseWrapper.message='Some error occured while saving response'  ;
        }
        return responseWrapper;

    }
}