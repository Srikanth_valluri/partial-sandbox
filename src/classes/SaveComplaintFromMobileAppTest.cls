@istest
public class SaveComplaintFromMobileAppTest {
    public  testmethod static void method()
    {
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='abc123545@testorg.com');
        insert u;
     
        SaveComplaintFromMobileApp nation=new SaveComplaintFromMobileApp();
        SaveComplaintFromMobileApp.SaveComplaintWrapper n=new SaveComplaintFromMobileApp.SaveComplaintWrapper(); 
        Account a=new Account();
        a.Name='test';
        insert a;
        n.AccountID=a.id;
        n.status='Submitted';
        //n.RecordType='Name Nationality Change';
        NSIBPM__Service_Request__c sr=new NSIBPM__Service_Request__c();
        insert sr;
        booking__c b1=new booking__c();
        b1.Deal_SR__c=sr.id;
        insert b1;
        Booking_Unit__c b=new Booking_Unit__c();
        b.Booking__c=b1.id;
        b.Unit_Name__c='test';
        insert b;
        n.BookingUnit=b.Unit_Name__c;
        n.userId=u.id;
       SaveComplaintFromMobileApp.SaveComplaint(n);
        
    }
}