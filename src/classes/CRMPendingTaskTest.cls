@isTest
private class CRMPendingTaskTest
{
//AKISHOR
    static testMethod void testMethod1() 
    {   
        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id aoptRecordTypeID = caseRecordTypes.get('AOPT').getRecordTypeId();

        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , aoptRecordTypeID);
        objCase.Status = 'Submitted';
        insert objCase;

        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        Account objAcc = [SELECT Id from Account LIMIT 1];
        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        
        List<Booking__c> lstBookings = new List<Booking__c>();
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        insert lstBookingUnits;
        
        list<Buyer__c> lstBuyer = new list<Buyer__c>();
        lstBuyer = TestDataFactory_CRM.createBuyer(lstBookingUnits[0].Booking__c,5, objAcc.Id);
        insert lstBuyer;
        
        PageReference pageRef = Page.MyPendingTasks;
        //pageRef.getParameters().put('id', String.valueOf(lstBookingUnits[0].Id));
        //pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        //pageRef.getParameters().put('SRType', 'FundTransferActiveUnits');
        
        //Test.setCurrentPage(pageRef);
        Test.startTest();
        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAssignment() );
        //ApexPages.StandardController sc = new ApexPages.StandardController(lstBookingUnits[0]);
        PendingSRTasks controller = new PendingSRTasks();
        controller.tasks();
        Test.stopTest();
    }

}