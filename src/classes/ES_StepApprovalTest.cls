@isTest
public class ES_StepApprovalTest {
    
    public static List<NSIBPM__Service_Request__c> createServiceRequestList = new List<NSIBPM__Service_Request__c>();
    private static List<NSIBPM__SR_Template__c> createSrTemplateList = new List<NSIBPM__SR_Template__c>();  
    private static List<NSIBPM__Status__c> createStatus = new List<NSIBPM__Status__c>();
    private static List<NSIBPM__Transition__c> createStepTransitionList = new List<NSIBPM__Transition__c>();
    private static List<NSIBPM__Step__c> createStepList = new List<NSIBPM__Step__c>();
    private static List<Group> createGroupList = new List<Group>();
    private static List<NSIBPM__SR_Status__c> createSrStatusList = new List<NSIBPM__SR_Status__c>(); 
    private static List<User> createTestUser = new List<User>();
    
    private static void init(){
        
        createTestUser = TestDataFactory.createTestUserRecords(new List<User>{new User(Email='abcd@damac.com')});
        system.runAs(new User(Id = UserInfo.getUserId())){
            createSrTemplateList = InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
            createSrStatusList = InitialiseTestData.createSrStatusRecords(new List<NSIBPM__SR_Status__c>{new NSIBPM__SR_Status__c(Name = 'Rejected', NSIBPM__Code__c = 'REJECTED', NSIBPM__Type__c = 'Rejected', NSIBPM__DEV_Id__c='REJECTED'),
                																						 new NSIBPM__SR_Status__c(Name = 'Submitted', NSIBPM__Code__c = 'SUBMITTED', NSIBPM__Type__c = '', NSIBPM__DEV_Id__c='SUBMITTED')});
            

            
            createServiceRequestList = 
                InitialiseTestData.createtestServiceRequestRecords(
                    new List<NSIBPM__Service_Request__c>{
                        new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration'),
                                                       NSIBPM__SR_Template__c = createSrTemplateList[0].Id)});
            createServiceRequestList[0].DOS_Email__c = 'abcd@damac.com';
            createServiceRequestList[0].Manager_Email__c = 'abcd1@damac.com';
            createServiceRequestList[0].HOS_Email__c = 'abcd2@damac.com';
            createServiceRequestList[0].HOD_Email__c = 'abcd3@damac.com';
            update createServiceRequestList;
            createStatus = InitialiseTestData.createStatusRecords(
                new List<NSIBPM__Status__c>{
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'UNDER_REVIEW', Name = 'Under Review'),
                        new NSIBPM__Status__c(NSIBPM__Code__c = 'REJECTED', Name = 'Rejected')});
            
            createStatus[0].NSIBPM__Rejection__c = false;
            createStatus[0].NSIBPM__Type__c = 'Start';
            createStatus[1].NSIBPM__Rejection__c = true;
            createStatus[1].NSIBPM__Type__c = 'End';
            update createStatus;
            
            system.debug(createStatus);
            
            createStepTransitionList =  InitialiseTestData.createTransitionRecords(new List<NSIBPM__Transition__c> {
                new NSIBPM__Transition__c(NSIBPM__From__c = createStatus[0].Id, NSIBPM__To__c = createStatus[1].Id, NSIBPM__Transition_Code__c = 'UNDER_REVIEW_TO_REJECTED')});
            
            
            NSIBPM__SR_Steps__c srStep1 = new NSIBPM__SR_Steps__c(Approver_Role__c='DOS', NSIBPM__Step_No__c = 1.0, NSIBPM__SR_Template__c = createSrTemplateList[0].Id, NSIBPM__Start_Status__c = createStatus[0].Id,
                                                                 NSIBPM__Execute_on_Submit__c = true, NSIBPM__Step_RecordType_API_Name__c = 'General' );
            insert srStep1;
            
            NSIBPM__SR_Steps__c srStep2 = new NSIBPM__SR_Steps__c(Approver_Role__c='GM');
            insert srStep2;
            
            createStepList = InitialiseTestData.createTestStepRecords(
                new List<NSIBPM__Step__c>{
                    new NSIBPM__Step__c(NSIBPM__SR__c = createServiceRequestList[0].id, NSIBPM__Status__c = createStatus[0].Id, NSIBPM__SR_Step__c = srStep1.id)});
            
            update createStepList;
            
            InitialiseTestData.createStepTransitionRecords(
                new List<NSIBPM__Step_Transition__c>{
                    new NSIBPM__Step_Transition__c(NSIBPM__SR_Step__c = srStep1.Id, NSIBPM__Transition__c = createStepTransitionList[0].Id, NSIBPM__SR_Status_Internal__c = createSrStatusList[0].Id, NSIBPM__SR_Status_External__c = createSrStatusList[0].Id)});
            
            Email_Service_Body_Split__c custSetting1 = new Email_Service_Body_Split__c(Name = 'All', Active__c = true, Split_By_Value__c = 'All');
            insert custSetting1;
            Email_Service_Body_Split__c custSetting2 = new Email_Service_Body_Split__c(Name = 'Regard', Active__c = true, Split_By_Value__c = 'Regard');
            insert custSetting2;
            Email_Service_Body_Split__c custSetting3 = new Email_Service_Body_Split__c(Name = 'Dear', Active__c = true, Split_By_Value__c = 'Dear');
            insert custSetting3;
            Email_Service_Body_Split__c custSetting4 = new Email_Service_Body_Split__c(Name = 'From', Active__c = true, Split_By_Value__c = 'From');
            insert custSetting4;
            
            
                
        }
        
        
    }
    
    static testMethod void myUnitTest1(){
        
        init();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        NSIBPM__Step__c stp = [SELECT Id, Name, NSIBPM__SR__c FROM NSIBPM__Step__c WHERE NSIBPM__SR__c =: createServiceRequestList[0].Id];
        email.fromAddress = 'abcd@damac.com';
        email.subject = 'An SR is Awaiting your approval ['+ stp.Name + ']';
        email.plainTextBody = 'reject [test] Dear';
        ES_StepApproval emailClass = new ES_StepApproval();
        Messaging.InboundEmailResult emailResult = emailClass.handleInboundEmail(email, new Messaging.InboundEnvelope());
        
    }
    
    static testMethod void myUnitTest2(){
        
        init();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        NSIBPM__Step__c stp = [SELECT Id, Name, NSIBPM__SR__c FROM NSIBPM__Step__c WHERE NSIBPM__SR__c =: createServiceRequestList[0].Id];
        email.fromAddress = 'abcd@damac.com';
        email.subject = 'An SR is Awaiting your approval ['+ stp.Name + ']';
        email.plainTextBody = 'reject [test] From';
        system.runAs(new User(Id = UserInfo.getUserId())){
            ES_StepApproval emailClass = new ES_StepApproval();
            Messaging.InboundEmailResult emailResult = emailClass.handleInboundEmail(email, new Messaging.InboundEnvelope());
            emailClass.makeSrDocsOptional(createServiceRequestList[0].Id);
        }

        
    }
}