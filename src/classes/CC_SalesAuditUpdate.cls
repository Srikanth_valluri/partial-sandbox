/**************************************************************************************************
* Name               : CC_SalesAuditUpdate
                                                 
**************************************************************************************************/
global without sharing class CC_SalesAuditUpdate implements NSIBPM.CustomCodeExecutable {
    public String EvaluateCustomCode(NSIBPM__Service_Request__c SR, NSIBPM__Step__c step) {
        String sSalesAuditFlag='N';
        if (step.NSIBPM__Step_Status__c =='Sales Audit Approved' || step.NSIBPM__Step_Status__c =='Sales Audit Not Applicable')
        {
        sSalesAuditFlag = 'Y';
        }
        system.debug('SalesAudit'+sSalesAuditFlag);
        String retStr = 'Success';
        List<Id> BookingIds= new List<id>();
        try{
            for(Booking__c Bookings :[select id from Booking__c where Deal_SR__c=: step.NSIBPM__SR__c]){
                BookingIds.add(Bookings.id);
            }
            if(BookingIds.size()>0){
                system.debug('#### invoking CC_AgentRegUpdate');
                system.enqueueJob(new AsyncSalesAuditUpd(BookingIds,'SalesAuditUpdate',sSalesAuditFlag));
            }
        
        } catch (Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
        }
        return retStr;
    }
}