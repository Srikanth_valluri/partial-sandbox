@istest
public class Damac_TourGuideControllerTest {
    
    @isTest static void test_method_0() {
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c=null;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        insert sr;
        
        NSIBPM__Step_Template__c stpTemplate =  InitializeSRDataTest.createStepTemplate('Deal','MANAGER_APPROVAL');
        insert stptemplate;
        
        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
            Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);
        
        
        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id,stepStatuses.values()[0].id,stptemplate.id);
        insert stp;
        
        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        insert lstbk;
        
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        insert loc;       
        
        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Registration_DateTime__c = DateTime.Now ();
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        insert bu;
        
        List<Account> accountList = new List<Account>();
        accountList = TestDataFactory.createAccountRecords(new List<Account>{new Account()});
        Buyer__c PB= new Buyer__c();
        PB.Primary_Buyer__c=true;
        PB.Buyer_Type__c='Individual';
        PB.Booking__c=lstbk[0].id;
        PB.Phone_Country_Code__c='India: 0091';
        PB.Passport_Expiry_Date__c='25/03/2017';
        PB.CR_Registration_Expiry_Date__c='25/12/2017';
        PB.City__c='Dubai';
        PB.Country__c='United Arab Emirates';
        PB.Address_Line_1__c='street1';
        PB.Address_Changed__c=true;
        PB.Date_of_Birth__c='25/12/1990';
        PB.Email__c='test@test.com';
        PB.First_Name__c='Buyer';
        PB.Last_Name__c='test';
        PB.Phone_Country_Code__c ='China: 0086'; 
        PB.Passport_Number__c='PP123'; 
        PB.Phone__c='53532255';
        PB.Place_of_Issue__c='Delhi'; 
        PB.Title__c='Mr.';
        PB.Account__c = accountList[0].Id;
        insert PB;
        apexpages.currentpage().getparameters().put('id', sr.id);
        Damac_TourGuideController obj = new Damac_TourGuideController ();
        obj.unit.Tour_Guide_Name__c =  'test';
        obj.unit.Tour_Guide_Email__c = 'test@test.com';
        obj.unit.Tour_Guide_Mobile_Phone_No__c = '8977365305';
        obj.unit.Tour_Guide_Passport_Number__c = '3esfa21';
        obj.unit.Tour_Guide_Mobile_Country_Code__c = 'China: 0086';
        obj.save ();
    }
    @isTest static void test_method_1() {
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c=null;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        insert sr;
        
        NSIBPM__Step_Template__c stpTemplate =  InitializeSRDataTest.createStepTemplate('Deal','MANAGER_APPROVAL');
        insert stptemplate;
        
        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
            Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);
        
        
        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id,stepStatuses.values()[0].id,stptemplate.id);
        insert stp;
        
        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        insert lstbk;
        
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        insert loc;       
        
        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Registration_DateTime__c = DateTime.Now ();
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        insert bu;
        
        List<Account> accountList = new List<Account>();
        accountList = TestDataFactory.createAccountRecords(new List<Account>{new Account()});
        Buyer__c PB= new Buyer__c();
        PB.Primary_Buyer__c=true;
        PB.Buyer_Type__c='Individual';
        PB.Booking__c=lstbk[0].id;
        PB.Phone_Country_Code__c='India: 0091';
        PB.Passport_Expiry_Date__c='25/03/2017';
        PB.CR_Registration_Expiry_Date__c='25/12/2017';
        PB.City__c='Dubai';
        PB.Country__c='United Arab Emirates';
        PB.Address_Line_1__c='street1';
        PB.Address_Changed__c=true;
        PB.Date_of_Birth__c='25/12/1990';
        PB.Email__c='test@test.com';
        PB.First_Name__c='Buyer';
        PB.Last_Name__c='test';
        PB.Phone_Country_Code__c ='China: 0086'; 
        PB.Passport_Number__c='PP123'; 
        PB.Phone__c='53532255';
        PB.Place_of_Issue__c='Delhi'; 
        PB.Title__c='Mr.';
        PB.Account__c = accountList[0].Id;
        insert PB;
        apexpages.currentpage().getparameters().put('id', sr.id);
        Damac_TourGuideController obj = new Damac_TourGuideController ();

        obj.save ();
    }
    @isTest static void test_method_2() {
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c=null;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        insert sr;
        
        NSIBPM__Step_Template__c stpTemplate =  InitializeSRDataTest.createStepTemplate('Deal','MANAGER_APPROVAL');
        insert stptemplate;
        
        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
            Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);
        
        
        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id,stepStatuses.values()[0].id,stptemplate.id);
        insert stp;
        
        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        insert lstbk;
        
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        insert loc;       
        
        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Registration_DateTime__c = DateTime.Now ();
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        insert bu;
        
        apexpages.currentpage().getparameters().put('id', sr.id);
        Damac_TourGuideController obj = new Damac_TourGuideController ();

        obj.save ();
    }
    @isTest static void test_method_3 () {
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c=null;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        insert sr;
        
        NSIBPM__Step_Template__c stpTemplate =  InitializeSRDataTest.createStepTemplate('Deal','MANAGER_APPROVAL');
        insert stptemplate;
        
        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
            Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);
        
        
        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id,stepStatuses.values()[0].id,stptemplate.id);
        insert stp;
        
        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        insert lstbk;
        
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        insert loc;       
        
        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Registration_DateTime__c = DateTime.Now ();
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        bu.Tour_Guide_Name__c = 'test';
        bu.Tour_Guide_Email__c = 'tst@test.com';
        bu.Tour_Guide_Mobile_Phone_No__c = '8977365305';
        bu.Tour_Guide_Passport_Number__c = 'test';
        bu.Tour_Guide_Mobile_Country_Code__c = 'China: 0086';
        insert bu;
        
        apexpages.currentpage().getparameters().put('id', sr.id);
        Damac_TourGuideController obj = new Damac_TourGuideController ();

        obj.save ();
    }
    
    
}