/**
 * @File Name          : FmCaseHtmlDocControllerTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10/21/2019, 4:24:29 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/21/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
public  class FmCaseHtmlDocControllerTest {
       static testMethod void unitTest() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;

        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;

        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;

        list<FM_Case__c> lstFMCases = new list<FM_Case__c>();

        Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Work Permit').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Apply_for_Work_Permits';
        fmCaseObj.Booking_Unit__c=lstBookingUnit[0].id;
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
        fmCaseObj.Status__c='Submitted';
        fmCaseObj.Origin__c='Portal';
        fmCaseObj.Approval_Status__c = 'Pending';
        fmCaseObj.Submit_for_Approval__c = true;
        fmCaseObj.Approving_Authorities__c='Property Manager__FM Manager,Master Community Property Manager';
        fmCaseObj.Account__c=objAcc.id;
        fmCaseObj.Current_Approver__c = 'FM Manager__';
        fmCaseObj.Tenant_Email__c = 'test@gmail.com';
        fmCaseObj.Notice_Type__c = 'First Notice';
        insert fmCaseObj;

        String JSONString = JSON.serialize(fmCaseObj);
        System.debug('JSONString::::'+JSONString);

        PageReference firstNoticePage = Page.FirstNoticeHTMLDocument;
        Test.setCurrentPage(firstNoticePage);
        ApexPages.currentPage().getParameters().put('srJSON',JSONString);
        FmCaseHtmlDocController ac = new FmCaseHtmlDocController();
        
        
      
    }
}