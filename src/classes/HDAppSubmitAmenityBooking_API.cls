/**********************************************************************************************************************
Description: This service is used for submitting the Amenity booking to SF.
===============================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By   | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   22-09-2020      | Shubham Suryawanshi | Added 'amenity_icon' attribute in the response params
***********************************************************************************************************************/

@RestResource(urlMapping='/submitAmenityBooking/*')
global class HDAppSubmitAmenityBooking_API {

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };


    @HttpPost 
    global static FinalReturnWrapper SubmitAmenityBookingCase() {

        SubmitSRResponseWrapper returnObj = new SubmitSRResponseWrapper();
        Booking_Unit__c objBU = new Booking_Unit__c();
        AmenityBookingWrapper amenityObj = new AmenityBookingWrapper();

        /*New*/
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();

        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString'+jsonString);
        System.debug('Request params:' + req.params);

        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();

        //Boolean isGuestBooking = false;
        Boolean isTenant = false;

        if(String.isBlank(jsonString)) {
            objMeta.message = 'No JSON body found';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        System.debug('jsonString:: ' + jsonString);


        if(String.isNotBlank(jsonString)) {

            amenityObj = (AmenityBookingWrapper)JSON.deserialize(jsonString,AmenityBookingWrapper.class);
            System.debug('amenityObj: ' + amenityObj);

            if(amenityObj.action == null || (!amenityObj.action.equalsIgnoreCase('draft') && !amenityObj.action.equalsIgnoreCase('submit') )) {
                objMeta.message = HDApp_Constants.commonErrorMsg;
                objMeta.status_code = 6;
                objMeta.title = mapStatusCode.get(6);
                objMeta.developer_message = 'Required parameter "action". With expected values : draft/submit';

                returnResponse.meta_data = objMeta;
                return returnResponse;
            }

            if(String.isNotBlank(amenityObj.fm_case_id)) {
                List<FM_Case__c> fmCase = [SELECT id FROM FM_Case__c WHERE id=:amenityObj.fm_case_id];
                
                if(fmCase == null || fmCase.isEmpty()) {
                    objMeta.message = 'No Fm Case found for given fm_case_id';
                    objMeta.status_code = 3;
                    objMeta.title = mapStatusCode.get(3);
                    objMeta.developer_message = null;
                    //objMeta.is_success = false;

                    returnResponse.meta_data = objMeta;
                    return returnResponse;
                }   
            }
            if(String.isNotBlank(amenityObj.account_id) && String.isNotBlank(amenityObj.booking_unit_id) ) {
                List<Account> acc = [SELECT id FROM Account WHERE id=:amenityObj.account_id];
                List<Booking_Unit__c> BU = [SELECT id
                                                 , Tenant__c
                                                 , Resident__c
                                                 , Booking__r.Account__c 
                                            FROM Booking_Unit__c
                                            WHERE id=:amenityObj.booking_unit_id];
                if(!acc.isEmpty() && !BU.isEmpty() ) {
                    if(!ValidateUnitToAccount(acc[0], BU[0])) {
                        objMeta.message = 'The account_id & booking_unit_id are not related';
                        objMeta.status_code = 3;
                        objMeta.title = mapStatusCode.get(3);
                        objMeta.developer_message = null;
                        //objMeta.is_success = false;

                        returnResponse.meta_data = objMeta;
                        return returnResponse;
                    }
                }
                else {
                    objMeta.message = acc.isEmpty() ? 'Invalid account_id' : BU.isEmpty() ? 'Invalid booking_unit_id' : '';
                    objMeta.status_code = 3;
                    objMeta.title = mapStatusCode.get(3);
                    objMeta.developer_message = null;
                    //objMeta.is_success = false;

                    returnResponse.meta_data = objMeta;
                    return returnResponse;
                }
            }
            else {
                objMeta.message = String.isBlank(amenityObj.account_id)  ? 'Missing parameter value : account_id' : String.isBlank(amenityObj.booking_unit_id) ? 'Missing parameter value : booking_unit_id' : '';
                objMeta.status_code = 3;
                objMeta.title = mapStatusCode.get(3);
                objMeta.developer_message = null;
                //objMeta.is_success = false;

                returnResponse.meta_data = objMeta;
                return returnResponse;
            }

            try {

                //if( String.isBlank(amenityObj.account_id) && String.isBlank(amenityObj.booking_unit_id) ) {
                //    isGuestBooking = true;
                //}

                list<Resource__c> resource = [SELECT Id
                                                   , Name
                                                   , Contact_Person_Email__c
                                                   , Contact_Person_Phone__c
                                                   , Contact_Person_Name__c
                                                   , Deposit__c
                                                   , Chargeable_Fee_Slot__c
                                                   , Deposit_for_Guest__c
                                                   , Chargeable_Fee_Slot_for_Guest__c
                                                   , No_of_deposit_due_days__c
                                                   , Amenity_Admin__c
                                                   , Amenity_Icon__c
                                              FROM Resource__c
                                              WHERE id =:amenityObj.amenity_id ];
                System.debug('resource: ' + resource);

                List<Resource_Slot__c> resSlot = [SELECT id
                                                       , Name
                                                       , Start_Time_p__c
                                                       , End_Time_p__c
                                                       , Resource__c
                                                       , Start_Date__c
                                                       , End_Date__c
                                                FROM Resource_Slot__c
                                                WHERE id =: amenityObj.amenity_slot_id];
                System.debug('resSlot:: ' + resSlot);                               


                String devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                                get('Amenity Booking').getRecordTypeId();

                FM_Case__c objFMCase = new FM_Case__c();
                if(String.isNotBlank(amenityObj.fm_case_id)) {
                    objFMCase.id = amenityObj.fm_case_id;
                }
                /*Populating data from JSON on FM Case*/

                if(String.isNotBlank(amenityObj.booking_unit_id)) { objFMCase.Booking_Unit__c = amenityObj.booking_unit_id; }
                if(String.isNotBlank(amenityObj.amenity_id)) { objFMCase.Resource__c = amenityObj.amenity_id; }
                if(String.isNotBlank(amenityObj.amenity_slot_id)) { objFMCase.Resource_SlotID__c = amenityObj.amenity_slot_id; }
                
                
                objFMCase.No_of_guests__c = amenityObj.no_of_guests != null ? amenityObj.no_of_guests : null;
                objFMCase.Description__c = amenityObj.comments;
                objFMCase.Booking_Date__c = String.isNotBlank(amenityObj.booking_date) ? Date.valueOf(amenityObj.booking_date) : null;
                objFMCase.Origin__c='Portal';  //'App-Hello Damac';
                objFMCase.RecordTypeId = devRecordTypeId;

                if( amenityObj.action.equalsIgnoreCase('draft') ) {
                    objFMCase.Status__c = 'Draft Request';  //before payment scenario;
                }
                else if( amenityObj.action.equalsIgnoreCase('submit') ) {
                    objFMCase.Amenity_Booking_Status__c = 'Booking Confirmed';
                    objFMCase.Status__c = 'Closed';
                }
                
                objFmCase.isHelloDamacAppCase__c = true;  //For cases created from HD APP
                objFMCase.Booking_Start_Time_p__c = resSlot[0].Start_Time_p__c;
                objFMCase.Booking_End_Time_p__c = resSlot[0].End_Time_p__c;
                objFMCase.Resource_Booking_Type__c = 'FM Amenity';
                objFMCase.Contact_Email__c = resource[0].Contact_Person_Email__c;
                objFMCase.Contact_Mobile__c = resource[0].Contact_Person_Phone__c;
                objFMCase.Contact_person__c = resource[0].Contact_Person_Name__c;

                //fetching the BU
                if(String.isNotBlank(amenityObj.booking_unit_id)) {

                    objBU = [SELECT id
                                  , Unit_Name__c
                                  , Booking__c 
                                  , Booking__r.Account__c
                            FROM Booking_Unit__c
                            WHERE Id =:amenityObj.booking_unit_id];
                }

                if(String.isNotBlank(amenityObj.account_id)) {
                    List<Account> lstAccount = new List<Account>();
                    lstAccount = [ SELECT Id
                                        , Email__pc
                                        , Mobile_Phone_Encrypt__pc
                                        , IsPersonAccount
                                        , Email__c
                                   FROM Account
                                   WHERE Id =: amenityObj.account_id ];
                    System.debug('lstAccount---------->'+lstAccount);
                    System.debug('objBU----------->'+objBU);
                    System.debug('objBU.Booking__r.Account__c--------->'+objBU.Booking__r.Account__c);
                    if(lstAccount.size() >0 && String.isNotBlank(objBU.Booking__r.Account__c)) {
                        isTenant = objBU.Booking__r.Account__c == lstAccount[0].Id ? false : true;
                    }
                    objFMCase.Initiated_by_tenant_owner__c = isTenant ? 'Tenant' : 'Owner';
                    if(isTenant && lstAccount.size() >0) {
                        objFMCase.Tenant__c = lstAccount[0].Id;
                        objFMCase.Account__c = objBU.Booking__r.Account__c;
                    }
                    else if(lstAccount.size() >0) {
                        //objSelectedAcc = lstAccount[0];
                        objFMCase.Account__c = objFMCase.Tenant__c = lstAccount[0].Id;
                    }
                    objFMCase.Tenant_Email__c = lstAccount[0].IsPersonAccount ? lstAccount[0].Email__pc : lstAccount[0].Email__c ;
                    System.debug('=isTenant====' +isTenant);
                }

                List<Location__c> lstBuildings = getBuildingFromBU(objBU);
                //Task taskToCollectFee = new Task();


                //if(isGuestBooking) {
                //  taskToCollectFee.Assigned_User__c = 'Amenity Admin';
                //    if(String.isNotBlank(resource[0].Amenity_Admin__c)) {
                //        objFMCase.Admin__c = taskToCollectFee.OwnerId = resource[0].Amenity_Admin__c;
                //    }
                //}
                //else{

                //  if(lstBuildings.size() > 0 && lstBuildings[0].FM_Users__r.size() > 0) {
                //        taskToCollectFee.Assigned_User__c = lstBuildings[0].FM_Users__r[0].FM_Role__c == 'FM Admin'
                //                            ? 'FM Admin' : 'Master Community Admin';
                //        objFMCase.Admin__c = taskToCollectFee.OwnerId = lstBuildings[0].FM_Users__r[0].FM_User__c;
                //    }

                //}
                if(lstBuildings.size() > 0 && lstBuildings[0].FM_Users__r.size() > 0) {
                    //objFMCase.Admin__c = taskToCollectFee.OwnerId =
                    //                            lstBuildings[0].FM_Users__r[0].FM_User__c;
                    objFMCase.Admin__c = lstBuildings[0].FM_Users__r[0].FM_User__c;
                }

                System.debug('objFMCase before submitting:: ' + objFMCase);

                upsert objFMCase;
                System.debug('objFMCase after submitting:: ' + objFMCase);

                if(String.isNotBlank(objFMCase.Id)) {
                    objFMCase = getAmenityBookingCase(objFMCase.Id);
                    System.debug('objFMCase after query:: ' + objFMCase);
                }

                //for return response
                //returnObj.FmCaseId = String.isNotBlank(objFMCase.Id) ? objFMCase.Id : '';
                //returnObj.FmCaseNumber = String.isNotBlank(objFMCase.Id) ? objFMCase.Name : '';
                //returnObj.FmCaseStatus = objFMCase.Status__c; 
                //returnObj.status = String.isNotBlank(objFMCase.Id) ? 'success' : 'failed';
                //returnObj.message = 'Successfull';

                objData.fm_case_id = objFMCase.Id;
                objData.amenity_name = objFMCase.Resource__r.Name;
                objData.booking_date = Datetime.newInstance(objFMCase.Booking_Date__c.year(), objFMCase.Booking_Date__c.month(), objFMCase.Booking_Date__c.day()).format('yyyy-MM-dd');
                objData.start_time = objFMCase.Booking_Start_Time_p__c;
                objData.end_time = objFMCase.Booking_End_Time_p__c;
                objData.fm_case_number = objFMCase.Name;
                objData.fm_case_creation_date = Datetime.newInstance(objFMCase.CreatedDate.year(), objFMCase.CreatedDate.month(), objFMCase.CreatedDate.day()).format('yyyy-MM-dd');
                objData.amenity_booking_status = objFMCase.Amenity_Booking_Status__c;
                objData.location_latitude = String.isNotBlank(objFmCase.Amenity_location_Latitude__c) ? Decimal.valueOf(objFmCase.Amenity_location_Latitude__c) : null;
                objData.location_longitude = String.isNotBlank(objFmCase.Amenity_location_Longitude__c) ? Decimal.valueOf(objFmCase.Amenity_location_Longitude__c) : null;
                objData.amenity_icon = resource.size() > 0 && String.isNotBlank(resource[0].Amenity_Icon__c) ? resource[0].Amenity_Icon__c : null;

                objMeta.status_code = 1;
                objMeta.message = 'Successful';


                //Creating task for FM Admin if fee is involved
                //if(((resource[0].Deposit__c !=NULL && resource[0].Deposit__c > 0)
                //    ||( resource[0].Chargeable_Fee_Slot__c !=NULL && resource[0].Chargeable_Fee_Slot__c > 0))
                //    || ((resource[0].Deposit_for_Guest__c !=NULL && resource[0].Deposit_for_Guest__c > 0)
                //    || (resource[0].Chargeable_Fee_Slot_for_Guest__c !=NULL && resource[0].Chargeable_Fee_Slot_for_Guest__c > 0))) {


                //        taskToCollectFee.whatId = objFMCase.Id;
                //        taskToCollectFee.Subject = Label.FM_CollectFeeTaskLabel;
                //        taskToCollectFee.Status = 'Not Started';
                //        taskToCollectFee.Priority = 'Normal';
                //        taskToCollectFee.Process_Name__c = 'Amenity Booking';
                //        taskToCollectFee.ActivityDate = Date.today().addDays(2);
                //        insert taskToCollectFee;

                //}

                 //Creating task for FM Admin if fee is involved
                //if(resource[0].Deposit__c !=NULL && resource[0].Deposit__c != 0
                //        ||( resource[0].Chargeable_Fee_Slot__c !=NULL
                //            && resource[0].Chargeable_Fee_Slot__c != 0)) {
                //    taskToCollectFee.whatId = objFMCase.Id;
                //    taskToCollectFee.Subject = Label.FM_CollectFeeTaskLabel;
                //    taskToCollectFee.Status = 'Not Started';
                //    taskToCollectFee.Priority = 'Normal';
                //    taskToCollectFee.Process_Name__c = 'Amenity Booking';
                //    taskToCollectFee.ActivityDate = Date.today().addDays(2);
                //    taskToCollectFee.Assigned_User__c = 'FM Admin';
                //    insert taskToCollectFee;
                //}

            }//try end
            catch(System.Exception excp) {
                insert new Error_Log__c(Process_Name__c = 'Amenity Booking',
                                        Account__c = String.isNotBlank(amenityObj.account_id) ? amenityObj.account_id : null,
                                        Booking_Unit__c = String.isNotBlank(amenityObj.booking_unit_id) ? amenityObj.booking_unit_id : null,
                                        Error_Details__c = excp.getMessage() + ' for amenity_id : '+amenityObj.amenity_id + ' & slotId : '+amenityObj.amenity_slot_id);
                //returnObj.status = 'failed';
                //returnObj.message = excp.getMessage() + ' for amenity_id : '+amenityObj.amenity_id + ' & slotId : '+amenityObj.amenity_slot_id;

                objMeta.status_code = 2;
                objMeta.message = excp.getMessage();

            }//catch end
        }

        //returnObj.account_id = String.isNotBlank(amenityObj.account_id) ? amenityObj.account_id : '';
        //returnObj.booking_unit_id = String.isNotBlank(amenityObj.booking_unit_id) ? amenityObj.booking_unit_id : '';
        //System.debug('returnObj :: ' + returnObj);

        objMeta.title = mapStatusCode.get(objMeta.status_code);
        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;

        System.debug('returnResponse :: ' + returnResponse);

        return returnResponse;

    }

    public static boolean ValidateUnitToAccount(Account account, Booking_Unit__c unit) {
        Boolean isAccountValid;
        System.debug('unit:: ' + unit);
        System.debug('account:: ' + account);

        if(unit.Tenant__c == account.id || unit.Resident__c == account.id || unit.Booking__r.Account__c == account.id) {
            isAccountValid = true;
        }
        else {
            isAccountValid = false;
        }
        System.debug('isAccountValid: ' + isAccountValid);
        return isAccountValid;
    }

    public static list<Location__c> getBuildingFromBU(Booking_Unit__c BUobj) {

        List<Location__c> lstBuildings = new List<Location__c>();
        //list<Booking_Unit__c> Bu = [SELECT id, Unit_Name__c FROM Booking_Unit__c WHERE id =: BUobj.Id];
        System.debug('BUobj: ' + BUobj);

        if(BUobj == null) {
            return null;
        }

        if(BUobj.Unit_Name__c != null) {
            String buildingName = BUobj.Unit_Name__c.split('/')[0];
            
            lstBuildings = [SELECT  Id
                                    , Name
                                    , Property_Name__c
                                    , ( SELECT  FM_User__c
                                                , FM_User__r.Email
                                                , FM_Role__c
                                                , FM_User__r.Name
                                        FROM FM_Users__r
                                        WHERE FM_Role__c = 'FM Admin'
                                        OR FM_Role__c = 'Master Community Admin'
                                        LIMIT 1)
                            FROM Location__c
                            WHERE Name =: buildingName
                            AND RecordType.DeveloperName = 'Building'
                            LIMIT 1];
            System.debug('===lstBuildings.size()==='+lstBuildings.size());
        }
        
        return lstBuildings;
    }

    public static FM_Case__c getAmenityBookingCase(String fmCaseId) {

        List<FM_Case__c> lstFMCase = [SELECT id
                                           , Name
                                           , Resource__c
                                           , Resource__r.Name
                                           , Status__c 
                                           , No_of_guests__c
                                           , Description__c
                                           , Booking_Date__c
                                           , Amenity_Booking_Status__c
                                           , Booking_Start_Time_p__c
                                           , Booking_End_Time_p__c
                                           , Resource_Booking_Type__c
                                           , Contact_Email__c
                                           , Contact_Mobile__c
                                           , Contact_person__c
                                           , CreatedDate
                                           , Amenity_location_Latitude__c
                                           , Amenity_location_Longitude__c
                                      FROM FM_Case__c 
                                      WHERE Id =: fmCaseId];
        
        return lstFMCase.size() > 0 ? lstFMCase[0] : null;                             
    }
    
    //Amenity wrapper class
    public class AmenityBookingWrapper {
        //public String account_id;    //0010Y00000MaKkw
        //public String booking_unit_id;    //a0x25000000OnEw
        //public String amenity_id;   //a4b250000013QUv
        //public String amenity_slot_id;   //a4a25000000FKNR
        //public String noOfBbqpit;   //1
        //public String swimmingPoolUsage;    //No
        //public String noOfHost; //5
        //public String noOfGuest;    //5
        //public String description;  //test
        //public String comments; //test
        //public String firstname;    //test
        //public String lastname; //test
        //public String email;    //test@test.com
        //public String mobCountryCode;   //India: 0091
        //public String mobile;   //123456789
        //public String bookingDate;  //2020-07-15

        public String action;
        public String fm_case_id;
        public String account_id;
        public String booking_unit_id;
        public String amenity_id;
        public String amenity_slot_id;

        public String booking_date;
        public Integer no_of_guests;
        public String comments;
    }

    public class SubmitSRResponseWrapper {
        public String status;
        public String message;
        public String account_id;
        public String booking_unit_id;
        public String FmCaseId;
        public String FmCaseNumber;
        public String FmCaseStatus;
    }

    /*New*/
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_meta_data {
        public Integer status_code;
        public String message;
        public String title;
        public String developer_message;   
    }

    public class cls_data {
        public String fm_case_id;
        public String amenity_name;
        public String booking_date;
        public String start_time;
        public String end_time;
        public String fm_case_number;
        public String fm_case_creation_date; 
        public String amenity_booking_status;
        public Decimal location_latitude;
        public Decimal location_longitude;
        public String amenity_icon;/*Added on 22/09/2020 as discussed with Charith*/
    }

}