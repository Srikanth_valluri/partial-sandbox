public class Invocable_PromoLetter {
    @InvocableMethod
    public static void UpdateBuyer(List<Id> stepId) {
        for (New_Step__c step :[SELECT Id, Service_Request__c FROM New_Step__c WHERE Id IN :stepId]) {
            EvaluateCustomCode(step);
        }
    }

    public static String EvaluateCustomCode(New_Step__c step) {
        system.debug('#### invoking CC_PromoLetter');
        String retStr = 'Success';
        List<Id> BookingIds = new List<Id> ();
        List<NSIBPM__SR_Doc__c> SRDocList = new List<NSIBPM__SR_Doc__c> ();
        Map<String, Id> promoMap = new Map<String, Id> ();
        try {
            for (NSIBPM__Document_Master__c DM :[SELECT Id, Name, NSIBPM__Code__c
                 FROM NSIBPM__Document_Master__c
                 WHERE Promotion_Letter__c = true]) {
                promoMap.put(DM.name, DM.id);
            }

            for (Booking_Unit__c BU :[SELECT Id, Related_Promotion_Title__c, Registration_ID__c, Booking__r.Deal_SR__c
                                      FROM Booking_Unit__c
                                      WHERE Status__c != 'Removed' AND
                                      Related_Promotion__c != null AND
                                      Booking__r.Deal_SR__c = :step.Service_Request__c]) 
            {
                if (BU.Related_Promotion_Title__c != null && promoMap.get(BU.Related_Promotion_Title__c) != null) {
                    NSIBPM__SR_Doc__c SRDoc = new NSIBPM__SR_Doc__c();
                    SRDoc.NSIBPM__Service_Request__c = step.Service_Request__c;
                    SRDoc.Booking_Unit__c = BU.id;
                    SRDoc.NSIBPM__Generate_Document__c = true;
                    SRDoc.NSIBPM__Document_Master__c = promoMap.get(BU.Related_Promotion_Title__c);
                    SRDoc.name = BU.Registration_ID__c + '- Promotion Letter';
                    SRDoc.NSIBPM__Status__c = 'Generated';
                    SRDoc.NSIBPM__Is_Not_Required__c = true;
                    SRDocList.add(SRDoc);
                }
            }
            if (!SRDocList.isEmpty()) {
                insert SRDocList;
            }
        } catch(Exception ex) {
            retStr = '#### Exception at line number = ' + ex.getLineNumber() + ' , Exception Message = ' + ex.getMessage();
        }
        return retStr;
    }
}