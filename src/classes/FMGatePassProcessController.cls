public without sharing virtual class FMGatePassProcessController {

    public list<SelectOption> lstUnits { get; set; }
    public Booking_Unit__c objUnit { get; set; }
    public Boolean isEdittable{get;set;}
    public Boolean isMandatory{get;set;}
    public FM_Case__c objFMCase { get; set; }
    public string strFMCaseId{get;set;}
    public String strSRType { get; set; }
    public String strSelectedUnit { get; set; }
    public String[] valuesOfDrawings { get; set; }
    public SelectOption[] valueListOfDrawings { get; set; }
    @testVisible protected String strAccountId ;
    public String[] values { get; set; }
    public SelectOption[] valueList { get; set; }
    protected List<FM_Case__c> instanceList;
    public decimal minimumOutstandingCharge{get;set;}
    public string shortDurationGatePassFee{get; set;}
    public string longDurationGatePassFee{get; set;}
    public string longDurationGatePassFee6Months{get; set;}
    public string longDurationGatePassFee12Months{get; set;}
    public string savedOrSubmitted{get;set;}

    public transient String strDocumentBody { get; set; }
    public transient String strDocumentName { get; set; }
    public transient String deleteAttRecId { get; set; }

    public list<FM_Documents__mdt> lstDocuments { get; set; }
    public list<SR_Attachments__c> lstRecentAttachment { get; set; }
    public list<SR_Attachments__c> lstUploadedDocs { get; set; }
    @testVisible protected map<String, FM_Documents__mdt> mapProcessDocuments ;
    @testVisible protected map<String, SR_Attachments__c> mapUploadedDocs ;
    protected String contractorConsultantSuccessMessage = 'Consultant/Contractor Notified Successfully';

    public FMGatePassProcessController() {
        instanceList=new List<FM_Case__c>();
        minimumOutstandingCharge = 0;
        valueListOfDrawings=new List<SelectOption>();
        valuesOfDrawings=new List<String>();
        isEdittable=true;
        valueList = new SelectOption[0];
        values = new String[0];
        isMandatory=false;
        strFMCaseId = ApexPages.currentPage().getParameters().get('Id');
        if(String.isBlank( strFMCaseId )){
            strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
            strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
            strSRType = ApexPages.currentPage().getParameters().get('SRType');
        }
        system.debug('== strAccountId =='+strAccountId );
        instanceList=FM_Utility.getExistingFMCase( 'Work_Permit', strAccountId, strSelectedUnit );
        System.debug('instanceList===='+instanceList);
        if((String.isNotBlank(strSelectedUnit)
            || String.isNotBlank(strFMCaseId))
            && instanceList.isEmpty()){
                init();
        }
        if(!instanceList.isEmpty()){
            objFMCase=new FM_Case__c();
            objFMCase.Account__c=instanceList[0].Account__c;
            objFMCase.status__c ='Draft Request';
            objFMcase.origin__c ='Walk-In';
            objFMCase.Booking_Unit__c=instanceList[0].Booking_unit__c;
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Gate Pass Request').getRecordTypeId();
            objFMCase.Request_Type_DeveloperName__c = strSRType ;
            objFMCase.Request_Type__c = 'Gate Pass Request';
            objFMCase.Description__c=instanceList[0].Description__c;
            objFMCase.Person_To_Collect__c=instanceList[0].Person_To_Collect__c;
            objFMCase.Contact_person__c=instanceList[0].Contact_person__c;
            objFMCase.Contact_person_contractor__c=instanceList[0].Contact_person_contractor__c;
            objFMCase.Mobile_no__c=instanceList[0].Mobile_no__c;
            objFMCase.Mobile_no_contractor__c=instanceList[0].Mobile_no_contractor__c;
            objFMCase.Contact_Email__c=instanceList[0].Contact_Email__c;
            objFMCase.Office_tel__c=instanceList[0].Office_tel__c;
            objFMCase.Duration_For_Gate_Pass__c=instanceList[0].Duration_For_Gate_Pass__c;
            objFMCase.Office_tel_contractor__c=instanceList[0].Office_tel_contractor__c;
            objFMCase.Mobile_Country_Code__c=instanceList[0].Mobile_Country_Code__c;
            objFMCase.Mobile_Country_Code_2__c=instanceList[0].Mobile_Country_Code_2__c;
            objFMCase.Mobile_Country_Code_3__c=instanceList[0].Mobile_Country_Code_3__c;
            objFMCase.Mobile_Country_Code_4__c=instanceList[0].Mobile_Country_Code_4__c;
            objFMCase.Parent_case__c = instanceList[0].Id;
            objFMCase.Email__c = instanceList[0].Email__c;
            objFMCase.Email_2__c = instanceList[0].Email_2__c;
            objUnit = getUnitDetails(strSelectedUnit) ;
            if( String.isNotBlank( strSRType ) && objUnit != NULL ) {
                processDocuments();
            }

        }
        List<Location__c> objList=new List<Location__c>();
        objList=[select As_Built_Drawing_Fee__c
                       ,Long_Duration_Gate_Pass_Fee__c
                       ,Short_Duration_Gate_Pass_Fee__c
                       ,Long_Duration_Gate_Pass_Fee_6_Months__c
                       ,Long_Duration_Gate_Pass_Fee_12_Months__c  
                   from Location__c 
                   where id=:objUnit.Inventory__r.Building_Location__c];
        if(!objList.isEmpty()){
              System.debug('obj=-========'+objList);
              longDurationGatePassFee=!String.isBlank(String.valueof(objList[0].Long_Duration_Gate_Pass_Fee__c)) ? String.valueof(objList[0].Long_Duration_Gate_Pass_Fee__c) : '0';
              shortDurationGatePassFee=!String.isBlank(String.valueof(objList[0].Short_Duration_Gate_Pass_Fee__c)) ? String.valueof(objList[0].Short_Duration_Gate_Pass_Fee__c) : '0';
              longDurationGatePassFee6Months=!String.isBlank(String.valueof(objList[0].Long_Duration_Gate_Pass_Fee_6_Months__c)) ? String.valueof(objList[0].Long_Duration_Gate_Pass_Fee_6_Months__c) : '0';
              longDurationGatePassFee12Months=!String.isBlank(String.valueof(objList[0].Long_Duration_Gate_Pass_Fee_12_Months__c)) ? String.valueof(objList[0].Long_Duration_Gate_Pass_Fee_12_Months__c) : '0';
              System.debug('longDurationGatePassFee-------'+longDurationGatePassFee);
              System.debug('shortDurationGatePassFee-------'+shortDurationGatePassFee);
         }
         if( objUnit != NULL ) {
            FmIpmsRestServices.DueInvoicesResult objResponse;
            try {
                 objResponse =FmIpmsRestServices.getDueInvoices( objUnit.Registration_Id__c,'',objUnit.Inventory__r.Property_Code__c );
                 system.debug('== objResult =='+objResponse);              
            }
            catch( Exception e ) {
                system.debug( '----Exception while fetching oustanding balance----'+e.getMessage() );
            }
            if( objResponse != NULL ) {
                objFMCase.Outstanding_service_charges__c = !String.isBlank( objResponse.totalDueAmount ) ? objResponse.totalDueAmount : '0';
            } else {
                objFMCase.Outstanding_service_charges__c = '0';
            }
        }
        FM_process__mdt fmProcessMetadata=[select Minimum_Outstanding_Charge__c from FM_process__mdt where DeveloperName =:objFMCase.Request_Type_DeveloperName__c];
        if(fmProcessMetadata != NULL){
            minimumOutstandingCharge = fmProcessMetadata.Minimum_Outstanding_Charge__c != NULL ? fmProcessMetadata.Minimum_Outstanding_Charge__c : 0;
        }
        system.debug( '==objFMCase.Outstanding_service_charges__c=='+objFMCase.Outstanding_service_charges__c );
        System.debug('objFMCase===='+objFMCase);
    }

    protected FMGatePassProcessController(Boolean shouldCall) {}

    public void init() {
        initializeFMCase();
        System.debug('objFMCase.Submitted__c======'+objFMCase.Submitted__c);
        if(objFMCase.Submitted__c==true){
            isEdittable=false;
        }
        System.debug('isEdittable======'+isEdittable);
        if(strFMCaseId == null){
            //Initialize the Additional details map.
            //mapAdditionalDetails = new map<String, list<FM_Additional_Detail__c>>();
            //FM_Additional_Detail__c objDetail = new FM_Additional_Detail__c();
            //objDetail.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
            //objDetail.Emergency_Contact_Case__c = objFMCase.Id ;
            //mapAdditionalDetails.put( 'Emergency Contact' , new list<FM_Additional_Detail__c> { objDetail } ) ;
        }else{
            system.debug('*****entered else*****');
            strSelectedUnit = objFMCase.Booking_Unit__c;
            System.debug('strSelectedUnit======'+strSelectedUnit);
        }
        objUnit = getUnitDetails(strSelectedUnit) ;
        System.debug('objUnit.Inventory__r.Building_Location__c'+objUnit.Inventory__r.Building_Location__c);
        if( String.isNotBlank( strSRType ) && objUnit != NULL ) {
            processDocuments();
        }
        System.debug('-----objFMCase.id-----'+objFMCase.id);
        System.debug('=====lstDocuments===='+lstDocuments);
        //getfieldValues();
    }

    public void getfieldValues(){
        /*Schema.DescribeFieldResult fieldResult = FM_Case__c.Permit_To_Work_For__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        System.debug('ple===='+ple);
       for( Schema.PicklistEntry f : ple)
       {
           System.debug('f-------'+f);
           System.debug('f.get'+f.getLabeL());
           valueListOfDrawings.add(new SelectOption(f.getLabel(),f.getLabel()));
       }*/

    }

    public static Booking_Unit__c getUnitDetails( String strUnitId ) {
        if (String.isBlank(strUnitId)) {
            return NULL;
        }
        return  FM_Utility.getUnitDetails(strUnitId);
    }

    protected void initializeFMCase() {
        system.debug('strFMCaseId*****'+strFMCaseId);
        
        if(strFMCaseId == null){
            
            objFMCase = new FM_Case__c();
            objFMCase = setCaseOrigin(objFMCase);
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Gate Pass Request').getRecordTypeId();
            objFMCase.Account__c = strAccountId ;
            objFMCase.Booking_Unit__c = strSelectedUnit ;
            objFMCase.Request_Type_DeveloperName__c = strSRType ;
            objFMCase.Request_Type__c = 'Gate Pass Request';
            objFMCase.status__c = 'Draft Request';
        } else {
            system.debug('Inside existing case*****');
            objFMCase = FM_Utility.getCaseDetails(strFMCaseId);
            System.debug('objFMCase======'+objFMCase);
            strAccountId = objFMCase.Account__c ;
            strSelectedUnit = objFMCase.Booking_Unit__c ;
            strSRType = objFMCase.Request_Type_DeveloperName__c ;
        }
    }

    public PageReference createRequestForWorkPermit() {
        if (objFMCase != NULL ) {
            System.debug('objFMCase = ' + objFMCase);
            createMethod();
            List<Profile> userProfile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
            String guestUserProfileName = '';
            if (!userProfile.isEmpty()) {
                guestUserProfileName = userProfile[0].Name;
            }
            if (guestUserProfileName == 'Facilities Management Site Profile') {
                PageReference pgRfSaved = Page.ThankYouPageSiteRedirect;
                pgRfSaved.setRedirect(true);
                savedOrSubmitted = 'saved';
                pgRfSaved.getParameters().put('savedOrSubmitted', savedOrSubmitted);
                return pgRfSaved;
            } else {
                PageReference objPage = new PageReference('/' + objFMCase.Id);
                return objPage ;
            }
        }
        return NULL;
    }

    public void createCaseShowUploadDoc(){
        createMethod();
        System.debug('objFMCase-----'+objFMCase);
        System.debug('oBjFMCase.id------'+oBjFMCase.id);
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            processDocuments();
        }
        strFMCaseId=objFMCase.id;
        System.debug('strFMCaseId----'+strFMCaseId);
    }
    public void createMethod(){
        
        if(!String.isBlank(objFMCase.Email__c) || !String.isBlank(objFMCase.Email_2__c)){
            objFMCase.Contact_Email__c = !String.isBlank(objFMCase.Email__c) ? objFMCase.Email__c : objFMCase.Email_2__c ;
        }
        objFMCase.Long_Duration_Gate_Pass_Fee__c = !String.isBlank(longDurationGatePassFee) ? Decimal.valueOf(longDurationGatePassFee) : 0;
        objFMCase.Short_Duration_Gate_Pass_Fee__c  = !String.isBlank(shortDurationGatePassFee) ? Decimal.valueOf(shortDurationGatePassFee) : 0;
        objFMCase.Long_Duration_Gate_Pass_Fee_6_Months__c = !String.isBlank(longDurationGatePassFee6Months) ? Decimal.valueOf(longDurationGatePassFee6Months) : 0;
        objFMCase.Long_Duration_Gate_Pass_Fee_12_Months__c = !String.isBlank(longDurationGatePassFee12Months) ? Decimal.valueOf(longDurationGatePassFee12Months) : 0;
        upsert objFMCase ;
    }

    protected virtual FM_Case__c setCaseOrigin(FM_Case__c fmCase) {
        fmCase.Origin__c='Walk-In';
        return fmCase;
    }

    public PageReference submitRequestForWorkPermit() {
        if( objFMCase != NULL ) {
            System.debug('objFMCase--------'+objFMCase);
            if(objFMCase.id==null){
                insert objFMCase;
            }
            if(!String.isBlank(objFMCase.Email__c) || !String.isBlank(objFMCase.Email_2__c)){
                objFMCase.Contact_Email__c = !String.isBlank(objFMCase.Email__c) ? objFMCase.Email__c : objFMCase.Email_2__c ;
            }
            //objFMCase.Permit_To_Work_For__c=String.join(valuesOfDrawings ,';');
            objFMCase.Long_Duration_Gate_Pass_Fee__c = !String.isBlank(longDurationGatePassFee) ? Decimal.valueOf(longDurationGatePassFee) : 0;
            objFMCase.Short_Duration_Gate_Pass_Fee__c  = !String.isBlank(shortDurationGatePassFee) ? Decimal.valueOf(shortDurationGatePassFee) : 0;
            objFMCase.Long_Duration_Gate_Pass_Fee_6_Months__c = !String.isBlank(longDurationGatePassFee6Months) ? Decimal.valueOf(longDurationGatePassFee6Months) : 0;
            objFMCase.Long_Duration_Gate_Pass_Fee_12_Months__c = !String.isBlank(longDurationGatePassFee12Months) ? Decimal.valueOf(longDurationGatePassFee12Months) : 0;
            objFMCase.Status__c='Submitted';
            objFMCase.Submitted__c=true;
            System.debug('objFMCase.submitted__c====='+objFMCase.Submitted__c);
            if(objFMCase.Submitted__c==true){
                System.debug('objFMCase.submitted__c=====1'+objFMCase.Submitted__c);
                if( String.isNotBlank( objUnit.Unit_Name__c ) ) {
                    list<FM_User__c> lstUsers = FM_Utility.getApprovingUsers( new set<String> { objUnit.Unit_Name__c.split('/')[0] } );
                    System.debug('lstUsers ======'+lstUsers);
                    for( FM_User__c objUser : lstUsers ) {
                        if( String.isNotBlank( objUser.FM_Role__c ) ) {
                          if( objUser.FM_Role__c == 'FM Manager' ) {
                            objFMCase.FM_Manager_Email__c = objUser.FM_User__r.Email;
                          }
                          else if( objUser.FM_Role__c == 'Admin' &&
                               objFMCase.Admin__c == NULL ) {
                               objFMCase.Admin__c = objUser.FM_User__c;
                          }else if(objuser.FM_Role__c == 'Property Manager'){
                              objFMCase.Property_Manager_Name__c = objUser.FM_User__r.Name;
                              objFMCase.Property_Manager_Email__c = objUser.FM_User__r.Email;
                          }else if(objuser.FM_Role__c == 'Fitout Incharge'){
                              objFMCase.Email_5__c = objUser.FM_User__r.Email;
                          }
                        }
                    }
                 }
                String strApprovingUsers='';
                System.debug('objFMCase.Request_Type_DeveloperName__c===='+objFMCase.Request_Type_DeveloperName__c);
                System.debug('objUnit.Property_City__c'+objUnit.Property_City__c);
                List<FM_Approver__mdt> listApproverUser=FM_Utility.fetchApprovers(objFMCase.Request_Type_DeveloperName__c,objUnit.Property_City__c);
                System.debug('listApproverUser====='+listApproverUser);
                if(!listApproverUser.isEmpty()){
                    System.debug('----1----');
                    for(FM_Approver__mdt fmApproverInstance:listApproverUser){
                        System.debug('----2----');
                        strApprovingUsers=strApprovingUsers+fmApproverInstance.Role__c+',';
                    }
                    System.debug('strApprovingUsers==='+strApprovingUsers);
                    strApprovingUsers=strApprovingUsers.removeEnd(',');
                    objFMCase.Approving_Authorities__c=strApprovingUsers;
                    objFMCase.Approval_Status__c='Pending';
                    //objFMCase.Submit_for_Approval__c=true;
                    update objFMCase;
                }
            }
            List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
            String guestUserProfileName='';
            if(!PROFILE.isEmpty()){
                guestUserProfileName= PROFILE[0].Name;
            }
            if(guestUserProfileName=='Facilities Management Site Profile'){
                PageReference pgRf=Page.ThankYouPageSiteRedirect;
                pgRf.setRedirect(true);
                savedOrSubmitted='submitted';
                pgRf.getParameters().put('savedOrSubmitted',savedOrSubmitted);
                return pgRf;
            }else{
                PageReference objPage = new PageReference('/' + objFMCase.Id );
                return objPage ;
            }
        }
        return NULL ;
    }

    public void notifyContractorConsultant() {
        try {
            System.debug('funccc called');
            //if (objFMCase.Id == NULL) {
            System.debug('objFMCase----'+objFMCase);
            createMethod();
            //}

            FM_NotifyGuestUserController objNotification = new FM_NotifyGuestUserController(new ApexPages.StandardController(objFMCase));
            objNotification.createSMSHistoryRecord();
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.CONFIRM, contractorConsultantSuccessMessage
            ));
        } catch(Exception e) {
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, e.getMessage() ) );
        }
    }

    @testVisible
    public virtual void processDocuments() {
        //System.debug('objFMCase.Person_To_Collect__c-----'+objFMCase.Person_To_Collect__c);
        //System.debug('strSRType====='+strSRType);
        //System.debug('objFMCase.Duration_For_Gate_Pass__c-----'+objFMCase.Duration_For_Gate_Pass__c);
        if(String.isBlank(strSRType)){
            strSRType=ApexPages.currentPage().getParameters().get('SRType');
        }
        lstUploadedDocs = NULL;
        lstDocuments = NULL;
        Set<FM_Documents__mdt> listSpecificDocs = new Set<FM_Documents__mdt>();
        List<String> lstOfString = new List<String>();
        if (objFMCase != NULL && objFMCase.Id != NULL) {
            mapUploadedDocs = new map<String, SR_Attachments__c>();
            for ( SR_Attachments__c objAttach : fetchUploadedDocs()) {
                mapUploadedDocs.put(objAttach.Name, objAttach);
            }
            if( mapUploadedDocs != NULL && !mapUploadedDocs.isEMpty() ) {
                lstUploadedDocs = new list<SR_Attachments__c>();
                lstUploadedDocs.addAll(mapUploadedDocs.values());
            }
        }
        mapProcessDocuments = new Map<String, FM_Documents__mdt>();
        lstDocuments = new List<FM_Documents__mdt>();
        System.debug('strSRType======' + strSRType);
        System.debug('objUnit++++++' + objUnit);
        //System.debug('objUnit.Property_City__c = ' + objUnit.Property_City__c);
        for (FM_Documents__mdt objDocMeta : FM_Utility.getDocumentsList( strSRType, objUnit.Property_City__c)) {
            System.debug('objDocMeta = ' + objDocMeta);
            mapProcessDocuments.put(objDocMeta.DeveloperName, objDocMeta);
            System.debug('mapProcessDocuments = ' + mapProcessDocuments);
            System.debug('mapUploadedDocs = ' + mapUploadedDocs);
            if (mapUploadedDocs != NULL && !mapUploadedDocs.containsKey(objDocMeta.MasterLabel)) {
                lstDocuments.add( objDocMeta );
            }
            if (objDocMeta.Mandatory__c == true) {
                lstOfString.add('true');
            }
        }
        System.debug('lstDocuments+++++' + lstDocuments);
        if (lstOfString.size() > 0) {
            isMandatory=true;
        }
        system.debug('maza map---'+mapUploadedDocs);
        for (FM_Documents__mdt ins:lstDocuments) {
            system.debug('maza ins---'+ins);
           if (!mapUploadedDocs.keyset().contains(ins.Document_Name__c)) {
               system.debug('if ichya aat---');
                if (objFMCase.Duration_For_Gate_Pass__c=='2 Days' && ins.Duration__c=='Short Term') {
                    system.debug('chota ins---'+ins);
                    listSpecificDocs.add(ins);
                }
                if ((objFMCase.Duration_For_Gate_Pass__c=='3 Months' || objFMCase.Duration_For_Gate_Pass__c=='12 Months' || objFMCase.Duration_For_Gate_Pass__c=='6 Months') && ins.Duration__c=='Long Term') {
                    system.debug('motha ins---'+ins);
                    listSpecificDocs.add(ins);
                }
           }
        }
        lstDocuments.clear();
        lstDocuments.addAll(listSpecificDocs);
        System.debug('lstDocuments+++++' + lstDocuments);
    }

    public void deleteAttachment() {
        if( String.isNotBlank( deleteAttRecId ) ) {
           delete new SR_Attachments__c( Id = deleteAttRecId );
           processDocuments();
        }
    }

    @testVisible
    protected void insertCustomAttachment( Id fmCaseId, UploadMultipleDocController.data objResponse ) {
        list<SR_Attachments__c> lstCustomAttachments = new list<SR_Attachments__c>();

        strDocumentName = strDocumentName.substring( 0, strDocumentName.lastIndexOf('.') );

        FM_Documents__mdt objDocMeta = mapProcessDocuments != null && mapProcessDocuments.containsKey( strDocumentName ) ?
                                       mapProcessDocuments.get( strDocumentName ) :
                                       NULL ;

        for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            SR_Attachments__c objCustAttach = new SR_Attachments__c();
            objCustAttach.Account__c = strAccountId ;
            objCustAttach.Attachment_URL__c = objFile.url;
            objCustAttach.Booking_Unit__c = strSelectedUnit ;
            objCustAttach.FM_Case__c = fmCaseId ;
            objCustAttach.Name = objDocMeta != NULL ? objDocMeta.MasterLabel : '' ;
            objCustAttach.Type__c = objDocMeta != NULL ? objDocMeta.Type__c : '' ;
            //objCustAttach.isValid__c = true;
            objCustAttach = setDocumentValidity(objCustAttach);
            lstCustomAttachments.add( objCustAttach );
            strDocumentName = '';
            strDocumentBody = '';
        }
        if( !lstCustomAttachments.isEmpty() ) {
            insert lstCustomAttachments ;
            system.debug('==lstCustomAttachments=='+lstCustomAttachments);

            processDocuments() ;
            system.debug('==lstUploadedDocs=='+lstUploadedDocs);
            system.debug('==lstDocuments=='+lstDocuments);
        }
    }

    protected virtual SR_Attachments__c setDocumentValidity(SR_Attachments__c doc) {
        doc.isValid__c = true;
        return doc;
    }

    public list<SR_Attachments__c> fetchUploadedDocs() {
        if( String.isNotBlank( objFMCase.Id ) ) {
            return [ SELECT Id
                          , Type__c
                          , isValid__c
                          , Attachment_URL__c
                          , Name
                       FROM SR_Attachments__c
                      WHERE FM_Case__c =: objFMCase.Id ];
        }
        return new list<SR_Attachments__c>();
    }

    public void uploadDocument() {
        system.debug('== strDocumentBody =='+strDocumentBody);
        system.debug('== strDocumentName =='+strDocumentName);
        //initializeFMCase();
        System.debug('objFMCase.id======'+objFMCase.id);
        System.debug('objFMCase======'+objFMCase);
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            System.debug('=========='+objFMCase != NULL && objFMCase.Id != NULL);
            UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
            list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
            if( String.isNotBlank( strDocumentName ) && String.isNotBlank( strDocumentBody ) ) {
                lstWrapper.add( PenaltyWaiverHelper.makeWrapperObject( EncodingUtil.Base64Encode( RentalPoolTerminationHelper.extractBody( strDocumentBody ) ) ,
                                                                       strDocumentName ,
                                                                       strDocumentName ,
                                                                       objUnit.Registration_Id__c, objFMCase.Id, '1' ) );

            }
            if( !lstWrapper.isEmpty() ) {
                objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper );
                system.debug('== objResponse document upload =='+objResponse);
                if( objResponse != NULL && objResponse.data != NULL ) {
                    insertCustomAttachment( objFMCase.Id, objResponse );
                }
            }
        }
    }
}