/*
Developed By: DAMAC IT Team
Usage:DAMAC_CDC_REST_UTILITY
*/
public class DAMAC_CDC_POST_OBJ_UNIT{

    public String regid;    //Hello World
    public String cdcstatus;    //verified
    public String cdccomments;  //
    public datetime checkindatetime;
    
    public static DAMAC_CDC_POST_OBJ_UNIT parse(String json){
        return (DAMAC_CDC_POST_OBJ_UNIT) System.JSON.deserialize(json, DAMAC_CDC_POST_OBJ_UNIT.class);
    }
    
}