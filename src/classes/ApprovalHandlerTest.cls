@isTest
/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  20/11/2017  Initial Draft
*/

public class ApprovalHandlerTest {
    
     static testMethod void Test1(){
        test.StartTest();
            
            Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Token Refund').getRecordTypeId();
            Case objCase = new Case();            
            objCase.RecordTypeID = devRecordTypeId;
            objCase.Total_Token_Amount__c = 1000;
            objCase.POA_Expiry_Date__c = System.today();
            objCase.IsPOA__c = true;
            objCase.Approving_Authorities__c  = 'VP';
            objCase.Approving_User_Role__c = 'VP';
            objCase.Approving_User_Id__c = UserInfo.getUserId();
            insert objCase;
            
            Set<Id> setCaseIds = new Set<Id>();
            setCaseIds.add(objCase.id);
            
            ApprovalHandler.custLabelVal = 'ON';
            
            ApprovalHandler.processApprovals(setCaseIds);
        
        test.StopTest();
     }    
     
     static testMethod void Test2(){
        test.StartTest();
            Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
            Case objCase = new Case();            
            objCase.RecordTypeID = devRecordTypeId;
            objCase.Total_Token_Amount__c = 1000;
            objCase.POA_Expiry_Date__c = System.today();
            objCase.IsPOA__c = true;
            objCase.Approving_Authorities__c  = 'VP';
            objCase.Approving_User_Role__c = 'VP';
            objCase.Approving_User_Id__c = UserInfo.getUserId();
            insert objCase;
            
            Set<Id> setCaseIds = new Set<Id>();
            setCaseIds.add(objCase.id);
            ApprovalHandler.custLabelVal = 'OFF';
            ApprovalHandler.processApprovals(setCaseIds);
        
        test.StopTest();
     }       
     
    static testMethod void Test3() {
       List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;        
        
        Id caseBCRecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Bounced Cheque SR').RecordTypeId;
        Id bouncedChequeRTId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Bounced Cheque').RecordTypeId;
        Account accObj = new Account(Name = 'Miss. Madina Alieva');
        insert accObj;
        
 
        
        Case bouncedChequeCaseObj = new Case(AccountId = accObj.Id, RecordTypeId = caseBCRecordTypeId, Status = 'New');
        insert bouncedChequeCaseObj;
        
        Calling_List__c bouncedCallObj = new Calling_List__c(Account__c = accObj.Id,Registration_ID__c = '84357',
                                             RecordTypeId =bouncedChequeRTId, Case__c = bouncedChequeCaseObj.Id
                                             , Approving_Authorities__c = 'VP'
                                             , Approving_User_Role__c = 'VP',Approving_User_Id__c = UserInfo.getUserId() );
        insert bouncedCallObj;

        ApprovalHandler.processCallingListApprovals(new Set<Id>{bouncedCallObj.Id});
        
    }
     
}