/**********************************************************************************************************
* Description - The class developed to share Account records with their secondary user and their manager
*
* Version            Date            Author            Description
* 1.0                28/05/18        Monali            Initial Draft
***********************************************************************************************************/
public with sharing class ShareAccount {
    public static void ShareAccountRecord(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        System.debug('=======ShareAccountRecord=='+mapNewRecords);
        System.debug('=======ShareAccountRecord=='+mapOldRecords);
    	//Id loggedInUserId = UserInfo.getUserId();
    	Id corporateAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
    	List<Account> oldAccountList = new List<Account>();
        List<Account> newAccountList = new List<Account>();

        for(Id thisKey : mapNewRecords.keySet()){
            Account newAccount = (Account)mapNewRecords.get(thisKey);
            Account oldAccount = (Account)mapOldRecords.get(thisKey);
            System.debug('====111=====ShareAccountRecord=='+newAccount.Is_Manager_Changed__c);
            if( //newAccount.Account_Owner__c == loggedInUserId && 
            	newAccount.RecordTypeId == corporateAccRTId
             && (newAccount.Secondary_Owner__c != oldAccount.Secondary_Owner__c || newAccount.Is_Manager_Changed__c == true)
            ){          
                System.debug('===222======ShareAccountRecord==');      
                if(oldAccount.Secondary_Owner__c != null){
                	// Remove existing share of secondary USer	
                    oldAccountList.add(oldAccount);
                } 
                if(newAccount.Secondary_Owner__c != null){
                    // update SecondaryOwner of Corporate account
                    newAccountList.add(newAccount);
                }
                //newAccount.Is_Manager_Changed__c = false;
            }
        }

        if(oldAccountList.size() > 0){
            RemoveAccountShare(oldAccountList);
        }

        if(newAccountList.size() > 0){
            CreateAccountShare(newAccountList);
        }
    }

    public static void CreateAccountShare(List<Account> newAccountList){
    	//System.debug('===========CreateAccountShare==='+newAccount);
        AccountTeamMember acctTM ;
        List<Account> accountIdList = new List<Account>();
        Map<Id,List<Account>> mapOwnerAcc = new Map<Id,List<Account>>();
        Map<Id,Set<User>> mapSecOwnerManager = new Map<Id,Set<User>>();
        List<AccountTeamMember> accTeamList = new List<AccountTeamMember>();

        for(Account acc : newAccountList){
            if(!mapOwnerAcc.containsKey(acc.Secondary_Owner__c)){
                System.debug('========if=');
                accountIdList = new List<Account>();
                accountIdList.add(acc);
                mapOwnerAcc.put(acc.Secondary_Owner__c,accountIdList);
            } else {
                System.debug('========else=');
                accountIdList = new List<Account>();
                accountIdList = mapOwnerAcc.get(acc.Secondary_Owner__c);
                accountIdList.add(acc);
                mapOwnerAcc.put(acc.Secondary_Owner__c,accountIdList);
            }
        }   
    	//Id secondaryOwnerId = newAccount.Secondary_Owner__c;
    	System.debug('=========mapOwnerAcc='+mapOwnerAcc);    	

    	for(User userObj : [SELECT Id
                                 , Name
                                 , IsActive
                                 , UserRole.DeveloperName
                                 , ManagerId
                                 , Manager.IsActive
                                 , Manager.UserRole.DeveloperName
                                 , Manager.ManagerId
                                 , Manager.Manager.IsActive
                                 , Manager.Manager.UserRole.DeveloperName
                                 , Manager.Manager.ManagerId
                                 , Manager.Manager.Manager.IsActive
                                 , Manager.Manager.Manager.UserRole.DeveloperName
                                 , Manager.Manager.Manager.ManagerId
                              FROM User 
                             WHERE Id =:mapOwnerAcc.keySet()
                             AND IsActive = true
                             AND (UserRole.DeveloperName LIKE 'DOS%'
                             OR UserRole.DeveloperName LIKE 'HOS%'
                             OR UserRole.DeveloperName LIKE 'HOD%'
                             OR UserRole.DeveloperName LIKE 'General_Manager%')
        ]) {
            Set<User> idLst = new Set<User>();
            if(!mapSecOwnerManager.containsKey(userObj.Id)){
                idLst = new Set<User>();
                idLst = getUsersList(userObj);  
                mapSecOwnerManager.put(userObj.Id,idLst);               
            } else {
                idLst = new Set<User>();
                idLst = mapSecOwnerManager.get(userObj.Id);
                idLst = getUsersList(userObj);  
                mapSecOwnerManager.put(userObj.Id,idLst);
            }
        }

        System.debug('==========mapSecOwnerManager====== '+mapSecOwnerManager);
        for(Id usrId : mapSecOwnerManager.keySet()){
            System.debug('======Secondary of Account====usrId====== '+usrId);
            //List<User> userList =  mapSecOwnerManager.get(usrId);
            for(Account acc : mapOwnerAcc.get(usrId)){
                System.debug('=====accList====to share====== '+acc);
                for(User innerUser : mapSecOwnerManager.get(usrId)){
                    System.debug('==acc=='+ acc +'==Share account with User List===== '+innerUser);
                    System.debug('====Role====== '+innerUser.UserRole.DeveloperName);
                    acctTM = AccountTeamRec(acc.Id, innerUser.Id, innerUser.UserRole.DeveloperName);
                    accTeamList.add(acctTM);
                }
            }
        }

        try{
        	insert accTeamList;
        	System.debug('==========accTeamList==SUCCESS==== '+accTeamList);
        } catch(Exception e){
        	System.debug('==========accTeamList==ERROR==== '+e);
        }
    }

    public static Set<User> getUsersList(User userObj){
        Set<User> idList = new Set<User>();
        System.debug('=========userObj==== '+userObj);
        if(userObj.UserRole.DeveloperName.startsWith('DOS')){
            System.debug('===1======DOS==== '+userObj);
            idList.add(userObj);
            if(String.isNotBlank(userObj.ManagerId) && userObj.Manager.IsActive == true){
                if( userObj.Manager.UserRole.DeveloperName.startsWith('HOS') 
                ){
                    System.debug('==1=======DOS=manager not null=== '+userObj.Manager);    
                    idList.add(userObj.Manager);

                    if(String.isNotBlank(userObj.Manager.ManagerId) && userObj.Manager.Manager.IsActive == true){
                        if( userObj.Manager.Manager.UserRole.DeveloperName.startsWith('HOD') 
                        ){
                            System.debug('=1========HOS=manager not null=== '+userObj.Manager.Manager);
                            idList.add(userObj.Manager.Manager);

                            if(String.isNotBlank(userObj.Manager.Manager.ManagerId) && userObj.Manager.Manager.Manager.IsActive == true){
                                if( userObj.Manager.Manager.Manager.UserRole.DeveloperName.startsWith('General_Manager') 
                                ){
                                    System.debug('==1=====HOD manager not null=== '+userObj.Manager.Manager.Manager);
                                    idList.add(userObj.Manager.Manager.Manager);
                                }
                            }
                        }
                    }
                }
            }
        } else if(userObj.UserRole.DeveloperName.startsWith('HOS')){
            System.debug('==2=====HOS==== '+userObj);
            idList.add(userObj);
            if(String.isNotBlank(userObj.ManagerId) && userObj.Manager.IsActive == true){
                if( userObj.Manager.UserRole.DeveloperName.startsWith('HOD') 
                ){
                    System.debug('===2======HOS manager not null==== '+userObj.Manager);
                    idList.add(userObj.Manager);

                    if(String.isNotBlank(userObj.Manager.ManagerId) && userObj.Manager.Manager.IsActive == true){
                        if( userObj.Manager.Manager.UserRole.DeveloperName.startsWith('General_Manager') 
                        ){
                            System.debug('===2=====HOD manager not null==== '+userObj.Manager.Manager);
                            idList.add(userObj.Manager.Manager);
                        }
                    }
                }
            }
        } else if(userObj.UserRole.DeveloperName.startsWith('HOD')){
            System.debug('==3=====HOD==== '+userObj);
            idList.add(userObj);
            if(String.isNotBlank(userObj.ManagerId) && userObj.Manager.IsActive == true){
                if( userObj.Manager.UserRole.DeveloperName.startsWith('General_Manager') 
                ){
                    System.debug('==3=====HOD manager not null===== '+userObj.Manager);
                    idList.add(userObj.Manager);
                }
            }
        } else if(userObj.UserRole.DeveloperName.startsWith('General_Manager')){
            System.debug('=4===GM===== '+userObj);
            idList.add(userObj);
        }
        System.debug('=====While return ===getUsersList===idList==='+idList);
        return idList;
    }

    public static void RemoveAccountShare(List<Account> oldAccountList){
    	System.debug('===========RemoveAccountShare==='+oldAccountList);
        Set<Id> secondaryOwnerId = new Set<Id>();
        Set<Id> accountIdList = new Set<Id>();
        for(Account oldAcc : oldAccountList){
            secondaryOwnerId.add(oldAcc.Secondary_Owner__c);
            accountIdList.add(oldAcc.Id);
        }
    	Set<Id> usrIdSet = new Set<Id>();
    	for( User userObj : [SELECT Id
                             , Name
                             , ContactId
                             , UserRole.DeveloperName
                             , ManagerId
                             , Manager.ManagerId
                             , Manager.Manager.ManagerId
                             , Manager.Manager.Manager.ManagerId
                          FROM User 
                         WHERE Id =:secondaryOwnerId
                         AND (UserRole.DeveloperName LIKE 'DOS%'
                             OR UserRole.DeveloperName LIKE 'HOS%'
                             OR UserRole.DeveloperName LIKE 'HOD%'
                             OR UserRole.DeveloperName LIKE 'General_Manager%')
                        ]){
            if(userObj.UserRole.DeveloperName.startsWith('DOS')){
                usrIdSet.add(userObj.Id);
                if(userObj.ManagerId != null ){
                    usrIdSet.add(userObj.ManagerId);
                    if(userObj.Manager.ManagerId != null){ 
                       usrIdSet.add(userObj.Manager.ManagerId);
                       if(userObj.Manager.Manager.ManagerId != null){ 
                           usrIdSet.add(userObj.Manager.Manager.ManagerId);
                        }
                    }
                }
            } else if(userObj.UserRole.DeveloperName.startsWith('HOS')){
                usrIdSet.add(userObj.Id);
                if(userObj.ManagerId != null ){
                   usrIdSet.add(userObj.ManagerId);
                   if(userObj.Manager.ManagerId != null){ 
                       usrIdSet.add(userObj.Manager.ManagerId);
                    }
                }
            } else if(userObj.UserRole.DeveloperName.startsWith('HOD')){
                usrIdSet.add(userObj.Id);
                if(userObj.ManagerId != null ){
                   usrIdSet.add(userObj.ManagerId);
                }

            } else if(userObj.UserRole.DeveloperName.startsWith('General_Manager')){
                usrIdSet.add(userObj.Id);
            }    
        }             

    	System.debug('===========usrIdSet==='+usrIdSet);

    	List<AccountTeamMember> accTeamList = [SELECT Id
    				    		                    , UserId
                                                    , AccountId
    				    		                    , AccountAccessLevel
    				    		                    , TeamMemberRole
    				    		                 FROM AccountTeamMember 
    				    		                WHERE UserId IN : usrIdSet 
    				    		                  AND (TeamMemberRole = 'DOS'
                                                   OR TeamMemberRole = 'HOS'
                                                   OR TeamMemberRole = 'HOD'
                                                   OR TeamMemberRole = 'General Manager')
    				    		                  AND AccountId IN :accountIdList
		    		                	 ];

    	System.debug('======AccountTeamMember List to remove==='+accTeamList);
    	try{
        	delete accTeamList;
        	System.debug('====RemoveAccountShare======accTeamList==SUCCESS==== '+accTeamList);
        } catch(Exception e){
        	System.debug('====RemoveAccountShare======accTeamList==ERROR==== '+e);
        }
    }


    public static AccountTeamMember AccountTeamRec(Id accId, Id userId, String teamRole){ 
    	System.debug('===========userId==='+userId);
    	AccountTeamMember acctTM = new AccountTeamMember();
        acctTM.AccountId = accId;
        acctTM.UserId = userId;        
        acctTM.AccountAccessLevel = 'Edit';
        if(teamRole.startsWith('DOS')){
            acctTM.TeamMemberRole = 'DOS';
        } else if(teamRole.startsWith('HOS')){
            acctTM.TeamMemberRole = 'HOS';
        } else if(teamRole.startsWith('HOD')){
            acctTM.TeamMemberRole = 'HOD';
        } else if(teamRole.startsWith('General_Manager')){
            acctTM.TeamMemberRole = 'General Manager';
        }
		return acctTM;
    }
}