/*-------------------------------------------------------------------------------------------------
Description: Test class for ViewSalesToursController

    =============================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    -------------------------------------------------------------------------------------------------------------
        1.0     | 12-03-2018       | Lochana Rajput   | 1. Initial draft
    -------------------------------------------------------------------------------------------------------------
        2.0     | 29-03-2018       | Lochana Rajput   | 1. Added test method to cover code and resolve Exception
   ===============================================================================================================
*/
@isTest(seeAllData=true)
public without sharing class ViewSalesToursControllerTest {
    @isTest
    public static void test_records() {
        User objUser =[SELECT Id from User WHERE Profile.Name='Promoter Community Profile' and isActive=true LIMIT 1];
        System.runAs(objUser) {
        Test.startTest();
        ViewSalesToursController controller = new ViewSalesToursController();
        list<Sales_Tours__c> salesTours = new list<Sales_Tours__c>();
        salesTours=controller.getlstSalesTours();
        System.assertEquals(true, salesTours.size()>0);
        Test.stopTest();
        System.debug('====salesTours===='+salesTours);
        }
    }

    @isTest
    public static void test_searchFunctionality() {
        User objUser =[SELECT Id from User WHERE Profile.Name='Promoter Community Profile' and isActive=true LIMIT 1];
        System.runAs(objUser) {
        Test.startTest();
        ViewSalesToursController controller = new ViewSalesToursController();
        // List<String> profileNames = Label.PromoterCommunityProfile.split(',');
        // Inquiry__c inq = [SELECT Id from Inquiry__c WHERE CreatedBy.Profile.Name IN : profileNames LIMIT 1];
        // Sales_Tours__c obj= [SELECT Tour_Outcome__c FROM Sales_Tours__c WHERE Inquiry__c =: inq.ID LIMIT 1];
        list<Sales_Tours__c> salesTours = new list<Sales_Tours__c>();
        list<Sales_Tours__c> lstSalesTours = new list<Sales_Tours__c>();
        salesTours=controller.getlstSalesTours();
        controller.searchText = '';
        controller.search();
        lstSalesTours = new list<Sales_Tours__c>();
        lstSalesTours = controller.getlstSalesTours();
        Test.stopTest();
        System.assertEquals(true, lstSalesTours.size()>0);
        System.debug('====salesTours===='+salesTours);
        }

    }
}