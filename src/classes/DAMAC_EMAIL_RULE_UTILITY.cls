/*

Controller for VF page on the Assignment Rule which shows the count of Inquiries from the Rule
*/
public class DAMAC_EMAIL_RULE_UTILITY {

    public transient integer resultrows{get;set;}

    public DAMAC_EMAIL_RULE_UTILITY (ApexPages.StandardController controller) {
        resultrows = 0;
    }
    
    public void onload(){   
    
        string ruleId = apexpages.currentpage().getparameters().get('id'); 
        String fields = getAllfields('Email_Request__c');
        string queryCondition = 'SELECT '+fields+' FROM Email_Request__c'
                            +' WHERE id= \''+ruleId+'\' LIMIT 1';
                            

        system.debug('rulequey'+queryCondition);
        Email_Request__c assignmentRule = new Email_Request__c ();
        assignmentRule = Database.query (queryCondition);
        String query = '';
        
        if (assignmentRule.SOQL_Query__c != NULL) {
            query = assignmentRule.SoQL_Query__c;    
        } else {
            // Childs and Query
            fields = getAllfields('Email_Request_Rule_Criteria__c');
            List <Email_Request_Rule_Criteria__c> childRules = new List <Email_Request_Rule_Criteria__c> ();
            childRules = Database.query ('SELECT '+fields+' FROM Email_Request_Rule_Criteria__c '
                                +' WHERE Email_Request__c = \''+ruleId+'\' Order by Order__c Asc');
                                
            String userLogic = assignmentRule.filter_logic__c;
            query = 'SELECT id FROM '+assignmentRule.Related_Object_API__c+' WHERE ';
            String output;
            Map <String, Email_Request_Rule_Criteria__c> filterMap = new Map <String, Email_Request_Rule_Criteria__c> ();
            for (Email_Request_Rule_Criteria__c con: childRules){
               filterMap.put (String.valueOf (con.order__c), con);
            }
            System.Debug (filterMap);
            
            if (String.isNotBlank (userLogic)) {
                query += +' ( '+ formatQuery (userLogic, filterMap)+' ) ';            
                system.debug('>>>FINAL QUERY'+query);
                if(assignmentRule.query_Limit__c != null){
                    query += ' LIMIT '+assignmentRule.query_Limit__c;
                }
            }
            
        }
        resultrows= RecordCount(query);
            system.debug('resultrows'+resultrows);
    }
    public String formatQuery (String queryLogic, Map <String, Email_Request_Rule_Criteria__c> rules) {
        String query = '';
        for (String key : queryLogic.split (' ')) {
            if (key.isNumeric ()) {
                System.Debug (key );
                query += +' '+rules.get (key).Query_Condition__c+' ';
            } else
                query += key;
        }
        System.debug (query);
        return query;
    }
    
    public static integer RecordCount (String soqlQuery) {        
        integer inquiriesCount = 0; 
        String UserId = UserInfo.getUserId();
        Send_SMS__c settings = Send_SMS__c.getInstance (UserInfo.getUserID ());
        System.Debug (settings);
        String SerViceURL = settings.Salesforce_Base_URL__c;
        
        partnerSoapSforceCom.Soap partner = new partnerSoapSforceCom.Soap();
        partnerSoapSforceCom.SessionHeader_element header=new partnerSoapSforceCom.SessionHeader_element();
        header.sessionId = userInfo.getSessionID ();
        
        partnerSoapSforceCom.LoginResult result = new partnerSoapSforceCom.LoginResult ();
        partner.SessionHeader = header;
        partner.endpoint_x = SerViceURL+'/services/Soap/u/33.0';
        partner.timeout_x = 120000;
        
        try{
            partnerSoapSforceCom.QueryResult qr = partner.Query (soqlQuery);
            inquiriesCount = qr.size;
            system.debug (inquiriesCount);
            
            
        } catch(Exception e ) {
            system.debug(e.getMessage ());
        }
        if (Test.isRunningTest ()){
            inquiriesCount = 1;
        }
        return inquiriesCount;
            
    }
    

    public static string getAllfields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ())
            fields += f+ ', ';
        return fields.removeEnd(', ');         
    }
    
    
    

}