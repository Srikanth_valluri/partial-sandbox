@isTest
private class GenericRelatedListDataProviderTest {
    @isTest
    static void testDataProvider() {
        //Id personAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account');
        Account account = new Account(
            Name = 'Test Account'
        );
        insert account;
        Contact contact = new Contact(
            LastName = 'Test Contact',
            AccountId = account.Id
        );
        insert contact;

        List<Map<String, String>> fieldList = new List<Map<String, String>> {
            new Map<String, String> {
                DataRendererUtils.FIELD_NAME => 'Id'
            },
            new Map<String, String> {
                DataRendererUtils.FIELD_NAME => 'Name'
            }
        };


        Test.startTest();

        DataDisplayConfig config = new DataDisplayConfig();
        config.fieldList = fieldList;
        config.detailFieldList = fieldList;
        config.objectName = 'Contact';
        config.parentField = 'AccountId';
        config.recordId = account.Id;
        config = new GenericRelatedListDataProvider().getData(config);

        Test.stopTest();

        System.assert(!config.dataList.isEmpty());
    }
}