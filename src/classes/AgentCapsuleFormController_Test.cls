/************************************************************************************************
 * @Name              : AgentCapsuleFormController
 * @Test Class Name   : AgentCapsuleFormController_Test
 * @Description       : Its a unit test class for AgentCapsuleFormController
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         QBurst         11/09/2020       Created
***********************************************************************************************/

@IsTest
Public Class AgentCapsuleFormController_Test{
    
    public static testmethod void unittestmethod1(){
        Id AgencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        
        ID ProfileID = [ Select id,UserType from Profile where name = 'Customer Community - Admin'].id;
        ID consultantId = [ Select id,UserType from Profile where name = 'Property Consultant'].id;
        ID adminProfileID = [ Select id,UserType from Profile where name = 'System Administrator'].id;
        ID adminRoleId = [ Select id from userRole where name = 'Chairman'].id;
        
        User u = new User( email='test-user1@fakeemail.com', profileid = adminProfileID, UserRoleId = adminRoleId,
                          UserName='test-user@fakeemail.com', alias='tuser2', CommunityNickName='tuser1', isActive = true,
                          TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
                          LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User');
        insert u;
        
        
        System.runAs(u){
            Account A1 = new Account(Name = 'Test Account', Agency_Type__c = 'Corporate',Agent_Client_Base_Country__c ='Aruba;india;');
            insert A1;
            List<Contact> conList = new List<Contact>();
            Contact ownerCon = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User1', Owner__c = true,
                                     email = 'test-user@testuser123.com' );
            conList.add(ownerCon);
            Contact signatoryCon = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User2', Authorised_Signatory__c = true,
                                     email = 'test-user@testuser123.com' );
            conList.add(signatoryCon);
            Contact C1 = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User3', 
                                     email = 'test-user@testuser1234.com' );
            conList.add(c1);
            insert conList; 
            
            Agency_PC__c agencyPc = new Agency_PC__c();
            agencyPc.Agency__c = a1.Id;
            agencyPc.User__c = u.Id;
            insert agencyPc;
         a1.recordTypeId = AgencyRecordTypeId;   
        a1.Agency_Short_Name__c = 'testShrName';
        a1.Party_ID__c = '769875';
        a1.Email__c = 'test@gmail.com';
        a1.Mobile__c = '7894561230';
        a1.Title__c = 'Ms.';
        a1.Passport_Number__c = '123654';
        update a1;
        
        NSIBPM__SR_Status__c srStatus = new NSIBPM__SR_Status__c();
        srStatus.NSIBPM__Code__c = 'SUBMITTED';
        insert srStatus;

        NSIBPM__Service_Request__c servceReq = new NSIBPM__Service_Request__c();
        servceReq.Agency__c = a1.Id;
        insert servceReq;

        Booking__c bookng = new Booking__c ();
        bookng.Deal_SR__c = servceReq.Id;
        insert bookng;
        
        Booking_Unit__c bookngUnit = new Booking_Unit__c ();
        bookngUnit.Booking__c = bookng.Id;
        bookngUnit.Registration_DateTime__c = system.now().addDays(-20);
        insert bookngUnit; 
        
      
        test.startTest();
        apexpages.currentpage().getparameters().put('id', a1.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(a1);
        AgentCapsuleFormController cntr = new AgentCapsuleFormController(sc);
        cntr.buList.add(bookngUnit);
        AgentCapsuleFormController_Remote cntr2 = new AgentCapsuleFormController_Remote(sc);
        AgentCapsuleFormController_Remote.getAccounts('Test Account');
        cntr.fetchData(a1.id);
        cntr.selAccountId = a1.id;
        cntr.fetchAccountDetails();
        cntr.saveAgentCapsuleForm();
        cntr.mobileViewUpdate();
        New_Partnership_Program__c prg =  new  New_Partnership_Program__c();
        prg.Account__c = a1.Id;
        prg.Budget__c = '10M';
        insert prg;
        
        cntr.fetchData(a1.id);
        test.stopTest();
                
        }
    }
}