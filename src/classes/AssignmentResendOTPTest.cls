/**********************************************************************************************
* Description - Test class developed for AssignmentResendOTP
*
* Version            Date            Author                       Description
* 1.0                22/11/17        Naresh Kaneriya (Accely)     Initial Draft
***********************************************************************************************/
@isTest
private class AssignmentResendOTPTest
{
    
   
    public static testmethod void Test_reSendOTP(){
         test.StartTest();
         
         Id RecordTypeIdCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
         Account AccData=TestDataFactory_CRM.createPersonAccount();
         Insert AccData;
         System.assert(AccData!=null);
         
         NSIBPM__Service_Request__c  sr  = TestDataFactory_CRM.createServiceRequest();
         insert sr;
         System.assert(sr!=null);
         
         List<Booking__c>  booking = TestDataFactory_CRM.createBookingForAccount(AccData.Id ,sr.Id,1);
         insert booking;
         System.assert(booking!=null);
         
         list<Buyer__c> buyer = TestDataFactory_CRM.createBuyer(booking[0].Id ,1, AccData.Id);
         buyer[0].Phone_Country_Code__c = 'India: 0091';
         buyer[0].Phone__c = '9865215789';
         insert buyer;
         System.assert(buyer!=null);
         
         Case CaseData=TestDataFactory_CRM.createCase(AccData.Id,RecordTypeIdCase);
         CaseData.Buyer__c = buyer[0].Id;
         CaseData.Buyer_POA_Country_Code__c = 'India: 0091';
         CaseData.Buyer_POA_Phone__c = '9865215789';
         Insert CaseData;
         System.assert(CaseData!=null);
         System.assertEquals('9865215789', CaseData.Buyer_POA_Phone__c);
         
         ApexPages.StandardController sc = new ApexPages.StandardController(CaseData);
         AssignmentResendOTP  obj = new AssignmentResendOTP (sc);
         
         ApexPages.currentPage().getParameters().put('caseId',CaseData.Id);
         obj.reSendOTP();
       
         CaseData.Seller_POA_Country_Code__c = 'India: 0091';
         CaseData.Seller_POA_Phone__c = '9865215789';
         CaseData.Buyer_POA_Phone__c = '';
         update CaseData ;
         
         System.assertEquals('9865215789', CaseData.Seller_POA_Phone__c);
         System.assertEquals('', CaseData.Buyer_POA_Phone__c);
         
         obj.reSendOTP();
         
         test.StopTest();
    }
    
    public static testmethod void Test_reSendOTPelsePart(){
         test.StartTest();
         
         Id RecordTypeIdCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
         Account AccData=TestDataFactory_CRM.createPersonAccount();
         Insert AccData;
         System.assert(AccData!=null);
    
         Case CaseData=TestDataFactory_CRM.createCase(AccData.Id,RecordTypeIdCase);
         Insert CaseData;
         System.assert(CaseData!=null);
         
         ApexPages.StandardController sc = new ApexPages.StandardController(CaseData);
         AssignmentResendOTP  obj = new AssignmentResendOTP (sc);
         
         ApexPages.currentPage().getParameters().put('caseId',CaseData.Id);
         obj.reSendOTP();
       
        test.StopTest();
    }
   
}