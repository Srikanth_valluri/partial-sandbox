public without sharing class CancelPromotionSR 
{ 
  public Id caseId;

  //Constructor
  public CancelPromotionSR(ApexPages.StandardController controller) 
  {
    caseId = ApexPages.currentPage().getParameters().get('id');
  }

  // method used to cancel Case 
  public PageReference cancelCase()
  { 
    system.debug('cancel case called');
    Savepoint sp;
    try
    {
      sp = Database.setSavepoint();
      if(String.isNotBlank(caseId))
      {
        system.debug('Case ID '+caseId);
          Case objCase = [ SELECT
                                 Id
                                ,Status
                                ,Approval_Status__c
                           FROM Case WHERE Id =:caseId
                         ];
          List<Task> lstTaskToUpdate = new List<Task>();

          
          if(!objCase.Status.equalsIgnoreCase('Closed'))
          {
            List<Task> objTaskList = [ SELECT 
                                     Id,Subject,Status 
                                     FROM Task
                                     WHERE WhatId =: caseId
                                     AND Status != 'Completed'
                                   ];
            if(objTaskList != null && objTaskList.size() > 0)
            {
              for(Task objTask : objTaskList)
              {

                lstTaskToUpdate.add(new Task(Id = objTask.Id,Status = 'Completed'));
              }
              update lstTaskToUpdate;
            }

            objCase.Status = 'Cancelled';
            update objCase;

            PageReference  pg = new PageReference ('/'+caseId);
            pg.setRedirect(true);
            return pg;
          }
          else
          {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'You cannot cancel SR as it is Closed.'));
            return null; 
          }
      }
      else
      {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Case Record is not present.'));
        return null; 
      }
    }
    catch(Exception exp)
    { 
      Database.rollback(sp);
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please contact your system adminstrator '+exp.getMessage()));
      return null; 
    }
  }

}