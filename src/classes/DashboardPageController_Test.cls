/*-------------------------------------------------------------------------------------------------
Description: Test class for DashboardPageController

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 29-03-2018       | Rajnish Kumar  | 1. Initial draft
   =============================================================================================================================
*/
@isTest
public  class DashboardPageController_Test{
    
    public static testMethod void createTestData() {
            
        Campaign__c  camp = new Campaign__c();
        camp.Campaign_Type_New__c  = 'Stands';
        camp.Campaign_Name__c='Test Campaign';
        camp.start_date__c = System.today();
        camp.end_date__c = System.Today().addDays(30);
        camp.Marketing_start_date__c = System.today();
        camp.Marketing_end_date__c = System.Today().addDays(30);
        camp.Language__c = 'English';
        camp.Marketing_Active__c = true;
        camp.Credit_Control_Active__c  = true;
        camp.Sales_Admin_Active__c  = true;
        insert camp;
        
        Account ac = new Account(name ='Grazitti') ;
        insert ac; 
       
        Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);
        insert con; 
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
            User u = new User(Alias = 'standt',Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',LastName='Testing',LanguageLocaleKey='en_US',ProfileId = p.id,LocaleSidKey='en_US',Stand_Id__c = camp.id,TimeZoneSidKey='America/Los_Angeles',UserName='samer.fahim@google.com');
            System.runAs(u) {
                // The following code runs as user 'u' 
                
                List<Inquiry__c> lstInquiry = new List<Inquiry__c>();

                for(Integer i=0; i<5; i++) {
                    Inquiry__c inquiryRecord = new Inquiry__c();
                    inquiryRecord.By_Pass_Validation__c = true;
                    inquiryRecord.Inquiry_Status__c = 'New';
                    inquiryRecord.Party_ID__c = '12345';
                    inquiryRecord.Title__c = 'MR.';
                    inquiryRecord.Title_Arabic__c ='MR.';
                    inquiryRecord.First_Name__c = 'Test1';
                    inquiryRecord.First_Name_Arabic__c ='Test';
                    inquiryRecord.Last_Name__c ='Test1';
                    inquiryRecord.Last_Name_Arabic__c = 'Test';
                    inquiryRecord.Month_Status__c = 'March';
                    inquiryRecord.Campaign__c  = camp.id;
                    inquiryRecord.created_by_promoter__c = u.id;
                    lstInquiry.add(inquiryRecord);
                }

                insert lstInquiry;

                Integer i= 0;
                for(Inquiry__c objInquiry : lstInquiry) {
                    Datetime yesterday = Datetime.now().addDays(i);
                    Test.setCreatedDate(objInquiry.Id, yesterday);
                    i -= 7;
                }
                
                
                Test.startTest();
           
                DashboardPageController DashboardPageControllerObj = new DashboardPageController();
                DashboardPageControllerObj.selectStand();
                DashboardPageController.inquiryStatusNew();
                DashboardPageController.searchEnquiries();
                DashboardPageController.searchUsers();
                DashboardPageController.searchUsersOnCriteria('All');
                DashboardPageController_LeaderBoard.searchUsers();
                DashboardPageController_LeaderBoard.searchUsersOnCriteria('All');

                Test.stopTest();

                
            }
    }
}