/***********************************************************************
* Description - Test class developed for FetchNotesFromCallingListHandler
*
* Version            Date            Author            Description
* 1.0                17/01/2018                         Initial Draft

**********************************************************************/

@isTest(seeAllData=false)
public class FetchNotesFromAccOnCallingControllerTest {
        
    public static testmethod void getTestedForCallingList(){
        Test.startTest();
        Account acc = TestDataFactory_CRM.createPersonAccount();
        insert acc;
        
        TriggerOnOffCustomSetting__c obj1 = new TriggerOnOffCustomSetting__c();
        obj1.Name = 'CallingListTrigger';
        obj1.OnOffCheck__c = true;
        insert obj1;
        
        Calling_List__c callObjInst = new Calling_List__c();
        callObjInst.RecordTypeId  = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        callObjInst.Account__c = acc.Id;  
        callObjInst.Mobile_Phone__c   = '9856214598';  
        callObjInst.Email__c   = 'abc@gmail.com'; 
        insert callObjInst;
        
        
        ContentVersion contentVersion = new ContentVersion(
        Title = 'Test',
        PathOnClient = 'test.jpg',
        VersionData = Blob.valueOf('Test Content'),
        IsMajorVersion = true);
        
        insert contentVersion; 
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
 
        ContentDocumentLink cdl = New ContentDocumentLink(
            LinkedEntityId = callObjInst.id, ContentDocumentId = documents[0].Id, shareType = 'I');
            insert cdl ;

       ContentDocumentLink cd2 = New ContentDocumentLink(
            LinkedEntityId = acc.id, ContentDocumentId = documents[0].Id, shareType = 'I');
            insert cd2 ;
      
      
            PageReference aoptPage = Page.FetchNotesFromAccOnCallingListPage;
            Test.setCurrentPage(aoptPage);
            aoptPage.getParameters().put('Id',String.valueOf(callObjInst.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(callObjInst);
            FetchNotesFromAccOnCallingController controllerObj = new FetchNotesFromAccOnCallingController(sc);
        
          
        Test.stopTest();
    }
   
    public static testmethod void getTestedInnerClass(){
        Test.startTest();
        Calling_List__c callObj;
        //Case caseObj;
         Note NoteListToMove;
        Account accObj;
        ContentNote NoteLst;
        Id NoteId;
        FetchNotesFromAccOnCallingController.CalllingToNotesWrapper obj = new FetchNotesFromAccOnCallingController.CalllingToNotesWrapper('Calling List',callObj,NoteLst,NoteListToMove,NoteId);
       // FetchNotesFromAccOnCallingController.CalllingToNotesWrapper obj2 = new FetchNotesFromCallingListHandler.CalllingToNotesWrapper('Cases',caseObj,NoteLst,NoteId);
        FetchNotesFromAccOnCallingController.CalllingToNotesWrapper obj3 = new FetchNotesFromAccOnCallingController.CalllingToNotesWrapper('Account',accObj,NoteLst,NoteListToMove,NoteId);
       
        Test.stopTest();
    }
}