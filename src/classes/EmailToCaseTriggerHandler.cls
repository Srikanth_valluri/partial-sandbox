/***************************************************************************************************
 * @Description : Populates Account on Case(EmailTOCase)                                           *
 *                                                                                                 *
 *                                                                                                 *
 *  Version     Author           Date         Description                                          *
 *  1.0                          17/12/2018   Initial Development                                  *
***************************************************************************************************/
public with sharing class EmailToCaseTriggerHandler {

    /***********************************************************************************************
    * @Description : This method is to populate Account on case                                    *
    *                                                                      *
    * @Params      : List of Case Ids                                              *
    * @Return      : None                                                                          *
    ***********************************************************************************************/
    @InvocableMethod
    public Static void populateAccountOnCase(List<Id> lstCaseId) {
        System.debug(' === pupulateAccountOnCase ===');
        System.debug(' === lstCaseId ===' + lstCaseId);

        //Record type Id Email Case recors type
        Id EmailRecTypeId =
            Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        Map<String, Case> mapSuppliedEmailAndCase = new Map<String, Case>();
        List<Case> lstEmailToCase = new List<Case>();

        for(Case caseObj : [SELECT
                                AccountId,
                                ContactId,
                                RecordTypeId,
                                SuppliedEmail,
                                Origin
                            FROM
                                Case
                            WHERE Id In : lstCaseId
                                AND RecordTypeId =: EmailRecTypeId
                                AND AccountId = null
                                AND ContactId = null
                                AND SuppliedEmail != null
                                AND Origin = 'Email']) {

            mapSuppliedEmailAndCase.put(caseObj.SuppliedEmail, caseObj);
        }
        System.debug(' mapSuppliedEmailAndCase  = ' +mapSuppliedEmailAndCase);
        Set<Id> setCaseId = new Set<Id>();
        if(!mapSuppliedEmailAndCase.isEmpty() && !mapSuppliedEmailAndCase.keySet().isEmpty()) {

            for(Account accountObj : [SELECT
                                        Email__c, Email__pc
                                    FROM
                                        Account
                                    WHERE
                                        (Email__c != null
                                        AND
                                        Email__c In : mapSuppliedEmailAndCase.keySet())
                                        OR
                                        (Email__pc != null
                                        AND
                                        Email__pc  In : mapSuppliedEmailAndCase.keySet())
                                    Order By CreatedDate Desc]) {

                if(mapSuppliedEmailAndCase.containsKey(accountObj.Email__c)
                &&!setCaseId.contains(mapSuppliedEmailAndCase.get(accountObj.Email__c).Id)) {
                    lstEmailToCase.add(
                        new Case(
                            Id = mapSuppliedEmailAndCase.get(accountObj.Email__c).Id,
                            AccountId = accountObj.Id
                        )
                    );
                    setCaseId.add(mapSuppliedEmailAndCase.get(accountObj.Email__c).Id);
                }
                else if(mapSuppliedEmailAndCase.containsKey(accountObj.Email__pc)
                &&!setCaseId.contains(mapSuppliedEmailAndCase.get(accountObj.Email__pc).Id)) {
                    lstEmailToCase.add(
                        new Case(
                            Id = mapSuppliedEmailAndCase.get(accountObj.Email__pc).Id,
                            AccountId = accountObj.Id
                        )
                    );
                    setCaseId.add(mapSuppliedEmailAndCase.get(accountObj.Email__pc).Id);
                }
            }
            System.debug('after update mapSuppliedEmailAndCase  = ' + lstEmailToCase);

            if(!lstEmailToCase.isEmpty()) {
                update lstEmailToCase;
            }
        }
    }
}