// 03/05/18 - Added logic for handling duplicate SR getting created via inbound mail
public class EmailCaseTriggerHandler {
    
    public static final String  ROUTING_ADD_ATYOURSERVICE   = 'atyourservice@damacproperties.com';
    public static final String  ROUTING_ADD_RENTALPOOL      = 'crmrentalpool@damacproperties.com';
    public static final String  ROUTING_ADD_COLLECTIONS     = 'collection@damacproperties.com';
    
    // Method will fire after insertion of Email Message record
    public static void onAfterInsert( List<EmailMessage> lstEmailMsg ) {
        EmailCaseTriggerHandler.duplicateCase(lstEmailMsg);
        EmailCaseTriggerHandler.updateCaseWithThreadId(lstEmailMsg);
    }// End of onAfterInsert
    
    // Method to populate thread id on case for outbound email
    public static void updateCaseWithThreadId( List<EmailMessage> lstEmailMsg ) {
        
        String strThreadId;
        set<Id> caseId = new set<Id>();
        Id EmailRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        Id RentalPoolEmailRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RentalPool Email').getRecordTypeId();
        Id CollectionEmailRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Collection Email').getRecordTypeId();

        set<Id> recTypeIdSet = new set<Id>{EmailRecTypeId,RentalPoolEmailRecTypeId,CollectionEmailRecTypeId};
        
        for(EmailMessage objEmailMsg: lstEmailMsg) {
            
            system.debug('objEmailMsg.TextBody : ' + objEmailMsg.TextBody);
            system.debug('objEmailMsg.Subject : ' + objEmailMsg.Subject);
            system.debug('objEmailMsg= : ' + objEmailMsg);
            
            if(objEmailMsg.ParentId != null && String.valueOf(objEmailMsg.ParentId).startsWith('500') &&
                !objEmailMsg.Incoming && String.isNotBlank ( objEmailMsg.Subject) ) {
                if( objEmailMsg.Subject.toLowerCase().contains('re:') && String.isNotBlank ( objEmailMsg.TextBody ) ) {
                    strThreadId = objEmailMsg.TextBody.substringBetween('ref:',':ref');
                    caseId.add(objEmailMsg.ParentId);
                }
            }
        }
        system.debug(' strThreadId updateCaseWithThreadId :  '+strThreadId);
        if( String.isNotBlank( strThreadId ) ) {
            strThreadId = 'ref:'+strThreadId+':ref';
            
            system.debug(' strThreadId in if :  '+strThreadId);
            system.debug(' caseId in if :  '+caseId);
            
            List<Case> lstCaseToUpdate = new List<Case>();
            Map<Id,List<Case>> mapAccIdToCases  = new Map<Id,List<Case>>();
    
            if( !caseId.isEmpty() ) {
                for( Case objCase : [Select ID
                                        ,Subject
                                        ,AccountId
                                        ,Origin
                                        ,Status
                                        ,RecordTypeId
                                        ,Receipt_Id__c
                                    From Case
                                    WHERE Id IN: caseId 
                                      AND RecordTypeId IN: recTypeIdSet
                                      AND Origin = 'Email' 
                                      AND Status != 'Closed'] ) {
                    system.debug(' objCase : '+objCase);
                    if( objCase != null ) {
                        objCase.Receipt_Id__c = strThreadId;
                        lstCaseToUpdate.add(objCase);
                    }
                }
                system.debug(' lstCaseToUpdate in if :  '+lstCaseToUpdate);
                if( lstCaseToUpdate.size() > 0  ) {
                    update lstCaseToUpdate;
                }
            }
        }
        
    }
    
    /* Method will collect all that email records whose TO , CC and BCC not empty 
       and then will keep one SR and close rest of the SR
    */
    public static void duplicateCase( List<EmailMessage> lstEmailMsg ) {
    
        set<String> setSubject = new set<String>();
        set<String> setToAddress  = new set<String>();
        set<String> setCcAddress  = new set<String>();
        set<String> setBccAddress = new set<String>();
        String strReplyEmail;
        set<Id> caseId = new set<Id>();
        
        Id EmailRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        Id RentalPoolEmailRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RentalPool Email').getRecordTypeId();
        Id CollectionEmailRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Collection Email').getRecordTypeId();
        
        /*map<String,Id> mapRouteEmailRecTypeId = new map<String,Id>{'atyourservice@damacproperties.com' => EmailRecTypeId 
                                                                  , 'crmrentalpool@damacproperties.com' => RentalPoolEmailRecTypeId
                                                                  , 'collection@damacproperties.com' => CollectionEmailRecTypeId};
                                                                  
        map<String,set<Id>> mapRouteEmailRestRecTypeId = new map<String,set<Id>>{'atyourservice@damacproperties.com' => new set<Id>{RentalPoolEmailRecTypeId,CollectionEmailRecTypeId} 
                                                                  , 'crmrentalpool@damacproperties.com' => new set<Id>{EmailRecTypeId,CollectionEmailRecTypeId}
                                                                  , 'collection@damacproperties.com' => new set<Id>{EmailRecTypeId,RentalPoolEmailRecTypeId}};*/
        
        for(EmailMessage objEmailMsg: lstEmailMsg) {
            if(objEmailMsg.ParentId != null && String.valueOf(objEmailMsg.ParentId).startsWith('500') &&
                objEmailMsg.Incoming && String.isNotBlank ( objEmailMsg.Subject) ) {
                
                caseId.add(objEmailMsg.ParentId);
                
                if( objEmailMsg.Subject.toLowerCase().contains('re:') && String.isNotBlank ( objEmailMsg.TextBody ) ) {
                    strReplyEmail = objEmailMsg.TextBody.substringBetween('ref:',':ref');
                }
                
                if( String.isNotBlank ( objEmailMsg.ToAddress ) ) {
                    system.debug(' in email address if ');
                    setSubject.add(objEmailMsg.Subject);
                    for ( String email : objEmailMsg.ToAddress.split(';') ) {
                        String trimmed = email.trim();
                        if ( trimmed.length() > 0 ) {
                            setToAddress.add(trimmed);
                        }
                    }
                    setCcAddress.add(objEmailMsg.CcAddress);
                    setBccAddress.add(objEmailMsg.BccAddress);
                }
            }
        } // End of for     

        system.debug(' setToAddress :  '+setToAddress);
        system.debug(' setToAddress :  '+setToAddress.size());
        system.debug(' strReplyEmail :  '+strReplyEmail);
        //system.debug(' mapRouteEmailRecTypeId :  '+mapRouteEmailRecTypeId);
        //system.debug(' mapRouteEmailRestRecTypeId :  '+mapRouteEmailRestRecTypeId);
        
        if( !setToAddress.isEmpty() ) {
            
            if( setToAddress.size() == 1 && ( setCcAddress.isEmpty() || setBccAddress.isEmpty() ) ) {
                if( setToAddress.contains(ROUTING_ADD_ATYOURSERVICE) ) {
                    //openSR( setSubject , EmailRecTypeId  ,caseId );
                }
                else if( setToAddress.contains(ROUTING_ADD_RENTALPOOL) ) {
                    //openSR( setSubject , RentalPoolEmailRecTypeId  ,caseId );
                }
                else if( setToAddress.contains(ROUTING_ADD_COLLECTIONS) ) {
                    //openSR( setSubject , CollectionEmailRecTypeId ,caseId );
                }
            } else if( setToAddress.size() == 1 && ( !setCcAddress.isEmpty() || !setBccAddress.isEmpty() ) ) {
                if( setToAddress.contains(ROUTING_ADD_ATYOURSERVICE) ) {
                    //openSR( setSubject , EmailRecTypeId ,caseId );
                    closedRestSR( setSubject , EmailRecTypeId , new set<Id>{RentalPoolEmailRecTypeId , CollectionEmailRecTypeId} ,caseId );
                }
                else if( setToAddress.contains(ROUTING_ADD_RENTALPOOL) ) {
                    //openSR( setSubject , RentalPoolEmailRecTypeId  ,caseId );
                    closedRestSR( setSubject , RentalPoolEmailRecTypeId  , new set<Id>{EmailRecTypeId , CollectionEmailRecTypeId} ,caseId);
                }
                else if( setToAddress.contains(ROUTING_ADD_COLLECTIONS) ) {
                    //openSR( setSubject , CollectionEmailRecTypeId ,caseId );
                    closedRestSR( setSubject , CollectionEmailRecTypeId , new set<Id>{RentalPoolEmailRecTypeId  , EmailRecTypeId}  ,caseId );
                }
            } else if( setToAddress.size() > 1  ) {
                
                if( String.isNotBlank( strReplyEmail ) ) {
                    strReplyEmail = 'ref:'+strReplyEmail+':ref';
                    system.debug('in reply mail' +strReplyEmail);
                    
                    closeReplyEmail(strReplyEmail,caseId,new set<Id>{EmailRecTypeId,CollectionEmailRecTypeId , RentalPoolEmailRecTypeId });
                    /*if( setToAddress.contains( strReplyEmail ) ) {
                        openSR( setSubject , mapRouteEmailRecTypeId.get(strReplyEmail)  );
                        closedRestSR( setSubject , strReplyEmail, mapRouteEmailRestRecTypeId.get(strReplyEmail) );
                    }*/
                }else {
                    if( setToAddress.contains(ROUTING_ADD_ATYOURSERVICE) 
                     && setToAddress.contains(ROUTING_ADD_COLLECTIONS) 
                     && setToAddress.contains(ROUTING_ADD_RENTALPOOL) ) {
                        //openSR( setSubject , EmailRecTypeId  ,caseId );
                        closedRestSR( setSubject , EmailRecTypeId, new set<Id>{CollectionEmailRecTypeId , RentalPoolEmailRecTypeId } ,caseId);
                    } else if( setToAddress.contains(ROUTING_ADD_ATYOURSERVICE) 
                            && setToAddress.contains(ROUTING_ADD_COLLECTIONS) ){
                            //openSR( setSubject , EmailRecTypeId ,caseId );
                            closedRestSR( setSubject , EmailRecTypeId, new set<Id>{CollectionEmailRecTypeId , RentalPoolEmailRecTypeId} ,caseId );
                    } else if( setToAddress.contains(ROUTING_ADD_ATYOURSERVICE) 
                            && setToAddress.contains(ROUTING_ADD_RENTALPOOL) ) {
                            //openSR( setSubject , EmailRecTypeId  ,caseId );
                            closedRestSR( setSubject , EmailRecTypeId, new set<Id>{CollectionEmailRecTypeId , RentalPoolEmailRecTypeId} ,caseId );
                    } else if ( setToAddress.contains(ROUTING_ADD_COLLECTIONS) 
                             && setToAddress.contains(ROUTING_ADD_RENTALPOOL) ) {
                             //openSR( setSubject , CollectionEmailRecTypeId ,caseId );
                            closedRestSR( setSubject , CollectionEmailRecTypeId  , new set<Id>{EmailRecTypeId , RentalPoolEmailRecTypeId} ,caseId  );
                    }
                }
            }
        }
    }
    
    // Method to closed unwanted SR which got created when customer reply to thread from inbound email
    public static void closeReplyEmail( String strReplyEmail , set<Id> caseId 
                                      , set<Id> setRecTypeId   ) {
        List<Case> lstCaseToUpdate = new List<Case>();
        List<EmailMessage> lstEmailMessageToDelete = new List<EmailMessage>();
        if( !caseId.isEmpty() ) {
            for( Case objCase : [Select ID
                                    , Subject
                                    , AccountId
                                    , Origin
                                    , Status
                                    , RecordTypeId,Receipt_Id__c
                                    , (Select id,Status From EmailMessages)
                                From Case
                                WHERE Id IN: caseId 
                                  AND RecordTypeId IN: setRecTypeId
                                  AND Origin = 'Email' ] ) {
                system.debug(' objCase closeReplyEmail : '+objCase);
                if( objCase.Receipt_Id__c == strReplyEmail ) {
                    objCase.Status = 'Working';
                    lstCaseToUpdate.add(objCase);
                } else {
                
                    if( objCase.EmailMessages.size() > 0  ) {
                        lstEmailMessageToDelete.addAll(objCase.EmailMessages);
                    }

                    objCase.Status = 'Closed';
                    objCase.Reason_to_Close_Email_Case__c = 'Duplicate';
                    lstCaseToUpdate.add(objCase);
                }
            } // End of for
            
            if( lstEmailMessageToDelete.size() > 0  ) {
                Delete lstEmailMessageToDelete;
            }
            
            if( lstCaseToUpdate.size() > 0  ) {
                update lstCaseToUpdate;
            }
        }
    }
    
    
    /*public static void openSR( set<String> setSubject , Id RecTypeIdToOpenSR ,set<Id> caseId ) {
        List<Case> lstCaseToUpdate = new List<Case>();
        Map<Id,List<Case>> mapAccIdToCases  = new Map<Id,List<Case>>();
    
        if( !setSubject.isEmpty() ) {
            for( Case objCase : [Select ID
                                    ,Subject
                                    ,AccountId
                                    ,Origin
                                    ,Status
                                    ,RecordTypeId
                                From Case
                                WHERE Subject IN: setSubject 
                                  AND RecordTypeId =: RecTypeIdToOpenSR
                                  AND Origin = 'Email' 
                                  AND Status = 'Closed'] ) {
                system.debug(' objCase : '+objCase);
                if( objCase != null ) {
                    if( mapAccIdToCases.containsKey(objCase.AccountId) && mapAccIdToCases.get( objCase.AccountId ) != null ) {
                        List<Case> lstCase = mapAccIdToCases.get( objCase.AccountId );
                        lstCase.add(objCase);
                        mapAccIdToCases.put( objCase.AccountId,lstCase );
                    } else {
                        mapAccIdToCases.put( objCase.AccountId,new List<Case>{objCase});
                    }
                }
            }

            
            
            if( !mapAccIdToCases.isEmpty() ) {
                for( Case objCase : [Select ID
                                    ,Subject
                                    ,AccountId
                                    ,Origin
                                    ,Status
                                    ,RecordTypeId,Receipt_Id__c
                                From Case
                                WHERE Id IN: caseId 
                                  AND RecordTypeId =: RecTypeIdToOpenSR
                                  AND Origin = 'Email' ] ) {
                    if( mapAccIdToCases.containsKey(objCase.AccountId) ) {
                        for( Case objCase1 : mapAccIdToCases.get(objCase.AccountId) ) {
                            objCase1.Status = 'Working';
                            lstCaseToUpdate.add(objCase1);
                        }
                    }
                }
            } // End of if
            
            
            if( lstCaseToUpdate.size() > 0  ) {
                update lstCaseToUpdate;
            }
        }
    }*/

    
    public static void closedRestSR( set<String> setSubject , Id RecTypeIdToKeep 
                                   , set<Id> recTypeIdToRemove ,set<Id> caseId) {
        
        List<Case> lstCaseToUpdate = new List<Case>();
        Map<Id,List<Case>> mapAccIdToCases  = new Map<Id,List<Case>>();
    
        if( !setSubject.isEmpty() ) {
            for( Case objCase : [Select ID
                                    ,Subject
                                    ,AccountId
                                    ,Origin
                                    ,Status
                                    ,RecordTypeId
                                From Case
                                WHERE Subject IN: setSubject 
                                AND RecordTypeId IN: recTypeIdToRemove 
                                AND Origin = 'Email' 
                                AND Status != 'Closed'] ) {
                system.debug(' objCase : '+objCase);
                if( objCase != null ) {
                    if( mapAccIdToCases.containsKey(objCase.AccountId) && mapAccIdToCases.get( objCase.AccountId ) != null ) {
                        List<Case> lstCase = mapAccIdToCases.get( objCase.AccountId );
                        lstCase.add(objCase);
                        mapAccIdToCases.put( objCase.AccountId,lstCase );
                    } else {
                        mapAccIdToCases.put( objCase.AccountId,new List<Case>{objCase});
                    }
                }
            }

            
            if( !mapAccIdToCases.isEmpty() ) {
                for( Case objCase : [Select ID
                                    ,Subject
                                    ,AccountId
                                    ,Origin
                                    ,Status
                                    ,RecordTypeId,Receipt_Id__c
                                From Case
                                WHERE Id IN: caseId 
                                  AND RecordTypeId IN: recTypeIdToRemove 
                                  AND Origin = 'Email' ] ) {
                    if( mapAccIdToCases.containsKey(objCase.AccountId) ) {
                        for( Case objCase1 : mapAccIdToCases.get(objCase.AccountId) ) {
                            objCase1.Status = 'Closed';
                            objCase1.Reason_to_Close_Email_Case__c = 'Closed as a system duplication JOB';
                            lstCaseToUpdate.add(objCase1);
                        }
                    }
                }
            } // End of if
            
            
            if( lstCaseToUpdate.size() > 0  ) {
                update lstCaseToUpdate;
            }
        }
    }
}