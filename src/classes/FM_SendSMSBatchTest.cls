@isTest
private without sharing class FM_SendSMSBatchTest {
    
    static testMethod void testMethodOne() {
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;
        
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;
        
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;
        
        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;
        
        list<FM_Case__c> lstFMCases = TestDataFactoryFM.createFMCase(1);
        lstFMCases[0].Tenant__c = objAcc.Id;
        lstFMCases[0].First_Name__c = 'Test';
        lstFMCases[0].Last_Name__c = 'Test';
        lstFMCases[0].Tenant_Email__c = 'test@test.com';
        lstFMCases[0].RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId();
        insert lstFMCases ;
        
        SMS_History__c objSMS = new SMS_History__c();
        objSMS.Phone_Number__c = '1234567890';
        objSMS.Message__c = 'Test';
        objSMS.Is_SMS_Sent__c = false ;
        objSMS.FM_Case__c = lstFMCases[0].Id ;
        insert objSMS ;
       
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
            Database.executeBatch( new FM_SendSMSBatch() );    
        test.stopTest();    
    }
}