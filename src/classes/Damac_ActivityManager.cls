/*
* Class Name        : Damac_ActivityManager
* Functionality     : To create the activity manager records for the tasks and sales tour and display the activities, 
                    sales tours on the page based on start date and end date
    
*/

global class Damac_ActivityManager {
    
    global List<Activity_Manager__c> activityManager { get; set; }
    global Map<ID, Sobject> activityORSTDetails { get; set; }
    global Sales_Tours__c salesTour { get; set; }
    global Task taskRecord { get; set; }
    global List<SelectOption> attendeeRecords { get; set; }
    global List<String> selectedAttendees { get; set; }
    global List<SelectOption> contactRecords{ get; set; }
    global List<String> relatedContacts { get; set; }
    global Task dateCriteria { get; set; }
    global Activity_Manager__c managerRec { get; set;}
    global List<Meeting_Attendees__c> taskAttendees { get; set; }
    global Meeting_Attendees__c otherAttendee { get; set; }
    global String meetingTypeFilter { get; set; }
    global String relatedToFilter { get; set; }
    global String message { get; set; }
    global List<String> ownerIds { get; set; }
    global String newMeetingType { get; set; }
    global Map<string,string> contactNamesMap;
    global Damac_ActivityManager(){
        
    }
    // This method will call from page action attribute and creates the Activity manager records for tasks and sales tours
    global void pageLoad(){
        message = '';
        newMeetingType = '';
        dateCriteria = new Task();
        dateCriteria.Start_Date__c = DateTime.Now();
        dateCriteria.End_Date__c = DateTime.Now();
        meetingTypeFilter = '';
        relatedToFilter = '';
        
        ownerIds = new List<String>();
        ownerIds.add(UserInfo.getUserID());

        activityManager = new List<Activity_Manager__c>();
        salesTour = new Sales_Tours__c();
        taskRecord = new Task();
        attendeeRecords = new List<SelectOption>();
        selectedAttendees = new List<String>();
        contactRecords = new List<SelectOption>();
        relatedContacts = new List<String>();
        //contactNamesMap = new Map<string,string>();
        activityORSTDetails = new Map<ID, SObject>();
        managerRec = new Activity_Manager__c();
        salesTours();
        activities(); 

        upsert activityManager Activity_or_Task_Id__c;

        activityManager = [SELECT Id, Related_to_Sales_Tours__c, Related_to_Sales_Tours__r.Name,
                                Related_to_Account__c, Related_to_Account__r.Name, Related_to_Sales_Tours__r.Inquiry__c,
                                Related_to_Inquiry__c, Related_to_Inquiry__r.Name, Related_to_Sales_Tours__r.Inquiry__r.Full_Name__c,
                                Related_to_Account__r.RecordType.Name, Related_to_Inquiry__r.FUll_Name__c, 
                                Purpose_of_Meeting_Customer__c, Purpose_of_Meeting_Inquiry__c,Task_Owner_Or_Created_By_Id__c,
                                Purpose_of_Meeting_Internal__c, Attendees__c, Activity_or_Task_Id__c 
                                FROM Activity_Manager__c
                                WHERE Activity_or_Task_Id__c IN: activityORSTDetails.keySet()];

        attendees();
        //allRelatedContacts();
                            
    }

    // To display the Activity manager records on the page
    global void init(){
        message = '';
        activityManager = new List<Activity_Manager__c>();
        salesTour = new Sales_Tours__c();
        attendeeRecords = new List<SelectOption>();
        selectedAttendees = new List<String>();
        contactRecords = new List<SelectOption> ();
        relatedContacts = new List<String>();
        activityORSTDetails = new Map<ID, SObject>();
        taskAttendees = new List<Meeting_Attendees__c>();
        otherAttendee = new Meeting_Attendees__c();
        managerRec = new Activity_Manager__c();

        Date startDateVal = dateCriteria.Start_Date__c.Date();
        Integer dt1 = startDateVal.daysBetween(System.today());
        System.Debug(dt1);
        if (dt1 <= 30) {
            
            salesTours();
            activities(); 
            System.Debug(':::::::: Map ::::::'+activityORSTDetails.keySet());
            upsert activityManager Activity_or_Task_Id__c;
            Set<ID> taskIds = new Set<ID>();
            taskIds = activityORSTDetails.keySet();
            String query = 'SELECT Related_to_Sales_Tours__c, Related_to_Sales_Tours__r.Name, Related_to_Sales_Tours__r.Inquiry__c,'
                                    +'Related_to_Account__c, Related_to_Account__r.Name, '
                                    +'Related_to_Account__r.RecordType.Name, '
                                    +'Related_to_Inquiry__c, Related_to_Inquiry__r.Full_Name__c, '
                                    +'Related_to_Inquiry__r.Name, Related_to_Sales_Tours__r.Inquiry__r.Full_name__c, '
                                    +'Purpose_of_Meeting_Customer__c, Purpose_of_Meeting_Inquiry__c,Task_Owner_Or_Created_By_Id__c, '
                                    +'Purpose_of_Meeting_Internal__c, Attendees__c, Activity_or_Task_Id__c  '
                                    +'FROM Activity_Manager__c '
                                    +'WHERE Activity_or_Task_Id__c IN : taskIds AND Task_Owner_Or_Created_By_Id__c IN: ownerIds ';
            
            String agencyRecordType = 'Agency';
            if (meetingTypeFilter == 'M') {
                
                
                if (relatedToFilter == 'Inquiry') {
                    query += ' AND Related_to_Inquiry__c != NULL ';
                }
    
                else if (relatedToFilter == 'Customer') {
                    query += ' AND Related_to_Account__c != NULL AND (NOT Related_to_Account__r.RecordType.Name LIKE \'%' + agencyRecordType+ '%\')' ;
                }
    
                else if (relatedToFilter == 'Agency') {
                    query += ' AND Related_to_Account__c != NULL AND Related_to_Account__r.RecordType.Name LIKE \'%' + agencyRecordType+ '%\' ' ;
                } else {
                    System.debug('Entered into this condition>>'+query);
                    query += ' AND (Related_to_Account__c != NULL OR  Related_to_Inquiry__c != NULL) ';
                }
            }
    
            if (meetingTypeFilter == 'IM') {
                
                query += 'AND Related_to_Account__c = NULL AND Related_to_Inquiry__c = NULL AND Related_to_Sales_Tours__c = NULL ';
            }
    
            if (meetingTypeFilter == 'ST') {
              
                if (relatedToFilter == 'Inquiry'){
                    query += 'AND Related_to_Sales_Tours__c != NULL AND Related_to_Sales_Tours__r.Inquiry__c != NULL ';
                } else {
                    query += 'AND Related_to_Sales_Tours__c != NULL ';
                }
            }
    
            if (meetingTypeFilter == '' || meetingTypeFilter == '--None--') {
                if (relatedToFilter == 'Inquiry') {
                    query += ' AND (Related_to_Inquiry__c != NULL OR (Related_to_Sales_Tours__c != NULL AND Related_to_Sales_Tours__r.Inquiry__c != NULL)) ';
                    system.debug('Inquiry Query>>'+ database.query(query));
                }
    
                if (relatedToFilter == 'Customer') {
                    query += 'AND Related_to_Account__c != NULL AND (NOT Related_to_Account__r.RecordType.Name LIKE \'%' + String.escapeSingleQuotes(agencyRecordType)+ '%\' ) ' ;
                }
    
                if (relatedToFilter == 'Agency') {
                    query += 'AND Related_to_Account__c != NULL AND Related_to_Account__r.RecordType.Name LIKE \'%' + String.escapeSingleQuotes(agencyRecordType)+ '%\' ' ;
                }
            }
    
            query += ' LIMIT 1000';
            System.Debug(query);
            activityManager = Database.query(query);
            attendees(); 
            //allRelatedContacts();   
        }
        else {
            message = 'Start Date should not be less than 30 days.';
        } 
    }
    // To get the task detail when user tries to edit the task on the page based on taskID parameter
    global void getTaskDetail(){
    system.debug('task details');
        managerRec.Purpose_of_Meeting_Internal__c = '';
        String taskID = Apexpages.currentpage().getparameters().get('taskID');
        system.debug('task details>>>>'+taskID);
        taskRecord = new Task();
        if (taskID != NULL && taskID != '') {
            taskRecord = [SELECT WhatId, what.Type, Activity_Type_3__c, ActivityDate,  
                            Activity_Outcome__c, Status, Subject,
                            Activity_Type__c, Activity_Sub_Type__c,
                            CreatedDate, Start_Date__c, End_Date__c, Description, DOS_Comments__c, HOS_Comments__c, RM_Comments__c
                            FROM Task 
                            WHERE id =: taskID
                            ORDER BY WHATID, CreatedDate NULLS Last];

            if (taskRecord.WhatId == NULL)
                managerRec.Purpose_of_Meeting_Internal__c = taskRecord.Subject;
            managerRec.Related_to_Account__c = NULL;
            managerRec.Related_to_Inquiry__c = NULL;
            if (taskRecord.whatId != NULL) {
                if (taskRecord.What.Type == 'Account') {
                    managerRec.Related_to_Account__c = taskRecord.WhatID;    
                }
                if (taskRecord.What.Type == 'Inquiry__c') {
                    managerRec.Related_to_Inquiry__c = taskRecord.WhatID;    
                }
            }
            
        }
        System.Debug(managerRec);
        allRelatedContacts();
        currentTaskAttendees();
    }
    
    public pagereference testMethod1(){
        system.debug('123 test');    
        return null;
    }

    // To create attendees for activity manager record
    global void createAttendees(){
        String taskID = Apexpages.currentpage().getparameters().get('taskID');
        String attendeeIds = apexpages.currentpage().getparameters().get('attendeesId');
        String otherAttendeeVal = apexpages.currentpage().getparameters().get('otherAttendee');
        String involvedContactsId = apexpages.currentpage().getparameters().get('involvedContactsId');
        
        Activity_Manager__c manager = new Activity_Manager__c();
        manager = [SELECT ID FROM Activity_Manager__c WHERE Activity_or_Task_Id__c =: taskID];
        List<ID> existingAttendees = new List<ID>();
        for (Meeting_Attendees__c obj :[SELECT Attendee__c,Attendee_Contacts__c FROM Meeting_Attendees__c WHERE Activity_Manager__c =: manager.ID]) {
            existingAttendees.add(obj.Attendee__c);
            existingAttendees.add(obj.Attendee_Contacts__c);
        }

      if (attendeeIds != '' && attendeeIds != NULL) {
        attendeeIds = attendeeIds.removeEnd(',');
                
      }
        if (involvedContactsId != '' && involvedContactsId != NULL) {
            involvedContactsId = involvedContactsId.removeEnd(',');  
        }
        SET <ID> attendeeIdsList = new SET<ID>();
        System.Debug(attendeeIds);
        
        if (attendeeIds != '' && attendeeIds != NULL) {
            if (attendeeIds.contains(',')) {
                for (String key :attendeeIds.split (','))
                    attendeeIdsList.add(key);
            }
            else
                attendeeIdsList.add(attendeeIds);
        }
        //test start
        
        if (involvedContactsId != '' && involvedContactsId != NULL) {
            if (involvedContactsId.contains(',')) {
                for (String key :involvedContactsId.split (','))
                    attendeeIdsList.add(key);
            }
            else
                attendeeIdsList.add(involvedContactsId);
        }        
        //test end
        
        List<Meeting_Attendees__c> attendeesList = new List<Meeting_Attendees__c>();
        
        for (ID key :attendeeIdsList) {
            if (key != NULL) {
                if (!existingAttendees.contains(key)) {
                    Meeting_Attendees__c obj = new Meeting_Attendees__c();
                    obj.Activity_Manager__c = manager.ID;
                    if(String.valueof(key).startsWith('005')){                    
                        obj.Attendee__c = key;                                        
                    }
                    if(String.valueof(key).startsWith('003')){                    
                        obj.Attendee_Contacts__c = key;                                        
                    }
                    attendeesList.add(obj);
                }
            }
        }
                
        if (otherAttendeeVal != NULL && otherAttendeeVal != '') {
            Meeting_Attendees__c obj = new Meeting_Attendees__c();
            obj.Activity_Manager__c = manager.ID;
            obj.Others__c = otherAttendeeVal;    
            attendeesList.add(obj);
        }
        if (attendeesList.size() > 0) {
            insert attendeesList;
            otherAttendee = new Meeting_Attendees__c();
        }
        init();
    }
    global Double offset{get{
        TimeZone tz = UserInfo.getTimeZone();
        //Milliseconds to Day
        return tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
    }}

    // Method to upsert the task 
    global void upsertTask(){
        System.Debug('newMeetingType ==> '+newMeetingType);
        message = '';
        String taskID = Apexpages.currentpage().getparameters().get('taskID');
        System.Debug('taskID in upsertTask'+taskID);
        if(taskID != NULL && taskID != '') {
            taskRecord.id = taskID;
            
            Task t = new Task();
            t = [SELECT WHatId FROM Task WHERE ID =: taskID];
            
            if (t.whatID == NULL) {
                taskRecord.Subject = managerRec.Purpose_of_Meeting_Internal__c;
            }
            update taskRecord;
        }
        else {
            if (taskRecord.ActivityDate >= Date.Today() 
            && taskRecord.Start_Date__c >= Date.Today() 
            && taskRecord.End_Date__c >= Date.Today() 
            && TaskRecord.Start_Date__c <= taskRecord.End_Date__c) {
            System.Debug('Inside If1');
                if (managerRec.Related_to_Inquiry__c != NULL)
                    taskRecord.whatID = managerRec.Related_to_Inquiry__c;
    
                if (managerRec.Activity_or_Task_Id__c != NULL) {
                    if (managerRec.Activity_or_Task_Id__c.startsWIth('001'))
                        taskRecord.whatID = managerRec.Activity_or_Task_Id__c;
                }
                if (taskRecord.whatID == NULL) {
                    taskRecord.Subject = managerRec.Purpose_of_Meeting_Internal__c;
                }
                if (newMeetingType == 'Internal Meeting') {
                    taskRecord.Is_Internal_Meeting__c = true;
                }
                System.Debug('taskRecord ==> '+taskRecord);
                insert taskRecord;
            } 
             else {
                System.Debug('Inside else1');
                if (taskRecord.Start_Date__c > taskRecord.End_Date__c)
                    message = 'End date should be greater than Start Date.';
                    
                if (taskRecord.Start_Date__c < Date.Today())
                    message = 'Start date should be greater than today.';
                    
                if (taskRecord.End_Date__c < Date.Today())
                    message = 'End date should be greater than today.';
                    
                if (taskRecord.ActivityDate < Date.Today())
                    message = 'Due date should be greater than today.';
                    
                System.Debug('-----------'+message);
            }
        }
        if (message == '' && taskRecord.ID != NULL) {
            taskRecord = [SELECT WhatId, Activity_Type_3__c, ActivityDate, 
                            Activity_Outcome__c, Status, Subject,
                            Activity_Type__c, Activity_Sub_Type__c,
                            CreatedDate, Agency_Contacts_Involved__c, Start_Date__c, End_Date__c, Description, DOS_Comments__c, HOS_Comments__c, RM_Comments__c
                            FROM Task WHERE ID =: taskRecord.ID];
            Activity_Manager__c manager = new Activity_Manager__c();
            manager.Activity_or_Task_Id__c = taskRecord.ID;
            if (taskRecord.whatId != NULL) {
                if (String.valueOf(taskRecord.whatId).startsWith ('001')) {
                    manager.Related_to_Account__c = taskRecord.WhatId;
                    manager.Purpose_of_Meeting_Customer__c = taskRecord.Subject;
                } 
                if (String.valueOf(taskRecord.whatId).startsWith ('a1A')) {
                    manager.Related_to_Inquiry__c = taskRecord.whatId;
                    manager.Purpose_of_Meeting_Inquiry__c = taskRecord.Subject;
                }
            } else {
                manager.Purpose_of_Meeting_Internal__c = taskRecord.Subject;
            }
            upsert manager Activity_or_Task_Id__c;
            init(); 
            
                  
        }
    }

    // Method to get the activity or sales tour related attendees
    global void currentTaskAttendees(){
        taskAttendees = new List<Meeting_Attendees__c>();
        String taskID = Apexpages.currentpage().getparameters().get('taskID');
        if (taskId != NULL && taskId != '') {
            Activity_Manager__c manager = new Activity_Manager__c();
            manager = [SELECT ID FROM Activity_Manager__c WHERE Activity_or_Task_Id__c =: taskID];
            
            taskAttendees = [SELECT Attendee__c, Attendee_Contacts__c, Attendee_Contacts__r.Name, Attendee_Contacts__r.Email, Others__c, Attendee__r.Name, Attendee__r.Email 
                             FROM Meeting_Attendees__c
                             WHERE Activity_Manager__c =: manager.ID];
        }
    }

    // Method to retrive the attendees as team, if it is Corporate agency then it is related users based on contact
    // If it is non corporate agency then it is manager, manager's manager and manager's manager's manager
    global void attendees(){
        selectedAttendees = new List<String>();
        attendeeRecords = new List<SelectOption>();
        User u = [SELECT ManagerId, Manager.Name, 
                       Manager.ManagerId, Manager.Manager.Name, 
                       Manager.Manager.ManagerId, Manager.Manager.Manager.Name 
                       FROM USER
                       
                      WHERE ID =: UserInfo.getUserID()];
        if (u.ManagerId != NULL) {
            attendeeRecords.add(new SelectOption(u.ManagerId, u.Manager.Name));
            if (u.Manager.ManagerId != NULL) {
                attendeeRecords.add(new SelectOption(u.Manager.ManagerId, u.Manager.Manager.Name));
                if (u.Manager.Manager.ManagerId != NULL)
                    attendeeRecords.add(new SelectOption(u.Manager.Manager.ManagerId, u.Manager.Manager.Manager.Name));
            }
        }

        String accountId = Apexpages.currentpage().getparameters().get('accountId');
        
    /*    if (accountID != NULL && accountID != '') {
            attendeeRecords = new List<SelectOption>();
            try {
                Account acc = [SELECT Name, RecordType.Name FROM Account WHERE ID =:accountID AND RecordType.Name LIKE '%Agency%'];
                if (acc.ID != NULL) {
                    List<ID> contactIds = new List<ID>();
                    for (Contact con : [SELECT accountId, Name FROM Contact WHERE accountId =: acc.ID]) {
                        contactIds.add(con.ID);                    
                    }

                    if (contactIds.size () > 0) {
                        for (User userRec : [SELECT Name FROM User WHERE contactId IN :contactIds AND contactID != NULL]) {
                            attendeeRecords.add(new SelectOption(userRec.ID, userRec.Name));
                        }
                    }
                }
            } catch(Exception e){}
        }*/
    }
    
    // Fetches all the correlated Contacts of the Account
    global pageReference allRelatedContacts(){  
          
        relatedContacts = new List<String>();
        contactRecords = new List<SelectOption>(); 
        contactNamesMap = new Map<String, String>();      
        String accountId = Apexpages.currentpage().getparameters().get('accountID');        
        if(String.isNotBlank(accountId)){
            List<Contact> contactList =  [SELECT Id, Name FROM Contact WHERE AccountId =: accountId];                
            if(contactList.size () > 0){
                for(Contact conList : contactList){
                    contactRecords.add(new SelectOption(conList.Id , conList.Name));
                    contactNamesMap.put(conList.Id,conList.Name);
                }
            }
           
        }            
        return null;
            
    }
    // adds selected Contact Names to the Agency Contacts Involved field on Task record
    global void createAgencyContactsInvolved(){
    
        String taskID = Apexpages.currentpage().getparameters().get('taskID');        
        String involvedContactsId = Apexpages.currentpage().getparameters().get('involvedContactsId');        
        Task agencyTask = new Task(); 
        set<String> agencyTaskSet = new set<string>(); 
           
        if(String.isNotBlank(taskID)){
        
            agencyTask = [SELECT Id, Agency_Contacts_Involved__c FROM Task WHERE Id =: taskID ];
        
        }
        if (String.isNotBlank(involvedContactsId) && involvedContactsId.contains(',')) {
            involvedContactsId = involvedContactsId.removeEnd(',');
        }
       
        if (involvedContactsId != '' && involvedContactsId != NULL) {
            if (involvedContactsId.contains(',')) {
                for (String splitId : involvedContactsId.split (',')){
                    agencyTaskSet.add(splitId);                                    
                }
            } else{
                agencyTaskSet.add(involvedContactsId );
            }
            String agencyContacts = '';
            if(agencyTask.Agency_Contacts_Involved__c != null){
                agencyContacts = agencyTask.Agency_Contacts_Involved__c; 
            }
            if(contactNamesMap != null){
                for (string splitString :agencyTaskSet) {   
                     agencyContacts += contactNamesMap.get(splitString )+'\n';
                }
            }
            if(agencyContacts != ''){
                agencyTask.Agency_Contacts_Involved__c = agencyContacts;
                update agencyTask;
            }
        }
    }

    /************************************************************************************************
* @Description : Utility Method to get all fields based on object name for query                *
* @Params      : Object API                                                                     *
* @Return      : Object related Field APIs as Comma seperated                                   *
************************************************************************************************/
    global static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        
        if (objectType == null)
            return fields;
        
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            
            fields += sfield.getDescribe().getName ()+ ', ';
            
        }
        return fields.removeEnd(', '); 
    }
    
    
    // Method to get the salestours based on check in date and check out date
    global void salesTours(){
        DateTime startDate = dateCriteria.Start_Date__c;
        DateTime endDate = dateCriteria.End_Date__c;
        
        String startDateStr = startDate+'';
        startDateStr = startDateStr.replace(' ', 'T')+'.000+0000';
        
        String endDateStr = endDate+'';
        endDateStr = endDateStr.replace(' ', 'T')+'.000+0000';
        
        String query = 'SELECT '+getAllFields('Sales_Tours__c')
                                    +', Owner.Name, Inquiry__r.Full_Name__c FROM Sales_Tours__c '
                                    +' WHERE Property_Consultant__c IN: ownerIds '
                                    +' AND CreatedDate >='+ startDateStr 
                                    +' AND CreatedDate  <='+ endDateStr
                                    +' AND Inquiry_Owner_Match__c =TRUE '
                                    +' Order BY Check_In_Date__c NULLS LAST';
        System.Debug(query);
        for (Sales_Tours__c tours :Database.query(query)) 
        {
            Activity_Manager__c manager = new Activity_Manager__c();
            manager.Related_to_Sales_Tours__c = tours.id;
            manager.Activity_or_Task_Id__c = tours.id;
            activityManager.add(manager);
            activityORSTDetails.put (tours.Id, tours);
            System.Debug(':::::::: Map ::::::'+activityORSTDetails.keySet());

        }       
    }

    // Method to get the activities based on start date and end date
    global void activities(){
        DateTime startDate = dateCriteria.Start_Date__c;
        DateTime endDate = dateCriteria.End_Date__c;
        
        String startDateStr = startDate+'';
        startDateStr = startDateStr.replace(' ', 'T')+'.000+0000';
        
        String endDateStr = endDate+'';
        endDateStr = endDateStr.replace(' ', 'T')+'.000+0000';
        
        Date startDateVal = startDate.Date();
        Date endDateVal = endDate.date();
        String statusVal = dateCriteria.Status;
        String assignedToOrCreatedId = dateCriteria.WhatId;
        
        System.Debug(statusVal+'---');        
        String Query = 'SELECT WhatId, Activity_Type_3__c, ActivityDate, '
                        +'Activity_Outcome__c, Status, Subject, '
                        +'Activity_Type__c, Activity_Sub_Type__c,OwnerId, CreatedById,'
                        +'CreatedDate, Start_Date__c, End_Date__c, Description, DOS_Comments__c, HOS_Comments__c, RM_Comments__c '
                        +'FROM Task '
                        +'WHERE (OwnerId IN: OwnerIds OR CreatedById IN: OwnerIds) AND '
                        +' ((Start_Date__c >='+startDateStr+'  AND End_Date__c <='+endDateStr+')  OR (((Start_Date__c = NULL OR End_Date__c = NULL) AND (ActivityDate >=: startDateVal  AND ActivityDate <=: endDateVal )))) ';
            
     /*   if(assignedToOrCreatedId != '' && assignedToOrCreatedId != NULL){
        
            query += ' AND OwnerId IN: ownerIds';
        
        }
        else{
        
            query += ' AND CreatedById IN: ownerIds';
            
        }*/
        
        if (statusVal != '' && statusVal != NULL && statusVal != '--None--')
                query += ' AND Status =: statusVal ';
                        
        query += ' ORDER BY Start_Date__c NULLS LAST';
        
        System.Debug(':::: Start Time ::: '+startDate+' ::: End Time :::'+endDate+'::::');
        System.Debug(query);
        for (Task t :Database.query(Query))
        {
            activityORSTDetails.put (t.Id, t);
            System.Debug(':::::::: Map ::::::'+activityORSTDetails.keySet());
            Activity_Manager__c manager = new Activity_Manager__c();
            if (t.whatId != NULL) {
                 manager.Task_Owner_Or_Created_By_Id__c = t.OwnerId;
                if (String.valueOf(t.whatId).startsWith ('001')) {
                    manager.Related_to_Account__c = t.WhatId;
                    manager.Purpose_of_Meeting_Customer__c = t.Subject;
                } 
                if (String.valueOf(t.whatId).startsWith ('a1A')) {
                    manager.Related_to_Inquiry__c = t.whatId;
                    manager.Purpose_of_Meeting_Inquiry__c = t.Subject;
                }
               
            } else {
                manager.Task_Owner_Or_Created_By_Id__c = t.CreatedById;
                manager.Purpose_of_Meeting_Internal__c = t.Subject;
            }
            
            manager.Activity_or_Task_Id__c = t.id;
            activityManager.add(manager);
        }
    }
     @RemoteAction 
     global static List<Inquiry__c> getInquiryDetails(String searchKey){
       searchKey = '%'+searchKey+'%';  
       List<Inquiry__c> inquiryList = new List<Inquiry__c>();
       
        SET<ID> userIds = new SET <ID> ();
        userIds.add (userInfo.getUserId ());
        
        for (User u : [SELECT Id FROM User WHERE ManagerId = :UserInfo.getUserId() 
                                                OR Manager.ManagerId = :UserInfo.getUserId()
                                                OR Manager.Manager.ManagerId =: UserInfo.getUserId()
                                                OR Manager.Manager.Manager.ManagerId =: UserInfo.getUserId()]) {
            userIds.add (u.id);
        }
        
       inquiryList = [Select Name From Inquiry__c Where (Name Like: searchKey OR Full_Name__C Like: searchKey) AND OwnerID IN: userIds LIMIT 10];
       return inquiryList;
    }
    @RemoteAction 
     global static List<Account> getAccountDetails(String searchKey, String meetingType){
       searchKey = '%'+searchKey+'%';  
       List<Account> accountList = new List<Account>();
       String agencyRecordType = '%Agency%';
       if(meetingType == 'Agency') {
           accountList = [Select Name From Account Where Name Like: searchKey AND RecordType.Name Like: agencyRecordType  LIMIT 10];
       }
       if(meetingType == 'Customer') {  
           accountList = [Select Name From Account Where Name Like: searchKey AND (NOT RecordType.Name Like: agencyRecordType)  LIMIT 10];
       }
       
       
       return accountList;
    }
}