public without sharing class AssignmentClosureInIPMS{
    @InvocableMethod
    public static void closeCaseInIPMS(list<Id> setCaseIds){
        system.debug('*****Close Case in IPMS called*****');
        list<Error_Log__c> listErrorLog = new list<Error_Log__c>();
        map<String, Object> mapDeserializeTask = new map<String, Object>();
        for(Case objCase : [Select Id
                                 , CaseNumber
                                 , Seller__c
                                 , AccountId
                                 , Booking_Unit__c
                                 , Status
                                 , Booking_Unit__r.Rental_Pool__c
                                 , RecordType.DeveloperName
                                 , Seller__r.Party_ID__c
                                 , Account.Party_ID__c
                                 , Owner.Name
                                 , Booking_Unit__r.Registration_ID__c
                                 , CreatedDate
                            from Case 
                            where Id IN : setCaseIds]){
            list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5> listObjBeans = 
            new list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5>();
            
            TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objHeaderBean = 
            new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
            String reqNo = '2-'+string.valueOf(System.currentTimeMillis());
            objHeaderBean.PARAM_ID = '2-'+objCase.CaseNumber;
            system.debug('Close Case in IPMS param id*****'+objHeaderBean.PARAM_ID);
            objHeaderBean.ATTRIBUTE1 = 'HEADER';
            if(objCase.Recordtype.DeveloperName.equalsIgnoreCase('Rental_Pool_Assignment')){
                objHeaderBean.ATTRIBUTE2 = 'Rental Pool Assignment';
            }else{
                objHeaderBean.ATTRIBUTE2 = 'Assignment';
            }
            objHeaderBean.ATTRIBUTE3 = objCase.Status;
            system.debug('Close Case in IPMS .Status*****'+objCase.Status);
            objHeaderBean.ATTRIBUTE4 = objCase.Owner.Name;
            objHeaderBean.ATTRIBUTE5 = objCase.Seller__r.Party_ID__c;
            system.debug('Close Case in IPMS  party*****'+objCase.Seller__r.Party_ID__c);
            objHeaderBean.ATTRIBUTE6 = objCase.Booking_Unit__r.Registration_ID__c;
            system.debug('Close Case in IPMS  regid*****'+objCase.Booking_Unit__r.Registration_ID__c);
            objHeaderBean.ATTRIBUTE7 = String.valueOf(system.now().format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
            system.debug('Close Case in IPMS  date*****'+String.valueOf(system.now().format('dd-MMM-yyyy').toUpperCase()));
            objHeaderBean.ATTRIBUTE8 = '';
            objHeaderBean.ATTRIBUTE9 = '';
            objHeaderBean.ATTRIBUTE10 = objCase.Account.Party_ID__c;
            objHeaderBean.ATTRIBUTE11 = objCase.Id;
            objHeaderBean.ATTRIBUTE12 = '';
            objHeaderBean.ATTRIBUTE13 = '';
            objHeaderBean.ATTRIBUTE14 = '';
            objHeaderBean.ATTRIBUTE15 = '';
            objHeaderBean.ATTRIBUTE16 = '';
            objHeaderBean.ATTRIBUTE17 = '';
            objHeaderBean.ATTRIBUTE18 = '';
            objHeaderBean.ATTRIBUTE19 = '';
            objHeaderBean.ATTRIBUTE20 = '';
            objHeaderBean.ATTRIBUTE21 = '';
            objHeaderBean.ATTRIBUTE22 = '';
            objHeaderBean.ATTRIBUTE23 = '';
            objHeaderBean.ATTRIBUTE24 = '';
            objHeaderBean.ATTRIBUTE25 = '';
            objHeaderBean.ATTRIBUTE26 = '';
            objHeaderBean.ATTRIBUTE27 = '';
            objHeaderBean.ATTRIBUTE28 = '';
            objHeaderBean.ATTRIBUTE29 = '';
            objHeaderBean.ATTRIBUTE30 = '';
            objHeaderBean.ATTRIBUTE31 = '';
            objHeaderBean.ATTRIBUTE32 = '';
            objHeaderBean.ATTRIBUTE33 = '';
            objHeaderBean.ATTRIBUTE34 = '';
            objHeaderBean.ATTRIBUTE35 = '';
            objHeaderBean.ATTRIBUTE36 = '';
            objHeaderBean.ATTRIBUTE37 = '';
            objHeaderBean.ATTRIBUTE38 = '';
            objHeaderBean.ATTRIBUTE39 = '';
            //objHeaderBean.ATTRIBUTE40 = '';
            objHeaderBean.ATTRIBUTE41 = '';
            objHeaderBean.ATTRIBUTE42 = '';
            objHeaderBean.ATTRIBUTE43 = '';
            objHeaderBean.ATTRIBUTE44 = '';
            objHeaderBean.ATTRIBUTE45 = '';
            objHeaderBean.ATTRIBUTE46 = '';
            objHeaderBean.ATTRIBUTE47 = '';
            objHeaderBean.ATTRIBUTE48 = '';
            objHeaderBean.ATTRIBUTE49 = '';
            objHeaderBean.ATTRIBUTE50 = '';
            listObjBeans.add(objHeaderBean);
            
            TaskCreationWSDL.TaskHttpSoap11Endpoint objClass = new TaskCreationWSDL.TaskHttpSoap11Endpoint();
            objClass.timeout_x = 120000;
            String response = objClass.SRDataToIPMSMultiple(reqNo, 'CREATE_SR', 'SFDC', listObjBeans);
            system.debug('Case closure resp*****'+response);
            if(String.isNotBlank(response)) {
                innerClass IC = (innerClass)JSON.deserialize(response, innerClass.class);
                system.debug('IC*****'+IC);
                if(IC.status.EqualsIgnoreCase('S')){
                    //objTask.Pushed_to_IPMS__c = true;
                }else{
                    Error_Log__c objErr = createErrorLogRecord(objCase.Seller__c, objCase.Booking_Unit__c, objCase.Id);
                    objErr.Error_Details__c = 'Case closure failed with Error : '+IC.message;
                    listErrorLog.add(objErr);
                }
            }
            else {
                Error_Log__c objErr = createErrorLogRecord(objCase.Seller__c, objCase.Booking_Unit__c, objCase.Id);
                objErr.Error_Details__c = 'Case closure failed with Error : No Response from IPMS for Task Creation';
                listErrorLog.add(objErr);
            }
        }
        if(!listErrorLog.isEmpty()) {
            insertErrorLog(listErrorLog);
        }
    }
    
    public static Error_Log__c createErrorLogRecord(Id accId, Id bookingUnitId, Id caseId){
        Error_Log__c objErr = new Error_Log__c();
        objErr.Account__c = accId;
        objErr.Booking_Unit__c = bookingUnitId;
        objErr.Case__c = caseId;
        objErr.Process_Name__c = 'Assignment';
        return objErr;
    }
    
    public static void insertErrorLog(list<Error_Log__c> listObjErr){
        try{
            insert listObjErr;
        }catch(Exception ex){
            //errorMessage = 'Error : '+ ex.getMessage();
            system.debug('Error Log ex*****'+ex);
        }
    }
    
    public class innerClass{
        public string message;
        public string status;

        public innerClass(){
        }
    }
}