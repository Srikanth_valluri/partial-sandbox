public class NotificationService {
    public class Notification {
        public String   accountId   {set;get;}
        public String   recordId    {set;get;}
        public String   title       {set;get;}
        public String   status      {set;get;}
        public Boolean  isRead      {set;get;}
        public String   response    {set;get;}
        public Sobject  record      {set;get;}
        public String   className   {set;get;}
    }

    public static List<Notification> getNotifications(Notification request){
        List<Notification> notificationList = new List<Notification>();
        List<Notification_Services__c> notificationServices = getNotificationProviders();

        for(Notification_Services__c serviceConfig :  notificationServices){
            Type classType = Type.forName( serviceConfig.Notification_Service_Class__c );
            IsNotificationService serviceProvider = (IsNotificationService)classType.newInstance();
            notificationList.addAll(serviceProvider.getNotifications(request));
        }
        return notificationList;
    }

    private static List<Notification_Services__c> getNotificationProviders(){
        return Notification_Services__c.getAll().values();
    }
}