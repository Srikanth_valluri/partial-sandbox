@isTest
public class Damac_AmeyoDialerDial2_Test{
    
    public static testMethod void method1 () {
        
        
        Inquiry__c inqObj= new Inquiry__c(Is_Meeting_Scheduled__c= true,Encrypted_Mobile_2__c='test', Meeting_Due_Date__c = Date.today (), Pre_InquiryId__c='123456',Mobile_Phone_Encrypt__c='456123',Mobile_CountryCode__c='American Samoa: 001684',mobile_phone__c='1234',email__c='mk@gmail.com',First_Name__c='Test',Last_Name__c='Last',CR_Number__c='0987',/*ORN_Number__c='7842',Agency_Type__c='Corporate',isDuplicate__c=false,*/Organisation_Name__c = 'Oliver');
        insert inqObj;
        
        PageReference pageRef = Page.Damachome;
        Test.setCurrentPage(pageRef);
        
        Ameyo_Credentials__c Ameyocredentials = new Ameyo_Credentials__c();
        Ameyocredentials .CampaignId__c  ='test';
        Ameyocredentials .Endpoint__c    ='test';
        Ameyocredentials .hash_key__c    ='testf08' ;
        Ameyocredentials .host__c  ='test';
        Ameyocredentials .Command__c   ='test';
        Ameyocredentials .policy_name__c='testpolicy';
        Ameyocredentials .requesting_host__c='testhost';
        
        insert Ameyocredentials ;
        
        
        Apexpages.StandardController sc = new Apexpages.StandardController(inqObj);
        apexpages.currentpage().getparameters().put('id', inqObj.Id);
        Damac_AmeyoDialer2 Damac_AD = new Damac_AmeyoDialer2 (sc);
        Damac_AD.dialAmeyo();
        
        
    }
}