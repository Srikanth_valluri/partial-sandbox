/**********************************************************************************************************
* Description - The class developed to share Account records with new manager
*
* Version            Date            Author            Description
* 1.0                29/05/18        Monali            Initial Draft
***********************************************************************************************************/
public with sharing class UserManagerChanged {
	public static List<AccountTeamMember> accTeamListToRemove = new List<AccountTeamMember>();
    public static void ShareRecordsWithNewManager(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
    	System.debug('===before======ShareRecordsWithNewManager====mapNewRecords==='+mapNewRecords);
    	System.debug('===before======ShareRecordsWithNewManager====mapOldRecords==='+mapOldRecords);

        Set<Id> newUserIdSet = new Set<Id>();
        Set<Id> oldUserIdSet = new Set<Id>();

    	for(Id thisKey : mapNewRecords.keySet()){
    		User newUser = (User)mapNewRecords.get(thisKey);
    		User oldUser = (User)mapOldRecords.get(thisKey);
    		if(newUser.ManagerId != null 
    		 && newUser.ManagerId != oldUser.ManagerId 
    		 && newUser.IsActive == true
    		){
    			System.debug('==newUser='+newUser);
    			System.debug('==oldUser='+oldUser);
				System.debug('==oldUser='+oldUser.ManagerId);
    			if(oldUser.ManagerId != null){
    				//RemoveShareRecords(oldUser,newUser);
    				oldUserIdSet.add(oldUser.Id);
    				
    			}
    			if(newUser.ManagerId != null){
    				//Share records with new manager
    				newUserIdSet.add(newUser.Id);
    			}
    		}
    	}

    	System.debug('==oldUserIdSet='+oldUserIdSet);
    	System.debug('====newUserIdSet==='+newUserIdSet);

    	if(oldUserIdSet.size() > 0){ 
    		RemoveAccountTeams(oldUserIdSet);
    	}

    	/*if(newUserIdSet.size() > 0){
    		CreateAccountTeams(newUserIdSet);
    	}*/

    }

    public static void ShareRecordsWithNewManagerAfter(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
    	System.debug('===After====ShareRecordsWithNewManager====mapNewRecords==='+mapNewRecords);
    	System.debug('==After=====ShareRecordsWithNewManager====mapOldRecords==='+mapOldRecords);

        Set<Id> newUserIdSet = new Set<Id>();
        Set<Id> oldUserIdSet = new Set<Id>();

    	for(Id thisKey : mapNewRecords.keySet()){
    		User newUser = (User)mapNewRecords.get(thisKey);
    		User oldUser = (User)mapOldRecords.get(thisKey);
    		if(newUser.ManagerId != null 
    		 && newUser.ManagerId != oldUser.ManagerId 
    		 && newUser.IsActive == true
    		){
    			System.debug('==newUser='+newUser);
    			System.debug('==oldUser='+oldUser);
    			
    			if(newUser.ManagerId != null){
    				//Share records with new manager
    				newUserIdSet.add(newUser.Id);
    			}
    		}
    	}

    	System.debug('==After==newUserIdSet==='+newUserIdSet);

    	
    	if(newUserIdSet.size() > 0){
    		CreateAccountTeams(newUserIdSet);
    	}

    }

    public static void RemoveAccountTeams(Set<Id> oldUserList){
    	System.debug('=====RemoveAccountTeams======oldUserList==='+oldUserList);

    	Set<Id> usrIdSet = new Set<Id>();
    	for( User userObj : [SELECT Id
                             , Name
                             , ContactId
                             , UserRole.DeveloperName
                             , ManagerId
                             , Manager.ManagerId
                             , Manager.Manager.ManagerId
                             , Manager.Manager.Manager.ManagerId
                          FROM User 
                         WHERE Id =:oldUserList
                         AND (UserRole.DeveloperName LIKE 'DOS%'
                             OR UserRole.DeveloperName LIKE 'HOS%'
                             OR UserRole.DeveloperName LIKE 'HOD%'
                             OR UserRole.DeveloperName LIKE 'General_Manager%')
        ]){

	        if(userObj.UserRole.DeveloperName.startsWith('DOS')){
	        	System.debug('=1=newUser='+userObj.UserRole.DeveloperName);
	            usrIdSet.add(userObj.Id);
	            if(userObj.ManagerId != null ){
	                usrIdSet.add(userObj.ManagerId);
	                if(userObj.Manager.ManagerId != null){ 
	                   usrIdSet.add(userObj.Manager.ManagerId);
	                   if(userObj.Manager.Manager.ManagerId != null){ 
	                       usrIdSet.add(userObj.Manager.Manager.ManagerId);
	                    }
	                }
	            }
	        } else if(userObj.UserRole.DeveloperName.startsWith('HOS')){
	        	System.debug('=2=newUser='+userObj.UserRole.DeveloperName);
	            usrIdSet.add(userObj.Id);
	            if(userObj.ManagerId != null ){
	               usrIdSet.add(userObj.ManagerId);
	               if(userObj.Manager.ManagerId != null){ 
	                   usrIdSet.add(userObj.Manager.ManagerId);
	                }
	            }
	        } else if(userObj.UserRole.DeveloperName.startsWith('HOD')){
	        	System.debug('=3=newUser='+userObj.UserRole.DeveloperName);
	            usrIdSet.add(userObj.Id);
	            if(userObj.ManagerId != null ){
	               usrIdSet.add(userObj.ManagerId);
	            }

	        } else if(userObj.UserRole.DeveloperName.startsWith('General_Manager')){
	        	System.debug('=4=newUser='+userObj.UserRole.DeveloperName);
	            usrIdSet.add(userObj.Id);
	        }  
        }               

    	System.debug('====RemoveAccountTeams=======usrIdSet==='+usrIdSet);

    	accTeamListToRemove = [SELECT Id
	    		                    , UserId
                                    , AccountId
	    		                    , AccountAccessLevel
	    		                    , TeamMemberRole
	    		                 FROM AccountTeamMember 
	    		                WHERE UserId IN : usrIdSet 
	    		                  AND (TeamMemberRole = 'DOS'
                                   OR TeamMemberRole = 'HOS'
                                   OR TeamMemberRole = 'HOD'
                                   OR TeamMemberRole = 'General Manager')
	    		                  //AND AccountId IN :accountIdList
	                	 ];

    	System.debug('=11111111=accTeamListToRemove====AccountTeamMember List to remove==='+accTeamListToRemove);
    	/*try{
        	delete accTeamList;
        	System.debug('====RemoveAccountTeams======accTeamList==SUCCESS==== '+accTeamList);
        } catch(Exception e){
        	System.debug('====RemoveAccountTeams======accTeamList==ERROR==== '+e);
        }*/

    }

    public static void CreateAccountTeams(Set<Id> newUserIdSet){
    	System.debug('===after==newUserIdSet======CreateAccountTeams==='+newUserIdSet);
    	Set<Id> newUserSet = new Set<Id>();
    	for( User userObj : [SELECT Id
                             , Name
                             , ContactId
                             , UserRole.DeveloperName
                             , ManagerId
                             , Manager.ManagerId
                             , Manager.Manager.ManagerId
                             , Manager.Manager.Manager.ManagerId
                          FROM User 
                         WHERE Id =:newUserIdSet
                         AND (UserRole.DeveloperName LIKE 'DOS%'
                             OR UserRole.DeveloperName LIKE 'HOS%'
                             OR UserRole.DeveloperName LIKE 'HOD%'
                             OR UserRole.DeveloperName LIKE 'General_Manager%')
        ]){
    		newUserSet.add(userObj.Id);
    	}
    	System.debug('=====newUserSet======newUserSet==='+newUserSet);
    	List<Account> accountList = new List<Account>();
    	List<AccountTeamMember> accTeamList = [SELECT Id
    				    		                    , UserId
                                                    , AccountId
                                                    , Account.Is_Manager_Changed__c
    				    		                    , AccountAccessLevel
    				    		                    , TeamMemberRole
    				    		                 FROM AccountTeamMember 
    				    		                WHERE UserId IN : newUserSet 
    				    		                  AND (TeamMemberRole = 'DOS'
                                                   OR TeamMemberRole = 'HOS'
                                                   OR TeamMemberRole = 'HOD'
                                                   OR TeamMemberRole = 'General Manager') ];
        System.debug('===========accTeamList==='+accTeamList);
        for(AccountTeamMember team : accTeamList){
        	team.Account.Is_Manager_Changed__c = true;
        	accountList.add(team.Account);
        }
        System.debug('===========accountList==='+accountList);
        System.debug('=22222=accTeamListToRemove====craete TEam method==='+accTeamListToRemove);
        try{
        	delete accTeamListToRemove;
        	System.debug('===========accTeamListToRemove=SUCCESS=='+accTeamListToRemove);
        	update accountList;
        	System.debug('====SUCCESS=======accountList==='+accountList);
    	} catch(Exception e){
    		System.debug('==ERROR==UserManagerChanged=======Exception==='+e);
    	}
    }
}