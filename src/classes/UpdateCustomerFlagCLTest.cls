@isTest
public with sharing class UpdateCustomerFlagCLTest {
    @IsTest
    static void positiveTestUpdateCustomerFlagonCallingListHandler (){
       
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
            
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR ;
            
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc ;
            
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objDealSR.Id, 1 );
            insert lstBooking ;
            
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
            insert lstBookingUnit ;
            
            List<Calling_List__c> lstCalling = TestDataFactory_CRM.createCallingList( 'Appointment_Scheduling', 1 , lstBookingUnit[0] );
            lstCalling[0].OwnerId =UserInfo.getUserId();
            lstCalling[0].RecordTypeId  = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Elite Calling List').RecordTypeId ;
            insert lstCalling ;
            
            //lstCalling[0].Call_outCome__c='test';
            //update lstCalling;
            
            UpdateCustomerFlagonCallingListHandler.updateFlagonCallingList(lstCalling);

            

            
            
            
            
        }
    
    
}