public class FM_EmailMessageBatch implements Database.Batchable<sObject>, Database.Stateful{
    map<Id,list<Case>> mapDirector_Cases = new map<Id,list<Case>>();
    Map<id,User> mapIdUserName = new map<id,User>();
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([Select Id
                                              , CaseNumber
                                              , Case_Reopen_Count__c
                                              , Subject
                                              , Property_Director__c
                                              , Origin
                                              , RecordtypeId
                                              , RecordType.DeveloperName
                                              , Escalation_Date__c
                                              , Status
                                              , isClosed
                                              , CreatedDate
                                              , OwnerId
                                              , Owner.Name
                                              , Case_Reopen_Date__c
                                         From Case
                                         where Escalation_Date__c <= TODAY
                                         and Property_Director__c != null
                                         and RecordtypeId != null
                                         and Recordtype.Developername = 'FM_Email'
                                         and isClosed = false]);
    }
    
    public void execute(Database.BatchableContext BC, List<Case> scope){
        for(Case objC : scope){
            if(!mapDirector_Cases.containsKey(objC.Property_Director__c)){
                mapDirector_Cases.put(objC.Property_Director__c,new list<Case>{objC});
            }else{
                mapDirector_Cases.get(objC.Property_Director__c).add(objC);
            }
        }
        mapIdUserName = new Map<Id,user>([select FirstName from user where id in :mapDirector_Cases.keyset()]);
    }
    
    public void finish(Database.BatchableContext BC){
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        list<Messaging.SingleEmailMessage> listOfMessages = new list<Messaging.SingleEmailMessage>();
        for(Id directorId : mapDirector_Cases.keySet()){
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            String formattedSubjectDate = Date.Today().day() + '-' + Date.Today().month() + '-' + Date.Today().year();
            email.setSubject('List of Pending Email Cases ' + formattedSubjectDate );
            email.setTargetObjectId(directorId);
            //List<String> lstString =  new List<String>();
            //lstString.add('anshulgarg9253@gmail.com');
            //email.setCcAddresses(lstString);
            email.saveAsActivity = false;
            String htmlBody = 'Dear ' + mapIdUserName.get(directorId).FirstName + ',<br/>' + 'Please note that the following email cases are pending closure:<br/><br/>';
            htmlBody = htmlBody + '<table style="border: 1px solid black; border-collapse: collapse;"><tr>';
            htmlBody = htmlBody +'<td style="border:1px solid black;border-collapse:collapse; text-align:center;">Sr.No</td>';
            htmlBody = htmlBody +'<td style="border:1px solid black;border-collapse:collapse; text-align:center;">Email Ref #</td>';
            htmlBody = htmlBody +'<td style="border:1px solid black;border-collapse:collapse; text-align:center;">Email Subject</td>';
            htmlBody = htmlBody +'<td style="border:1px solid black;border-collapse:collapse; text-align:center;">Submitted Date</td>';
            htmlBody = htmlBody +'<td style="border:1px solid black;border-collapse:collapse; text-align:center;">Email Owner</td>';
            htmlBody = htmlBody +'<td style="border:1px solid black;border-collapse:collapse; text-align:center;">Email Re-open Count</td>';
            htmlBody = htmlBody +'<td>Email Pending Since</td></tr>';
            Integer i = 1;
            for(Case objC : mapDirector_Cases.get(directorId)){
                String openSince = objC.Case_Reopen_Date__c != NULL ? String.valueOf(objC.Case_Reopen_Date__c.daysBetween(date.today())) : ' ';
                String formattedCreatedDate = objC.CreatedDate.day() + '-' + objC.CreatedDate.month() + '-' + objC.CreatedDate.year();
                String subject = !String.isEmpty(objC.Subject) ? objC.Subject : '' ;
                Integer reOpenCount = objC.Case_Reopen_Count__c != NULL ? Integer.valueOf(objC.Case_Reopen_Count__c) : 0 ;
                htmlBody = htmlBody + '<tr><td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+i+'</td>';
                htmlBody = htmlBody +'<td style="border:1px solid black;border-collapse:collapse;text-align:center;"><a href="'+ baseUrl + '/'+objC.Id +'">'+objC.CaseNumber+'</a></td>';
                htmlBody = htmlBody +'<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+ subject +'</td>';
                htmlBody = htmlBody +'<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+formattedCreatedDate+'</td>';
                htmlBody = htmlBody +'<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+ objC.Owner.Name +'</td>';
                htmlBody = htmlBody +'<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+ reOpenCount +'</td>';
                htmlBody = htmlBody +'<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+openSince+'</td></tr>';
                i++;
            }
            htmlBody = htmlBody + '</table>';
            email.setHtmlBody(htmlBody);
            listOfMessages.add(email);
        }
        Messaging.sendEmail(listOfMessages);
    }
}