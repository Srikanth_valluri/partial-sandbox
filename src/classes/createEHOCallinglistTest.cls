@isTest
public class createEHOCallinglistTest{


 @isTest 
 static void getTest(){
     
      Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        set<Id> setCaseId = new set<Id>();
        Case Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Type = 'Early_Handover';
        insert Cas;
        System.assert(Cas != null);
        setCaseId.add(Cas.Id);
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
        
        Task tsk = new Task();
        tsk.WhatId = Cas.Id;
        tsk.ActivityDate = System.today()+2;
        tsk.Subject = 'Subject';
        tsk.Status = 'Not Started';
        tsk.Assigned_User__c = 'Finance'; 
        tsk.Status = 'Not Started';
        tsk.Process_Name__c = 'Early Handover';
        insert tsk ;  
        List<Task> lstTask =  new List<Task>();
        lstTask.add(tsk);
        
         List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        createEHOCallinglist handler = new createEHOCallinglist();
        test.startTest();
        handler.invokeApex(setCaseId);
        test.stopTest();
    }
}