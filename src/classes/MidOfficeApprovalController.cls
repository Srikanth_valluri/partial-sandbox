/************************************************************************************************
 * @Name              : MidOfficeApprovalController
 * @Test Class Name   : MidOfficeApprovalController_Test
 * @Description       : Controller Class for MidOfficeApproval VF Page 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         QBurst         16/04/2020       Created
***********************************************************************************************/
public without sharing class MidOfficeApprovalController extends QueuesDescribeSobjectAccess {
    public String updateStatus { get; set; }
    public New_Step__c currentStep {get; set;}
    public String newStatusTransitionId {get; set;}
    public List<SelectOption> checklistOption {get; set;}
    public List<String> checkList {get; set;}
    public Map<String, Step_Status_Transition__c> stepStatusTransitionMap;
    public Map<String, String> checkListMap {get; set;}
    public Map<String, String> prioritySubheadingMap {get; set;}
    public Map<String, String> itemSubpriorityMap {get; set;}
    public Set<String> subheadings{get; set;}
    public Set<String> prioritySet {get; set;}
    public Map<String, List<String>> checkListPriorityMap {get; set;}
    public Map<String, Reservation_Form_Check_List__c> checkListRecordMap {get; set;}

    public MidOfficeApprovalController(ApexPages.StandardController std) {
        sObjectAPIName = 'New_Step__c';
        updateStatus = '';
        recordId = std.getId();
        getAccess();
        checkList = new List<String>();
        subheadings = new Set<String>();
        checkListMap = new Map<String, String>();
        prioritySubheadingMap = new Map<String, String>();
        itemSubpriorityMap = new Map<String, String>();
        prioritySet = new Set<String>();
        checkListPriorityMap = new Map<String, List<String>>();
        checkListRecordMap = new Map<String, Reservation_Form_Check_List__c> ();
        stepStatusTransitionMap = new Map<String, Step_Status_Transition__c>();
        checklistOption = new List<SelectOption>();
        currentStep = [SELECT Id, Booking_Unit_Status_Code__c, Change_Status__c, Comments__c, HOS_Email__c, Service_Request__c,
                            Step_No__c, Step_Status__c, Step_Type__c, Service_Request__r.Is_VAT_SR__c,
                            Service_Request__r.RecordType.DeveloperName, Service_Request__r.Is_UK_Deal__c
                        FROM New_Step__c
                        WHERE Id =: recordId];
        system.debug('currentStep: ' + currentStep);
        newStatusTransitionId = '';
        for(Reservation_Form_Check_List__c chkList: [SELECT Id, Checklist_Item__c, Service_Request__c, Validated__c, Subheading__c
                                                     FROM Reservation_Form_Check_List__c
                                                     WHERE Service_Request__c =: currentStep.Service_Request__c]){
            checkListRecordMap.put(chkList.Checklist_Item__c, chkList);
        }
        List<String> UKDealList = new List<String>{'All'};
        if(currentStep.Service_Request__r.Is_UK_Deal__c){
            UKDealList.add('UK Only');
        } else{
            UKDealList.add('Non UK');
        }
        checklistOption.add(new SelectOption('No', 'No'));
        checklistOption.add(new SelectOption('Yes', 'Yes'));
        for (Step_Status_Transition__c transition : [SELECT Id, From_Step_Status__c, To_Step_Status__c, Step_Type__c, SR_External_Status__c, 
                                                             SR_Internal_Status__c, Is_Closed__c
                                                     FROM Step_Status_Transition__c 
                                                     WHERE Step_Type__c = 'Mid Office Approval']) {
            stepStatusTransitionMap.put(transition.To_Step_Status__c, transition);
        }
        for(Mid_Office_Approval_Checklist__mdt item: [SELECT Id, MasterLabel, Checklist_Item__c, Subheading__c,
                                                                Item_Priority__c, Item_Sublist_Priority__c
                                                       FROM Mid_Office_Approval_Checklist__mdt
                                                       ORDER BY Item_Priority__c ASC, Item_Sublist_Priority__c ASC]){
            String subheading = 'Others';
            List<string> itemList = new List<String>();
            
            if(item.subheading__c != null && item.subheading__c != ''){
                subheading = item.subheading__c;
                prioritySubheadingMap.put(item.Checklist_Item__c, String.valueOf(item.Item_Sublist_Priority__c));
            }
            System.debug (item.item_priority__C);
            String key = String.valueOf(item.Item_Priority__c.round(System.RoundingMode.FLOOR));
            if(checkListPriorityMap.containsKey(key)){
                itemList = checkListPriorityMap.get(key);
            }
            if(!prioritySubheadingMap.containsKey(key)){
                prioritySubheadingMap.put(key, subheading);
            }
            if(!checkListRecordMap.containsKey(item.Checklist_Item__c)){
                String itemPriority = ''+item.Item_Priority__c.round(System.RoundingMode.FLOOR);
                if (item.Item_Sublist_Priority__c != null) {
                    itemPriority = itemPriority+'.'+item.Item_Sublist_Priority__c.round(System.RoundingMode.FLOOR);
                }
                
                Reservation_Form_Check_List__c chkList = new Reservation_Form_Check_List__c();
                chkList.Checklist_Item__c = item.Checklist_Item__c;
                chkList.Service_Request__c = currentStep.Service_Request__c;
                chkList.Validated__c = false;
                chkList.Subheading__c = item.subheading__c;
                chkList.Name = itemPriority;
                checkListRecordMap.put(item.Checklist_Item__c, chkList);
            } else {
                Reservation_Form_Check_List__c chkList = checkListRecordMap.get (item.Checklist_Item__c);
                String itemPriority = ''+item.Item_Priority__c;
                if (item.Item_Sublist_Priority__c != null) {
                    itemPriority = itemPriority+'.'+item.Item_Sublist_Priority__c;
                }
                chkList.Name = itemPriority;
                checkListRecordMap.put(item.Checklist_Item__c, chkList);
                
            }
            itemList.add(item.Checklist_Item__c);
            checkListPriorityMap.put(key, itemList);
            String response = 'No';
            if(checkListRecordMap.get(item.Checklist_Item__c).Validated__c){
                response = 'Yes';
            }
            checkListMap.put(item.Checklist_Item__c, response);
            
        }
        prioritySet = checkListPriorityMap.keyset();
        system.debug('prioritySet: ' + prioritySet);
        system.debug('prioritySubheadingMap: ' + prioritySubheadingMap);
        system.debug('checkListPriorityMap: ' + checkListPriorityMap);
        system.debug('checkListMap: ' + checkListMap);
        system.debug('checkListRecordMap: ' + checkListRecordMap);
    }

    public PageReference updateStatus() {
        List <Reservation_Form_Check_List__c> checkListToDel = new List <Reservation_Form_Check_List__c> ();
        
        PageReference returnPageReference = null;
        Boolean reject = false;
        updateStatus = '';
        try {
           for(String item: checkListMap.keyset()){
                
                Reservation_Form_Check_List__c chkList = checkListRecordMap.get(item);
                
                
                System.debug (chkList );
                String response = checkListMap.get(item);
                System.debug (response);
                
                if (currentStep.Service_Request__r.Is_UK_Deal__c == false && chkList.Checklist_Item__c == 'Solicitor details added') {
                    response = 'Yes';
                    checkListToDel.add(chkList);
                }
                if(response == 'Yes'){
                    chkList.Validated__c = true;
                } else{
                    chkList.Validated__c = false;
                    reject = true;
                }
                checkListRecordMap.put(item, chkList);
            }
            upsert checkListRecordMap.values();
            
            try {
                delete checkListToDel;
            } catch (Exception e) {}
            Boolean pendingDocs = false;
            if (currentStep.Service_Request__r.Is_UK_Deal__c) {
                for (Unit_Documents__c doc : [SELECT ID FROM Unit_documents__c WHERE Status__c = 'Pending Upload' 
                                        AND is_required__c = TRUE
                                        AND Service_request__c =: currentStep.Service_Request__c]) {
                    pendingDocs = true;
                }
            } 
            if (pendingDocs == true) {
                reject = true;
            }
            Step_Status_Transition__c transition = new Step_Status_Transition__c();
            if(reject){
                transition = stepStatusTransitionMap.get('Mid Office Rejected');
            } else{
                transition = stepStatusTransitionMap.get('Mid Office Approved');
            }
            currentStep.Step_Status__c = transition.To_Step_Status__c;
            currentStep.Is_Closed__c = transition.Is_Closed__c;
            NSIBPM__Service_Request__c serviceRequest = new NSIBPM__Service_Request__c();
            serviceRequest.Id = currentStep.Service_Request__c;
            if(String.isNotBlank(transition.SR_Internal_Status__c)) {
                serviceRequest.Internal_Status__c = transition.SR_Internal_Status__c;
            }
            if(String.isNotBlank(transition.SR_External_Status__c)) {
                serviceRequest.External_Status__c = transition.SR_External_Status__c;
            }
            System.debug (currentStep);
        
            if(canEditRecord){
                //check if user has permission to edit the step record
                update serviceRequest;
                update currentStep;
                String checkList = '';
                try {
                    checkList = apexpages.currentpage().getparameters().get('checkList');
                    if (checkList == 'true') {
                        updateStatus = 'Success';
                        returnPageReference = null;
                    } else {
                        returnPageReference = new PageReference('/' + currentStep.Service_Request__c);
                    } 
                } catch (Exception e) {
                    returnPageReference = new PageReference('/' + currentStep.Service_Request__c);
                }
            } else{
                updateStatus = 'You don\'t have rights to edit this Step';
                //user doesn't have persmission to edit
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'You don\'t have rights to edit this Step'));
            }
            
        } catch (Exception ex) {
            System.debug('Exception --> ' + ex.getMessage());
            if (ex.getMessage().contains('INSUFFICIENT_ACCESS_OR_READONLY')) {
                updateStatus = 'You don\'t have rights to edit this Step';
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'You don\'t have rights to edit this Step'));
            } else {
                updateStatus = ex.getMessage ();
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            }
            return null;
        }
        System.debug (returnPageReference );
        
        return returnPageReference;
    }

    public PageReference cancelUpdate() {
        return new PageReference('/' + currentStep.Service_Request__c);
    }
    
    // To make the unit document as Invalid adn creating a place holder of new unit document
    public void makeUnitsInvalid () {
        String unitIds = apexpages.currentpage().getparameters().get('unitIds');
        if (unitIds != '' && unitIds != null) {
            List <ID> unitsToInvalid = new List <ID> ();
            if (unitIds.contains(';'))
                unitsToInvalid = unitIds.split(';');
            else
                unitsToInvalid.add (unitIds);
            
            List <Unit_Documents__c> invalidDocs = new List <Unit_Documents__c> ();
            List <Unit_Documents__c> clonedInvalidDocs = new List <Unit_Documents__c> ();
            for (Unit_Documents__c docs: [SELECT Is_Required__c,Service_Request__c, Buyer__c, Booking__c, Booking_Unit__c,
                                         Status__c, Document_Name__c, Document_Template__c, Document_type__c, Is_Invalid__c
                                          FROM Unit_Documents__c WHERE ID IN: unitsToInvalid]) 
            {
                docs.Is_Invalid__c = true;
                invalidDocs.add (docs);
                
                Unit_Documents__c invalidDoc = new Unit_Documents__c ();
                invalidDoc.Is_Required__c = docs.Is_Required__c;
                invalidDoc.Service_Request__c = docs.Service_Request__c;
                invalidDoc.Buyer__c = docs.Buyer__c;
                invalidDoc.Booking__c = docs.Booking__c;
                invalidDoc.Booking_Unit__c = docs.Booking_Unit__c;
                invalidDoc.Document_Name__c = docs.Document_Name__c;
                invalidDoc.Document_Template__c = docs.Document_Template__c;
                invalidDoc.Document_type__c = docs.Document_type__c;
                invalidDoc.Is_Invalid__c = false;
                invalidDoc.Id = NULL;
                invalidDoc.Status__c = 'Pending Upload';
                clonedInvalidDocs.add (invalidDoc);           
                
            }
            update invalidDocs;
            insert clonedInvalidDocs;
        }        
    }
}