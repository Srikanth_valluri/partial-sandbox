@isTest
/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  20/11/2017  Initial Draft
*/

public class OpenSRutilityTest {
    static Account  objAcc;
    static Case objCase;
    static Booking__c objBooking;
    static Booking_Unit__c objBookingUnit;
    static NSIBPM__Service_Request__c objSR;
    static Booking_Unit_Active_Status__c objBUActive;
    static Option__c objOptions;
    static Payment_Plan__c objPaymentPlan;
    static SR_Attachments__c objSRatt ;
    static SR_Booking_Unit__c objSRB;
    
    static testMethod void initialMethod(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'Test Unit';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        objBUActive = new Booking_Unit_Active_Status__c ();
        objBUActive.name = 'Test Active Status';
        objBUActive.Status_Value__c = 'Agreement executed by DAMAC'; 
        insert objBUActive; 
        
        objOptions = new Option__c();
        objOptions.Booking_Unit__c = objBookingUnit.id;
        objOptions.PromotionName__c = 'PromotionName__c';
        objOptions.SchemeName__c = 'SchemeName__c';
        objOptions.OptionsName__c = 'OptionsName__c';
        objOptions.CampaignName__c = 'CampaignName__c';
        insert objOptions;
        
        objPaymentPlan = new Payment_Plan__c();
        objPaymentPlan.Booking_Unit__c = objBookingUnit.id;
        objPaymentPlan.Status__C = '';
        insert objPaymentPlan;
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Token Refund').getRecordTypeId();
        objCase = new Case();
        objCase.AccountID = objAcc.id ;
        objCase.RecordTypeID = devRecordTypeId;
        objCase.Total_Token_Amount__c = 1000;
        objCase.POA_Expiry_Date__c = System.today();
        objCase.IsPOA__c = true;
        insert objCase;
        
        objSRatt = new SR_Attachments__c ();
        objSRatt.name = 'Test NOC';
        objSRatt.Case__c = objCase.Id;         
        objSRatt.type__c = 'NOC' ;
        //insert objSRatt;
        
        objSRB = new SR_Booking_Unit__c();
        objSRB.Case__c = objCase.id;
        objSRB.Booking_Unit__c = objBookingUnit.id;   
        insert objSRB;
    }
    
    static testMethod void Test1(){
        test.StartTest();
        initialMethod();
        
        List<Case> lstCase = new List<Case>();
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Token Refund').getRecordTypeId();
        Case objCase1 = new Case();
        objCase1.AccountID = objAcc.id;
        objCase1.Status = 'New';
        objCase1.Booking_Unit__c = objBookingUnit.id;
        objCase1.RecordTypeID = devRecordTypeId;
        lstCase.add(objCase1) ;
        
        Case objCase2 = new Case();
        objCase2.AccountID = objAcc.id;
        objCase2.Status = 'New';
        objCase2.Booking_Unit__c = objBookingUnit.id;
        lstCase.add(objCase2) ;
        
        insert lstCase ;
        
        Booking_Unit__c objBookingUnit1 = new Booking_Unit__c();
        objBookingUnit1.Registration_ID__c  = '123456';
        objBookingUnit1.Unit_Name__c  = 'Test Unit 1';
        objBookingUnit1.Booking__c  = objBooking.id;
        objBookingUnit1.Mortgage__c = true;        
        objBookingUnit1.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit1;
        
        List<SR_Booking_Unit__c> lstSRatt = new List<SR_Booking_Unit__c>();
        
        SR_Booking_Unit__c SRB = new SR_Booking_Unit__c();
        SRB.Case__c = objCase1.id;
        SRB.Booking_Unit__c = objBookingUnit.id;
        lstSRatt.add(SRB);
        
        SR_Booking_Unit__c SRB1 = new SR_Booking_Unit__c();
        SRB1.Case__c = objCase2.id;
        SRB1.Booking_Unit__c = objBookingUnit1.id;
        lstSRatt.add(SRB1);
                
        insert lstSRatt;
        
        List<String> lstBookingUnitID = new List<String>();
        lstBookingUnitID.add(objBookingUnit.id);
        lstBookingUnitID.add(objBookingUnit1.id);
        
        OpenSRutility.getOpenSR(lstBookingUnitID,''); 
        
        lstCase[0].status = 'Rejected';  
        lstCase[1].status = 'Rejected';
        update lstCase;
         
        OpenSRutility.getClosedSR(lstBookingUnitID,''); 
        
        List<Case> lstUpdatedCase = new List<Case>();
        lstUpdatedCase = [Select id,RecordType.developerName from Case where id IN : lstCase];
        set<String> setSRInitiationRecordType = new set<String>();
        setSRInitiationRecordType.add('Token_Refund');
        OpenSRutility.validateSRInitiation(setSRInitiationRecordType,lstUpdatedCase,''); 
       
        test.StopTest();
    }       
     
     
}