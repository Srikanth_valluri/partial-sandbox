/*
 * Description - Extension class for ProcessSelectionPage visualforce page
 *
 * Version      Date        Author               Description
 * 1.0          28/01/18    Vivek Shinde        Initial Draft
 * 1.1          28/02/18    Vivek Shinde        Changes for fetching POAs
 * 2.0                      Arjun Khatri        Added logic for Service Charge Reset Date Process
 * 3.0          17/06/19    Arjun Khatri        Added logic for Overdue Rebate/Discount Process 
 * 3.1          25/06/19    AKISHOR             Added logic for rebate on advance
 * 4.0          12/9/19     Aishwarya Todkar    Added logic for Riyadh Rotana Conversion process
 * 5.0          6/1/2020    Aishwarya Todkar    Added logic for Waiver process.
 * 6.0          27/02/2020  Arjun Khatri        Added logic for Case Summary - Client Relation process SR.
 * 7.0          6/01/2020   Aishwarya Todkar    Added logic for Cheque Release Process
 */
public without sharing class ProcessSelectionExtension {
    
    public String srAccountName                         {get;set;}
    public String srType                                {get;set;}
    public List<Account> accountsList                   {get;set;}
    public list<SelectOption> listAccountOptions        {get;set;}
    public Id selectedAccountId                         {get;set;}
    public Id selectedCaseId                            {get;set;}
    public Boolean isAccountSelected                    {get;set;}
    public Map<Id,Account> mapIdAccount                 {get;set;}
    public Account account                              {get;set;}
    public string strAccountId                          {get;set;}
    public Account objAccount                           {get;set;}// account object to display details for AOPT process
    public Boolean isOrgAccount                         {get;set;}
    public String selectedAccountName                   {get; set;}
    public Boolean isFieldDisabled                      {get; set;}
    public List<POAUtility.POA> lstPOA                  {get; set;}

      // Instance fields
    public String searchTerm {get; set;}
    public String selectedAccount {get; set;}

    public list<SelectOption> listCRESRs                {get;set;}
    public Id selectedCRE                               {get;set;}
    
    public ProcessSelectionExtension sharedInstance {
        get{
            return this;
        }
        set;
    } 
    
    public ProcessSelectionExtension(ApexPages.StandardSetController cases) {
        init();
    }
    
    public ProcessSelectionExtension(ApexPages.StandardController cases) {
        init();
    }
    
    public void init() {
        selectedAccountName = '';
        srType = ApexPages.currentPage().getParameters().get('SRType');
        system.debug('srType == '+srType );
        strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
        system.debug('strAccountId'+strAccountId);
        selectedCaseId = ApexPages.currentPage().getParameters().get('CaseId');
        system.debug('selectedCaseId'+selectedCaseId);

        populateCreateSRBoolean();
        isOrgAccount = true;
        accountsList = new List<Account>();
        mapIdAccount= new Map<Id,Account>();
        account = new Account();
        
        String currentAccountId = ApexPages.currentPage().getParameters().get('id');
        if(String.isNotBlank(currentAccountId) && currentAccountId.startsWith('001')) {
            strAccountId = currentAccountId;
        }

        if (strAccountId != null && strAccountId != '') {
            selectedAccountId = strAccountId;
            //isAccountSelected = true;
            selectAccount();
            if (account != null) {
                selectedAccountName = account.Name;
            }
        } else {
            isAccountSelected = false;
        }

        if (strAccountId != null && strAccountId != '' && srType != null && srType != '' && selectedAccountName != null) {
            isFieldDisabled = true;
        } else {
            isFieldDisabled = false;
        }

        listAccountOptions = new list <SelectOption>();
        listAccountOptions.add(new SelectOption('', '--NONE--'));
        List<Account> listAccount = [SELECT Name, Id, Organisation_Name__c,Salutation
                                        ,IsPersonAccount,PersonEmail,Phone,Nationality__pc,Party_Type__c,SLA__c, bypass_FAM__c
                                     FROM Account
                                     ORDER BY Name
                                     LIMIT 100
                                    ];
        for (Account account: listAccount) {
            listAccountOptions.add(new SelectOption(account.Id, account.Name));
            mapIdAccount.put(account.Id,account);
        }
        // fetch account details for AOPT process
        getCustomerPortfolio();
    }

    public Pagereference populateCreateSRBoolean(){
        system.debug('srType$$$$$'+srType);

        listCRESRs = new list<SelectOption>();
        listCRESRs.add(new selectOption('', '- None -'));
        listCRESRs.add(new selectoption('AdditionalParking','ADDITIONAL PARKING'));
        listCRESRs.add(new selectoption('AssignmentRequest','ASSIGNMENT REQUEST'));
        //listCRESRs.add(new selectoption('BouncedCheque','BOUNCED CHEQUE'));
        listCRESRs.add(new selectoption('AOPT','AOPT'));
        listCRESRs.add(new selectoption('FundTransfer','FUND TRANSFER REJECTED UNITS'));
        listCRESRs.add(new selectoption('FundTransferActiveUnits','FUND TRANSFER ACTIVE UNITS'));
        //listCRESRs.add(new selectoption('FurniturePackage','FURNITURE PACKAGE'));
        listCRESRs.add(new selectoption('Mortgage','MORTGAGE'));
        listCRESRs.add(new selectoption('PenaltyWaiver','PENALTY WAIVER'));
        listCRESRs.add(new selectoption('ProofOfPayment','PROOF OF PAYMENT'));
        listCRESRs.add(new selectoption('Promotions','PROMOTIONS'));
        listCRESRs.add(new selectoption('Refunds','REFUNDS'));
        listCRESRs.add(new selectoption('COCD','COD'));
        listCRESRs.add(new selectoption('Early_Handover','EARLY HANDOVER'));
        listCRESRs.add(new selectoption('Lease_Handover', 'LEASE HANDOVER'));
        listCRESRs.add(new selectoption('NOCVisa','NOC FOR VISA'));
        listCRESRs.add(new selectoption('Complaint','COMPLAINT'));
        listCRESRs.add(new selectoption('RentalPoolAgreement','RENTAL POOL AGREEMENT'));
        listCRESRs.add(new selectoption('RentalPoolEHO','RENTAL POOL EHO'));
        //listCRESRs.add(new selectoption('Home','Home'));
        listCRESRs.add(new selectoption('TitleDeed','TITLE DEED'));
        //listCRESRs.add(new selectoption('Handover','HANDOVER'));
        listCRESRs.add(new selectoption('RentalPoolTermination','RENTAL POOL TERMINATION'));
        listCRESRs.add(new selectoption('RentalPoolAssignment','RENTAL POOL ASSIGNMENT'));
        listCRESRs.add(new selectoption('ChequeReplacement','CHEQUE REPLACEMENT'));
        listCRESRs.add(new selectoption('PlotHandover','PLOT HANDOVER'));
        listCRESRs.add(new selectoption('EOIRefund','EOI REFUND'));
        listCRESRs.add(new selectoption('POA','POA'));
        listCRESRs.add(new selectoption('EarlySettlement','EARLY SETTLEMENT'));
        listCRESRs.add(new selectoption('SERVICE_CHARGE_RESET','SERVICE CHARGE RESET'));
        listCRESRs.add(new selectoption('OVERDUE_REBATE_DISCOUNT','OVERDUE REBATE/DISCOUNT'));
        listCRESRs.add(new selectoption('REBATE_ADVANCE','REBATE ON ADVANCE'));//AKISHOR RIYADH_ROTANA_CONVERSION
        listCRESRs.add(new selectoption('RIYADH_ROTANA_CONVERSION','RIYADH ROTANA CONVERSION'));
        listCRESRs.add(new selectoption('WAIVER', 'WAIVER'));
        listCRESRs.add(new selectoption('Case_Summary_Client_Relation', 'CASE SUMMARY - CLIENT RELATION'));
       // listCRESRs.add(new selectoption('Cheque Release', 'CHEQUE RELEASE'));
        system.debug('--selectedAccountId--'+selectedAccountId);
        
        String strPageUrl = '';
        
        if(String.isNotBlank(srType)){
            Boolean inValid = checkFam( selectedAccountId );
            
            Boolean isVatAddnm = Label.VAT_Addnm.ContainsIgnoreCase('Y') ? checkVATAddnm( selectedAccountId ) : false;
            
            if(srType.equals('AdditionalParking')){
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.ParkingRequestProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } 
            else if(srType.equals('RentalPoolAgreement')) {
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.RentalPoolAgreementProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } 
            else if(srType.equals('RentalPoolEHO')) {       
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.RentalPoolAgreementEHOProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;     
            }
            else if(srType.equals('Promotions')){
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.PromotionsProcessRequestPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            }
            else if(srType.equals('AssignmentRequest')){
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.AssignmentRequestProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } else if(srType.equals('AOPT')){
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.AOPTProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } else if(srType.equals('FundTransfer') || srType.equals('FundTransferActiveUnits')){
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.FundTransferProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } /*else if(srType.equals('FundTransferActiveUnits')){
                createFundTransferActiveUnits = true;
            }
            else if(srType.equals('FurniturePackage')){
            } */
            else if(srType.equals('Mortgage')){
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.MortgageProccessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            }
            else if(srType.equals('PenaltyWaiver')){
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.PenaltyWaiverProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } else if(srType.equals('ProofOfPayment')){
            //strPageUrl = Page.POPProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl() : Label.Is_POP_New_Process.ContainsIgnoreCase('Y') ? 
                Label.LWC_Page_Base_URL+'ProofOfPaymentPage?c__accountId='+ selectedAccountId : Page.POPProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } else if(srType.equals('Refunds')){
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.RefundsProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } else if(srType.equals('COCD')){
                strPageUrl = Page.COCDProccessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } else if(srType.equals('Early_Handover')){
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.Early_HandoverProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } else if(srType.equals('Lease_Handover')) {
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.Lease_HandoverProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } else if(srType.equals('NOCVisa')){
                 strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.NocForVisaProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } else if(srType.equals('Complaint')){
                strPageUrl = 
                inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  : Page.ComplaintProccessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } else if(srType.equals('Home')){
            } else if(srType.equals('RentalPool')){
            } else if(srType.equals('TitleDeed')){
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.TitleDeedProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } else if(srType.equals('Handover')){
            
            } 
            else if(srType.equals('RentalPoolTermination')){
            
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.RentalPoolTerminationProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } else if(srType.equals('RentalPoolAssignment')){
                strPageUrl = Page.RPAssignmentRequestProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } 
            else if(srType.equals('PlotHandover')){
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.PlotHandoverProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } else if(srType.equals('ChequeReplacement')){
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.ChequeReplacementProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } else if(srType.equals('EOIRefund')){
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.EOIRefundProcessPage.getUrl() + '?SRType=' + srType;
            } else if(srType.equals('POA')){
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.POAManagementProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            } else if(srType.equals('EarlySettlement')){
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.AOPTProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            }
            else if(srType.equals('SERVICE_CHARGE_RESET')) {
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.ResetDateProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            }
            else if(srType.equals('OVERDUE_REBATE_DISCOUNT')) {
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.OverdueRebateDiscProcessPage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            }
             else if(srType.equals('REBATE_ADVANCE')) {//AKISHOR
                strPageUrl = inValid || isVatAddnm ? Page.NotAuthorize.getUrl() :
                    Page.RebateOnAdvancePage.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            }
            else if(srType.equals('RIYADH_ROTANA_CONVERSION')) {
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.RRC_SR_Creation.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            }
            else if(srType.equals('WAIVER')) {
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                    Page.WaiverSR.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            }
            else if(srType.equals('Case_Summary_Client_Relation')) {
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                 Page.Case_Summary_Client_Relation.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            }
            /*
            //added logic for Cheque Release process
             else if(srType.equals('Cheque Release')) {
                strPageUrl = inValid ? Page.NotAuthorize.getUrl() : isVatAddnm ? Page.NotAuthorizeVATAddnm.getUrl()  :
                 Page.ChequeRelease.getUrl() + '?AccountID=' + selectedAccountId + '&SRType=' + srType;
            }*/
        } else {
        }
        listCRESRs.sort();
        system.debug('isAccountSelected '+isAccountSelected);
        
        system.debug('--strPageUrl--'+strPageUrl);
        if(String.isNotBlank(strPageUrl)) {
            Pagereference pageRef = new Pagereference(strPageUrl);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }
    

    public void selectAccount(){
        system.debug('selectedAccountId '+selectedAccountId);
        if(selectedAccountId != null){
            isAccountSelected = true;
            isOrgAccount = true;
            //account = mapIdAccount.get(selectedAccountId);
            account = [ SELECT Id
                             , Title__c
                             , Corporate_Customer_Mobile__c
                             , Customer_Masked_Mobile__c
                             , Customer_Email__c
                             , Name
                             , Party_ID__c
                             , Nationality__c
                             , Passport_Number__c
                             , IsPersonAccount
                             , PersonMobilePhone
                             , Salutation
                             , Phone
                             , PersonEmail
                             , Nationality__pc
                             , SLA__c
                             , Organisation_Name__c
                             , CR_Number__c
                             , Mobile__c
                             , Email__c
                             , Email__pc
                             , PersonMailingStreet
                             , PersonMailingPostalCode
                             , PersonMailingCity
                             , PersonMailingState
                             , PersonMailingCountry
                             , Address_Line_1__c
                             , Address_Line_2__c
                             , Address_Line_3__c
                             , Address_Line_4__c
                             , Party_Type__c,
                             Mobile_Phone_Encrypt__pc,
                             Address_Line_1__pc,
                             Address_Line_2__pc,
                             Address_Line_3__pc,
                             Address_Line_4__pc,
                             City__c,
                             Country__c,
                             City__pc,
                             Country__pc
                             ,Passport_Number__pc
                             ,bypass_FAM__c
                             //Joint Buyer info pending
                          FROM Account
                         WHERE Id = :selectedAccountId ];
             srAccountName = account.Name;
             lstPOA = POAUtility.getPOAListForAccount(selectedAccountId);
             if(account.Party_Type__c == 'PERSON') {
                 isOrgAccount = false;
             }
        } else {
            isAccountSelected = false;
        }
        system.debug('isAccountSelected '+isAccountSelected);
    }
    
    // method used to retrieve customer portfolio details
    public void  getCustomerPortfolio()
    {
        if(strAccountId != null) {
          objAccount = [ SELECT Id,Name,Title__c,Organisation_Name__c
                         ,PersonTitle,PersonMobilePhone
                         ,PersonEmail,FirstName,LastName
                         ,MiddleName,Party_ID__c,Nationality__pc
                         ,Party_Type__c,Passport_Number__c,CR_Number__c,bypass_FAM__c
                         FROM Account WHERE ID = : strAccountId ];
        }
    }
    
    
    public Boolean checkVATAddnm( String strAccountId ) {
        Boolean blnVatAdnm = false ;
        if(String.isNotBlank(strAccountId)){
            List<Account> accountList = [SELECT Id
                                              , Refuse_To_Sign_VAT_Addnm__c
                                           FROM Account
                                          WHERE Id = :strAccountId 
                                          LIMIT 1];
            if(  accountList != Null && accountList[0] != NULL ) {
                System.debug('in If' + accountList);
                blnVatAdnm = accountList[0].Refuse_To_Sign_VAT_Addnm__c ? true : false;
            }
        }
        return blnVatAdnm;
    }
    
    // method used to retrieve customer portfolio details
    public Boolean checkFam(String strAccountId){
        String CurrentUserId = UserInfo.getUserId(); 
        Boolean booleanFAM = false ;
        if(String.isNotBlank(strAccountId)){
            List<Account> accountList =[SELECT Id,                          
                       FAM_CAUTION__c,
                        Primary_CRE__c,Secondary_CRE__c,Tertiary_CRE__c,bypass_FAM__c
                        FROM Account
                      WHERE Id = :strAccountId 
                       ];
                        
            if(Label.FAM_Check.ContainsIgnoreCase('Y')){
                if(  accountList[0] != NULL && accountList[0].bypass_FAM__c == false  && String.isNotBlank(accountList[0].FAM_CAUTION__c) && Label.FAMFieldValue.containsIgnoreCase(accountList[0].FAM_CAUTION__c) ){
                        System.debug('Outer If');
                        if(accountList[0].Primary_CRE__c == NULL && accountList[0].Secondary_CRE__c == NULL && accountList[0].Tertiary_CRE__c == NULL){
                            System.debug('Iner If');
                            booleanFAM = true ;
                        }else if( !((accountList[0].Primary_CRE__c != NULL && CurrentUserId.contains(accountList[0].Primary_CRE__c)) ||
                            (accountList[0].Secondary_CRE__c != NULL && CurrentUserId.contains(accountList[0].Secondary_CRE__c)) ||
                            (accountList[0].Tertiary_CRE__c != NULL && CurrentUserId.contains(accountList[0].Tertiary_CRE__c))) ){
                            System.debug('Iner else If');
                            booleanFAM = true ;
                            
                        }
                }
            }
        }
        return booleanFAM;
    }
}