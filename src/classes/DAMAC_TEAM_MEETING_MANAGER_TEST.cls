@isTest
private class DAMAC_TEAM_MEETING_MANAGER_TEST {
    
    @isTest static void test_TeamMeetings() {
        Inquiry__c  inqObj = createInquiries();
        Task objTask = createTask(inqObj.Id);
       // Sales_Tours__c objSalesTour = createSalesTour(inqObj.Id);
        Account acc = createAccount();
        Task objAccTask = createTask(acc.Id);

        DAMAC_TEAM_MEETING_MANAGER obj = new DAMAC_TEAM_MEETING_MANAGER();
        obj.myTeamIds.add(UserInfo.getUserID());

        obj.myTeam();
        obj.dateCriteria.Start_date__c = DateTime.Now().addDays(-2);
        obj.dateCriteria.End_date__c = DateTime.Now().addDays(2);
        obj.MeetingTypeFilter = 'M';
        obj.relatedToFilter = 'Inquiry';
        obj.init();
    }

    @isTest static void test_TeamCustomerMeetings() {
        Inquiry__c  inqObj = createInquiries();
        Task objTask = createTask(inqObj.Id);
       // Sales_Tours__c objSalesTour = createSalesTour(inqObj.Id);
        Account acc = createAccount();
        Task objAccTask = createTask(acc.Id);

        DAMAC_TEAM_MEETING_MANAGER obj = new DAMAC_TEAM_MEETING_MANAGER();
        obj.myTeamIds.add(UserInfo.getUserID());

        obj.myTeam();
        obj.dateCriteria.Start_date__c = DateTime.Now().addDays(-2);
        obj.dateCriteria.End_date__c = DateTime.Now().addDays(2);        
        obj.MeetingTypeFilter = 'M';
        obj.relatedToFilter = 'Customer';
        obj.init();
    }

    @isTest static void test_TeamInternalMeetings() {
        Inquiry__c  inqObj = createInquiries();
        Task objTask = createTask(inqObj.Id);
      //  Sales_Tours__c objSalesTour = createSalesTour(inqObj.Id);
        Account acc = createAccount();
        Task objAccTask = createTask(NULL);

        DAMAC_TEAM_MEETING_MANAGER obj = new DAMAC_TEAM_MEETING_MANAGER();
        obj.myTeamIds.add(UserInfo.getUserID());

        obj.myTeam();
        obj.dateCriteria.Start_date__c = DateTime.Now().addDays(-2);
        obj.dateCriteria.End_date__c = DateTime.Now().addDays(2);      
        obj.MeetingTypeFilter = 'IM';
        obj.relatedToFilter = '';
        obj.init();
    }
    
    @isTest static void test_TaskComments() {
        Inquiry__c  inqObj = createInquiries();
        Task objTask = createTask(inqObj.Id);
       // Sales_Tours__c objSalesTour = createSalesTour(inqObj.Id);
        Account acc = createAccount();
        Task objAccTask = createTask(acc.Id);

        DAMAC_TEAM_MEETING_MANAGER obj = new DAMAC_TEAM_MEETING_MANAGER();
        
        Apexpages.currentpage().getparameters().put('taskID', objAccTask.Id);
        obj.myTeamIds.add(UserInfo.getUserID());

        obj.myTeam();
        obj.dateCriteria.Start_date__c = DateTime.Now().addDays(-2);
        obj.dateCriteria.End_date__c = DateTime.Now().addDays(2);
        obj.MeetingTypeFilter = 'M';
        obj.relatedToFilter = 'Inquiry';
        obj.init();
        obj.getTaskComments();
    }
    
    public static Inquiry__c createInquiries() {
        Inquiry__c inquiryInst = new Inquiry__c(
            First_Name__c = 'Test',
            Last_Name__c = 'Last Test',
            Inquiry_Source__c = 'Call Center',
            Inquiry_Status__c = 'Active',
            Email__c = 'test@gmail.com',
            OwnerId = UserInfo.getuserid()
        );
        insert inquiryInst;
        return inquiryInst;
    }

    public static Account createAccount(){
        Id RecTypeCorporateAgency = Schema.SObjectType.Account
                                    .getRecordTypeInfosByName()
                                    .get('Corporate Agency')
                                    .getRecordTypeId();
        Account acc = new account(recordtypeid = RecTypeCorporateAgency, name='test',Trade_License_Number__c = 'test');
        insert acc;
        return acc;

    }
    
    /**
     * Method to insert records for Task
     */
    public static Task createTask(Id inqId) {
        Task objTask = new Task(
            WhatId = inqId,
            Subject = 'Last Test',
            Description = 'Last Test',
            Start_date__c = System.today(),
            End_Date__c = System.today(),
            OwnerId = UserInfo.getuserid()
        );
        insert objTask;
        return objTask;
    }

     /**
     * Method to insert records for Sales Tour
     
    public static Sales_Tours__c createSalesTour(Id inqId) {
        Sales_Tours__c objSalesTour = new Sales_Tours__c(
            Inquiry__c = inqId,
            OwnerId = UserInfo.getuserid(),
            Tour_Outcome__c = 'No Show - Follow up needed',
            Pickup_Location__c = 'Park Towers',
            Check_In_Date__c = System.today(),
            Check_Out_Date__c = System.today(),
            Comments__c = 'Test'
        );
        insert objSalesTour;
        return objSalesTour;
    }
    */
}