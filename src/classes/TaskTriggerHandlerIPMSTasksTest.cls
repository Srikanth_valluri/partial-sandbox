@isTest
private class TaskTriggerHandlerIPMSTasksTest
{
  public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';
  
  public class MockHttpResponseGenerator1 implements HttpCalloutMock { 
      public HTTPResponse respond(HTTPRequest req) {
               
                
                // Create a fake response
                HttpResponse res = new HttpResponse();
                res.setHeader('Content-Type', 'application/json');
                res.setBody('{ "OutputParameters" : { "@xmlns" : "http://xmlns.oracle.com/apps/ont/rest/XXDC_PROCESS_SERVICE_WS/process/", "@xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance", "X_RESPONSE_MESSAGE" : { "X_RESPONSE_MESSAGE_ITEM" : [ { "PARAM_ID" : null, "PROC_STATUS" : "S", "PROC_MESSAGE" : null, "ATTRIBUTE1" : "10483929", "ATTRIBUTE2" : "3679041", "ATTRIBUTE3" : null, "ATTRIBUTE4" : null } ] }, "X_RETURN_STATUS" : "S", "X_RETURN_MESSAGE" : "Process Completed Returning 1 Response Message(s)..." } }');
                res.setStatusCode(200);
                return res;
    }
  }
  
   @testSetup()
  private static void testData(){
      List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);    
      settingLst2.add(newSetting1);
      insert settingLst2;
       Skip_Task_Trigger_for_Informatica__c objCS =new Skip_Task_Trigger_for_Informatica__c();
        objCS.SetupOwnerId=Userinfo.getUserId();
        objCS.Enable__c = true;
        insert objCS;
  }
  
 static testMethod void getTask_test_scenario4()
  {
    List<Booking__c> bookingList = new List<Booking__c>();
    List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
    List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
    set<Id> caseIdSet = new set<Id>();
    set<Id> taskIdSet = new set<Id>();
    //insert sample data for Account
    Account objAccount = TestDataFactory_CRM.createPersonAccount();
    insert objAccount;
    
    Id aoptRecordTypeID = getRecordTypeIdForAOPT();
    
      Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , aoptRecordTypeID);
      objCase.Status = 'Submitted';
      insert objCase;

      //create Deal SR record
    NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
    insert objDealSR;

    //create Booking record for above created Deal and Account
    bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
    insert bookingList;

    //create Property data
    Property__c objProperty = new Property__c();
    objProperty.Name = 'Test Project';
    objProperty.Property_Name__c = 'Test Property';
    objProperty.Property_ID__c = 3431;
    objProperty.CurrencyIsoCode = 'AED';
    insert objProperty;

    //create data for Inventory
    Inventory__c objInventory = new Inventory__c();
    objInventory.Property__c = objProperty.Id;
    objInventory.Unit__c = '1345';
    insert objInventory;

    //create Booking Units record for above created Bookings
    bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
    for(Booking_Unit__c objBookingUnit : bookingUnitList)
    {
      objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
      objBookingUnit.Inventory__c = objInventory.Id;
      objBookingUnit.Registration_ID__c = '74712';
    }
    insert bookingUnitList;

    srBookingUnitList = TestDataFactory_CRM.createSRBookingUnis(objCase.Id , bookingUnitList);
    insert srBookingUnitList;

    objCase.Booking_Unit__c = bookingUnitList[0].Id;
    objCase.OwnerId = UserInfo.getUserId();
    update objCase;
    
    Case_Extension__c objCaseExt = new Case_Extension__c();
    objCaseExt.Case__c = objCase.id;
    objCaseExt.Payment_Mode__c = 'Transfer';
    
    insert objCaseExt;
    
    Credentials_Details__c settings = new Credentials_Details__c();
    settings.Name = 'Customer And EOI Refund';
    settings.User_Name__c = 'Some Value';
    settings.Password__c = 'Some Value';
    settings.Resource__c = 'Some Value';
    settings.grant_type__c = 'Some Value';
    settings.Endpoint__c = 'https://ws.dev.aramex.net/ShippingAPI.V2/Shipping/Service_1_0.svc/json/CreateShipments';
    insert settings;
    
    User objUser = createTestUser();
    insert objUser;
    
    Task objTask = new Task();
            objTask.Subject = 'Verify Proof of Payment Details in IPMS';
            objTask.Assigned_User__c = 'Finance';
            objTask.ActivityDate = Date.today()+2;
            //objTask.OwnerId = objCase.OwnerId;
            objTask.Priority = 'Normal';
            objTask.CurrencyIsoCode = objCase.CurrencyIsoCode;
            objTask.Status = 'Not Started';
            objTask.WhatId = objCase.Id;
            objTask.Process_Name__c = 'POP';
            insert objTask;
    
    Task objTask20 = TaskUtility.getTask((SObject)objCase, 'Update EOI Refund Details in IPMS', 'Finance', 
                                'EOI Refund', system.today().addDays(1));
    objTask20.OwnerId = objUser.Id;
    insert objTask20;
    taskIdSet.add(objTask20.Id);
    caseIdSet.add(objTask20.WhatId);
        
      Test.startTest();    
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator1()); 
        TaskTriggerHandlerIPMSTasks.genericServiceToPushTOIPMSEOIAndCustomer(taskIdSet,caseIdSet);
        //objTask20.Status = 'Completed';
        //update objTask20;
      Test.stopTest();
  }
  
   private static Id getRecordTypeIdForAOPT()
  {
    Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
    Id aoptRecordTypeID = caseRecordTypes.get('EOI Refund').getRecordTypeId();
    return aoptRecordTypeID;
  }
  
  public static User createTestUser()
  {
      String orgId = UserInfo.getOrganizationId();
      String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
      Profile objProfile = [Select Id from Profile where Name='System Administrator' LIMIT 1];
     
      Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
      String uniqueName = orgId + dateString + randomInt;
      User tuser = new User(  firstname = 'fName',
                              lastName = 'lName',
                              email = uniqueName + '@test' + orgId + '.org',
                              Username = uniqueName + '@test' + orgId + '.org',
                              EmailEncodingKey = 'ISO-8859-1',
                              Alias = uniqueName.substring(18, 23),
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US',
                              ProfileId = objProfile.Id
                              );
      return tuser;
  }
}