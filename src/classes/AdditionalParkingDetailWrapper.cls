public class AdditionalParkingDetailWrapper 
{

    public class AdditionalParkingDetails 
    {
        public List<Data> data;
        public String message;
        public String status;
        public String customErrorMsg;
    }

    public class Data 
    {
        public Integer ORG_ID {get;set;}
        public String STATUS {get;set;}
        public Integer DETAIL_ID {get;set;}
        public Integer PRICE {get;set;}
        public Integer PROPERTY_ID {get;set;}
        public String PARKING_TYPE_DESC {get;set;}
        public String STATUS_CODE {get;set;}
        public String PARKING_TYPE {get;set;}
        public Integer BUILDING_ID {get;set;}
        public String PROPERTY_NAME {get;set;}
        public String BUILDING_NAME {get;set;}
        public String PARKING_BAY_NUMBER {get;set;}
        public Boolean blnIsSelected {get;set;}
    }

    
    public static AdditionalParkingDetails parse(String json) 
    {
        return (AdditionalParkingDetails) System.JSON.deserialize(json, AdditionalParkingDetails.class);
    }
}