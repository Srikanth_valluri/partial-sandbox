// Batch Job for Processing the Records


global class CloseMortgageSRAfter110DaysBatch implements Database.Batchable<sObject>
                                                    , Database.Stateful
                                                    , Database.AllowsCallouts {
 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
         
         String strQuery = 'SELECT Id,RecordType.DeveloperName,Mortgage_Flag__c,Status,CreatedDate,Age_Days__c,' +
                        ' Approval_Status__c ' +
                        ' FROM Case' +
                        ' WHERE RecordType.DeveloperName = \'Mortgage\' ' +
                        ' AND Status = \'Submitted\' AND Mortgage_Flag__c = \'I\' ' ;

                        if( String.isNotBlank ( Label.FM_Service_Rec_Id_For_Test ) 
                        && Label.FM_Service_Rec_Id_For_Test.contains('On') 
                        && String.isNotBlank( Label.FM_Service_Rec_Id_For_Test.substringAfter('-') ) ) {
                            strQuery = strQuery + ' AND Id = \''+Label.FM_Service_Rec_Id_For_Test.substringAfter('-')+ '\'  ';
                        }else{
                            strQuery = strQuery + ' AND Age_Days__c >= 110  AND Extended__c = true';
                        }

         
         return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<Case > scope){ 
        Set<Id> caseIdS = new Set<Id>();
        for(Case caseObj : scope){
            System.debug('caseObj == ' + caseObj);
            caseIdS.add(caseObj.Id);
            caseObj.Status = 'Cancelled';
            caseObj.Approval_Status__c = 'Cancelled';
            }
            
            System.debug('caseIdS == ' + caseIdS);
         update scope;
           List<Task> TaskList = [Select Id, Status From Task WHERE
            WhatId IN :caseIdS
            AND (Status = 'NEW' OR Status = 'Open' OR Status = 'Not Started'  ) ];
           for(Task taskObj : TaskList){
            System.debug('taskObj == ' + taskObj);
            taskObj.Status = 'Cancelled';
           }
           update TaskList;

       

        //update scope;
    }//End Execute Method
  
    global void finish(Database.BatchableContext BC){
        System.debug('***FMAmountBUEmailNotifierBatch finish****');
        
    }
}