/****************************************************************************************************
* Name          : UserActionPlanTriggerHandler                                                      *
* Description   : Contains all events related to the User Action Plan object                        *
* Created Date  : 27/03/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    27/03/2018      Initial Draft.                                          *
****************************************************************************************************/
public class UserActionPlanTriggerHandler implements TriggerFactoryInterface{
    
    public UserActionPlanTriggerHandler() {
    }

    public void executeBeforeUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        UpdateUserActionPlan.verifyActionPlanTargetFields(mapOldRecords, mapNewRecords);
    }

    public void executeBeforeInsertTrigger(List<sObject> newRecordsList) {}
    public void executeAfterInsertTrigger(Map<Id, sObject> mapNewRecords){}
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
    public void executeBeforeInsertUpdateTrigger(list<sObject> lstNewRecords, Map<Id,sObject> mapOldRecords){}
    public void executeBeforeDeleteTrigger(Map<Id, sObject> mapOldRecords){}
    public void executeAfterDeleteTrigger(Map<Id, sObject> mapOldRecords){}
    public void executeAfterUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> mapOldRecords){}


}