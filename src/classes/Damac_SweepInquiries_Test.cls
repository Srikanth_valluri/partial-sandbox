@isTest
public class Damac_SweepInquiries_Test {
    public static testMethod void method1 () {
        Campaign__c camp = New Campaign__c();
            camp.End_Date__c = system.today().addmonths(10);
            camp.Marketing_End_Date__c = system.today().addmonths(10);
            camp.Marketing_Start_Date__c = system.today().addmonths(-10);
            camp.Start_Date__c =  system.today().addmonths(-10);
        insert camp;
        Inquiry__c inq = new Inquiry__c ();
            inq.Activity_Counter__c =101;
            inq.Inquiry_Status__c='Active';
            inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
            inq.campaign__c = camp.id;
            inq.Inquiry_Source__c = 'Customer Referral';
            inq.ownerId = userinfo.getUserId();
        insert inq;
        inq.Pre_InquiryId__c = inq.id;
        update inq;
        Task t = new Task();
            t.Subject='Donni';
            t.Status='Not Started';
            t.Priority='Normal';
            t.WhatID = inq.id;
            t.Activity_Outcome__c = 'Show';
            t.Activity_Type_3__c = 'Meeting at Office';
        insert t;       
        Damac_SweepInquiries obj = new Damac_SweepInquiries();
        DataBase.executeBatch(obj); 
        Damac_SweepInquiriesSchedule sh1 = new Damac_SweepInquiriesSchedule();
        sh1.execute(null); 
    }
    
    public static testMethod void method2 () {
        Campaign__c camp = New Campaign__c();
            camp.End_Date__c = system.today().addmonths(10);
            camp.Marketing_End_Date__c = system.today().addmonths(10);
            camp.Marketing_Start_Date__c = system.today().addmonths(-10);
            camp.Start_Date__c =  system.today().addmonths(-10);
        insert camp;
        Inquiry__c inq = new Inquiry__c ();
            inq.Activity_Counter__c =101;
            inq.Inquiry_Status__c='Active';
            inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
            inq.campaign__c = camp.id;
            inq.Inquiry_Source__c = 'Customer Referral';
            inq.ownerId = userinfo.getUserId();
        insert inq;
        inq.Pre_InquiryId__c = inq.id;
        update inq;
        Task t = new Task();
            t.Subject='Donni';
            t.Status='Not Started';
            t.Priority='Normal';
            t.WhatID = inq.id;
            t.Activity_Outcome__c = 'Show';
            t.Activity_Type_3__c = 'Meeting at Office';
        insert t;       
        Damac_SweepNewInquiries obj = new Damac_SweepNewInquiries();
        DataBase.executeBatch(obj); 
        Damac_SweepNewInquiriesSchedule sh1 = new Damac_SweepNewInquiriesSchedule();
        sh1.execute(null); 
    }
}