@isTest
public class SR_TenantPassortRESTresourceTest {
    @isTest(seeAllData = true)
    static void  test1(){
        User owner1 = [SELECT id, name, profileId, profile.name, Contact.accountId 
                       FROM User WHERE profile.name = 'Customer Community Login User(Use this)' 
                       AND IsActive = TRUE AND Contact.accountId != NULL LIMIT 1];
        User tenant1 = [SELECT id, name, profileId, profile.name, Contact.accountId 
                       FROM User WHERE profile.name = 'Tenant Community Login User' 
                       AND IsActive = TRUE AND Contact.accountId != NULL LIMIT 1];
        
        String ownerAcc1_ID, tenentAcc1_ID;
        if(NULL != owner1 && NULL != owner1.Contact && NULL != owner1.Contact.accountId){
            ownerAcc1_ID = String.valueOf(owner1.Contact.accountId);
        }
        if(NULL != tenant1 && NULL != tenant1.Contact && NULL != tenant1.Contact.accountId){
            tenentAcc1_ID = String.valueOf(tenant1.Contact.accountId);
        }
        
        Id passDetailChgCase_rtId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Passport Detail Update').getRecordTypeId();
        FM_Case__c testPassDetChgCase = new FM_Case__c();
        testPassDetChgCase.recordTypeId = passDetailChgCase_rtId;
        testPassDetChgCase.Account__c = tenentAcc1_ID;
        testPassDetChgCase.Type__c = 'Passport Detail Update';
        testPassDetChgCase.Request_Type__c = 'Passport Detail Update';
        testPassDetChgCase.Origin__c = 'Portal';
        testPassDetChgCase.Status__c = 'Draft Request';
        testPassDetChgCase.New_CR__c = 'PL0098RT98765';
        testPassDetChgCase.Passport_Issue_Date__c = Date.today();
        testPassDetChgCase.Passport_Issue_Place__c = 'document_issue_place';
        testPassDetChgCase.Passport_File_URL__c = 'passport_file_url';
        testPassDetChgCase.Additional_Doc_File_URL__c = 'additional_doc_file_url';
        insert testPassDetChgCase;
        
        FM_Case__c srCase = [SELECT id, name, Account__c FROM FM_Case__c WHERE Account__c = :tenentAcc1_ID LIMIT 1];
        
        String baseUrl = URL.getOrgDomainUrl().toExternalForm(); /* URL.getSalesforceBaseUrl() */
        
        Test.startTest();
        
        RestRequest request = new RestRequest();
        request.requestUri = baseUrl + '/services/apexrest/srCase/tenant/passport';
        request.httpMethod = 'GET';
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        SR_TenantPassortRESTresource.doGet(); /* 1 */
        
        request.addParameter('case_number', 'invalid');
        SR_TenantPassortRESTresource.doGet(); /* 2 */
        
        request.addParameter('account_id', String.valueOf(srCase.id));
        SR_TenantPassortRESTresource.doGet(); /* 3 */
        
        request.addParameter('account_id', String.valueOf(tenentAcc1_ID));
        SR_TenantPassortRESTresource.doGet(); /* 4 */
        
        request.addParameter('case_number', String.valueOf(srCase.name));
        SR_TenantPassortRESTresource.doGet(); /* 5 */
        
        
        SR_TenantPassortRESTresource.doDeleteDraft(); /* 6 */
        
        Test.stopTest();
    }
    
    @isTest(seeAllData = true)
    static void  test2(){
        User owner1 = [SELECT id, name, profileId, profile.name, Contact.accountId 
                       FROM User WHERE profile.name = 'Customer Community Login User(Use this)' 
                       AND IsActive = TRUE AND Contact.accountId != NULL LIMIT 1];
        User tenant1 = [SELECT id, name, profileId, profile.name, Contact.accountId 
                       FROM User WHERE profile.name = 'Tenant Community Login User' 
                       AND IsActive = TRUE AND Contact.accountId != NULL LIMIT 1];
        
        String ownerAcc1_ID, tenentAcc1_ID;
        if(NULL != owner1 && NULL != owner1.Contact && NULL != owner1.Contact.accountId){
            ownerAcc1_ID = String.valueOf(owner1.Contact.accountId);
        }
        if(NULL != tenant1 && NULL != tenant1.Contact && NULL != tenant1.Contact.accountId){
            tenentAcc1_ID = String.valueOf(tenant1.Contact.accountId);
        }
        
        Id personAcc_rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc1 = new Account( LastName = 'Test Account1',
                                       Party_ID__c = '63066662',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'test1@mailinator.com');
        insert acc1;
        Account acc2 = new Account( LastName = 'Test Account2',
                                       Party_ID__c = '63066663',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'test2@mailinator.com');
        acc2.Primary_Language__c = 'Arabic';
        insert acc2;
        
        Id passDetailChgCase_rtId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Passport Detail Update').getRecordTypeId();
        FM_Case__c testPassDetChgCase1 = new FM_Case__c();
        testPassDetChgCase1.recordTypeId = passDetailChgCase_rtId;
        testPassDetChgCase1.Account__c = ownerAcc1_ID;
        testPassDetChgCase1.Type__c = 'Passport Detail Update';
        testPassDetChgCase1.Request_Type__c = 'Passport Detail Update';
        testPassDetChgCase1.Origin__c = 'Portal';
        testPassDetChgCase1.Status__c = 'Draft Request';
        testPassDetChgCase1.New_CR__c = 'PL0098RT98765';
        testPassDetChgCase1.Passport_Issue_Date__c = Date.today();
        testPassDetChgCase1.Passport_Issue_Place__c = 'document_issue_place';
        testPassDetChgCase1.Passport_File_URL__c = 'passport_file_url';
        testPassDetChgCase1.Additional_Doc_File_URL__c = 'additional_doc_file_url';
        insert testPassDetChgCase1;
        
        FM_Case__c testPassDetChgCase2 = new FM_Case__c();
        testPassDetChgCase2.recordTypeId = passDetailChgCase_rtId;
        testPassDetChgCase2.Account__c = tenentAcc1_ID;
        testPassDetChgCase2.Type__c = 'Passport Detail Update';
        testPassDetChgCase2.Request_Type__c = 'Passport Detail Update';
        testPassDetChgCase2.Origin__c = 'Portal';
        testPassDetChgCase2.Status__c = 'Draft Request';
        testPassDetChgCase2.New_CR__c = 'PL0098RT98765';
        testPassDetChgCase2.Passport_Issue_Date__c = Date.today();
        testPassDetChgCase2.Passport_Issue_Place__c = 'document_issue_place';
        testPassDetChgCase2.Passport_File_URL__c = 'passport_file_url';
        testPassDetChgCase2.Additional_Doc_File_URL__c = 'additional_doc_file_url';
        insert testPassDetChgCase2;
        
        Map<String, Object> requestMap_post = new Map<String, Object>();
        requestMap_post.put('is_draft', false);
        requestMap_post.put('account_id', '');
        requestMap_post.put('draft_sr_id', '');
        requestMap_post.put('document_number', '');
        requestMap_post.put('document_expiry_date_string', '12/31/2029');
        requestMap_post.put('document_issue_place', 'Dubai');
        requestMap_post.put('passport_file_url', '');
        requestMap_post.put('additional_doc_file_url', 'https://testcdn/docs/6758765465');
        
        String reqJSON = JSON.serialize(requestMap_post);
        String baseUrl = URL.getOrgDomainUrl().toExternalForm(); /* URL.getSalesforceBaseUrl() */
        
        Test.startTest();
        
        RestRequest request = new RestRequest();
        request.requestUri = baseUrl + '/services/apexrest/srCase/tenant/passport';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(reqJSON);
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        callPostMethod(requestMap_post); /* 1 */
        
        requestMap_post.put('account_id', String.valueOf(testPassDetChgCase1.id));
        callPostMethod(requestMap_post); /* 2 */
        
        requestMap_post.put('document_number', 'TEST987654321');
        requestMap_post.put('is_draft', true);
        callPostMethod(requestMap_post); /* 3 */
        
        requestMap_post.put('document_expiry_date_string', '2029-12-30');
        callPostMethod(requestMap_post); /* 4 */
        
        requestMap_post.put('account_id', ownerAcc1_ID);
        callPostMethod(requestMap_post); /* 5 */
        
        requestMap_post.put('account_id', tenentAcc1_ID);
        callPostMethod(requestMap_post); /* 6 */
        
        requestMap_post.put('draft_sr_id', String.valueOf(testPassDetChgCase1.id));
        callPostMethod(requestMap_post); /* 7 */
        
        requestMap_post.put('draft_sr_id', String.valueOf(testPassDetChgCase2.id));
        callPostMethod(requestMap_post); /* 8 */
        
        requestMap_post.put('is_draft', false);
        requestMap_post.put('draft_sr_id', '');
        callPostMethod(requestMap_post); /* 9 */
        
        requestMap_post.put('draft_sr_id', String.valueOf(testPassDetChgCase1.id));
        callPostMethod(requestMap_post); /* 10 */
        
        requestMap_post.put('draft_sr_id', String.valueOf(testPassDetChgCase2.id));
        callPostMethod(requestMap_post); /* 11 */
        
        requestMap_post.put('passport_file_url', 'https://testcdn/docs/6758765465');
        callPostMethod(requestMap_post); /* 12 */
        
        Test.stopTest();
    }
    
    private static void callPostMethod(Map<String, Object> requestMap_post){
        SR_TenantPassortRESTresource.doPost(
        (Boolean)requestMap_post.get('is_draft'), 
        (String)requestMap_post.get('account_id'), 
        (String)requestMap_post.get('draft_sr_id'), 
        (String)requestMap_post.get('document_number'), 
        (String)requestMap_post.get('document_expiry_date_string'), 
        (String)requestMap_post.get('document_issue_place'), 
        (String)requestMap_post.get('passport_file_url'), 
        (String)requestMap_post.get('additional_doc_file_url')
        );
    }
}