@isTest
public class FundTransferFMFinanceTaskTest {
    public static testmethod void test_IPMSTaskCreation(){

        Id fundTransferRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Fund Transfer').getRecordTypeId();

        Account acctIns = TestDataFactoryFM.createAccount();
        acctIns.party_id__c = '123';
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c frombuIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        frombuIns.Inventory__c = invObj.id;
        insert frombuIns;

        Booking_Unit__c tobuIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        tobuIns.Inventory__c = invObj.id;
        tobuIns.Registration_ID__c = '111';
        insert tobuIns;

        FM_Case__c fmCaseIns = new FM_Case__c();
        fmCaseIns.Fund_Transfer_Amount__c = 10000;
        fmCaseIns.Fund_Transfer_To_Unit__c = tobuIns.Id;
        fmCaseIns.Type_of_Fund_Transfer__c = 'To property payment';
        fmCaseIns.Fund_Transfer_From_Unit__c = frombuIns.Id;
        fmCaseIns.Status__c = 'Submitted';
        fmCaseIns.recordtypeid = fundTransferRecordTypeId ;
        insert fmCaseIns;

        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());

        Task taskIns = new task();
        taskIns.subject = Label.FM_Fund_Transfer_Allocate_Funds_task_subject;
        taskIns.process_name__c = 'FM Fund Transfer';
        taskIns.status = 'Completed';
        taskIns.whatId = fmCaseIns.id;
        taskIns.Priority = 'Normal';
        taskIns.ActivityDate = Date.today().addDays(2);
        insert taskIns;

        List<Task> lstTask = new List<Task>();
        lstTask.add(taskIns);

        FundTransferFMFinanceTask.createTaskForFMFinance(lstTask);

    }
}