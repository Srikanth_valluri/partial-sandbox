public class InventoryDesignChangeController extends InventoryDesignChangeUtility {
    
    public string loadMsg{get; set;}
    public string cadURL{get; set;} 
    public string reasonForChange{get; set;}
    public list<LineData> csvLines{get; set;}
    public Design_Change_Request__c dcr{get; set;}
    public list<Design_Change_Request__c> pendingRequests{get; set;}
    public list<Design_Change_Request__c> completedRequests{get; set;}
    public list<SelectOption> propertyOptions{get; set;}
    public list<SelectOption> buildingOptions{get; set;}
    public string selBuilding{get; set;}
    public string selProperty{get; set;}
    public string recordId{get; set;}
    public string requestId{get; set;}
    
    private static Final ID recordTypeId = Schema.SObjectType.Design_Change_Request__c.getRecordTypeInfosByName()
        .get('JOPD').getRecordTypeId();
        
    public Boolean isVilla { get; set; }
    public InventoryDesignChangeController() {
        prepareData();
    }

    public void prepareData(){
        isVilla = false;
        requestId = ApexPages.CurrentPage().getParameters().get('id');
        csvLines = new list<LineData>();
        dcr = new Design_Change_Request__c();
        if(requestId != null){
            dcr = [Select id, Property__c, Building__c, status__c from Design_Change_Request__c where id=: requestId];
            readAttachment(requestId);
        }

        string mainQuery = 'Select id, Property__r.Name, '+string.join(readFieldset('DesignChangeFields', 'Design_Change_Request__c'),',')+' from Design_Change_Request__c ';
        list<string> compStatus = new list<string> {'Approved','Rejected'};        
        
        pendingRequests = new list<Design_Change_Request__c>();
        pendingRequests = database.query(mainQuery+' where RecordTypeId !=: recordTypeId AND status__c not in: compStatus order by createdDate desc');
        
        completedRequests = new list<Design_Change_Request__c>();
        completedRequests = database.query(mainQuery+' where RecordTypeId !=: recordTypeId AND status__c in: compStatus order by createdDate desc');
        
        propertyOptions = new list<SelectOption>();
        propertyOptions.add(new SelectOption('','--Select a property--'));
        for(Property__c prop: [Select id, name from Property__c]){
            propertyOptions.add(new SelectOption(prop.Id, prop.Name));
        }

        buildingOptions = new list<SelectOption>();
        buildingOptions.add(new SelectOption('','--Select a Building--'));
    }

    public void fetchBuildings(){
        System.debug('>>>>>>selProperty>>>>>>>'+selProperty);
        buildingOptions = new list<SelectOption>();
        buildingOptions.add(new SelectOption('','--Select a Building--'));
        for(Location__c loc : [Select id, name from Location__c where RecordType.name = 'Building' and Property_Name__c =: selProperty]){
            buildingOptions.add(new SelectOption(loc.id, loc.Name));
        }
    }

    public void uploadFile(){

        String fileString =  system.ApexPages.currentPage().getParameters().get('fileBlob');
        String fileName =  system.ApexPages.currentPage().getParameters().get('fileName');
        blob contentFile = EncodingUtil.base64Decode(fileString);

        string DataAsString = blobToString(contentFile,'ISO-8859-1');
        
        DataAsString = DataAsString.replaceAll('(\r\n|\r)','\n');
        list<string> Lines = DataAsString.split('\n');
        system.debug('fileLines Size: ' + Lines.size());
        isVilla = false;
        
        for (integer i = 0; i < 1; i++) {
            String line = lines[i].trim();
            list<string> vals = line.split(',');
            if (vals[0].toLowerCase() == 'plot no') {
                isVilla = true;
            }
        }
        if (isVilla) {
            for(integer i = 1; i< lines.size(); i++){
                string line = lines[i].trim();
                
                
                list<string> vals = line.split(',');
                system.debug (vals);
                if (vals.size () > 0) {
                    LineData d = new lineData();
                    d.plotNo =  vals[0];
                    d.protoType = vals[1];
                    d.View_Type = vals[2];
                    d.space_type = vals[3];
                    d.position = vals[4];
                    d.free_parking = vals[5];
                    d.plot_area_sqm = vals[6];
                    d.plot_area_sqft = vals[7];
                    d.max_gfa_sqm = vals[8];
                    d.max_gfa_sqft = vals[9];
                    d.net_sellablearea_sqm = vals[10];
                    d.net_sellablearea_sqft = vals[11];
                    csvLines.add(d);
                }
            }
        }
        if (!isVilla) {
            for(integer i = 1; i< lines.size(); i++){
                string line = lines[i].trim();
                list<string> vals = line.split(',');
                LineData d = new lineData();
                d.floor = vals[0];
                d.unit_no = vals[1];
                d.merge_unit_no = vals[2];
                d.bedroom = vals[3];
                d.suite_area_sqm = vals[4];
                d.suite_area_sqft = vals[5];
                d.balcony_area_sqm = vals[6];
                d.balcony_area_sqft = vals[7];
                d.terrace_area_sqm = vals[8];
                d.terrace_area_sqft = vals[9];
                d.common_area_sqm = vals[10];
                d.common_area_sqft = vals[11];
                
                d.sellable_area_sqm = vals[12];
                d.sellable_area_sqft = vals[13];
                d.View_Type = vals[14];
                d.space_type = vals[15];
                csvLines.add(d);
            }
        }
        /*
        list<string> csvLines2 = csvLines.clone();
        csvLines = new list<string>();
        for(string str : csvLines2){
            string replaceStr = str.replaceAll(',,,,,,,,,,,,,,,','');
            System.debug('>>>>>>>>replaceStr>>>>>>>>'+replaceStr);
            replaceStr = str.replaceAll('\r','');
            System.debug('>>>>>>>replaceStr>>>>>>>>'+replaceStr);
            if(str != '' && replaceStr != '')
                csvLines.add(str);
        }
        */

        dcr = new Design_Change_Request__c(); 
        dcr.CAD_URL__c = cadURL; 
        dcr.Reason_for_Change__c = reasonForChange;
        dcr.Status__c = 'Draft';
        dcr.File_Name__c = fileName;
        dcr.No_Of_lines__c = csvLines.size();
        insert dcr;

        Attachment att = new Attachment();
        att.parentId = dcr.Id;
        att.Body = contentFile;
        att.name = fileName;
        insert att;

        dcr.Attachment_View_URL__c = '/servlet/servlet.FileDownload?file='+att.Id;
        update dcr;

        loadMsg = 'File Uploaded Successfully'; 
        
    } 

    
    public void submitRequest(){
        loadMsg = '';
        if(dcr.Property__c != null && dcr.Building__c != null){
            dcr.Status__c = 'Submitted';
            update dcr;
            
            Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
            approvalRequest.setComments('DCR Submitted for approval');
            approvalRequest.setObjectId(dcr.Id);
            Approval.ProcessResult approvalResult = Approval.process(approvalRequest);

            loadMsg = 'SUCCESS';
        }
        else
            loadMsg = 'Please assign project and building.';
    }

    public void reviewRequest(){

    }

    public void assignProject(){
        dcr.Property__c = selProperty;
        dcr.Building__c = selBuilding;
        update dcr;

        Property__c prop = [Select id, name from Property__c where id =: selProperty];
        Location__c build = [Select id, name from Location__c where id =: selBuilding];
        
        set<string> unitKeys = new set<string>();
        for(linedata line : csvLines){
            unitKeys.add(prop.name+'/'+build.name+'/'+line.floor.replaceAll('Floor','').trim());
        }

        map<string, Inventory__c> mpExisting = new map<string, Inventory__c>();
        for(Inventory__c inv : [Select id, Unit_Key__c from Inventory__c where Unit_Key__c in: unitKeys]){
            mpExisting.put(inv.Unit_Key__c, inv);
        }

        for(linedata line : csvLines){
            string key = prop.name+'/'+build.name+'/'+line.floor.replaceAll('Floor','').trim();
            line.action = mpExisting.containsKey(key) ? 'Update' : 'Create';
            line.project = prop.name;
            line.building = build.name;
            line.projectId = prop.Id;
            line.buildingId = build.Id;
        }

    }

    public void deleteRequest(){
        System.debug('>>>>>>>>>>>>>>'+recordId);
        delete [Select id from Design_change_request__c where id=: recordId];
        prepareData(); 
    }

    public void readAttachment(Id requestId){
        isVilla = false;
        Attachment att = [Select id, name, body from Attachment where parentId =: requestId];
        Design_Change_Request__c icd = [Select id, name, Property__c, Property__r.name, Building__c, Building__r.name, status__c from Design_Change_Request__c where id =: requestId]; 
        string DataAsString = blobToString(att.body,'ISO-8859-1');
        DataAsString = DataAsString.replaceAll('(\r\n|\r)','\n');
        
        list<string> Lines = DataAsString.split('\n');
        system.debug('fileLines Size: ' + Lines.size());
        csvLines = new list<LineData>();

        set<string> unitKeys = new set<string>();
        for(integer i = 1; i< lines.size(); i++){
            list<string> vals = lines[i].split(',');
            if (vals.size () > 0)
                unitKeys.add(icd.Property__r.name+'/'+icd.Building__r.name+'/'+vals[0].replaceAll('Floor','').trim());
        }

        map<string, Inventory__c> mpExisting = new map<string, Inventory__c>();
        for(Inventory__c inv : [Select id, Unit_Key__c from Inventory__c where Unit_Key__c in: unitKeys]){
            mpExisting.put(inv.Unit_Key__c, inv);
        }
        for (integer i = 0; i < 1; i++) {
            String line = lines[i].trim();
            list<string> vals = line.split(',');
            if (vals[0].toLowerCase() == 'plot no') {
                isVilla = true;
            }
        }


        if (isVilla) {
            for(integer i = 1; i< lines.size(); i++){
                list<string> vals = lines[i].split(',');
                if (vals.size () > 0) {
                    LineData d = new lineData();
                    string key = icd.Property__r.name+'/'+icd.Building__r.name+'/'+vals[0].replaceAll('Floor','').trim();
                    if(icd.Property__c != null)
                        d.action = mpExisting.containsKey(key) ? 'Update' : 'Create';
                    else
                        d.action = '';
                    d.plotNo =  vals[0];
                    d.protoType = vals[1];
                    d.View_Type = vals[2];
                    d.space_type = vals[3];
                    d.position = vals[4];
                    d.free_parking = vals[5];
                    d.plot_area_sqm = vals[6];
                    d.plot_area_sqft = vals[7];
                    d.max_gfa_sqm = vals[8];
                    d.max_gfa_sqft = vals[9];
                    d.net_sellablearea_sqm = vals[10];
                    d.net_sellablearea_sqft = vals[11];
                    d.project = icd.Property__r.name;
                    d.building = icd.Building__r.name;
                    d.projectId = icd.Property__c;
                    d.buildingId = icd.Building__c;
                    csvLines.add(d);
                }
            }
        } else {
            for(integer i = 1; i< lines.size(); i++){
                list<string> vals = lines[i].split(',');
                LineData d = new lineData();
                string key = icd.Property__r.name+'/'+icd.Building__r.name+'/'+vals[0].replaceAll('Floor','').trim();
                if(icd.Property__c != null)
                    d.action = mpExisting.containsKey(key) ? 'Update' : 'Create';
                else
                    d.action = '';
                d.floor = vals[0];
                d.unit_no = vals[1];
                d.unit_Code = icd.Building__r.name+'/'+vals[0].replaceAll('Floor','').trim()+'/'+vals[1];
                d.floor_Code = vals[0].replaceAll('Floor','').trim();
                d.merge_unit_no = vals[2]; 
                d.bedroom = vals[3];
                d.suite_area_sqm = vals[4];
                d.suite_area_sqft = vals[5];
                d.balcony_area_sqm = vals[6];
                d.balcony_area_sqft = vals[7];
                d.terrace_area_sqm = vals[8];
                d.terrace_area_sqft = vals[9];
                d.common_area_sqm = vals[10];
                d.common_area_sqft = vals[11];
                d.sellable_area_sqm = vals[12];
                d.sellable_area_sqft = vals[13];
                d.view_type = vals[14];
                d.space_type = vals[15];
                
                d.project = icd.Property__r.name;
                d.building = icd.Building__r.name;
                d.projectId = icd.Property__c;
                d.buildingId = icd.Building__c;
                csvLines.add(d);
            }
        }

        System.debug('>>>>>>>>>>>>'+csvLines.size());
    }
}