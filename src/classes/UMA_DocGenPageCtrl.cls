/*--------------------------------------------------------------------------------------------------------------------_
Description: Controller to generate UMA document
=======================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By   | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     | 27-02-2020       | Aishwarya Todkar   | 1. Initial Draft
=======================================================================================================================
*/
public with sharing class UMA_DocGenPageCtrl {
    static Id caseId;
    public UMA_DocGenPageCtrl(ApexPages.StandardController controller) {
        Case objCase = ( Case )controller.getRecord();
        caseId = objCase.Id;
    }

/**********************************************************************************************************************
 * Description: method to generate UMA, POA/LOA document
 * Pararmeter(s):document Id
 * return: None
 **********************************************************************************************************************/
    public static void generateUMA() {
        generateDoc( 'LHO - UMA' );
        //generateDoc( 'LHO - POA' );
        generateDoc( 'LHO - LOA' );
    }

/**********************************************************************************************************************
 * Description: method to generate document from drawloop
 * Pararmeter(s): meta data name
 * return: None
 **********************************************************************************************************************/
    public static void generateDoc( String csName ) {
        //'LHO - UMA' 
        Riyadh_Rotana_Drawloop_Doc_Mapping__c cs = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( csName );
        if(cs != null ) {
                
            if( String.isBlank( cs.Drawloop_Document_Package_Id__c )) {
                ApexPages.addmessage(
                    new ApexPages.message( ApexPages.severity.ERROR, 
                    'Please provide Drawloop Document package Id of ' + csName )
                );
            }

            else if( String.isBlank( cs.Delivery_Option_Id__c )) {
                ApexPages.addmessage(
                    new ApexPages.message( ApexPages.severity.ERROR, 
                    'Please provide Delivery Option Id of ' + csName)
                );
            }
            else {
                try {
                    DrawloopDocGen.generateDoc( cs.Drawloop_Document_Package_Id__c
                                                , cs.Delivery_Option_Id__c
                                                , caseId);
                    ApexPages.addmessage(
                        new ApexPages.message( ApexPages.severity.INFO, 
                        'UMA Document generated!')
                    );                                
                } 
                catch (Exception ex ) {
                    ApexPages.addmessage(
                        new ApexPages.message( ApexPages.severity.ERROR, 
                        ex.getMessage() )
                    );
                }
                
            }
        }//End cs If
        else {
            ApexPages.addmessage(
                    new ApexPages.message( ApexPages.severity.ERROR, 
                    'Can not find Drawloop Document Package - ' + csName)
            );
        }
    }
}