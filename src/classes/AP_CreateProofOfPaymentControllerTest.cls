@isTest
public with sharing class AP_CreateProofOfPaymentControllerTest{
    @isTest
    public static void testWithBookunitId() {

        //Deal_SR__c
        NSIBPM__Service_Request__c serviceRequest =
            InitialiseTestData.createBookingServiceRequest(true, true, null, System.today());
        //Booking__c
        Booking__c booking = InitialiseTestData.createBookingRecords(null, serviceRequest, 200.00, 'Test');

        //Creating Booking_Unit__c test data
        List<Booking_Unit__c> listBookingUnit = new List<Booking_Unit__c>();
        listBookingUnit.add(new Booking_Unit__c(Booking__c = booking.Id));
        Booking_Unit__c bookingUnit = InitialiseTestData.createBookingUnitRecords(listBookingUnit).get(0);
        List<Buyer__c> buyerList = new List<Buyer__c>();
        buyerList.add(new Buyer__c(Booking__c = booking.Id));
        Buyer__c buyerObj = InitialiseTestData.createBuyerRecordsPortal(buyerList).get(0);
        AP_CreateProofOfPaymentController controller;

        Test.startTest();
        PageReference pageRef = Page.AP_CreateProofOfPaymentPage;
        Test.setCurrentPage(pageRef);
        //Set bookingUnitId param
        Apexpages.currentpage().getparameters().put('bookingUnitId', bookingUnit.Id);
        Apexpages.currentpage().getparameters().put('tokenAmt','200');
        controller = new AP_CreateProofOfPaymentController();

        controller.uploadSRAttachment('test','test');
        PageReference pg = controller.submitSR();
        //PageReference pg1 = controller.Save();
    //  testSave(serviceRequest,listBookingUnit,booking);
        Test.stopTest();

        /*System.assert(controller.lstSRRelatedBookingUnits.size() > 0);
        System.assertEquals(controller.selectedBookingUnit, String.ValueOf(bookingUnit.Id));
        System.assertEquals([SELECT
                                Booking__r.Deal_SR__c
                            FROM
                                Booking_Unit__c
                            WHERE
                                Id =: bookingUnit.Id].Booking__r.Deal_SR__c,
            AP_CreateProofOfPaymentController.insertPOP.Deal_SR__c);*/
    }

    @isTest
    public static void testWithoutBookunitId() {
        //Deal_SR__c
        NSIBPM__Service_Request__c serviceRequest =
            InitialiseTestData.createBookingServiceRequest(true, true, null, System.today());
        //Booking__c
        Booking__c booking = InitialiseTestData.createBookingRecords(null, serviceRequest, 200.00, 'Test');

        //Creating Booking_Unit__c test data
        List<Booking_Unit__c> listBookingUnit = new List<Booking_Unit__c>();
        listBookingUnit.add(new Booking_Unit__c(Booking__c = booking.Id));
        Booking_Unit__c bookingUnit = InitialiseTestData.createBookingUnitRecords(listBookingUnit).get(0);
        List<Buyer__c> buyerList = new List<Buyer__c>();
        buyerList.add(new Buyer__c(Booking__c = booking.Id));
        Buyer__c buyerObj = InitialiseTestData.createBuyerRecordsPortal(buyerList).get(0);
        Test.startTest();
        PageReference pageRef = Page.AP_CreateProofOfPaymentPage;
        Test.setCurrentPage(pageRef);

        //Set parentId param
        Apexpages.currentpage().getparameters().put('id', serviceRequest.Id);
        AP_CreateProofOfPaymentController controller = new AP_CreateProofOfPaymentController();
        PageReference pg = controller.submitSR();
        //PageReference pg1 = controller.Save();
        //testSave(serviceRequest,listBookingUnit,booking);
        Test.stopTest();

        
    }

    @isTest
    public static void testGetBookingUnits() {
        //Creating test service Request
        NSIBPM__Service_Request__c serviceRequest =
            InitialiseTestData.createBookingServiceRequest(true, true, null, System.today());

        //Creating test Booking
        Booking__c booking = InitialiseTestData.createBookingRecords(null, serviceRequest, 200.00, 'Test');

        //Creating test Booking_Unit__c
        List<Booking_Unit__c> listBookingUnit = new List<Booking_Unit__c>();
        listBookingUnit.add(new Booking_Unit__c(Booking__c = booking.Id));
        List<Booking_Unit__c> createBookingUnitList =
            InitialiseTestData.createBookingUnitRecords(listBookingUnit);
        List<Buyer__c> buyerList = new List<Buyer__c>();
        buyerList.add(new Buyer__c(Booking__c = booking.Id));
        Buyer__c buyerObj = InitialiseTestData.createBuyerRecordsPortal(buyerList).get(0);
        Test.startTest();
        PageReference pageRef = Page.AP_CreateProofOfPaymentPage;
        Test.setCurrentPage(pageRef);

        //Set parentId param
        Apexpages.currentpage().getparameters().put('id', serviceRequest.Id);
        AP_CreateProofOfPaymentController controller = new AP_CreateProofOfPaymentController();
        List<SelectOption> options = AP_CreateProofOfPaymentController.getbookingUnitStatus();
        Test.stopTest();

        System.assert(options.size() > 0);
    }

    @isTest
    public static void testGetPaymentMode() {
        Test.startTest();
        PageReference pageRef = Page.AP_CreateProofOfPaymentPage;
        Test.setCurrentPage(pageRef);
        List<String> options = AP_CreateProofOfPaymentController.getPaymentMode();
        Test.stopTest();

        System.assert(options.size() > 0);
    }
    public static testmethod void WithoutCaseID(){
      
      testSave();
    }
    //public static void testSave(NSIBPM__Service_Request__c serviceRequest,List<Booking_Unit__c> listBookingUnit,Booking__c booking) {
    public static void testSave(){
    //Deal_SR__c
        NSIBPM__Service_Request__c serviceRequest =
            InitialiseTestData.createBookingServiceRequest(true, true, null, System.today());
        //Booking__c
        Booking__c booking = InitialiseTestData.createBookingRecords(null, serviceRequest, 200.00, 'Test');

        //Creating Booking_Unit__c test data
        List<Booking_Unit__c> listBookingUnit = new List<Booking_Unit__c>();
        listBookingUnit.add(new Booking_Unit__c(Booking__c = booking.Id));
        Booking_Unit__c bookingUnit = InitialiseTestData.createBookingUnitRecords(listBookingUnit).get(0);
        //List<Booking_Unit__c> createBookingUnitList =InitialiseTestData.createBookingUnitRecords(listBookingUnit);
            List<Buyer__c> buyerList = new List<Buyer__c>();
        buyerList.add(new Buyer__c(Booking__c = booking.Id));
        Buyer__c buyerObj = InitialiseTestData.createBuyerRecordsPortal(buyerList).get(0);
        string fileBody= 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        string fileName= 'C/fakepath/Document_1.txt';
        String jsonStr = '{"bu":"' + bookingUnit.Id +
                        '","curr":"curr","pa":"2000","pd":"01/01/2018","pm":"Cash","dealSR1":"' +
                        serviceRequest.Id +'","fileBody":"'+fileBody+'","fileName":"'+fileName+
                        '","blobfileThirdParty":"","filenameThirdParty":""}';
        Id POPcase = Schema.SObjectType.case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        case objcase = new case();
        //objcase.AccountID = loggedinuser.accountid;
        objcase.RecordtypeID = POPcase;
        objcase.SR_Type__c = 'Proof of Payment SR';
        objcase.Type = 'Proof of Payment SR';
        insert objcase;
        
        //PageReference pageRef = Page.AP_CreateProofOfPaymentPage;
        //Test.setCurrentPage(pageRef);
        //Proof_of_Payment__c pop = AP_CreateProofOfPaymentController.save(jsonStr);
        AP_CreateProofOfPaymentController controller = new AP_CreateProofOfPaymentController();
        
        
        
        SOAPCalloutServiceMock.returnToMe = new Map<String, MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
        MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
        response.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"SR Header Data Created for SR # :2-005180","PARAM_ID":"2-005180"},{"PROC_STATUS":"S","PROC_MESSAGE":"Created SR Task Data for SR # :2-005180 Task :Verify Proof of Payment Details in IPMS","PARAM_ID":"2-005180"},{"PROC_STATUS":"S","PROC_MESSAGE":"[UNIT] Created Unit Details for SR # 2-005180 Reg Id : 60652","PARAM_ID":"2-005180"},{"PROC_STATUS":"S","PROC_MESSAGE":"[UNIT] Created Unit Details for SR # 2-005180 Reg Id : 60301","PARAM_ID":"2-005180"}],"message":"Process Completed Returning 4 Response Message(s)","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        response.return_x = '';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response);
       
        Test.startTest();
        //PageReference pg = controller.submitSR();
             Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );
            controller.strCaseID =objcase.id ;
            controller.stringifyinqobj = jsonStr;
            controller.Save();
        Test.stopTest();

        //System.assert(pop.Id != null );
    }

}