public without sharing class MyDocumentsController {
    public Id personAccountId{get; set{
            personAccountId = value;
            attachmentListMethod();
        }}
    public List<SR_Attachments__c> attachmentList {get; set;}
    public MyDocumentsController () {
        attachmentList = new List<SR_Attachments__c>();
    }
    public void attachmentListMethod() {
        if (personAccountId != NULL) {
            attachmentList = [
                SELECT Id
                     , Name
                     , IsValid__c
                     , Attachment_URL__c
                  FROM SR_Attachments__c
                 WHERE Case__c IN (SELECT Id
                                     FROM Case
                                    WHERE AccountId = :personAccountId
                                  )
            ];
        }

        //return attachmentList;
    }
}