/***********************************************************************
* Description - Test class for DuesPopulationBatch batch class
*
* Version            Date            Author        Description
* 1.0                09/09/19        Arjun		   Initial Draft
**********************************************************************/
@isTest
private class DuesPopulationBatchTest{

    private static Id getRecordTypeId(){
        return Schema.SObjectType.Case.getRecordTypeInfosByName().get('Mortgage').getRecordTypeId();
    }

	@isTest static void noException() {

        DuesPopulationBatch batchCls = new DuesPopulationBatch();

        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;

        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;

        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id; 

        BU.Registration_ID__c = '123135'; 
        BU.Seller_Name__c = 'test';
        BU.Unit_Location_AR__c = 'test';
        BU.Plot_Number__c = 'test';
        BU.Property_Name__c = 'test';
        BU.Property_Name_Arabic__c = 'test';
        BU.Seller_Name_AR__c = 'test';
        BU.Requested_Price__c = 435531;
        insert BU;

        Case caseObj = TestDataFactory_CRM.createCase(Acc.Id, getRecordTypeId());

        caseObj.Booking_Unit__c = BU.Id;
        caseObj.Mortgage_Status__c = 'Mortgage Generate NOC';
        caseObj.Mortgage_Bank_Name__c = 'Test Bank';
        caseObj.Mortgage_Value__c = 21313332;
        caseObj.Mortgage_Start_Date__c = System.today();
        caseObj.Mortgage_End_Date__c = System.today();
        caseObj.EOI_reference_Number__c = '115';
        caseObj.status = 'Submitted';
        caseObj.Type = 'Mortgage';
        caseObj.Mortgage_Flag__c = 'I';
        insert caseObj;


		RefreshTokenService.AgingBucketResponse agingBucket = new RefreshTokenService.AgingBucketResponse();
		agingBucket.ATTRIBUTE2 = '';
		agingBucket.ATTRIBUTE5 = '';
		agingBucket.ATTRIBUTE6 = '';
		agingBucket.ATTRIBUTE13 = '';
		agingBucket.ATTRIBUTE14 = '';
		agingBucket.ATTRIBUTE15 = '';
		agingBucket.ATTRIBUTE16 = '';
		agingBucket.ATTRIBUTE17 = '';
		agingBucket.ATTRIBUTE18 = '';
		agingBucket.ATTRIBUTE19 = '';
		agingBucket.ATTRIBUTE20 = '';
		agingBucket.ATTRIBUTE21 = '';	
		agingBucket.ATTRIBUTE22 = '';
		agingBucket.ATTRIBUTE23 = '';		

		Test.startTest();
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockCallingList());
            Database.executeBatch(batchCls);
		Test.stopTest();
	}

	@isTest static void withException() {

        DuesPopulationBatch batchCls = new DuesPopulationBatch();

        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;

        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;

        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id; 

        BU.Registration_ID__c = '123135'; 
        BU.Seller_Name__c = 'test';
        BU.Unit_Location_AR__c = 'test';
        BU.Plot_Number__c = 'test';
        BU.Property_Name__c = 'test';
        BU.Property_Name_Arabic__c = 'test';
        BU.Seller_Name_AR__c = 'test';
        BU.Requested_Price__c = 435531;
        insert BU;

        Case caseObj = TestDataFactory_CRM.createCase(Acc.Id, getRecordTypeId());

        caseObj.Booking_Unit__c = BU.Id;
        caseObj.Mortgage_Status__c = 'Mortgage Generate NOC';
        caseObj.Mortgage_Bank_Name__c = 'Test Bank';
        caseObj.Mortgage_Value__c = 21313332;
        caseObj.Mortgage_Start_Date__c = System.today();
        caseObj.Mortgage_End_Date__c = System.today();
        caseObj.EOI_reference_Number__c = '115';
        caseObj.status = 'Submitted';
        caseObj.Type = 'Mortgage';
        caseObj.Mortgage_Flag__c = 'I';
        insert caseObj;


		RefreshTokenService.AgingBucketResponse agingBucket = new RefreshTokenService.AgingBucketResponse();
		agingBucket.ATTRIBUTE2 = '';
		agingBucket.ATTRIBUTE5 = '';
		agingBucket.ATTRIBUTE6 = '';
		agingBucket.ATTRIBUTE13 = '';
		agingBucket.ATTRIBUTE14 = '';
		agingBucket.ATTRIBUTE15 = '';
		agingBucket.ATTRIBUTE16 = '';
		agingBucket.ATTRIBUTE17 = '';
		agingBucket.ATTRIBUTE18 = '';
		agingBucket.ATTRIBUTE19 = '';
		agingBucket.ATTRIBUTE20 = '';
		agingBucket.ATTRIBUTE21 = '';	
		agingBucket.ATTRIBUTE22 = '';
		agingBucket.ATTRIBUTE23 = '';		

		Test.startTest();
            Database.executeBatch(batchCls);
		Test.stopTest();
	}
}