/****************************************************************************************
* Description - Test  Class for utilComArabicXsd
*
* Version            Date            Author                              Description
* 1.0                23/11/17        Naresh Kaneriya (Accely)           Initial Draft
****************************************************************************************/
@isTest
public class utilComArabicXsdTest{

  public static testmethod void AttributeArrForAll(){
      
     utilComArabicXsd.AttributeArrForAll   objAttributeArrForAll = new utilComArabicXsd.AttributeArrForAll ();  
     utilComArabicXsd.AttributeArr1   objAttributeArr1 = new utilComArabicXsd.AttributeArr1 ();  
     System.assert(objAttributeArrForAll!=null);
     System.assert(objAttributeArr1!=null);
    } 
  
}