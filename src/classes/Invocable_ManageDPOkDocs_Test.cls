@isTest
public class Invocable_ManageDPOkDocs_Test {
    
    @testSetup 
    static void setupData() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;
        
        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.id;
        nstp1.Step_No__c = 2;
        nstp1.Step_Status__c = 'Docs OK';
        nstp1.Step_Type__c = 'Document Verification';
        insert nstp1;
    }
    
    @isTest 
    static void test_Invocable_ManageDPOkDocs() {
        Invocable_ManageDPOkDocs obj = new Invocable_ManageDPOkDocs();
        NSIBPM__Service_Request__c sr = [select id from NSIBPM__Service_Request__c limit 1];
        New_Step__c stp = [select id From New_Step__c where Service_Request__c =: sr.id Limit 1];
        Invocable_ManageDPOkDocs.UpdateBuyer(new List<Id>{stp.Id});
    }
}