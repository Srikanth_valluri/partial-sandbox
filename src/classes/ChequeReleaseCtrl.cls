/*************************************************************************************************************************************
* Description - ChequeReleaseSR Process page controller.
*------------------------------------------------------------------------------------------------------------------------------------
* Version        Date          Author              Description
*------------------------------------------------------------------------------------------------------------------------------------
* 1.0            05/01/2021   Aishwarya Todkar     Initial Draft
**************************************************************************************************************************************/
public class ChequeReleaseCtrl {
    
    public transient String jsonFiles{get;set;} // files json string storing property with saving view state
    Public List<Wrapper>lstfilesWrap{get;set;}
    public Id accId;
    public String selectedDocumentType {get;set;}
    
    Public Case caseObj{get;set;}
    // constructor for class
    public ChequeReleaseCtrl(){ 
      
        caseObj=new Case(); 
        accId=ApexPages.currentPage().getParameters().get('AccountId');
          system.debug('AccountId:-'+accId);
    }
    // assign doucment types to selectoptions
    public List<SelectOption> getDocumentType() 
    {
       List < SelectOption > lstDocumentTypes=new List < SelectOption >();
        Schema.DescribeFieldResult schemaResult = SR_Attachments__c.Document_Type__c.getDescribe ();
        List < Schema.PicklistEntry > lstPicklistValues = schemaResult.getPicklistValues();
        //system.debug(lstPicklistValues);
        for ( Schema.PicklistEntry picklistValue: lstPicklistValues ) {
            if(lstDocumentTypes.isEmpty()){
              lstDocumentTypes.add(new SelectOption('---None---','---None---'));  
            }
            else{
            lstDocumentTypes.add(new SelectOption(picklistValue.getLabel(), picklistValue.getValue()));
            }
        }
        return lstDocumentTypes;
    }
    //Method deserialize the json string to wrapper class
    public void getfiles() {
        system.debug('jsonString:='+jsonFiles);
        lstfilesWrap=(List<Wrapper>)System.JSON.deserialize(jsonFiles,List<Wrapper>.class); //deserialize json string to wrapper class 
        system.debug('jsonfiles:-'+lstfilesWrap); 
        List<SR_Attachments__c> lstSRAttach=new List<SR_Attachments__c>();
        for(Wrapper wrapObj: lstfilesWrap){
        SR_Attachments__c  attchObj=new SR_Attachments__c();
                           attchObj.Name=wrapObj.filename;
                         // attchObj.Attachment__c=wrapObj.filebody;
                          attchObj.Document_Type__c=wrapObj.documentType;
            lstSRAttach.add(attchObj);
        }
        try{
           insert lstSRAttach;
        }
         catch(DmlException e) {
         System.debug('An unexpected error has occurred: ' + e.getMessage());
        }
        
    }
    
    //Wrapper class for files
    public class Wrapper{  
        public String filename{get;set;}
        public String filebody{get;set;}
        public String documentType{get;set;}
        // Wrapper class constructor
        public Wrapper(String filename,String filebody,String documentType){      
            this.filename=filename;
            this.filebody=filebody;
            this.documentType=documentType;
        }
        
    } 
    
}