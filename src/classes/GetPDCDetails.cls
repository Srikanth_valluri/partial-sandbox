/**********************************************************************************************
Description: A Service to fetch PDC details and create PDC Calling List and Post Dated Cheque
-----------------------------------------------------------------------------------------------
Version Date(DD-MM-YYYY)    Author              Version History
-----------------------------------------------------------------------------------------------
1.0     10-03-2029          Aditya Kishor       Initial Draft
2.0     09-09-2020          Aishwarya Todkar    Populated booking unit and Account
***********************************************************************************************/
public class GetPDCDetails{//AKISHOR: 10March2020 to fetch pdc details
    
    public list<Account> acntid;
    public static Map<String, Object> output;   

    public static void fetchPdcDetails() {
        
        Credentials_Details__c creds = Credentials_Details__c.getInstance( 'PDC Details' );
        
        /*String userName = 'oracle_user';
        String password = 'crp1user';
        String endPoint = 'http://83.111.194.181:8045/webservices/rest/XXDC_PROCESS_SERVICE_WS/retrieve/';*/
        if( creds != null ) {
            String userName = creds.user_Name__c;
            String password = creds.password__c;
            String endPoint = creds.endPoint__c;
        
            String requestBody = '{'+
                                '   "RETRIEVE_Input": {'+
                                '       "RESTHeader": {'+
                                '           "Responsibility": "ONT_ICP_SUPER_USER",'+
                                '           "RespApplication": "ONT",'+
                                '           "SecurityGroup": "STANDARD",'+
                                '           "NLSLanguage": "AMERICAN"'+
                                '       },'+
                                '       "InputParameters": {'+
                                '           "P_REQUEST_NUMBER": "A2232",'+
                                '           "P_SOURCE_SYSTEM": "SFDC",'+
                                '           "P_REQUEST_NAME": "GET_PDC_DETAILS"'+
                                '           ,'+
                                '           "P_REQUEST_MESSAGE": {'+
                                '               "PARAM_ID": "",'+
                                '               "ATTRIBUTE1": "4"'+
                                '           }'+
                                '       }'+
                                '   }'+
                                '}';
            
            System.debug('request = ' + requestBody);
            String headerValue = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(userName + ':' + password));            
            HttpRequest request = new HttpRequest();
            request.setMethod('POST');
            request.setHeader('Accept', 'application/json');
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Accept-Language', 'en-US');
            request.setHeader('Authorization', headerValue);
            request.setBody(requestBody);
            request.setEndPoint(endPoint);
            System.debug('request = ' + request);
            System.debug('requestBody = ' + request.getBody());
            Http http = new Http();
            HttpResponse response = new HttpResponse();

            try {
                
                response = http.send(request);

                System.debug('response body ==' + response.getBody());

                if( response != null && response.getBody() != null) {
                    
                    //Parse the response
                    PDCResponse responseObj = ( PDCResponse )System.JSON.deserialize( response.getBody(), PDCResponse.class );
                    Id pdcCLRecTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('PDC Calling List').getRecordTypeId();
                    Map<String, Booking_Unit__c> mapRegIdToBU = new Map<String, Booking_Unit__c>();
                    Set<String> setRegIds = new Set<String>();
                    List<Calling_List__c> listCl = new List<Calling_List__c>();
                    List<Post_Dated_Cheque__c> listPDC = new List<Post_Dated_Cheque__c>();
                    System.debug('deserialized response 2: ' + responseObj);
                    
                    if( responseObj!= null
                    && responseObj.OutputParameters != null 
                    && responseObj.OutputParameters.X_RESPONSE_MESSAGE != null 
                    && responseObj.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM != null
                    && responseObj.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.size() > 0 ) {
                        
                        for( X_RESPONSE_MESSAGE_ITEM objItem : responseObj.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM ) {
                            if( String.isNotBlank( objItem.ATTRIBUTE1 ) ) {
                                setRegIds.add( objItem.ATTRIBUTE1 );
                            }
                        }

                        if( !setRegIds.isEmpty() ) {
                            for( Booking_Unit__c objBu : [SELECT 
                                                                Id
                                                                , Booking__r.Account__c
                                                                , Registration_Id__c
                                                            FROM
                                                                Booking_Unit__c
                                                            WHERE
                                                                Registration_Id__c IN: setRegIds ] ) {
                                mapRegIdToBU.put( objBu.Registration_Id__c, objBu );
                            }
                        }

                        for( X_RESPONSE_MESSAGE_ITEM objItem : responseObj.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM ) {

                            System.debug('Inside For loop--' + responseObj.OutputParameters );
                            //Creating Calling List
                            Calling_List__c objCl = new Calling_List__c();
                            objCl.Registration_ID__c = objItem.ATTRIBUTE1; 
                            objCl.PDC_Cheque_Amount__c = objItem.ATTRIBUTE4;
                            objCl.PDC_Check__c = objItem.ATTRIBUTE2;
                            objCl.PDC_Cheque_Receipt_Id__c = objItem.ATTRIBUTE6;
                            objCl.PDC_Check_Date__c = objItem.ATTRIBUTE3;
                            objCl.PDC_Comments__c = objItem.ATTRIBUTE7;
                            objCl.RecordTypeId = pdcCLRecTypeId;
                            
                            listCl.add( objCl );
                            
                            //Creating PDC
                            Post_Dated_Cheque__c objPDC = new Post_Dated_Cheque__c();
                            objPDC.Registration_ID__c = objItem.ATTRIBUTE1; 
                            objPDC.Amount__c = Decimal.valueOf(objItem.ATTRIBUTE4);
                            objPDC.Cheque_Number__c = objItem.ATTRIBUTE2;
                            objPDC.Cheque_Date__c = formatDate(objItem.ATTRIBUTE3);
                            
                            /**
                             * Added by Aishwarya Todkar to populate booking unit and Account 
                             * Date - 09-09-2020
                             */
                            
                            if( mapRegIdToBU.containsKey( objItem.ATTRIBUTE1 ) ) {
                                objPDC.Booking_Unit__c = mapRegIdToBU.get( objItem.ATTRIBUTE1 ).Id;
                                objPDC.Account__c = mapRegIdToBU.get( objItem.ATTRIBUTE1 ).Booking__r.Account__c;
                            }
                            listPDC.add( objPDC );  
                            
                                
                        }//End For

                        System.debug('listCl size--' + listCl.size());
                        System.debug('listCl--' + listCl);

                        if( listCl != null && listCl.size() > 0 )
                            insert listCl;
                        insert listPDC; 
                    }//End Response Parsing
                }//End Response if
            }//End try
            catch ( Exception ex ) {
                System.debug( 'ex==' + ex);
            }
        }
    }      
    //'20-JUL-2020'
    public static  date formatDate( String strDate ){
        Map<String, Integer> myMap = new Map<String, Integer>{
                                                    'JAN' => 01,
                                                    'FEB' => 02, 
                                                    'MAR' => 03,
                                                    'APR' => 04,
                                                    'MAY' => 05, 
                                                    'JUN' => 06,
                                                    'JUL' => 07,
                                                    'AUG' => 08, 
                                                    'SEP' => 09,
                                                    'OCT' => 10,
                                                    'NOV' => 11, 
                                                    'DEC' => 12
                                                    };
        
        List<String > lstStr = strDate.Split('-');
        return Date.newInstance(Integer.valueOf(lstStr[2]),
            myMap.get(lstStr[1]),
            Integer.valueOf(lstStr[0])
        );
        
    }

    public class PDCResponse{       
        public OutputParameters OutputParameters; 
    }
    
    public class OutputParameters {  
        public String xmlns;
        public String xmlns_xsi;
        public X_RESPONSE_MESSAGE X_RESPONSE_MESSAGE;
        public String X_RETURN_STATUS;
        public String X_RETURN_MESSAGE;
    }
    

    public class X_RESPONSE_MESSAGE_ITEM {
        public String ATTRIBUTE1;
        public String ATTRIBUTE2;
        public String ATTRIBUTE3;
        public String ATTRIBUTE4;
        public String ATTRIBUTE5;
        public String ATTRIBUTE6;
        public String ATTRIBUTE7;
        public Object ATTRIBUTE8;
        public Object ATTRIBUTE9;
        public Object ATTRIBUTE10;
        public Object ATTRIBUTE11;
        public Object ATTRIBUTE12;
        public Object ATTRIBUTE13;
        public Object ATTRIBUTE14;
        public Object ATTRIBUTE15;
        public Object ATTRIBUTE16;
        public Object ATTRIBUTE17;
        public Object ATTRIBUTE18;
        public Object ATTRIBUTE19;
        public Object ATTRIBUTE20;
        public Object ATTRIBUTE21;
        public Object ATTRIBUTE22;
        public Object ATTRIBUTE23;
        public Object ATTRIBUTE24;
        public Object ATTRIBUTE25;
        public Object ATTRIBUTE26;
        public Object ATTRIBUTE27;
        public Object ATTRIBUTE28;
        public Object ATTRIBUTE29;
        public Object ATTRIBUTE30;
        public Object ATTRIBUTE31;
        public Object ATTRIBUTE32;
        public Object ATTRIBUTE33;
        public Object ATTRIBUTE34;
        public Object ATTRIBUTE35;
        public Object ATTRIBUTE36;
        public Object ATTRIBUTE37;
        public Object ATTRIBUTE38;
        public Object ATTRIBUTE39;
        public Object ATTRIBUTE40;
    }

    public class X_RESPONSE_MESSAGE {
        public List<X_RESPONSE_MESSAGE_ITEM> X_RESPONSE_MESSAGE_ITEM;
    }
}