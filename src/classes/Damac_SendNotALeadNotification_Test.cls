@istest
public class Damac_SendNotALeadNotification_Test{
    
    
    static testmethod void sendEmailNotification(){
        Inquiry__c i = new Inquiry__c();
        i.First_Name__c = 'Lead';
        i.Last_Name__c = 'Lead';
        i.Email__c = 'lead@lead.com';
        i.Inquiry_Source__c = 'Web';
        i.Organisation_Name__c = 'abc';
        i.Inquiry_Status__c  = 'Not a Lead';
        insert i;
        
        // Insert Inquiry related tasks
        task tsk = new task();
        tsk.whatid = i.id;
        tsk.subject = 'Testing';
        tsk.Calling_Number__c = '971557030756';
        insert tsk;
        
        Damac_SendNotALeadNotification.sendNotificationEmail (new List <ID> {tsk.id});
    }
}