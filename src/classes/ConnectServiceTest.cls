@isTest
public class ConnectServiceTest {


    
    /*public class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
           
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"data":{"response":"2349"},"meta_data":""}');
            res.setStatusCode(200);
            return res;
        }
    }*/
    
     public class MockHttpResponseGenerator1 implements HttpCalloutMock {
    // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
           
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"data":{"access_token":"23499890879879879"},"meta_data":""}');
            res.setStatusCode(200);
            return res;
        }
    }
    @isTest
    static void testPushDetails(){
    
        String label = Label.Validate_Request_and_Calculations;
       
        Account objAccount = new Account();
        objAccount.LastName = 'test';
        objAccount.Email__c = 'success@mailinator.com';
        objAccount.Party_ID__c = '1234';
        insert objAccount;
        
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('DEWA_Refund').getRecordTypeId();
        
        Case objCase = new Case();
        objCase.recordTypeId = recordTypeId;
        objCase.BIC_IBAN__c = 'IBAN';
        objCase.Bank_Name__c = 'name';
        objCase.Bank_Code__c = 'code';
        objCase.DEWA_Transfered_Date__c = System.Today();
        objCase.Client_Dewa_No__c = '12345';
        objCase.Move_In_Date__c = System.Today();
        objCase.Refund_Amount__c = 12000.0;
        //objCase.Unit_Name__c = 'test';
        insert objCase;
        
        
        Location__c objLocation = new Location__c();
        objLocation.Name = 'test';
        objLocation.Location_ID__c = '12345';
        insert objLocation;
        
        Inventory__c objInventory = new Inventory__c();
        objInventory.Building_Code__c = 'TXR';
        objInventory.Building__c = objLocation.Id;
        insert objInventory;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id; 
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = '3901';
        objBU.Unit_Name__c = 'test/test';
        objBU.Booking__c = objBooking.Id;
        objBU.Recovery__c = 'A';
        objBU.Inventory__c = objInventory.Id;
        insert objBU;
        
           
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'Connect Service Token';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        settings.Endpoint__c = 'https://ws.dev.aramex.net/ShippingAPI.V2/Shipping/Service_1_0.svc/json/CreateShipments';
        insert settings;
        
        Credentials_Details__c settings1 = new Credentials_Details__c();
        settings1.Name = 'Connect Service';
        settings1.User_Name__c = 'Some Value';
        settings1.Password__c = 'Some Value';
        settings1.Resource__c = 'Some Value';
        settings1.grant_type__c = 'Some Value';
        settings1.Endpoint__c = 'https://stgservices.damacgroup.com/api/projectconnect/workflow/73';
        insert settings1;
        
        List<Task> taskList = new List<Task>();
        Task objTask = new Task();
        objTask.WhatId = objCase.Id;
        objTask.Subject = System.Label.Validate_Request_and_Calculations;
        insert objTask;
        taskList.add(objTask);
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator1());
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        Test.startTest();
        ConnectService objConnect = new ConnectService();
        objConnect.pushDetailsToConnect(taskList);
        Test.stopTest();
    }
}