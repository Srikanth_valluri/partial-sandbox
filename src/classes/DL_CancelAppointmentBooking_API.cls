/**********************************************************************************************************************
Description: This API is used for cancelling an Appointment booked by the portal account user.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   27-11-2020      | Subin Antony		  | Created initial draft
***********************************************************************************************************************/

@RestResource(urlMapping='/appointment/cancell')
global class DL_CancelAppointmentBooking_API {
    public static Map<Integer, String> statusCodeMap;
    static {
        statusCodeMap = new Map<Integer, String>{
            1 => 'Success',
            2 => 'Failure',
            3 => 'Bad Request',
            6 => 'Something went wrong',
            7 => 'Info'
        };
    }
	
    @HttpPost
    global static FinalReturnWrapper cancelAppointmentBooking(String account_id, String bu_id, String appointment_id) {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg, exceptionMsg, successMsg;
        
        if(String.isBlank(account_id)){
            errorMsg = 'Please provide account_id.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        if(String.isBlank(bu_id)){
            errorMsg = 'Please provide bu_id.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        if(String.isBlank(appointment_id)){
            errorMsg = 'Please provide appointment_id.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        Account accountRecord;
        try{
            accountRecord = [SELECT Id, Name, isPersonAccount, Email__pc, Email__c, 
                             Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c, Primary_Language__c 
                             FROM Account WHERE Id = :account_id];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
            accountRecord = NULL;
        }
        if(NULL == accountRecord){
            errorMsg = 'The account_id provided does not match any records.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        Booking_unit__c bookingUnit;
        try{
            bookingUnit = [Select Id, Name, Property_Name__c, Unit_Name__c, Registration_ID__c, Booking__c, Booking__r.Account__c 
                           FROM Booking_Unit__c WHERE Booking__r.Account__c = :account_id AND Id = :bu_id];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
            bookingUnit = NULL;
        }
        if(NULL == bookingUnit) {
            errorMsg = 'The booking unit ID provided does not match any valid records.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        // query the list of booked appointments(calling list)
        Id appointSchedRecTypID = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
        List<Calling_List__c> bookedAppointments;
        try{
            bookedAppointments = [SELECT id, name, Appointment_Status__c 
                                  FROM Calling_List__c WHERE recordTypeID = :appointSchedRecTypID 
                                  AND Account__c = :account_id AND Booking_Unit__c = :bu_id AND id = :appointment_id];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
            bookedAppointments = NULL;
        }
        if(NULL == bookedAppointments || bookedAppointments.size() < 1) {
            errorMsg = 'Could not retrive the appointment record.';
            return getErrorResponse(2, statusCodeMap.get(2), errorMsg, exceptionMsg);
        }
        else {
            Calling_List__c appointmentToCancel = bookedAppointments[0];
            if(!appointmentToCancel.Appointment_Status__c.equalsIgnoreCase('Cancelled')) {
                appointmentToCancel.Appointment_Status__c = 'Cancelled';
                
                try {
                    update appointmentToCancel;
                }
                catch(Exception ex){
                    system.debug(ex.getMessage());
                    exceptionMsg = ex.getMessage() + ' : ' + ex.getStackTraceString();
                    errorMsg = 'Error while cancelling the appointment.';
                    return getErrorResponse(2, statusCodeMap.get(2), errorMsg, exceptionMsg);
                }
            }
            
            // send success response
            FinalReturnWrapper responseWrapper = new FinalReturnWrapper();
            cls_meta_data responseMetaData = new cls_meta_data();
            responseMetaData.status_code = 1;
            responseMetaData.title = statusCodeMap.get(1);
            responseMetaData.message = 'Successfully cancelled the appointment booking.';
            responseWrapper.meta_data = responseMetaData;
            
            cls_data appointmentDetails = new cls_data();
            appointmentDetails.appointment_id = appointmentToCancel.id;
            appointmentDetails.appointment_status = appointmentToCancel.Appointment_Status__c;
            responseWrapper.data = appointmentDetails;
            
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
            return responseWrapper;
        }
    }
    
    private static FinalReturnWrapper getErrorResponse(Integer statusCode, String title, 
    String responseMessage, String devMessage) {
        FinalReturnWrapper responseWrapper = new FinalReturnWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = statusCode;
        responseMetaData.title = title;
        responseMetaData.message = responseMessage;
        responseMetaData.developer_message = devMessage;
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
        return responseWrapper;
    }
    
    global class FinalReturnWrapper {
        public cls_meta_data meta_data;
        public cls_data data;
    }
    
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }
    
    public class cls_data {
        public String appointment_id;
        public String appointment_status;
    }
}