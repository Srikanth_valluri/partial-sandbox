@isTest
private class UpdateCountOfCallLogOnCLBatchTest {
    
    static testMethod void test1 () {
        Id callingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
        
        List<TriggerOnOffCustomSetting__c> settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'CreateHoCLOnUnitNoticeSent',OnOffCheck__c = true));
        insert settingLst2;
        
        // Insert Account
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        List<Calling_List__c> lstCallingLists = createCallingList(callingRecordTypeId , 1 ,objAcc.Id ,false);
        insert lstCallingLists;
        
        Call_Log__c objCallLog = new Call_Log__c();
        objCallLog.Call_Recording_URL__c = '34534dfsgabds';
        objCallLog.Calling_Number__c = '342354234';
        objCallLog.Calling_List__c = lstCallingLists[0].Id;
        insert objCallLog;
       
        Test.startTest();
            Database.executeBatch(new UpdateCountOfCallLogOnCLBatch());
        Test.stopTest();
    }
       

    /*
     @ Description : To create calling list reocord
     @ Return      : list of calling list to be created
    */
    public static List<Calling_List__c> createCallingList( Id RecordTypeIdCollection, Integer counter , Id acctId,Boolean flag ) {
        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        for( Integer i=0; i<counter; i++ ) {
            lstCallingLists.add(new Calling_List__c(IsHideFromUI__c = false, Account__c = acctId  , Customer_Flag__c  = flag ,RecordTypeId = RecordTypeIdCollection ) );
            //lstCallingLists.add(new Calling_List__c(IsHideFromUI__c = false, Account__c = acctId  , Customer_Flag__c  = true ,RecordTypeId = RecordTypeIdCollection ) );
        }
        return lstCallingLists;
    }   
}