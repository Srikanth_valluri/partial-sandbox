/*-------------------------------------------------------------------------------------------------
Description: Controller for NOC for Fit Out component
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
v1.0     | 14-02-2019       | Lochana Rajput   | 1. Updated code to display total fees with VAT for
                                                    NON -DAMAC Hills
v2.0     | 19-02-2019       | Lochana Rajput   | 1. Updated approving authorities based on building type
=============================================================================================================================
*/

public virtual without sharing class NOCForFitOutController {

    public String strHelpPageId { get; set; }
    public String strAccountId { get; set; }
    public String strSelectedUnit { get; set; }
    public String strCaseId { get; set; }
    public String strSRType { get; set; }

    public transient String strDocumentBody { get; set; }
    public transient String strDocumentName { get; set; }
    public transient String deleteAttRecId { get; set; }
    public transient Boolean doInsertCase {get; set;}
    //public transient String strDisplayMessage { get; set; }
    //public transient String strMinorChangeNote { get; set; }
    //public transient String strMajorChangeNote { get; set; }
    public Decimal decMinServiceCharge { get; set; }

    public Booking_Unit__c objUnit { get; set; }

    public FM_Case__c objFMCase { get; set; }

    public Location__c objLocation { get; set; }

    public list<SelectOption> lstAlterationTypes { get; set; }
    public list<SelectOption> lstNOCTypes { get; set; }
    public list<String> lstSelectedAltTypes { get; set; }
    public list<String> lstSelectedNOCTypes { get; set; }

    public map<String, SR_Attachments__c> mapUploadedDocs { get; set; }
    public map< String, FM_Documents__mdt > mapMetaDocs { get; set; }

    public Integer mapDocSize {get;set;}

    @testVisible
    protected map<String, FM_Documents__mdt> mapProcessDocuments { get; set; }
    protected String contractorConsultantSuccessMessage = 'Consultant/Contractor Notified Successfully';


    public NOCForFitOutController() {

        strCaseId = ApexPages.currentPage().getParameters().get('Id');
        System.debug('strCaseId: ' + strCaseId);
        doInsertCase = false;

        if( String.isBlank( strCaseId ) ) {
            strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
            strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
            strSRType = ApexPages.currentPage().getParameters().get('SRType');
        }
        else {
            objFMCase = FM_Utility.getCaseDetails( strCaseId ) ;
            strAccountId = objFMCase.Account__c ;
            strSelectedUnit = objFMCase.Booking_Unit__c ;
            strSRType = objFMCase.Request_Type_DeveloperName__c ;
        }

        System.debug('objFMCase: ' +  objFMCase);

        if( String.isNotBlank( strSelectedUnit ) ) {
            init();
        }
        mapDocSize = 0;
    }

    protected NOCForFitOutController(Boolean shouldCall) {}

    public void init() {

        doInsertCase = false;

        objUnit = FM_Utility.getUnitDetails( strSelectedUnit ) ;

        if( String.isBlank( strCaseId ) && String.isNotBlank( strAccountId ) && String.isNotBlank( strSelectedUnit ) ){
            list<FM_Case__c> lstExistingCases = FM_Utility.getExistingFMCase( 'Request_For_Built_in_Drawings', strAccountId, strSelectedUnit );
            objFMCase = lstExistingCases != NULL && lstExistingCases.size() > 0 ? lstExistingCases[0] : NULL ;
            initializeFMCase();
        }

        if(validateOutstandingServiceCharges()) {

            system.debug('== Outstanding_service_charges__c =='+objFMCase.Outstanding_service_charges__c);

            initAlterationTypeWrapperList();

            /*if( String.isNotBlank( strSRType ) && objUnit != NULL ) {
                initDocumentInfoWrapperList();
            }*/
            if( objUnit != NULL && String.isNotBlank( objUnit.Unit_Name__c ) ) {
                list<Location__c> lstLocation = FM_Utility.getLocationDetails( new set<String>{ objUnit.Unit_Name__c.subString( 0, objUnit.Unit_Name__c.indexOf( '/' ) ) } );
                system.debug('== lstLocation =='+lstLocation);
                if( lstLocation != NULL && !lstLocation.isEmpty() ) {
                    objLocation = lstLocation[0];
                    system.debug('== objLocation =='+objLocation);
                    Boolean areaIsNumeric = true;
                    try {
                        Decimal jopdArea = Decimal.valueOf(objUnit.JOPD_Area__c);
                    } catch(Exception excp) {
                        areaIsNumeric = false;
                    }
                    if( objLocation.Drawing_Review_Charges__c != NULL && objUnit != NULL &&
                    	String.isNotBlank( objUnit.JOPD_Area__c ) && areaIsNumeric ) {
                    	objFMCase.NOC_Fees__c = objLocation.Drawing_Review_Charges__c * Decimal.valueOf( objUnit.JOPD_Area__c ) ;
                    	objFMCase.NOC_Fees__c = objFMCase.NOC_Fees__c > objLocation.Maximum_Cap__c ? objLocation.Maximum_Cap__c : objFMCase.NOC_Fees__c ;
                        // v1.0
                        //Get VAT value to be applied from label
                        Decimal VATvalue = Decimal.valueOf(Label.FM_NOC_VAT);
                        System.debug('VATvalue = ' + VATvalue);
                        objFMCase.NOC_Fees_Including_VAT__c = objFMCase.NOC_Fees__c + (objFMCase.NOC_Fees__c*VATvalue)/100;
                        objFMCase.NOC_Fees_Including_VAT__c = objFMCase.NOC_Fees_Including_VAT__c.setScale(2);
                        objFMCase.NOC_Fees__c = objFMCase.NOC_Fees__c.setScale(2);
                        System.debug('objFMCase.NOC_Fees_Including_VAT__c = ' + objFMCase.NOC_Fees_Including_VAT__c);
                    }
                    System.debug('objFMCase.NOC_Fees__c = ' + objFMCase.NOC_Fees__c);
                }
            }
            if( String.isNotBlank( strSRType ) && objUnit != NULL ) {
                initDocumentInfoWrapperList();
            }
        }
    }

    protected virtual Boolean validateOutstandingServiceCharges() {
        if( objUnit != NULL ) {
            try {
                FmIpmsRestServices.DueInvoicesResult objResult =
                                        FmIpmsRestServices.getDueInvoices( objUnit.Registration_Id__c,
                                                                           '',
                                                                           objUnit.Inventory__r.Property_Code__c );
                system.debug('== objResult =='+objResult);
                //if( objResult != NULL ) {
                    objFMCase.Outstanding_service_charges__c = objResult != NULL && String.isNotBlank( objResult.totalDueAmount ) ? objResult.totalDueAmount : '0';
                //}
            }
            catch( Exception e ) {
            	objFMCase.Outstanding_service_charges__c = '0';
                system.debug( '==Exception while fecthing oustanding balance=='+e.getMessage() );
            }
        }

        system.debug('== Outstanding_service_charges__c =='+objFMCase.Outstanding_service_charges__c);


        list<FM_process__mdt> lstfmProcessMetadata = [ SELECT Minimum_Outstanding_Charge__c
                                                         FROM FM_process__mdt
                                                        WHERE DeveloperName = :strSRType ];


        decMinServiceCharge = lstfmProcessMetadata != NULL && !lstfmProcessMetadata.isEmpty() &&
                              lstfmProcessMetadata[0].Minimum_Outstanding_Charge__c != NULL ?
                              lstfmProcessMetadata[0].Minimum_Outstanding_Charge__c : 0 ;


        system.debug('== decMinServiceCharge =='+decMinServiceCharge );

        return objFMCase != NULL
                && String.isNotBlank(objFMCase.Outstanding_service_charges__c)
                && Decimal.valueOf(objFMCase.Outstanding_service_charges__c) <= decMinServiceCharge;
    }

    public PageReference createNOCForFitOutCase() {
        if( objFMCase != NULL ) {
            objFMCase.Type_of_Alteration__c = lstSelectedAltTypes != NULL && !lstSelectedAltTypes.isEmpty() ?
                                              String.join( lstSelectedAltTypes, ';' ) : '';

            objFMCase.Type_of_NOC__c = lstSelectedNOCTypes != NULL && !lstSelectedNOCTypes.isEmpty() ?
                                       String.join( lstSelectedNOCTypes, ';' ) : '';
            objFMCase.Contact_Email__c = objFMCase.Email__c != NULL ? objFMCase.Email__c : objFMCase.Email_2__c ;
            objFMCase.Status__c = 'In Progress';
            try {
                //addContactInformation();
                upsert objFMCase ;

                List<Profile> lstProfiles = [ SELECT Name
                                                FROM Profile
                                               WHERE Id = :UserInfo.getProfileId() LIMIT 1 ];

                if( lstProfiles != NULL && !lstProfiles.isEmpty() &&
                    lstProfiles[0].Name.equalsIgnoreCase( 'Facilities Management Site Profile' ) ) {
                    ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.CONFIRM, 'Application Saved Successfully' ) );
                    return NULL;
                }

                return new PageReference('/' + objFMCase.Id);
            }
            catch( Exception e ) {
                Error_Log__c objError = FM_Utility.createErrorLog( strAccountId,
                                                                   strSelectedUnit,
                                                                   objFMCase.Id,
                                                                   'NOC for Fit out/Alterations',
                                                                   e.getMessage() + ' ' + e.getLineNumber() );
                insert objError ;
            }
        }
        return NULL ;
    }

    public PageReference submitNOCForFitOutCase() {
        system.debug( '==objFMCase=='+objFMCase );
        if( objFMCase != NULL ) {
            objFMCase.Type_of_Alteration__c = lstSelectedAltTypes != NULL && !lstSelectedAltTypes.isEmpty() ?
                                              String.join( lstSelectedAltTypes, ';' ) : '';

            objFMCase.Type_of_NOC__c = lstSelectedNOCTypes != NULL && !lstSelectedNOCTypes.isEmpty() ?
                                       String.join( lstSelectedNOCTypes, ';' ) : '';
            objFMCase.Contact_Email__c = objFMCase.Email__c != NULL ? objFMCase.Email__c : objFMCase.Email_2__c ;
            objFMCase.Status__c = 'Submitted';

            //Populate the FM Manager email address and Admin details.
            if( String.isNotBlank( objFMCase.Unit_Name__c ) ) {
                list<FM_User__c> lstUsers = FM_Utility.getApprovingUsers( new set<String> { objFMCase.Unit_Name__c.split('/')[0] } );
                for( FM_User__c objUser : lstUsers ) {
                    if( String.isNotBlank( objUser.FM_Role__c ) ) {
                        if( objUser.FM_Role__c.containsIgnoreCase( 'Manager') ) {
                            objFMCase.FM_Manager_Email__c = objUser.FM_User__r.Email;
                        }
                        else if( objUser.FM_Role__c.containsIgnoreCase( 'Admin') &&
                                 objFMCase.Admin__c == NULL ) {
                            objFMCase.Admin__c = objUser.FM_User__c;
                        }
                    }
                }
            }

            try {
                //addContactInformation();
                submitForApproval();
                system.debug( '==objFMCase=='+objFMCase );
                upsert objFMCase ;

                List<Profile> lstProfiles = [ SELECT Name
                                                FROM Profile
                                               WHERE Id = :UserInfo.getProfileId() LIMIT 1 ];

                if( lstProfiles != NULL && !lstProfiles.isEmpty() &&
                    lstProfiles[0].Name.equalsIgnoreCase( 'Facilities Management Site Profile' ) ) {
                    ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.CONFIRM, 'Application Submitted Successfully' ) );
                    return NULL;
                }

                PageReference objPage = new PageReference('/' + objFMCase.Id );
                return objPage ;

            }
            catch( Exception e ) {
                Error_Log__c objError = FM_Utility.createErrorLog( strAccountId,
                                                                   strSelectedUnit,
                                                                   objFMCase.Id,
                                                                   'NOC for Fit out/Alterations',
                                                                   e.getMessage() + ' ' + e.getLineNumber() );
                insert objError ;
            }
        }
        return NULL ;
    }

    public void uploadDocument() {
        if(objFMCase != NULL) {
            System.debug( '-->> objFMCase in uploadDocument: '+objFMCase );
            if (objFMCase.Id == NULL) {
                //insertCase(); Commented this due to error of uncommited work pending
                // if (objFMCase.Id == NULL) {
                ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.WARNING, Label.NOC_Fitout_Upload_Error ) );
                System.debug( '-->> hereee ');
                return;
                // }
            }
            UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
            list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
            if( String.isNotBlank( strDocumentName ) && String.isNotBlank( strDocumentBody ) ) {
                lstWrapper.add( PenaltyWaiverHelper.makeWrapperObject( EncodingUtil.Base64Encode( RentalPoolTerminationHelper.extractBody( strDocumentBody ) ) ,
                                                                       strDocumentName ,
                                                                       strDocumentName ,
                                                                       objUnit.Registration_Id__c, objFMCase.Id, '1' ) );

            }
            if( !lstWrapper.isEmpty() ) {
                objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper );
                if( objResponse != NULL && objResponse.data != NULL ) {
                    insertCustomAttachment( objFMCase.Id, objResponse );
                }
            }
        }
    }

    @testVisible
    protected void insertCustomAttachment( Id fmCaseId, UploadMultipleDocController.data objResponse ) {

        strDocumentName = strDocumentName.substring( 0, strDocumentName.lastIndexOf('.') );

        SR_Attachments__c objCustAttach ;
        for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            objCustAttach = new SR_Attachments__c();
            objCustAttach.FM_Account__c = strAccountId ;
            objCustAttach.Attachment_URL__c = objFile.url;
            objCustAttach.Booking_Unit__c = strSelectedUnit ;
            objCustAttach.FM_Case__c = fmCaseId ;
            objCustAttach.Name = strDocumentName ;
            //objCustAttach.Type__c = objDocMeta != NULL ? objDocMeta.Type__c : '' ;
            objCustAttach = setDocumentValidity(objCustAttach);
            strDocumentName = '';
            strDocumentBody = '';
        }

        insert objCustAttach ;
        initDocumentInfoWrapperList();
    }

    protected virtual SR_Attachments__c setDocumentValidity(SR_Attachments__c doc) {
        doc.isValid__c = true;
        return doc;
    }

    public void deleteAttachment() {

        System.debug('deleteAttRecId: ' + deleteAttRecId);
        System.debug('mapUploadedDocs.values(): ' + mapUploadedDocs.values());

        if( String.isNotBlank( deleteAttRecId ) ) {
            for( SR_Attachments__c objAttach  : mapUploadedDocs.values() ) {
                if( objAttach.Id == deleteAttRecId ) {
                    delete objAttach ;
                }
            }
            mapUploadedDocs.clear();
            initDocumentInfoWrapperList();
        }
    }

    public void insertCase() {
        try {
            system.debug( '==Inside TRY==' );
            System.debug('objFMCase: ' + objFMCase);
            if( objFMCase.Id == NULL ) {
                system.debug( '==Inserting Case=='+objFMCase );
                insert objFMCase ;
                System.debug('After insert objFMCase: ' + objFMCase);
                objFMCase = FM_Utility.getCaseDetails( objFMCase.Id );
            }

        }
        catch( Exception e ) {
            system.debug( '==Exception=='+e.getMessage() );
            Error_Log__c objError = FM_Utility.createErrorLog( strAccountId,
                                                               strSelectedUnit,
                                                               objFMCase.Id,
                                                               'NOC for Fit out/Alterations',
                                                               e.getMessage() );
            insert objError ;
        }
    }

    public void notifyContractorConsultant() {
        try {
        	system.debug('==Person_To_Collect__c=='+objFMCase.Person_To_Collect__c);
        	system.debug('==Email__c=='+objFMCase.Email__c);
        	system.debug('==Email_2__c=='+objFMCase.Email_2__c);
        	if( String.isNotBlank( objFMCase.Person_To_Collect__c ) &&
        	( ( objFMCase.Person_To_Collect__c.equalsIgnoreCase('CONTRACTOR') && String.isNotBlank( objFMCase.Email_2__c ) ) ||
        	  ( objFMCase.Person_To_Collect__c.equalsIgnoreCase('CONSULTANT') ) && String.isNotBlank( objFMCase.Email__c ) ) ) {
        		objFMCase.Contact_Email__c = objFMCase.Email__c != NULL ? objFMCase.Email__c : objFMCase.Email_2__c ;
        		objFMCase.Is_Contractor_Consultant_Notified__c=true;
        		upsert objFMCase ;
        		/*if( objFMCase.Id == NULL  ) {
	                insertCase();
	            }
	            FM_NotifyGuestUserController objNotification = new FM_NotifyGuestUserController( new ApexPages.StandardController( objFMCase ) );
	            objNotification.createSMSHistoryRecord();*/
	            ApexPages.addMessage(new ApexPages.Message(
                    ApexPages.Severity.CONFIRM, contractorConsultantSuccessMessage
                ));
        	}
        	else {
        		ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.WARNING, 'Person to collect must be Consultant/Contractor and respective email should be provided' ) );
        	}
        }
        catch( Exception e ) {
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, e.getMessage() ) );
        }
    }

    @testVisible
    protected void submitForApproval() {
        //Identifying the building type i.e property type
        //Get MAster Community property names from label
        List<String> lstPropertyNames = new List<String>();
        lstPropertyNames = Label.DAMAC_Hills_Properties.split(',');
        String buildingType = lstPropertyNames.contains(objUnit.Property_Name__c) ?
                                Label.FM_Master_Community_Label : Label.FM_Other_Building_label;
        //objFMCase.Approval_Status__c = 'Pending';
        //objFMCase.Submit_for_Approval__c = true ;
        objFMCase.Approving_Authorities__c = '';
        if( String.isNotBlank( objFMCase.Request_Type_DeveloperName__c ) ) {
        	list<FM_Approver__mdt> lstApprovers = [SELECT Id
							                            , Role__c
							                            , DeveloperName
														, FM_Process__r.DeveloperName
							                         FROM FM_Approver__mdt
							                        WHERE FM_Process__r.DeveloperName = 'NOC_for_Fit_out_Alterations'
							                          AND Is_Active__c = true
							                          AND City__c IN ( 'All', :objFMCase.Booking_Unit__r.Property_City__c )
                                                      AND Building_Type__c =: buildingType
												 ORDER BY Level__c ASC] ;
            for( FM_Approver__mdt objApproMeta : lstApprovers ) {
                objFMCase.Approving_Authorities__c += objApproMeta.Role__c + ',';
            }
            objFMCase.Approving_Authorities__c = objFMCase.Approving_Authorities__c.removeEnd(',');
        }
    }

    /*@testVisible
    protected void addContactInformation() {
        if( objFMCase != NULL && objFMCase.Account__c != NULL ) {
            Account objAcc = fetchAccount( objFMCase.Account__c );
            objFMCase.Mobile_Country_Code__c = objAcc.Mobile_Country_Code__c ;
            objFMCase.Mobile_Country_Code_2__c = objAcc.Mobile_Country_Code_2__c ;
            objFMCase.Mobile_Country_Code_3__c = objAcc.Mobile_Country_Code_3__c ;
            objFMCase.Mobile_Country_Code_4__c = objAcc.Mobile_Country_Code_4__c ;
            objFMCase.Mobile_Country_Code_5__c = objAcc.Mobile_Country_Code_5__c ;
            objFMCase.Mobile_no__c = objAcc.Mobile_Phone_Encrypt__c ;
            objFMCase.Mobile_Phone_2__c = objAcc.Mobile_Phone_2__c ;
            objFMCase.Mobile_Phone_3__c = objAcc.Mobile_Phone_3__c ;
            objFMCase.Mobile_Phone_4__c = objAcc.Mobile_Phone_4__c ;
            objFMCase.Mobile_Phone_5__c = objAcc.Mobile_Phone_5__c ;
            objFMCase.Email__c = objAcc.Email__c ;
            objFMCase.Email_2__c = objAcc.Email_2__c ;
            objFMCase.Email_3__c = objAcc.Email_3__c ;
        }
    }*/

    @testVisible
    protected void initializeFMCase() {
        objFMCase = objFMCase == NULL ? new FM_Case__c() : objFMCase ;
        //Populating Account email
        if(String.isNotBlank( strAccountId )) {
            for(Account objAcc : [SELECT Email__pc FROM Account where id=: strAccountId]) {
                objFMCase.Account_Email__c = objAcc.Email__pc != NULL ? objAcc.Email__pc : '';
            }
        }
        if( String.isBlank( strCaseId ) &&
            String.isNotBlank( strAccountId ) &&
            String.isNotBlank( strSelectedUnit ) ) {
            objFMCase.Id = NULL ;
        }
        objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('NOC For FitOut').getRecordTypeId();
        objFMCase.Account__c = strAccountId ;
        objFMCase.Booking_Unit__c = strSelectedUnit ;
        objFMCase.Status__c = 'New';
        objFMCase = setCaseOrigin(objFMCase);
        objFMCase.Request_Type__c = 'NOC for Fit out/Alterations';
        objFMCase.Request_Type_DeveloperName__c = strSRType ;
    }

    protected virtual FM_Case__c setCaseOrigin(FM_Case__c fmCase) {
        fmCase.Origin__c = 'Walk-In';
        return fmCase;
    }

    @testVisible
    protected void initAlterationTypeWrapperList() {
        //Alteration Types
        lstSelectedAltTypes = objFMCase != NULL && String.isNotBlank( objFMCase.Type_of_Alteration__c ) ?
                              objFMCase.Type_of_Alteration__c.split(';') :
                              new list<String>() ;

        lstAlterationTypes = new list<SelectOption>();
        for( Schema.PicklistEntry f : FM_Case__c.Type_of_Alteration__c.getDescribe().getPicklistValues() ) {
            lstAlterationTypes.add( new SelectOption( f.getValue(), f.getLabel() ) );
        }

        //NOC Types
        lstSelectedNOCTypes = objFMCase != NULL && String.isNotBlank( objFMCase.Type_of_NOC__c ) ?
                              objFMCase.Type_of_NOC__c.split(';') :
                              new list<String>() ;

        lstNOCTypes = new list<SelectOption>();
        for( Schema.PicklistEntry f : FM_Case__c.Type_of_NOC__c.getDescribe().getPicklistValues() ) {
            lstNOCTypes.add( new SelectOption( f.getValue(), f.getLabel() ) );
        }
    }


    public void initDocumentInfoWrapperList() {
        System.debug('mapUploadedDocs: ' + mapUploadedDocs);
        mapUploadedDocs = mapUploadedDocs == NULL ? new map<String, SR_Attachments__c>() : mapUploadedDocs ;
        mapMetaDocs = new map< String, FM_Documents__mdt >();
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            for( SR_Attachments__c objAttch : FM_Utility.getDocumentsList( objFMCase.Id ) ) {
                mapUploadedDocs.put(objAttch.Name, objAttch );
            }
        }
        System.debug('mapUploadedDocs 2: ' + mapUploadedDocs);

        for( FM_Documents__mdt objDoc : FM_Utility.getDocumentsList( strSRType, objUnit.Property_City__c ) ) {
            system.debug('== objLocation =='+objLocation);
            system.debug('== objDoc =='+objDoc);

            //system.debug('== objLocation.Property_Name__r.Name =='+objLocation.Property_Name__r.Name);

            //Added the bellow code to test the document upload part.
            // if(objDoc.MasterLabel != NULL){
            //     system.debug('==>> 12 == '+objDoc.MasterLabel);
            //     mapMetaDocs.put( objDoc.MasterLabel , objDoc );
            //     break;
            // }
            

            if(objLocation.Property_Name__c != NULL && objLocation.Property_Name__r.Name != NULL
                && objLocation.Property_Name__r.Name.contains('DAMAC HILLS') && objDoc.isMasterCommunity__c &&
                !mapUploadedDocs.containsKey( objDoc.MasterLabel )){
                mapMetaDocs.put( objDoc.MasterLabel , objDoc );
            }
            else if(objLocation.Property_Name__r.Name != NULL
            && objLocation.Property_Name__r.Name.contains('DAMAC HILLS') == FALSE && !mapUploadedDocs.containsKey( objDoc.MasterLabel ) && String.isNotBlank( objDoc.NOC_Type__c ) &&
            	checkCommonElements( objDoc.NOC_Type__c.split(','), lstSelectedNOCTypes ) ) {
                mapMetaDocs.put( objDoc.MasterLabel , objDoc );
            }
        }

        mapUploadedDocs = mapUploadedDocs.isEmpty() ? NULL : mapUploadedDocs ;
        mapMetaDocs = mapMetaDocs.isEmpty() ? NULL : mapMetaDocs ;
        system.debug('== mapUploadedDocs =='+mapUploadedDocs);
        //system.debug('== mapUploadedDocs size=='+mapUploadedDocs.size());
        system.debug('== mapMetaDocs =='+mapMetaDocs);
        if(mapMetaDocs != NULL && !mapMetaDocs.isEmpty()) {
            mapDocSize = mapMetaDocs.size();
        }
        //system.debug('== mapMetaDocs size=='+mapMetaDocs.size());

        //inserting Case;
        System.debug('doInsertCase: ' + doInsertCase);
        if(doInsertCase == null) {
            doInsertCase = false;
        }
        
        if(doInsertCase) {
            insertCase();
        }
        

    }

    @testVisible
    protected Boolean checkCommonElements( list<String> lstNOCTypes, list<String> lstSelectedNOC ) {
    	if( lstNOCTypes != NULL && !lstNOCTypes.isEmpty() &&
    		lstSelectedNOC != NULL && !lstSelectedNOC.isEmpty() ) {
    		lstSelectedNOC.add('All');
    		for( String strNOC : lstNOCTypes ) {
    			if( lstSelectedNOC.contains( strNOC ) ) {
    				return true ;
    			}
    		}
    	}
    	return false ;
    }

    /*@testVisible
    protected Account fetchAccount( Id accountId ){
        return [ SELECT Mobile_Country_Code__c
                      , Mobile_Country_Code_2__c
                      , Mobile_Country_Code_3__c
                      , Mobile_Country_Code_4__c
                      , Mobile_Country_Code_5__c
                      , Mobile_Phone_Encrypt__c
                      , Mobile_Phone_2__c
                      , Mobile_Phone_3__c
                      , Mobile_Phone_4__c
                      , Mobile_Phone_5__c
                      , Email__c
                      , Email_2__c
                      , Email_3__c
                   FROM Account
                  WHERE Id = :accountId ];

    }*/

    /*public class DocumentInfoWrapper {
        public FM_Documents__mdt objDocMeta { get; set; }
        public SR_Attachments__c objInsertedDoc { get; set; }

        public DocumentInfoWrapper ( FM_Documents__mdt objDocMetaTemp, SR_Attachments__c objDocTemp ) {
            objDocMeta = objDocMetaTemp ;
            objInsertedDoc = objDocTemp ;
        }
    }   */
}