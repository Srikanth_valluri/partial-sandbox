@isTest
public class Damac_ParseCSVToResetInquiry_Test{


    static testmethod void TestM1(){
        Id RecordTypeIdForRule = Schema.SObjectType.Inquiry_Assignment_Rules__c.getRecordTypeInfosByName().get('Reset Inquiry Fields').getRecordTypeId();
        
        list<Inquiry_Assignment_Rules__c> inqAssign = new list<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c inqAssign1 = new Inquiry_Assignment_Rules__c();
        inqAssign1.Active__c = True;
        inqAssign1.Write_to_CSV__c = true;
        inqAssign1.Execute_on__c = 'Reassign';
        inqAssign1.Filter_Logic__c = '1';
        inqAssign1.Inquiry_Record_Type__c = 'Inquiry';
        inqAssign1.recordtypeId = RecordTypeIdForRule ;
        inqAssign.add(inqAssign1);
        insert inqAssign;
        
        
        id recTypeid = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        list<Inquiry__c> inqs = new list<Inquiry__c> ();
        Inquiry__c inq = new Inquiry__c ();        
        inq.RecordTypeId = recTypeid;
        inqs.add(inq);        
        insert inqs;
        
        string strCsv = inqs[0].id+','+inqs[0].id;
        blob csvFileBody = Blob.valueOf(strCsv);
        
        attachment a = new attachment();
        a.parentId = inqAssign[0].id;
        a.name = 'ids.txt';
        a.body = csvFileBody;
        insert a;
        
        Damac_ParseCSVToResetInquiry.ParseCSVRecord(inqAssign[0].id);

       
    
    
    }


}