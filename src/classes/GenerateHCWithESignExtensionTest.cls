/***********************************************************************************************
 * @Name              : GenerateHCWithESignExtensionTest
 * @Description       : Class to test GenerateHCWithESignExtension Class.
 * Modification Log
 * VERSION     AUTHOR            DATE            Update Log
 * 1.0         Neha Dave         26/08/2020      Created
***********************************************************************************************/
@isTest
class GenerateHCWithESignExtensionTest {
	/************************************************************************
	* @Description : Method to test the creation of Handover Checklist document
	*                with E-sign
	* @Params      : None
	* @Return      : None
	*************************************************************************/
	@isTest
	public static void testCreateESignforEarlyHandover(){
		Id CaseRecordTypeId =
			Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();

		Account   Acc = TestDataFactory_CRM.createPersonAccount();
		Acc.Nationality__c = 'UAE';
		insert Acc;
		System.assert(Acc != null);
		
		NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
		insert Sr;
		System.assert(Sr != null);
		
		Booking__c Booking =  new Booking__c();
		Booking.AWB_Number__c = 'Test AWB';
		Booking.Account__c = Acc.Id;
		Booking.Deal_SR__c = Sr.Id;
		insert Booking;
		System.assert(Booking != null);
		
		Booking_Unit__c BU =  new Booking_Unit__c();
		BU.Unit_Name__c = 'Test Units';
		BU.Registration_Status__c  = 'Active';
		BU.Registration_ID__c   = '1234';
		BU.Unit_Selling_Price_AED__c  = 100;
		BU.Property_Name__c = 'Akoya';
		BU.Unit_Type__c = 'Villa';
		BU.Property_Country__c = 'United Arab Emirates';
		BU.Booking__c = Booking.Id;
		insert BU;
		System.assert(BU != null);
		
		Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
		pCas.Booking_Unit__c = BU.Id ;
		pCas.Type = 'NOCVisa';
		pCas.Document_Verified__c = true;
		pCas.Payment_Verified__c = true;
		insert pCas;
		
		Case Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
		Cas.Booking_Unit__c = BU.Id ;
		Cas.Type = 'NOCVisa';
		Cas.Payment_Verified__c = true;
		Cas.ParentId = pCas.Id;
		insert Cas;
		
		SR_Attachments__c objSR = new SR_Attachments__c();
		objSR.Name = '1020 Signed Letter of Discharge 1025';
		objSR.Case__c = Cas.Id;
		objSR.isValid__c = true;
		objSR.Attachment_URL__c = 'ww.google.com';
		insert objSR;
		System.assert(Cas != null);
		
		List<Riyadh_Rotana_Drawloop_Doc_Mapping__c> newCustomSettingList = new List<Riyadh_Rotana_Drawloop_Doc_Mapping__c>();
		for(Integer i=1;i<12;i++){
			Riyadh_Rotana_Drawloop_Doc_Mapping__c customSettingObj = new Riyadh_Rotana_Drawloop_Doc_Mapping__c();
			customSettingObj.Name = 'CHECKLIST_OP_'+i+'_WITH_AV_E-SIGN';
			customSettingObj.Delivery_Option_Id__c = 'a0T0E000007BesO';
			customSettingObj.Drawloop_Document_Package_Id__c = 'a0V0E000003E0vR';
			customSettingObj.Document_Name__c = 'CheckList OP'+i+'with AV E-sign';
			newCustomSettingList.add(customSettingObj);
		}
		
		for(Integer i=1;i<12;i++){
			Riyadh_Rotana_Drawloop_Doc_Mapping__c customSettingObj1 = new Riyadh_Rotana_Drawloop_Doc_Mapping__c();
			customSettingObj1.Name = 'CHECKLIST_OP_'+i+'_AV';
			customSettingObj1.Delivery_Option_Id__c = 'a0T0E000007BesO';
			customSettingObj1.Drawloop_Document_Package_Id__c = 'a0V0E000003E0vR';
			customSettingObj1.Document_Name__c = 'CheckList OP'+i+'with AV';
			newCustomSettingList.add(customSettingObj1);
		}
		
		for(Integer i=1;i<12;i++){
			Riyadh_Rotana_Drawloop_Doc_Mapping__c customSettingObj = new Riyadh_Rotana_Drawloop_Doc_Mapping__c();
			customSettingObj.Name = 'CHECKLIST_OP_'+i+'_E-SIGN';
			customSettingObj.Delivery_Option_Id__c = 'a0T0E000007BesO';
			customSettingObj.Drawloop_Document_Package_Id__c = 'a0V0E000003E0vR';
			customSettingObj.Document_Name__c = 'CheckList OP'+i+'with AV E-sign';
			newCustomSettingList.add(customSettingObj);
		}
		
		for(Integer i=1;i<12;i++){
			Riyadh_Rotana_Drawloop_Doc_Mapping__c customSettingObj1 = new Riyadh_Rotana_Drawloop_Doc_Mapping__c();
			customSettingObj1.Name = 'CHECKLIST_OP_'+i;
			customSettingObj1.Delivery_Option_Id__c = 'a0T0E000007BesO';
			customSettingObj1.Drawloop_Document_Package_Id__c = 'a0V0E000003E0vR';
			customSettingObj1.Document_Name__c = 'CheckList OP'+i+'without E-Sign';
			newCustomSettingList.add(customSettingObj1);
		}
		
		insert newCustomSettingList;

		ApexPages.currentPage().getParameters().put('id',Cas.Id);
		
		Test.startTest();
		PageReference pageRef = Page.GenerateHCWithESign;
		pageRef.getParameters().put('id', String.valueOf(Cas.Id));
		Test.setCurrentPage(pageRef);
	   
		ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
		GenerateHCWithESignExtension controller = new GenerateHCWithESignExtension(sc);
		controller.callGenerateHC();
		
		PageReference pageRef1 = Page.GenerateHC;
		pageRef1.getParameters().put('id', String.valueOf(Cas.Id));
		pageRef1.getParameters().put('isEsign', 'true');
		System.debug('pageRef'+pageRef1);
		Test.setCurrentPage(pageRef1);
	   
		ApexPages.StandardController sc1 = new ApexPages.StandardController(Cas);
		GenerateHandoverChecklist controller1 = new GenerateHandoverChecklist(sc1);
		controller1.init();
		Test.stopTest();
		
		List<Attachment> attList = [SELECT Id, ParentId FROM Attachment WHERE ParentId = :BU.Id];
		System.assertEquals(false, !attList.isEmpty(), 'Handover Checklist document should be created on Booking unit.');
	}
}