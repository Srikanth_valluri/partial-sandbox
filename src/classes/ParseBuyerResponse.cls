public class ParseBuyerResponse {
    
    public String responseId;   //77604
    public String responseTime; //Wed Feb 28 10:58:38 GST 2018
    public String requestName;  //REGISTRATION
    public String extRequestName;   //SR-216260
    public cls_actions actions;
    public cls_responseLines[] responseLines;
    public boolean complete;
    public class cls_actions {
    }
    public class cls_responseLines {
        public String id;   //77604
        public cls_lineItems[] lineItems;
    }
    public class cls_lineItems {
        public cls_lineItems[] lineItems;
        public String id;   //10416
        public String processName;  //CUSTOMER_ADDRESS
        public String processStatus;    //S
        public String startTime;    //28-Feb-2018 10:58:13
        public String endTime;  //28-Feb-2018 11:02:51
        public cls_attributes[] attributes;
    }
    public class cls_attributes {
        public String name; //REGISTRATION_ID
        public String value;    //98945
    }
    public static ParseBuyerResponse parse(String json){
        return (ParseBuyerResponse) System.JSON.deserialize(json, ParseBuyerResponse.class);
    }
}