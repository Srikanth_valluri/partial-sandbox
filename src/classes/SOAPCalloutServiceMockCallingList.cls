@isTest
public class SOAPCalloutServiceMockCallingList implements WebServiceMock {
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        if(request instanceof CollectionUpdateService.getCollectionCallingListIPMS_element)
        {
          CollectionUpdateService.getCollectionCallingListIPMSResponse_element responseNew = new CollectionUpdateService.getCollectionCallingListIPMSResponse_element();
          responseNew.return_x = '{"ATTRIBUTE2":"323925","ATTRIBUTE5":"1394","ATTRIBUTE6":"67615","ATTRIBUTE13":"Agreement executed by DAMAC","ATTRIBUTE14":"269885","ATTRIBUTE15":"1080115","ATTRIBUTE16":"00","ATTRIBUTE17":"34645","ATTRIBUTE18":"234864653","ATTRIBUTE19":"5453","ATTRIBUTE20":"67500","ATTRIBUTE21":"1509","ATTRIBUTE22":"239654","ATTRIBUTE23":"534864"}';
          response.put('response_x', responseNew); 
        }

        if(request instanceof CollectionUpdateService.getNUpdateCollectionCallingListMonthly_element)
        {
          CollectionUpdateService.getNUpdateCollectionCallingListMonthlyResponse_element responseNew = new CollectionUpdateService.getNUpdateCollectionCallingListMonthlyResponse_element();
          responseNew.return_x = 'Mock Response';
          response.put('response_x', responseNew); 
        }

        if(request instanceof CollectionUpdateService.getNUpdateCollectionCallingListHourlySF_element)
        {
          CollectionUpdateService.getNUpdateCollectionCallingListHourlySFResponse_element responseNew = new CollectionUpdateService.getNUpdateCollectionCallingListHourlySFResponse_element();
          responseNew.return_x = 'Mock Response';
          response.put('response_x', responseNew); 
        }
        
        if(request instanceof FlagUpdateService.UpdateAddressVerifiedFlag_element)
        {
          FlagUpdateService.UpdateAddressVerifiedFlagResponse_element response_x = new FlagUpdateService.UpdateAddressVerifiedFlagResponse_element();
          response_x.return_x = '{"data":[{"PROC_STATUS":"E","PROC_MESSAGE":"Either Registration ID or Address Verified Flag is Invalid (Reg ID, Address Verified Flag): 12851,NULL","PARAM_ID":"12851","Message_ID":"1124"}],"message":"[WARNING] Check the PROC_MESSAGE attribute for Actual Error Message(s), Error Message Count = 1","status":"S"}';
          response.put('response_x', response_x); 
        }
        
        if(request instanceof FlagUpdateService.UpdateRecoveryFlag_element)
        {
          FlagUpdateService.UpdateRecoveryFlagResponse_element response_x = new FlagUpdateService.UpdateRecoveryFlagResponse_element();
          response_x.return_x = '{"data":[{"PROC_STATUS":"E","PROC_MESSAGE":"Either Registration ID or Recovery Flag is Invalid (Reg ID, Recovery Flag): 12851,NULL","PARAM_ID":"12851","Message_ID":"1124"}],"message":"[WARNING] Check the PROC_MESSAGE attribute for Actual Error Message(s), Error Message Count = 1","status":"S"}';
          response.put('response_x', response_x); 
        }
   }
}