/***********************************************************************************
 * Controller Class : ProposeValueController
 * Created By : Srikanth Valluri
 -----------------------------------------------------------------------------------
 * Description : This is a controller class for AddProposalValues
 *
 * Test Data : 
 -----------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE              DETAIL FEATURES
   1.0         Srikanth Valluri     16/02/2018        Initial Development
 **********************************************************************************/
public without sharing class ProposeValueController {
    public List <InventoryWrapper> inventoryWrapperList {get; set;}
    String currentId = '';
    public ProposeValueController (ApexPages.StandardController controller) {
        inventoryWrapperList = new List <InventoryWrapper>();       
        currentId = Apexpages.currentpage().getparameters().get('id');
        for (Inventory__c eachInventory: [SELECT
                                                Unit__c, Floor__c, Name, 
                                                Floor_Package_Name__c,
                                                Comments__c,
                                                Unit_Type__c, UA_Proposal_Price__c
                                            FROM
                                                Inventory__c
                                            WHERE
                                                Unit_Assignment__c =: currentId]) {
            inventoryWrapperList.add(new InventoryWrapper(eachInventory));
        }
    }
    /*****************************************************************************************************
    * @Description : This method will update the Inventories and Unit Assignemt with new proposal values *
    * @Params      :                                                                                     *
    * @Return      : void                                                                                *
    *****************************************************************************************************/
    
    public void updateInventories () {
    try {
        Unit_Assignment__c uAsgn =[Select Id,Deal_Value__c,Proposal_Price__c, CreatedBy.ID from Unit_Assignment__c where id= :currentId];
        List<Inventory__c> inventoryList = new List<Inventory__c>();
        for (InventoryWrapper eachRecord: inventoryWrapperList) {
            if (eachRecord.isSelected) {
                inventoryList.add(eachRecord.invObj);
            }
        }
        if (inventoryList.size() > 0) {
            update inventoryList;
            
            aggregateResult[] groupedResults=[SELECT SUM(Selling_Price__c)sumPrice,
                                                     SUM(UA_Proposal_Price__c)sumProposal
                                                  FROM 
                                                   Inventory__c 
                                                  WHERE 
                                                   Unit_Assignment__c= :currentId                                                  
                                                   ];  
                
                for (AggregateResult ar : groupedResults)  {
                    uAsgn.Deal_Value__c = (Decimal)ar.get('sumPrice');
                    uAsgn.Proposal_Price__c = (Decimal)ar.get('sumProposal');
                }
                update uAsgn ;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, System.label.Add_Inventory));
                
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.label.Select_Inventory ));
            }
        } catch (Exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }
    }
    
    public Class InventoryWrapper {        
        public Inventory__c invObj {get; set;}
        public Boolean isSelected {get; set;}
        public InventoryWrapper(Inventory__c pInvObj) {            
            invObj = pInvObj;
            isSelected = false;
        }
    }

}