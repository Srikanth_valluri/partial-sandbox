@isTest
private class UnitDetailsViewDataProviderTest {
    @isTest
    static void testGetData() {
    	Account account = new Account(Name = 'Test');
    	insert account;
        NSIBPM__Service_Request__c dealSr = TestDataFactory_CRM.createServiceRequest();
        insert dealSr;
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(Account.Id, dealSr.Id, 1);
        insert lstBooking;
        List<Booking_Unit__c> lstUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        insert lstUnit;

        DataDisplayConfig config = new DataDisplayConfig();
        config.recordId = lstUnit[0].Id;
        config.fieldList = new List<Map<String, String>> {
            new Map<String, String> {
                'field'=> 'Id',
                'title'=> 'Id'
            }
        };
        config.detailFieldList = new List<Map<String, String>> {
            new Map<String, String> {
                'field'=> 'Name',
                'title'=> 'Name'
            }
        };
        config.objectName = 'Booking_Unit__c';
        new UnitDetailsViewDataProvider().getData(config);
    }
}