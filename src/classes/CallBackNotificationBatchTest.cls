/*
* Description - Test class developed for CallBackNotificationBatchForCallingList 
*
* Version            Date            Author            Description
* 1.0                8/1/2018                               Initial Draft
*/
@isTest
public class CallBackNotificationBatchTest {
    public static Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
   
     static testMethod void test_CallBackNotificationBatch () {
        CallBackNotificationBatchForCallingList batchCls = new CallBackNotificationBatchForCallingList ();
         List<TriggerOnOffCustomSetting__c>settingLst = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst.add(newSetting);
        insert settingLst;
        Id RecordTypeIdCollection = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Collections_Calling_List'
            AND IsActive = TRUE LIMIT 1
        ].Id;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        List<Calling_List__c> listCallingList = new List<Calling_List__c> ();
       Calling_List__c collectionCallInstManagement = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                                         Call_Outcome__c = 'Call Back',
                                                                         Account__c = objAcc.id,
                                                                         Customer_Flag__c = true,
                                                                         Unit_Name__c = 'demo unit name',
                                                                         Inv_Due__c = 80,
                                                                         DM_Due_Amount__c = 10,
                                                                         Amount_Pending__c =10.20,
                                                                         Call_Back_Date__c = System.Now(),
                                                                         IsHideFromUI__c = false,
                                                                         Notification_Time_for_outcome__c = system.now(),
                                                                         Registration_ID__c = '1000',
                                                                         Party_ID__c = '1177923'    );
       listCallingList.add(collectionCallInstManagement);
       Calling_List__c collectionCallInstManagement2 = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                                         Call_Outcome__c = 'Call Back',
                                                                         Account__c = objAcc.id,
                                                                         Customer_Flag__c = true,
                                                                         Unit_Name__c = 'demo unit name1',
                                                                         Inv_Due__c = 87,
                                                                         Call_Back_Date__c = System.Now(),
                                                                         DM_Due_Amount__c = 17,
                                                                         Amount_Pending__c =1720,
                                                                         IsHideFromUI__c = false,
                                                                         Registration_ID__c = '10300',
                                                                         Party_ID__c = '11723'  );
                                                                         
        listCallingList.add(collectionCallInstManagement2);
        
        Calling_List__c collectionCallInstManagement3 = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                                         Call_Outcome__c = 'Promise to Pay',
                                                                         Account__c = objAcc.id,
                                                                         PTP_Date__c = System.Today(),
                                                                         Customer_Flag__c = true,
                                                                         Unit_Name__c = 'demo unit name1',
                                                                         Inv_Due__c = 87,
                                                                         Call_Back_Date__c = System.Now(),
                                                                         DM_Due_Amount__c = 17,
                                                                         Amount_Pending__c =1720,
                                                                         IsHideFromUI__c = false,
                                                                         Registration_ID__c = '10300',
                                                                         Party_ID__c = '11723'  );
                                                                         
        listCallingList.add(collectionCallInstManagement3);
        
        
        Calling_List__c collectionCallInstManagement4 = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                                         Call_Outcome__c = 'Unreachable',
                                                                         Calling_List_Type__c = 'DP Calling',
                                                                         DP_CL_OutCome_Update_Date__c = System.Today().addDays(-2),
                                                                         Account__c = objAcc.id,
                                                                         Customer_Flag__c = true,
                                                                         Unit_Name__c = 'demo unit name1',
                                                                         Inv_Due__c = 87,
                                                                         Call_Back_Date__c = System.Today(),
                                                                         DM_Due_Amount__c = 17,
                                                                         Amount_Pending__c =1720,
                                                                         IsHideFromUI__c = false,
                                                                         Registration_ID__c = '10300',
                                                                         Party_ID__c = '11723'  );
                                                                         
        listCallingList.add(collectionCallInstManagement4);
        
        Calling_List__c collectionCallInstManagement5 = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                                         Call_Outcome__c = 'Call Back',
                                                                         Calling_List_Type__c = 'DP Calling',
                                                                         DP_CL_OutCome_Update_Date__c = System.Today().addDays(-5),
                                                                         Account__c = objAcc.id,
                                                                         Customer_Flag__c = true,
                                                                         Unit_Name__c = 'demo unit name1',
                                                                         Inv_Due__c = 87,
                                                                         Call_Back_Date__c = System.Now(),
                                                                         DM_Due_Amount__c = 17,
                                                                         Amount_Pending__c =1720,
                                                                         IsHideFromUI__c = false,
                                                                         Registration_ID__c = '10300',
                                                                         Party_ID__c = '11723'  );
                                                                         
        listCallingList.add(collectionCallInstManagement5);
        
        
        
        insert listCallingList;
        
        
        
        Test.startTest();
            Database.executeBatch(batchCls);
        Test.stopTest();
    }

}