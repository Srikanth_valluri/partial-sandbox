@isTest
public Class TravelUtilizationControllerTest {

    static testmethod void testmethod_1(){
         
         Inquiry__c inqObj= new Inquiry__c(
         //RecordTypeId=agenTeamRT,
         Inquiry_Source__c='Agent Referral',
         Mobile_Phone_Encrypt__c='456123',
         Mobile_CountryCode__c='American Samoa: 001684',
         Mobile_Phone__c='1234',
         Email__c='mk@gmail.com',
         First_Name__c='Test',
         Last_Name__c='Last',
         Meeting_Type__c = 'Scheduled Tour'
         );
            
         insert inqObj;
            
         Travel_Details__c td = new Travel_Details__c(Inquiry__c=inqObj.id);
         td.From_Date__c = Date.newInstance(2018,07,06);
         td.To_Date__c = Date.newInstance(2018,07,08);
         td.Status__c = 'Draft';
         td.Utilization_Status__c = 'Flyin Completed';
         td.Email_Sent_Time__c = datetime.newInstance(2018, 7,16, 10, 30, 0);
         insert td;
            
         ApexPages.StandardController sc = new ApexPages.StandardController(td);
         Test.startTest();
             TravelUtilizationController obj = new TravelUtilizationController(sc);
             obj.updateUtilization ();
         Test.stopTest();
     }
 }