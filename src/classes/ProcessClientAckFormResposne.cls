/*
 * Description: Class used to create SR Attachment and send email on Client Acknowledgment Form generation through Drawloop
 */

public class ProcessClientAckFormResposne{
    
    //@InvocableMethod 
    public static void createSrAttach(  List<Attachment> lstNewAttachment ){
        set<Id> setBuId = new set<Id>();
        set<Id> setAttachId = new set<Id>();
        
        if( lstNewAttachment != null && lstNewAttachment.size() > 0 ) {
            for(Attachment objAttachment : lstNewAttachment){
                system.debug('objAttachment.Name*********'+objAttachment.Name);
                system.debug('objAttachment.ParentId*********'+objAttachment.ParentId);
                if((String.valueof(objAttachment.Name).startsWith('Client Acknowledgment Form') 
                        || String.valueof(objAttachment.Name).startsWith('Notice of Completion')
                        || String.valueof(objAttachment.Name).startsWith('CheckList')
                        )
                   && String.ValueOf(objAttachment.ParentId).startsWith(keyPrefix('Booking_Unit__c'))){
                    setBuId.add(objAttachment.ParentId);
                    setAttachId.add(objAttachment.Id);
                }
            }   
        }
        
        if( setBuId != null && setBuId.size() > 0 ) {
           attachEmailToSRAndSendEmail( setBuId , setAttachId );
        }
    }
    public Static String keyPrefix( String strObjName ) {
        if( String.isBlank(strObjName) ) {
            return null;
        }
        Map<String, Schema.SObjectType> m  = Schema.getGlobalDescribe() ;
        system.debug('==>m is==>'+m);
        Schema.SObjectType s = m.get(strObjName) ;
        system.debug('==>Sobject Type is ==>'+s);
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        String keyPrefix = r.getKeyPrefix();
        return keyPrefix;
    }

    @future(Callout=true)
    public static void attachEmailToSRAndSendEmail( set<Id> setBuId , set<Id> attachId ) {
        
        if( setBuId  != null && setBuId.size() > 0 ) {
            List<Booking_Unit__c> lstBu = new List<Booking_Unit__c>();
            String buildingName;
            String propName = '';

            String checkListOrigin ='';String strCaseId;

            List<Booking_Unit__c> lstBookingUnitIds = [SELECT Id ,Property_Name_Inventory__c,Unit_Name__c,Value__c,Registration_Id__c,
                                                              Customer_Name__c,Key_Release_Date__c,FM_Email__c,Loams_Email__c,Handover_Notice_URLs__c
                                                              ,HO_EHO_PHO_CaseId__c
                                                     FROM Booking_Unit__c 
                                                    WHERE Id IN: setBuId   limit 1];    

            if (lstBookingUnitIds != NULL && lstBookingUnitIds.size()>0 && String.isNotBlank(lstBookingUnitIds[0].Unit_Name__c)) {
                    buildingName = lstBookingUnitIds[0].Unit_Name__c.substringBefore('/');
            }
            
            if (lstBookingUnitIds != NULL && lstBookingUnitIds.size()>0 && String.isNotBlank(lstBookingUnitIds[0].HO_EHO_PHO_CaseId__c)){
                checkListOrigin = 'Case'   ;
                strCaseId = lstBookingUnitIds[0].HO_EHO_PHO_CaseId__c ;
            }

            List<Location__c> lstLocation = [Select Id
                                                , Project_Name_for_HO_notice__c
                                                , Name
                                        From Location__c 
                                        Where Name =: buildingName
                                         LIMIT 1];                     
            
            if( lstLocation != NULL && lstLocation.size() > 0 && String.isBlank(lstLocation[0].Project_Name_for_HO_notice__c)   )
            propName = lstLocation[0].Project_Name_for_HO_notice__c;
                
             
            
            List<Attachment> lstAttachments = [SELECT Id
                                                    ,Body
                                                    ,Name
                                                    ,ParentId
                                                FROM Attachment 
                                                WHERE ParentId IN: setBuId AND Id IN: attachId 
                                                AND ( Name = 'Client Acknowledgment Form.pdf' OR Name LIKE 'Notice of Completion%'
                                                OR Name LIKE 'CheckList%' ) 
                                                LIMIT 1];
                                        
            if( lstAttachments != null && lstAttachments.size() > 0 && checkListOrigin != 'Case' ) {
                
                List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest = new  List<UploadMultipleDocController.MultipleDocRequest>();
                List<SR_Attachments__c> lstSRAttachment = new List<SR_Attachments__c>();
                integer intIncrementor = 0;
                List<SR_Attachments__c> lstSRAttachmentToDelete = new List<SR_Attachments__c>();                

                
                
                if( lstBookingUnitIds != null && lstBookingUnitIds.size() > 0 ) {
                        if( lstBookingUnitIds[0].Unit_Name__c != '' ) {
                            if( lstAttachments[0].Body != null 
                              && String.isNotBlank( lstAttachments[0].Name )) {
                                
                                UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
                                
                                if(lstAttachments[0].Name == 'Client Acknowledgment Form.pdf'){
                                    objDocRequest.category =  'Document';
                                    objDocRequest.entityName = 'Damac Service Requests'; 
                                    objDocRequest.fileDescription  =  'Client Acknowledgment Form';        
                                }else if(lstAttachments[0].Name.startsWith('Notice of Completion')){
                                    objDocRequest.category =  'Document';
                                    objDocRequest.entityName = 'Damac Service Requests'; 
                                    objDocRequest.fileDescription  =  'Notice of Completion'; 

                                }else if(lstAttachments[0].Name.startsWith('CheckList')){
                                    objDocRequest.category =  'Document';
                                    objDocRequest.entityName = 'Damac Service Requests'; 
                                    objDocRequest.fileDescription  =  'CheckList'; 

                                }
                                
                                
                                
                                intIncrementor++;
                                
                                String stType = extractType(lstAttachments[0].Name);
                                String crfAttachmentName = extractName(lstAttachments[0].Name);
                                crfAttachmentName = crfAttachmentName.replaceAll(' ','_');
                                crfAttachmentName = crfAttachmentName.replaceAll('/','_');
                                system.debug('crfAttachmentName************'+crfAttachmentName);                                
                                
                                objDocRequest.fileId = lstBookingUnitIds[0].Id+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+stType;
                                objDocRequest.fileName = lstBookingUnitIds[0].Id+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+stType;
                                
                                System.debug('---System.currentTimeMillis()--'+System.currentTimeMillis());
                                System.debug('---intIncrementor--'+intIncrementor);
                                System.debug('---objDocRequest.fileName--'+objDocRequest.fileName);
                                
                                //if(objCase != null){
                                objDocRequest.registrationId =  lstBookingUnitIds[0].Registration_Id__c;
                                //}
                                objDocRequest.sourceFileName  = 'IPMS-'+lstBookingUnitIds[0].Registration_Id__c+'-'+crfAttachmentName+'.'+stType;
                                objDocRequest.sourceId  =  'IPMS-'+lstBookingUnitIds[0].Registration_Id__c+'-'+crfAttachmentName;                        
                                if( lstAttachments[0].Body != null ){
                                    blob objBlob = lstAttachments[0].Body;
                                    if(objBlob != null){
                                        objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
                                    }
                                }
                                //objDocRequest.base64Binary  =  crfAttachmentBody;
                                lstMultipleDocRequest.add(objDocRequest);
                            }
                                                        
                            //lstCaseToUpdate.add(objCase);
                            if(lstAttachments[0].Name == 'Client Acknowledgment Form.pdf' 
                                         ){
                                SR_Attachments__c objAttach = new SR_Attachments__c();
                                //objAttach.Case__c  = objCase.Id;
                                objAttach.Booking_Unit__c  = lstBookingUnitIds[0].Id;
                                objAttach.isValid__c = true;
                                objAttach.IsRequired__c = true;
                                objAttach.Name = (lstAttachments[0].Name == 'Client Acknowledgment Form.pdf') ?'Client Acknowledgment Form' :
                                lstAttachments[0].Name.startsWith('RTN Slip')?'RTN Slip':
                                lstAttachments[0].Name.startsWith('Termination Letter')?'Termination Letter':
                                lstAttachments[0].Name.startsWith('Courier Slip')?'Courier Slip':'Error doc';           
                                //objAttach.Attachment_URL__c = 'www.salesforce.com';
                                objAttach.Need_Correction__c =  false;
                                lstSRAttachment.add(objAttach); 
                            } 
                            
                            
                        }
                    //}
                    system.debug('lstMultipleDocRequest : ' +lstMultipleDocRequest );
                    if( lstMultipleDocRequest.Size() > 0 ) {
                        UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();
                        if( !Test.isRunningTest() ) { 
                            MultipleDocData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocRequest);
                        }else {
                            
                            List<UploadMultipleDocController.MultipleDocResponse> data = new List<UploadMultipleDocController.MultipleDocResponse>();
                            UploadMultipleDocController.MultipleDocResponse objMultipleDocResponse = new UploadMultipleDocController.MultipleDocResponse();
                            objMultipleDocResponse.PROC_STATUS = 'S';
                            objMultipleDocResponse.url = 'www.google.com';
                            data.add(objMultipleDocResponse);
                            
                            MultipleDocData.Data = data;
                        }
                        System.debug('--MultipleDocData--'+MultipleDocData);
                        if( MultipleDocData != null && MultipleDocData.status == 'Exception' ) {
                            //for( Case objCase : lstCase) {
                                errorLogger(MultipleDocData.message,'',lstBookingUnitIds[0].Id);
                            //}
                        }                                   
                        if(MultipleDocData != null && (MultipleDocData.Data == null || MultipleDocData.Data.Size() == 0) ) {
                            //for( Case objCase : lstCase) {
                                errorLogger(MultipleDocData.message,'',lstBookingUnitIds[0].Id);
                            //}                           
                        }

                        if( MultipleDocData != null ) {
                            system.debug('MultipleDocData '+ MultipleDocData );
                            for( Integer objInt = 0; objInt < lstSRAttachment.Size(); objInt++ ) {
                                UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[objInt];
                                system.debug('objData '+ objData );
                                if( objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url) ) {
                                    
                                    system.debug('lstAttachments[0].Name'+ lstAttachments[0].Name);
                                    
                                    if(lstAttachments[0].Name == 'Client Acknowledgment Form.pdf'){
                                        lstSRAttachment[objInt].Attachment_URL__c = objData.url;
                                        lstBookingUnitIds[0].Value__c = objData.url;
                                        lstBu.add(lstBookingUnitIds[0]);
                                    }
                                    
                                }
                            }
                            if(lstAttachments[0].Name.startsWith('Notice of Completion' )){
                            UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[0];
                                        system.debug('in else if' );
                                        string docURL;
                                        if( String.isNotBlank( buildingName) && buildingName.equalsIgnoreCase('DTPC') ) {
                                            docURL = '<li><a href="'+ objData.url + '"target="_blank">'+ 'Notice of Completion' + '</a></li>';
                                        }else {
                                            docURL = '<li><a href="'+ objData.url + '"target="_blank">'+ 'Notice of Completion and Possession for your property in '+ propName + '</a></li>';
                                        }
                                        system.debug('docURL ='+ docURL );
                                        lstBookingUnitIds[0].Notice_Url__c = docURL;
                                        //lstBookingUnitIds[0].Handover_Notice_URLs__c = String.isNotBlank( lstBookingUnitIds[0].Handover_Notice_URLs__c )?
                                        //                                              lstBookingUnitIds[0].Handover_Notice_URLs__c+docURL :
                                        //                                              lstBookingUnitIds[0].Handover_Notice_URLs__c;
                                        lstBu.add(lstBookingUnitIds[0]);
                                    }
                             else if( lstAttachments[0].Name.startsWith('CheckList' )){
                             UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[0];
                                        system.debug('in else if' );
                                        string docURL;
                                        
                                        docURL = '<li><a href="'+ objData.url + '"target="_blank">'+ 'CheckList' + '</a></li>';
                                        
                                        system.debug('docURL ='+ docURL );
                                        lstBookingUnitIds[0].CheckListUrl__c = docURL;
                                        lstBu.add(lstBookingUnitIds[0]);
                                    }
                            system.debug('lstSRAttachment : ' +lstSRAttachment );

                            if( lstSRAttachment.Size() > 0 ){
                                insert lstSRAttachment;
                                //sendEmailOnDocumentGeneration(lstBookingUnitIds );
                            }  
                        }
                    }
                    

                    if( lstBu != null && lstBu.size() > 0 ) {
                        update lstBu;
                    }
                }
            }
            //TODO added to kandle case button for checklist
            else if( lstAttachments != null && lstAttachments.size() > 0 && checkListOrigin == 'Case' ) {

                List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest = new  List<UploadMultipleDocController.MultipleDocRequest>();
                list<SR_Attachments__c> lstCaseAttachment = new list<SR_Attachments__c>();
                integer intIncrementor = 0;
                                

                
                
                if( lstBookingUnitIds != null && lstBookingUnitIds.size() > 0 ) {
                        if( lstBookingUnitIds[0].Unit_Name__c != '' ) {
                            if( lstAttachments[0].Body != null 
                              && String.isNotBlank( lstAttachments[0].Name )) {
                                
                                UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
                                
                                if(lstAttachments[0].Name.startsWith('CheckList')){
                                    objDocRequest.category =  'Document';
                                    objDocRequest.entityName = 'Damac Service Requests'; 
                                    objDocRequest.fileDescription  =  'CheckList'; 

                                }
                                
                                
                                intIncrementor++;
                                
                                String stType = extractType(lstAttachments[0].Name);
                                String crfAttachmentName = extractName(lstAttachments[0].Name);
                                crfAttachmentName = crfAttachmentName.replaceAll(' ','_');
                                crfAttachmentName = crfAttachmentName.replaceAll('/','_');
                                system.debug('crfAttachmentName************'+crfAttachmentName);                                
                                
                                objDocRequest.fileId = lstBookingUnitIds[0].Id+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+stType;
                                objDocRequest.fileName = lstBookingUnitIds[0].Id+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+stType;
                                
                                System.debug('---System.currentTimeMillis()--'+System.currentTimeMillis());
                                System.debug('---intIncrementor--'+intIncrementor);
                                System.debug('---objDocRequest.fileName--'+objDocRequest.fileName);
                                
                                //if(objCase != null){
                                objDocRequest.registrationId =  lstBookingUnitIds[0].Registration_Id__c;
                                //}
                                objDocRequest.sourceFileName  = 'IPMS-'+lstBookingUnitIds[0].Registration_Id__c+'-'+crfAttachmentName+'.'+stType;
                                objDocRequest.sourceId  =  'IPMS-'+lstBookingUnitIds[0].Registration_Id__c+'-'+crfAttachmentName;                        
                                if( lstAttachments[0].Body != null ){
                                    blob objBlob = lstAttachments[0].Body;
                                    if(objBlob != null){
                                        objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
                                    }
                                }
                                //objDocRequest.base64Binary  =  crfAttachmentBody;
                                lstMultipleDocRequest.add(objDocRequest);
                            }

                    if( lstMultipleDocRequest.Size() > 0 ) {
                        UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();
                        if( !Test.isRunningTest() ) { 
                            MultipleDocData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocRequest);
                        }else {
                            
                            List<UploadMultipleDocController.MultipleDocResponse> data = new List<UploadMultipleDocController.MultipleDocResponse>();
                            UploadMultipleDocController.MultipleDocResponse objMultipleDocResponse = new UploadMultipleDocController.MultipleDocResponse();
                            objMultipleDocResponse.PROC_STATUS = 'S';
                            objMultipleDocResponse.url = 'www.google.com';
                            data.add(objMultipleDocResponse);
                            
                            MultipleDocData.Data = data;
                        }
                        System.debug('--MultipleDocData--'+MultipleDocData);
                        if( MultipleDocData != null && MultipleDocData.status == 'Exception' ) {
                            //for( Case objCase : lstCase) {
                                errorLogger(MultipleDocData.message,'',lstBookingUnitIds[0].Id);
                            //}
                        }                                   
                        if(MultipleDocData != null && (MultipleDocData.Data == null || MultipleDocData.Data.Size() == 0) ) {
                            //for( Case objCase : lstCase) {
                                errorLogger(MultipleDocData.message,'',lstBookingUnitIds[0].Id);
                            //}                           
                        }

                        if( MultipleDocData != null ) {
                            system.debug('MultipleDocData '+ MultipleDocData );
                            
                                UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[0];
                                system.debug('objData '+ objData );
                                if( objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url) ) {
                                    
                                    Id checklistId = fetchDocument('Handover Checklist',strCaseId);
                                    Id signedChecklistId = fetchDocument('Signed Handover Checklist',strCaseId);
                                    SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                                    objCaseAttachment.Case__c = strCaseId;
                                    objCaseAttachment.Name = ' Handover Checklist ' +system.now();
                                    objCaseAttachment.Attachment_URL__c = objData.url;
                                    objCaseAttachment.Booking_Unit__c = lstBookingUnitIds[0].Id;
                                    objCaseAttachment.IsValid__c = true;
                                    objCaseAttachment.Id = checklistId;
                                    lstCaseAttachment.add(objCaseAttachment);
                                    
                                    SR_Attachments__c objCaseAttachment1 = new SR_Attachments__c();
                                    objCaseAttachment1.Case__c = strCaseId;
                                    objCaseAttachment1.Name = ' Signed Handover Checklist ' +system.now();
                                    objCaseAttachment1.Booking_Unit__c = lstBookingUnitIds[0].Id;
                                    objCaseAttachment1.Id = signedChecklistId;
                                    lstCaseAttachment.add(objCaseAttachment1);
                                        
                                }
                                    
                            }

                            system.debug('lstCaseAttachment '+lstCaseAttachment );
                            if (lstCaseAttachment != null && lstCaseAttachment.size()>0){
                                upsert lstCaseAttachment;
                            }
                        
                        }
                    }
                }
            }


        }
    }

    public Static Id fetchDocument(String docName,String caseId){
        system.debug('docName*******'+docName);
        
        docName= '%'+docName+'%';
        list<SR_Attachments__c> lstSR = [Select Id, Name, Case__c 
                                        from SR_Attachments__c 
                                        where Case__c =:caseId
                                        and Name Like :docName limit 1];
        system.debug('lstSR*****'+lstSR);
        if(lstSR != null && !lstSR.isEmpty()){
            return lstSR[0].Id;
        }else{
            return null;
        }
    }
    
    private static void errorLogger(string strErrorMessage, string strCaseID,string strBookingUnitID){
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        if(String.isNotBlank(strCaseID) && strCaseID.startsWith('500')){
            objError.Case__c = strCaseID;
        }
        if(String.isNotBlank(strBookingUnitID)){
            objError.Booking_unit__c = strBookingUnitID;
        }
        insert objError;
    }

    @TestVisible
    private static String extractName(String strName) {
        return strName.substring(strName.lastIndexOf('\\') + 1);
    }
    @TestVisible
    private static String extractType( String strName ) {
        strName = strName.substring( strName.lastIndexOf('\\')+1 );
        return strName.substring( strName.lastIndexOf('.')+1 ) ;
    }   
    
    
    
}