public without sharing class AP_CILDetailPageController {
    public list<Buyer__c> lstBuyers {get;set;}
    public list<Task> lstTask {get;set;}
    public Inquiry__c objInquiry {get;set;}
    public string currentInquiryId {get;set;}
    public static string tempId {get;set;}
    public static List<Unit_Documents__c> lstUnitDocsTemp{get;set;}
    public static List<Proof_of_Payment__c> lstPOPTemp{get;set;}
    public static List<Receipt__c> lstReceiptTemp{get;set;}
    public List<Unit_Documents__c> lstUnitDocsNew{get;set;} 
    public String customStatus {get;set;}
    public NSIBPM__Service_Request__c objSR{get;set;}
    public boolean hasInquiry{get;set;}
    public string SRID;   
    public list<myObject> lstMyObject {get;set;}
    public String strcode1 {get; set;}
    public String strcode2 {get; set;}
    public String strcode3 {get; set;}
    public String strcode4 {get; set;}
    public String strcode5 {get; set;}
    public String strSelectedLanguage {get; set;}
    public String usrEmail;
    
    public AP_CILDetailPageController() {
        objSR = new NSIBPM__Service_Request__c();
        usrEmail = UserInfo.getUserEmail();
        System.debug('>>>>usrEmail'+usrEmail);
        currentInquiryId = apexpages.currentPage().getParameters().get('inquiryId');
        strSelectedLanguage = apexpages.currentPage().getParameters().get('langCode');
        SRID = apexpages.currentPage().getParameters().get('Id');
        if(SRID!= null){
            objSR = [SELECT id, (SELECT id 
                                        FROM Bookings__r)
                            FROM NSIBPM__Service_Request__c
                            WHERE id =: SRID];
            system.debug('objCurrentSR--'+objSR);
        }
        objInquiry = new Inquiry__c();
        if(String.isNotBlank(currentInquiryId))
        {
            objInquiry = [Select id,Name,Last_Name__c,First_Name__c,Mobile_Phone_Encrypt__c,Mobile_Phone__c
                                ,Mobile_Phone_2__c,Mobile_Phone_3__c,Mobile_Phone_4__c,Mobile_Phone_5__c
                                ,Passport_Expiry_Date__c,Passport_Number__c,Passport_Place_of_Issue__c
                                ,Preferred_Language__c,Budget__c,Financing__c,Inquiry_Source__c,CreatedBy.Name
                                ,CreatedDate,LastModifiedBy.Name,Email__c,Country__c,Phone_CountryCode__c
                                ,Mobile_CountryCode__c,Mobile_Country_Code_2__c,Mobile_Country_Code_3__c
                                ,Mobile_Country_Code_4__c,Mobile_Country_Code_5__c,Campaign_Name_Text__c
                                ,Type_of_Property_Interested_in__c,Time_Frame__c,
                                (SELECT id, Buyer_Type__c,Name,First_Name__c,Last_Name__c 
                                FROM Buyers__r)
                          FROM Inquiry__c
                          WHERE Id =: currentInquiryId];
            system.debug('objInquiry----'+objInquiry);
            
            /* 20.12.2018 */
            /* ---------------------------------------------------------------------- */
            if(String.isNotBlank(objInquiry.Mobile_CountryCode__c)) {
                strcode1 = objInquiry.Mobile_CountryCode__c.split(':').get(1);
            }
            if(String.isNotBlank(objInquiry.Mobile_Country_Code_2__c)) {
                strcode2 = objInquiry.Mobile_Country_Code_2__c.split(':').get(1);
            }
            if(String.isNotBlank(objInquiry.Mobile_Country_Code_3__c)) {
                strcode3 = objInquiry.Mobile_Country_Code_3__c.split(':').get(1);
            }
            if(String.isNotBlank(objInquiry.Mobile_Country_Code_4__c)) {
                strcode4 = objInquiry.Mobile_Country_Code_4__c.split(':').get(1);
            }
            if(String.isNotBlank(objInquiry.Mobile_Country_Code_5__c)) {
                strcode5 = objInquiry.Mobile_Country_Code_5__c.split(':').get(1);
            }
            /* ---------------------------------------------------------------------- */
            
            lstTask = new list<Task>();
            lstTask = [Select id,subject,OwnerId,owner.Name,Owner.FirstName,Owner.LastName,Whatid,ActivityDate,Status
                        ,CreatedDate,Activity_Outcome__c,Type 
                        ,Activity_Type_3__c
                        //,Meeting_Outcome__c
                        ,Task_Due_Date__c,CreatedBy.Name
                        from Task where whatId = :currentInquiryId]; 
            system.debug('lstTaskSize----'+lstTask.size());
            system.debug('lstTask----'+lstTask);

            set<id> taskOwnerSet = new set<id>();
            for(task objTask: lstTask) {
                taskOwnerSet.add(objTask.ownerId);
            }       
            system.debug('taskOwnerSet----'+taskOwnerSet);

            list<user> lstTaskOwner = new list<user>();
            lstTaskOwner = [select id, fullphotourl from user where id in: taskOwnerSet];
            system.debug('lstTaskOwner----'+lstTaskOwner);
            map<id,string> mapTaskOwnerAndPhotoURL = new map<id,string>();
            for(user objUser: lstTaskOwner){
                if(!mapTaskOwnerAndPhotoURL.containsKey(objUser.id)){
                    //mapTaskOwnerAndPhotoURL.get(objUser.id).add(new string(objUser.fullphotourl));
                    mapTaskOwnerAndPhotoURL.put(objUser.id,objUser.fullphotourl);
                } 
                /*else {
                    mapTaskOwnerAndPhotoURL.put(objUser.id,objUser.fullphotourl);
                }*/
            }
            system.debug('mapTaskOwnerAndPhotoURL'+mapTaskOwnerAndPhotoURL);
            lstMyObject = new list<myObject>();

            for(task objTask: lstTask){
                lstMyObject.add(new myObject(objTask,mapTaskOwnerAndPhotoURL.get(objTask.ownerId)));
            }
            system.debug('lstMyObject'+lstMyObject);
        }
    }

    public void sendContactDetails(){
        Messaging.reserveSingleEmailCapacity(1);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        list<String> toAddresses = new list<String>() ;
        toAddresses.add(usrEmail );
        OrgWideEmailAddress[] owea;
        owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
        mail.setToAddresses(toAddresses);
        mail.setReplyTo('agentsfsupport@damacgroup.com');
        //mail.setSenderDisplayName('DAMAC');
        mail.setSubject('Contact Details');
        mail.setBccSender(false);
        mail.setUseSignature(false);
        if ( owea.size() > 0 ) {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }

        mail.setHtmlBody('Dear '+UserInfo.getFirstName()+' '+UserInfo.getLastName()+',<br/><br/>'+
                          'Please find below the details of the CIL '+objInquiry.Name+' as requested<br/><br/>'+
                          'Name: '+objInquiry.First_Name__c + ' ' + objInquiry.Last_Name__c + '<br/>'+
                          'Contact Number : '+ objInquiry.Mobile_Phone__c+'<br/>'+
                          'Email Address : ' + objInquiry.Email__c+'<br/><br/>'+
                          'If you have not requested for this information,'+
                          ' please let us know immediately on agentsfsupport@damacgroup.com.<br/>'+
                          'It is also recommended that you reset the password of the portal'+
                          ' immediately and that you do not share these credentials.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

    public class myObject {
        
        public task objTask { get; set; }
        public string photoURL {get;set;}
        public myObject(task objTask,String photoURL) {
            this.objTask = objTask;
            this.photoURL = photoURL;
        }
    }
}