public class SRStepDealVFEmailCtrl {

    public string stepId{get; set;}
    
    public list<Booking__c> bookings{
        get{
            System.debug(stepId+'>>>>>>>>>>>>>');
            if(stepId != null){
                New_step__c step = [Select id, Service_request__c from New_step__c where id=: stepId];
                list<Booking__c> lstBookings = [Select id from Booking__c where Deal_SR__c =: step.Service_request__c];
                return lstBookings;
            }
            else
                return new list<Booking__c>();
        }
        set;
        
    }
}