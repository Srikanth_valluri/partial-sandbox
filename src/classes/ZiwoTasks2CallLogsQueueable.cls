global class ZiwoTasks2CallLogsQueueable implements Database.Batchable<sobject> {
    
    global database.querylocator start(Database.BatchableContext bc){
        Map<string, Scheduler_Settings__c> settings = Scheduler_settings__c.getAll();
        Scheduler_Settings__c ziwoSettings = settings.containsKey('Tasks2CallLogs') ? settings.get('Tasks2CallLogs'): new Scheduler_Settings__c(name = 'Tasks2CallLogs');
        DateTime LastRunTime = (ziwoSettings.Last_Run_Time__c == null ? System.now().addMinutes(-15) : ziwoSettings.Last_Run_Time__c);
        
        string likeStr = 'Ziwo%';
        string query = 'Select id, CrtObjectId__c, Subject, WhatId, Recorded_Call__c, Call_End_Time__c, Call_Start_Time__c, Activity_type_3__c, OwnerId'; 
        query += ' from Task where Event_Type__c like: likeStr AND Ameyo_Dialer_Disposition__c = \'CALLING LIST\' AND LastModifiedDate >=: LastRunTime';

        ziwoSettings.Last_Run_Time__c = system.now();
        upsert ziwosettings;

        return database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, list<Task> scope){    
        set<string> ziwoCallIds = new set<string>();
        for(Task tObj : scope){
            ziwoCallIds.add(tObj.CrtObjectId__c);
        }
        
        map<string, string> mpCallNos = new map<string, string>();
        for(Waybeo_logs__c log : [Select id, Ziwo_CallID__c, Ziwo_callerIDNumber__c, Ziwo_Activity_Subject_New__c, Ziwo_Actual_Talktime_New__c,
                        Ziwo_Task_Call_End_Time__c, Ziwo_Task_Call_Start_Time__c, Ziwo_Activity_Type__c, Ziwo_recordingFile__c 
                        from Waybeo_logs__c where Ziwo_callID__c in: ziwoCallIds])
            mpCallNos.put(log.Ziwo_CallID__c, log.Ziwo_callerIDNumber__c);
        
        list<Call_Log__c> lstLogs2Upsert = new list<Call_log__c>();
        for(Task tObj : scope){    
            Call_Log__c log = new Call_Log__c();
            log.OwnerId = tObj.OwnerId;
            log.Call_Recording_URL__c = tObj.Recorded_Call__c;
            log.Call_Type__c = tObj.Activity_type_3__c;
            log.Calling_Number__c = mpCallNos.get(tObj.CrtObjectId__c);
            log.End_time__c = tObj.Call_End_Time__c;
            log.Start_Time__c = tObj.Call_Start_Time__c;
            log.Calling_List__c = tObj.WhatId;
            log.Call_ID__c = tObj.CrtObjectId__c;
            log.System_Disposition__c = tObj.Subject;
            lstLogs2Upsert.add(log);
            
        }
        
        if(!lstLogs2Upsert.isEmpty())
            upsert lstLogs2Upsert Call_ID__c;
    }    
        
    global void finish(Database.BatchableContext bc){    
        if(!test.isRunningTest() && System.label.Ziwo_Call_History_Switcher == 'true')
            ZIWOCallHistoryScheduler.executeFollowing();
    }
}