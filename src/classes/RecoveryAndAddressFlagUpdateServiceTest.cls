/***********************************************************************
* Description - Test class developed for RecoveryAndAddressFlagUpdateService
*
* Version            Date            Author        Description
* 1.0                30/11/17        Arjun         Initial Draft
**********************************************************************/
@isTest
private class RecoveryAndAddressFlagUpdateServiceTest
{
  @isTest static void testupdateFlagInIPMS() {
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accountobj[0].Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;

        //String recordTypeDevName = ;
        List<Calling_List__c> listCallingList = TestDataFactory_CRM.createCallingList( 'Welcome_Calling_List', 1 , bookingUnitsObj[0] );
        listCallingList[0].Address_Verified__c = true;
        insert listCallingList;
        system.debug(listCallingList);
        //RecoveryAndAddressFlagUpdateService obj = new RecoveryAndAddressFlagUpdateService();
        
        Test.startTest();
    
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockCallingList());
    
        RecoveryAndAddressFlagUpdateService.updateFlagInIPMS(listCallingList);
        RecoveryAndAddressFlagUpdateService.Data objR = new RecoveryAndAddressFlagUpdateService.Data();
        objR.message = 'test';
        objR.status  = 'Success';     
       Test.stopTest();
            
  }
  @isTest static void testgetRecoveryFlagUpdate() {
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accountobj[0].Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;

        //String recordTypeDevName = ;
        List<Calling_List__c> listCallingList = TestDataFactory_CRM.createCallingList( 'Recovery_Calling_List', 1 , bookingUnitsObj[0] );
        listCallingList[0].Recovery__c  = true;
        insert listCallingList;
        system.debug(listCallingList);
        //RecoveryAndAddressFlagUpdateService obj = new RecoveryAndAddressFlagUpdateService();
        
        Test.startTest();
    
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockCallingList());
    
        RecoveryAndAddressFlagUpdateService.updateFlagInIPMS(listCallingList);
        RecoveryAndAddressFlagUpdateService.Data objR = new RecoveryAndAddressFlagUpdateService.Data();
        objR.message = 'test';
        objR.status  = 'Success';     
       Test.stopTest();
            
  }
  @isTest static void recoveryFlagUpdatewithException() {
       list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accountobj[0].Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;

        //String recordTypeDevName = ;
        List<Calling_List__c> listCallingList = TestDataFactory_CRM.createCallingList( 'Recovery_Calling_List', 1 , bookingUnitsObj[0] );
        listCallingList[0].Recovery__c  = true;
        insert listCallingList;
        RecoveryAndAddressFlagUpdateService.updateFlagInIPMS(listCallingList);
  } 
  @isTest static void addressFlagUpdatewithException() {
  list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accountobj[0].Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;

        //String recordTypeDevName = ;
        List<Calling_List__c> listCallingList = TestDataFactory_CRM.createCallingList( 'Welcome_Calling_List', 1 , bookingUnitsObj[0] );
        listCallingList[0].Address_Verified__c = true;
        insert listCallingList;
        system.debug(listCallingList);
        
        RecoveryAndAddressFlagUpdateService.updateFlagInIPMS(listCallingList);
  }  
}