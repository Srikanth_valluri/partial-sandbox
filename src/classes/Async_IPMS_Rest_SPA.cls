/**************************************************************************************************
* Name               : Async_IPMS_Rest_SPA
* Description        : Queueble Class to get the SPA URLs from the Process URL stored on Booking
*                        See Also:IPMS_REST_SPA_getProcessID Class prior to this
* Test Class         : IPMS_SPA_Tests
* Created By         : DAMAC IT
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR               DATE             Comments
* 1.0                                               Created
* 1.1         QBurst             02/04/2020         SOAP to REST change in Webservice call
**************************************************************************************************/
global class Async_IPMS_Rest_SPA implements Queueable, Database.AllowsCallouts {
    public string bookingid;
    public string processURL;
    @testvisible
    private Id srstatus;

    public Async_IPMS_Rest_SPA(string IPMSprocessURL, string id){
            bookingid = id;
            processURL = IPMSprocessURL;
    }

    public void execute(QueueableContext context) {
        POLLForSPAAsync(processURL, bookingid); // Do Callout
    }

    public void POLLForSPAAsync(string processURL, string bookingId){
        try{
            boolean requestSuccess;
            processURL = processURL.remove('"');
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(processURL);
            req.setMethod('GET');
            req.setheader('Content-Type', 'application/json');
            system.debug(req.getbody().length());
            // Send the request, and return a response
            HttpResponse res = h.send(req);
            system.debug(res.getBody());
            if(res.getStatusCode() != 200){
                requestSuccess = false;
                createLog(bookingId, res.getBody());
            } else{
                IPMS_JSON_SPA_Response respCls = new IPMS_JSON_SPA_Response();
                String json = res.getBody();
                respCls  = IPMS_JSON_SPA_Response.parse(json);
                system.debug('RESPONSE*********' + respCls.Results.size());
                if(respCls != null){
                    Update_SF(processURL, respCls);
                }
            }
        } catch(exception  e){
            createLog(bookingId, e.getmessage());
        }
    }

    public void Update_SF(string processURL, IPMS_JSON_SPA_Response resp){
        boolean pollAgain = false;
        boolean hasError = false;
        string bookingId;  
        bookingId = resp.extRequestKey;
        string processId = bookingId + '-' + resp.requestStart;
        if(resp.requestStatus == 'C'){ // Completed
            pollAgain = false;
        } else if(resp.requestStatus == 'P'){ // In Progress
            pollAgain = true;   
        } else if(resp.requestStatus == 'E'){ // Error
            hasError = true;
            createLog(bookingId ,string.valueof(resp));
        }
        if(!hasError){   // No Error continue parsing
            map<string,string> spaMap = new map<string,string>();
            map<string,string> dpMap = new map<string,string>();
            map<string,string> promotionMap = new map<string,string>();
            map<string,string> optionMap = new map<string,string>();
            map<string,string> schemeMap = new map<string,string>();
            //New Report Names added on Oct 21
            map<string,string> CAMPAIGNSPAmap = new map<string,string>();//CAMPAIGN-SPA
            map<string,string> DPINVOICESPAmap = new map<string,string>();//DP INVOICE-SPA
            map<string,string> OPTIONSSPAmap = new map<string,string>();//OPTIONS-SPA
            map<string,string> PROMOTIONSPAmap = new map<string,string>();//PROMOTION-SPA
            map<string,string> SPAABNmap = new map<string,string>(); // SPA-ABN
            map<string,string> SPAAGSmap = new map<string,string>(); // SPA-AGS
            map<string,string> SPAAGSXmap = new map<string,string>(); //SPA-AGSX
            map<string,string> SPACRTmap = new map<string,string>(); // SPA-CRT
            map<string,string> SPALAGXmap = new map<string,string>(); // SPA-LAGX
            map<string,string> SPASPAmap = new map<string,string>(); // SPA-SPA
            map<string,string> SPAHALmap = new map<string,string>(); // SPA-HAL
            // Process what has come
            if(resp.results.size() > 0){
                for(integer i = 0; i < resp.results.size(); i++){
                    if(resp.results[i].reportName == 'SPA'){
                        spaMap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                    if(resp.results[i].reportName == 'PROMOTION'){
                        promotionMap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                    if(resp.results[i].reportName == 'DP INVOICE'){
                        dpMap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                    if(resp.results[i].reportName == 'OPTIONS'){
                       optionMap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                    if(resp.results[i].reportName == 'SCHEME' || resp.results[i].reportName == 'SCHEME-SPA'){ // ADDED ON Nov 29 2018
                       schemeMap.put(resp.results[i].registrationId, resp.results[i].reportURL);  
                    }
                    // New Reports added on Oct 21 2018
                    if(resp.results[i].reportName == 'CAMPAIGN-SPA'){
                        CAMPAIGNSPAmap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                    if(resp.results[i].reportName == 'DP INVOICE-SPA'){
                        DPINVOICESPAmap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                    if(resp.results[i].reportName == 'OPTIONS-SPA'){
                        OPTIONSSPAmap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                    if(resp.results[i].reportName == 'PROMOTION-SPA'){
                        PROMOTIONSPAmap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }                    
                    if(resp.results[i].reportName == 'SPA-ABN'){
                        SPAABNmap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                    if(resp.results[i].reportName == 'SPA-AGS'){
                        SPAAGSmap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                    if(resp.results[i].reportName == 'SPA-AGSX'){
                        SPAAGSXmap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                    if(resp.results[i].reportName == 'SPA-CRT'){
                        SPACRTmap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                    if(resp.results[i].reportName == 'SPA-LAGX'){
                        SPALAGXmap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                    if(resp.results[i].reportName == 'SPA-SPA'){
                        SPASPAmap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                    if(resp.results[i].reportName == 'SPA-SAGSX'){
                        SPASPAmap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                    if(resp.results[i].reportName == 'SPA-SAGS'){
                        SPASPAmap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                    if(resp.results[i].reportName == 'SPA-HLA'){
                        SPAHALmap.put(resp.results[i].registrationId, resp.results[i].reportURL);
                    }
                }
                // Prepare RegId Set
                set<string> regIds = new set<string>();
                regIds.addall(dpMap.keyset());
                regIds.addall(promotionMap.keyset());
                regIds.addall(spaMap.keyset());
                Booking__c booking = new booking__c();
                if(!test.isrunningtest()){
                // Query Units and upsert  Docs
                booking = [SELECT Id, Deal_SR__c, Poll_for_SPA__c, Poll_for_SPA_Count__c,
                                (SELECT Id, Registration_ID__c, booking__r.Deal_SR__c FROM Booking_units__r) 
                           FROM booking__c WHERE Id =: bookingId LIMIT 1]; 
                } else{
                    // Query Units and upsert  Docs
                    booking = [SELECT Id, Poll_for_SPA__c, Deal_SR__c, Poll_for_SPA_Count__c,
                                    (SELECT Id, Registration_ID__c, booking__r.Deal_SR__c FROM Booking_units__r) 
                                FROM booking__c WHERE Booking_Channel__c='Office' AND Deal_SR__r.ownerid =: userinfo.getuserid()  LIMIT 1]; 
                }
                try{
                    list<Unit_Documents__c> Docstoinsert = new list<Unit_Documents__c>();
                    for(booking_unit__c bu:booking.Booking_units__r){
                        Unit_Documents__c doc = new Unit_Documents__c();
                        doc.document_type__c = 'SPA Docs';
                        doc.Registration_Id__c = bu.registration_id__c+'-'+processId;
                        doc.booking_unit__c = bu.id;
                        doc.DP_INVOICE__c = dpMap.get(bu.registration_id__c);
                        doc.OPTION__c = optionMap.get(bu.registration_id__c);
                        doc.PROMOTION__c = promotionMap.get(bu.registration_id__c);
                        doc.SPA__c = spaMap.get(bu.registration_id__c);
                        doc.scheme__c = schemeMap.get(bu.registration_id__c);
                        doc.Booking__c = booking.id;
                        doc.Service_Request__c = bu.booking__r.Deal_SR__c;
                        // Added on Oct 21 2018
                        if(doc.SPA__c == null){
                            doc.SPA__c = SPASPAmap.get(bu.registration_id__c);
                        }
                        if(doc.PROMOTION__c == null){
                            doc.PROMOTION__c = PROMOTIONSPAmap.get(bu.registration_id__c);
                        }
                        if(doc.OPTION__c == null){
                            doc.OPTION__c = OPTIONSSPAmap.get(bu.registration_id__c);
                        }
                        if(doc.DP_INVOICE__c == null){
                            doc.DP_INVOICE__c = DPINVOICESPAmap.get(bu.registration_id__c);
                        }
                        doc.Campaign_SPA__c = CAMPAIGNSPAmap.get(bu.registration_id__c);
                        doc.SPA_ABN__c = SPAABNmap.get(bu.registration_id__c);
                        doc.SPA_AGS__c = SPAAGSmap.get(bu.registration_id__c);
                        doc.SPA_AGSX__c = SPAAGSXmap.get(bu.registration_id__c);
                        doc.SPA_CRT__c = SPACRTmap.get(bu.registration_id__c);
                        doc.SPA_LAGX__c = SPALAGXmap.get(bu.registration_id__c);
                        doc.SPA_HAL__c = SPAHALmap.get(bu.registration_id__c);
                        Docstoinsert.add(doc);
                    }
                    system.debug('Entered');
                    if( pollAgain && (booking.Poll_for_SPA_Count__c <= 100 || booking.Poll_for_SPA_Count__c == null)){
                        system.debug('Entered');
                        booking.Poll_for_SPA__c = true;
                        if(booking.Poll_for_SPA_Count__c != null){
                            booking.Poll_for_SPA_Count__c = booking.Poll_for_SPA_Count__c+1;
                        } else{
                            booking.Poll_for_SPA_Count__c = 1;
                        }
                        System.debug('>>>>>>booking.Poll_for_SPA_Count__c>>>>>>>>>'+booking.Poll_for_SPA_Count__c);
                        system.enqueueJob(new Async_IPMS_Rest_SPA(ProcessURL,bookingId));
                        //pollmore =true;
                    } else{
                        system.debug('Entered false');
                        booking.Poll_for_SPA__c = false;
                        booking.Poll_for_SPA_Count__c = 0;
                        // Update SR and Send the Status to IPMS
                        srstatus = [SELECT Id, NSIBPM__Code__c FROM NSIBPM__SR_Status__c WHERE NSIBPM__Code__c = 'AGREEMENT_GENERATED'].Id;
                        NSIBPM__Service_Request__c SR = [SELECT Id, SPA_Generation_Status__c, Delivery_Mode__c, NSIBPM__Internal_SR_Status__c,
                                                            NSIBPM__External_SR_Status__c, NSIBPM__Internal_SR_Status__r.NSIBPM__Code__c 
                                                        FROM NSIBPM__Service_Request__c WHERE Id =: booking.Deal_SR__c];
                        SR.NSIBPM__Internal_SR_Status__c = srstatus;
                        SR.NSIBPM__External_SR_Status__c = srstatus;
                        SR.SPA_Generation_Status__c = null;
                        update SR; 
                        //Call status Update
                        list<Id> bookingIds = new list<Id>();
                        bookingIds.add(booking.id);
                        // system.enqueueJob(new AsyncReceiptWebservice (bookingIds, 'StatusUpdate')); // commented out as part of 1.1
                        DAMAC_IPMS_PARTY_CREATION.statusUpdate(bookingIds, '', ''); // 1.1
                    }
                    upsert Docstoinsert Registration_Id__c ;
                    update booking;
                }catch(exception e){
                    createLog(bookingId ,string.valueof(e.getMessage()));
                }
            } 
        } 
    }

    //Create Log for Error
    public void createLog(string parentId,String ErrorMessage){
        try{
            log__c log = new log__c();
            log.Booking__c = parentId;
            log.Description__c = ErrorMessage;
            insert log;
        } catch(exception e){}
    }
}