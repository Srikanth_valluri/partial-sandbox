/****************************************************************************************
 * Class : CreateProofOfPaymentControllerTest 
 * Created By : ES
 -----------------------------------------------------------------------------------
 * Description : Test class for CreateProofOfPaymentPage
 -----------------------------------------------------------------------------------
 * Version History:
 * Version    Developer Name    Date          Detail Features
   1.0        Nikhil Pote       16/08/2018    Initial Development
 **********************************************************************************/
@isTest
private class CreateProofOfPaymentControllerTest {

    static testMethod void myUnitTest() {
      NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
      SR.Delivery_mode__c='Email';
      SR.Deal_ID__c='1001';
      insert SR;
      
      update SR;
      List<NSIBPM__Service_Request__c > listSR = new List<NSIBPM__Service_Request__c >();
      listSR =[select Name
                 from NSIBPM__Service_Request__c 
                 where ID=: SR.ID];                
                 
       System.debug('...SR...'+listSR[0].Name);
       ApexPages.currentPage().getParameters().put('SRId',listSR[0].Name);
        Booking__c objB = new Booking__c();
        objB.Deal_SR__c = SR.ID;
        insert objB;
        
        Booking_Unit__c obj3 = new Booking_Unit__c();
        obj3.Booking__c = objB.ID;
        obj3.Registration_ID__c = '13550';
        insert obj3;
        
        List<Booking_Unit__c> listB = new List<Booking_Unit__c>();
        listB  =[select id,
                        Booking__r.Deal_SR__r.Name,
                        Registration_ID__c
                 from Booking_Unit__c
                 where id=: obj3.ID];                
                 
        System.debug('...listB  ...'+listB);
        
        CreateProofOfPaymentController.POPDetails jsonobject_POP = new CreateProofOfPaymentController.POPDetails();
        jsonobject_POP.dealSR1= SR.ID;
        jsonobject_POP.curr= '50';
        jsonobject_POP.pa= '80';
        jsonobject_POP.bu= obj3.ID;
        jsonobject_POP.pd= '10/10/2018';
        jsonobject_POP.pm= 'Cash';
        jsonobject_POP.pr= 'Test';        
        //jsonobject_POP.Status__c= 'SR Submitted to Finance';
        String POPJSON = JSON.serialize(jsonobject_POP);     
        
        PageReference pageRef = Page.CreateProofOfPaymentPage; 
        pageRef.getParameters().put('id', String.valueOf(SR.Id));
        Test.setCurrentPage(pageRef);
        
        CreateProofOfPaymentController.POPDetails test = new CreateProofOfPaymentController.POPDetails();
        CreateProofOfPaymentController objController = new CreateProofOfPaymentController();
        
        /*
        objController.objAttachment.Name='Unit';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        objController.objAttachment.body=bodyBlob;
        objController.objAttachment.parentId=cse.id;
        insert attach; */
        CreateProofOfPaymentController.Save(POPJSON);   
        List<SelectOption> selBUs=CreateProofOfPaymentController.getBookingUnits();  
        system.debug('mode======='+selBUs);
        System.assertEquals(selBUs.size(), 2);  
        
    }
}