public without sharing class FmcRegisterComplaintController extends RegisterComplaintController {
    public list<SelectOption> lstUnits {get; set;}
    String caseId {get;set;}
    //@TestVisible public String strSelectedUnit {get; set;}
    public Booking_Unit__c objBu {get; set;}
    public Location__c objLoc{get;set;}
    public map<Id,Booking_Unit__c> mapUnitsOwned {get; set;}
    public map<Id,Booking_Unit__c> mapMyUnits {get; set;}
    public List<Booking_Unit__c> objUnitList  {get; set;}
    public List<Id> objUnitRes                {get; set;}
    public List<Id> objUnitOwner              {get; set;}

    public FmcRegisterComplaintController() {
        super(false);
        if (FmcUtils.isCurrentView('servicechargeenquiry')) {
            super();
            Map<String, String> params = ApexPages.currentPage().getParameters();
            caseId = params.get('id');
            if(String.isBlank(caseId)) {
                strAccountId = CustomerCommunityUtils.customerAccountId;
            } else {
                objFMCase = FM_Utility.getCaseDetails( caseId );
                strAccountId = objFMCase.Account__c ;
                objFMCase = FM_Utility.getCaseDetails(caseId);
                objFMCase.Origin__c = 'Portal';
            }
            strSRType='Register_Complaint';
            System.debug('== strAccountId ==' + strAccountId );
            System.debug('== strSelectedUnit ==' + strSelectedUnit );
            System.debug('== strFMCaseId ==' + strFMCaseId );
            fetchUnits();
        }
    }

    /*public static Booking_Unit__c getUnitDetails(String strUnitId) {
        return RegisterComplaintController.getUnitDetails(strUnitId);
    }*/

    public void getUnitDetails(){
        system.debug('==strUnitId=='+strSelectedUnit);
        if(String.isNotBlank(strSelectedUnit)) {
            objBU = FM_Utility.getUnitDetails(strSelectedUnit);
            system.debug('==objBU=='+objBU);
            init();
            if(objBU != NULL) {
                if(objBU.Unit_Name__c != NULL) {
                    list<Location__c> lstLocs = new list<Location__c>();
                     lstLocs = [SELECT Community_Name__c, Move_In_Move_Out_security_cheque_amount__c
                                FROM Location__c
                                WHERE Name =: objBU.Unit_Name__c.substringBefore('/') LIMIT 1];
                    if(lstLocs.size() > 0) {
                        objLoc = lstLocs[0];
                    }
                }
            }
        }
    }

    public void fetchUnits(){
        Set<String> activeStatusSet= Booking_Unit_Active_Status__c.getall().keyset();
        mapUnitsOwned = new map<Id,Booking_Unit__c>();
        objUnitOwner = new List<Id>();
        objUnitRes = new List<Id>();
        mapMyUnits = new map<Id,Booking_Unit__c>();
        lstUnits = new list<SelectOption>();
        lstUnits.add(new selectOption('None','--None--'));
        objUnitList = [SELECT Id
                            , Owner__c
                            , Booking__r.Account__c
                            , Booking__r.Account__r.Name
                            , Owner__r.IsPersonAccount
                            , Tenant__r.IsPersonAccount
                            , Owner__r.Name
                            , Tenant__c
                            , Tenant__r.Name
                            , Resident__c
                            , Resident__r.Name
                            , Unit_Name__c
                            , Registration_Id__c
                            , Property_Name__c
                         FROM Booking_Unit__c
                        WHERE ( Resident__c = :strAccountId
                                OR Booking__r.Account__c = :strAccountId
                                OR Tenant__c = :strAccountId )
                          AND ( Handover_Flag__c = 'Y'
                                OR Early_Handover__c = true )
                          AND Registration_Status__c IN :activeStatusSet
                    ];
        if(!objUnitList.isEmpty() && objUnitList != NULL){
            for( Booking_Unit__c objBU : objUnitList ){
                if( objBU.Owner__c == strAccountId ){
                    objUnitOwner.add(objBU.Id);
                    mapUnitsOwned.put(objBU.Id,objBU);
                }

                if( ( objBU.Tenant__c == strAccountId || objBU.Resident__c == strAccountId )&& objBU.Owner__c != strAccountId ){
                     objUnitRes.add(objBU.Id);
                     mapMyUnits.put(objBU.Id,objBU);
                }
                lstUnits.add(new selectOption(objBU.Id,objBU.Unit_Name__c));
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'No Booking Units available'));
        }
        system.debug('==lstUnits=='+lstUnits);
    }
    public override void createMethod() {
        objFMCase.Origin__c='Portal';
        objFMCase.Status__c = 'Draft Request';
        try {
            upsert objFMCase;
        } catch(Exception e) {
            System.debug(e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'There was an error processing your request. Please try again later.'));
        }
    }

    public override void createCaseShowUploadDoc(){

        if ( objFMCase != NULL){
            createMethod();
            System.debug('oBjFMCase.id------'+oBjFMCase.id);
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please select Booking Unit'));
        }
        System.debug('objFMCase-----'+objFMCase);
        if ( objFMCase != NULL && objFMCase.Id != NULL ) {
            processDocuments();
        }
        if ( objFMCase != NULL){
            strFMCaseId=objFMCase.id;
            System.debug('strFMCaseId----'+strFMCaseId);
        }
    }

    public override PageReference returnBackToCasePage() {
        System.debug('objFMCase.id-------3---------'+objFMCase.id);
        if ( objFMCase != NULL ) {
            PageReference objPage = new PageReference('/');
            return objPage ;
        }
        return NULL;
    }

    public PageReference saveSr() {
        objFMCase.Origin__c = 'Portal';
        PageReference nextPage = createRequestForComplaint();
        //System.debug('nextPage = ' + nextPage);
        if (nextPage != NULL) {
            //System.debug('nextPage Url = ' + nextPage.getUrl());
            String unitId = nextPage.getUrl().substringAfterLast('/');
            nextPage = new PageReference(ApexPages.currentPage().getUrl());
            nextPage.getParameters().clear();
            nextPage.getParameters().put('view', 'CaseDetails');
            nextPage.getParameters().put('id', unitId);
            nextPage.setRedirect(true);
        }
        return nextPage;
    }

    public PageReference submitSr() {
        objFMCase.Origin__c = 'Portal';
        PageReference nextPage = submitRequestForComplaint();
        if (nextPage != NULL) {
            String unitId = nextPage.getUrl().substringAfterLast('/');
            nextPage = new PageReference(ApexPages.currentPage().getUrl());
            nextPage.getParameters().clear();
            nextPage.getParameters().put('view', 'CaseDetails');
            nextPage.getParameters().put('id', unitId);
            nextPage.setRedirect(true);
        }
        //System.debug('nextPage = ' + nextPage);
        //System.debug('nextPage Url = ' + nextPage.getUrl());
        return nextPage;
    }
}