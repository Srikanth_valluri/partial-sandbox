@istest
public class SaveFCMTokenFromMobileAppTest {
    public  testmethod static void method()
    {
        Account a=new Account();
        a.name='test1';
        insert a;
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='abc123545@testorg.com');
        insert u;
        FCM_Token__c f=new FCM_Token__c();
        f.Account__c=a.id;
        f.Fcm_Token__c='test';
        SaveFCMTokenFromMobileApp.SaveFCMTokenFromMobileApps(a.id,u.id,f.Fcm_Token__c);
    }
    public  testmethod static void method1()
    {
        Account a=new Account();
        a.name='test';
        insert a;
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='abc12354@testorg.com');
      
        insert u;
        FCM_Token__c f=new FCM_Token__c();
        f.Account__c=a.id;
        f.Fcm_Token__c='test1';
        insert f;
        SaveFCMTokenFromMobileApp.SaveFCMTokenFromMobileApps(a.id,u.id,f.Fcm_Token__c);
    }
}