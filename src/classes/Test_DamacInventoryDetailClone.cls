@isTest
private class Test_DamacInventoryDetailClone {
    public static Contact adminContact;
    public static User portalUser;
    public static Account adminAccount;
    public static User portalOnlyAgent;
    private static Inventory__c inventoryObj;
    private static  SalesOffer_Record__c offObj;
    public static Currency_Rate__c currencyRateObj;
    public static Currency_Rate__c currencyRateObjNew;
    static void init(){

        adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        insert adminAccount;
        
        adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        insert adminContact;
        
        Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
        insert agentContact;
        
        //portalUser = InitialiseTestData.getPortalUser('test@test.com', adminContact.Id, 'Admin');
        //portalOnlyAgent = InitialiseTestData.getPortalUser('test1@test.com', agentContact.Id, 'Agent');
        
        Location__c firstLoc = new Location__c();
        firstLoc.Location_Code__c = 'CRT';
        firstLoc.Name = 'CRT';
        firstLoc.Location_ID__c = '74588';
        firstLoc.Location_Type__c = 'Building';
        firstLoc.Building_Name__c = 'COURSETIA @ AKOYA OXYGEN';

        insert firstLoc;
        
        //System.runAs(portalUser){
           Property__c property = InitialiseTestData.insertProperties();
           inventoryObj = InitialiseTestData.getInventoryDetails('INV-001','123','ACZ11',123,123);
           inventoryObj.Property_City__c ='DUBAI';
           inventoryObj.Unit_Location__c=firstLoc.id;
           inventoryObj.Status__c='Released';
           insert inventoryObj;
           
           Inventory__c invenobj = InitialiseTestData.getInventoryDetails('INV-002','123','ACZ11',123,123);
           invenobj = InitialiseTestData.getInventoryDetails('INV-002','123','ACZ11',123,123);
           invenobj.Property_City__c ='RIYADH';
           invenobj.Unit_Location__c=firstLoc.id;
           invenobj.Status__c='Released';
           invenobj.Currency_of_Sale__c = 'BHD';
           insert invenobj;
            
           SalesOfferAdditionalCharges__c cs = new SalesOfferAdditionalCharges__c();
           cs.name='DUBAI';
           cs.Land_Registration_Fee_A__c='Selling_Price_excl_VAT__c M 0.04';
           cs.DSR_Fees_D__c='Selling_Price_excl_VAT__c M 0.04';
           cs.Disclaimer__c='Selling_Price_excl_VAT__c M 0.04';
           cs.OQOOD_Fee_C__c='Selling_Price_excl_VAT__c M 0.04';
           cs.Title_Deed_B__c='Selling_Price_excl_VAT__c M 0.04';
           cs.Other_Fee__c='Selling_Price_excl_VAT__c M 0.04';
           insert cs;
            offObj = new SalesOffer_Record__c();
            offObj.Trasaction_User__c = UserInfo.getUserId();
            offObj.Selected_Inventory_Ids__c = inventoryObj.id+','+invenobj.id;
            insert offObj;
           
            currencyRateObj = new Currency_Rate__c();
            currencyRateObj.Conversion_Date__c = system.today();
            currencyRateObj.Conversion_Rate__c = 9.800000;
            currencyRateObj.From_Currency__c = 'BHD';
            currencyRateObj.To_Currency__c = 'AED';
            //currencyRateObj.Conversion_Date_c = 
            insert currencyRateObj;
            currencyRateObjNew = new Currency_Rate__c();
            currencyRateObjNew.Conversion_Date__c = system.today();
            currencyRateObjNew.Conversion_Rate__c = 9.800000;
            currencyRateObjNew.From_Currency__c = 'AED';
            currencyRateObjNew.To_Currency__c = 'BHD';
            //currencyRateObj.Conversion_Date_c = 
            insert currencyRateObjNew;

        //}
    }

    @isTest static void InventoryPDFDetail() {
        Test.startTest();
        init();

        //System.runAs(portalUser){
            
             
            PageReference pageRef = Page.damac_inventorydetail_clone;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('recId', String.valueOf(offObj.Id));
            pageRef.getParameters().put('selectedCurrency', 'BHD');
            ApexPages.StandardController inventoryStd = new ApexPages.StandardController(offObj);         
            InventoryPDFDetailPage_Clone inventory = new InventoryPDFDetailPage_Clone(inventoryStd);
            
             Location__c firstLoc = new Location__c();
            firstLoc.Location_Code__c = 'CRTB';
            firstLoc.Name = 'CRTB';
            firstLoc.Location_ID__c = '745884';
            firstLoc.Location_Type__c = 'Building';
            firstLoc.Building_Name__c = 'COURSETIA @ AKOYA OXYGEN 123';
    
            insert firstLoc;
        
           //Inventory__c invenobj = InitialiseTestData.getInventoryDetails('INV-002','123','ACZ121',123,123);
           Inventory__c invenobj  = InitialiseTestData.getInventoryDetails('INV-011','123','ACZ121',123,123);
           invenobj.Property_City__c ='RIYADH  Test';
           invenobj.Unit_Location__c=firstLoc.id;
           invenobj.Status__c='Released';
           invenobj.Currency_of_Sale__c = 'BHD';
            invenobj.Plot_Plan_SF_Link__c ='https://damacholding--c.eu18.content.force.com/servlet/servlet.FileDownload?file=00P0Y00000XBK4bUAH';
            invenobj.Floor_Plan_SF_Link__c ='https://damacholding--c.eu18.content.force.com/servlet/servlet.FileDownload?file=00P0Y00000XBK4bUAH';
            insert invenobj;
            inventory.fillTheMapStructure(invenobj);         
            inventory.calculate(0.2,'D',3.1);
            inventory.calculate(0.2,'S',3.1);
            inventory.calculate(0.2,'A',3.1);
        //}

        Test.stopTest();
    }
    
}