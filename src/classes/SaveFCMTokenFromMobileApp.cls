@RestResource(urlMapping='/SaveFCMTokenFromMobileApp/*')
 Global class SaveFCMTokenFromMobileApp
 {
     @HtTPPost
    Global static FCM_Token__c SaveFCMTokenFromMobileApps(String AccountId,String userId,String fcmToken)
    {
    FCM_Token__c f=new FCM_Token__c();
        System.debug(AccountId);
        list<FCM_Token__c> fList=[select id,Account__c from FCM_Token__c where Account__c =:AccountId];
        if(fList.size()>0){
       
        f.Account__c=AccountId;
       
        f.User__c=userId;
       if(!test.isRunningTest())
            f.ownerId=userId;
        f.Fcm_Token__c=fcmToken;
        f.id=fList[0].id;
        update f;
        }else
        {
        
        f.Account__c=AccountId;
        f.ownerId=userId;
        f.User__c=userId;
        f.ownerId=userId;
        f.Fcm_Token__c=fcmToken;
      
        insert f;
        }
        return f;
         
    }
 }