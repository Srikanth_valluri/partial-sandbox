@isTest
private class MyTripsController_TEST{

    static testMethod void testTripController() {
        
        Trip_Tracking_Status__c status = new Trip_Tracking_Status__c();
        status.Name = '1';
        status.Status__c = 'Started';
        insert status;
        
        Trip__c trip = new Trip__c ();
        trip.Trip_Requester_Comments__c = 'Testing';
        trip.start_time__c = DateTime.Now();
        trip.end_time__c = DateTime.Now();
        trip.Status__c = 'Test';
        insert trip;
        
        Apexpages.currentpage().getparameters().put('tripId', trip.Id);
        Apexpages.currentpage().getparameters().put('type', 'CheckIn');
        Apexpages.currentpage().getparameters().put('lat', '25.142903333333333');
        Apexpages.currentpage().getparameters().put('lang', '55.142903333333333');
        
        MyTripsController obj = new MyTripsController(null);
        obj.updateLocation ();
        obj.Loadtrips();
        obj.reload();
        
        Damac_VehicleLocationJSON_RESP resp = new Damac_VehicleLocationJSON_RESP ();
        Damac_VehicleLocationJSON_RESP.parse('{"status" : 200 }');
        Damac_VehicleLocationJSON_RESP.cls_results results = new Damac_VehicleLocationJSON_RESP.cls_results ();
    }
}