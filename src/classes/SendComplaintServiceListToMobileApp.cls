@RestResource(urlMapping='/SendComplaintServiceListToMobileApp/*')
 Global class SendComplaintServiceListToMobileApp
 {
     @HtTPGet
    Global static list<String> SendComplaintServiceListToMobile()
    {
       List<String> pickListValuesList= new List<String>();
       Schema.DescribeFieldResult fieldResult = Case.Complaint_Type__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       for( Schema.PicklistEntry pickListVal : ple){
           pickListValuesList.add(pickListVal.getLabel());
           } 
           
           return pickListValuesList;
    }
 }