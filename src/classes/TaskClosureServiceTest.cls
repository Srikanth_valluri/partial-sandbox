/*-------------------------------------------------------------------------------------------------
Description: Test class for TaskClosureService

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By  | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 22-11-2017       | Lochana Rajput    | 1. To verify Case attachment
        1.1     | 19-12-2017       | Vivek Shinde      | 1. Test method for task closure of title deed
   =============================================================================================================================
*/
@isTest
private class TaskClosureServiceTest {

    @isTest static void test_verifyCaseAttachment() {
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Fund Transfer Active Units').getRecordTypeId();
        Id userId = [SELECT Id FROM User WHERE isActive=true LIMIT 1].Id;
        list<Task> lstTask = new list<Task>();
        list<Case> lstCases = new list<Case>();
        list<SR_Attachments__c> lstAtt = new list<SR_Attachments__c>();
        List<Account> lstAcc = new List<Account>();
        lstAcc = TestDataFactory_CRM.createCustomers(20);
        insert lstAcc;
        for(Account objAcc : lstAcc) {
            Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
            objCase.OwnerId = userId;
            lstCases.add(objCase);
        }
        insert lstCases;
        for(Case objCase : lstCases) {
            SR_Attachments__c objAttPOA = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'Power Of Attorney');
            objAttPOA.Attachment_URL__c = 'test url';
            lstAtt.add(objAttPOA);
            Task objTask = TestDataFactory_CRM.createTask(objCase, 'Test Subject', 'Legal', 'Fund Transfer', System.today().addDays(1));
            objTask.SR_Attachment_Id__c = objAttPOA.Id;
            objTask.Document_URL__c = objAttPOA.Attachment_URL__c;
            objTask.Status = 'Completed';
            lstTask.add(objTask);
        }
        insert lstAtt;
        for(Integer i=0; i<lstAtt.size();i++) {
            Task objTask = TestDataFactory_CRM.createTask(lstCases[i], 'Test Subject', 'Legal', 'Fund Transfer', System.today().addDays(1));
            objTask.SR_Attachment_Id__c = lstAtt[i].Id;
            objTask.Document_URL__c = lstAtt[i].Attachment_URL__c;
            objTask.Status = 'Completed';
            lstTask.add(objTask);
        }

        Test.startTest();
        insert lstTask;
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        TaskClosureService.verifyCaseAttachment(lstTask);
        for(SR_Attachments__c att : [SELECT isValid__c FROM SR_Attachments__c]) {
            System.assertEquals(true, att.isValid__c);
        }
        Test.stopTest();
    }

    @isTest static void testTitleDeedTaskClosure() {
        Id idSVPRole;
        List<UserRole> lstRole = [SELECT Id, DeveloperName FROM UserRole WHERE DeveloperName = 'SVP_Operations' LIMIT 1];
        if(lstRole != null && !lstRole.isEmpty()) {
            idSVPRole = lstRole[0].Id;
        }
        else {
            UserRole SVP_OperationsRole = new UserRole(DeveloperName = 'SVP_Operations', Name = 'SVP Operations');
            insert SVP_OperationsRole;
            idSVPRole= SVP_OperationsRole.Id;
        }

        User SVP_OperationsUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com1' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = idSVPRole
        );
        insert SVP_OperationsUser;

        System.runAs(SVP_OperationsUser) {
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Title Deed').getRecordTypeId();
        Id userId = [SELECT Id FROM User WHERE isActive=true LIMIT 1].Id;
        list<Task> lstTask = new list<Task>();
        list<Case> lstCases = new list<Case>();
        List<Account> lstAcc = new List<Account>();
        lstAcc = TestDataFactory_CRM.createCustomers(1);
        insert lstAcc;
        for(Account objAcc : lstAcc) {
            Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
            objCase.OwnerId = userId;
            lstCases.add(objCase);
        }
        insert lstCases;
        for(Case objCase : lstCases) {
            Task objTask = TestDataFactory_CRM.createTask(objCase, 'Submit details in DSR and generate NOC', 'Business Development', 'Title Deed', System.today().addDays(1));
            objTask.Status = 'Completed';
            objTask.Assigned_User__c = 'Business Development';
            lstTask.add(objTask);
        }
        insert lstTask;
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        Test.startTest();
        TaskClosureService.verifyCaseAttachment(lstTask);
        Test.stopTest();
        }
    }
}