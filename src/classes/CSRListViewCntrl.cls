public class CSRListViewCntrl {
    public List<Case> caseList{get;set;}
   //   public CSRListViewWrapper objCsrListView {get;set;}
    public String userid{get;set;}

    
     public Double offset{get{   TimeZone tz = UserInfo.getTimeZone(); return tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);}}
    
    @RemoteAction
    public static List<case> getPendingSRList() {
        return init();
    }
 
    @RemoteAction
    public static Integer getPendingSRCount() {
        return init().size();
    }
 
    public static List<Case> init() {
        Id idOfCurrentLoggedInUser = Userinfo.getuserid();
        //userid = Userinfo.getUserId();
        List<Case> pendingCaseList = new List<Case>();
        pendingCaseList = [SELECT Id,
                    CaseNumber,
                    CreatedDate,
                    Approving_User_Role__c,
                    Approving_User_Name__c,
                    Account.Name,AccountId,
                    (SELECT Id,
                             Name,Status__c,
                             Manager_Approval_Assignment_Date__c,
                             HOD_Approval_Assignment_Date__c,
                             Committee_Approval_Assignment_Date__c,
                             Director_Approval_Assignment_Date__c,
                             CRE_Approval_Assignment_Date__c,
                             RequestType__c
                             FROM CaseSummaryRequests__r)
                    FROM Case
                    WHERE RecordType.Name = 'Case Summary - Client Relation'
                    AND Approving_User_Id__c = : idOfCurrentLoggedInUser
                    AND Status = 'Submitted'
                    ORDER BY CreatedDate ASC
                   ];
        return pendingCaseList;
    }
    
    
    
    
    
    /*public CSRListViewCntrl() {
        init();
    }
    public void init() {
        Id idOfCurrentLoggedInUser = Userinfo.getuserid();
        userid = Userinfo.getUserId();

        caseList = [SELECT Id,
                    CaseNumber,
                    CreatedDate,
                    Approving_User_Role__c,
                    Approving_User_Name__c,
                    Account.Name,AccountId,
                    (SELECT Id,
                             Name,Status__c,
                             Manager_Approval_Assignment_Date__c,
                             HOD_Approval_Assignment_Date__c,
                             Committee_Approval_Assignment_Date__c,
                             Director_Approval_Assignment_Date__c,
                             RequestType__c
                             FROM CaseSummaryRequests__r)
                    FROM Case
                    WHERE RecordType.Name = 'Case Summary - Client Relation'
                    AND Approving_User_Id__c = : idOfCurrentLoggedInUser
                    AND Status = 'Submitted'
                   ];
        //System.debug('caseList--->'+caseList);
        showCountOfCaseSummary();
    }
    
    public void showCountOfCaseSummary() {
        Set<String> caseNumbers = new Set<String>();
        objCsrListView = new CSRListViewWrapper();
        
        
        
        List<CaseSummaryRequest__c> caseSummaryList = [ Select Id,
                                                                Name,
                                                                 Manager_Approval_Assignment_Date__c,
                                                                 HOD_Approval_Assignment_Date__c,
                                                                 Committee_Approval_Assignment_Date__c,
                                                                 Director_Approval_Assignment_Date__c,
                                                                 RequestType__c,
                                                                 Manager_Id__c,
                                                                 Manager_Name__c,
                                                                 Manager_Approval_Status__c,
                                                                 HOD_Id__c,HOD_Name__c,
                                                                 HOD_Approval_Status__c,
                                                                 Director_Id__c,Director_Name__c,
                                                                 Director_Approval_Status__c,
                                                                 Committee_Id__c,Committee_Name__c,
                                                                 Committee_Approval_Status__c,
                                                                 Case__r.CaseNumber,
                                                                 Case__r.Account.Name,
                                                                 Case__r.AccountId,
                                                                 Case__r.Status,
                                                                 Case__c,
                                                                 Case__r.Approving_User_Role__c,
                                                                 Case__r.Approving_User_Name__c,
                                                                 Committee_Approval_Date__c,
                                                                 Director_Approval_Date__c,
                                                                 HOD_Approval_Date__c,
                                                                 Manager_Approval_Date__c
                                                        FROM    CaseSummaryRequest__c 
                                                        WHERE   (Manager_Id__c = : Userinfo.getuserid()
                                                        OR      HOD_Id__c = : Userinfo.getuserid()
                                                        OR      Director_Id__c = : Userinfo.getuserid()
                                                        OR      Committee_Id__c = : Userinfo.getuserid())
                                                        AND    (Manager_Approval_Status__c != Null
                                                        OR    HOD_Approval_Status__c != Null
                                                        OR    Director_Approval_Status__c != Null
                                                        OR    Committee_Approval_Status__c!= Null)
                                                                 ];
        //System.debug('caseSummaryList-->'+caseSummaryList);
        //System.debug('caseSummaryListSize-->'+caseSummaryList.size());
      if(caseSummaryList != null && caseSummaryList.size() > 0) {
        Map<String,List<CaseSummaryRequest__c>> approvedCaseSummaryMap = new Map<String,List<CaseSummaryRequest__c>>();
        Map<String,List<CaseSummaryRequest__c>> approvedWithChangesCaseSummaryMap = new Map<String,List<CaseSummaryRequest__c>>();
        Map<String,List<CaseSummaryRequest__c>> rejectedCaseSummaryMap = new Map<String,List<CaseSummaryRequest__c>>();
        Map<String,List<CaseSummaryRequest__c>> needMoreInfoCaseSummaryMap = new Map<String,List<CaseSummaryRequest__c>>();
        for(CaseSummaryRequest__c objCaseSummary : caseSummaryList) {
           caseNumbers.add(objCaseSummary.Case__r.CaseNumber);
           if( (objCaseSummary.Manager_Id__c == Userinfo.getuserid() 
               && objCaseSummary.Manager_Approval_Status__c == 'Approved' ) || 
               (objCaseSummary.HOD_Id__c == Userinfo.getuserid() 
               && objCaseSummary.HOD_Approval_Status__c == 'Approved') || 
                (objCaseSummary.Director_Id__c == Userinfo.getuserid() 
               && objCaseSummary.Director_Approval_Status__c == 'Approved' ) ||
                (objCaseSummary.Committee_Id__c == Userinfo.getuserid() 
               && objCaseSummary.Committee_Approval_Status__c == 'Approved' )) {
                      if(approvedCaseSummaryMap.containsKey(objCaseSummary.Case__r.CaseNumber) 
                        && approvedCaseSummaryMap.get(objCaseSummary.Case__r.CaseNumber) != NULL
                        && approvedCaseSummaryMap.get(objCaseSummary.Case__r.CaseNumber).size() > 0){
                            List<CaseSummaryRequest__c> caseSummaryLst = approvedCaseSummaryMap.get(objCaseSummary.Case__r.CaseNumber);
                            caseSummaryLst.add(objCaseSummary);
                            approvedCaseSummaryMap.put(objCaseSummary.Case__r.CaseNumber,caseSummaryLst);
                      }
                      else {
                         List<CaseSummaryRequest__c> caseSummaryLst = new List<CaseSummaryRequest__c>();
                         caseSummaryLst.add(objCaseSummary);                 
                         approvedCaseSummaryMap.put(objCaseSummary.Case__r.CaseNumber,caseSummaryLst);
                      }
                //approvedCaseSummaryList.add(objCaseSummary.Case__r.CaseNumber);
            }
            else if( (objCaseSummary.Manager_Id__c == Userinfo.getuserid() 
               && objCaseSummary.Manager_Approval_Status__c == 'Approved with changes' ) ||
                (objCaseSummary.HOD_Id__c == Userinfo.getuserid() 
               && objCaseSummary.HOD_Approval_Status__c == 'Approved with changes' )|| 
                (objCaseSummary.Director_Id__c == Userinfo.getuserid() 
               && objCaseSummary.Director_Approval_Status__c == 'Approved with changes' )|| 
                (objCaseSummary.Committee_Id__c == Userinfo.getuserid() 
               && objCaseSummary.Committee_Approval_Status__c == 'Approved with changes' )) {
                      if(approvedWithChangesCaseSummaryMap.containsKey(objCaseSummary.Case__r.CaseNumber) 
                        && approvedWithChangesCaseSummaryMap.get(objCaseSummary.Case__r.CaseNumber) != NULL
                        && approvedWithChangesCaseSummaryMap.get(objCaseSummary.Case__r.CaseNumber).size() > 0){
                            List<CaseSummaryRequest__c> caseSummaryLst = approvedWithChangesCaseSummaryMap.get(objCaseSummary.Case__r.CaseNumber);
                            caseSummaryLst.add(objCaseSummary);
                            approvedWithChangesCaseSummaryMap.put(objCaseSummary.Case__r.CaseNumber,caseSummaryLst);
                      }
                      else {
                         List<CaseSummaryRequest__c> caseSummaryLst = new List<CaseSummaryRequest__c>();
                         caseSummaryLst.add(objCaseSummary);                 
                         approvedWithChangesCaseSummaryMap.put(objCaseSummary.Case__r.CaseNumber,caseSummaryLst);
                      }
                //approvedCaseSummaryList.add(objCaseSummary.Case__r.CaseNumber);
            }
            
            else if( (objCaseSummary.Manager_Id__c == Userinfo.getuserid() 
               && objCaseSummary.Manager_Approval_Status__c == 'Rejected' )
            || (objCaseSummary.HOD_Id__c == Userinfo.getuserid() 
               && objCaseSummary.HOD_Approval_Status__c == 'Rejected' )
            || (objCaseSummary.Director_Id__c == Userinfo.getuserid() 
               && objCaseSummary.Director_Approval_Status__c == 'Rejected' )
            || (objCaseSummary.Committee_Id__c == Userinfo.getuserid() 
               && objCaseSummary.Committee_Approval_Status__c == 'Rejected' )) {
                //rejectedCaseSummaryList.add(objCaseSummary);
                    
                     if(rejectedCaseSummaryMap.containsKey(objCaseSummary.Case__r.CaseNumber) 
                        && rejectedCaseSummaryMap.get(objCaseSummary.Case__r.CaseNumber) != NULL
                        && rejectedCaseSummaryMap.get(objCaseSummary.Case__r.CaseNumber).size() > 0){
                            List<CaseSummaryRequest__c> caseSummaryLst = rejectedCaseSummaryMap.get(objCaseSummary.Case__r.CaseNumber);
                            caseSummaryLst.add(objCaseSummary);
                            rejectedCaseSummaryMap.put(objCaseSummary.Case__r.CaseNumber,caseSummaryLst);
                      }
                      else {
                         List<CaseSummaryRequest__c> caseSummaryLst = new List<CaseSummaryRequest__c>();
                         caseSummaryLst.add(objCaseSummary);                 
                         rejectedCaseSummaryMap.put(objCaseSummary.Case__r.CaseNumber,caseSummaryLst);
                      }
            }
            else if( (objCaseSummary.Manager_Id__c == Userinfo.getuserid() 
               && objCaseSummary.Manager_Approval_Status__c == 'Need More Info' )
              || (objCaseSummary.HOD_Id__c == Userinfo.getuserid() 
               && objCaseSummary.HOD_Approval_Status__c == 'Need More Info' )
               || (objCaseSummary.Director_Id__c == Userinfo.getuserid() 
               && objCaseSummary.Director_Approval_Status__c == 'Need More Info' )
               || (objCaseSummary.Committee_Id__c == Userinfo.getuserid() 
               && objCaseSummary.Committee_Approval_Status__c == 'Need More Info')) {
                //needMoreInfoCaseSummaryList.add(objCaseSummary);
                    if(needMoreInfoCaseSummaryMap.containsKey(objCaseSummary.Case__r.CaseNumber) 
                        && needMoreInfoCaseSummaryMap.get(objCaseSummary.Case__r.CaseNumber) != NULL
                        && needMoreInfoCaseSummaryMap.get(objCaseSummary.Case__r.CaseNumber).size() > 0){
                            List<CaseSummaryRequest__c> caseSummaryLst = needMoreInfoCaseSummaryMap.get(objCaseSummary.Case__r.CaseNumber);
                            caseSummaryLst.add(objCaseSummary);
                            needMoreInfoCaseSummaryMap.put(objCaseSummary.Case__r.CaseNumber,caseSummaryLst);
                      }
                      else {
                         List<CaseSummaryRequest__c> caseSummaryLst = new List<CaseSummaryRequest__c>();
                         caseSummaryLst.add(objCaseSummary);                 
                         needMoreInfoCaseSummaryMap.put(objCaseSummary.Case__r.CaseNumber,caseSummaryLst);
                      }
            }
        }
        
        
        objCsrListView.approvedCount = approvedCaseSummaryMap.keyset().size();
        objCsrListView.approvedCSRCount = approvedCaseSummaryMap.values().size();
        objCsrListView.mapOfCaseNumberToApprovedList = approvedCaseSummaryMap;
        
        objCsrListView.approvedWithChangesCount = approvedWithChangesCaseSummaryMap.keyset().size();
        objCsrListView.approvedWithChangesCSRCount = approvedWithChangesCaseSummaryMap.values().size();        
        objCsrListView.mapOfCaseNumberToApprovedWithChangesList = approvedWithChangesCaseSummaryMap;
        
        objCsrListView.rejectedCount = rejectedCaseSummaryMap.keyset().size();
        objCsrListView.rejectedCSRCount = rejectedCaseSummaryMap.values().size();            
        objCsrListView.mapOfCaseNumberToRejectedList = rejectedCaseSummaryMap;
        objCsrListView.needMoreInfoCount = needMoreInfoCaseSummaryMap.keyset().size();
        objCsrListView.needMoreInfoCSRCount = needMoreInfoCaseSummaryMap.values().size();        
        objCsrListView.mapOfCaseNumberToNeedMoreInfoList = needMoreInfoCaseSummaryMap;
        objCsrListView.totalSRCount = caseList.size() + approvedCaseSummaryMap.keyset().size() + approvedWithChangesCaseSummaryMap.keyset().size() + rejectedCaseSummaryMap.keyset().size() + needMoreInfoCaseSummaryMap.keyset().size();
        System.debug('needMoreInfoCaseSummaryMap-->'+needMoreInfoCaseSummaryMap.keyset().size());
        System.debug('rejectedCaseSummaryMap-->'+rejectedCaseSummaryMap.keyset().size());
        System.debug('approvedCaseSummaryMap-->'+approvedCaseSummaryMap.keyset().size());
        System.debug('caseNumbers-->'+caseNumbers);
        System.debug('objCsrListView-->'+objCsrListView);
        }
        
    }*/
  /*  
public class CSRListViewWrapper  {
    
   public Integer approvedCount {get;set;}
   public Integer approvedCSRCount {get;set;}
   public Map<String,List<CaseSummaryRequest__c>> mapOfCaseNumberToApprovedList {get;set;}
   public Integer approvedWithChangesCount {get;set;}
   public Integer approvedWithChangesCSRCount {get;set;}
   public Map<String,List<CaseSummaryRequest__c>> mapOfCaseNumberToApprovedWithChangesList {get;set;}
   public Integer rejectedCount {get;set;}
   public Integer rejectedCSRCount {get;set;}
   public Map<String,List<CaseSummaryRequest__c>> mapOfCaseNumberToRejectedList {get;set;}
   public Integer needMoreInfoCount {get;set;}
   public Integer needMoreInfoCSRCount {get;set;}
   public Map<String,List<CaseSummaryRequest__c>> mapOfCaseNumberToNeedMoreInfoList {get;set;}
   public Integer totalSRCount {get;set;}
   
   public CSRListViewWrapper (){
       this.approvedCount = 0;
       this.approvedCSRCount = 0;
       this.mapOfCaseNumberToApprovedList = new Map<String,List<CaseSummaryRequest__c>>();
       this.approvedWithChangesCount = 0;
       this.approvedWithChangesCSRCount = 0;
       this.mapOfCaseNumberToApprovedWithChangesList = new Map<String,List<CaseSummaryRequest__c>>();
       this.rejectedCount = 0;
       this.rejectedCSRCount = 0;
       this.mapOfCaseNumberToRejectedList = new Map<String,List<CaseSummaryRequest__c>>();
       this.needMoreInfoCount = 0;
       this.needMoreInfoCSRCount = 0;
       this.mapOfCaseNumberToNeedMoreInfoList = new Map<String,List<CaseSummaryRequest__c>>();
       this.totalSRCount = 0;
       
   }
       
}*/
}