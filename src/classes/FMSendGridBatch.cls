public without sharing class FMSendGridBatch
        implements Database.batchable<SendGridIterable.SendGridEmail>, Database.AllowsCallouts {

    List<SendGridIterable.SendGridEmail> lstWrapper;

    public FMSendGridBatch(List<SendGridIterable.SendGridEmail> lstWrapper) {
        this.lstWrapper = lstWrapper;
    }

    public Iterable<SendGridIterable.SendGridEmail> start(Database.BatchableContext BC) {
        // List<SendGridIterable.SendGridEmail> lst = new List<SendGridIterable.SendGridEmail>();
        return new SendGridIterable(lstWrapper);
    }

    public void execute(Database.BatchableContext bc, List<SendGridIterable.SendGridEmail> scope) {

        //System.debug('===scope===' + scope);

        List<EmailMessage> lstEmail = new List<EmailMessage>();
        List<Error_Log__c> lstErrors = new List<Error_Log__c>();

        try {
            for(SendGridIterable.SendGridEmail objWrapper : scope) {
                SendGridEmailService.SendGridResponse response = SendGridEmailService.sendEmailService(
                    objWrapper.toAddress, objWrapper.toName, objWrapper.ccAddress, objWrapper.ccName,
                    objWrapper.bccAddress, objWrapper.bccName, objWrapper.subject, objWrapper.substitutions,
                    objWrapper.fromAddress, objWrapper.fromName, objWrapper.replyToAddress, objWrapper.replyToName,
                    objWrapper.contentType, objWrapper.contentValue, objWrapper.templateId, new List<Attachment>()
                );
                //System.debug('===response===' + response);
                if (response.ResponseStatus == 'Accepted') {
                    EmailMessage mail = new EmailMessage();
                    mail.Subject = objWrapper.subject;
                    mail.MessageDate = System.Today();
                    mail.Status = '3';
                    //mail.RelatedToId = objWrapper.relatedId;//Put FM case id
                    mail.RelatedToId = String.isBlank(objWrapper.accountId) ? NULL : objWrapper.accountId;
                    mail.ToAddress = objWrapper.toAddress;
                    mail.FromAddress = objWrapper.fromAddress;
                    mail.HtmlBody = objWrapper.contentValue;
                    //mail.TextBody = objWrapper.contentValue;
                    mail.CcAddress = objWrapper.ccAddress;
                    mail.BccAddress = objWrapper.bccAddress;
                    mail.Sent_By_Sendgrid__c = true;
                    mail.Sendgrid_Status__c = response.ResponseStatus;
                    mail.SentGrid_MessageId__c = response.messageId;
                    mail.Booking_Unit__c = objWrapper.buId == '' ? NULL : objWrapper.buId;
                    mail.Account__c = String.isBlank(objWrapper.accountId) ? NULL : objWrapper.accountId;
                    lstEmail.add(mail);
                } else {
                    Error_Log__c objError = new Error_Log__c();
                    objError.Account__c = String.isBlank(objWrapper.accountId) ? NULL : objWrapper.accountId;
                    objError.Error_Details__c = 'Request Failed from SendGrid';
                    objError.Process_Name__c = 'Generic Email';
                    lstErrors.add(objError);
                }
            }//for

            insert lstEmail;
            insert lstErrors;

        } catch(System.Exception excp) {
            //System.debug('=====excp===='+excp.getMessage());
            //System.debug('=====excp===='+excp);
        }
    }//execute

    public void finish(Database.BatchableContext bc) {}
}