/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SendEmailtoLeaseTeamLHOTest {

    static testMethod void myUnitTest() {
        
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        list<Id> lstCaseId = new list<Id>();
        Case Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Type = 'Lease_Handover';
        Cas.Booking_Unit__c = BU.Id;
        insert Cas;
        System.assert(Cas != null);
        
        list<SR_Attachments__c> lstAttach = new list<SR_Attachments__c>();
        SR_Attachments__c objAttach = new SR_Attachments__c ();
        objAttach.Name = 'Doc 1';
        objAttach.Attachment_URL__c = 'www.google.com';
        objAttach.Case__c = Cas.Id;
        lstAttach.add(objAttach);
        
        SR_Attachments__c objAttach2 = new SR_Attachments__c ();
        objAttach2.Name = 'Doc 2';
        objAttach2.Attachment_URL__c = 'www.google.com';
        objAttach2.Case__c = Cas.Id;
        lstAttach.add(objAttach2);
        
        insert lstAttach;
        
        test.startTest();            
         	lstCaseId.add(Cas.Id);
            SendEmailtoLeaseTeamLHO.invokeApex(lstCaseId);
        test.stopTest();
        
    }
}