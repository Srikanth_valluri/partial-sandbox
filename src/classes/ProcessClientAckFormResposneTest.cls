@isTest
public class ProcessClientAckFormResposneTest {
    public ProcessClientAckFormResposneTest() {
    }
    
    static testMethod void testAttachments()
    {
        TestUtility utility = new TestUtility();
        List<Case> caseList = new List<Case>();
        list<id> buId = new list<id>();
        
        
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;
        
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
            objBookingUnit.Okay_to_release_keys__c = true;
            objBookingUnit.Keys_Released__c = true;
        }
        insert bookingUnitList;
        
        for (Booking_Unit__c objBu : bookingUnitList) {
            buId.add(objBu.id);
        }
        
        Case obj=new Case();
        
        obj.Status='New';
        obj.Origin='Call';
        obj.Priority='Medium';
        obj.Booking_Unit__c = buId[0];
        obj.RecordTypeId = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        insert obj;     
        
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.EHO_Email_Template__c = 'EHO';
        loc.LHO_Email_Template__c = 'EHO';
        loc.HO_Email_Template__c = 'EHO';
        loc.EHO_Notification_1_Email_Template__c = 'NHO';
        loc.EHO_Notification_2_Email_Template__c = 'NHO';
        loc.Notification_1_Email_Template__c = 'NHO';
        loc.Notification_2_Email_Template__c = 'NHO';
        loc.Plot_HO_Email_Template__c = 'NHO';
        loc.Plot_EHO_Email_Template__c = 'NHO';
        loc.Chinese_Email_Template_Name__c = 'EHO';
        insert loc; 
        
        List<Attachment> attachList = new List<Attachment>();
        for(Integer i=0;i<1;i++) {
            Attachment attobj=new Attachment();
            
            attobj.Name='Client Acknowledgment Form.pdf';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attobj.body=bodyBlob;
            attobj.ContentType = 'application/pdf';
            
            attobj.parentId=buId[0];
            attachList.add(attobj);
        }//end for
        insert attachList;
        
        System.debug('attachList' + attachList);
        System.assertEquals(1, attachList.size());
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        Test.stopTest();
        
        ProcessClientAckFormResposne controller = new ProcessClientAckFormResposne();
        ProcessClientAckFormResposne.createSrAttach(attachList);
    }
    
    static testMethod void testAttachments2()
    {
        TestUtility utility = new TestUtility();
        List<Case> caseList = new List<Case>();
        list<id> buId = new list<id>();
        
        
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;
        
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
            objBookingUnit.Okay_to_release_keys__c = true;
            objBookingUnit.Keys_Released__c = true;
        }
        insert bookingUnitList;
        
        for (Booking_Unit__c objBu : bookingUnitList) {
            buId.add(objBu.id);
        }
        
        Case obj=new Case();
        
        obj.Status='New';
        obj.Origin='Call';
        obj.Priority='Medium';
        obj.Booking_Unit__c = buId[0];
        obj.RecordTypeId = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        insert obj;     
        
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.EHO_Email_Template__c = 'EHO';
        loc.LHO_Email_Template__c = 'EHO';
        loc.HO_Email_Template__c = 'EHO';
        loc.EHO_Notification_1_Email_Template__c = 'NHO';
        loc.EHO_Notification_2_Email_Template__c = 'NHO';
        loc.Notification_1_Email_Template__c = 'NHO';
        loc.Notification_2_Email_Template__c = 'NHO';
        loc.Plot_HO_Email_Template__c = 'NHO';
        loc.Plot_EHO_Email_Template__c = 'NHO';
        loc.Chinese_Email_Template_Name__c = 'EHO';
        insert loc; 
        
        List<Attachment> attachList = new List<Attachment>();
        for(Integer i=0;i<1;i++) {
            Attachment attobj=new Attachment();
            
            attobj.Name='Notice of Completion.pdf';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attobj.body=bodyBlob;
            attobj.ContentType = 'application/pdf';
            
            attobj.parentId=buId[0];
            attachList.add(attobj);
        }//end for
        insert attachList;
        
        System.debug('attachList' + attachList);
        System.assertEquals(1, attachList.size());
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        Test.stopTest();
        
        ProcessClientAckFormResposne controller = new ProcessClientAckFormResposne();
        ProcessClientAckFormResposne.createSrAttach(attachList);
    }
    
    static testMethod void testAttachments3()
    {
        TestUtility utility = new TestUtility();
        List<Case> caseList = new List<Case>();
        list<id> buId = new list<id>();
        
        
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;
        
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
            objBookingUnit.Okay_to_release_keys__c = true;
            objBookingUnit.Keys_Released__c = true;
        }
        insert bookingUnitList;
        
        for (Booking_Unit__c objBu : bookingUnitList) {
            buId.add(objBu.id);
        }
        
        Case obj=new Case();
        
        obj.Status='New';
        obj.Origin='Call';
        obj.Priority='Medium';
        obj.Booking_Unit__c = buId[0];
        obj.RecordTypeId = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        insert obj;     
        
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.EHO_Email_Template__c = 'EHO';
        loc.LHO_Email_Template__c = 'EHO';
        loc.HO_Email_Template__c = 'EHO';
        loc.EHO_Notification_1_Email_Template__c = 'NHO';
        loc.EHO_Notification_2_Email_Template__c = 'NHO';
        loc.Notification_1_Email_Template__c = 'NHO';
        loc.Notification_2_Email_Template__c = 'NHO';
        loc.Plot_HO_Email_Template__c = 'NHO';
        loc.Plot_EHO_Email_Template__c = 'NHO';
        loc.Chinese_Email_Template_Name__c = 'EHO';
        insert loc; 
        
        List<Attachment> attachList = new List<Attachment>();
        for(Integer i=0;i<1;i++) {
            Attachment attobj=new Attachment();
            
            attobj.Name='CheckList.pdf';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attobj.body=bodyBlob;
            attobj.ContentType = 'application/pdf';
            
            attobj.parentId=buId[0];
            attachList.add(attobj);
        }//end for
        insert attachList;
        
        System.debug('attachList' + attachList);
        System.assertEquals(1, attachList.size());
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        Test.stopTest();
        
        ProcessClientAckFormResposne controller = new ProcessClientAckFormResposne();
        ProcessClientAckFormResposne.createSrAttach(attachList);
    }
    
    @isTest static void testAttachments4() {
        TestUtility utility = new TestUtility();
        List<Case> caseList = new List<Case>();
        list<id> buId = new list<id>();
        
        
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;
        
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
            objBookingUnit.Okay_to_release_keys__c = true;
            objBookingUnit.Keys_Released__c = true;
        }
        insert bookingUnitList;
        
        for (Booking_Unit__c objBu : bookingUnitList) {
            buId.add(objBu.id);
        }
        
        Case obj=new Case();
        
        obj.Status='New';
        obj.Origin='Call';
        obj.Priority='Medium';
        obj.Booking_Unit__c = buId[0];
        obj.RecordTypeId = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        insert obj; 
        
        
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
            objBookingUnit.HO_EHO_PHO_CaseId__c = obj.Id; 
        }    
        
        update bookingUnitList;
        
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.EHO_Email_Template__c = 'EHO';
        loc.LHO_Email_Template__c = 'EHO';
        loc.HO_Email_Template__c = 'EHO';
        loc.EHO_Notification_1_Email_Template__c = 'NHO';
        loc.EHO_Notification_2_Email_Template__c = 'NHO';
        loc.Notification_1_Email_Template__c = 'NHO';
        loc.Notification_2_Email_Template__c = 'NHO';
        loc.Plot_HO_Email_Template__c = 'NHO';
        loc.Plot_EHO_Email_Template__c = 'NHO';
        loc.Chinese_Email_Template_Name__c = 'EHO';
        insert loc; 
        
        List<Attachment> attachList = new List<Attachment>();
        for(Integer i=0;i<1;i++) {
            Attachment attobj=new Attachment();
            
            attobj.Name='CheckList.pdf';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attobj.body=bodyBlob;
            attobj.ContentType = 'application/pdf';
            
            attobj.parentId=buId[0];
            attachList.add(attobj);
        }//end for
        insert attachList;
        
        System.debug('attachList' + attachList);
        System.assertEquals(1, attachList.size());
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        Test.stopTest();
        
        ProcessClientAckFormResposne controller = new ProcessClientAckFormResposne();
        ProcessClientAckFormResposne.createSrAttach(attachList);
    }
    
    @isTest static void testAttachments5() {
        TestUtility utility = new TestUtility();
        List<Case> caseList = new List<Case>();
        list<id> buId = new list<id>();
        
        
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;
        
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
            objBookingUnit.Okay_to_release_keys__c = true;
            objBookingUnit.Keys_Released__c = true;
        }
        insert bookingUnitList;
        
        for (Booking_Unit__c objBu : bookingUnitList) {
            buId.add(objBu.id);
        }
        
        Case obj=new Case();
        
        obj.Status='New';
        obj.Origin='Call';
        obj.Priority='Medium';
        obj.Booking_Unit__c = buId[0];
        obj.RecordTypeId = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        insert obj; 
        
        
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
            objBookingUnit.HO_EHO_PHO_CaseId__c = obj.Id; 
        }    
        
        update bookingUnitList;
        
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.EHO_Email_Template__c = 'EHO';
        loc.LHO_Email_Template__c = 'EHO';
        loc.HO_Email_Template__c = 'EHO';
        loc.EHO_Notification_1_Email_Template__c = 'NHO';
        loc.EHO_Notification_2_Email_Template__c = 'NHO';
        loc.Notification_1_Email_Template__c = 'NHO';
        loc.Notification_2_Email_Template__c = 'NHO';
        loc.Plot_HO_Email_Template__c = 'NHO';
        loc.Plot_EHO_Email_Template__c = 'NHO';
        loc.Chinese_Email_Template_Name__c = 'EHO';
        loc.Project_Name_for_HO_notice__c = 'test';
        insert loc; 
        
        List<Attachment> attachList = new List<Attachment>();
        for(Integer i=0;i<1;i++) {
            Attachment attobj=new Attachment();
            
            attobj.Name='Courier Slip.pdf';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attobj.body=bodyBlob;
            attobj.ContentType = 'application/pdf';
            
            attobj.parentId=buId[0];
            attachList.add(attobj);
        }//end for
        insert attachList;
        
        System.debug('attachList' + attachList);
        System.assertEquals(1, attachList.size());
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        Test.stopTest();
        
        ProcessClientAckFormResposne controller = new ProcessClientAckFormResposne();
        ProcessClientAckFormResposne.createSrAttach(attachList);
    }
    
}