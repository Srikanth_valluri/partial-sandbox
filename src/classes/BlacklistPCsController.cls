/***********************************************************************************
 * Controller Class : BlacklistPCsControllerNew
 * Created By : Tejashree Chavan 
 -----------------------------------------------------------------------------------
 * Description : This is a controller class for BlacklistPCsNew
 *
 * Test Data : BlacklistPCsControllerNewTest
 -----------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE                DETAIL FEATURES
   1.0         Tejashree Chavan     12/12/2017  Initial Development
   2.0         Dipika Rajput        06/03/2018  Changes To Blacklisting PC Tab(#258)
   3.0         Craig Lobo           31/10/2018  Updated the code to Blacklist TSA Users
   4.0         Bhanu Gupta          29/11/2018  Updated the code to add Custom setting
 ***************************************************************************************************/
public without sharing class BlacklistPCsController{
    
    public final String QUERY_STRING = ' SELECT '
                                     + ' Name, UserName, Languages_Known__c, Profile.Name, ' 
                                     + ' BlackList_End_Date__c, BlackList_Start_Date__c, '
                                     + ' Blacklist_Reason__c ' 
                                     + ' FROM USER '
                                     + ' WHERE IsActive = true ';

    /*
    public final String queryString  = ' SELECT '
                                     + ' Name, UserName, Languages_Known__c, Profile.Name ' 
                                     + ' FROM USER '
                                     + ' WHERE IsActive = true '
                                     + ' AND (Profile.Name  = \'Property Consultant\' '
                                     + ' OR Profile.Name  = \'Telesales Team\' '
                                     + ' OR Profile.Name  = \'Telesales Team TL\' ) '
                                     + ' AND Is_Blacklisted__c = false ';

    public final String queryStringNew  = ' SELECT '
                                        + ' Name, UserName, Languages_Known__c, Profile.Name, '
                                        + ' BlackList_End_Date__c, BlackList_Start_Date__c, '
                                        + ' Blacklist_Reason__c ' 
                                        + ' FROM USER '
                                        + ' WHERE IsActive = true '
                                        + ' AND (Profile.Name  = \'Property Consultant\' '
                                        + ' OR Profile.Name  = \'Telesales Team\' '
                                        + ' OR Profile.Name  = \'Telesales Team TL\' ) '
                                        + ' AND Is_Blacklisted__c = true ';
*/
    public String queryStringNew = '';
    public String queryString = '';

    private List<User> userList; 
    public String currentTab                                            {get; set;}
    public String errorMessage                                          {get; set;}
    public String reason                                                {get; set;}
    public String selDOS                                                {get; set;} // Selected Director of Sales
    public String selBlacklistedDOS                                     {get; set;}
    public boolean flag                                                 {get; set;}
    public List<USER> myPCsList                                         {set; get;}
    public Integer noOfRecToDisplay                                     {get; set;}
    public Integer noOfRecToDisplayForUnblackListUsers                  {get; set;} //Added for Unblacklist Query
    public Id startId                                                   {get; set;}
    public Id endId                                                     {get; set;}
    public Id prevStartId                                               {get; set;}
    public String startDate                                             {get; set;}
    public String endDate                                               {get; set;}
    public CampaignManage_Helper manageHelper                           {get; set;} 
    public String selBlacklistedHOS                                     {get; set;} 
    public String selBlacklistedUserName                                {get; set;}
    public String selBlacklistedTSAUserName                             {get; set;} 
    public String selHOS                                                {get; set;} 
    public String selUserName                                           {get; set;}
    public String selTSAUserName                                        {get; set;}
    public String selAccOwner                                           {get; set;}
    public List <selectPCWrapper> selectPCWrapperList                   {get; set;}
    public String msgType                                               {get; set;}
    public String isDisplay                                             {get; set;}
    public String message                                               {get; set;}
    public String messageUnblack                                        {get; set;}
    public List <selectPCWrapperBlacklisted> selectPCWrapperBlackList   {get; set;}
    private String blackListQuery;
    private String unblackListQuery;
    public String displayTSA                                            {get; set;}
    public String displayPC                                             {get; set;}
    public List<String> blacklistProfilesLst;
    public String conStringList                                         {get; set;}
    public String qString                                           {get; set;}
    public List<Blacklist_PCs_Profiles__c> allProfilesLst;
    public User currentUser;

    /************************************************************************************************
    * @Description : This is Constructor method                                                     *
    * @Params      :                                                                                *
    * @Return      :                                                                                *
    *************************************************************************************************/
    public BlacklistPCsController() {
        allProfilesLst = new List<Blacklist_PCs_Profiles__c>();
        blacklistProfilesLst = new List<String>();
        currentUser = [SELECT Id, Profile.Name 
                              FROM User 
                             WHERE Id = :UserInfo.getUserId() 
                             LIMIT 1];
        //v4.0: Bhanu Gupta Updated to get profiles from custom settings.
        Map<String,Blacklist_PCs_Profiles__c> blacklistProfiles = Blacklist_PCs_Profiles__c.getAll();
        system.debug('---blacklistProfiles----'+blacklistProfiles);
        if(blacklistProfiles != null){
            allProfilesLst = blacklistProfiles.values();
        }
        if(allProfilesLst !=null && !allProfilesLst.isEmpty() ){
            for(Blacklist_PCs_Profiles__c custmObj : allProfilesLst){
                if(currentUser.Profile.Name == 'Telesales Team Manager' 
                    && custmObj.Department__c == 'Telesales'){
                    blacklistProfilesLst.add(custmObj.Name); 
                }else if((currentUser.Profile.Name == 'Director of Sales' 
                    || currentUser.Profile.Name == 'Head of Sales')
                            && custmObj.Department__c == 'Property Consultant'){
                    blacklistProfilesLst.add(custmObj.Name); 
                }
                /*else if(custmObj.Department__c == 'Property Consultant'){
                    blacklistProfilesLst.add(custmObj.Name); 
                }*/
            }
        }
        system.debug('---blacklistProfilesLst----'+blacklistProfilesLst);
        
        conStringList = getIdListForQuery(blacklistProfilesLst,'page');
        if((blacklistProfilesLst).isEmpty()){
            /*qString = '( Profile.Name ='+ '\'\''+')';*/
            qString = '';
        }else{
            qString = '( Profile.Name IN '+conStringList+' ) AND ';
        }
        system.debug('---queryString----'+qString); 
        if (message == '' || message == null) {
            isDisplay = 'hide';
        }
        msgType = '';
        message = '';
        messageUnblack = '';
        noOfRecToDisplay = 5;
        noOfRecToDisplayForUnblackListUsers = 50;
        currentTab = 'tab-default-1';
        flag = false;
        selDOS = '';
        selHOS = '';
        selUserName = '';
        selAccOwner = '';
        userList = new List<User>();
        currentUserQueryString();
        //selectPCWrapperList.clear();
        selectPCWrapperList = new List <selectPCWrapper>();
        selectPCWrapperBlackList = new List <selectPCWrapperBlacklisted>();
        queryString = blackListQuery;
        queryStringNew = unblackListQuery;
        //blackListQuery = queryString;
        //unblackListQuery = queryStringNew;
        firstBtn();
        FirstbtnBlacklisted();
        system.debug('flag cons '+flag);
    }

    private static string getIdListForQuery(List<String> lstId, String pageString) {
        String ids = '';
        for (String recordId : lstId) {
            ids += '\'' + recordId + '\'' + ', ';
        }
        System.debug('>>>>ids'+ids);
        if(pageString == 'page'){
            ids = ids.replace('\'', '\\\'');
            System.debug('>>>>ids'+ids);
            return '(' + ids.removeEnd(', ') +')';
        }else{
            return '(' + ids.removeEnd(', ') +')';
        }
    }

    public void currentUserQueryString() {
        /*User currentUser = [SELECT Id, Profile.Name 
                              FROM User 
                             WHERE Id = :UserInfo.getUserId() 
                             LIMIT 1];*/
        System.debug('>>>>currentUser'+currentUser);
        String query = QUERY_STRING;
        if (currentUser.Profile.Name == 'Telesales Team Manager') {
            displayPC = 'none';
            /*query = query + ' AND ( Profile.Name  = \'Telesales Team\' '
                  + ' OR Profile.Name  = \'Telesales Team TL\' ) '
                  + ' AND ManagerId = \'' + currentUser.Id + '\' ';*/
            query = query + ' AND Profile.Name IN '+ getIdListForQuery(blacklistProfilesLst,'')
                  + ' AND ManagerId = \'' + currentUser.Id + '\' ';
        } else if (currentUser.Profile.Name == 'Director of Sales' 
                    || currentUser.Profile.Name == 'Head of Sales'){
            displayTSA = 'none';
            /*query = query + 'AND Profile.Name  = \'Property Consultant%\' ';*/
            
            query = query + 'AND Profile.Name IN '+ getIdListForQuery(blacklistProfilesLst,'');
        }
        blackListQuery = query + ' AND Is_Blacklisted__c = false ';
        unblackListQuery = query + ' AND Is_Blacklisted__c = true ';
    }

    public void fetchPCs(){
        getTotalRecords();
        firstBtn();
    }

    public void fetchPCsBlackListed(){
        getTotalRecordsBlackListed();
        FirstbtnBlacklisted();
    }

    public void getTotalRecords(){
        String whereCondition = getWhereCondition();
        system.debug('--------blackListQuery-----'+blackListQuery);
        blackListQuery = queryString + whereCondition;
        system.debug('--------blackListQuery-----'+blackListQuery);
    }
    public void getTotalRecordsBlackListed(){
        String whereCondition = getWhereConditionBlcklisted();
         system.debug('>>>>>whereCondition>>>>>'+whereCondition);
        unblackListQuery = queryStringNew + whereCondition;
        system.debug('>>>>>unblackListQuery>>>>>'+unblackListQuery);
        FirstbtnBlacklisted();
    }
     public String getWhereConditionBlcklisted(){
        String whereCondition = '';
        system.debug('>>>>>selUserName>>>>>'+selBlacklistedUserName);
        if(String.isNotBlank(selBlacklistedDOS)) {
            whereCondition += ' AND DOS_Name__c =\'' + selBlacklistedDOS + '\'';
        }
        if(String.isNotBlank(selBlacklistedHOS)) {
            whereCondition += ' AND HOS_Name__c =\'' + selBlacklistedHOS + '\'';
        }
        if(String.isNotBlank(selBlacklistedUserName)) {
            whereCondition += ' AND Name = \'' +selBlacklistedUserName+'\'';
        } else {
            whereCondition += ' AND Name = \'' +selBlacklistedTSAUserName+'\'';
        }

         return whereCondition;
     }
    /************************************************************************************************
    * @Description : This method will return the String which is where condition for query          *
    * @Params      : String                                                                         *
    * @Return      :String                                                          *
    *************************************************************************************************/
    public String getWhereCondition(){
        String whereCondition = '';
        system.debug('>>>>>selUserName>>>>>'+selUserName);
        if(String.isNotBlank(selDOS)) {
            whereCondition += ' AND DOS_Name__c =\'' + selDOS + '\'';
        }
        if(String.isNotBlank(selHOS)) {
            whereCondition += ' AND HOS_Name__c =\'' + selHOS + '\'';
        }
        if(String.isNotBlank(selUserName)) {
            whereCondition += ' AND Name = \'' +selUserName+'\'';
        } else {
            whereCondition += ' AND Name = \'' +selTSAUserName+'\'';
        }
        
        system.debug('----whereCondition----'+whereCondition);
         return whereCondition;
     }

    public PageReference blackListPCs(){
        flag = true;
        system.debug('>>>>startDate>>>>'+startDate);
        try{
            isDisplay = 'hide';
            if((String.isNotBlank(startDate)) && (String.isNotBlank(endDate)) && (String.isNotBlank(reason)) ){
                userList = new List<User>();
                for (selectPCWrapper pcLoopObj : selectPCWrapperList) {
                    String s2 = startDate.mid(0, 2);
                    String s3 = startDate.mid(3, 2);
                    String s4 = startDate.mid(6, 4);
                    String startfinal = s4 +'-' + s2 + '-' + s3;
                    String s5 = endDate.mid(0, 2);
                    String s6 = endDate.mid(3, 2);
                    String s7 = endDate.mid(6, 4);
                    String endfinal = s7 +'-' + s5 + '-' + s6; 
                    DateTime dT = System.now();
                    Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
                    Date blackListStartDate = Date.valueOf(startfinal);
                    Date blackListEndDate = Date.valueOf(endfinal);
                    Date myBlackListStartDate = date.newinstance(blackListStartDate.year(), blackListStartDate.month(),blackListStartDate.day());
                    Date myBlackListEndDate = date.newinstance(blackListEndDate.year(), blackListEndDate.month(),blackListEndDate.day());
                    system.debug('>>>>myBlackListStartDate>>>>'+myBlackListStartDate);
                    system.debug('>>>>myBlackListEndDate>>>>'+myBlackListEndDate);
                    system.debug('>>>>myDate>>>>'+myDate);
                    if(myBlackListStartDate< myDate || myBlackListEndDate< myDate || myBlackListEndDate<myBlackListStartDate){
                         msgType = 'error';
                         isDisplay = 'show';
                         message = 'Start Date should be current or in future and End date should be in Future.';
                    }else{
                        if (pcLoopObj.isSelected == true) {
                            User userRec = pcLoopObj.userObj;
                            userRec.BlackList_Start_Date__c = Date.valueOf(startfinal);
                            userRec.BlackList_End_Date__c = Date.valueOf(endfinal);
                            userRec.Blacklist_Reason__c = reason;
                            userList.add(userRec);
                            system.debug('-------userRec---'+userRec);
                            system.debug('-------userRec.Extension_Unique__c---'+userRec.Extension_Unique__c);
                        }
                    }
                }
                if (!userList.isEmpty()) {
                        update userList;
                        msgType = 'success';
                        isDisplay = 'show';
                        message = 'Users blacklisted successfully.';
                        Firstbtn();
                    }else{
                        msgType = 'error';
                         isDisplay = 'show';
                         message = 'Please select atlest One User.';
                    }
            } else{
                //errorMessage = 'Please Fill all required fields.';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please Fill all required fields.'));
                if(String.isBlank(startDate) || String.isBlank(endDate)){
                     msgType = 'error';
                     isDisplay = 'show';
                     message = 'Please add the Blacklist Start Date and End Date.';
                   // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please add the Blacklist Start Date and End Date'));
                }else{
                    if(String.isBlank(reason)){
                        msgType = 'error';
                        isDisplay = 'show';
                        message = 'Please add blackList Reason.';
                       // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please add blackList Reason'));
                    }
                }
            }
            system.debug('page reference called');
            system.debug('selectPCWrapperList '+selectPCWrapperList);
        }catch (Exception e) {
            system.debug('exp '+e.getStackTraceString());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }
        return null;
    }
    
     public PageReference unBlackListPCs(){
        flag = true;
        try{
                userList = new List<User>();
                for (selectPCWrapperBlacklisted pcLoopObj : selectPCWrapperBlackList) { 
                    if (pcLoopObj.isSelected == true) {
                        User userRec = pcLoopObj.userObj;
                        userRec.BlackList_Start_Date__c = null;
                        userRec.BlackList_End_Date__c = null;
                        userRec.Blacklist_Reason__c = '';
                        userList.add(userRec);
                    }
                }
                if (!userList.isEmpty()) {
                    update userList;
                    msgType = 'success';
                    isDisplay = 'show';
                    messageUnblack = 'Users Unblacklisted successfully.';
                    FirstbtnBlacklisted();
                }else{
                        msgType = 'error';
                         isDisplay = 'show';
                         messageUnblack = 'Please select atlest One User.';
                }
        }catch (Exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }
        return null;
    }
    /************************************************************************************************
    * @Description : This method will return the list of selectPCWrapper                           *
    * @Params      : String                                                                         *
    * @Return      :List<selectPCWrapper>                                                          *
    *************************************************************************************************/
    public List<selectPCWrapper> getAllPCs(String pQueryString) {
       // selectPCWrapperList.clear();
        List<selectPCWrapper> pcWrapList = new List<selectPCWrapper>();
        if (String.isNotBlank(pQueryString)) {
            for (User userObj : DataBase.Query(pQueryString)) { 
                selectPCWrapper userWrap = new selectPCWrapper(userObj.Id, userObj, false);
                pcWrapList.add(userWrap);
            }
        }

        return pcWrapList;
    }
     public List<selectPCWrapperBlacklisted> getAllBlacklistedPCs(String pQueryString) {
       // selectPCWrapperList.clear();
        List<selectPCWrapperBlacklisted> pcWrapList = new List<selectPCWrapperBlacklisted>();
        if (String.isNotBlank(pQueryString)) {
            for (User userObj : DataBase.Query(pQueryString)) { 
                selectPCWrapperBlacklisted userWrap = new selectPCWrapperBlacklisted(userObj.Id, userObj, false);
                pcWrapList.add(userWrap);
            }
        }
        return pcWrapList;
    }

    /************************************************************************************************
    * @Description : this method will set hte offsetVal to display first page records               *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void Firstbtn(){
        selectPCWrapperList.clear();
        selectPCWrapperList = new List <selectPCWrapper>();
        String pcQueryString   = blackListQuery 
                                + ' ORDER BY Id Asc LIMIT ' 
                                + noOfRecToDisplay;
        system.debug('-----pcQueryString---'+pcQueryString);
        selectPCWrapperList = getAllPCs(pcQueryString);
        system.debug('---selectPCWrapperList---'+selectPCWrapperList);
        if(selectPCWrapperList != null && !selectPCWrapperList.isEmpty()) {
            prevStartId = selectPCWrapperList[0].recordId;
            startId = selectPCWrapperList[0].recordId;
            endId = selectPCWrapperList[selectPCWrapperList.size()-1].recordId;
        }
    }
    public void FirstbtnBlacklisted(){
        selectPCWrapperBlackList.clear();
        String pcQueryString   = unblackListQuery 
                                + ' ORDER BY Id Asc LIMIT ' 
                                + noOfRecToDisplayForUnblackListUsers;  //Added by Dipika                      
        selectPCWrapperBlackList = getAllBlacklistedPCs(pcQueryString);
        system.debug('>>>>selectPCWrapperBlackList>>>>'+selectPCWrapperBlackList);
        if(selectPCWrapperBlackList != null && !selectPCWrapperBlackList.isEmpty()) {
            prevStartId = selectPCWrapperBlackList[0].recordId;
            startId = selectPCWrapperBlackList[0].recordId;
            endId = selectPCWrapperBlackList[selectPCWrapperBlackList.size()-1].recordId;
        }
    }
    
    /************************************************************************************************
    * @Description : this method will set hte offsetVal to display previous page records            *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void prvbtn() {
        if(startId != prevStartId ) {
            String pcQueryString   = blackListQuery 
                                    + ' AND Id < \'' 
                                    + startId 
                                    + '\' AND Id >= \''
                                    + prevStartId 
                                    + '\''
                                    + ' ORDER BY Id Asc LIMIT '
                                    + noOfRecToDisplay;
            List<selectPCWrapper> tempPCWrapperList = getAllPCs(pcQueryString);
            if(tempPCWrapperList != null && !tempPCWrapperList.isEmpty()) {
                selectPCWrapperList.clear();
                selectPCWrapperList.addAll(tempPCWrapperList);
                startId = tempPCWrapperList[0].recordId;
                endId = tempPCWrapperList[tempPCWrapperList.size()-1].recordId;
                tempPCWrapperList.clear();
                String pcQueryString1  = queryString
                                        + ' AND Id < \'' 
                                        + startId 
                                        + '\' ORDER BY Id DESC LIMIT '
                                        + noOfRecToDisplay;
                List<selectPCWrapper> tempPCWrapperList1 = getAllPCs(pcQueryString1);
                if(tempPCWrapperList1 != null && tempPCWrapperList1.isEmpty() == false) {
                   prevStartId = tempPCWrapperList1[tempPCWrapperList1.size() - 1].recordId;
                }
                else {
                    prevStartId = selectPCWrapperList[0].recordId;
                }
            }
        }
    }
    public void prvbtnBlacklisted() {
        if(startId != prevStartId ) {
            String pcQueryString   = unblackListQuery 
                                    + ' AND Id < \'' 
                                    + startId 
                                    + '\' AND Id >= \''
                                    + prevStartId 
                                    + '\''
                                    + ' ORDER BY Id Asc LIMIT '
                                    + noOfRecToDisplay;
            List<selectPCWrapperBlacklisted> tempPCWrapperList = getAllBlacklistedPCs(pcQueryString);
            system.debug('>>>>selectPCWrapperBlackList>>>>'+selectPCWrapperBlackList);
            if(tempPCWrapperList != null && !tempPCWrapperList.isEmpty()) {
                selectPCWrapperBlackList.clear();
                selectPCWrapperBlackList.addAll(tempPCWrapperList);
                startId = tempPCWrapperList[0].recordId;
                endId = tempPCWrapperList[tempPCWrapperList.size()-1].recordId;
                tempPCWrapperList.clear();
                String pcQueryString1  = queryString
                                        + ' AND Id < \'' 
                                        + startId 
                                        + '\' ORDER BY Id DESC LIMIT '
                                        + noOfRecToDisplay;
                List<selectPCWrapperBlacklisted> tempPCWrapperList1 = getAllBlacklistedPCs(pcQueryString1);
                if(tempPCWrapperList1 != null && tempPCWrapperList1.isEmpty() == false) {
                   prevStartId = tempPCWrapperList1[tempPCWrapperList1.size() - 1].recordId;
                }
                else {
                    prevStartId = selectPCWrapperBlackList[0].recordId;
                }
            }
        }
    }
    
    /************************************************************************************************
    * @Description : this method will set hte offsetVal to display next page records                *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void Nxtbtn() {
        prevStartId = selectPCWrapperList[0].userObj.Id;
        String pcQueryString   = blackListQuery
                                + ' AND Id > \'' 
                                + endId 
                                + '\'' 
                                + ' ORDER BY Name Asc LIMIT ' 
                                + noOfRecToDisplay;
        List<selectPCWrapper> tempPCWrapperList = getAllPCs(pcQueryString);
        if(tempPCWrapperList != null && !tempPCWrapperList.isEmpty()) {
            startId = tempPCWrapperList[0].recordId;
            endId = tempPCWrapperList[tempPCWrapperList.size()-1].recordId;
            selectPCWrapperList.clear();
            selectPCWrapperList.addAll(tempPCWrapperList);
        }
    }
    public void NxtbtnBlacklIsted() {
        prevStartId = selectPCWrapperBlackList[0].userObj.Id;
        String pcQueryString   = unblackListQuery
                                + ' AND Id > \'' 
                                + endId 
                                + '\'' 
                                + ' ORDER BY Id Asc LIMIT ' 
                                + noOfRecToDisplay;
        List<selectPCWrapperBlacklisted> tempPCWrapperList = getAllBlacklistedPCs(pcQueryString);
        if(tempPCWrapperList != null && !tempPCWrapperList.isEmpty()) {
            startId = tempPCWrapperList[0].recordId;
            endId = tempPCWrapperList[tempPCWrapperList.size()-1].recordId;
            selectPCWrapperBlackList.clear();
            selectPCWrapperBlackList.addAll(tempPCWrapperList);
        }
    }
    /************************************************************************************************
    * @Description : this method will set hte offsetVal to display last page records                *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void LastBtn() {
        String pcQueryString   = blackListQuery 
                                + ' ORDER BY Id Desc LIMIT '
                                + noOfRecToDisplay 
                                + noOfRecToDisplay;
        List<selectPCWrapper> tempPCWrapperList = getAllPCs(pcQueryString);
        if(tempPCWrapperList.isEmpty() == false && tempPCWrapperList.size() >= noOfRecToDisplay) {
            string endId = tempPCWrapperList[0].recordId;
            string strtId = tempPCWrapperList[noOfRecToDisplay-1].recordId;
            prevStartId = tempPCWrapperList[tempPCWrapperList.size() - 1].recordId;
            String pcQueryString1  = queryString 
                                    + ' AND Id >= \''
                                    + strtId 
                                    + '\' AND Id <= \'' 
                                    + endId 
                                    + '\' ORDER BY Id Asc LIMIT ' 
                                    + noOfRecToDisplay;
            selectPCWrapperList = getAllPCs(pcQueryString1);
        }
        if(selectPCWrapperList != null && selectPCWrapperList.isEmpty() == false) {
            startId = selectPCWrapperList[0].recordId;
            endId = selectPCWrapperList[selectPCWrapperList.size()-1].recordId;
        }
    }
    public void LastBtnBlacklisted() {
        String pcQueryString   = unblackListQuery 
                                + ' ORDER BY Id Desc LIMIT '
                                + noOfRecToDisplay 
                                + noOfRecToDisplay;
        List<selectPCWrapperBlacklisted> tempPCWrapperList = getAllBlacklistedPCs(pcQueryString);
        if(tempPCWrapperList.isEmpty() == false && tempPCWrapperList.size() >= noOfRecToDisplay) {
            string endId = tempPCWrapperList[0].recordId;
            string strtId = tempPCWrapperList[noOfRecToDisplay-1].recordId;
            prevStartId = tempPCWrapperList[tempPCWrapperList.size() - 1].recordId;
            String pcQueryString1  = queryString 
                                    + ' AND Id >= \''
                                    + strtId 
                                    + '\' AND Id <= \'' 
                                    + endId 
                                    + '\' ORDER BY Id Asc LIMIT ' 
                                    + noOfRecToDisplay;
            selectPCWrapperBlackList = getAllBlacklistedPCs(pcQueryString1);
        }
        if(selectPCWrapperBlackList != null && selectPCWrapperBlackList.isEmpty() == false) {
            startId = selectPCWrapperBlackList[0].recordId;
            endId = selectPCWrapperBlackList[selectPCWrapperBlackList.size()-1].recordId;
        }
    }
    public Class selectPCWrapper {
        public Id recordId                                  {get; set;}
        public User userObj                                 {get; set;}
        public Boolean isSelected                           {get; set;}

        public selectPCWrapper(Id pRecordId, User pUserObj, Boolean pIsSelected) {
            recordId = pRecordId;
            userObj = pUserObj;
            isSelected = pIsSelected;
        }
    }
    public Class selectPCWrapperBlacklisted {
        public Id recordId                                  {get; set;}
        public User userObj                                 {get; set;}
        public Boolean IsSelected                           {get; set;}

        public selectPCWrapperBlacklisted(Id pRecordId, User pUserObj, Boolean pIsSelected) {
            recordId = pRecordId;
            userObj = pUserObj;
            isSelected = pIsSelected;
        }
    }
}