public class Damac_ZiwoCallHistory_RESP {

    public Boolean result;
    public Content content;
    public ExtendedInfo info;

    public class Content {
        public Integer id;
        public String callID;
        public String startedAt;
        public String endedAt;
        public String answeredAt;
        public String queueEnteredAt;
        public String agentRingStartedAt;
        public String queueAnsweredAt;
        public Integer agentWaitTime;
        public Integer audioQuality;
        public String callerIDName;
        public String callerIDNumber;
        public String channelName;
        public String didCalled;
        public String direction;
        public String disposition;
        public Integer duration;
        public Integer talkTime;
        public Integer ringTime;
        public String gateway;
        public String hangupBy;
        public String hangupCause;
        public Integer holdTime;
        public String ivrFile;
        public Boolean lostInIVR;
        public Boolean nonWorkingHours;
        public Integer queueTalkTime;
        public Integer queueWaitTime;
        public String recordingFile;
        public Boolean voicemail;
        public Integer flags;
        public Integer offeredToAgentId;
        public Integer positionId;
        public Integer agentId;
        public String otherSideUserId;
        public String numberId;
        public String queueId;
        public Integer nonFCRRepeats;
        public String ivrTime;
        public String result;
        public String surveyRating;
        public String surveyRecording;
        public String createdAt;
        public String updatedAt;
        public ExtendedInfo extendedInfo;
        public String startDateTime;
        public String endDateTime;
        public String answeredDateTime;
        public String queueEnterDateTime;
        public String agentRingStartDateTime;
        public String queueAnsweredDateTime;
    }

    public class ExtendedInfo {
    }

    
    public static Damac_ZiwoCallHistory_RESP parse(String json) {
        return (Damac_ZiwoCallHistory_RESP) System.JSON.deserialize(json, Damac_ZiwoCallHistory_RESP.class);
    }
}