/****************************************************************************************************
* Name          : UpdateUserActionPlan                                                              *
* Description   : 1. Class to validate the User Action Plan records                                 *
                  2. Refresh the User Actuals and populate it on the User Action Plan               *
* Created Date  : 27/03/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    27/03/2018      Initial Draft.                                          *
* 1.1         Twinkle P     18/06/2018      Updated the functionality as per picklist changes       *
* 1.2         Twinkle P     19/08/2018      Display the count of Actual Unit Sold per User          *
****************************************************************************************************/
public with sharing class UpdateUserActionPlan {

    public static String USER_ACTION_PLAN_QUERY = ' SELECT Id, PC__c, Action_Plan__c, '
                                                + ' Zero_Sales_User__r.Closed__c '
                                                + ' FROM User_Action_Plan__c '
                                                + ' WHERE Zero_Sales_User__r.Closed__c = false '
                                                + ' AND PC__c != null ';
    public static String DECISION_ERROR         = ' Decision can be filled only after the 26th of the Month. ';
    public static String ACTION_PLAN_ERROR_1    = ' Target fields can be filled only after 1st of the Month. ';
    public static String ACTION_PLAN_ERROR_2    = ' Target fields can be filled only after 15th of the Month. ';
    public static String HOS_ERROR_1            = ' Decision field can be filled only after 15th of the Month. ';
    public static String HOS_ERROR_2            = ' Decision field can be filled only after 25th of the Month. ';
    public static String FIELD_UPDATE_ERROR     = ' Target field values cannot be updated. ';
    public static String ACTION_PLAN_TYPE_1     = '1st to 25th';

    //public static String ACTION_PLAN_TYPE_1     = '1st to 15th';
    //public static String ACTION_PLAN_TYPE_2     = '16th to 25th';
    public static Date TODAYS_DATE              = System.today();
    public static Integer DAY                   = TODAYS_DATE.day();
    public String actionPlan                    = '';

    public List<String> actionPlanIdList = new List<String>();
    public List<String> userIdList = new List<String>();
    public Map<String, String> userIdZeroSalesIdMap = new Map<String, String>();
    public Map<String, ActualValues> actionPlanMap = new Map<String, ActualValues>();

    public UpdateUserActionPlan() {

    }

    /**
     * Method to validate the User Action Plan fields
     */
    public static void verifyActionPlanTargetFields(
        Map<Id, sObject> pOldUserActionPlanRec,
        Map<Id, sObject> pNewUserActionPlanRec
    ) {
        System.debug('pNewUserActionPlanRec>>>>>>> ' + pNewUserActionPlanRec);
        for(User_Action_Plan__c newActionPlanRec : (List<User_Action_Plan__c>) pNewUserActionPlanRec.values()
        ) {
            User_Action_Plan__c oldActionPlanRec
                = (User_Action_Plan__c) pOldUserActionPlanRec.get(newActionPlanRec.Id);

            // If targets have been updated throw an error
            if ((oldActionPlanRec.Target_Meetings__c != null
                    && newActionPlanRec.Target_Meetings__c != null
                    && oldActionPlanRec.Target_Meetings__c != newActionPlanRec.Target_Meetings__c
                ) || (
                    oldActionPlanRec.Target_Outbound_Calls__c != null
                    && newActionPlanRec.Target_Outbound_Calls__c != null
                    && oldActionPlanRec.Target_Outbound_Calls__c != newActionPlanRec.Target_Outbound_Calls__c
                ) || (
                    oldActionPlanRec.Target_Prospects_Created__c != null
                    && newActionPlanRec.Target_Prospects_Created__c != null
                    && oldActionPlanRec.Target_Prospects_Created__c != newActionPlanRec.Target_Prospects_Created__c
                )|| (
                    oldActionPlanRec.Target_Booking_Units__c != null
                    && newActionPlanRec.Target_Booking_Units__c != null
                    && oldActionPlanRec.Target_Booking_Units__c != newActionPlanRec.Target_Booking_Units__c
                )
            ) {
                newActionPlanRec.addError(FIELD_UPDATE_ERROR);
            }
        }
    }

    /**
     * Method to refresh the all User Action Plan records with the actual values
     */
    public void refreshUserActionPlanRecords(String pPC, List<String> pActionPlanIdList) {

        String dateCondition = '';
        String actionPlanQuery = USER_ACTION_PLAN_QUERY;

        Date startDate1 = Date.newInstance(TODAYS_DATE.year(), TODAYS_DATE.month(), 1);
        Date endDate1 = Date.newInstance(TODAYS_DATE.year(), TODAYS_DATE.month(), 25);
        /*Date startDate2 = Date.newInstance(TODAYS_DATE.year(), TODAYS_DATE.month(), 16);
        Date endDate2 = Date.newInstance(TODAYS_DATE.year(), TODAYS_DATE.month(), 25);*/
        Date startDateAllPc = Date.newInstance(TODAYS_DATE.year(), TODAYS_DATE.month(), 1);
        Date endDateAllPc = Date.newInstance(TODAYS_DATE.year(), TODAYS_DATE.month(), 25);

        if (pActionPlanIdList != null) {
            actionPlanIdList.addAll(pActionPlanIdList);
        }

        if (pPC == 'Parent') {

            // Get the Action Plan record for the Zero Sales User record Id
            actionPlanQuery += ' AND Id IN :actionPlanIdList ';
            populateWrapperFields(actionPlanQuery);
            dateCondition += ' AND CreatedDate >= :startDateAllPc '
                           + ' AND CreatedDate <= :endDateAllPc ';
        } else if (pPC == 'All') {

            // Get all the Action Plan records
            populateWrapperFields(actionPlanQuery);
            dateCondition += ' AND CreatedDate >= :startDateAllPc '
                           + ' AND CreatedDate <= :endDateAllPc ';
        } else {

            // Get the Action Plan record for which the refresh button is selected
            actionPlanQuery += ' AND Id IN :actionPlanIdList ';
            populateWrapperFields(actionPlanQuery);
            if (actionPlan == '1st to 25th') {
                dateCondition += ' AND CreatedDate >= :startDate1 '
                               + ' AND CreatedDate <= :endDate1 ';
            }
            /*else if (actionPlan == '16th to 25th') {
                dateCondition += ' AND CreatedDate >= :startDate2 '
                               + ' AND CreatedDate <= :endDate2 ';
            }*/
        }

        // Method to update the Zero Sales User record with the current purchase
        updateUserCurrentSales(userIdZeroSalesIdMap);

        System.debug('actionPlanMap>>>>>>>  ' + actionPlanMap);
        // Get the Tasks (Call and Meetings)
        String taskQuery = ' SELECT Id, OwnerId, Activity_Type_3__c, CreatedDate '
                         + ' FROM Task '
                         + ' WHERE OwnerId IN :userIdList '
                         + ' AND Status = \'Completed\' '
                         + dateCondition ;
        System.debug('taskQuery>>>>>>>  ' + taskQuery);

        for (Task taskRec : DataBase.query(taskQuery)) {
            if (String.isNotBlank(taskRec.Activity_Type_3__c)) {

                // Task Date for 1 to 15
                if (taskRec.CreatedDate >= startDate1
                    && taskRec.CreatedDate <= endDate1 ) {
                    String mapKey1 = taskRec.OwnerId + '-' + ACTION_PLAN_TYPE_1;
                    if (taskRec.Activity_Type_3__c == 'Outbound'
                        || taskRec.Activity_Type_3__c == 'Inbound') {
                        actionPlanMap.get(mapKey1).calls += 1;
                    } else if (taskRec.Activity_Type_3__c.startsWithIgnoreCase('Meeting')) {
                        actionPlanMap.get(mapKey1).meetings += 1;
                    }
                }

                // Task Date for 15 to 25
                /*if (taskRec.CreatedDate >= startDate2
                    && taskRec.CreatedDate <= endDate2 ) {
                    String mapKey2 = taskRec.OwnerId + '-' + ACTION_PLAN_TYPE_2;
                    if (taskRec.Activity_Type_3__c == 'Outbound'
                        || taskRec.Activity_Type_3__c == 'Inbound') {
                        actionPlanMap.get(mapKey2).calls += 1;
                    } else if (taskRec.Activity_Type_3__c.startsWithIgnoreCase('Meeting')) {
                        actionPlanMap.get(mapKey2).meetings += 1;
                    }
                }*/

            }
        }

        // Get the Inquiries
        String inquiryQuery = ' SELECT Id, OwnerId, CreatedDate '
                            + ' FROM Inquiry__c '
                            + ' WHERE OwnerId IN :userIdList '
                            + ' AND Inquiry_Source__c = \'Prospecting\''
                            + dateCondition ;
        System.debug('inquiryQuery>>>>>>>  ' + inquiryQuery);

        for (Inquiry__c inq : DataBase.query(inquiryQuery)) {
            if (inq.CreatedDate >= startDate1
                && inq.CreatedDate <= endDate1
            ) {
                String mapKey1 = inq.OwnerId + '-' + ACTION_PLAN_TYPE_1;
                actionPlanMap.get(mapKey1).prospects += 1;
            }
            /*if (inq.CreatedDate >= startDate2
                && inq.CreatedDate <= endDate2
            ) {
                String mapKey2 = inq.OwnerId + '-' + ACTION_PLAN_TYPE_2;
                actionPlanMap.get(mapKey2).prospects += 1;
            }*/
        }
        //V1.3 Twinkle P : Display the Count of Actual Booking Unit Sold by the User START
        String BU_QUERY                      = ' SELECT Id, Registration_Status__c, Registration_DateTime__c, '
                                                + ' Booking__r.Deal_SR__r.OwnerId, Requested_Price__c '
                                                + ' FROM Booking_Unit__c '
                                                + ' WHERE Booking__r.Deal_SR__r.OwnerId IN :userIdList '
                                                + ' AND Registration_Status__c != \'Agreement Transfer\' '
                                                + ' AND Registration_Status__c != \'Terminated & Released to Sales Admin\' '
                                                + ' AND Registration_Status__c != \'Agreement Rejected By Sales Admin\' '
                                                + ' AND Registration_Status__c != \'Auto terminated and Released\' '
                                                + ' AND Registration_Status__c != \'Direct Released\' '
                                                + ' AND Registration_Status__c != \'Rejected due to Time Out\' '
                                                + ' AND Registration_Status__c != \'Rejected During Assignment\' '
                                                + ' AND Registration_Status__c != \'Reserved\' '
                                                + ' AND Registration_Status__c != \'\' '
                                                + ' AND Requested_Price__c != null ';
        // Calculation of Sales for the current 1 months
        String bookingUnitQuery = BU_QUERY
                                + ' AND Requested_Price__c > 0 '
                                + ' AND Registration_DateTime__c = THIS_MONTH ';
        for (Booking_Unit__c bookingUnitObj : Database.query(bookingUnitQuery)) {
                String mapKey1 = bookingUnitObj.Booking__r.Deal_SR__r.OwnerId + '-' + ACTION_PLAN_TYPE_1;
                actionPlanMap.get(mapKey1).bookingUnits += 1;

        }
        //V1.3 Twinkle P : Display the Count of Actual Booking Unit Sold by the User END
        // Update the User Action Plan with the Actual values
        List<User_Action_Plan__c> updateUserActionPlan = new List<User_Action_Plan__c>();
        for (String userIdActionPlan : actionPlanMap.keySet()) {
            User_Action_Plan__c updatedPlan         = new User_Action_Plan__c();
            updatedPlan.Actual_Meetings__c          = actionPlanMap.get(userIdActionPlan).meetings;
            updatedPlan.Actual_Outbound_Calls__c    = actionPlanMap.get(userIdActionPlan).calls;
            updatedPlan.Actual_Prospects_Created__c = actionPlanMap.get(userIdActionPlan).prospects;
            updatedPlan.Actual_Booking_Units__c     = actionPlanMap.get(userIdActionPlan).bookingUnits;
            updatedPlan.Id                          = actionPlanMap.get(userIdActionPlan).userActionPlanId;
            updateUserActionPlan.add(updatedPlan);
        }
        System.debug('updateUserActionPlan>>>>>>>  ' + updateUserActionPlan);

        // DML
        update updateUserActionPlan;
    }

    /**
     * Method to populate the Wrapper in the Map.
     * Populate the collections for processing.
     */
    public void populateWrapperFields(String pQuery) {

        System.debug('pQuery>>>>>>>  ' + pQuery);
        if (String.isNotBlank(pQuery)) {
            for(User_Action_Plan__c actionPlanObj : Database.query(pQuery)) {
                String uniqueActionPlan = actionPlanObj.PC__c + '-' + actionPlanObj.Action_Plan__c;
                actionPlanMap.put(uniqueActionPlan, new ActualValues());
                actionPlanMap.get(uniqueActionPlan).pcId = actionPlanObj.PC__c;
                actionPlanMap.get(uniqueActionPlan).userActionPlanId = actionPlanObj.Id;
                userIdList.add(actionPlanObj.PC__c);
                userIdZeroSalesIdMap.put(actionPlanObj.PC__c, actionPlanObj.Zero_Sales_User__c);
                actionPlan = actionPlanObj.Action_Plan__c;
            }
        }
    }

    /**
     * Method to fetch the Units sold by user for the current month
     * and populate the sum on the Zero Sales User Record
     */
    public void updateUserCurrentSales(Map<String, String> pUserIdZeroSalesIdMap) {

        if (pUserIdZeroSalesIdMap != null
            && !pUserIdZeroSalesIdMap.isEmpty()
        ) {
            Decimal currentSalesAmount;
            List<Zero_Sales_User__c> zsUserList = new List<Zero_Sales_User__c>();
            Map<String, Double> userIdCurrSalesAmountMap = new Map<String, Double>();
            Date currentMonthFirstDay = Date.newInstance(TODAYS_DATE.year(), TODAYS_DATE.month(), 1);
            Date currentMonthLastDay = Date.newInstance(TODAYS_DATE.year(), TODAYS_DATE.month(), 25);
            List<String> userIds = new List<String>();
            userIds.addAll(pUserIdZeroSalesIdMap.keySet());

            String bookingUnitQueryCurrentMonth = ' SELECT Id, Registration_Status__c, '
                                                + ' Registration_DateTime__c, Requested_Price__c, '
                                                + ' Booking__r.Deal_SR__r.OwnerId '
                                                + ' FROM Booking_Unit__c '
                                                + ' WHERE Booking__r.Deal_SR__r.OwnerId IN :userIds '
                                                + ' AND Registration_DateTime__c >= :currentMonthFirstDay '
                                                + ' AND Registration_DateTime__c <= :currentMonthLastDay '
                                                + ' AND Requested_Price__c != null '
                                                + ' AND Registration_Status__c != \'Agreement Transfer\' '
                                                + ' AND Registration_Status__c != \'Terminated & Released to Sales Admin\' '
                                                + ' AND Registration_Status__c != \'Agreement Rejected By Sales Admin\' '
                                                + ' AND Registration_Status__c != \'Auto terminated and Released\' '
                                                + ' AND Registration_Status__c != \'Direct Released\' '
                                                + ' AND Registration_Status__c != \'Rejected due to Time Out\' '
                                                + ' AND Registration_Status__c != \'Rejected During Assignment\' '
                                                + ' AND Registration_Status__c != \'Reserved\' '
                                                + ' AND Registration_Status__c != \'\' ';
            System.debug('Database.query(bookingUnitQuery)>>>>>>> ' + bookingUnitQueryCurrentMonth);

            // Get the Users current month bookings
            for (Booking_Unit__c bookingUnitObj : Database.query(bookingUnitQueryCurrentMonth)) {
                currentSalesAmount = 0;
                if (userIdCurrSalesAmountMap.containskey(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId)) {
                    currentSalesAmount = userIdCurrSalesAmountMap.get(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId);
                    currentSalesAmount += bookingUnitObj.Requested_Price__c;
                    userIdCurrSalesAmountMap.put(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId, currentSalesAmount);
                } else {
                    currentSalesAmount += bookingUnitObj.Requested_Price__c;
                    userIdCurrSalesAmountMap.put(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId, currentSalesAmount);
                }
            }

            // Update the Zero Sales User record with the Users sale for the current month
            for (String userId : pUserIdZeroSalesIdMap.keySet()) {
                if (userIdCurrSalesAmountMap.containsKey(userId)) {
                    Zero_Sales_User__c zsRecord = new Zero_Sales_User__c();
                    zsRecord.Id = pUserIdZeroSalesIdMap.get(userId);
                    zsRecord.Current_Month_Sale__c = userIdCurrSalesAmountMap.get(userId);
                    zsUserList.add(zsRecord);
                }
            }

            // DML
            update zsUserList;
        }

    }

    /**
     * Wrapper Class
     */
    public class ActualValues {
        public String userActionPlanId;
        public String pcId;
        public Integer calls;
        public Integer meetings;
        public Integer prospects;
        public Integer bookingUnits;

        /**
         * Wrapper Class Constructor
         */
        public ActualValues() {
            calls = 0;
            meetings = 0;
            prospects = 0;
            bookingUnits = 0;
        }
    }

}