global class ScheduleCallingListBatchCX implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        
        CallingListEmailToCXBatch b = new CallingListEmailToCXBatch ();
        database.executebatch(b,200);
    }
   
}