//AKISHOR
@isTest
private class CancelHOApptTest{
    static testMethod void test_UndoTest() {          
             
    List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
     TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                     OnOffCheck__c = true);
      
    settingLst2.add(newSetting1);
    insert settingLst2;
         
    // Insert Accont
    Id accntrecid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
    Account objAcc = new Account();
    objAcc.FirstName='AK';
    objAcc.LastName='TEST';
    objAcc.Type='Person';
    objAcc.Party_Id__c='79658';
    objAcc.RecordTypeId=accntrecid;
    insert objAcc;
    Id dealrecid=Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
    NSIBPM__Service_Request__c Deal = new NSIBPM__Service_Request__c();
    Deal.RecordTypeId=dealrecid;
    insert Deal;
    Booking__c B=new Booking__c();
    B.Booking_Channel__c='Web';
    B.Deal_SR__c=Deal.Id;
    insert B;
    Booking_Unit__c BU= new Booking_Unit__c ();
    BU.Registration_ID__c='100679';
     BU.Unit_Name__c='AK/10/1010';
     BU.Party_Id__c='79658';
     BU.Booking__c=B.Id;
     insert BU;
    //Insert & Upd Case
    Id CLRecId= Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
    Calling_List__c CLObj =new Calling_List__c(Account__c=objAcc.id,Appointment_Status__c='Requested',Calling_List_Status__c='New',RecordTypeId=CLRecId,Booking_Unit__c=BU.Id);
    insert CLObj;
    system.debug('caseObj : '+CLObj);
         test.startTest();
         PageReference pageRef = Page.CancelAllApts;
         Test.setCurrentPage(pageRef);
         ApexPages.StandardController sc = new ApexPages.standardController(CLObj);
         CancelAllAppts controller = new CancelAllAppts(sc);
         controller.AptId = CLObj.Id;
         Pagereference newPg = controller.UpdateAppointment();
         //System.assertEquals(null,controller.cancelCase());
         test.stopTest();
     }
}