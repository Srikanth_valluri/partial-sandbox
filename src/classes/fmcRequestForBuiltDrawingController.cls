public without sharing class fmcRequestForBuiltDrawingController
                                extends RequestForBuiltDrawingController {
            String                      caseId                      {get;set;}
    public  list<SelectOption>          lstUnits 					{get; set;}
    public  List<Booking_Unit__c>       objUnitList  			    {get; set;}
    public  map<Id,Booking_Unit__c>     mapUnitsOwned 		        {get; set;}
    public  map<Id,Booking_Unit__c>     mapMyUnits 			        {get; set;}
    public  List<Id>                    objUnitOwner                {get; set;}
    public  List<Id>                    objUnitRes                  {get; set;}
    public  Decimal                     valfeeAsBuiltDrawing;

    public fmcRequestForBuiltDrawingController() {
        super(false);
        if (FmcUtils.isCurrentView('asbuiltdrawings')) {
           	//super();
            isEdittable = true;
            //valueList = new SelectOption[0];
            //values = new String[0];
            isMandatory = false;
            // strAccountId = CustomerCommunityUtils.customerAccountId;
            strFMCaseId = ApexPages.currentPage().getParameters().get('Id');
            system.debug('strFMCaseId : '+strFMCaseId);
            objFMCase = new FM_Case__c();
            valueListOfDrawings = new List<SelectOption>();
            strSRType = 'Request_for_As_built_drawings';
            if(String.isBlank(strFMCaseId)) {
                strAccountId = CustomerCommunityUtils.customerAccountId;
                strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
            }else {
                objFMCase = FM_Utility.getCaseDetails( strFMCaseId );
                strAccountId = objFMCase.Account__c ;
                strSelectedUnit = objFMCase.Booking_Unit__c;
                objFMCase = FM_Utility.getCaseDetails( strFMCaseId );
                fetchUnits();
                getfieldValues();
                init();
            }
            /*if(String.isBlank( strFMCaseId )){
strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
strSRType = ApexPages.currentPage().getParameters().get('SRType');
}else{*/
            system.debug('== strSelectedUnit =='+strSelectedUnit );
            // strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
            strSRType = 'Request_for_As_built_drawings';
            objUnit = new Booking_Unit__c();
            feeAsBuiltDrawing = '';
            valfeeAsBuiltDrawing = 0.0;
            //}
            //if(String.isNotBlank(strSelectedUnit) || String.isNotBlank(strFMCaseId)){
            //init();
            //}
            fetchUnits();
        }
    }

    public override void init() {
        //initializeFMCase();
        System.debug('objFMCase.Submitted__c======'+objFMCase.Submitted__c);
        if(objFMCase.Submitted__c==true){
            isEdittable=false;
        }
        System.debug('isEdittable======'+isEdittable);
        objUnit = getUnitDetails(strSelectedUnit) ;
        system.Debug('>>>objUnit : '+objUnit);
        System.debug('objUnit.Inventory__r.Building_Location__c'
                                +objUnit.Inventory__r.Building_Location__c);
        List<Location__c> objList = new List<Location__c>();
        objList = [ SELECT As_Built_Drawing_Fee__c
                    FROM Location__c
                    WHERE id=:objUnit.Inventory__r.Building_Location__c ];
        System.debug('obj=-========'+objList);
        if(!objList.isEmpty()) {
            System.debug('obj=-========'+objList);
            System.debug('obj.As_Built_Drawing_Fee__c=-========'+objList[0].As_Built_Drawing_Fee__c);
            feeAsBuiltDrawing = objList[0].As_Built_Drawing_Fee__c != NULL
                                ? String.valueof(objList[0].As_Built_Drawing_Fee__c) : '';
            System.debug('feeAsBuiltDrawing : '+feeAsBuiltDrawing);
            valfeeAsBuiltDrawing = String.isNotBlank(feeAsBuiltDrawing)
                                    ? Decimal.valueOf(feeAsBuiltDrawing) : 0;
            System.debug('valfeeAsBuiltDrawing : '+valfeeAsBuiltDrawing);
        }
        system.debug('strSRType : '+strSRType);
        if( String.isNotBlank( strSRType ) && objUnit != NULL ) {
            processDocuments();
        }
        System.debug('-----objFMCase.id-----'+objFMCase.id);
        System.debug('=====lstDocuments===='+lstDocuments);
        /*ID recordType= Schema.SObjectType.Pricing__c.getRecordTypeInfosByName().get('Request For As-built Drawings').getRecordTypeId();
for(Pricing__c record: [select id,Cost_del__c,Pricing_Name__c from Pricing__c where recordTypeId=:recordType AND Building__c=:objUnit.Inventory__r.Building_Location__c ])
valuelist.add(new selectoption(record.id, record.Pricing_Name__c));*/
    }

    public pageReference getUnitDetailsNew() {
    	objFMCase.Description__c = '';
        objFMCase.Person_To_Collect__c = '';
        valueListOfDrawings.clear();
        valueListOfDrawings=new List<SelectOption>();
        system.debug('strSelectedUnit : '+strSelectedUnit);
        if (String.isBlank(strSelectedUnit)) {
            return NULL;
        }
        //init();
        getfieldValues();
        System.debug('>>--valueListOfDrawings : '+valueListOfDrawings);
        System.debug('=====lstDocuments===='+lstDocuments);
        return null;
    }

    public PageReference fetchUnits() {
        Set<String> activeStatusSet= Booking_Unit_Active_Status__c.getall().keyset();
        mapUnitsOwned = new map<Id,Booking_Unit__c>();
        objUnitOwner = new List<Id>();
        objUnitRes = new List<Id>();
        mapMyUnits = new map<Id,Booking_Unit__c>();
        lstUnits = new list<SelectOption>();
        lstUnits.add(new selectOption('None','--None--'));
        objUnitList = [SELECT Id
                             , Owner__c
                             , Booking__r.Account__c
                             , Booking__r.Account__r.Name
                             , Owner__r.IsPersonAccount
                             , Tenant__r.IsPersonAccount
                             , Owner__r.Name
                             , Tenant__c
                             , Tenant__r.Name
                             , Resident__c
                             , Resident__r.Name
                             , Unit_Name__c
                             , Registration_Id__c
                             , Property_Name__c
                       FROM Booking_Unit__c
                       WHERE ( Resident__c = :strAccountId
                              OR Booking__r.Account__c = :strAccountId
                              OR Tenant__c = :strAccountId )
                       AND ( Handover_Flag__c = 'Y'
                            OR Early_Handover__c = true )
                       AND Registration_Status__c IN :activeStatusSet ];
        if(!objUnitList.isEmpty() && objUnitList != NULL) {
            for( Booking_Unit__c objBU : objUnitList ) {
                if( objBU.Owner__c == strAccountId ) {
                    objUnitOwner.add(objBU.Id);
                    mapUnitsOwned.put(objBU.Id,objBU);
                }

                if( ( objBU.Tenant__c == strAccountId || objBU.Resident__c == strAccountId )
                            && objBU.Owner__c != strAccountId ) {
                    objUnitRes.add(objBU.Id);
                    mapMyUnits.put(objBU.Id,objBU);
                }
                lstUnits.add(new selectOption(objBU.Id,objBU.Unit_Name__c));
            }
        }else{
            ApexPages.addmessage(new ApexPages.message(
                            ApexPages.severity.Error,'No Booking Units available'));
        }
        system.debug('==lstUnits=='+lstUnits);
        return NULL;
    }

    public override void createMethod() {
        System.debug('objFMCase----->'+objFMCase);
        if(objFMCase != NULL) {
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                                            get('Request For Built-in Drawings').getRecordTypeId();
            objFMCase.Account__c = strAccountId ;
            objFMCase.Booking_Unit__c = strSelectedUnit ;
            objFMCase.Request_Type_DeveloperName__c = strSRType ;
            objFMCase.status__c ='Draft Request';
            objFMCase.Request_Type__c = 'Request For Built-in Drawings';
            objFMCase.Origin__c='Portal';
            if(valuesOfDrawings != NULL && !valuesOfDrawings.isEmpty()) {
                objFMCase.Type_of_Drawings__c = String.join(valuesOfDrawings ,';');
            }
            System.debug('>>>>---objFMCase.Type_of_Drawings__c--- : '+objFMCase.Type_of_Drawings__c);
            if(!String.isBlank(objFMCase.Email__c) || !String.isBlank(objFMCase.Email_2__c)) {
                objFMCase.Contact_Email__c = !String.isBlank(objFMCase.Email__c)
                                                 ? objFMCase.Email__c : objFMCase.Email_2__c ;
            }
            System.debug('valfeeAsBuiltDrawing---->'+ valfeeAsBuiltDrawing);
            objFMCase.As_Built_Fee__c= valfeeAsBuiltDrawing;
            System.debug('As_Built_Fee__c---->'+ objFMCase.As_Built_Fee__c);
        }
        upsert objFMCase;
    }

    public override PageReference createRequestForWorkPermit() {
        if( objFMCase != NULL ) {
            System.debug('objFMCase------'+objFMCase);
            init();
            createMethod();

            PageReference objPage = new PageReference('/' + objFMCase.Id );
            if (objPage != NULL) {
                String unitId = objPage.getUrl().substringAfterLast('/');
                objPage = new PageReference(ApexPages.currentPage().getUrl());
                objPage.getParameters().clear();
                objPage.getParameters().put('view', 'CaseDetails');
                objPage.getParameters().put('id', unitId);
                objPage.setRedirect(true);
            }
            return objPage ;
        }
        return NULL ;
    }

    public override PageReference submitRequestForWorkPermit() {
        try{
            if( objFMCase != NULL ) {
                init();
                System.debug('objFMCase Override : '+objFMCase);
                System.debug('objFMCase Override Id : '+objFMCase.id);
                if(objFMCase.id==null){
                    objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName()
                                                .get('Request For Built-in Drawings').getRecordTypeId();
                    objFMCase.Account__c = strAccountId ;
                    objFMCase.Booking_Unit__c = strSelectedUnit ;
                    objFMCase.Request_Type_DeveloperName__c = strSRType ;
                    objFMCase.Request_Type__c = 'Request For Built-in Drawings';
                    System.debug('objFMCase.RecordTypeId----->'+objFMCase.RecordTypeId);
                    insert objFMCase;
                }
                if(!String.isBlank(objFMCase.Email__c) || !String.isBlank(objFMCase.Email_2__c)){
                    objFMCase.Contact_Email__c = !String.isBlank(objFMCase.Email__c)
                                                    ? objFMCase.Email__c : objFMCase.Email_2__c ;
                }
                System.debug('valfeeAsBuiltDrawing----->'+valfeeAsBuiltDrawing);
                objFMCase.As_Built_Fee__c= valfeeAsBuiltDrawing;
                //objFMCase.Type_of_Drawings__c=String.join(valuesOfDrawings ,';');
	            if(valuesOfDrawings != NULL && !valuesOfDrawings.isEmpty()) {
	                objFMCase.Type_of_Drawings__c=String.join(valuesOfDrawings ,';');
	            }
                System.debug('>>>>---objFMCase.Type_of_Drawings__c--- : '
                                                    +objFMCase.Type_of_Drawings__c);
                objFMCase.Status__c='Submitted';
                objFMCase.Submitted__c=true;
                System.debug('objFMCase.submitted__c====='+objFMCase.Submitted__c);
                if(objFMCase.Submitted__c==true){
                    System.debug('objFMCase.submitted__c=====1'+objFMCase.Submitted__c);

                    System.debug('objFMCase.Unit_Name__c-----'+objUnit.Unit_Name__c);
                    if( String.isNotBlank( objUnit.Unit_Name__c ) ) {
                        list<FM_User__c> lstUsers = FM_Utility.getApprovingUsers( new set<String> {
                                                            objUnit.Unit_Name__c.split('/')[0] } );
                        System.debug('lstUsers ======'+lstUsers);
                        for( FM_User__c objUser : lstUsers ) {
                            if( String.isNotBlank( objUser.FM_Role__c ) ) {
                                if( objUser.FM_Role__c.containsIgnoreCase( 'Manager') ) {
                                    objFMCase.FM_Manager_Email__c = objUser.FM_User__r.Email;
                                }
                                else if( objUser.FM_Role__c.containsIgnoreCase( 'Admin') &&
                                                objFMCase.Admin__c == NULL ) {
                                            objFMCase.Admin__c = objUser.FM_User__c;
                                }
                            }
                        }
                    }
                    String strApprovingUsers='';
                    System.debug('objFMCase.Request_Type_DeveloperName__c===='
                                            +objFMCase.Request_Type_DeveloperName__c);
                    System.debug('objUnit.Property_City__c'+objUnit.Property_City__c);
                    List<FM_Approver__mdt> listApproverUser=FM_Utility.fetchApprovers(
                                  objFMCase.Request_Type_DeveloperName__c,objUnit.Property_City__c);
                    System.debug('listApproverUser====='+listApproverUser);
                    if(!listApproverUser.isEmpty()){
                        System.debug('----1----');
                        for(FM_Approver__mdt fmApproverInstance:listApproverUser){
                            System.debug('----2----');
                            strApprovingUsers=strApprovingUsers+fmApproverInstance.Role__c+',';
                        }
                        System.debug('strApprovingUsers==='+strApprovingUsers);
                        strApprovingUsers=strApprovingUsers.removeEnd(',');
                        objFMCase.Approving_Authorities__c=strApprovingUsers;
                        objFMCase.Approval_Status__c='Pending';
                        //objFMCase.Submit_for_Approval__c=true;
                        update objFMCase;
                    }
                }
                PageReference objPage = new PageReference('/' + objFMCase.Id );
                if (objPage != NULL) {
                    String unitId = objPage.getUrl().substringAfterLast('/');
                    objPage = new PageReference(ApexPages.currentPage().getUrl());
                    objPage.getParameters().clear();
                    objPage.getParameters().put('view', 'CaseDetails');
                    objPage.getParameters().put('id', unitId);
                    objPage.setRedirect(true);
                }
                return objPage ;
            }
        }
        catch(Exception excp) {
            ApexPages.addmessage(new ApexPages.message(
                        ApexPages.severity.Error, excp.getMessage() +': ' + excp.getLineNumber()));
        }
        return null;
    }
}