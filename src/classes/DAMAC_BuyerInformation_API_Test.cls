/************************************************************************************************
 * @Name              : DAMAC_BuyerInformation_API_Test
 * @Description       : Test Class for DAMAC_BuyerInformation_API
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         03/08/2020       Created
***********************************************************************************************/
@isTest
public class DAMAC_BuyerInformation_API_Test{

    @isTest
    static void testAPI(){
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.Mode_of_Payment__c = 'Cheque';
        insert sr;
        
        Booking__c booking = new Booking__c();
        booking.Deal_SR__c = sr.Id;
        insert booking;
        
        Buyer__c primaryBuyer = new Buyer__c();
        primaryBuyer.Booking__c = booking.Id;
        primaryBuyer.First_Name__c = 'John';
        primaryBuyer.Last_Name__c = 'Wayne';
        primaryBuyer.Primary_Buyer__c = true;        
        primaryBuyer.Buyer_Type__c = 'Individual';
        primaryBuyer.Title__c = 'Mr.';             
        primaryBuyer.DOB__c = Date.newInstance(1999, 12, 9);
        primaryBuyer.Passport_Expiry__c = Date.newInstance(2030, 12, 9);
        primaryBuyer.Gender__c = 'Male';
        primaryBuyer.Email__c = 'jithesh@test.com';                                         
        primaryBuyer.Phone_Country_Code__c = 'India: 0091';
        primaryBuyer.Phone__c = '7829472432'; 
        primaryBuyer.Country__c = 'India';
        primaryBuyer.City__c = 'Kochi';
        primaryBuyer.Nationality__c = 'Indian';
        primaryBuyer.EID_No__c = '784121221266665';
        primaryBuyer.Damac_Agent_Action_Type__c = 'Saved as Draft';
        primaryBuyer.Address_Line_1__c = 'line1';
        primaryBuyer.Address_Line_2__c = '';
        primaryBuyer.Address_Line_3__c = ''; 
        primaryBuyer.Address_Line_4__c = ''; 
        primaryBuyer.Passport_Number__c = 'AVZpgbsj477883';
        primaryBuyer.Place_of_Issue__c = 'Thrissur'; 
        insert primaryBuyer;
        
        Buyer__c jointBuyer1 = new Buyer__c();
        jointBuyer1.Booking__c = booking.Id;
        jointBuyer1.First_Name__c = 'Todd';
        jointBuyer1.Last_Name__c = 'Philips';
        jointBuyer1.Primary_Buyer__c = false;
        jointBuyer1.Buyer_Type__c = 'Individual';
        jointBuyer1.Title__c = 'Mr.';             
        jointBuyer1.DOB__c = Date.newInstance(1999, 12, 9);
        jointBuyer1.Passport_Expiry__c = Date.newInstance(2030, 12, 9);
        jointBuyer1.Gender__c = 'Male';
        jointBuyer1.Email__c = 'jithesh@test.com';                                         
        jointBuyer1.Phone_Country_Code__c = 'India: 0091';
        jointBuyer1.Phone__c = '7829472432'; 
        jointBuyer1.Country__c = 'India';
        jointBuyer1.City__c = 'Kochi';
        jointBuyer1.Nationality__c = 'Indian';
        jointBuyer1.EID_No__c = '784121221266665';
        jointBuyer1.Damac_Agent_Action_Type__c = 'Saved as Draft';
        jointBuyer1.Address_Line_1__c = 'line1';
        jointBuyer1.Address_Line_2__c = '';
        jointBuyer1.Address_Line_3__c = ''; 
        jointBuyer1.Address_Line_4__c = ''; 
        jointBuyer1.Passport_Number__c = 'AVZpgbsj477883';
        jointBuyer1.Place_of_Issue__c = 'Thrissur'; 
        insert jointBuyer1;
        
        Buyer__c jointBuyer2 = new Buyer__c();
        jointBuyer2.Booking__c = booking.Id;
        jointBuyer2.First_Name__c = 'Bradley';
        jointBuyer2.Last_Name__c = 'Cooper';
        jointBuyer2.Primary_Buyer__c = false;
        jointBuyer2.Buyer_Type__c = 'Individual';
        jointBuyer2.Title__c = 'Mr.';             
        jointBuyer2.DOB__c = Date.newInstance(1999, 12, 9);
        jointBuyer2.Passport_Expiry__c = Date.newInstance(2030, 12, 9);
        jointBuyer2.Gender__c = 'Male';
        jointBuyer2.Email__c = 'jithesh@test.com';                                         
        jointBuyer2.Phone_Country_Code__c = 'India: 0091';
        jointBuyer2.Phone__c = '7829472432'; 
        jointBuyer2.Country__c = 'India';
        jointBuyer2.City__c = 'Kochi';
        jointBuyer2.Nationality__c = 'Indian';
        jointBuyer2.EID_No__c = '784121221266665';
        jointBuyer2.Damac_Agent_Action_Type__c = 'Saved as Draft';
        jointBuyer2.Address_Line_1__c = 'line1';
        jointBuyer2.Address_Line_2__c = '';
        jointBuyer2.Address_Line_3__c = ''; 
        jointBuyer2.Address_Line_4__c = ''; 
        jointBuyer2.Passport_Number__c = 'AVZpgbsj477883';
        jointBuyer2.Place_of_Issue__c = 'Thrissur';         
        insert jointBuyer2;
        
        Buyer__c jointBuyer3 = new Buyer__c();
        jointBuyer3.Booking__c = booking.Id;
        jointBuyer3.First_Name__c = 'Bradley';
        jointBuyer3.Last_Name__c = 'Cooper';
        jointBuyer3.Primary_Buyer__c = false;
        jointBuyer3.Buyer_Type__c = 'Individual';
        jointBuyer3.Title__c = '';             
        jointBuyer3.DOB__c = null;//Date.newInstance(1999, 12, 9);
        jointBuyer3.Passport_Expiry__c = null;//Date.newInstance(2030, 12, 9);
        jointBuyer3.Gender__c = 'Male';
        jointBuyer3.Email__c = '';                                         
        jointBuyer3.Phone_Country_Code__c = '';
        jointBuyer3.Phone__c = ''; 
        jointBuyer3.Country__c = '';
        jointBuyer3.City__c = '';
        jointBuyer3.Nationality__c = '';
        jointBuyer3.EID_No__c = '';
        jointBuyer3.Damac_Agent_Action_Type__c = 'Saved as Draft';
        jointBuyer3.Address_Line_1__c = '';
        jointBuyer3.Address_Line_2__c = '';
        jointBuyer3.Address_Line_3__c = ''; 
        jointBuyer3.Address_Line_4__c = ''; 
        jointBuyer3.Passport_Number__c = '';
        jointBuyer3.Place_of_Issue__c = '';         
        insert jointBuyer3;
        
        Unit_Documents__c unit1 = new Unit_Documents__c();
        unit1.Buyer__c = primaryBuyer.Id;
        unit1.Service_Request__c = sr.Id;
        unit1.Document_Name__c = 'Reservation Copy';
        unit1.Status__c = 'Uploaded';
        insert unit1;
        
        Blob b = Blob.valueOf('Test Data');   
        
        Attachment attachment1 = new Attachment();
        attachment1.ParentId = unit1.Id;
        attachment1.Name = 'Test Attachment for Parent';
        attachment1.Body = b;
        insert(attachment1);
        
        Unit_Documents__c unit2 = new Unit_Documents__c();
        unit2.Buyer__c = primaryBuyer.Id;
        unit2.Service_Request__c = sr.Id;
        unit2.Document_Name__c = 'Token Copy';
        unit2.Status__c = 'Uploaded';
        insert unit2;
        
        //Blob b = Blob.valueOf('Test Data');      
        Attachment attachment2 = new Attachment();
        attachment2.ParentId = unit2.Id;
        attachment2.Name = 'Test Attachment for Parent';
        attachment2.Body = b;
        insert(attachment2);
        
        Unit_Documents__c unit3 = new Unit_Documents__c();
        unit3.Buyer__c = jointBuyer1.Id;
        unit3.Service_Request__c = sr.Id;
        unit3.Document_Name__c = 'John-Passport Copy';
        unit3.Status__c = 'Uploaded';
        insert unit3;
        
        //Blob b = Blob.valueOf('Test Data');      
        Attachment attachment3 = new Attachment();
        attachment3.ParentId = unit3.Id;
        attachment3.Name = 'Test Attachment for Parent';
        attachment3.Body = b;
        insert(attachment3);
        
        Unit_Documents__c unit4 = new Unit_Documents__c();
        unit4.Buyer__c = jointBuyer2.Id;
        unit4.Service_Request__c = sr.Id;
        unit4.Document_Name__c = 'Bradley-Passport Copy';
        unit4.Status__c = 'Uploaded';
        insert unit4;
        
        //Blob b = Blob.valueOf('Test Data');      
        Attachment attachment4 = new Attachment();
        attachment4.ParentId = unit4.Id;
        attachment4.Name = 'Test Attachment for Parent';
        attachment4.Body = b;
        insert(attachment4);
        
        Unit_Documents__c unit5 = new Unit_Documents__c();
        unit5.Buyer__c = jointBuyer2.Id;
        unit5.Service_Request__c = sr.Id;
        unit5.Document_Name__c = 'Bradley-Passport Copy';
        unit5.Status__c = 'Uploaded';
        insert unit5;
        
        //Blob b = Blob.valueOf('Test Data');      
        Attachment attachment5 = new Attachment();
        attachment5.ParentId = unit5.Id;
        attachment5.Name = 'Test Attachment for Parent';
        attachment5.Body = b;
        insert(attachment5);
        
        Unit_Documents__c unit6 = new Unit_Documents__c();
        unit6.Buyer__c = jointBuyer2.Id;
        unit6.Service_Request__c = sr.Id;
        unit6.Document_Name__c = 'Bradley-Passport Copy';
        unit6.Status__c = 'Pending Upload';
        insert unit6;
        
        /*
        Attachment attachment6 = new Attachment();
        attachment6.ParentId = unit6.Id;
        attachment6.Name = 'Test Attachment for Parent';
        attachment6.Body = b;
      
        insert(attachment6);
        
        */
        
        
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.params.put ('bookingId',booking.Id );
        req.requestUri = '/buyerInformation';
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_BuyerInformation_API.getResults();
        
    }
}