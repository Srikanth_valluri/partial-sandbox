/****************************************************************************************************
* Name          : RefreshZeroSalesUsersBatchTest                                                    *
* Description   : Test class for RefreshZeroSalesUsersBatch                                         *
* Created Date  : 08/04/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    08/04/2018      Initial Draft.                                          *
****************************************************************************************************/
@isTest 
public class RefreshZeroSalesUsersBatchTest {
    
    @isTest static void testMethod1 (){

        Profile profileObj = [SELECT Id FROM Profile WHERE Name = 'Property Consultant' LIMIT 1]; 
        User userObj = new User(
            Alias = 'stand', 
            Email = 'user@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObj.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfd@testorg.com'
        );
        insert userObj;

        Zero_Sales_User__c zs = new Zero_Sales_User__c();
        zs.Employee__c = userObj.Id;
        zs.HOS_Descision__c = 'Pending';
        insert zs;

        DateTime todaysDateTime = datetime.now();
        String lastMonthName = todaysDateTime.addmonths(-1).format('MMMMM');
        String secondLastMonthName = todaysDateTime.addmonths(-2).format('MMMMM');

        Target__c targetObjMonth3 = new Target__c(
            Target__c = 999,
            User__c = userObj.Id,
            Month__c =  Datetime.now().format('MMMMM'),
            Year__c = String.valueOf(System.today().year())
        );
        insert targetObjMonth3;


        Test.startTest();
        ScheduleRefreshZeroSalesUsersBatch schedulerInstance2 = new ScheduleRefreshZeroSalesUsersBatch();
        String cronStr2 = '0 2 0 1 1/1 ? *'; 
        system.schedule('Test RefreshZeroSalesUsersBatch', cronStr2, schedulerInstance2);
        RefreshZeroSalesUsersBatch zsUserBatch = new RefreshZeroSalesUsersBatch();
        ID batchProcessId2 = Database.executeBatch(zsUserBatch, 100);
        Test.stopTest();

    }
}