public with sharing class CompareDocsController {
    public string selectedDocument {get;set;}
    public string originalDocument {get;set;}
    public List<Selectoption> documentList{
        get{
            
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('--None--', '--None--'));
            for(SR_Attachments__c objDoc : [select Id
            									, Name 
            									, Case__c
            								from SR_Attachments__c
            								where Case__c=: strPageID]){
                
                options.add(new SelectOption(objDoc.Id , objDoc.Name ));
            }
            system.debug('?????options'+options);
            return options;
        }
        set;
    }
    
    String strPageID;
    public CompareDocsController(ApexPages.standardController controller){
    	strPageID = ApexPages.currentPage().getParameters().get('id');
		system.debug('maza strPageID====='+strPageID);
    }//End constructor
    
    public PageReference CompareDocs() {
    	system.debug('!!!!!!selectedDocument'+selectedDocument);
    	system.debug('!!!!!!originalDocument'+originalDocument);
    	list<Document_Accuracy__c> lstDocumentAccuracy = new list<Document_Accuracy__c>();
	    list<Error_Log__c> lstErrorLog = new list<Error_Log__c>();
    	if (originalDocument == '--None--') {
    		ApexPages.Message myMsg = 
    			new ApexPages.Message
    			(ApexPages.Severity.WARNING,'Original Document not selected. Request you to select same.');
            ApexPages.addMessage(myMsg);
            return null;
    	} else if (selectedDocument == '--None--') {
    		ApexPages.Message myMsg = 
    			new ApexPages.Message
    			(ApexPages.Severity.WARNING,'Comparison Document not selected. Request you to select same.');
            ApexPages.addMessage(myMsg);
            return null;
    	} else if (originalDocument == selectedDocument){
    		ApexPages.Message myMsg = 
    			new ApexPages.Message
    			(ApexPages.Severity.WARNING,'Same documents have been selected. Request you choose different document');
            ApexPages.addMessage(myMsg);
            return null;
    	} else {
    		String originalURL, comparisonURL;
    		system.debug('maza!!!!!!selectedDocument'+selectedDocument);
    		system.debug('maza!!!!!!originalDocument'+originalDocument);
			system.debug('maza!!!!!!strPageID'+strPageID);

    		if (originalDocument != '--None--') {
    			SR_Attachments__c originalDoc = [Select Id
    												, Name
    												, Attachment_URL__c
    												, Case__c
    											 From SR_Attachments__c
    											 Where Case__c =: strPageID
    											 And Id =: originalDocument];
 				if (originalDoc.Attachment_URL__c != null){
 					originalURL = originalDoc.Attachment_URL__c;
 				}
    		}
    		system.debug('maza originalURL====='+originalURL);
    		if (selectedDocument != '--None--'){
    			SR_Attachments__c selectedDoc = [Select Id
    												, Name
    												, Attachment_URL__c
    												, Case__c
    											 From SR_Attachments__c
    											 Where Case__c =: strPageID
    											 And Id =: selectedDocument];
 				if (selectedDoc.Attachment_URL__c != null){
 					comparisonURL = selectedDoc.Attachment_URL__c;
 				}
    		}
			system.debug('maza comparisonURL====='+comparisonURL);
			system.debug('maza strPageID====='+strPageID);
    		DocumentComparisonRestService.DocComparison objComparison;
    		if (!string.isBlank(originalURL) && !string.isBlank(comparisonURL)){
    			objComparison = DocumentComparisonRestService.compareDocs(originalURL, comparisonURL, strPageID);
    			system.debug('maza objComparison====='+objComparison);
    			if (objComparison != null){
	    			Document_Accuracy__c objDA = createDocumentAccRecord(objComparison, 
	    				originalURL, comparisonURL, strPageID);
	    			lstDocumentAccuracy.add(objDA);
	    		} else {
	    			Error_Log__c objError = createErrorLogRecord(strPageID);
	    			lstErrorLog.add(objError);
	    		}
    		}
    	}
    	if (!lstDocumentAccuracy.isEmpty()){
	    	insert lstDocumentAccuracy;
	    }
	    if (!lstErrorLog.isEmpty()){
	    	insert lstErrorLog;
	    }
    	return null;
    }
    
    public static Error_Log__c createErrorLogRecord(Id caseId) {
	 	Error_Log__c objError = new Error_Log__c();
	 	objError.Case__c = caseId;
	 	objError.Error_Details__c = 'Error in Document Comparison Service.';
	 	return objError;
	 }
	 
	 public static Document_Accuracy__c createDocumentAccRecord(DocumentComparisonRestService.DocComparison objComarison
	 	, String originalURL, String newURL, Id caseId){
	 	Document_Accuracy__c objDA = new Document_Accuracy__c();
	 	objDA.Accuracy__c = objComarison.accuracy;
	 	objDA.Additional_Characters__c = objComarison.addChar;
	 	objDA.Case__c = caseId;
	 	objDA.Deleted_Characters__c = objComarison.deleteChar;
	 	objDA.Difference_Report_URL__c = objComarison.diffReport;
	 	objDA.Original_Document_URL__c = originalURL;
	 	objDA.Page_Accuracy__c = objComarison.pageAccuracy;
	 	objDA.Signed_Document_URL__c = newURL;
	 	objDA.Total_Characters__c = objComarison.totalChar;
	 	return objDA;
	 }
}