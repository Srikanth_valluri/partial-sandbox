/****************************************************************************************************
* Name               : ValidateInquiryEmailAndNumbersBatch                                          *
* Description        : Batch to Udpate the Inquiry Number and Email Validity                        *
*                      This class calls the () from the phoneValidation class to validate           *
*                       the Inquiry record Phone Numbers                                            *
* Created Date       : 13-06-2018                                                                   *
* Created By         : ESPL                                                                         *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE            COMMENTS                                                *
* 1.0   Craig Lobo          13-06-2018      Initial Draft.                                          *
****************************************************************************************************/

global class ValidateInquiryEmailAndNumbersBatch implements Database.Batchable<sObject>, Database.AllowsCallouts { 

    String inquiryQuery = '';

    public ValidateInquiryEmailAndNumbersBatch(String pQuery) {

        System.debug('pQuery>>> ' + pQuery);
        if (String.isNotBlank(pQuery)) {
            inquiryQuery = pQuery;
            System.debug('PARAMETER>>> ' + pQuery);
        } else {
            inquiryQuery = ' SELECT Id FROM Inquiry__c '
                         + ' WHERE Validity__c = \'\' '
                         + ' OR BVEmailStatus__c = \'\' ';
            System.debug('DEFAULT>>> ' + pQuery);
        }
    }

    /**
    
     * Start Block: Fetch the records
     */
    global Database.QueryLocator start(Database.BatchableContext objBatchableContext) { 
        System.debug('inquiryQuery>>> ' + inquiryQuery);
        return Database.getQueryLocator(inquiryQuery);
    }

    /**
     * Execute Block: Process the records
     */
    global void execute(Database.BatchableContext objBatchableContext, List<Inquiry__c> listInquiry) {
        System.debug('ValidateInquiryEmailAndNumbersBatch>>> ' + listInquiry);
        Set<Id> inqIdSet = new Set<Id>();
        for (Inquiry__c inq : listInquiry) {
            inqIdSet.add(inq.Id);
        }
        ValidateInquiryEmailAndNumbers.verifyInqEmailAndNumbersFromWebService(inqIdSet);
    }

    /**
     * Finish Block: 
     */
    global void finish(Database.BatchableContext info){

    } 
}