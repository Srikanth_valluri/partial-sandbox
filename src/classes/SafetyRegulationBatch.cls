global class SafetyRegulationBatch implements Database.Batchable<sObject>,
                        Database.Stateful,Database.AllowsCallouts, Schedulable {

    global Database.QueryLocator start(Database.BatchableContext BC){
				String strQuery = 'SELECT id' +
				 													', Account__c' +
																	', Origin__c' +
																	', Request_Type_DeveloperName__c' +
																	', First_Name__c' +
																	', Last_Name__c' +
																	', Account__r.Name' +
																	', Booking_Unit__c' +
																	', Booking_Unit__r.Unit_Name__c' +
																	', Booking_Unit__r.Property_Name__c' +
                                  ', Tenant__c' +
                                  ', Tenant__r.Name' +
																	', (SELECT id' +
																						 ', Name' +
																			' FROM Attachments' +
																			' WHERE Name LIKE \'' + 'Safety Undertaking Letter%' + '\')' +
													' FROM FM_Case__c' +
													' WHERE Origin__c IN (\'' + 'Portal' + '\',\'' + 'Portal - Guest' + '\')' +
													' AND Request_Type_DeveloperName__c IN (\'' + 'Tenant_Registration' + '\',\'' +
																																				'Move_in_Request_T' + '\',\'' +
																																				'Move_in_Request' + '\')' +
													' AND Status__c = \'' + 'Submitted' + '\'';
				System.debug('--- strQuery --- : '+strQuery);
				return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<FM_Case__c> lstFmCases){
				System.debug('lstFmCases : '+lstFmCases);
				List<Attachment> lstAttachment = new List<Attachment>();
				String termsLink = '';
				if(!lstFmCases.isEmpty()) {
					for(FM_Case__c objFMCase : lstFmCases) {
							if(objFMCase.Attachments.size() == 0) {
								System.debug('--- objFMCase --- : '+objFMCase);
								String tenantName = '';
								if(objFMCase.First_Name__c != NULL && objFMCase.Last_Name__c != NULL) {
									tenantName = objFMCase.First_Name__c + ' ' +objFMCase.Last_Name__c;
								}
								System.debug('--- tenantName --- : '+tenantName);
								if(objFMCase != NULL && objFMCase.id != NULL) {
									System.debug('---objFMCase Id---: '+objFMCase.id);
									if(objFMCase.Request_Type_DeveloperName__c == 'Tenant_Registration' && String.isNotBlank(tenantName)) {
										termsLink = '/apex/safetyregulationspage?CaseId='+objFMCase.id+'&tenantName='+tenantName;
									}else {
										termsLink = '/apex/safetyregulationspage?CaseId='+objFMCase.id;
									}
								}
								System.debug('>>>---termsLink--- : '+termsLink);

								PageReference pageRef = new PageReference(termsLink);
								System.debug('>>>---pageRef--- : '+pageRef);
								Blob pdf1;
								if(Test.isRunningTest()) {
										System.debug('--- If test ---');
										pdf1 = blob.valueOf('Unit.Test');
								}
								else {
										System.debug('--- else if code ---');
										try{
												//pdf1 = pageRef.getcontentAsPDF();
                        pdf1 = pageRef.getcontent();
										}
										catch (Exception e) {
												System.debug('Exception : '+e);
												pdf1 = blob.valueOf('data');
										}
								}
								System.debug('>>>---Content--- : '+ pdf1);

                if(objFmCase.Request_Type_DeveloperName__c == 'Move_in_Request_T'
                      &&  objFMCase.Tenant__c != NULL) {
                  Attachment objAttachment = new Attachment();
                  objAttachment.Body = pdf1;
                  objAttachment.Name = 'Safety Undertaking Letter.pdf';
                  objAttachment.IsPrivate = false;
                  objAttachment.ParentId = objFMCase.Tenant__c;
                  lstAttachment.add(objAttachment);
                }else if(objFMCase.Account__c != NULL){
                  Attachment objAttachment = new Attachment();
                  objAttachment.Body = pdf1;
                  objAttachment.Name = 'Safety Undertaking Letter.pdf';
                  objAttachment.IsPrivate = false;
                  objAttachment.ParentId = objFMCase.Account__c;
                  lstAttachment.add(objAttachment);
                }

                if(objFMCase != NULL){
                  Attachment objAttachment2 = new Attachment();
  								objAttachment2.Body = pdf1;
  								objAttachment2.Name = 'Safety Undertaking Letter.pdf';
  								objAttachment2.IsPrivate = false;
  								objAttachment2.ParentId = objFMCase.Id;
  								lstAttachment.add(objAttachment2);
                }
						}
					}
					System.debug('---lstAttachment--- : '+lstAttachment);
				}

				if(!lstAttachment.isEmpty() && lstAttachment.size() > 0) {
						insert lstAttachment;
				}
				System.debug('---lstAttachment--- : '+lstAttachment);
	  }

    global void finish(Database.BatchableContext BC){
        System.debug('***SafetyRegulationBatch finish****');
    }

    global void execute(SchedulableContext sc) {
        SafetyRegulationBatch objSafetyRegulationBatch = new SafetyRegulationBatch();
        Database.executebatch(objSafetyRegulationBatch);
    }
}