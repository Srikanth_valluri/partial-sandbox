@isTest
private class GenericRecordDetailsDataProviderTest {
    @isTest
    static void testGetData() {
        
        List<Map<String,String>> mapLst; 
        Account account = new Account(Name = 'Test');
        insert account;

        mapLst = new List<Map<String, String>> {
            new Map<String, String> {
                'field'=> 'Name',
                'title'=> 'Name'
            }
        };

        DataDisplayConfig config = new DataDisplayConfig();
        config.recordId = account.Id;
        config.displayAs = 'recordDetail';
        config.fieldset = NULL;
        config.params = NULL;
        config.fieldList = new List<Map<String, String>> {
            new Map<String, String> {
                'field'=> 'Id',
                'title'=> 'Id'
            }
        };

        config.mapRecordTypeToFieldList = new Map<String, List<Map<String, String>>> {
                'Master'=> mapLst
            };

        config.detailFieldList = new List<Map<String, String>> {
            new Map<String, String> {
                'field'=> 'Name',
                'title'=> 'Name'
            }
        };
        config.objectName = 'Account';
        new GenericRecordDetailsDataProvider().getData(config);
    }
}