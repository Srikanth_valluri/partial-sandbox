/*
  For sending Case details to mobile app
*/

@RestResource(urlMapping='/getCaseDetails/*')
 Global class SendCaseDetailsToMobileApp
 {
     @HtTPPost
     Global static List<Case> SendCaseDetails(String createdbyId, String status)
     {
        List<Case> caseList=[SELECT      Id,
                                CaseNumber,
                                CreatedDate,
                                Type,
                                SR_type__c,
                                RecordType.Name,
                                Description,
                                Contact.Name,
                                Account.Name,
                                Booking_Unit__r.Unit_Name__c,
                                Account.AnnualRevenue,
                                AccountId,
                                ContactId,
                                Origin,
                                Priority,
                                Status,
                                (
                                SELECT  Id,
                                        Booking_Unit__r.Unit_Name__c
                                FROM    SR_Booking_Units__r
                                ) 
                      FROM Case
                    WHERE createdbyId =:createdbyId and Status=:status  ORDER BY CreatedDate desc];
         return caseList; 
     }
 }