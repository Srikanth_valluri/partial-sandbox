/**************************************************************************************************
 * @Name              : DAMAC_IPMS_PARTY_CREATION_RESPONSE
 * @Test Class Name   : DAMAC_IPMS_PARTY_CREATION_TEST
 * @Description       : JSON Class for DAMAC_IPMS_PARTY_CREATION_RESPONSE
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0                        14/08/2019       Created
 * 1.1         QBurst         03/02/2020       Modifications for DP Deals  [SOAP to REST]  
**************************************************************************************************/
public class DAMAC_IPMS_PARTY_CREATION_RESPONSE{

    public class ResponseLines {
        public String bookingLocation;
        public String partyId;
        public String extCustomerNumber;
        public String status;
        public String startTime;
        public String endTime;
        public List<LineStatus> lineStatus;
    }

    public String responseId;
    public String responseTime;
    public String status;
    public String responseMessage;
    public Integer elapsedTimeMs;
    public List<ResponseLines> responseLines;
    public Boolean complete;

    public class LineStatus {
        public String status;
        public String message;
        public String entity;
        public String entityId;
    }

    public static DAMAC_IPMS_PARTY_CREATION_RESPONSE parse(String json) {
        return (DAMAC_IPMS_PARTY_CREATION_RESPONSE) System.JSON.deserialize(json, DAMAC_IPMS_PARTY_CREATION_RESPONSE.class);
    }
}