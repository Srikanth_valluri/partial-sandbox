public without sharing class DailyBatchforEmails implements Database.Batchable<sObject>, Database.AllowsCallouts,Database.Stateful {

    String query, dayOfWeek, strDay;
    Date today = Date.today();
    public list<SMS_History__c> lstSMSHistory ;
    public Database.QueryLocator start(Database.BatchableContext BC) {
        Datetime dt = DateTime.newInstance(today, Time.newInstance(0, 0, 0, 0));
        dayOfWeek=dt.format('EEEE');
        System.debug('Day : ' + dayOfWeek);
        Integer day = today.day();
        system.debug('!!!!day'+day);
        strDay = string.valueOf(day);
        query = 'Select Id, SMS_Template__c, Booking_Unit__c, Comminication_Type__c, Email_Engine__c, Email_Template__c, Frequency__c, From_Address__c, Booking_Unit__r.Booking__r.Account__r.isPersonAccount,';
        query += 'Booking_Unit__r.Booking__r.Account__c, Booking_Unit__r.Booking__r.Account__r.Email__pc, Booking_Unit__r.Booking__r.Account__r.Mobile_Phone_Encrypt__pc,Booking_Unit__r.Booking__r.Account__r.ZBEmailStatus__c,';
        query += 'Booking_Unit__r.Booking__r.Account__r.Email__c, Schedule_Day_of_Month__c, Scheduled_Date__c, Scheduled_Time__c, Scheule_Day__c, Last_Updated__c From Schedule_Email__c';
        system.debug('!!!!!query'+query);
        query += ' Where Last_Updated__c != Today And (Frequency__c = \''+'Daily' +'\' OR Scheduled_Date__c = Today OR Scheule_Day__c =\''+ dayOfWeek+ '\' OR Schedule_Day_of_Month__c =\''+strDay+ '\') ';
        query += 'And Booking_Unit__r.Booking__r.Account__r.ZBEmailStatus__c = \''+ String.escapeSingleQuotes('Valid')+ '\'';
        system.debug('^^^query'+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<Schedule_Email__c> scope) {
        list<String> lstFromAddress = new list<String>();
        lstSMSHistory = new list<SMS_History__c>();
        map<String, string> mapBUIdSchedIdTemplate = new map<String, string>();
        map<String, string> mapBUIdSMSTemplate = new map<String, String>();
        map<String, EmailTemplate> mapEmailTemp = new map<String, EmailTemplate>();
        map<String, String> mapBUIDTempBody = new map<String, String>();
        map<String, String> mapBUIDTempSubj = new map<String, String>();
        map<String, String> mapbuIdMergBody = new map<String, String>();
        map<String, String> mapbuIdMergSubj = new map<String, String>();
        map<String, Id> mapOrgAddId = new map<String, Id>();
        
        for (Schedule_Email__c objEmail : scope) {
            if (objEmail.Email_Template__c != null) {
                mapBUIdSchedIdTemplate.put(objEmail.Booking_Unit__c+'-'+objEmail.Email_Template__c, objEmail.Email_Template__c);
            }
            if (objEmail.SMS_Template__c != null) {
                mapBUIdSchedIdTemplate.put(objEmail.Booking_Unit__c+'-'+objEmail.SMS_Template__c, objEmail.SMS_Template__c);
            }
            lstFromAddress.add(objEmail.From_Address__c);
            //mapBUIdSMSTemplate.put(objEmail.Booking_Unit__c, objEmail.SMS_Template__c);
        }
        
        for (OrgWideEmailAddress objAdd: [select Id, Address from OrgWideEmailAddress where Address =: lstFromAddress]) {
            mapOrgAddId.put(objAdd.Address, objAdd.Id);
        }
        for (EmailTemplate selectedEmailTemplate : [SELECT ID, Subject, Body, HtmlValue, TemplateType, Markup, DeveloperName 
                                                    FROM EmailTemplate 
                                                    WHERE DeveloperName =:mapBUIdSchedIdTemplate.values()
                                                    ]) {
            mapEmailTemp.put(selectedEmailTemplate.DeveloperName, selectedEmailTemplate);
        }
        
        for (String buID : mapBUIdSchedIdTemplate.keyset()) {
            if(mapEmailTemp != null && mapBUIdSchedIdTemplate != null && mapBUIdSchedIdTemplate.containsKey(buID) &&
                mapEmailTemp.containsKey(mapBUIdSchedIdTemplate.get(buID))) {
                if(mapEmailTemp.get(mapBUIdSchedIdTemplate.get(buID)).TemplateType == 'text') {
                    mapBUIDTempBody.put(buID, mapEmailTemp.get(mapBUIdSchedIdTemplate.get(buID)).Body);
                } else if (mapEmailTemp.get(mapBUIdSchedIdTemplate.get(buID)).TemplateType == 'custom' ||
                    mapEmailTemp.get(mapBUIdSchedIdTemplate.get(buID)).TemplateType == 'html'){
                    mapBUIDTempBody.put(buID, mapEmailTemp.get(mapBUIdSchedIdTemplate.get(buID)).HtmlValue);
                } 
                system.debug('^^^^mapBUIDTempBody'+mapBUIDTempBody);
                mapBUIDTempSubj.put(buID, mapEmailTemp.get(mapBUIdSchedIdTemplate.get(buID)).Subject);
            }
        }
        mapbuIdMergBody = MergeFields.parse(mapBUIDTempBody);
        mapbuIdMergSubj = MergeFields.parse(mapBUIDTempSubj);
        system.debug('!!!!!!!!scope'+scope);
        list<Schedule_Email__c> lstEmails = new list<Schedule_Email__c>();
        list<Messaging.SingleEmailMessage> messages =  new list<Messaging.SingleEmailMessage>();
        list<EmailWrapper.EmailDetails> lstSGMails = new list<EmailWrapper.EmailDetails>();
        DateTime currentTime = System.Now();
        String strHR = currentTime.format('HH:mm');
        string minutes = strHR.substringAfter(':');
        strHR = strHR.replace(minutes , '00');
        system.debug('!!!strHR'+strHR);
        for (Schedule_Email__c objEmail : scope) {
            if ((objEmail.Frequency__c == 'Daily' && objEmail.Scheduled_Time__c == Null) ||
                (objEmail.Frequency__c == 'Weekly' && objEmail.Scheule_Day__c == dayOfWeek) ||
                (objEmail.Frequency__c == 'Monthly' && objEmail.Schedule_Day_of_Month__c == strDay) ||
                (objEmail.Frequency__c == 'On Date /Time' && objEmail.Scheduled_Time__c == Null && objEmail.Scheduled_Date__c == today)
            ) {
                system.debug('!!!!!!inside if');
                String toMail = objEmail.Booking_Unit__r.Booking__r.Account__r.Email__pc;
                String phoneNo = objEmail.Booking_Unit__r.Booking__r.Account__r.Mobile_Phone_Encrypt__pc;
                String fromMail = mapOrgAddId.get(objEmail.From_Address__c);
                String emailContent = mapbuIdMergBody.get(objEmail.Booking_Unit__c+'-'+objEmail.Email_Template__c);
                String smsContent;
                if (objEmail.SMS_Template__c != null) {
                    smsContent = mapbuIdMergBody.get(objEmail.Booking_Unit__c+'-'+objEmail.SMS_Template__c );
                }
                system.debug('!!!!!!!!!!!mapbuIdMergBody'+mapbuIdMergBody);
                system.debug('!!!!!!!!!!!mapbuIdMerg'+mapbuIdMergBody.get(objEmail.Booking_Unit__c));
                system.debug('!!!!!!!!!!!emailContent'+emailContent);
                String emailSub = mapbuIdMergSubj.get(objEmail.Booking_Unit__c+'-'+objEmail.Email_Template__c);
                String accountId = objEmail.Booking_Unit__r.Booking__r.Account__c;
                String contentType = mapEmailTemp.get(objEmail.Email_Template__c).TemplateType;
                if (objEmail.Email_Engine__c == 'Salesforce') {
                    Messaging.SingleEmailMessage Email = sendMail(toMail , fromMail, emailContent, emailSub, 
                    objEmail.Booking_Unit__c, accountId, contentType);
                    messages.add(Email);
                } else if (objEmail.Email_Engine__c == 'SendGrid') {
                    EmailWrapper.EmailDetails SGmail = new EmailWrapper.EmailDetails();
                    SGmail.toAddress = toMail;
                    SGmail.toName = '';
                    SGmail.ccAddress = '';
                    SGmail.ccName = '';
                    SGmail.bccAddress = '';
                    SGmail.bccName = '';
                    SGmail.subject = emailSub;
                    SGmail.substitutions = '';
                    SGmail.fromAddress = objEmail.From_Address__c;
                    SGmail.fromName = '';
                    SGmail.replyToAddress = '';
                    SGmail.replyToName = '';
                    if (contentType == 'text') {
                        SGmail.contentType = 'text/plain';
                    } else if (contentType == 'html' || contentType == 'custom') {
                        SGmail.contentType = 'text/html';
                    }
                    SGmail.contentValue = emailContent;
                    SGmail.templateId = '';
                    SGmail.attachmentContentType = null;
                    SGmail.attachmentName = '';
                    SGmail.accountId = accountId;
                    SGmail.fromId = fromMail;
                    lstSGMails.add(SGmail);
                }
                objEmail.Last_Updated__c = System.Today();
                lstEmails.add(objEmail);
                if (objEmail.Comminication_Type__c == 'Email & SMS' || objEmail.Comminication_Type__c == 'SMS Only') {
                    lstSMSHistory.add(new SMS_History__c(Customer__c = accountId, Is_SMS_Sent__c = false,
                            Phone_Number__c = phoneNo, Message__c = smsContent ));
                }
            } else if ((objEmail.Frequency__c == 'Daily' && objEmail.Scheduled_Time__c != Null) ||
               (objEmail.Frequency__c == 'On Date /Time' && objEmail.Scheduled_Time__c != Null && objEmail.Scheduled_Date__c == today)
                ) {                    
                if (objEmail.Scheduled_Time__c == strHR ) {
                    system.debug('!!!!!!inside else');
                    String toMail = objEmail.Booking_Unit__r.Booking__r.Account__r.Email__pc;
                    String phoneNo = objEmail.Booking_Unit__r.Booking__r.Account__r.Mobile_Phone_Encrypt__pc;
                    String fromMail = mapOrgAddId.get(objEmail.From_Address__c);
                    String emailContent = mapbuIdMergBody.get(objEmail.Booking_Unit__c+'-'+objEmail.Email_Template__c);
                    String smsContent;
                    if (objEmail.SMS_Template__c != null) {
                        smsContent = mapbuIdMergBody.get(objEmail.Booking_Unit__c+'-'+objEmail.SMS_Template__c );
                    }
                    String emailSub = mapbuIdMergSubj.get(objEmail.Booking_Unit__c+'-'+objEmail.Email_Template__c);
                    String accountId = objEmail.Booking_Unit__r.Booking__r.Account__c;
                    String contentType = mapEmailTemp.get(objEmail.Email_Template__c).TemplateType;
                    if (objEmail.Email_Engine__c == 'Salesforce') {
                        Messaging.SingleEmailMessage Email = sendMail(toMail , fromMail, emailContent, emailSub, 
                            objEmail.Booking_Unit__c, accountId, contentType);
                        messages.add(Email);
                    } else if (objEmail.Email_Engine__c == 'SendGrid') {
                        EmailWrapper.EmailDetails SGmail = new EmailWrapper.EmailDetails();
                        SGmail.toAddress = toMail;
                        SGmail.toName = '';
                        SGmail.ccAddress = '';
                        SGmail.ccName = '';
                        SGmail.bccAddress = '';
                        SGmail.bccName = '';
                        SGmail.subject = emailSub;
                        SGmail.substitutions = '';
                        SGmail.fromAddress = objEmail.From_Address__c;
                        SGmail.fromName = '';
                        SGmail.replyToAddress = '';
                        SGmail.replyToName = '';
                        if (contentType == 'text') {
                            SGmail.contentType = 'text/plain';
                        } else if (contentType == 'html' || contentType == 'custom') {
                            SGmail.contentType = 'text/html';
                        }
                        SGmail.contentValue = emailContent;
                        SGmail.templateId = '';
                        SGmail.attachmentContentType = null;
                        SGmail.attachmentName = '';
                        SGmail.accountId = accountId;
                        SGmail.fromId = fromMail;
                        lstSGMails.add(SGmail);
                    }
                    objEmail.Last_Updated__c = System.Today();
                    lstEmails.add(objEmail);
                    if (objEmail.Comminication_Type__c == 'Email & SMS' || objEmail.Comminication_Type__c == 'SMS Only') {
                        lstSMSHistory.add(new SMS_History__c(Customer__c = accountId, Is_SMS_Sent__c = false,
                                Phone_Number__c = phoneNo, Message__c = smsContent ));
                    }
                }
            }
        }
        
        try {
            system.debug('!!!!!messages'+messages);
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);//Sending emails
            system.debug('!!!!!!!!results'+results );
            //map<String, String> mapSendGridResponse = new map<String, String>();
            list<EmailMessage> lstActHistory = new list<EmailMessage>();
            if (lstSGMails != null && lstSGMails.size() > 0) {
                for (EmailWrapper.EmailDetails objMail : lstSGMails) {
                     SendGrid sendgrid = new SendGrid();
                    
                    SendGrid.email email = new SendGrid.Email();
                    email.addTo(objMail.toAddress);
                    email.addBcc(objMail.bccAddress);
                    email.setFrom(objMail.fromName);
                    email.setSubject(objMail.subject);
                    if (objMail.contentType == 'text/plain') {
                        email.setText(objMail.contentValue);
                    } else if (objMail.contentType == 'text/html') {
                        email.setHtml(objMail.contentValue);
                    }
                    
                    SendGrid.SendGridResponse response = sendgrid.send(email);
                     /*String strResponse = SendGridEmailService.sendEmailService(objMail.toAddress, objMail.toName, objMail.ccAddress, 
                        objMail.ccName, objMail.bccAddress, objMail.bccName, objMail.subject, objMail.substitutions, objMail.fromAddress,
                        objMail.fromName, objMail.replyToAddress, objMail.replyToName, objMail.contentType, objMail.contentValue, objMail.templateId,
                        objMail.attachmentContentType, objMail.attachmentName);*/
                    /*if (response.success == True) {
                        EmailMessage mail = new EmailMessage();
                        mail.Subject = objMail.subject;
                        mail.MessageDate = System.Today();
                        mail.Status = 'Sent';
                        mail.RelatedToId = objMail.accountId;
                        mail.ToAddress = objMail.toAddress;
                        mail.FromAddress = objMail.fromAddress;
                        mail.TextBody = objMail.contentValue;
                        mail.CcAddress = objMail.ccAddress;
                        mail.BccAddress = objMail.bccAddress;
                        lstActHistory.add(mail);
                    }  */
                }                
            }
            if (lstEmails != null && lstEmails.size() > 0) {
                update lstEmails;
            }
            /*if (lstActHistory != null && lstActHistory.size() > 0) {
                insert lstActHistory;
            } */ 
        
        } catch (Exception e) {
            System.debug('e.getMessage()'+ e.getMessage());
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        if (lstSMSHistory != null && lstSMSHistory.size()>0) {
            insert lstSMSHistory;
            Database.executeBatch(new CustomerNotificationSendSMSBatch(lstSMSHistory),90);
        }
    }
    
    public Messaging.SingleEmailMessage sendMail(String strMail, String fromAddress, String mailBody, String mailSubject, 
        String buId, string accId, string contentType ) {
        String mailMessage= '';
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] {strMail};
        message.subject = mailSubject;
        mailMessage = mailBody;
        if (contentType == 'html' || contentType == 'custom') {
            message.setHtmlBody(mailMessage);
        } else if (contentType == 'text'){
            message.setPlainTextBody(mailMessage);
        }
        message.setSaveAsActivity(true);
        message.setWhatId(accId);
        return message;
    }
}