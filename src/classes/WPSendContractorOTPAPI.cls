@RestResource(urlMapping='/wpsendcontractorotp/*')
global class WPSendContractorOTPAPI {
    public static String responseMessage = 'Please enter the verification code sent on your registered mobile number';
    
    @HttpGet
    global static FinalReturnWrapper getContractorOTP() {
        
        RestRequest r = RestContext.request;
        System.debug('Request Headers:'+r.headers);
        System.debug('Request params:'+r.params);
        
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        HDApp_Utility.cls_meta_data objMeta = new HDApp_Utility.cls_meta_data();
        
        if(!r.params.containsKey('accountId') || (r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId')))) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No account_id found in request',3);
            return returnResponse;
        }
        else if(!r.params.containsKey('fmCaseNumber') || (r.params.containsKey('fmCaseNumber') && String.isBlank(r.params.get('fmCaseNumber')))) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No fmCaseNumber found in request',3);
            return returnResponse;
        }
        
        Integer statusCode;
        
        try {
            String accountId = r.params.get('accountId');
            String fmCaseNumber = r.params.get('fmCaseNumber');
            
            List<FM_Case__c> listFMCase = [SELECT Id,Name 
                                           FROM FM_Case__c 
                                           WHERE Name LIKE :fmCaseNumber];
            
            if(listFMCase != NULL && !listFMCase.isEmpty()) {
                String otpCode = String.valueOf((Math.random() * 10000).intValue()).leftPad(4, '0');
                System.debug('otpCode:'+otpCode);
                
            	responseMessage += ' ';
                
                statusCode = 1;
            }
            else {
                objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No FM Case found for given fmCaseNumber',6);
                
                returnResponse.meta_data = objMeta;
                
                return returnResponse;
            }
        }
        catch(Exception e) {
            System.debug('Exception:'+e.getMessage());
            
            statusCode = 6;
        }
        
        if(statusCode == 1) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.successMsg,responseMessage, 1);
        }
        else {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'Error in generating OTP',6);
        }
        
        returnResponse.meta_data = objMeta;
        
        System.debug('returnResponse:'+returnResponse);
        
        return returnResponse;
    }
  	
    global class FinalReturnWrapper {
        public String data;
        public HDApp_Utility.cls_meta_data meta_data;
    }
}