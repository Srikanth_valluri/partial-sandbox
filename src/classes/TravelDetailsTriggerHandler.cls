public with sharing class TravelDetailsTriggerHandler implements TriggerFactoryInterface{

    public void executeBeforeInsertTrigger(list<sObject> lstNewRecords){}
    public void executeBeforeUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        System.debug('executeBeforeUpdateTrigger mapNewRecords>>>>> ' + mapNewRecords); 
        ValidateTravelDetails.validateTravelDetailsPOP(mapNewRecords);
    }
    public void executeBeforeDeleteTrigger(Map<Id, sObject> mapOldRecords){}
    public void executeBeforeInsertUpdateTrigger(list<sObject> lstNewRecords, Map<Id,sObject> mapOldRecords){}
    
    //After Trigger Methods
    public void executeAfterInsertTrigger(Map<Id, sObject> mapNewRecords){}
    public void executeAfterUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
    public void executeAfterDeleteTrigger(Map<Id, sObject> mapOldRecords){}
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}

}