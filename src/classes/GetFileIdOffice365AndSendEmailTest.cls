@isTest
public class GetFileIdOffice365AndSendEmailTest {
	@isTest
    static void testMethod1(){
		Account objAccount = new Account();
        objAccount.LastName = 'test';
        objAccount.Email__c = 'success@mailinator.com';
        objAccount.Party_ID__c = '1234';
        insert objAccount;
        
        Case objCase = new Case();
        insert objCase;
        
        Location__c objLocation = new Location__c();
        objLocation.Name = 'test';
        objLocation.Location_ID__c = '12345';
        insert objLocation;
        
        Inventory__c objInventory = new Inventory__c();
        objInventory.Building__c = objLocation.Id;
        insert objInventory;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id; 
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = '3901';
        objBU.Unit_Name__c = 'test/test';
        objBU.Booking__c = objBooking.Id;
        objBU.Recovery__c = 'A';
        //objBU.EHO_Notice_Sent__c = true;
        objBU.Inventory__c = objInventory.Id;
        insert objBU;
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        
		Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        //Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositiveForWhtsApp());
        
		System.Test.startTest();		
			GetFileIdOffice365AndSendEmailToCustomer.getFileAndReturnAttachment('test.png','');
		System.Test.stopTest();
	}	
    
    	@isTest
    static void testMethod2(){
		Account objAccount = new Account();
        objAccount.LastName = 'test';
        objAccount.Email__c = 'success@mailinator.com';
        objAccount.Party_ID__c = '1234';
        insert objAccount;
        
        Case objCase = new Case();
        insert objCase;
        
        Location__c objLocation = new Location__c();
        objLocation.Name = 'test';
        objLocation.Location_ID__c = '12345';
        insert objLocation;
        
        Inventory__c objInventory = new Inventory__c();
        objInventory.Building__c = objLocation.Id;
        insert objInventory;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id; 
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = '3901';
        objBU.Unit_Name__c = 'test/test';
        objBU.Booking__c = objBooking.Id;
        objBU.Recovery__c = 'A';
        //objBU.EHO_Notice_Sent__c = true;
        objBU.Inventory__c = objInventory.Id;
        insert objBU;
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
		
		List<Attachment> lstAttach = new List<Attachment>();
		
		Attachment attach = new Attachment();
		attach.contentType = 'application/pdf';
		attach.name = 'Test.png';
		attach.body = blob.valueOf('test');
		lstAttach.add(attach);  
        
		Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        Booking_Unit__c objBU1 = [Select Id,
                                          Registration_ID__c,
                                  		  Unit_Name__c,
                                          Booking__c,Booking__r.Account__r.Name,
                                  		  Booking__r.Account__r.Email__pc
                                  from Booking_Unit__c where Id =:objBU.Id limit 1];
		System.Test.startTest();		
			GetFileIdOffice365AndSendEmailToCustomer.sendEmailToCustomer(objBU1,lstAttach,'Hospitality_Template','M11','','2020');
		System.Test.stopTest();
	}
    
        @isTest
    static void testMethod3(){
		Account objAccount = new Account();
        objAccount.LastName = 'test';
        objAccount.Email__c = 'success@mailinator.com';
        objAccount.Party_ID__c = '1234';
        insert objAccount;
        
        Case objCase = new Case();
        insert objCase;
        
        Location__c objLocation = new Location__c();
        objLocation.Name = 'test';
        objLocation.Location_ID__c = '12345';
        insert objLocation;
        
        Inventory__c objInventory = new Inventory__c();
        objInventory.Building__c = objLocation.Id;
        insert objInventory;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id; 
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = '3901';
        objBU.Unit_Name__c = 'test/test';
        objBU.Booking__c = objBooking.Id;
        objBU.Recovery__c = 'A';
        //objBU.EHO_Notice_Sent__c = true;
        objBU.Inventory__c = objInventory.Id;
        insert objBU;
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
		
		List<Attachment> lstAttach = new List<Attachment>();
		
		Attachment attach = new Attachment();
		attach.contentType = 'application/pdf';
		attach.name = 'Test.png';
		attach.body = blob.valueOf('test');
		lstAttach.add(attach);  
        
		Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositiveForWhtsApp());
        Booking_Unit__c objBU1 = [Select Id,
                                          Registration_ID__c,
                                  		  Unit_Name__c,
                                          Booking__c,Booking__r.Account__r.Name,
                                  		  Booking__r.Account__r.Email__pc
                                  from Booking_Unit__c where Id =:objBU.Id limit 1];
		System.Test.startTest();		
			GetFileIdOffice365AndSendEmailToCustomer.sendEmailToCustomer(objBU1,lstAttach,'Hospitality_Template','','Q2','2020');
		System.Test.stopTest();
	}	
}