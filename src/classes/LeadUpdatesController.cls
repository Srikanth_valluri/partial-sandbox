public with sharing class LeadUpdatesController {
    public Inquiry__c inq{get; set;}
    public LeadUpdatesController(ApexPages.StandardController sc) {
        inq = (Inquiry__c)sc.getRecord();
    }

    public pagereference updateInquiry(){
        try{
            update inq;
            return new pagereference('/'+inq.Id);
        }
        catch(Exception e){
            Apexpages.addMessage(new Apexpages.message(ApexPages.severity.fatal, e.getMessage()));
        }
        return null;
    }
}