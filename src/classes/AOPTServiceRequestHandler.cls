global class AOPTServiceRequestHandler 
{
    @InvocableMethod
    public static void applyNewPaymentTerms(List<Case> lstSR)
    {

        system.debug('method called');
        Case objCase = new Case();
        String caseId;
        if(lstSR != null && lstSR.size() > 0)
        {
            //Case objCase = lstSR.get(0);
            objCase = lstSR.get(0);
            caseId = objCase.Id;
        }
        
        if(String.isNotBlank(caseId))
        {
            makeCallout(caseId);
        }
    }

    // method to create new PT in IPMS and if it is successfull then create same in SF.
    // using future method as uncommited work pending error was coming
    @future(callout=true)
    public static void makeCallout(String strCaseID)
    {
        try
        {
            Case objCase = [ SELECT Id, NewPaymentTermJSON__c,CaseNumber, Owner.Name
                             ,RecordType.Name,Status,Account.Party_ID__c,CreatedDate
                             ,(Select Id,Booking_Unit__c,Booking_Unit__r.Registration_ID__c FROM SR_Booking_Units__r)
                             FROM Case WHERE ID =: strCaseID
                           ];
            if(objCase != null && String.isNotBlank(objCase.NewPaymentTermJSON__c))
            {
                List<AOPTServiceRequestControllerLDS.SelectedNewPaymentTermsByCRE>lstSelectedNewPaymentTermsByCRE 
                = (List<AOPTServiceRequestControllerLDS.SelectedNewPaymentTermsByCRE>)JSON.deserialize(objCase.NewPaymentTermJSON__c,List<AOPTServiceRequestControllerLDS.SelectedNewPaymentTermsByCRE>.class);
                system.debug('lstSelectedNewPaymentTermsByCRE '+lstSelectedNewPaymentTermsByCRE.size());
                
                
                Set<Id> bookingUnitIDSet = new Set<Id>();
                List<String> lstRegIds = new List<String>();

                List<SR_Booking_Unit__c> lstSRBU = [select Id,Booking_Unit__c,Booking_Unit__r.Unit_Selling_Price_AED__c,Registration_ID__c from SR_Booking_Unit__c where Case__c =: strCaseID];
                system.debug('lstSRBU '+lstSRBU.size());
                Map<Id,String> responseBUMap = new Map<Id,String>();
                
                paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5 [] lstPaymentTermInIPMS  = new paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5[lstSelectedNewPaymentTermsByCRE.size()];
                
                for(Integer i=0 ; i < lstSRBU.size();i++)
                {
                    lstRegIds.add(lstSRBU[i].Registration_ID__c);
                    bookingUnitIDSet.add(lstSRBU[i].Booking_Unit__c);
                    Integer index =0;
                    String strResponse;
                    for(Integer j=0 ;j < lstSelectedNewPaymentTermsByCRE.size();j++)
                    {
                        paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5 objIPMSPT = new paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5();
                        
                        objIPMSPT.DESCRIPTION = lstSelectedNewPaymentTermsByCRE[j].description;
                        system.debug('lstSelectedNewPaymentTermsByCRE[j].paymentDate '+lstSelectedNewPaymentTermsByCRE[j].paymentDate);
                        if(lstSelectedNewPaymentTermsByCRE[j].blnNewTerm != null && lstSelectedNewPaymentTermsByCRE[j].blnNewTerm)
                        {   
                            system.debug('lstSelectedNewPaymentTermsByCRE[j].paymentDate 1 : '+lstSelectedNewPaymentTermsByCRE[j].paymentDate);
                            if(String.isNotBlank(lstSelectedNewPaymentTermsByCRE[j].paymentDate))
                            {
                                objIPMSPT.PAYMENT_DATE = AOPTUtility.formatDate((lstSelectedNewPaymentTermsByCRE[j].paymentDate));
                            }
                            system.debug('objIPMSPT.PAYMENT_DATE 2 : '+objIPMSPT.PAYMENT_DATE);                            
                            objIPMSPT.LINE_ID = lstSelectedNewPaymentTermsByCRE[j].strLineID;
                            objIPMSPT.TERM_ID = lstSelectedNewPaymentTermsByCRE[j].strTermID;
                        }
                        else
                        {
                            //objIPMSPT.PAYMENT_DATE = lstSelectedNewPaymentTermsByCRE[j].paymentDate;
                            system.debug('lstSelectedNewPaymentTermsByCRE[j].paymentDate 3 : '+lstSelectedNewPaymentTermsByCRE[j].paymentDate);                       
                            if(String.isNotBlank(lstSelectedNewPaymentTermsByCRE[j].paymentDate) &&  AOPTUtility.checkDateFormate(lstSelectedNewPaymentTermsByCRE[j].paymentDate))
                            {
                                objIPMSPT.PAYMENT_DATE = AOPTUtility.formatDate((lstSelectedNewPaymentTermsByCRE[j].paymentDate));
                            }
                            else
                            {
                               objIPMSPT.PAYMENT_DATE = lstSelectedNewPaymentTermsByCRE[j].paymentDate;
                            }
                            system.debug('objIPMSPT.PAYMENT_DATE 4 : '+objIPMSPT.PAYMENT_DATE);                            
                        }
                        objIPMSPT.INSTALLMENT = lstSelectedNewPaymentTermsByCRE[j].installment;

                        
                        objIPMSPT.MILESTONE_EVENT = lstSelectedNewPaymentTermsByCRE[j].mileStoneEvent;
                        objIPMSPT.PERCENT_VALUE = lstSelectedNewPaymentTermsByCRE[j].percentValue;
                        objIPMSPT.REGISTRATION_ID = lstSRBU[i].Registration_ID__c;
                        if(lstSRBU[i].Booking_Unit__r.Unit_Selling_Price_AED__c != null)
                        {
                            Decimal decPercent = Decimal.valueOf(lstSelectedNewPaymentTermsByCRE[j].percentValue);
                            Decimal decAmount = ((lstSRBU[i].Booking_Unit__r.Unit_Selling_Price_AED__c * decPercent )/100);
                            system.debug('decPercent '+decPercent);
                            system.debug('decAmount '+decAmount);
                            objIPMSPT.PAYMENT_AMOUNT = String.valueOf(decAmount);
                        }

                        if(lstSelectedNewPaymentTermsByCRE[j].blnNewTerm != null && lstSelectedNewPaymentTermsByCRE[j].blnNewTerm)
                        {
                            objIPMSPT.TRANSFER_AR_INTER_FLAG = 'N';// for new PT pass value as N
                        }
                        else
                        {
                            objIPMSPT.TRANSFER_AR_INTER_FLAG = 'Y';// for existing PT pass value as Y
                        }
                        
                        lstPaymentTermInIPMS[ index ] = objIPMSPT;
                        index++;
                        system.debug('index '+index);
                    }
                    strResponse = AOPTMQService.createPaymentPlanIPMS(lstSRBU[i].Registration_ID__c,objCase.CaseNumber,'AMENDMENT_OF_PAYMENT_TERMS',lstPaymentTermInIPMS);
                    //strResponse = AOPTMQService.createPaymentPlanIPMS(lstSRBU[i].Registration_ID__c,'000141','AMENDMENT_OF_PAYMENT_TERMS',lstPaymentTermInIPMS);
                    system.debug('strResponse '+strResponse);
                    responseBUMap.put(lstSRBU[i].Booking_Unit__c, strResponse);
                }
                
                system.debug('lstPaymentTermInIPMS   '+lstPaymentTermInIPMS.size());
                system.debug('lstPaymentTermInIPMS '+lstPaymentTermInIPMS);
                system.debug('lstPaymentTermInIPMS '+JSON.serialize(lstPaymentTermInIPMS));
                

                system.debug('Map responseBUMap '+responseBUMap);
                Boolean blnApplyPP = false;
                for(String strResponseVal : responseBUMap.values())
                {
                    Map<String, Object> mapResponse = (Map<String, Object>)JSON.deserializeUntyped(strResponseVal);
                    if(mapResponse.get('message')!= null && mapResponse.get('status') != null)
                    {
                        system.debug('status '+(String)mapResponse.get('status'));
                        if( String.valueOf(mapResponse.get('status')).equalsIgnoreCase('S'))
                        {
                            blnApplyPP = true;
                        }
                        else
                        {
                            blnApplyPP = false;
                            break;
                        }
                    }
                }
                
                system.debug('blnApplyPP '+blnApplyPP);
                
                if(blnApplyPP)
                {   

                    system.debug('in flag true');
                    
                    List<Payment_Plan__c> lstPaymentPlan = new List<Payment_Plan__c>();
                    List<Payment_Plan__c> lstPaymentPlanToUpdate = new List<Payment_Plan__c>();
                    List<Payment_Terms__c> lstApplyNewPaymentTerms = new List<Payment_Terms__c>();
                    List<PaymentPlanWrapper> lstPaymentPlanWrapper = new List<PaymentPlanWrapper>();

                    for(Payment_Plan__c objPaymentPlan : [SELECT Id,Booking_Unit__c,Booking_Unit__r.Unit_Selling_Price_AED__c,Status__c FROM Payment_Plan__c WHERE Booking_Unit__c IN :bookingUnitIDSet AND (Status__c = 'Active' OR Status__c = null)])
                    {   
                        // add new Payment Plans for selected booking units in set bookingUnitIDSet
                        Payment_Plan__c objPaymentPlanCreate = new Payment_Plan__c();
                        objPaymentPlanCreate.Booking_Unit__c = objPaymentPlan.Booking_Unit__c;
                        objPaymentPlanCreate.Parent_Payment_Plan__c = objPaymentPlan.Id;
                        objPaymentPlanCreate.Effective_From__c = system.today();
                        objPaymentPlanCreate.Status__c = 'Active';

                        lstPaymentPlan.add(objPaymentPlanCreate);

                        //store payment plan in wrapper
                        PaymentPlanWrapper objPaymentPlanWrapper = new PaymentPlanWrapper();
                        objPaymentPlanWrapper.objPaymentPlan = objPaymentPlanCreate;
                        objPaymentPlanWrapper.unitPrice = objPaymentPlan.Booking_Unit__r.Unit_Selling_Price_AED__c;
                        lstPaymentPlanWrapper.add(objPaymentPlanWrapper);

                        // update old Payment plan status to Inactive
                        Payment_Plan__c objPaymentPlanUpdate = new Payment_Plan__c(Id = objPaymentPlan.Id);
                        objPaymentPlanUpdate.Status__c = 'InActive';
                        objPaymentPlanUpdate.Effective_To__c = system.today();
                        lstPaymentPlanToUpdate.add(objPaymentPlanUpdate);
                    }

                    if(lstPaymentPlan != null && lstPaymentPlan.size()>0)
                    {
                        //Savepoint sp = Database.setSavepoint();
                        system.debug('lstPaymentPlan '+lstPaymentPlan);
                        system.debug('lstPaymentPlanWrapper '+lstPaymentPlanWrapper);
                        
                        try
                        {
                            insert lstPaymentPlan;
                            update lstPaymentPlanToUpdate;
                            system.debug('lstPaymentPlan upsert size '+lstPaymentPlan.size());
                            system.debug('lstPaymentPlan upsert '+lstPaymentPlan);
                            for(PaymentPlanWrapper objPaymentPlanWrap : lstPaymentPlanWrapper)
                            {
                                for(AOPTServiceRequestControllerLDS.SelectedNewPaymentTermsByCRE objSelectedPTApply : lstSelectedNewPaymentTermsByCRE)
                                {
                                    Payment_Terms__c objNewPT = new Payment_Terms__c();
                                    objNewPT.Installment__c = objSelectedPTApply.installment;
                                    objNewPT.Description__c = objSelectedPTApply.description;
                                    objNewPT.Percent_Value__c = objSelectedPTApply.percentValue;
                                    objNewPT.Milestone_Event__c = objSelectedPTApply.mileStoneEvent;
                                    objNewPT.Milestone_Event_Arabic__c = objSelectedPTApply.mileStoneEventArabic;
                                    //if date is in IPMS format
                                    if(String.isNotBlank(objSelectedPTApply.paymentDate) && !AOPTUtility.checkDateFormate(objSelectedPTApply.paymentDate))
                                    {
                                        objNewPT.Payment_Date__c = AOPTUtility.convertDateFormat((objSelectedPTApply.paymentDate));
                                    }

                                    // if date is in SF format
                                    if(String.isNotBlank(objSelectedPTApply.paymentDate) && AOPTUtility.checkDateFormate(objSelectedPTApply.paymentDate))
                                    {
                                        objNewPT.Payment_Date__c = AOPTUtility.getDateInstanceFromString(objSelectedPTApply.paymentDate);
                                    }
                                    system.debug('Unit_Selling_Price_AED__c '+objPaymentPlanWrap.unitPrice);
                                    if(objPaymentPlanWrap.unitPrice != null)
                                    {
                                        Decimal decPercentPT = Decimal.valueOf(objSelectedPTApply.percentValue);
                                        Decimal decAmountPT = ((objPaymentPlanWrap.unitPrice * decPercentPT )/100);
                                        system.debug('decPercentPT '+decPercentPT);
                                        system.debug('decAmountPT '+decAmountPT);
                                        objNewPT.Payment_Amount__c = String.valueOf(decAmountPT);
                                    }
                                    else
                                    {
                                        system.debug('amount is empty');
                                    }
                                    objNewPT.Payment_Plan__c = objPaymentPlanWrap.objPaymentPlan.Id;
                                    //objNewPT.Booking_Unit__c = objInsertedPaymentPlan.Booking_Unit__c;
                                    lstApplyNewPaymentTerms.add(objNewPT);
                                }
                            }
                            system.debug('lstApplyNewPaymentTerms '+lstApplyNewPaymentTerms.size());
                            system.debug('lstApplyNewPaymentTerms JSON '+JSON.serialize(lstApplyNewPaymentTerms.size()));
                            if(lstApplyNewPaymentTerms != null && lstApplyNewPaymentTerms.size() > 0)
                            {
                                system.debug('lstApplyNewPaymentTerms '+lstApplyNewPaymentTerms.size());
                                
                                insert lstApplyNewPaymentTerms;

                                // create task for Update AOPT details in IPMS
                                Task objTask = new Task();
                                objTask.ActivityDate = System.today()+1;
                                objTask.Assigned_User__c = 'Finance';
                                objTask.CurrencyIsoCode = 'AED';
                                objTask.Priority = 'High';
                                objTask.Process_Name__c = 'AOPT';
                                objTask.Status = 'Not Started';
                                objTask.Subject = 'Update AOPT Details in IPMS';
                                objTask.WhatId = strCaseID;
                                insert objTask;
                            }
                            //update field of case which states that new payment terms have been applies in SF and IPMS
                            Case objCaseToUpdate = new Case( Id = Id.valueOf(strCaseID));
                            objCaseToUpdate.Is_New_Payment_Terms_Applied__c = true;
                            update objCaseToUpdate;
                            //sendEmail('Payment Plan & Terms Applied success');
                            AOPTUtility.errorLogger('Payment Plan & Terms Applied success',strCaseID,'');
                        }
                        catch(Exception exp)
                        {   
                            //Database.rollback(sp);
                            system.debug('--stack trace--'+exp.getStackTraceString());
                            AOPTUtility.errorLogger('Error PP '+exp.getMessage(),strCaseID,'');
                            //sendEmail('Error PP '+exp.getMessage());
                            //throw exp;
                        }
                    }
                }
                else
                {
                    Task objTask = TaskUtility.getTask((SObject)objCase, 'Update to IPMS failed. Click on Re-Submit Button', 'CRE', 'AOPT', system.today().addDays(1));
                    insert objTask;
                    AOPTUtility.errorLogger('Error in response '+JSON.serialize(responseBUMap),strCaseID,'');
                    //sendEmail('Error in response '+JSON.serialize(responseBUMap));
                }
            }
        }
        catch(Exception exp)
        {   
            AOPTUtility.errorLogger('General Error  '+exp.getMessage(),strCaseID,'');
            system.debug('general error '+exp.getMessage());
            //sendEmail('General Error  '+exp.getMessage());
            //throw exp;
        }
    }

    /*private static void sendEmail(String strBody)
    {   
      system.debug('strBody '+strBody);
      List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();  
      // Step 1: Create a new Email
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    
      // Step 2: Set list of people who should get the email
      List<String> sendTo = new List<String>();
      sendTo.add('hardikmehta9720@gmail.com');
      mail.setToAddresses(sendTo);
    
      // Step 3: Set who the email is sent from
      mail.setSenderDisplayName('IPMS Payment Terms Status');

      // Step 4. Set email contents - you can use variables!
      mail.setSubject('IPMS Payment Terms Status');
      String body = strBody;
      mail.setPlainTextBody(body);
    
      // Step 5. Add your email to the master list
      mails.add(mail);
      try
      {
        // Step 6: Send all emails in the master list
        Messaging.sendEmail(mails);
        system.debug('send email success');
      }
      catch(Exception exp)
      {
        system.debug('exp in send email '+exp.getMessage());
      }
      
    }
    */
    //wrapper class for Payment Plan which are inserted with unit price
    public class PaymentPlanWrapper
    {
        Payment_Plan__c objPaymentPlan;
        Decimal unitPrice;
    }
}