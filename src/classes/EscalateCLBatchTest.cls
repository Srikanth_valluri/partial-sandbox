@isTest
public with sharing class EscalateCLBatchTest {
    @IsTest
    static void positiveTestForDirector(){
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        //ID adminRoleId = [ Select id from userRole where name = 'Director'].id;
        UserRole userRoleObj = new UserRole(Name = 'Director', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        User u4 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
                           LocaleSidKey='en_US', ProfileId = p2.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='sta4@damactest1234.com');
        insert u4;
       User u3 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
                           LocaleSidKey='en_US', ProfileId = p2.Id,ManagerID = u4.id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='sta1@damactest1234.com');
        insert u3;
        User u2 = new User(Alias = 'standt2', Email='stand2@testorg.com', userRoleId=userRoleObj.Id, isActive = true,
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p2.Id, ManagerID = u3.id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='adm@testorg123.com');
        insert u2;
        User u1 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
                           LocaleSidKey='en_US', ProfileId = p2.Id, ManagerID = u2.id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='sta0@damactest1234.com');
        insert u1;
      
        System.runAs(u2) {
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
            
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR ;
            
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc ;
            
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objDealSR.Id, 1 );
            insert lstBooking ;
            
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
            insert lstBookingUnit ;
            Id recTypeCOD = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
            List<Calling_List__c> lstCalling = TestDataFactory_CRM.createCallingList( 'Appointment_Scheduling', 1 , lstBookingUnit[0] );
            lstCalling[0].OwnerId =u1.Id;
            lstCalling[0].Director_Escl_Date__c =system.now()-1;
            lstCalling[0].Director_Outcome__c = 'Refuse to Pay';
            lstCalling[0].RecordTypeId = recTypeCOD;
            lstCalling[0].ForQlik__c = true;
             lstCalling[0].Current_Escl_with__c = u3.Id;
            insert lstCalling ;
            
            Database.executeBatch(new EscalateCLBatch(),200);
            
        }
    }
    
    
    @IsTest
    static void positiveTest3(){
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        //ID adminRoleId = [ Select id from userRole where name = 'Director'].id;
        UserRole userRoleObj = new UserRole(Name = 'Director', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        User u3 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
                           LocaleSidKey='en_US', ProfileId = p2.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='sta1@damactest1234.com');
        insert u3;
        User u2 = new User(Alias = 'standt2', Email='stand2@testorg.com', userRoleId=userRoleObj.Id, isActive = true,
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p2.Id, ManagerID = u3.id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='adm@testorg123.com');
        insert u2;
        User u1 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
                           LocaleSidKey='en_US', ProfileId = p2.Id, ManagerID = u2.id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='sta0@damactest1234.com');
        insert u1;
      
        System.runAs(u2) {
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
            
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR ;
            
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc ;
            
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objDealSR.Id, 1 );
            insert lstBooking ;
            
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
            insert lstBookingUnit ;
            Id recTypeCOD = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
            List<Calling_List__c> lstCalling = TestDataFactory_CRM.createCallingList( 'Appointment_Scheduling', 1 , lstBookingUnit[0] );
            lstCalling[0].OwnerId =u1.Id;
            //lstCalling[0].RTP_Escl_Date__c =date.newInstance(2020,05,04);
            lstCalling[0].RTP_Escl_Date__c = system.now()-1;
            lstCalling[0].TL_Manager_Outcome__c  = 'Refuse to Pay';
            lstCalling[0].RecordTypeId = recTypeCOD;
            lstCalling[0].ForQlik__c = true;
            lstCalling[0].Current_Escl_with__c = u2.Id;
            insert lstCalling ;
            
            Database.executeBatch(new EscalateCLBatch(),200);
            
        }
    }
    
  @IsTest
   static void positiveTest4(){
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        //ID adminRoleId = [ Select id from userRole where name = 'Director'].id;
        UserRole userRoleObj = new UserRole(Name = 'Director', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        User u3 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
                           LocaleSidKey='en_US', ProfileId = p2.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='sta1@damactest1234.com');
        insert u3;
        User u2 = new User(Alias = 'standt2', Email='stand2@testorg.com', userRoleId=userRoleObj.Id, isActive = true,
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p2.Id, ManagerID = u3.id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='adm@testorg123.com');
        insert u2;
        User u1 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
                           LocaleSidKey='en_US', ProfileId = p2.Id, ManagerID = u2.id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='sta0@damactest1234.com');
        insert u1;
      
        System.runAs(u2) {
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
            
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR ;
            
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc ;
            
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objDealSR.Id, 1 );
            insert lstBooking ;
            
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
            insert lstBookingUnit ;
            Id recTypeCOD = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
            List<Calling_List__c> lstCalling = TestDataFactory_CRM.createCallingList( 'Appointment_Scheduling', 1 , lstBookingUnit[0] );
            lstCalling[0].OwnerId =u1.Id;
            //lstCalling[0].RTP_Escl_Date__c =date.newInstance(2020,05,04);
            lstCalling[0].CreatedDate = system.now()-1;
            lstCalling[0].TL_Manager_Outcome__c  = 'Refuse to Pay';
            lstCalling[0].RecordTypeId = recTypeCOD;
            lstCalling[0].ForQlik__c = true;
            lstCalling[0].PTP_Date__c =system.today();
            lstCalling[0].PTP_Date_Mgr__c= system.today();
            //lstCalling[0].Current_Escl_with__c = u2.Id;
            insert lstCalling ;
            
            Database.executeBatch(new EscalateCLBatch(),200);
            
        }
    }
    
     @IsTest
   static void positiveTest5(){
     Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        //ID adminRoleId = [ Select id from userRole where name = 'Director'].id;
        UserRole userRoleObj = new UserRole(Name = 'Director', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        User u4 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
                           LocaleSidKey='en_US', ProfileId = p2.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='sta4@damactest1234.com');
        insert u4;
       User u3 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
                           LocaleSidKey='en_US', ProfileId = p2.Id,ManagerID = u4.id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='sta1@damactest1234.com');
        insert u3;
        User u2 = new User(Alias = 'standt2', Email='stand2@testorg.com', userRoleId=userRoleObj.Id, isActive = true,
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p2.Id, ManagerID = u3.id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='adm@testorg123.com');
        insert u2;
        User u1 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
                           LocaleSidKey='en_US', ProfileId = p2.Id, ManagerID = u2.id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='sta0@damactest1234.com');
        insert u1;
      
        System.runAs(u2) {
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
            
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR ;
            
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc ;
            
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objDealSR.Id, 1 );
            insert lstBooking ;
            
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
            insert lstBookingUnit ;
            Id recTypeCOD = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
            List<Calling_List__c> lstCalling = TestDataFactory_CRM.createCallingList( 'Appointment_Scheduling', 1 , lstBookingUnit[0] );
            lstCalling[0].OwnerId =u1.Id;
            lstCalling[0].Director_Escl_Date__c =system.now()-1;
            lstCalling[0].Director_Outcome__c = 'Refuse to Pay';
            lstCalling[0].RecordTypeId = recTypeCOD;
            lstCalling[0].PTP_Date__c =system.today();
            lstCalling[0].Promise_To_Pay_Date__c =system.today();
            lstCalling[0].Offered_PSF_del__c =5000;
            lstCalling[0].ForQlik__c = true;
            lstCalling[0].Call_Outcome__c = 'Promise to Pay';
            lstCalling[0].Result__c = 'Will Pay next month';
            lstCalling[0].Current_Escl_with__c = u3.Id;
            insert lstCalling ;
            
            Database.executeBatch(new EscalateCLBatch(),200);
            
        }
    }
    
    
        
}