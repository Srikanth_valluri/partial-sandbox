/******************************************************************************************************************************************
Description: Assigning tasks to owner in round robin fashion
===========================================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
--------------------------------------------------------------------------------------------------------------------------------------------
1.0     | 23-12-2020        | Aishwarya Todkar  | 1. Initial Draft
******************************************************************************************************************************************/
public without sharing class RoundRobinAssignment {
    
    public static void changeOwner( List<Task> listTasks ) {
        if( listTasks != null && !listTasks.isEmpty() ) {
            //Task objtask = listTasks[0];
            for(Task objtask : listTasks) {
                if( String.isNotBlank( objTask.Subject )
                && String.isNotBlank( objTask.Process_Name__c )
                && objTask.Subject.equalsIgnoreCase( 'Verify Proof of Payment Details in IPMS' ) 
                && objTask.Process_Name__c.equalsIgnoreCase( 'POP' ) ) {
                    applyRoundRobin( objTask, 'POP' );
                }
            }        
        }
    }

    static void applyRoundRobin( Task objTask, String ProcessName )  {

        Round_Robin_Owner_Assignment__c objCurrentOwner = new Round_Robin_Owner_Assignment__c();
        Round_Robin_Owner_Assignment__c objNextOwner = new Round_Robin_Owner_Assignment__c();
        List<Round_Robin_Owner_Assignment__c> listOwnerCsToUpdate = new List<Round_Robin_Owner_Assignment__c>();
        Map<Integer, Round_Robin_Owner_Assignment__c> mapOwner = new map<Integer, Round_Robin_Owner_Assignment__c>();

        for( Round_Robin_Owner_Assignment__c objCs : [ SELECT 
                                                            Owner_Id__c
                                                            , Is_Current_Owner__c
                                                            , Order__c
                                                            , Process_Name__c 
                                                        FROM 
                                                            Round_Robin_Owner_Assignment__c
                                                        WHERE
                                                            Process_Name__c =: processName
                                                        ORDER BY
                                                            Order__c ASC
                                                    ]) {
            mapOwner.put( Integer.valueOf( objCs.Order__c ), objCs);
            if( objCs.Is_Current_Owner__c ) {
                objCurrentOwner = objCs;
            }
        }
        System.debug( 'objCurrentOwner ==' + objCurrentOwner);
        if( !mapOwner.isEmpty() && objCurrentOwner != null ) {
            
            objTask.OwnerId = Id.valueOf( objCurrentOwner.Owner_Id__c );
            Integer nextOwnerOrder = objCurrentOwner.Order__c == mapOwner.size() ? 1 : Integer.valueOf( objCurrentOwner.Order__c + 1 );
            system.debug('nextOwnerOrder=='+nextOwnerOrder);
            if( mapOwner.containsKey( nextOwnerOrder ) ) {
                objNextOwner = mapOwner.get( nextOwnerOrder );
                system.debug('objNextOwner=='+objNextOwner);
                if( objNextOwner != null ) {
                    objCurrentOwner.Is_Current_Owner__c = false;
                    objNextOwner.Is_Current_Owner__c = true;
                    listOwnerCsToUpdate.add( objCurrentOwner );
                    listOwnerCsToUpdate.add( objNextOwner );

                    if( !listOwnerCsToUpdate.isEmpty() ) {
                        update listOwnerCsToUpdate;
                    }
                }
            }
        }
    }
}