/*********************************************************************************
* Description - HTTP Mock callout for FM Service Charges.                        *
*--------------------------------------------------------------------------------*
* Version   Date            Author             Description                       *
* 1.0       04/03/2019      Aishwarya Todkar   Initial Draft                     *
* 1.1       05/03/2019      Arjun Khatri        1.Added response for LOAMS SMS   *
**********************************************************************************/
@isTest
global Class FMServiceChargesHttpMock  implements HttpCalloutMock {

    integer resNumber;
    
    public FMServiceChargesHttpMock() {}
    public FMServiceChargesHttpMock(integer num) {
        resNumber = num;
    }
    global HTTPResponse respond(HTTPRequest req) {
        FmIpmsRestServices.DueInvoicesResult dueRes;
        HttpResponse res = new HttpResponse();
        
        //response for Sendgrid email
        if(resNumber == 1) {

            FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
            invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
            invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
            invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
            invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
            invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
            invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
                new Map<String, String> {
                    'ATTRIBUTE1' => 'registrationId',
                    'ATTRIBUTE2' => 'unitName',
                    'ATTRIBUTE3' => 'projectName',
                    'ATTRIBUTE4' => 'customerId',
                    'ATTRIBUTE5' => 'orgId',
                    'ATTRIBUTE6' => 'partyId',
                    'ATTRIBUTE7' => 'partyName',
                    'ATTRIBUTE8' => 'trxNumber',
                    'ATTRIBUTE9' => 'creationDate',
                    'ATTRIBUTE10' => 'callType',
                    'ATTRIBUTE11' => '5000',
                    'ATTRIBUTE12' => '10000',
                    'ATTRIBUTE13' => '5000',
                    'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                    'ATTRIBUTE15' => 'trxType',
                    'ATTRIBUTE16' => '0'
                });
            res.setBody(Json.serialize(invoicesResponse));
            res.setStatusCode(200);
        }
        
        //response for LOAMS SMS
        if(resNumber == 2) { 
            res.setBody('SMS message(s) sent');
            res.setStatusCode(200);
        }
        return res;
    }
}