/***************************************************************************************************
 * @Name              : DAMAC_Agents_LeadCampaignNamePicklist
 * @Test Class Name   : DAMAC_Agents_LeadCampaignNameTest
 * @Description       : RestResource Class to GET the Campaigns on Lead as picklist values
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         18/06/2020       Created
****************************************************************************************************/

@RestResource(urlMapping='/GetCampaignNames/')
/* 
URL to test in Postman or Workbench 
GET  - /services/apexrest/inquiries?agentId=0031n00002BfvRo&agencyId=0011n00002LcMI0&startDate=2015-06-02 14:14:25&endDate=2020-06-02 14:14:25
POST - /services/apexrest/inquiries
*/
global with sharing class DAMAC_Agents_LeadCampaignNamePicklist{   

    @HttpGet
    global static void doGet(){
        RestRequest request = RestContext.request;
        String campaignName = '';      
        campaignName = request.requestURI.substringAfter('GetCampaignNames/');
        system.debug('campaignName'+campaignName);
        String jsonBody = '';
        User currentUser = [SELECT Id, accountId, contact.AccountId, contactId, Profile.Name FROM User WHERE Id =: UserInfo.getUserId()];
       // Integer statusCode = 204;
        try{        
            //if(String.isBlank(campaignName)){
             if( request.requestURI == '/GetCampaignNames/' ){
                 if(currentUser.Profile.Name == 'Customer Community - Agent' || currentUser.Profile.Name == 'Customer Community - Agent + Admin' || currentUser.Profile.Name == 'Customer Community - Super User' ){ 
                    // statusCode = 200;
                     Map<String,String> marketinCampaignNameMap = new Map<String,String>();
                     List<Assigned_Agent__c> assignedAgentList = new List<Assigned_Agent__c>();
                     Date toDay = Date.Today();
                     assignedAgentList = [SELECT Id,Campaign__c,Campaign__r.Campaign_Name__c,User__c,Agency__r.Name,Contact__r.Name, Agency_Name__c  
                                         From Assigned_Agent__c
                                         WHERE  User__c = :UserInfo.getUserId() AND 
                                                Campaign__r.Eligible_for_Agent_Portal__c = true AND
                                                Campaign__r.Active__c = true AND Campaign__r.Start_Date__c <=: toDay AND Campaign__r.End_Date__c >=: toDay]; 
                    
                    for( Assigned_Agent__c assignedObj : assignedAgentList){                
                        marketinCampaignNameMap.put(assignedObj.Campaign__c, assignedObj.Campaign__r.Campaign_Name__c);                     
                    }                                             
                    system.debug('marketinCampaignNameMap >>'+marketinCampaignNameMap );
                    
                    for(String campaignId: marketinCampaignNameMap.keySet()){
                                                                                
                        if(jsonBody != ''){
                        
                            jsonBody += ', ' + '{ "Campaign_Id": "' + campaignId +'","Campaign_Name":"' +marketinCampaignNameMap.get(campaignId) + '"';
                        } 
                        else{
                        
                            jsonBody += '{' + '"Data": [{'  + ' "Campaign_Id": "' + campaignId +'","Campaign_Name":"' +marketinCampaignNameMap.get(campaignId) + '"}';
                        }
                     
                    }
                    if(jsonBody != ''){
                    
                        jsonBody += '}]}' ;
                    }
                    else{
                    
                        jsonBody += '{' + '"Data":'+' []}';
                     
                    }
                     
                    system.debug('jsonBody >>'+jsonBody );
                } 
                else{
                    jsonBody = 'You do not have access to this';
                    jsonBody = jSON.serialize(jsonBody ); 
                
                } 
            }
            else{
               // statusCode = 404;
                jsonBody = 'Bad Input';
                jsonBody = jSON.serialize(jsonBody ); 
            }
        }
        catch(Exception e){
             system.debug('exception');
             jsonBody = e.getMessage();
             jsonBody = jSON.serialize(jsonBody);   
        }
        RestResponse response = RestContext.response;
        response.statusCode = 200;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(jsonBody);  
        
    }
}