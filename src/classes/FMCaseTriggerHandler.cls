/**
 * FM Case Trigger Handler
 *
 * Revision History
 * 28-05-2018   Lochana Rajput      Added method invokation for approval process
 */
public with sharing class FMCaseTriggerHandler {

    public static void beforeInsert(Map<Id, FM_Case__c> newMap, Map<Id, FM_Case__c> oldMap) {
        FMCaseTriggerHelper_Escalation.beforeInsert_updateFMCaseForEscation(Trigger.new);
        FMCaseTriggerHelper.setOwnerIdForPortalCases(Trigger.new, Trigger.old);
    }
    public static void afterInsert(List<FM_Case__c> scope) {
        //FMCaseTriggerHelper.sendSMS_HelpdeskSRRaised(Trigger.new);
    }

    public static void beforeUpdate(Map<Id, FM_Case__c> newMap, Map<Id, FM_Case__c> oldMap ) {
        FMCaseTriggerHelper_Escalation.updateFMCaseForEscation(Trigger.new, oldMap);
        FMCaseTriggerHelper.setOwnerIdForPortalCases(Trigger.new, Trigger.old);
        //FMCaseRejectionHelper.FMCaseRejectionHelper(oldMap,Trigger.new);
        // FMCaseTriggerHelper.sendSMS_HelpdeskSRClosed(newMap, oldMap);
    }

    public static void afterUpdate(Map<Id, FM_Case__c> newMap, Map<Id, FM_Case__c> oldMap ) {
        //Call approval process
        System.debug('==callAfterInsert_update==');
        // FMCaseTriggerHelper.sendSMS_HelpdeskSRClosed(newMap, oldMap);
        FM_ApprovalsHandler.callApprovalProcess(newMap, oldMap);
        //FM_ApprovalsHandler.callRecallPendingApproval(newMap, oldMap);
         FMCaseTriggerHelper.updateRejectioncommentsOnFMCase(newMap, oldMap);
        /*FMCaseTriggerHelper.closeManagerTasks(newMap,oldMap);*/
        //FMCaseTriggerHelper.changeDetailOnAccount(newMap);
        FMCaseTriggerHelper.createTaskForFinance(newMap, oldMap);
        
        // NotifyTenant.NotificationEmail(newMap);
    }

}