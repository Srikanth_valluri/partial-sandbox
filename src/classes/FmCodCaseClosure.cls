public without sharing class FmCodCaseClosure{
    @invocableMethod
    public static void updateCustomer(list<FM_Case__c> lstCase) {
        set<Id> setAccountIds = new set<Id>();
        for(FM_Case__c objFM : lstCase) {
            if(objFM.Account__c != null) {
                setAccountIds.add(objFM.Account__c);
            }
        }

        if(!setAccountIds.isEmpty()) {
            map<Id,Account> mapId_Account = new map<Id,Account>([SELECT Id
                                                                      , Name
                                                                      , IsPersonAccount
                                                                      , Email__pc
                                                                      , Email__c
                                                                 FROM Account
                                                                 WHERE Id IN : setAccountIds]);
            if(!mapId_Account.isEmpty()) {
                list<Account> lstAccountToUpdate = new list<Account>();
                for(FM_Case__c objFM : lstCase) {
                    if(objFM.Account__c != null) {
                        Account objA = new Account();
                        objA.Id = objFM.Account__c;
                        if(objFM.Request_Type__c.equalsIgnoreCase('Change of Contact Details')) {

                            String strCountryCode = String.isNotBlank(objFM.Mobile_Country_Code__c)
                                                    && objFM.Mobile_Country_Code__c.split(':').size() >=1 ?
                                                        objFM.Mobile_Country_Code__c.split(':')[1] :
                                                        String.isNotBlank(objFM.Mobile_Country_Code__c) ?
                                                        objFM.Mobile_Country_Code__c : '';
                            strCountryCode = strCountryCode.trim();
                            System.debug('strCountryCode -- : '+strCountryCode);

                            if(mapId_Account.get(objFM.Account__c).isPersonAccount){
                                objA.Mobile_Country_Code__pc = objFM.Mobile_Country_Code__c;
                                //objA.Mobile_Phone_Encrypt__pc = objFM.Mobile_no__c;
                                objA.Mobile_Phone_Encrypt__pc = strCountryCode + Long.valueOf(objFM.Mobile_no__c.remove('-').remove(' ').remove('+').removeStart('0').removeStart('0') );
                                objA.PersonMobilePhone = objFM.Mobile_no__c;
                                objA.PersonEmail = objFM.Contact_Email__c;
                                objA.Email__pc = objFM.Contact_Email__c;
                                objA.Old_Email__c = mapId_Account.get(objFM.Account__c).Email__pc;
                            }else {
                                objA.Old_Email__c = mapId_Account.get(objFM.Account__c).Email__c;
                            }
                            objA.Mobile_Country_Code__c = objFM.Mobile_Country_Code__c;
                            objA.Mobile__c = objFM.Mobile_no__c;
                            objA.Mobile_Encrypt__c = objFM.Mobile_no__c;
                            objA.Mobile_Phone_Encrypt__c = objFM.Mobile_no__c;
                            objA.Email__c = objFM.Contact_Email__c;
                        }else if(objFM.Request_Type__c.equalsIgnoreCase('Passport Detail Update')) {
                            if(mapId_Account.get(objFM.Account__c).isPersonAccount) {
                                objA.Passport_Number__pc = objFM.New_CR__c;
                                objA.Passport_Expiry_Date__pc = objFM.Passport_Issue_Date__c;
                                objA.Passport_Issue_Place__pc = objFM.Passport_Issue_Place__c;
                                objA.Passport_Issue_Place_Arabic__pc = objFM.Passport_Issue_Place_Arabic__c;
                            }
                            objA.Passport_Number__c = objFM.New_CR__c;
                            objA.CR_Number__c = objFM.New_CR__c;
                            objA.Passport_Expiry_Date__c = objFM.Passport_Issue_Date__c;
                            objA.Passport_Issue_Place__c = objFM.Passport_Issue_Place__c;
                            objA.Passport_Issue_Place_Arabic__c = objFM.Passport_Issue_Place_Arabic__c;
                        }
                        lstAccountToUpdate.add(objA);
                    } // end of Account__c poopulated check
                } // end of for loop

                if(!lstAccountToUpdate.isEmpty()) {
                    update lstAccountToUpdate;
                }
            } // end of mapId_Account not empty check
        } // end of setAccountIds not empty check
    } // end of method
}