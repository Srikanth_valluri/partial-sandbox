public without sharing class FMNoticeGenerationBatch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {

    private list<String> lstBUIdNew;
    private String query, strQuarter;

    public FMNoticeGenerationBatch(list<String> lstBookingUnitIds, String Quarter) {
        lstBUIdNew = new list<String>();
        lstBUIdNew.addAll(lstBookingUnitIds);
        strQuarter = Quarter;
        //lstBUIdNew = lstBookingUnitIds;
        system.debug('!!!!lstBUIdNew'+lstBUIdNew);
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('ala ekdacha !!!!lstBUIdNew'+lstBUIdNew);
        query = 'SELECT Id,Booking__r.Account__r.Email__c,Registration_ID__c, Unit_Name__c, Unit_Type__c, ';
        query += 'Approval_Status__c, Inventory__r.Building_Location__r.Additional_Document_Name__c, ';
        query += 'Inventory__c, Inventory__r.Building_Location__c ';
        query += 'From Booking_Unit__c Where Id IN: lstBUIdNew';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List< Booking_Unit__c > scope) {
         system.debug('!!!!!scope'+scope);
         list<String> lstServices = new list<String>();
         DisplayRelatedUnitsController.avoidErrorLogCreation = true;
         system.debug('!!!!!.avoidErrorLogCreation!!'+DisplayRelatedUnitsController.avoidErrorLogCreation);
         Report_Types__c objRT = Report_Types__c.getValues('FM Quarterly Notice');
         system.debug('!!!!objRT'+objRT);
         if (objRT.Services_Required__c != null){
            lstServices = objRT.Services_Required__c.split(',');
         }
         system.debug('!!!!lstServices'+lstServices);
         list<Booking_Unit__c> lstBU = new list<Booking_Unit__c>();
         for (Booking_Unit__c objUnit : scope){
             String urlDocs;
             Integer urlCount = 0;
             map<String, String> dataParams = new map<String, String>();
             dataParams.put('regID_BUID', objUnit.Id+'-'+objUnit.Registration_ID__c);
             dataParams.put('__quarter', strQuarter);
             system.debug('!!!!dataParams'+dataParams);
             if (!lstServices.isEmpty()){
                 for (String strService : lstServices){
                     Service_Information__c objService = Service_Information__c.getValues(strService);
		             system.debug('!!!objService'+objService);
		             Type t =  Type.forName(objService.ClassName__c);
		             WebServiceAdapter serv = (WebServiceAdapter)t.newInstance();            
		             system.debug('!!!t'+t);
		             String url = serv.setParam(dataParams).call().getResponse();	
                     system.debug('!!!url'+url);
                     String hypeUrl;
                     if (!String.isblank(url)){
	                     urlCount ++;  
	                     system.debug('!!!urlCount'+urlCount);         
			             hypeUrl = '<a href="'+ url + '" target="_blank">'+strService + '</a>';
	                     system.debug('!!!hypeUrl'+hypeUrl);
                     }
                     if(!String.isblank(hypeUrl)){
			             if (String.isblank(urlDocs)) {	                         
			                 urlDocs = '<ul><li>'+ hypeUrl + '</li></ul>';
			             } else {	                            
	                       if (urlDocs.contains('<ul>')) {
	                           urlDocs = urlDocs.replace('<ul>', '');
	                        }
	                        urlDocs = '<ul><li> ' + hypeUrl + '</li>' +urlDocs;                          
			             }
                     }
                 }// end of for service
                 String docURL = urlDocs;
                 system.debug('!!!docURL'+docURL);
                 list<String> lstdocNames = new list<String>();
	                if (objUnit.Inventory__r.Building_Location__r.Additional_Document_Name__c != null) {
                        system.debug('!!!ala Additional_Document_Name__c chya if madhe');
	                    lstdocNames = 
	                        objUnit.Inventory__r.Building_Location__r.Additional_Document_Name__c.split(',');
                        system.debug('!!!lstdocNames'+lstdocNames);
	                    for (String objName : lstdocNames){
	                        Additional_Document_URLs__c document = Additional_Document_URLs__c.getValues(objName);
                            system.debug('!!!document'+document);
	                        if(document.Document_URL__c != null) {
                                if(string.isnotblank(docURL)){
                                    if (docURL.contains('<ul>')) {
                                        docURL = docURL.replace('<ul>', '');
                                    }
                                    docURL = '<ul><li> ' + '<a href="'+ document.Document_URL__c + '" target="_blank">'+document.Document_Name__c + '</a>' + '</li>' +docURL;
                                    if (docURL.contains('</ul>')) {
                                            docURL = docURL.replace('</ul>', '');
                                    }
                                    docURL = docURL+'<li>'+Label.FM_Invoice_Letter+'</li></ul>';
                                    system.debug('!!!docURL update zalela'+docURL);
                                }
	                        }
	                    }
	                    objUnit.URLs_of_Document__c = docURL;
	                } else {
	                    objUnit.URLs_of_Document__c = urlDocs;
	                }// end of additional doc logic
                    if (urlCount != null){
                        objUnit.No_of_URL_Documents__c = urlCount;
                    }
                    system.debug('!!!objUnit.No_of_URL_Documents__c'+objUnit.No_of_URL_Documents__c);
                    system.debug('!!!objUnit.URLs_of_Document__c'+objUnit.URLs_of_Document__c);
	                lstBU.add(objUnit);
             }             
         }
         system.debug('!!!lstBU'+lstBU);
         if (lstBU != null && !lstBU.isEmpty()){
             update lstBU;
             system.debug('!!!update zalela lstBU'+lstBU);
         }
    }   

    public void finish(Database.BatchableContext BC) {
        DisplayRelatedUnitsController.avoidErrorLogCreation = false;
    }
 
}