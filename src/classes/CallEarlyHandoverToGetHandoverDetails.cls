public with sharing class CallEarlyHandoverToGetHandoverDetails {
    public static CallEarlyHandoverToGetHandoverDetails.EarlyHandoverMQService1Response CallEarlyHandoverMQService1GetHandoverDetails(String regId) {
        //system.debug('regId'+regId);
        CallEarlyHandoverToGetHandoverDetails.EarlyHandoverMQService1Response objDetailsWrapper ;
        EarlyHandoverMQService1.HandoverHttpSoap11Endpoint objEligibleForHandover = new EarlyHandoverMQService1.HandoverHttpSoap11Endpoint();
        objEligibleForHandover.timeout_x = 120000 ;
        
        string strHand = objEligibleForHandover.GetHandoverDetails( '2-'+String.valueOf( Datetime.now().getTime()),'GET_HANDOVER_DETAILS','SFDC',regId);
        system.debug('strHand '+strHand );
        JSON2Apex  objEligibleForHandoverResponse = new JSON2Apex ();
        objEligibleForHandoverResponse = (CallEarlyHandoverToGetHandoverDetails.JSON2Apex)JSON.deserialize(strHand, CallEarlyHandoverToGetHandoverDetails.JSON2Apex.class);
        system.debug('objEligibleForHandoverResponse'+objEligibleForHandoverResponse);
        system.debug('t'+(CallEarlyHandoverToGetHandoverDetails.JSON2Apex) System.JSON.deserialize(strHand, JSON2Apex.class));
        //resObj = (EarlyHandoverMQService1Response)JSON.deserialize(strHand, CallEarlyHandoverToGetHandoverDetails.EarlyHandoverMQService1Response.class);

        //String strResponse = strHand.removeStart('[').removeEnd(']');
        try{
        String strResponse = strHand.substringAfter('[').substringBeforeLast(']');
        /*Map<String, Object> objUnitDetails = (Map<String, Object>)JSON.deserializeUntyped( strResponse.replace('\\','').removeStart('"').removeEnd('"') );
        system.debug('>>>>'+objUnitDetails.keyset());
        system.debug('****'+objUnitDetails.get('data').ATTRIBUTE1);
        */
        system.debug('*****'+strResponse);
        Map<String, object> objUnitDetails = (Map<String, object>)JSON.deserializeUntyped( strResponse.replace('\\','').removeStart('"').removeEnd('"') );
                  system.debug('objUnitDetails '+objUnitDetails );
                  
                  //Assign values to the wrapper object.
                objDetailsWrapper = new CallEarlyHandoverToGetHandoverDetails.EarlyHandoverMQService1Response();
                objDetailsWrapper.P_REQUEST_NUMBER= String.valueOf( objUnitDetails.get('P_REQUEST_NUMBER') );
                objDetailsWrapper.P_REQUEST_NAME= String.valueOf( objUnitDetails.get('P_REQUEST_NAME') );
                objDetailsWrapper.P_SOURCE_SYSTEM= String.valueOf( objUnitDetails.get('P_SOURCE_SYSTEM') );
                objDetailsWrapper.Registrationid = String.valueOf( objUnitDetails.get('ATTRIBUTE1') );
                objDetailsWrapper.JOPD_Suite_Area= String.valueOf( objUnitDetails.get('ATTRIBUTE2') );
                objDetailsWrapper.JOPD_Balcony_Area= String.valueOf( objUnitDetails.get('ATTRIBUTE3') );
                objDetailsWrapper.JOPD_Total_Area= String.valueOf( objUnitDetails.get('ATTRIBUTE4') );
                objDetailsWrapper.JOPD_Entitlement= String.valueOf( objUnitDetails.get('ATTRIBUTE5') );
                objDetailsWrapper.Litigation_Flag= String.valueOf( objUnitDetails.get('ATTRIBUTE6') );
                objDetailsWrapper.Mortgage_Flag= String.valueOf( objUnitDetails.get('ATTRIBUTE7') );
                objDetailsWrapper.Status= String.valueOf( objUnitDetails.get('ATTRIBUTE8') );
                objDetailsWrapper.Billable_Amount= String.valueOf( objUnitDetails.get('ATTRIBUTE9') );
                objDetailsWrapper.Payment_Received= String.valueOf( objUnitDetails.get('ATTRIBUTE10') );
                objDetailsWrapper.Deposit_Received= String.valueOf( objUnitDetails.get('ATTRIBUTE11') );
                objDetailsWrapper.Legal_Flag= String.valueOf( objUnitDetails.get('ATTRIBUTE12') );
                objDetailsWrapper.Default_Flag= String.valueOf( objUnitDetails.get('ATTRIBUTE13') );
                objDetailsWrapper.Under_Termination= String.valueOf( objUnitDetails.get('ATTRIBUTE14') );
                objDetailsWrapper.Area_Variation= String.valueOf( objUnitDetails.get('ATTRIBUTE15') );
                objDetailsWrapper.ACD= String.valueOf( objUnitDetails.get('ATTRIBUTE16') );
        system.debug(objDetailsWrapper);
        } catch (Exception e ){
            objDetailsWrapper = null;
           // resObj.url = 'https://www.google.com';
        }
        return objDetailsWrapper ;
    }
    public class JSON2Apex {
        public List<EarlyHandoverMQService1Response> data;
        public String message;
        public String status;
    }
    public class EarlyHandoverMQService1Response {
        public String P_REQUEST_NUMBER {get;set;}
        public String P_REQUEST_NAME{get;set;}
        public String P_SOURCE_SYSTEM{get;set;}
        public String Registrationid{get;set;}
        public String JOPD_Suite_Area{get;set;}
        public String JOPD_Balcony_Area{get;set;}
        public String JOPD_Total_Area{get;set;}
        public String JOPD_Entitlement{get;set;}
        public String Litigation_Flag{get;set;}
        public String Mortgage_Flag{get;set;}
        public String Status{get;set;}
        public String Billable_Amount{get;set;}
        public String Payment_Received{get;set;}
        public String Deposit_Received{get;set;}
        public String Legal_Flag{get;set;}
        public String Default_Flag{get;set;}
        public String Under_Termination{get;set;}
        public String Area_Variation{get;set;}
        public String ACD{get;set;}

    }
}