public class CampaignLeadsController {
    public Inquiry__c newInquiry{get; set;}
    public CampaignLeadsController(ApexPages.StandardController sc) {
        newInquiry = new Inquiry__c(
                RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId(),
                Campaign__c = System.label.Portal_Campaign_ID
                );
                //Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.info, 'Inquiry created successfully.'));
    }

    public void saveLead(){
        try {
            insert newInquiry;
            newInquiry = new Inquiry__c(
                RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId(),
                Campaign__c = System.label.Portal_Campaign_ID
                );
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.info, 'Inquiry created successfully.'));
        }
        catch(Exception ex) {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.fatal, ex.getMessage()));    
        }
    }
}