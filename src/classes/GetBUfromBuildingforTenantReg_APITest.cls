@isTest
public class GetBUfromBuildingforTenantReg_APITest {
    @isTest
    static void testBookingUnits(){
        
        Booking_Unit_Active_Status__c objBookingUnit = new Booking_Unit_Active_Status__c();
        objBookingUnit.Name = 'Agreement Cancellation Verified';
        objBookingUnit.Status_Value__c = 'Agreement Cancellation Verified';
        insert objBookingUnit;
        
        Location__c lstLocations = new Location__c();
        lstLocations.Building_Name__c = 'test';
        lstLocations.Location_Type__c = 'Building';
        lstLocations.Location_ID__c = '12345';
        insert lstLocations;
        
        FM_Case__c objFM = new FM_Case__c();
        objFM.Origin__c = 'Portal - Guest';
        objFM.Request_Type__c = 'Tenant Registration';
        objFM.Status__c = 'Draft Request';
        insert objFM;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR; 
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        insert objBooking;
        
        Inventory__c objInventory = new     Inventory__c();
        objInventory.Building_Location__c = lstLocations.Id;
        objInventory.Building_ID__c = '12345';
        objInventory.Property_Status__c = 'Ready';
        insert objInventory;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c ='test';
        objBU.Property_Name__c = 'test';
        objBU.Dummy_Booking_Unit__c = True;
        objBU.Booking__c = objBooking.Id;
        objBU.Registration_Status__c = 'Agreement Cancellation Verified';
        insert objBU;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getUnitsFromBuilding';  
        req.addParameter('buildingId', lstLocations.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetBUfromBuildingforTenantReg_API.getBookingUnits();
        
        
    }
    
     @isTest
    static void testBookingUnits1(){
        
        Booking_Unit_Active_Status__c objBookingUnit = new Booking_Unit_Active_Status__c();
        objBookingUnit.Name = 'Agreement Cancellation Verified';
        objBookingUnit.Status_Value__c = 'Agreement Cancellation Verified';
        insert objBookingUnit;
        
        Location__c lstLocations = new Location__c();
        lstLocations.Building_Name__c = 'test';
        lstLocations.Location_Type__c = 'Building';
        lstLocations.Location_ID__c = '12345';
        insert lstLocations;
        
        FM_Case__c objFM = new FM_Case__c();
        objFM.Origin__c = 'Portal - Guest';
        objFM.Request_Type__c = 'Tenant Registration';
        objFM.Status__c = 'Draft Request';
        insert objFM;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR; 
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        insert objBooking;
        
        Inventory__c objInventory = new     Inventory__c();
        objInventory.Building_Location__c = lstLocations.Id;
        objInventory.Building_ID__c = '12345';
        objInventory.Property_Status__c = 'Ready';
        insert objInventory;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c ='test';
        objBU.Property_Name__c = 'test';
        objBU.Dummy_Booking_Unit__c = True;
        objBU.Booking__c = objBooking.Id;
        objBU.Registration_Status__c = 'Agreement Cancellation Verified';
        insert objBU;
        
        Booking_Unit__c objBU1 = new Booking_Unit__c();
        objBU1.Unit_Name__c ='test1';
        objBU1.Property_Name__c = 'test1';
        objBU1.Dummy_Booking_Unit__c = True;
        objBU1.Booking__c = objBooking.Id;
        objBU1.Registration_Status__c = 'Agreement Cancellation Verified';
        insert objBU1;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getUnitsFromBuilding';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetBUfromBuildingforTenantReg_API.getBookingUnits();
        
        
    }
    
      @isTest
    static void testBookingUnits2(){
        
        Booking_Unit_Active_Status__c objBookingUnit = new Booking_Unit_Active_Status__c();
        objBookingUnit.Name = 'Agreement Cancellation Verified';
        objBookingUnit.Status_Value__c = 'Agreement Cancellation Verified';
        insert objBookingUnit;
        
        Location__c lstLocations = new Location__c();
        lstLocations.Building_Name__c = 'test';
        lstLocations.Location_Type__c = 'Building';
        lstLocations.Location_ID__c = '12345';
        insert lstLocations;
        
        FM_Case__c objFM = new FM_Case__c();
        objFM.Origin__c = 'Portal - Guest';
        objFM.Request_Type__c = 'Tenant Registration';
        objFM.Status__c = 'Draft Request';
        insert objFM;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR; 
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        insert objBooking;
        
        Inventory__c objInventory = new     Inventory__c();
        objInventory.Building_Location__c = lstLocations.Id;
        objInventory.Building_ID__c = '12345';
        objInventory.Property_Status__c = 'Ready';
        insert objInventory;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c ='test';
        objBU.Property_Name__c = 'test';
        objBU.Dummy_Booking_Unit__c = True;
        objBU.Booking__c = objBooking.Id;
        objBU.Registration_Status__c = 'Agreement Cancellation Verified';
        insert objBU;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getUnitsFromBuilding';  
        req.addParameter('buildingId', '');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetBUfromBuildingforTenantReg_API.getBookingUnits();
      
    }
    
    @isTest
    static void testBookingUnits3(){
        
        Booking_Unit_Active_Status__c objBookingUnit = new Booking_Unit_Active_Status__c();
        objBookingUnit.Name = 'Agreement Cancellation Verified';
        objBookingUnit.Status_Value__c = 'Agreement Cancellation Verified';
        insert objBookingUnit;
        
        Location__c lstLocations = new Location__c();
        lstLocations.Building_Name__c = 'test';
        lstLocations.Location_Type__c = 'Building';
        lstLocations.Location_ID__c = '12345';
        insert lstLocations;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR; 
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        insert objBooking;
        
        Inventory__c objInventory = new     Inventory__c();
        objInventory.Building_Location__c = lstLocations.Id;
        //objInventory.Building_Location__c = lstLocations.Location_ID__c;
        objInventory.Building_ID__c = '12345';
        objInventory.Property_Status__c = 'Ready';
        insert objInventory;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c ='test';
        objBU.Property_Name__c = 'test';
        objBU.Dummy_Booking_Unit__c = True;
        objBU.Booking__c = objBooking.Id;
        objBU.Registration_Status__c = 'Agreement Cancellation Verified';
        objBU.Inventory__c = objInventory.Id;
        insert objBU;
        
        FM_Case__c objFM = new FM_Case__c();
        objFM.Origin__c = 'Portal - Guest';
        objFM.Request_Type__c = 'Tenant Registration';
        objFM.Status__c = 'Draft Request';
        objFM.Booking_Unit__c = objBU.Id;
        insert objFM;
        
               
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getUnitsFromBuilding'; 
        req.addParameter('buildingId', lstLocations.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetBUfromBuildingforTenantReg_API.getBookingUnits();
        
        
    }
    
}