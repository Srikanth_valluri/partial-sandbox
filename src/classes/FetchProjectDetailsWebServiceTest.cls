@isTest
public class FetchProjectDetailsWebServiceTest {
       static testMethod void  testGetRestService(){
       
       Property__c objProject =new Property__c();
       objProject.name='DAMAC HILLS - ROCHESTER';
       objProject.Property_ID__c= 3212;
       objProject.Property_Code__c='RR';
       objProject.RERA_Project_Number__c='1410';
       objProject.Latitude__c = 55.25206210;
       objProject.Longitude__c = 25.02125400;
       insert   objProject;  
           
       Inventory__c objInventory =new Inventory__c();
       objInventory.Marketing_Name__c= 'BEVERLY HILLS BOUTIQUE VILLAS';
       objInventory.Property__c = objProject.Id;
       insert   objInventory;  

       
       Test.startTest();
       
           //As Per Best Practice it is important to instantiate the Rest Context 
           
           RestRequest req = new RestRequest(); 
           RestResponse res = new RestResponse();
                 
           req.requestURI = '/services/apexrest/fetchProjectDetails';  //Request URL
           req.httpMethod = 'GET';//HTTP Request Type
           RestContext.request = req;
           RestContext.response= res;
           List<FetchProjectDetailsWebService.ResponseResult> response = FetchProjectDetailsWebService.getProjectsDetails();
       Test.stopTest();
       system.assertEquals(response[0].DLD_PROJECT_NO, objProject.RERA_Project_Number__c);
       system.assertEquals(response[0].LAT, objProject.Latitude__c);
       system.assertEquals(response[0].LONGITUDE,objProject.Longitude__c);
   
   }
}