@isTest
private class SendCODNotificationTest {

    public static Id CODRecordTypeId = DamacUtility.getRecordTypeId('Case', 'Change of Details');
    public static Id COJBRecordTypeId = DamacUtility.getRecordTypeId('Case', 'Change of Joint Buyer');
    public static Id NameAndNationalityRecordTypeId = DamacUtility.getRecordTypeId('Case', 'Name Nationality Change');
    public static Id PassportDetailsUpdateRecordTypeId = DamacUtility.getRecordTypeId('Case', 'Passport Detail Update');

  @isTest static void TestMethod1() {
    Account accObj = TestDataFactory_CRM.createPersonAccount();
    accObj.Email__pc = 'test@test.com';
    accObj.Email__pc = 'test1@test.com';
    accObj.Mobile_Phone_Encrypt__pc = '545464524534';
    accObj.Mobile__c = '7245742742';
    accObj.Address_Line_1__pc = 'Test Add';
    accObj.Address_Line_1__c = 'Test Add 1';
    insert accObj;

    Case caseObj = TestDataFactory_CRM.createCase(accObj.Id, CODRecordTypeId);
    caseObj.Status = 'Closed';
    insert caseObj;
    
    Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
    Test.startTest();
         //Test.setMock( HttpCalloutMock.class, new MollakHttpMock());
        SendCODNotification.sendSMSToCustomer( new List<Case>{caseObj});
    Test.stopTest();

  }

  @isTest static void TestMethod2() {
    Account accObj = TestDataFactory_CRM.createPersonAccount();
    accObj.Email__pc = 'test@test.com';
    accObj.Email__pc = 'test1@test.com';
    accObj.Mobile_Phone_Encrypt__pc = '545464524534';
    accObj.Mobile__c = '7245742742';
    accObj.Address_Line_1__pc = 'Test Add';
    accObj.Address_Line_1__c = 'Test Add 1';
    insert accObj;

    Case caseObj = TestDataFactory_CRM.createCase(accObj.Id, COJBRecordTypeId);
    caseObj.Status = 'Closed';
    caseObj.Is_Primary_Customer_Updated__c = true;
    insert caseObj;
    
    Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
    Test.startTest();      
        //Test.setMock( HttpCalloutMock.class, new MollakHttpMock());
        SendCODNotification.sendSMSToCustomer( new List<Case>{caseObj});
    Test.stopTest();

  }
  
  @isTest static void TestMethod3() {
    Account accObj = TestDataFactory_CRM.createPersonAccount();
    accObj.Email__pc = 'test@test.com';
    accObj.Email__pc = 'test1@test.com';
    accObj.Mobile_Phone_Encrypt__pc = '545464524534';
    accObj.Mobile__c = '7245742742';
    accObj.Address_Line_1__pc = 'Test Add';
    accObj.Address_Line_1__c = 'Test Add 1';
    insert accObj;

    Case caseObj = TestDataFactory_CRM.createCase(accObj.Id, NameAndNationalityRecordTypeId);
    caseObj.Status = 'Closed';
    insert caseObj;

    Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
    Test.startTest();
        SendCODNotification.sendSMSToCustomer( new List<Case>{caseObj});
    Test.stopTest();
  }
  
  @isTest static void TestMethod4() {
    Account accObj = TestDataFactory_CRM.createPersonAccount();
    accObj.Email__pc = 'test@test.com';
    accObj.Email__pc = 'test1@test.com';
    accObj.Mobile_Phone_Encrypt__pc = '545464524534';
    accObj.Mobile__c = '7245742742';
    accObj.Address_Line_1__pc = 'Test Add';
    accObj.Address_Line_1__c = 'Test Add 1';
    insert accObj;

    Case caseObj = TestDataFactory_CRM.createCase(accObj.Id, PassportDetailsUpdateRecordTypeId);
    caseObj.Status = 'Closed';
    insert caseObj;
    
    Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
    Test.startTest();        
        //Test.setMock( HttpCalloutMock.class, new MollakHttpMock());
        SendCODNotification.sendSMSToCustomer( new List<Case>{caseObj});
    Test.stopTest();
  }
}