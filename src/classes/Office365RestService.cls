/**
* @File Name          : Office365RestService.cls
* @Description        : 
* @Author             : Arjun@espl.com
* @Group              : 
* @Last Modified By   : Arjun@espl.com
* @Last Modified On   : 10/8/2019, 5:50:18 PM
* @Modification Log   : 
* Ver       Date            Author                 Modification
* 1.0       10/8/2019       Arjun@espl.com         Initial Version
**/

public with sharing class Office365RestService {
    @testVisible private static final String OFFICE_REST_BASEURL    = 'https://login.microsoftonline.com';
    @testVisible private static final String OFFICE_LOGIN           = '/57e43429-428a-4973-9720-e6907ae94def/oauth2/token';
    @testVisible private static final String BASE_GRAPH_URL         = 'https://graph.microsoft.com/v1.0';
    @testVisible private static final String OFFICE_UPLOAD          = '/users/64f37cbe-8a05-427b-86ff-557a312b9890/';
    @testVisible private static final String SEARCH_URL              = '/dda64af8-f563-4be6-b413-6ea59f511956/drive/root/';
    @testVisible private static final String OFFICE_DRIVE_PATH      = 'drive/root:/';
    @testVisible private static final String OFFICE_DRIVE_PATH_NAME = 'drive/root/';
    @testVisible private static final String OFFICE_DOWNLOAD_PATH   = 'drive/items/';
    @testVisible private static final String CONTENT_TYPE           = 'Content-Type';
    @testVisible private static final String APPLICATION_URLENCODED = 'application/x-www-form-urlencoded';
    @testVisible private static final String APPLICATION_STREAM     = 'application/octet-stream';
    @testVisible private static final String APPLICATION_JSON       = 'application/json';
    @testVisible private static final String POST                   = 'POST';
    @testVisible private static final String UTF_8                  = 'UTF-8';
    @testVisible private static final String ACCESS_TOKEN           = 'access_token';
    @testVisible private static final String ID                     = 'id';
    @testVisible private static final String WEBURL                 = 'webUrl';
    @testVisible private static final String DOWNLOADURL            = '@microsoft.graph.downloadUrl';
    @testVisible private static final String CONTENT                = ':/content';
    @testVisible private static final String PUT                    = 'PUT';
    @testVisible private static final String AUTHORIZATION          = 'Authorization';
    @testVisible private static final String BEARER                 = 'Bearer ';
    @testVisible private static final String ACCEPT                 = 'ACCEPT';
    @testVisible private static final String PREVIEW                = '/preview';
    @testVisible private static final String PREVIEWCONTENT                = '/content';
    @testVisible private static final String CONTENT_LENGTH         = 'Content-Length';
    @testVisible private static final String GETURL                 = 'getUrl';
    @testVisible private static final String GET                    = 'GET';
    @testVisible private static final String HospitaityStatmentsPath    = 'drive/items/01NUEFKAXV6QG43QJBTFDKMNYWEVR5FGE7/';

    public static Boolean isForHandoverStatment = false;

    public Office365RestService() {
    }
    /*********************************************************************************
    * Class Name : sendEmail
    * Description : class for Storing the responce
    **********************************************************************************/  
    // public class downloadResponceWrapper{
    //     public class value{
    //         string id;
    //     }
    //     List<value> value;
    // }
    public class ResponseWrapper {
        public @testVisible String downloadUrl;
        public @testVisible String id;
        public @testVisible String webUrl;
        public List<Error_Log__c> Error_log_lst = new List<Error_Log__c>();
        public ResponseWrapper(){}
        public ResponseWrapper( String id, String webUrl, String downloadUrl ){
            this.downloadUrl = downloadUrl;
            this.id = id;
            this.webUrl = webUrl;
        }
        public ResponseWrapper( List<Error_Log__c> errLst ){
            this.Error_log_lst = errLst;
        }
        public ResponseWrapper( String downloadUrl ){
            this.downloadUrl = downloadUrl;
        }
    }

    
    
    /*********************************************************************************
    * Method Name : getNewBearerToken
    * Description : Bring the access_token.
    * Return Type : void
    * Parameter(s): no
    **********************************************************************************/  
    public static String getNewBearerToken() {
        Credentials_Details__c office365RestServicesObj =Credentials_Details__c.getInstance('office365RestServices'); // Use existing cs 'Credentials details'
        String client_id = string.isNotBlank(office365RestServicesObj.User_Name__c)?office365RestServicesObj.User_Name__c:'' ;
        String Client_secret = string.isNotBlank(office365RestServicesObj.Password__c)?office365RestServicesObj.Password__c:'' ;
        String Resource = string.isNotBlank(office365RestServicesObj.Resource__c)?office365RestServicesObj.Resource__c:'' ; // Create field in cs
        String grant_type = string.isNotBlank(office365RestServicesObj.grant_type__c)?office365RestServicesObj.grant_type__c:'';// Create field in cs
        //System.debug('** o ffice365RestServices -->'+office365RestServices);
        //System.debug('** client_id -->'+client_id);
        //System.debug('** Client_secret -->'+Client_secret);
        //System.debug('** Resource -->'+Resource);
        //System.debug('** grant_type -->'+grant_type);
        if( String.isNotBlank( client_id ) && String.isNotBlank( Client_secret ) && 
            String.isNotBlank( Resource ) && String.isNotBlank( grant_type )) {
                HttpRequest request = new HttpRequest();
                request.setEndpoint( Office_REST_BASEURL + OFFICE_LOGIN );
                request.setMethod( POST ); // Store in static variables
                request.setHeader( CONTENT_TYPE , APPLICATION_URLENCODED ); // Store in static variables
                String payload = 'client_id=' + EncodingUtil.urlEncode( client_id,UTF_8 ) +
                    '&client_secret=' + EncodingUtil.urlEncode( Client_secret,UTF_8 ) +
                    '&Resource=' + EncodingUtil.urlEncode( Resource , UTF_8 )+
                    '&grant_type=' + grant_type;
                request.setBody(payload);
                System.debug('request ::: ' + request);
                System.debug('payload ::: ' + payload);
                HttpResponse response = new Http().send(request); // create error log - GenericUtility.createErrorLog
                if(response.getStatusCode() == 200) {
                    Map<String, Object> reponcemessage = (Map<String, Object>) JSON.deserializeUntyped(response.getBody()); // change variable name
                    //System.debug(reponcemessage.get(ACCESS_TOKEN));
                    return (String)(reponcemessage.get(ACCESS_TOKEN));
                }else{
                    return null;
                }
            } else{
                return null;
            }
    }
    /*********************************************************************************
    * Method Name : uploadToOffice365Result
    * Description : return Wrap http responce for upload
    * Return Type : UploadResponseWrapper
    * Parameter(s): fileToUpload, fileName, AccountId, BookingunitId, CaseId, ClId
    **********************************************************************************/  
    public static  ResponseWrapper uploadToOffice365Result(
                                                                 blob fileToUpload,
                                                                 String fileName,
                                                                 String accountId,
                                                                 String buId, 
                                                                 String ClId, 
                                                                 String CaseId
                                                                ) {// Add parameters - String accountId, String buId, String ClId, String CaseId
        HttpResponse resultHttpResponse = new HttpResponse();
        resultHttpResponse = uploadToOffice365(fileToUpload, fileName, accountId, buId, buId, CaseId);
        system.debug(' resultHttpResponse.getStatusCode()  : ' + resultHttpResponse.getStatusCode());
        system.debug( ' resultHttpResponse.getBody()  : ' + Json.serializePretty( resultHttpResponse.getBody() )  );
        if(resultHttpResponse.getStatusCode() == 200 || resultHttpResponse.getStatusCode() == 201 ) {
            Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(
                resultHttpResponse.getBody() // Handle response
            );
            if( responseMap.containsKey( ID ) &&
                responseMap.containsKey( DOWNLOADURL ) 
            ) {
                system.debug(  'weburl : ' +   ( String )responseMap.get( WEBURL ));
                ResponseWrapper ResponseWrapperObj = new ResponseWrapper( //variable name should not be  start with an action
                    ( String )responseMap.get( ID) ,
                    ( String )responseMap.get( WEBURL ),
                    ( String )responseMap.get( DOWNLOADURL )
                );
                system.debug(' ResponseWrapperObj  ' + ResponseWrapperObj );
                
                return ResponseWrapperObj;
            }
            else{
                return null;
            } //variable name should be proper and descriptive
        }else{
            Map<String, Object> errorResult = (Map<String, Object>) JSON.deserializeUntyped(resultHttpResponse.getBody());
            List<Error_Log__c> Error_log_lst = new List<Error_Log__c>();
            if( errorResult.containsKey('reason')) {
            Error_Log__c objErrorLog =  GenericUtility.createErrorLog( (String)errorResult.get('reason')
                                                                        , accountId
                                                                        , buId
                                                                        , ClId
                                                                        , CaseId);
            Error_log_lst.add( objErrorLog );
            ResponseWrapper ResponseWrapperObj = new ResponseWrapper( Error_log_lst );
            return ResponseWrapperObj;
            }else {
                return null;
            }
        }
    } 
    /*********************************************************************************
    * Method Name : uploadToOffice365
    * Description : return raw http responce for upload
    * Return Type : HttpResponse
    * Parameter(s): fileToUpload, fileName, AccountId, BookingunitId, CaseId, ClId
    **********************************************************************************/
    public static HttpResponse uploadToOffice365(
                                                 blob fileToUpload,
                                                 String fileName,
                                                 String accountId,
                                                 String buId, 
                                                 String ClId, 
                                                 String CaseId
                                                ) {
        String Access_token = getNewBearerToken(); // Add check for Access_token
        if(String.isNotBlank( Access_token )){
            // Attachment  atchObj = [SELECT
            //                          id,
            //                          name,
            //                          body 
            //                        FROM
            //                          Attachment Where Id = '00P25000005h2aA' Limit 1];
            // fileToUpload = atchObj.body;
            // fileName = atchObj.name;
            system.debug( ' fileToUpload : ' + fileToUpload );
            HttpRequest request = new HttpRequest();
            string encodedFileName = EncodingUtil.urlEncode(fileName,UTF_8);
            system.debug( ' encodedFileName : ' + encodedFileName );            

            request.setEndpoint(BASE_GRAPH_URL+OFFICE_UPLOAD + OFFICE_DRIVE_PATH + encodedFileName + CONTENT);
            request.setMethod(PUT);
            request.setHeader(AUTHORIZATION, BEARER+Access_token );
            request.setHeader(CONTENT_TYPE , 'application/octet-stream');
            request.setHeader( ACCEPT , APPLICATION_JSON );
            System.debug('request:: ' + request);
            //request.setBody( EncodingUtil.base64Encode(fileToUpload));
            request.setBodyAsBlob(  EncodingUtil.base64Decode(EncodingUtil.base64Encode(fileToUpload)));
            Http http = new Http();
            HttpResponse uplinkResponse = http.send(request); //Create error log
            System.debug('uplinkResponse:: ' + uplinkResponse);
            return uplinkResponse;
        }else{
            return null;
        }
    }
    /*********************************************************************************
    * Method Name : downloadFromOffice365
    * Description : return download url of data in wrapper
    * Return Type : ResponseWrapper
    * Parameter(s): fileId, fileName, AccountId, BookingunitId, CaseId, ClId
    **********************************************************************************/
    public static ResponseWrapper downloadFromOffice365(
                                                 String fileId,
                                                 String fileName,
                                                 String accountId,
                                                 String buId, 
                                                 String ClId, 
                                                 String CaseId
                                              ) {
        if( String.isNotBlank(fileId) ){
            return downloadFromOffice365ById(fileId,fileName, accountId, buId, buId, CaseId);
        }else if(String.isNotBlank(fileName)){
            fileId = downloadFromOffice365ByName( fileName, accountId, buId, buId, CaseId);
            return downloadFromOffice365ById(fileId,fileName, accountId, buId, buId, CaseId);     
        }else{
            return null;
        }
    }
    
    /*********************************************************************************
    * Method Name : downloadFromOffice365
    * Description : return download url of data in wrapper
    * Return Type : ResponseWrapper
    * Parameter(s): fileId, fileName, AccountId, BookingunitId, CaseId, ClId
    **********************************************************************************/
    public static ResponseWrapper downloadFromOffice365ById(
                                                 String fileId,
                                                 String fileName,
                                                 String accountId,
                                                 String buId, 
                                                 String ClId, 
                                                 String CaseId
                                              ) {// Add parameters
            String Access_token = getNewBearerToken(); //Add check
            if(String.isNotBlank( Access_token )){
                HttpRequest request = new HttpRequest();
                String Endpoint= BASE_GRAPH_URL+OFFICE_UPLOAD + OFFICE_DOWNLOAD_PATH + fileId + PREVIEW; //Add space
                request.setEndpoint(Endpoint);
                request.setMethod( POST ); //Use static variables
                request.setHeader( AUTHORIZATION , BEARER + Access_token );
                request.setHeader( CONTENT_TYPE, APPLICATION_JSON );
                request.setHeader(CONTENT_LENGTH, '0');
                request.setHeader( ACCEPT, APPLICATION_JSON );
                Http http = new Http();
                HttpResponse downloadResponse = http.send(request);
                // System.debug(downloadResponse) ;// handle exception
                // System.debug('body--'+downloadResponse.getBody());
                // System.debug('Header--'+downloadResponse.getHeaderKeys());
                // System.debug('Header--'+downloadResponse.getHeader('Location'));
                if(downloadResponse.getStatusCode() == 200){
                    Map<String, Object> downloadresult = (Map<String, Object>) JSON.deserializeUntyped(downloadResponse.getBody());//variable name
                    System.debug('--->'+downloadresult);
                    if(downloadresult.containsKey('getUrl')){
                        ResponseWrapper ResponseWrapperObj = new ResponseWrapper( (String)downloadresult.get('getUrl') );
                        return ResponseWrapperObj;    
                    }else{
                        return null;
                    }
                }else {
                    Map<String, Object> errorResult = (Map<String, Object>) JSON.deserializeUntyped(downloadResponse.getBody());
                    List<Error_Log__c> Error_log_lst = new List<Error_Log__c>();
                    if( errorResult.containsKey('reason')) {
                    Error_Log__c objErrorLog =  GenericUtility.createErrorLog( (String)errorResult.get('reason')
                                                                                , accountId
                                                                                , buId
                                                                                , ClId
                                                                                , CaseId);
                    Error_log_lst.add( objErrorLog );
                    ResponseWrapper ResponseWrapperObj = new ResponseWrapper( Error_log_lst );
                    return ResponseWrapperObj;
                    }else {
                        return null;
                    }
                }
            }else{
                return null;
            }
    }
    /*********************************************************************************
    * Method Name : downloadFromOffice365
    * Description : return download url of data in wrapper specially for whtsapp
    * Return Type : ResponseWrapper
    * Parameter(s): fileId, fileName, AccountId, BookingunitId, CaseId, ClId
    **********************************************************************************/
    public static ResponseWrapper downloadFromOffice365ByIdWhatsapp(
                                                 String fileId,
                                                 String fileName,
                                                 String accountId,
                                                 String buId, 
                                                 String ClId, 
                                                 String CaseId
                                              ) {// Add parameters
            String Access_token = getNewBearerToken(); //Add check
            if(String.isNotBlank( Access_token )){
                HttpRequest request = new HttpRequest();
                String Endpoint= BASE_GRAPH_URL+OFFICE_UPLOAD + OFFICE_DOWNLOAD_PATH + fileId + PREVIEWCONTENT; //Add space
                request.setEndpoint(Endpoint);
                request.setMethod( POST ); //Use static variables
                request.setHeader( AUTHORIZATION , BEARER + Access_token );
                request.setHeader( CONTENT_TYPE, APPLICATION_JSON );
                request.setHeader(CONTENT_LENGTH, '0');
                request.setHeader( ACCEPT, APPLICATION_JSON );
                Http http = new Http();
                HttpResponse downloadResponse = http.send(request);
                System.debug(downloadResponse) ;// handle exception
                System.debug('body--'+downloadResponse.getBody());
                System.debug('Header--'+downloadResponse.getHeaderKeys());
                System.debug('Header--'+downloadResponse.getHeader('Location'));
                if(downloadResponse.getStatusCode() == 302){
                    System.debug('Header--'+downloadResponse.getHeader('Location'));
                    ResponseWrapper ResponseWrapperObj = new ResponseWrapper( (String)downloadResponse.getHeader('Location') );
                    System.debug('ResponseWrapper when status 302 :'+ResponseWrapperObj);
                    return ResponseWrapperObj; 

                }else {
                    Map<String, Object> errorResult = (Map<String, Object>) JSON.deserializeUntyped(downloadResponse.getBody());
                    List<Error_Log__c> Error_log_lst = new List<Error_Log__c>();
                    if( errorResult.containsKey('reason')) {
                    Error_Log__c objErrorLog =  GenericUtility.createErrorLog( (String)errorResult.get('reason')
                                                                                , accountId
                                                                                , buId
                                                                                , ClId
                                                                                , CaseId);
                    Error_log_lst.add( objErrorLog );
                    ResponseWrapper ResponseWrapperObj = new ResponseWrapper( Error_log_lst );
                    return ResponseWrapperObj;
                    }else {
                        return null;
                    }
                }
            }else{
                return null;
            }
    }
    
    /*********************************************************************************
    * Method Name : downloadFromOffice365ByName
    * Description : return id to download file
    * Return Type : HttpResponse
    * Parameter(s): fileName, AccountId, BookingunitId, CaseId, ClId
    **********************************************************************************/
    public static String downloadFromOffice365ByName(String fileName,
                                                 String accountId,
                                                 String buId, 
                                                 String ClId, 
                                                 String CaseId
                                                ) {
        String Access_token = getNewBearerToken(); //Add check
        if(String.isNotBlank( Access_token )){
            String fileNameUpdated = '\''+ fileName + '\'';
            //String SEARCH='search(q='+fileNameUpdated+')';
            String SEARCH= 'children?$filter=startswith(name,'+fileNameUpdated+')';
            HttpRequest request = new HttpRequest();
            String strEndpoint= BASE_GRAPH_URL + OFFICE_UPLOAD + OFFICE_DRIVE_PATH_NAME + SEARCH ; //Add space
            if(isForHandoverStatment == true){
                strEndpoint= BASE_GRAPH_URL + OFFICE_UPLOAD + HospitaityStatmentsPath + SEARCH ; //Add space
            }
            system.debug('strEndpoint is : '+strEndpoint);
            request.setEndpoint(strEndpoint);
            request.setMethod( GET );
            request.setHeader( AUTHORIZATION , BEARER + Access_token );
            request.setHeader( CONTENT_TYPE, APPLICATION_STREAM );
            request.setHeader(CONTENT_LENGTH, '0');
            request.setHeader( ACCEPT, APPLICATION_JSON );
            Http http = new Http();
            HttpResponse downloadResponse = http.send(request);
            system.debug('downloadResponse.getBody() '+downloadResponse.getBody());
            if(downloadResponse.getStatusCode() == 200) {
                System.debug('Response:====='+Json.serializePretty(downloadResponse.getBody())  );
                Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(
                     downloadResponse.getBody() // Handle response
                 );
                System.debug('responseMap---------->'+responseMap);
                List<Object> listObject = (List<Object>)responseMap.get('value');
                List<Map<String, Object>> listMapObject = new List<Map<String, Object>>();
                for(Object objectItr : listObject){
                    Map<String, Object> mapObject = (Map<String, Object>)objectItr;
                    listMapObject.add(mapObject);
                }
                system.debug('---------->'+( List<Object> )responseMap.get( 'value' ));
                system.debug('-------Listmap--->'+JSON.serializePretty(listMapObject));
                if(listMapObject.isEmpty()){
                    return '';
                }else{
                    system.debug('-------Listmap--->'+JSON.serializePretty(listMapObject[0].get('id')));
                    string id = String.isNotBlank((String)listMapObject[0].get('id')) ? (String)listMapObject[0].get('id') : '';
                    return id ;
                }
                
            }else{
                return null;
            }
        }else {
            return null;
        }                                               
    }                                                
    
}
/*
System.debug('.....................'+ 
Office365RestService.downloadFromOffice365ByName('OoPdfFormExample.pdf','','','','')); 

System.debug('.....................'+ 
Office365RestService.uploadToOffice365Result(Blob.valueOf('abcd'),'TTF.pdf','','','',''));

System.debug('.....................'+ 
Office365RestService.downloadFromOffice365('01NUEFKAR3ZP2BFWRPDZC3Y7HNECBB5BHB','','','','','')); 

*/