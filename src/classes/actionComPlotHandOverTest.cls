@isTest 

public class actionComPlotHandOverTest{
    
    static testMethod void Test1(){       
           
      
        test.StartTest();      
        
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(10));  
        
        actionComPlotHandOver.PlotHandoverHttpSoap11Endpoint  objReq = new actionComPlotHandOver.PlotHandoverHttpSoap11Endpoint ();        
        objReq.getFacilityDue('2-id','FACILITY_DUE_YN','SFDC','123','2-CaseID','','','','','');       
        
        test.StopTest();
    
    }
    
    static testMethod void Test2(){       
           
      
        test.StartTest();      
        
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(11));  
        
        actionComPlotHandOver.PlotHandoverHttpSoap11Endpoint  objReq = new actionComPlotHandOver.PlotHandoverHttpSoap11Endpoint ();        
        List<processXxdcProcessServiceWsPlsqlSoPlot.APPSXXDC_PROCESS_SERX1794747X1X5> lstObjPro = new List<processXxdcProcessServiceWsPlsqlSoPlot.APPSXXDC_PROCESS_SERX1794747X1X5>();
        objReq.GetPlotHandoverDetails('test','test','test',lstObjPro );       
        
        test.StopTest();
    
    }
    
    static testMethod void Test3(){       
           
      
        test.StartTest();      
        
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(12));  
        
        actionComPlotHandOver.PlotHandoverHttpSoap11Endpoint  objReq = new actionComPlotHandOver.PlotHandoverHttpSoap11Endpoint ();        
        List<processXxdcProcessServiceWsPlsqlSoPlot.APPSXXDC_PROCESS_SERX1794747X1X5> lstObjPro = new List<processXxdcProcessServiceWsPlsqlSoPlot.APPSXXDC_PROCESS_SERX1794747X1X5>();
        objReq.UpdatePlotHandoverDetails('test','test','test',lstObjPro );       
        
        test.StopTest();
    
    }
    
    static testMethod void Test4(){       
           
      
        test.StartTest();      
        
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(13));  
        
        actionComPlotHandOver.PlotHandoverHttpSoap11Endpoint  objReq = new actionComPlotHandOver.PlotHandoverHttpSoap11Endpoint ();        
        List<processXxdcProcessServiceWsPlsqlSoPlot.APPSXXDC_PROCESS_SERX1794747X1X5> lstObjPro = new List<processXxdcProcessServiceWsPlsqlSoPlot.APPSXXDC_PROCESS_SERX1794747X1X5>();
        objReq.commencementCondition('test','test','test','test');       
        
        test.StopTest();
    
    }
    
    static testMethod void Test5(){       
           
      
        test.StartTest();      
        
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(14));  
        
        actionComPlotHandOver.PlotHandoverHttpSoap11Endpoint  objReq = new actionComPlotHandOver.PlotHandoverHttpSoap11Endpoint ();        
        List<processXxdcProcessServiceWsPlsqlSoPlot.APPSXXDC_PROCESS_SERX1794747X1X5> lstObjPro = new List<processXxdcProcessServiceWsPlsqlSoPlot.APPSXXDC_PROCESS_SERX1794747X1X5>();
        objReq.invoiceAmountAreavariation('test','test','test','test');       
        
        test.StopTest();
    
    }
    

}