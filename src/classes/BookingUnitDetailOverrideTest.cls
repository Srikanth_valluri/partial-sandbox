@isTest
private class BookingUnitDetailOverrideTest{
    static Account objAcc;
    static NSIBPM__Service_Request__c objDealSR;
    static list<Booking__c> listCreateBookingForAccount;
    static list<Booking_Unit__c> listCreateBookingUnit;
    static Location__c objL;
    
    static void init(){
        objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objDealSR.Id, 1);
        insert listCreateBookingForAccount;
        
        objL = new Location__c();
        objL.Name = 'DFA';
        objL.Location_Id__c = 'DFA';
        insert objL;
        
        listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        listCreateBookingUnit[0].Unit_Name__c = 'DFA/ABC/123';
        listCreateBookingUnit[0].Building_Name__c = 'DFA';
        insert listCreateBookingUnit;
    }
    
    private static testMethod void positiveTest(){
        init();
        FM_User__c objUser = new FM_User__c();
        objUser.Building__c = objL.Id;
        objUser.FM_User__c = UserInfo.getUserId();
        objUser.FM_Role__c = 'FM Admin';
        insert objUser;
        
        Test.setCurrentPage(Page.Booking_Unit_Detail_Override);
        ApexPages.StandardController sc = new ApexPages.StandardController(listCreateBookingUnit[0]);
        BookingUnitDetailOverride objCls = new BookingUnitDetailOverride(sc);
        objCls.redirectToDetail();
    }
    
    private static testMethod void negativeTest(){
        init();
        
        Test.setCurrentPage(Page.Booking_Unit_Detail_Override);
        ApexPages.StandardController sc = new ApexPages.StandardController(listCreateBookingUnit[0]);
        BookingUnitDetailOverride objCls = new BookingUnitDetailOverride(sc);
        objCls.redirectToDetail();
    }
}