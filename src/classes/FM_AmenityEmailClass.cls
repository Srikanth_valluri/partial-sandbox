/*-------------------------------------------------------------------------------------------------
Description: to send email to customer when booking is confirmed
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 20-11-2018       | Lochana Rajput   | 1. Get rules for the amenity and attach to email
=============================================================================================================================
*/
public without sharing class FM_AmenityEmailClass {
	@InvocableMethod
	public static void sendConfirmationEmail(List<FM_Case__c> lstFMCases) {
		Set<Id> ids = new Set<Id>();
		List<Error_Log__c> lstErrorLogs = new List<Error_Log__c>();
		Map<Id, Id> mapCDId_ResId = new Map<Id, Id>();
		for(FM_Case__c obj : lstFMCases) {
			ids.add(obj.Resource__c);
		}
		System.debug('===Resource__c====' +lstFMCases[0].Resource__c);
		if(ids.size() > 0) {
			for(ContentDocumentLink CDL : [select id,LinkedEntityId,ContentDocumentId
	                                       from ContentDocumentLink
	                                       where LinkedEntityId IN : ids]) {
	           mapCDId_ResId.put(CDL.ContentDocumentId, CDL.LinkedEntityId);
	        }//for
		}

		System.debug('==mapCDId_ResId==' + mapCDId_ResId);
		Map<Id, Id> mapResId_CVId = new Map<Id, Id>();
        for(contentVersion CV : [select id,ContentDocumentId
                                from contentVersion
                                where ContentDocumentId IN: mapCDId_ResId.keySet()]) {
            if(mapCDId_ResId.containsKey(CV.ContentDocumentId)) {
                mapResId_CVId.put(mapCDId_ResId.get(CV.ContentDocumentId), CV.Id);
            }
        }//for
		System.debug('==mapResId_CVId==' + mapResId_CVId);
		//Template when amenity has rules and regulations
		EmailTemplate templateObjWithAtt = [SELECT Id
									FROM EmailTemplate
									WHERE DeveloperName = : Label.AmenityConfirmationTemplateLabel
									LIMIT 1];
		//Template when amenity does not have rules and regulations
		EmailTemplate templateObj = [SELECT Id
									FROM EmailTemplate
									WHERE DeveloperName = : Label.AmenityConfirmationWithoutAttachmentTemplateLabel
									LIMIT 1];
		Contact objCon = [SELECT Id FROm Contact LIMIT 1];
		List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
		if(templateObj != NULL && templateObjWithAtt != NULL) {
			for(FM_Case__c objFMCase : lstFMCases) {
				Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
				email.setToAddresses(new String[]{objFMCase.Tenant_Email__c});
				email.setTemplateId(templateObj.ID);
				email.setWhatId(objFMCase.Id);
				if(mapResId_CVId.containsKey(objFMCase.Resource__c)) {
					email.setTemplateId(templateObjWithAtt.ID);
					email.setEntityAttachments(new String[] {mapResId_CVId.get(objFMCase.Resource__c)});
				}
				email.setTargetObjectId(objCon.Id);
				email.setTreatTargetObjectAsRecipient(false);
				email.setSaveAsActivity(false);
				emailList.add(email);
				system.debug('emailList'+emailList);
			}//for
			//Send email
			 Messaging.SendEmailResult[] emailResults= Messaging.sendEmail(emailList);

			 for(Messaging.SendEmailResult er : emailResults) {
				 for(Messaging.SendEmailError error : er.getErrors()) {
					 lstErrorLogs.add(new Error_Log__c(Process_Name__c = 'Amenity Booking',
					 Error_Details__c = error.getMessage()));
				 }
			 }//for
		}//if
		insert lstErrorLogs;
	}//method
}