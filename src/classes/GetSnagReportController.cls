/********************************************************************************
* Description - Controller of  GetSnagReport to show snag report of Case        *
*                                                                               *
* Version            Date            Author                    Description      *
* 1.0                17/01/2019      Arjun Khatri              Initial Draft    *
********************************************************************************/
public Class GetSnagReportController {
    
    String caseId = '';
    String onsiteUrl = '';
    static String  strSnagReportName = 'Snag Report';
    
    public GetSnagReportController() {}
    
    public GetSnagReportController(ApexPages.standardController controller) {
        caseId = ApexPages.currentPage().getParameters().get('id');
        System.debug('caseId == >' + caseId);
        //init();
    }   
    
    public PageReference init() {
        if(String.isNotBlank(caseId) ) {
            List<Case> lstCase = [SELECT Id
                                    , AccountId
                                    , CaseNumber
                                    , Booking_Unit__r.Unit_Name__c
                                    , Booking_Unit__c
                                    , Booking_Unit__r.Property_Name_Inventory__c
                                    , (SELECT 
                                            Id 
                                            , Name 
                                        FROM 
                                            SR_Attachments__r 
                                        WHERE 
                                            Name = 'Snag Report' AND Attachment_URL__c != '')
                                    FROM 
                                        Case
                                    WHERE 
                                        Id =: caseId
                                        AND
                                     ( RecordType.DeveloperName = 'Handover' 
                                        OR RecordType.DeveloperName = 'Early_Handover' ) ];
            
            System.Debug('lstCase == >' + lstCase);
            if(lstCase.size() > 0 
            //&& String.isNotBlank(lstCase[0].Booking_Unit__r.Property_Name_Inventory__c)
            && String.isNotBlank(lstCase[0].Booking_Unit__r.Unit_Name__c)) {
               
               //Format onsite end point URL
                String projectName = lstCase[0].Booking_Unit__r.Unit_Name__c.substringBefore('/');
                if(SNAG_URLs__c.getInstance(projectName) != null
                && SNAG_URLs__c.getInstance(projectName).URL__c != NULL) {
                    String propertyName = SNAG_URLs__c.getInstance(projectName).URL__c;
                    
                    onsiteUrl = String.format(Label.Onsite_Snag_SRF_PDF_Direct_Download, 
                                New List<String>{propertyName 
                                                , 'GetSRFPDFByRef'
                                                , lstCase[0].Booking_Unit__r.Unit_Name__c } );
                } else {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'SNAG-R url not found.');
                    ApexPages.addMessage(myMsg);
                    return null;
                }
                
            
            System.debug('onsiteUrl ::: ' + onsiteUrl);
                /*string strEndPointURL = 'https://damac.onsiteapp.co/capitalbay/api/OnsiteApi/GetSRFPDFByRef?ExtRef=CBC/9/903&key=AlrVru8Yy%2fvuHfHqNrFzQyJvIZpyqpmODWWStL8xn3wb0CaQWV0bIA%3d%3d';*/
                
                List<Attachment> lstAttahments = new List<Attachment>();
                
                //Send Api Request
                if(String.isNotBlank(onsiteUrl)) {
                        
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint(onsiteUrl);
                    req.setMethod('GET');
                    req.setHeader('ACCEPT','application/json');
                    req.setHeader('Content-Type', 'application/json');
                    req.setTimeout(120000);
                    
                    Http http = new Http();
                    HTTPResponse res;
                    res = http.send(req);
                
                
                    system.debug('!!!!!!!!!!res'+res.getStatusCode());
                    system.debug('!!!!!!!!!!res'+res.getBody());
                    
                    //Create attachment on Case
                    if(res.getStatusCode() == 200 && String.isNotBlank( res.getBody())) {
                        Blob blobVal = res.getBodyAsBlob();//Blob.valueOf( res.getBody() ) ;
                        system.debug('!!!!!!!!!!blobVal ::  '+blobVal);
                        Attachment objAttahment = new Attachment();
                        objAttahment.Name = strSnagReportName + '.pdf';
                        objAttahment.ContentType = 'application/pdf';
                        objAttahment.Body = blobVal ;
                        objAttahment.ParentId = lstCase[0].Id;
                        lstAttahments.add(objAttahment);
                        insert lstAttahments;
                    }//End Response if
                    
                    //Stamp error log on case 
                    else {
                        errorLogger(res.getBody(),lstCase[0].Id,lstCase[0].Booking_Unit__c);
                    }
                }//End onsite EndPoint url if
                System.debug('Attachment === ' + lstAttahments);
                
                //List of cases to be update
                
                if (lstAttahments.size() > 0 && lstAttahments[0].Id != Null) {
                    System.debug('objAttahment created:: + ' + lstAttahments);
                    
                    //Call method to create SR Attachments.
                    //List<SR_Attachments__c> lstSRAttachment = 
                    updateSnagReport(lstCase[0].Id
                                        , strSnagReportName
                                        , strSnagReportName
                                        , true);
                    
                }//End Attachment if
            }//End list Case Size If
            else {
                ApexPages.addmessage(
                    new ApexPages.message(
                        ApexPages.severity.INFO,
                        'Please Populate Booking Unit or Property for the requested SR.')
                );
                return null;
            }
        }//End Case Id if
        
        PageReference pg = new PageReference('/'+caseId);
        pg.setRedirect(true);
        return pg;
    }
    
    @future(callout = true)
    static public void updateSnagReport( Id caseId 
                                         , String fileDescription
                                         , String attachmentName 
                                         , Boolean delSRAttachment ) {
        System.debug('=== updateSnagReport ====');
        List<SR_Attachments__c> lstSRAttachment;
        List<Case>  lstCase = new List<Case>();
        lstCase = [SELECT Id
                        , CaseNumber
                        , Booking_Unit__c
                        , Booking_Unit__r.Unit_Name__c
                        , (SELECT 
                            Id 
                            , Name 
                            FROM 
                                SR_Attachments__r 
                            WHERE 
                                Name = 'Snag Report' 
                            AND Attachment_URL__c != '')
                    FROM 
                        Case 
                    WHERE Id =: caseId];
        List<Attachment> attachments = [SELECT
                                    Body
                                    , Name
                                FROM 
                                    Attachment
                                WHERE 
                                    ParentId =: caseId];
        Map<Id, Attachment> mapCaseIdAttachment = new Map<Id, Attachment>{caseId => attachments[0]};
        if( lstCase != null && lstCase.size() > 0 ) {
            
            List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest = 
                new  List<UploadMultipleDocController.MultipleDocRequest>();
            lstSRAttachment = new List<SR_Attachments__c>();
            integer intIncrementor = 0;
            List<SR_Attachments__c> lstSRAttachmentToDelete = new List<SR_Attachments__c>();     
            
            
            for(  Case objCase : lstCase ) {                    
                if( objCase.Booking_Unit__r.Unit_Name__c != '' ) {
                    if( mapCaseIdAttachment.get(objCase.Id).Body != null 
                      && String.isNotBlank( mapCaseIdAttachment.get(objCase.Id).Name )) {
                        UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
                        objDocRequest.category =  'Document';
                        objDocRequest.entityName = 'Damac Service Requests'; 
                        objDocRequest.fileDescription  =  fileDescription; //'Snag Report';                        
                        
                        intIncrementor++;
                        objDocRequest.fileId = objCase.CaseNumber + '-' 
                                                + String.valueOf(System.currentTimeMillis() + intIncrementor) + '.' 
                                                + mapCaseIdAttachment.get(objCase.Id).Name;
                        objDocRequest.fileName = objCase.CaseNumber+'-' 
                                                + String.valueOf(System.currentTimeMillis() + intIncrementor)+'.'
                                                + mapCaseIdAttachment.get(objCase.Id).Name;
                        
                        System.debug('---System.currentTimeMillis()--'+System.currentTimeMillis());
                        System.debug('---intIncrementor--'+intIncrementor);
                        System.debug('---objDocRequest.fileName--'+objDocRequest.fileName);
                        
                        if(objCase != null){
                            objDocRequest.registrationId =  objCase.CaseNumber;
                        }
                        objDocRequest.sourceFileName  = 'IPMS-' + objCase.CaseNumber + '-' 
                                                        + mapCaseIdAttachment.get(objCase.Id).Name;
                        objDocRequest.sourceId  =  'IPMS-'+objCase.CaseNumber + '-' 
                                                    + mapCaseIdAttachment.get(objCase.Id).Name;                        
                        if( mapCaseIdAttachment.get(objCase.Id).Body != null ){
                            blob objBlob = mapCaseIdAttachment.get(objCase.Id).Body;
                            if(objBlob != null){
                                objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
                            }
                        }
                        //objDocRequest.base64Binary  =  crfAttachmentBody;
                        lstMultipleDocRequest.add(objDocRequest);
                    }
                    if( delSRAttachment ) {             
                        if( objCase.SR_Attachments__r.size() > 0 ) {
                            lstSRAttachmentToDelete.addAll(objCase.SR_Attachments__r);
                        }
                    }
                    /*objCase.Snags_Reported__c = true;
                    lstCaseToUpdate.add(objCase);*/
                    
                    SR_Attachments__c objAttach = new SR_Attachments__c();
                    objAttach.Case__c  = objCase.Id;
                    objAttach.isValid__c = false;//true;
                    objAttach.IsRequired__c = true;
                    objAttach.Name = attachmentName; //'Snag Report';
                    //objAttach.Attachment_URL__c = 'www.salesforce.com';
                    objAttach.Need_Correction__c =  false;
                    lstSRAttachment.add(objAttach);     
                }
            }
            system.debug('lstMultipleDocRequest : ' +lstMultipleDocRequest );
            if( lstMultipleDocRequest.Size() > 0 ) {
                UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();
                if( !Test.isRunningTest() ) { 
                    MultipleDocData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocRequest);
                }else {
                    
                    List<UploadMultipleDocController.MultipleDocResponse> data = new List<UploadMultipleDocController.MultipleDocResponse>();
                    UploadMultipleDocController.MultipleDocResponse objMultipleDocResponse = new UploadMultipleDocController.MultipleDocResponse();
                    objMultipleDocResponse.PROC_STATUS = 'S';
                    objMultipleDocResponse.url = 'www.google.com';
                    data.add(objMultipleDocResponse);
                    
                    MultipleDocData.Data = data;
                }
                System.debug('--MultipleDocData--'+MultipleDocData);
                if( MultipleDocData != null && MultipleDocData.status == 'Exception' ) {
                    for( Case objCase : lstCase) {
                        errorLogger(MultipleDocData.message,objCase.Id,objCase.Booking_Unit__c);
                    }
                }                                   
                if(MultipleDocData != null && (MultipleDocData.Data == null || MultipleDocData.Data.Size() == 0) ) {
                    for( Case objCase : lstCase) {
                        errorLogger(MultipleDocData.message,objCase.Id,objCase.Booking_Unit__c);
                    }                           
                }
    
                if( MultipleDocData != null ) {
                    for( Integer objInt = 0; objInt < lstSRAttachment.Size(); objInt++ ) {
                        UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[objInt];
                        if( objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url) ) {
                            lstSRAttachment[objInt].Attachment_URL__c = objData.url;
                        }
                    }
                    system.debug('lstSRAttachment : ' +lstSRAttachment );
                    if( lstSRAttachment.Size() > 0 ){
                        insert lstSRAttachment;
                    }  
                }
            }
            
            if ( delSRAttachment && lstSRAttachmentToDelete != null && lstSRAttachmentToDelete.size() > 0 ) {
                Delete lstSRAttachmentToDelete;
            }
            
            List<Case> lstCaseToUpdate = new List<Case>();
            if(lstSRAttachment.size() > 0 && lstSRAttachment != Null) {
                lstCase[0].SNAG_Report_Sent__c = true;
                lstCaseToUpdate.add(lstCase[0]);
            }
            //Update Snag on Case
            if(lstCaseToUpdate.size() > 0) {
                update lstCaseToUpdate;
            }
        }
    }

    private static void errorLogger(string strErrorMessage, string strCaseID,string strBookingUnitID){
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage.stripHtmlTags();
        if(String.isNotBlank(strCaseID) && strCaseID.startsWith('500')){
            objError.Case__c = strCaseID;
        }
        if(String.isNotBlank(strBookingUnitID)){
            objError.Booking_unit__c = strBookingUnitID;
        }
        insert objError;
    }
}