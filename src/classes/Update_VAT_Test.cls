@isTest
private class Update_VAT_Test {
    static testMethod void myUnitTest(){
     
        Account agency1 = InitialiseTestData.getCorporateAccount('AgencyVAT');
        insert agency1;
        
        Agent_Site__c agSite= new Agent_Site__c();
        agSite.Name='UAE';
        agSite.Agency__c=agency1.Id;
    
           List<NSIBPM__SR_Template__c> SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
            List<NSIBPM__Service_Request__c> SRList = InitialiseTestData.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Update'),
                                               NSIBPM__SR_Template__c = SRTemplateList[0].Id),
                    new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Update'),
                                                   NSIBPM__SR_Template__c = SRTemplateList[0].Id)});


            NSIBPM__Service_Request__c serviceRequestObject = SRList[0] ;
            serviceRequestObject.NSIBPM__Customer__c = agency1.Id ;
            serviceRequestObject.Last_Name__c = 'VATUser' ;
            update serviceRequestObject ;
        
        list<NSIBPM__Step__c> createStepList = InitialiseTestData.createTestStepRecords(new List<NSIBPM__Step__c>{new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id),new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id)});
        
        Update_VAT objCC = new Update_VAT();
        objCC.EvaluateCustomCode(null,createStepList[0]);
    }
}