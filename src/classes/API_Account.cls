@RestResource(urlMapping='/api_account/*')
global with sharing class API_Account {
    
    @HttpGet
    global static void doGet() {
        RestRequest request = RestContext.request;
       
        String recordType = request.requestURI.substring(request.requestURI.lastIndexOf('/' ) + 1);
        String queryOffsetString = RestContext.request.params.get('offset');
        String queryLimitString = RestContext.request.params.get('limit');
        String sortBy = RestContext.request.params.get('sortBy');
        String sortType = RestContext.request.params.get('sortType');
        
        String whereClause = RestContext.request.params.get('whereClause');
        system.debug(recordType);
        Integer queryOffset = 0;
        Integer queryLimit = 15;
        
        if (!String.isBlank(queryOffsetString)) {
            queryOffset = Integer.valueOf(queryOffsetString);
        }
        
        if (!String.isBlank(queryLimitString)) {
            queryLimit = Integer.valueOf(queryLimitString);
        }
        List<Account> accountList = getAccountList(queryOffset, queryLimit, sortBy, sortType, whereClause, recordType);
        
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(accountList));        
    }
    
    global static List<Account> getAccountList(Integer queryOffset, Integer queryLimit, String sortBy, String sortType, String whereClause, String recordType) {
        if (String.isNotBlank(whereClause)) {
            whereClause = ' AND ' + whereClause;
        } else {
            whereClause = '';
        }
        if (String.isBlank(recordType) || recordType =='api_account') {
           recordType = ''; 
        } else {
            recordType = ' AND RecordType.DeveloperName = \'' + recordType + '\'';  
        }
        system.debug(recordType);
        if (String.isBlank(sortBy)) {
            sortBy = 'Name';
        }

        if (String.isBlank(sortType)) {
            sortType = 'ASC';
        }
        String accountQuery = SOQLHelper.getObjectQuery('Account');
        accountQuery += ' WHERE Id != null ';
        accountQuery += recordType;
        accountQuery += whereClause;
        accountQuery += ' ORDER BY ' + sortBy + ' ' + sortType;
        accountQuery += ' LIMIT :queryLimit ';
        accountQuery += ' OFFSET :queryOffset';
        system.debug(accountQuery);
        List<account> accountList = Database.query(accountQuery);
        return accountList;
    }
}