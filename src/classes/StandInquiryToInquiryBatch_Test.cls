@isTest
public class StandInquiryToInquiryBatch_Test {
    public static testMethod void method1() {
        List<Stand_Inquiry__c> toInsertList = new List<Stand_Inquiry__c>();
        for(Integer i=0; i<50; i++){
            Stand_Inquiry__c inq = new Stand_Inquiry__c ();
            inq.First_Name__c = 'Test on Jan07-';
            inq.Last_Name__c = String.valueOf(i+1);
            inq.Age__c = '25-30';
            inq.Age_Above_30_Years__c = 'Yes';
          //  inq.Agency__c = '0011X00000RFgJL'; //Lookup Account
           // inq.Agent_Name__c = '0031X00000M0r2b'; //Lookup Contact
            inq.Assigned_PC__c = UserInfo.getUserId();
            inq.Budget__c = '2M-5M';
           // inq.Campaign__c = 'a123O000000Cud1';// Lookup(Marketing Campaign)
            inq.City__c = 'Test City Tokyo';            
            inq.Comments__c = 'Test for Mass Insert of Stand Inquiries';
            inq.Country__c = 'Argentina';
            inq.Country_of_Permanent_Residence__c = 'Argentina';
            inq.Disposable__c = 'Yes';
            inq.Living_In_Dubai__c = 'Yes';
            inq.You_rent_or_own_your_current_residence__c = 'Rent';
            inq.Mobile_Phone_Encrypt__c = '8977365305';
            inq.Email__c = 'testemail@test123.com';            
            inq.Expected_Usage_of_Property__c = 'Yes';
            inq.For_how_long__c = '7 days';
            inq.Gender__c = 'Male';
            inq.Have_you_purchased_a_home_before__c = 'Yes';
            inq.Purchased_with_DAMAC_before__c = 'Yes';
            inq.Visited_Dubai_Before__c = 'Yes';
            inq.Interested_Development__c = 'DAMAC Hills';
            inq.Interested_Location__c = 'DUBAI';            
            inq.Potential_Individual_Agent__c = 'Yes';
            inq.City_of_Permanent_Residence__c = 'Buenos Aires';
            inq.Language_2__c = 'Afghani';
            inq.Language_3__c = 'Arabic';
            inq.Meeting_Type__c = 'Direct Tour';
            inq.Mobile_CountryCode__c = 'India: 0091';
            inq.Mobile_Country_Code_2__c = 'India: 0091';
            inq.Mobile_Phone_Encrypt_2__c = '8977365305';
            inq.Monthly_Income_AED__c = '15K-30K';
            inq.Nationality__c = 'Argentinean';
            inq.How_many_bedrooms_are_you_looking_for__c = '2 to 3';
            inq.NSUUID__c = '89773653099' + String.valueOf(i);
            inq.Ported__c = 'Yes';
            inq.Preferred_Language__c = 'Azerbaijani';            
            inq.Profession__c = 'Developer';
            inq.Interested_Projects__c = 'Dubai';
            inq.Property_Type__c = 'Individual';
            inq.Purchased_before_in_preferred_city__c = 'No';
            inq.Reachable__c = 'Reachable';
            inq.Readiness__c = 'Yes';
            inq.Resident_Type__c = 'Resident';
            inq.Roaming__c = 'Yes';            
            inq.Tour_Date_Time__c = System.now();
           // inq.Tour_Outcome__c = '8977365305';
            inq.Toxic__c = 'No';
            inq.Validity__c = 'Unknown';
            inq.Whats_App_Initiated__c = true;
            inq.Who_is_your_current_employer__c = 'Unknown';
            inq.Willing_to_meet_this_week__c = 'No';
            inq.BVEmailStatus__c = 'Valid';
            inq.BVSecondaryStatus__c = 'role_based';           
            toInsertList.add(inq);
        }
        insert toInsertList;
        Database.executeBatch(new StandInquiryToInquiryBatch(), 200);


    }
}