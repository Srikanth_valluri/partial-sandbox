/**
 * Utility class for CTI Integration webservices
 */
public class CtiTntegrationUtility {

    /**
     * Method to get account for calling number
     */
    public static List<Account> getRegisteredAccount(String callingNumber) {
        List<Account> accounts = new List<Account>();
        accounts = searchAccount(callingNumber);
        if (accounts.size() == 0) {
            accounts = searchContact(callingNumber);
        }
        System.debug('>>>>>>>>>>>>>> accounts : ' + accounts);
        return accounts;
    }

    public static List<Account> searchAccount(String callingNumber) {
        List<Account> accounts = new List<Account>();
        String baseQuery =
            'SELECT ' +
                'Id, ' +
                'Name, ' +
                'FAM_CAUTION__c,'+
                'AnnualRevenue, ' +
                'Primary_Language__c, ' +
                'Primary_CRE__c, ' +
                'Secondary_CRE__c, ' +
                'Tertiary_CRE__c, ' +
                'Party_ID__c, ' +
                'Customer_Type__c, ' +
                'Record_Type_Name__c, ' +
                'Mobile_Phone_Encrypt__pc, ' +
                'Mobile_Phone_Encrypt_2__pc, ' +
                'Mobile_Phone_Encrypt_3__pc, ' +
                'Mobile_Phone_Encrypt_4__pc, ' +
                'Mobile_Phone_Encrypt_5__pc, ' +
                'Mobile__c, ' +
                'Telephone__c, ' +
                'Customer_Category__c, ' +
                'Fax__c ' +
            ' FROM ' +
                'Account';

        String whereClause = ' WHERE ';
        Boolean firstField = false;
        for (Schema.FieldSetMember field : getAccounttPhoneFieldsToSearch()) {
            if (!firstField) {
                whereClause += ' ' + field.getFieldPath() + ' = ' + '\'' + callingNumber + '\'';
                firstField = true;
            } else {
                whereClause += ' OR ' + field.getFieldPath() + ' = ' + '\'' + callingNumber + '\'';
            }
        }

        String accountQuery = baseQuery + whereClause;
        System.debug('======== accountQuery : ' + accountQuery);
        accounts = Database.query(accountQuery);
        return accounts;
    }

    public static List<Account> searchContact(String callingNumber) {
        List<Account> accounts = new List<Account>();
        List<Contact> contacts = new List<Contact>();
        String baseQuery =
            'SELECT ' +
                'Id, ' +
                'Name, ' +
                'Account.Id, ' +
                'Account.Name, ' +
                'Account.AnnualRevenue, ' +
                'Account.FAM_CAUTION__c, ' +
                'Account.Primary_Language__c, ' +
                'Account.Primary_CRE__c, ' +
                'Account.Secondary_CRE__c, ' +
                'Account.Tertiary_CRE__c, ' +
                'Account.Party_ID__c, ' +
                'Account.Customer_Type__c, ' +
                'Account.Record_Type_Name__c, ' +
                'Account.Mobile__c, ' +
                'Account.Telephone__c, ' +
                'Account.Customer_Category__c, '+
                'Account.Fax__c ' +
                
            ' FROM ' +
                'Contact';

        String whereClause = ' WHERE ';
        Boolean firstField = false;
        for (Schema.FieldSetMember field : getContactPhoneFieldsToSearch()) {
            if (!firstField) {
                whereClause += ' ' + field.getFieldPath() + ' = ' + '\'' + callingNumber + '\'';
                firstField = true;
            } else {
                whereClause += ' OR ' + field.getFieldPath() + ' = ' + '\'' + callingNumber + '\'';
            }
        }

        String contactQuery = baseQuery + whereClause;
        System.debug('======== contactQuery : ' + contactQuery);
        contacts = Database.query(contactQuery);
        if (contacts.size() > 0) {
            System.debug('=========== contacts : ' + contacts);
            if (contacts[0].Account != null) {
                System.debug('=========== contacts[0].Account : ' + contacts[0].Account);
                accounts.add(contacts[0].Account);
            }
        }
        return accounts;
    }

    /**
    /**
    * Method to get datetime for passed string in user timezone.
    * @param: dateTimeString: Date time value in string format
    * @return: DateTime
    **/
    public static DateTime getDateTimeForUserTimeZone(String dateTimeString) {

        DateTime newDateTime;
        if(String.isBlank(dateTimeString)) {
            return newDateTime;
        }
        list<String> spiltStrList = dateTimeString.split(' ');
        if(spiltStrList.size() >= 2) {
            list<String> dateComponentsList = spiltStrList[0].split('/');
            list<String> timeComponentsList = spiltStrList[1].split(':');

            if(dateComponentsList.size() == 3 && timeComponentsList.size() == 3) {
                newDateTime = DateTime.newInstance(Integer.valueOf(dateComponentsList[2]),
                                                   Integer.valueOf(dateComponentsList[0]),
                                                   Integer.valueOf(dateComponentsList[1]),
                                                   Integer.valueOf(timeComponentsList[0]),
                                                   Integer.valueOf(timeComponentsList[1]),
                                                   Integer.valueOf(timeComponentsList[2]));
            }

            return newDateTime;
        }
        return newDateTime;
    }

    public static String getCallingCountryFromCallingNumber(String callingNumber) {
        if (callingNumber.length() < 8) {
            return '';
        }
        if (mapCountryCodeToCountry.get(callingNumber.substring(0, 8)) != null) {
           System.debug('======== : country code : ' + callingNumber.substring(0, 8));
           System.debug('======== : country : ' + mapCountryCodeToCountry.get(callingNumber.substring(0, 8)));
           return mapCountryCodeToCountry.get(callingNumber.substring(0, 8));
        } else if (mapCountryCodeToCountry.get(callingNumber.substring(0, 7)) != null) {
            System.debug('======== : country code : ' + callingNumber.substring(0, 7));
            System.debug('======== : country : ' + mapCountryCodeToCountry.get(callingNumber.substring(0, 7)));
            return mapCountryCodeToCountry.get(callingNumber.substring(0, 7));
        } else if (mapCountryCodeToCountry.get(callingNumber.substring(0, 6)) != null) {
            System.debug('======== : country code : ' + callingNumber.substring(0, 6));
            System.debug('======== : country : ' + mapCountryCodeToCountry.get(callingNumber.substring(0, 6)));
            return mapCountryCodeToCountry.get(callingNumber.substring(0, 6));
        } else if (mapCountryCodeToCountry.get(callingNumber.substring(0, 5)) != null) {
            System.debug('======== : country code : ' + callingNumber.substring(0, 5));
            System.debug('======== : country : ' + mapCountryCodeToCountry.get(callingNumber.substring(0, 5)));
            return mapCountryCodeToCountry.get(callingNumber.substring(0, 5));
        } else if (mapCountryCodeToCountry.get(callingNumber.substring(0, 4)) != null) {
            System.debug('======== : country code : ' + callingNumber.substring(0, 4));
            System.debug('======== : country : ' + mapCountryCodeToCountry.get(callingNumber.substring(0, 4)));
            return mapCountryCodeToCountry.get(callingNumber.substring(0, 4));
        } else if (mapCountryCodeToCountry.get(callingNumber.substring(0, 3)) != null) {
            System.debug('======== : country code : ' + callingNumber.substring(0, 3));
            System.debug('======== : country : ' + mapCountryCodeToCountry.get(callingNumber.substring(0, 3)));
            return mapCountryCodeToCountry.get(callingNumber.substring(0, 3));
        }
        return '';
    }

    public static List<Schema.FieldSetMember> getAccounttPhoneFieldsToSearch() {
        return SObjectType.Account.FieldSets.InboundAccountPhoneFields.getFields();
    }

    public static List<Schema.FieldSetMember> getContactPhoneFieldsToSearch() {
        return SObjectType.Contact.FieldSets.InboundContactPhoneFields.getFields();
    }

    public static Map<String, String> mapCountryCodeToCountry = new Map<String, String> {
        '00971' => 'United Arab Emirates: 00971',
        '00966' => 'Saudi Arabia: 00966',
        '0091' => 'India: 0091',
        '0092' => 'Pakistan: 0092',
        '00962' => 'Jordan: 00962',
        '0044' => 'United Kingdom: 0044',
        '0020' => 'Egypt: 0020',
        '00963' => 'Syria: 00963',
        '001' => 'Canada: 001/United States: 001',
        '00961' => 'Lebanon: 00961',
        '00249' => 'Sudan: 00249',
        '00965' => 'Kuwait: 00965',
        '0098' => 'Iran: 0098',
        '00964' => 'Iraq: 00964',
        '0093' => 'Afghanistan: 0093',
        '00355' => 'Albania: 00355',
        '00213' => 'Algeria: 00213',
        '001684' => 'American Samoa: 001684',
        '00376' => 'Andorra: 00376',
        '00244' => 'Angola: 00244',
        '001264' => 'Anguilla: 001264',
        '00672' => 'Antarctica: 00672',
        '001268' => 'Antigua and Barbuda: 001268',
        '0054' => 'Argentina: 0054',
        '00374' => 'Armenia: 00374',
        '00297' => 'Aruba: 00297',
        '0061' => 'Australia: 0061/Christmas Island: 0061/Cocos Islands: 0061',
        '0043' => 'Austria: 0043',
        '00994' => 'Azerbaijan: 00994',
        '001242' => 'Bahamas: 001242',
        '00973' => 'Bahrain: 00973',
        '00880' => 'Bangladesh: 00880',
        '001246' => 'Barbados: 001246',
        '00375' => 'Belarus: 00375',
        '0032' => 'Belgium: 0032',
        '00501' => 'Belize: 00501',
        '00229' => 'Benin: 00229',
        '001441' => 'Bermuda: 001441',
        '00975' => 'Bhutan: 00975',
        '00591' => 'Bolivia: 00591',
        '00387' => 'Bosnia and Herzegovina: 00387',
        '00267' => 'Botswana: 00267',
        '0055' => 'Brazil: 0055',
        '00246' => 'British Indian Ocean Territory: 00246',
        '001284' => 'British Virgin Islands: 001284',
        '00673' => 'Brunei: 00673',
        '00359' => 'Bulgaria: 00359',
        '00226' => 'Burkina Faso: 00226',
        '00257' => 'Burundi: 00257',
        '00855' => 'Cambodia: 00855',
        '00237' => 'Cameroon: 00237',
        '00238' => 'Cape Verde: 00238',
        '001345' => 'Cayman Islands: 001345',
        '00236' => 'Central African Republic: 00236',
        '00235' => 'Chad: 00235',
        '0056' => 'Chile: 0056',
        '0086' => 'China: 0086',
        '0057' => 'Colombia: 0057',
        '00269' => 'Comoros: 00269',
        '00682' => 'Cook Islands: 00682',
        '00506' => 'Costa Rica: 00506',
        '00385' => 'Croatia: 00385',
        '0053' => 'Cuba: 0053',
        '00599' => 'Curacao: 00599/Netherlands Antilles: 00599',
        '00357' => 'Cyprus: 00357',
        '00420' => 'Czech Republic: 00420',
        '00243' => 'Democratic Republic of the Congo: 00243',
        '0045' => 'Denmark: 0045',
        '00253' => 'Djibouti: 00253',
        '001767' => 'Dominica: 001767',
        '001809' => 'Dominican Republic: 001809',
        '001829' => 'Dominican Republic: 001829',
        '001849' => 'Dominican Republic: 001849',
        '00670' => 'East Timor: 00670',
        '00593' => 'Ecuador: 00593',
        '00503' => 'El Salvador: 00503',
        '00240' => 'Equatorial Guinea: 00240',
        '00291' => 'Eritrea: 00291',
        '00372' => 'Estonia: 00372',
        '00251' => 'Ethiopia: 00251',
        '00500' => 'Falkland Islands: 00500',
        '00298' => 'Faroe Islands: 00298',
        '00679' => 'Fiji: 00679',
        '00358' => 'Finland: 00358',
        '0033' => 'France: 0033',
        '00689' => 'French Polynesia: 00689',
        '00241' => 'Gabon: 00241',
        '00220' => 'Gambia: 00220',
        '00995' => 'Georgia: 00995',
        '0049' => 'Germany: 0049',
        '00233' => 'Ghana: 00233',
        '00350' => 'Gibraltar: 00350',
        '0030' => 'Greece: 0030',
        '00299' => 'Greenland: 00299',
        '001473' => 'Grenada: 001473',
        '001671' => 'Guam: 001671',
        '00502' => 'Guatemala: 00502',
        '00441481' => 'Guernsey: 00441481',
        '00224' => 'Guinea: 00224',
        '00245' => 'Guinea-Bissau: 00245',
        '00592' => 'Guyana: 00592',
        '00509' => 'Haiti: 00509',
        '00504' => 'Honduras: 00504',
        '00852' => 'Hong Kong: 00852',
        '0036' => 'Hungary: 0036',
        '00354' => 'Iceland: 00354',
        '0062' => 'Indonesia: 0062',
        '00353' => 'Ireland: 00353',
        '00441624' => 'Isle of Man: 00441624',
        '00972' => 'Israel: 00972',
        '0039' => 'Italy: 0039',
        '00225' => 'Ivory Coast: 00225',
        '001876' => 'Jamaica: 001876',
        '0081' => 'Japan: 0081',
        '00441534' => 'Jersey: 00441534',
        '007' => 'Kazakhstan: 007/Russia: 007',
        '00254' => 'Kenya: 00254',
        '00686' => 'Kiribati: 00686',
        '00383' => 'Kosovo: 00383',
        '00996' => 'Kyrgyzstan: 00996',
        '00856' => 'Laos: 00856',
        '00371' => 'Latvia: 00371',
        '00266' => 'Lesotho: 00266',
        '00231' => 'Liberia: 00231',
        '00218' => 'Libya: 00218',
        '00423' => 'Liechtenstein: 00423',
        '00370' => 'Lithuania: 00370',
        '00352' => 'Luxembourg: 00352',
        '00853' => 'Macao: 00853',
        '00389' => 'Macedonia: 00389',
        '00261' => 'Madagascar: 00261',
        '00265' => 'Malawi: 00265',
        '0060' => 'Malaysia: 0060',
        '00960' => 'Maldives: 00960',
        '00223' => 'Mali: 00223',
        '00356' => 'Malta: 00356',
        '00692' => 'Marshall Islands: 00692',
        '00222' => 'Mauritania: 00222',
        '00230' => 'Mauritius: 00230',
        '00262' => 'Mayotte: 00262/Reunion: 00262',
        '0052' => 'Mexico: 0052',
        '00691' => 'Micronesia: 00691',
        '00373' => 'Moldova: 00373',
        '00377' => 'Monaco: 00377',
        '00976' => 'Mongolia: 00976',
        '00382' => 'Montenegro: 00382',
        '001664' => 'Montserrat: 001664',
        '00212' => 'Morocco: 00212/Western Sahara: 00212',
        '00258' => 'Mozambique: 00258',
        '0095' => 'Myanmar: 0095',
        '00264' => 'Namibia: 00264',
        '00674' => 'Nauru: 00674',
        '00977' => 'Nepal: 00977',
        '0031' => 'Netherlands: 0031',
        '00687' => 'New Caledonia: 00687',
        '0064' => 'New Zealand: 0064/Pitcairn: 0064',
        '00505' => 'Nicaragua: 00505',
        '00227' => 'Niger: 00227',
        '00234' => 'Nigeria: 00234',
        '00683' => 'Niue: 00683',
        '001670' => 'Northern Mariana Islands: 001670',
        '00850' => 'North Korea: 00850',
        '0047' => 'Norway: 0047/Svalbard and Jan Mayen: 0047',
        '00968' => 'Oman: 00968',
        '00680' => 'Palau: 00680',
        '00970' => 'Palestine: 00970',
        '00507' => 'Panama: 00507',
        '00675' => 'Papua New Guinea: 00675',
        '00595' => 'Paraguay: 00595',
        '0051' => 'Peru: 0051',
        '0063' => 'Philippines: 0063',
        '0048' => 'Poland: 0048',
        '00351' => 'Portugal: 00351',
        '001787' => 'Puerto Rico: 001787',
        '001939' => 'Puerto Rico: 001939',
        '00974' => 'Qatar: 00974',
        '00242' => 'Republic of the Congo: 00242',
        '0040' => 'Romania: 0040',
        '00250' => 'Rwanda: 00250',
        '00590' => 'Saint Barthelemy: 00590/Saint Martin: 00590',
        '00290' => 'Saint Helena: 00290',
        '001869' => 'Saint Kitts and Nevis: 001869',
        '001758' => 'Saint Lucia: 001758',
        '00508' => 'Saint Pierre and Miquelon: 00508',
        '001784' => 'Saint Vincent and the Grenadines: 001784',
        '00685' => 'Samoa: 00685',
        '00378' => 'San Marino: 00378',
        '00239' => 'Sao Tome and Principe: 00239',
        '00221' => 'Senegal: 00221',
        '00381' => 'Serbia: 00381',
        '00248' => 'Seychelles: 00248',
        '00232' => 'Sierra Leone: 00232',
        '0065' => 'Singapore: 0065',
        '001721' => 'Sint Maarten: 001721',
        '00421' => 'Slovakia: 00421',
        '00386' => 'Slovenia: 00386',
        '00677' => 'Solomon Islands: 00677',
        '00252' => 'Somalia: 00252',
        '0027' => 'South Africa: 0027',
        '0082' => 'South Korea: 0082',
        '00211' => 'South Sudan: 00211',
        '0034' => 'Spain: 0034',
        '0094' => 'Sri Lanka: 0094',
        '00597' => 'Suriname: 00597',
        '00268' => 'Swaziland: 00268',
        '0046' => 'Sweden: 0046',
        '0041' => 'Switzerland: 0041',
        '00886' => 'Taiwan: 00886',
        '00992' => 'Tajikistan: 00992',
        '00255' => 'Tanzania: 00255',
        '0066' => 'Thailand: 0066',
        '00228' => 'Togo: 00228',
        '00690' => 'Tokelau: 00690',
        '00676' => 'Tonga: 00676',
        '001868' => 'Trinidad and Tobago: 001868',
        '00216' => 'Tunisia: 00216',
        '0090' => 'Turkey: 0090',
        '00993' => 'Turkmenistan: 00993',
        '001649' => 'Turks and Caicos Islands: 001649',
        '00688' => 'Tuvalu: 00688',
        '001340' => 'U.S. Virgin Islands: 001340',
        '00256' => 'Uganda: 00256',
        '00380' => 'Ukraine: 00380',
        '00598' => 'Uruguay: 00598',
        '00998' => 'Uzbekistan: 00998',
        '00678' => 'Vanuatu: 00678',
        '00379' => 'Vatican: 00379',
        '0058' => 'Venezuela: 0058',
        '0084' => 'Vietnam: 0084',
        '00681' => 'Wallis and Futuna: 00681',
        '00967' => 'Yemen: 00967',
        '00260' => 'Zambia: 00260',
        '00263' => 'Zimbabwe: 00263'
    };
}