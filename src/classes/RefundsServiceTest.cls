@isTest
/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  21/11/2017  Initial Draft
*/
public class RefundsServiceTest {
    static testMethod void Test1(){
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new RefundsMock());        
        RefundsService.getTokenRefund('81965');        
    }
    
    static testMethod void Test11(){
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new RefundsMock.RefundsMock1());        
        RefundsService.getTokenRefund('81965');        
    }
    
    static testMethod void Test2(){
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new RefundsExcessAmountMock());               
        RefundsService.getExcessAmount('81965');        
    }
    
    static testMethod void Test22(){
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new RefundsExcessAmountMock.RefundsExcessAmountMock1());              
        RefundsService.getExcessAmount('81965');        
    }
    
    static testMethod void Test3(){
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new RefundsApprovingAuthoritiesMock());               
        RefundsService.getApprovingAuthorities('81965','Token Refund','Refunds','Pune','Project','','','','','','','','','','','','');    
    }
    
    static testMethod void Test4(){
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new RefundsApprovingAuthoritiesMock.RefundsApprovingAuthoritiesMock1());               
        RefundsService.getApprovingAuthorities('81965','Token Refund','Refunds','Pune','Project','','','','','','','','','','','','');    
    }
    
    
    static testMethod void Test10(){
       RefundsService.RefundRuleResponse obj = new RefundsService.RefundRuleResponse('','','');
       obj.strAllowed = 'Yes';
       obj.strMessage = 'Yes';
       obj.strApprovingAuthority = 'Yes';
       
       RefundsService.RefundResponse obj1 = new RefundsService.RefundResponse('',10,'');
       obj1.strStatus = 'Yes';
       obj1.decAmount = 10;
       obj1.strMessage = 'Yes';
    }
}