@isTest
public with sharing class TestUtility{

    public  List<Account> accInsrt(Integer num){
        Id personAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName()
                                .get('Person Account').getRecordTypeId();
        List<Account> acclist = new List<Account>();
        for(Integer i=0;i<num;i++)
           {
               Account obj=new Account();

               obj.RecordTypeId = personAccountId;
               obj.party_Id__c = '145158'+i;
               obj.firstName = 'firstName'+i;
               obj.lastName = 'lastName'+i;
               obj.Email__c = 'test@test.com';
               obj.Email__pc = 'test@test.com';
               obj.PersonEmail = 'test@test.com';
               obj.Mobile__c = '5652';
               obj.Mobile_Phone_Encrypt__pc = '5652';
               accList.add(obj);
           }//end for
           insert accList;
           System.debug('accList' + accList);

           return accList;
    }

    public List<Case> caseInsert(Integer num){
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName()
                                .get('Change of Details').getRecordTypeId();
        List<Case> caseList = new List<Case>();
        for(Integer i=0;i<num;i++)
            {
                Case obj=new Case();
					   obj.RecordTypeId = recordTypeId;
                       obj.Status='New';
                       obj.Origin='Call';
                       obj.Priority='Medium';
                       caseList.add(obj);
           }//end for
        insert caseList;
        System.debug('caseList' + caseList);

        return caseList;
    }
public List<Case> caseInsertChangeofJointBuyer(Integer num){
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName()
                                .get('Change of Joint Buyer').getRecordTypeId();
        List<Case> caseList = new List<Case>();
        for(Integer i=0;i<num;i++)
            {
                Case obj=new Case();
					   obj.RecordTypeId = recordTypeId;
                       obj.Status='New';
                       obj.Origin='Call';
                       obj.Priority='Medium';
                       caseList.add(obj);
           }//end for
        insert caseList;
        System.debug('caseList' + caseList);

        return caseList;
    }
    
    public List<Case> caseInsertAssignment(Integer num){
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName()
                                .get('Assignment').getRecordTypeId();
        List<Case> caseList = new List<Case>();
        for(Integer i=0;i<num;i++)
            {
                Case obj=new Case();
					   obj.RecordTypeId = recordTypeId;
                       obj.Status='New';
                       obj.Origin='Call';
                       obj.Priority='Medium';
                       caseList.add(obj);
           }//end for
        insert caseList;
        System.debug('caseList' + caseList);

        return caseList;
    }

    public List<Case> caseInsertPOP(Integer num){
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName()
                                .get('POP').getRecordTypeId();
        List<Case> caseList = new List<Case>();
        for(Integer i=0;i<num;i++)
            {
                Case obj=new Case();
             obj.RecordTypeId = recordTypeId;
                       obj.Status='New';
                       obj.Origin='Call';
                       obj.Priority='Medium';
                       caseList.add(obj);
           }//end for
        insert caseList;
        System.debug('caseList' + caseList);

        return caseList;
    }
    
    public List<CaseComment> caseCommntInsert(Integer num){
        List<CaseComment> caseCommntList = new List<CaseComment>();
        for(Integer i=0;i<num;i++) {
            CaseComment obj=new CaseComment();

           obj.CommentBody='Hello ABC';
           //obj.Origin='Call';
           //obj.Priority='Medium';
           caseCommntList.add(obj);
       }//end for
       insert caseCommntList;
       System.debug('caseCommntList' + caseCommntList);

       return caseCommntList;
    }

    public static EmailTemplate createEmailTemplate(String name, String body) {
        EmailTemplate template = new EmailTemplate(
            isActive = true,
            Name = name,
            DeveloperName = name.replaceAll( '\\s+', '') + System.now().getTime() + ((Integer)Math.random()*1000),
            TemplateType = 'Text',
            FolderId = UserInfo.getOrganizationId(),
            Body = body,
            HtmlValue = body
        );
        return template;
    }

    public static Customer_Community_Language_Settings__c createCommunityLanguageSetting(
        String language, String emailTemplate
    ) {
        return new Customer_Community_Language_Settings__c(
            Name = language,
            language__c = language,
            Community_Invitation_Email__c = emailTemplate
        );
    }

    public  List<User> userInsrt(Integer num){
         List<User> LstUser = new List<User>();
         Id p = [select id from profile where name='Partner Community User'].id;

        Account ac = new Account(name ='Grazitti') ;
        insert ac;

        Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);
        insert con;
         for(Integer i=0;i<num;i++)
        {
            User u = new User();
            u.alias = 'test123';
            u.email='test123@noemail.com';
                    u.emailencodingkey='UTF-8';
                    u.lastname='Testing';
                    u.languagelocalekey='en_US';
                    u.localesidkey='en_US';
                    u.profileid = p;
                    u.country='United States';
                    u.IsActive =true;
                    u.ContactId = con.Id;
                    u.timezonesidkey='America/Los_Angeles';
                    u.username='tester@noemail.com';
                    LstUser.add(u);
       }
        insert LstUser;
        return LstUser;
    }
}