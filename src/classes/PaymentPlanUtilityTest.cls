/* * * * * * * * * * * * * *
*  Class Name:   AOPTMQClassTest
*  Purpose:      Unit test class for PaymentPlanUtility
*  Author:       Hardik Mehta - ESPL
*  Company:      ESPL
*  Created Date: 21-Nov-2017
*  Type:         Test Class
* * * * * * * * * * * * */
@isTest
public class PaymentPlanUtilityTest{
 public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';
    public static testmethod void testPaymentPlanTypeForStructKw(){
        List<Payment_Terms__c> lstPaymentTerms =PaymentPlanUtilityTest.createPaymentTerms();
        PaymentPlanUtility objPaymentPlanUtility = new PaymentPlanUtility();
        PaymentPlanUtility.getPaymentPlanType(lstPaymentTerms);
    }
  /* * * * * * * * * * * * *
  *  Method Name:  testWrapper
  *  Purpose:      This method is used to test the Wrapper
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */
    public static testmethod void testWrapper(){
        List<AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper> lstNewPaymentTermsWrapper = new List<AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper>();
          lstNewPaymentTermsWrapper = PaymentPlanUtilityTest.createPaymenttermsDataFactory();
          PaymentPlanUtility.getPaymentPlanTypeFromWrapper(lstNewPaymentTermsWrapper);
    }
  /* * * * * * * * * * * * *
  *  Method Name:  createPaymentTerms
  *  Purpose:      This method is used to test the create Payment Terms
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */
    public static List<Payment_Terms__c> createPaymentTerms(){
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
        
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;
            
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
        }
        insert bookingUnitList;
    
        Payment_Plan__c objPaymentPlan = new Payment_Plan__c();
        objPaymentPlan.Booking_Unit__c = bookingUnitList[0].Id;
        objPaymentPlan.Status__c = 'Active';
        insert objPaymentPlan;
        
        List<Payment_Terms__c> lstPaymentTerms = new List<Payment_Terms__c>();
        
        Payment_Terms__c objPaymentTerm1 = new Payment_Terms__c();
        objPaymentTerm1.Payment_Plan__c = objPaymentPlan.Id;
        objPaymentTerm1.Installment__c = 'DP';
        objPaymentTerm1.Description__c = 'DEPOSIT';
        objPaymentTerm1.Percent_Value__c = '24';
        objPaymentTerm1.Milestone_Event__c = 'Immediate';
        lstPaymentTerms.add(objPaymentTerm1);
        
        Payment_Terms__c objPaymentTerm2 = new Payment_Terms__c();
        objPaymentTerm2.Payment_Plan__c = objPaymentPlan.Id;
        objPaymentTerm2.Installment__c = 'I001';
        objPaymentTerm2.Description__c = '1ST INSTALLMENT';
        objPaymentTerm2.Percent_Value__c = '0';
        objPaymentTerm2.Milestone_Event__c = 'Structure';
        lstPaymentTerms.add(objPaymentTerm2);
        
        Payment_Terms__c objPaymentTerm3 = new Payment_Terms__c();
        objPaymentTerm3.Payment_Plan__c = objPaymentPlan.Id;
        objPaymentTerm3.Installment__c = 'I002';
        objPaymentTerm3.Description__c = '2ND INSTALLMENT';
        objPaymentTerm3.Percent_Value__c = '0';
        objPaymentTerm3.Milestone_Event__c = 'Project completion';
        lstPaymentTerms.add(objPaymentTerm3);
        
        Payment_Terms__c objPaymentTerm4 = new Payment_Terms__c();
        objPaymentTerm4 .Payment_Plan__c = objPaymentPlan.Id;
        objPaymentTerm4 .Installment__c = 'I003';
        objPaymentTerm4 .Description__c = '3ND INSTALLMENT';
        objPaymentTerm4 .Percent_Value__c = '0';
        objPaymentTerm4 .Milestone_Event__c = 'Building completion';
        lstPaymentTerms.add(objPaymentTerm4);
        
        Payment_Terms__c objPaymentTerm5 = new Payment_Terms__c();
        objPaymentTerm5 .Payment_Plan__c = objPaymentPlan.Id;
        objPaymentTerm5 .Installment__c = 'I004';
        objPaymentTerm5 .Description__c = '4ND INSTALLMENT';
        objPaymentTerm5 .Percent_Value__c = '0';
        objPaymentTerm5 .Milestone_Event__c = 'Villa completion';
        lstPaymentTerms.add(objPaymentTerm5);
        
        Payment_Terms__c objPaymentTerm6 = new Payment_Terms__c();
        objPaymentTerm6 .Payment_Plan__c = objPaymentPlan.Id;
        objPaymentTerm6 .Installment__c = 'I004';
        objPaymentTerm6 .Description__c = '4ND INSTALLMENT';
        objPaymentTerm6 .Percent_Value__c = '0';
        objPaymentTerm6 .Milestone_Event__c = 'Foundation';
        lstPaymentTerms.add(objPaymentTerm6);
        
        Payment_Terms__c objPaymentTerm7 = new Payment_Terms__c();
        objPaymentTerm7 .Payment_Plan__c = objPaymentPlan.Id;
        objPaymentTerm7 .Installment__c = 'I004';
        objPaymentTerm7 .Description__c = '4ND INSTALLMENT';
        objPaymentTerm7 .Percent_Value__c = '0';
        objPaymentTerm7 .Milestone_Event__c = 'Basement';
        lstPaymentTerms.add(objPaymentTerm7);
        
        insert lstPaymentTerms;
        return lstPaymentTerms;   
    } 
  /* * * * * * * * * * * * *
  *  Method Name:  createPaymentTerms
  *  Purpose:      This method is used as a PaymentTermDataFactory
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */    
    public static List<AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper> createPaymenttermsDataFactory(){
        List<AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper> lstpayment = new List<AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper>();
        AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper obj1 = new AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper(); 
        obj1.strMileStoneEvent = 'Floor';
        lstpayment.add(obj1);
        
       
        AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper obj2 = new AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper(); 
        obj2.strMileStoneEvent = 'Structure';
        lstpayment.add(obj2);
        
        
        AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper obj3 = new AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper(); 
        obj3.strMileStoneEvent = 'Foundation';
        lstpayment.add(obj3);
        
     
        AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper obj4 = new AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper(); 
        obj4.strMileStoneEvent = 'Project completion';
        lstpayment.add(obj4);
        
       
        AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper obj5 = new AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper(); 
        obj5.strMileStoneEvent = 'Building completion';
        lstpayment.add(obj5);
        
     
        AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper obj6 = new AOPTServiceRequestControllerLDS.NewPaymentTermsWrapper(); 
        obj6.strMileStoneEvent = 'Villa completion';
        lstpayment.add(obj6);
        return lstpayment;
    
    }     
}