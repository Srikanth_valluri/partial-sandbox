/****************************************************************************************************
* Name          : CreateActionPlanBatchTest                                                         *
* Description   : Test class for CreateActionPlanBatch                                              *
* Created Date  : 01/04/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    01/04/2018      Initial Draft.                                          *
****************************************************************************************************/
@isTest 
public class CreateActionPlanBatchTest {
    @isTest static void testMethod1 (){
        
		Profile profileObjNew = [SELECT Id FROM Profile WHERE Name = 'Director of Sales' LIMIT 1];
        Profile profileObj = [SELECT Id FROM Profile WHERE Name = 'Property Consultant' LIMIT 1];
        User userObjNew = new User(
            Alias = 'stand', 
            Email = 'userNew@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObjNew.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfdNew@testorg.com'
        );
        insert userObjNew;
        User userObj = new User(
            Alias = 'stand', 
            Email = 'user@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObj.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfd@testorg.com',
            managerId = userObjNew.id
        );
        insert userObj;
        system.debug('>>>userObj>>>>>'+userObj);

        DateTime todaysDateTime = datetime.now();
        String lastMonthName = todaysDateTime.addmonths(-1).format('MMMMM');
        String secondLastMonthName = todaysDateTime.addmonths(-2).format('MMMMM');
		
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);
        sr.ID_Type__c = null;
        sr.OwnerId = userObj.Id;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c=null;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        insert sr;
        
       
        Booking__c bkObj = new Booking__c();
        bkObj.Deal_SR__c = sr.id;
        bkObj.Booking_Channel__c = 'Web';
        insert bkObj;
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        insert loc;       
        
        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = bkObj.id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        bu.Registration_Status__c = 'test';
        bu.Requested_Price__c = 1500;
        bu.Registration_DateTime__c = System.today().addmonths(-3); 
        insert bu;
        
        Target__c targetObjMonth1 = new Target__c(
            Target__c = 456,
            User__c = userObj.Id,
            Month__c = lastMonthName,
            CreatedDate = System.today().addmonths(-1) ,
            Year__c = String.valueOf(System.today().year())
        );
        insert targetObjMonth1;

        Target__c targetObjMonth2 = new Target__c(
            Target__c = 123,
            User__c = userObj.Id,
            Month__c = secondLastMonthName,
            CreatedDate = System.today().addmonths(-3) ,
            Year__c = String.valueOf(System.today().year())
        );
        insert targetObjMonth2;

        Test.startTest();
        ScheduleUserActionPlan schedulerInstance = new ScheduleUserActionPlan();
        String cronStr = '0 0 23 * * ?'; 
        system.schedule('Test ScheduleUserActionPlan', cronStr, schedulerInstance); 
        CreateActionPlanBatch createPlan = new CreateActionPlanBatch();
        ID batchprocessid = Database.executeBatch(createPlan, 200);
        Test.stopTest();

    }
    @isTest static void testMethod2 (){

        Profile profileObjNew = [SELECT Id FROM Profile WHERE Name = 'Director of Sales' LIMIT 1];
        Profile profileObj = [SELECT Id FROM Profile WHERE Name = 'Property Consultant' LIMIT 1];
        User userObjNew = new User(
            Alias = 'stand', 
            Email = 'userNew@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObjNew.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfdNew@testorg.com'
        );
        insert userObjNew;
        User userObj = new User(
            Alias = 'stand', 
            Email = 'user@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObj.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfd@testorg.com',
            managerId = userObjNew.id
        );
        insert userObj;
        system.debug('>>>userObj>>>>>'+userObj);

        DateTime todaysDateTime = datetime.now();
        String lastMonthName = todaysDateTime.addmonths(-1).format('MMMMM');
        String secondLastMonthName = todaysDateTime.addmonths(-2).format('MMMMM');    
        Target__c targetObjMonth1 = new Target__c(
            Target__c = 456,
            User__c = userObj.Id,
            Month__c = lastMonthName,
            CreatedDate = System.today().addmonths(-1) ,
            Year__c = String.valueOf(System.today().year())
        );
        insert targetObjMonth1;

        Target__c targetObjMonth2 = new Target__c(
            Target__c = 123,
            User__c = userObj.Id,
            Month__c = secondLastMonthName,
            CreatedDate = System.today().addmonths(-3) ,
            Year__c = String.valueOf(System.today().year())
        );
        insert targetObjMonth2;

        Test.startTest();
        ScheduleUserActionPlan schedulerInstance = new ScheduleUserActionPlan();
        String cronStr = '0 0 23 * * ?'; 
        system.schedule('Test ScheduleUserActionPlan', cronStr, schedulerInstance); 
        CreateActionPlanBatch createPlan = new CreateActionPlanBatch();
        ID batchprocessid = Database.executeBatch(createPlan, 200);
        Test.stopTest();

    }
}