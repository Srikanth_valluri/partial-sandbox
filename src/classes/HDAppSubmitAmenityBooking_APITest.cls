@isTest
private class HDAppSubmitAmenityBooking_APITest {

    @TestSetup
    static void TestData() {
    
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;

        Property__c propObj = TestDataFactoryFM.createProperty();
        insert propObj;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        Resource__c objRes = new Resource__c();
        objRes.Chargeable_Fee_Slot__c = 10;
        //objRes.Non_Operational_Days__c = 'Sunday';
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.No_of_advance_booking_days__c = 2;
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.Deposit__c = 3;
        objRes.Chargeable_Fee_Slot__c = 5;
        objRes.No_of_deposit_due_days__c = 1;
        objRes.Contact_Person_Email__c = 'test@test.com';   //
        objRes.Contact_Person_Name__c = 'Test'; //
        objRes.Contact_Person_Phone__c = '1234567890';  //
        insert objRes;

        Resource_Sharing__c objRS= new Resource_Sharing__c();
        objRS.Building__c = locObj.Id;
        objRS.Resource__c = objRes.Id;
        insert objRS;

        Resource_Slot__c objResSlot1 = new Resource_Slot__c();
        objResSlot1.Resource__c = objRes.Id;
        objResSlot1.Start_Date__c = Date.Today();
        objResSlot1.End_Date__c = Date.Today().addDays(30);
        objResSlot1.Start_Time_p__c = '09:00';
        objResSlot1.End_Time_p__c = '09:30';
        insert objResSlot1;

        Resource_Slot__c objResSlot2 = new Resource_Slot__c();
        objResSlot2.Resource__c = objRes.Id;
        objResSlot2.Start_Date__c = Date.Today();
        objResSlot2.End_Date__c = Date.Today().addDays(30);
        objResSlot2.Start_Time_p__c = '10:00';
        objResSlot2.End_Time_p__c = '10:30';
        insert objResSlot2;

        Non_operational_days__c obj = new Non_operational_days__c();
        obj.Non_operational_date__c = Date.today().addDays(3);
        obj.Resource__c = objRes.Id;
        insert obj;
        
    }

    @isTest
    static void SubmitAmenityBookingCaseTest1() {
        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        Account acc = [SELECT id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Resource_Slot__c objSlot = [SELECT id FROM Resource_Slot__c WHERE Resource__c =: objRes.id LIMIT 1];

        String jsonRequest =  '   {  '  + 
        ' "fm_case_id" : "",  '  + 
        ' "account_id" : "'+acc.id+'",  '  + 
        ' "booking_unit_id" : "'+objBU.id+'",  '  + 
        ' "amenity_id" : "'+objRes.id+'",  '  + 
        ' "amenity_slot_id" : "'+objSlot.id+'",  '  + 
        ' "booking_date" : "'+dateTomorrow+'",  '  + 
        ' "no_of_guest" : 3,  '  + 
        ' "comments" : "Testing by Shubham"  '  + 
        '  }  ' ; 

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/submitAmenityBooking';  
        req.requestBody = Blob.valueOf(jsonRequest);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        HDAppSubmitAmenityBooking_API.SubmitAmenityBookingCase();
        Test.Stoptest();


    }

    //Negative scenarios

    @isTest
    static void SubmitAmenityBookingCaseTestBlankUnitAccount() {
        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        Account acc = [SELECT id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Resource_Slot__c objSlot = [SELECT id FROM Resource_Slot__c WHERE Resource__c =: objRes.id LIMIT 1];

        String jsonRequest =  '   {  '  + 
        ' "fm_case_id" : "",  '  + 
        ' "account_id" : "'+''+'",  '  + 
        ' "booking_unit_id" : "'+''+'",  '  + 
        ' "amenity_id" : "'+objRes.id+'",  '  + 
        ' "amenity_slot_id" : "'+objSlot.id+'",  '  + 
        ' "booking_date" : "'+dateTomorrow+'",  '  + 
        ' "no_of_guest" : 3,  '  + 
        ' "comments" : "Testing by Shubham"  '  + 
        '  }  ' ; 

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/submitAmenityBooking';  
        req.requestBody = Blob.valueOf(jsonRequest);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        HDAppSubmitAmenityBooking_API.SubmitAmenityBookingCase();
        Test.Stoptest();


    }

    @isTest
    static void SubmitAmenityBookingCaseTestInvaliUnitAccount() {
        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        Account acc = [SELECT id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Resource_Slot__c objSlot = [SELECT id FROM Resource_Slot__c WHERE Resource__c =: objRes.id LIMIT 1];

        String jsonRequest =  '   {  '  + 
        ' "fm_case_id" : "",  '  + 
        ' "account_id" : "'+'1234'+'",  '  + 
        ' "booking_unit_id" : "'+'1234'+'",  '  + 
        ' "amenity_id" : "'+objRes.id+'",  '  + 
        ' "amenity_slot_id" : "'+objSlot.id+'",  '  + 
        ' "booking_date" : "'+dateTomorrow+'",  '  + 
        ' "no_of_guest" : 3,  '  + 
        ' "comments" : "Testing by Shubham"  '  + 
        '  }  ' ; 

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/submitAmenityBooking';  
        req.requestBody = Blob.valueOf(jsonRequest);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        HDAppSubmitAmenityBooking_API.SubmitAmenityBookingCase();
        Test.Stoptest();


    }

    @isTest
    static void SubmitAmenityBookingCaseTestUnittoInvalidAcc() {
        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        //Account acc = [SELECT id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Resource_Slot__c objSlot = [SELECT id FROM Resource_Slot__c WHERE Resource__c =: objRes.id LIMIT 1];

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc = new Account( LastName = 'Test Account1',
                                       Party_ID__c = '1234',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert acc;


        String jsonRequest =  '   {  '  + 
        ' "fm_case_id" : "",  '  + 
        ' "account_id" : "'+acc.id+'",  '  + 
        ' "booking_unit_id" : "'+objBU.id+'",  '  + 
        ' "amenity_id" : "'+objRes.id+'",  '  + 
        ' "amenity_slot_id" : "'+objSlot.id+'",  '  + 
        ' "booking_date" : "'+dateTomorrow+'",  '  + 
        ' "no_of_guest" : 3,  '  + 
        ' "comments" : "Testing by Shubham"  '  + 
        '  }  ' ; 

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/submitAmenityBooking';  
        req.requestBody = Blob.valueOf(jsonRequest);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        HDAppSubmitAmenityBooking_API.SubmitAmenityBookingCase();
        Test.Stoptest();


    }

    @isTest
    static void SubmitAmenityBookingCaseTestBlankJSON() {

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/submitAmenityBooking';  
        req.requestBody = Blob.valueOf('');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        HDAppSubmitAmenityBooking_API.SubmitAmenityBookingCase();
        Test.Stoptest();


    }


}


/*
{
    "fm_case_id" : "",
    "account_id" : "0010Y00000MaEKX",
    "booking_unit_id" : "a0x0Y000002UIDF",
    "amenity_id" : "a5Y1w000000CiSFEA0",
    "amenity_slot_id" : "a5X1w000000CyIKEA0",
    "booking_date" : "2020-07-29",
    "no_of_guest" : 3,
    "comments" : "Testing by Shubham"
}
*/