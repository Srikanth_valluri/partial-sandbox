/*********************************************************************************
* Description - Test Class for class UpdateFMOutsandingCntrl batch class         *
*                                                                                *
**********************************************************************************/
@isTest
Public Class UpdateFMOutsandingCntrlTest {
    @isTest
    public static void test1() {
         //Create Customer record
        Id businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(Name = 'Test Account',
                                    recordtypeid = businessRecTypeId,
                                    email__c = 'tt@tfdst.com',
                                    ZBEmailStatus__c = 'Valid');
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].FM_Outstanding_Amount__c = '1000';
        listBUs[0].Property_Country__c = 'United Arab Emirates';
        listBUs[0].Email__c = '321test@test.com';
        listBUs[0].Handover_Flag__c = 'Y';
        listBUs[0].Registration_ID__c = '123123';
        listBUs[0].Registration_Status_Code__c= 'MM';
        //listBUs[0].Registration_Status__c= 'executed';
        listBUs[0].SC_From_RentalIncome__c = false;
        insert listBUs;
        system.debug('listBUs== ' + listBUs);
        
        insert new IpmsRestServices__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            BaseUrl__c = 'http://0.0.0.0:8080/webservices/rest',
            Username__c = 'username',
            Password__c = 'password',
            Timeout__c = 120000
        );
        Test.StartTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            PageReference pageRef = Page.UpdatePaymentPlanPage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Id', listBUs[0].Id);
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(listBUs[0]);
            UpdateFMOutsandingCntrl objController = new UpdateFMOutsandingCntrl(standardControllerInstance);
            objController.objBu =  listBUs[0];
            objController.getFMOutstanding();
        Test.StopTest();
    }

   @isTest
    public static void test2() {
         //Create Customer record
        Id businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(Name = 'Test Account',
                                    recordtypeid = businessRecTypeId,
                                    email__c = 'tt@tfdst.com',
                                    ZBEmailStatus__c = 'Valid');
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].FM_Outstanding_Amount__c = '1000';
        listBUs[0].Property_Country__c = 'United Arab Emirates';
        listBUs[0].Email__c = '321test@test.com';
        listBUs[0].Handover_Flag__c = 'Y';
        listBUs[0].Registration_ID__c = '123123';
        listBUs[0].Registration_Status_Code__c= 'MM';
        //listBUs[0].Registration_Status__c= 'executed';
        listBUs[0].SC_From_RentalIncome__c = false;
        insert listBUs;
        system.debug('listBUs== ' + listBUs);
        
        insert new IpmsRestServices__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            BaseUrl__c = 'http://0.0.0.0:8080/webservices/rest',
            Username__c = 'username',
            Password__c = 'password',
            Timeout__c = 120000
        );
        Test.StartTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1,1));
            PageReference pageRef = Page.UpdatePaymentPlanPage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Id', listBUs[0].Id);
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(listBUs[0]);
            UpdateFMOutsandingCntrl objController = new UpdateFMOutsandingCntrl(standardControllerInstance);
            objController.objBu =  listBUs[0];
            objController.getFMOutstanding();
        Test.StopTest();
    }

}