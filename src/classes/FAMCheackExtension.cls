/**

{!IF ( AND(Account.FAM_CAUTION__c = $Label.FAMFieldValue,
 NOT(OR(AND(CONTAINS($User.Id, Account.Primary_CREId__c ),
 NOT(ISNULL(Account.Primary_CREId__c))),AND(CONTAINS($User.Id, Account.Secondary_CREId__c ),
 NOT(ISNULL(Account.Primary_CREId__c))),AND(CONTAINS($User.Id, Account.Tertiary_CREId__c ),
 NOT(ISNULL(Account.Tertiary_CREId__c)))))), URLFOR("/apex/NotAuthorize"),URLFOR("/apex/ProcessSelectionAccountPage?id="&Account.Id ))}
*/


public without sharing class FAMCheackExtension {
    ID recordId;
    public FAMCheackExtension(ApexPages.StandardController controller) {
        recordId = controller.getId();
        System.debug('strAccountId'+recordId);
    }


    public Pagereference checkFam(){
        
        
        System.debug('strAccountId'+recordId);
        PageReference pg = new PageReference( Label.CSRBaseURL+'/apex/ProcessSelectionAccountPage?scontrolCaching=1&id='+recordId );
        
        if(recordId != null) {
          Account objAccount = [ Select Id, Email__pc,Primary_CRE__c,
                        Secondary_CRE__c,
                        Tertiary_CRE__c,FAM_CAUTION__c, Name,
                        bypass_FAM__c
                         FROM Account WHERE ID = : recordId ];
            
            String CurrentUserId = UserInfo.getUserId();            
            
            System.debug('objAccount'+objAccount);
            if(String.isNotBlank(objAccount.FAM_CAUTION__c) && Label.FAMFieldValue.containsIgnoreCase(objAccount.FAM_CAUTION__c) && objAccount.bypass_FAM__c == false){
                System.debug('Outer If');
                if(objAccount.Primary_CRE__c == NULL && objAccount.Secondary_CRE__c == NULL && objAccount.Tertiary_CRE__c == NULL){
                    System.debug('Iner If');
                    pg = new PageReference(Label.CSRBaseURL+'/apex/NotAuthorize');
                }else if( !((objAccount.Primary_CRE__c != NULL && CurrentUserId.contains(objAccount.Primary_CRE__c)) ||
                    (objAccount.Secondary_CRE__c != NULL && CurrentUserId.contains(objAccount.Secondary_CRE__c)) ||
                    (objAccount.Tertiary_CRE__c != NULL && CurrentUserId.contains(objAccount.Tertiary_CRE__c))) ){
                    System.debug('Iner else If');
                    pg = new PageReference( Label.CSRBaseURL+'/apex/NotAuthorize');
                    
                }
            }
        }
        pg.setRedirect(true);
        return pg;
    }
    public Pagereference checkFamApn(){
        
        
        System.debug('strAccountId'+recordId);
        PageReference pg = new PageReference( Label.CSRBaseURL+'/apex/RaiseAnAppointmentAccount?scontrolCaching=1&id='+recordId );
        
        if(recordId != null) {
          Account objAccount = [ Select Id, Email__pc,Primary_CRE__c,
                        Secondary_CRE__c,
                        Tertiary_CRE__c,FAM_CAUTION__c, Name,
                         bypass_FAM__c
                         FROM Account WHERE ID = : recordId ];
            
            String CurrentUserId = UserInfo.getUserId();            
            
            System.debug('objAccount'+objAccount);
            if(String.isNotBlank(objAccount.FAM_CAUTION__c) &&
                Label.FAMFieldValue.containsIgnoreCase(objAccount.FAM_CAUTION__c) &&
                objAccount.bypass_FAM__c == false){
                System.debug('Outer If');
                if(objAccount.Primary_CRE__c == NULL && objAccount.Secondary_CRE__c == NULL && objAccount.Tertiary_CRE__c == NULL){
                    System.debug('Iner If');
                    pg = new PageReference(Label.CSRBaseURL+'/apex/NotAuthorize');
                }else if( !((objAccount.Primary_CRE__c != NULL && CurrentUserId.contains(objAccount.Primary_CRE__c)) ||
                    (objAccount.Secondary_CRE__c != NULL && CurrentUserId.contains(objAccount.Secondary_CRE__c)) ||
                    (objAccount.Tertiary_CRE__c != NULL && CurrentUserId.contains(objAccount.Tertiary_CRE__c))) ){
                    System.debug('Iner else If');
                    pg = new PageReference( Label.CSRBaseURL+'/apex/NotAuthorize');
                    
                }
            }
        }
        pg.setRedirect(true);
        return pg;
    }

    public Pagereference checkFamApnBU(){
        
        
        System.debug('strAccountId'+recordId);
        PageReference pg = new PageReference( Label.CSRBaseURL+'/apex/RaiseAnAppointmentBookingUnit?scontrolCaching=1&id='+recordId );
        
        if(recordId != null) {

            Booking_Unit__c objBU = [ Select Id,Block_Appointment__c,Staycae_RP__c, Booking__r.Account__r.Primary_CRE__c,
            Booking__r.Account__r.Secondary_CRE__c,
            Booking__r.Account__r.Tertiary_CRE__c,Booking__r.Account__r.FAM_CAUTION__c, Booking__r.Account__r.Name,Booking__r.Account__r.bypass_FAM__c
                         FROM Booking_Unit__c WHERE ID = : recordId ];
            
            String CurrentUserId = UserInfo.getUserId();    
                    
            
            System.debug('objBU'+objBU);
            
            if(objBU.Staycae_RP__c){
                pg = new PageReference( Label.CSRBaseURL+'/apex/ErrorStaycea');
            }
            else if(objBU.Block_Appointment__c){
                pg = new PageReference(Label.CSRBaseURL+'/apex/NotAuthorize');
            }
            else if(String.isNotBlank(objBU.Booking__r.Account__r.FAM_CAUTION__c) && Label.FAMFieldValue.containsIgnoreCase(objBU.Booking__r.Account__r.FAM_CAUTION__c) && objBu.Booking__r.Account__r.bypass_FAM__c == false){
                System.debug('Outer If');
                if(objBU.Booking__r.Account__r.Primary_CRE__c == NULL && objBU.Booking__r.Account__r.Secondary_CRE__c == NULL && objBU.Booking__r.Account__r.Tertiary_CRE__c == NULL){
                    System.debug('Iner If');
                    pg = new PageReference(Label.CSRBaseURL+'/apex/NotAuthorize');
                }else if( !((objBU.Booking__r.Account__r.Primary_CRE__c != NULL && CurrentUserId.contains(objBU.Booking__r.Account__r.Primary_CRE__c)) ||
                    (objBU.Booking__r.Account__r.Secondary_CRE__c != NULL && CurrentUserId.contains(objBU.Booking__r.Account__r.Secondary_CRE__c)) ||
                    (objBU.Booking__r.Account__r.Tertiary_CRE__c != NULL && CurrentUserId.contains(objBU.Booking__r.Account__r.Tertiary_CRE__c))) ){
                    System.debug('Iner else If');
                    pg = new PageReference( Label.CSRBaseURL+'/apex/NotAuthorize');
                    
                }
            } 
        }
        pg.setRedirect(true);
        return pg;
    }
    
}