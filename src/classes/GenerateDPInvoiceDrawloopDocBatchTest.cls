/**
 * @File Name          : GenerateDPInvoiceDrawloopDocBatchTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10/16/2019, 5:14:32 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    10/16/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
private class GenerateDPInvoiceDrawloopDocBatchTest{
    @isTest
    static void itShould(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Riyadh_Rotana_Drawloop_Doc_Mapping__c setting = new Riyadh_Rotana_Drawloop_Doc_Mapping__c();
        setting.Name = 'Cover Letter Date Based';
        setting.Delivery_Option_Id__c = 'a0T25000005EKAY';
        setting.Document_Name__c = 'a0V25000002xufZ';
        setting.Drawloop_Document_Package_Id__c = 'CoverLetter';
        insert setting;


        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;
        
        DP_Invoices__c obj=new DP_Invoices__c();
        obj.Accounts__c = objAccount.id;
        obj.BookingUnits__c = bookingUnitList[0].id;
       // obj.Cover_Letter__c = '';
        obj.SOA__c= 'test';
        obj.Trxn_Number__c= 'test';
        obj.TAX_Invoice__c= 'test';
        obj.COCD_Letter__c= 'test';
        obj.Type_of_Milestone__c= 'Date based';
        obj.Milestone__c= ' Date  test';
        obj.Other_Language_TAX_Invoice__c = 'test';
        insert obj;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());

        GenerateDPInvoiceDrawloopDocumentBatch objClass = new GenerateDPInvoiceDrawloopDocumentBatch();
        Database.Executebatch(objClass);
        Test.stopTest();
    }
}