/**************************************************************************************************
* Name               : Invocable_UpdateBuyer_Test
* Description        : Test Class for Invocable_UpdateBuyer Class
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0                         16/06/2019      Created
**************************************************************************************************/
@isTest
public class Invocable_UpdateBuyer_Test {
    @testSetup 
    static void setupData() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;

        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.Id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;

        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.Id;
        nstp1.Step_No__c = 2;
        nstp1.Step_Status__c = 'Awaiting Token Deposit';
        nstp1.Step_Type__c = 'Token Payment';
        insert nstp1;

        Booking__c booking = new Booking__c(Account__c = acc.Id, Deal_SR__c = sr.Id);
        insert booking;

        Inquiry__c inquiryRecord = new Inquiry__c();
        inquiryRecord.By_Pass_Validation__c = true;
        inquiryRecord.Party_ID__c = '12345';
        inquiryRecord.Title__c = 'MR.';
        inquiryRecord.Title_Arabic__c = 'MR.';
        inquiryRecord.First_Name__c = 'Test';
        inquiryRecord.First_Name_Arabic__c = 'Test';
        inquiryRecord.Last_Name__c = 'Test';
        inquiryRecord.Last_Name_Arabic__c = 'Test';
        insert inquiryRecord;

        Buyer__c buyer = new Buyer__c();
        buyer.Buyer_Type__c = 'Individual';
        buyer.Address_Line_1__c = 'Ad1';
        buyer.Country__c = 'United Arab Emirates';
        buyer.City__c = 'Dubai';
        buyer.Inquiry__c = inquiryRecord.Id;
        buyer.Date_of_Birth__c = string.valueof(system.today().addyears(-30));
        buyer.Email__c = 'test@test.com';
        buyer.First_Name__c = 'firstname';
        buyer.Primary_Buyer__c = true;
        buyer.Last_Name__c = 'lastname';
        buyer.Nationality__c = 'Indian';
        buyer.Passport_Expiry_Date__c = string.valueof(system.today().addyears(20));
        buyer.Passport_Number__c = 'J0565556';
        buyer.Phone__c = '569098767';
        buyer.Phone_Country_Code__c = 'United Arab Emirates: 00971';
        buyer.Place_of_Issue__c = 'India';
        buyer.Title__c = 'Mr';
        buyer.booking__c = booking.Id;
        buyer.Address_Changed__c = True;
        insert buyer;
    }

    @isTest
    static void test_method1() {
        New_Step__c stp = [SELECT Id FROM New_Step__c LIMIT 1];
        Invocable_UpdateBuyer.UpdateBuyer(new List<Id>{stp.Id});
        Buyer__c buyer = [SELECT Id FROM Buyer__c LIMIT 1];
        buyer.Primary_Buyer__c = False;
        buyer.Status__c = 'Updated';
        update buyer;
        Invocable_UpdateBuyer.UpdateBuyer(new List<Id>{stp.Id});
    }
}