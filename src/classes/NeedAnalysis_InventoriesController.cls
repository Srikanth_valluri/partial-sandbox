public without sharing class NeedAnalysis_InventoriesController{
    public Map<String ,List<Inventory__c>> propertyWithInventories { get; set; }
    public Map<String ,Integer> propertyWithInventoriesCount { get; set; }
    public NeedAnalysis_InventoriesController(){
         Id inquiryId = apexpages.currentpage().getparameters().get('id');
         propertyWithInventories = new Map<String ,List<Inventory__c>>();
         propertyWithInventoriesCount = new Map<String ,Integer>();
         Inquiry__c inquiry = [SELECT Our_Quote__c,No_Of_Bed_Rooms_Availble_in_SF__c,Price_Expectation__c ,
                                  Budget__c,Project_Status__c,Type_of_Property_Interested_in__c , Purchased_with_DAMAC_before__c,
                                  Purpose_Of_Buying__c ,Would_you_like_assistance_on_mortgage__c 
                                  FROM Inquiry__c WHERE ID =: inquiryId];
         Decimal budget = 0;
         
         for(Inventory__c eachInventory : [Select Name,Property__c ,Property__r.Name,Unit_Location__r.Name,Property_Type__c ,
                                                 ACD_Date__c,Unit_Name__c ,Unit_Type__c ,IPMS_Bedrooms__c ,Area__c FROM Inventory__c 
                                                 WHERE Status__c =: 'Released' AND 
                                             Selling_Price__c <=: inquiry.Our_Quote__c AND Special_Price__c <=: inquiry.Price_Expectation__c AND 
                                             Property_Type__c =: inquiry.Type_of_Property_Interested_in__c AND 
                                             IPMS_Bedrooms__c  =:inquiry.No_Of_Bed_Rooms_Availble_in_SF__c
                                             ]){
                                             /*No_Of_Bed_Rooms_Availble_in_SF__c,
                                              Budget__c,Project_Status__c, Purchased_with_DAMAC_before__c,
                                              Purpose_Of_Buying__c ,Would_you_like_assistance_on_mortgage__c                                  
                                              AND
                                             Building_Cons_Status__c =: inquiry.Project_Status__c                                             
                                             */
             if(eachInventory.Property__c != null){                                       
                 if(propertyWithInventories.containsKey(eachInventory.Property__r.Name)){
                    propertyWithInventories.get(eachInventory.Property__r.Name).add(eachInventory); 
                 }else{
                     propertyWithInventories.put(eachInventory.Property__r.Name ,new List<Inventory__c>{eachInventory} );
                 }
             }
         }
         for(String property : propertyWithInventories.keySet()){
             propertyWithInventoriesCount.put(property ,propertyWithInventories.get(property).size());
         }
    }
}