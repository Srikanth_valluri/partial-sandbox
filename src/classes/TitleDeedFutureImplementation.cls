public class TitleDeedFutureImplementation implements Queueable {
    
    private Case caseTD;
    
    public TitleDeedFutureImplementation (Case objCase) {
        this.caseTD = objCase;
    }

    public void execute(QueueableContext context) {
        List<Task> lstTasks = new List<Task>();
        Set<Id> setTDId = new Set<Id>();
        Task objTask = TaskUtility.getTask(
                    (SObject)caseTD,
                    'Check NoDu n arrange TD Pymnt',
                    'Finance',
                    'Title Deed',
                    System.today().addDays(1)
                    );                    
        lstTasks.add(objTask);
        if (!lstTasks.isEmpty()) {
            insert lstTasks;
        }
        
        for (SR_Attachments__c objSRAtt : [Select Id, Case__c 
                                            From SR_Attachments__c 
                                            Where Case__c =:caseTD.Id ]) {
            setTDId.add(objSRAtt.Id);                          
        }
        
        UploadDocToCentralRepoBatch batchInstance = new UploadDocToCentralRepoBatch(setTDId);
        Database.executeBatch(batchInstance, 1);
    
    }
}