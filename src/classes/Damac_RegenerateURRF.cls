global class Damac_RegenerateURRF {

    webservice static String generateURRF(String formReqId, ID srId) {
        Form_request__c req = new Form_request__c ();
        
        NSIBPM__Service_Request__c serviceReq = new NSIBPM__Service_Request__c ();
        serviceReq = [SELECT Form_Request__c, Is_uk_Deal__c, Reservation_Form_Attachment_Name__c FROM NSIBPM__Service_Request__c WHERE Id =: srID ];
        
        Boolean pendingDocs = false;
        if (serviceReq.Is_uk_deal__c) {
            for (Unit_Documents__c doc : [SELECT ID FROM Unit_documents__c WHERE Status__c = 'Pending Upload' 
                                    AND is_required__c = TRUE AND Document_name__c != 'Reservation Copy'
                                    AND Service_request__c =: srId]) {
                pendingDocs = true;
            }
        }
        if (pendingDocs == false) { 
            try {
                if (formReqId != '' && formReqId != null) {
                    req = Database.query ('SELECT RecordType.Name, '+getAllFields ('Form_Request__c')+' FROM Form_Request__c WHERE Id =: formReqId');
                } else {
                    
                    String name = serviceReq.Reservation_Form_Attachment_Name__c;
                    
                    req = Database.query ('SELECT '+getAllFields ('Form_Request__c')+' FROM Form_Request__c WHERE Name =: name');
                }
            } catch (Exception e) {}
            
            Form_request__c reqCopy = new Form_request__c ();
            if (req.Id != null) {
                reqCopy = req;
                reqCopy.Id = null;
                reqCopy.Sent_for_second_signer__c = false;
                reqCopy.Status__c = '';
                reqCopy.Service_request__c = srId;
                insert reqCopy;
                 
                NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c ();
                sr.Id = srId;        
                sr.Final_Version_Form_Request__c = reqCopy.Id;
                update sr;        
                
                List <Form_Request_Inventories__c> formReqInventories = new List <Form_Request_Inventories__c> ();
                for (Form_Request_Inventories__c reqInv : Database.query ('SELECT '+getAllFields ('Form_Request_Inventories__c')
                                                                +' FROM Form_Request_Inventories__c WHERE Form_Request__c =: formReqId')) {
                
                    Form_Request_Inventories__c reqInvCopy = new Form_Request_Inventories__c ();
                    reqInvCopy = reqInv;
                    reqInvCopy.ID = null;
                    reqInvCopy.Form_Request__c = reqCopy.Id;
                    formReqInventories.add (reqInvCopy);
                }
                insert formReqInventories;
            }
            if (req.Id == null) {
                String invIds = '';
                String paymentPlans = '';
                for (Booking_Unit__c unit :[SELECT Inventory__c, Payment_Plan_Id__c FROM Booking_Unit__c WHERE Booking__r.Deal_Sr__c =: srId]) {
                    invIds += unit.Inventory__c+',';
                    paymentPlans += unit.Inventory__c+'-'+unit.Payment_Plan_Id__c +',';
                }
                reqCopy.Id = DAMAC_Project_Avaliability_Clone.createFormRequestWithPlans (invIds, paymentPlans);
            }
            
            if (serviceReq.Form_request__c == null) {
                serviceReq.Form_Request__c = req.Id;
                update serviceReq;
            }
            if (req.recordType.Name == 'UK Form') {
                return '/apex/damac_reservationForm_UK?id='+reqCopy.Id;
            } else {
                return '/apex/damac_reservationForm?id='+reqCopy.Id;
            }
        } else {
            return '/apex/UploadSRDocs?id='+srId;
        }
        
    }
    
    /* To Get all the Fields of an Object - used in Query String */
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);

        if (objectType == null)
            return fields;

        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();

        for (Schema.SObjectField sfield : fieldMap.Values()) {

            fields += sfield.getDescribe().getName ()+ ', ';

        }
        return fields.removeEnd(', ');
    }
}