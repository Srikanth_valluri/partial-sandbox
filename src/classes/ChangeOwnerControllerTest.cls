@isTest
private class ChangeOwnerControllerTest {
   @testSetup 
   static void testData(){
       List<Calling_List__c>callingLst = new List<Calling_List__c>();
         Account accObj1 = new Account(Name = 'Miss. Madina Alieva');
        insert accObj1;
        Account accObj2 = new Account(Name = 'Miss. Madina Alieva Test');
        insert accObj2;
      Calling_List__c callObj1 = new Calling_List__c(Account__c = accObj1.Id,Registration_ID__c = '12851');
         callingLst.add(callObj1);
        Calling_List__c callObj2 = new Calling_List__c(Account__c = accObj2.Id,Registration_ID__c = '88547');
         callingLst.add(callObj2);
         insert callingLst;
    }
    static testMethod void changeOwnerTest(){
        List<Calling_List__c> callObjInstLst = [SELECT Id,
                                              Account__c,
                                              Registration_ID__c
                                         FROM Calling_List__c
                                         LIMIT 2];
        System.debug('inside test class:callObjInst::'+callObjInstLst);
       // System.assertNotEquals(null,callObjInstLst.Id);
        Test.startTest();
            PageReference changeOwnerPage = Page.Change_Owner_Page;
            Test.setCurrentPage(changeOwnerPage);
            //changeOwnerPage.getParameters().put('Id',String.valueOf(callObjInst.Id));
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(callObjInstLst);
            ChangeOwnerController controllerObj = new ChangeOwnerController(sc);
            controllerObj.saveOwner();
        Test.stopTest();
        //controllerObj.fetchCases();
    }
}