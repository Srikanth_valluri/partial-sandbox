/*
Developed By: DAMAC IT Team
Usage:DAMAC_REST_RESP_UNIT
*/
public class DAMAC_REST_RESP_UNIT{

    public String ID;    //Hello World
    public String STATUS;    //verified
    public String MESSAGE;  //
    
    public static DAMAC_REST_RESP_UNIT parse(String json){
        return (DAMAC_REST_RESP_UNIT) System.JSON.deserialize(json, DAMAC_REST_RESP_UNIT.class);
    }
}