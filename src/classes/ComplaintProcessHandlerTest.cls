/***********************************************************************
* Description - Test class developed for ComplaintProcessUtility
*
* Version            Date            Author            Description
* 1.0                12/12/17        Naresh (accely)           Initial Draft
**********************************************************************/
@isTest(SeeAllData=false)
private class ComplaintProcessHandlerTest
{
    
    // Test Method: sendNotificationToMA  
    public static testmethod void Test_sendNotificationToMA(){
        test.StartTest();

        /*UserRole roleManagerObj = TestDataFactory_CRM.createRole('Manager', 'Manager');
        insert roleManagerObj;

        // Profile managerProfileObj = [SELECT Id FROM Profile WHERE Name='Collection - Manager'];
        Profile managerProfileObj = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User managerUser = TestDataFactory_CRM.createUser('Manager', managerProfileObj.Id, roleManagerObj.Id);
        managerUser.Email = 'Test@email.com';
        insert managerUser;


        Profile creProfile = [SELECT Id FROM Profile WHERE Name='Non Elite - CRE'];
        User creUser = TestDataFactory_CRM.createUser('Manager', creProfile.Id, null);
        creUser.ManagerId = managerUser.Id;
        creUser.Email = 'Test@email.com';
        insert creUser;

        System.runAs( creUser ) {*/

            Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Complaint').getRecordTypeId();
            List<Case> caseList = new  List<Case>();
            Account acc = TestDataFactory_CRM.createPersonAccount();
            insert acc;
            System.assert(acc != null); 
          
            Case newCase = new Case(); 
            newCase.AccountId = acc.Id ;
            newCase.RecordTypeId  = devRecordTypeId;
            newCase.Status = 'Submitted';
            newCase.Type = 'Complaints SR';
            newCase.Origin = 'Walk-In';
            caseList.add( newCase );
            //insert newCase; 

           /* Test.startTest();
              Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList());
              //SendSmsService.Sendtextmessage( phoneNos ,'test message content', String.valueOf( listCallingList[0].Id ));
            Test.stopTest();*/

           /* newCase.Customer_Connected_Through_CTI__c = true;
            newCase.Status = 'Closed';
            update newCase;*/
            
            Case newPortalCase = new Case(); 
            newPortalCase.AccountId = acc.Id ;
            newPortalCase.RecordTypeId  = devRecordTypeId;
            newPortalCase.Status = 'Submitted';
            newPortalCase.Type = 'Complaints SR';
            newPortalCase.Origin = 'Portal';
            caseList.add( newPortalCase );
            //insert newPortalCase; 

            Case newClosedCase = new Case(); 
            newClosedCase.AccountId = acc.Id ;
            newClosedCase.RecordTypeId  = devRecordTypeId;
            newClosedCase.Status = 'Closed';
            newClosedCase.Type = 'Complaints SR';
            newClosedCase.Customer_Connected_Through_CTI__c = true;
            caseList.add( newClosedCase );
            insert caseList; 
            
            ComplaintProcessHandler.HandleProcess( caseList );
            Error_Log__c errorLogObj = ComplaintProcessHandler.logError( newClosedCase.Id, 'Test' );

        //}
        
        
        test.StopTest();
    }
}