public without sharing class GenerateNoticeForPlotHandover{
    public boolean blnContinue{get;set;}
    string strPageID;
    String regId;
    Case objCase;
    public String NOCUrl;
    UnitDetailsService.BookinUnitDetailsWrapper objBookinUnitDetailsWrapper ;
    //public string strPopupMessage {get;set;}
    public String selectedNotice {get;set;}
    
    public GenerateNoticeForPlotHandover(ApexPages.standardController controller){
       
    }//End constructor
    
    public pagereference init(){
        strPageID = ApexPages.currentPage().getParameters().get('id');
        blnContinue = false;
        //strPopupMessage = returnHTMLBody('KRF_Message');
        objCase = [Select Id
                     , Booking_Unit__c
                     , Booking_Unit__r.Handover_Flag__c
                     , Booking_Unit__r.Early_Handover__c
                     , Booking_Unit__r.Registration_ID__c           
                     , Registration_ID__c
                     , Admin_Fee_For_Title_Deed__c
                     , Plot_Handover_Status__c
                     , Status
                     , Possession_Date__c
                     , Construction_Commencement_Date__c
                     , OwnerId
                     , CurrencyIsoCode
                     , AccountId
                from Case 
                where Id =: strPageID];
        regId = objCase.Booking_Unit__r.Registration_ID__c;
        return null;
    }
    
     /*public string returnHTMLBody(String templateName){
        // 'KRF_Message'
        return [Select e.Name
                     , e.IsActive
                     , e.Id
                     , e.HtmlValue
                     , e.DeveloperName
                     , e.Body 
                From EmailTemplate e 
                where e.DeveloperName =:templateName limit 1][0].HtmlValue;
    }*/
    
    /*public pageReference continueNotice(){
         Case objCase = new Case(Id = strPageID);
         objCase.Possession_Date__c = system.today();
         update objcase;
         return returnToCase();
         blnContinue = false;
         return null;
    }*/
    
    public void generateNotice() {
        system.debug('!!!!!selectedNotice'+selectedNotice );  
        system.debug('!!!!!strPageID'+strPageID );  
        Boolean callBatch = false;
        String strError;
        regId = objCase.Booking_Unit__r.Registration_ID__c;
        system.debug('*****regId'+regId);
        Case objUpdateCase = new Case(Id = strPageID); 
        if (objCase.Status == 'Submitted') {
            if ( selectedNotice == 'Commencement of Construction') {
                if (objCase.Plot_Handover_Status__c == 'Handover Documents Signed') {
                    Decimal paidPercent;                    
                    if (!string.isblank(regId)) {
                        objBookinUnitDetailsWrapper =
                            UnitDetailsService.getBookingUnitDetails(regId);   
                        paidPercent = decimal.ValueOf(objBookinUnitDetailsWrapper.strPaidPercent);  
                        if (objBookinUnitDetailsWrapper != null && !String.isBlank(objBookinUnitDetailsWrapper.strPaidPercent)
                            && decimal.ValueOf(objBookinUnitDetailsWrapper.strPaidPercent) > 
                                decimal.valueOf(Label.Plot_NOC_Max_Paid_Percent)) { 
                            if (objCase.Booking_Unit__c != null 
                                && (objCase.Booking_Unit__r.Handover_Flag__c == 'Y'
                                    || objCase.Booking_Unit__r.Early_Handover__c == true) ) {
                                callBatch = true;
                                objUpdateCase.Admin_Fee_For_Title_Deed__c = 'NOC Commence Construction';
                                objUpdateCase.Construction_Commencement_Date__c = system.today();   
                                /*if (paidPercent == 104)   {
                                    createTitleDeed();
                                }*/     
                            } else {
                                strError = 'Unit is not Handed Over, Request you to Handover the Unit.';    
                            } 
                        } else {
                            strError = 'Atleast 50% of payment should be paid, currently only '+paidPercent+'% received';
                        }
                    } else {
                        strError = 'Registration Id not available on Unit';
                    }  
                }   else {
                    strError = 'Notice can not be generated at this Stage.';
                }       
            } else if (selectedNotice == 'Obtain Telecommunication Services') {
                if (objCase.Plot_Handover_Status__c == 'Handover Documents Signed') {
                    objUpdateCase.Admin_Fee_For_Title_Deed__c = 'NOC TelComm Services';
                    callBatch = true;
                } else {
                    strError = 'Notice can not be generated at this Stage.';
                }
            } else if (selectedNotice == 'Building Design Plan') {
                if (objCase.Plot_Handover_Status__c == 'Building Designs Approved by Design Team') {
                    objUpdateCase.Admin_Fee_For_Title_Deed__c = 'NOC for Building Plan';
                    callBatch = true;
                } else {
                    strError = 'Notice can not be generated at this Stage.';
                }
            } else if (selectedNotice == 'Approval of Contractor/Consultants') {
                objUpdateCase.Admin_Fee_For_Title_Deed__c = 'NOC Approval Constructor';
                callBatch = true;
            } else if (selectedNotice == 'Obtain Water and Electricity Services') {
                if (objCase.Plot_Handover_Status__c == 'Handover Documents Signed') {
                    objUpdateCase.Admin_Fee_For_Title_Deed__c = 'NOC Water Elctrcity Serv';
                    callBatch = true;
                } else {
                    strError = 'Notice can not be generated at this Stage.';
                }
            } else if (selectedNotice == 'Obtain Demarcation Certificate') {
                if (objCase.Plot_Handover_Status__c == 'Building Design Template Received') {
                    objUpdateCase.Admin_Fee_For_Title_Deed__c = 'NOC Demarc Certificate';
                    objUpdateCase.Possession_Date__c = system.today();
                    callBatch = true;
                } else {                    
                    strError = 'Notice can not be generated at this Stage.';
                }
            } else if (selectedNotice == 'Obtain Building Permit') {
                if (objCase.Plot_Handover_Status__c == 'Building Designs Approved by Design Team') {
                    String strDueResponse;
                    Decimal fmSOABalance;
                    system.debug('!!!!!regId'+regId);
                    if (!string.isblank(regId)) {
                        strDueResponse = assignmentEndpoints.fetchAssignmentDues(regId);
                    }
                    if(String.isNotBlank(strDueResponse)){
                        map<String,Object> mapDeserializeDue 
                            = (map<String,Object>)JSON.deserializeUntyped(strDueResponse);
                        if(mapDeserializeDue.get('status') == 'S'){
                            strDueResponse = strDueResponse.remove('{');
                            strDueResponse = strDueResponse.remove('}');
                            strDueResponse = strDueResponse.remove('"');
                            for(String st : strDueResponse.split(',')){
                                String strKey = st.substringBefore(':').trim();
                                if(!strKey.equalsIgnoreCase('Status')) {
                                    if(Decimal.valueOf(st.subStringAfter(':').trim()) != null) {
                                        if ( !strKey.contains('FM')) {
                                            if( strKey.equalsIgnoreCase('FM Balance as per SOA')) {
                                                fmSOABalance 
                                                    = Decimal.valueOf(st.subStringAfter(':').trim());
                                            }// end of if FM Balance
                                        }
                                    }// end of if for trim
                                }
                            }// end of for
                        }// end of if Status
                    }// end of if strDueResponse
                    if (fmSOABalance == null || fmSOABalance <= 0) {
                        objUpdateCase.Admin_Fee_For_Title_Deed__c = 'NOC Building Permit';
                        callBatch = true;
                    } else {
                        strError = 'Service charge on Unit, can not generate Notice.';
                    }
                } else {
                    strError = 'Notice can not be generated at this Stage.';
                }
            }
            update objUpdateCase;
        } else {
            strError = 'Notice can not be generated as Case is not Submitted.';
        }
        
        if (callBatch == true) {
            executeBatch();
            //return null;
        } else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,strError);
            ApexPages.addMessage(myMsg);
        }        
    }
    
     public pageReference returnToCase(){
        pagereference newpg = new Pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+strPageID);
        newpg.setRedirect(true);
        system.debug('newpg********'+newpg);
        return newpg;
    }
    
    public list<SelectOption> getNotices () {
    
        list<SelectOption> lstNotices = new list<SelectOption>();
        List<Plot_Handover_Notice__c> notices = Plot_Handover_Notice__c.getall().values();
        lstNotices.add( new SelectOption( 'None', 'None') );
        for (Plot_Handover_Notice__c objNotice : notices) {
            lstNotices.add( new SelectOption( objNotice.Name, objNotice.Type__c) );
        }
        return lstNotices;
    }
    
    public void executeBatch(){
        GenerateDrawloopDocumentBatch objInstance = new GenerateDrawloopDocumentBatch(strPageID 
                                                                                         , System.Label.Plot_Demarcation_DDP_Id
                                                                                         , System.Label.Plot_Demarcation_Template_Id);
         Id batchId = Database.ExecuteBatch(objInstance);
         if(String.valueOf(batchId) != '000000000000000'){ 
            if (selectedNotice == 'Building Design Plan') {
                  SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                  objCaseAttachment.Case__c = strPageID;
                  objCaseAttachment.Name = 'Signed NOC to ' + selectedNotice ;
                  objCaseAttachment.Booking_Unit__c = objCase.Booking_Unit__c;
                  insert objCaseAttachment;  
            }           
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Your request for Notice was successfully submitted. Please check the documents section for the document in a while.');
             ApexPages.addMessage(myMsg);
         } else {
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Your request for Notice could not be completed. Please try again later.');
             ApexPages.addMessage(myMsg);
         }
        
    }
    
    /*public void createTitleDeed() {
         Id recType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Title Deed').getRecordTypeId();
         map<Id,Boolean> mapBUidExists = new map<Id,Boolean>();
         map<Id,list<SR_Attachments__c>> mapId_Docs = new map<Id,list<SR_Attachments__c>>();
         list<Case> lstTitleDeed = new list<Case>();
         
         Id buId = objCase.Booking_Unit__c;
         for(Case objC : [Select Id
                                , ParentId
                                , Booking_Unit__c
                                , Booking_Unit__r.Finance_Flag__c
                        From Case
                        Where RecordTypeId =:recType
                        and  Booking_Unit__c =: buId ]){
            mapBUidExists.put(objC.Booking_Unit__c, true);          
        }// end of for      
        
        for(SR_Attachments__c objSR : [Select Id
                                              , Name
                                              , Case__c
                                              , IsValid__c
                                              , Attachment_URL__c
                                       from SR_Attachments__c
                                       where Case__c = : objCase.id]){
            if(!mapId_Docs.containsKey(objSR.Case__c)){
                mapId_Docs.put(objSR.Case__c, new list<SR_Attachments__c>{objSR});
            }else{
                mapId_Docs.get(objSR.Case__c).add(objSR);
            }
        } 
        
        if (mapBUidExists.isEmpty() || !mapBUidExists.containsKey(buId)) {
            Case objNewCase = new Case();
            objNewCase.RecordTypeId = recType;
            objNewCase.ParentId = objCase.Id;
            objNewCase.AccountId = objCase.AccountId;
            objNewCase.Booking_Unit__c = objCase.Booking_Unit__c;
            objNewCase.CurrencyIsoCode = objCase.CurrencyIsoCode;
            objNewCase.Status = 'Submitted';
            objNewCase.OwnerId = objCase.OwnerId;
            lstTitleDeed.add(objNewCase);           
        }
        
        if(!lstTitleDeed.isEmpty()){
            insert lstTitleDeed;
            list<SR_Attachments__c> titleDeedDocs = new list<SR_Attachments__c>();
            
            for(Case objC : lstTitleDeed){
                if(mapId_Docs.containsKey(objC.ParentId)){
                    for(SR_Attachments__c objS : mapId_Docs.get(objC.ParentId)){
                        SR_Attachments__c obj = new SR_Attachments__c();
                        obj.Name = objS.Name;
                        obj.Case__c = objC.Id;
                        obj.IsValid__c= objS.IsValid__c;
                        obj.Attachment_URL__c= objS.Attachment_URL__c;
                        titleDeedDocs.add(obj);
                    }
                }
            }
            
            if(!titleDeedDocs.isEmpty()){
                insert titleDeedDocs;
                
                Set<Id> setTDId = new Set<Id>();
                for(SR_Attachments__c objSRAtt: titleDeedDocs) {
                    setTDId.add(objSRAtt.Id);
                }
                
                UploadDocToCentralRepoBatch batchInstance = new UploadDocToCentralRepoBatch(setTDId);
                Database.executeBatch(batchInstance, 1);
            }
        }
    }*/
}