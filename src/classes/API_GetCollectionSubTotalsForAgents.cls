/**********************************************************************************************************************
Class Name : API_GetCollectionSubTotalsForAgents 
Description: To get the sub-totals of collection amounts by project/unit for an agent.
Test Class : API_GetCollectionSubTotalsForAgentsTest
========================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.1     |   14-12-2020      | Subin C Antony	  | Initial Draft
***********************************************************************************************************************/

@RestResource(urlMapping='/API_GetCollectionSubTotalsForAgents')
global without sharing class API_GetCollectionSubTotalsForAgents {

    @HttpGET
    global static void doGET() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg;
        
        String groupBy = req.params.containskey('group_by') ? req.params.get('group_by') : '';
        if(String.isBlank(groupBy)){
            errorMsg = 'Please choose unit or project as the param to group the results.';
            getErrorResponse(3, errorMsg, errorMsg);
            return;
        }
        
        String period = req.params.containskey('period') ? req.params.get('period') : '';
        if(String.isBlank(period)){
            errorMsg = 'Please choose the time period for which the results should be fetched.';
            getErrorResponse(3, errorMsg, errorMsg);
            return;
        }
        
        Set <ID> agentIds = new Set <ID> ();
        Set <ID> contactIds = new Set <ID> ();
        User u = [SELECT ContactId, profile.Name, AccountId FROM User WHERE ID =: UserInfo.getUserId()];
        agentIds.add (UserInfo.getUserId());
        contactIds.add (u.contactId);
        
        // To fetch the account related contacts and thereby, those contact realted user's when the API request is coming from a Super User Profile
        if (u.profile.Name == 'Customer Community - Super User' || Test.isRunningTest()) {
            Set <ID> conIds = new Set <ID> ();
            for (Contact con : [SELECT ID FROM Contact WHERE accountId = :u.accountId]) {
                conIds.add (con.ID);
            }
            
            for (User usr : [SELECT ContactId, AccountID FROM User WHERE ContactId IN :conIds AND IsActive = TRUE]) {
                contactIds.add (usr.contactId);
                agentIds.add (usr.id);
            }
        }
        
        // to get the total collections, booking amount from SR
        Set <ID> srIds = new Set <ID> ();
        if(u.profile.Name == 'Customer Community - Super User' || Test.isRunningTest()) {
            for (NSIBPM__Service_Request__c sr : [SELECT NSIBPM__Internal_Status_Name__c, Total_Booking_Amount__c, Token_Amount_AED__c, List_of_Units__c 
                                                    FROM NSIBPM__Service_Request__c 
                                                    WHERE 
                                                    NSIBPM__External_Status_Code__c != 'Draft'
                                                    AND 
                                                    Agency__r.Id=:u.accountId]) /* OR NSIBPM__Contact__c IN: contactIds */ {
                srIds.add (sr.Id); /* sr.Total_Booking_Amount__c */
            }
        } /* (u.profile.Name == 'Customer Community - Super User' || Test.isRunningTest()) */
        
        if(u.profile.Name != 'Customer Community - Super User' || Test.isRunningTest()) {
            for (NSIBPM__Service_Request__c sr : [SELECT NSIBPM__Internal_Status_Name__c, Total_Booking_Amount__c, Token_Amount_AED__c, List_of_Units__c 
                                                  FROM NSIBPM__Service_Request__c 
                                                  WHERE 
                                                  NSIBPM__External_Status_Code__c != 'Draft'
                                                  AND 
                                                  (Agent_Name__c IN: agentIds)]) /* OR NSIBPM__Contact__c IN: contactIds */ {
                srIds.add (sr.Id); /* sr.Total_Booking_Amount__c */
            }
        } /* (u.profile.Name != 'Customer Community - Super User' || Test.isRunningTest()) */
        
        System.debug('SR_IDs .............................: ' + srIds);
        
        String query = 'SELECT id, name, Unit_Location__c, Unit_Name__c, Booking__c, Total_Collections__c, Requested_Price__c, Token_Amount__c, ' + 
                      'Registration_Date__c, Registration_ID__c, Booking__r.Deal_Sr__r.IPMS_Registration_ID__c, ' + 
                      'Property_Name_Inventory__c, Inventory__r.Marketing_Plan__c, Booking__r.Deal_Sr__r.NSIBPM__External_Status_Code__c ' + 
                      'FROM Booking_Unit__c WHERE Booking__r.Deal_Sr__c != null ' + 
                      'AND Booking__r.Deal_Sr__c IN :srIds'; // (' + srIdList + ')
        if(period.equalsIgnoreCase('LAST_WEEK')) {
            query += ' AND Registration_Date__c >= LAST_WEEK';
        }
        else if(period.equalsIgnoreCase('LAST_MONTH')) {
            query += ' AND Registration_Date__c >= LAST_MONTH';
        }
        else if(period.equalsIgnoreCase('LAST_3_MONTHS')) {
            query += ' AND Registration_Date__c >= LAST_N_MONTHS:3';
        }
        else if(period.equalsIgnoreCase('LAST_1_YEAR')) {
           query += ' AND Registration_Date__c >= LAST_N_MONTHS:12'; // LAST_YEAR
        }
        
        List<Booking_Unit__c> buList;
        try {
            /* buList = [SELECT id, name, Unit_Location__c, Unit_Name__c, Booking__c, Total_Collections__c, Requested_Price__c, Token_Amount__c, 
                      Registration_Date__c, Registration_ID__c, Booking__r.Deal_Sr__r.IPMS_Registration_ID__c, 
                      Property_Name_Inventory__c, Inventory__r.Marketing_Plan__c, Booking__r.Deal_Sr__r.NSIBPM__External_Status_Code__c 
                      FROM Booking_Unit__c WHERE Booking__r.Deal_Sr__c != null 
                      AND Booking__r.Deal_Sr__c IN :srIds]; */
            buList = (List<Booking_Unit__c>) Database.query(query);
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            errorMsg = 'Error while trying to fetch results';
            getErrorResponse(2, errorMsg, (ex.getMessage() + ' : ' + ex.getStackTraceString()));
            return;
        }
        
        if(NULL == buList || buList.size() < 1) {
            errorMsg = 'No results to display.';
            getErrorResponse(2, errorMsg, NULL);
            return;
        }
        
        ResponseWrapper responseWrapper = new ResponseWrapper();
        
        HDApp_Utility.cls_meta_data responseMetaData = HDApp_Utility.ReturnMetaResponse(
            'Success in fetching sub-totals for agent collection', NULL, 1);
        responseWrapper.meta_data = responseMetaData;
        
        cls_data responseData = new cls_data();
        responseData.dp_collected_total = 0.00;
        responseData.dp_pending_total = 0.00;
        
        List<item_wrap> subTotalsWrapList = new List<item_wrap>();
        
        if(String.isNotBlank(groupBy) && groupBy.equalsIgnoreCase('PROJECT')) { /* subTotals by PROJECT */
            Map<String, item_wrap> subTotalsWrapMapByProject = new Map<String, item_wrap>();
            for(Booking_Unit__c bUnit : buList) {
                Decimal bookingAmount = (NULL == bUnit.Requested_Price__c ? 0.00 : bUnit.Requested_Price__c);
                Decimal collectedAmount = (NULL == bUnit.Total_Collections__c ? 0.00 : bUnit.Total_Collections__c);
                Decimal pendingAmount = (bookingAmount - collectedAmount);
                String projectName = String.isBlank(bUnit.Property_Name_Inventory__c) ? 'MISC' : bUnit.Property_Name_Inventory__c;
                
                responseData.dp_collected_total += collectedAmount;
                responseData.dp_pending_total += pendingAmount;
                
                if(subTotalsWrapMapByProject.containsKey(projectName)) {
                    item_wrap subTotalsWrap = subTotalsWrapMapByProject.get(bUnit.Property_Name_Inventory__c);
                    subTotalsWrap.name = API_GetAgentCommisionDetails.getInProperCase(projectName); // bUnit.Property_Name_Inventory__c;
                    subTotalsWrap.unit_count = (subTotalsWrap.unit_count + 1);
                    subTotalsWrap.dp_collected = (subTotalsWrap.dp_collected + collectedAmount);
                    subTotalsWrap.dp_pending = (subTotalsWrap.dp_pending + pendingAmount);
                    
                    subTotalsWrapMapByProject.put(bUnit.Property_Name_Inventory__c, subTotalsWrap);
                }
                else {
                    item_wrap subTotalsWrap = new item_wrap();
                    subTotalsWrap.name = API_GetAgentCommisionDetails.getInProperCase(projectName); // bUnit.Property_Name_Inventory__c;
                    subTotalsWrap.unit_count = 1;
                    subTotalsWrap.dp_collected = collectedAmount;
                    subTotalsWrap.dp_pending = pendingAmount;
                    
                    subTotalsWrapMapByProject.put(bUnit.Property_Name_Inventory__c, subTotalsWrap);
                }
            }
            
        	responseData.sub_totals = subTotalsWrapMapByProject.values();
        }
        else { /* subTotals by UNIT */
            for(Booking_Unit__c bUnit : buList) {
                Decimal bookingAmount = (NULL == bUnit.Requested_Price__c ? 0.00 : bUnit.Requested_Price__c);
                Decimal collectedAmount = (NULL == bUnit.Total_Collections__c ? 0.00 : bUnit.Total_Collections__c);
                Decimal pendingAmount = (bookingAmount - collectedAmount);
                
                responseData.dp_collected_total += collectedAmount;
                responseData.dp_pending_total += pendingAmount;
                
                item_wrap subTotalsWrap = new item_wrap();
                subTotalsWrap.name = bUnit.Unit_Location__c; // bUnit.Unit_Name__c;
                subTotalsWrap.unit_count = 1;
                subTotalsWrap.dp_collected = collectedAmount;
                subTotalsWrap.dp_pending = pendingAmount;
                
                subTotalsWrapList.add(subTotalsWrap);
            }
            
        	responseData.sub_totals = subTotalsWrapList;
        }
        
        responseWrapper.data = responseData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    } /* doGET() */
    
    private static void getErrorResponse(Integer statusCode, String responseMessage, String devMessage) {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        HDApp_Utility.cls_meta_data responseMetaData = HDApp_Utility.ReturnMetaResponse(
            responseMessage, devMessage, statusCode);
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }

    /* Wrapper classes for returning reponse */
    public class ResponseWrapper {
        public HDApp_Utility.cls_meta_data meta_data;
        public cls_data data;
    }

    public class cls_data {
        public Decimal dp_collected_total;
        public Decimal dp_pending_total;
        public item_wrap[] sub_totals;
    }

    public class item_wrap {
        public String name;
        public Integer unit_count;
        public Decimal dp_collected;
        public Decimal dp_pending;
    }
}