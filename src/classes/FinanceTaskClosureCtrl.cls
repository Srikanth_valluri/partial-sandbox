/********************************************************************************************************************
* Description - Controller of VF page 'FinanceTaskClosurepage' to close task from                                   *
*                button 'Close Finance Task'                                                                        *
*-------------------------------------------------------------------------------------------------------------------*
* Version   Date            Author              Description                                                         *
* 1.0       09/07/2019      Arjun Khatri        Initial Draft.                                                      *
*                                                                                                                   *
* 1.1       20/11/2019      Aishwarya Todkar    1.Called method to calculate rebate                                 *
*                                               2.Called method to create offers                                    *
*                                                                                                                   *
* 1.2       30/12/2019      Aishwarya Todkar    1.removed method that create offers                                 *
*                                               2.Called method to waive penalty                                    *
*                                               3.Closed finance task As requested by Aditya                        *
*                                                                                                                   *
* 1.3       15/01/2020      Aishwarya Todkar    1.Added check for Promotion Type                                    *
*                                                                                                                   *
* 1.4       29/03/2020      Aishwarya Todkar    1.Added logic for new promotion                                     *
*                                                                                                                   *
* 1.5       11/05/2020      Aishwarya Todkar    1.Enabled more promotions in old overdue process                    *
*********************************************************************************************************************/
public class FinanceTaskClosureCtrl {
    public Case objCase{get; set;}
    public Boolean showDetails {get; set;}
    List<Task> lstFinanceTasks;
    @TestVisible List<String> lstApiNames;
    List<Case> lstCases;
    public List<FieldDetailsWrapper> lstFieldDetailsWrapper {get; set;}
    Map<String, Schema.SObjectField> fieldMap;
    String recordTypeName;
    Id caseId;
    
    public FinanceTaskClosureCtrl (ApexPages.StandardController stdController) {
        //this.objCase = (Case)stdController.getRecord();
        caseId = stdController.getRecord().Id;
    }

/*************************************************************************************
 Description : Method to get finance tasks details.
 Parameters : None
 Return type : void
*************************************************************************************/    
    public void init() {

        
        showDetails = true;
        
        //Get Case record type
        
        if( caseId != null ) {
            String processVersion =  '';
            objCase = [SELECT
                            recordType.Name
                            , Run_New_SR_Process__c
                            FROM
                                Case 
                            WHERE 
                                Id =: caseId
                            ];
            
            if( objCase.Run_New_SR_Process__c ) {
                showDetails = true;
                processVersion = 'Overdue Rebate/Discount - New';
            }
            else {

                //call method to calculate and apply rebate : Added by Aihwarya Todkar as requested by Kanu
                showDetails = Test.isRunningTest() ? true : OverdueRebateCalculator.applyRebateOnUnits( new List<Id>{ objCase.Id } );
                processVersion = 'Overdue Rebate/Discount - Old';
            }
            
            System.debug('showDetails --- ' + showDetails );
        
            if( showDetails ) {

                // Get record from Custom setting according to case record type
                
                List<Finance_Task_Record_Type_Field_Mapping__mdt> lstFinanceTaskFieldMappings = 
                    new List<Finance_Task_Record_Type_Field_Mapping__mdt>();
                if( String.isNotBlank( processVersion ) ) {
                    lstFinanceTaskFieldMappings = [SELECT
                                                    MasterLabel
                                                    , Case_Field_API_Names__c
                                                    , Finance_Task_Name__c
                                                FROM
                                                    Finance_Task_Record_Type_Field_Mapping__mdt
                                                WHERE
                                                    MasterLabel =: processVersion
                                            ];
                
                    System.debug('lstFinanceTaskFieldMappings--- ' + lstFinanceTaskFieldMappings);
                }
                
                //Get finance task from case
                lstFinanceTasks = new List<Task>();
                if(lstFinanceTaskFieldMappings.size() > 0 ) {
                    lstFinanceTasks = [SELECT
                                            Subject
                                            , Status
                                        FROM
                                            Task
                                        WHERE
                                            Subject =: lstFinanceTaskFieldMappings[0].Finance_Task_Name__c
                                        AND whatId =: objCase.Id];
                    
                    //Show error message if task is already completed
                    if(lstFinanceTasks.size() > 0) {
                        
                        if(lstFinanceTasks[0].Status == 'Closed' || lstFinanceTasks[0].Status == 'Completed') {
                            showDetails = false;
                            ApexPages.addMessage( 
                                new ApexPages.Message(ApexPages.Severity.ERROR
                                                    , 'Task ' + lstFinanceTasks[0].Subject 
                                                    + ' is already completed.')
                            );
                        }
                        //Get and display values from case record
                        else {
                            showDetails = true;
                            String query;
                            if(Test.isRunningTest()) {
                                lstApiNames =  new List<String>{'Is_Overdue_Rebate_Letter_Genrated__c','Paid_Amount__c', 'Status'
                                                    , 'Overdue_Rebate_Letter_Date__c', 'Account_Email__c', 'Phone_Number__c'
                                                    , 'Promotion_Type__c', ' Promotion_Type__c'};
                                    query = 'SELECT Id ,Is_Overdue_Rebate_Letter_Genrated__c,Paid_Amount__c, Status,'
                                            +'Overdue_Rebate_Letter_Date__c,Account_Email__c,Phone_Number__c,Promotion_Type__c,'
                                            +'Run_New_SR_Process__c FROM Case Where Id = '
                                            + '\'' + objCase.Id + '\' ';
                                            System.debug('query == ' + query);
                                fillWrapper(query);
                            }
                            else {
                                
                                if(String.isNotBlank( lstFinanceTaskFieldMappings[0].Case_Field_API_Names__c)) {
                                    lstApiNames = lstFinanceTaskFieldMappings[0].Case_Field_API_Names__c.split(',');
                                    query = 'SELECT Id, Approved_Amount__c,Booking_Unit__r.Registration_ID__c,RecordType.Name,AccountId '
                                            + ',Booking_Unit__c,OwnerId, Promotion_Type__c,Run_New_SR_Process__c, '+ 
                                            + lstFinanceTaskFieldMappings[0].Case_Field_API_Names__c 
                                                    + ' FROM Case Where Id = ' + '\'' + objCase.Id + '\' ';//AND Id = \'a0x0Y000002Toww\'  '
                                }
                                System.debug('query == ' + query);
                                fillWrapper(query);
                            }
                        }
                    }//End lstFinanceTasks if
                }//End lstFinanceTaskFieldMappings if
            }//End showDetails if
        }//End objCase if
    }

/*************************************************************************************
 Description : Method to do operation before closingn task
 Parameters : None
 Return type : PageReference
*************************************************************************************/
    public PageReference closeFinanceTask() {
        Case caseToUpdate = new Case();
        caseToUpdate.Id = objCase.Id;
        Boolean updateCase = false;
        
        System.debug('closeFinanceTask objCase - ' + objCase);
        if(lstFieldDetailsWrapper!= null && lstFieldDetailsWrapper.size()>0) {
            for(FieldDetailsWrapper objWrap : lstFieldDetailsWrapper) {
                
                //if any of the value is blank then show error message
                /*if(String.isBlank(String.valueOf(objCase.get(objWrap.apiName)))) {
                    ApexPages.addMessage( 
                        new ApexPages.Message(ApexPages.Severity.ERROR
                                        , 'Please fill all values.')
                    );
                    return null;
                }
                //else close the task
                else {*/
                    //if(String.isBlank(String.valueOf(lstCases[0].get(objWrap.apiName)))) {
                        //caseToUpdate.put('Id', objCase.Id);
                        if( fieldMap.get( objWrap.apiName ).getDescribe().isUpdateable() )
                            caseToUpdate.put( objWrap.apiName, objCase.get( objWrap.apiName ) );
                        updateCase = true;
                    //} 
                //}
            }//End For

            //Call method to create offers : Added by Asihwarya Todkar as requested by Kanu
            //OfferCallout.createOffers( objCase.Id, lstFinanceTasks[0].id );
            //Commented as requested by Aditya
        }

        if(updateCase) {
            if( lstCases[0].Run_New_SR_Process__c )
                caseToUpdate.Status = 'Closed';
            update caseToUpdate;
        }
            //caseToUpdate.Close_Finance_Task__c = true;
        
        lstFinanceTasks[0].Status = 'Closed';
        lstFinanceTasks[0].Update_IPMS__c = true;
        lstFinanceTasks[0].Process_Name__c = recordTypeName;
        update lstFinanceTasks;

        if( !lstCases[0].Run_New_SR_Process__c &&
        lstCases[0].Promotion_Type__c == null ) {
            System.debug('Callingg Penalty Waiver service---');
            if( !Test.isRunningTest() )
                PenaltyWaiverService.updateDetailsInIPMS( lstCases );
        }
        return new PageReference('/' + objCase.ID);
    }

/*************************************************************************************
 Description : Method to prepare wrapper to display fields on page
 Parameters : None
 Return type : void
*************************************************************************************/
    void fillWrapper(String query) {
        try {
            lstCases = Database.query(query);
            lstFieldDetailsWrapper = new List<FieldDetailsWrapper>();

            fieldMap = Schema.getGlobalDescribe().get('Case').getDescribe().fields.getMap();
            
            if(lstCases != null) {
                objCase = lstCases[0];
                //fillWrapper();
                for(String strApiName : lstApiNames) {

                    FieldDetailsWrapper objWrap = new FieldDetailsWrapper();
                    objWrap.apiName = strApiName;
                    objWrap.label = fieldMap.get(strApiName).getDescribe().getLabel();
                    objWrap.fieldValue = String.valueOf(lstCases[0].get(strApiName)) != null ? String.valueOf(lstCases[0].get(strApiName)) : '';

                    if(String.valueOf(String.valueOf(fieldMap.get(strApiName).getDescribe().getType())) == 'BOOLEAN') {
                        objWrap.dataType = 'checkbox';
                        objWrap.fieldValue = String.valueOf(lstCases[0].get(strApiName));
                    }
                    else if((String.valueOf(fieldMap.get(strApiName).getDescribe().getType()) == 'STRING') || 
                            (String.valueOf(fieldMap.get(strApiName).getDescribe().getType()) == 'ID') || 
                            (String.valueOf(fieldMap.get(strApiName).getDescribe().getType()) == 'TextArea')) {
                        objWrap.dataType = 'text'; 
                
                    }
                    else if(String.valueOf(fieldMap.get(strApiName).getDescribe().getType()) == 'PICKLIST') {
                        objWrap.dataType = 'picklist';
                        objWrap.pickValues = new List<SelectOption>();
                        for(Schema.PicklistEntry pickVal : fieldMap.get(strApiName).getDescribe().getPickListValues()) {
                            objWrap.pickValues.add(new SelectOption(pickVal.getLabel(), pickVal.getValue()));
                        }
                    }
                    else if((String.valueOf(fieldMap.get(strApiName).getDescribe().getType()) == 'PERCENT') || 
                            (String.valueOf(fieldMap.get(strApiName).getDescribe().getType()) == 'CURRENCY') ||
                            (String.valueOf(fieldMap.get(strApiName).getDescribe().getType()) == 'DOUBLE')) {
                        objWrap.dataType = 'number';
                    }
                    else if(String.valueOf(fieldMap.get(strApiName).getDescribe().getType()) == 'DATE') {
                        objWrap.dataType = 'date';
                    }
                    else if(String.valueOf(fieldMap.get(strApiName).getDescribe().getType()) == 'DATETIME') {
                        objWrap.dataType = 'datetime-local';
                
                    }
                    else if(String.valueOf(fieldMap.get(strApiName).getDescribe().getType()) == 'EMAIL') {
                        objWrap.dataType = 'email';
                    }
                    else if(String.valueOf(fieldMap.get(strApiName).getDescribe().getType()) == 'PHONE') {
                        objWrap.dataType = 'tel';
                    }
                    else {
                        objWrap.dataType = 'text'; 
                    }   
                    lstFieldDetailsWrapper.add(objWrap);
                }
            }
            System.debug('lstFieldDetailsWrapper == ' + lstFieldDetailsWrapper);
        }
        catch(Exception ex) {
            ApexPages.addMessage( 
                new ApexPages.Message(ApexPages.Severity.ERROR
                                , ex.getMessage())
            );
        }
    }

/*************************************************************************************
 Description : Wrapper of field details
*************************************************************************************/
    Class FieldDetailsWrapper {
        public String apiName{get; set;} 
        public String label{get;set;}
        public String dataType{get;set;}
        public String fieldValue{get;set;}
        public List<SelectOption> pickValues {get;set;}

        public FieldDetailsWrapper() {
            apiName ='';
            fieldValue = '';
            dataType ='';
            label ='';

        }
    }
}