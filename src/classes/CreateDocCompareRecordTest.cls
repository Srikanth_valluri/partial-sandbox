/*-------------------------------------------------------------------------------------------------
Description: Test Class for CreateDocCompareRecord
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 09-04-2018       | Lochan Karle     | 1. Added logic to test CreateDocCompareRecord functionality
=============================================================================================================================
*/
@isTest
private class CreateDocCompareRecordTest{
	@isTest
	static void itShould(){
		Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('AOPT').getRecordTypeId();
		Case objCase = new Case();
        objCase.Status = 'NotSubmitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';  
		objCase.recordtypeId = devRecordTypeId;
        insert objCase;

		list<DocumentComparisonRestService.pages> lstPages = new list<DocumentComparisonRestService.pages>();
		DocumentComparisonRestService.pages objPage = new DocumentComparisonRestService.pages();
		objPage.accuracy = 2;
		objPage.addChar = 2;
		objPage.delChar = 2;
		objPage.originalPageNumber = 5;
		objPage.scannedPageNumber = 5;
		objPage.totalChar = 20;
		lstPages.add(objPage);
		
		DocumentComparisonRestService.DocComparison objDocCom = new DocumentComparisonRestService.DocComparison();
		objDocCom.accuracy = 2;
		objDocCom.addChar = 2;
		objDocCom.deleteChar = 2;
		objDocCom.diffReport = 'test';
		objDocCom.pageAccuracy = '5';
		objDocCom.totalChar = 20;
		objDocCom.pages = lstPages;
		
		list<Id> lstAttach = new list<Id>();
        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.case__c  = objCase.Id;
		objAttach.Attachment_URL__c = 'https://sftest.deeprootsurface.com/docs/t/IPMS-1031899-POAFile.pdf';
        objAttach.isValid__c = true;
        objAttach.Description__c = 'Abctd';
        objAttach.IsRequired__c = True;
        objAttach.Name = 'Signed Test';
        objAttach.type__c = 'Test';
        objAttach.isValid__c = true;
        objAttach.Need_Correction__c =  False;
        insert objAttach;

		SR_Attachments__c objAttach1 = new SR_Attachments__c();
        objAttach1.case__c  = objCase.Id;
		objAttach1.Attachment_URL__c = 'https://sftest.deeprootsurface.com/docs/t/IPMS-1131899-POAFile.pdf';
        objAttach1.isValid__c = true;
        objAttach1.Description__c = 'Abctd';
        objAttach1.IsRequired__c = True;
        objAttach1.Name = 'Test';
        objAttach1.type__c = 'Test';
        objAttach1.isValid__c = true;
        objAttach1.Need_Correction__c =  False;
        insert objAttach1;
		lstAttach.add(objAttach.id);
		lstAttach.add(objAttach1.id);

		Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"accuracy":0.0,"addChar":0,"deleteChar":0,"diffReport":null,"pageAccuracy":null,"pages":[],"totalChar":0}',
                                                 null);
		Test.setMock(HttpCalloutMock.class, fakeResponse);
		CreateDocCompareRecord objClass = new CreateDocCompareRecord();
		CreateDocCompareRecord.invokeApex(lstAttach);
		CreateDocCompareRecord.createErrorLogRecord(objCase.id);
		CreateDocCompareRecord.createDocumentAccRecord(objDocCom,'https://sftest.deeprootsurface.com/docs/t/IPMS-1031899-POAFile.pdf','https://sftest.deeprootsurface.com/docs/t/IPMS-79042-39839721_75740_CRF.pdf',objCase.id);
		Test.stopTest();
	}
}