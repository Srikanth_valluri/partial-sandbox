@isTest
public class Damac_doZBCalloutTest {
    static testMethod void doZBCallout(){
        Inquiry__c inq = new Inquiry__c ();
        inq.Activity_Counter__c = 101;
        inq.Inquiry_Status__c = 'Active';
        inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        inq.Telesales_Executive__c = UserInfo.getUserId();
        inq.Promoter_Name__c = 'Test';
        inq.Has_show_task__c = true;
        inq.Mobile_Phone__c  = '8977365305';
        inq.Mobile_Phone_2__c = '233121312';
        inq.Email__c = 'test@test.com';
        inq.Email_2__c = 'test@test.com';
        insert inq;
        Test.startTest();
            Damac_doZBCallout.doCallout(new List<Id>{inq.id});
        Test.stopTest();       
    }
}