/************************************************************************************************
 * @Name              : DAMAC_Agents_GetLeadPicklists_API_Test
 * @Description       : Test Class for DAMAC_Agents_GetLeadPicklists_API
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         01/06/2020       Created
***********************************************************************************************/

@isTest
public class DAMAC_Agents_GetLeadPicklists_API_Test{
    
    
     @isTest
    static void testAPI(){
     
        
        Account acc = InitialiseTestData.getCorporateAccount('TestAgency');
        insert acc ;

        Contact con = new Contact();
        con.LastName = 'Corporate Agency test';
        con.AccountId = acc.Id;
        insert con ;
        
        Inquiry__c inq = new Inquiry__c();
        inq.First_Name__c = 'Jithesh';
        inq.Last_Name__c = 'Vasudevan';
        inq.Title__c = 'MR.';      
        inq.Preferred_Language__c= 'English';
        inq.Inquiry_Source__c= 'Agent Referral';
        inq.Mobile_Phone__c = '00917829472432';
        inq.Email__c = 'jithesh@qburst.com';
        inq.Buyer_Type__c = '';
        inq.Nationality__c =  'Indian';
        inq.Agency_Name__c = acc.Id;
        inq.Agent_Name__c= con.Id;
        insert inq;
     //DAMAC_Agents_GetLeadPicklists_API d = new DAMAC_Agents_GetLeadPicklists_API();
       // d.pickListApiName = 'title__c';

        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/GetLeadPicklists/' + 'title__c';
        request.httpMethod = 'GET';
       // request.addHeader('Content-Type', 'application/json'); 
       // request.requestBody = Blob.valueof(myJSON);
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_GetLeadPicklists_API.doGet();
        Test.stopTest();
        
        
    }
     @isTest
    static void testAPI2(){
     
        
        Account acc = InitialiseTestData.getCorporateAccount('TestAgency');
        insert acc ;

        Contact con = new Contact();
        con.LastName = 'Corporate Agency test';
        con.AccountId = acc.Id;
        insert con ;
        
        Inquiry__c inq = new Inquiry__c();
        inq.First_Name__c = 'Jithesh';
        inq.Last_Name__c = 'Vasudevan';
        inq.Title__c = 'MR.';      
        inq.Preferred_Language__c= 'English';
        inq.Inquiry_Source__c= 'Agent Referral';
        inq.Mobile_Phone__c = '00917829472432';
        inq.Email__c = 'jithesh@qburst.com';
        inq.Buyer_Type__c = '';
        inq.Nationality__c =  'Indian';
        inq.Agency_Name__c = acc.Id;
        inq.Agent_Name__c= con.Id;
        insert inq;
     

        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/GetLeadPicklists/';
        request.httpMethod = 'GET';       
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_GetLeadPicklists_API.doGet();
        Test.stopTest();
        
        
    }
}