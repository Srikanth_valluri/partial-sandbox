/*-------------------------------------------------------------------------------------------------
Description: Apex controller to
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
v1.0     | 05-12-2018       | Lochana Rajput   | 1. Initial draft
----------------------------------------------------------------------------------------------------------------------------
v2.0     | 16-01-2019       | Lochana Rajput   | 1. Removed task to verify customer
----------------------------------------------------------------------------------------------------------------------------
v3.0     | 05-02-2019       | Lochana Rajput   | 1. Added logic for CRF doc generation
=============================================================================================================================
*/
public without sharing class FM_FundTransferController {
    // public list<SelectOption> lstUnits {get; set;}
    Integer cnt=0;
    public Boolean pollerBool{get;set;}
    public List<Attachment> lstCRFAttachments;
    public String attachmentURL{get;set;}
    private String batchId;
    public FM_Case__c objFMCase{get;set;}
    public Integer attchmentIndex{get;set;}
    public String strFMCaseId{get;set;}
    public Integer noOfRequDocs{get;set;}
    public list<FM_Documents__mdt> lstDocuments { get; set; }
    public String selectedToUnit{get;set;}
    public List<SelectOption> lstToUnits{get;set;}
    private List<SelectOption> lstPrpertyPaymentUnits;
    private List<SelectOption> lstServiceChargeUnits;
    private Account objAcc;
    public Boolean isMandatory{get;set;}
    public Boolean allowSR{get;set;}
    public Boolean isEdittable{get;set;}
    public Boolean showDocPanel{get;set;}
    public String strDocBody{get;set;}
    public String strDocName{get;set;}
    public String strSelectedUnit{get;set;}
    public list<SR_Attachments__c> uploadedDocuments{get;set;}
    public Booking_Unit__c objBU{get;set;}
    private String strAccountId,strSRType;
    private Set<String> activeStatusSet;
    private map<String, FM_Documents__mdt> mapProcessDocuments ;
    private Set<String> uploadedDocLst;
    public String docIdToDelete{get;set;}
    private Id FMCollectionUserId;
    private list<String> lstReqDocs;
    private list<String> lstReqDocsTemp;
    public FM_FundTransferController() {
        //Get logged in user details
        //Get FM Collections user
        lstCRFAttachments = new List<Attachment>();
        lstReqDocs = new List<String>();
        lstReqDocsTemp = new List<String>();
        allowSR = true;
        isMandatory=false;
        isEdittable=true;
        List<User> lstUser = new List<User>();
        lstUser = [SELECT ID FROM User
                  where Profile.Name = 'FM Collections'
                  AND Id =: UserInfo.getUserId() LIMIT 1];
        if(lstUser.isEmpty()) {
            allowSR = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                'Only FM Collections user can initiate Fund Transfer request'));
        }
        else {
            FMCollectionUserId = lstUser[0].Id;
        }
        System.debug('==FMCollectionUserId=='+FMCollectionUserId);
        objFMCase = new FM_Case__c();
        noOfRequDocs=0;
        uploadedDocLst = new Set<String>();
        uploadedDocuments = new List<SR_Attachments__c>();
        lstPrpertyPaymentUnits = new List<SelectOption>();
        showDocPanel = false;
        lstServiceChargeUnits = new List<SelectOption>();
        activeStatusSet = Booking_Unit_Active_Status__c.getall().keyset();
        objAcc = new Account();
        objBU = new Booking_Unit__c();
        strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
        strFMCaseId = ApexPages.currentPage().getParameters().get('Id');
        System.debug('==strSelectedUnit===' + strSelectedUnit);

        if(String.isBlank(strFMCaseId)){

            strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
            strSRType = ApexPages.currentPage().getParameters().get('SRType');
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                                    get('Fund Transfer').getRecordTypeId();
        }
        else {//Get existing case details
            getCaseDetails();

        }
        if(objFMCase.Submitted__c==true){
            isEdittable=false;
        }
        if(String.isNotBlank(strSelectedUnit) && strSelectedUnit != 'None') {
            List<Booking_Unit__c> lstBU = new List<Booking_Unit__c>();
            lstBU = [SELECT Id, Unit_Name__c,Property_City__c, Booking__r.Account__c
                     FROM Booking_Unit__c
                     WHERE Id=:strSelectedUnit];
            if(lstBU.size() > 0) {
                System.debug('==strAccountId===' + (strAccountId != objBU.Booking__r.Account__c));
                objBU = lstBU[0];
                System.debug('==objBU.Booking__r.Account__c===' + objBU.Booking__r.Account__c);
                if(strAccountId != objBU.Booking__r.Account__c) {

                    allowSR = false;
                    System.debug('==allowSR===' + allowSR);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                'Selected account is not owner of the selected unit, fund transfer cannot be done'));
                }
                System.debug('==allowSR===' + allowSR);
            }

        //Case initiation
        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                            get('Fund Transfer').getRecordTypeId();
        objFMCase.Request_Type_DeveloperName__c = strSRType;
        System.debug('===strSRType==='+strAccountId);
        objFMCase.RecordTypeId = devRecordTypeId;
        objFMCase.Origin__c='Walk-In';
        // objFMCase.Tenant__c = strAccountId;
        objFMCase.Account__c = strAccountId;
        objFMCase.Fund_Transfer_From_Unit__c = strSelectedUnit ;
        objFMCase.Booking_Unit__c = strSelectedUnit ;
        objFMCase.Request_Type__c = 'Fund Transfer Request';
        //get account details
        if(String.isNotBlank(strAccountId)) {
            List<Account> lstAcc = new List<Account>();
            lstAcc = [SELECT Id, Party_ID__c,Name,Mobile_Phone__pc,Email__pc,IsPersonAccount
                    FROM Account
                    WHERE Id =: strAccountId
                    AND IsPersonAccount = TRUE];
            // if(lstAcc.size() >0) {
            //     objAcc = lstAcc[0];
            //     objFMCase.Tenant_email__c = objAcc.Email__pc;
            //     objFMCase.Tenant__c = objAcc.Id;
            // }
            //Get owned units
            List<Booking_Unit__c> lstUnits = new List<Booking_Unit__c>();
            lstUnits = [SELECT Id, Unit_Name__c,Handover_Flag__c,Property_City__c
                        FROM Booking_Unit__c
                        WHERE Booking__r.Account__c =: strAccountId
                        AND Registration_Status__c IN :activeStatusSet];
            for(Booking_Unit__c obj : lstUnits) {
                if(obj.Id != objBU.Id) {
                    if(obj.Handover_Flag__c == 'Y') {
                        lstServiceChargeUnits.add(new SelectOption(obj.Id, obj.Unit_Name__c));
                    }
                    lstPrpertyPaymentUnits.add(new SelectOption(obj.Id, obj.Unit_Name__c));
                }
            }
        }
        getDocuments();//Get documents to upload from metadata
        if(String.isNotBlank(strFMCaseId)){
            getToUnits();
            selectedToUnit = objFMCase.Fund_Transfer_To_Unit__c;
            showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
        }
        }
        // lstUnits = new list<SelectOption>();
        // lstUnits.add(new selectOption('None','--None--'));
        // Set<String> activeStatusSet= Booking_Unit_Active_Status__c.getall().keyset();
        // for( Booking_Unit__c objBU : [SELECT Id, Unit_Name__c
        //                                 FROM Booking_Unit__c
        //                                 WHERE Booking__r.Account__c = :strAccountId
        //                 // WHERE ( Resident__c = :strAccountId
        //                 //         OR Booking__r.Account__c = :strAccountId
        //                 //         OR Tenant__c = :strAccountId )
        //                   AND ( Handover_Flag__c = 'Y'
        //                         OR Early_Handover__c = true )
        //                   AND Registration_Status__c IN :activeStatusSet]){
        //
        //     lstUnits.add(new selectOption(objBU.Id,objBU.Unit_Name__c));
        // }

    }//constructor
    /*
    public void getDetails() {
        System.debug('==strSelectedUnit===' + strSelectedUnit);

        if(String.isBlank(strFMCaseId)){

            // strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
            strSRType = ApexPages.currentPage().getParameters().get('SRType');
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                                    get('Fund Transfer').getRecordTypeId();
        }
        else {//Get existing case details
            getCaseDetails();

        }
        if(objFMCase.Submitted__c==true){
            isEdittable=false;
        }
        if(String.isNotBlank(strSelectedUnit) && strSelectedUnit != 'None') {
            List<Booking_Unit__c> lstBU = new List<Booking_Unit__c>();
            lstBU = [SELECT Id, Unit_Name__c,Property_City__c, Booking__r.Account__c
                     FROM Booking_Unit__c
                     WHERE Id=:strSelectedUnit];
            if(lstBU.size() > 0) {
                System.debug('==strAccountId===' + (strAccountId != objBU.Booking__r.Account__c));
                System.debug('==objBU.Booking__r.Account__c===' + objBU.Booking__r.Account__c);
                objBU = lstBU[0];
                if(strAccountId != objBU.Booking__r.Account__c) {

                    allowSR = false;
                    System.debug('==allowSR===' + allowSR);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                'Selected account is not owner of the selected unit, fund transfer cannot be done'));
                }
                System.debug('==allowSR===' + allowSR);
            }

        //Case initiation
        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                            get('Fund Transfer').getRecordTypeId();
        objFMCase.Request_Type_DeveloperName__c = strSRType;
        System.debug('===strSRType==='+strAccountId);
        objFMCase.RecordTypeId = devRecordTypeId;
        objFMCase.Origin__c='Walk-In';
        // objFMCase.Tenant__c = strAccountId;
        objFMCase.Account__c = strAccountId;
        objFMCase.Fund_Transfer_From_Unit__c = strSelectedUnit ;
        objFMCase.Booking_Unit__c = strSelectedUnit ;
        objFMCase.Request_Type__c = 'Fund Transfer Request';
        //get account details
        if(String.isNotBlank(strAccountId)) {
            List<Account> lstAcc = new List<Account>();
            lstAcc = [SELECT Id, Party_ID__c,Name,Mobile_Phone__pc,Email__pc,IsPersonAccount
                    FROM Account
                    WHERE Id =: strAccountId
                    AND IsPersonAccount = TRUE];
            // if(lstAcc.size() >0) {
            //     objAcc = lstAcc[0];
            //     objFMCase.Tenant_email__c = objAcc.Email__pc;
            //     objFMCase.Tenant__c = objAcc.Id;
            // }
            //Get owned units
            List<Booking_Unit__c> lstUnits = new List<Booking_Unit__c>();
            lstUnits = [SELECT Id, Unit_Name__c,Handover_Flag__c,Property_City__c
                        FROM Booking_Unit__c
                        WHERE Booking__r.Account__c =: strAccountId
                        AND Registration_Status__c IN :activeStatusSet];
            for(Booking_Unit__c obj : lstUnits) {
                if(obj.Id != objBU.Id) {
                    if(obj.Handover_Flag__c == 'Y') {
                        lstServiceChargeUnits.add(new SelectOption(obj.Id, obj.Unit_Name__c));
                    }
                    lstPrpertyPaymentUnits.add(new SelectOption(obj.Id, obj.Unit_Name__c));
                }
            }
        }
        getDocuments();//Get documents to upload from metadata
        if(String.isNotBlank(strFMCaseId)){
            getToUnits();
            selectedToUnit = objFMCase.Fund_Transfer_To_Unit__c;
            showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
        }
        }
    }*/
    public void getCaseDetails() {
        objFMCase = [SELECT Id,Request_Type_DeveloperName__c,Tenant__c,
                            Type_of_Fund_Transfer__c,
                            Name,Account__c,
                            Fund_Transfer_Amount__c,
                            Fund_Transfer_From_Unit__c,
                            Fund_Transfer_To_Unit__c,
                            Booking_Unit__c,
                            Submitted__c,
                            (SELECT Type__c,Id,Name, View__c,Attachment_URL__c
                            FROM Documents__r)
                    FROM FM_Case__c
                    WHERE Id =: strFMCaseId];
        uploadedDocuments.addAll(objFMCase.Documents__r);
        getUploadedDocuments();
        // selectedValuesOfAccessCard.addAll(objFMCase.Access_Card_Permission__c.split(';'));
        strAccountId = objFMCase.Account__c;
        strSelectedUnit = objFMCase.Fund_Transfer_From_Unit__c;
        strSRType = objFMCase.Request_Type_DeveloperName__c;
    }

    private void getUploadedDocuments() {
        uploadedDocLst = new Set<String>();
        for(SR_Attachments__c obj : uploadedDocuments) {
            uploadedDocLst.add(obj.Name);
        }
    }

    public void getToUnits() {
        lstToUnits = new List<SelectOption>();
        System.debug('==Type_of_Fund_Transfer__c=='+objFMCase.Type_of_Fund_Transfer__c);
        if(objFMCase.Type_of_Fund_Transfer__c == 'To service charge account') {
            lstToUnits.addAll(lstServiceChargeUnits);
        }
        else if(objFMCase.Type_of_Fund_Transfer__c == 'To property payment') {
            lstToUnits.addAll(lstPrpertyPaymentUnits);
        }
    }
    public void deleteDocument() {
        try{
            //docIdToDelete contains Document id to delete and its name resp separated by ','
            List<FM_Documents__mdt> lstDocumentsTemp = new List<FM_Documents__mdt>();
            List<String> tempLst = docIdToDelete.split(',');
            System.debug('tempLst===' + tempLst);
            delete [SELECT Id from SR_Attachments__c where Id=:tempLst[0]];
            uploadedDocuments = new List<SR_Attachments__c>();
            uploadedDocuments = [SELECT Type__c,Name, View__c,Attachment_URL__c
                                FROM SR_Attachments__c
                                WHERE FM_Case__c =:objFMCase.Id];
            // getUploadedDocuments();
            if(mapProcessDocuments.containsKey(tempLst[1])) {
                lstDocumentsTemp.add(mapProcessDocuments.get(tempLst[1]));
                // lstDocuments.add(mapProcessDocuments.get(tempLst[1]));
            }
            lstDocumentsTemp.addAll(lstDocuments);
            lstDocuments = new List<FM_Documents__mdt>();
            lstDocuments.addAll(lstDocumentsTemp);
            if(lstReqDocs.contains(tempLst[1])) {
                lstReqDocsTemp.add(tempLst[1]);
            }
            isMandatory = lstReqDocsTemp.size() > 0 ? TRUE : FALSE;
            showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
            // getDocumentsList();
        }
        catch(System.Exception excp) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
    }
    private void getDocuments() {
        lstDocuments = new List<FM_Documents__mdt>();
        lstReqDocs = new List<String>();
        mapProcessDocuments = new map<String,FM_Documents__mdt>();
        List<String> lstOfString = new List<String>();
        System.debug('uploadedDocLst======' + uploadedDocLst);
        System.debug('objBU++++++' + objBU);
        System.debug('objBU.Property_City__c = ' + objBU.Property_City__c);
        for (FM_Documents__mdt objDocMeta : FM_Utility.getDocumentsList( strSRType, objBU.Property_City__c)) {
            System.debug('objDocMeta = ' + objDocMeta);
            mapProcessDocuments.put(objDocMeta.Document_Name__c, objDocMeta);
            if(!uploadedDocLst.contains(objDocMeta.Document_Name__c)) {
                    lstDocuments.add(objDocMeta);
            }
            if(objDocMeta.Mandatory__c) {
                lstReqDocs.add(objDocMeta.Document_Name__c);
                if(!lstReqDocsTemp.contains(objDocMeta.Document_Name__c)) {
                    lstReqDocsTemp.add(objDocMeta.Document_Name__c);
                }
            }
            if(objDocMeta.Mandatory__c && !uploadedDocLst.contains(objDocMeta.Document_Name__c)) {
                lstOfString.add('true');
            }
        }
        if (lstOfString.size() > 0) {
            isMandatory=true;
        }
        System.debug('lstDocuments++++++' + lstDocuments);
    }

    public PageReference uploadDocument() {
        try{
            System.debug('==strDocBody==='+strDocBody);
            System.debug('==strDocName==='+strDocName);
            if(String.isNotBlank(strDocBody) && String.isNotBlank(strDocName)) {
                System.debug('==strDocName==='+strDocName);
                List<UploadMultipleDocController.MultipleDocRequest> lstReq = new List<UploadMultipleDocController.MultipleDocRequest>();
                List<SR_Attachments__c> lstAttachments = new List<SR_Attachments__c>();
                SR_Attachments__c objAtt = new SR_Attachments__c();
                objAtt.Name = strDocName;
                // objAtt.Type__c = fileNames.size() ==2 ? fileNames[1]: '';
                objAtt.FM_Case__c = objFMCase.Id;
                objAtt.Booking_Unit__c = objBU.Id;
                lstAttachments.add(objAtt);
                System.debug('==lstAttachments==='+lstAttachments);
                UploadMultipleDocController.MultipleDocRequest reqObj = new UploadMultipleDocController.MultipleDocRequest();
                reqObj.category = 'Document';
                reqObj.entityName = 'Damac Service Requests';

                blob objBlob = FM_Utility.extractBody(strDocBody);
                if(objBlob != null){
                    reqObj.base64Binary =  EncodingUtil.base64Encode(objBlob);
                }
                reqObj.fileDescription = strDocName;
                reqObj.fileId = objFMCase.Name + '-'+ String.valueOf(System.currentTimeMillis()) +'.'
                + strDocName.substringAfterLast('.');
                reqObj.fileName = objFMCase.Name + '-'
                            + String.valueOf(System.currentTimeMillis())  +'.'+ strDocName.substringAfterLast('.');
                reqObj.registrationId = objFMCase.Name;
                reqObj.sourceFileName = 'IPMS-'+objAcc.party_ID__C+'-'+(strDocName);
                reqObj.sourceId = 'IPMS-'+objAcc.party_ID__C+'-'+(strDocName);
                lstReq.add(reqObj);
                System.debug('==strDocName==='+strDocName);
                if(lstReq.size() > 0) {
                    UploadMultipleDocController.data respObj = new UploadMultipleDocController.data();
                    respObj= UploadMultipleDocController.getMultipleDocUrl(lstReq);
                    System.debug('===objData.PARAM_ID==' + respObj);
                    if(respObj != NULL && lstReq.size() > 0) {
                        if(respObj.status == 'Exception'){
                            ApexPages.addmessage(new ApexPages.message(
                                ApexPages.severity.Error,respObj.message));
                            // errorLogger(respObj.message, '', '');
                            return null;
                       }
                       System.debug('===lstAttachment==' + lstAttachments);
                       if(respObj.Data == null || respObj.Data.size()==0){
                           ApexPages.addmessage(new ApexPages.message(
                           ApexPages.severity.Error,'Problems while getting response from document upload'));
                           // errorLogger('Problems while getting response from document upload', '', '');
                           return null;
                       }
                       System.debug('===lstAttachment==' + lstAttachments);
                       Set<SR_Attachments__c> setSRAttachments_Valid = new Set<SR_Attachments__c>();
                        for(SR_Attachments__c att : lstAttachments) {
                            for(UploadMultipleDocController.MultipleDocResponse objData : respObj.data) {
                                System.debug('===objData.PARAM_ID==' + objData.PARAM_ID);
                                System.debug('===lstAttachments==' + 'IPMS-'+objAcc.party_ID__C+'-'+(strDocName));
                                if('IPMS-'+objAcc.party_ID__C+'-'+(strDocName) == objData.PARAM_ID){
                                   if(objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)){
                                       att.Attachment_URL__c = objData.url;
                                       att.FM_Case__c = objFMCase.Id;
                                       setSRAttachments_Valid.add(att);
                                       System.debug('==att.Name==='+att.Name);
                                       if(att.Name == 'Customer Request Form') {
                                           System.debug('==att.Name==='+att.Name);
                                          objFMCase.Additional_Doc_File_URL__c = objData.url;
                                      }
                                   }
                                   else{
                                       ApexPages.addmessage(new ApexPages.message(
                                       ApexPages.severity.Error,'Problems while getting response from document '+objData.PARAM_ID));
                                       // errorLogger('Problems while getting response from document '+objData.PARAM_ID, '', '');
                                       return null;
                                   }
                               }
                            }
                        }//outer for
                        List<SR_Attachments__c> lstAtt = new List<SR_Attachments__c>();

                        lstAtt.addAll(setSRAttachments_Valid);
                        for(SR_Attachments__c att : lstAtt) {
                            att.FM_Case__c = objFMCase.Id;
                        }
                        upsert lstAtt;
                        uploadedDocuments = [SELECT Type__c,Name, View__c,
                                                    Attachment_URL__c
                                            from SR_Attachments__c
                                            WHERE FM_Case__c =:objFMCase.Id];
                        System.debug('===uploadedDocuments=='+uploadedDocuments);
                        for(Integer i=0; i<lstDocuments.size(); i++) {
                            if(lstDocuments[i].Document_Name__c == strDocName) {
                                lstDocuments.remove(i);
                                if(lstReqDocs.contains(strDocName)) {
                                    lstReqDocsTemp.remove(i);
                                }
                                break;
                            }
                        }
                        showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
                        isMandatory = lstReqDocsTemp.size() > 0 ? TRUE : FALSE;
                        System.debug('===isMandatory=='+isMandatory);
                        // lstDocuments.remove(lstDocuments.indexOf(fileNames[0]));//removing uploaded doc placeholder
                }//res
            }//req
            }
        }//try
        catch(Exception excp) {
            System.debug('excp==='+excp);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
        strDocBody = '';
        return null;
    }
    public void createCaseShowUploadDoc(){
        upsert objFMCase;
        System.debug('objFMCase-----'+objFMCase);
        System.debug('objFMCase.id------'+objFMCase.id);
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            getDocuments();
        }
        strFMCaseId=objFMCase.id;
        System.debug('strFMCaseId----'+strFMCaseId);
        showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
    }

    public void addDocument() {
        String attName = 'Attachment '+ (lstDocuments.size() ==0 ? 1 : lstDocuments.size());
        lstDocuments.add(new FM_Documents__mdt(Document_Name__c=attName,
        Mandatory__c=false));
        showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
    }

    public void removeDocument() {
        System.debug('===attchmentIndex======' + attchmentIndex);
        if(attchmentIndex != NULL) {
            lstDocuments.remove(attchmentIndex);
        }
    }

    public PageReference saveAsDraft() {
        objFMCase.Fund_Transfer_To_Unit__c = selectedToUnit;
        objFMCase.Status__c='Draft Request';
        upsert objFMCase;
        return new PageReference('/' + objFMCase.Id);
    }

    public PageReference submitSR() {
        objFMCase.Submitted__c = true;
        objFMCase.Fund_Transfer_To_Unit__c = selectedToUnit;
        objFMCase.Status__c='Submitted';
        upsert objFMCase;
        // Removed task for customer verification, istead added checkbox on UI
        //Create task to verify customer
        // Task objTask = new Task();
        // objTask.whatId = objFMCase.Id;
        // objTask.Subject = Label.FM_Verify_Customer_Label;
        // objTask.Status = 'Not Started';
        // objTask.Priority = 'Normal';
        // objTask.Process_Name__c = 'FM Fund Transfer';
        // objTask.ActivityDate = Date.today().addDays(2);
        // objTask.Assigned_User__c = 'FM Collection';
        // if(String.isNotBlank(FMCollectionUserId)) {
        //     objTask.OwnerId = FMCollectionUserId;
        // }
        // insert objTask;

        //Create task to allocate funds
        Task objTask = new Task();
        objTask.whatId = objFMCase.Id;
        objTask.Subject = Label.FM_Fund_Transfer_Allocate_Funds_task_subject;
        objTask.Status = 'Not Started';
        objTask.Priority = 'Normal';
        objTask.Process_Name__c = 'FM Fund Transfer';
        objTask.ActivityDate = Date.today().addDays(2);
        objTask.Assigned_User__c = 'FM Finance';
        insert objTask;
        //Create task in IPMS
        if(! Test.isRunningTest()) {
            FundTransferFMFinanceTask.createTaskinIPMS(new set<Id>{objTask.Id}, new set<Id>{objFMCase.Id});
        }
        return new PageReference('/' + objFMCase.Id);
    }

    public void generateNDA() {
        System.debug('objFMCase--------'+objFMCase.Id);
        if( objFMCase.Id != NULL ) {
            lstCRFAttachments = [SELECT id,
                                     name,
                                     parentId
                                 FROM Attachment
                                 WHERE parentId =: objFMCase.id
                                 AND name =: System.Label.CRF_Document
                                 ORDER By createddate desc
                                 LIMIT 1];
            system.debug('lstCRFAttachments'+lstCRFAttachments);
            if(!lstCRFAttachments.isEmpty()) {
                String idDocument = lstCRFAttachments[0].Id;
                attachmentURL = '/servlet/servlet.FileDownload?file=' + idDocument;
            }
            if(String.isBlank(attachmentURL)) {
                FM_GenerateDrawloopDocumentBatch objInstance = new FM_GenerateDrawloopDocumentBatch(objFMCase.Id
                                 , System.Label.CRF_Doc_DDP_Template_Id
                                 , System.Label.CRF_Doc_Template_Delivery_Id);
                batchId = Database.ExecuteBatch(objInstance);
                System.debug('==batchId===='+batchId);
                pollerBool = true;
            }//if
        }//if
    }//generateNDA

    public void checkDrawloopBatchStatus() {
        AsyncApexJob job = [SELECT Id, Status FROM AsyncApexJob WHERE Id =: batchId];
        System.debug('objFMCase--------'+objFMCase.Id);
        System.debug('job--------'+job.Status);
        System.debug('pollerBool--------'+pollerBool);
        if(job.Status == 'Completed') {
            lstCRFAttachments = [SELECT id, parentId
                                 FROM Attachment
                                 WHERE parentId =: objFMCase.id
                                 AND name =: System.Label.CRF_Document
                                 ORDER By createddate desc
                                 LIMIT 1];
             System.debug('lstCRFAttachments--------'+lstCRFAttachments);
            if(lstCRFAttachments.size() > 0) {
                pollerBool = false;
                system.debug('objAttach==='+lstCRFAttachments[0]);
                String idDocument = lstCRFAttachments[0].Id;
                attachmentURL = '/servlet/servlet.FileDownload?file=' + idDocument;
            }
        }
        else {
            pollerBool = true;
        }
    }
}