public with sharing class ReceiptDetailsController {
    public static String GetReceiptDetails(String strPartyId) {
    	String strResponse = null;
    	if( String.isNotBlank( strPartyId ) ) { 
    		ReceiptDetails1.ReceiptIPMSHttpSoap11Endpoint objReceipt = new ReceiptDetails1.ReceiptIPMSHttpSoap11Endpoint();
    		objReceipt.timeout_x = 120000;

	    	ReceiptDetails3.APPSXXDC_PROCESS_SERX1794747X2X4 regTerms = new ReceiptDetails3.APPSXXDC_PROCESS_SERX1794747X2X4();
			regTerms.ATTRIBUTE1 = '';
			regTerms.ATTRIBUTE10 = '';
			regTerms.ATTRIBUTE11 = '';
			regTerms.ATTRIBUTE12 = '';
			regTerms.ATTRIBUTE13 = '';
			regTerms.ATTRIBUTE14 = '';
			regTerms.ATTRIBUTE15 = '';
			regTerms.ATTRIBUTE16 = '';
			regTerms.ATTRIBUTE17 = '';
			regTerms.ATTRIBUTE18 = '';
			regTerms.ATTRIBUTE19 = '';
			regTerms.ATTRIBUTE2 = '';
			regTerms.ATTRIBUTE20 = '';
			regTerms.ATTRIBUTE21 = '';
			regTerms.ATTRIBUTE22 = '';
			regTerms.ATTRIBUTE23 = '';
			regTerms.ATTRIBUTE24 = '';
			regTerms.ATTRIBUTE25 = '';
			regTerms.ATTRIBUTE26 = '';
			regTerms.ATTRIBUTE27 = '';
			regTerms.ATTRIBUTE28 = '';
			regTerms.ATTRIBUTE29 = '';
			regTerms.ATTRIBUTE3 = '';
			regTerms.ATTRIBUTE30 = '';
			regTerms.ATTRIBUTE31 = '';
			regTerms.ATTRIBUTE32 = '';
			regTerms.ATTRIBUTE33 = '';
			regTerms.ATTRIBUTE34 = '';
			regTerms.ATTRIBUTE35 = '';
			regTerms.ATTRIBUTE36 = '';
			regTerms.ATTRIBUTE37 = '';
			regTerms.ATTRIBUTE38 = '';
			regTerms.ATTRIBUTE39 = '';
			regTerms.ATTRIBUTE4 = '';
			regTerms.ATTRIBUTE41 = '';
			regTerms.ATTRIBUTE42 = '';
			regTerms.ATTRIBUTE43 = '';
			regTerms.ATTRIBUTE44 = '';
			regTerms.ATTRIBUTE45 = '';
			regTerms.ATTRIBUTE46 = '';
			regTerms.ATTRIBUTE47 = '';
			regTerms.ATTRIBUTE48 = '';
			regTerms.ATTRIBUTE49 = '';
			regTerms.ATTRIBUTE5 = '';
			regTerms.ATTRIBUTE50 = '';
			regTerms.ATTRIBUTE6 = '';
			regTerms.ATTRIBUTE7 = '';
			regTerms.ATTRIBUTE8 = '';
			regTerms.ATTRIBUTE9 = '';
			regTerms.PARAM_ID = strPartyId;

			try {
                strResponse = objReceipt.GetReceiptDetails('2-'+String.valueOf( Datetime.now().getTime()),'GET_RECEIPT_LIST','SFDC', regTerms);
                system.debug('== Receipt strResponse ==  '+strResponse  );
            }
            catch( Exception e ) {
                system.debug(' Exception occured while fetching unit details ==  '+e.getMessage() );
                return strResponse ;
            }
		}	
		return strResponse;
    }
}