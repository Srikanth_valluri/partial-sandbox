/***********************************************************************
* Description - Test class developed for PromotorSearchController
*
* Version            Date            Author            Description
* 1.0                27/04/18       Arun Kumar            Initial Draft
**********************************************************************/
@istest
public class PromoterInventoryFilterController_Test{


    static testmethod void callMethods(){
        user u = [select id from user where id!=:userInfo.getUserId() LIMIT 1];
        System.runAs(u){
        List<Inventory__c> invList=TestDataFactory.CreateInventoryRecords(new List<Inventory__c>{new Inventory__c()});
        List<Inventory__c> invList1=TestDataFactory.CreateInventoryRecords(new List<Inventory__c>{new Inventory__c()});
        List<Marketing_Documents__c> MarDocList=TestDataFactory.CreateMarketingDocRecords(new List<Marketing_Documents__c>{new Marketing_Documents__c()});
        MarDocList[0].Name='test';
        update MarDocList[0];

        invList1[0].status__c = 'Released';
        invList1[0].property_city__c = 'Dubai';
        invList1[0].Property_Status__c = 'Ready';
        invList1[0].Unit_Type__c = 'Hotel';
        invList1[0].property_name__c = 'DAMAC TOWER';
       // invList1[0].Marketing_Name_Doc__c=MarDocList[0].id;
        invList1[0].Bedroom_Type__c= 'Single';
        invList1[0].Floor_Package_Name__c='Test';
        update invList1[0];

        Inventory_User__c invUser=new Inventory_User__c();
        invUser.Inventory__c=invList[0].id;
        invUser.User__c=userInfo.getUserId();
        insert invUser;
        
        Inventory_User__c invUser1=new Inventory_User__c();
        invUser1.Inventory__c=invList1[0].id;
        invUser1.User__c=u.id;
        insert invUser1;
        
        // System.runAs(u){
        
            PromoterInventoryFilterController con = new PromoterInventoryFilterController();
            //PromoterInventoryFilterController.idsselected = MarDocList[0].id;

            con.getItems();
            con.getPropStatus();
            con.getCities();
            con.getUnitTypes();
            con.getPropertyNames();
            con.getAcd();
            con.getBedRooms();
            con.getPkgNames();
            //con.getTimeZone(); 
            con.pageload();
            list<inventory_user__c> invo = new list<inventory_user__c>();
            invo.add(invuser1);
            con.otherUserinventory = invo;        
            con.filterdata();
            con.preparedata();
            //con.getTimeZone();
            PromoterInventoryFilterController.searchinventory();
        }
        
        
    }
}