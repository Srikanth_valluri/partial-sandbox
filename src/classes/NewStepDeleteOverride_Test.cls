@isTest
public class NewStepDeleteOverride_Test {
    
    @testSetup 
    static void setupData() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;
        
        List<New_Step__c> newStepsList = new List<New_Step__c>();
        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.id;
        nstp1.Step_No__c = 2;
        nstp1.Step_Status__c = 'Awaiting Token Deposit';
        nstp1.Step_Type__c = 'Token Payment';
        newStepsList.add(nstp1);
        
        New_Step__c nstp2 = new New_Step__c();
        nstp2.Service_Request__c = sr.id;
        nstp2.Step_No__c = 3;
        nstp2.Step_Status__c = 'Awaiting PC Confirmation';
        nstp2.Step_Type__c = 'PC Confirmation';
        newStepsList.add(nstp2);
        insert newStepsList;
    }
    
    @isTest
    static void testNewStepEditDeleteOverride(){
        Test.startTest();
        
        New_Step__c step = [Select Id From New_Step__c Limit 1];
        ApexPages.StandardController std = new ApexPages.StandardController(step);
        NewStepDeleteOverride_ctrl overrideDeleteNewStep = new NewStepDeleteOverride_ctrl(std);
        
        Test.stopTest();
    }
    
}