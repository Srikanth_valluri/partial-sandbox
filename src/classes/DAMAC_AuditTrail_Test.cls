@istest(SeeAllData=true)
public class DAMAC_AuditTrail_Test{
    static testmethod void DAMAC_AuditTrail_Batch(){
        /*
        AuditTrail__c a = new AuditTrail__c();
        a.AuditTrail_Id__c = '123';
        insert a;
        */
       
        
        test.starttest();
            DAMAC_AuditTrail batchclass = new DAMAC_AuditTrail();
            DataBase.executeBatch(batchclass,200);             
        test.stoptest();
    }
    
    
    static testMethod void DAMAC_AuditTrail_Batch_Sch() {    
        Test.StartTest();
            DAMAC_AuditTrail_Sch sh1 = new DAMAC_AuditTrail_Sch();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test DAMAC_AuditTrail_Batch_Sch', sch, sh1);             
        Test.stopTest(); 
    }
 


}