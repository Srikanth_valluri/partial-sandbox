public with sharing class CheckAllTasksOfHOBuilding {
    
    @InvocableMethod (label='' description='')
    public static void updateTask (List<Id> listTaskId) {
        list<task> lstTasks = new list<Task>();
        set<Id> setLocId = new set<id>();
        map<Id, set<String>> mapLocIdTasks = new map<Id, set<String>>();
        list<Location__c> lstLocation = new list<Location__c>();
        //map<Id, Location__c> mapLocIdRecord = new map<Id, Location__c>();
        for (Task objtask : [Select Id, WhatId, Subject, Status From Task where Id IN: listTaskId]) {           
            lstTasks.add(objtask);
            if (String.valueOf(objTask.WhatId).startsWith('a1J')){
                setLocId.add(objtask.WhatId);
            }
        }
        
        /*for (Location__c objLoc : [Select Id, Finance_Confirmation_Complete__c 
                                    From Location__c
                                    Where Id IN: setLocId]) {
            mapLocIdRecord.put( objLoc.id, objLoc);
        }*/
        for (Task objtask : [Select Id, WhatId, Subject, Status From Task where WhatId IN:setLocId and Status = 'Completed']){
            if (mapLocIdTasks.containsKey(objtask.WhatId)){
                set<String> lstTask = mapLocIdTasks.get(objtask.WhatId);
                lstTask.add(objtask.Subject);
                mapLocIdTasks.put(objtask.WhatId, lstTask);
            } else {
                mapLocIdTasks.put(objtask.WhatId, new set<String> {objtask.Subject});
            }
        }
        list<Task> lstTask = new list<Task>();
        for (Task objTask : lstTasks) {
            set<String> lstRelatedTasks = mapLocIdTasks.get(objTask.WhatId);
            if (lstRelatedTasks.contains('Confirm Libor Delay applied Handover') && lstRelatedTasks.contains('Confirm penalties posted - Handover')
                && lstRelatedTasks.contains('Confirm Area Variation posted - Handover') && lstRelatedTasks.contains('Confirm Recharges Posted - Handover')
                && lstRelatedTasks.contains('Confirm final invoice generated - Handover') && lstRelatedTasks.contains('Confirm service charges posted - Handover')) {
                    Location__c objLocation = new Location__c(Id = objTask.WhatId); 
                    objLocation.Finance_Confirmation_Complete__c = True;
                    lstLocation.add(objLocation);
            }
        }
        System.debug('lstLocation:::'+lstLocation);
        System.debug('lstLocationsize():::'+lstLocation.size());
        if (lstLocation != null && lstLocation.size() > 0) {
            update lstLocation;
        }  
    }
    
}