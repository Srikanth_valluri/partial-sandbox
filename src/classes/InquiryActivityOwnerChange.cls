/****************************************************************************************************
* Name          : InquiryActivityOwnerChange  
* Test Class    : InquiryTriggerHandler_NewTest
* Description   : Disbling the automatic transfer of Activity Owner when Inquiry Owner changes
* Created Date  : 20-05-2020
* Created By    : QBurst
* --------------------------------------------------------------------------------------------------
* VER   AUTHOR              DATE        COMMENTS
* 1.0  QBurst           20-05-2020    Initial Draft.
****************************************************************************************************/
public without sharing class InquiryActivityOwnerChange {
    public static Map<Id, Id> originalActivityOwnerIds = new Map<Id, Id>();
    public static Void setOwnerIds(Map<Id, Inquiry__c> oldMap, Map<Id, Inquiry__c> newMap) {
        List<Id> changedInquiries = new List<Id>();
        for(Id hhId : newMap.keySet()) {
            if(oldMap.get(hhId).OwnerId != newMap.get(hhId).OwnerId) {
                changedInquiries.add(hhId);
            }
        }
        for(Task tsk : [Select Id, WhatId, Subject, OwnerId, Owner.Name from Task where WhatId in :changedInquiries]) {
            originalActivityOwnerIds.put(tsk.Id, tsk.OwnerId);
        }
    }

    public static Void updateOwnerIds() {
        if(originalActivityOwnerIds.isEmpty()){
            return;
        }
        Map<Id, User> activeUsers = new Map<Id, User>([SELECT Id, Name FROM User 
                                                        WHERE IsActive = true AND Id IN: originalActivityOwnerIds.values()]);
        List<Task> tasksToUpdate = new List<Task>();
        for(Task tsk : [SELECT Id, Subject, OwnerId, Owner.Name FROM Task WHERE Id IN : originalActivityOwnerIds.keySet()]) {
            if(originalActivityOwnerIds.get(tsk.Id) != tsk.OwnerId && activeUsers.keySet().contains(originalActivityOwnerIds.get(tsk.Id))) {
                tsk.OwnerId = originalActivityOwnerIds.get(tsk.Id);
                tasksToUpdate.add(tsk);
            }
        }
        if(tasksToUpdate.size() > 0) {
            update tasksToUpdate;
        }
    }
}