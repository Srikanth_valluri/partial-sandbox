/**
 * @File Name          : DPInvoicePaymentCallCoverLetterResponse.cls
 * @Description        : Invoked from Attachment Trigger to populate cover letter field.
 * @Author             : Dipika Rajput
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/27/2019, 3:16:56 PM
 * @Modification Log   : 
 *==============================================================================================
 * Ver         Date                     Author                    Modification
 * 
 *==============================================================================================
 * 1.0    6/25/2019                   Dipika Rajput           Initial Version
**/
public without sharing class DPInvoicePaymentCallCoverLetterResponse { 
    
    // This method is used to fetch attachment records.
    public static void getAllAttachmentRecordsToPopulateCoverLetter( List<Attachment> lstNewAttachment ){ 
        set<Id> setDPIId = new set<Id>();
        set<Id> setAttachId = new set<Id>();
        
        if( lstNewAttachment != NULL && lstNewAttachment.size() > 0 ) {
            for(Attachment objAttachment : lstNewAttachment){
                system.debug('objAttachment.Name*********'+objAttachment.Name);
                system.debug('objAttachment.ParentId*********'+objAttachment.ParentId);
                if(String.valueof(objAttachment.Name).startsWith(Label.Payment_Call_Cover_Letter)
                   && String.ValueOf(objAttachment.ParentId).startsWith(keyPrefix('DP_Invoices__c'))){
                    setDPIId.add(objAttachment.ParentId);
                    setAttachId.add(objAttachment.Id);
                }
            }   
        }  
        
        if( setDPIId != NULL && setDPIId.size() > 0 ) {
           populateCoverLetterOnDpInvoices( setDPIId , setAttachId );
        }
    }

    // Get Prefix of Object.
    public Static String keyPrefix( String strObjName ) {
        if( String.isBlank(strObjName) ) {
            return null;
        }
        Map<String, Schema.SObjectType> m  = Schema.getGlobalDescribe();
        system.debug('==>m is==>'+m);
        Schema.SObjectType s = m.get(strObjName) ;
        system.debug('==>Sobject Type is ==>'+s);
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        String keyPrefix = r.getKeyPrefix();
        return keyPrefix;
    }

    // This Method is used to populate cover letter url on DP Invoice records.
    @future(Callout=true)
    public static void populateCoverLetterOnDpInvoices( set<Id> setDPIId , set<Id> attachId ) {
        List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest;
        if( setDPIId  != null && setDPIId.size() > 0 ) {
            List<DP_Invoices__c> lstDPInvoiceIds = [SELECT Id, 
                                                            BookingUnits__r.Registration_Id__c, 
                                                            BookingUnits__c 
                                                       FROM DP_Invoices__c 
                                                      WHERE Id 
                                                        IN :setDPIId];      
            
            List<Attachment> lstAttachments = [SELECT Id,
                                                      Body,
                                                      Name,
                                                      ParentId 
                                                  FROM Attachment 
                                                  WHERE ParentId IN: setDPIId 
                                                  AND Id IN: attachId 
                                                  AND Name LIKE :Label.Payment_Call_Cover_Letter+'%' 
                                                  ORDER BY  CreatedDate DESC 
                                                  LIMIT 1];
                                        
            if( lstAttachments != null && lstAttachments.size() > 0 ) {
                
                lstMultipleDocRequest = new  List<UploadMultipleDocController.MultipleDocRequest>();
               
                integer intIncrementor = 0;
                
                    if( lstAttachments[0].Body != null 
                        && String.isNotBlank( lstAttachments[0].Name )) {
                        UploadMultipleDocController.MultipleDocRequest objDocRequest = 
                            new UploadMultipleDocController.MultipleDocRequest();

                        objDocRequest.category =  'Document';
                        objDocRequest.entityName = 'Damac Service Requests'; 
                        objDocRequest.fileDescription  =  Label.Payment_Call_Cover_Letter;
                        
                        intIncrementor++;
                        
                        String stType = extractType(lstAttachments[0].Name);
                        String coverLetterAttachmentName = extractName(lstAttachments[0].Name);
                        coverLetterAttachmentName = coverLetterAttachmentName.replaceAll(' ','_');
                        coverLetterAttachmentName = coverLetterAttachmentName.replaceAll('/','_');
                        system.debug('coverLetterAttachmentName************'+coverLetterAttachmentName);                                
                        
                        objDocRequest.fileId = lstDPInvoiceIds[0].Id+
                                                '-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+
                                                '.'+stType;
                        objDocRequest.fileName = lstDPInvoiceIds[0].Id+
                                                 '-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+
                                                 '.'+stType;
                        
                        System.debug('---System.currentTimeMillis()--'+System.currentTimeMillis());
                        System.debug('---intIncrementor--'+intIncrementor);
                        System.debug('---objDocRequest.fileName--'+objDocRequest.fileName);
                        
                        if(lstDPInvoiceIds[0].BookingUnits__c != NULL && 
                            lstDPInvoiceIds[0].BookingUnits__r.Registration_Id__c != NULL){
                               objDocRequest.registrationId =  lstDPInvoiceIds[0].BookingUnits__r.Registration_Id__c;
                               objDocRequest.sourceFileName =  'IPMS-'+lstDPInvoiceIds[0].BookingUnits__r.Registration_Id__c+
                                                                '-'+coverLetterAttachmentName+'.'+stType;
                                    objDocRequest.sourceId  =  'IPMS-'+lstDPInvoiceIds[0].BookingUnits__r.Registration_Id__c+
                                                                '-'+coverLetterAttachmentName;
                        }
                                                
                        if( lstAttachments[0].Body != NULL ){
                            blob objBlob = lstAttachments[0].Body;
                            if(objBlob != NULL){
                                objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
                            }
                        }

                        //objDocRequest.base64Binary  =  crfAttachmentBody;
                        lstMultipleDocRequest.add(objDocRequest);
                    }
                }
            system.debug('lstMultipleDocRequest : ' +lstMultipleDocRequest );

            if( lstMultipleDocRequest != NULL && lstMultipleDocRequest.Size() > 0  ) {
                UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();
                if( !Test.isRunningTest() ) { 
                    MultipleDocData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocRequest);
                }
                else {
                    List<UploadMultipleDocController.MultipleDocResponse> data = 
                        new List<UploadMultipleDocController.MultipleDocResponse>();
                    UploadMultipleDocController.MultipleDocResponse objMultipleDocResponse = 
                        new UploadMultipleDocController.MultipleDocResponse();
                    objMultipleDocResponse.PROC_STATUS = 'S';
                    objMultipleDocResponse.url = 'www.google.com';
                    data.add(objMultipleDocResponse);
                    
                    MultipleDocData.Data = data;
                }
                System.debug('--MultipleDocData--'+MultipleDocData);
                if( MultipleDocData != null && MultipleDocData.status == 'Exception' ) {
                    system.debug('exception====MultipleDocData.message'+MultipleDocData.message);
                        errorLogger(MultipleDocData.message,'',lstDPInvoiceIds[0].Id);
                }                                   
                if(MultipleDocData != null && (MultipleDocData.Data == null || MultipleDocData.Data.Size() == 0) ) {
                    //for( Case objCase : lstCase) {
                        system.debug('====MultipleDocData.message'+MultipleDocData.message);
                        errorLogger(MultipleDocData.message,'',lstDPInvoiceIds[0].Id);
                    //}                           
                }

                map <id,string> mapAttachURLwithDPInvoice = new map <id,string>();
                if( MultipleDocData != null ) {
                    for( Integer objInt = 0; objInt < lstAttachments.Size(); objInt++ ) {
                        UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[objInt];
                        if( objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url) ) {
                            system.debug('objData.url===='+objData.url);
                            mapAttachURLwithDPInvoice.put(lstAttachments[objInt].parentId,objData.url);
                            //lstAttachments[objInt].Attachment_URL__c = objData.url;
                        }
                    }
                    system.debug('mapAttachURLwithDPInvoice==: ' +mapAttachURLwithDPInvoice);
                    list<DP_Invoices__c> lstDPInvoices = new list<DP_Invoices__c>();
                    if(mapAttachURLwithDPInvoice != NULL){
                        lstDPInvoices = [SELECT id,
                                                Cover_Letter__c 
                                           FROM DP_Invoices__c 
                                          WHERE id IN: mapAttachURLwithDPInvoice.keyset()];
                    }
                    system.debug('lstDPInvoices : ' +lstDPInvoices );
                    list<DP_Invoices__c> lstDPInvoicesToUpdate = new list<DP_Invoices__c>();

                    for(DP_Invoices__c objDPI: lstDPInvoices){
                        if(mapAttachURLwithDPInvoice.containsKey(objDPI.id)){
                            system.debug('mapAttachURLwithDPInvoice.get(objDPI.id)===: ' +mapAttachURLwithDPInvoice.get(objDPI.id));
                            objDPI.Cover_Letter__c = mapAttachURLwithDPInvoice.get(objDPI.id);
                        }
                        lstDPInvoicesToUpdate.add(objDPI);
                    }
                    system.debug('lstDPInvoicesToUpdate: ' +lstDPInvoicesToUpdate);
                    system.debug('lstAttachments : ' +lstAttachments );
                    if( lstDPInvoicesToUpdate.Size() > 0 ){
                        update lstDPInvoicesToUpdate;
                        //upsert lstAttachments;
                        //sendEmailOnDocumentGeneration(lstDPInvoiceIds );
                    }  
                }
            }
        }
    } 
    
    private static void errorLogger(string strErrorMessage, string strCaseID,string strBookingUnitID){
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        
        if(String.isNotBlank(strBookingUnitID)){
            objError.Booking_unit__c = strBookingUnitID;
        }
        insert objError;
    } 

    @TestVisible
    private static String extractName(String strName) {
        return strName.substring(strName.lastIndexOf('\\') + 1);
    }
    @TestVisible
    private static String extractType( String strName ) {
        strName = strName.substring( strName.lastIndexOf('\\')+1 );
        return strName.substring( strName.lastIndexOf('.')+1 ) ;
    }   
}