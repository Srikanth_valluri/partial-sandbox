/*
Test Class : FmTaskInvocableClassTest 
*/
public without sharing class GenerateIpmsBeans{
    /*
    Send Tasks and map of FM Case Id and Case
    */
    public void mapIpmsBeans(list<Task> lstTask, map<Id,FM_Case__c> mapId_FmCase){
        set<Id> setFMCaseIds = new set<Id>();
        for(FM_Case__c objFM : mapId_FmCase.values()){
            if(!String.isBlank(objFM.Request_Type__c)
            && objFM.Request_Type__c.equalsIgnoreCase('Proof Of Payment')){
                setFMCaseIds.add(objFM.Id);
            }
        }
        list<SR_Booking_Unit__c> lstSR = new list<SR_Booking_Unit__c>();
        if(!setFMCaseIds.isEmpty()){
            lstSR = FM_Utility.fetchSrBookings(setFMCaseIds);
        }
        system.debug('Method called********');
        list<Error_Log__c> listErrors = new list<Error_Log__c>();
        list<Task> lstTaskToUpdate = new list<Task>();
        for(Task objT : lstTask){
            
            system.debug('Inside FOR for processing********');
            list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5> regTerms = new list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5>();
            // create HEADER bean
            TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objHeaderBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
            objHeaderBean.PARAM_ID = mapId_FmCase.get(objT.WhatId).Name;
            objHeaderBean.ATTRIBUTE1 = 'HEADER';
            objHeaderBean.ATTRIBUTE2 = objT.Process_Name__c;
            objHeaderBean.ATTRIBUTE3 = mapId_FmCase.get(objT.WhatId).Status__c;
            objHeaderBean.ATTRIBUTE4 = objT.Owner.Name;
            objHeaderBean.ATTRIBUTE5 = mapId_FmCase.get(objT.WhatId).Account__r.Party_Id__c;
            objHeaderBean.ATTRIBUTE6 = mapId_FmCase.get(objT.WhatId).Booking_Unit__r.Registration_ID__c;
            objHeaderBean.ATTRIBUTE7 = String.valueOf(objT.CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
            objHeaderBean.ATTRIBUTE8 = '';
            objHeaderBean.ATTRIBUTE9 = '';
            createTaskBean10_24(objHeaderBean,'',objT.WhatId,'','','','','','','','','','','','','');
            createTaskBean25_39(objHeaderBean,'','','','','','','','','','','','','','','');
            regTerms.add(objHeaderBean);
            
            // create TASK bean
            TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objTaskBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
            objTaskBean.PARAM_ID = mapId_FmCase.get(objT.WhatId).Name;
            objTaskBean.ATTRIBUTE1 = 'TASK';
            objTaskBean.ATTRIBUTE2 = objT.Subject;
            objTaskBean.ATTRIBUTE3 = objT.Status;
            objTaskBean.ATTRIBUTE4 = objT.IPMS_Role__c;
            objTaskBean.ATTRIBUTE5 = '';
            objTaskBean.ATTRIBUTE6 = '';
            objTaskBean.ATTRIBUTE7 = String.valueOf(objT.CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
            objTaskBean.ATTRIBUTE8 = objT.Id;
            Datetime dt = objT.ActivityDate;
            objTaskBean.ATTRIBUTE9 = String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase());
            if(Label.POP_Task_Subject_FM_Finance == objT.Subject){ createTaskBean10_24(objTaskBean
                                    , ''
                                    , mapId_FmCase.get(objT.WhatId).Account__r.Party_Id__c
                                    , mapId_FmCase.get(objT.WhatId).Booking_Unit__r.Registration_ID__c
                                    , mapId_FmCase.get(objT.WhatId).Booking_Unit__r.Unit_Name__c
                                    , objT.Description
                                    , objT.Assigned_User__c
                                    , string.valueOf(mapId_FmCase.get(objT.WhatId).Payment_Date__c)
                                    , mapId_FmCase.get(objT.WhatId).Payment_Mode__c
                                    , mapId_FmCase.get(objT.WhatId).Payment_Allocation_Details__c
                                    , mapId_FmCase.get(objT.WhatId).Currency__c 
                                    , string.valueOf(mapId_FmCase.get(objT.WhatId).Total_Amount__c)
                                    , mapId_FmCase.get(objT.WhatId).Sender_Name__c
                                    , mapId_FmCase.get(objT.WhatId).Bank_Name__c
                                    , mapId_FmCase.get(objT.WhatId).Swift_Code__c
                                    , '');
            }else{
                createTaskBean10_24(objTaskBean
                                    , '', mapId_FmCase.get(objT.WhatId).Account__r.Party_Id__c
                                    , mapId_FmCase.get(objT.WhatId).Booking_Unit__r.Registration_ID__c
                                    , mapId_FmCase.get(objT.WhatId).Booking_Unit__r.Unit_Name__c, objT.Description
                                    , objT.Assigned_User__c, '', '', '', '', ''
                                    , '', '', '', '');
            }
            createTaskBean25_39(objTaskBean,'','','','','','','','','','','','','','','');
            createTaskBean41_50(objTaskBean,'','','',''
                                , mapId_FmCase.get(objT.WhatId).Additional_Doc_File_URL__c
                                ,'','','','','');
            regTerms.add(objTaskBean);
            
            if(Label.POP_Task_Subject_FM_Finance == objT.Subject){ for(SR_Booking_Unit__c objBU : lstSR){
                    TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objUnitBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
                    objUnitBean.PARAM_ID = mapId_FmCase.get(objT.WhatId).Name;
                    objUnitBean.ATTRIBUTE1 = 'UNITS';
                    objUnitBean.ATTRIBUTE2 = objT.Process_Name__c;
                    objUnitBean.ATTRIBUTE3 = '';
                    objUnitBean.ATTRIBUTE4 = '';
                    objUnitBean.ATTRIBUTE5 = '';
                    objUnitBean.ATTRIBUTE6 = objBU.Booking_Unit__r.Registration_ID__c;
                    objUnitBean.ATTRIBUTE7 = '';
                    objUnitBean.ATTRIBUTE8 = '';
                    objUnitBean.ATTRIBUTE9 = '';
                    createTaskBean10_24(objUnitBean,'','','','','','','','','','','','','','','');
                    createTaskBean25_39(objUnitBean,'','','','','','','','','','','','','','','');
                    regTerms.add(objUnitBean);
                }
            }else{
                // create UNIT bean
                TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objUnitBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
                objUnitBean.PARAM_ID = mapId_FmCase.get(objT.WhatId).Name;
                objUnitBean.ATTRIBUTE1 = 'UNITS';
                objUnitBean.ATTRIBUTE2 = objT.Process_Name__c;
                objUnitBean.ATTRIBUTE3 = '';
                objUnitBean.ATTRIBUTE4 = '';
                objUnitBean.ATTRIBUTE5 = '';
                objUnitBean.ATTRIBUTE6 = mapId_FmCase.get(objT.WhatId).Booking_Unit__r.Registration_ID__c;
                objUnitBean.ATTRIBUTE7 = '';
                objUnitBean.ATTRIBUTE8 = '';
                objUnitBean.ATTRIBUTE9 = '';
                createTaskBean10_24(objUnitBean,'','','','','','','','','','','','','','','');
                createTaskBean25_39(objUnitBean,'','','','','','','','','','','','','','','');
                regTerms.add(objUnitBean);
            }
            
            if(Label.Push_Task_to_IPMS_for_EHO_Control_Flag == 'Y' && mapId_FmCase.get(objT.WhatId).Unit_Name__c !=NULL && mapId_FmCase.get(objT.WhatId).Unit_Name__c.startsWithIgnoreCase('DTPC')){
                system.debug('-->> ala itthe: ');
                objT.Status = 'Closed';
                lstTaskToUpdate.add(objT);
            } else {
                FM_PushTaskToIPMS objClass = new FM_PushTaskToIPMS();
                String response = objClass.pushTaskToIpms(regTerms);
                system.debug('response*****'+response);if(response.equalsIgnoreCase('Success')){objT.Pushed_To_IPMS__c = true;
                    lstTaskToUpdate.add(objT); }else{
                    Error_Log__c objErr = FM_PushTaskToIPMS.createErrorLogRecord(mapId_FmCase.get(objT.WhatId).Account__c
                                                    ,mapId_FmCase.get(objT.WhatId).Booking_Unit__c
                                                    ,objT.WhatId);
                    objErr.Error_Details__c = response;
                    objErr.Process_Name__c = objT.Process_Name__c;
                    listErrors.add(objErr);
                }
            }
            
        } // end of for loop
        if(!listErrors.isEmpty()){ insert listErrors;}
        if(!lstTaskToUpdate.isEmpty()){
            update lstTaskToUpdate;
        }
    } // end of mapIpmsBeans
    
    public void createTaskBean10_24(TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objTBean,
                                    String a10, String a11, String a12,
                                    String a13, String a14, String a15,
                                    String a16, String a17, String a18,
                                    String a19, String a20, String a21,
                                    String a22, String a23, String a24){
        objTBean.ATTRIBUTE10 = a10;
        objTBean.ATTRIBUTE11 = a11;
        objTBean.ATTRIBUTE12 = a12;
        objTBean.ATTRIBUTE13 = a13;
        objTBean.ATTRIBUTE14 = a14;
        objTBean.ATTRIBUTE15 = a15;
        objTBean.ATTRIBUTE16 = a16;
        objTBean.ATTRIBUTE17 = a17;
        objTBean.ATTRIBUTE18 = a18;
        objTBean.ATTRIBUTE19 = a19;
        objTBean.ATTRIBUTE20 = a20;
        objTBean.ATTRIBUTE21 = a21;
        objTBean.ATTRIBUTE22 = a22;
        objTBean.ATTRIBUTE23 = a23;
        objTBean.ATTRIBUTE24 = a24;
    }
    
    public void createTaskBean25_39(TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objTBean,
                                    String a25, String a26, String a27, 
                                    String a28, String a29, String a30, 
                                    String a31, String a32, String a33, 
                                    String a34, String a35, String a36, 
                                    String a37, String a38, String a39){
        objTBean.ATTRIBUTE25 = a25;
        objTBean.ATTRIBUTE26 = a26;
        objTBean.ATTRIBUTE27 = a27;
        objTBean.ATTRIBUTE28 = a28;
        objTBean.ATTRIBUTE29 = a29;
        objTBean.ATTRIBUTE30 = a30;
        objTBean.ATTRIBUTE31 = a31;
        objTBean.ATTRIBUTE32 = a32;
        objTBean.ATTRIBUTE33 = a33;
        objTBean.ATTRIBUTE34 = a34;
        objTBean.ATTRIBUTE35 = a35;
        objTBean.ATTRIBUTE36 = a36;
        objTBean.ATTRIBUTE37 = a37;
        objTBean.ATTRIBUTE38 = a38;
        objTBean.ATTRIBUTE39 = a39;
    }
    
    public void createTaskBean41_50(TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objTBean,
                                    String a41, String a42, String a43, 
                                    String a44, String a45, String a46, 
                                    String a47, String a48, String a49, 
                                    String a50){
        objTBean.ATTRIBUTE41 = a41;
        objTBean.ATTRIBUTE42 = a42;
        objTBean.ATTRIBUTE43 = a43;
        objTBean.ATTRIBUTE44 = a44;
        objTBean.ATTRIBUTE45 = a45;
        objTBean.ATTRIBUTE46 = a46;
        objTBean.ATTRIBUTE47 = a47;
        objTBean.ATTRIBUTE48 = a48;
        objTBean.ATTRIBUTE49 = a49;
        objTBean.ATTRIBUTE50 = a50;
    }
} // end of class