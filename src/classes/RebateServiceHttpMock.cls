@isTest
global class RebateServiceHttpMock implements HttpCalloutMock{
    
    Integer responseNumber;

    global RebateServiceHttpMock(Integer num) {
        responseNumber = num;
    }

    global HttpResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type','application/json');
        String strResponse;

        if(responseNumber == 1) {
            strResponse = '{' +
                                '"OutputParameters": ' +
                                '{' +
                                    '"@xmlns": "http://xmlns.oracle.com/apps/ont/rest/Rebate/xxdc_open_receipt_proc/",'+
                                    '"@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",'+
                                    '"L_OPEN_REC_TBL": '+
                                   ' {'+
                                       ' "L_OPEN_REC_TBL_ITEM":'+
                                        '['+
                                            '{'+
                                                '"REGISTRATION_ID": "106569",'+
                                                '"CASH_RECEIPT_ID": "3206718",'+
                                                '"RECEIPT_NUMBER": "Unionpay/ 6222***8059/ 012345",'+
                                                '"RECEIPT_DATE": "2019-03-12T00:00:00.000+04:00",'+
                                                '"RECEIPT_AMOUNT": "-1277.9"'+
                                            '}'+
                                        ']'+
                                    '},'+
                                    '"X_ERRBUF": "Below Receipts found: ",'+
                                    '"X_ERRCODE": "S"'+
                                    '}'+
                               '}';
            res.setStatusCode(200);
        }
        
        if(responseNumber == 2) {
            strResponse = '{' +
                                '"OutputParameters": ' +
                                '{' +
                                    '"@xmlns": "http://xmlns.oracle.com/apps/ont/rest/Rebate/xxdc_open_receipt_proc/",'+
                                    '"@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",'+
                                    
                                    '"X_ERRBUF": "Error",'+
                                    '"X_ERRCODE": "E"'+
                                    '}'+
                               '}';
            res.setStatusCode(400);
        }

        if(responseNumber == 3) {
            strResponse ='';
            res.setStatusCode(400);
        }
        res.setBody(strResponse );
        return res;
    }
}