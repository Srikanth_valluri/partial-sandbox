@isTest
public class CampaignLeadsController_Test {
    
    static testmethod void m1(){
        CampaignLeadsController obj = new CampaignLeadsController(new ApexPages.StandardController(new Inquiry__c()));
        obj.newInquiry.Email__c = 'c@k2.com';
        obj.newInquiry.First_name__c = 'fn';
        obj.newInquiry.Last_Name__c = 'ln';
        obj.newInquiry.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        obj.newInquiry.Mobile_Phone_Encrypt__c = '124342526';
        obj.newInquiry.Type_of_Property_Interested_in__c = 'Hotel';
        obj.newInquiry.Preferred_Language__c = 'English';
        obj.saveLead();
    }

}