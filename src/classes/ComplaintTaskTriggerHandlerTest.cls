/** 
 * Description: Test class for ComplaintTaskTriggerHandler class
 */

@isTest
private class ComplaintTaskTriggerHandlerTest {

    /**
     * Test method to check if the error is thrown when Complaint Process related Tasks are closed without 
     * filling certain fields on Case like "CRE Analysis", "Complaint Outcome"
     */
    private static testmethod void testValidationOnCaseForCRE() {
        
        // Creation of Case
        Case objCase = new Case();
        objCase.Status = 'NotSubmitted';
        objCase.recordTypeId = getRecordTypeIdForComplaintCase();
        insert objCase;

        // Creation of Task
        Task objTask = new Task();
        objTask.subject = 'New Complaint Request';
        objTask.Assigned_User__c = 'CRE';
        objTask.Process_Name__c = 'Complaint';
        objTask.ActivityDate = System.Today();
        objTask.WhatId = objCase.Id;
        objTask.status = 'In Progress';
        objTask.description = 'Test comments';
        insert objTask;
        
        Test.startTest();
        
            // Mark the task as complete without filling the required Case fields
            try {
                objTask.status = 'Completed';
                update objTask; 
            } catch (Exception objException) {
                Boolean expectedExceptionThrown =  objException.getMessage().contains('Please enter CRE Analysis and Complaint Outcome field values on related case record') ? True : False;
                System.assertEquals(True, expectedExceptionThrown);
            }
        Test.stopTest();
    }

    /**
     * Test method to check if the error is thrown when Complaint Process related Tasks are closed without 
     * filling certain fields on Case like "Manager Analysis", "Complaint Outcome"
     */
    private static testmethod void testValidationOnCaseForCREManager() {
        
        // Creation of Case
        Case objCase = new Case();
        objCase.Status = 'NotSubmitted';
        objCase.recordTypeId = getRecordTypeIdForComplaintCase();
        insert objCase;

        // Creation of Task
        Task objTask = new Task();
        objTask.subject = 'Complaint case not closed after 24 hours';
        objTask.Assigned_User__c = 'CRE Manager';
        objTask.Process_Name__c = 'Complaint';
        objTask.ActivityDate = System.Today();
        objTask.WhatId = objCase.Id;
        objTask.status = 'In Progress';
        objTask.description = 'Test comments';
        insert objTask;
        
        Test.startTest();
        
            // Mark the task as complete without filling the required Case fields
            try {
                objTask.status = 'Completed';
                update objTask; 
            } catch (Exception objException) {
                Boolean expectedExceptionThrown =  objException.getMessage().contains('Please enter Manager Analysis and Complaint Outcome field values on related case record') ? True : False;
                System.assertEquals(True, expectedExceptionThrown);
            }
        Test.stopTest();
    }

    /**
     * Test method to check if the error is thrown when Complaint Process related multiple Tasks are closed without 
     * filling certain fields on Case like "CRE Analysis", "Complaint Outcome". 
     */
    private static testmethod void testMultipleTaskValidationOnCaseForCRE() {
        
        // Creation of Case
        Case objCase = new Case();
        objCase.Status = 'NotSubmitted';
        objCase.recordTypeId = getRecordTypeIdForComplaintCase();
        insert objCase;

        // Creation of Tasks
        List<Task> lstTask = new List<Task>();
        Task objTask = new Task();
        objTask.subject = 'New Complaint Request';
        objTask.Assigned_User__c = 'CRE';
        objTask.Process_Name__c = 'Complaint';
        objTask.ActivityDate = System.Today();
        objTask.WhatId = objCase.Id;
        objTask.status = 'In Progress';
        objTask.description = 'Test comments';
        lstTask.add(objTask);
        
        Task objCRETask = new Task();
        objCRETask.subject = 'New Complaint Request';
        objCRETask.Assigned_User__c = 'CRE';
        objCRETask.Process_Name__c = 'Complaint';
        objCRETask.ActivityDate = System.Today();
        objCRETask.WhatId = objCase.Id;
        objCRETask.status = 'In Progress';
        objCRETask.description = 'Test comments';
        lstTask.add(objCRETask);
        
        insert lstTask;
        
        Test.startTest();
        
            // Mark both the tasks as complete without filling the required Case fields
            try {
                for (Task objTaskInstance : lstTask) {
                    objTaskInstance.status = 'Completed';
                }
                update lstTask;
            } catch (Exception objException) {
                Boolean expectedExceptionThrown =  objException.getMessage().contains('Please enter CRE Analysis and Complaint Outcome field values on related case record') ? True : False;
                System.assertEquals(True, expectedExceptionThrown);
            }
        Test.stopTest();
    }

    /**
     * Test method to check if the error is thrown when Complaint Process related multiple Tasks are closed without 
     * filling certain fields on Case like "Manager Analysis", "Complaint Outcome"
     */
    private static testmethod void testMultipleTaskValidationOnCaseForCREManager() {
        
        // Creation of Case
        Case objCase = new Case();
        objCase.Status = 'NotSubmitted';
        objCase.recordTypeId = getRecordTypeIdForComplaintCase();
        insert objCase;

        // Creation of Tasks
        List<Task> lstTask = new List<Task>();
        Task objTask = new Task();
        objTask.subject = 'Complaint case not closed after 24 hours';
        objTask.Assigned_User__c = 'CRE Manager';
        objTask.Process_Name__c = 'Complaint';
        objTask.ActivityDate = System.Today();
        objTask.WhatId = objCase.Id;
        objTask.status = 'In Progress';
        objTask.description = 'Test comments';
        lstTask.add(objTask);
        
        Task objCRETask = new Task();
        objCRETask.subject = 'Complaint case not closed after 24 hours';
        objCRETask.Assigned_User__c = 'CRE Manager';
        objCRETask.Process_Name__c = 'Complaint';
        objCRETask.ActivityDate = System.Today();
        objCRETask.WhatId = objCase.Id;
        objCRETask.status = 'In Progress';
        objCRETask.description = 'Test comments';
        lstTask.add(objCRETask);
        
        insert lstTask;
        
        Test.startTest();
        
            // Mark both the tasks as complete without filling the required Case fields
            try {
                for (Task objTaskInstance : lstTask) {
                    objTaskInstance.status = 'Completed';
                }
                update lstTask;
            } catch (Exception objException) {
                Boolean expectedExceptionThrown = objException.getMessage().contains('Please enter Manager Analysis and Complaint Outcome field values on related case record') ? True : False;
                System.assertEquals(True, expectedExceptionThrown);
            }
        Test.stopTest();
    }

    /**
     * Method to get the "Complaint" record type
     */
    private static Id getRecordTypeIdForComplaintCase() {
        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id complaintRecordTypeID = caseRecordTypes.get('Complaint').getRecordTypeId();
        return complaintRecordTypeID;
    }
}