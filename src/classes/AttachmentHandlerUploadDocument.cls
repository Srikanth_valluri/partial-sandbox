/*--------------------------------------------------------------------------------------------------------------------------
Description: Attachement trigger helper
============================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 23-01-2019        | Lochana Rajput    | 1. Added handler method to handle FM NOC attachments craeted from drawloop

1.1     | 16-09-2019        | Aishwarya Todkar  | 1.Generated documents for 'Riyadh Rotana Conversion' SR Process.

1.2     | 01-10-2019        | Aishwarya Todkar  | 1.Added logic to generate NOC For Visa.

1.3     | 13-10-2019        | Aishwarya Todkar  | 1.Added logic to generate Letter Of Authorization (LOA).

1.4     | 25-12-2019        | Arjun Khatri      | 1.Added logic to generate COCD Letter.

1.5     | 09-01-2020        | Aishwarya Todkar  | 1.Shorten the SR Attachment file names for Riyadh Process

1.6     | 03-02-2020        | Aishwarya Todkar  | 1.Added logic to generate Pacifica Villa documents.

1.7     | 02-03-2020        | Aishwarya Todkar  | 1.Added logic to generate UMA,POA/LOA documents

1.8     | 18-03-2020        | Aishwarya Todkar  | 1.Added logic to generate ADVANCE PAYMENT REBATE LETTER  document

1.9     | 18-05-2020        | Aishwarya Todkar  | 1.Added logic to generate KYC and Third party Form  document

1.10    | 26-08-2020        | Aishwarya Todkar  | 1.Added logic to generate Handover checklist

1.11    | 01-09-2020        | Neha Dave         | 1.Restricted creation of UHR document for Handover checklist (sent)
=============================================================================================================================
*/
public class AttachmentHandlerUploadDocument {
	/*public static void assignAttachmentAdminUser(List<Attachment> newlstAttachment){
		Map<Id, Case> mapIdCase = new Map<Id, Case>();
		set<Id> setCaseIds = new set<Id>();
		set<Id> setCaseRTId = new set<Id>();
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId());
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Assignment').getRecordTypeId());

		system.debug('>>>>newlstAttachment : '+newlstAttachment.size());
		for(Attachment objAttachment : newlstAttachment){
			system.debug('objAttachment.Name*********'+objAttachment.Name);
			system.debug('objAttachment.ParentId*********'+objAttachment.ParentId);
			if(String.valueof(objAttachment.Name).startsWith('NOC Document')
			   && String.ValueOf(objAttachment.ParentId).startsWith('500')){
				setCaseIds.add(objAttachment.ParentId);
			}
		}

		if(setCaseIds != null && setCaseRTId != null){
			mapIdCase = fetchCaseDocument(setCaseIds, setCaseRTId);
		}
		system.debug('mapIdCase : '+mapIdCase);

		for(Attachment objAttachment : newlstAttachment){
			if(String.valueof(objAttachment.Name).startsWith('NOC Document')
			   && mapIdCase.containsKey(objAttachment.ParentId)){
			   system.debug('>>>>>IN IF');
				objAttachment.OwnerId= Label.DefaultCaseOwnerId;
				//objAttachment.NOC_Processing_Pending__c = false;
			}
		}*/

		/*List<Case> lstCaseUpdate = new List<Case>();

		for(Case objCase : mapIdCase.Values()){
			objCase.OwnerId = Label.DefaultCaseOwnerId;
			lstCaseUpdate.add(objCase);
		}

		if(!lstCaseUpdate.isEmpty()){
			update lstCaseUpdate;
		}*/
	//}

	/************************************************************************
	* @Description : Method to check prefix of an Object.
	* @Params      : Object Name
	* @Return      : Object Id Prefix
	*************************************************************************/
	public Static String keyPrefix( String strObjName ) {
		if( String.isBlank(strObjName) ) {
			return null;
		}
		Map<String, Schema.SObjectType> m  = Schema.getGlobalDescribe() ;
		Schema.SObjectType s = m.get(strObjName) ;
		Schema.DescribeSObjectResult r = s.getDescribe() ;
		String keyPrefix = r.getKeyPrefix();
		return keyPrefix;
	}

	/************************************************************************
	* @Description : Method to create SR attachments.
	* @Params      : List<Attachment>
	* @Return      : None
	*************************************************************************/
	public static void createSRAttachment(List<Attachment> newlstAttachment){
		FM_AttachmentHelper.createFM_SR_Attachment(newlstAttachment);
		set<Id> setCaseIds = new set<Id>();
		set<Id> setAttachmentIds = new set<Id>();
		set<Id> setCaseRTId = new set<Id>();
		Set<Id> setBuyerId = new Set<Id>();
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId());
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId());
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Assignment').getRecordTypeId());
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('AOPT').getRecordTypeId());
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Settlement').getRecordTypeId());
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Parking').getRecordTypeId());
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Agreement').getRecordTypeId());
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId());
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId());
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Plot NOC').getRecordTypeId());
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Overdue Rebate/Discount').getRecordTypeId());
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Riyadh Rotana Conversion').getRecordTypeId());
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId());
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId());
		setCaseRTId.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId());

		Set<String> setRiyadhRotanaDocs = getRiyadhRotanaDocs();
		System.debug( 'setRiyadhRotanaDocs' + setRiyadhRotanaDocs);
		integer intCounter = 1;
		integer flag = 0;
		system.debug('newlstAttachment : '+newlstAttachment.size());
		for(Attachment objAttachment : newlstAttachment){
			if(objAttachment.ParentId != NULL
			   && String.ValueOf(objAttachment.ParentId).startsWith('500')){
				if(intCounter <= 99
				&& (String.valueof(objAttachment.Name).startsWith('NOC Document')
				|| String.valueof(objAttachment.Name).startsWith('NDA Document')
				|| String.valueof(objAttachment.Name).startsWith('CRF Document')
				|| String.valueof(objAttachment.Name).startsWith('Offer and Acceptance Letter')
				|| String.valueof(objAttachment.Name).startsWith('Confirmation Letter')
				|| String.valueof(objAttachment.Name).startsWith('Parking Offer & Acceptance Letter')
				|| String.valueof(objAttachment.Name).startsWith('Parking reservation letter')
				|| String.valueof(objAttachment.Name).startsWith('Rental Pool Addendum')
				|| String.valueof(objAttachment.Name).startsWith('UHR Document')
				|| String.valueof(objAttachment.Name).startsWith('LMA Request')
				|| String.valueof(objAttachment.Name).startsWith('UMA Document')
				|| String.valueof(objAttachment.Name).startsWith('Agreement - Non resident')
				|| String.valueof(objAttachment.Name).startsWith('Agreement with RGS')
				|| String.valueof(objAttachment.Name).startsWith('NOC')
				|| String.valueof(objAttachment.Name).startsWith('Undertaking Letter')
				|| String.valueof(objAttachment.Name).startsWith('PHR Document')
				|| String.valueof(objAttachment.Name).startsWith('OVERDUE AMOUNT REBATE LETTER')
				|| String.valueof(objAttachment.Name).startsWith('Letter Of Authorization')
				|| String.valueof(objAttachment.Name).startsWith('COCDLetter')
				|| String.valueof(objAttachment.Name).startsWith('AreaVariationLetter')
				|| String.valueof(objAttachment.Name).startsWith('Villa Short')
				|| String.valueof(objAttachment.Name).startsWith('Old Villa')
				|| String.valueof(objAttachment.Name).startsWith('Pacifica Villa Type')
				|| String.valueof(objAttachment.Name).startsWith('ADVANCE PAYMENT REBATE LETTER')
				|| String.valueof(objAttachment.Name).startsWith('Cash Payment KYC')
				|| String.valueof(objAttachment.Name).startsWith('3rd party')
				|| String.valueof(objAttachment.Name).startsWith('Handover Checklist')
				|| String.valueof(objAttachment.Name).equalsIgnoreCase('Handover Checklist (Signed).pdf')
				|| String.valueof(objAttachment.Name).equalsIgnoreCase('Handover Checklist (sent).pdf')
				|| setRiyadhRotanaDocs.contains(String.valueof(objAttachment.Name)))
				&& String.ValueOf(objAttachment.ParentId).startsWith('500')){
					setCaseIds.add(objAttachment.ParentId);
					setAttachmentIds.add(objAttachment.Id);
					intCounter++;
					flag = 1;
				}else{
					uploadDocumentCentralRepository(setAttachmentIds, setCaseIds, setCaseRTId);
					setCaseIds = new Set<Id>();
					setAttachmentIds = new Set<Id>();
					intCounter = 1;
					flag = 0;
				}
			}
			else if( objAttachment.ParentId != NULL
			   && String.ValueOf(objAttachment.ParentId).startsWith( keyPrefix( 'Buyer__c' ))) {
				if( objAttachment.Name.startsWith('NOC For Visa') ) {
					setAttachmentIds.add(objAttachment.Id);
					setBuyerId.add( objAttachment.ParentId );
				}
			}
		}

		if(flag == 1){
			uploadDocumentCentralRepository(setAttachmentIds, setCaseIds, setCaseRTId);
			setCaseIds = new Set<Id>();
			setAttachmentIds = new Set<Id>();
			intCounter = 1;
			flag = 0;
		}

		if( !setAttachmentIds.isEmpty() && !setBuyerId.isEmpty() ) {
			uploadDocumentCentralRepository_Buyer( setAttachmentIds, setBuyerId);
		}
	}

	@future(callout=true)
	public static void uploadDocumentCentralRepository_Buyer(Set<Id> setAttachmentIds, Set<Id> setBuyerId) {
		System.debug(' ==== uploadDocumentCentralRepository_Buyer =====');
		System.debug('  setAttachmentIds = ' + setAttachmentIds); 
		System.debug('  setBuyerId = ' + setBuyerId); 
		UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
		List<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new List<UploadMultipleDocController.MultipleDocRequest>();
		//Set<Id> setCaseId = new Set<Id>();
		Map<Id, Id> mapBuyerIdToCaseId = new Map<Id,Id>();

		if( !setBuyerId.isEmpty() ) { 
			for(Buyer__c objBuyer : [SELECT Id
											, Case__c
										FROM
											Buyer__c
										WHERE
											Id =: setBuyerId] ) {
				mapBuyerIdToCaseId.put( objBuyer.Id, objBuyer.Case__c );
			}
			System.debug('  mapBuyerIdToCaseId = ' + mapBuyerIdToCaseId);
			if( !mapBuyerIdToCaseId.isEmpty() ) {
				Map<Id, Case> mapOfCase = new Map<Id, Case>( [SELECT
														Id
														, CaseNumber
														, Registration_ID__c
														, Booking_Unit__c
													FROM
														Case
													WHERE
														Id IN : mapBuyerIdToCaseId.Values() ] );
				System.debug('  mapOfCase = ' + mapOfCase); 
				for(Attachment objAttachment : [Select a.Parent.RecordTypeId,
													a.Parent.Id,
													a.Parent.Type,
													a.ParentId,
													a.OwnerId,
													a.Name,
													a.Id,
													a.BodyLength,
													a.Body
											From Attachment a
											Where a.Id IN: setAttachmentIds
											AND a.ParentId IN :setBuyerId
											AND a.Name LIKE 'NOC For Visa%']) {
					if( !mapOfCase.isEmpty() && !mapBuyerIdToCaseId.isEmpty() 
					&& mapBuyerIdToCaseId.containsKey( objAttachment.ParentId )
					&& mapOfCase.containsKey( mapBuyerIdToCaseId.get( objAttachment.ParentId ) ) ) {
						lstWrapper.add( 
							PenaltyWaiverHelper.makeWrapperObject( 
								EncodingUtil.Base64Encode(objAttachment.Body)
								, objAttachment.Name
								, objAttachment.Name
								, String.valueOF( mapOfCase.get( mapBuyerIdToCaseId.get( objAttachment.ParentId ) ).Registration_ID__c)
								, String.valueOF( mapOfCase.get( mapBuyerIdToCaseId.get( objAttachment.ParentId ) ).CaseNumber)
								, '1' ));
					}
				}
				if( !lstWrapper.isEmpty() ) {
					try {
						objResponse = UploadMultipleDocController.getMultipleDocUrl(lstWrapper);
						System.debug( 'objResponse ==' + objResponse);
						/*if (objResponse != null && objResponse.status == 'Exception') {
							ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, objResponse.message));
							errorLogger(objResponse.message, caseRecord.ID, selectedUnit);
						}
						if (objResponse != null && (objResponse.Data == null || objResponse.Data.Size() == 0)) {
							ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, objResponse.message));
						}*/
						Map<String, String> mapCaseNumber_URL = new Map<String, String>();
						if (objResponse != null 
						&& objResponse.data != null 
						&& objResponse.Data.Size() > 0 
						&& objResponse.status != 'Exception')  {

							for (UploadMultipleDocController.MultipleDocResponse objData: objResponse.Data) {

								if (objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url) ) {

									String strPROCMESSAGE = objData.PROC_MESSAGE;

									List<String> strSplit = strPROCMESSAGE.trim().split(':');

									List<String> strNewlst = strSplit[1].split('-');

									String strCaseNumber = strNewlst[0].replace('[','').trim();

									system.debug('strCaseNumber : '+ strCaseNumber);

									mapCaseNumber_URL.put(strCaseNumber, String.ValueOf(objData.url));

									//mapURLS.put(pdfurl.substring( pdfurl.lastIndexOf('/')+1 ),pdfurl);
								} else {
									//ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'error... ' + objData.PARAM_ID));
								}
							}// END objResponse for loop
							System.debug( 'mapCaseNumber_URL ==' + mapCaseNumber_URL);
						}// END objResponse if
						List<SR_Attachments__c> lstNocSRAttach = new List<SR_Attachments__c>();
						Map<Id, List<SR_Attachments__c>> mapCaseToNoc = new Map<Id, List<SR_Attachments__c>>();
						if( !mapCaseNumber_URL.isEmpty() && ! mapOfCase.isEmpty() ) {
							for( Case objCase : mapOfCase.Values() ) {
								if ( mapCaseNumber_URL.containsKey( objCase.CaseNumber ) ) {
									//Error_Log__c objErr = createErrorLogRecord(caseRecord.Seller__c, caseRecord.Booking_Unit__c, caseRecord.Id);
									//objErr.Error_Details__c = 'Error : No Response from IPMS for Document Generation for LOA Completed Template';
									// insertErrorLog(objErr);

									SR_Attachments__c srAttach = new SR_Attachments__c();
									//srAttach.Description__c = varBuyer.First_Name__c + varBuyer.Last_Name__c;
									srAttach.Case__c = objCase.Id;

									srAttach.Type__c = 'NOC For VISA';
									srAttach.isValid__c = true;
									srAttach.Name = 'NOC For Visa-' + objCase.Registration_ID__c;
									srAttach.Attachment_URL__c = mapCaseNumber_URL.get( objCase.CaseNumber );
									lstNocSRAttach.add(srAttach);

									if(mapCaseToNoc.containsKey( objCase.Id )) {
										mapCaseToNoc.get( objCase.Id ).add( srAttach );
									}
									else {
										mapCaseToNoc.put( objCase.Id, new List<SR_Attachments__c> { srAttach } );
									}
								}
							}// END mapOfCase for loop
							System.debug( 'mapCaseToNoc ==' + mapCaseToNoc);
							System.debug( 'lstNocSRAttach ==' + lstNocSRAttach);
							if( !lstNocSRAttach.isEmpty()) {
								insert lstNocSRAttach;
							}

							//Marking NOC details on Case and BU
							List<Case> lstCasesToUpdate = new List<Case>();
							List<Booking_Unit__c> lstBuToUpdate = new List<Booking_Unit__c>();
							for( Id caseId : mapCaseToNoc.keySet()) {
								lstCasesToUpdate.add(new Case( Id = caseId
								, Is_NOC_For_Visa_Generated__c = true ));

								lstBuToUpdate.add( new Booking_Unit__c( Id = mapOfCase.get( caseId ).Booking_Unit__c
																		, Number_Of_NOC__c = mapCaseToNoc.get( caseId ).size() ) );
							}
							System.debug( 'lstCasesToUpdate ==' + lstCasesToUpdate);
							System.debug( 'lstBuToUpdate ==' + lstBuToUpdate);

							if( !lstCasesToUpdate.isEmpty()) {
								update lstCasesToUpdate;
							}
							if( !lstBuToUpdate.isEmpty()) {
								update lstBuToUpdate;
							}
						} // End mapCaseNumber_URL If
					}//End try
					catch( Exception ex) {
						System.debug( 'Exception ====* ' + ex.getMessage() );
					}
				}//END lstWrapper
			}//END mapBuyerIdToCaseId
		}// END setBuyerId
	}

	/*Description : uploadDocumentCentralRepository is used to upload the document that is being attached on Attachment*/
	@future(callout=true)
	public static void uploadDocumentCentralRepository(Set<Id> setAttachmentIds, Set<Id> setCaseIds, Set<Id> setCaseRTId){
		Map<Id, Case> mapIdCase = new Map<Id, Case>();
		Boolean isOfferAcceptanceLetter = false;
		Boolean isParkingReservationLetter = false;
		Boolean isLMARequest = false;
		Boolean isUMA = false;
		Boolean isPOA = false;
		Boolean isRGSAgreement = false;
		Boolean isLOA = false;
		Boolean isVillaShort = false;
		Boolean isVillaOld = false;
		Boolean isVillaType = false;
		Boolean isKYC = false;
		Boolean isThirdParty = false;
		Boolean isHoChecklist = false;
		Boolean RRC1 = false;
		Boolean RRC2  = false;
		Boolean RRC3  = false;
		Boolean RRC4  = false;
		Boolean RRC5  = false;
		Boolean RRC6  = false;
		Boolean RRC7  = false;
		Boolean RRC8  = false;
		Boolean RRC9  = false;
		Boolean RRC10  = false;
		Boolean isAreaVariationLetter  = false;

		String      strRRC1 = 'Addendum to the Sale and Purchase Agreement (P)';
		String      strRRC2 = 'Addendum to the Sale and Purchase Agreement (F)';
		String      strRRC3 = 'Addendum to the Agreement to Sell (P)';
		String      strRRC4 = 'Addendum to the Agreement to Sell (F)';
		String      strRRC5 = 'Long Term Lease';
		String      strRRC6 = 'Letter of Discharge and Adherence';
		String      strRRC7 = 'Power of Attorney for Hotel Operation';
		String      strRRC8 = 'Power of Attorney for Constitution of Owners Association (OA)';
		String      strRRC9 = 'Deed of Adherence';
		String      strRRC10 = 'JOPD Document';

		Set<String> setRiyadhRotanaDocs = getRiyadhRotanaDocs(); //AT
		if(setCaseIds != null && setCaseRTId != null){
			system.debug('!!!!!setCaseIds'+setCaseIds);
			system.debug('!!!!!setCaseRTId'+setCaseRTId);
			mapIdCase = fetchCaseDocument(setCaseIds, setCaseRTId);
        }
        
        system.debug('mapIdCase : '+mapIdCase);

		List<Attachment> lstAttachment = new List<Attachment>();
		List<Case> lstCaseUpdate = new List<Case>();

		if(setAttachmentIds != null && setCaseRTId != null){
			system.debug('!!!!!setAttachmentIds'+setAttachmentIds);
			system.debug('!!!!!setCaseRTId'+setCaseRTId);
			lstAttachment = QueryAttachment(setAttachmentIds, setCaseRTId);
			system.debug('>>>>>lstAttachment'+lstAttachment);
		}

		isOfferAcceptanceLetter = lstAttachment.size() > 0 && lstAttachment[0].Name.startsWith('Parking Offer') ? true : false;
		isParkingReservationLetter = lstAttachment.size() > 0 && lstAttachment[0].Name.startsWith('Parking reservation letter') ? true : false;
		isKYC = lstAttachment.size() > 0 && lstAttachment[0].Name.startsWith('Cash Payment KYC') ? true : false;
		isThirdParty = lstAttachment.size() > 0 && lstAttachment[0].Name.startsWith('3rd party') ? true : false;
		
		UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
		String attachmentName;
		list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
		if(lstAttachment != null && !lstAttachment.isEmpty()){
			for(Attachment objAttachment : lstAttachment){

				system.debug('!!!!objAttachment.Name'+objAttachment.Name);
				lstWrapper.add( PenaltyWaiverHelper.makeWrapperObject( EncodingUtil.Base64Encode(objAttachment.Body),
																	  objAttachment.Name.startsWith('Offer') ? 'Offer and Acceptance Letter' : 
																	  objAttachment.Name.startsWith('Confirmation') ? 'Confirmation Letter' : 
																	  objAttachment.Name.startsWith('Parking Offer') ? 'Parking Offer and Acceptance Letter' :
																	  objAttachment.Name.startsWith('Parking reservation') ? 'Parking reservation letter' :
																	  objAttachment.Name.startsWith('Rental Pool') ? 'Rental Pool Addendum' : 
																	  objAttachment.Name.startsWith('OVERDUE AMOUNT REBATE LETTER')? 'OVERDUE AMOUNT REBATE LETTER' : 
																	  objAttachment.Name.startsWith('Letter Of Authorization') ? 'Letter Of Authorization' :
																	  objAttachment.Name.startsWith('COCDLetter') ? 'COCDLetter' :
																	  objAttachment.Name.startsWith('AreaVariationLetter') ? 'AreaVariationLetter' :
																	  objAttachment.Name.startsWith('Villa Short') ? 'Villa Short' :
																	  objAttachment.Name.startsWith('Old Villa') ? 'Old Villa' :
																	  objAttachment.Name.startsWith('Pacifica Villa Type') ? 'Pacifica Villa Type' :
																	  objAttachment.Name.startsWith('ADVANCE PAYMENT REBATE LETTER') ? 'ADVANCE PAYMENT REBATE LETTER' :
																	  objAttachment.Name.startsWith('Cash Payment KYC') ? 'Cash Payment KYC' :
																	  objAttachment.Name.startsWith('3rd party') ? '3rd party' :
																	  setRiyadhRotanaDocs.contains( objAttachment.Name ) ? objAttachment.Name : 'NOC Document',
																	  objAttachment.Name ,
																	  String.valueOF(mapIdCase.get(objAttachment.ParentId).Registration_ID__c),
																	  String.valueOf(mapIdCase.get(objAttachment.ParentId).CaseNumber),
																	  '1' ));
				
				if (objAttachment.Name.contains('LMA Request')) {
					isLMARequest = true;
				}
				else if (objAttachment.Name.contains('Agreement with RGS')) {
					isRGSAgreement = true;
				}
				else if( objAttachment.Name.contains( 'Letter Of Authorization' ) ) {
					isLOA = true;
				}
				else if(objAttachment.Name.contains(strRRC1)) {
					RRC1 = true;
				}
				else if(objAttachment.Name.contains(strRRC2)) {
					RRC2 = true;
				}
				else if(objAttachment.Name.contains(strRRC3)) {
					RRC3 = true;
				}
				else if(objAttachment.Name.contains(strRRC4)) {
					RRC4 = true;
				}
				else if(objAttachment.Name.contains(strRRC5)) {
					RRC5 = true;
				}
				else if(objAttachment.Name.contains(strRRC6)) {
					RRC6 = true;
				}
				else if(objAttachment.Name.contains(strRRC7)) {
					RRC7 = true;
				}
				else if(objAttachment.Name.contains(strRRC8)) {
					RRC8 = true;
				}
				else if(objAttachment.Name.contains(strRRC9)) {
					RRC9 = true;
				}
				else if(objAttachment.Name.contains(strRRC10)) {
					RRC10 = true;
				}
				else if( objAttachment.Name.contains( 'Villa Short' )) {
					isVillaShort = true;
				}
				else if( objAttachment.Name.contains( 'Old Villa' )) {
					isVillaOld = true;
				}
				else if( objAttachment.Name.contains( 'Pacifica Villa Type' )) {
					isVillaType = true;
				}
				else if( objAttachment.Name.contains( 'AreaVariationLetter' )) {
					isAreaVariationLetter = true;
				}
				else if (objAttachment.Name.contains('UMA Document') ) {
					isUMA = true;
				}
				else if ( objAttachment.Name.contains('Power Of Attorney')) {
					isPOA = true;
				}else if ( objAttachment.Name.contains('Cash Payment KYC')) {
					isKYC = true;
				}else if ( objAttachment.Name.contains('3rd party')) {
					isThirdParty = true;
                }else if ( objAttachment.Name.equalsIgnoreCase( 'Handover Checklist.pdf' )
                || objAttachment.Name.equalsIgnoreCase( 'Handover Checklist (signed).pdf' )
                || objAttachment.Name.equalsIgnoreCase( 'Handover Checklist (sent).pdf' ) ) {
                    isLOA = false;
                    isHoChecklist = true;
                }
				attachmentName = objAttachment.Name;
			}
			if( !lstWrapper.isEmpty() ) {
				objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper );
				system.debug('>>>objResponse :'+objResponse);
				system.debug('>>>Done with the callout :');
				Map<String, String> mapCaseNumber_URL = new Map<String, String>();
				for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
				system.debug('>>>Done with the objFile.PROC_MESSAGE :'+objFile.PROC_MESSAGE);
					String strPROCMESSAGE = objFile.PROC_MESSAGE;
					List<String> strSplit = strPROCMESSAGE.trim().split(':');
					List<String> strNewlst = strSplit[1].split('-');
					String strCaseNumber = strNewlst[0].replace('[','').trim();
					system.debug('strCaseNumber : '+strCaseNumber);
					mapCaseNumber_URL.put(strCaseNumber,String.ValueOf(objFile.url));
				}
				system.debug('mapCaseNumber_URL : '+mapCaseNumber_URL);
				List<SR_Attachments__c> lstSRAttcahment = new List<SR_Attachments__c>();
				system.debug('>>>mapIdCase : '+mapIdCase);
				if(mapIdCase != null){
					for(Case objCase : mapIdCase.Values()){
						SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
						system.debug('>>objCase.SR_Attachments__r.size() : '+objCase.SR_Attachments__r.size());
						if(objCase.SR_Attachments__r.size() > 0 
						&& !objCase.RecordType.DeveloperName.equals('Parking')
						&& !objCase.RecordType.DeveloperName.equals('Lease_Handover')
						&& !objCase.RecordType.DeveloperName.equals('Plot_Handover')
						&& !objCase.RecordType.DeveloperName.equals('Early_Handover')){
							system.debug('>>objCase.SR_Attachments__r : '+objCase.SR_Attachments__r);
							objCaseAttachment.Id = objCase.SR_Attachments__r[0].Id;
						} 
						else{

							objCaseAttachment.Name = objCase.RecordType.DeveloperName.equals('AOPT') || objCase.RecordType.DeveloperName.equals('Early_Settlement') ? 'Offer and Acceptance Letter'
													: objCase.RecordType.DeveloperName.equals('Parking') && isOfferAcceptanceLetter ? 'Parking Offer & Acceptance Letter'
													: objCase.RecordType.DeveloperName.equals('Parking') && isParkingReservationLetter ? 'Parking reservation letter'
													: objCase.RecordType.DeveloperName.equals('Parking') ? 'Confirmation Letter'
													: objCase.RecordType.DeveloperName.equals('Rental_Pool_Agreement') ? 'Rental Pool Addendum'
													: objCase.RecordType.DeveloperName.equals('Early_Handover') && isHoChecklist ? attachmentName//'Handover Checklist'
													: objCase.RecordType.DeveloperName.equals('Early_Handover') && !isLOA && !isHoChecklist ? 'UHR Document'
													: objCase.RecordType.DeveloperName.equals('Change_of_Details') ? 'COCDLetter'
													: objCase.RecordType.DeveloperName.equals('Early_Handover') && isAreaVariationLetter ? 'Area Variation Letter'
													: objCase.RecordType.DeveloperName.equals('Handover') && isAreaVariationLetter ? 'Area Variation Letter'
													: objCase.RecordType.DeveloperName.equals('Early_Handover') && isLOA ? 'Letter Of Authorization'
													: objCase.RecordType.DeveloperName.equals('Handover') && isLOA ? 'Letter Of Authorization'
													: objCase.RecordType.DeveloperName.equals('Handover') && isVillaShort ? 'Villa Short'
													: objCase.RecordType.DeveloperName.equals('Handover') && isVillaOld ? 'Old Villa'
													: objCase.RecordType.DeveloperName.equals('Handover') && isVillaType ? 'Pacifica Villa Type'
													: objCase.RecordType.DeveloperName.equals('Handover') && isHoChecklist ? attachmentName//'Handover Checklist'
													: objCase.RecordType.DeveloperName.equals('Overdue_Rebate_Discount') ? 'Overdue Rebate Letter'
													: objCase.RecordType.DeveloperName.equals('Rebate_On_Advance') ? 'ADVANCE PAYMENT REBATE LETTER'
													: isKYC ? 'KYC'
													: isThirdParty ? 'Third Party Form'
													: objCase.RecordType.DeveloperName.equals('Plot_Handover') ? attachmentName
													: objCase.RecordType.DeveloperName.equals('Lease_Handover') && isLMARequest == true ? 'LMA Request'
													: objCase.RecordType.DeveloperName.equals('Lease_Handover') 
													&& isLMARequest == false 
													&& isRGSAgreement == false 
													&& !isUMA && !isPOA && !isLOA ? 'Agreement - Non resident'
													: objCase.RecordType.DeveloperName.equals('Lease_Handover') 
													&& isRGSAgreement == true ? 'Agreement with RGS'
													: objCase.RecordType.DeveloperName.equals('Lease_Handover') && isUMA == true ? 'UMA Document'
													: objCase.RecordType.DeveloperName.equals('Lease_Handover') && isPOA == true ? 'Power Of Attorney'
													: objCase.RecordType.DeveloperName.equals('Lease_Handover') && isLOA == true ? 'Letter Of Authorization'
													: objCase.RecordType.DeveloperName.equals('Riyadh_Rotana_Conversion') && RRC1 ? 'Addendum to the SPA(P)'
													: objCase.RecordType.DeveloperName.equals('Riyadh_Rotana_Conversion') && RRC2 ? 'Addendum to the SPA(F)'
													: objCase.RecordType.DeveloperName.equals('Riyadh_Rotana_Conversion') && RRC3 ? 'Addendum to the AGS (P)'
													: objCase.RecordType.DeveloperName.equals('Riyadh_Rotana_Conversion') && RRC4 ? 'Addendum to the AGS(F)'
													: objCase.RecordType.DeveloperName.equals('Riyadh_Rotana_Conversion') && RRC5 ? 'Long Term Lease'
													: objCase.RecordType.DeveloperName.equals('Riyadh_Rotana_Conversion') && RRC6 ? 'Letter of Discharge and Adherence'
													: objCase.RecordType.DeveloperName.equals('Riyadh_Rotana_Conversion') && RRC7 ? 'POA for Hotel Operation'
													: objCase.RecordType.DeveloperName.equals('Riyadh_Rotana_Conversion') && RRC8 ? 'POA for Constitution of OA'
													: objCase.RecordType.DeveloperName.equals('Riyadh_Rotana_Conversion') && RRC9 ? 'Deed of Adherence'
													: objCase.RecordType.DeveloperName.equals('Riyadh_Rotana_Conversion') && RRC10 ? 'JOPD Document'
													:'NOC Document - '+ objCase.NOC_Template__c + ' - ' + system.now();
						}

						if(objCase.RecordType.DeveloperName.equals('Change_of_Details')){
							system.debug('COCDLetter');
							objCaseAttachment.Name = 'COCDLetter' ;
						}

						objCaseAttachment.Case__c = objCase.id;
						if(mapCaseNumber_URL != null && mapCaseNumber_URL.get(String.ValueOf(objCase.CaseNumber)) != null){
							objCaseAttachment.Attachment_URL__c = mapCaseNumber_URL.get(String.ValueOf(objCase.CaseNumber));
						}
						objCaseAttachment.Booking_Unit__c = objCase.Booking_Unit__c;
						objCaseAttachment.IsValid__c = true;
						objCaseAttachment.Type__c = objCase.RecordType.DeveloperName.equals('AOPT') || objCase.RecordType.DeveloperName.equals('Early_Settlement') ? 'Offer & Acceptance Letter'
													: objCase.RecordType.DeveloperName.equals('Parking') && isOfferAcceptanceLetter ? 'Parking Offer & Acceptance Letter'
													: objCase.RecordType.DeveloperName.equals('Parking') && isParkingReservationLetter ? 'Parking reservation letter'
													: objCase.RecordType.DeveloperName.equals('Parking') ? 'Allocation of Parking acceptance'
													: objCase.RecordType.DeveloperName.equals('Rental_Pool_Agreement') ? 'Rental Pool Addendum' : null;

						/*if(objCase.RecordType.DeveloperName.equals('Riyadh_Rotana_Conversion')) {
							SR_Attachments__c objSignedAtt = new SR_Attachments__c();
							String attName = RRC1 ? 'Addendum to SPA (P)' : 
											RRC2 ? 'Addendum to SPA (F)' : 
											RRC3 ? 'Addendum to AGS (P)' :
											RRC4 ? 'Addendum to AGS (F)' :
											RRC5 ? 'Long Term Lease' :
											RRC6 ? 'Discharge and Adherence Letter' :
											RRC7 ? 'POA for Hotel Operation' :
											RRC8 ? 'POA Owners Association' :
											RRC9 ? 'Deed of Adherence' :
											RRC10 ? 'JOPD Document' : '';
							objSignedAtt.Name = objCase.Registration_ID__c + '-Signed '+ attName + '-'+ system.today();
							objSignedAtt.Case__c = objCase.id;
							objSignedAtt.Booking_Unit__c = objCase.Booking_Unit__c;
							objSignedAtt.IsValid__c = true;
							lstSRAttcahment.add(objSignedAtt);
						}*/
						lstSRAttcahment.add(objCaseAttachment);
						system.debug('lstSRAttcahment' + lstSRAttcahment);
						objCase.NOC_Processing_Pending__c = false;
						lstCaseUpdate.add(objCase);
					}
				}
				
				System.debug('lstSRAttcahment+++++++++++'+lstSRAttcahment);
				if(!lstSRAttcahment.isEmpty()){
					upsert lstSRAttcahment;
				}
				if(!lstCaseUpdate.isEmpty()){
					update lstCaseUpdate;
				}
			}
		}

	} // end of uploadDocumentCentralRepository method
	/*Query on Attachment with filters that
	 *the parent of attachment should be cases,
	 *name of attachment should start with NOC Document
	 *Record Type of related case should be Assignment / Rentalpool Assignment*/
	public static List<Attachment> QueryAttachment(Set<Id> setAttachmentIds, Set<Id> setCaseRTId){
		Set<String> setRiyadhRotanaDocs = getRiyadhRotanaDocs();
		System.debug('setRiyadhRotanaDocs == ' + setRiyadhRotanaDocs);
		return new List<Attachment>([Select a.Parent.RecordTypeId,
											a.Parent.Id,
											a.Parent.Type,
											a.ParentId,
											a.OwnerId,
											a.Name,
											a.Id,
											a.BodyLength,
											a.Body
									From Attachment a
									Where a.Id IN: setAttachmentIds
									AND a.Parent.Type = 'Case'
									AND (a.Name LIKE 'NOC Document%' OR a.Name LIKE 'Offer%' OR a.Name LIKE 'Confirmation%'
										OR a.Name LIKE 'Parking Offer%'  OR a.Name LIKE 'Parking reservation%' OR a.Name LIKE 'Rental Pool%' OR a.Name LIKE 'UHR Document%'
										OR a.Name LIKE 'LMA Request%' OR a.Name LIKE 'Agreement - Non resident%'
										OR a.Name LIKE 'Agreement with RGS%' OR a.Name LIKE 'NOC%' OR a.Name LIKE 'Undertaking Letter%'
										OR a.Name LIKE 'PHR Document%' OR a.Name LIKE 'OVERDUE AMOUNT REBATE LETTER%' 
										OR a.Name LIKE 'Letter Of Authorization%'
										OR a.Name LIKE 'COCDLetter%'
										OR a.Name LIKE 'AreaVariationLetter%'
										OR a.Name LIKE 'Villa Short%'
										OR a.Name LIKE 'Old Villa%'
										OR a.Name LIKE 'Pacifica Villa Type%'
										OR a.Name LIKE 'UMA Document%'
										OR a.Name LIKE 'Power Of Attorney%'
										OR a.Name LIKE 'ADVANCE PAYMENT REBATE LETTER%'
										OR a.Name LIKE 'Cash Payment KYC%'
										OR a.Name LIKE '3rd party%'
										OR a.Name LIKE 'Handover Checklist%'
										OR a.Name IN : setRiyadhRotanaDocs)
									AND a.Parent.RecordTypeId IN: setCaseRTId]);
	}
	/*Query on Case with filters that
	 *fetch related SR Attachment with Name NOC document with most recent created,
	 *Record Type of case should be Assignment / Rentalpool Assignment*/
	public static Map<Id, Case> fetchCaseDocument(Set<Id> setCaseIds, set<Id> setCaseRTId){
		return new Map<Id, Case>([Select c.Id,
									 c.CaseNumber,
									 c.Registration_ID__c,
									 c.RecordTypeId,
									 c.RecordType.DeveloperName,
									 c.Booking_Unit__c,
									 c.NOC_Template__c,
									 (Select Id,
											 Name,
											 Account__c,
											 Attachment_External_URL__c,
											 Attachment_URL__c,
											 Attachment__c,
											 Booking_Unit__c,
											 Case__c,
											 CreatedDate,
											 IsRequired__c,
											 Signed_by_Customer__c,
											 Signed_by_DAMAC__c,
											 isValid__c,
											 Document_Type__c
									  From SR_Attachments__r
									  Where (Name Like 'NOC Document%' OR Name Like 'Offer%' OR Name LIKE 'Confirmation%'
										  OR Name LIKE 'Parking Offer%' OR Name LIKE 'Parking reservation%' OR Name LIKE 'Rental Pool%' OR Name LIKE 'UHR Document%'
										  OR Name LIKE 'LMA Request%' OR Name LIKE 'Agreement - Non resident%'
										  OR Name LIKE 'Agreement with RGS%' OR Name LIKE 'NOC%' OR Name LIKE 'Undertaking Letter%'
										  OR Name LIKE 'PHR Document%' )
									  Order By CreatedDate DESC LIMIT 1)
							   From Case c
							   Where c.Id IN: setCaseIds
							   AND c.RecordTypeId IN: setCaseRTId FOR UPDATE]);

	}

	public static Set<String> getRiyadhRotanaDocs() {
		Set<String> setRiyadhRotanaDocs = new Set<String>();
		setRiyadhRotanaDocs.add('Addendum to the Sale and Purchase Agreement (P).pdf');
		setRiyadhRotanaDocs.add('Addendum to the Sale and Purchase Agreement (F).pdf');
		setRiyadhRotanaDocs.add('Addendum to the Agreement to Sell (P).pdf');
		setRiyadhRotanaDocs.add('Addendum to the Agreement to Sell (F).pdf');
		setRiyadhRotanaDocs.add('Long Term Lease.pdf');
		setRiyadhRotanaDocs.add('Letter of Discharge and Adherence.pdf');
		setRiyadhRotanaDocs.add('Power of Attorney for Hotel Operation.pdf');
		setRiyadhRotanaDocs.add('Power of Attorney for Constitution of Owners Association (OA).pdf');
		setRiyadhRotanaDocs.add('Deed of Adherence.pdf');
		setRiyadhRotanaDocs.add('JOPD Document.pdf');
		return setRiyadhRotanaDocs;
	}
}