/*
* Description - Test class developed for GenerateDocForFMOnAccountCntrl and GenerateDocForFMOnBUCntrl 
*/
@isTest
public class GenerateDocForFMOnAccountCntrlTest  {
    static final String TEST_URL = 'https://test.damacgroup.com/test.pdf';
    
    static testMethod void testGenerateFMSOA() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        Test.startTest();
            PageReference popPage = Page.GeneratePartyWiseSOAForFMAccount;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf( objAcc.Id ));
            ApexPages.StandardController sc = new ApexPages.standardController( objAcc );
            GenerateDocForFMOnAccountCntrl objController = new GenerateDocForFMOnAccountCntrl(sc);
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;
            
            objController.generateFMPartySOA();
            
        Test.stopTest();
    }
    
    static testMethod void testGenerateFMPartySOA1() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
        }
        insert lstBookingUnits;

        
        Test.startTest();
            PageReference popPage = Page.GenerateUnitSOAForFMBU;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstBookingUnits[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstBookingUnits[0] );
            GenerateDocForFMOnBUCntrl objController = new GenerateDocForFMOnBUCntrl(sc);
            
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;

            objController.generateFMSOA();
        Test.stopTest();
    }
   
    static testMethod void testGenerateFMPartySOA2() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
        }
        insert lstBookingUnits;

        
        Test.startTest();
            PageReference popPage = Page.GenerateUnitSOAForFMBU;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstBookingUnits[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstBookingUnits[0] );
            GenerateDocForFMOnBUCntrl objController = new GenerateDocForFMOnBUCntrl(sc);
            
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;

            objController.generateFMSOA();
        Test.stopTest();
    }   
   
    static testMethod void test_GenerateFMPropertyWiseSOA() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = 'AD/201/2015';
        }
        insert lstBookingUnits;
        
        Test.startTest();
            PageReference popPage = Page.GeneratePartyWiseSOAForFMBU;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstBookingUnits[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstBookingUnits[0] );
            GenerateDocForFMOnBUCntrl objController = new GenerateDocForFMOnBUCntrl(sc);
            
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;
            objController.generateFMPartySOA();
        Test.stopTest();
    }
    
    static testMethod void test_GenerateAndEmailFMUnitSOA() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__pc = 'test@test.com';
        objAcc.Email__c = 'test1@test.com';
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = 'AD/201/2015';
        }
        insert lstBookingUnits;
        
        Test.startTest();
            PageReference popPage = Page.GenerateAndEmailFMUnitSOA;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstBookingUnits[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstBookingUnits[0] );
            GenerateDocForFMOnBUCntrl objController = new GenerateDocForFMOnBUCntrl(sc);
            
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;
            
            objController.generateAndEmailFMUnitSOA();
        Test.stopTest();
    }
    
    static testMethod void test_GenerateAndEmailFMUnitSOA1() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__pc = 'test@test.com';
        objAcc.Email__c = 'test1@test.com';
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = 'AD/201/2015';
        }
        insert lstBookingUnits;
        
        Test.startTest();
            PageReference popPage = Page.GenerateAndEmailFMUnitSOA;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstBookingUnits[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstBookingUnits[0] );
            GenerateDocForFMOnBUCntrl objController = new GenerateDocForFMOnBUCntrl(sc);
            
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;
            objController.CancelGeneration();
        Test.stopTest();
    }
    
    
    static testMethod void test_GenerateAndEmailFMUnitSOA2() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__pc = 'test@test.com';
        objAcc.Email__c = 'test1@test.com';
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = 'AD/201/2015';
        }
        insert lstBookingUnits;
        
        Test.startTest();
            PageReference popPage = Page.GenerateAndEmailFMUnitSOA;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstBookingUnits[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstBookingUnits[0] );
            GenerateDocForFMOnBUCntrl objController = new GenerateDocForFMOnBUCntrl(sc);
            
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;
            GenerateDocForFMOnBUCntrl.errorLogger('Test',null,lstBookingUnits[0].Id,null);
        Test.stopTest();
    }
    
    static testMethod void test_GenerateAndEmailFMUnitSOA3() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__pc = 'test@test.com';
        objAcc.Email__c = 'test1@test.com';
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Unit_Name__c = 'AD/201/2015';
        }
        insert lstBookingUnits;
        
        Test.startTest();
            PageReference popPage = Page.GenerateAndEmailFMUnitSOA;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstBookingUnits[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstBookingUnits[0] );
            GenerateDocForFMOnBUCntrl objController = new GenerateDocForFMOnBUCntrl(sc);
            
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;
            objController.CancelGeneration();
        Test.stopTest();
    }
    
    static testMethod void test_GenerateAndEmailFMPartySOA1() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__pc = 'test@test.com';
        objAcc.Email__c = 'test1@test.com';
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = 'AD/201/2015';
            objUnit.Party_ID__c = '65432';
        }
        insert lstBookingUnits;
        
        Test.startTest();
            PageReference popPage = Page.GenerateAndEmailFMUnitSOA;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstBookingUnits[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstBookingUnits[0] );
            GenerateDocForFMOnBUCntrl objController = new GenerateDocForFMOnBUCntrl(sc);
            
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;
            objController.generateAndEmailFMPartySOA();
        Test.stopTest();
    }
    
    static testMethod void test_GenerateAndEmailFMPartySOA2() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__pc = 'test@test.com';
        objAcc.Email__c = 'test1@test.com';
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = 'AD/201/2015';
        }
        insert lstBookingUnits;
        
        Test.startTest();
            PageReference popPage = Page.GenerateAndEmailFMUnitSOA;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstBookingUnits[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstBookingUnits[0] );
            GenerateDocForFMOnBUCntrl objController = new GenerateDocForFMOnBUCntrl(sc);
            
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;
            objController.generateAndEmailFMPartySOA();
        Test.stopTest();
    }
}