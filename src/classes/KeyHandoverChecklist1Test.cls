@isTest
public class KeyHandoverChecklist1Test{
    static list<RuleEngineDocuments__c> lstREDocs;
    static Account objA, objA1, objA2;
    static list<Booking__c> lstBookings;
    static list<Booking_Unit__c> lstBU;
    static Id recTypeId;
    static list<Buyer__c> lstBuy;
    static NSIBPM__Service_Request__c objSR;
    
    static void init(){
        lstREDocs = TestDataFactory_CRM.createRuleDocs();
        insert lstREDocs;
        objA = TestDataFactory_CRM.createPersonAccount();
        objA.Nationality__pc = 'Afghanistan';
        objA.Country__pc = 'Afghanistan';
        insert objA;
        //objA1 = TestDataFactory_CRM.createPersonAccount();
        //insert objA1;
        objA2 = TestDataFactory_CRM.createBusinessAccount();
        insert objA2;
        objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objA.Id, objSR.Id, 2);
        insert lstBookings;
        lstBU = TestDataFactory_CRM.createBookingUnits(lstBookings,2);
        insert lstBU;
        lstBuy = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 1, objA.Id);
        lstBuy[0].Primary_Buyer__c = true;
        lstBuy.addAll(TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 1, objA2.Id));
        insert lstBuy;
        recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId(); 
    }
    
    private static testMethod void parentCase1(){
        init();
        
        Case objC = TestDataFactory_CRM.createCase(objA.Id, recTypeId);
        objC.Booking_Unit__c = lstBU[0].Id;
        insert objC;
        
        list<Case> lstCase = new list<Case>();
        Case objC1 = new Case();
        objC1.AccountId = objA.Id;
        objC1.Booking_Unit__c = lstBU[0].Id;
        objC1.ParentId = objC.Id;
        objC1.RecordTypeId = recTypeId;
        lstCase.add(objC1);
        
        Case objC2 = new Case();
        objC2.AccountId = objA.Id;
        objC2.Booking_Unit__c = lstBU[1].Id;
        objC2.ParentId = objC.Id;
        objC2.RecordTypeId = recTypeId;
        lstCase.add(objC2);
        insert lstCase;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockHO());
        //Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objC);
        KeyHandoverChecklist1 objClass = new KeyHandoverChecklist1(sc);
        objClass.confirmationFromUser();
        Test.stopTest();
    }
    
    private static testMethod void testChildCase(){
        init();
        objA.Nationality__pc = 'UAE';
        objA.Country__pc = 'United Arab Emirates';
        update objA;
        
        Case objC = TestDataFactory_CRM.createCase(objA.Id, recTypeId);
        objC.Booking_Unit__c = lstBU[0].Id;
        insert objC;
        
        list<Case> lstCase = new list<Case>();
        Case objC1 = new Case();
        objC1.AccountId = objA.Id;
        objC1.Booking_Unit__c = lstBU[0].Id;
        objC1.ParentId = objC.Id;
        objC1.RecordTypeId = recTypeId;
        lstCase.add(objC1);
        insert lstCase;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockHO());
        //Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objC1);
        KeyHandoverChecklist1 objClass = new KeyHandoverChecklist1(sc);
        objClass.confirmationFromUser();
        objClass.cancel();
        Test.stopTest();
    }
}