/****************************************************************************************
Description : Controller of VF page 'CollectionMemoDocGen' that generates Collection    *
                Memo letter                                                             *
----------------------------------------------------------------------------------------*
Version     Date        Author              Description                                 *
                                                                                        *
1.0         18/03/2020  Aishwarya Todkar    Initial Drafet                              *
*****************************************************************************************/

public Class CollectionMemoDocGenCtrl {

    Static Id caseId;
    public CollectionMemoDocGenCtrl(ApexPages.StandardController controller) {
        Case objCase = ( Case )controller.getRecord();
        caseId = objCase.Id;
    }

/**************************************************************************************
Description : method to generate Collection memo letter from Drawloop
Parameters  : None
Return type : void
***************************************************************************************/
    public static void generateCollectionMemo() {
        if( caseId != null ) {
            String csName = 'Rebate Advance Payment Letter';
            Riyadh_Rotana_Drawloop_Doc_Mapping__c cs = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( csName );
            System.debug('cs--'+cs);
            if(cs != null ) {
                    
                if( String.isBlank( cs.Drawloop_Document_Package_Id__c )) {
                    ApexPages.addmessage(
                        new ApexPages.message( ApexPages.severity.ERROR, 
                        'Please provide Drawloop Document package Id of ' + csName )
                    );
                }

                else if( String.isBlank( cs.Delivery_Option_Id__c )) {
                    ApexPages.addmessage(
                        new ApexPages.message( ApexPages.severity.ERROR, 
                        'Please provide Delivery Option Id of ' + csName)
                    );
                }
                else {
                    try {
                        DrawloopDocGen.generateDoc( cs.Drawloop_Document_Package_Id__c
                                                    , cs.Delivery_Option_Id__c
                                                    , caseId);
                        ApexPages.addmessage(
                            new ApexPages.message( ApexPages.severity.INFO, 
                            'Advance Payment Rebate letter generated!')
                        );                                
                    } 
                    catch (Exception ex ) {
                        ApexPages.addmessage(
                            new ApexPages.message( ApexPages.severity.ERROR, 
                            ex.getMessage() )
                        );
                    }
                    
                }
            }//End cs If
            else {
                ApexPages.addmessage(
                    new ApexPages.message( ApexPages.severity.ERROR, 
                    'Can not find Drawloop Document Package - ' + csName)
                );
            }
        }//End CaseId if
        else {
            ApexPages.addmessage(
                new ApexPages.message( ApexPages.severity.ERROR, 
                'Case Id not found!')
            );
        }
    }//Method end
}