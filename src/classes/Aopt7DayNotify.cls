public class Aopt7DayNotify {
    @InvocableMethod 
    public static void checkIfAddendumUploaded( List<Case> lstCases )
    {
        Set<Id> setCaseId = new Set<Id>();
        system.debug( 'processDocument method called'+lstCases );
        for( Case objCase : lstCases ) {
            if( !objCase.O_A_Signed_Copy_Uploaded__c ) {
                setCaseId.add(objCase.Id);
            }
        }
        
        if(!setCaseId.isEmpty()) {
            List<Case> lstCasesToUpdate = new List<Case>();
            for(Case objCase: [Select Id, the7thDay__c, AccountId, Account_Email__c From Case Where Id IN: setCaseId And Account_Email__c != null]) {
                objCase.the7thDay__c = true;
                lstCasesToUpdate.add(objCase);
            }
            update lstCasesToUpdate;
        }
    }

}