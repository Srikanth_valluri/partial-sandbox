public with sharing class ComplaintProcessEscalationHandler {

    @InvocableMethod
    public static void HandleEscalationProcess (List<Case> lstSR) {
        
        List<Id> objCaseIds = new List<Id>();
        for (Case objCase : lstSR) {
            objCaseIds.add(objCase.Id);
        }
        
        if (objCaseIds != null && !objCaseIds.isEmpty()) {
            lstSR = [SELECT Id, Customer_Connected_Through_CTI__c, Status, OwnerId, CaseNumber FROM Case WHERE Id IN: objCaseIds];
        }

        List<Case> escalateToDirCase = new List<Case>();
        List<Case> escalateToSVPCase = new List<Case>();
        for(Case caseObj: lstSR){
            if( caseObj.Customer_Connected_Through_CTI__c == false && caseObj.Status != 'Closed' ){
                escalateToDirCase.add(caseObj);
            }/* else if( caseObj.Customer_Connected_Through_CTI__c == false && caseObj.Status == 'Escalated to Director' ){
                escalateToSVPCase.add(caseObj);
            }*/
        }

        if(!escalateToDirCase.isEmpty()){
            sendNotificationToDirector(escalateToDirCase);
        }

        /*if(!escalateToSVPCase.isEmpty()){
            sendNotificationToSVP(escalateToSVPCase);
        }*/
    }

    public static void sendNotificationToDirector(List<Case> lstSR){

        Set<String> PCIds = new Set<String>();
        for( Case sr : lstSR){
            if(!String.ValueOf((sr.OwnerId)).startsWith('00G')){
                PCIds.add(sr.OwnerId);
            }
        }

        if(PCIds != null && !PCIds.isEmpty()){
            List<Task> lstTaskToInsert = new List<Task>();
            Map<Id, User> UserId_UserDetailMap = new Map<Id, User>([SELECT Id, Name, Email, Phone, MobilePhone, Address, Manager.Email, Manager.Name, ManagerId FROM User where Id IN : PCIds]);
            List<String> toAddresses = new List<String>();

            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            for(Case sr : lstSR ){
                    Messaging.SingleEmailMessage mailToDirector = new Messaging.SingleEmailMessage();

                    mailToDirector.setSubject('Alert!! Case has not been closed for 24 hours: '+sr.CaseNumber );
                    if( Test.isRunningTest() ) {
                        mailToDirector.setToAddresses(new String[]{'test@test.com'});
                    } else {
                        mailToDirector.setToAddresses(new String[]{UserId_UserDetailMap.get(sr.OwnerId).Manager.Email});
                        Task objTask = TaskUtility.getTask((SObject)sr, 'Complaint case not closed after 24 hours', 'CRE Manager', 'Complaints', system.today().addDays(1));
                        objTask.OwnerId = UserId_UserDetailMap.get(sr.OwnerId).ManagerId;
                        lstTaskToInsert.add(objTask);
                    }
                    //mailToDirector.setToAddresses(new String[]{'amit.joshi@eternussolutions.com'});
                    String mailBodyDir = 'Hi,<br/>';
                    mailBodyDir += '<p>There is a Complaint request that has not been resolved for the last 24 hours. This is an escalation email. Please review.</p>';
                    mailBodyDir += 'Thank you,<br/>DAMAC Team.';
                    mailToDirector.setHtmlBody( mailBodyDir );
                    mails.add(mailToDirector);
                    sr.Status = 'Escalated to Director';
            }
            insert lstTaskToInsert;
            Messaging.sendEmail(mails);
            update lstSR;
        }
    }

    /*public static void sendNotificationToSVP(List<Case> lstSR){

            List<String> toAddresses = new List<String>();

            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            for(Case sr : lstSR ){
                    Messaging.SingleEmailMessage mailToSVP = new Messaging.SingleEmailMessage();

                    mailToSVP.setSubject('Alert!! Case has not been closed for 48 hours: '+sr.CaseNumber );
                    mailToSVP.setToAddresses(new String[]{'snehil.karn@eternussolutions.com'});
                    String mailBodySvp = 'Hi,<br/>';
                    mailBodySvp += '<p>There is a Complaint request that has not been solved for the last 24 hours. This is an escalation email. Please review.</p>';
                    mailBodySvp += 'Thank you,<br/>DAMAC Team.';
                    mailToSVP.setHtmlBody( mailBodySvp );
                    mails.add(mailToSVP);
            }
            Messaging.sendEmail(mails);
    }*/
}