@isTest
private class GenerateKeyReleaseFormExtensionTest {
    
    @testSetup
    static void setup() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        insert objCase ;
        
        NSIBPM__Service_Request__c objReq = TestDataFactory_CRM.createServiceRequest();
        insert objReq ;
        
        List<Booking__c> lstBooking =  TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objReq.Id,1 );
        insert lstBooking ;
        
        List<Booking_Unit__c> lstUnits = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
        insert lstUnits ;
        
        SR_Attachments__c objAttach = TestDataFactory_CRM.createCaseDocument( null, 'Key Release Form' );
        objAttach.Booking_unit__c = lstUnits[0].Id ;
        insert objAttach ;
    }
    
    static testMethod void testConstructor() {
        list<Booking_Unit__c> lstUnits = [ SELECT Id FROM Booking_Unit__c LIMIT 1 ];
 		
        ApexPages.StandardController stdController = new ApexPages.StandardController( lstUnits[0] )  ;
        test.startTest();
            GenerateKeyReleaseFormExtension objServ = new GenerateKeyReleaseFormExtension( stdController );
        test.stopTest(); 
    }
    
    static testMethod void testCheckEligiblity1() {
        list<Booking_Unit__c> lstUnits = [ SELECT Id FROM Booking_Unit__c LIMIT 1 ];
 		
        ApexPages.StandardController stdController = new ApexPages.StandardController( lstUnits[0] )  ;
        test.startTest();
            GenerateKeyReleaseFormExtension objServ = new GenerateKeyReleaseFormExtension( stdController );
        	objServ.checkEligiblity();
        test.stopTest(); 
    }
    
    static testMethod void testCheckEligiblity2() {
        list<Booking_Unit__c> lstUnits = [ SELECT Id FROM Booking_Unit__c LIMIT 1 ];
 		lstUnits[0].Okay_to_release_keys__c = true ;
        update lstUnits ;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController( lstUnits[0] )  ;
        test.startTest();
            GenerateKeyReleaseFormExtension objServ = new GenerateKeyReleaseFormExtension( stdController );
        	objServ.checkEligiblity();
        test.stopTest(); 
    }
    
    static testMethod void testCheckEligiblity3() {
        list<Booking_Unit__c> lstUnits = [ SELECT Id FROM Booking_Unit__c LIMIT 1 ];
        
        ApexPages.StandardController stdController = new ApexPages.StandardController( lstUnits[0] )  ;
        test.startTest();
            GenerateKeyReleaseFormExtension objServ = new GenerateKeyReleaseFormExtension( stdController );
        	objServ.bookingUnitId = NULL;
        	objServ.checkEligiblity();
        test.stopTest(); 
    }
}