public without sharing Class CourierCompCtrl {

    public Courier_Delivery__c courierInst {get;set;}
    public String accId;
    public String caseId;
    public String buId;
    public String recordLink{get;set;}
    public Boolean isPersonAcc{get;set;}
    public Boolean displayDetails{get;set;}
    public String maskedPhoneNumber1 {get;set;}
    public String maskedPhoneNumber2 {get;set;}
    public Boolean initialize;
    List<Courier_Delivery__c> courierList;
    public List<Courier_Delivery__c> courierOpenList{get;set;}

    
    public CourierCompCtrl() {
        courierInst = new Courier_Delivery__c();
        courierList = new List<Courier_Delivery__c>();
        displayDetails = true;
        recordLink = '';
        initialize = true;
        String parentId = apexpages.currentpage().getparameters().get('id');
        if(parentId.startsWith('a0x')) {
             courierOpenList = [Select Name,
                                  Airway_Bill__c,
                                  Courier_Status__c,
                                  Request_Number__c,
                                  UnitName__c,
                                  Case__c,
                                  Type_of_Document__c
                           FROM Courier_Delivery__c
                           WHERE Courier_Status__c = 'Success'
                           AND UnitName__c =:parentId 
                           
                           ];
        }
        else if(parentId.startsWith('500')) {
             courierOpenList = [Select Name,
                                  Airway_Bill__c,
                                  Courier_Status__c,
                                  Request_Number__c,
                                  UnitName__c,
                                  Case__c
                           FROM Courier_Delivery__c
                           WHERE Courier_Status__c = 'Success'
                           AND  Case__c =:parentId
                           
                           ];
        }
       
        //showThankYouMsg = false;
        //setCourierInst();
        
    }
    
    public void setcaseId(String argId) {
        caseId = argId;
        system.debug('caseId-->'+caseId);
        if(caseId != null && !String.isBlank(caseId)){
            Case caseInst = [SELECT Id
                                  , Booking_Unit__c
                             FROM Case
                             WHERE Id = :caseId 
                             LIMIT 1];
            system.debug('caseInst-->'+caseInst);
            courierInst.UnitName__c = caseInst.Booking_Unit__c;
        }
    }
    public String getcaseId(){
        return caseId; 
    }
    
    public void setbuId(String argId) {
        buId = argId;
        if(buId != null && !String.isBlank(buId)) {
            Booking_Unit__c objBookingUnit = [SELECT Id,
                                                     Unit_Name__c
                                             FROM Booking_Unit__c 
                                             WHERE Id =: buId];
            courierInst.UnitName__c = buId;
            courierInst.Unit_Names__c = objBookingUnit.Unit_Name__c;
        }
    }
    public String getbuId(){
        return buId; 
    }

    public void setaccId(String argId) {
        accId = argId;
        system.debug('accId---->'+accId);
        List<Account> accList = new List<Account>();
        if(accId!=null) {
            accList = [SELECT id
                            , Name
                            , Master_Address__c
                            , Zip_Postal_Code__c
                            , City__c
                            , Country__c
                            , Phone_1__c
                            , Phone_2__c
                            , Fax__c
                            , IsPersonAccount
                            , Mobile_Person_Business__c
                            , City__pc 
                            , Country__pc
                            , Zip_Postal_Code_Acc__pc
                            , Fax_Acc__pc
                            , Mobile_Phone_Encrypt__pc
                            , Mobile__c
                            
                       FROM Account
                       WHERE Id = :accId];
        }
        if(accList.size() > 0 && initialize) {
            isPersonAcc = accList[0].IsPersonAccount;
            courierInst.Account__c = accList[0].Id;
            if(isPersonAcc) {
                               
                courierInst.Postal_Code__c = accList[0].Zip_Postal_Code_Acc__pc;
                courierInst.City__c = accList[0].City__pc;
                courierInst.Country__c = accList[0].Country__pc;
                courierInst.Phone_1__c = accList[0].Mobile_Phone_Encrypt__pc;    
                courierInst.Phone_2__c = accList[0].Mobile__c;
                courierInst.Fax__c = accList[0].Fax_Acc__pc;
                maskedPhoneNumber1 = String.isNotBlank(accList[0].Mobile_Phone_Encrypt__pc) ? accList[0].Mobile_Phone_Encrypt__pc.left(3)+'xxxxxx'+accList[0].Mobile_Phone_Encrypt__pc.right(4):'';
               
                maskedPhoneNumber2 = String.isNotBlank(accList[0].Mobile__c) ? accList[0].Mobile__c.left(3)+'xxxxxx'+accList[0].Mobile__c.right(4):'';
            } else {
                
                courierInst.Postal_Code__c = accList[0].Zip_Postal_Code__c;
                courierInst.City__c = accList[0].City__c;
                courierInst.Country__c = accList[0].Country__c;
                courierInst.Phone_1__c = accList[0].Phone_1__c;          
                courierInst.Phone_2__c = accList[0].Phone_2__c;
                courierInst.Fax__c = accList[0].Fax__c;
                maskedPhoneNumber1 = String.isNotBlank(accList[0].Phone_1__c) ? accList[0].Phone_1__c.left(3)+'xxxxxx'+accList[0].Phone_1__c.right(4):'';
                maskedPhoneNumber2 = String.isNotBlank(accList[0].Phone_2__c) ? accList[0].Phone_2__c.left(3)+'xxxxxx'+accList[0].Phone_2__c.right(4):'';
            }
            courierInst.Address_1__c = accList[0].Master_Address__c;
            courierInst.Consignee_Name__c = accList[0].Name;
            courierInst.Company_Name__c = accList[0].Name;
            //courierInst.Phone_2__c = accList[0].Mobile_Person_Business__c;
            
           
            
            initialize = false;
            //courierList.add(courierInst);
        }
        
    }
    public String getaccId(){
        return accId; 
    }
    public PageReference submitDetails() {
        List<Courier_Item__c> ciList = new List<Courier_Item__c>();
        //List<Id> courierDeliveryIds = new List<Id>();
        /*If(buId != null && !String.isBlank(buId)) {
            courierInst.UnitName__c = buId;
        }*/
        courierInst.Case__c = caseId;
        courierInst.Number_of_Pieces__c = '1';
        system.debug('courierInst.Case__c--->'+courierInst.Case__c);
        courierList.add(courierInst);
        system.debug('courierList in submit--->'+courierList);
        List<Courier_Delivery__c> courierListUpdate = new List<Courier_Delivery__c>();
        String confMsg = 'Courier Request Created Successfully. AWB Number is: ';
        try{
            if(courierInst.Courier_Service__c == 'First Flight') {
                courierListUpdate = FirstFlightService.generateAirwayBill(courierList);
            } else if(courierInst.Courier_Service__c == 'Aramex' || courierInst.Courier_Service__c == 'Aramex CDS' ) {
                courierListUpdate = AramexService.createShipping(courierInst.Courier_Service__c,courierList); // Aremax service
            }
            if(courierListUpdate != Null && courierListUpdate.size() > 0) {
                displayDetails = false;
                insert courierListUpdate;
                for(Courier_Delivery__c acInst : courierListUpdate) {
                    //courierDeliveryIds.add(acInst.Id);
                    if(acInst.Case__c != null || acInst.UnitName__c != null ) {
                        Courier_Item__c ciInst = new Courier_Item__c();
                         if(acInst.Case__c != null ){
                            ciInst.Case__c = acInst.Case__c;
                         }
                         if(acInst.UnitName__c != null){
                              ciInst.Booking_Unit__c = acInst.UnitName__c;
                         }
                        ciInst.Courier_Delivery__c = acInst.Id;
                        ciList.add(ciInst);
                     }
                    
                }
                insert ciList;
                //if(courierDeliveryIds != Null && courierDeliveryIds.size() > 0) {
                    //createAirWayBillCourierDelivery(courierDeliveryIds);
                //}
                for(Courier_Delivery__c acInst : courierListUpdate) {
                    createAirWayBillCourierDelivery(acInst.Id);
                    confMsg = confMsg + acInst.Request_Number__c + ' ';
                }
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,confMsg));
            } else {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Some error occured while creating the courier Request. Please try again after some time.'));
            }
            
            //PageReference pg = new PageReference('/' + courierInst.id);
            //pg.setRedirect(true);
            return null;
        } catch(Exception e) {
            system.debug('exception--->'+e);
            confMsg = 'Some error occured while creating the courier Request. Please try again after some time.';
            return null;
        }
    }
    @future(callout=true)
    public static void createAirWayBillCourierDelivery(Id courierDeliveryIds) {
        Courier_Delivery__c courierDeliveryObj = [Select Id
                                                       , Airway_Bill__c
                                                       , Airway_PDF__c 
                                                       , Courier_Service__c
                                                       , Country__c
                                                    From Courier_Delivery__c 
                                                   Where id =: courierDeliveryIds
                                                   LIMIT 1];
        GenerateAirwayBillController objcontroller = new GenerateAirwayBillController(new ApexPages.StandardController(courierDeliveryObj));
        objcontroller.uploadAirwayBill();
    }
    
}