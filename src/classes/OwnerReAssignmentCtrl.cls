/********************************************************************************************************************************
 * Description : Class to re-assign owner of records in bulk
 *===============================================================================================================================
 * Ver      Date-DD/MM/YYYY     Author              Modification
 *===============================================================================================================================
 * 1.0      06/05/2020          Aishwarya Todkar    Initial Draft
*********************************************************************************************************************************/
public class OwnerReAssignmentCtrl {

    public String selectedObject {get; set;}
    public List<SelectOption> listObjects { get; set; }
    public List<SelectOption> allFields{get; set;}
    public LisT<FieldDetailsWrapper> listFieldDetails {get; set; }
    public String pickValMapJson { get; set; }
    public String query { get; set; }
    public String ownerType { get; set; }
    public String ownerSearchVal { get; set; }
    public List<User> owners { get; set;}
    public List<Group> queues { get; set;}
    public Id changeOwnerToId { get; set; }
    public List<sObject> callingList { get; set;}
    public List<String> selectedFields {get; set;}
    public String queryErr { get; set; }
    public String clIdsToReAssign { get; set; }
    public List<DisplayFieldsWrapper> listDisplayFields { get; set; }
    public String strDisplayFields { get; set; }

    public OwnerReAssignmentCtrl() {
        selectedObject = 'Account';
        query = '';
        
        queues = new List<Group>();
        owners = new List<User>();
        listObjects = new List<SelectOption>();

        for(Schema.SObjectType st : Schema.getGlobalDescribe().Values() ) {
            if( !st.getDescribe().isCustomSetting() && st.getDescribe().isAccessible() && st.getDescribe().isQueryable() && st.getDescribe().isUpdateable() ) {
                listObjects.add( new SelectOption( st.getDescribe().getName(),st.getDescribe().getLabel() ) );
            }
        }
        listObjects.sort(); 
        getAllFields();
    }

/********************************************************************************************************************************
* Method Name : getAllFields
* Description : method to get all fields of calling list
* Return Type : void
* Parameter(s): None
********************************************************************************************************************************/
    public void getAllFields() {
        listFieldDetails = new LisT<FieldDetailsWrapper>();
        System.debug( 'selectedObject--' + selectedObject);
        if( String.isNotBlank( selectedObject ) ) {

            Map< String, List<String>> mapPickValues = new Map<String, List<String>>();
            Schema.SObjectType ObjectSchema = Schema.getGlobalDescribe().get(selectedObject);
            Map<String, Schema.SObjectField> fieldMap = ObjectSchema.getDescribe().fields.getMap();
            List<String>listFields = new List<String>();
            listFields.addAll( fieldMap.keySet() );
            listFields.sort();

            for ( String fieldName: listFields ) {

                Schema.DescribeFieldResult fieldDescribe = fieldMap.get(fieldName).getDescribe();
                
                if( /*fieldDescribe.isUpdateable() && */ fieldDescribe.isAccessible() && fieldDescribe.isFilterable()) {

                    FieldDetailsWrapper wrapObj = new FieldDetailsWrapper();

                    wrapObj.apiName = fieldName;
                    wrapObj.label = fieldDescribe.getLabel();
                    wrapObj.fieldType = String.valueOf( fieldDescribe.getType() );
                    
                    //For Picklist
                    if( String.valueOf( fieldDescribe.getType() ).equalsIgnoreCase( 'PickList' ) ) {
                        List<String> pickValues = new List<String>();

                        for( Schema.PicklistEntry p : fieldDescribe.getPicklistValues() ) {
                            if( p.isActive() ) {
                                pickValues.add( p.getValue() );
                            }
                        }
                        mapPickValues.put( fieldDescribe.getLabel(), pickValues);
                        pickValMapJson = JSON.serialize( mapPickValues );
                        pickValMapJson = pickValMapJson.replace('\'','#');
                        wrapObj.fieldType = 'PickList';
                    }

                    //For Record Type
                    if( fieldDescribe.getLabel().equalsIgnoreCase( 'Record Type Id' ) ) {
                        List<String> pickValues = new List<String>();
                        for( Schema.RecordTypeInfo rt : ObjectSchema.getDescribe().getRecordTypeInfos() ) {
                            if( rt.isActive() ) {
                                pickValues.add( rt.getName() );
                            }
                        }
                        mapPickValues.put( 'Record Type', pickValues);
                        pickValMapJson = JSON.serialize( mapPickValues );
                        pickValMapJson = pickValMapJson.replace('\'','#');
                        wrapObj.label = 'Record Type';
                        wrapObj.apiName = 'RecordType.Name';
                        wrapObj.fieldType = 'PickList';
                    }
                    listFieldDetails.add( wrapObj );
                }
            }
        }
    } // End of getAllFields

/********************************************************************************************************************************
* Method Name : executeQuery
* Description : Method to get filtered Calling Lists.
* Return Type : void
* Parameter(s): None
********************************************************************************************************************************/
    public void executeQuery(){
        queryErr = '';
        callingList = new List<sObject>();
        List<sObject> sobjectRecords = new List<sObject>();
        listDisplayFields = new List<DisplayFieldsWrapper>();
        listDisplayFields = ( List<DisplayFieldsWrapper> ) JSON.deserialize( strDisplayFields, List<DisplayFieldsWrapper>.Class);
        
        System.debug('query--'+ query );//IMP
        try {
            sobjectRecords = Database.query( query );
            Map<Id, sObject> mapIdTosObject = new Map<Id, sObject>();
            for( sObject obj : sobjectRecords ) {
                mapIdTosObject.put( (Id)obj.get('Id'), obj );
            }
            for(Integer i = 0 ; i < (sobjectRecords.size() / 200)+1 ; i++){
                Set<Id>setIds = new Set<Id>();
                for( Integer j=(i*200);(j<(i*200)+200) && j<sobjectRecords.size() ; j++){
                    setIds.add(sobjectRecords.get(j).Id );
                }

                //At a time 200 records of UserRecordAccess can be queried
                if( Limits.getQueries() < Limits.getLimitQueries() ) {
                    for( UserRecordAccess access : [ SELECT 
                                                        RecordId
                                                        , HasEditAccess 
                                                        , HasReadAccess
                                                        FROM 
                                                            UserRecordAccess 
                                                        WHERE 
                                                            UserId =: changeOwnerToId 
                                                        AND 
                                                            RecordId In : setIds ] ) {
                        if( access.HasReadAccess && mapIdTosObject.containsKey( access.RecordId ) ) {
                            callingList.add( mapIdTosObject.get( access.RecordId ) );                
                        }                        
                    }
                }
                System.debug('Limits.getQueries() --' + Limits.getQueries() ); //IMP
                System.debug('Limits.getLimitQueries() --' + Limits.getLimitQueries() ); //IMP
                System.debug('callingList---'+ callingList.size());  //IMP
            }
            queryErr = sobjectRecords.size() == 0 ? 'No Records Found!' 
                    : callingList.size() == 0 ? 'Filtered records are not accessible to selected owner!' : '';
        }
        catch( Exception e ) {
            queryErr = e.getMessage();
        }
    }

/********************************************************************************************************************************
* Method Name : searchOwners
* Description : Method to search owners to re-assign
* Return Type : void
* Parameter(s): None
********************************************************************************************************************************/    
    public void searchOwners() {
        owners.clear();
        queues.clear();
        callingList = new List<sObject>();
        if(ownerType == 'User'){
            string searchquery='select Id, Name from user where name  like \'%' + ownerSearchVal + '%\' ';
            owners = Database.query(searchquery);
        }
        
        else if(ownerType == 'Queue'){
            string searchquery='select Id, Name from Group where type= \'Queue\' and name like \'%' + ownerSearchVal + '%\' ';
            queues = Database.query(searchquery);
        } 
        
        else if(ownerType == 'Portal User'){
            string searchquery = 'select Id,Name from user where name  like \'%' +
                                ownerSearchVal + 
                                '%\' AND(profile.name = \'Customer Community Login User\' OR profile.name = \'Tenant Community Login User\')';
            owners = Database.query(searchquery);
        }
    }

/********************************************************************************************************************************
* Method Name : reAssignOwner
* Description : Method to re-assign owner of selected Calling lists
* Return Type : void
* Parameter(s): None
********************************************************************************************************************************/
    public void reAssignOwner() {
        System.debug('clIdsToReAssign=='+clIdsToReAssign);
        System.debug('changeOwnerToId=='+changeOwnerToId);

        if( String.isNotBlank( clIdsToReAssign ) && String.isNotBlank( changeOwnerToId ) ) {
            clIdsToReAssign = clIdsToReAssign.removeEnd( ',' );
            Set<String> setToAvoidDuplicate = new Set<String>();
            List<Calling_List__c> listClToReAssign = new List<Calling_List__c>();

            for( String clId : clIdsToReAssign.split(',') ) {
                setToAvoidDuplicate.add( clId );
            }
            try {
                Id bId = Database.executeBatch( new OwnerReAssignmentBatch( setToAvoidDuplicate, changeOwnerToId, selectedObject ), 20 );

                callingList = new List<Calling_List__c>();
                ApexPages.addmessage( 
                    new ApexPages.message( 
                        ApexPages.severity.INFO
                        , 'Batch executed to reassign owner.Please check after sometime.' 
                    )
                );
            }
            catch( Exception e ) {
                ApexPages.addmessage( 
                    new ApexPages.message( 
                        ApexPages.severity.ERROR
                        , e.getMessage()
                    )
                );
            }
        }
    }

/********************************************************************************************************************************
Wrapper class to hold field details
********************************************************************************************************************************/
    public Class FieldDetailsWrapper {
        public List<String> pickValues { get; set; }
        public String apiName { get; set; }
        public String label { get; set; }
        public String fieldType { get; set; }        
        
        public FieldDetailsWrapper() {
            apiName = '';
            label = '';
            fieldType = '';
            pickValues = new List<String>();
        }
    }
    
    public Class DisplayFieldsWrapper {
        public String apiName { get; set; }
        public String label { get; set; }    
        
        Public DisplayFieldsWrapper() {
            apiName = '';
            label = '';
        }
    }
}