/****************************************************************************************************************
Description : Class to send OTP for Assignment cases
-----------------------------------------------------------------------------------------------------------------
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
-----------------------------------------------------------------------------------------------------------------
1.0     | 30-09-2020        |Aishwarya Todkar   | Initial Draft
*****************************************************************************************************************/
public class AssignmentOtpSender {

/*****************************************************************************************************************
 * Description      : Method to send otp
 * Parameter(s)     : List of Ids
 * Return           : void
 * process builder  : 'Assignment : Update Case on Task Closure'
 *****************************************************************************************************************/
    @InvocableMethod
    public static void sendOtp(List<Id> listIds) {
        if( !listIds.isEmpty() ) {
            for( Case objCase : [ SELECT
                                        Id
                                        , Account.Mobile_Person_Business__c 
                                    FROM
                                        Case
                                    WHERE
                                        RecordType.Name = 'Assignment' 
                                    AND
                                        AccountId != null
                                    AND 
                                        Id In : listIds
                                     ]) {
                OTPService.Sendtextmessage( objCase.Id + ':' + objCase.Account.Mobile_Person_Business__c );
            }
        }
    }
}