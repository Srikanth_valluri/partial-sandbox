/**********************************************************************************************************************
Description: This API is used for getting Amenities + unit milestone timeline + LDP Info + Handover Status from given RegId.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   09-07-2020      | Shubham Suryawanshi | Created
1.1     |   03-09-2020      | Subin C Antony	  | Added 'timeline' object in the response.
1.2     |   14-09-2020      | Subin C Antony	  | Modification of 'timeline' object in the response: added handover related dates
1.3     |   23-09-2020      | Subin C Antony	  | Modified 'timeline' in response : changed the object fields from which the dates are fetched.
***********************************************************************************************************************/

@RestResource(urlMapping='/getUnitDetailsWithAmenities/*')
global class HDAppGetAmentiesWithUnitDetails_API {

	public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    @HttpGet
    global static FinalReturnWrapper MainResponseCreator() {

    	RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_data objData = new cls_data();
        cls_meta_data objMeta = new cls_meta_data();

         if(!r.params.containsKey('regId')) {

         	objMeta = ReturnMetaResponse('Missing parameter : regId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;

        }
        else if(r.params.containsKey('regId') && String.isBlank(r.params.get('regId'))) {
        	objMeta = ReturnMetaResponse('Missing parameter value : regId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        if(r.params.containsKey('regId') && String.isNotBlank(r.params.get('regId'))) {

        	List<Booking_Unit__c> lstBU = GetBookingUnit(r.params.get('regId'));

        	if(lstBU.size() > 0) {
        		//main Logic
        		if(lstBU[0].DLP_End_Date__c != null) {
        			objData.dlp_expiry_date = Datetime.newInstance(lstBU[0].DLP_End_Date__c.year(), lstBU[0].DLP_End_Date__c.month(), lstBU[0].DLP_End_Date__c.day()).format('yyyy-MM-dd');
        			objData.is_under_dlp = Date.today() < lstBU[0].DLP_End_Date__c ? true : false;
                    if(objData.is_under_dlp && NULL != lstBU[0].Inventory__r && 
                       NULL != lstBU[0].Inventory__r.Building_Location__r){
                        objData.dlp_manual_link = lstBU[0].Inventory__r.Building_Location__r.Handover_Manual__c;
                    } /* if block added on 2Sep 2020 : Azure ticket 1558 */
        		}
                
                /* Section added on 3 Sep 2020: Azure ticket 1551 : BEGIN 
				 * Modified on 14 Sep 2020 to add 3x handover and 1x handover_notice dates 
				 * Modified on 23 Sep 2020: field mapping changed for 'Unit Booking' and 'Key Handover' */
                String tenantRegnDate = NULL;
                List<movement_spec> moveOutDates = new List<movement_spec>();
                List<movement_spec> moveInDates = new List<movement_spec>();
                if(NULL != lstBU[0].FM_Cases__r && lstBU[0].FM_Cases__r.size() > 0){
                    for(FM_Case__c srCase : lstBU[0].FM_Cases__r){
                        if(String.isNotBlank(srCase.Request_Type__c) && 
                           srCase.Request_Type__c.equalsIgnoreCase('Tenant Registration') && 
                           srCase.Status__c.equalsIgnoreCase('Tenant Registered; Awaiting Move in confirmation') && 
                           String.isBlank(tenantRegnDate) && NULL != srCase.Move_in_date__c){
                            tenantRegnDate = DateTime.newInstance(srCase.Move_in_date__c.year(),
                                                                  srCase.Move_in_date__c.month(),
                                                                  srCase.Move_in_date__c.day()).format('yyyy-MM-dd');
                        }
                        else if(String.isNotBlank(srCase.Request_Type__c) && srCase.Request_Type__c.equalsIgnoreCase('Move In') && 
                           srCase.Status__c.equalsIgnoreCase('Closed') && NULL != srCase.Move_in_date__c){
                            movement_spec movementSpec = new movement_spec();
                            if(NULL != srCase.Account__c && srCase.Account__c.equals(srCase.CreatedBy.AccountId)){
                                /* srCase.Account__c is owner account of BU */
                                movementSpec.client_type = 'Owner';
                            }
                            else {
                                movementSpec.client_type = 'Tenant';
                            }
                            String moveInDate =  DateTime.newInstance(srCase.Move_in_date__c.year(),
                                                                  srCase.Move_in_date__c.month(),
                                                                  srCase.Move_in_date__c.day()).format('yyyy-MM-dd');
                            movementSpec.moved_on = moveInDate;
                            moveInDates.add(movementSpec);
                        }
                        else if(String.isNotBlank(srCase.Request_Type__c) && srCase.Request_Type__c.equalsIgnoreCase('Move Out') && 
                           srCase.Status__c.equalsIgnoreCase('Closed') && NULL != srCase.Expected_move_out_date__c){
                            movement_spec movementSpec = new movement_spec();
                            if(NULL != srCase.Account__c && srCase.Account__c.equals(srCase.CreatedBy.AccountId)){
                                /* srCase.Account__c is owner account of BU */
                                movementSpec.client_type = 'Owner';
                            }
                            else {
                                movementSpec.client_type = 'Tenant';
                            }
                            String moveOutDate = DateTime.newInstance(srCase.Expected_move_out_date__c.year(),
                                                                  srCase.Expected_move_out_date__c.month(),
                                                                  srCase.Expected_move_out_date__c.day()).format('yyyy-MM-dd');
                            movementSpec.moved_on = moveOutDate;
                            moveOutDates.add(movementSpec);
                        } 
                    }
                }
                
                bu_timeline timeLineWrapper = new bu_timeline();
                
                DateTime bookUntSRdate = lstBU[0].Registration_Date__c;
                if(NULL != bookUntSRdate){
                	timeLineWrapper.unit_booking_sr_submission = bookUntSRdate.format('yyyy-MM-dd');
                }
                Date spaExcecutedDt = lstBU[0].Agreement_Date__c;
                if(NULL != spaExcecutedDt){
                	timeLineWrapper.spa_executed = DateTime.newInstance(spaExcecutedDt.year(), 
                                                                        spaExcecutedDt.month(), 
                                                                        spaExcecutedDt.day()).format('yyyy-MM-dd');
                }
                Date pccObtainedDt = lstBU[0].PCC_Date__c;
                if(NULL != pccObtainedDt){
                	timeLineWrapper.payment_clearance = DateTime.newInstance(pccObtainedDt.year(), 
                                                                             pccObtainedDt.month(), 
                                                                             pccObtainedDt.day()).format('yyyy-MM-dd');
                }
                Date handOverNoticeDt = lstBU[0].Handover_Notice_Sent_Date__c;
                if(NULL != handOverNoticeDt){
                	timeLineWrapper.unit_handover_notice_sent_on = DateTime.newInstance(handOverNoticeDt.year(), 
                                                                             handOverNoticeDt.month(), 
                                                                             handOverNoticeDt.day()).format('yyyy-MM-dd');
                }
                
                Date earlyHandOverDt = lstBU[0].Early_Handover_Date__c;
                if(NULL != earlyHandOverDt){
                	timeLineWrapper.unit_early_handover_on = DateTime.newInstance(earlyHandOverDt.year(), 
                                                                             earlyHandOverDt.month(), 
                                                                             earlyHandOverDt.day()).format('yyyy-MM-dd');
                }
                Date handOverDt = lstBU[0].Handover_Date__c;
                if(NULL != handOverDt){
                	timeLineWrapper.unit_handover_on = DateTime.newInstance(handOverDt.year(), 
                                                                             handOverDt.month(), 
                                                                             handOverDt.day()).format('yyyy-MM-dd');
                }
                Date leaseHandOverDt = lstBU[0].Lease_Handover_Date__c;
                if(NULL != leaseHandOverDt){
                	timeLineWrapper.unit_lease_handover_on = DateTime.newInstance(leaseHandOverDt.year(), 
                                                                             leaseHandOverDt.month(), 
                                                                             leaseHandOverDt.day()).format('yyyy-MM-dd');
                }
                
                
                Date keyHandoverDt = lstBU[0].Key_Release_Date__c;
                if(NUll != keyHandoverDt){
                	timeLineWrapper.key_handover = DateTime.newInstance(keyHandoverDt.year(), 
                                                                        keyHandoverDt.month(), 
                                                                        keyHandoverDt.day()).format('yyyy-MM-dd');
                }
                else { /* Added on 23 Sep 2020 to merge all handover dates into single EXOR logic */
                    /* if(NUll != earlyHandOverDt) {
                        timeLineWrapper.key_handover = timeLineWrapper.unit_early_handover_on;
                    }
                    else if(NUll != handOverDt) {
                        timeLineWrapper.key_handover = timeLineWrapper.unit_handover_on;
                    }
                    else if(NUll != leaseHandOverDt) {
                        timeLineWrapper.key_handover = timeLineWrapper.unit_lease_handover_on;
                    }
                    else {
                        timeLineWrapper.key_handover = NULL;
                    } */
                    
                    timeLineWrapper.key_handover = 
                        (NUll != earlyHandOverDt) ? (timeLineWrapper.unit_early_handover_on) : 
                    	( (NUll != handOverDt) ? (timeLineWrapper.unit_handover_on) : 
                          ( (NUll != leaseHandOverDt) ? (timeLineWrapper.unit_lease_handover_on) : 
                            NULL ) );
                }
                
                timeLineWrapper.tenant_registration = tenantRegnDate;
                timeLineWrapper.move_in = moveInDates;
                timeLineWrapper.move_out = moveOutDates;
                
                objData.timeline = timeLineWrapper;
                /* Section added on 3 Sep 2020: Azure ticket 1551 : END */

        		List<String> listHandedOverNames = new List<String>{'Handed Over', 'Early Handed Over'};
        		System.debug('listHandedOverNames:: ' + listHandedOverNames);
                objData.is_unit_handed_over = listHandedOverNames.contains(lstBU[0].Handover_Status__c) ? true : false; 

                //project features addition logic
                if(lstBU[0].Inventory__c != null) {
                    List <String> features = new List<String> ();
                    features.add('Project Features');
                    features.add('About');
                    String invId = lstBU[0].Inventory__c;    
                    Inventory__c invRec = [SELECT Inspection_URL__c,Marketing_Name_Doc__c,Property__c FROM Inventory__c WHERE Id =: invId];            
                    System.debug('invRec::: ' + invRec);
                    if(invRec.Marketing_Name_Doc__c != null ){
                        ID marketingDocId = invRec.Marketing_Name_Doc__c;
                        id propId = invRec.property__c;
                        List<Project_Information__c> projInformation 
                            = Database.query ('SELECT Project_Features__c'
                                              +' FROM Project_Information__c '
                                              + 'WHERE Features__c IN :features AND Marketing_Documents__c = :MarketingDocId AND property__c=:propId');               

                        System.debug('projInformation:: ' + projInformation);  
                        if(projInformation.size() > 0) {
                            for(Project_Information__c objProjInfo : projInformation) {
                                if(String.isNotBlank(objProjInfo.Project_Features__c) && objProjInfo.Project_Features__c != null) {
                                     objData.project_features = objProjInfo.Project_Features__c.contains(';') ? objProjInfo.Project_Features__c.split(';') : new List<String>{objProjInfo.Project_Features__c};
                                }
                               
                            }
                        }              
                    }

                }
                //project features addition logic END

                //Adding lat/long
                objData.latitude = lstBU[0].Inventory__r.Property__r.Latitude__c != null ? lstBU[0].Inventory__r.Property__r.Latitude__c : null;
                objData.longitude = lstBU[0].Inventory__r.Property__r.Longitude__c != null ? lstBU[0].Inventory__r.Property__r.Longitude__c : null;
                //END of adding lat/long

        		objMeta = ReturnMetaResponse('successful', 1);
	            
        	}
        	else {
        		objMeta = ReturnMetaResponse('No Booking Unit record found for given regId', 3);
	            returnResponse.meta_data = objMeta;
	            return returnResponse;
        	}

        }

        returnResponse.meta_data = objMeta;
        returnResponse.data = objData;
        System.debug('returnResponse:: ' + returnResponse);

        return returnResponse;

    }

    public static List<Booking_Unit__c> GetBookingUnit(String regId) {

    	System.debug('regId:: ' + regId);
    	List<Booking_Unit__c> fetchBU = [SELECT id 
    	                                      , Unit_Name__c
    	                                      , Registration_ID__c
    										  , DLP_End_Date__c
    										  , DLP_Start_date__c
    										  , DLP_Validity__c
    										  , Handover_Status__c
                                              , Inventory__c
                                              , Inventory__r.Property__r.Latitude__c
                                              , Inventory__r.Property__r.Longitude__c
                                              , Inventory__r.Building_Location__r.Handover_Manual__c
    										  , Registration_Date__c
    										  , Agreement_Date__c
    										  , Key_Release_Date__c
    										  , PCC_Date__c
    										  , Eligible_for_Handover_Notice_Date__c
    										  , Handover_Notice_Sent_Date__c 
    										  , Early_Handover_Date__c 
    										  , Handover_Date__c 
    										  , Lease_Handover_Date__c 
    										  , (SELECT id, name, Status__c, Request_Type__c, 
                                                 Move_in_date__c, Expected_move_out_date__c, 
                                                 Actual_move_in_date__c, Actual_move_out_date__c, 
                                                 Account__c, CreatedBy.AccountId 
                                                 FROM FM_Cases__r 
                                                 WHERE Request_Type__c IN ('Move In','Move Out','Tenant Registration') 
                                                 ORDER By CreatedDate DESC)
    									FROM Booking_Unit__c
    									WHERE Registration_ID__c =: regId];

        System.debug('fetchBU:: ' + fetchBU);
    	return fetchBU;
    }

    public static cls_meta_data ReturnMetaResponse(String message, Integer statusCode) {
    	cls_meta_data retMeta = new cls_meta_data();

    	retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = null;

        return retMeta;
    }

    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }

    public class cls_data {
        //public cls_get_amenity_list[] amenities;
        public boolean is_under_dlp;
        public String dlp_expiry_date;
        public String dlp_manual_link; /* added on 2Sep 2020 : Azure ticket 1558 */
        public boolean is_unit_handed_over;
        public Decimal latitude;
        public Decimal longitude;
        public List<String> project_features;
        public bu_timeline timeline; /* added on 3Sep 2020 : Azure ticket 1551 */
    }
    
    public class bu_timeline { /* added on 3Sep 2020 : Azure ticket 1551 */
        public String spa_executed;
        public String unit_booking_sr_submission;
        public String payment_clearance;
        public String unit_handover_notice_sent_on; /* added on 14 Sep 2020 */
        public String key_handover;
        public String unit_early_handover_on; /* added on 14 Sep 2020 */
        public String unit_handover_on; /* added on 14 Sep 2020 */
        public String unit_lease_handover_on; /* added on 14 Sep 2020 */
        public String tenant_registration;
        public List<movement_spec> move_in;
        public List<movement_spec> move_out;
    }
    
    public class movement_spec {
        public String client_type;
        public string moved_on;
    }
}