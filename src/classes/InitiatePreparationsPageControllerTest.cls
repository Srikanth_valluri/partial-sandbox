/******************************************************************************
* Description - Test class developed for InitiatePreparationsPageController
*
* Version            Date            Author                    Description
* 1.0                05/02/18       Lochan Karle              Initial Draft
********************************************************************************/

@isTest
public class InitiatePreparationsPageControllerTest{
    @istest static void TestMEthod1(){
       Id LocRecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();

        Location__c objLoc = new Location__c();
        objLoc.Name = 'ABC';
        objLoc.Location_ID__c = '12345';
        objLoc.RecordTypeId = LocRecordTypeId;
        insert objLoc;
            
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();
        TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response1 = new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
        response1.return_x = '{'+
        ''+
        '"message":"message",'+
        '"status":"E"'+
        '}';
       SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
       Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
       
        PageReference pageRef = Page.InitiatePreparationsPage;
        pageRef.getParameters().put('id', String.valueOf(objLoc.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(objLoc);
        InitiatePreparationsPageController controller = new InitiatePreparationsPageController(sc);  
       
        pageRef = controller.createTasks();
        Test.stopTest();
    }
    
    @isTest static void TestMethod2() {    
        
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Id CallListRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Handover Calling List').getRecordTypeId();
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);       
         
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100; 
        BU.Registration_ID__c  = '12345';   
        BU.Booking__c = Booking.Id;                         
        insert BU;
        System.assert(BU != null);         
        
        Case Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        //Cas.Type = 'NOCVisa';
        Cas.Booking_Unit__c = BU.Id;
        insert Cas;
        System.assert(Cas != null);
        
         List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        list<Calling_List__c> lstCallList = new list<Calling_List__c>();
        Calling_List__c objCallList = new Calling_List__c();
        objCallList.Case__c = Cas.Id;
        objCallList.RecordTypeId = CallListRecordTypeId;
        objCallList.Registration_ID__c = BU.Registration_ID__c;
        objCallList.Customer_Flag__c=true;
        objCallList.Booking_Unit__c = BU.Id;
        insert objCallList;
        
        lstCallList.add(objCallList);
        
        test.StartTest();
        /*InitiatePreparationsPageController.createCallingList(lstCallList);
        HO_UnitViewing.createCallingList(lstCallList);
        HO_KeyHandover.createCallingList(lstCallList);*/
        
        Calling_List__c objCLUpdate = new Calling_List__c (Id= objCallList.id);
        objCLUpdate.Documentation_Appointment_Date__c  = Date.newInstance(2018, 19, 2);
        objCLUpdate.Documentation_Appointment_Start_Time__c = '09:00';
        objCLUpdate.Unit_Viewing_Date__c = Date.newInstance(2018, 19, 2);
        objCLUpdate.Viewing_Start_Time__c = '13:00';
        //objCLUpdate.Key_Handover_Appointment_Date__c = Date.newInstance(2018, 19, 2);
        //objCLUpdate.Key_Handover_Appointment_Start_Time__c = '15:00';
        update objCLUpdate;
        test.stopTest();    
    }
    
    @isTest static void TestMethod3() {    
        
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Id CallListRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Handover Calling List').getRecordTypeId();
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);       
         
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100; 
        BU.Registration_ID__c  = '12345';   
        BU.Booking__c = Booking.Id;                         
        insert BU;
        System.assert(BU != null);         
        
        Case Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        //Cas.Type = 'NOCVisa';
        Cas.Booking_Unit__c = BU.Id;
        insert Cas;
        System.assert(Cas != null);
        
         List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        list<Calling_List__c> lstCallList = new list<Calling_List__c>();
        Calling_List__c objCallList = new Calling_List__c();
        objCallList.Case__c = Cas.Id;
        objCallList.RecordTypeId = CallListRecordTypeId;
        objCallList.Registration_ID__c = BU.Registration_ID__c;
        objCallList.Customer_Flag__c=true;
        objCallList.Booking_Unit__c = BU.Id;
        objCallList.Documentation_Appointment_Date__c  = Date.newInstance(2018, 19, 2);
        objCallList.Documentation_Appointment_Start_Time__c = '09:00';
        objCallList.Unit_Viewing_Date__c = Date.newInstance(2018, 19, 2);
        objCallList.Viewing_Start_Time__c = '13:00';
        insert objCallList;
        
        lstCallList.add(objCallList);
        
        test.StartTest();
        
        Calling_List__c objCLUpdate = new Calling_List__c (Id= objCallList.id);
        
        objCLUpdate.Key_Handover_Appointment_Date__c = Date.newInstance(2018, 19, 2);
        objCLUpdate.Key_Handover_Appointment_Start_Time__c = '15:00';
        update objCLUpdate;
        test.stopTest();    
    }
}