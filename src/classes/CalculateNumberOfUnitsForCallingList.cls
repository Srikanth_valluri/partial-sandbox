public class CalculateNumberOfUnitsForCallingList {
   
     public static Map<Id,List<Calling_List__c>>accountIdsToCallingLstUpdateUnits;
     public static List<Calling_List__c>callingListToUpdateLst;
     //public static Map<Id,Integer>AccountIdToCountOfCallingList;
     public static Map<Id,Map<Id,List<Calling_List__c>>>accIdToMapOfRecTypeIdToListOfCalling;
     public static List<Account>listOfAllCallingListOfAllAccounts;
     /*this method is used to calculate Number_of_Units__c */
      
     
     public static void calculateNumberOfUnits(List<Calling_List__c>callingLstToUpdateUnits){
        accIdToMapOfRecTypeIdToListOfCalling = new Map<Id,Map<Id,List<Calling_List__c>>>();
        accountIdsToCallingLstUpdateUnits = new Map<Id,List<Calling_List__c>>();
        Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
     // AccountIdToCountOfCallingList = new Map<Id,Integer>();
        callingListToUpdateLst = new List<Calling_List__c>();
        if(callingLstToUpdateUnits != null && !callingLstToUpdateUnits.isEmpty()){
            for(Calling_List__c callInst : callingLstToUpdateUnits){
                if( callInst.RecordTypeId == collectioncallingRecordTypeId ) {
                    if(!accountIdsToCallingLstUpdateUnits.containsKey(callInst.Account__c)){
                        accountIdsToCallingLstUpdateUnits.put(callInst.Account__c,new List<Calling_List__c>{callInst});
                    }
                    else{
                        accountIdsToCallingLstUpdateUnits.get(callInst.Account__c).add(callInst);
                    }
                }
            }
        }
        System.debug('accountIdsToCallingLstUpdateUnits::all accoun to calling list :::'+accountIdsToCallingLstUpdateUnits);
        Set<Id>accountIdSet = new Set<Id>();
        accountIdSet.addAll(accountIdsToCallingLstUpdateUnits.keySet());
        if(accountIdSet != null && !accountIdSet.isEmpty()){
            listOfAllCallingListOfAllAccounts =[SELECT Id,
                                                        (SELECT Id,
                                                                Name,
                                                                Number_of_Units__c,
                                                                Account__c,
                                                                RecordTypeId
                                                          FROM Calling_List__r
                                                         WHERE IsHideFromUI__c = false
                                                           AND RecordTypeId =: collectioncallingRecordTypeId
                                                           )
                                                  FROM Account
                                                  WHERE Id IN :accountIdSet];
        }
        System.debug('listOfAllCallingListOfAllAccounts::'+listOfAllCallingListOfAllAccounts);
        if(listOfAllCallingListOfAllAccounts != null && !listOfAllCallingListOfAllAccounts.isEmpty()){
            for(Account accObj : listOfAllCallingListOfAllAccounts){
                Map<Id,List<Calling_List__c>>RecordTypeIdToLstOfCallingInnerMap = new Map<Id,List<Calling_List__c>>();
                if(accObj.Calling_List__r != null && !accObj.Calling_List__r.isEmpty()){
                    RecordTypeIdToLstOfCallingInnerMap = new Map<Id,List<Calling_List__c>>();
                    for(Calling_List__c callObj : accObj.Calling_List__r){
                        if( callObj.RecordTypeId == collectioncallingRecordTypeId ) {
                            if(!RecordTypeIdToLstOfCallingInnerMap.containsKey(callObj.RecordTypeId)){
                                RecordTypeIdToLstOfCallingInnerMap.put(callObj.RecordTypeId,new List<calling_List__c>{callObj});
                            }
                            else{
                                RecordTypeIdToLstOfCallingInnerMap.get(callObj.RecordTypeId).add(callObj);
                            }
                        }
                    }
                }
                accIdToMapOfRecTypeIdToListOfCalling.put(accObj.Id,RecordTypeIdToLstOfCallingInnerMap);
            }
        }
        /**/
        if(accIdToMapOfRecTypeIdToListOfCalling != null && !accIdToMapOfRecTypeIdToListOfCalling.isEmpty()){
            for(Id accId : accIdToMapOfRecTypeIdToListOfCalling.keySet()){
                Map<Id,List<Calling_List__c>>recIdToLstCalling = new Map<Id,List<Calling_List__c>>();
                recIdToLstCalling.putAll(accIdToMapOfRecTypeIdToListOfCalling.get(accId));
                for(Id recId : recIdToLstCalling.keySet()){
                    List<Calling_List__c>callingLst = new List<Calling_List__c>();
                     callingLst.addAll(recIdToLstCalling.get(recId));
                        for(Calling_List__c callObj : callingLst){
                            callObj.Number_of_Units__c = callingLst.size();
                            callingListToUpdateLst.add(callObj); 
                        }
                    }
                }
            }
            /**/
            Database.update(callingListToUpdateLst,false);
        }
  }