/**********************************************************************************************************************
Class Name : API_GetCommisionSubTotalsPlotData 
Description: To fetch graph cordinate data for plotting the topper list of commision amounts by period/project/agent.
Test Class : API_GetCommisionSubTotalsForAgentsTest
========================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.1     |   15-12-2020      | Subin C Antony	  | Initial Draft
***********************************************************************************************************************/

@RestResource(urlMapping='/API_GetAgentCommisionSubTotalPlots')
global without sharing class API_GetCommisionSubTotalsPlotData {
    private static final Integer PLOT_POINTS_LIMIT = 7;

    @HttpGET
    global static void doGET() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg;
        
        String graphType = req.params.containskey('graph_type') ? req.params.get('graph_type') : '';
        if(String.isBlank(graphType)){
            errorMsg = 'Please provide the graph-type for plotting.';
            getErrorResponse(3, errorMsg, errorMsg);
            return;
        }
        
        String period = req.params.containskey('period') ? req.params.get('period') : '';
        if(String.isBlank(period)){
            errorMsg = 'Please choose the time period for which the results should be fetched.';
            getErrorResponse(3, errorMsg, errorMsg);
            return;
        }
        
        Set <ID> agentIds = new Set <ID> ();
        Set <ID> contactIds = new Set <ID> ();
        User u = [SELECT ContactId, profile.Name, AccountId FROM User WHERE ID =: UserInfo.getUserId()];
        agentIds.add (UserInfo.getUserId());
        contactIds.add (u.contactId);
        
        // To fetch the account related contacts and thereby, those contact realted user's when the API request is coming from a Super User Profile
        if (u.profile.Name == 'Customer Community - Super User' || Test.isRunningTest()) {
            Set <ID> conIds = new Set <ID> ();
            for (Contact con : [SELECT ID FROM Contact WHERE accountId = :u.accountId]) {
                conIds.add (con.ID);
            }
            
            for (User usr : [SELECT ContactId, AccountID FROM User WHERE ContactId IN :conIds AND IsActive = TRUE]) {
                contactIds.add (usr.contactId);
                agentIds.add (usr.id);
            }
        }
        
        // to get the total collections, booking amount from SR
        Set <ID> srIds = new Set <ID> ();
        if(u.profile.Name == 'Customer Community - Super User' || Test.isRunningTest()) {
            for (NSIBPM__Service_Request__c sr : [SELECT NSIBPM__Internal_Status_Name__c, Total_Booking_Amount__c, Token_Amount_AED__c, List_of_Units__c 
                                                    FROM NSIBPM__Service_Request__c 
                                                    WHERE 
                                                    NSIBPM__External_Status_Code__c != 'Draft'
                                                    AND 
                                                    Agency__r.Id=:u.accountId]) /* OR NSIBPM__Contact__c IN: contactIds */ {
                srIds.add (sr.Id); /* sr.Total_Booking_Amount__c */
            }
        } /* (u.profile.Name == 'Customer Community - Super User' || Test.isRunningTest()) */
        
        if(u.profile.Name != 'Customer Community - Super User' || Test.isRunningTest()) {
            for (NSIBPM__Service_Request__c sr : [SELECT NSIBPM__Internal_Status_Name__c, Total_Booking_Amount__c, Token_Amount_AED__c, List_of_Units__c 
                                                  FROM NSIBPM__Service_Request__c 
                                                  WHERE 
                                                  NSIBPM__External_Status_Code__c != 'Draft'
                                                  AND 
                                                  (Agent_Name__c IN: agentIds)]) /* OR NSIBPM__Contact__c IN: contactIds */ {
                srIds.add (sr.Id); /* sr.Total_Booking_Amount__c */
            }
        } /* (u.profile.Name != 'Customer Community - Super User' || Test.isRunningTest()) */
        
        System.debug('SR_IDs .............................: ' + srIds);
        
        String query = 'SELECT Agency__r.name, Broker_Name__c, Amount__c, DP_Amount__c, DP_Amount_Received__c, Percent_Applied__c, ' + 
            			'Booking_Unit__c, Booking_Unit__r.Unit_Location__c, Booking_Unit__r.Property_Name_Inventory__c, ' + 
            			'Booking_Unit__r.Registration_Date__c, Booking_Unit__r.Registration_ID__c, ' + 
            			'Booking__r.Deal_Sr__r.NSIBPM__External_Status_Code__c, Booking__r.Deal_SR__r.Agent_Name__c, ' + 
            			'Booking__r.Deal_SR__r.Agent_Name__r.name ' + 
            			'FROM Agent_Commission__c WHERE Booking__r.Deal_SR__c IN :srIds AND Amount__c != null';
        if(period.equalsIgnoreCase('LAST_MONTH')) {
            query += ' AND Booking_Unit__r.Registration_Date__c >= LAST_MONTH';
        }
        else if(period.equalsIgnoreCase('LAST_3_MONTHS')) {
            query += ' AND Booking_Unit__r.Registration_Date__c >= LAST_N_MONTHS:3';
        }
        else if(period.equalsIgnoreCase('LAST_6_MONTHS')) {
            query += ' AND Booking_Unit__r.Registration_Date__c >= LAST_N_MONTHS:6';
        }
        else if(period.equalsIgnoreCase('LAST_12_MONTHS')) {
            query += ' AND Booking_Unit__r.Registration_Date__c >= LAST_N_MONTHS:12';
        }
        else if(period.equalsIgnoreCase('LAST_4_QUARTERS')) {
           query += ' AND Booking_Unit__r.Registration_Date__c >= LAST_N_FISCAL_QUARTERS:4'; 
        }
        query += ' ORDER BY Booking_Unit__r.Registration_Date__c DESC';
        
        List<Agent_Commission__c> commisionList;
        try {
            commisionList = (List<Agent_Commission__c>) Database.query(query);
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            errorMsg = 'Error while trying to fetch results';
            getErrorResponse(2, errorMsg, (ex.getMessage() + ' : ' + ex.getStackTraceString()));
            return;
        }
        
        if(NULL == commisionList || commisionList.size() < 1) {
            errorMsg = 'No results to display.';
            getErrorResponse(2, errorMsg, NULL);
            return;
        }
        
        ResponseWrapper responseWrapper = new ResponseWrapper();
        
        
        HDApp_Utility.cls_meta_data responseMetaData = HDApp_Utility.ReturnMetaResponse(
            'Success in fetching sub-totals for agent collection', NULL, 1);
        responseWrapper.meta_data = responseMetaData;
        
        cls_data responseData = new cls_data();
        /* List<item_wrap> commissionTileDataList = new List<item_wrap>();
        for(Agent_Commission__c commision : commisionList) {
            String agentName = (NULL == commision.Booking__r.Deal_SR__r.Agent_Name__c ? 
                                '' : commision.Booking__r.Deal_SR__r.Agent_Name__r.name); // commision.Broker_Name__c
            String agencyName = (NULL == commision.Agency__c ? '' : commision.Agency__r.name);
            String projectName = (NULL == commision.Booking_Unit__c ? '' : commision.Booking_Unit__r.Property_Name_Inventory__c);
            String unitName = (NULL == commision.Booking_Unit__c ? '' : commision.Booking_Unit__r.Unit_Location__c);
            Decimal commisionAmount = (NULL == commision.Amount__c ? 0.00 : commision.Amount__c);
            Decimal dealAmount = (NULL == commision.DP_Amount__c ? 0.00 : commision.DP_Amount__c);
            Decimal commisionRate = (dealAmount <= 0.00 ? 0.00 : (100 * commisionAmount)/dealAmount);
            commisionRate = commisionRate.setScale(2, RoundingMode.HALF_UP);
            Date salesDate = commision.Booking_Unit__r.Registration_Date__c;
            DateTime tempDT = (NULL == salesDate ? NULL : DateTime.newInstance(salesDate, Time.newInstance(12, 0, 0, 0)));
            
            item_wrap commissionData = new item_wrap();
            commissionData.project_name = projectName;
            commissionData.unit_name = unitName;
            commissionData.agent_name = agentName;
            commissionData.agency_name = agencyName;
            commissionData.sale_date_string = (NULL == tempDT ? '' : tempDT.format('yyyy-MM-dd'));
            commissionData.sale_date = salesDate;
            commissionData.agent_commision = commisionAmount;
            commissionData.agent_commision_rate = commisionRate;
            
            commissionTileDataList.add(commissionData);
        }
        responseData.tile_data = commissionTileDataList; */
        
        Map<String, Decimal> commisionPlotDataMap = new Map<String, Decimal>();
        for(Agent_Commission__c commision : commisionList) {
            String plotKey;
            if(graphType.equalsIgnoreCase('DEFAULT')) {
                Date salesDate = commision.Booking_Unit__r.Registration_Date__c;
            	DateTime tempDT = (NULL == salesDate ? NULL : DateTime.newInstance(salesDate, Time.newInstance(12, 0, 0, 0)));
                if(period.equalsIgnoreCase('LAST_4_QUARTERS')) {
                	plotKey = getQuarterOfYearFromDate(salesDate);
                }
                else {
                    if(NULL != tempDT) {
                        plotKey = (tempDT.format('yyyy') + ' ' + tempDT.format('MMM'));
                    } else {
                        plotKey = NULL;
                    }
                }
            }
            else if(graphType.equalsIgnoreCase('TOP_AGENTS')) {
                String agentName = (NULL == commision.Booking__r.Deal_SR__r.Agent_Name__c ? 
                                NULL : commision.Booking__r.Deal_SR__r.Agent_Name__r.name);
                plotKey = API_GetAgentCommisionDetails.getInProperCase(agentName);
            }
            else if(graphType.equalsIgnoreCase('TOP_PROJECTS')) {
                String projectName = (NULL == commision.Booking_Unit__c ? NULL : commision.Booking_Unit__r.Property_Name_Inventory__c);
                plotKey = API_GetAgentCommisionDetails.getInProperCase(projectName);
            }
            
            Decimal commisionAmount = (NULL == commision.Amount__c ? 0.00 : commision.Amount__c);
            if(NULL != plotKey && commisionPlotDataMap.containsKey(plotKey)) {
                Decimal plotValue = (commisionPlotDataMap.get(plotKey) + commisionAmount);
                commisionPlotDataMap.put(plotKey, plotValue);
            }
            else if(NULL != plotKey) {
                commisionPlotDataMap.put(plotKey, commisionAmount);
            }
        }
        
        List<String> plotKeyList = new List<String>(commisionPlotDataMap.keySet());
        plotKeyList.sort();
        List<plot_point> commissionReportPlotPointsList = new List<plot_point>();
        for(String mapKey : plotKeyList) {
            plot_point plotPoint = new plot_point();
            plotPoint.x_label = mapKey;
            plotPoint.y_value = commisionPlotDataMap.get(mapKey);
            
            commissionReportPlotPointsList.add(plotPoint);
        }
        
        if(!graphType.equalsIgnoreCase('DEFAULT')) {
            commissionReportPlotPointsList.sort(); 
            while(commissionReportPlotPointsList.size() > PLOT_POINTS_LIMIT) {
                commissionReportPlotPointsList.remove(PLOT_POINTS_LIMIT);
            }
        }
        
        responseData.plot_data = commissionReportPlotPointsList;
        responseWrapper.data = responseData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    } /* doGET() */
    
    private static String getQuarterOfYearFromDate(Date dateObj) {
        if(NULL == dateObj) {return NULL;}
        
        String quarter = '';
        if(dateObj.month() <= 3) {quarter = (dateObj.year() + ' Q1');}
        else if(dateObj.month() > 3 && dateObj.month() <= 6) {quarter = (dateObj.year() + ' Q2');}
        else if(dateObj.month() > 6 && dateObj.month() <= 9) {quarter = (dateObj.year() + ' Q3');}
        else if(dateObj.month() > 9 && dateObj.month() <= 12) {quarter = (dateObj.year() + ' Q4');}
        
        return quarter;
    }
    
    private static void getErrorResponse(Integer statusCode, String responseMessage, String devMessage) {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        HDApp_Utility.cls_meta_data responseMetaData = HDApp_Utility.ReturnMetaResponse(
            responseMessage, devMessage, statusCode);
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }

    /* Wrapper classes for returning reponse */
    public class ResponseWrapper {
        public HDApp_Utility.cls_meta_data meta_data;
        public cls_data data;
    }

    public class cls_data {
        public plot_point[] plot_data;
        /* public item_wrap[] tile_data; */
    }

    public class plot_point implements Comparable {
        public String x_label;
        public Decimal y_value;
        
        public Integer compareTo(Object compareTo) {
            if(NULL == compareTo) {return -1;} 
            else {
                plot_point toCompare = (plot_point)compareTo;
                if(NULL != this.y_value && NULL != toCompare.y_value && 
                this.y_value > toCompare.y_value) {return -1;}
                else if(NULL != this.y_value && NULL != toCompare.y_value && 
                this.y_value == toCompare.y_value) {return 0;}
                else {return 1;}
            }
        }
    }

    /*
     public class item_wrap {
        public String project_name;
        public String unit_name;
        public String agent_name;
        public String agency_name;
        public String sale_date_string;
        public Date sale_date;
        public Decimal agent_commision;
        public Decimal agent_commision_rate;
    } */
}