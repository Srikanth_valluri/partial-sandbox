@isTest
private class ViolationNoticeInvocableTest {

	@isTest static void test_method_one() {
		FM_Case__c fmObj = new FM_Case__c();
		Id RecordTypeIdFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Violation Notice').getRecordTypeId();
    fmObj.recordtypeid = RecordTypeIdFMCase;
    insert fmObj;
		Task taskObj = new Task();
		taskObj.Description = 'Test'; //string
    taskObj.Status ='Completed';
    taskObj.Subject = Label.TaskToCreateVNForPM;
    taskObj.WhatId = fmObj.id; //record id
    insert taskObj;
        ViolationNoticeInvocable.closeViolationNoticeFMCase(new List<Task>{taskObj});
	}



}