/*******************************************************************************************************
Description : Class to generate Addedndum document from drawloop for VAT HO SR
--------------------------------------------------------------------------------------------------------
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
--------------------------------------------------------------------------------------------------------
1.0     | 13-02-2020        |Aishwarya Todkar    | Initial Draft
*****************************************************************************************************/

public Class VAT_AddendumDocGenCase {

    
    Static Id CaseId;
    
    public VAT_AddendumDocGenCase(ApexPages.StandardController controller) {
        caseId = ( ( Case )Controller.getRecord() ).Id;
    }

/********************************************************************************************************
    Method to generate VAT Addedndum on Case from button 'Generate Addendum'
*********************************************************************************************************/
    public static void generateVAT() {

        List<Case> listCases = new List<Case>( [SELECT
                                                    Id
                                                    , Booking_Unit__c
                                                    , Booking_Unit__r.Permitted_Use_Type__c
                                                FROM
                                                    Case
                                                WHERE
                                                    Id =: caseId ] );
        if( !listCases.isEmpty() ) {
            Case objCase = listCases[0];
            String ddpId ='', deliveryId = '';

            if( objCase.Booking_Unit__r.Permitted_Use_Type__c.equalsIgnoreCase( 'Units With VAT' ) ) {
                
                Riyadh_Rotana_Drawloop_Doc_Mapping__c cs 
                    = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( 'Unit with HO VAT Addendum-Case' );
                
                System.debug( 'spa cs 1-' + cs);
                
                if(cs != null
                && String.isNotBlank( cs.Drawloop_Document_Package_Id__c ) 
                && String.isNotBlank( cs.Delivery_Option_Id__c )) {
                    ddpId = cs.Drawloop_Document_Package_Id__c;
                    deliveryId = cs.Delivery_Option_Id__c;
                }
                else {
                    ApexPages.addmessage( 
                        new ApexPages.message(ApexPages.severity.Error
                        ,'Please configure \'Unit with HO VAT Addendum-Case\' DDP details properly.')
                    );
                }
            }
            else if( objCase.Booking_Unit__r.Permitted_Use_Type__c.equalsIgnoreCase( 'Units Without VAT' ) ) {
                
                Riyadh_Rotana_Drawloop_Doc_Mapping__c cs 
                    = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( 'Unit w/o HO VAT Addendum-Case' );
                
                System.debug( 'spa cs 2-' + cs);
                
                if(cs != null
                && String.isNotBlank( cs.Drawloop_Document_Package_Id__c ) 
                && String.isNotBlank( cs.Delivery_Option_Id__c )) {
                    ddpId = cs.Drawloop_Document_Package_Id__c;
                    deliveryId = cs.Delivery_Option_Id__c;
                }
                else {
                    ApexPages.addmessage( new ApexPages.message(ApexPages.severity.Error
                        ,'Please configure \'Unit w/o HO VAT Addendum-Case\' DDP details properly.')
                    );
                }
            }

            if( String.isNotBlank( ddpId ) 
            && String.isNotBlank( deliveryId ) 
            && !Test.isRunningTest() ) {

                Database.ExecuteBatch( new GenerateDrawloopDocumentBatch( caseId , ddpId, deliveryId ) );

            }// End SPA Addendum generation
            ApexPages.addmessage(
                new ApexPages.message( ApexPages.severity.INFO, 
                'Your request was successfully submitted. Please check the documents section for the document in a while.')
            );
        }
    }

/********************************************************************************************************
    Method to upload VAT Addendum to central repo.
*********************************************************************************************************/    
    @future( callout = true )
    public static void uploadVAT( List<Id> listAttachId ) {
        
        if( !listAttachId.isEmpty() ) {
            Attachment objAtt = [ SELECT
                                        Id
                                        , Body
                                        , Name
                                        , ParentId
                                    FROM
                                        Attachment
                                    WHERE
                                        Id =: listAttachId[0] 
                                ];
            Id caseId = objAtt.ParentId;
            List<SR_Attachments__c> lstSRAttachment = new List<SR_Attachments__c>();
            List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocReq = new  List<UploadMultipleDocController.MultipleDocRequest>();
            
            UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();
            
            UploadMultipleDocController.MultipleDocRequest reqObjCRF = new UploadMultipleDocController.MultipleDocRequest();
            reqObjCRF.base64Binary = EncodingUtil.base64Encode(objAtt.Body);
            reqObjCRF.category = 'Document';
            reqObjCRF.entityName = 'VAT Addendum.pdf';
            reqObjCRF.fileDescription = 'VAT Addendum.pdf';
            reqObjCRF.fileId = caseId + '-VAT Addendum.'+'pdf';
            reqObjCRF.fileName = caseId +'-VAT Addendum.'+'pdf';
            reqObjCRF.registrationId = caseId;
            reqObjCRF.sourceFileName = 'IPMS-' + caseId + '-' + 'CallTranscript'+'.'+'pdf';
            reqObjCRF.sourceId = 'IPMS-' + caseId + '-' + 'CallTranscript';
            lstMultipleDocReq.add(reqObjCRF);
            
            SR_Attachments__c objAttach = new SR_Attachments__c();
            objAttach.Case__c  = caseId;
            objAttach.isValid__c = true;
            objAttach.IsRequired__c = true;
            objAttach.Name = 'VAT Addendum';
            objAttach.Need_Correction__c =  false;
            lstSRAttachment.add(objAttach);
            
            System.debug( 'VAT lstSRAttachment == ' + lstSRAttachment );
            
            if( !Test.isRunningTest() ) {
                MultipleDocData = UploadMultipleDocController.getMultipleDocUrl( lstMultipleDocReq );
            }
            else {
                            
                List<UploadMultipleDocController.MultipleDocResponse> data = new List<UploadMultipleDocController.MultipleDocResponse>();
                UploadMultipleDocController.MultipleDocResponse objMultipleDocResponse = new UploadMultipleDocController.MultipleDocResponse();
                objMultipleDocResponse.PROC_STATUS = 'S';
                objMultipleDocResponse.url = 'www.google.com';
                data.add(objMultipleDocResponse);
                
                MultipleDocData.Data = data;
            }
            System.debug( 'VAT MultipleDocData == ' + MultipleDocData );

            if( MultipleDocData != null 
            && MultipleDocData.status != 'Exception'
            && (MultipleDocData.Data != null || MultipleDocData.Data.Size() > 0)) {
                system.debug('MultipleDocData '+ MultipleDocData );
                for( Integer objInt = 0; objInt < lstSRAttachment.Size(); objInt++ ) {
                    UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[objInt];
                    system.debug('objData '+ objData );
                    if( objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url) ) {
                        lstSRAttachment[objInt].Attachment_URL__c = objData.url;
                    }
                }
            }
            
            if( !lstSRAttachment.isEmpty() )
                insert lstSRAttachment;
        }
    }
}