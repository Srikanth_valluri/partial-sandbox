Public class Damac_FleetRootService{

    public static HttpResponse getVehicleLocationByImeiNo(String imeiNo) {
        Fleet_Root_API__c credentials = Fleet_Root_API__c.getInstance(UserInfo.getUserID());
        String endpoint = credentials.Endpoint_Url__c+'api/getvehiclelocation?apiKey='+credentials.API_key__c+'&imeino='+imeiNo;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeOut (120000);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest())
            res = http.send(req);
        System.Debug(res.getBody());
        return res;
    }

    public static void getVehicleLocationByVehicalId(String vehicleId) {
        Fleet_Root_API__c credentials = Fleet_Root_API__c.getInstance(UserInfo.getUserID());
        String endpoint = credentials.Endpoint_Url__c+'api/getvehiclelocation?apiKey='+credentials.API_key__c+'&id='+vehicleId;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeOut (120000);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest())
            res = http.send(req);
        System.Debug(res.getBody());
    }

    public static HttpResponse getVechicleCurrentStatus(String vehicleId) {
        Fleet_Root_API__c credentials = Fleet_Root_API__c.getInstance(UserInfo.getUserID());
        String endpoint = credentials.Endpoint_Url__c+'api/getvehiclecurrentstatus?apiKey='+credentials.API_key__c+'&vehicleID='+vehicleId;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeOut (120000);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest())
            res = http.send(req);
        System.Debug(res.getBody());
        return res;
    }

    public static HttpResponse getVehicleHistory(String vehicleId, String fromDate, String toDate) {
        Fleet_Root_API__c credentials = Fleet_Root_API__c.getInstance(UserInfo.getUserID());
        String endpoint = credentials.Endpoint_Url__c + 'api/getvehiclehistory?apiKey='+ credentials.API_key__c
                        +'&VehicleID='+vehicleId+'&fromDate='+fromDate+'&toDate='+toDate;
       
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeOut (120000);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest())
        res = http.send(req);
        System.Debug(res.getBody());
        return res;
    }

    public static void getActiveVehicleSnapshotinAccountSG() {
        Fleet_Root_API__c credentials = Fleet_Root_API__c.getInstance(UserInfo.getUserID());
        String endpoint = credentials.Endpoint_Url__c + 'api/getactivevehiclesnapshotinaccountSG?apiKey='+ credentials.API_key__c;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeOut (120000);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest())
        res = http.send(req);
        System.Debug(res.getBody());
    }

    public static void getAlerts(String dateVal) {
        Fleet_Root_API__c credentials = Fleet_Root_API__c.getInstance(UserInfo.getUserID());
        String endpoint = credentials.Endpoint_Url__c + 'api/getalerts?apiKey='+ credentials.API_key__c+'&date='+dateVal;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeOut (120000);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest())
        res = http.send(req);
        System.Debug(res.getBody());
    }

    public static void getAlertsByVehicles(String vehicleId, String fromDate, String toDate, String vehicleAlertType) {
        Fleet_Root_API__c credentials = Fleet_Root_API__c.getInstance(UserInfo.getUserID());
        String endpoint = credentials.Endpoint_Url__c + 'api/getalertsbyvehicle?apiKey='+ credentials.API_key__c+'&VehicleID='
                        +vehicleId+'&fromDate='+fromDate
                        +'&toDate='+toDate+'&VehicleAlertType='+vehicleAlertType;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeOut (120000);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest())
        res = http.send(req);
        System.Debug(res.getBody());
    }

    public static void getDrivers() {
        Fleet_Root_API__c credentials = Fleet_Root_API__c.getInstance(UserInfo.getUserID());
        String endpoint = credentials.Endpoint_Url__c + 'api/getdrivers?apiKey='+ credentials.API_key__c;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeOut (120000);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest())
        res = http.send(req);
        System.Debug(res.getBody());
    }

    public static void getIdleVehicles() {
        Fleet_Root_API__c credentials = Fleet_Root_API__c.getInstance(UserInfo.getUserID());
        String endpoint = credentials.Endpoint_Url__c + 'api/getdisconnectedvehicles?apiKey='+ credentials.API_key__c;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeOut (120000);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest())
        res = http.send(req);
        System.Debug(res.getBody());
    }

    public static void getStoppedVehicles() {
        Fleet_Root_API__c credentials = Fleet_Root_API__c.getInstance(UserInfo.getUserID());
        String endpoint = credentials.Endpoint_Url__c + 'api/getstoppedvehicles?apiKey='+ credentials.API_key__c;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeOut (120000);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest())
        res = http.send(req);
        System.Debug(res.getBody());
    } 

    public static HttpResponse getRunningVehicles() {
        Fleet_Root_API__c credentials = Fleet_Root_API__c.getInstance(UserInfo.getUserID());
        String endpoint = credentials.Endpoint_Url__c + 'api/getrunningvehicles?apiKey='+ credentials.API_key__c;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeOut (120000);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest())
        res = http.send(req);
        System.Debug(res.getBody());
        return res;
    } 

    public static void getDisconnectedVehicles() {
        Fleet_Root_API__c credentials = Fleet_Root_API__c.getInstance(UserInfo.getUserID());
        String endpoint = credentials.Endpoint_Url__c + 'api/getdisconnectedvehicles?apiKey='+ credentials.API_key__c;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeOut (120000);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest())
        res = http.send(req);
        System.Debug(res.getBody());
    }  

    public static void getVechicleGroups() {
        Fleet_Root_API__c credentials = Fleet_Root_API__c.getInstance(UserInfo.getUserID());
        String endpoint = credentials.Endpoint_Url__c + 'api/getvhiclegroups?apiKey='+ credentials.API_key__c;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeOut (120000);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest())
        res = http.send(req);
        System.Debug(res.getBody());
    }    

    public static void getassets() {
        Fleet_Root_API__c credentials = Fleet_Root_API__c.getInstance(UserInfo.getUserID());
        String endpoint = credentials.Endpoint_Url__c + 'api/getassets?apiKey='+ credentials.API_key__c;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeOut (120000);

        Http http = new Http();
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest())
        res = http.send(req);
        System.Debug(res.getBody());
    }

    public static void getAllVechiclesCurrentStatus() {
        Fleet_Root_API__c credentials = Fleet_Root_API__c.getInstance(UserInfo.getUserID());
        String endpoint = credentials.Endpoint_Url__c + 'api/getallvehiclecurrentstatus?apiKey=' + credentials.API_key__c;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setTimeOut (120000);
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest())
        res = http.send(req);
        System.Debug(res.getBody());
    }

}