public without sharing class FetchPOPCasesForCallingListController {
    public String callingListId {get;set;}
    public Id caseRecordTypeId {get;set;}
    public Id callingListRecordTypeId {get;set;}
    public List<Calling_List__c>callingList {get;set;}
    public List<Id>accList {get;set;}
    public List<Case>caseList {get;set;}
    public FetchPOPCasesForCallingListController(ApexPages.StandardController controller) {
        fetchCases();
    }
    /*This Method is used to fetch all the cases of POP record type*/
    public void fetchCases(){
        callingList = new List<Calling_List__c>();
        Id bouncedChequeRTId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Bounced Cheque').RecordTypeId;
        accList = new List<Id>();
        caseList = new List<Case>();
        caseRecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('POP').RecordTypeId;
        callingListRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
        callingListId = ApexPages.currentPage().getParameters().get('id');
        callingListId = String.valueOf(callingListId).substring(0, 15);
        callingList = [SELECT Id,
                            Name,
                            Account__r.Name,
                            Account__c,
                            RecordTypeId,
                            Case__c,
                            RecordType.Name
                       FROM Calling_List__c
                      WHERE Id = :callingListId
                       ];//AND RecordTypeId = :callingListRecordTypeId
       //Method to display Cases for Bounced Cheque record type
       if( !callingList.isEmpty() && callingList[0].RecordTypeId == bouncedChequeRTId && callingList[0].Case__c != NULL) {
           displayBouncedCheque_POPCases(callingList[0].Case__c);
       }
       else {

            for(Calling_List__c callingInst : callingList){
            	if( callingInst.Account__c != null )
                	accList.add(callingInst.Account__c);
            }
            System.debug('accList::::'+accList);
             if(accList != null && accList.isEmpty() == false ) {
    	        	caseList = [SELECT Id,
    	                           caseNumber,
    	                           //Status,
    	                          // RecordType.Name,
    	                           AccountId,
    	                           Total_Amount__c,
    	                           Payment_Date__c,
    	                           Payment_Mode__c
    	                      FROM Case
    	                     WHERE AccountId IN :accList
    	                       AND RecordTypeId = :caseRecordTypeId
    	                  ORDER BY CreatedDate 
                             desc
                             limit 950
    	                       ];
    	         System.debug('caseList::in pop::'+caseList);
            }
       }//else
   }//fetchCases

   /*
     @ Description : Method to get POP Cases for Bounced Cheque Calling List
	*/
    public void displayBouncedCheque_POPCases(Id caseId) {
        //Get SR_Booking_Unit__c for the caseId
        Map<ID,List<Case>> mapBUId_Cases = new Map<ID,List<Case>>();
        List<String> lstBUIds = new List<String>();
        for(SR_Booking_Unit__c srBU : [SELECT
                                        Booking_Unit__c
                                        FROM SR_Booking_Unit__c
                                        WHERE Case__c =: caseId]) {
            lstBUIds.add(srBU.Booking_Unit__c);
        }
        mapBUId_Cases = openSRUtility.getOpenSR(lstBUIds, String.valueOf(caseId));
        caseList = new List<Case>();
        List<Case> temp_caseList = new List<Case>();
        for(Id BUId : mapBUId_Cases.keySet()) {
            temp_caseList.addAll(mapBUId_Cases.get(BUId));
        }

        //Adding POP Cases record type
        for(Case objCase : temp_caseList) {
            if(objCase.RecordTypeId == caseRecordTypeId) {
                caseList.add(objCase);
            }

        }

    }

}