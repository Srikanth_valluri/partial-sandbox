@isTest
private class CommunityPaymentSuccessExtensionTest {
    @isTest
    static void testExtension() {
        Account account = TestDataFactoryFM.createAccount();
        account.Email__pc = 'test@test.com';
        insert account;
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(account);
        insert sr;
        Booking__c booking = TestDataFactoryFM.createBooking(account, sr);
        insert booking;
        Booking_unit__c bookingUnit = TestDataFactoryFM.createBookingUnit(account, booking);
        insert bookingUnit;


        FM_Receipt__c fmReceipt = new FM_Receipt__c();
        insert fmReceipt;
        insert new FM_Unit_Invoice_Payment__c(
            Booking_Unit__c = bookingUnit.Id,
            FM_Receipt__c = fmReceipt.Id
        );

        Test.startTest();
            CommunityPaymentSuccessExtension extension = new CommunityPaymentSuccessExtension(
                new ApexPages.StandardController(fmReceipt)
            );
        Test.stopTest();

        System.assert(String.isNotBlank(extension.unitNames));
    }
}