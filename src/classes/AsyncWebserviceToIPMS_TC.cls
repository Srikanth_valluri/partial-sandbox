@isTest
public class AsyncWebserviceToIPMS_TC {
    static testMethod void IPMS() {
        Unit_Assignment__c uAObj = new Unit_Assignment__c();
        uAObj.Start_Date__c = Date.parse('11/12/17');
        uAObj.End_Date__c =  Date.parse('11/12/18');
        uAObj.Unit_Assignment_Name__c = 'UA-Test';
        uAObj.Reason_For_Unit_Assignment__c = 'Reason of UA';
        insert uAObj;
        
        Inventory__c inventory;
        List<Inventory__c> inventoriesList = new  List<Inventory__c>();
        for(Integer i = 0; i <= 40; i++) {
            inventory = InitialiseTestData.getInventoryDetails('345'+i,'123'+i,'234'+i,908+i,765+i); 
            inventory.Tagged_To_Unit_Assignment__c = false;
            inventory.Is_Assigned__c = false;
            inventory.Status__c = 'Released';
            
            inventory.Unit_Type__c = 'HOTEL APARTMENTS';
            inventory.Floor__c = 'Floor';
            inventory.Unit__c = 'Unit';
            
            inventoriesList.add(inventory);
        }
        insert inventoriesList ;
        
        AsyncWebserviceToIPMS.createUAJSON(inventoriesList, uAObj, false, false);
        AsyncWebserviceToIPMS.createUAJSON(inventoriesList, uAObj, false, true);
        IPMSJSONCreation obj = new IPMSJSONCreation ();
        obj.sourceSystem = 'SFDC';
        obj.extRequestNumber = uAObj.Name;
        List <IPMSJSONCreation.blockLines> items = new List <IPMSJSONCreation.blockLines>();
        IPMSJSONCreation.blockLines item = new IPMSJSONCreation.blockLines ();
        item.subRequestName = 'UNIT_BLOCK_AVL';
        items.add(item);
        obj.blockLines = items;
        IPMSJSONCreation.parse(JSON.serializePretty(obj));
        String jsonBody = '{"responseLines": [{"status": "S"}]}';
        AsyncWebserviceToIPMS.parseResponse (jsonBody);
    }
}