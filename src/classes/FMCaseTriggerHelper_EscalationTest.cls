@isTest
private class FMCaseTriggerHelper_EscalationTest {
    @testSetup
    static void testDataCreation() {
        Booking_Unit__c bu1 = new Booking_Unit__c();
        Account a = new Account();
      Booking__c  bk = new  Booking__c();
      Location__c locObj = new Location__c();
        a.Name = 'Test Account';
    a.party_ID__C = '1039032';
    insert a;
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
    Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
    sr.NSIBPM__Customer__c = a.id;
    sr.RecordTypeId = RecType;
    insert sr;

    bk.Account__c = a.id;
    bk.Deal_SR__c = sr.id;
    insert bk;
        bu1.Booking__c = bk.id;
    bu1.Unit_Name__c = 'LSB/10/B1001';
    bu1.Owner__c = a.id;
    bu1.Property_City__c = 'Dubai';
    insert bu1;

    locObj.Name  = 'LSB';
    locObj.Location_ID__c = '83488';
    insert locObj;

        FM_User__c userObj = new FM_User__c();
        userObj.Building__c = locObj.id;
    userObj.FM_Role__c = 'FM Director';
    insert userObj;

    }

    @isTest static void testBeforeInsertForEscalation() {
        Booking_Unit__c bu1 = new Booking_Unit__c();
        bu1 = [SELECT Id from Booking_Unit__c limit 1];
        Test.startTest();
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Status__c = 'Submitted';
        objFMCase.Booking_Unit__c = bu1.Id;
        insert objFMCase;
        Test.stopTest();
    }

    @isTest static void test_updateFMCaseForEscalation() {
            Booking_Unit__c bu1 = new Booking_Unit__c();
            bu1 = [SELECT Id from Booking_Unit__c limit 1];
            Test.startTest();
            FM_Case__c objFMCase = new FM_Case__c();
            objFMCase.Status__c = 'New';
            objFMCase.Booking_Unit__c = bu1.Id;
            insert objFMCase;
            objFMCase.Status__c = 'Submitted';
            Update objFMCase;
            Test.stopTest();
    }
    
    @isTest static void test_POPEscalation() {
            Booking_Unit__c bu1 = new Booking_Unit__c();
            bu1 = [SELECT Id from Booking_Unit__c limit 1];
            Test.startTest();
            FM_Case__c objFMCase = new FM_Case__c();
            objFMCase.Status__c = 'New';
            objFMCase.Request_Type_DeveloperName__c = 'Proof_of_Payment';
            insert objFMCase;
            objFMCase.Status__c = 'Submitted';
            Update objFMCase;
            Test.stopTest();
    }


}