/**************************************************************************************************
 * @Name              : createAttachmentsForPlanScheduler
 * @Test Class Name   : 
 * @Description       : Schedulable class to invoke Queueable class InventoryQueueable
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst          29/09/2020       Created
**************************************************************************************************/
public class createAttachmentsForPlanScheduler implements Schedulable {
    public Set<Id> inventoryIdsForPlans {get;set;}
    
    public createAttachmentsForPlanScheduler(Set<Id> inventoryIdsForPlans){
        this.inventoryIdsForPlans = inventoryIdsForPlans;
    }
    public void execute(SchedulableContext sc) {
        Database.executeBatch (new createAttachmentsForPlanBatch (inventoryIdsForPlans),  1);
        // Abort the job once the job is queued
        System.abortJob(sc.getTriggerId());
    }
}