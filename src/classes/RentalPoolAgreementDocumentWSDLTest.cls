@isTest
public class RentalPoolAgreementDocumentWSDLTest {
    @isTest static void instanceForClass_TERMINATE_RP_RECORD_element() {
        Test.startTest();
        RentalPoolAgreementDocumentWSDL.TERMINATE_RP_RECORD_element TERMINATE_RP_RECORD_element_Obj = 
            new RentalPoolAgreementDocumentWSDL.TERMINATE_RP_RECORD_element();
        Test.stopTest();
    }

    @isTest static void instanceForClass_TERMINATE_RP_RECORDResponse_element() {
        Test.startTest();
        RentalPoolAgreementDocumentWSDL.TERMINATE_RP_RECORDResponse_element TERMINATE_RP_RECORDResponse_element_Obj = 
            new RentalPoolAgreementDocumentWSDL.TERMINATE_RP_RECORDResponse_element();
        Test.stopTest();
    }

    @isTest static void instanceForClass_RentalPoolAgreement_element() {
        Test.startTest();
        RentalPoolAgreementDocumentWSDL.RentalPoolAgreement_element RentalPoolAgreement_element_Obj = 
            new RentalPoolAgreementDocumentWSDL.RentalPoolAgreement_element();
        Test.stopTest();
    }

    @isTest static void instanceForClass_RentalPoolAgreementResponse_element() {
        Test.startTest();
        RentalPoolAgreementDocumentWSDL.RentalPoolAgreementResponse_element RentalPoolAgreementResponse_element_Obj = 
            new RentalPoolAgreementDocumentWSDL.RentalPoolAgreementResponse_element();
        Test.stopTest();
    }

    @isTest static void instanceForClass_RPAHttpSoap11Endpoint() {
        list<RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5> regTerms = 
            new list<RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5>();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(1));
        RentalPoolAgreementDocumentWSDL.RPAHttpSoap11Endpoint RPAHttpSoap11Endpoint_Obj = 
            new RentalPoolAgreementDocumentWSDL.RPAHttpSoap11Endpoint();
        RPAHttpSoap11Endpoint_Obj.RentalPoolAgreement('String P_REQUEST_NUMBER','String P_REQUEST_NAME','String P_SOURCE_SYSTEM', regTerms);
        RPAHttpSoap11Endpoint_Obj.TERMINATE_RP_RECORD('String P_REQUEST_NUMBER','String P_REQUEST_NAME','String P_SOURCE_SYSTEM', regTerms);
        
        Test.stopTest();
    }
}