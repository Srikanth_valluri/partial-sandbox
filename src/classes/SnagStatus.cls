/***************AKISHOR**********for snagger integration*****************
****************AKISHOR:31Jan20, added email+SMS code********************
*****************AKISHOR: 10feb20Added handover exec*********************
************AKISHOR: added resubmissionteragetdate *********************/
@RestResource(urlMapping='/UnitInspection/*')
global with sharing class SnagStatus{
    public static Boolean bUUpdateflag = false;
    public static List<Booking_Unit__c> bookingUnitList;
    public static list<string> lstHandoverCaseRecTypes = new list<string>{
        'Early_Handover',
        'Handover'
    };

    @HttpPatch
    global static void CaptureSnagStatus( String OnsiteHODate,String ViewingStat, 
                                          String UnitName, String SnagStat, String SnagCompletionTargetDate, 
                                          String SnagCompletionRevisedTargetDate,Integer TotalSnags, Integer OpenSnags, Integer ClosedSnags,
                                          String VerifiedSnags,String FirstViewingDate,String SecondViewingDate, 
                                          String ThirdViewingDate, String HE1, String HE2, String HE3, String Remarks, String ResubTrgetDate,String ActlCompDate) {

       ResponseWrapper objResponseWrapper = new ResponseWrapper();
        Set<Id> recTypeIdSet=new Set<Id>();
        for(RecordType rec :[SELECT DeveloperName,Id,Name
                 FROM RecordType
                 WHERE SobjectType = 'Case'
                 And DeveloperName IN : lstHandoverCaseRecTypes //(Name='Handover' OR Name='Early Handover') 
        ]){
            recTypeIdSet.add(rec.Id);
        }
        System.debug('-->>input: '+ UnitName+SnagStat+SnagCompletionTargetDate);
        List<Booking_Unit__c> SngBUList= [Select Id, Booking__r.Account__c,Booking__r.Account__r.Mobile_Phone_Encrypt__pc,
                                    Booking__r.Account__r.Name,Booking__r.Account__r.Email__pc,Unit_Name__c,SRF_Date__c,Revised_SRF_Date__c,
                                    Registration_Id__c,Onsite_Handover_Date__c,Snags_Reported__c,Snags_Completed__c,Snag_Status_Onsite__c,
                                    Viewing_Status_onsite__c,Total_Snags_Open__c,FirstViewingDate__c,SecondViewingDate__c,
                                    ThirdViewingDate__c,Snag_Completion_Target_Date__c,Snag_Completion_Revised_Target_Date__c,
                                    Total_Snags_Closed__c,Total_Snags_Fixed__c, Calling_List__r.Virtual_Viewing__c, SnagValue__c
                                    from Booking_Unit__c where Unit_Active__c='Active' 
                                    AND Unit_Name__c=:UnitName limit 1];
        
        if(SngBUList == NULL || SngBUList.isEmpty()){              
            objResponseWrapper.status = 'No Unit exists, please select correct unit.';
            objResponseWrapper.statusCode = '400';
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
        }
        String RegistrationId1;
        if(!SngBUList.isEmpty()){
            RegistrationId1=SngBUList[0].Registration_Id__c;
            System.debug('-->>input2: '+ RegistrationId1+SnagStat+SnagCompletionTargetDate);
            if(RegistrationId1!='') {
                List<Case> TaskLst= new List<Case>();
                List<Booking_Unit__c> lstBu = new  List<Booking_Unit__c>();

                List <Case> CurrTaskLst = new List<Case>();
                CurrTaskLst = [Select Id
                                    ,Booking_Unit__r.Id
                                    ,Booking_Unit__r.Snag_Completion_Revised_Target_Date__c
                                    ,Booking_unit__r.Snag_Completion_Target_Date__c
                            from Case
                            where Registration_ID__c =: RegistrationId1
                            AND RecordTypeId IN :recTypeIdSet
                            limit 1];

                System.debug('CurrTaskLst :' + CurrTaskLst);
                if(SnagStat=='Open' || SnagStat=='Submitted') {
                    if(CurrTaskLst.size()>0){
                        Case csk = new Case();                   
                        csk.Snags_Reported_at_first__c=True;csk.Id=CurrTaskLst[0].Id;
                        if(String.isNotBlank( SnagCompletionTargetDate ) ) {
                            csk.Snag_Completion_Target_Date__c  = date.valueOf(SnagCompletionTargetDate);
                        }
                        TaskLst.add(csk);
                    }

                    if( String.isNotBlank( SnagCompletionTargetDate ) ) {
                        //csk.Snag_Completion_Target_Date__c  = date.valueOf(SnagCompletionTargetDate);
                        //if(CurrTaskLst[0].Booking_Unit__c != null) {
                            Booking_Unit__c objBu = new Booking_Unit__c();
                            objBu.Id = SngBUList[0].Id;//CurrTaskLst[0].Booking_Unit__r.Id;
                            objBu.Snag_Completion_Target_Date__c = date.valueOf(SnagCompletionTargetDate);
                            objBu.Viewing_Status_onsite__c=ViewingStat;
                            objBu.Total_Snags_Open__c=OpenSnags;
                            objBu.Total_Snags_Fixed__c=TotalSnags;
                            objBu.Snags_Reported__c=True;
                            objBu.Snag_Status_Onsite__c=SnagStat;
                            if(Remarks !=''){
                                objBu.Snag_Remarks__c = Remarks;
                            }
                            objBu.SRF_Date__c=system.today();
                            if( String.isNotBlank( FirstViewingDate) ) {
                                objBu.FirstViewingDate__c=date.valueOf(FirstViewingDate);
                                objBu.HE1__c=HE1;
                            }
                            if(String.isNotBlank( ActlCompDate) ) {
                                objBu.Actual_Sng_Completion_Date__c=date.valueOf(ActlCompDate);
                            }
                            lstBu.add(objBu);
                        //}
                    }           
                
                }
                else if(SnagStat=='RESUBMIT')
                {//AK:5sep2020 for new revised date
                    Booking_Unit__c objBu = new Booking_Unit__c();
                                                     
                        if( String.isNotBlank( ResubTrgetDate ) ) {
                            objBu.Id = SngBUList[0].Id;//CurrTaskLst[0].Booking_Unit__r.Id;
                            //objBu.SnagCompletionResubmittedTargetDate__c= date.valueOf(ResubTrgetDate); 
                            objBu.SnagCompletionResubmittedTargetDate__c= date.valueOf(ResubTrgetDate); 
                            objBu.Snag_Status_Onsite__c=SnagStat;
                            if( String.isNotBlank( ActlCompDate)){
                                objBu.Actual_Sng_Completion_Date__c=date.valueOf(ActlCompDate);
                            }
                         }
                            lstBu.add(objBu);
                }
                else if(SnagStat=='Revised')
                {
                    if( CurrTaskLst != null && CurrTaskLst.size()>0 && String.isNotBlank( SnagCompletionRevisedTargetDate ) ) {
                        Case csk = new Case(); 
                        csk.Id=CurrTaskLst[0].Id; 
                        csk.Snag_Completion_Revised_Target_Date__c  = date.valueOf( SnagCompletionRevisedTargetDate );
                        TaskLst.add(csk);
                    }                   
                    
                    if( String.isNotBlank( SnagCompletionRevisedTargetDate ) ) {                   
                        //if(CurrTaskLst[0].Booking_Unit__c != null) {
                            Booking_Unit__c objBu = new Booking_Unit__c();
                            objBu.Id = SngBUList[0].Id;//CurrTaskLst[0].Booking_Unit__r.Id;
                           objBu.Snag_Completion_Target_Date__c = date.valueOf(SnagCompletionRevisedTargetDate);
                           // objBu.SnagCompletionResubmittedTargetDate__c= date.valueOf(SnagCompletionRevisedTargetDate);
                            if( String.isNotBlank( SecondViewingDate) ){ objBu.SecondViewingDate__c=date.valueOf(SecondViewingDate);objBu.HE2__c=HE2;}
                            if( String.isNotBlank(ThirdViewingDate ) ){objBu.ThirdViewingDate__c=date.valueOf(ThirdViewingDate);objBu.HE3__c=HE3;}
                            objBu.Revised_SRF_Date__c=system.today();
                            objBu.Viewing_Status_onsite__c=ViewingStat;
                            objBu.Snag_Status_Onsite__c=SnagStat;
                            if(Remarks !=''){
                            objBu.Snag_Remarks__c = Remarks;}
                            if( String.isNotBlank( ActlCompDate) ){
                                objBu.Actual_Sng_Completion_Date__c=date.valueOf(ActlCompDate);}
                            lstBu.add(objBu);
                        //}
                    }
                
                }
                else if(SnagStat=='Ready for Handover'||SnagStat=='Handed Over')
                {
                    if(CurrTaskLst.size()>0){
                    Case csk = new Case();                
                    csk.SNAGs_Completed__c=True;csk.Id=CurrTaskLst[0].Id;
                    csk.Snag_Completion_Date__c = Date.today();
                    TaskLst.add(csk);
                    }

                        if(SngBUList.size() > 0) {
                            Booking_Unit__c objBu = new Booking_Unit__c();
                            objBu.Id = SngBUList[0].Id;//CurrTaskLst[0].Booking_Unit__r.Id;
                            if( String.isNotBlank( SnagCompletionRevisedTargetDate ) ) {
                                objBu.Snag_Completion_Revised_Target_Date__c = date.valueOf(SnagCompletionRevisedTargetDate);
                            }
                            objBu.Total_Snags_Closed__c=ClosedSnags;objBu.Viewing_Status_onsite__c=ViewingStat;
                            objBu.Total_Snags_Open__c=0; objBu.Total_Snags_Fixed__c=TotalSnags;
                            if( String.isNotBlank( OnsiteHODate ) ) {
                                objBu.Onsite_Handover_Date__c=date.valueOf(OnsiteHODate);
                            }
                            
                            if( String.isNotBlank( FirstViewingDate ) ){
                                objBu.FirstViewingDate__c=date.valueOf(FirstViewingDate);objBu.HE1__c=HE1;
                            }
                            if( String.isNotBlank( SecondViewingDate ) ){
                                objBu.SecondViewingDate__c=date.valueOf(SecondViewingDate);objBu.HE2__c=HE2;
                            }
                            objBu.Ready_for_Handover_Date__c=system.today();
                            objBu.Ready_for_Handover_DateTime__c = system.now();
                            if(Remarks !=''){
                            objBu.Snag_Remarks__c = Remarks;}
                            if(String.isNotBlank( ThirdViewingDate ) ) {
                                objBu.ThirdViewingDate__c = date.valueOf(ThirdViewingDate);
                            }
                            
                            objBu.Snags_Completed__c=True;
                            objBu.Snag_Status_Onsite__c=SnagStat;
                            if( String.isNotBlank( ActlCompDate ) ) {
                                objBu.Actual_Sng_Completion_Date__c=date.valueOf(ActlCompDate);
                            }
                            lstBu.add(objBu);
                        } 
                
                }

                System.debug('BU list: ' + lstBu);
                if(lstBu.size() > 0) {
                    bookingUnitList = lstbu;
                    bUUpdateflag = true;
                    if(SnagStat=='Revised' || SnagStat=='Open' || SnagStat=='Submitted' || SnagStat=='RESUBMIT' || SnagStat=='Ready for Handover'){
                        System.debug('Inside revised open submitted');
                        SendSRFToCustomer.fetchSRFDocfromOnsiteAPI(SngBUList, SnagStat);
                    }
                    if(bUUpdateflag == true) {
                        update lstbu;
                    }
                    
                    objResponseWrapper.status = 'Unit Updated Successfully';
                        objResponseWrapper.statusCode = '200';
                        System.debug('objResponseWrapper:::' +objResponseWrapper);
                        System.debug(JSON.serialize(objResponseWrapper));
                        String str = JSON.serialize(objResponseWrapper);
                        System.debug('str:::' +str);
                        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                        Restcontext.response.statusCode = 200;

                }

                System.debug('Case List is: ' + TaskLst);

                if(TaskLst.size() > 0 ) {
                    try{
                        Database.update(TaskLst); //add BU update DML
                        objResponseWrapper.status = 'Snag Status Updated Successfully';
                        objResponseWrapper.statusCode = '200';
                        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                        Restcontext.response.statusCode = 200;
                    } catch( Exception ex ) {
                        system.debug( ex.getMessage() );
                        objResponseWrapper.errorMessage = ex.getMessage();
                        objResponseWrapper.statusCode = '400';
                        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                        Restcontext.response.statusCode = 400;
                        System.debug('============= response : ' + JSON.serialize(objResponseWrapper));
                        return;
                    }
                }
            }//end of regid If
            else if(RegistrationId1 == null || RegistrationId1=='') {
                objResponseWrapper.status = 'Unit is not valid';
                objResponseWrapper.statusCode = '400';
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                Restcontext.response.statusCode = 400;
            }
            
            
        }
        
}
public static void SendSnagEmail(List<Booking_Unit__c> SngBUList, Attachment att, String strSnagEmailTemplate){//to send emails
 //List<Messaging.SingleEmailMessage> messages =   new List<Messaging.SingleEmailMessage>();
 List<OrgWideEmailAddress> owea;EmailTemplate SNAGCompleteTemplate;//String strSnagEmailTemplate = 'SNAG_Completion_Template';
 List<OrgWideEmailAddress> owea_damac;
 owea = new List<OrgWideEmailAddress>();
 owea_damac = new List<OrgWideEmailAddress>();
 SNAGCompleteTemplate = [SELECT ID, Subject, Body, HtmlValue FROM EmailTemplate 
                                    WHERE DeveloperName =: strSnagEmailTemplate limit 1];
 //Get Org wide email  address details
       /*     owea = [select Id from OrgWideEmailAddress 
                    where Address = 'atyourservice@damacproperties.com'];
            
            owea_damac = [select Id, Address from 
                            OrgWideEmailAddress where DisplayName = 'DAMAC'];*/
           system.debug('SngBUList[0].Booking__r.Account__r.Email__pc  == ' + SngBUList[0].Booking__r.Account__r.Email__pc );
            if(SngBUList[0].Booking__r.Account__r.Email__pc != null) {
                        List<EmailMessage> lstEmails = new List<EmailMessage>();
                        String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';
                        String subject, strAccountId, strDPIId, bccAddress ='', strCCAddress ='';
                              // Callout to sendgrid to send an email
                            strAccountId = SngBUList[0].Booking__r.Account__c;
                            toAddress = SngBUList[0].Booking__r.Account__r.Email__pc;
                            bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
                            fromAddress = Label.AT_Your_Service;

                            subject =  SNAGCompleteTemplate.Subject.replace('{!Case.Unit_Name__c}', SngBUList[0].Unit_Name__c);
                            contentType = 'text/html';
                            contentValue = contentValue = SNAGCompleteTemplate.htmlValue;
                            
                            if(SNAGCompleteTemplate.body != NULL){
                                contentBody =  SNAGCompleteTemplate.body;
                                //Todo added to have cx name
                                contentBody = contentBody.replace('{!Case.Account}', SngBUList[0].Booking__r.Account__r.Name);
                                contentBody = contentBody.replace('{!Case.Unit_Name__c}', SngBUList[0].Unit_Name__c);
                                
                                
                            }
                            if(string.isblank(contentValue)){ contentValue='No HTML Body';    }
                             //Todo added to have cx name 
                            contentValue = contentValue.replace('{!Case.Account}', SngBUList[0].Booking__r.Account__r.Name);
                            contentValue = contentValue.replace('{!Case.Unit_Name__c}', SngBUList[0].Unit_Name__c);
                            
                           


                            List<Attachment> lstAttach = new List<Attachment>();
                            lstAttach.add(att);

                            SendGridEmailService.SendGridResponse objSendGridResponse =
                                SendGridEmailService.sendEmailService(
                                    toAddress
                                    , ''
                                    , strCCAddress
                                    , ''
                                    , bccAddress
                                    , ''
                                    , subject
                                    , ''
                                    , fromAddress
                                    , ''
                                    , replyToAddress
                                    , ''
                                    , contentType
                                    , contentValue
                                    , ''
                                    , lstAttach
                                );

                            System.debug('SendGrid response:::' +objSendGridResponse.ResponseStatus);
                                String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                                if (responseStatus == 'Accepted') {
                                    EmailMessage mail = new EmailMessage();
                                    mail.Subject = subject;
                                    mail.MessageDate = System.Today();
                                    mail.Status = '3';//'Sent';
                                    mail.RelatedToId = strAccountId;
                                    mail.Account__c  = strAccountId;
                                    mail.Type__c = 'CaptureSnagStatus';
                                    mail.ToAddress = toAddress;
                                    mail.FromAddress = fromAddress;
                                    mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                                    mail.Sent_By_Sendgrid__c = true;
                                    mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                                    mail.CcAddress = strCCAddress;
                                    mail.BccAddress = bccAddress;
                                    lstEmails.add(mail);
                                    system.debug('Mail obj == ' + mail);


                                }//End Email Activity Creation
                                /**  Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                                    String mailMessage;
                                    //message.setOrgWideEmailAddressId(owea.get(0).Id);
                                    mailMessage = SNAGCompleteTemplate.Body;
                                    mailMessage = mailMessage.replace('{!Case.Account}', SngBUList[0].Booking__r.Account__r.Name);
                                    mailMessage = mailMessage.replace('{!Case.Unit_Name__c}', SngBUList[0].Unit_Name__c);              
                                    message.subject = SNAGCompleteTemplate.Subject.replace('{!Case.Unit_Name__c}', SngBUList[0].Unit_Name__c);
                                    message.setPlainTextBody(mailMessage);
                                    message.setSaveAsActivity(true);
                                    message.setWhatId(SngBUList[0].Booking__r.Account__c);
                                    List<String> listToAddresses = new List<String>();
                                    listToAddresses.add(SngBUList[0].Booking__r.Account__r.Email__pc);
                                    //if(owea_damac != null) {
                                    //  listToAddresses.add(owea_damac[0].Address);
                                    // }
                                    message.toAddresses = listToAddresses;
                                    messages.add(message);
                                */
                            if( lstEmails != null && lstEmails.size() > 0 ) {
                                    insert lstEmails;
                            }
                         }     

            /**if (messages != null && messages.size() >0) {
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);//Sending emails
            } */
    }

    

    //for sending response
    public class ResponseWrapper {
        public String status;
        public String statusCode;
        public String errorMessage;

        public ResponseWrapper() {
            this.status = '';
            this.statusCode = '';
            this.errorMessage = '';
        }
       }
}