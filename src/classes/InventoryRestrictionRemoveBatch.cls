/**************************************************************************************************
* Name         : InventoryRestrictionRemoveBatch
* Test Class  : InventoryRestrictionRemoveBatchTest
* Description: Batch class to change the Inventory status back from Restriction
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR      DATE
* 1.0         QBurst      29/09/2020
**************************************************************************************************/
public class InventoryRestrictionRemoveBatch implements Database.Batchable<sObject>{

    /*********************************************************************************************
    * @Description: Implementing the start method of batch interface, contains query.
    * @Params      : Database.BatchableContext
    * @Return      : Database.QueryLocator
    *********************************************************************************************/  
    public Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator([SELECT Id, Name, Inventory__c, Inventory__r.Status__c,
                                                        Inventory__r.Name, Inventory__r.Unit_Name__c,
                                                        Inventory_Restriction_Request__c, Inventory_Initial_Status__c,
                                                        Inventory_Restriction_Request__r.Restriction_End_Date__c,
                                                        Inventory_Restriction_Request__r.Restriction_Extended__c
                                                     FROM Inventory_Restriction_Unit__c
                                                WHERE Inventory_Restriction_Request__c != NULL
                                                    AND Inventory_Restriction_Request__r.Status__c = 'Completed'
                                                    AND Inventory__r.Status__c = 'Restricted'
                                                    AND (Inventory_Restriction_Request__r.Request_Type__c = 'Inventory Restriction'
                                                        OR Inventory_Restriction_Request__r.Request_Type__c = 'Restriction Extension')
                                                    AND Inventory_Restriction_Request__r.Restriction_End_Date__c != NULL
                                                    AND Inventory_Restriction_Request__r.Restriction_End_Date__c = TODAY
                                                    AND Inventory_Restriction_Request__r.Restriction_Extended__c = false]); 
    }

    /*********************************************************************************************
    * @Description: Implementing the execute method of the batch interface, contains the criteria.
    * @Params      : Database.BatchableContext, List<sObject>
    * @Return: void
    *********************************************************************************************/
    public void execute(Database.BatchableContext BC, List<Inventory_Restriction_Unit__c> scope){
        system.debug('scope: ' +  scope);
        Map<Id, Inventory__c> invMap = new Map<Id, Inventory__c>();
        for(Inventory_Restriction_Unit__c unit: scope){
            Inventory__c inv = new Inventory__c();
            inv.Id = unit.Inventory__c;
            inv.Status__c = 'Inventory'; 
            invMap.put(inv.Id, inv);  
        }
        if(invMap.keyset().size() > 0){
            update invMap.values();    
        }
    }

    /*********************************************************************************************
    * @Description: Implementing the Finish method, to send an email after job completion.
    * @Params      : Database.BatchableContext
    * @Return: void
    *********************************************************************************************/
    public void finish(Database.BatchableContext BC){
    }
}// End of class.