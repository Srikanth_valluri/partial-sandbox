/**************************************************************************************************
* Name               : InquiryActivitiesController                                                *
* Description        : Custom code invoked on New Activity button on Inquiries                    *
                       and Edit Activity button on Account Task RecordType                        *
* Created Date       : 20-Nov-2017                                                                *
* Created By         : ESPL                                                                       *
* ----------------------------------------------------------------------------------------------- *
* VERSION       AUTHOR        DATE            COMMENTS                                              *
* 1.0         Craig Lobo    20-Nov-2017     Initial Draft.                                        *
***************************************************************************************************/

public with sharing class InquiryActivitiesController {

    public Set<String> amendmentIdsSet;
    public List<Amendment__c> amendmentsList;
    public List<AmendmentWrapper> amendmentWrapperList1         {get; set;}
    public List<AmendmentWrapper> amendmentWrapperList2         {get; set;}
    public User userObj                                         {get; set;}
    public Task completedtaskObj                                {get; set;}
    public Task scheduledtaskObj                                {get; set;}
    public Task editedTask                                      {get; set;}
    public String relatedToName                                 {get; set;}
    public String msgType                                       {get; set;}
    public String isDisplay                                     {get; set;}
    public String message                                       {get; set;}
    public String completedTaskOwner                            {get; set;}
    public String scheduledTaskOwner                            {get; set;}
    public String inqId                                         {get; set;}
    public Boolean displayPopUp                                 {get; set;}
    public Boolean isFollowUp                                   {get; set;}
    public Boolean isEditTask                                   {get; set;}
    public Boolean isAgentsTeam                                 {get; set;}


    /*********************************************************************************************************
    * @Description : Constructor                                                                             *
    * @Params      : Standard controller instance                                                            *
    * @Return      : Void                                                                                    *
    **********************************************************************************************************/
    public InquiryActivitiesController(ApexPages.StandardController controller) {
        initActivity();
    }

    /*********************************************************************************************************
    * @Description : Initialize the Task, Get logged in UserId, Get Inquiry in current context               *
    *                Get Amendments related logged in to UserId                                              *
    * @Params      :                                                                                         *
    * @Return      : Void                                                                                    *
    **********************************************************************************************************/
    public void initActivity() {
        //
        String loggedinUserId = UserInfo.getUserId();
        isFollowUp = false;
        if (message == '' || message == null) {
            isDisplay = 'hide';
        }
        displayPopUp = false;
        msgType = '';
        message = '';
        inqId = ApexPages.currentPage().getParameters().get('id');
        if (String.isBlank(inqId)) {
            inqId = ApexPages.currentPage().getParameters().get('relatedTo');
        }
        String taskId = ApexPages.currentPage().getParameters().get('taskId');
        system.debug('inqId >>>  ' + inqId);
        system.debug('inqId >>>  ' + inqId);

        String accountTaskRTId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Account Task').getRecordTypeId();
        system.debug('accountTaskRTId >>>  ' + accountTaskRTId);
        userObj = new User();
        userObj = [SELECT Id, Name FROM User WHERE Id = :UserInfo.getUserId()];
        Date todaysDate = System.today();
        String userProfileId = UserInfo.getProfileId();
        completedtaskObj = new Task();
        scheduledtaskObj = new Task();
        completedTaskOwner = userObj.Id;
        scheduledTaskOwner = userObj.Id;

        amendmentsList = new List<Amendment__c>();
        amendmentsList = [SELECT Id, Name, Inquiry__c, 
                                 First_Name__c, Last_Name__c
                            FROM Amendment__c
                           WHERE Inquiry__c = :inqId
        ];
        system.debug('amendmentsList >>>>>>  ' + amendmentsList);

        if (String.isBlank(taskId)) {
            isEditTask = false;
            if (String.isNotBlank(accountTaskRTId)) {
                completedtaskObj.RecordTypeId = accountTaskRTId;
            }
            completedtaskObj.OwnerId = userObj.Id;
            completedtaskObj.WhatId = inqId;
            completedtaskObj.ActivityDate = todaysDate;
            completedtaskObj.Status = 'Completed';
            completedtaskObj.Priority = 'Normal';

            if (String.isNotBlank(accountTaskRTId)) {
                scheduledtaskObj.RecordTypeId = accountTaskRTId;
            }
            scheduledtaskObj.OwnerId = userObj.Id;
            scheduledtaskObj.ActivityDate = todaysDate;
            scheduledtaskObj.WhatId = inqId;

            populateInquiryAmendments(null);

        } else if (String.isNotBlank(taskId)) {
            editedTask = [SELECT Id, Subject, ActivityDate, WhatId, OwnerId, Status,
                                 Agency_Contacts_Involved__c, Activity_Members__c,
                                 Start_Date__c, End_Date__c, Description, Priority,
                                 Activity_Sub_Type__c, Activity_Type__c, WhoId,
                                 Agency_Contacts_Ids__c
                            FROM Task 
                           WHERE Id = :taskId];
            system.debug('editedTask >>>>>>  ' + editedTask);
            if (editedTask != null) {
                completedtaskObj = editedTask;
                userObj = [SELECT Id, Name FROM User WHERE Id = :completedtaskObj.OwnerId];
                isEditTask = true;
                if (String.isNotBlank(completedtaskObj.Agency_Contacts_Ids__c)) {
                    List<String> editedTaskamendmentIds = completedtaskObj.Agency_Contacts_Ids__c.split(',');
                    if (!editedTaskamendmentIds.isEmpty()) {
                        amendmentIdsSet = new Set<String>(editedTaskamendmentIds);
                        populateInquiryAmendments(amendmentIdsSet);
                    } else {
                        populateInquiryAmendments(null);
                    }
                } else {
                    populateInquiryAmendments(null);
                }
            }
        }

    }


    /*********************************************************************************************************
    * @Description : Populate the Amendment records from the Inquiry                                         *
    * @Params      : Set<String> > Set of Amendment Ids                                                      *
    * @Return      :                                                                                         *
    *********************************************************************************************************/
    public void populateInquiryAmendments(Set<String> pAmendmentIdsSet) {

        amendmentWrapperList1 = new List<AmendmentWrapper>();
        amendmentWrapperList2 = new List<AmendmentWrapper>();
        AmendmentWrapper amendmentWrapperObj1;
        AmendmentWrapper amendmentWrapperObj2;
        for (Amendment__c amendmentObj : amendmentsList) {
            String amendmentName = amendmentObj.First_Name__c + ' ' + amendmentObj.Last_Name__c;
            System.debug('amendmentList>>>>  '+ amendmentsList);
            if (pAmendmentIdsSet != null && !pAmendmentIdsSet.isEmpty()) {
                if (pAmendmentIdsSet.contains(amendmentObj.Id)) {
                    amendmentWrapperObj1 = new AmendmentWrapper(amendmentObj.Id, amendmentName, true);
                } else {
                    amendmentWrapperObj1 = new AmendmentWrapper(amendmentObj.Id, amendmentName, false);
                }
                amendmentWrapperList1.add(amendmentWrapperObj1);
            } else {
                amendmentWrapperObj1 = new AmendmentWrapper(amendmentObj.Id, amendmentName, false);
                amendmentWrapperObj2 = new AmendmentWrapper(amendmentObj.Id, amendmentName, false);
                amendmentWrapperList1.add(amendmentWrapperObj1);
                amendmentWrapperList2.add(amendmentWrapperObj2);
            }
        }

    }

    /***************************************************************************************************
    * @Description : Method to Create new Activity record                                              *
    * @Params      :                                                                                   *
    * @Return      :                                                                                   *
    ****************************************************************************************************/
    public void saveActivity() {
        isDisplay = 'hide';
        System.debug('saveActivity>>>>  '+ isFollowUp);
        System.debug('completedTaskOwner>>>>  '+ completedTaskOwner );
        System.debug('scheduledTaskOwner>>>>  '+ scheduledTaskOwner );

        //List<Contact_Activity__c> contactActivityInsertList1 = new List<Contact_Activity__c>();
        //List<Contact_Activity__c> contactActivityInsertList2 = new List<Contact_Activity__c>();
        Boolean isValidCompletedTask = validateActivity(completedtaskObj);
        if (!isFollowUp) {
            if (isValidCompletedTask) {
                completedtaskObj.Subject = completedtaskObj.Activity_Type__c;
                completedtaskObj.OwnerId = UserInfo.getUserId();
                completedtaskObj.Created_on_Inquiry__c = true;
                stampAmendmentOnInquiry(completedtaskObj, amendmentWrapperList1);
                System.debug('completedtaskObj>>>>  '+ completedtaskObj);
                insert completedtaskObj;
                messageBlock('show', 'success', 'Activity Logged Successfully');
                /*contactActivityInsertList1 = createContactActivityRecords(completedtaskObj, amendmentWrapperList1);
                if (!contactActivityInsertList1.isEmpty()) {
                    insert contactActivityInsertList1;
                    if (isAgentsTeam) {
                        completedtaskObj.OwnerId = completedTaskOwner;
                    }
                    update completedtaskObj;
                }*/
            } else {
                messageBlock('show', 'error', message);
            }
        } else if (isFollowUp) {
            System.debug('isFollowUp>>>>  ' +isFollowUp);
            Boolean isValidScheduledTask = validateActivity(scheduledtaskObj);
            if (isValidScheduledTask && isValidCompletedTask) {
                completedtaskObj.Subject = completedtaskObj.Activity_Type__c;
                scheduledtaskObj.Subject = scheduledtaskObj.Activity_Type__c;
                completedtaskObj.Created_on_Inquiry__c = true;
                scheduledtaskObj.Created_on_Inquiry__c = true;
                stampAmendmentOnInquiry(completedtaskObj, amendmentWrapperList1);
                stampAmendmentOnInquiry(scheduledtaskObj, amendmentWrapperList2);
                insert completedtaskObj;
                insert scheduledtaskObj;
                /*contactActivityInsertList1 = createContactActivityRecords(completedtaskObj, amendmentWrapperList1);
                contactActivityInsertList2 = createContactActivityRecords(scheduledtaskObj, amendmentWrapperList2);
                contactActivityInsertList1.addAll(contactActivityInsertList2);
                if (!contactActivityInsertList1.isEmpty()) {
                    insert contactActivityInsertList1;
                    if (isAgentsTeam) {
                        completedtaskObj.OwnerId = completedTaskOwner;
                        scheduledtaskObj.OwnerId = scheduledTaskOwner;
                    }
                    update completedtaskObj;
                    update scheduledtaskObj;
                }*/
                messageBlock('show', 'success', 'Activity Logged Successfully and Created a Follow-up Task ');
            } else {
                messageBlock('show', 'error', message);
            }
        }
    }

    /***************************************************************************************************
    * @Description : Method to validate the task record                                                *
    * @Params      : pTaskObj > Task object to be validated                                            *
    * @Return      : Boolean                                                                           *
    ****************************************************************************************************/
    public Boolean validateActivity(Task pTaskObj) {

        Boolean isValid = true;
        message = '';
        if (pTaskObj != null) {
            if (pTaskObj.Start_Date__c == null) {
                isValid = false;
                message += ' Please Enter Start Date for Activity <br/>';
            }
            if(pTaskObj.End_Date__c == null) {
                isValid = false;
                message += ' Please Enter End Date for Activity <br/>';
            } else {
                DateTime endDateTime = pTaskObj.End_Date__c;
                Date dueDate = Date.NewInstance(
                    endDateTime.year(), endDateTime.month(), endDateTime.day()
                );
                pTaskObj.ActivityDate = dueDate;
            }
            if (pTaskObj.Status == null ) {
                isValid = false;
                message += ' Please Enter Status for Activity <br/>';
            }
            if (pTaskObj.Activity_Type__c == null) {
                isValid = false;
                message += ' Please Enter Activity Type for Activity <br/>';
            }

            if (pTaskObj.Activity_Type__c != null
                && pTaskObj.Activity_Sub_Type__c == null
                && (pTaskObj.Activity_Type__c.equals('Queries')
                    || pTaskObj.Activity_Type__c.equals('Complaints')
                )
            ) {
                isValid = false;
                message += 'Please Enter Activity Sub Type for Activity <br/>';
            }
            return isValid;
        } else {
            return false;
        }
    }


    /***************************************************************************************************
    * @Description : Method to Create new Activity record                                              *
    * @Params      :                                                                                   *
    * @Return      :                                                                                   *
    ****************************************************************************************************/
    public void updateActivity() {
        isDisplay = 'hide';
        System.debug('updateActivity>>>>  ');
        List<Contact_Activity__c> contactActivityInsertList = new List<Contact_Activity__c>();
        Boolean isValidCompletedTask = validateActivity(completedtaskObj);
        if (isValidCompletedTask) {
            completedtaskObj.Subject = completedtaskObj.Activity_Type__c;
            completedtaskObj.Created_on_Inquiry__c = true;
            stampAmendmentOnInquiry(completedtaskObj, amendmentWrapperList1);
            update completedtaskObj;
            /*contactActivityInsertList = createContactActivityRecords(completedtaskObj, contactsWrapperList1);
            if (!contactActivityInsertList.isEmpty()) {
                deleteContactActivityRecords(completedtaskObj);
                insert contactActivityInsertList;
                update completedtaskObj;
            }*/
            messageBlock('show', 'success', 'Activity Logged Successfully');
        } else {
            messageBlock('show', 'error', message);
        }
    }

    /****************************************************************************************************
    * @Description : Method to Stamp the Amendments on the Inquiry record                               *
    * @Params      : pTaskObj > Task Record, pWrapperList > success/error, pMessage > custom message    *
    * @Return      : void                                                                               *
    *****************************************************************************************************/
    public void stampAmendmentOnInquiry(Task pTaskObj, List<AmendmentWrapper> pWrapperList) {
        String activityAmendmentNames = '';
        String activityAmendmentIds = '';
        Integer totalActivityAmendments = 0;
        System.debug('pWrapperList>>>>  '+ pWrapperList);
        for (AmendmentWrapper wrapAmendment : pWrapperList) {
            if (wrapAmendment.isSelected == true) {
                activityAmendmentNames += wrapAmendment.wrapAmendmentName + ',';
                activityAmendmentIds += wrapAmendment.wrapAmendmentId + ',';
                totalActivityAmendments++;
            }
        }
        System.debug('activityAmendmentNames>>>>  '+ activityAmendmentNames);
        System.debug('activityAmendmentIds>>>>  '+ activityAmendmentIds);
        System.debug('totalActivityAmendments>>>>  '+ totalActivityAmendments);
        if (String.isNotBlank(activityAmendmentNames)) {
            pTaskObj.Agency_Contacts_Involved__c = activityAmendmentNames.removeEnd(',');
        } else {
                pTaskObj.Agency_Contacts_Involved__c = '';
        }
        if (String.isNotBlank(activityAmendmentIds)) {
            pTaskObj.Agency_Contacts_Ids__c = activityAmendmentIds.removeEnd(',');
        } else {
            pTaskObj.Agency_Contacts_Ids__c = '';
        }
        if (totalActivityAmendments != 0) {
            pTaskObj.Activity_Members__c = totalActivityAmendments;
        } else {
            pTaskObj.Activity_Members__c = 0;
        }
        /*if (String.isNotBlank(activityAmendmentNames) 
            && String.isNotBlank(activityAmendmentIds)
            && totalActivityAmendments != 0
        ) {
            pTaskObj.Agency_Contacts_Involved__c = activityAmendmentNames.removeEnd(',');
            pTaskObj.Agency_Contacts_Ids__c = activityAmendmentIds.removeEnd(',');
            pTaskObj.Activity_Members__c = totalActivityAmendments;
        }*/
        System.debug('pTaskObj>>>>  '+ pTaskObj);
    }

    /************************************************************************************************
    * @Description : Method to Display the success/Error messages                                   *
    *                depending on their visiblity, Value and Type                                   *
    * @Params      : pDisplay > show/hide, pType > success/error, pMessage > custom message         *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void messageBlock(String pDisplay, String pType, String pMessage) {
        isDisplay = pDisplay;
        msgType = pType;
        message = pMessage ;
    }

    /***************************************************************************************************
    * @Description : Method to Create new Contact Activity records(Junction Object)                    *
    * @Params      :                                                                                   *
    * @Return      : List<Contact_Activity__c>                                                         *
    ****************************************************************************************************/
    /*public List<Contact_Activity__c> createContactActivityRecords(Task pTaskObj, List<ContactsWrapper> pWrapperList) {

        String activityContactNames = '';
        String activityContactIds = '';
        Integer totalActivityContacts = 0;
        List<Contact_Activity__c> contactActivityList = new List<Contact_Activity__c>();

        System.debug('pWrapperList>>>>  '+ pWrapperList);
        for (ContactsWrapper wrapContact : pWrapperList) {
            if (wrapContact.isSelected == true) {
                Contact_Activity__c contactActivityObj = new Contact_Activity__c();
                contactActivityObj.Name = accountObj.Name;
                contactActivityObj.Contact__c = wrapContact.wrapContactId;
                contactActivityObj.TaskId__c = pTaskObj.Id;
                contactActivityObj.Activity_Type__c = pTaskObj.Activity_Type__c;
                contactActivityObj.Activity_Sub_Type__c = pTaskObj.Activity_Sub_Type__c;
                contactActivityObj.Activity_Status__c = pTaskObj.Status;
                contactActivityObj.Activity_Start_Date__c = pTaskObj.Start_Date__c;
                contactActivityObj.Activity_End_Date__c = pTaskObj.End_Date__c;
                contactActivityObj.Activity_Subject__c = pTaskObj.Activity_Type__c;
                contactActivityObj.Activity_Description__c = pTaskObj.Description;
                contactActivityObj.Activity_Priority__c = pTaskObj.Priority;
                activityContactNames += wrapContact.wrapContactName + ',';
                activityContactIds += wrapContact.wrapContactId + ',';
                totalActivityContacts++;
                contactActivityList.add(contactActivityObj);
            }
        }
        if (!contactActivityList.isEmpty()) {
            pTaskObj.Agency_Contacts_Involved__c = activityContactNames.removeEnd(',');
            pTaskObj.Activity_Members__c = totalActivityContacts;
            pTaskObj.Agency_Contacts_Ids__c = activityContactIds.removeEnd(',');
        } else {
            Contact_Activity__c contactActivityObj = new Contact_Activity__c();
            contactActivityObj.Name = accountObj.Name;
            contactActivityObj.TaskId__c = pTaskObj.Id;
            contactActivityObj.Activity_Type__c = pTaskObj.Activity_Type__c;
            contactActivityObj.Activity_Sub_Type__c = pTaskObj.Activity_Sub_Type__c;
            contactActivityObj.Activity_Status__c = pTaskObj.Status;
            contactActivityObj.Activity_Start_Date__c = pTaskObj.Start_Date__c;
            contactActivityObj.Activity_End_Date__c = pTaskObj.End_Date__c;
            contactActivityObj.Activity_Subject__c = pTaskObj.Activity_Type__c;
            contactActivityObj.Activity_Description__c = pTaskObj.Description;
            contactActivityObj.Activity_Priority__c = pTaskObj.Priority;
            contactActivityList.add(contactActivityObj);
        }
        return contactActivityList;

    }*/

    /***************************************************************************************************
    * @Description : Method to Delete Contact Activity records(Junction Object)                    *
    * @Params      :                                                                                   *
    * @Return      : List<Contact_Activity__c>                                                         *
    ****************************************************************************************************/
    /*public void deleteContactActivityRecords(Task pTaskObj) {

        List<Contact_Activity__c> contactActivityList = new List<Contact_Activity__c>();

        System.debug('pTaskObj DELETE>>>>  '+ pTaskObj);
        if (pTaskObj != null) {
            if (String.isNotBlank(pTaskObj.Agency_Contacts_Ids__c)) {
                List<String> contactIdsList = pTaskObj.Agency_Contacts_Ids__c.split(',');
                //if (contactIdsList != null && !contactIdsList.isEmpty()) {
                    //System.debug('contactIdsList>>>>  '+ contactIdsList);
                    contactActivityList = [ SELECT Id, TaskId__c
                                              FROM Contact_Activity__c
                                             WHERE TaskId__c = :pTaskObj.Id
                    ];
                    if (!contactActivityList.isEmpty()) {
                        delete contactActivityList;
                    }
                //}
            }
        }

    }*/

    /************************************************************************************************
    * @Description : Wrapper Class to display the Amendments with a check box on the page           *
    * @Params      :                                                                                *
    * @Return      :                                                                                *
    *************************************************************************************************/
    public class AmendmentWrapper {
        public Id wrapAmendmentId                  {get; set;}
        public String wrapAmendmentName            {get; set;}
        public Boolean isSelected                  {get; set;}

        public AmendmentWrapper (Id pAmendmentId, String pAmendmentName, Boolean pSelected) {
            wrapAmendmentId = pAmendmentId;
            wrapAmendmentName = pAmendmentName;
            isSelected = pSelected;
        }
    }

}