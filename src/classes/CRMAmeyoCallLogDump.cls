@RestResource(urlMapping='/CRMAmeyoCallLogDump/*')
global with sharing class CRMAmeyoCallLogDump{
    
//*******************AKISHOR: Created On: 11/Jan/2021 for Ameyo to Dump CRM call logs in SF ******************//

 @HttpPatch
    global static void CRMAmeyoCallLogDump(String CrtObjectId, String ObjectType, String RecordId, 
                                            String Remarks, String CallType
                                            ,String AccountName, String AccountId, String CallURL,
                                            String StartDate, String EndDate, String ACW,String Outcome,
                                            String AHT, String CallingExt, String CallingCRE,String CallingNum, 
                                            String CalledNumber,String IVRResponse, String CustomerFeedback, 
                                            String PreviewTime, String EncriptedNum, String system_disposition, 
                                            String attempt_time, String talk_time) {
                                                
   
        
    // ResponseWrapper objResponseWrapper = new ResponseWrapper();
        System.debug('-->>RecordId: '+ RecordId+ObjectType);         
     List<Ameyo_Log__c> CallLog= new List<Ameyo_Log__c>();  
     List <User> Exn = [Select Id,Extension from User where UserName=: CallingCRE AND IsActive= TRUE];     
     
     List<Calling_List__c> clLst = new List<Calling_List__c>();    
                                    
     
        if(CrtObjectId!= '')//RecordId!='' && CallingCRE!='') 
        {
            Ameyo_Log__c callLogc = new Ameyo_Log__c();
            callLogc.userId__c= CallingCRE ;//Exn[0].Id;
            //callLogc.Calling_Number__c = CallingNum;
            callLogc.SF_record_id__c = RecordId;//UtilityHelperCls.decryptMobile(EncriptedNum);
            callLogc.Called_Number__c = CalledNumber;
            callLogc.callType__c= CallType;
            callLogc.callConnectedTime__c= makeDate(StartDate);
            callLogc.callEndTime__c= makeDate(EndDate);
            callLogc.AHT_Ameyo__c= AHT;
            callLogc.ACW_Time__c= ACW;
            callLogc.Calling_Number_Encrypted__c= EncriptedNum;
            callLogc.PreviewTime__c= PreviewTime;
            callLogc.Description__c='CRM Ameyo call log';
            callLogc.recording_url__c= CallURL;
            callLogc.systemDisposition__c=system_disposition;
            callLogc.talkTime__c=talk_time;
            callLogc.CrtObjectId__c=CrtObjectId;
            callLogc.attempt_time__c=attempt_time;
            
                CallLog.add(callLogc);
            }                        
          
            if(CallLog.size() > 0 ) {
                ResponseWrapper objResponseWrapper = new ResponseWrapper();
                try{
                    Database.insert(CallLog);
                    objResponseWrapper.status = 'Call Log Created Sucessfully';
                    objResponseWrapper.statusCode = '200';
                    RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                    Restcontext.response.statusCode = 200;                 
                } catch( Exception ex ) {
                    system.debug( ex.getMessage() );
                    objResponseWrapper.errorMessage = ex.getMessage();
                    objResponseWrapper.statusCode = '400';
                    System.debug('============= response : ' + objResponseWrapper);
                    System.debug('============= response : ' + JSON.serialize(objResponseWrapper));
                    RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                    Restcontext.response.statusCode = 400;
                    System.debug('============= response : ' + JSON.serialize(objResponseWrapper));
                    return;                 
                }   
            }

}   

    public static Datetime makeDate(string vdate){        
        //string vdate = '2020-07-19T14:57:48+0400';        
        system.debug(vdate);
        system.debug(vdate.substringBefore('T').substringBefore('+'));
        system.debug(vdate.substringAfter('T').substringBefore('+'));
        String  strDate = vdate.substringBefore('T').substringBefore('+');
        String strTime = vdate.substringAfter('T').substringBefore('+');
        Datetime myDate = datetime.valueOf(strDate +' '+ strTime);
        system.debug(myDate);
        return myDate;
    }

    public class ResponseWrapper {
        public String status;
        public String statusCode;
        public String errorMessage;
       
        public ResponseWrapper() {
            this.status = '';
            this.statusCode = '';
            this.errorMessage = '';
        }
       }
}