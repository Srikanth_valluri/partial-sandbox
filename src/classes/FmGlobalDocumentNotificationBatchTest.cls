@isTest
private class FmGlobalDocumentNotificationBatchTest {

    @testSetup
    static void setupTestData() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;
        upsert new Account(
            Id = portalAccountId,
            Email__pc = 'test@mailinator.com',
            Email__c = 'test@mailinator.com',
            Mobile_Phone_Encrypt__pc = '+971559270956'
        );

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Location__c location = new Location__c(
            Name = 'Unit Name',
            Location_ID__c = 'LOC',
            Property_Name__c = property.Id,
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        Inventory__c damacInventory = TestDataFactory_CRM.createInventory(property.Id);
        damacInventory.Building_Location__c = location.Id;
        damacInventory.Status__c = 'Restricted';
        damacInventory.Property_Country__c = 'United Arab Emirates';
        damacInventory.Tenant__c = portalAccountId;
        insert damacInventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
            unit.Tenant__c = portalAccountId;
            unit.Registration_Status__c = 'Agreement Executed by Damac';
            unit.Property_Country__c = 'United Arab Emirates';
            unit.Property_City__c = 'DUBAI';
        }
        insert lstBookingUnit;

        insert new Board_Member__c(
            Property__c = property.Id,
            Board_Member__c = portalAccountId
        );

        insert TestDataFactory_CRM.createActiveFT_CS();

        List<FM_Document_Upload_Batch__c> lstUploadBatch = new List<FM_Document_Upload_Batch__c>();
        lstUploadBatch.add(uploadBatchForGlobalDocuments('Everyone'));
        for (FM_Document_Upload_Batch__c uploadBatch : lstUploadBatch) {
            uploadBatch.Current_Approver__c = UserInfo.getUserId();
            uploadBatch.Approval_Status__c = 'Approved';
        }

        //Except DTPC
        List<FM_Document_Upload_Batch__c> lstUploadBatch1 = new List<FM_Document_Upload_Batch__c>();
        lstUploadBatch1.add(uploadBatchForExceptDTPCDocuments('Everyone'));
        for (FM_Document_Upload_Batch__c uploadBatch : lstUploadBatch1) {
            uploadBatch.Current_Approver__c = UserInfo.getUserId();
            uploadBatch.Approval_Status__c = 'Approved';
        }

        Upsert lstUploadBatch1;



    }

    @isTest
    static void emailDocumentsToFmRecipientsTest() {
        //Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());

        FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            'https://api.sendgrid.com/v3/mail/send' => sendGridResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));


        Test.startTest();
            for (FM_Document_Upload_Batch__c fmDocUploadBatch : [
                SELECT  Id
                FROM    FM_Document_Upload_Batch__c
                WHERE   Document_Level__c = 'Global'
            ]) {
                Database.executeBatch(new FmGlobalDocumentEmailNotificationBatch(fmDocUploadBatch.Id));
            }
        Test.stopTest();
    }

    @isTest
    static void emailExceptDTPCDocumentsToFmRecipientsTest() {
        //Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());

        FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            'https://api.sendgrid.com/v3/mail/send' => sendGridResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));


        Test.startTest();
            for (FM_Document_Upload_Batch__c fmDocUploadBatch : [
                SELECT  Id,
                        Document_Level__c    
                FROM    FM_Document_Upload_Batch__c
                WHERE   Document_Level__c = 'Everyone except DTPC'
            ]) {
                Database.executeBatch(new FmGlobalDocumentEmailNotificationBatch(fmDocUploadBatch.Id, fmDocUploadBatch.Document_Level__c));
            }
        Test.stopTest();
    }

    static FM_Document_Upload_Batch__c uploadBatchForPropertyLevelDocuments(String fmRecipient) {
        String docType = 'Notice', docLevel = 'property';
        FM_Document_Upload_Batch__c uploadBatch = new FM_Document_Upload_Batch__c(
            Document_Type__c = docType,
            Document_Level__c = (
              docLevel == 'global' ? 'Global' : (docLevel == 'building' ? 'Building Specific' : 'Property Specific')
            ),
            FM_Recipient__c = fmRecipient,
            Notification_Email_Subject__c = 'Test',
            Notification_Email_Message__c = 'Hello and Bye',
            Approval_Status__c = 'Pending'
        );
        insert uploadBatch;

        Property__c property = [SELECT Id FROM Property__c LIMIT 1];
        Location__c location = [SELECT Id FROM Location__c LIMIT 1];

        SR_Attachments__c doc = new SR_Attachments__c(
            FM_Document_Upload_Batch__c = uploadBatch.Id,
            FM_Recipient__c = fmRecipient,
            Property__c = property.Id,
            Document_Type__c = docType
        );
        insert doc;

        return uploadBatch;

    }

    static FM_Document_Upload_Batch__c uploadBatchForBuildingLevelDocuments(String fmRecipient) {
        String docType = 'Notice', docLevel = 'building';
        FM_Document_Upload_Batch__c uploadBatch = new FM_Document_Upload_Batch__c(
            Document_Type__c = docType,
            Document_Level__c = (
              docLevel == 'global' ? 'Global' : (docLevel == 'building' ? 'Building Specific' : 'Property Specific')
            ),
            FM_Recipient__c = fmRecipient,
            Notification_Email_Subject__c = 'Test',
            Notification_Email_Message__c = 'Hello and Bye',
            Approval_Status__c = 'Pending'
        );
        insert uploadBatch;

        Property__c property = [SELECT Id FROM Property__c LIMIT 1];
        Location__c building = [SELECT Id FROM Location__c LIMIT 1];

        SR_Attachments__c doc = new SR_Attachments__c(
            FM_Document_Upload_Batch__c = uploadBatch.Id,
            FM_Recipient__c = fmRecipient,
            Building__c = building.Id,
            Document_Type__c = docType
        );
        insert doc;

        return uploadBatch;

    }

    static FM_Document_Upload_Batch__c uploadBatchForGlobalDocuments(String fmRecipient) {
        String docType = 'Notice', docLevel = 'global';
        FM_Document_Upload_Batch__c uploadBatch = new FM_Document_Upload_Batch__c(
            Document_Type__c = docType,
            Document_Level__c = (
              docLevel == 'global' ? 'Global' : (docLevel == 'building' ? 'Building Specific' : 'Property Specific')
            ),
            FM_Recipient__c = fmRecipient,
            Notification_Email_Subject__c = 'Test',
            Notification_Email_Message__c = 'Hello and Bye',
            Approval_Status__c = 'Pending'
        );
        insert uploadBatch;

        SR_Attachments__c doc = new SR_Attachments__c(
            FM_Document_Upload_Batch__c = uploadBatch.Id,
            FM_Recipient__c = fmRecipient,
            Document_Type__c = docType
        );
        insert doc;

        return uploadBatch;

    }

    static FM_Document_Upload_Batch__c uploadBatchForExceptDTPCDocuments(String fmRecipient) {
        String docType = 'Notice', docLevel = 'Except DTPC';
        FM_Document_Upload_Batch__c uploadBatch = new FM_Document_Upload_Batch__c(
            Document_Type__c = docType,
            Document_Level__c = (
              docLevel == 'Except DTPC' ? 'Everyone except DTPC' :(docLevel == 'global' ? 'Global' : (docLevel == 'building' ? 'Building Specific' : 'Property Specific'))
            ),
            FM_Recipient__c = fmRecipient,
            Notification_Email_Subject__c = 'Test',
            Notification_Email_Message__c = 'Hello and Bye',
            Notification_SMS_Message__c = 'Hello test',
            Approval_Status__c = 'Pending'
        );
        insert uploadBatch;

        SR_Attachments__c doc = new SR_Attachments__c(
            FM_Document_Upload_Batch__c = uploadBatch.Id,
            FM_Recipient__c = fmRecipient,
            Document_Type__c = docType
        );
        insert doc;

        return uploadBatch;

    }

}