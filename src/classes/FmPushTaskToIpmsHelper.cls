/*
Test Class : FmTaskInvocableClassTest
*/
public without sharing class FmPushTaskToIpmsHelper implements Queueable, Database.AllowsCallouts {
    public set<Id> setTaskIds;
    public set<Id> setFmCaseIds;
    
    public void execute(QueueableContext context) {
        createIpmsTask(setTaskIds,setFmCaseIds);
    }
    
    public void createIpmsTask(set<Id> setTaskIds, set<Id> setFmCaseIds){
        map<Id,FM_Case__c> mapId_FmCase = new map<Id,FM_Case__c>(FM_Utility.fetchFmCaseDetails(setFmCaseIds));
        system.debug('mapId_FmCase*****'+mapId_FmCase);
        
        map<Id,Task> mapId_Task = new map<Id,Task>(FM_Utility.fetchTaskDetails(setTaskIds));
        system.debug('mapId_Task*****'+mapId_Task);
        
        GenerateIpmsBeans objCls = new GenerateIpmsBeans();
        objCls.mapIpmsBeans(mapId_Task.values(),mapId_FmCase);
        
        /*
        FM_PushTaskToIPMS objClass = new FM_PushTaskToIPMS();
        String response = objClass.pushTaskToIpms(regTerms);
        system.debug('response*****'+response);
        */
    }
}