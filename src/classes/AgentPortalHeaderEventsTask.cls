/**************************************************************************************************
* Name               : 
* Description        : An apex page controller for showing all active announcements                                             
* Created Date       : 31/07/2017                                                                         
* Created By         : Pratiksha Narvekar                                                                 
* Last Modified Date :  30.8.2017                                                                          
* Last Modified By   :  Lovel                                                                          
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                Comments                                                                   
* 1.0         Pratiksha                 30.8.2017           Initial draft
**************************************************************************************************/
 
public with sharing class AgentPortalHeaderEventsTask {
	public Decimal NotificationCount{get;set;}
	public Decimal AnnouncementCount{get;set;}
	public Decimal TaskCount{get;set;}
	DetailsClass objDetailsClass;
	public DetailsClass getNotification(){
		objDetailsClass = new DetailsClass();
		return objDetailsClass;
	}
    public DetailsClass getAnnouncement(){
		objDetailsClass = new DetailsClass();
		return objDetailsClass;
	}
	public DetailsClass getTask(){
		objDetailsClass = new DetailsClass();
		return objDetailsClass;
	}
    public class DetailsClass{
    
    }
}