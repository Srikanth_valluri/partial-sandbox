/*
* Name : Pavithra Gajendra
* Date : 02/08/2017
* Purpose : Test class for Account handler
* Company : NSI Gulf
*
*/
@isTest(seeAllData=true)
private class AccountTrgHandlerTest {

    public static Account acc ;
    public static Contact cont1 ;

    @isTest static void blacklistAgency() {

        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            Test.startTest();
            acc = InitialiseTestData.getCorporateAccount('Test Agency11');
            insert acc ;

            List<Agent_Site__c> lstagsites = new List<Agent_Site__c>();
            lstagsites.add(new Agent_Site__c(Start_Date__c=system.today(),End_Date__c = system.today(),Agency__c = acc.id));
            lstagsites.add(new Agent_Site__c(Start_Date__c=system.today(),IsAccountBlackListedTerminated__c = true,Agency__c = acc.id));
            insert lstagsites;

            cont1 = InitialiseTestData.getAgentContact('Agency Con',acc.Id);
            insert cont1 ;
            User contactUser1 = InitialiseTestData.getPortalUser('Agent11@mail.com',cont1.Id);
            insert contactUser1 ;
            acc.Blacklisted__c = true ;
            update acc ;
            User blacklistedUser = [Select id,isActive FROM User Where Id=:contactUser1.Id];
            acc.Blacklisted__c = false ;
            update acc ;
            System.debug('blacklistedUser '+blacklistedUser);
            Test.stopTest();
        }
    }


    @isTest static void unTerminateAgency() {
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz2@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz2@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            Test.startTest();
            acc = InitialiseTestData.getTerminatedAccount('Test Agency12');
            insert acc ;
            cont1 = InitialiseTestData.getAgentContact('Agency Con',acc.Id);
            insert cont1 ;
            User contactUser2 = InitialiseTestData.getPortalUser('Agent12@mail.com',cont1.Id);
            insert contactUser2 ;
            acc.Terminated__c = false ;
            update acc ;
            User unTerminatedUser = [Select id,isActive FROM User Where Id=:contactUser2.Id];
            System.assertEquals(unTerminatedUser.isActive, true);
            Test.stopTest();
        }
    }

    @isTest static void encrypt(){

        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz3@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz3@email.com', UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            Test.startTest();
            List<Account> lstAccount = new List<Account>();
            Id RecTypeIndividual = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            Id RecTypepersonal = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            List<Account> lstaccounts = new List<account>();
            lstaccounts.add(new Account(Name = 'testLN',recordtypeid = RecTypeIndividual));
            lstaccounts.add(new Account(FirstName = 'testLN',LastName ='teste',recordtypeid = RecTypepersonal));
            lstaccounts.add(new Account(FirstName = 'testLN',LastName ='teste',recordtypeid = RecTypepersonal,
                                        Asst_Phone_Country_Code__c = 'India: 0091',Asst_Phone_Encrypt__c = ''));
            lstaccounts.add(new Account(FirstName = 'testLN',LastName ='teste',recordtypeid = RecTypepersonal,
                                        Asst_Phone_Encrypt__c = 'wdwsfddf'));

            lstaccounts.add(new Account(FirstName = 'testLN',LastName ='teste',recordtypeid = RecTypepersonal,
                                        Home_Phone_Country_Code__c = 'India: 0091',Home_Phone_Encrypt__c = ''));
            lstaccounts.add(new Account(FirstName = 'testLN',LastName ='teste',recordtypeid = RecTypepersonal,
                                        Home_Phone_Encrypt__c = 'wdwsfddf'));

            lstaccounts.add(new Account(FirstName = 'testLN',LastName ='teste',recordtypeid = RecTypepersonal,
                                        Mobile_Country_Code__c = 'India: 0091',Mobile_Phone_Encrypt__c = ''));
            lstaccounts.add(new Account(FirstName = 'testLN',LastName ='teste',recordtypeid = RecTypepersonal,
                                        Mobile_Phone_Encrypt__c = 'wdwsfddf'));

            lstaccounts.add(new Account(FirstName = 'testLN',LastName ='teste',recordtypeid = RecTypepersonal,
                                        Other_Phone_Country_Code__c = 'India: 0091',Other_Phone_Encrypt__c = ''));
            lstaccounts.add(new Account(FirstName = 'testLN',LastName ='teste',recordtypeid = RecTypepersonal,
                                        Other_Phone_Encrypt__c = 'wdwsfddf'));

            lstaccounts.add(new Account(FirstName = 'testLN',LastName ='teste',recordtypeid = RecTypepersonal,
                                        Phone_Country_Code__c = 'India: 0091',Phone_Encrypt__c = ''));
            lstaccounts.add(new Account(FirstName = 'testLN',LastName ='teste',recordtypeid = RecTypepersonal,
                                        Phone_Encrypt__c = 'wdwsfddf'));
            /*lstaccounts.add(new Account(FirstName = 'testLN',LastName ='teste',recordtypeid = RecTypepersonal,
                                        Asst_Phone_Country_Code__c = 'India: 0091',Asst_Phone_Encrypt__c = 'encryt',
                                        Home_Phone_Country_Code__c = 'India: 0091',Home_Phone_Encrypt__c = 'asda',
                                        Mobile_Country_Code__c = 'India: 0091',Mobile_Phone_Encrypt__c = 'asdasd',
                                        Other_Phone_Country_Code__c = 'India: 0091',Other_Phone_Encrypt__c = 'rwerwe',
                                        Phone_Country_Code__c = 'India: 0091',Phone_Encrypt__c= 'rwerwe'));*/

            try{        insert lstaccounts;}
            catch(exception ex){}
            //objinstance.encryptMobileNumbers(new list<account> {acc,pacc});
            Test.stopTest();
        }
    }

    @isTest static void OwnerChange() {
        Map<String, Profile> profileInfo = DamacUtility.getProfileDetails(new Set<String>{
                                            'Property Consultant',
                                            'Head of Sales'});


        User  HOS = new User();
        HOS.FirstName = 'test';
        HOS.LastName = 'HOS';
        HOS.ProfileId = profileInfo.get('Head of Sales').Id;
        HOS.email = 'testhos@gmail.com';
        HOS.alias = 'testhos1';
        HOS.emailencodingkey='UTF-8';
        HOS.languagelocalekey='en_US';
        HOS.localesidkey='en_US';
        HOS.country='United Arab Emirates';
        HOS.IsActive =true;
        HOS.timezonesidkey='America/Los_Angeles';
        HOS.username='testhos@account.com';
        HOS.Languages_Known__c='English;Arabic;Hindi;French';
        insert HOS;

        User PC = new User();
        PC.FirstName = 'test';
        PC.LastName = 'PC';
        PC.ProfileId = profileInfo.get('Property Consultant').Id;
        PC.email = 'testpc@gmail.com';
        PC.alias = 'testpc2';
        PC.emailencodingkey='UTF-8';
        PC.languagelocalekey='en_US';
        PC.localesidkey='en_US';
        PC.country='United Arab Emirates';
        PC.IsActive =true;
        PC.timezonesidkey='America/Los_Angeles';
        PC.username='testpc@account.com';
        PC.Languages_Known__c='English;Arabic;Hindi;French';
        insert PC;

        Account acc = new Account();
        acc.LastName = 'Test Account';
        acc.OwnerId = PC.Id;
        acc.Agency_Type__c = 'Individual';
        acc.Vendor_ID__c = '0000009999';
        acc.recordtypeId = DamacUtility.getRecordTypeId('Account',Label.Account_Individual_RecordType);
        insert acc;

        Test.startTest();
        acc.OwnerId = HOS.Id;
        acc.Vendor_ID__c = '00099009900';
        update acc;
        Test.stopTest();
    }
}