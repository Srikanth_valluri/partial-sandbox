/*
----------------------------------------------------------------------
* Name               : emailValidation
* Description        : This is to Call Zero Bounce WS to Validate Numbers.
* Created Date       : 17/01/2018                                                                
* Created By         : Alok Chauhan                                                       
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE                                                              
* 1.0         Alok Chauhan    17/01/2018 
--------------------------------------------------------------------------
*/
global class emailValidation{
    public static String isValid;
    public static Map<String, Object> responseMap;
    public static Map<String, Object> sendZBCallout(string email){
        try{
            
            String endPointURL = System.Label.ZB;
            String apiKey = System.Label.ZBAPI_Key;
            String finalURL = endPointURL+'apikey='+apikey+'&email='+email;
            
            
            Httprequest request = new HttpRequest();
            Http http = new Http();
            
            request.setMethod('GET');
            request.setEndpoint(finalURL);
            request.setTimeout(120000); 
            
            //Making call to external REST API
            HttpResponse response = http.send(request);  
            responseMap = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
            
            System.debug('responseBody: '+response.getBody());
        }catch(Exception e){
            System.debug('Error::'+e.getMessage());
        }
        return(responseMap);
    }
}