/***************************************************************************************************************
Description: Class to send mail via SendGrid. It is replacement of Std Send Mail on Account
________________________________________________________________________________________________________________
Version Date(DD-MM-YYYY)    Author              Description
----------------------------------------------------------------------------------------------------------------
1.0     03-05-2020          Aishwarya Todkar    Initial Draft
****************************************************************************************************************/
public class AccountSendEmailExtension {

    public String fromAddress { get; set; }
    public String toAddress { get; set; }
    public String replyToAddress { get; set; }
    public String toName { get; set; }
    public String additionalTo { get; set; }
    public String ccAddress { get; set; }
    public String bccAddress { get; set; }
    public String subject { get; set; }
    public String emailFormat { get; set; }
    public String emailBody { get; set; }
    public String selectedTemplateFolder { get; set; }
    public String selectedEmailTemplate { get; set; } 
    public String selectedDocId { get; set; }
    public String selectedFileLocation { get; set; } 
    public String emailSearchKey {get;set;}
    public String docSearchKey {get;set;}
    public String emailType {get; set;}
    public String docJosn {get; set;}
    public Boolean sizeOfMap {get;set;}
    public Boolean validEmail { get; set; }
    public List<SelectOption> lstFromAddress{ get; set; }
    public List<SelectOption> lstToAddress{ get; set; }
    public List<SelectOption> listTemplateFolder { get; set; }
    public List<SelectOption> listFileLocation { get; set; }
    public Map<Id, EmailTemplate> mapOfEmailTemplate { get; set; }
    public List<Document> listExistingDocs { get; set; }
    public List<Attachment> listNewAddingDocs { get; set; }
    public List<Attachment> listFinalAttach { get; set; }
    public EmailTemplate selectedTemplateObj { get; set; }
    Id accountId,buId,recordId;
    public Integer counter=0;
    public Integer list_size=10;
    public Integer total_size;
    public Integer counterForAttachment=0;
    public Integer listSizeForAttachment=10;
    public Integer totalSizeForAttachment;

    public AccountSendEmailExtension(ApexPages.StandardController controller) {
        
        listFinalAttach = new List<Attachment>();
        sizeOfMap = false;
        validEmail = true;
        emailBody = '';
        getTemplateFolders();
        getFileLocations();
        getEmailTemplatesOnSelect();
        selectedTemplateObj = new EmailTemplate();
        recordId = ApexPages.currentPage().getParameters().get('id');
        init();
    }

/***************************************************************************************************************
Description: Method for intializations
Return Type: void
Parameters : None
****************************************************************************************************************/
    public void init() {

        bccAddress = Label.Default_BCC_Address;
        if( recordId != null ) {
            if( String.valueOf( recordId ).startsWith( '001' ) ) {
                accountId = recordId;
            }
            else if( String.valueOf( recordId ).startsWith( 'a0x' ) ) {
                buId = recordId;
                List<Booking_Unit__c> listBu = new List<Booking_Unit__c>( [ SELECT
                                                                                Booking__r.Account__c
                                                                            FROM
                                                                                Booking_Unit__c
                                                                            WHERE
                                                                                Id =: buId] );
                if( listBu.size() > 0 )
                    accountId = listBu[0].Booking__r.Account__c;
                else {
                    validEmail = false;
                    ApexPages.addmessage( 
                        new ApexPages.message( 
                            ApexPages.severity.Error
                            , 'Booking Unit not found!' 
                        )
                    );
                }
            }
            if( accountId != null ) {

                //Get Account details
                List<Account> listAcc = [ SELECT
                                                Id
                                                , Name
                                                , Email__pc 
                                                , Email_2__pc
                                                , Email_3__pc
                                                , Email_4__pc
                                                , Email_5__pc
                                                , Email__c
                                                , Email_1__c
                                                , Email_2__c
                                                , Email_3__c
                                                , IsPersonAccount
                                            FROM
                                                Account
                                            WHERE
                                                Id =: accountId
                                            AND
                                                ( 
                                                    ( IsPersonAccount = true
                                                        AND ( Email__pc != null 
                                                            OR Email_2__pc != null 
                                                            OR Email_3__pc != null 
                                                            OR Email_4__pc != null
                                                            OR Email_5__pc != null
                                                        )
                                                    ) 
                                                    OR ( IsPersonAccount  = false
                                                        AND ( Email__c != null 
                                                            OR Email_2__c != null 
                                                            OR Email_3__c != null
                                                        ) 
                                                    )
                                                )
                                            ];
                //Set email details
                if( listAcc != null && listAcc.size() > 0 ) {
                    lstToAddress = new List<SelectOption>();
                    lstFromAddress = new List<SelectOption>();
                    Account objAcc = listAcc[ 0 ];
                    toName = objAcc.Name;
                    
                    //Set to address with masking
                    if( objAcc.IsPersonAccount ) {
                        if( objAcc.Email__pc != null )
                            lstToAddress.add( new SelectOption( objAcc.Email__pc, objAcc.Email__pc.replaceAll('(^[^@]{2}|(?!^)\\G)[^@]', '$1*') ) );
                        if( objAcc.Email_2__pc != null )
                            lstToAddress.add( new SelectOption( objAcc.Email_2__pc, objAcc.Email_2__pc.replaceAll('(^[^@]{2}|(?!^)\\G)[^@]', '$1*') ) );  
                        if( objAcc.Email_3__pc != null )
                            lstToAddress.add( new SelectOption( objAcc.Email_3__pc, objAcc.Email_3__pc.replaceAll('(^[^@]{2}|(?!^)\\G)[^@]', '$1*') ) );
                        if( objAcc.Email_4__pc != null )
                            lstToAddress.add( new SelectOption( objAcc.Email_4__pc, objAcc.Email_4__pc.replaceAll('(^[^@]{2}|(?!^)\\G)[^@]', '$1*') ) ); 
                        if( objAcc.Email_5__pc != null )
                            lstToAddress.add( new SelectOption( objAcc.Email_5__pc, objAcc.Email_5__pc.replaceAll('(^[^@]{2}|(?!^)\\G)[^@]', '$1*') ) );  
                    }
                    else {
                        if( objAcc.Email__c != null )
                            lstToAddress.add( new SelectOption( objAcc.Email__c, objAcc.Email__c.replaceAll('(^[^@]{2}|(?!^)\\G)[^@]', '$1*') ) ); 
                        if( objAcc.Email_2__c != null )
                            lstToAddress.add( new SelectOption( objAcc.Email_2__c, objAcc.Email_2__c.replaceAll('(^[^@]{2}|(?!^)\\G)[^@]', '$1*') ) ); 
                        if( objAcc.Email_3__c != null )
                            lstToAddress.add( new SelectOption( objAcc.Email_3__c, objAcc.Email_3__c.replaceAll('(^[^@]{2}|(?!^)\\G)[^@]', '$1*') ) );  
                    }
                    System.debug( 'lstToAddress -- ' + lstToAddress);

                    //Set from address
                    for( String fromAdd : Custom_Send_Email_From_Address__c.getAll().keySet() ) {
                        lstFromAddress.add( new SelectOption( fromAdd, fromAdd ) );
                    }
                    System.debug( 'lstFromAddress -- ' + lstFromAddress);

                }
                else {
                    validEmail = false;
                    ApexPages.addmessage( 
                        new ApexPages.message( 
                            ApexPages.severity.Error
                            , 'Please make sure email address is updated on Account!' 
                        )
                    );
                }
            }
            else {
                validEmail = false;
                ApexPages.addmessage( 
                    new ApexPages.message( 
                        ApexPages.severity.Error
                        , 'Account not found!' 
                    )
                );
            }
        }
        else {
            validEmail = false;
            ApexPages.addmessage( 
                new ApexPages.message( 
                    ApexPages.severity.Error
                    , 'Record Id can not be blank!' 
                )
            );
        }
    }

/***************************************************************************************************************
Description: Method to send email via SendGrid
Return Type: void
Parameters : pageReference
****************************************************************************************************************/   
    public pageReference sendEmail() {
        System.debug( 'emailFormat ==' + emailFormat);
        emailBody = emailBody.replace(']]>','');
        System.debug( 'emailBody ==' + emailBody);
        System.debug( 'docJosn ==' + docJosn);
        System.debug('additionalTo-->'+additionalTo);
        listFinalAttach = new List<Attachment>();
        
        System.debug('bccAddress-->'+bccAddress);
        if( String.isNotBlank(additionalTo)) {
            toAddress = additionalTo + ','+ toAddress;
        }
        System.debug('toAddress-->'+toAddress );
        /****************************************************************************************************************************
            Preparing Attachments
        ****************************************************************************************************************************/
        if( String.isNotBlank( docJosn ) ) { 
            try {
                List<DocumentWrapper> wrapList = new List<DocumentWrapper>();
                wrapList = ( List<DocumentWrapper> )JSON.deserialize( docJosn, List<DocumentWrapper>.Class);
                System.debug('wrapList.size-->'+wrapList.size() );
                Set<String> setDocId = new Set<String>();

                for( DocumentWrapper wrapObj : wrapList ) {
                    System.debug('wrapObj -- ' + wrapObj);
                    if( wrapObj.docType.equalsIgnoreCase('new') ) {

                        //Uploaded from local system
                        String strDocBody = EncodingUtil.base64Decode( wrapObj.docBodyOrId ).toString();
                        Blob DocBody = EncodingUtil.base64Decode( strDocBody.substring( strDocBody.lastIndexOf(',')+1 ) );
                        Attachment at = new Attachment();
                        at.name = wrapObj.docName;
                        at.body = DocBody;
                        listFinalAttach.add( at );
                    }
                    else {
                        //uploaded from Salesforce document
                        setDocId.add( wrapObj.docBodyOrId);
                    }
                }
                if( setDocId != null && setDocId.size() > 0 ) {
                    for( Document doc: [SELECT Id
                                                , Name
                                                , Body
                                                , Type
                                            FROM
                                                Document
                                            WHERE
                                                Id IN : setDocId ] ) {
                            system.debug( 'doc -- ' + doc);
                        String docName = !doc.Name.contains('.') || ( doc.Name.contains('.') && String.isBlank( doc.Name.subStringAfter('.') ) ) 
                                        ? doc.Name + '.' + doc.Type : doc.Name;
                        listFinalAttach.add( new Attachment( body = doc.body , Name = docName ) );
                    }
                }
            }
            catch( Exception e ) {
                validEmail = false;
                ApexPages.addmessage( 
                    new ApexPages.message( 
                        ApexPages.severity.Error
                        , e.getMessage() 
                    )
                );
            }
            System.debug( 'listFinalAttach ==' + listFinalAttach);
            
        }//End docJson if

        if( String.isNotBlank( fromAddress ) 
        && String.isNotBlank( toAddress ) ) {

            String contentType = 'text/html';
            SendGridEmailService.SendGridResponse objSendGridResponse = SendGridEmailService.sendEmailService(
                                                                                toAddress, toName
                                                                                , ccAddress, ''
                                                                                , bccAddress, ''
                                                                                , subject, ''
                                                                                , fromAddress, ''
                                                                                , replyToAddress, ''
                                                                                , contentType
                                                                                , emailBody, ''
                                                                                , listFinalAttach );
        
            system.debug('objSendGridResponse === ' + objSendGridResponse);
            
            String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
            System.debug('responseStatus== ' + responseStatus);

            //Create reportable activity 
            if (responseStatus == 'Accepted') {
                EmailMessage mail = new EmailMessage();
                mail.Subject = subject;
                mail.MessageDate = System.Today();
                mail.Status = '3';//'Sent';
                mail.RelatedToId = accountId;
                //mail.Type__c = 'Robo call picked up notification';
                mail.ToAddress = toAddress;
                mail.FromAddress = fromAddress;
                mail.TextBody = emailBody.replaceAll('\\<.*?\\>', '');
                mail.HtmlBody = emailBody;
                mail.Sent_By_Sendgrid__c = true;
                mail.SentGrid_MessageId__c = objSendGridResponse.MessageId;
                mail.CcAddress = ccAddress;
                mail.BccAddress = bccAddress;
                insert mail;
                System.debug('Mail obj == ' + mail);

                
                if( listFinalAttach != null && listFinalAttach.size() > 0 ) {
                    for( Attachment att : listFinalAttach) {
                        att.ParentId = mail.Id;
                    }

                    insert listFinalAttach;
                }

                return new pageReference('/' + accountId);
            }
        }
        return null;
    }

/***************************************************************************************************************
Description: Method to get Email Template folders
Return Type: void
Parameters : none
****************************************************************************************************************/    
    public void getTemplateFolders() {
        listTemplateFolder = new List<SelectOption>();
        for( Folder objFolder : [SELECT 
                                    Id
                                    , Name
                                    , Type
                                FROM 
                                    Folder
                                WHERE
                                    Type = 'Email'] ) {
            listTemplateFolder.add( new SelectOption( objFolder.Id, objFolder.Name ) );                            
        }
        selectedTemplateFolder =  listTemplateFolder[0].getValue();
    }

/***************************************************************************************************************
Description: Method to get email templates on change of folder
Return Type: void
Parameters : pageReference
****************************************************************************************************************/ 
    public void getEmailTemplatesOnSelect() {
        
        System.debug( ' selectedTemplateFolder -- ' + selectedTemplateFolder );
        initPagenination( 'Email' );
        getEmailTemplates();

    }

/***************************************************************************************************************
Description: Method to get email templates on change of pagination
Return Type: void
Parameters : pageReference
****************************************************************************************************************/ 
        public void getEmailTemplates() {

        System.debug( ' selectedTemplateFolder -- ' + selectedTemplateFolder );
        mapOfEmailTemplate = new Map<Id, EmailTemplate >();
        
        if( String.isNotBlank( selectedTemplateFolder ) ) {
            for( EmailTemplate objTemplate : [ SELECT
                                                    Id
                                                    , Name
                                                    , Body
                                                    , HtmlValue
                                                    , MarkUp
                                                    , Subject
                                                    , TemplateType
                                                    , Description
                                                    , BrandTemplateId
                                                FROM
                                                    EmailTemplate
                                                WHERE
                                                    FolderId =: selectedTemplateFolder
                                                LIMIT : list_size
                                                OFFSET  : counter]) {
                mapOfEmailTemplate.put( objTemplate.Id, objTemplate );
                total_size= [ SELECT  
                                COUNT()
                            FROM
                                EmailTemplate
                            WHERE
                                FolderId =: selectedTemplateFolder
                            ];
            }
        }

        //System.debug(' listEmailTemplates-->' + listEmailTemplates );
    }

/***************************************************************************************************************
Description: Method to set email details on selection of template 
Return Type: void
Parameters : none
****************************************************************************************************************/  
    public void setEmailTemplate() {
        System.debug( 'selectedEmailTemplate --' + selectedEmailTemplate );
        selectedTemplateObj = new EmailTemplate();
        if( mapOfEmailTemplate.containsKey( selectedEmailTemplate  ) ) {
            selectedTemplateObj = mapOfEmailTemplate.get( selectedEmailTemplate );
            System.debug( 'selectedTemplateObj --' + selectedTemplateObj );
            emailType = selectedTemplateObj.TemplateType;
            if( selectedTemplateObj.TemplateType.equalsIgnoreCase( 'HTML' ) ) {
                emailBody = selectedTemplateObj.HtmlValue;
            }
            else if ( selectedTemplateObj.TemplateType.equalsIgnoreCase( 'Custom' ) )
                emailBody = selectedTemplateObj.HtmlValue;
            else if( selectedTemplateObj.TemplateType.equalsIgnoreCase( 'Visualforce' ) )
                emailBody = selectedTemplateObj.MarkUp;
            else
                emailBody = selectedTemplateObj.Body;
            
            String tempSubject = replaceMergeFields( selectedTemplateObj.subject );
            
            subject = String.isNotBlank( tempSubject ) ? tempSubject : selectedTemplateObj.subject;

            String newEmailBody = replaceMergeFields( emailBody );

            if( String.isNotBlank( emailBody ) ) {
                emailBody = newEmailBody;
            }
        }
    }

/***************************************************************************************************************
Description: Method to get document folders
Return Type: void
Parameters : none
****************************************************************************************************************/ 
    public void getFileLocations() {

        listNewAddingDocs = new List<Attachment>();
        listFileLocation = new List<SelectOption>();
        listFileLocation.add( new SelectOption( 'None', 'My Computer' ) );    
        for( Folder objFolder : [SELECT 
                                    Id
                                    , Name
                                    , Type
                                FROM 
                                    Folder
                                WHERE
                                    Type = 'Document'] ) {
            listFileLocation.add( new SelectOption( objFolder.Id, objFolder.Name ) );                            
        }
        selectedFileLocation =  listFileLocation[0].getValue();
    }

/***************************************************************************************************************
Description: Method to get document of selected folder
Return Type: void
Parameters : none
****************************************************************************************************************/
    public void getAttachments() {

        listExistingDocs = new List<Document>();
        listExistingDocs = [ SELECT
                                Id
                                , Name
                                , Author.Name
                                , BodyLength
                            FROM
                                Document
                            WHERE
                                FolderId =: selectedFileLocation
                           LIMIT   :listSizeForAttachment
                           OFFSET  :counterForAttachment ];
         totalSizeForAttachment =  [SELECT count()                               
                                   FROM
                                        Document
                                   WHERE
                                   FolderId =: selectedFileLocation ];
         //listExistingDocs != null && listExistingDocs.size() > 0 ? listExistingDocs.size() : 0;
                           
    }

/***************************************************************************************************************
Description: Method to get documents on change of folder
Return Type: void
Parameters : none
****************************************************************************************************************/
    public void getAttachmentsOnChange() {
        
        docSearchKey='';
        initPagenination( 'Attachment' );
        getAttachments();
                             
    }

/***************************************************************************************************************
Description: Method to search documents
Return Type: void
Parameters : none
****************************************************************************************************************/
    public void searchDocuments() {
       System.debug('docSearchKey ---'+docSearchKey);
       listExistingDocs = new List<Document>();
       string searchquery='select FolderId,IsInternalUseOnly,Name,Author.Name,BodyLength,'
                           +'Folder.Name from Document where name like \'%'+docSearchKey+'%\' ';
       listExistingDocs = Database.query(searchquery);
       System.debug('listExistingDocs- '+listExistingDocs);
    }

/***************************************************************************************************************
Description: Method to search email templates
Return Type: void
Parameters : none
****************************************************************************************************************/
    public void searchTemplates() {
        mapOfEmailTemplate = new Map<Id, EmailTemplate >();
        string searchquery='SELECT  Id,Name, Body, HtmlValue, MarkUp, Subject, TemplateType,'
                            + 'Description, BrandTemplateId FROM EmailTemplate where name like \'%'+emailSearchKey+'%\' ';
        for(EmailTemplate objTemplate : Database.query(searchquery)) {
            mapOfEmailTemplate.put( objTemplate.Id, objTemplate );
        }
        if(mapOfEmailTemplate.size() == 0) {
            sizeOfMap = true;
        }
        else if (mapOfEmailTemplate.size() > 0) {
            sizeOfMap = false;
        }
        
    }

/***************************************************************************************************************
Description: Method to replcae merge fields
Return Type: String
Parameters : Email Body
****************************************************************************************************************/
    public String replaceMergeFields( String strContents ) {
        if( String.isNotBlank( strContents ) ) {
            Set<String> setApiNames = new Set<String>();

            String processName = buId != null ? 'Custom Send Email-BU' : 'Custom Send Email-Account';
            List<Merge_Field_Mapping__mdt> listMergeFldMdt = new List<Merge_Field_Mapping__mdt>();
            for( Merge_Field_Mapping__mdt mdt : [ SELECT
                                                    MasterLabel
                                                    , Merge_Field__c
                                                    , Process_Name__c
                                                FROM
                                                    Merge_Field_Mapping__mdt
                                                WHERE
                                                    Process_Name__c =: processName] ) {
                setApiNames.add( mdt.MasterLabel );
                listMergeFldMdt.add( mdt );
            }

            if( setApiNames != null && setApiNames.size() > 0 && listMergeFldMdt != null && listMergeFldMdt.size() > 0 ) {
                String strQuery = 'SELECT ';
                for( String apiName : setApiNames ) {
                    strQuery += apiName + ' ,';
                }
                strQuery = strQuery.removeEnd( ',');

                if( processName == 'Custom Send Email-BU' ) {
                    strQuery = strQuery 
                            + ' FROM Booking_Unit__c'
                            + ' WHERE Id=\'' + buId + '\'';
                }
                else {
                    strQuery = strQuery 
                            + ' FROM Accounnt'
                            + ' WHERE Id=\'' + accountId + '\'';
                }

                List<sObject> listObj = Database.query( strQuery );
                if( listObj != null && listObj.size() > 0 ) {
                    
                    for( Merge_Field_Mapping__mdt mdt : listMergeFldMdt) {
                        if( strContents.contains( mdt.Merge_Field__c ) ) {
                            String strVal = getStringValue( mdt.MasterLabel, listObj[0]);
                            
                            if( String.isNotBlank( strVal ) ) {
                                if( mdt.Merge_Field__c.equalsIgnoreCase( '{!Customer_Name__c}' )) {
                                    strVal = GenericUtility.getCamelCase( strVal );
                                }
                                strContents = strContents.replace( mdt.Merge_Field__c , strVal );
                            }
                            else {
                                strContents = strContents.replace( mdt.Merge_Field__c, '');
                            }
                        } 
                    }
                    return strContents;
                }
            }
        }
        return null;  
    }

/**********************************************************************************************
Method Description  : Method to return String value from record
Parameters          : Api Names, sObject Instance
Return Type         : String
**********************************************************************************************/
    public static String getStringValue( String field_API_Name, sObject obj ) {
        
        if( field_API_Name.contains('.') ) {
            String strAfter = field_API_Name.subStringAfter('.');
            String relatedObject1 = field_API_Name.subStringBefore('.');
            if( strAfter.contains('.') ) {
                
                String relatedObject2 = strAfter.subStringBefore('.');
                strAfter = strAfter.subStringAfter('.');

                if( strAfter.contains('.') ) {

                    // 3 level parenting
                    String relatedObject3 = strAfter.subStringBefore('.');
                    String fieldOfRelatedObj = strAfter.subStringAfter('.');
                    System.debug('Parenting 3---');
                    System.debug('fieldOfRelatedObj-'+fieldOfRelatedObj);
                    System.debug('related expression--' + relatedObject1 + '.' + relatedObject2 + '.' + relatedObject3 + '.' + fieldOfRelatedObj );
                    return String.valueOf( obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).getSobject( relatedObject3 ).get( fieldOfRelatedObj ) );
                }
                else {

                    // 2 level parenting
                    String fieldOfRelatedObj = strAfter;
                    System.debug('Parenting 2---');
                    System.debug('fieldOfRelatedObj-'+fieldOfRelatedObj);
                    System.debug('related expression--' + relatedObject1 + '.' + relatedObject2 + '.' + fieldOfRelatedObj );
                    return String.valueOf( obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).get( fieldOfRelatedObj ) );
                }
            }
            else {

                // 1 level parenting
                System.debug('Parenting 1---');
                System.debug('fieldOfRelatedObj-' + strAfter);
                System.debug('related expression--' + relatedObject1 + '.' + strAfter );
                return String.valueOf( obj.getSobject( relatedObject1 ).get( strAfter ) );
            }
        }
        else {
            return String.valueOf( obj.get( field_API_Name ) );
        }
    }
/***************************************************************************************************************
Description: Method to initialize pagination variables
Return Type: void
Parameters : pageReference
****************************************************************************************************************/
    public void initPagenination( String type) {
        if( type.equalsIgnoreCase( 'Email') ) {
            counter = 0;
            list_size = 10;
        }
        else if( type.equalsIgnoreCase( 'Attachment') ) {
            counterForAttachment=0;
            listSizeForAttachment=10;
        }
    }

/***************************************************************************************************************
    Email Template Pagination starts
****************************************************************************************************************/

    public PageReference beginning() {
      counter = 0;
      getEmailTemplates();
      return null;
    }
  
    public PageReference previous() {
        counter -= list_size;
        getEmailTemplates();
        return null;
    }

    public PageReference next() {
        counter += list_size;
        getEmailTemplates();
        return null;
    }
  
    public PageReference end() {
        //counter = total_size - Math.mod(total_size, list_size);
        
        if(Math.mod(total_size, list_size)==0) {
            counter = total_size - list_size;
        }
         
        else {  
            counter = total_size - Math.mod(total_size, list_size);
        }  
          getEmailTemplates(); 
        return null;
    }
  
    public boolean getDisablePrevious() {
        if (counter>0) {
            return false;
        }
        else {
             return true;
        }
    }
  
    public boolean getDisableNext() {
        if (counter + list_size < total_size) {
            return false;
        }
        else {
            return true;
        }
    }

    public Integer getTotal_size() {
        return total_size;
    }
 
    public Integer getPageNumber() {
        return counter/list_size + 1;
    }

    public Integer getTotalPages() {
        if (Math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        }
        else {
            return (total_size/list_size);
        }
    }

/***************************************************************************************************************
    Attachment Pagination starts
****************************************************************************************************************/ 
    public PageReference beginningForAttachment() {
        counterForAttachment = 0;
        getAttachments();
        return null;
    }
  
    public PageReference previousForAttachment() {
        counterForAttachment -= listSizeForAttachment;
        getAttachments();
        return null;
    }

    public PageReference nextForAttachment() {
        counterForAttachment += listSizeForAttachment;
        getAttachments();
        return null;
    }
  
    public PageReference endForAttachment() {
        //counterForAttachment = totalSizeForAttachment - Math.mod(totalSizeForAttachment, listSizeForAttachment);
        //
        if(Math.mod(totalSizeForAttachment, listSizeForAttachment) == 0) {
            counterForAttachment = totalSizeForAttachment - listSizeForAttachment;
        }
        else {
            counterForAttachment = totalSizeForAttachment - Math.mod(totalSizeForAttachment, listSizeForAttachment);
        }
        getAttachments();
        return null;
    }
  
    public boolean getDisablePreviousForAttachment() {
        if (counterForAttachment > 0) {
            return false;
        }
        else {
             return true;
        }
    }
  
    public boolean getDisableNextForAttachment() {
        if (counterForAttachment + listSizeForAttachment < totalSizeForAttachment) {
            return false;
        }
        else {
            return true;
        }
    }

    public Integer getTotalSizeForAttachment() {
        return totalSizeForAttachment;
    }
 
    public Integer getPageNumberForAttachment() {
        return counterForAttachment/listSizeForAttachment + 1;
    }

    public Integer getTotalPagesForAttachment() {
        if (Math.mod(totalSizeForAttachment, listSizeForAttachment) > 0) {
            return totalSizeForAttachment/listSizeForAttachment + 1;
        }
        else {
            return (totalSizeForAttachment/listSizeForAttachment);
        }
    }

    Class DocumentWrapper {
        String docName;
        String docBodyOrId;
        String docType;
    }
    
}