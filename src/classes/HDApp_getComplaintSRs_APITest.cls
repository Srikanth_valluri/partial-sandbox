@isTest
private class HDApp_getComplaintSRs_APITest {

	@isTest
	static void getComplaintsSRTest1() {

		NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'Y',
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;

        Id suggestionRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName()
                                        .get('Suggestion').getRecordTypeId();
        Case objCase = new Case();
        objCase.AccountId = account.Id;
        objCase.Booking_Unit__c = bookingUnit.id; 
        objCase.RecordTypeId = ComplaintProcessUtility.getRecordTypeId();
        objCase.Status=ComplaintProcessUtility.COMPLAINT_SUBMIT_STATUS;
        insert objCase;

        FM_Case__c objFmCase = new FM_Case__c();
        objFmCase.Booking_Unit__c = bookingUnit.id;
        objFmCase.Account__c = account.id;
        objFmCase.RecordtypeId = suggestionRecordTypeId;
        objFmCase.Submitted__c = true;
        objFmCase.Status__c = 'Submitted';
        objFmCase.Type__c = 'Complaint';
        insert objFmCase;

        Test.startTest();

        	RestRequest req = new RestRequest();
	        RestResponse res = new RestResponse();
	        req.requestURI = '/complaintSR/getPastComplaints';  
	        req.addParameter('accountId',account.id);
	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;
	        HDApp_getComplaintSRs_API.getComplaintsSR();

        Test.stopTest();
	}

	@isTest
	static void getComplaintsSRTest2() {

		Test.startTest();

        	RestRequest req = new RestRequest();
	        RestResponse res = new RestResponse();
	        req.requestURI = '/complaintSR/getPastComplaints';  
	        //req.addParameter('accountId',account.id);
	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;
	        HDApp_getComplaintSRs_API.getComplaintsSR();

	        req = new RestRequest();
	        res = new RestResponse();
	        req.requestURI = '/complaintSR/getPastComplaints';  
	        req.addParameter('accountId','');
	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;
	        HDApp_getComplaintSRs_API.getComplaintsSR();

        Test.stopTest();
	} 
}