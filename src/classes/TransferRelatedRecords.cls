/****************************************************************************************************
* Name          : TransferRelatedRecords                                                            *
* Description   : Transfer the PreInquiry related Records to the Inquiry Record                     *
* Created Date  : 15-07-2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE        COMMENTS
* 1.0   Craig Lobo          15-07-2018  Initial Draft.
* 1.1   QBurst              15-07-2018  Modified the task owner change
****************************************************************************************************/

Global without sharing class TransferRelatedRecords {

    public static Map<Id, String> inquiryOwnerMap = new Map<Id, String>();


    public TransferRelatedRecords() {

    }

    /**
     * Update the Travel Details Inquiry Lookup field from Pre Inquiry record to Inquiry Record
     */
    public static void moveTravelDetailRecords(Map<Id, Inquiry__c> newInquiriesMap) {

        List<Travel_Details__c> travelDetailsUpdateList = new List<Travel_Details__c>();
        Map<Id, String> preInquiryMap = new Map<Id, String>();
        Map<Id, String> inquiryOwnerMap = new Map<Id, String>();
        Map<Id, String> inquiryTSAMap = new Map<Id, String>();
        Map<Id, String> inquiryOwnerMobileMap = new Map<Id, String>();
        Map<Id, String> inquiryOwnerEmailMap = new Map<Id, String>();
        for(Inquiry__c inq : newInquiriesMap.values()) {
            if(inq.Pre_InquiryId__c != null) {
                preInquiryMap.put(inq.Id, inq.Pre_InquiryId__c);
                inquiryOwnerMap.put(inq.Id, inq.OwnerId);
                inquiryOwnerMobileMap.put(inq.Id, inq.Owner_Mobile__c);
                inquiryOwnerEmailMap.put(inq.Id, inq.Owner_Email__c);
                if (inq.Telesales_Executive__c != null) {
                    inquiryTSAMap.put(inq.Id, inq.Telesales_Executive__c);
                }
            }
        }
        System.debug('preInquiryMap>>>>>  ' + preInquiryMap);
        System.debug('inquiryOwnerMap>>>>>  ' + inquiryOwnerMap);
        System.debug('preInquiryMap.values()>>>>>  ' + preInquiryMap.values());

        if(!preInquiryMap.isEmpty()) {
            Map<String,List<Travel_Details__c>> inquiryTravelDetailsMap = new Map<String,List<Travel_Details__c>>();
            if(newInquiriesMap != null && !newInquiriesMap.isEmpty()) {
                for(Travel_Details__c td : [ SELECT Id, Inquiry__c
                                            FROM Travel_Details__c
                                            WHERE Inquiry__c IN :preInquiryMap.values()]) {
                    System.debug('####td>>>> ' + td);
                    if(!inquiryTravelDetailsMap.containsKey(td.Inquiry__c)) {
                        inquiryTravelDetailsMap.put(td.Inquiry__c ,new List<Travel_Details__c>{});
                    }
                    inquiryTravelDetailsMap.get(td.Inquiry__c).add(td);
                }
            }
            System.debug('####inquiryTravelDetailsMap>>>> ' + inquiryTravelDetailsMap);
            if(!inquiryTravelDetailsMap.isEmpty()) {
                for(Id inqId : preInquiryMap.keySet()) {
                    System.debug('inqId>>>>>  ' + inqId);
                    if(inquiryTravelDetailsMap.containsKey(preInquiryMap.get(inqId))) {
                        for(Travel_Details__c tdRec : inquiryTravelDetailsMap.get(preInquiryMap.get(inqId))) {
                            System.debug('tdRec>>>>>  ' + tdRec);
                            tdRec.Inquiry__c = inqId; 
                            tdRec.PC_Email__c = inquiryOwnerEmailMap.get(inqId);
                            tdRec.PC_Mobile__c = inquiryOwnerMobileMap.get(inqId);
                            tdRec.Inquiry_Converted__c = true;
                            tdRec.Telesales_Executive__c = inquiryTSAMap.get(inqId);
                            tdRec.OwnerId = inquiryOwnerMap.get(inqId);
                            travelDetailsUpdateList.add(tdRec);
                        }
                    }
                }
            }
        }
        System.debug('travelDetailsUpdateList AFTER INQUIRY>>>>>  ' + travelDetailsUpdateList);
        update travelDetailsUpdateList;
        createTourRecord(travelDetailsUpdateList);
    }

    /** 
     * 
     */
    public static void createTourRecord(List<Travel_Details__c> travedDetailsList) {
        List<Sales_Tours__c> tourList = new List<Sales_Tours__c>();
        for(Travel_Details__c td : travedDetailsList) {
            Sales_Tours__c tour = new Sales_Tours__c();
            tour.Status__c = 'Not Started';
            tour.Inquiry__c = td.Inquiry__c;
            tour.Tour_Type__c = 'Flyin';
            tour.Property_Consultant__c = inquiryOwnerMap.get(td.Inquiry__c);
            tourList.add(tour);
        }
        insert tourList;
    }
    Webservice static void TransferActivitiestoInq (String preInqId){
        system.debug('>>>preInqId>'+preInqId);
        Map<Id,String> inquiryOwnerMap = new Map<Id,String>();
        Map<Id,String> preInquiryMap = new Map<Id,String>();
        List<Task> activites = new List<Task>();
        for(Inquiry__c inq : [SELECT
                                id,Pre_InquiryId__c,ownerId
                            FROM
                                Inquiry__c 
                            
                            WHERE  
                                Pre_InquiryId__c like :preInqId+'%' 
                                ORDER BY createddate DESC LIMIT 1]) {
            if(inq.Pre_InquiryId__c != null) {
                preInquiryMap.put(inq.Id, inq.Pre_InquiryId__c);
                inquiryOwnerMap.put(inq.Id, inq.OwnerId);
            }
        }
        system.debug('>>>preInquiryMap>'+preInquiryMap);
        system.debug('>>inquiryOwnerMap>>'+inquiryOwnerMap);

        if(!preInquiryMap.isEmpty()) {
            Map<String,List<Task>> preInquiryActivities = getPreInquiryActivities(preInquiryMap.values());
            System.debug('preInquiryActivities>>>>>  ' + preInquiryActivities);
            List<String> meetingTypeList = new List<String>(); // 1.1
            meetingTypeList = Label.Activity_Type_for_Owner_Change.split(','); // 1.1
            if(preInquiryActivities != null && !preInquiryActivities.isEmpty()) {
                for(Id inqId : preInquiryMap.keySet()) {
                    System.debug('inqId>>>>>  ' + inqId);
                    if(preInquiryActivities.containsKey(preInquiryMap.get(inqId))) {
                        for(Task activity : preInquiryActivities.get(preInquiryMap.get(inqId))) {
                            System.debug('activity>>>>>  ' + activity);
                            activity.WhatId = inqId;

                            // Updated the Not Started Task Owner to the Inquiry Owner 
                            if (activity.Status == 'Not Started' && inquiryOwnerMap.containsKey(inqId) 
                                && inquiryOwnerMap.get(inqId) != null 
                                    && meetingTypeList.contains(activity.Activity_Type_3__c // 1.1
                                    )) {
                                activity.OwnerId = inquiryOwnerMap.get(inqId);
                                System.debug('Owner Update>>>>>  ' + activity);
                            }
                            activites.add(activity);
                        }
                    }
                }
                if(!activites.isEmpty()) {
                    System.debug('BEFORE UPDATE activites>>>>>  ' + activites);
                    update activites;
                }
            }
        }

    }
    public static Map<String,List<Task>> getPreInquiryActivities(List<String> preInquires) {
        Map<String,List<Task>> preInquiryActivities = new Map<String,List<Task>>();
        if(preInquires != null && !preInquires.isEmpty()) {
            for(Task td : [SELECT Id, WhatId, Subject, Status, OwnerId, Owner.Name, Activity_Type_3__c
                          FROM Task
                          WHERE WhatId In :preInquires]) {
                if(!preInquiryActivities.containsKey(td.WhatId)) {
                    preInquiryActivities.put(td.WhatId,new List<Task>{});
                }
                preInquiryActivities.get(td.WhatId).add(td);
            }
        }
        return preInquiryActivities;
    }
}