@isTest
class CloseTaskForCollectionCreBatchTest {

    public static testMethod void testBatchWithNoDummyInboundCase() {
        User collectionCre = createCollectionCreUser('batchTest@collection1.com', '9999');
        System.assert(collectionCre != null);
        System.assert(collectionCre.Id != null);
        List<Task> tasksForCre = createTasksForCollectionCre(collectionCre, 10, 10);
        System.assertEquals(20, tasksForCre.size());
        for (Task task : tasksForCre) {
            System.assertEquals(false, task.IsClosed);
        }

        Test.startTest();
        Database.executeBatch(new CloseTaskForCollectionCreBatch());
        Test.stopTest();

        List<Task> closedTask = [Select Id, Subject, IsClosed, Status FROM Task];
        System.assertEquals(20, closedTask.size());
        for (Task task : closedTask) {
            System.assertEquals('Closed', task.Status);
        }
    }

    public static testMethod void testBatchWithDummyInboundCase() {
        Case dummyInboundCase = createDummyInboundCase();
        System.assert(dummyInboundCase != null);
        System.assert(dummyInboundCase.Id != null);

        User collectionCre = createCollectionCreUser('batchTest@collection2.com', '9999');
        System.assert(collectionCre != null);
        System.assert(collectionCre.Id != null);
        List<Task> tasksForCre = createTasksForCollectionCre(collectionCre, 10, 10);
        System.assertEquals(20, tasksForCre.size());
        for (Task task : tasksForCre) {
            System.assertEquals(false, task.IsClosed);
        }

        Test.startTest();
        Database.executeBatch(new CloseTaskForCollectionCreBatch());
        Test.stopTest();

        List<Task> closedTask = [Select Id, Subject, IsClosed, Status FROM Task];
        System.assertEquals(20, closedTask.size());
        for (Task task : closedTask) {
            System.assertEquals('Closed', task.Status);
        }
    }

    public static User createCollectionCreUser(String creUserName, String extension) {
        User creUser;
        System.runAs(new User(Id = UserInfo.getUserID())) {
            creUser = new User(
                Username = creUserName,
                Extension = extension,
                Alias = 'TestCRE',
                Email = creUserName,
                Emailencodingkey = 'UTF-8',
                Lastname = 'Test CRE',
                Languagelocalekey = 'en_US',
                Localesidkey = 'en_US',
                ProfileId = [Select Id From Profile Where Name='Collection - CRE'].Id,
                UserRoleId = [Select Id From UserRole Where Name='Collection - CRE'].Id,
                Country = 'United Arab Emirates',
                IsActive = true,
                Timezonesidkey='America/Los_Angeles'
            );
            insert creUser;
        }
        return creUser;
    }

    public static List<Task> createTasksForCollectionCre(
            User collectionCre,
            Integer noOfInboundTasks,
            Integer noOfOutboundTasks
    ) {
        List<Task> tasksForCre = new List<Task>();
        for (Integer index = 0; index < noOfInboundTasks; index++) {
            Task inboundTask = new Task(
                Subject = 'Inbound:999999999' + index,
                Assigned_User__c = 'CRE',
                Type = 'Call',
                Process_Name__c = 'CTI Inbound/Outbound Calls',
                OwnerId = collectionCre.Id
            );
            tasksForCre.add(inboundTask);
        }
        for (Integer index = 0; index < noOfOutboundTasks; index++) {
            Task outboundTask = new Task(
                Subject = 'Outbound:999999999' + index,
                Assigned_User__c = 'CRE',
                Type = 'Call',
                Process_Name__c = 'CTI Inbound/Outbound Calls',
                OwnerId = collectionCre.Id
            );
            tasksForCre.add(outboundTask);
        }
        insert tasksForCre;
        return tasksForCre;
    }

    public static Case createDummyInboundCase() {

        // Create test account
        RecordType personAccountRecordType =
            [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
        Account account = new Account(
            Name = 'TEST BOOKING',
            RecordType = personAccountRecordType
        );
        insert account;

        // Create dummuy case
        RecordType nonSrRecordType =
            [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Non_SR_case'];
        Group collectionQueue =
            [SELECT Id, Name, DeveloperName FROM Group WHERE DeveloperName = 'Collection_Queue'];
        Case collectionsInboundDummyCase = new Case(
            Subject = System.Label.Collections_Inbound_Dummy_Case_Subject,
            RecordTypeId = nonSrRecordType.Id,
            OwnerId = collectionQueue.Id,
            Status = 'Closed',
            AccountId = account.Id
        );
        insert collectionsInboundDummyCase;
        return collectionsInboundDummyCase;
    }
}