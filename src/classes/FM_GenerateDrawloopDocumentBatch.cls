public without sharing class FM_GenerateDrawloopDocumentBatch implements Database.Batchable<sObject>,
            Database.AllowsCallouts, Database.Stateful {

    private String query;
    String recordId;
    String strDDPTemplateId;
    String strTemplateDeliveryId;
    public String SERVER_URL;
    public String SESSION_ID;


    public FM_GenerateDrawloopDocumentBatch(String recordId,
                        String strDDPTemplateId, String strTemplateDeliveryId){
        this.recordId = recordId ;
        this.strDDPTemplateId = strDDPTemplateId;
        this.strTemplateDeliveryId = strTemplateDeliveryId;
    }

    public Database.Querylocator start( Database.BatchableContext bc ) {
        login();
        query = 'SELECT Id '+
                'FROM fm_case__c Where '+
                'Id = :recordId';
        System.debug('===mazi query='+query);
        return Database.getQueryLocator(query);
    }

    public void execute( Database.BatchableContext bc, list<fm_case__c> scope ) {
        system.debug('SESSION_ID*********'+SESSION_ID);

        if(SESSION_ID != null && SESSION_ID != ''){
            system.debug('maza scope*********'+scope);
            system.debug('SESSION ID received*********');
            system.debug('== List Size ==' +scope.size() );
            for( fm_case__c objSobject : scope ){
            try {
                Loop.loopMessage lm = new Loop.loopMessage();
                Map<string, string> variables = new map<String,String>(); // MAIN RECORD ID – SAME OBJECT AS THE DDP RECORD TYPE SPECIFIES’ // DDP ID
                    variables.put('deploy', strTemplateDeliveryId);
                    lm.requests.add(new Loop.loopMessage.loopMessageRequest(
                                    objSobject.Id,
                                    strDDPTemplateId,
                                    variables));
                    String response = lm.sendAllRequests();
                    system.debug('== response ==' +response );
                    lm.batchNotification = Loop.loopMessage.Notification.ON_COMPLETE;
                    system.debug('== lm.batchNotification ==' +lm.batchNotification );
                    //Loop.loopMessage.send('a4025000000VGxX','a0V25000002uHVu','a0T25000004m9wY');
                } catch (exception e){
                    system.debug('== e ==' +e );
                }
            }

        }
    }

    public void finish( Database.BatchableContext bc ) {

    }

    //Method used for login purpose to get the session id
    public void login(){
        Admin_Login_for_Drawloop__c mc = Admin_Login_for_Drawloop__c.getOrgDefaults();
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://' + mc.Domain__c + '.salesforce.com/services/Soap/u/28.0');
        system.debug('mc.Domain__c'+mc.Domain__c);
        system.debug('mc.Username__c'+mc.Username__c);
        system.debug('mc.Password__c'+mc.Password__c);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', '""');
        request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + mc.Username__c + '</username><password>' + mc.Password__c + '</password></login></Body></Envelope>');

        HttpResponse response = h.send(request);
        system.debug('----Response Body-----'+response.getBody());

        Dom.XmlNode resultElmt = response.getBodyDocument().getRootElement()
        .getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/')
        .getChildElement('loginResponse','urn:partner.soap.sforce.com')
        .getChildElement('result','urn:partner.soap.sforce.com');

        SERVER_URL = resultElmt.getChildElement('serverUrl','urn:partner.soap.sforce.com').getText().split('/services')[0];
        SESSION_ID = resultElmt.getChildElement('sessionId','urn:partner.soap.sforce.com').getText();

        system.debug('--SERVER_URL---'+SERVER_URL);
        system.debug('--SESSION_ID---'+SESSION_ID);
        if(Test.isRunningTest()){
            SESSION_ID = 'none';
        }
    }

}