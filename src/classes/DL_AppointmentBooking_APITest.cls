@isTest
public class DL_AppointmentBooking_APITest {
    static String ownerAcc1_ID;
    static String bu1_ID;
    static String slot1_ID;
    static String slot2_ID;
    
    static void fetchValidUserAccountToContext() {
        User owner1 = [SELECT id, name, profileId, profile.name, Contact.accountId 
                       FROM User WHERE profile.name = 'Customer Community Login User(Use this)' 
                       AND IsActive = TRUE AND Contact.accountId != NULL LIMIT 1];
        ownerAcc1_ID = String.valueOf(owner1.Contact.accountId);
    }
    
    @isTest(seeAllData = true)
    static void  test_AppointmentPurposeListing() {
        Test.startTest();
        fetchValidUserAccountToContext();
        
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/appointmentPurposeDetails';
        request.httpMethod = 'GET';
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        AppointmentPurposeDetailsAPI.getAppointmentPurposeDetails(); /* 1 */
        
        request.addParameter('accountId', '');
        AppointmentPurposeDetailsAPI.getAppointmentPurposeDetails(); /* 2 */
        
        request.addParameter('accountId', 'invalid');
        AppointmentPurposeDetailsAPI.getAppointmentPurposeDetails(); /* 2i */
        
        System.debug('---------TEST------------------: AccID_static: ' + ownerAcc1_ID);
        request.addParameter('accountId', ownerAcc1_ID);
        AppointmentPurposeDetailsAPI.getAppointmentPurposeDetails(); /* 3 */
        
        Test.stopTest();
    }
    
    static void setupTestData() {
        ID personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, 
                                     FirstName='Test FirstName', 
                                     LastName='Test LastName', 
                                     Type='Person', party_ID__C='123456');
        objAcc.Email__pc = 'new@no.com';
        insert objAcc;
        ownerAcc1_ID = String.valueOf(objAcc.id);
                
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        // objSR.Account__c = ownerAcc1_ID;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = ownerAcc1_ID;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '123123456';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; /* 'Agreement Rejected By Sales Admin' */
        insert objBookingUnit;
        bu1_ID = objBookingUnit.id;
        
        Location__c loc = new Location__c();
        loc.Location_ID__c = '112233656';
        loc.Name = 'JNU';
        insert loc;
        
        DateTime futureDate = DateTime.now().addDays(14);
        
        Appointment__c objApp1 = new Appointment__c();
        objApp1.Appointment_Date__c = futureDate.date();
        objApp1.Appointment_End_Date__c = futureDate.date().addDays(2);
        objApp1.Building__c = loc.id;
        objApp1.Process_Name__c = 'Collections';
        objApp1.Processes__c = 'Collections';
        objApp1.Sub_Process_Name__c = 'Payments Related';
        objApp1.Sub_Processes__c = 'Payments Related';
        objApp1.Slots__c = '10:00 - 11:00';
        insert objApp1;
        slot1_ID = objApp1.id;
        
        Appointment__c objApp2 = new Appointment__c();
        objApp2.Appointment_Date__c = futureDate.date();
        objApp2.Appointment_End_Date__c = futureDate.date().addDays(2);
        objApp2.Building__c = loc.id;
        objApp2.Process_Name__c = 'Cheque Collection';
        objApp2.Processes__c = 'Cheque Collection';
        objApp2.Sub_Process_Name__c = 'Cheque Collection';
        objApp2.Sub_Processes__c = 'Cheque Collection';
        objApp2.Slots__c = '11:00 - 12:00';
        insert objApp2;
        slot2_ID = objApp2.id;
        
        TriggerOnOffCustomSetting__c objOff = new TriggerOnOffCustomSetting__c ();
        objOff.Name = 'CallingListTrigger';
        objOff.OnOffCheck__c = true;
        insert objOff;
        
        Id handoverCLId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Handover Calling List').getRecordTypeId();
        Calling_list__c objCL = new Calling_list__c();
        objCL.RecordTypeId = handoverCLId;
        objCL.Account__c = ownerAcc1_ID;
        objCL.Booking_Unit__c = objBookingUnit.id;
        insert objCL;
    }
    
    @isTest
    static void  test_AvailableUnitListing() {
        Test.startTest();
        setupTestData();
        
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/appointment/fetchAvailableUnits';
        request.httpMethod = 'GET';
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        DL_GetAvailableUnitsForAppointment_API.fetchAvailableUnitsForAppointment(); /* 1 */
        
        request.addParameter('account_id', 'invalid');
        DL_GetAvailableUnitsForAppointment_API.fetchAvailableUnitsForAppointment(); /* 2 */
        
        request.addParameter('account_id', ownerAcc1_ID);
        DL_GetAvailableUnitsForAppointment_API.fetchAvailableUnitsForAppointment(); /* 3 */
        
        Test.stopTest();
    }
    
    @isTest
    static void  test_AppointmentSlotListing() {
        Test.startTest();
        setupTestData();
        
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/appointment/fetchAvailableSlots';
        request.httpMethod = 'GET';
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        DL_GetAvailableAppointmentSlots_API.fetchAvailableAppointmentSlots(); /* 1 */
        
        request.addParameter('process', 'invalid');
        request.addParameter('sub_process', 'invalid');
        DL_GetAvailableAppointmentSlots_API.fetchAvailableAppointmentSlots(); /* 2 */
        
        request.addParameter('bu_id', 'invalid');
        DL_GetAvailableAppointmentSlots_API.fetchAvailableAppointmentSlots(); /* 3 */
        
        request.addParameter('appointment_date_string', '');
        DL_GetAvailableAppointmentSlots_API.fetchAvailableAppointmentSlots(); /* 4 */
        
        request.addParameter('appointment_date_string', 'YYYY-MM-DD');
        DL_GetAvailableAppointmentSlots_API.fetchAvailableAppointmentSlots(); /* 5 */
        
        request.addParameter('appointment_date_string', DateTime.now().addDays(14).format('yyyy-MM-dd'));
        DL_GetAvailableAppointmentSlots_API.fetchAvailableAppointmentSlots(); /* 6 */
        
        request.addParameter('account_id', 'invalid');
        DL_GetAvailableAppointmentSlots_API.fetchAvailableAppointmentSlots(); /* 7 */
        
        request.addParameter('account_id', ownerAcc1_ID);
        DL_GetAvailableAppointmentSlots_API.fetchAvailableAppointmentSlots(); /* 8 */
        
        request.addParameter('bu_id', bu1_ID);
        DL_GetAvailableAppointmentSlots_API.fetchAvailableAppointmentSlots(); /* 9 */
        
        request.addParameter('process', 'Collections');
        request.addParameter('sub_process', 'Payments Related');
        DL_GetAvailableAppointmentSlots_API.fetchAvailableAppointmentSlots(); /* 10 */
        
        Test.stopTest();
    }
    
    @isTest
    static void  test_AppointmentBooking() {
        Test.startTest();
        setupTestData();
        
        Map<String, Object> requestMap_post = new Map<String, Object>();       
        requestMap_post.put('process_name', '');
        requestMap_post.put('sub_process_name', '');
        requestMap_post.put('bu_id', '');
        requestMap_post.put('appointment_date_string', '');
        requestMap_post.put('account_id', '');
        requestMap_post.put('selected_appointment_slot', '');
        requestMap_post.put('remarks', '');
        
        String reqJSON = JSON.serialize(requestMap_post);
        
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/appointment/submit';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(reqJSON);
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        doAppointmentSubmission(requestMap_post); /* 1 */
        
        requestMap_post.put('process_name', 'invalid');
        requestMap_post.put('sub_process_name', 'invalid');
        doAppointmentSubmission(requestMap_post); /* 2 */
        
        requestMap_post.put('bu_id', 'invalid');
        doAppointmentSubmission(requestMap_post); /* 3 */
        
        requestMap_post.put('appointment_date_string', '');
        doAppointmentSubmission(requestMap_post); /* 4 */
        
        requestMap_post.put('appointment_date_string', 'YYYY-MM-DD');
        doAppointmentSubmission(requestMap_post); /* 5 */
        
        requestMap_post.put('appointment_date_string', DateTime.now().addDays(14).format('yyyy-MM-dd'));
        doAppointmentSubmission(requestMap_post); /* 6 */
        
        requestMap_post.put('account_id', 'invalid');
        doAppointmentSubmission(requestMap_post); /* 7 */
        
        requestMap_post.put('account_id', ownerAcc1_ID);
        doAppointmentSubmission(requestMap_post); /* 8 */
        
        requestMap_post.put('bu_id', bu1_ID);
        doAppointmentSubmission(requestMap_post); /* 9 */
        
        requestMap_post.put('selected_appointment_slot', slot1_ID);
        doAppointmentSubmission(requestMap_post); /* 10 */
        
        requestMap_post.put('process_name', 'Collections');
        requestMap_post.put('sub_process_name', 'Payments Related');
        doAppointmentSubmission(requestMap_post); /* 11 */
        
        Test.stopTest();
    }
    
    @isTest
    static void  test_AppointmentBookingListing() {
        Test.startTest();
        setupTestData();
        
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/appointment/listAll';
        request.httpMethod = 'GET';
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        DL_GetAllAppointmentsBooked_API.fetchAllAppointmentsBooked(); /* 1 */
        
        request.addParameter('account_id', 'invalid');
        DL_GetAllAppointmentsBooked_API.fetchAllAppointmentsBooked(); /* 2 */
        
        request.addParameter('bu_id', 'invalid');
        DL_GetAllAppointmentsBooked_API.fetchAllAppointmentsBooked(); /* 3 */
        
        request.addParameter('account_id', ownerAcc1_ID);
        DL_GetAllAppointmentsBooked_API.fetchAllAppointmentsBooked(); /* 4 */
        
        request.addParameter('bu_id', bu1_ID);
        DL_GetAllAppointmentsBooked_API.fetchAllAppointmentsBooked(); /* 5 */
        
        /* Calling_List__c appointmentBooking_1 = new Calling_List__c(); */
        Map<String, Object> requestMap_post = new Map<String, Object>();
        requestMap_post.put('process_name', 'Collections');
        requestMap_post.put('sub_process_name', 'Payments Related');
        requestMap_post.put('account_id', ownerAcc1_ID);
        requestMap_post.put('bu_id', bu1_ID);
        requestMap_post.put('appointment_date_string', DateTime.now().addDays(14).format('yyyy-MM-dd'));
        requestMap_post.put('selected_appointment_slot', slot1_ID);
        requestMap_post.put('remarks', 'TEST');
        doAppointmentSubmission(requestMap_post);
        
        DL_GetAllAppointmentsBooked_API.fetchAllAppointmentsBooked(); /* 6 */
        
        Test.stopTest();
    }
    
    @isTest
    static void  test_AppointmentCancelling() {
        Test.startTest();
        setupTestData();
        
        Map<String, Object> requestMap_post = new Map<String, Object>();       
        requestMap_post.put('account_id', '');
        requestMap_post.put('bu_id', '');
        requestMap_post.put('appointment_id', '');
        
        String reqJSON = JSON.serialize(requestMap_post);
        
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/appointment/cancell';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(reqJSON);
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        doAppointmentCancellation(requestMap_post); /* 1 */
        
        requestMap_post.put('account_id', 'invalid');
        doAppointmentCancellation(requestMap_post); /* 2 */
        
        requestMap_post.put('bu_id', 'invalid');
        doAppointmentCancellation(requestMap_post); /* 3 */
        
        requestMap_post.put('appointment_id', 'invalid');
        doAppointmentCancellation(requestMap_post); /* 4 */
        
        requestMap_post.put('account_id', ownerAcc1_ID);
        doAppointmentCancellation(requestMap_post); /* 5 */
        
        requestMap_post.put('bu_id', bu1_ID);
        doAppointmentCancellation(requestMap_post); /* 6 */
        
        Id appointSchedRecTypID = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
        Calling_List__c appointmentBooking_1 = new Calling_List__c();
        appointmentBooking_1.RecordTypeID = appointSchedRecTypID;
        appointmentBooking_1.Service_Type__c = 'Collections';
        appointmentBooking_1.Sub_Purpose__c = 'Payments Related';
        appointmentBooking_1.Appointment_Status__c = 'Confirmed';
        appointmentBooking_1.Account__c = ownerAcc1_ID;
        appointmentBooking_1.Booking_Unit__c = bu1_ID;
        appointmentBooking_1.Appointment__c = slot1_ID;
        insert appointmentBooking_1;
        
        requestMap_post.put('appointment_id', appointmentBooking_1.id);
        doAppointmentCancellation(requestMap_post); /* 7 */
        
        Test.stopTest();
    }
    
    private static void doAppointmentSubmission(Map<String, Object> requestMap_post){
        DL_SubmitAppointmentBooking_API.submitAppointmentBooking(
        (String)requestMap_post.get('process_name'), 
        (String)requestMap_post.get('sub_process_name'), 
        (String)requestMap_post.get('bu_id'), 
        (String)requestMap_post.get('appointment_date_string'), 
        (String)requestMap_post.get('account_id'), 
        (String)requestMap_post.get('selected_appointment_slot'), 
        (String)requestMap_post.get('remarks')
        );
    }
    
    private static void doAppointmentCancellation(Map<String, Object> requestMap_post){
        DL_CancelAppointmentBooking_API.cancelAppointmentBooking(
        (String)requestMap_post.get('account_id'), 
        (String)requestMap_post.get('bu_id'), 
        (String)requestMap_post.get('appointment_id')
        );
    }
}