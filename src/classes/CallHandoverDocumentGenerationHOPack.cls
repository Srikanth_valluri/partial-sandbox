public with sharing class CallHandoverDocumentGenerationHOPack {
    
    public static string CallHandoverDocumentGenerationHOPackName(string regId, string projectName, string strBuildingName, 
        string strUnitName, string strCustomerName, string strAddCity, string strPassport, string strBank, string strProjCity, string strBedroomType ) {
        HandoverDocumentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint objEligibleForHandover = new HandoverDocumentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint();
        objEligibleForHandover.timeout_x = 120000 ;
        list<HandoverDocumentGenerationUtilComXsd.AttributeArrForAll> lstReg1 = new list<HandoverDocumentGenerationUtilComXsd.AttributeArrForAll>();
        HandoverDocumentGenerationUtilComXsd.AttributeArrForAll reg1 = new HandoverDocumentGenerationUtilComXsd.AttributeArrForAll();
        reg1.attribute1='';
        reg1.attribute10='';
        reg1.attribute11='';
        reg1.attribute12='';
        reg1.attribute13='';
        reg1.attribute14='';
        reg1.attribute15='';
        reg1.attribute16='';
        reg1.attribute17='';
        reg1.attribute18='';
        reg1.attribute19='';
        reg1.attribute2='';
        reg1.attribute20='';
        reg1.attribute3='';
        reg1.attribute4='';
        reg1.attribute5='';
        reg1.attribute6='';
        reg1.attribute7='';
        reg1.attribute8='';
        reg1.attribute9='';
        lstReg1.add(reg1);
        
        list<HandoverDocumentGenerationUtilComXsd.AttributeArr1> lstReg2 = new list<HandoverDocumentGenerationUtilComXsd.AttributeArr1>();
        HandoverDocumentGenerationUtilComXsd.AttributeArr1 reg2 = new HandoverDocumentGenerationUtilComXsd.AttributeArr1();
        
        reg2.attribute1='';
        reg2.attribute2 = lstReg1;
        lstReg2.add(reg2);
        
        
        HandoverDocumentGenerationDtoComXsd.DocGenDTO reg= new HandoverDocumentGenerationDtoComXsd.DocGenDTO();
        reg.regId = regId;
        reg.ATTRIBUTE1= projectName;
        reg.ATTRIBUTE10='';
        reg.ATTRIBUTE100='';
        reg.ATTRIBUTE101='';
        reg.ATTRIBUTE102='';
        reg.ATTRIBUTE103='';
        reg.ATTRIBUTE104='';
        reg.ATTRIBUTE105='';
        reg.ATTRIBUTE106='';
        reg.ATTRIBUTE107='';
        reg.ATTRIBUTE108='';
        reg.ATTRIBUTE109='';
        reg.ATTRIBUTE11='';
        reg.ATTRIBUTE110='';
        reg.ATTRIBUTE111='';
        reg.ATTRIBUTE112='';
        reg.ATTRIBUTE113='';
        reg.ATTRIBUTE114='';
        reg.ATTRIBUTE12='';
        reg.ATTRIBUTE13='';
        reg.ATTRIBUTE14='';
        reg.ATTRIBUTE15='';
        reg.ATTRIBUTE16='';
        reg.ATTRIBUTE17='';
        reg.ATTRIBUTE18='';
        reg.ATTRIBUTE19='';
        reg.ATTRIBUTE2= strBuildingName;
        reg.ATTRIBUTE20='';
        reg.ATTRIBUTE21='';
        reg.ATTRIBUTE22='';
        reg.ATTRIBUTE23='';
        reg.ATTRIBUTE24='';
        reg.ATTRIBUTE25='';
        reg.ATTRIBUTE26='';
        reg.ATTRIBUTE27='';
        reg.ATTRIBUTE28='';
        reg.ATTRIBUTE29='';
        reg.ATTRIBUTE3= strUnitName;
        reg.ATTRIBUTE30='';
        reg.ATTRIBUTE31='';
        reg.ATTRIBUTE32='';
        reg.ATTRIBUTE33='';
        reg.ATTRIBUTE34='';
        reg.ATTRIBUTE35='';
        reg.ATTRIBUTE36='';
        reg.ATTRIBUTE37='';
        reg.ATTRIBUTE38='';
        reg.ATTRIBUTE39='';
        reg.ATTRIBUTE4= strCustomerName;
        reg.ATTRIBUTE40='';
        reg.ATTRIBUTE41='';
        reg.ATTRIBUTE42='';
        reg.ATTRIBUTE43='';
        reg.ATTRIBUTE44='';
        reg.ATTRIBUTE45='';
        reg.ATTRIBUTE46='';
        reg.ATTRIBUTE47='';
        reg.ATTRIBUTE48='';
        reg.ATTRIBUTE49='';
        reg.ATTRIBUTE5= strAddCity;
        reg.ATTRIBUTE50='';
        reg.ATTRIBUTE51='';
        reg.ATTRIBUTE52='';
        reg.ATTRIBUTE53='';
        reg.ATTRIBUTE54='';
        reg.ATTRIBUTE55='';
        reg.ATTRIBUTE56='';
        reg.ATTRIBUTE57='';
        reg.ATTRIBUTE58='';
        reg.ATTRIBUTE59='';
        reg.ATTRIBUTE6= strPassport;
        reg.ATTRIBUTE60='';
        reg.ATTRIBUTE61='';
        reg.ATTRIBUTE62='';
        reg.ATTRIBUTE63='';
        reg.ATTRIBUTE64='';
        reg.ATTRIBUTE65='';
        reg.ATTRIBUTE66='';
        reg.ATTRIBUTE67='';
        reg.ATTRIBUTE68='';
        reg.ATTRIBUTE69='';
        reg.ATTRIBUTE7= strBank;
        reg.ATTRIBUTE70='';
        reg.ATTRIBUTE71='';
        reg.ATTRIBUTE72='';
        reg.ATTRIBUTE73='';
        reg.ATTRIBUTE74='';
        reg.ATTRIBUTE75='';
        reg.ATTRIBUTE76='';
        reg.ATTRIBUTE77='';
        reg.ATTRIBUTE78='';
        reg.ATTRIBUTE79='';
        reg.ATTRIBUTE8= strProjCity ;
        reg.ATTRIBUTE80='';
        reg.ATTRIBUTE81='';
        reg.ATTRIBUTE82='';
        reg.ATTRIBUTE83='';
        reg.ATTRIBUTE84='';
        reg.ATTRIBUTE85='';
        reg.ATTRIBUTE86='';
        reg.ATTRIBUTE87='';
        reg.ATTRIBUTE88='';
        reg.ATTRIBUTE89='';
        reg.ATTRIBUTE9= strBedroomType ;
        reg.ATTRIBUTE90='';
        reg.ATTRIBUTE91='';
        reg.ATTRIBUTE92='';
        reg.ATTRIBUTE93='';
        reg.ATTRIBUTE94='';
        reg.ATTRIBUTE95='';
        reg.ATTRIBUTE96='';
        reg.ATTRIBUTE97='';
        reg.ATTRIBUTE98='';
        reg.ATTRIBUTE99='';              
        reg.ATTRIBUTEARR1 = lstReg2;
        string strHand;
         try {
            strHand = objEligibleForHandover.DocGeneration('HO_Pack_Jordan',reg);
            system.debug('strHand '+strHand );
        } catch (Exception e){
            strHand = 'Exception Occured';
        }
                
        return strHand;     
    }
    
    public class HandoverDocumentGenerationResponse {
        public String P_Template_Name {get;set;}
        public HandoverDocumentGenerationDtoComXsd.DocGenDTO regist;
    }
}