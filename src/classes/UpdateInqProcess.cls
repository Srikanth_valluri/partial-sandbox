@RestResource(urlMapping='/UpdateInquiry/*')
global without sharing class UpdateInqProcess{   
    @httpPOST
    global static void doPOST () {
        RestRequest  req     = RestContext.request;   
        String jsonInput  = req.requestBody.toString();
        System.debug('::::::::::Response Body::::::::::::'+jsonInput );
        if (jsonInput != NULL && jsonInput != '') {
            Set <ID> inqIds = new Set <ID> ();
            Map <String, Object> response = (Map <String, Object>) JSON.deserializeuntyped(jsonInput );
            String srID = String.valueOf (response.get ('srId'));
            for(Buyer__c b:[select id,Inquiry__c, Account__c from Buyer__c where Inquiry__c!=null AND Booking__r.Deal_SR__c=:srID ]){
                //inqIds.add(b.Inquiry__c);
                if(b.Account__c == null)
                    inqIds.add(b.id);
            } 
            System.Debug (inqids);
            //InquiryConversionProcess.convertInquiry(inqIds);
            if(!inqIds.isEmpty())
                buyerConversionProcess.convertBuyer(inqIds);
        }
  }
}