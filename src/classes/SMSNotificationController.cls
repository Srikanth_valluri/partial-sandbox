/**************************************************************************************************
* Class: AgencyPCManagement_Controller
* VF Page : SMSNotofication
* Created by : Tejashree Chavan
---------------------------------------------------------------------------------------------------
* Purpose/Methods: Controller of SMS Notification console VF page
---------------------------------------------------------------------------------------------------
* Test Data: SMSNotificationControllerTest
----------------------------------------------------------------------------------
* Version History: (All changes and TA reworks should be entered as new row )
* VERSION     DEVELOPER NAME       DATE          DETAIL FEATURES
    1.1       Tejashree Chavan     23/12/2017    Initial Draft
***************************************************************************************************/
public with sharing class SMSNotificationController {
    public Account accountObj {get; set;}
    public String selCampaign {get; set;}
    public String startDate {get; set;}
    public String endDate {get; set;}
    public String scheduleDate {get; set;}
    public String mobileCountryVal {get; set;}    
    public List<SelectOption> mobileCountryList;
    public List<SelectOption> CountryList;
    public List<String> inquiryStatusVal {get; set;}
    public List<SelectOption> inquiryStatusList;
    public List<SelectOption> accountRecordType;
    public List<String> selAccountRecordType {get; set;}
    public List<String> selCountryToExclude {get; set;}
    public String selTier {get; set;}
    public String selMobileNo {get; set;}
    public List<String> contactRole {get; set;}
    public Integer inquirySearchCount {get; set;}
    public Integer accountSearchCount {get; set;}
    private Date inquieryStartDate;
    private Date inquieryEndDate;
    public List<String> ownerVal {get; set;}
    public List<SelectOption> ownerList;
    public String  currentTab {get; set;}
    public String message {get; set;}
    public String msgType {get; set;}
    public String isDisplay {get; set;}
    public List<agentWrappper> agents {get; set;}
    public Campaign_SMS__c newSMSRequest {get; set;}
    public List<Campaign_SMS__c> smsHistory {get; set;}
    public string CampsmsId {get;set;}
    public String selTime {get; set;}
    public Map<String,String> sobjetcQueryMap = new Map<String,String>();

    public SMSNotificationController() {
        inquirySearchCount = 0;
        accountSearchCount = 0;
        currentTab = 'tab-default-1';
        isDisplay = 'hide';
        selAccountRecordType = new List<String>();
        selCountryToExclude = new List<String>();
        ownerVal = new List<String>();
        contactRole = new List<String>();
        mobileCountryList = new List<SelectOption>();        
        setMobileCountryList(new List<SelectOption>());
        CountryList = new List<SelectOption>();
        setCountryList(new List<SelectOption>());
        ownerList = new List<SelectOption>();
        setOwnerList(new List<SelectOption>());
        inquiryStatusList = new List<SelectOption>();
        setInquiryStatusList(new List<SelectOption>());
        accountRecordType = new List<SelectOption>();
        setAccountRecordType(new List<SelectOption>());
        accountObj = new Account();
        agents = new List<agentWrappper>();
        newSMSRequest = new Campaign_SMS__c();
        selMobileNo = 'Contacts';
        fetchSMSHistory();
    }

    public void searchInquiries() {
        String inquiryFilterCriteria = getInquiryFilterCriteria();
        String inquiryQuery = 'SELECT Id,Name FROM Inquiry__c'+
                              ' WHERE Validity__c = \'Valid\' AND Reachable__c = \'Reachable\'';
        if(String.isNotBlank(inquiryFilterCriteria)) {
            inquiryQuery += inquiryFilterCriteria;
        }
        inquirySearchCount = Database.query(inquiryQuery).size();
        if(Database.query(inquiryQuery).size() == 0) {
            messageBlock('show', 'error', 'No Search result found.');
        }
    }

    public void searchAgentAccount() {
        accountSearchCount= 0;
        String accountFilterCriteria = getAccountFilter();
        String contactFilterCriteria  = getContactFilter();
        String accountQuery = '';
        String contactQuery = '';
        isDisplay = 'hide';

        /*if(String.isNotBlank(accountFilterCriteria)) {
            if(String.isNotBlank(contactFilterCriteria)) {
                accountQuery += 'SELECT Id,Name,Agency_Mobile_combined__c,' +
                                '(SELECT Id,Name,Contact_Mobile__c FROM Contacts' +
                                ' WHERE Account.Agency_Type__c =\'' +
                                Label.Agency_Type_Corporate + '\' ' + contactFilterCriteria +
                                ' ) FROM Account WHERE RecordTypeId != null' +
                                accountFilterCriteria ;
            }
            else {
                accountQuery += 'SELECT id,Name,Agency_Mobile_combined__c FROM Account'+
                                ' WHERE RecordTypeId != null' + accountFilterCriteria;
            }

        }*/

        if(String.isNotBlank(contactFilterCriteria) && String.isBlank(accountFilterCriteria)) {
            contactQuery += 'SELECT Id,Name,Contact_Mobile__c FROM Contact ' +
                            'WHERE MobilePhone != null ' +
                            contactFilterCriteria;
        }
        else if(String.isNotBlank(accountFilterCriteria)) {
            if(String.isNotBlank(selMobileNo) &&
               selMobileNo.equalsIgnoreCase('NONE')) {
                accountQuery += 'SELECT Id,Name,Agency_Mobile_combined__c' +
                                ' FROM Account WHERE RecordTypeId != null AND Agency_Mobile__c != null ' +
                                accountFilterCriteria ;
                contactQuery += 'SELECT Id,Name,Contact_Mobile__c FROM Contact ' +
                                'WHERE MobilePhone != null ' +
                                contactFilterCriteria;
            }
            else {
            accountQuery += 'SELECT id,Name,Agency_Mobile_combined__c FROM Account'+
                            ' WHERE RecordTypeId != null AND Agency_Mobile__c != null ' +
                            accountFilterCriteria;
            }
        }
        if(String.isNotBlank(accountQuery)) {
            List<Account> accounts = Database.query(accountQuery);
            if(!accounts.isEmpty() && accounts != null) {
                //sobjetcQuery = accountQuery;
                sobjetcQueryMap.put('Account',accountQuery);
                accountSearchCount = accounts.size();
                /*for(Account acc : accounts) {
                    if(acc.Contacts != null && acc.Contacts.size() > 0) {
                        system.debug('-----acc.Contacts.size()-----'+acc.Contacts.size());
                        accountSearchCount = accountSearchCount + acc.Contacts.size();
                    }
                    /*agentWrappper wrp = new agentWrappper();
                    wrp.recordId = acc.id;
                    wrp.recordName = acc.Name;
                    agents.add(wrp);
                }*/
            }
        }
        if(String.isNotBlank(contactQuery)) {
            List<Contact> contacts = Database.query(contactQuery);
            if(contacts != null && !contacts.isEmpty()) {
                //sobjetcQuery = contactQuery;
                sobjetcQueryMap.put('Contact',contactQuery);
                accountSearchCount = accountSearchCount + contacts.size();
            }
        }
        fetchSMSHistory();
    }

    public void resetInquiries() {
        startDate = '';
        endDate = '';
        mobileCountryVal =  '';
        inquiryStatusVal.clear();
        selCampaign = '';
        isDisplay = 'hide';
    }

    public void resetAccount() {
        selAccountRecordType.clear();
        selCountryToExclude.clear();
        selTier = '';
        contactRole.clear();
        selMobileNo = 'Contacts';
        ownerVal.clear();
        accountSearchCount = 0;
        accountObj = new account();
        isDisplay = 'hide';
    }

    public void setMobileCountryList(List<SelectOption> mobileList)
    {
        mobileCountryList.add(new SelectOption('NONE','--- Select Mobile Country ---'));
        Schema.DescribeFieldResult fieldResult = Inquiry__c.Mobile_CountryCode__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            mobileList.add(new SelectOption(f.getLabel(), f.getValue()));
            mobileList.sort();
        }
        mobileCountryList.addall(mobileList);
    }
    
    public List<SelectOption> getMobileCountryList(){
      return mobileCountryList;
    }
    
    public void setCountryList(List<SelectOption> mobileList)
    {
        CountryList.add(new SelectOption('NONE','--- Select Country to Exclude ---'));
        Schema.DescribeFieldResult fieldResult = Account.Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            mobileList.add(new SelectOption(f.getLabel(), f.getValue()));
            mobileList.sort();
        }
        CountryList.addall(mobileList);
    }
    
    public List<SelectOption> getCountryList(){
      return CountryList;
    }

    

    public void  setInquiryStatusList(List<SelectOption> inquiryList)
    {
        inquiryStatusList.add(new SelectOption('NONE','--- Select Inquiry Status ---'));
        Schema.DescribeFieldResult fieldResult = Inquiry__c.Inquiry_Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            inquiryList.add(new SelectOption(f.getLabel(), f.getValue()));
            inquiryList.sort();
        }
        inquiryStatusList.addall(inquiryList);
    }

    public List<SelectOption> getInquiryStatusList(){
      return inquiryStatusList;
    }

    public void setAccountRecordType(List<SelectOption> recordTypes) {
        Set<String> exceptionRecordTypes = new Set<String>{'Person Account','Business Account','Master','Corporate Agency Blacklisted','Individual Agency','Individual Agency - Blacklisted','Individual Agency - Terminated','Corporate Agency Terminated','POA'};
        accountRecordType.add(new SelectOption('NONE','--- Select RecordType ---'));
        Map<String, Schema.RecordTypeInfo> accountRecordTypeInfo =
            Schema.SObjectType.Account.getRecordTypeInfosByName();
        for(Schema.RecordTypeInfo rec : accountRecordTypeInfo.values()) {
            if(!exceptionRecordTypes.contains(rec.getName())) {
                recordTypes.add(new SelectOption(rec.getRecordTypeId(),rec.getName()));
            }
        }
        accountRecordType.addAll(recordTypes);
    }

    public List<SelectOption> getAccountRecordType(){
      return accountRecordType;
    }

    public String getInquiryFilterCriteria() {
        String inquiryCriteria = '';
        system.debug('-----selCampaign----'+selCampaign);
        if(String.isNotBlank(selCampaign)) {
            inquiryCriteria += ' AND Campaign__r.Name = \''+selCampaign+'\'';
        }
        if(String.isNotBlank(mobileCountryVal) && !mobileCountryVal.equals('NONE')) {
            inquiryCriteria += ' AND Mobile_CountryCode__c = \''+mobileCountryVal+'\'';
        }
        if(!inquiryStatusVal.isEmpty()) {
            inquiryCriteria += ' AND Inquiry_Status__c IN ( ';
            for(String str : inquiryStatusVal) {
                inquiryCriteria += '\''+str+'\',';
            }
            inquiryCriteria = inquiryCriteria.removeEnd(',') +' )   ';
        }
        if(String.isNotBlank(startDate)) {
            inquieryStartDate = Date.ValueOf(startDate.replace('/', '-'));
            inquiryCriteria += ' AND Inquiry_Created_Date__c >= :inquieryStartDate';
        }
        if(String.isNotBlank(endDate)) {
            inquieryEndDate = Date.ValueOf(endDate.replace('/', '-'));
            inquiryCriteria += ' AND Inquiry_Created_Date__c <= :inquieryEndDate';
        }
        return inquiryCriteria;
    }

    public String getAccountFilter() {
        String accountCriteria = '';
        if(contactRole.isEmpty() &&
           (String.isNotBlank(selMobileNo) &&
            !selMobileNo.equals('Contacts'))) {
            if(!selAccountRecordType.isEmpty()) {
                accountCriteria += ' AND RecordTypeId IN ( ';
                for(String str : selAccountRecordType) {
                    accountCriteria += '\''+str+'\',';
                }
                 accountCriteria = accountCriteria.removeEnd(',') +' )';
            }
            else {
                accountCriteria += ' AND RecordTypeId IN ( ';
                for(SelectOption opt : accountRecordType) {
                    if(!opt.getValue().equalsIgnoreCase('NONE')) {
                        accountCriteria += '\''+opt.getValue()+'\',';
                    }
                }
                accountCriteria = accountCriteria.removeEnd(',') +' )';
            }

            if(ownerVal.size() > 0) {
                accountCriteria += ' AND Owner.Name IN ( ';
                for(String str : ownerVal) {
                    accountCriteria += '\''+str+'\',';
                }
                accountCriteria = accountCriteria.removeEnd(',') +' )';
            }
            
            if(selCountryToExclude.size() > 0) {
                accountCriteria += ' AND Country__c NOT IN ( ';
                for(String str : selCountryToExclude) {
                    accountCriteria += '\''+str+'\',';
                }
                accountCriteria = accountCriteria.removeEnd(',') +' )';
            }
            /*
            if(String.isNotBlank(accountObj.Country__c)) {
                accountCriteria += ' AND Country__c != \''+ accountObj.Country__c + '\'';
            }
            */

            if(String.isNotBlank(accountObj.City__c)) {
                accountCriteria += ' AND City__c = \''+ accountObj.City__c + '\'';
            }

            if(String.isNotBlank(selMobileNo) && !selMobileNo.equals('NONE')) {
                if(selMobileNo.equalsIgnoreCase('Agency Mobile')) {
                    accountCriteria += ' AND Agency_Mobile_combined__c != null';
                }
            }
            else {
                accountCriteria += ' AND Agency_Mobile_combined__c != null';
            }

            if(String.isNotBlank(selTier) && !selTier.equals('NONE')) {
                if(selTier.equalsIgnoreCase('Yes')) {
                    accountCriteria += ' AND Eligible_For_Tier_Program__c = true';
                }
                else if(selTier.equalsIgnoreCase('No')){
                    accountCriteria += ' AND Eligible_For_Tier_Program__c = false';
                }
            }
        }
        return accountCriteria;
    }

    public String getContactFilter() {
        String contactCriteria ='';
        if(!contactRole.isEmpty() ||
           (selMobileNo.equalsIgnoreCase('NONE') ||
            selMobileNo.equalsIgnoreCase('Contacts'))) {
            if(String.isNotBlank(selMobileNo) && !selMobileNo.equals('NONE')) {
                if(selMobileNo.equalsIgnoreCase('Contacts')) {
                    contactCriteria += ' AND Contact_Mobile__c != null';
                }
            }
            else {
                contactCriteria += ' AND Contact_Mobile__c != null';
            }

            if(!contactRole.isEmpty()) {
                contactCriteria += ' AND (';
                for(String str : contactRole) {
                    if(str.equals('Owner')) {
                        contactCriteria += ' Owner__c = true OR ';
                    }
                    if(str.equals('Authorised Signatory')) {
                        contactCriteria += ' Authorised_Signatory__c = true OR ';
                    }
                    if(str.equals('Portal Administrator')) {
                        contactCriteria += ' Portal_Administrator__c = true OR';
                    }
                    if(str.equals('Agent Representative')) {
                        contactCriteria += ' Agent_Representative__c = true OR';
                    }
                }
                system.debug('-----contactCriteria1111---'+contactCriteria);
                contactCriteria = contactCriteria.trim().removeEndIgnoreCase('OR') + ')';
            }

            if(!selAccountRecordType.isEmpty()) {
                contactCriteria += ' AND Account.RecordTypeId IN ( ';
                for(String str : selAccountRecordType) {
                    contactCriteria += '\''+str+'\',';
                }
                 contactCriteria = contactCriteria.removeEnd(',') +' )';
            }
            else {
                contactCriteria += ' AND Account.RecordTypeId IN ( ';
                for(SelectOption opt : accountRecordType) {
                    if(!opt.getValue().equalsIgnoreCase('NONE')) {
                        contactCriteria += '\''+opt.getValue()+'\',';
                    }
                }
                contactCriteria = contactCriteria.removeEnd(',') +' )';
            }

            if(ownerVal.size() > 0) {
                contactCriteria += ' AND Account.Account_Owner__c IN ( ';
                for(String str : ownerVal) {
                    contactCriteria += '\''+str+'\',';
                }
                contactCriteria = contactCriteria.removeEnd(',') +' )';
            }
            
            if(selCountryToExclude.size() > 0) {
                contactCriteria += ' AND Account.Country__c NOT IN ( ';
                for(String str : selCountryToExclude) {
                    contactCriteria += '\''+str+'\',';
                }
                contactCriteria = contactCriteria.removeEnd(',') +' )';
            }
            /*
            if(String.isNotBlank(accountObj.Country__c)) {
                contactCriteria += ' AND Account.Country__c != \''+ accountObj.Country__c + '\'';
            }
            */

            if(String.isNotBlank(accountObj.City__c)) {
                contactCriteria += ' AND Account.City__c = \''+ accountObj.City__c + '\'';
            }

            if(String.isNotBlank(selTier) && !selTier.equals('NONE')) {
                if(selTier.equalsIgnoreCase('Yes')) {
                    contactCriteria += ' AND Account.Eligible_For_Tier_Program__c = true';
                }
                else if(selTier.equalsIgnoreCase('No')){
                    contactCriteria += ' AND Account.Eligible_For_Tier_Program__c = false';
                }
            }
        }
        return contactCriteria;
    }

    public void messageBlock(String pDisplay, String pType, String pMessage) {
        isDisplay = pDisplay;
        msgType = pType;
        message = pMessage;
    }

    public void  setOwnerList(List<SelectOption> ownerActList)
    {
        ownerList.add(new SelectOption('NONE','--- User ---'));
        List<AggregateResult> accountOwners = [ SELECT
                                                    Owner.Name,
                                                    COUNT_DISTINCT(OwnerId)
                                                FROM Account
                                                WHERE 
                                                    recordtype.name != null
                                                    AND
                                                    recordtype.name != 'Person Account'
                                                    AND
                                                    recordtype.name != 'Business Account'
                                               GROUP BY Owner.Name];
        for( AggregateResult activeUser : accountOwners)
        {
            ownerActList.add(new SelectOption((String)activeUser.get('Name'),
                                              (String)activeUser.get('Name')));
            ownerActList.sort();
        }
        system.debug('>>>>>'+ownerActList);
        ownerList.addall(ownerActList);
         system.debug(ownerList);
    }

    public List<SelectOption> getOwnerList(){
      return ownerList;
    }

     public void submitSMSForApproval(){
        //getDatetimeFormat(scheduleDate+' '+selTime);
        DateTime dateVal = NULL;
        if (scheduleDate != '' && scheduleDate != NULL) {
            Integer mm = Integer.valueOf(scheduleDate.split ('/')[0]);
            Integer dd = Integer.valueOf(scheduleDate.split ('/')[1]);
            Integer yy = Integer.valueOf(scheduleDate.split ('/')[2]);
            
            Integer hrs = 00, mins = 00;
            String amorPM = '';
            if (selTime != NULL && selTime != '') {
                hrs = Integer.valueOf(selTime.split (':')[0]);
                String minsVal = selTime.split (':')[1];
               
                amOrPM = selTime.right(2);
                minsVal = minsVal.removeEnd (amorPM).trim();
                
                mins= Integer.valueOf (minsVal);
                
            }
            System.Debug (yy+'=='+mm+'=='+dd);
            dateVal = Datetime.newInstanceGmt(yy,mm,dd,hrs,mins, 00);
            if (amOrPM == 'PM') {
                dateVal = dateVal.addHours(12);
            }
        }
        
        
        System.Debug(dateVal);
        Savepoint sp = Database.setSavepoint();
        try{
            if(String.isNotBlank(newSMSRequest.sms_Message__c) &&
               (accountSearchCount > 0 || inquirySearchCount > 0) &&
               String.isNotBlank(scheduleDate) &&
               String.isNotBlank(selTime)) {
                newSMSRequest.Total_Inquiries_in_the_Date_Range__c = accountSearchCount;
                newSMSRequest.User__c = UserInfo.getUserId();
                newSMSRequest.Scheduled_Date_Time__c = dateVal;
                newSMSRequest.Sobject_Query__c = JSON.serialize(sobjetcQueryMap);
                insert newSMSRequest;
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setObjectId(newSMSRequest.id);
                Approval.ProcessResult result = Approval.process(req);
                newSMSRequest = new campaign_SMS__c();
                fetchSMSHistory();
                messageBlock('show',
                             'success',
                             Label.SMS_Schedule_Success_Msg);
            }
            else{
                if(String.isBlank(newSMSRequest.sms_Message__c)) {
                    messageBlock('show',
                                 'error',
                                 Label.SMS_Schedule_Msg_Error);
                }
                else if(String.isBlank(scheduleDate) || String.isBlank(selTime)) {
                    messageBlock('show',
                                 'error',
                                 Label.SMS_Schedule_Date_Error);
                }
            }
        }catch(exception e){
            ApexPages.addMessages(e);
            Database.rollback(sp);
        }
    }

    // Get the SMS Hitory
    public void fetchSMSHistory(){
        smsHistory = new list<Campaign_SMS__c>();
        smsHistory = [SELECT id,
                        SMS_Message__c,
                        createddate,
                        Scheduled_Date_Time__c,
                        SMS_Response__c,
                        SMS_Queued__c,
                        Total_Mobile_Numbers__c,
                        SMS_Process_Complete__c ,
                        Approval_Status__c,
                        Total_Inquiries_in_the_Date_Range__c,
                        Error_From_Provider__c,
                        User__c
                     FROM Campaign_SMS__c
                     WHERE User__c = :userInfo.getuserid()
                     ORDER BY CREATEDDATE DESC LIMIT 100];
    }

    public void callSMSJob(){
        List<Campaign_sms__c> campSMS = [SELECT
                                            Id,
                                            SMS_Queued__c,
                                            Inquiry_Start_Date__c,
                                            Inquiry_End_Date__c,
                                            Total_Inquiries_in_the_Date_Range__c,
                                            User__c,
                                            SMS_Message__c,
                                            Sobject_Query__c
                                         FROM Campaign_sms__c
                                         WHERE id=:CampsmsId Limit 1000];
        if(campSMS != null && !campSMS.isEmpty() && campSMS[0].User__c != null) {
            if(campSMS[0].SMS_Message__c != null &&
               campSMS[0].Total_Inquiries_in_the_Date_Range__c > 0 && campSMS[0].Sobject_Query__c != null) {
                Set<String> mobileNumbers = getMobileNumbers(campSMS[0].Sobject_Query__c);
                if(mobileNumbers != null && !mobileNumbers.isEmpty()) {
                    system.enqueueJob(new Async_Rest_SMS(CampsmsId,
                                                         mobileNumbers,
                                                         campSMS[0].SMS_Message__c,
                                                         'Account'));
                    messageBlock('show','success',Label.SMSQueuedMsg);
                    campSMS[0].SMS_Queued__c =true;
                    update campSMS;
                    fetchSMSHistory();
                }
            }
            else {
                messageBlock('show','error',Label.SMSBlankErrorMsg);
            }
        }
    }

    public Set<String> getMobileNumbers(String queryStr){
        Set<String> mobileNumbers = new Set<String>();
        if(String.isNotBlank(queryStr)) {
            Map<String,Object> sobjectsQueryMap = (Map<String,Object>)JSON.deserializeUntyped(queryStr);
            if(sobjectsQueryMap != null && !sobjectsQueryMap.isEmpty() &&
               sobjectsQueryMap.containsKey('Account')) {
                List<Sobject> results= Database.query(String.valueOf(sobjectsQueryMap.get('Account')));
                if(results instanceof List<Account>) {
                    for(Account acc : (List<Account>) results) {
                        mobileNumbers.add(acc.Agency_Mobile_combined__c);
                    }
                }
            }
            if(sobjectsQueryMap != null && !sobjectsQueryMap.isEmpty() &&
               sobjectsQueryMap.containsKey('Contact')) {
                List<Sobject> results= Database.query(String.valueOf(sobjectsQueryMap.get('Contact')));
                if(results instanceof List<Contact>) {
                    for(Contact con : (List<Contact>) results) {
                        mobileNumbers.add(con.Contact_Mobile__c);
                    }
                }
            }
        }
        return mobileNumbers;
    }

    public Class agentWrappper {
        public String recordId {get; set;}
        public String recordName {get; set;}

        public agentWrappper() {}
    }
}