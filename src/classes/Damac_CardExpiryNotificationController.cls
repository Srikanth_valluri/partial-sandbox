/*
Created Date: 17/06/2020
Batch Class: Damac_CardExpiryNotificationBatch_Acc, Damac_CardExpiryNotificationBatch_Con
Test Class: Damac_CardExpiryNotificationBatchTest
VisualForce Component: Damac_CardExpiryNotification
Description: To get the record information for Account and its related Contacts and use in VF component
*/
global class Damac_CardExpiryNotificationController {
    
    public ID accountId { get; set; }
    
    public Damac_CardExpiryNotificationController () {
    }
    
    public class accountFields {
        public String fieldName { get; set; }
        public Date expiryDate { get; set; }
    }
    
    public class contactFields {
        public String contactName { get; set; }
        public String fieldName { get; set; }
        public Date expiryDate { get; set; }
    }
    
    public List<accountFields> getAccountDetails() {
        try {
            Account acc = [SELECT Expired_Cards__c, Agency_Type__c, Name, RERA_Expiry_Date__c, Passport_Expiry_Date__c, 
                           Broker_Card_Expiry_Date__pc, Owner.Name, Trade_License_Expiry_Date__c 
                           FROM Account WHERE ID =: accountId
                           AND Expired_Cards__c != null];
            
            List<accountFields> expiredDocuments = new List<accountFields> ();
            system.debug (acc.Expired_cards__c);
            List <String> accountExpiryFields = new List <String> ();
            if (acc.Expired_Cards__c != null) {
                if (acc.Expired_Cards__c.contains (',')) {
                    accountExpiryFields = acc.Expired_Cards__c.split (',');
                } else {
                    accountExpiryFields.add (acc.Expired_Cards__c);
                }
                 
                for(String fieldAPI : accountExpiryFields) {   
                    System.debug (fieldAPI);
                    System.debug (acc);         
                    if(acc.get (fieldAPI) != null) {
                        accountFields obj = new accountFields ();
                        obj.fieldName = Schema.getGlobalDescribe().get('Account')
                            .getDescribe().fields.getMap()
                            .get(fieldAPI).getDescribe().getlabel();
                        obj.expiryDate = Date.valueOf (acc.get (fieldAPI));
                        expiredDocuments.add (obj);
                    
                    }
                }
            }
            return expiredDocuments;
        } catch (Exception e) {
            return new List<accountFields>();
        }    
    }
    
    public List<contactFields> getContactDetails() {
        
        List<String> customLabelList = new List<String> (); 
        String cardExpiry = System.Label.CardExpiryNotification;
        
        if (cardExpiry.contains (',')) {
            customLabelList = cardExpiry.split (',');
        } else {
            customLabelList.add (cardExpiry);
        }
          
        String contactStatus = 'Cancelled';
        
        List<Contact> contactsList = new List<Contact>();
        
        contactsList = [SELECT Name, Expired_Cards__c, ID_Expiry_Date__c, Broker_Card_Expiry_Date__c, Passport_Expiry_Date__c 
                        FROM Contact 
                        WHERE User_Type__c IN: customLabelList 
                        AND AccountId =: accountId 
                        AND Status__c !=: contactStatus
                        AND Expired_Cards__c != null];
                        
        if (Test.isRunningTest()) {
            contactsList = [SELECT Name, Expired_Cards__c, ID_Expiry_Date__c, Broker_Card_Expiry_Date__c, Passport_Expiry_Date__c 
                        FROM Contact 
                        WHERE AccountId =: accountId ];
        
        }
        
        List<contactFields> expiredDocuments = new List<contactFields>();
        for(Contact con :contactsList) {
        system.debug (con);
            List <String> contactExpiryFields = new List <String> ();
            if (Test.isRunningTest ()) {
                contactExpiryFields.add ('ID_Expiry_Date__c');
            }
            if (con.Expired_Cards__c != null || Test.isRunningTest()) {
                if (!Test.isRunningTest()) {
                    if (con.Expired_Cards__c.contains (',')) {
                        contactExpiryFields = con.Expired_Cards__c.split (',');
                    } else {
                        contactExpiryFields.add (con.Expired_Cards__c);
                    }
                }
                for(String fieldAPI : contactExpiryFields) {            
                    if(con.get(fieldAPI) != null) {
                        contactFields obj = new contactFields();
                        obj.ContactName = con.Name;
                        obj.fieldName = Schema.getGlobalDescribe().get('Contact')
                            .getDescribe().fields.getMap()
                            .get(fieldAPI).getDescribe().getlabel();
                        obj.expiryDate = Date.valueOf(con.get(fieldAPI));
                        expiredDocuments.add (obj);
                    
                    }
                }
            }
        }
        return expiredDocuments;
    }
}