@isTest
Public Class PlotHoKrfNotificationQueueableTest {
    
    @isTest
    public static void test1() {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
        }
        insert bookingUnitList;
        
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Case   Cas = TestDataFactory_CRM.createCase(objAccount.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = bookingUnitList[0].Id ;
        Cas.Type = 'NOCVisa';
        Cas.IsRunningTest__c = true;
        Cas.PCC_Issued__c = true;
        insert Cas;

        SR_Attachments__c objSR_Attachments = new SR_Attachments__c();
        objSR_Attachments.Name = 'Key Release Authorization Form';
        objSR_Attachments.Case__c = Cas.Id;
        objSR_Attachments.Attachment_URL__c = 'www.google.com';
        insert objSR_Attachments;

        Test.startTest();
        System.enqueueJob(new PlotHoKrfNotificationQueueable(Cas.Id));
        Test.stopTest();
    }
}