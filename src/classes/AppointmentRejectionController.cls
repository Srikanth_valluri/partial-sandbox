public class AppointmentRejectionController{
    public Calling_List__c objCalling {get;set;}
    
    public AppointmentRejectionController(ApexPages.StandardController stdController) {
        objCalling = new Calling_List__c();
        this.objCalling = (Calling_List__c)stdController.getRecord();
    }
    
    public pageReference updateCallingList(){
        try{
            list<Calling_List__c> lstCallList = new list<Calling_List__c>();
            objCalling = [Select id,name,OwnerID, Account__c,Appointment_Date__c,
                         Account__r.Name,Account__r.Email__c,Account__r.PersonEmail,
                         Appointment_Slot__c,Account_Name_for_Walk_In__c, 
                         RecordType.DeveloperName, Assigned_CRE__c,Service_Type__c, 
                         Assigned_CRE__r.id, Customer_Name__c, Handover_Team_Email__c,
                         HO_Calling_list_Owner__c, CRE_Email__c, Account_Email__c,
                         Appointment_Status__c, Sub_Purpose__c, Booking_Unit_Name__c,Booking_Unit__r.Property_Name__c,Account__r.IsPersonAccount,Account__r.PersonMobilePhone,Account__r.Mobile_Phone_Encrypt__pc,Account__r.Mobile__c,
                         (Select id,StartDateTime,EndDateTime,Subject,status__c,description,OwnerId 
                         from Events where status__c = 'Requested')
                         from Calling_List__c where id=: objCalling.id ];
            
            if(objCalling.Events.Size()>0){
                objCalling.Events[0].status__c = 'Rejected';
                
                objCalling.Appointment_Status__c = 'Rejected';
                System.debug('-->21 '+objCalling.Booking_Unit__r.Property_Name__c);
                try{
                    update objCalling;
                    System.debug('-->0 '+lstCallList);             
                    update objCalling.Events[0];
                    System.debug('-->1 '+lstCallList);
                } catch (exception e) {
                    system.debug('!!!!!!!!!exception'+e);
                }
                lstCallList.add(objCalling);
                sendEmail(lstCallList);
                pageReference pgr = new pageReference('/'+objCalling.id);
                pgr.setRedirect(true);
                return pgr;
            }
            else{
                 ApexPages.addmessage(new ApexPages.message(
                 ApexPages.severity.Warning,'This calling list does not have requested event to reject')); 
            }            
            return null;
    
        }
        catch(exception ex){
            ApexPages.addmessage(new ApexPages.message(
            ApexPages.severity.Error,ex.getMessage()+'----'+ex.getLineNumber()));
            return null;
        } 

    }
    
    public pageReference back(){
        pageReference pgr = new pageReference('/'+objCalling.id);
        pgr.setRedirect(true);
        return pgr;
    }
    
    public void sendEmail(List<Calling_List__c> lstAppointment) {
        List<Messaging.SingleEmailMessage> messages =   new List<Messaging.SingleEmailMessage>();
        //EmailTemplate reqEmailTemplate =  [SELECT ID, Subject, Body, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Appointment_Notifications_to_Customer' limit 1];
        EmailTemplate reqEmailTemplate =  [SELECT ID, Subject, Body, BrandTemplateId, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Appointments_Rejected' limit 1];
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
        String strMessageBody='Your appointment for <Purpose> at <Project Name>, <Unit Number>, on <dd/mm/yyyy> at <hh:mm> (UAE time) has been Declined. An email has been sent to your registered e-mail address with more information. For assistance call +971 4 2375000 or write to atyourservice@damacproperties.com';
        GenericEmailUtility objGenericEmailUtility = new GenericEmailUtility();
        for (Calling_List__c objCL : lstAppointment) {
            if (objCL != null && objCL.Appointment_Status__c == 'Rejected') {
                //objGenericEmailUtility.sendEmail(new List<Calling_List__c> {objCL}, 'Unit_Viewing_Appointments_Confirmed');
                //objGenericEmailUtility.sendMessage(new List<Calling_List__c> {objCL}, strMessageBody);
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setWhatId(objCL.Id);
                mail.setSaveAsActivity(true);
                list<String> bccAddress = new list<String>();
                List<String> sendTo = new List<String>();
                String body, sub;
                if (reqEmailTemplate != null) {
                    body = reqEmailTemplate.HtmlValue;
                    sub = reqEmailTemplate.Subject;
                    if (objCL.Customer_Name__c!= null) {
                        body = body.replace('{!Calling_List__c.Customer_Name__c}', objCL.Customer_Name__c);
                    } else {
                        body = body.replace('{!Calling_List__c.Customer_Name__c}', '');
                    }
                    if (objCL.Sub_Purpose__c!= null ) {
                        if( objCL.Sub_Purpose__c == 'Unit Viewing' || objCL.Sub_Purpose__c == 'Key Handover' || objCL.Sub_Purpose__c == 'Unit Viewing or Key Handover') {
                            String strBody = objCL.Sub_Purpose__c + ' at '+objCL.Booking_Unit__r.Property_Name__c+'-Handover Office';
                            if (body.contains('{!Calling_List__c.Sub_Purpose__c}')) {
                                body = body.replace('{!Calling_List__c.Sub_Purpose__c}', strBody);
                            }
                        } else if(objCL.Sub_Purpose__c == 'Documentation') {
                            String strBody = objCL.Sub_Purpose__c + ' at Executive Heights – Barsha Heights';
                            if (body.contains('{!Calling_List__c.Sub_Purpose__c}')) {
                                body = body.replace('{!Calling_List__c.Sub_Purpose__c}', strBody);
                            }
                        }
                    } else {
                        if (body.contains('{!Calling_List__c.Sub_Purpose__c}')) {
                            body = body.replace('{!Calling_List__c.Sub_Purpose__c}', '');
                        }
                    }
                    
                    if (objCL.Booking_Unit_Name__c != null) {
                        body = body.replace('{!Calling_List__c.Booking_Unit_Name__c}', objCL.Booking_Unit_Name__c);
                    } else {
                        body = body.replace('{!Calling_List__c.Booking_Unit_Name__c}', '');
                    }
                    if (objCL.Appointment_Date__c != null) {
                        body = body.replace('{!Calling_List__c.Appointment_Date__c}', String.valueOf(objCL.Appointment_Date__c));
                    } else {
                        body = body.replace('{!Calling_List__c.Appointment_Date__c}', '');
                    }
                    if (objCL.Appointment_Slot__c != null) {
                        body = body.replace('{!Calling_List__c.Appointment_Slot__c}', objCL.Appointment_Slot__c);
                    } else {
                        body = body.replace('{!Calling_List__c.Appointment_Slot__c}', '');
                    }
                    if (objCL.Booking_Unit__r.Property_Name__c != null) {
                       // body = body.replace('<Project Name>', objCL.Booking_Unit__r.Property_Name__c);
                       body = body.replace('{!Project Name}', objCL.Booking_Unit__r.Property_Name__c);
                    } else {
                        // body = body.replace('at <Project Name>', '');
                        body = body.replace('at {!Project Name}', '');
                    }
                    
                    /*if (objCL.Sub_Purpose__c!= null) {
                        if( objCL.Sub_Purpose__c == 'Unit Viewing' || objCL.Sub_Purpose__c == 'Key Handover' || objCL.Sub_Purpose__c == 'Unit Viewing or Key Handover') {
                            if (body.contains('{!Location__c.Name}')) {
                                body = body.replace('{!Location__c.Name}', 'Handover Office');
                            }
                        }
                    }*/
                    if (objCL.Sub_Purpose__c!= null) {
                        if( objCL.Sub_Purpose__c == 'Unit Viewing' || objCL.Sub_Purpose__c == 'Key Handover' || objCL.Sub_Purpose__c == 'Unit Viewing or Key Handover') {
                            if (objCL.Booking_Unit__r.Property_Name__c != null) {
                                body = body.replace('{!Location__c.Name}', objCL.Booking_Unit__r.Property_Name__c+' -Handover Office');
                            } else {
                                body = body.replace('{!Location__c.Name}', 'Handover Office');
                            }
                        }
                    }
                    body = body.replace(']]>', '');
                    mail.setHtmlBody(body);
                    mail.setSubject(sub);
                }
                if ((objCL.Sub_Purpose__c == 'Key Handover' || objCL.Sub_Purpose__c == 'Unit Viewing' || objCL.Sub_Purpose__c == 'Unit Viewing or Key Handover') &&
                    objCL.Handover_Team_Email__c != null) {
                    bccAddress.add(objCL.Handover_Team_Email__c);
                }
                if (objCL.HO_Calling_list_Owner__c != null) {
                    bccAddress.add(objCL.HO_Calling_list_Owner__c);
                }
                if (objCL.CRE_Email__c != null) {
                    bccAddress.add(objCL.CRE_Email__c);
                }
                if (bccAddress != null) {
                    mail.setBccAddresses(bccAddress);
                }
                sendTo.add(objCL.Account_Email__c);
                mail.setToAddresses(sendTo);            
                messages.add(mail);
                
                if (owea.size() > 0 ) {
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);
                }             
            } /* else {
                Messaging.SingleEmailMessage mail = 
                            new Messaging.SingleEmailMessage();
                mail.setWhatId(objCL.Id);
                mail.setSaveAsActivity(true); 
                list<String> bccAddress = new list<String>();
                List<String> sendTo = new List<String>();
                String body, sub;
                if (reqEmailTemplate != null) {
                    body = reqEmailTemplate.HtmlValue;
                    sub = reqEmailTemplate.Subject;
                    if (objCL.Customer_Name__c!= null) {
                        body = body.replace('{!Calling_List__c.Customer_Name__c}', objCL.Customer_Name__c);
                    } else {
                        body = body.replace('{!Calling_List__c.Customer_Name__c}', '');
                    }
                    if (objCL.Sub_Purpose__c!= null) {
                        body = body.replace('{!Calling_List__c.Sub_Purpose__c}', objCL.Sub_Purpose__c);
                    } else {
                        body = body.replace('{!Calling_List__c.Sub_Purpose__c}', '');
                    }
                    if (objCL.Appointment_Status__c != null) {
                        body = body.replace('{!Calling_List__c.Appointment_Status__c}', objCL.Appointment_Status__c);
                    } else {
                        body = body.replace('{!Calling_List__c.Appointment_Status__c}', '');
                    }
                    if (objCL.Booking_Unit_Name__c!= null) {
                        body = body.replace('{!Calling_List__c.Booking_Unit_Name__c}', objCL.Booking_Unit_Name__c);
                    } else {
                        body = body.replace('{!Calling_List__c.Booking_Unit_Name__c}', '');
                    }
                    if (objCL.Service_Type__c != null) {
                        body = body.replace('{!Calling_List__c.Service_Type__c}', objCL.Service_Type__c);
                    } else {
                        body = body.replace('{!Calling_List__c.Service_Type__c}', '');
                    }
                    if (objCL.Appointment_Date__c != null) {
                        body = body.replace('{!Calling_List__c.Appointment_Date__c}', String.valueOf(objCL.Appointment_Date__c));
                    } else {
                        body = body.replace('{!Calling_List__c.Appointment_Date__c}', null);
                    }
                    if (objCL.Appointment_Slot__c != null) {
                        body = body.replace('{!Calling_List__c.Appointment_Slot__c}', objCL.Appointment_Slot__c);
                    } else {
                        body = body.replace('{!Calling_List__c.Appointment_Slot__c}', '');
                    }
                    body = body.replace(']]>', '');
                    if (objCL.Service_Type__c != null) {
                        sub = sub.replace('{!Calling_List__c.Service_Type__c}', objCL.Service_Type__c);
                    } else {
                        sub = sub.replace('{!Calling_List__c.Service_Type__c}', '');
                    }
                    if (objCL.Appointment_Status__c != null) {
                        sub = sub.replace('{!Calling_List__c.Appointment_Status__c}', objCL.Appointment_Status__c);
                    } else {
                        sub = sub.replace('{!Calling_List__c.Appointment_Status__c}', '');
                    }
                    mail.setHtmlBody(body);
                    mail.setSubject(sub);
                }
                if ((objCL.Sub_Purpose__c == 'Key Handover' || objCL.Sub_Purpose__c == 'Unit Viewing' || objCL.Sub_Purpose__c == 'Unit Viewing or Key Handover' ) &&
                    objCL.Handover_Team_Email__c != null) {
                    bccAddress.add(objCL.Handover_Team_Email__c);
                }
                if (objCL.HO_Calling_list_Owner__c != null) {
                    bccAddress.add(objCL.HO_Calling_list_Owner__c);
                }
                if (objCL.CRE_Email__c != null) {
                    bccAddress.add(objCL.CRE_Email__c);
                }
                if (bccAddress != null) {
                    mail.setBccAddresses(bccAddress);
                }
                sendTo.add(objCL.Account_Email__c);
                mail.setToAddresses(sendTo);            
                messages.add(mail);
                
                if ( owea.size() > 0 ) {
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            } */
        }
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        objGenericEmailUtility.sendMessage(lstAppointment, strMessageBody);
    }
}