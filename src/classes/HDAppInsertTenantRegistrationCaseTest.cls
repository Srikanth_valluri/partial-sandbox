@isTest 
private class HDAppInsertTenantRegistrationCaseTest {

     @isTest
     static void InsertTenantRegCaseTest() {

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;

        String requestJson =  '   {  '  + 
        '    "action": "draft",  '  + 
        '    "id": "",  '  + 
        '    "booking_unit_id": "'+bookingUnit.id+'",  '  + 
        '    "first_name": "Testing",  '  + 
        '    "last_name": "TR",  '  + 
        '    "gender": "",  '  + 
        '    "date_of_birth": "",  '  + 
        '    "no_of_adults": "0",  '  + 
        '    "no_of_children": "0",  '  + 
        '    "is_person_with_spl_needs": true,  '  + 
        '    "is_having_pets": true,  '  + 
        '    "nationality": "UAE",  '  + 
        '    "passport_number": "1234",  '  + 
        '    "passport_expiry_date": "",  '  + 
        '    "emirates_id": "11222",  '  + 
        '    "mobile_code": "India: 0091",  '  + 
        '    "mobile_number": "8888857410",  '  + 
        '    "email_address": "shubham.suryawanshi@eternussolutions.com",  '  + 
        '    "lease_start_date": "2020-07-15",  '  + 
        '    "lease_end_date": "2020-08-15",  '  + 
        '    "ejari_number": "2020",  '  + 
        '    "move_in_date": "2020-08-17",  '  + 
        '    "move_in_type": "Self move-in",  '  + 
        '    "moving_company_name": "",  '  + 
        '    "moving_contractor_name": "",  '  + 
        '    "moving_contractor_phone": "",  '  + 
        '    "vehicle_details": [  '  + 
        '        {  '  + 
        '           "id": "",  '  + 
        '           "vehicle_number": "1234",  '  + 
        '           "vehicle_make": "Honda"  '  + 
        '        }  '  + 
        '    ],  '  + 
        '    "emergency_contact_details": [  '  + 
        '       {  '  + 
        '           "id": "",  '  + 
        '           "emergency_full_name" : "Test",  '  + 
        '           "emergency_relationship" : "Shubham",  '  + 
        '           "emergency_email_address" : "no@no.com",  '  + 
        '           "emergency_mobile_number" : "231324"  '  + 
        '       }  '  + 
        '      '  + 
        '    ],  '  + 
        '    "additional_members":[  '  + 
        '       {  '  + 
        '           "id": "",  '  + 
        '           "member_first_name" : "Test",  '  + 
        '           "member_last_name" : "Test",  '  + 
        '           "member_gender" : "Male",  '  + 
        '           "member_date_of_birth" : "",  '  + 
        '           "member_nationality" : "",  '  + 
        '           "member_passport_number" : "",  '  + 
        '           "member_emirates_id" : "",  '  + 
        '           "member_mobile_number" : "",  '  + 
        '           "member_email_address" : ""  '  + 
        '       }  '  + 
        '        '  + 
        '    ],  '  + 
        '    "otp_code_details": {'  + 
        '       "mobile_otp": "1234",  '  + 
        '       "email_otp": "5678",  '  + 
        '       "guid": "b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5"  '  + 
        '     }  '  + 
        '   }  '  + 
        '     '  + 
        '    ' ;

        Test.startTest();
            HDAppInsertTenantRegistrationCase.InsertTenantRegCase(requestJson);
        Test.stopTest();

     }

      @isTest
     static void InsertTenantRegCaseTest2() {

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;

        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Tenant_Registration',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId(),
                                           Request_Type__c = 'Tenant Registration',
                                           Booking_Unit__c = bookingUnit.Id);
        insert fmCase;

        OTP__c otp = new OTP__c(FM_Case__c = fmCase.Id,
                                FM_Process_Name__c = 'Tenant Registration', 
                                FM_SMS_OTP__c = '1234',
                                FM_Email_OTP__c = '5678',
                                GUID__c = 'b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5');
        insert otp;

        String requestJson =  '   {  '  + 
        '    "action": "submit",  '  + 
        '    "id": "'+fmCase.Id+'",  '  + 
        '    "booking_unit_id": "'+bookingUnit.id+'",  '  + 
        '    "first_name": "Testing",  '  + 
        '    "last_name": "TR",  '  + 
        '    "gender": "",  '  + 
        '    "date_of_birth": "",  '  + 
        '    "no_of_adults": "0",  '  + 
        '    "no_of_children": "0",  '  + 
        '    "is_person_with_spl_needs": true,  '  + 
        '    "is_having_pets": true,  '  + 
        '    "nationality": "UAE",  '  + 
        '    "passport_number": "1234",  '  + 
        '    "passport_expiry_date": "",  '  + 
        '    "emirates_id": "11222",  '  + 
        '    "mobile_code": "India: 0091",  '  + 
        '    "mobile_number": "8888857410",  '  + 
        '    "email_address": "shubham.suryawanshi@eternussolutions.com",  '  + 
        '    "lease_start_date": "2020-07-15",  '  + 
        '    "lease_end_date": "2020-08-15",  '  + 
        '    "ejari_number": "2020",  '  + 
        '    "move_in_date": "2020-08-17",  '  + 
        '    "move_in_type": "Self move-in",  '  + 
        '    "moving_company_name": "",  '  + 
        '    "moving_contractor_name": "",  '  + 
        '    "moving_contractor_phone": "",  '  + 
        '    "vehicle_details": [  '  + 
        '        {  '  + 
        '           "id": "",  '  + 
        '           "vehicle_number": "1234",  '  + 
        '           "vehicle_make": "Honda"  '  + 
        '        }  '  + 
        '    ],  '  + 
        '    "emergency_contact_details": [  '  + 
        '       {  '  + 
        '           "id": "",  '  + 
        '           "emergency_full_name" : "Test",  '  + 
        '           "emergency_relationship" : "Shubham",  '  + 
        '           "emergency_email_address" : "no@no.com",  '  + 
        '           "emergency_mobile_number" : "231324"  '  + 
        '       }  '  + 
        '      '  + 
        '    ],  '  + 
        '    "additional_members":[  '  + 
        '       {  '  + 
        '           "id": "",  '  + 
        '           "member_first_name" : "Test",  '  + 
        '           "member_last_name" : "Test",  '  + 
        '           "member_gender" : "Male",  '  + 
        '           "member_date_of_birth" : "",  '  + 
        '           "member_nationality" : "",  '  + 
        '           "member_passport_number" : "",  '  + 
        '           "member_emirates_id" : "",  '  + 
        '           "member_mobile_number" : "",  '  + 
        '           "member_email_address" : ""  '  + 
        '       }  '  + 
        '        '  + 
        '    ],  '  + 
        '    "otp_code_details": {'  + 
        '       "mobile_otp": "1234",  '  + 
        '       "email_otp": "5678",  '  + 
        '       "guid": "b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5"  '  + 
        '     }  '  + 
        '   }  '  + 
        '     '  + 
        '    ' ;

        Test.startTest();
            HDAppInsertTenantRegistrationCase.InsertTenantRegCase(requestJson);
        Test.stopTest();

     }
    
     @isTest
     static void InsertTenantRegCaseTest3() {
     
         String requestJson =  '   {  '  + 
        '    "action": "",  '  + 
        '    "id": "'+'1234'+'",  '  + 
        '    "booking_unit_id": "'+'1234'+'",  '  + 
        '    "first_name": "Testing",  '  + 
        '    "last_name": "TR",  '  + 
        '    "gender": "",  '  + 
        '    "date_of_birth": "",  '  + 
        '    "no_of_adults": "0",  '  + 
        '    "no_of_children": "0",  '  + 
        '    "is_person_with_spl_needs": true,  '  + 
        '    "is_having_pets": true,  '  + 
        '    "nationality": "UAE",  '  + 
        '    "passport_number": "1234",  '  + 
        '    "passport_expiry_date": "",  '  + 
        '    "emirates_id": "11222",  '  + 
        '    "mobile_code": "India: 0091",  '  + 
        '    "mobile_number": "8888857410",  '  + 
        '    "email_address": "shubham.suryawanshi@eternussolutions.com",  '  + 
        '    "lease_start_date": "2020-07-15",  '  + 
        '    "lease_end_date": "2020-08-15",  '  + 
        '    "ejari_number": "2020",  '  + 
        '    "move_in_date": "2020-08-17",  '  + 
        '    "move_in_type": "Self move-in",  '  + 
        '    "moving_company_name": "",  '  + 
        '    "moving_contractor_name": "",  '  + 
        '    "moving_contractor_phone": "",  '  + 
        '    "vehicle_details": [  '  + 
        '        {  '  + 
        '           "id": "",  '  + 
        '           "vehicle_number": "1234",  '  + 
        '           "vehicle_make": "Honda"  '  + 
        '        }  '  + 
        '    ],  '  + 
        '    "emergency_contact_details": [  '  + 
        '       {  '  + 
        '           "id": "",  '  + 
        '           "emergency_full_name" : "Test",  '  + 
        '           "emergency_relationship" : "Shubham",  '  + 
        '           "emergency_email_address" : "no@no.com",  '  + 
        '           "emergency_mobile_number" : "231324"  '  + 
        '       }  '  + 
        '      '  + 
        '    ],  '  + 
        '    "additional_members":[  '  + 
        '       {  '  + 
        '           "id": "",  '  + 
        '           "member_first_name" : "Test",  '  + 
        '           "member_last_name" : "Test",  '  + 
        '           "member_gender" : "Male",  '  + 
        '           "member_date_of_birth" : "",  '  + 
        '           "member_nationality" : "",  '  + 
        '           "member_passport_number" : "",  '  + 
        '           "member_emirates_id" : "",  '  + 
        '           "member_mobile_number" : "",  '  + 
        '           "member_email_address" : ""  '  + 
        '       }  '  + 
        '        '  + 
        '    ],  '  + 
        '    "otp_code_details": {'  + 
        '       "mobile_otp": "1234",  '  + 
        '       "email_otp": "5678",  '  + 
        '       "guid": "b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5"  '  + 
        '     }  '  + 
        '   }  '  + 
        '     '  + 
        '    ' ;
     
         Test.startTest();
            HDAppInsertTenantRegistrationCase.InsertTenantRegCase(requestJson);
         Test.stopTest();
     }
    
    @isTest
     static void InsertTenantRegCaseTest4() {
     
         String requestJson =  '   {  '  + 
        '    "action": "draft",  '  + 
        '    "id": "'+'1234'+'",  '  + 
        '    "booking_unit_id": "'+'1234'+'",  '  + 
        '    "first_name": "Testing",  '  + 
        '    "last_name": "TR",  '  + 
        '    "gender": "",  '  + 
        '    "date_of_birth": "",  '  + 
        '    "no_of_adults": "0",  '  + 
        '    "no_of_children": "0",  '  + 
        '    "is_person_with_spl_needs": true,  '  + 
        '    "is_having_pets": true,  '  + 
        '    "nationality": "UAE",  '  + 
        '    "passport_number": "123456789123456",  '  + 
        '    "passport_expiry_date": "",  '  + 
        '    "emirates_id": "11222",  '  + 
        '    "mobile_code": "India: 0091",  '  + 
        '    "mobile_number": "8888857410",  '  + 
        '    "email_address": "shubham.suryawanshi@eternussolutions.com",  '  + 
        '    "lease_start_date": "2020-07-15",  '  + 
        '    "lease_end_date": "2020-08-15",  '  + 
        '    "ejari_number": "2020",  '  + 
        '    "move_in_date": "2020-08-17",  '  + 
        '    "move_in_type": "Self move-in",  '  + 
        '    "moving_company_name": "",  '  + 
        '    "moving_contractor_name": "",  '  + 
        '    "moving_contractor_phone": "",  '  + 
        '    "vehicle_details": [  '  + 
        '        {  '  + 
        '           "id": "",  '  + 
        '           "vehicle_number": "1234",  '  + 
        '           "vehicle_make": "Honda"  '  + 
        '        }  '  + 
        '    ],  '  + 
        '    "emergency_contact_details": [  '  + 
        '       {  '  + 
        '           "id": "",  '  + 
        '           "emergency_full_name" : "Test",  '  + 
        '           "emergency_relationship" : "Shubham",  '  + 
        '           "emergency_email_address" : "no@no.com",  '  + 
        '           "emergency_mobile_number" : "231324"  '  + 
        '       }  '  + 
        '      '  + 
        '    ],  '  + 
        '    "additional_members":[  '  + 
        '       {  '  + 
        '           "id": "",  '  + 
        '           "member_first_name" : "Test",  '  + 
        '           "member_last_name" : "Test",  '  + 
        '           "member_gender" : "Male",  '  + 
        '           "member_date_of_birth" : "",  '  + 
        '           "member_nationality" : "",  '  + 
        '           "member_passport_number" : "",  '  + 
        '           "member_emirates_id" : "",  '  + 
        '           "member_mobile_number" : "",  '  + 
        '           "member_email_address" : ""  '  + 
        '       }  '  + 
        '        '  + 
        '    ],  '  + 
        '    "otp_code_details": {'  + 
        '       "mobile_otp": "1234",  '  + 
        '       "email_otp": "5678",  '  + 
        '       "guid": "b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5"  '  + 
        '     }  '  + 
        '   }  '  + 
        '     '  + 
        '    ' ;
     
         Test.startTest();
            HDAppInsertTenantRegistrationCase.InsertTenantRegCase(requestJson);
         Test.stopTest();
     }    
    
    @isTest
    static void testAccountEmailCase(){
        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Tenant_Registration',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId(),
                                           Request_Type__c = 'Tenant Registration');
        insert fmCase;
        
        HDAppInsertTenantRegistrationCase.FmCaseWrapper objFmCaseWrap = new HDAppInsertTenantRegistrationCase.FmCaseWrapper();
        
        Test.startTest();
        HDAppInsertTenantRegistrationCase.getAccountEmailForCase(objFmCaseWrap,fmCase);
        Test.stopTest();
    }
    
     @isTest
    static void testAccountEmailCaseElseCond(){
        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Tenant_Registration',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId(),
                                           Request_Type__c = 'Tenant Registration');
        insert fmCase;
        
        HDAppInsertTenantRegistrationCase.FmCaseWrapper objFmCaseWrap = new HDAppInsertTenantRegistrationCase.FmCaseWrapper();
        objFmCaseWrap.action = 'test';
        objFmCaseWrap.id = 'test';
        objFmCaseWrap.email_address = 'abc@xyz.com';
        //insert objFmCaseWrap;
        
        Test.startTest();
        HDAppInsertTenantRegistrationCase.getAccountEmailForCase(objFmCaseWrap,fmCase);
        Test.stopTest();
    }
}