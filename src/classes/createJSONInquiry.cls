/*****************************************************************************************
Class createJSONInquiry - Created For Accepting JSON Input and creating Inquiry 
Version   Author        Date          Comments 
1.1       Alok Chauhan  08/April/2018 Added Condition to route all leads to Wealth Advisors
.
.
.
// New Field Mapping added on Jan 21 2019
// New Field Mapping added on Jan 27 2019

*******************************************************************************************
*/

@RestResource(urlMapping='/CreateRestInquiry/*')
global with sharing class createJSONInquiry{
@HttpPut
    global static void createInquiry() {
       
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        System.debug('>>requestbody'+request.requestbody);
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> inqReq = (Map<String, Object>)JSON.deserializeUntyped(request.requestbody.tostring());
        Map<String,String> inqReqMap = new Map<String,String>();
        String sCampType='',sParentCampRecType='',gParentCampRectype='',campCountry='';
        String campName='',parentCampName='',gParentCampName='';
        String TSATeam='';
        for(String fieldName : inqReq.keySet()) {
        inqReqMap.put(fieldName, String.valueof(inqReq.get(fieldName)));
        }
        try{
        Inquiry__c  newInquiryRecord= new Inquiry__c();
        newInquiryRecord.UK_Referral_Party_Id__c = inqReqMap.get('ref_partyid');
        newInquiryRecord.ref_partyid__c = inqReqMap.get('ref_partyid');
        newInquiryRecord.First_Name__c = inqReqMap.get('firstname');
        newInquiryRecord.Last_Name__c = inqReqMap.get('lastname');
        newInquiryRecord.Email__c = inqReqMap.get('email');
        newInquiryRecord.Title__c = inqReqMap.get('title');
        newInquiryRecord.Inquiry_source__c = inqReqMap.get('Source');
        newInquiryRecord.UTM_Source__c =inqReqMap.get('UTMSource');
        newInquiryRecord.UTM_medium__c = inqReqMap.get('UTMMedium');
        newInquiryRecord.UTM_campaign__c = inqReqMap.get('UTMCampaign');
        newInquiryRecord.Heard_Of_Damac__c = inqReqMap.get('HeardOfDAMAC');
        newInquiryRecord.Description__c = inqReqMap.get('message');
        String mCode = inqReqMap.get('mobileCode');
        newInquiryRecord.Mobile_CountryCode__c = String.isNotBlank(mCode) ? mcode:'Unknown';
        newInquiryRecord.Mobile_Phone_Encrypt__c = inqReqMap.get('telephone');
        newInquiryRecord.city__c = inqReqMap.get('city');
        newInquiryRecord.Country__c = inqReqMap.get('country');
        newInquiryRecord.Nationality__c = inqReqMap.get('nationality');
        newInquiryRecord.Inquiry_status__c = 'New';
        
        newInquiryRecord.Web_Source__c = inqReqMap.get('WebSource');
        newInquiryRecord.Adgroup__c = inqReqMap.get('Adgroup');
        newInquiryRecord.Campaign_Name_HOD__c = inqReqMap.get('CampaignNameHOD');
        newInquiryRecord.Goal__c = inqReqMap.get('Goal');
        newInquiryRecord.Digital_Source__c = inqReqMap.get('DigitalSource');
        newInquiryRecord.Channel_Cluster__c= inqReqMap.get('ChannelCluster');
        newInquiryRecord.Banner_Size__c = inqReqMap.get('BannerSize');
        newInquiryRecord.Keyword__c = inqReqMap.get('Keyword');
        newInquiryRecord.Placement__c = inqReqMap.get('Placement');
        newInquiryRecord.Ad_position__c = inqReqMap.get('Adposition');
        newInquiryRecord.Match_Type__c = inqReqMap.get('MatchType');
        newInquiryRecord.Network__c = inqReqMap.get('Network');
        newInquiryRecord.Bid_Type__c = inqReqMap.get('BidType');
        newInquiryRecord.Lead_Source__c = inqReqMap.get('LeadSource');
        newInquiryRecord.Last_Mile_Conversion__c = inqReqMap.get('LastMileConversion');
        newInquiryRecord.Device__c = inqReqMap.get('Device');
        newInquiryRecord.Project_Name__c = inqReqMap.get('ProjectName');
        newInquiryRecord.OS__c = inqReqMap.get('OS');
        newInquiryRecord.Resolution__c = inqReqMap.get('Resolution');
        newInquiryRecord.Browser__c = inqReqMap.get('Browser');
        //newInquiryRecord.GA_Client_Id__c= inqReqMap.get('GAClientId');
        newInquiryRecord.GCLID__c    = inqReqMap.get('GCLID');
        newInquiryRecord.Time_Spent_before_Form_Submit__c = inqReqMap.get('TimeSpentbeforeFormSubmit');
        newInquiryRecord.IP_Address__c = inqReqMap.get('IPAddress');
        newInquiryRecord.Landing_Page_URL__c = inqReqMap.get('LandingPageURL');
        newInquiryRecord.Website_Language__c = inqReqMap.get('WebsiteLanguage');
        newInquiryRecord.Original_Web_Soruce__c = inqReqMap.get('OriginalWebSource');
        newInquiryRecord.Original_Mobile_Number__c = inqReqMap.get('OriginalMobileNumber');
        newInquiryRecord.Original_Email_Id__c = inqReqMap.get('OriginalEmailId');
        newInquiryRecord.Ported__c = inqReqMap.get('Ported');
        newInquiryRecord.Phone_Number_Type__c = inqReqMap.get('PhoneNumberType');
        newInquiryRecord.Reachable__c =inqReqMap.get('Reachable');
        newInquiryRecord.Roaming__c= inqReqMap.get('Roaming');
        newInquiryRecord.Validity__c =inqReqMap.get('Validity');
        newInquiryRecord.Mobile_Country__c =inqReqMap.get('PhoneCountry');
        newInquiryRecord.BVEmailStatus__c = inqReqMap.get('EmailStatus');
        newInquiryRecord.BVSecondaryStatus__c = inqReqMap.get('SecondaryStatus');
        newInquiryRecord.Disposable__c = inqReqMap.get('Disposable');
        newInquiryRecord.Toxic__c = inqReqMap.get('Toxic');
        newInquiryRecord.First_Touch_Channel__c = inqReqMap.get('FirstTouchChannel');
        newInquiryRecord.ga_client_id__c = inqReqMap.get('ga_client_id');
        newInquiryRecord.uniq_vis_id__c = inqReqMap.get('uniq_vis_id');
        newInquiryRecord.hashed_phone__c = inqReqMap.get('hashed_phone');
        newInquiryRecord.hashed_email__c = inqReqMap.get('hashed_email');
        
        // New Field Mapping added on Jan 21 2019
        newInquiryRecord.Budget__c = inqReqMap.get('budget');
        newInquiryRecord.Location__c = inqReqMap.get('building_location');
        newInquiryRecord.No_of_Bedrooms__c = inqReqMap.get('no_of_bedrooms');
        newInquiryRecord.Project_Status__c = inqReqMap.get('project_status');
        newInquiryRecord.Purpose_Of_Buying__c = inqReqMap.get('purpose_of_buying');
        newInquiryRecord.Financing__c = inqReqMap.get('financing');
        
        // New Field Mapping added on Jan 27 2019
        newInquiryRecord.Employment_Status__c = inqReqMap.get('employment_status');

        String campaignObjectPrefix = Campaign__c.sobjecttype.getDescribe().getKeyPrefix();
        String campaignid = inqReqMap.get('campaignid');
        boolean isPCCamp=false;
         if(String.isNotBlank(campaignid) && campaignid.startsWith(campaignObjectPrefix)){
             newInquiryRecord.Campaign__c = campaignid;
            //START :ALOK DAMAC 27/Sep/2017 Added For Telesales
            List<Campaign__c> sCampDetails = [SELECT Id,TSA_Team__c,PCAssignment__c,Campaign_Record_Type__c,Parent_Campaign_Rec_Type__c,Grand_Parent_Campaign_Rec_Type__c,Country__c,Parent_s_Parent_Campaign_Name__c,Parent_Campaign_Name__c,Campaign_Name__c from Campaign__c where id=:campaignid limit 1];
             if (sCampDetails.size()>0){
            isPCCamp = sCampDetails[0].PCAssignment__c;
            sParentCampRecType= sCampDetails[0].Parent_Campaign_Rec_Type__c;
            sCampType = sCampDetails[0].Campaign_Record_Type__c;
            gParentCampRectype = sCampDetails[0].Grand_Parent_Campaign_Rec_Type__c;
            campCountry = sCampDetails[0].Country__c;
            campName = sCampDetails[0].Campaign_Name__c;
            parentCampName= sCampDetails[0].Parent_Campaign_Name__c;
            gParentCampName=sCampDetails[0].Parent_s_Parent_Campaign_Name__c;
            TSATeam =sCampDetails[0].TSA_Team__c;
            //String dtpCamp = System.Label.DTPCampaignName;
            Id recTypeId = System.Label.PreInquiryRId;
            System.Debug('Parent Camp Rec Type>>>'+sParentCampRecType+'Camp Type>>>>>'+sCampType);
            /*String camp1= System.Label.TSCampName;
            
            set<string> tsCamps= new set<string>();
            tsCamps.addall(Label.TSCampName.split(','));
            System.Debug('TSCamps'+tsCamps);*/
            //IF(tsCamps.contains(campName)||tsCamps.contains(parentCampName)||tsCamps.contains(gParentCampName))
            System.Debug('TSATeams<<<<<<>>>>'+TSATeam);
            
            
            
            IF(TSATeam !=null && TSATeam !='')
            {
            System.Debug('Inside TSA IS NOT NULL');
            //newInquiryRecord.RecordTypeId =recTypeId;
            newInquiryRecord.TSA_Team__c=TSATeam ;
            }
            else{
            //newInquiryRecord.RecordTypeId =recTypeId;
            newInquiryRecord.TSA_Team__c='Stands';
            }
                       
            /*if(campName==camp1||parentCampName == camp1||gParentCampName==camp1){
            newInquiryRecord.RecordTypeId =recTypeId ;
            }*/
            
            /*
            if (sParentCampRecType=='Roadshows' || sCampType=='Roadshows' || gParentCampRectype=='Roadshows') {
            //Id recTypeId ='0123E0000008q3y';
            System.Debug('Rec Type Id<<<<>>>>'+ recTypeId);
            if(campCountry !='Qatar'){
            newInquiryRecord.RecordTypeId =recTypeId ;
            }
            else
            newInquiryRecord.TSA_Team__c ='';
            newInquiryRecord.isRoadshowCampaign__c='Y';
            }
            else{
            if(isPCCamp ==false){
            newInquiryRecord.RecordTypeId =recTypeId;
            }
            }
            */
            
            
            if( isPCCamp == true ){ // Assign Recordtype = Inquiry
                string inquiryrtypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_RT).getRecordTypeId();
                newInquiryRecord.RecordTypeId = inquiryrtypeId ;                
            }else if( isPCCamp == false ){ // Recordtype =  Pre Inquiry
                newInquiryRecord.RecordTypeId =recTypeId;
            }
            
            /*
            if(campCountry =='Qatar'){
                newInquiryRecord.isRoadshowCampaign__c='Y';
                string decryptedRtypeid = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.Inquiry_Decrypted_RT).getRecordTypeId();
                newInquiryRecord.RecordTypeId =decryptedRtypeid;
            }
            */
            
            //END :ALOK DAMAC 27/Sep/2017 Added For Telesales
            }
          }  
        
        Insert newInquiryRecord;
       
        res.addHeader('Content-Type', 'application/json');
        res.responseBody = Blob.valueOf('{ "status" : "SUCCESS", "Success" : "true"}');
        
       }
       catch(Exception ex){
            system.debug('Exception at line number = '+ex.getLineNumber()+', Exception message = '+ex.getmessage());
            String erMsg =ex.getmessage();
            res.addHeader('Content-Type', 'application/json');
            //res.responseBody = Blob.valueOf('{ "status" : "ERROR", "Success" : "False","Message":}');
            String retString = '{ "status" : "ERROR", "Success" : "False","Message":'+'"'+ex.getmessage()+'"'+'}';
            res.responseBody = Blob.valueOf(retString);
            
            Log__c objLog = new Log__c();
            objLog.Description__c = 'Inquiry Details: Firstname = '+inqReqMap.get('firstname')+'; Lastname = '+inqReqMap.get('lastname')+'; Email = '+inqReqMap.get('email')+
                                    '; Phone = '+inqReqMap.get('telephone')+inqReqMap.get('mobilecode')+'; Campaign Id = '+inqReqMap.get('campaignid')+
                                    '; Error at line number = '+ex.getLineNumber()+'; Error message = '+ex.getmessage();
            objLog.Type__c = 'Error while creating inquiry: Service = JSONRestInquiry';
            insert objLog;
        }    
       
    } 
    
  }