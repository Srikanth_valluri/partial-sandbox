@isTest
private class FM_CreateTenantPortalUserTest {
    static testMethod void testMethodOne() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;
        
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;
        
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;
        
        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;
        
        list<FM_Case__c> lstFMCases = TestDataFactoryFM.createFMCase(1);
        lstFMCases[0].Tenant__c = objAcc.Id;
        lstFMCases[0].First_Name__c = 'Test';
        lstFMCases[0].Last_Name__c = 'Test';
        lstFMCases[0].Tenant_Email__c = 'test@test.com';
        insert lstFMCases ;
        
        LoamsCommunitySettings__c objSetting = new LoamsCommunitySettings__c();
        objSetting.TenantProfileName__c = 'Tenant Community Login User';
        objSetting.OwnerProfileName__c = 'Customer Community Login User(Use this)';
        objSetting.GuestUserName__c = 'Community Portal Site Guest User';
        objSetting.BaseURL__c = 'http://partial-servicecloudtrial';
        insert objSetting ;
        
        test.startTest();
            FM_CreateTenantPortalUser.createPortalUser( lstFMCases );
            FM_CreateTenantPortalUser.createPortalUser( lstFMCases );
            FM_CreateTenantPortalUser.activateTenantUser( lstFMCases[0] );
        test.stopTest();
    }
    
    static testMethod void testMethodTwo() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;
        
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;
        
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;
        
        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;
        
        list<FM_Case__c> lstFMCases = TestDataFactoryFM.createFMCase(1);
        lstFMCases[0].Tenant__c = objAcc.Id;
        lstFMCases[0].First_Name__c = 'Test';
        lstFMCases[0].Last_Name__c = 'Test';
        lstFMCases[0].Tenant_Email__c = 'test@test.com';
        insert lstFMCases ;
        
                
        test.startTest();
            FM_CreateTenantPortalUser.createPortalUser( lstFMCases );
        test.stopTest();
    }
}