@isTest
public class StepStatusTransitionExtension_Test {
    
    @testSetup 
    static void setupData() {
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('pcUser@pc.com');
        insert pcUser;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Agent Registration',false,null);
        sr.New_Country_of_Sale__c = 'UAE';
        insert sr;
        
        New_Step__c stp = new New_Step__c(Service_Request__c = sr.Id, Step_No__c  = 1, Step_Type__c = 'Agent Executive Review', Step_Status__c = 'Awaiting Additional Info');
        insert stp;
        
        List<string> statuses = new list<string>{'UNDER_REVIEW','AWAITING_FFA_AA'};
            Map<string,NSIBPM__SR_Status__c> mpsrStatus =  InitializeSRDataTest.createSRStatus(statuses);
        
        Step_Status_Transition__c stepTransition = new Step_Status_Transition__c();
        stepTransition.From_Step_Status__c = 'Awaiting Additional Info';
        stepTransition.To_Step_Status__c = 'Agent HOD Approved';
        stepTransition.Step_Type__c = 'Agent Executive Review';
        stepTransition.SR_External_Status__c = 'AWAITING_FFA_AA';
        stepTransition.SR_Internal_Status__c = 'AWAITING_FFA_AA';
        stepTransition.Is_Closed__c = true;
        stepTransition.Step_No__c = 1;
        stepTransition.Service_Request_Type__c = 'Agent_Registration';
        stepTransition.UK_Deal__c = 'All';
        
        insert stepTransition;
    }
    
    @isTest 
    static void test_method_1() {
        
        New_Step__c newStep = new New_Step__c();
        for (New_Step__c step : [SELECT Id, Change_Status__c, Comments__c, Is_Closed__c, Is_UK_Deal__c,
                                 Service_Request__c, Step_No__c, Step_Status__c, Step_Type__c, Service_Request__r.Is_VAT_SR__c,
                                 Service_Request__r.RecordType.DeveloperName, Service_Request__r.NSIBPM__Required_Docs_not_Uploaded__c, Service_Request__r.Is_UK_Deal__c
                                 FROM New_Step__c]) 
        {
            newStep = step;
        }
        
        Test.startTest();
        
        ApexPages.StandardController std = new ApexPages.StandardController(newStep);
        
        StepStatusTransitionExtension extension = new StepStatusTransitionExtension(std);
        extension.newStatusTransitionId = extension.toStatusSelectOption[0].getValue();
        
        extension.updateStatus();
        extension.cancelUpdate();
        
        Test.stopTest();
    }
    
    @isTest 
    static void test_method_2() {
        
        List<NSIBPM__Service_Request__c> srList = new List<NSIBPM__Service_Request__c>([SELECT Id FROM NSIBPM__Service_Request__c]);
        for (NSIBPM__Service_Request__c sr : srList) {
            sr.NSIBPM__Required_Docs_not_Uploaded__c = true;
        }
        update srList;
        
        New_Step__c newStep = new New_Step__c();
        for (New_Step__c step : [SELECT Id, Change_Status__c, Comments__c, Is_Closed__c, Is_UK_Deal__c,
                                 Service_Request__c, Step_No__c, Step_Status__c, Step_Type__c, Service_Request__r.Is_VAT_SR__c,
                                 Service_Request__r.RecordType.DeveloperName, Service_Request__r.NSIBPM__Required_Docs_not_Uploaded__c, Service_Request__r.Is_UK_Deal__c
                                 FROM New_Step__c]) 
        {
            newStep = step;
        }
        
        Test.startTest();
        
        ApexPages.StandardController std = new ApexPages.StandardController(newStep);
        
        StepStatusTransitionExtension extension = new StepStatusTransitionExtension(std);
        extension.newStatusTransitionId = extension.toStatusSelectOption[0].getValue();
        
        extension.updateStatus();
        extension.cancelUpdate();
        extension.updateAgentSite();
        Test.stopTest();
    }
    
    @isTest 
    static void test_method_3() {
        
        List<NSIBPM__Service_Request__c> srList = new List<NSIBPM__Service_Request__c>([SELECT Id FROM NSIBPM__Service_Request__c]);
        for (NSIBPM__Service_Request__c sr : srList) {
            sr.NSIBPM__Required_Docs_not_Uploaded__c = true;
        }
        update srList;
        
        User pcUser = [Select Id From User Where isActive = True AND Profile.Name = 'Property Consultant' Limit 1];
        System.runAs(pcUser) {
            Test.startTest();
            New_Step__c newStep = [SELECT Id, Change_Status__c, Comments__c, Is_Closed__c, Is_UK_Deal__c,
                                   Service_Request__c, Step_No__c, Step_Status__c, Step_Type__c, Service_Request__r.Is_VAT_SR__c,
                                   Service_Request__r.RecordType.DeveloperName, Service_Request__r.NSIBPM__Required_Docs_not_Uploaded__c, Service_Request__r.Is_UK_Deal__c
                                   FROM New_Step__c Limit 1];
            
            
            ApexPages.StandardController std = new ApexPages.StandardController(newStep);
            
            StepStatusTransitionExtension extension = new StepStatusTransitionExtension(std);
            
            extension.updateStatus();
            extension.cancelUpdate();
            
            Test.stopTest();
        }
    }
    
}