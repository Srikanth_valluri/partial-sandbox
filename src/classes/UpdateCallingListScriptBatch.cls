public class UpdateCallingListScriptBatch implements Database.Batchable<sObject>{

    String query;

    public Database.QueryLocator start(Database.BatchableContext BC){

    Set<Id> recTypeIdSet=new Set<Id>();
    for(RecordType rec :[SELECT DeveloperName,Id,Name 
                 FROM RecordType 
                 WHERE SobjectType = 'Calling_List__c' 
                 And (Name='Collections Calling List' 
                 OR Name='DP Calling List'
                  OR Name= 'Aging Calling List')  ]){
      recTypeIdSet.add(rec.Id);
    }
    
      query = 'Select id , IsHideFromUI__c,IsHideFalse_Relationship__c from Calling_List__c Where IsHideFalse_Relationship__c != null AND IsHideFromUI__c = true AND RecordTypeId IN :recTypeIdSet';
      return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext BC, List<Calling_List__c> callingLst){
      System.debug('callingLst size:::'+callingLst.size());
      List<Calling_List__c>callingLstToHide = new List<Calling_List__c>();
      for(Calling_List__c callingObj : callingLst){
        if(callingObj.IsHideFalse_Relationship__c != null && callingObj.IsHideFromUI__c == true ){
          callingObj.IsHideFalse_Relationship__c = null;
          callingLstToHide.add(callingObj);
        }
      }
      
      System.debug('callingLstToHide::'+callingLstToHide.size());
      update callingLstToHide;
    }
    public void finish(Database.BatchableContext BC){}
}