public without sharing class AgentOnGoingBookingsController {
  
    public final String queryFields = ' Name, List_of_Units__c, DP_ok__c , Doc_ok__c , Agency__r.Name, '
                                    + ' Token_Deposit_Due_Date_Time__c, NSIBPM__External_Status_Code__c, '
                                    + ' NSIBPM__Internal_SR_Status__c, Total_Token_Amount__c, '
                                    + ' Total_Booking_Amount__c, Agent_Name__r.Name , Delivery_Mode__c '
                                    + ' FROM  NSIBPM__Service_Request__c ';
    public List<NSIBPM__Service_Request__c> lstSR             {get; set;}
    public List<NSIBPM__Service_Request__c> listToDisplay     {get; set;}
    public ID SRID                                            {get; set;}
    public String SrStatus                                    {get; set;}
    public set<String>  SetSrid                               {get; set;} 
    public String filterValue                                 {get; set;} 
    public String isClear                                     {get; set;} 
    public Map<string, string> mapSRStatusToUserDisplayStatus {get;set;}
    public Boolean userAccess                                 {get;set;}
    
    // Constructor
    public AgentOnGoingBookingsController(){
        isClear = 'none';
                                                                     
        user objUser = [Select Id,contactid,contact.account.name from User where id=:userinfo.getuserid()];
        string name = objUser.contact.account.name;
        system.debug('name'+name);
        if(name.equals(Label.POP_Account_access)) {
            userAccess = true;
        } else {
            userAccess = false;
        }
        //fetchTheUserDisplayStatus ();
        init();

    }    
        
    /**
     * Method to fetch the Custom Setting Records to display the Status as per
     * configuration
     */
     /*
    public void fetchTheUserDisplayStatus () {
        Map<String, SRStatus__c> mapStatusNameCustomSetting = SRStatus__c.getAll();
        mapSRStatusToUserDisplayStatus = new Map<String, string>();
        for(SRStatus__c status : mapStatusNameCustomSetting.values()){            
            mapSRStatusToUserDisplayStatus.put(status.name,status.User_Display_Value__c);
        }

        List<NSIBPM__SR_Status__c> srStatusUpdate = [SELECT Id,Name,NSIBPM__Code__c FROM NSIBPM__SR_Status__c LIMIT 100 ];

        for( NSIBPM__SR_Status__c srStatus : srStatusUpdate) {
            if(!mapSRStatusToUserDisplayStatus.containsKey(srStatus.NSIBPM__Code__c)) {
                mapSRStatusToUserDisplayStatus.put(srStatus.NSIBPM__Code__c,srStatus.Name);
            }
        }
        system.debug('mapSRStatusToUserDisplayStatus---'+mapSRStatusToUserDisplayStatus);
        system.debug('mapStatusNameCustomSetting---'+mapStatusNameCustomSetting);

    }*/

    //On Load Variables and Queries
    public void init() {  
        filterValue = ''; 
        System.debug('...SRID...'+SRID);
        system.debug('userInfo.getName()'+userInfo.getName()); 
        string userId = UserInfo.getUserId();
        List<User> listUser = new List<User>();
        listUser = [SELECT Id, ContactId FROM User WHERE Id = :userId LIMIT 1];
        List<Contact> listConn = new List<Contact>();  
        listConn = [ SELECT  ID, 
                             AccountId,
                             Owner__c,
                             Portal_Administrator__c
                        FROM Contact  
                       WHERE Id = :listUser[0].ContactId  
                       LIMIT 1];
        system.debug('Lovel'+listConn);

        
        for(Contact objCon : listConn){            
            // If Loop
          if(objCon.Owner__c == true || objCon.Portal_Administrator__c == true ){
            lstSR= new List<NSIBPM__Service_Request__c>();
           
            //Select Id, Booking__c,SR_Id__c, DP_Due_Date__c, Booking__r.Deal_SR__r.id from Booking_Unit__c where Booking__r.Deal_SR__c = 'a0M0E000000Z0Na'
            system.debug('Inside Agent or Owner');
            /*lstSR =  [Select id,
                             Name,
                             List_of_Units__c, 
                             DP_ok__c ,
                             Doc_ok__c ,
                             Token_Deposit_Due_Date_Time__c, 
                             NSIBPM__External_Status_Code__c,
                             NSIBPM__Internal_SR_Status__c,
                             Total_Token_Amount__c, 
                             Total_Booking_Amount__c,
                             Agency__r.Name, 
                             Agent_Name__r.Name , 
                             Delivery_Mode__c 
                             from NSIBPM__Service_Request__c 
                             where  Agency__r.Id = :objCon.AccountId 
                             order By CreatedDate DESC];
            */ 
            String queryString  = 'SELECT '
                                + queryFields
                                + ' WHERE Agency__r.Id = \''
                                + objCon.AccountId 
                                + '\' ';
            if(apexpages.currentPage().getParameters().get('sr') != null 
                && isClear == 'none') {
                srID = apexpages.currentPage().getParameters().get('sr');
                queryString = queryString 
                            + ' AND Id = \'' 
                            + srID 
                            + '\' ';
                isClear = 'block';
            } else {
                isClear = 'none';
            }
            queryString = queryString 
                        + ' ORDER BY CreatedDate DESC '; 
            System.debug('queryString--------------------- ' + queryString); 
            system.debug('IF queryString: ' + queryString); 
            lstSR= Database.query(queryString);
            System.debug('Inside IF lstSR: ' + lstSR); 
            System.debug('Inside IF lstSR: ' + lstSR.size());              
            
          }
          // Else
          else{
             system.debug('Inside NOt Agent or Owner'); 
             lstSR= new List<NSIBPM__Service_Request__c>();
             /*lstSR = [Select id,
                             Name,
                             List_of_Units__c, 
                             DP_ok__c ,
                             Doc_ok__c ,
                             Token_Deposit_Due_Date_Time__c, 
                             NSIBPM__External_Status_Code__c,
                             NSIBPM__Internal_SR_Status__c,
                             Total_Token_Amount__c, 
                             Total_Booking_Amount__c,
                             Agency__r.Name, 
                             Agent_Name__r.Name , 
                             Delivery_Mode__c 
                             from NSIBPM__Service_Request__c 
                             where  Agency__r.Id = :objCon.AccountId 
                             and Agent_Name__r.Id = :userId 
                             order By CreatedDate DESC ]; 
             */
             String queryString  = ' SELECT '
                                 + queryFields
                                 + ' WHERE Agency__r.Id = \''
                                 + objCon.AccountId 
                                 + '\' AND Agent_Name__r.Id = \''
                                 + userId 
                                 + '\' ';
                if(apexpages.currentPage().getParameters().get('sr') != null 
                    && isClear == 'none') {
                    srID = apexpages.currentPage().getParameters().get('sr');
                    queryString = queryString 
                                + ' AND Id = \'' 
                                + srID 
                                + '\' ';
                    isClear = 'block';
                } else {
                    isClear = 'none';
                }
                queryString = queryString 
                            + ' ORDER BY CreatedDate DESC ';
                system.debug('ELSE queryString: ' + queryString); 
                lstSR= Database.query(queryString);
                System.debug('Inside ELSE lstSR: ' + lstSR);   
                       
           
            }
         }
    }
   
    public void showAll() {
        isClear = 'block';
        init();
    }
    
    // Method to send Email 
    public void SendPayementLinkMail(){  
        set<Id>  SetSrid =  new Set<Id>();
        SetSrid.add(SRID);
        AgentEmailSend.SendMail(SetSrid);
    }

}