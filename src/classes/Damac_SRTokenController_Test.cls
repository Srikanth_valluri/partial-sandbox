/************************************************************************************************
 * @Name              : Damac_SRTokenController_Test
 * @Description       : Test Class for Damac_SRTokenController
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         QBurst         06/05/2020       Created
***********************************************************************************************/

@isTest
Private class Damac_SRTokenController_Test {

    @testSetup static void setup() {
        NSIBPM__SR_Status__c status = new NSIBPM__SR_Status__c ();
        status.NSIBPM__Code__c = 'SUBMITTED';
        insert status;
        Account acc = new Account ();
        acc.Name = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '969696';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        insert acc;

        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c = 1;
        newProperty.Property_Code__c = 'VIR';
        newProperty.Property_Name__c = 'VIRIDIS @ AKOYA OXYGEN';
        newProperty.District__c = 'AL YUFRAH 2';
        newProperty.AR_Transaction_Type__c = 'INV VIR';
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR';
        newProperty.Brokerage_Distribution_Set__c = '11600';
        newProperty.Sales_Commission_Dist_Set__c = '11601';
        newProperty.Currency_Of_Sale__c = 'AED' ;
        newProperty.Signature_Col_Customer_Stmt__c = 'Front Line Investment Management Co. LLC';
        newProperty.EOI_Enabled__c = true;
        insert newProperty;

        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        loc.Property_ID__c = '123';
        insert loc;   

        Inventory__c invent = new Inventory__c();
        invent.CM_Price_Per_Sqft__c = 20;
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1';
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.Id;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Released';
        invent.special_price__c = 1000000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234'; 
        invent.Floor_Package_ID__c = ''; 
        invent.building_location__c = loc.Id;
        invent.property_id__c = newProperty.Id;
        insert invent;

        NSIBPM__Service_Request__c sr = InitializeSRDataTest.getSerReq('Deal', false, null);
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        sr.Agent_Name__c = UserInfo.getUserId();
        sr.Token_Amount_AED__c = 40000;
        sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Change Agent').getRecordTypeId();
        sr.Booking_Wizard_Level__c = null;
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE';
        sr.Mode_of_Payment__c = 'Cash';
        sr.agency__c = acc.Id;
        insert sr;

        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.Id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.Id));
        lstbk[0].Deal_SR__c = sr.Id;
        insert lstbk;

        buyer__c b = new buyer__c();
        b.Buyer_Type__c = 'Individual';
        b.Address_Line_1__c = 'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.City__c = 'Dubai' ;       
        b.dob__c = system.today().addyears(-30);
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname';
        b.Last_Name__c = 'lastname';
        b.Nationality__c = 'Indian';
        b.Passport_Expiry__c = system.today().addyears(20);
        b.Passport_Number__c = 'J0565556';
        b.Phone__c = '569098767';
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971';
        b.Place_of_Issue__c = 'India';
        b.Title__c = 'Mr';
        b.booking__c = lstbk[0].Id;
        b.Primary_Buyer__c = true;
        b.Account__c = acc.Id;
        b.Unique_Key__c = lstbk[0].Id + ' ' + acc.Id;
        insert b;

        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today(); 
        pp.Building_Location__c =  loc.Id;  
        pp.term_id__c = '1234';   
        insert pp; 

        DAMAC_Constants.skip_BookingUnitTrigger = true;
        List<Booking_Unit__c> buList = new List<Booking_Unit__c>();
        Booking_Unit__c bu1 = new Booking_Unit__c();
        bu1.Booking__c = lstbk[0].Id;
        bu1.Payment_Method__c = 'Cash';
        bu1.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu1.Primary_Buyer_s_Name__c = 'testNSI';
        bu1.Primary_Buyer_s_Nationality__c = 'Russia';
        bu1.Inventory__c = invent.Id;
        bu1.Registration_ID__c = '1234';
        bu1.Payment_Plan_Id__c = pp.Id;
        buList.add(bu1);
        insert buList;

        Deal_Team__c dealTeam = New Deal_Team__c ();
        dealTeam.Associated_Deal__c = sr.Id;
        dealTeam.Associated_PC__c = UserInfo.getUserId();
        dealTeam.Associated_DOS__c = UserInfo.getUserId();
        dealTeam.Associated_HOS__c = UserInfo.getUserId();
        dealTeam.Associated_HOD__c = UserInfo.getUserId();
        insert dealTeam;

        Unit_Documents__c srDoc = new Unit_Documents__c ();
        srDoc.Service_Request__c = sr.Id;
        srDoc.Document_Type__c = 'SPA Docs';
        srDoc.Status__c = 'Pending Upload';
        srDoc.Is_Required__c = true;
        insert srDoc;

        ContentVersion cv = new Contentversion();
        cv.title = 'ABC';
        cv.PathOnClient = 'test';
        Blob blobdata = Blob.valueOf('Unit Test Attachment Body');
        cv.versiondata = EncodingUtil.base64Decode('Unit Test Attachment Body');
        insert cv;

        Attachment att = new Attachment ();
        att.ParentId = sr.Id;
        att.Name = 'test.PNG';
        att.Body = Blob.valueOf ('test');
        insert att;

        Attachment att1 = new Attachment ();
        att1.ParentId = sr.Id;
        att1.Name = 'token.PNG';
        att1.Body = Blob.valueOf ('test');
        insert att1;

        Attachment att2 = new Attachment ();
        att2.ParentId = sr.Id;
        att2.Name = 'reservation Form.PNG';
        att2.Body = Blob.valueOf ('test');
        insert att2;

        sr.Token_Attachment_Name__c = 'token.PNG';
        sr.Reservation_Form_Attachment_Name__c = 'reservation Form.PNG';
        update sr;

        New_Step__c stp = new New_Step__c();
        stp.Step_Status__c = 'Awaiting Token Deposit';
        stp.Step_Type__c = 'Token Payment';
        stp.Step_No__c = 2.1;
        stp.Service_Request__c = sr.Id;
        insert stp;

        Step_Status_Transition__c trans = new Step_Status_Transition__c();
        trans.From_Step_Status__c = 'Awaiting Token Deposit';
        trans.To_Step_Status__c = 'Token Deposit Paid';
        trans.Step_Type__c = 'Token Payment';
        trans.Step_No__c = 2.1;
        trans.SR_External_Status__c = '';
        trans.SR_Internal_Status__c = '';
        trans.Service_Request_Type__c = 'Deal';
        trans.Is_Closed__c = false;
        insert trans;

        Step_Status_Transition__c trans2 = new Step_Status_Transition__c();
        trans2.From_Step_Status__c = 'Awaiting Token Deposit';
        trans2.To_Step_Status__c = 'Token Document Rejected';
        trans2.Step_Type__c = 'Token Payment';
        trans2.Step_No__c = 2.1;
        trans2.SR_External_Status__c = '';
        trans2.SR_Internal_Status__c = '';
        trans2.Service_Request_Type__c = 'Deal';
        trans2.Is_Closed__c = false;
        insert trans2;
    }

    static testmethod void Damac_SRAttachmentView (){
        NSIBPM__Service_Request__c SR = [SELECT Id FROM NSIBPM__Service_Request__c WHERE CreatedDate = TODAY LIMIT 1];
        Unit_Documents__c doc = [SELECT Id FROM Unit_Documents__c WHERE CreatedDate = TODAY LIMIT 1];
        ContentVersion version = [SELECT Id FROM ContentVersion WHERE CreatedDate = TODAY LIMIT 1];
        Attachment att = [SELECT Id FROM Attachment WHERE CreatedDate = TODAY LIMIT 1];

        apexpages.currentpage().getparameters().put ('id', SR.Id);
        Damac_SRTokenController obj = new Damac_SRTokenController ();
        obj.init ();
        obj.loadTokenRecord ();
        apexpages.currentpage().getparameters().put ('attachmentId', att.Id);
        apexpages.currentpage().getparameters().put ('fileId', version.Id);
        apexpages.currentpage().getparameters().put ('fileId', 'Attachment');
        obj.previewAttachment ();
        Damac_SRTokenController.updateDocCVID (doc.Id, version.Id);
        Damac_SRTokenController.getRelatedParentId (version.Id, sr.Id);
        Damac_SRTokenController.getFileId (sr.Id, att.Id);
        damac_SrTokenController.getDocumentName (att.Id, 'testDoc');
        
    }

    static testmethod void Damac_SRTokenView (){
       
        NSIBPM__Service_Request__c SR = [SELECT Id FROM NSIBPM__Service_Request__c WHERE CreatedDate = TODAY LIMIT 1];
        Unit_Documents__c doc = [SELECT Id FROM Unit_Documents__c WHERE CreatedDate = TODAY LIMIT 1];
        ContentVersion version = [SELECT Id FROM ContentVersion WHERE CreatedDate = TODAY LIMIT 1];
        Attachment att = [SELECT Id FROM Attachment WHERE CreatedDate = TODAY LIMIT 1];
        apexpages.currentpage().getparameters().put ('id', sr.Id);
        apexpages.currentpage().getparameters().put('unitIds', doc.Id);
        Damac_SRTokenController obj = new Damac_SRTokenController ();
        obj.init();
        obj.fetchstepDetails();
        obj.approveRejectStep ();
        
    }
}