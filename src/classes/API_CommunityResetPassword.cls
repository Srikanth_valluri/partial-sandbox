/*
Class Name : API_CommunityResetPassword
Description : To resest the password for Community user from the API.
Sample URL: /services/apexrest/resetpassword?username=*****@*****.com
Method     : POST   
Test Class : API_Community_Test           
    
*/

@RestResource(urlMapping='/resetpassword/*')
global without sharing class API_CommunityResetPassword{ 

    
    @HttpPost
    global static LoginResponse resetPassword() {
        LoginResponse objResponse = new LoginResponse();
        
        String username = RestContext.request.params.get('username');
        User u = new User ();
        try {
            u = [SELECT ID FROM User WHERE userName =: userName AND Profile.Name LIKE '%COMMUNITY%' LIMIT 1];
            
            if (!Test.isRunningTest())
                System.resetPassword(u.Id, true);
            
            objResponse.isSuccess = true;
            objResponse.sessionId = '';
            objResponse.statusMessage = 'Password Reset email has been Initiated.';
            objResponse.statusCode = 200;
        } catch (Exception e) {
            objResponse.isSuccess = false;
            objResponse.sessionId = '';
            objResponse.statusMessage = 'UserName is not a valid Community user.';
            objResponse.statusCode = 400;
        }
        return objResponse;        
        
    }
    global class LoginResponse {
        public String sessionId {get; set;}
        public Boolean isSuccess {get; set;}
        public String statusMessage {get; set;}
        public Integer statusCode {get; set;}
    }
}