@RestResource(urlMapping='/submitLoggedInSr/*')
global class SubmitLoggedInSR_API {

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Technical error'
    };

    @HttpPost 
    global static FinalReturnWrapper SubmitSR() {

        SubmitSRResponseWrapper objSrResponse = new SubmitSRResponseWrapper();
        FinalReturnWrapper retunResponse = new FinalReturnWrapper();

        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String requestString = body.toString();
        System.debug('requestString'+requestString);
        System.debug('Request params:' + req.params);

        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();


        if(req.params.containskey('recordType') && String.isNotBlank( req.params.get('recordType') )) {

            String recordType = req.params.get('recordType');
            System.debug('recordType:: ' + recordType);

            if(String.isBlank(requestString)) {
                objSrResponse.status_code = 3;
                objSrResponse.message = 'No JSON body found';
            }
            /************************** Move-In Start*****************************************/
            else if(recordType == 'Move In') {
                System.debug('requestString:: ' + requestString);
                HDAppInsertMoveInCase.SubmitSRResponseWrapper obj = new HDAppInsertMoveInCase.SubmitSRResponseWrapper();
                    obj = HDAppInsertMoveInCase.InsertMoveInCase(requestString);
                    objSrResponse = (SubmitSRResponseWrapper)JSON.deserialize(JSON.serialize(obj), SubmitSRResponseWrapper.class);

            }
            /************************** Move-In End*****************************************/

            /************************** Amenity Booking Start*****************************************/
            else if(recordType == 'Amenity Booking') {

            }
            /************************** Amenity Booking Start*****************************************/
            else {
                objSrResponse.status_code = 3;
                objSrResponse.message = 'Unknown value found in paramter recordType : '+req.params.get('recordType');
            }
        }
        else {
            objSrResponse.status_code = 3;
            objSrResponse.message = 'Missing Parameter : recordType in the request URL';
        }

        objMeta.message = objSrResponse.message;
        objMeta.status_code = objSrResponse.status_code;
        objMeta.title = mapStatusCode.get(objSrResponse.status_code);

        //objData.tenant_id = objSrResponse.accountId;
        objData.booking_unit_id = objSrResponse.bookingUnitId;
        objData.fm_case_id = objSrResponse.FmCaseId;
        objData.fm_case_number = objSrResponse.FmCaseNumber;
        objData.fm_case_status = objSrResponse.FmCaseStatus;
        objData.owner_party_id = objSrResponse.ownerPartyId;
        objData.unit_reg_id = objSrResponse.regId;
        objData.case_submission_date = objSrResponse.submissionDate;
        objData.fm_case_details = objSrResponse.fm_case_details;

        retunResponse.data = objData;
        retunResponse.meta_data = objMeta;

        System.debug('final return response : ' + retunResponse);
        return retunResponse;

    }

    public class SubmitSRResponseWrapper {
        public String status;
        public String accountId;
        public String bookingUnitId;
        public String FmCaseId;
        public String FmCaseNumber;
        public String FmCaseStatus;
        public String ownerPartyId;
        public String regId;
        public String submissionDate;
        public HDAppInsertMoveInCase.FmCaseReturnWrapper fm_case_details;

        public String title;
        public String message;
        public Integer status_code;
    }

    //Wrapper classes for returning reponse
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_meta_data {
        public Integer status_code;
        public String message;
        public String title;
        public String developer_message;   
    }

    public class cls_data {
        public String booking_unit_id;
        public String fm_case_id;
        public String fm_case_number;
        public String fm_case_status;
        public String owner_party_id;
        public String unit_reg_id; 
        public String case_submission_date;
        public HDAppInsertMoveInCase.FmCaseReturnWrapper fm_case_details;
    }


}