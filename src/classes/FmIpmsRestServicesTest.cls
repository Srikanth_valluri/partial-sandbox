/************************************************************************************
Version History                                                                     *
------------------------------------------------------------------------------------*
Version    Date        Author            Description                                *
1.0                                      Initial Draft                              *
1.1        04/03/2019  Aishwarya Todkar  1.Added testmethod test_getAllInvoices     *
*************************************************************************************/
@isTest
private class FmIpmsRestServicesTest {

    static final String TEST_URL = 'https://test.damacgroup.com/test.pdf';

    @isTest
    static void getReceiptsForPartyIdNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestServices.getReceiptsForPartyId(NULL));
        Test.stopTest();
    }

    @isTest
    static void getReceiptsForPartyIdTest() {
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', '[]'));

        String responseBody;

        Test.startTest();
            responseBody = FmIpmsRestServices.getReceiptsForPartyId('1234');
        Test.stopTest();

        System.assertEquals('[]', responseBody);
    }

    @isTest
    static void generateDuplicateReceiptNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestServices.generateDuplicateReceipt(NULL));
        Test.stopTest();
    }

    @isTest
    static void generateDuplicateReceiptTest() {
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
            System.assertEquals(TEST_URL, FmIpmsRestServices.generateDuplicateReceipt('1234'));
        Test.stopTest();
    }

    @isTest
    static void getUnitSoaByRegistrationIdNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestServices.getUnitSoaByRegistrationId(NULL));
        Test.stopTest();
    }

    @isTest
    static void getUnitSoaByRegistrationIdTest() {
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
            System.assertEquals(TEST_URL, FmIpmsRestServices.getUnitSoaByRegistrationId('1234'));
        Test.stopTest();
    }

    @isTest
    static void getBulkSoaForPartyIdNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestServices.getBulkSoaForPartyId(NULL));
        Test.stopTest();
    }

    @isTest
    static void getBulkSoaForPartyIdTest() {
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
            System.assertEquals(TEST_URL, FmIpmsRestServices.getBulkSoaForPartyId('1234').actions[0].url);
        Test.stopTest();
    }

    @isTest
    static void getBulkSoaUrlForPartyIdTest() {
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
            System.assertEquals(TEST_URL, FmIpmsRestServices.getBulkSoaUrlForPartyId('1234'));
        Test.stopTest();
    }

    @isTest
    static void getBulkSoaForPartyIdAndProjectCodeNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestServices.getBulkSoaForPartyIdAndProjectCode('1234', NULL));
        Test.stopTest();
    }

    @isTest
    static void getBulkSoaForPartyIdAndProjectCodeTest() {
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
            System.assertEquals(
                TEST_URL,
                FmIpmsRestServices.getBulkSoaForPartyIdAndProjectCode('1234', 'TEST').actions[0].url
            );
        Test.stopTest();
    }

    @isTest
    static void getBulkSoaUrlForPartyIdAndProjectCodeTest() {
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
            System.assertEquals(TEST_URL, FmIpmsRestServices.getBulkSoaUrlForPartyIdAndProjectCode('1234', 'TEST'));
        Test.stopTest();
    }

    @isTest
    static void getInvoiceByRegistrationIdNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestServices.getInvoiceByRegistrationId(NULL));
        Test.stopTest();
    }

    @isTest
    static void getInvoiceByRegistrationIdTest() {
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
            System.assertEquals(TEST_URL, FmIpmsRestServices.getInvoiceByRegistrationId('1234'));
        Test.stopTest();
    }

    @isTest
    static void getInvoiceByRegistrationIdAndDateNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestServices.getInvoiceByRegistrationIdAndDate('1234', NULL));
        Test.stopTest();
    }

    @isTest
    static void getInvoiceByRegistrationIdAndDateTest() {
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
            System.assertEquals(TEST_URL, FmIpmsRestServices.getInvoiceByRegistrationIdAndDate('1234', Date.today()));
        Test.stopTest();
    }

    @isTest
    static void getBulkInvoiceByPartyIdNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestServices.getBulkInvoiceByPartyId(NULL));
        Test.stopTest();
    }

    @isTest
    static void getBulkInvoiceByPartyIdTest() {
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
            System.assertEquals(TEST_URL, FmIpmsRestServices.getBulkInvoiceByPartyId('1234'));
        Test.stopTest();
    }

    @isTest
    static void getBulkInvoiceByPartyIdAndProjectCodeNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestServices.getBulkInvoiceByPartyIdAndProjectCode('1234', NULL));
        Test.stopTest();
    }

    @isTest
    static void getBulkInvoiceByPartyIdAndProjectCodeTest() {
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
            System.assertEquals(TEST_URL, FmIpmsRestServices.getBulkInvoiceByPartyIdAndProjectCode('1234', 'TEST'));
        Test.stopTest();
    }

    @isTest
    static void getBulkInvoiceByPartyIdAndDateNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestServices.getBulkInvoiceByPartyIdAndDate('1234', NULL));
        Test.stopTest();
    }

    @isTest
    static void getBulkInvoiceByPartyIdAndDateTest() {
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
            System.assertEquals(TEST_URL, FmIpmsRestServices.getBulkInvoiceByPartyIdAndDate('1234', Date.today()));
        Test.stopTest();
    }

    @isTest
    static void getBulkInvoiceByPartyIdDateRangeAndProjectCodeNullTest() {
        Test.startTest();
            System.assertEquals(
                NULL,
                FmIpmsRestServices.getBulkInvoiceByPartyIdDateRangeAndProjectCode(
                    '1234', Date.today(), Date.today(), NULL
                )
            );
        Test.stopTest();
    }

    @isTest
    static void getBulkInvoiceByPartyIdDateRangeAndProjectCodeTest() {
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
            System.assertEquals(
                TEST_URL,
                FmIpmsRestServices.getBulkInvoiceByPartyIdDateRangeAndProjectCode(
                    '1234', Date.today(), Date.today(), 'TEST'
                )
            );
        Test.stopTest();
    }

    @isTest
    static void fetchResponseUrlNullTest() {
        FmIpmsRestServices.Response nullResponse;
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();

        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestServices.fetchResponseUrl(JSON.serialize(nullResponse)));
            System.assertEquals(NULL, FmIpmsRestServices.fetchResponseUrl(JSON.serialize(response)));
        Test.stopTest();
    }

    @isTest
    private static void getDueInvoicesNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestServices.getDueInvoices('', '', ''));
            System.assertEquals(NULL, FmIpmsRestServices.getDueInvoices('1234', '', ''));
        Test.stopTest();
    }

    /*@testSetup static void setupIpmsRestServices() {
        insert new IpmsRestServices__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            BaseUrl__c = 'http://0.0.0.0:8080/webservices/rest',
            Username__c = 'username',
            Password__c = 'password',
            Timeout__c = 120000
        );
    }*/
    @isTest
    private static void getDueInvoicesTest() {
        insert new IpmsRestServices__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            BaseUrl__c = 'http://0.0.0.0:8080/webservices/rest',
            Username__c = 'username',
            Password__c = 'password',
            Timeout__c = 120000
        );

        FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => 'registrationId',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName',
                'ATTRIBUTE8' => 'trxNumber',
                'ATTRIBUTE9' => 'creationDate',
                'ATTRIBUTE10' => 'callType',
                'ATTRIBUTE11' => '50',
                'ATTRIBUTE12' => '100',
                'ATTRIBUTE13' => '50',
                'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                'ATTRIBUTE15' => 'trxType',
                'ATTRIBUTE16' => '0'
            }
        );
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));

        Test.startTest();
            FmIpmsRestServices.DueInvoicesResult result = FmIpmsRestServices.getDueInvoices('1234', '', '');
            System.assertNotEquals(NULL, result);
        Test.stopTest();
    }

    @isTest
    private static void createReceiptNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestServices.createReceipt(NULL));
        Test.stopTest();
    }

    @isTest
    private static void createReceiptTest() {

        FmIpmsRestServices.ApplicationLine applicationLine = new FmIpmsRestServices.ApplicationLine();
        applicationLine.registrationId  = '25';
        applicationLine.amountToApply   = 2000;
        FmIpmsRestServices.CreateReceiptRequest request = new FmIpmsRestServices.CreateReceiptRequest();
        request.sourceSystem        = 'SFDC';
        request.extRequestNumber    = 'RECEIPT-12345';
        request.businessGroup       = 'FM';
        request.partyId             = '3893';
        request.receiptNumber       = 'Test-0910181';
        request.paidAmount          = 4000;
        request.currencyCode        = 'AED';
        request.applyReceipt        = 'Yes';
        request.comments            = 'This is a general Payment done';
        request.applicationLines    = new List<FmIpmsRestServices.ApplicationLine> {applicationLine};
        String mockRequestJson = JSON.serialize(request);

        String mockResponseJson = '{"responseId": "260645","responseTime": "Sun Oct 14 13:13:50 GST 2018","requestName": "CREATE_RECEIPT","extRequestName": "RECEIPT-12345","responseMessage": "CREATE_RECEIPT Completed, Generate the Receipt to View the Receipt File","actions": [{"action": "GENERATE_RECEIPT","method": "GET","url": "https://ptctest.damacgroup.com/DCOFFEE/report/RECEIPT/IPMS/NO_KEY/RECEIPT_ID/2137546"}],"complete": true}';

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', mockResponseJson));

        Test.startTest();
            System.assert(String.isNotBlank(FmIpmsRestServices.createReceipt(request)));
        Test.stopTest();
    }

    @isTest
    private static void test_getAllInvoices() {
         insert new IpmsRestServices__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            BaseUrl__c = 'http://0.0.0.0:8080/webservices/rest',
            Username__c = 'username',
            Password__c = 'password',
            Timeout__c = 120000
        );
        Test.setMock(HttpCalloutMock.class, new FMServiceChargesHttpMock(1));
        Test.startTest();
            FmIpmsRestServices.DueInvoicesResult dueTestRes = FmIpmsRestServices.getAllInvoices('1231','43434', 'testProperty');
            system.debug('dueTestRes== ' + dueTestRes);
        Test.stopTest();
    }
}