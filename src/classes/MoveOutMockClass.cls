@isTest 
global with sharing class MoveOutMockClass implements WebServiceMock {
     global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType){
               MultipleDocUploadService.DocumentAttachmentMultipleResponse_element  calloutObj = new  MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
               calloutObj.return_x= '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [null-1531216121646.]","PARAM_ID":"IPMS-1039032-VfRemoting.Js","URL":"https://sftest.deeprootsurface.com/docs/t/null-1531216121646."}],"message":"Service Process Completed","status":"S"}'; 
               response.put('response_x', calloutObj);  
          }
}