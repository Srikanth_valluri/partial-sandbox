public class InventoryDesignChangeDetailController extends InventoryDesignChangeUtility {
    
    public Design_Change_Request__c icd{get; set;}
    public list<LineData> csvLines{get; set;}
    public string comments{get; set;}
    public string actionType{get; set;}
    public boolean isVilla { get; set; }
    
    public InventoryDesignChangeDetailController() {
        prepareData();
    }

    public void prepareData(){
        csvLines = new list<LineData>();
        isVilla = false;
        id changeId = ApexPages.CurrentPage().getParameters().get('id');
        icd = [Select id, name, Property__c, Property__r.name, Building__c, 
                Building__r.name, status__c, Approval_comments__c 
                from Design_Change_Request__c 
                where id =: changeId]; 

        attachment att = [Select id, name, body from Attachment where parentId =: icd.Id];
        string DataAsString = blobToString(att.body,'ISO-8859-1');
        DataAsString = DataAsString.replaceAll('(\r\n|\r)','\n');
            
        list<string> Lines = DataAsString.split('\n');
        system.debug('fileLines Size: ' + Lines.size());

        set<string> unitKeys = new set<string>();
        for(integer i = 1; i< lines.size(); i++){
            list<string> vals = lines[i].split(',');
            if (vals.size () > 0)
                unitKeys.add(icd.Property__r.name+'/'+icd.Building__r.name+'/'+vals[0].replaceAll('Floor','').trim());
        }

        map<string, Inventory__c> mpExisting = new map<string, Inventory__c>();
        for(Inventory__c inv : [Select id, Unit_Key__c from Inventory__c where Unit_Key__c in: unitKeys]){
            mpExisting.put(inv.Unit_Key__c, inv);
        }
        
        for (integer i = 0; i < 1; i++) {
            String line = lines[i].trim();
            list<string> vals = line.split(',');
            if (vals[0].toLowerCase() == 'plot no') {
                isVilla = true;
            }
        }
        if (isVilla) {
            for(integer i = 1; i< lines.size(); i++){
                string line = lines[i].trim();
                list<string> vals = line.split(',');
                if (vals.size () > 0) {
                    LineData d = new lineData();
                    string key = icd.Property__r.name+'/'+icd.Building__r.name+'/'+vals[0].replaceAll('Floor','').trim();
                    d.action = mpExisting.containsKey(key) ? 'Update' : 'Create';
                    d.plotNo =  vals[0];
                    d.protoType = vals[1];
                    d.View_Type = vals[2];
                    d.space_type = vals[3];
                    d.position = vals[4];
                    d.free_parking = vals[5];
                    d.plot_area_sqm = vals[6];
                    d.plot_area_sqft = vals[7];
                    d.max_gfa_sqm = vals[8];
                    d.max_gfa_sqft = vals[9];
                    d.net_sellablearea_sqm = vals[10];
                    d.net_sellablearea_sqft = vals[11];
                    d.project = icd.Property__r.name;
                    d.building = icd.Building__r.name;
                    d.projectId = icd.Property__c;
                    d.buildingId = icd.Building__c;
                    d.inventoryId = mpExisting.containsKey(key) ? mpExisting.get(key).id : null;
                    csvLines.add(d);
                }
            }
        }
        else {
            for(integer i = 1; i< lines.size(); i++){
                string line = lines[i].trim();
                list<string> vals = line.split(',');
                LineData d = new lineData();
                string key = icd.Property__r.name+'/'+icd.Building__r.name+'/'+vals[0].replaceAll('Floor','').trim();
                d.action = mpExisting.containsKey(key) ? 'Update' : 'Create';
                d.floor = vals[0];
                d.floor_Code = vals[0].replaceAll('Floor','').trim();
                d.unit_no = vals[1];
                d.unit_Code = icd.Building__r.name+'/'+vals[0].replaceAll('Floor','').trim()+'/'+vals[1];
                d.bedroom = vals[3];
                d.merge_unit_no = vals[2];
                d.suite_area_sqm = vals[4];
                d.suite_area_sqft = vals[5];
                d.balcony_area_sqm = vals[6];
                d.balcony_area_sqft = vals[7];
                d.terrace_area_sqm = vals[8];
                d.terrace_area_sqft = vals[9];
                d.common_area_sqm = vals[10];
                d.common_area_sqft = vals[11];
                d.sellable_area_sqm = vals[12];
                d.sellable_area_sqft = vals[13];
                d.View_Type = vals[14];
                d.space_type = vals[15].trim();
                d.project = icd.Property__r.name;
                d.building = icd.Building__r.name;
                d.projectId = icd.Property__c;
                d.buildingId = icd.Building__c;
                d.inventoryId = mpExisting.containsKey(key) ? mpExisting.get(key).id : null;
                csvLines.add(d);
            }
        }

        //validateData(csvLines,icd);
    }

    public void submitApproval(){
        
        system.debug('>>>>>action>>>>>>>>>'+actionType);
        system.debug('>>>>>comments>>>>>>>>>'+comments);
        system.debug('>>>>>comments>>>>>>>>>'+ApexPages.Currentpage().getParameters().get('aType'));
        
        list<ProcessInstanceWorkitem> items = [Select Id 
                                                from ProcessInstanceWorkitem p
                                                where ProcessInstance.TargetObjectId =: icd.Id];
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setComments(comments);
        req.setAction(actionType);
        req.setWorkitemId(items[0].Id);
        Approval.ProcessResult result =  Approval.process(req);

        prepareData();
        
    }
}