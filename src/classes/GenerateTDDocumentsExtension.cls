/*
 * Description: Class used to generate SR Attachments for Title Deed Case
 */

public class GenerateTDDocumentsExtension {
    public Case objCase;

    public GenerateTDDocumentsExtension(ApexPages.StandardController controller) {
        objCase = (Case)controller.getrecord();
    }
    
    /**
     * Method used to generate SR Attachments for Title Deed Case from Handover and parent Handover case
     */
    public pageReference generateSRAttachments() {
        
        List<SR_Attachments__c> lstSRAttachments = new List<SR_Attachments__c>();
        String strQuery = 'SELECT Id, Name, Case__c, IsValid__c, Attachment_URL__c FROM SR_Attachments__c WHERE ';
        
        try {

            // Fetch the Handover Case ID and its parent Handover Case ID
            objCase = [SELECT ParentId, Parent.ParentId FROM Case WHERE Id =: objCase.Id];

            // Check if Handover Case ID and its parent Handover Case ID is present or not
            if (objCase.ParentId == null && objCase.Parent.ParentId == null) {
                ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Error,'Unexpected Exception has occured.Parent Handover Case and Handover Case are missing'));
                return null;            
            } else if (objCase.ParentId != null && objCase.Parent.ParentId == null) {
                strQuery += 'Case__c = '+'\'' + objCase.ParentId + '\'';
            } else if (objCase.ParentId != null && objCase.Parent.ParentId != null) {
                strQuery += 'Case__c = '+ '\'' + objCase.ParentId + '\'' + ' OR Case__c = '+ '\''+ objCase.Parent.ParentId + '\'';
            }

            // Fetch the SR Attachments to be inserted in Title Deed Case
            lstSRAttachments = Database.query(strQuery);

            if (lstSRAttachments.size() > 0) {
                List<SR_Attachments__c> lstNewSRAttachments = new List<SR_Attachments__c>();
                
                for (SR_Attachments__c objSRAttachment : lstSRAttachments) {
                    SR_Attachments__c objNewSRAttachment = new SR_Attachments__c();
                    objNewSRAttachment.Name = objSRAttachment.Name;
                    objNewSRAttachment.Case__c = objCase.Id;
                    objNewSRAttachment.IsValid__c = objSRAttachment.IsValid__c;
                    objNewSRAttachment.Attachment_URL__c= objSRAttachment.Attachment_URL__c;
                    lstNewSRAttachments.add(objNewSRAttachment);
                }
                
                insert lstNewSRAttachments;

                if(!lstNewSRAttachments.isEmpty()) {
                    Set<Id> setSRAttachmentsId = new Set<Id>();
                    for(SR_Attachments__c objSRAtt : lstNewSRAttachments) {
                        setSRAttachmentsId .add(objSRAtt.Id);
                    }

                    // Upload the SR Attachments to IPMS
                    UploadDocToCentralRepoBatch batchInstance = new UploadDocToCentralRepoBatch(setSRAttachmentsId);
                    Database.executeBatch(batchInstance, 1);
                }
            }
        } catch (Exception objException) {
            Error_Log__c objError = new Error_Log__c(Error_Details__c = objException.getMessage(), Process_Name__c = 'Title Deed', Case__c = objCase.Id);
            insert objError;

            ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.Error,'Unexpected Exception has occured. Please contact System Administrator for furthur details'));
            return null;
        }

        // Redirect to Case detail page
        PageReference caseDetailPage = new PageReference ('/'+objCase.Id);
        return caseDetailPage;
    }
}