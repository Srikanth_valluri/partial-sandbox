public without sharing class SRAttachmentController {
    public String selectedType                                         {get;set;}
    public Boolean selectedAwesomeness                                 {get;set;} 
    public String description                                          {get;set;}
    public Case objCase                                                {get;set;}
    public String fileName                                             {get;set;}
    public Blob fileBody                                               {get;set;}
    public String nameDocument                                         {get;set;}
    public String selectedDocType                                      {get;set;}
    public List<selectOption> lstDocType                                     {get;set;}
    public Id recIdCOD;
    public Id recIdJB;
    public Id recIdNNC;
    public Id recIdPP;
    public Case objCase1;
    public SR_Attachments__c attachmentObj;


    public SRAttachmentController(ApexPages.StandardController controller) {
        System.debug('===Constructor called==' + controller);
        this.objCase = (Case) controller.getRecord();
        System.debug('===this.objCase==' + this.objCase);
        Map<String, Schema.RecordTypeInfo> schemaMap = Schema.SObjectType.Case.getRecordTypeInfosByName();
        System.debug('===Constructor schemaMap==' + schemaMap);
        recIdCOD=schemaMap.get('Change of Details').getRecordTypeId();
        recIdJB=schemaMap.get('Change of Joint Buyer').getRecordTypeId();
        recIdNNC=schemaMap.get('Name Nationality Change').getRecordTypeId();
        recIdPP=schemaMap.get('Passport Detail Update').getRecordTypeId();
        //selectedDocType = objCase.Document_Type__c;
        /*lstDocType = new List<selectOption>();
        Schema.sObjectType objType = SR_Attachments__c.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        list<Schema.PicklistEntry> values =
        fieldMap.get('Document_Type__c').getDescribe().getPickListValues();
        // Add these values to the selectoption list.
        lstDocType.add(new SelectOption('none', '--Select--'));
        for (Schema.PicklistEntry a : values)
        { 
            lstDocType.add(new SelectOption(a.getLabel(), a.getValue())); 
        }
        System.debug('===lstDocType==' + lstDocType);*/
    }

    // creates a new SR_Attachments__c record
    @TestVisible
    private Database.SaveResult saveCustomAttachment() {
        attachmentObj = new SR_Attachments__c();
        attachmentObj = getDocUrl(attachmentObj);
        system.debug('getDocUrl obj : '+attachmentObj);
        return Database.insert(attachmentObj);
    }

    public PageReference processUpload() {
        selectedDocType = objCase.Document_Type__c;
        if(objCase.RecordTypeId == recIdCOD || objCase.RecordTypeId == recIdJB 
            || objCase.RecordTypeId == recIdNNC || objCase.RecordTypeId == recIdPP
        ) {
            if(selectedDocType == 'none') {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select Document Type.'));
                return null;
            }
        }
        try {
            Database.SaveResult customAttachmentResult = saveCustomAttachment();

            if (customAttachmentResult == null || !customAttachmentResult.isSuccess()) {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                    'Could not save attachment.'));
                return null;
            } else {
                if(objCase.RecordTypeId == recIdCOD || objCase.RecordTypeId == recIdJB 
                || objCase.RecordTypeId == recIdNNC || objCase.RecordTypeId == recIdPP
                ) {
                    if(selectedDocType.equalsIgnoreCase('Additional Document')) {
                        objCase1.Additional_Doc_File_URL__c = attachmentObj.Attachment_URL__c;
                    } else if(selectedDocType.equalsIgnoreCase('COD Document')) {
                        objCase1.CRF_File_URL__c = attachmentObj.Attachment_URL__c;
                    } else if(selectedDocType.equalsIgnoreCase('Court Order')) {
                        objCase1.CRF_File_URL__c = attachmentObj.Attachment_URL__c;
                    } else if(selectedDocType.equalsIgnoreCase('LOD Document')) {
                        objCase1.OD_File_URL__c = attachmentObj.Attachment_URL__c;
                    } else if(selectedDocType.equalsIgnoreCase('Passport Document')) {
                        objCase1.Passport_File_URL__c = attachmentObj.Attachment_URL__c;
                    } else if(selectedDocType.equalsIgnoreCase('POA document')) {
                        objCase1.POA_File_URL__c = attachmentObj.Attachment_URL__c;
                    }
                    objCase1.Document_Type__c = selectedDocType;
                    update objCase1;
                    System.debug('===Case updated ==' + objCase1);
                }
            }
        } catch (Exception e) {
            ApexPages.AddMessages(e);
            return null;
        }
        // return null;
        return new PageReference('/' + objCase.Id);
    }

    public PageReference back() {
        return new PageReference('/' + objCase.Id);
    }

    public SR_Attachments__c getDocUrl(SR_Attachments__c obj) {
        objCase1 = [Select Id,
                            CaseNumber,
                            Booking_Unit__c,
                            Booking_Unit__r.Registration_ID__c,
                            Additional_Doc_File_URL__c,
                            CRF_File_URL__c,
                            OD_File_URL__c,
                            Passport_File_URL__c,
                            POA_File_URL__c,
                            Document_Type__c
                       From Case
                      Where Id =: objCase.Id
                    ];
        if (fileName != null && fileBody != null) {
            obj.Case__c = objCase.Id;
            obj.Name = nameDocument;
            obj.description__c = description;
            obj.type__c = selectedType;
            obj.isValid__c = selectedAwesomeness;

            List < UploadMultipleDocController.MultipleDocRequest > lstMultipleDocReq = new List < UploadMultipleDocController.MultipleDocRequest > ();
            UploadMultipleDocController.data respObj = new UploadMultipleDocController.data();
            List < SR_Attachments__c > lstSRAttacg = new List < SR_Attachments__c > ();
            String errorMessage = '';
            List < Error_Log__c > errorLogList = new List < Error_Log__c > ();
            String stType = extractType(fileName);
            String crfAttachmentName = extractName(fileName);
            crfAttachmentName = crfAttachmentName.replaceAll(' ','_');
            crfAttachmentName = crfAttachmentName.replaceAll('/','_');
            system.debug('crfAttachmentName************'+crfAttachmentName);

            UploadMultipleDocController.MultipleDocRequest reqObjCRF = new UploadMultipleDocController.MultipleDocRequest();
            reqObjCRF.base64Binary = EncodingUtil.base64Encode(fileBody);
            reqObjCRF.category = 'Document';
            reqObjCRF.entityName = 'Damac Service Requests';
            reqObjCRF.fileDescription = fileName;
            //reqObjCRF.fileId = 'IPMS-' + objCase1.CaseNumber + '-' + crfAttachmentName;
            //reqObjCRF.fileName = 'IPMS-' + objCase1.CaseNumber + '-' + crfAttachmentName;
            reqObjCRF.fileId = objCase1.CaseNumber + String.valueOf(System.currentTimeMillis())+'.'+stType;
            reqObjCRF.fileName = objCase1.CaseNumber + String.valueOf(System.currentTimeMillis())+'.'+stType;
            
            reqObjCRF.registrationId = objCase1.CaseNumber;
            reqObjCRF.sourceFileName = 'IPMS-' + objCase1.CaseNumber + '-' + crfAttachmentName +'.'+stType;
            reqObjCRF.sourceId = 'IPMS-' + objCase1.CaseNumber + '-' + crfAttachmentName;
            //fileBody = NULL; //if user deletes, and saves the draft
            lstMultipleDocReq.add(reqObjCRF);

            if (lstMultipleDocReq.size() > 0) {
                respObj = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);
                System.debug('===respObj==' + respObj);
            }

            if (respObj != NULL && lstMultipleDocReq.size() > 0) {
                if (respObj.status == 'Exception') {
                    errorMessage = respObj.message;
                    Error_Log__c objErr = new Error_Log__c();
                    errorMessage = respObj.message;
                    objErr.Error_Details__c = errorMessage;
                    objErr.Case__c = objCase.Id;
                    errorLogList.add(objErr);
                }
                if (respObj.Data == null || respObj.Data.size() == 0) {
                    errorMessage = 'Problems while getting response from document upload';
                    Error_Log__c objErr = new Error_Log__c();
                    errorMessage = respObj.message;
                    objErr.Error_Details__c = errorMessage;
                    objErr.Case__c = objCase.Id;
                    errorLogList.add(objErr);
                }

                for (UploadMultipleDocController.MultipleDocResponse objData: respObj.data) {
                    if (objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)) {
                        obj.Attachment_URL__c = objData.url;
                        system.debug('objData.url : '+objData.url);
                        system.debug('obj.Attachment_URL__c : '+obj.Attachment_URL__c);
                    } else {
                        errorMessage = 'Problems while getting response from document ' + objData.PARAM_ID;
                        Error_Log__c objErr = new Error_Log__c();
                        errorMessage = respObj.message;
                        objErr.Error_Details__c = errorMessage;
                        objErr.Case__c = objCase.Id;
                        errorLogList.add(objErr);
                    }

                }

            }
            
            if (errorLogList != null && errorLogList.size() > 0) {
                insert errorLogList;
                System.debug('===errorLogList==' + errorLogList);
            }
        }
        return obj;
    }
    @TestVisible
    private String extractName(String strName) {
        return strName.substring(strName.lastIndexOf('\\') + 1);
    }
    @TestVisible
    private String extractType( String strName ) {
        strName = strName.substring( strName.lastIndexOf('\\')+1 );
        return strName.substring( strName.lastIndexOf('.')+1 ) ;
    }
}