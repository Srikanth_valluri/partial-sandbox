@isTest
public class AP_FormRequestControllerTest{
    testMethod static void formRequestDisplay(){
        Account acc = new Account();
        acc.Name = 'Testing Account';
        acc.Agency_Email__c = 'acc@email.com';
        insert acc;
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c = 'First';
        inquiry.Last_Name__c = 'Last';
        inquiry.Inquiry_Status__c = 'Active';
        inquiry.Email__c = 'test@email.com';
        insert inquiry;
        Form_Request__c formRequest = new Form_Request__c();
        formRequest.Inquiry__c = inquiry.Id;
        formRequest.Status__c = 'Sign Now';
        insert formRequest;
                                                    
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
        
        //Get Content Documents
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = formRequest.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
        
        AP_FormRequestController formReqContr = new AP_FormRequestController();
        AP_FormRequestController.previewSignedForm (formRequest.Id);
    }
    
}