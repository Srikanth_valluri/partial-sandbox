public without sharing class AOPT30thDayNotification {
    @InvocableMethod 
    public static void checkIfAddendumUploaded( List<Case> lstCases )
    {
        Set<Id> setCaseId = new Set<Id>();
        system.debug( 'checkIfAddendumUploaded 30 method called'+lstCases );
        for( Case objCase : lstCases ) {
            setCaseId.add(objCase.Id);
        }
            
        
        if(!setCaseId.isEmpty()) {
            List<Case> lstCasesToUpdate = new List<Case>();
            for(Case objCase: [Select Id, Is_30th_Day__c From Case Where Id IN: setCaseId]) {
                objCase.Is_30th_Day__c = true;
                lstCasesToUpdate.add(objCase);
            }
            update lstCasesToUpdate;
        }
    }
}