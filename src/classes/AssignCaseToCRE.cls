/**************************************************************************************************
 * Class : AssignCaseToCRE
 --------------------------------------------------------------------------------------------------
 * Description : Called from Process Builder to assign CRE to Registered VIP from present Primary,
 *               Secondary,Tertiary CRE
 * Test Class : 
 **************************************************************************************************/ 
public class AssignCaseToCRE{

    private static final String QUEUE_NAME = System.Label.NoCreAvailableQueue;
    
    
   /**
     * Method to check if a account is vip account or not
     */
    public static Boolean isVipAccount(Account account) {
        List <CRE_Allocation_Customer_Type_Setting__mdt> lstCustomerTypesSetting = getCustomerTypesSetting();

        // Get customer type of account
        String customerType = getCustomerType(
            account.AnnualRevenue,
            lstCustomerTypesSetting
        );

        if (String.isNotBlank(customerType) && customerType != 'Others') {
            return true;
        } else {
            return false;
        }
    }

    public static List <CRE_Allocation_Customer_Type_Setting__mdt> getCustomerTypesSetting() {
        return new List <CRE_Allocation_Customer_Type_Setting__mdt>(
            [ SELECT Id
                   , MasterLabel
                   , Maximum_Value__c
                   , Minimum_Value__c
                   , DeveloperName
                   , Is_Max__c
                FROM CRE_Allocation_Customer_Type_Setting__mdt ] ) ;
    }

    public static String getCustomerType(
        Decimal decAmount,
        List <CRE_Allocation_Customer_Type_Setting__mdt> lstCustomerTypesSetting
    ) {
        for( CRE_Allocation_Customer_Type_Setting__mdt objType : lstCustomerTypesSetting ) {
            if( objType.Minimum_Value__c <= decAmount && ( ( objType.Maximum_Value__c != null && objType.Maximum_Value__c > decAmount ) || objType.Is_Max__c )  ) {
                return objType.MasterLabel;
            }
        }
        return '';
    }
    
    /**
     * To assign appropriate CRE to Case
     */
    @InvocableMethod public static void updateCases(List<ID> ids) {
        List<Case> toUpdateCases = new List<Case>();
        List<Group> groupList = [SELECT 
                                    DeveloperName,
                                    Id,
                                    Name 
                                 FROM Group 
                                 WHERE DeveloperName =: QUEUE_NAME ];
                                
        List<Case> casesList = [SELECT 
                                    Account.Primary_CRE__c, 
                                    Account.Secondary_CRE__c, 
                                    Account.Tertiary_CRE__c,
                                    Account.Primary_Language__c,
                                    Account.AnnualRevenue,
                                    OwnerId, 
                                    Subject 
                                FROM Case 
                                WHERE Id IN : ids];
     
        for(Case caseObj : casesList) {
            Boolean val = isVipAccount(caseObj.Account);
            if(isVipAccount(caseObj.Account) != true) {
                return;
            }
            
            if(!String.isBlank(caseObj.Account.Primary_CRE__c)) {
                 caseObj.OwnerId = caseObj.Account.Primary_CRE__c;
            }
            else if(!String.isBlank(caseObj.Account.Secondary_CRE__c)) {
                 caseObj.OwnerId = caseObj.Account.Secondary_CRE__c;
            } 
            else if(!String.isBlank(caseObj.Account.Tertiary_CRE__c)) {
                caseObj.OwnerId = caseObj.Account.Tertiary_CRE__c; 
            }
            else{
                 caseObj.OwnerId = groupList[0].Id;
            }         
            toUpdateCases.add(caseObj); 
        }
        update toUpdateCases;
  }
}