@isTest
private class AssignCaseToFAMQueueTest {
    
    private static testMethod void testMethodToAddress(){
        /*Group testGroup = new Group(Name='Email - Service Request', Type='Queue', DeveloperName = Label.FAMQueue);
        insert testGroup;*/
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
        
        Case caseObj = new Case(Subject = 'Test Case', Origin = 'Email', Priority = 'Medium',AccountId =objAcc.Id, 
                                RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Email').getRecordTypeId());
        insert caseObj;
        
        EmailMessage[] newEmail = new EmailMessage[0];
        newEmail.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            Incoming = True, ToAddress= 'atyourservice@damacproperties.com', 
            Subject = 'famproperties', 
            TextBody = '23456 ', 
            ParentId = caseObj.Id)); 
        insert newEmail;
        List<Case> caseList = [SELECT Id FROM Case];
        List<Id> caseIds = new List<Id>();
            for(Case thiscase : caseList) {
                caseIds.add(thiscase.Id);
            }
        //AssignCaseToFAMQueue  assignCase = new AssignCaseToFAMQueue();
        AssignCaseToFAMQueue.updateCases(caseIds);
        
    }
    
    private static testMethod void testMethodBccAddress(){
        /*Group testGroup = new Group(Name='Email - Service Request', Type='Queue', DeveloperName = Label.FAMQueue);
        insert testGroup;*/
        
        String valueToCheck = Label.ValueToCheckOnEmailToReRoute;
        List<String> listvalueToCheck = new List<String>();
        listvalueToCheck = valueToCheck.split(',');  
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
        
        Case caseObj = new Case(Subject = 'Test Case', Origin = 'Email', Priority = 'Medium',AccountId = objAcc.Id,
                                RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Email').getRecordTypeId());
        caseObj.Subject = listvalueToCheck[0];
        caseObj.Description = listvalueToCheck[0];
        insert caseObj;
        
        EmailMessage[] newEmail = new EmailMessage[0];
        newEmail.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            Incoming = True, ToAddress= 'collectios@damacproperties.com',
            BccAddress = 'collectios@damacproperties.com',
            Subject = 'famproperties', 
            TextBody = '23456 ', 
            ParentId = caseObj.Id)); 
        insert newEmail;
        List<Case> caseList = [SELECT Id FROM Case];
        List<Id> caseIds = new List<Id>();
            for(Case thiscase : caseList) {
                caseIds.add(thiscase.Id);
            }
        //AssignCaseToFAMQueue  assignCase = new AssignCaseToFAMQueue();
        AssignCaseToFAMQueue.updateCases(caseIds);
        
    }
    
    private static testMethod void testMethodCcAddress(){
        /*Group testGroup = new Group(Name='Email - Service Request', Type='Queue', DeveloperName = Label.FAMQueue);
        insert testGroup;*/
        
        String valueToCheck = Label.ValueToCheckOnEmailToReRoute;
        List<String> listvalueToCheck = new List<String>();
        listvalueToCheck = valueToCheck.split(',');  
        
        Case caseObj = new Case(Subject = 'Test Case', Origin = 'Email', Priority = 'Medium',
                                RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Email').getRecordTypeId());
        caseObj.Subject = listvalueToCheck[0];
        caseObj.Description = listvalueToCheck[0];
        insert caseObj;
        
           
        
        EmailMessage[] newEmail = new EmailMessage[0];
        newEmail.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            Incoming = True, ToAddress= 'collectios@damacproperties.com',
            BccAddress = 'collectios@damacproperties.com',
            ccAddress = 'collectios@damacproperties.com',
            Subject = listvalueToCheck[0], 
            TextBody = listvalueToCheck[0], 
            ParentId = caseObj.Id)); 
        insert newEmail;
        List<Case> caseList = [SELECT Id FROM Case];
        List<Id> caseIds = new List<Id>();
            for(Case thiscase : caseList) {
                caseIds.add(thiscase.Id);
            }
        //AssignCaseToFAMQueue  assignCase = new AssignCaseToFAMQueue();
        AssignCaseToFAMQueue.updateCases(caseIds);
        
    }
}