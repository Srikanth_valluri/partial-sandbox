@isTest
public class VirtualAccountTest {
    static testMethod void testVirtualAccountPositive() {
        Account objAccount = new Account();
        objAccount.Email__c = 'test@gmail.com';
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        Booking__c booking = new Booking__c();
        booking.Account__c = objAccount.Id;
        booking.Deal_SR__c = objDealSR.Id;
        insert booking;
        
        Booking_Unit__c bookingUnit = new Booking_Unit__c();
        bookingUnit.Registration_Status_Code__c = 'ZA';
        bookingUnit.Booking__c = booking.Id;
        bookingUnit.Registration_Id__c = '1234';
        insert bookingUnit;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = '/VirtualAccount';  // sample Endpoint
        req.httpMethod = 'PATCH';
        RestContext.request = req;
        RestContext.response = res;
        System.Test.startTest();
          VirtualAccount.VirtualAccount(bookingUnit.Registration_Id__c,objAccount.Id,'');
        System.Test.stopTest();
        
        String strExpectedReponse = '{"statusCode":"200","status":"Account Details Updated Successfully","errorMessage":""}';
        System.assertEquals(strExpectedReponse, RestContext.response.responseBody.toString());
        System.assertEquals(200, RestContext.response.statusCode);
    }
    
    static testMethod void testVirtualAccountNegative() {
        Account objAccount = new Account();
        objAccount.Email__c = 'test@gmail.com';
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = '/VirtualAccount';  // sample Endpoint
        req.httpMethod = 'PATCH';
        RestContext.request = req;
        RestContext.response = res;
        
        System.Test.startTest();
          VirtualAccount.VirtualAccount('1221', objAccount.Id, '');
        System.Test.stopTest();
        
        String strExpectedReponse = '{"statusCode":"400","status":"No Unit exists, please select correct unit.","errorMessage":""}';
        System.assertEquals(strExpectedReponse, RestContext.response.responseBody.toString());
    }
}