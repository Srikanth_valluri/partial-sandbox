/**************************************************************************************************
* Name               : InventoryShareRemoveBatch
* Test Class         : InventoryShareRemoveBatchSchedulerTest
* Description        : Batch class to remove the Share records related to Inventories
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR      DATE
* 1.0         QBurst      11/08/2020
**************************************************************************************************/
public class InventoryShareRemoveBatch implements Database.Batchable<sObject>{

    /*********************************************************************************************
    * @Description : Implementing the start method of batch interface, contains query.
    * @Params      : Database.BatchableContext
    * @Return      : Database.QueryLocator
    *********************************************************************************************/  
    public Database.QueryLocator start(Database.BatchableContext BC){
        Integer hourLimit = -2;
        if(Label.Inventory_Share_Remove_Hours != null && Label.Inventory_Share_Remove_Hours != ''){
            if(Label.Inventory_Share_Remove_Hours.isNumeric()){
                hourLimit = 0 - Integer.valueOf(Label.Inventory_Share_Remove_Hours);
            }
        }
        system.debug('hourLimit: ' + hourLimit);
        DateTime sharedTimeLimit = System.now().addHours(hourLimit);
        return Database.getQueryLocator([SELECT Id, Inventory__c, Inventory__r.status__c, Inventory_Share_Time__c,
                                                Inventory_Shared_to_RM__c, Inventory_Initial_Status__c, 
                                                Deal_Exception_Request__r.RM__c
                                         FROM Deal_Exception_Unit__c
                                         WHERE Inventory__c != NULL
                                             AND Deal_Exception_Request__c != NULL
                                             AND Deal_Exception_Request__r.RM__c != NULL
                                             AND Inventory_Shared_to_RM__c = TRUE
                                             AND Inventory__r.status__c = 'Released'
                                             AND Inventory_Initial_Status__c != NULL
                                             AND Inventory_Share_Time__c <: sharedTimeLimit
                                             ]); 
    }

    /*********************************************************************************************
    * @Description : Implementing the execute method of batch interface, contains the criteria.
    * @Params      : Database.BatchableContext, List<sObject>
    * @Return      : void
    *********************************************************************************************/
    public void execute(Database.BatchableContext BC, List<Deal_Exception_Unit__c> scope){
        InventoryShareRemovalController.removeShareDetails(scope);
    }

    /*********************************************************************************************
    * @Description : Implementing Finish method, to end an email after job completion.
    * @Params      : Database.BatchableContext
    * @Return      : void
    *********************************************************************************************/
    public void finish(Database.BatchableContext BC){
    }
}// End of class.