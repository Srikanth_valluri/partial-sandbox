/*
 * Description: Class used to delete the associated DDI and DDI Child records for Assignment process
 */

public class AssignmentDrawloopUtility {

    public static void deleteDDIRecords(List<Case> lstCases) {
        deleteDDI(lstCases);
    }

    /**
     * Method used to delete the DDI and DDI Child records
     */
    @InvocableMethod
    public static void deleteDDI(List<Case> lstCases) {
        Set<Id> setDDIIds = new Set<Id>();
        List<DDI__c> lstDDI = new List<DDI__c>();
        Set<Id> setCaseIds = new Set<Id>();
        List<DDI_Child_1__c> lstDDIChilds = new List<DDI_Child_1__c>();

        for (Case objCase : lstCases) {
            setCaseIds.add(objCase.Id);
            if (String.isNotBlank(objCase.DDI__c)) {
                setDDIIds.add(objCase.DDI__c);
            }
        }
        if (!setDDIIds.isEmpty()) {
            for (Id objDDIIds : setDDIIds) {
                DDI__c objDDI = new DDI__c(Id = objDDIIds);
                lstDDI.add(objDDI);
            }
        }
        if (!lstDDI.isEmpty()) {
            delete lstDDI;
        }

        lstDDIChilds = [SELECT Id FROM DDI_Child_1__c WHERE Case__c IN: setCaseIds];
        if (!lstDDIChilds.isEmpty()) {
            delete lstDDIChilds;
        }
    }

}