@isTest
public class HDAppInsertMoveInCaseTest {
    @isTest
    static void TestMoveInDraft() {

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;

        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Move_in_Request',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move In').getRecordTypeId(),
                                           Request_Type__c = 'Move In',
                                           Booking_Unit__c = bookingUnit.Id);
        insert fmCase;

        String requestJson =  '   {  '  + 
        '      "action": "draft",  '  + 
        '      "id": "",  '  + 
        '      "booking_unit_id": "'+bookingUnit.Id+'",  '  + 
        '      "move_in_date": "2020-07-29",  '  + 
        '      "move_in_type": "Using moving company",     '  + 
        '      "moving_company_name" : "ABC",  '  + 
        '      "moving_contractor_name" : "ABC contractor",  '  + 
        '      "moving_contractor_phone" : "021321",  '  + 
        '      "no_of_adults": "1",                                                    '  + 
        '       "no_of_children": "0",                                                  '  + 
        '       "is_person_with_spl_needs": false,                                      '  + 
        '       "is_having_pets": true,  '  + 
        '      "additional_members": [                                                 '  + 
        '          {  '  + 
        '              "id": "",                                                             '  + 
        '              "member_first_name": "Shubham",  '  + 
        '              "member_last_name": "Suryawanshi",  '  + 
        '              "member_gender": "Male",                                                '  + 
        '              "member_date_of_birth": "1996-07-15",  '  + 
        '              "member_nationality": "",                                           '  + 
        '              "member_passport_number": "123",  '  + 
        '              "member_emirates_id": "",  '  + 
        '              "member_mobile_number": "",  '  + 
        '              "member_email_address": ""  '  + 
        '          }  '  + 
        '      ],  '  + 
        '      "vehicle_details": [                                                    '  + 
        '          {  '  + 
        '              "id": "",                                                             '  + 
        '              "vehicle_number": "6176",  '  + 
        '              "vehicle_make": "Mahindra",  '  + 
        '              "vehicle_access_card_number" : "12345",  '  + 
        '              "vehicle_parking_slot_number" : "234"  '  + 
        '          }  '  + 
        '      ],  '  + 
        '      "emergency_contact_details": [                                          '  + 
        '          {  '  + 
        '            "id": "",                                                             '  + 
        '            "emergency_full_name": "Shubham Suryawanshi",  '  + 
        '            "emergency_relationship": "Test",                                          '  + 
        '            "emergency_mobile_number": "",  '  + 
        '            "emergency_email_address": ""  '  + 
        '          }  '  + 
        '      ]  '  + 
        '  }  ' ;

        Test.startTest();
            HDAppInsertMoveInCase.InsertMoveInCase(requestJson);
        Test.stopTest();
    }
    // covering only 6%
    @isTest
    static void TestMoveInSubmit() {

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;

        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Move_in_Request',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move In').getRecordTypeId(),
                                           Request_Type__c = 'Move In',
                                           Booking_Unit__c = bookingUnit.Id);
        insert fmCase;

        String requestJson =  '   {  '  + 
        '      "action": "submit",  '  + 
        '      "id": "'+fmCase.id+'",  '  + 
        '      "booking_unit_id": "'+bookingUnit.Id+'",  '  + 
        '      "move_in_date": "2020-07-29",  '  + 
        '      "move_in_type": "Using moving company",     '  + 
        '      "moving_company_name" : "ABC",  '  + 
        '      "moving_contractor_name" : "ABC contractor",  '  + 
        '      "moving_contractor_phone" : "021321",  '  + 
        '      "no_of_adults": "1",                                                    '  + 
        '       "no_of_children": "0",                                                  '  + 
        '       "is_person_with_spl_needs": false,                                      '  + 
        '       "is_having_pets": true,  '  + 
        '      "additional_members": [                                                 '  + 
        '          {  '  + 
        '              "id": "",                                                             '  + 
        '              "member_first_name": "Shubham",  '  + 
        '              "member_last_name": "Suryawanshi",  '  + 
        '              "member_gender": "Male",                                                '  + 
        '              "member_date_of_birth": "1996-07-15",  '  + 
        '              "member_nationality": "",                                           '  + 
        '              "member_passport_number": "123",  '  + 
        '              "member_emirates_id": "",  '  + 
        '              "member_mobile_number": "",  '  + 
        '              "member_email_address": ""  '  + 
        '          }  '  + 
        '      ],  '  + 
        '      "vehicle_details": [                                                    '  + 
        '          {  '  + 
        '              "id": "",                                                             '  + 
        '              "vehicle_number": "6176",  '  + 
        '              "vehicle_make": "Mahindra",  '  + 
        '              "vehicle_access_card_number" : "12345",  '  + 
        '              "vehicle_parking_slot_number" : "234"  '  + 
        '          }  '  + 
        '      ],  '  + 
        '      "emergency_contact_details": [                                          '  + 
        '          {  '  + 
        '            "id": "",                                                             '  + 
        '            "emergency_full_name": "Shubham Suryawanshi",  '  + 
        '            "emergency_relationship": "Test",                                          '  + 
        '            "emergency_mobile_number": "",  '  + 
        '            "emergency_email_address": ""  '  + 
        '          }  '  + 
        '      ]  '  + 
        '  }  ' ;

        Test.startTest();
            HDAppInsertMoveInCase.InsertMoveInCase(requestJson);
        Test.stopTest();
    }
    
    @isTest
    static void TestMoveInDraftWithActionNull() {

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;

        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Move_in_Request',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move In').getRecordTypeId(),
                                           Request_Type__c = 'Move In',
                                           Booking_Unit__c = bookingUnit.Id);
        insert fmCase;
        
        SR_Attachments__c objSRAttachment = new SR_Attachments__c();
        objSRAttachment.Name = 'test';
        objSRAttachment.FM_Case__c = fmCase.Id;
        insert objSRAttachment;

        String requestJson =  '   {  '  + 
        '      "action": "draft",  '  + 
        '      "id": "",  '  + 
        '      "booking_unit_id": "'+bookingUnit.Id+'",  '  + 
        '      "move_in_date": "2020-07-29",  '  + 
        '      "move_in_type": "Using moving company",     '  + 
        '      "moving_company_name" : "ABC",  '  + 
        '      "moving_contractor_name" : "ABC contractor",  '  + 
        '      "moving_contractor_phone" : "021321",  '  + 
        '      "no_of_adults": "1",                                                    '  + 
        '       "no_of_children": "0",                                                  '  + 
        '       "is_person_with_spl_needs": false,                                      '  + 
        '       "is_having_pets": true,  '  + 
        '      "additional_members": [                                                 '  + 
        '          {  '  + 
        '              "id": "",                                                             '  + 
        '              "member_first_name": "Shubham",  '  + 
        '              "member_last_name": "Suryawanshi",  '  + 
        '              "member_gender": "Male",                                                '  + 
        '              "member_date_of_birth": "1996-07-15",  '  + 
        '              "member_nationality": "",                                           '  + 
        '              "member_passport_number": "123",  '  + 
        '              "member_emirates_id": "",  '  + 
        '              "member_mobile_number": "",  '  + 
        '              "member_email_address": "shubhamsuryawanshi@test.com"  '  + 
        '          }  '  + 
        '      ],  '  + 
        '      "vehicle_details": [                                                    '  + 
        '          {  '  + 
        '              "id": "",                                                             '  + 
        '              "vehicle_number": "6176",  '  + 
        '              "vehicle_make": "Mahindra",  '  + 
        '              "vehicle_access_card_number" : "12345",  '  + 
        '              "vehicle_parking_slot_number" : "234"  '  + 
        '          }  '  + 
        '      ],  '  + 
        '      "emergency_contact_details": [                                          '  + 
        '          {  '  + 
        '            "id": "",                                                             '  + 
        '            "emergency_full_name": "Shubham Suryawanshi",  '  + 
        '            "emergency_relationship": "Test",                                          '  + 
        '            "emergency_mobile_number": "",  '  + 
        '            "emergency_email_address": ""  '  + 
        '          }  '  + 
        '      ]  '  + 
        '  }  ' ;

        Test.startTest();
            HDAppInsertMoveInCase.InsertMoveInCase(requestJson);
            HDAppInsertMoveInCase.VerifyMandatoryDocs(fmCase.Id);
        Test.stopTest();
    }

}