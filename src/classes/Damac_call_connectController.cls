// Class to open the call wrap up page
global without sharing class Damac_call_connectController {

    public string userExtension { get; set; }
    public Task calltask { get; set; }
    public Task followUpTask { get; set; }
    public Task meetingScheduledTask { get; set; }
    
    public String customMessage { get; set; }
    public String messageType { get; set; }    
    
    public Inquiry__c inq { get; set; }
    public String inqRecId { get; set; }
    public Wrapper wrapperObj { get; set; }
   
    public List <Task> callTaskList { get; set; } // It will store only one task record
    public Boolean enableFields { get; set; }
    
    public transient Map<String, List<String>> results { get; set; }
    public String callDetailsOutcome { get; set; }
    

    public Damac_call_connectController () {
        init ();
    }
    public Damac_call_connectController (ApexPages.StandardController stdController) {
        init ();
    }   
    // Method to insert follow up task from the page
    public void createFollowup () {
        followUpTask.WhatID = apexpages.currentpage().getparameters().get('InqId');
        if (followUpTask.ID != NULL)
            update followUpTask;    
        else
            insert followUpTask;
        customMessage = 'Followup call created';
        messageType = 'success';
    }
    // Init handler on page load
    public void init () {
        followUpTask = new Task ();
        meetingScheduledTask = new Task ();
        meetingScheduledTask.Subject = 'Meeting at Office';
        meetingScheduledTask.Activity_Type_3__c = 'Meeting at Office';
        enableFields = false;
        callTaskList = new List <Task > ();
        userExtension = ''; 
        calltask = new Task ();
        callDetailsOutcome = '';
        messageType = '';
        customMessage = '';
        
        inq = new Inquiry__c ();
        inqRecId = apexpages.currentpage().getparameters().get('InqId');
        results = new Map <String, List <String>> ();
        wrapperObj = new Wrapper ();
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get ('Task').getDescribe ().fields.getMap ();
        
        Schema.DescribeFieldResult theFieldResult = fieldMap.get('Activity_Outcome__c').getDescribe();
        Schema.sObjectField theField = theFieldResult.getSObjectField();
        
        Schema.DescribeFieldResult ctrlFieldResult = fieldMap.get('Activity_Type_3__c').getDescribe();
        Schema.sObjectField ctrlField = ctrlFieldResult.getSObjectField();
        
        
        results = DependentPickListFields.getDependentOptionsImpl(theField, ctrlField);
        if (results.containsKey ('Call-Inbound'))
            results.put ('Inbound', results.get ('Call-Inbound'));
        if (results.containsKey ('Call - Outbound'))
            results.put ('Outbound', results.get ('Call - Outbound'));
    }
    // To get the user extension
    public void getUserExtension () {
        userExtension = [SELECT Extension FROM User WHERE ID =: UserInfo.getUserID ()].Extension;        
    }
    // To update inquiry details, task fields when call events came to salesforce
    @remoteAction
    global static callDetails checkDetails (String customerCalId, String callId, String userExt, String inqID, String direction, 
    String status, String calledNumber) {
        Inquiry__c inq = new Inquiry__c ();
        if (direction == 'Outbound') {
        //try {
            String phoneNumber = Damac_PhoneEncrypt.decryptPhoneNumber (customerCalId);
            System.Debug (phoneNumber);
            User u = new User ();
            u = [SELECT ID FROM User WHERE Extension =: userExt ];
            inq = [SELECT ID, OwnerId,Call_ID__c FROM Inquiry__c WHERE OwnerID =:u.ID AND Mobile_Phone__c =: phoneNumber LIMIT 1];
            inqID = inq.id;
        //}catch (Exception e) {}
        }
        Boolean isNew = false;
        if (inqID != '' && inqID != NULL) {
            inq.Call_ID__c = callID;
            //inq.OwnerId = UserInfo.getUserID ();
            inq.ID = inqID;
            Update inq;
        } else {
            inq.Call_ID__c = callID;
            isNew = true;
            Id preInquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
            inq.RecordTypeId = preInquiryRecordTypeId;
            inq.Is_New_Waybeo__c = TRUE;
            insert inq;
            inqId = inq.Id;
        }
        
        Task t = new Task ();
        inq = [ SELECT Mobile_phone__c, Campaign__c FROM Inquiry__c WHERE ID =:inqId];
        JO_Campaign_Virtual_Number__c vNumber = new JO_Campaign_Virtual_Number__c ();          
        try {
            t = [SELECT ID, sql_ID__c FROM Task WHERE SQL_Id__c =: callID];

            vNumber = [SELECT Related_Campaign__c, Related_Virtual_Number__r.Waybeo_Called_Number__c
                     FROM JO_Campaign_Virtual_Number__c WHERE Related_Campaign__c =: inq.Campaign__c LIMIT 1];
                        
        }
        catch (Exception e) {}
        t.Sql_Id__c = callID;
        t.whatID = inq.ID;
        t.Calling_Number__c = inq.Mobile_phone__c;
        if (vNumber.iD != NULL)
            t.Campaign_Number__c = vNumber.Related_Virtual_Number__r.Waybeo_Called_Number__c;
        if (direction == 'Inbound') {
            //t.Activity_Type_3__c = 'Call-Inbound';
            t.Activity_Type_3__c = 'Inbound';
            t.Subject = 'Call-Inbound';
            t.Campaign_Number__c = calledNumber;
        }
        if (direction == 'Outbound') {
            //t.Activity_Type_3__c = 'Call - Outbound';
            t.Activity_Type_3__c = 'Outbound';
            t.Subject = 'Call - Outbound';
            t.Campaign_Number__c = calledNumber;
        }
        if (status == 'Ringing') {
            //t.Activity_Type_3__c = 'Call-Outbound Attempted';
            t.Activity_Type_3__c = 'Outbound';
            t.Subject = 'Call-Outbound Attempted';
        }
        System.Debug ('==='+callID);
        System.Debug ('==='+t.sql_id__c);
        string msg = '';
        try {
            upsert t Sql_Id__c;
            msg = t.id;
        } catch (Exception e) {
            msg = e.getMessage ();
            msg = msg.substringBetween ('id: ', ':');
            System.Debug ('==='+msg);
            if (!msg.startsWith ('00T')) {
                msg = '';
            } else {
                if (direction == 'Outbound') {
                    
                    t = new Task ();
                    t.id = msg;
                    if (vNumber.iD != NULL)
                        t.Campaign_Number__c = vNumber.Related_Virtual_Number__r.Waybeo_Called_Number__c;
                    t.Calling_Number__c = inq.Mobile_phone__c;
                    t.Campaign_Number__c = calledNumber;
                    t.whatId = inq.id;
                    t.status = 'Not Started';
                    t.Activity_Type_3__c = 'Call - Outbound';
                    t.Subject = 'Call - Outbound';
                    update t;
                }
            }
        }
        /* Creating Waybeo User which restrict him to get another call when he is on the call */
        Waybeo_CTI_User__c rec = new Waybeo_CTI_User__c ();
        try {
            rec = [SELECT User__c FROM Waybeo_CTI_User__c WHERE User__c  =: UserInfo.getUserId ()];
        }
        catch (Exception e) {}
        rec.User__c = UserInfo.getUserId ();
        if (rec.ID == NULL)
            insert rec;
        else
            update rec;
            
        callDetails details = new callDetails ();
        details.inquiryID = inq.ID;
        details.taskID = msg;
        details.isNew = isNew;
        return details;
    } 
    global class callDetails {
        public ID inquiryID;
        public ID taskID;
        public Boolean isNew;
    }
    public class Wrapper{
        
        public List<Task> taskList {get; set;}
        public List<String> fieldLabelList {get; set;}
        public List<String> fieldList {get; set;}
        public Wrapper (){
            taskList = new List <Task> ();
            fieldLabelList = new List <String> ();
            fieldList = new List <String> ();
        }
    }
    // Utility method to get all fields related to object
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null)
            return fields;
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            fields += sfield.getDescribe().getName ()+ ', ';
        }
        return fields.removeEnd(', '); 
    }
    // Method to display task details and inquiry details on page
    public void displayDetails () {
        wrapperObj = new Wrapper ();
        String inquiryId = apexpages.currentpage().getparameters().get('InqId');
        String taskID = apexpages.currentpage().getparameters().get('tID');
        String direction = apexpages.currentpage().getparameters().get('dir');
        
        init ();
        String query = 'SELECT SQL_ID__c, ';
        for(Schema.FieldSetMember f : SObjectType.Task.FieldSets.CustomTaskList.getFields()){
            wrapperObj.fieldList.add(f.getFieldPath());
            query += f.getFieldPath() + ', ';
        }
        query = query.removeEND (', ');
        query += ' FROM Task WHERE ID =: taskID '; 
        callTask = Database.query (query);
        if (direction == 'Inbound') {
            callTask.Activity_Type_3__c = 'Call-Inbound';
            callTask.Subject = 'Call-Inbound';
        }
        callTaskList.add (calltask);
        System.Debug (callTaskList.size () );
        inq = Database.query ('SELECT '+getAllFields ('Inquiry__c')+' FROM Inquiry__c WHERE ID =: inquiryId');
        UserRecordAccess  access = new UserRecordAccess ();
        access = [SELECT RecordId, HasReadAccess FROM UserRecordAccess 
                            WHERE UserId=:UserInfo.getUserId()
                            AND RecordId = :inquiryId];
        Boolean readAccess = access.HasReadAccess;
        if (readAccess) {
            enableFields = inq.Is_New_Waybeo__c;
            String dialingListId = apexpages.currentpage().getparameters().get('did');
    
            String enableFieldParam = apexpages.currentpage().getparameters().get('enableFields');
            
            if (enableFieldParam == 'true')
                enableFields = TRUE;
            if (dialingListId != NULL && dialingListId != '') {
                enableFields = TRUE;
            }
            
            //getPendingTasks ();
        }
    }
    // Method to update Task details from page
    public void updateTask () {
        System.Debug (callDetailsOutcome);
        
        if (callTask.Id != NULL) {
            if (callDetailsOutcome != NULL && callDetailsOutcome != '') {
                callTask.Status = 'Completed';
                callTask.Activity_Outcome__c = callDetailsOutcome;
                update callTask;
                customMessage = 'Wrapup Completed ';
                messageType = 'success';
                Waybeo_CTI_User__c rec = new Waybeo_CTI_User__c ();
                try {
                    rec = [SELECT User__c FROM Waybeo_CTI_User__c WHERE User__c  =: UserInfo.getUserId ()];
                }
                catch (Exception e) {}
                rec.User__c = UserInfo.getUserId ();
                if (rec.ID == NULL)
                    insert rec;
                else
                    update rec;
                    
                //getPendingTasks ();
            } else {
                customMessage = 'Outcome is required.';
                messageType = 'error';
            }
        } else {
            customMessage = 'Call id is missing to update.';
            messageType = 'error';
        }
        
    }
    // to clear page messages
    public void clearValues () 
    {
        customMessage = '';
        messageType = '';
        
    }
    // Method to update Inquiry details
    public void updateInquiry () {
        try {
            UpdateTask ();
            
            inq.Is_New_Waybeo__c = false;
            inq.OwnerId = Userinfo.getUserid ();
            //update inq;
            
            String dialingListId = apexpages.currentpage().getparameters().get('did');
            if (dialingListId != NULL && dialingListId != '') {
                Dialing_List__c dialList = new Dialing_List__c ();
                dialList.id = dialingListId;
                dialList.call_back_Date_Time__c = inq.call_back_Date_Time__c;
                //if (inq.call_back_Date_Time__c != NULL)
                  //  diallist.Status__c = NULL;
                dialList.Wrap_up_Pending__c= False;
                update dialList;
            }
            customMessage += ' and Inquiry Details updated';
            if (messageType != 'error')
                messageType = 'success';
        }
        catch (Exception e) {
            customMessage = 'Error Occured at line number'+e.getLineNumber ()+' '+e.getMessage ();
            messageType = 'error';
        }
    }
    
    public List<SelectOption> getLocation(){
        
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Inquiry__c.Interested_Location__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        //ple.sort();
        for( Schema.PicklistEntry f : ple) {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }
    public List<SelectOption> getDevelopement(){
        
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Inquiry__c.Interested_Development__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        //ple.sort();
        for( Schema.PicklistEntry f : ple) {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }
    
    public void createMeetingFollowup () {
        try {
        
        ID inqId = apexpages.currentpage().getparameters().get('InqId');
        if (inq.Sweep_Inquiry__c)        
            meetingScheduledTask.is_Meeting_Scheduled__c = true;
        meetingScheduledTask.WhatID = inqId;
        if (inq.Meeting_Due_Date__c != NULL)
            meetingScheduledTask.ActivityDate = Date.ValueOf (inq.Meeting_Due_Date__c);
        if (meetingScheduledTask.ID != NULL)
            update meetingScheduledTask;    
        else
            insert meetingScheduledTask;
        inq.Id = apexpages.currentpage().getparameters().get('InqId');
        if (inq.Sweep_Inquiry__c)        
            inq.is_Meeting_Scheduled__c = true;
        if (inq.Interested_Location__c != NULL) {
            String location = inq.Interested_Location__c;
            if (location.contains(', ')) {
                location = location.removeStart('[').removeEnd(']').replaceAll(', ', ';');
            } else {
                location = location.removeStart('[').removeEnd(']');
            }
            System.Debug(location);
            inq.Interested_Location__c = location;
        }
        if (inq.Interested_Development__c != NULL) {
            String location = inq.Interested_Development__c;
            if (location.contains(', ')) {
                location = location.removeStart('[').removeEnd(']').replaceAll(', ', ';');
            } else {
                location = location.removeStart('[').removeEnd(']');
            }
            inq.Interested_Development__c = location;
        }
        update inq;
        customMessage = 'Meeting Scheduled';
        messageType = 'success';
        }
        catch (Exception e) {
            messageType = 'error';
            customMessage = e.getMessage ();
        }
    }
}