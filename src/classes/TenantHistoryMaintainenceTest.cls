@isTest
private without sharing class TenantHistoryMaintainenceTest {
    static testMethod void testMethodOne() {
        
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
       
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Nationality__c ='Pakistani';
        fmCaseObj.Booking_Unit__c = buIns.Id ;
        fmCaseObj.Tenant__c = acctIns.Id ;
        fmCaseObj.Account__c = acctIns.Id ;
        fmCaseObj.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId();
        
        
        FM_Case__c fmCaseObj1 = new FM_Case__c();
        fmCaseObj1.Issue_Date__c=Date.today();
        fmCaseObj1.Contact_person__c='test';
        fmCaseObj1.Description__c='test';
        fmCaseObj1.Contact_person_contractor__c='test';
        fmCaseObj1.Nationality__c ='Pakistani';
        fmCaseObj1.Booking_Unit__c = buIns.Id ;
        fmCaseObj1.Tenant__c = acctIns.Id ;
        fmCaseObj1.Account__c = acctIns.Id ;
        fmCaseObj1.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId();
        
        
        test.startTest();
        
            TenantHistoryMaintainence.reEvaluateTenantHistory( new list<FM_Case__c>{ fmCaseObj } );
            TenantHistoryMaintainence.reEvaluateTenantHistory( new list<FM_Case__c>{ fmCaseObj1 } );
            //TenantHistoryMaintainence.getExistingTenantHistory( acctIns.Id, acctIns.Id, acctIns.Id, false );
            //TenantHistoryMaintainence.getExistingTenantHistory( NULL, NULL, NULL, false );
        test.stopTest();
    }
}