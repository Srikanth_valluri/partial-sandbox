/**************************************************************************************************
* Name               : InvRestrictionExtNotificationScheduler
* Test Class         : InvRestrictionExtNotificationTest
* Description        : Scheduler class for InvRestrictionExtNotificationBatch class.
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst        06/10/2020      Initial Draft.
**************************************************************************************************/
public with sharing class InvRestrictionExtNotificationScheduler implements Schedulable{

    public void execute(SchedulableContext SC) {
        InvRestrictionExtNotificationBatch batchObject = new InvRestrictionExtNotificationBatch();
        Database.executeBatch(batchObject, 10); 
    }
}