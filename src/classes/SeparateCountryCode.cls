// Created By :  Naresh 
//modified by : pratiksha Narvekar logic change
public class SeparateCountryCode{

 public static String  getCountryCode(String PhoneNumber,string varCountryCode){
        String CountryCode;
        List<String> countryNames = new List<String>();
        String mobilewithcountrycode;
        if(PhoneNumber != '' && PhoneNumber != null && varCountryCode !='' && varCountryCode != null ){  
             CountryCode= varCountryCode.substringAfter(': ').trim();
             if((PhoneNumber.startsWithIgnoreCase(CountryCode))){
               mobilewithcountrycode = PhoneNumber.substringAfter(CountryCode).trim();
             }
             else{
             mobilewithcountrycode = PhoneNumber;
             }
        }
        System.debug('...mobilewithcountrycode ...'+mobilewithcountrycode);
        return mobilewithcountrycode ;
 }
  public static String  concatCountryCode(String PhoneNumber,string varCountryCode){
        String CountryCode;
        List<String> countryNames = new List<String>();
        String mobilewithcountrycode;
        if(PhoneNumber != '' && PhoneNumber != null && varCountryCode !='' && varCountryCode != null ){  
             CountryCode= varCountryCode.substringAfter(': ').trim();
             if((PhoneNumber.startsWithIgnoreCase(CountryCode))){
               mobilewithcountrycode =CountryCode + PhoneNumber.substringAfter(CountryCode).trim();
             }
             else{
             mobilewithcountrycode =CountryCode + PhoneNumber;
             }
        }
        System.debug('...mobilewithcountrycode ...'+mobilewithcountrycode);
        return mobilewithcountrycode ;
 }

}