/*
Description : to check user's delete access on the new step
*/
public with sharing class NewStepDeleteOverride_ctrl extends Describe_Sobject_Access{
    
    public NewStepDeleteOverride_ctrl(ApexPages.StandardController controller){
        sObjectName = 'New_Step';
        getAccess();
    }
}