/**************************************************************************************************
* Name               : createAttachmentsForPlanBatch                                             *
* Description        : To Create Plan URL from the Inventory record                               *
* Created Date       : 24/04/2018                                                                 *
* Created By         : DAMAC IT                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR          DATE          COMMENTS                                              *
* 1.0         Srikanth V  24/04/2018    Initial Draft                                         *
**************************************************************************************************/
global class createAttachmentsForPlanBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {

    public String querystr;
    public Set <ID> InvIds;
 
    public createAttachmentsForPlanBatch (Set <ID> inventoryIds) {
        InvIds = new Set <ID> ();
        InvIds = inventoryIds;
        querystr =  'Select ID, Name ,Unit_Plan__c,plot_plan__c,floor_plan__c from inventory__c where status__c= \'Released\'';
        
        if (invIds != NULL) {
            if (InvIds.size () > 0) {
                queryStr += ' AND ID IN : InvIds ';
            }
        }
    }
 
    global Database.QueryLocator start (Database.BatchableContext BC) {
        return Database.getQueryLocator(querystr);
    }

     global void execute(Database.BatchableContext BC, List<inventory__c> scope) { 
        
        Damac_Constants.Skip_AttachmentTrigger_SMS = true;
        List <Attachment> existingAttachmentList = new List <Attachment> ();
        List <ContentVersion> contentVersionList = new List <ContentVersion> ();
        HttpResponse unitPlanResponse = new HttpResponse ();
        HttpResponse floorPlanResponse = new HttpResponse ();
        HttpResponse plotPlanResponse = new HttpResponse ();
        
        Inventory__c inv = new Inventory__c ();
        inv = scope[0];
        if (inv.unit_plan__c != null) {
            
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setTimeOut (120000);
            req.setEndpoint (inv.unit_plan__c);
            req.setMethod ('GET');            
            unitPlanResponse = h.send(req);
            
        } 
        // Floor
        if (inv.floor_plan__c != null) {
            
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setTimeOut (120000);
            req.setEndpoint (inv.floor_plan__c);
            req.setMethod('GET');            
            floorPlanResponse = h.send(req);
            
        } 
        
        //Plot Plan
        // Floor
        if (inv.plot_plan__c != null) {            
                        
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setTimeOut (120000);
            req.setEndpoint (inv.plot_plan__c);
            req.setMethod ('GET');            
            plotPlanResponse = h.send(req);
        }
        if (unitPlanResponse.getStatusCode () == 200) {
            existingAttachmentList = [SELECT ID FROM Attachment WHERE Name LIKE 'DAMAC_UP_%' AND ParentID =: inv.ID];
            if (existingAttachmentList.size () > 0)
                DELETE existingAttachmentList;
            
            List<ID> invDocuments = new List<ID> ();
            for (ContentDocumentLink link :[SELECT ContentDocumentId FROM COntentDocumentLink WHERE LinkedEntityId =: inv.Id]) {
                invDocuments.add(link.ContentDocumentId);
            }            
            
            contentVersionList = [SELECT ID, ContentDocumentId FROM contentVersion WHERE Title LIKE 'DAMAC_UP_%' AND ContentDocumentId IN: invDocuments];
            if (contentVersionList.size() > 0) {
                Set <ID> recIds = new Set<ID>();
                for (ContentVersion recId : contentVersionList) {
                    recIds.add(recId.contentDocumentId);
                }
                DELETE [SELECT ID FROM COntentDocument WHERE Id IN:recIds];
            }
                
            ContentVersion cont = new ContentVersion();
            
            cont.Title = 'DAMAC_UP_'+System.Now ()+'.jpg';
            cont.PathOnClient = 'DAMAC_UP_'+System.Now ()+'.jpg';
            if (!Test.isRunningTest())
                cont.VersionData = unitPlanResponse.getBodyAsBlob ();
            else
                cont.VersionData = Blob.valueOf('test');
            cont.Origin = 'C';                
            insert cont;
            
            ContentDistribution newDistribution = new ContentDistribution(
                ContentVersionId = cont.id, Name = 'External Link',
                PreferencesNotifyOnVisit = false);
            insert newDistribution;
            
            ContentDistribution distributionURL = [SELECT ContentDocumentId, ContentDownloadUrl
                                                   FROM ContentDistribution 
                                                   WHERE Id =: newDistribution.ID
                                                   order BY LastModifiedDate DESC];
                                                   
            COntentDocumentLink link = new COntentDocumentLink();
            link.ContentDocumentId = distributionURL.ContentDocumentId;
            link.ShareType = 'V';
            link.LinkedEntityId = inv.Id;
            insert link;
            
            /*
            //Create attachment under inventory     
            attachment a = new attachment();
            a.parentid = inv.id;
            a.body = unitPlanResponse.getBodyAsBlob ();                
            a.contenttype='image/jpeg';
            a.Name = 'DAMAC_UP_'+System.Now ()+'.jpg';
            insert a;
            */
            inv.Unit_Plan_SF_Link__c = distributionURL.ContentDownloadUrl;
        }
        if (PlotPlanResponse.getStatusCode () == 200) {
            existingAttachmentList = [SELECT ID FROM Attachment WHERE Name LIKE 'DAMAC_PP_%' AND ParentID =: inv.ID];
            if (existingAttachmentList.size () > 0)
                DELETE existingAttachmentList;
                        
            /*
            //Create attachment under inventory     
            attachment a = new attachment();
            a.parentid = inv.id;
            a.body = PlotPlanResponse.getBodyAsBlob ();                
            a.contenttype='image/jpeg';
            a.Name = 'DAMAC_PP_'+System.Now ()+'.jpg';
            insert a;
            */
            List<ID> invDocuments = new List<ID> ();
            for (ContentDocumentLink link :[SELECT ContentDocumentId FROM COntentDocumentLink WHERE LinkedEntityId =: inv.Id]) {
                invDocuments.add(link.ContentDocumentId);
            }            
            
            contentVersionList = [SELECT ID, ContentDocumentId FROM contentVersion WHERE Title LIKE 'DAMAC_PP_%' AND ContentDocumentId IN: invDocuments];
            if (contentVersionList.size() > 0) {
                Set <ID> recIds = new Set<ID>();
                for (ContentVersion recId : contentVersionList) {
                    recIds.add(recId.contentDocumentId);
                }
                DELETE [SELECT ID FROM COntentDocument WHERE Id IN:recIds];
            }    
            ContentVersion cont = new ContentVersion();
            
            cont.Title = 'DAMAC_PP_'+System.Now ()+'.jpg';
            cont.PathOnClient = 'DAMAC_PP_'+System.Now ()+'.jpg';
            if (!Test.isRunningTest())
                cont.VersionData = plotPlanResponse.getBodyAsBlob ();
            else
                cont.VersionData = Blob.valueOf('test');
            cont.Origin = 'C';                
            insert cont;
            
            ContentDistribution newDistribution = new ContentDistribution(
                ContentVersionId = cont.id, Name = 'External Link',
                PreferencesNotifyOnVisit = false);
            insert newDistribution;
            
            ContentDistribution distributionURL = [SELECT ContentDocumentId, ContentDownloadUrl
                                                   FROM ContentDistribution 
                                                   WHERE Id =: newDistribution.ID
                                                   order BY LastModifiedDate DESC];
                                                   
            COntentDocumentLink link = new COntentDocumentLink();
            link.ContentDocumentId = distributionURL.ContentDocumentId;
            link.ShareType = 'V';
            link.LinkedEntityId = inv.Id;
            insert link;
            
            inv.plot_Plan_SF_Link__c = distributionURL.ContentDownloadUrl;
        }
        if (floorPlanResponse.getStatusCode () == 200) {
            existingAttachmentList = [SELECT ID FROM Attachment WHERE Name LIKE 'DAMAC_FP_%' AND ParentID =: inv.ID];
            if (existingAttachmentList.size () > 0)
                DELETE existingAttachmentList;
            
            /*
            //Create attachment under inventory     
            attachment a = new attachment();
            a.parentid = inv.id;
            a.body = floorPlanResponse.getBodyAsBlob ();                
            a.contenttype = 'image/jpeg';
            a.Name = 'DAMAC_FP_'+System.Now ()+'.jpg';
            insert a;
            */

            List<ID> invDocuments = new List<ID> ();
            for (ContentDocumentLink link :[SELECT ContentDocumentId FROM COntentDocumentLink WHERE LinkedEntityId =: inv.Id]) {
                invDocuments.add(link.ContentDocumentId);
            }            
            
            contentVersionList = [SELECT ID, ContentDocumentId FROM contentVersion WHERE Title LIKE 'DAMAC_FP_%' AND ContentDocumentId IN: invDocuments];
            if (contentVersionList.size() > 0) {
                Set <ID> recIds = new Set<ID>();
                for (ContentVersion recId : contentVersionList) {
                    recIds.add(recId.contentDocumentId);
                }
                DELETE [SELECT ID FROM COntentDocument WHERE Id IN:recIds];
            }
                
            ContentVersion cont = new ContentVersion();
            
            cont.Title = 'DAMAC_FP_'+System.Now ()+'.jpg';
            cont.PathOnClient = 'DAMAC_FP_'+System.Now ()+'.jpg';
            if (!Test.isRunningTest())
                cont.VersionData = floorPlanResponse.getBodyAsBlob ();
            else
                cont.VersionData = Blob.valueOf('test');
            cont.Origin = 'C';                
            insert cont;
            
            ContentDistribution newDistribution = new ContentDistribution(
                ContentVersionId = cont.id, Name = 'External Link',
                PreferencesNotifyOnVisit = false);
            insert newDistribution;
            
            ContentDistribution distributionURL = [SELECT ContentDocumentId, ContentDownloadUrl
                                                   FROM ContentDistribution 
                                                   WHERE Id =: newDistribution.ID
                                                   order BY LastModifiedDate DESC];
                                                   
            COntentDocumentLink link = new COntentDocumentLink();
            link.ContentDocumentId = distributionURL.ContentDocumentId;
            link.ShareType = 'V';
            link.LinkedEntityId = inv.Id;
            insert link;
            
            
            inv.floor_Plan_SF_Link__c = distributionURL.ContentDownloadUrl;
        }
            
        System.Debug (inv);
        DAMAC_Constants.skip_InventoryTrigger = true;
        Update inv;
    }   

    global void finish(Database.BatchableContext BC){    
    }
}