@isTest
public class DAMAC_PromoterStandChecklist_Test{
    testMethod static void promoterSCL(){
        
        Campaign__c camp = new Campaign__c ();
        camp.Campaign_Name__c = 'Test';
        camp.End_Date__c = Date.today();
        camp.Marketing_End_Date__c = Date.today();
        camp.Marketing_Start_Date__c = Date.today();
        camp.Start_Date__c = Date.today();
        insert camp;
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABCXYZ@test.com');
        pcUser.Stand_Id__c = camp.id;
        insert pcUser;
        
        System.runAs(pcUser) {
            System.Test.startTest();
                DAMAC_PromoterStandChecklistController promt = new DAMAC_PromoterStandChecklistController();
                List<SelectOption> pro = promt.getOptions();
                List<SelectOption> conditions = promt.getConditions();
                promt.standChecklist.IT_Equipments__c = '[ , ]';
                promt.standChecklist.Stand_Condition__c = '[, ]';
                DAMAC_PromoterStandChecklistController.getUserDetails('sri');
                promt.insertStandchecklist();
            System.Test.stopTest();
        }
   }
}