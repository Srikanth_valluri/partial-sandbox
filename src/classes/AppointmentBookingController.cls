/**********************************************************************************************************************
Description: This is the controller class for AppointmentBooking VF component.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   18-10-2020      | Shubham Suryawanshi | Uncommented Booking Unit check in createAppointment() method
***********************************************************************************************************************/


public without sharing class AppointmentBookingController extends AppointmentSelectionHandler {

    public          Account                     objAcc                              {get;set;}
    public          String                      dateStr                             {get;set;}
    public          string                      strSelectedAccount                  {get;set;}
    public          string                      strSelectedProcess                  {get;set;}
    public          String                      strSelectedSubProcess               {get;set;}
    public          String                      strSelectedBookingUnits             {get;set;}
    public          String                      strSelectedDate                     {get;set;}
    public          string                      strSelectedSlot                     {get;set;}
    public          String                      visitPurpose                        {get;set;}
    public          String                      strSource                           {get;set;}
    public          Boolean                     processSelected                     {get;set;}
    public          Boolean                     subProcessSelected                  {get;set;}
    public          Boolean                     unitSelected                        {get;set;}
    public          List<SelectOption>          lstSRs                              {get;set;}
    public          List<SelectOption>          lstSubProcessOptions                {get;set;}
    public          List<SelectOption>          lstBookingUnitOptions               {get;set;}
    public          List<SelectOption>          appointmntObjLst                    {get;set;}
    public          Boolean                     disableBooking                      {get;set;}
    public          Boolean                     shwKeyHOmsg                         {get;set;}

    public          String                      strCallingId;
    public          Calling_List__c             objCalling;
    public          list<AppointmentWrapper>    lstAppointmentWrapper;
    public          list<AppointmentWrapper>    lstAppWrapper;
    public          list<AppointmentWrapper>    lstWrapper;

    public   transient   List<String>           appointmntSlotSet;

    private  Id  devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName()
                                        .get('Appointment Scheduling').getRecordTypeId();

    public AppointmentBookingController() {
        if (CustomerCommunityUtils.isCurrentView('appointmentview')) {

            disableBooking = false;
            shwKeyHOmsg = false;

            initializeVariables();
            fetchPageParams();
            processSelected = String.isNotBlank(strSelectedProcess);
            subProcessSelected = String.isNotBlank(strSelectedSubProcess);
            unitSelected = String.isNotBlank(strSelectedBookingUnits);
            if (unitSelected) {
                mapBookingUnit.put(strSelectedBookingUnits, new Booking_Unit__c());
            }
            strSelectedAccount = CustomerCommunityUtils.customerAccountId;
            System.debug('--- strSelectedAccount ---'+strSelectedAccount );
            if (String.isNotBlank(strSelectedAccount)) {

                //Added the check for FAM/CAUTION == 'FAM', then disable the Appointment booking - Shubham 13/05/2020
                Account account = [SELECT id, FAM_CAUTION__c FROM Account WHERE id=:strSelectedAccount];
                System.debug('account: ' + account);
                if(String.isNotBlank(account.FAM_CAUTION__c) && account.FAM_CAUTION__c.equalsIgnoreCase('FAM')) {
                    disableBooking = true;
                }

                initMethod();
                system.debug('== strSelectedProcess =='+strSelectedProcess );
            }
        }
        System.debug('--- strSelectedProcess --- : '+strSelectedProcess);
        System.debug('--- strSelectedSubProcess --- : '+strSelectedSubProcess);
    }

    private void initializeVariables() {
        mapBookingUnit = new Map<Id, Booking_Unit__c>();
        strSelectedAccount = '';
        lstAppointmentWrapper = new list<AppointmentWrapper>();
        lstAppWrapper = new list<AppointmentWrapper>();
        lstWrapper = new list<AppointmentWrapper>();
        strSource = 'Portal';
        lstBookingUnitOptions = new List<SelectOption>();
        lstSRs = new List<SelectOption>();
        lstSubProcessOptions = new List<SelectOption>();
        appointmntObjLst = new List<SelectOption>();
        appointmntSlotSet = new List<String>();
    }

    private void fetchPageParams() {
        Map<String, String> pageParams = ApexPages.currentPage().getParameters();
        strSelectedProcess      = pageParams.get('process');
        strSelectedSubProcess   = pageParams.get('subProcess');
        strSelectedBookingUnits = pageParams.get('unitId');
    }

     public String getName() {
        System.debug('--getName strSelectedAccount ---'+strSelectedAccount );
        return strSelectedAccount;
    }

    public void initMethod() {
        objAcc = new Account();
        System.debug('--- strSelectedAccount --- : '+strSelectedAccount);
        if(string.isNotBlank(strSelectedAccount)) {
            objAcc = [ SELECT name
                              , Primary_Language__c
                              , IsPersonAccount
                              , Email__pc
                              , Email__c
                      FROM Account
                      WHERE id = :strSelectedAccount ];
        }
        System.debug('--- objAcc --- : '+objAcc);

        List<Active_Process_on_Portal_Meta__mdt> lstActiveProcess =
                                    new List<Active_Process_on_Portal_Meta__mdt>();
        lstActiveProcess = [ SELECT id
                                   , MasterLabel
                                   , DeveloperName
                                   , Available_on_Portal__c
                                   , CRE_MasterLabel__c
                             FROM Active_Process_on_Portal_Meta__mdt
                             WHERE Available_on_Portal__c = true ];
        lstSRs = new List<SelectOption>();
        lstSRs.add(new selectOption('', '--None--'));
        for(Active_Process_on_Portal_Meta__mdt obj : lstActiveProcess) {
            if(String.isNotBlank(obj.CRE_MasterLabel__c)) {
                lstSRs.add(new selectoption(obj.CRE_MasterLabel__c,obj.CRE_MasterLabel__c));
            }
        }
        lstSRs.sort();

        List<Booking_Unit__c> lstBookingUnit = new List<Booking_Unit__c>();
        lstBookingUnit = [SELECT id
                                 ,name
                                 , Unit_Name__c
                                 , Registration_ID__c
                                 , Dispute_Flag__c
                                 , Handover_Notice_Sent_Date__c
                                 , EHO_Notice_Sent__c
                                 , Okay_to_release_keys__c
                                 , Pre_HandOver__c
                          FROM Booking_Unit__c
                          WHERE Booking__r.Account__c =: strSelectedAccount
                          AND Registration_ID__c != NULL
                          AND Unit_Name__c != NULL
        ];
        lstBookingUnitOptions = new List<SelectOption>();
        if(lstBookingUnit.Size()>0){
            lstBookingUnitOptions.add(new SelectOption('','--None--' ));
            for(Booking_Unit__c objBookingUnit : lstBookingUnit) {
                lstBookingUnitOptions.add(new SelectOption(objBookingUnit.id,objBookingUnit.Unit_Name__c ));
                mapBookingUnit.put(objBookingUnit.id,objBookingUnit);
            }
        }
        System.debug('-----mapBookingUnit---'+mapBookingUnit);
    }

    public PageReference getCallingLists(){
       //strSelectedDate = '';
       shwKeyHOmsg = false;
       system.debug('--strSelectedBookingUnits----: '+strSelectedBookingUnits);
       system.debug('--strSelectedProcess----: '+strSelectedProcess);

       //Added to block Key handover & Unit Viewing for Units with Staycae_RP__c = true (Shubham - 13/05/2020)
       List<Booking_Unit__c> lstBU = new List<Booking_Unit__c>();
       if(String.isNotBlank(strSelectedBookingUnits)) {
           lstBU = [SELECT id, Staycae_RP__c FROM Booking_Unit__c WHERE id =:strSelectedBookingUnits]; 
       }

       if(!lstBU.isEmpty() && lstBU.size() > 0) {
           
           strSelectedSubProcess = '';  //resetting the Sub-Process

           try {
                if(String.isNotBlank(strSelectedProcess)) {
                    appointmntObjLst = new List<SelectOption>();
                    appointmntObjLst.add(new selectOption('', '--None--'));
                    lstSubProcessOptions = new List<SelectOption>();
                    lstSubProcessOptions.add(new selectOption('', '--None--'));

                    List<Sub_Process_for_Portal__mdt> lstSubProcess = new List<Sub_Process_for_Portal__mdt>();
                    lstSubProcess = [ SELECT id
                                            , MasterLabel
                                            , DeveloperName
                                            , Active_Process_on_Portal__c
                                            , Available_on_Portal__c
                                      FROM Sub_Process_for_Portal__mdt
                                      WHERE Active_Process_on_Portal__r.CRE_MasterLabel__c =: strSelectedProcess
                                      ANd Available_on_Portal__c = true ];
                    System.debug('lstSubProcess: '+lstSubProcess);                    

                    if(lstSubProcess.Size()>0) {
                        for(Sub_Process_for_Portal__mdt objSubProcess : lstSubProcess ) {
                            if(lstBU[0].Staycae_RP__c && (objSubProcess.MasterLabel.containsIgnoreCase('Key Handover') || objSubProcess.MasterLabel.containsIgnoreCase('Unit Viewing'))) {
                                System.debug('objSubProcess: ' + objSubProcess);
                                shwKeyHOmsg = true;
                                continue;   // to bypass adding the unitViewing & KeyHandover selectOptions
                            }
                            lstSubProcessOptions.add(new selectoption(objSubProcess.MasterLabel
                                                                        ,objSubProcess.MasterLabel ));
                        }
                        System.debug('lstSubProcessOptions: ' + lstSubProcessOptions);
                    }
                }
            }
            catch(exception ex){
                ApexPages.addmessage(new ApexPages.message( ApexPages.severity.Error, ex.getMessage()));
                System.debug('----getLineNumber-----'+ex.getMessage()+'----'+ex.getLineNumber()) ;
            }
            
       } //END of Staycae_RP__c check

       System.debug('shwKeyHOmsg: ' + shwKeyHOmsg);

        system.debug('--strSelectedSubProcess----: '+strSelectedSubProcess);
        if((strSelectedProcess ==  '' || strSelectedProcess == NULL)
                && (strSelectedSubProcess == '' || strSelectedSubProcess == NULL)) {
            Map<String, String> pageParams = ApexPages.currentPage().getParameters();
            strSelectedProcess      = pageParams.get('process');
            strSelectedSubProcess   = pageParams.get('subProcess');
        }
        system.debug('strSelectedProcess=== : '+strSelectedProcess);
        system.debug('strSelectedSubProcess=== : '+strSelectedSubProcess);
        if((strSelectedProcess == '01-Handover' || strSelectedProcess == 'Handover')
                && (strSelectedSubProcess == 'Unit Viewing'
                    || strSelectedSubProcess == 'Key Handover'
                    || strSelectedSubProcess == 'Unit Viewing or Key Handover')) {
            strSelectedDate = String.valueOf( system.today() );
            System.debug('---strSelectedDate--- : '+strSelectedDate);
            strSelectedDate = formatDate( strSelectedDate ) ;
            return NULL;
        }
        return NULL;
    }

    public void initiationSlots() {
        System.debug('--- strSelectedAccount --- : '+strSelectedAccount);
        strSelectedBookingUnits = '';
        strSelectedDate = '';

        System.debug('--- strSelectedProcess --- : '+strSelectedProcess);
        try {
            if(String.isNotBlank(strSelectedProcess)) {
                appointmntObjLst = new List<SelectOption>();
                appointmntObjLst.add(new selectOption('', '--None--'));
                lstSubProcessOptions = new List<SelectOption>();
                lstSubProcessOptions.add(new selectOption('', '--None--'));

                List<Sub_Process_for_Portal__mdt> lstSubProcess = new List<Sub_Process_for_Portal__mdt>();
                lstSubProcess = [ SELECT id
                                        , MasterLabel
                                        , DeveloperName
                                        , Active_Process_on_Portal__c
                                        , Available_on_Portal__c
                                  FROM Sub_Process_for_Portal__mdt
                                  WHERE Active_Process_on_Portal__r.CRE_MasterLabel__c =: strSelectedProcess
                                  ANd Available_on_Portal__c = true ];
                System.debug('lstSubProcess: '+lstSubProcess);                    

                if(lstSubProcess.Size()>0) {
                    for(Sub_Process_for_Portal__mdt objSubProcess : lstSubProcess ) {
                        lstSubProcessOptions.add(new selectoption(objSubProcess.MasterLabel
                                                                    ,objSubProcess.MasterLabel ));
                    }
                }
            }
        }
        catch(exception ex){
            ApexPages.addmessage(new ApexPages.message( ApexPages.severity.Error, ex.getMessage()));
            System.debug('----getLineNumber-----'+ex.getMessage()+'----'+ex.getLineNumber()) ;
        }
    }

    @RemoteAction
    public static DAMACHolidaySettings__c holidayFunction(String datesel) {
        datesel = formatDate( datesel ) ;
        Date objDate = Date.valueOf( datesel );
        DAMACHolidaySettings__c holSettings = DAMACHolidaySettings__c.getValues('Holidays2018');
        System.debug('----holSettings------: '+holSettings);
        if(holSettings != null) {
            Date FromDate = holSettings.From__c;
            Boolean holiday = false;
            Date ToDate = holSettings.To__c;
            if(objDate >= FromDate && objDate <= ToDate) {
                holiday = true;
                return holSettings;
            }
        }
        return null;
    }

    public PageReference getAvailableAppointments() {
        appointmntObjLst.clear();
        appointmntObjLst = new List<SelectOption>();
        appointmntObjLst.add(new selectOption('', '--None--'));
        appointmntSlotSet = new List<String>();
        lstWrapper = new List<AppointmentWrapper>();
        strSelectedSlot = '';
        system.debug('--- strSelectedDate --- : '+strSelectedDate );
        if(String.isNotBlank(strSelectedDate)) {
            strSelectedDate = formatDate( strSelectedDate ) ;
        }
        system.debug('== Date3 =='+strSelectedDate );
        dateStr = strSelectedDate;
        System.debug('--- dateStr --- : '+dateStr);

        system.debug('--strSelectedAccount--:'+strSelectedAccount);
        system.debug('--strSelectedBookingUnits--:'+strSelectedBookingUnits);
        system.debug('--strSelectedProcess--:'+strSelectedProcess);
        system.debug('--strSelectedSubProcess--:'+strSelectedSubProcess);
        system.debug('--strSelectedDate--:'+strSelectedDate);
        if( String.isNotBlank( strSelectedAccount ) && String.isNotBlank(strSelectedBookingUnits)
                && String.isNotBlank(strSelectedProcess) && String.isNotBlank(strSelectedDate)) {
            lstAppointmentWrapper = availableSlots(strSelectedAccount,
                                                   strSelectedBookingUnits,
                                                   (strSelectedProcess.contains('-')
                                                        ? strSelectedProcess.substringAfter('-')
                                                        : strSelectedProcess),
                                                   (String.isNotBlank(strSelectedSubProcess)
                                                        ? strSelectedSubProcess : ''),
                                                   'Portal',
                                                   strSelectedDate,
                                                   false);
            System.debug('--- lstAppointmentWrapper --- : '+lstAppointmentWrapper);
        }

        if(lstAppointmentWrapper != NULL && lstAppointmentWrapper.size() > 0) {
            for(AppointmentWrapper obj : lstAppointmentWrapper) {
                if(obj.ErrorMessage != '' && String.isNotBlank(obj.ErrorMessage)) {
                    System.debug('--- obj.ErrorMessage --- : '+obj.ErrorMessage);
                    ApexPages.addmessage(new ApexPages.message(
                                ApexPages.severity.Error, obj.ErrorMessage));
                    return NULL;
                }
                System.debug('Appointment Calling Lists : '+ obj.objApp.Calling_List__r);
                System.debug('--- obj.ObjApp.Slots__c --- : '+obj.ObjApp.Slots__c);
                if(obj.objApp.Calling_List__r.isEmpty()) {
                    System.debug('Appointment Slot not Booked');
                    if(!appointmntSlotSet.contains(obj.ObjApp.Slots__c)) {
                        appointmntSlotSet.add(obj.ObjApp.Slots__c);
                        lstWrapper.add(obj);
                    }
                }
            }
        }
        System.debug('--- appointmntSlotSet --- : '+appointmntSlotSet);
        System.debug('--- lstWrapper --- : '+lstWrapper);

        if(appointmntSlotSet != null && !appointmntSlotSet.isEmpty()) {
            for(string setElement : appointmntSlotSet) {
                System.debug('>>>---setElement--- : '+setElement);
                if(setElement != NULL && String.isNotBlank(setElement)) {
                    appointmntObjLst.add(new selectoption(setElement,setElement));
                }
            }
        }
        appointmntObjLst.sort();
        System.debug('--- appointmntObjLst --- : '+appointmntObjLst);

        if( String.isNotBlank(strSelectedDate) ) {
            Date dt = Date.valueOf(strSelectedDate);
            strSelectedDate = DateTime.newInstance(dt.year(), dt.month(), dt.day()).format('dd-MM-yyyy');
        }

        if( String.isNotBlank(strSelectedProcess) ) {
            strSelectedProcess = (strSelectedProcess.contains('-')
                                    ? strSelectedProcess.substringAfter('-') : strSelectedProcess);
            System.debug('--- strSelectedProcess --- : '+strSelectedProcess);
        }
        return NULL;
    }

    public PageReference slotSelected () {
        System.debug('--- strSelectedSlot --- : '+strSelectedSlot);
        lstAppWrapper.clear();
        lstAppWrapper = new list<AppointmentWrapper>();
        System.debug('--- lstWrapper --- : '+lstWrapper);
        if(lstWrapper != NULL && lstWrapper.size() > 0) {
            for(AppointmentWrapper obj : lstWrapper) {
                if(obj.ObjApp.Slots__c == strSelectedSlot) {
                    obj.isSelected = true;
                    lstAppWrapper.add(obj);
                }
                else {
                    obj.isSelected = false;
                    lstAppWrapper.add(obj);
                }
            }
        }
        System.debug('--- lstAppWrapper --- : '+lstAppWrapper);
        return NULL;
    }

    public pageReference createAppointment() {

        if((strSelectedProcess == '01-Handover' || strSelectedProcess == 'Handover')
                && String.isNotBlank( strSelectedBookingUnits )
                && (strSelectedSubProcess == 'Unit Viewing'
                    || strSelectedSubProcess == 'Key Handover'
                    || strSelectedSubProcess == 'Unit Viewing or Key Handover')) {
            List<Calling_List__c> lstCl = [ Select Id
                                                  , RecordTypeId
                                                  , Appointment_Date__c
                                                  , Appointment_Status__c
                                                  , Calling_List__c
                                                  , Sub_Purpose__c
                                            From Calling_List__c
                                            Where RecordTypeId =: devRecordTypeId
                                            AND (Appointment_Status__c != 'Rejected'
                                                 AND Appointment_Status__c != 'Cancelled')
                                            AND Appointment_Date__c > TODAY
                                            AND Booking_Unit__c =: strSelectedBookingUnits     //Uncommented this for - 'Appointment already available for this Unit, please cancel existing appointment if want to reschedule same.' issue  
                                            AND Sub_Purpose__c = :strSelectedSubProcess
                                            AND Service_type__c =: (strSelectedProcess.contains('-')
                                                                    ? strSelectedProcess.substringAfter('-')
                                                                    : strSelectedProcess) ];
            System.debug('--- lstCl --- : '+lstCl);
            if (lstCl != null && lstCl.size() > 0) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,
                            'Appointment already available for this Unit, please cancel'
                            +' existing appointment if want to reschedule same.'));
                return NULL;
            }
        }
        else if( String.isNotBlank(strSelectedDate) && String.isNotBlank( strSelectedBookingUnits )
                            && String.isNotBlank( strSelectedProcess )
                            && strSelectedProcess != 'Handover') {
            Date objSelectedDate = Date.valueOf(formatDate(strSelectedDate));
            List<Calling_List__c> lstCL = [ SELECT Id
                                            FROM Calling_List__c
                                            WHERE RecordTypeId =: devRecordTypeId
                                            AND (Appointment_Status__c != 'Rejected'
                                                 AND Appointment_Status__c != 'Cancelled')
                                            AND Appointment_Date__c =: objSelectedDate
                                            AND Service_type__c =: (strSelectedProcess.contains('-') ? strSelectedProcess.substringAfter('-') : strSelectedProcess)
                                            AND Booking_Unit__c =: strSelectedBookingUnits ];
            System.debug('--- lstCL --- : '+lstCL);
            if (lstCL.size() > 0) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                                String.format(Label.Error_Message_For_Multiple_Calling_List,
                                new List<String>{strSelectedProcess, formatDate(strSelectedDate)})));
                return null;
            }
        }

        strSelectedDate = dateFormat(strSelectedDate);
        system.debug('--- strSelectedDate ---:'+strSelectedDate);

        System.debug('--- lstAppWrapper --- : '+lstAppWrapper);
        System.debug('---objAcc--- : '+objAcc);
        if( lstAppWrapper != NULL && lstAppWrapper.size() > 0
                    && String.isNotBlank(strSelectedBookingUnits)
                    && String.isNotBlank(strSelectedProcess) && String.isNotBlank(strSelectedDate)
                    && objAcc != NULL ) {
            createAppointments(lstAppWrapper,
                               (String.isNotBlank(visitPurpose) ? visitPurpose : ''),
                               strSelectedBookingUnits,
                               objAcc,
                               (String.isNotBlank(strSelectedSubProcess)
                                        ? strSelectedSubProcess : ''),
                               (strSelectedProcess.contains('-')
                                        ? strSelectedProcess.substringAfter('-')
                                        : strSelectedProcess),
                               strSelectedDate/*,
                               false*/);
        }
        String successMsg = 'Dear Customer, your appointment for ' + strSelectedSubProcess
                                + ' on '+ strSelectedDate +' has been requested at '
                                + strSelectedSlot +'. You will get notification on confirmation'
                                +' of your appointment';
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,successMsg));

        return NULL;
    }

    // method used to format date to yyyy-MM-dd
    public static String formatDate( String dt ) {
        list<String> lstDateComponents;
        system.debug('== Date1 =='+dt );
        if( dt.contains('/') ) {
            lstDateComponents = dt.split('/');
        }
        else if( dt.contains('-') ) {
            lstDateComponents = dt.split('-');
        }
        system.debug('lstDateComponents'+lstDateComponents);
        Date objDateToFormat;
        if(lstDateComponents[2].length() == 4 ) {
            objDateToFormat = date.newinstance( Integer.valueOf( lstDateComponents[2] ),
                                    Integer.valueOf( lstDateComponents[1] ),
                                    Integer.valueOf( lstDateComponents[0] ));
        }
        else {
            objDateToFormat = Date.valueOf(dt);
        }
        system.debug('== Date2 =='+objDateToFormat );
        system.debug('>>Datetime'+DateTime.newInstance(objDateToFormat.year(),
                            objDateToFormat.month(),objDateToFormat.day()).format('yyyy-MM-dd'));
        return DateTime.newInstance(objDateToFormat.year(),objDateToFormat.month(),
                            objDateToFormat.day()).format('yyyy-MM-dd');
    }

    // method used to format date as 'mm/dd/yyyy'
    public static String dateFormat(String strSelectedDate) {
        system.debug('--- strSelectedDate ---:'+strSelectedDate);
        if(String.isNotBlank(strSelectedDate)) {
            List<String> parts = new List<String>();
            if( strSelectedDate.contains('/') ) {
                parts = strSelectedDate.split('/');
            }
            else if( strSelectedDate.contains('-') ) {
                parts = strSelectedDate.split('-');
            }
            Integer intDate, intMonth, intYear;
            if (!parts.isEmpty() && parts.size() == 3){
                intDate = integer.valueOf(parts[0]);
                intMonth = integer.valueOf(parts[1]);
                intYear = integer.valueOf(parts[2]);
            }
            Datetime dt = Datetime.newInstance(intYear, intMonth, intDate);
            strSelectedDate = dt.format('MM/dd/yyyy');
        }
        system.debug('--- strSelectedDate ---:'+strSelectedDate);
        return strSelectedDate;
    }
}