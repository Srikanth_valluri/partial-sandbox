/*
 * Description - Test class for FM_ProcessSelectionExtension visualforce page
 *
 * Version        Date            Author            Description
 * 1.0          15/05/2018      Ashish Agarwal      Initial Draft
 */
 
@isTest
private class FM_ProcessSelectionExtensionTest  {

    static testMethod void testMethod1() {
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;
        
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;
        
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;
        
        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;
        
        Test.setCurrentPageReference( new PageReference('FMProcessSelectionPage') );
        ApexPages.currentPage().getParameters().put('SRType','Move_in_Request');
        FM_ProcessSelectionExtension  obj = new FM_ProcessSelectionExtension( new ApexPages.StandardController( new FM_Case__c() ) );
        
        obj.selectedAccountId = objAcc.Id ;
        obj.selectAccount() ;
        obj.fetchPOPDetails();
        obj.redirectToProcessPage();
        
        
    }
    static testMethod void testMethod2() {
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;
        
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;
        
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;
        
        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;

        FM_Case__c fmObj  = new FM_Case__c();
        fmObj.Booking_Unit__c = lstBookingUnit[0].id;
        fmObj.Expected_move_out_date__c = Date.Today();
        fmObj.Tenant__c = objAcc.id;
        fmObj.Account__c = objAcc.id;
        insert fmObj;
        Test.setCurrentPageReference( new PageReference('FMProcessSelectionPage') );
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        FM_ProcessSelectionExtension  obj = new FM_ProcessSelectionExtension( new ApexPages.StandardController( new FM_Case__c() ) );
       
    }
    static testMethod void testMethod3() {
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;
        
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;
        
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;
        
        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;

        FM_Case__c fmObj  = new FM_Case__c();
        fmObj.Booking_Unit__c = lstBookingUnit[2].id;
        fmObj.Expected_move_out_date__c = Date.Today();
        fmObj.Tenant__c = objAcc.id;
        fmObj.Account__c = objAcc.id;
        insert fmObj;


        Test.setCurrentPageReference( new PageReference('FMProcessSelectionPage') );
         ApexPages.currentPage().getParameters().put('AccountId',objAcc.id);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        ApexPages.currentPage().getParameters().put('SRType','Proof_of_Payment');
        FM_ProcessSelectionExtension  obj = new FM_ProcessSelectionExtension( new ApexPages.StandardController( new FM_Case__c() ) );
        obj.strSelectedUnit = String.valueOf(lstBookingUnit[2].id);
        obj.fetchEligibleSRTypes();
        obj.redirectToPOPProcessPage();
       
    }
}