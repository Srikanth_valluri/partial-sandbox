/**************************************************************************************************
* Name               : CC_UpdateRegnStatus
* Test Class         : DealCustomCodesTest
* Description        : This is the custom code class for invoking Registration Update webservice on status change
* Created Date       : 06/04/2017
* Created By         : NSI - Kaavya Raghuram
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE
* 1.0         NSI - Kaavya    20/04/2017
* 1.1         QBurst          02/04/2020         SOAP to REST change in Webservice call
**************************************************************************************************/
global without sharing class CC_UpdateRegnStatus implements NSIBPM.CustomCodeExecutable {
    public String evaluateCustomCode(NSIBPM__Service_Request__c SR, NSIBPM__Step__c step) {
        String retStr = 'Success';
        List<Id> bookingIds = new List<Id>();
        try{
            for(Booking__c bookings :[SELECT Id FROM Booking__c WHERE Deal_SR__c =: step.NSIBPM__SR__c]){
                bookingIds.add(bookings.id);
            }
            if(bookingIds.size() > 0){
                system.debug('#### invoking REST statusUpdate');
                DAMAC_IPMS_PARTY_CREATION.statusUpdate(bookingIds, '', step.Id); // 1.1
            }
        } catch (Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
        }
        return retStr;
    }
}