/*
The Class provides the Remote action Methods for the Booking Page - damacquickbook
Developed by DAMAC IT TEAM
Methods referred from the Previous Booking Class - PropertyBookingController.
1.1 - QBurst   13/01/2020   Method to add Reservation Form Name
*/

global class DAMAC_UNIT_BOOKING_REMOTE{

   /* public DAMAC_UNIT_BOOKING_REMOTE(DAMAC_logacall controller) {

    }*/


       
    public DAMAC_UNIT_BOOKING_REMOTE(ApexPages.StandardController controller) {   
        
    }
    
    /*
Updating the Token Attachment Name on the SR.
*/
    @remoteAction
    public static String updateFileName (String srId,String fileName) {
        NSIBPM__Service_Request__c srRecord = new NSIBPM__Service_Request__c ();
        srRecord.id = srId;
        srRecord.Token_Attachment_Name__c  = fileName;
        update srRecord;
        
        try {
            Attachment att = [SELECT ID FROM Attachment where Name =: fileName AND ParentId =: SrId Order By createdDate DESC LIMIT 1];
            
            Unit_documents__c uDoc = new Unit_Documents__c ();
            uDoc.Document_Name__c = 'Booking Token';
            uDoc.Service_Request__c = srid;
            uDoc.Sys_Doc_ID__c = att.id;
            uDoc.Status__c = 'Uploaded';
            insert uDoc;
            
            
        } catch (Exception e) {}
        
        return 'success';
    }
    
    @remoteAction
    public static String updatePassport (String srId,String fileName, String attachmentId) {
        NSIBPM__Service_Request__c srRecord = new NSIBPM__Service_Request__c ();
        srRecord.id = srId;
        srRecord.Passport_Name__c = fileName;
        update srRecord;
        
        try {
            
            Unit_documents__c uDoc = new Unit_Documents__c ();
            uDoc.Document_Name__c = 'Passport Copy';
            uDoc.Document_Type__c = 'Booking Docs';
            uDoc.Service_Request__c = srid;
            uDoc.Sys_Doc_ID__c = attachmentId;
            uDoc.Status__c = 'Uploaded';
            insert uDoc;
            
            
        } catch (Exception e) {}
        
        return 'success';
    }


    /* 1.1 updating Reservation Form Name */
 
    @remoteAction
    public static String updateReservationFormName ( String srId, String fileName){
        NSIBPM__Service_Request__c srRecord = new NSIBPM__Service_Request__c ();
        srRecord.id = srId;
        srRecord.Reservation_Form_Attachment_Name__c= fileName;
        update srRecord;
        return 'success';
    }    
     
    /*
Query the Individual Agnecy Details and return results on the Page
*/
    @RemoteAction 
    global static List<Account> getAgencyDetails(String searchKey){
        List<Account> agencyList = new List<Account>();
        List<Id> accountIdsList = new List<Id>();
        for(Agency_PC__c thisAgency : [SELECT Id, user__c, Agency__c
                                       FROM Agency_PC__c 
                                       WHERE user__c =: UserInfo.getUserId() AND 
                                       (Agency__r.RecordTypeId =: DamacUtility.getRecordTypeId('Account', 'Individual Agency'))]){
                                           accountIdsList.add(thisAgency.Agency__c);   
                                       }
        system.debug('#### accountIdsList = '+accountIdsList);
        for(Account thisAccount : [SELECT Id, Name, RecordTypeId, RecordType.Name,
                                   (SELECT Id, Name, Org_ID_formula__c, Org_ID__c
                                    FROM Agent_Sites__r 
                                    WHERE End_Date__c = NULL) 
                                   FROM Account 
                                   WHERE Name LIKE: '%'+searchKey+'%' AND
                                   (Id IN: accountIdsList OR
                                    (RecordTypeId =: DamacUtility.getRecordTypeId('Account', 'Corporate Agency') AND 
                                     Blacklisted__c = false AND 
                                     Terminated__c = false))]){
                                         agencyList.add(thisAccount);    
                                     }
        return agencyList;
    }
    
    /*
Query the Corporate Agnecy Details and return results on the Page   
*/
    @RemoteAction 
    global static List<Contact> getCorporateAgents(String selectedCorporateAgency){
        List<Contact> agentsList = new List<Contact>();
        //Id sPC = UserInfo.getUserId();
        try {
            //contact c = [select id,Name from contact where ownerid=:sPC and AccountId=:selectedCorporateAgency];
            agentsList = [select id,Name from contact where AccountId=:selectedCorporateAgency];
            //agentsList.add(c); 
        } Catch (Exception e) {}
        
        return agentsList;
    }  
    
    /*
Query the Inquiry details owned by the User Logged in and return the results on the Page
*/
    @RemoteAction 
    global static List<Inquiry__c> getInquiryDetails(String searchKey){
        String key = '%'+searchKey+'%';
        List<Inquiry__c> inquiryList = new List<Inquiry__c>();
        User userObj = [SELECT IsPortalEnabled  FROM user WHERE id=:userinfo.getuserid() LIMIT 1];
        list<string> status2Consider = System.label.InquiryStatus2consider.split(',');
        if(userObj.IsPortalEnabled) {
            inquiryList = [select id,Name,First_name__c,Last_Name__c,Email__c,Mobile_Phone__c,Passport_Number__c 
                    from inquiry__c 
                    where ( First_Name__c LIKE:key or Last_Name__c LIKE:key or Name LIKE:key ) 
                    and  Inquiry_Status__c in: status2Consider
                    and (recordtype.Name='CIL') 
                    and ownerid=:userinfo.getuserid() LIMIT 10];  

        } else {
            inquiryList = [select id,Name,First_name__c,Last_Name__c,Email__c,Mobile_Phone__c,Passport_Number__c 
                            from inquiry__c 
                            where (First_Name__c LIKE:key or Last_Name__c LIKE:key or Name LIKE:key) 
                            and Inquiry_Status__c in: status2Consider
                            and (recordtype.Name='Inquiry' OR recordtype.Name='Inquiry Decrypted') and ownerid=:userinfo.getuserid() LIMIT 10];  
        }
        return inquiryList;
    }
    
    /*
Query the Account Details owned by the Logged in User and return results on to the Page
*/
    @RemoteAction 
    global static List<Account> getAccountDetails(String searchKey,string btype){
        system.debug(searchkey+'####');
        System.debug('---btype -----' + btype);
        List<Account> AcconuntList = new List<Account>();
        if(btype == 'Individual'){        
            AcconuntList = [select id,name,FirstName,LastName from Account where ownerid=:userinfo.getuserid()  and  (firstname LIKE:searchKey or lastName LIKE:searchKey or name LIKE:'%'+searchkey+'%' ) and recordtype.Name='Person Account'  LIMIT 10];
            System.debug('indiviual acoiunt -----' + AcconuntList);
        }
        if(btype == 'Corporate'){        
            AcconuntList = [select id,name,FirstName,LastName from Account where ownerid=:userinfo.getuserid()  and  (firstname LIKE:searchKey or lastName LIKE:searchKey or name LIKE:'%'+searchkey+'%' ) and recordtype.Name='Business Account' LIMIT 10];            
            System.debug('Corporate acoiunt -----' + AcconuntList);
        }        
        return AcconuntList;
    } 
    
    
}