public without sharing class FmUploadDocumentsController {

    private static final String CATEGORY = 'Document';
    private static final String ENTITY_NAME = 'Damac Service Requests';
    private static final String IPMS_PREFIX = 'IPMS-';


    @RemoteAction
    public static List<Location__c> queryBuildings(String propertyId) {
        Map<Id, Location__c> mapBuilding = new Map<Id, Location__c>();
        String strProfileName = System.Label.FMDocUpload_UserCheckProfileIds;
        List<String> listProfileName = strProfileName.split(',');
        Boolean isProfileAdmin = false;
        if(!listProfileName.isEmpty()){
            List<Profile> listProfileId = [SELECT Id FROM Profile WHERE Name IN :listProfileName];
            if(!listProfileId.isEmpty()){
                for(Profile objProfileIds: listProfileId){
                    System.debug('-->> UserInfo.getProfileId(): '+ UserInfo.getProfileId());
                    System.debug('-->> objProfileIds.Id: '+ objProfileIds.Id);
                    if(objProfileIds.Id == UserInfo.getProfileId()){
                        isProfileAdmin = true;
                        break;
                    }
                }
            }
        }
        System.debug('-->> isProfileAdmin: '+ isProfileAdmin);
        String buildingQuery;
        if(isProfileAdmin){
            buildingQuery = 'SELECT  Id ' +
                                       ', Name ' +
                                        ', Property_Name__c ' +
                                        ', Property_Name__r.Id ' +
                                        ', Property_Name__r.Name ' +
                                        ', Building_Name__c ' +
                                        ', Location_Code__c ' +
                                'FROM    Location__c '+
                                'WHERE   (Location_Type__c = \'Building\' OR Location_Type__c = NULL) ';
            if (String.isNotBlank(propertyId)) {
                buildingQuery += 'AND Property_Name__c = \'' + propertyId + '\' ';
            }
            buildingQuery += 'LIMIT   10000 ';
            List<Location__c> listLocationToReturn = Database.query(buildingQuery);
            System.debug('-->> listLocationToReturn: '+ listLocationToReturn);
            System.debug('-->> listLocationToReturn.size(): '+ listLocationToReturn.size());
            return listLocationToReturn;
        } else {
            buildingQuery = 'SELECT  Id ' +
                                        ', Building__c ' +
                                        ', Building__r.Id ' +
                                        ', Building__r.Name ' +
                                        ', Building__r.Property_Name__c ' +
                                        ', Building__r.Property_Name__r.Id ' +
                                        ', Building__r.Property_Name__r.Name ' +
                                        ', Building__r.Building_Name__c ' +
                                        ', Building__r.Location_Code__c ' +
                                'FROM    FM_User__c '+
                                'WHERE   (Building__r.Location_Type__c = \'Building\' OR Building__r.Location_Type__c = NULL) ' +
                                        'AND FM_User__c = \'' + UserInfo.getUserId() + '\' ';

            if (String.isNotBlank(propertyId)) {
                buildingQuery += 'AND Building__r.Property_Name__c = \'' + propertyId + '\' ';
            }
            buildingQuery += 'LIMIT   10000 ';
            for (FM_User__c fmUser : Database.query(buildingQuery)) {
                mapBuilding.put(fmUser.Building__c, fmUser.Building__r);
            }
            System.debug('-->> mapBuilding: '+ mapBuilding);
            System.debug('-->> mapBuilding.size(): '+ mapBuilding.size());
            return mapBuilding.values();
        }
    }

    @RemoteAction
    public static FM_Document_Upload_Batch__c upsertFmDocUploadBatch(FM_Document_Upload_Batch__c fmDocUploadBatch) {
        upsert fmDocUploadBatch;
        return fmDocUploadBatch;
    }

    @RemoteAction
    public static FmUploadDocumentsController.Document uploadDocument(
        String docJson
    ) {
        FmUploadDocumentsController.Document doc;

        try {
            doc = (FmUploadDocumentsController.Document)
                    JSON.deserialize(docJson, FmUploadDocumentsController.Document.class);
        } catch(Exception excp) {
            throw new FmUploadDocumentsController.UploadException('File Upload Error: ' + excp.getMessage());
        }

        if (doc == NULL) {
            return doc;
        }

        UploadMultipleDocController.Data response;
        //System.debug('doc.docId = ' + doc.docId);
        if (doc.isParent == NULL || !doc.isParent) {
            List<UploadMultipleDocController.MultipleDocRequest> lstMultiDocUploadReq =
                    new List<UploadMultipleDocController.MultipleDocRequest> { createUploadRequest(doc) };
            response  = UploadMultipleDocController.getMultipleDocUrl(lstMultiDocUploadReq);
        }
        //System.debug('doc.docId = ' + doc.docId);
        deleteTemporaryAttachment(doc.docId);

        SR_Attachments__c attachment = createSrAttachment(doc, response);
        List<FM_Event__c> lstFmEvent = new List<FM_Event__c>();

        Id buildingId = NULL;
        Id propertyDirectorId = NULL;
        if (attachment.Property__c != NULL) {
            List<FM_User__c> lstPropertyDirector = [
                SELECT  Id
                        , Building__c
                        , FM_User__c
                FROM    FM_User__c
                WHERE   Building__r.Property_Name__c = :attachment.Property__c
                    AND FM_Role__c = 'Property Director'
                LIMIT   1
            ];
            if (!lstPropertyDirector.isEmpty()) {
                buildingId = lstPropertyDirector[0].Building__c;
                propertyDirectorId = lstPropertyDirector[0].FM_User__c;
            }
        } else if (attachment.Building__c != NULL) {
            buildingId = attachment.Building__c;
        }

        attachment.Approval_Status__c = 'Pending';
        attachment.Approver__c = buildingId == NULL
                                    ? Label.FmGlobalDocumentApproverId
                                    : (attachment.Property__c != NULL
                                        ? propertyDirectorId
                                        : FM_Utility.getFmUsersByLocationAndRole(new Set<Id>{buildingId})
                                            .get(buildingId).get('Property Director')
                                    );
        doc.approverId = attachment.Approver__c;


        attachment.FM_Recipient__c = doc.recipient;

        if ('Notice'.equalsIgnoreCase(doc.docType)) {

            if (doc.startDate != NULL) {
                lstFmEvent.add(new FM_Event__c(
                    Name = attachment.Name,
                    StartDateTime__c =  Datetime.newInstance(
                        doc.startDate.year(), doc.startDate.month(), doc.startDate.day()
                    ),
                    IsAllDayEvent__c = doc.endDate == NULL,
                    EndDateTime__c = doc.endDate == NULL ? NULL
                    : Datetime.newInstance(doc.endDate.year(), doc.endDate.month(), doc.endDate.day(), 11, 59, 59),
                    AttachmentURL__c = attachment.Attachment_URL__c,
                    BuildingLocation__c = attachment.Building__c,
                    Description__c = attachment.Description__c,
                    IsVisibleOnPortal__c = false,
                    Type__c = 'Notice'
                ));
            }
        }

        List<SR_Attachments__c> lstAttachment = new List<SR_Attachments__c> { attachment };

        try {
            insert lstAttachment;
            insert lstFmEvent;
        } catch(System.Exception excp) {
            //System.debug('excp = ' + JSON.serializePretty(excp.getMessage()));
            insert FM_Utility.createErrorLog( NULL, NULL, NULL, 'FM Upload Documents', excp.getMessage() );
            throw new FmUploadDocumentsController.UploadException(
                'File Upload Error : ' + excp.getMessage()
            );
        }

        for (Integer i = 0; i < lstFmEvent.size(); i++) {
            lstFmEvent[i].Document__c = lstAttachment[i].Id;
        }

        for (Integer i = 0; i < lstAttachment.size(); i++) {
            doc.documentId = lstAttachment[0].Id;
            doc.documentUrl = lstAttachment[0].Attachment_URL__c;
        }

        return doc;
    }

    private static void deleteTemporaryAttachment(String docId) {
        List<Attachment> lstAttachment = [
            SELECT  Id
                    , Body
            FROM    Attachment
            WHERE   Id = :docId
        ];
        if (!lstAttachment.isEmpty()) {
            try {
                delete lstAttachment;
            } catch(Exception e) {
                //System.debug(e.getMessage());
            }
            lstAttachment = new List<Attachment>();
        }
    }

    public static UploadMultipleDocController.MultipleDocRequest createUploadRequest(
        FmUploadDocumentsController.Document doc
    ) {
        UploadMultipleDocController.MultipleDocRequest docUploadReq =
                new UploadMultipleDocController.MultipleDocRequest();

        String docName = doc.docName;
        String docType = extractType(doc.fileName);
        String guid = DateTime.now().getTime() + '-' + 1;
        String fileGuid = doc.docType + '-' + guid;

        List<Attachment> lstAttachment = [
            SELECT  Id
                    , Body
            FROM    Attachment
            WHERE   Id = :doc.docId
        ];
        if (!lstAttachment.isEmpty()) {
            //docUploadReq.base64Binary = lstAttachment[0].Body.toString();
            docUploadReq.base64Binary = EncodingUtil.base64Encode(lstAttachment[0].Body);
            //lstAttachment = new List<Attachment>();
            //System.debug('lstAttachment = ' + lstAttachment[0].Id);
        }
        //System.debug('doc.docId = ' + doc.docId);

        //docUploadReq.base64Binary = EncodingUtil.base64Encode(doc.blobData);
        //docUploadReq.base64Binary = doc.strBlob;
        docUploadReq.category = 'Document';
        docUploadReq.entityName = 'Damac Service Requests';
        docUploadReq.fileDescription = doc.description;
        docUploadReq.fileId = fileGuid + '.' + docType;
        docUploadReq.fileName = fileGuid + '.' + docType;

        docUploadReq.registrationId = guid;
        docUploadReq.sourceFileName = /*IPMS_PREFIX + guid + '-' +*/ docName + '.' + docType;
        docUploadReq.sourceId = IPMS_PREFIX + guid /*+ '-' + docName*/;
        //fileBody = NULL; //if user deletes, and saves the draft
        //lstMultiDocUploadReq.add(docUploadReq);

        return docUploadReq;
    }

    @RemoteAction
    public static SendGridEmailService.SendGridResponse sendTestEmailNotification(String uploadBatchId, String email) {
        FM_Document_Upload_Batch__c docBatch = [
                SELECT Id
                        , Document_Level__c
                        , FM_Recipient__c
                        , Notification_Email_Message__c
                        , Notification_Email_Subject__c
                        , Process_Name__c
                        , Related_to_Id__c
                        , ( SELECT  Id
                                    , Name
                                    , Document_Type__c
                                    , Attachment_URL__c
                                    , FM_Document_Upload_Batch__c
                                    , Building__c
                                    , Building__r.Id
                                    , Building__r.Name
                                    , Building__r.Building_Name__c
                                    , Building__r.Property_Name__c
                                    , Building__r.Property_Name__r.Id
                                    , Building__r.Property_Name__r.Name
                                    , Property__c
                                    , Property__r.Id
                                    , Property__r.Name
                            FROM    Documents__r
                        )
                FROM    FM_Document_Upload_Batch__c
                WHERE   Id = :uploadBatchId
        ];

        if (String.isNotBlank(docBatch.Notification_Email_Message__c)
            && String.isNotBlank(docBatch.Notification_Email_Subject__c)
        ) {
            List<OrgWideEmailAddress> orgWideAddressHelloDamac = [
                SELECT  Id
                        , Address
                        , DisplayName
                FROM    OrgWideEmailAddress
                WHERE   Id = :Label.CommunityPortalOrgWideAddressId
            ];

            String mailSubject = 'New FM Document Upload Notification';

            String htmlBody = '';

            htmlBody += '<head>' +
                            '<style type="text/css">' +
                                'table, thead, tbody, tr, th, td {' +
                                    'border:1px solid;' +
                                '}' +
                                'table {' +
                                    'border-collapse: collapse;' +
                                    'font-family: "Trebuchet MS", Helvetica, sans-serif;' +
                                '}' +
                                'thead {' +
                                    'background: #D3D3D3;' +
                                    'background: #36304a;' +
                                '}' +
                                'th {' +
                                    'color: #fff;' +
                                    'font-weight: normal;' +
                                '}' +
                                'tbody tr:nth-child(even) {' +
                                    'background-color: #f5f5f5;' +
                                '}' +
                                'tbody tr {' +
                                    'color: #555;' +
                                '}' +
                            '</style>' +
                        '</head>';

            String processName = docBatch.Process_Name__c;
            String fmCaseId = docBatch.Related_to_Id__c;

            if (String.isNotBlank(docBatch.Notification_Email_Subject__c)) {
                mailSubject = docBatch.Notification_Email_Subject__c;
            }

            htmlBody += String.isNotBlank(docBatch.Notification_Email_Message__c)
                        ? (docBatch.Notification_Email_Message__c) : '';

            htmlBody += '<br/><br/><br/>';

            //htmlBody += 'Please find the list of newly uploaded Document(s) below: <br/><br/>';

            htmlBody += '<table>' +
                            '<thead>' +
                                '<tr>' +
                                    '<th>Sr. No</th>' +
                                    '<th>Property Name</th>' +
                                    '<th>Building Name</th>' +
                                    '<th>Document Name</th>' +
                                    '<th>Document Type</th>' +
                                    '<th>Document URL</th>' +
                                '</tr>' +
                            '</thead>';
            htmlBody += '<tbody>';

            Integer docNumber = 1;
            for (SR_Attachments__c doc : docBatch.Documents__r) {
                htmlBody += '<tr>' +
                                '<td>' + (docNumber++) + '</td>' +
                                '<td>'
                                    + ('Property Specific'.equalsIgnoreCase(docBatch.Document_Level__c)
                                        ? doc.Property__r.Name
                                        : ('Building Specific'.equalsIgnoreCase(docBatch.Document_Level__c)
                                            ? doc.Building__r.Property_Name__r.Name : ''
                                        )
                                    ) +
                                    '</td>' +
                                '<td>'
                                    + ('Building Specific'.equalsIgnoreCase(docBatch.Document_Level__c)
                                        ? (doc.Building__r.Name +
                                            (String.isBlank(doc.Building__r.Building_Name__c)
                                                ? '' : (' - ' + doc.Building__r.Building_Name__c))
                                        ) : '') +
                                '</td>' +
                                '<td>' + doc.Name + '</td>' +
                                '<td>' + doc.Document_Type__c + '</td>' +
                                '<td><a href="' + doc.Attachment_URL__c + '">' + doc.Attachment_URL__c + '</a></td>' +
                            '</tr>';
            }
            htmlBody += '</tbody>' +
                    '</table>';

            return SendGridEmailService.sendEmailService(
                email, '', '', '', '', '', mailSubject, '', orgWideAddressHelloDamac[0].Address,
                orgWideAddressHelloDamac[0].DisplayName, '', orgWideAddressHelloDamac[0].DisplayName, 'text/html',
                htmlBody, '', new List<Attachment>()
            );
        }
        return NULL;
    }

    @RemoteAction
    public static Boolean sendTestSmsNotification(String uploadBatchId, String mobileNumber) {
        FM_Document_Upload_Batch__c docBatch = [
            SELECT  Id
                    , Notification_SMS_Message__c
            FROM    FM_Document_Upload_Batch__c
            WHERE   Id = :uploadBatchId
        ];

        if (String.isNotBlank(docBatch.Notification_SMS_Message__c)) {
            SMS_History__c smsHistory = new SMS_History__c();
            smsHistory.Message__c = docBatch.Notification_SMS_Message__c;
            smsHistory.Phone_Number__c = mobileNumber;
            insert smsHistory;

            System.enqueueJob(new SendSMSQueueable(new Set<String> { smsHistory.Id } ));
            return true;
        }
        return false;
    }

    private static SR_Attachments__c createSrAttachment(
        FmUploadDocumentsController.Document doc, UploadMultipleDocController.Data response
    ) {
        SR_Attachments__c srAttachment = new SR_Attachments__c(
            Name = doc.docName,
            Document_Type__c = doc.docType,
            Description__c = doc.description,
            IsValid__c = doc.isValid,
            Building__c = String.isBlank(doc.buildingId) ? NULL : (String.isBlank(doc.parentId) ? doc.buildingId : NULL),
            Property__c = String.isBlank(doc.propertyId) ? NULL : (String.isBlank(doc.parentId) ? doc.propertyId : NULL),
            Valid_Till__c = doc.validTillDate,
            FM_Document_Upload_Batch__c = doc.uploadBatchId
        );

        if (doc.isParent == NULL || !doc.isParent) {
            //System.debug('Document isParent :'+doc.isParent);
            //System.debug('response : '+response.data);
            if (response != NULL && response.data != NULL && !response.data.isEmpty()) {
                UploadMultipleDocController.MultipleDocResponse data = response.data[0];
                if (data.PROC_STATUS == 'S' && String.isNotBlank(data.url)) {
                    srAttachment.Attachment_URL__c  = data.url;
                } else {
                    throw new FmUploadDocumentsController.UploadException('An error occurred while Uploading File');
                }
            } else {
                //System.debug('File Upload Error : '+response.message);
                throw new FmUploadDocumentsController.UploadException(
                    'File Upload Error : ' + (String.isBlank(response.message) ? '' : response.message)
                );
            }
            //System.debug('doc.parentId = ' + doc.parentId);
            srAttachment.Parent_Id__c = String.isBlank(doc.parentId) ? NULL : doc.parentId;
        }

        return srAttachment;
    }

    //@TestVisible
    //private static String extractName(String documentName) {
    //    return documentName.substring(documentName.lastIndexOf('\\') + 1);
    //}

    @TestVisible
    private static String extractType( String documentName ) {
        documentName = documentName.substring(documentName.lastIndexOf('\\') + 1);
        return documentName.substring(documentName.lastIndexOf('.') + 1) ;
    }


    public class Document {
        //public Blob     blobData        {get; set;}
        public String   strBlob         {get; set;}
        public String   docName         {get; set;}
        public String   fileName        {get; set;}
        public String   docType         {get; set;}
        public String   description     {get; set;}
        public Boolean  isValid         {get; set;}
        public String   buildingId      {get; set;}
        public String   propertyId      {get; set;}
        public Boolean  isParent        {get; set;}
        public String   parentId        {get; set;}
        public String   uploadBatchId   {get; set;}
        public String   documentUrl     {get; set;}
        public String   documentId      {get; set;}
        public Date     startDate       {get; set;}
        public Date     endDate         {get; set;}
        public Date     validTillDate   {get; set;}
        public String   recipient       {get; set;}
        public String   approverId      {get; set;}
        public String   docId           {get; set;}
    }

    public class UploadException extends Exception {}

}