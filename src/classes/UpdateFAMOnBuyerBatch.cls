public class UpdateFAMOnBuyerBatch implements Database.Batchable<sObject>{
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id, 
                                         (SELECT Id
                                          FROM Bookings__r
                                         ) 
                                         FROM Account
                                         WHERE FAM_CAUTION__c = 'FAM' 
                                         AND    Secondary_buyer_FAM_Check__c = false]); 
    }
    public void execute(Database.BatchableContext BC, List<Account> scope){
        Set<Id> boookingIdSet = new Set<Id>();
        Set<Id> accountIdSet = new Set<Id>();
        List<Account> accountToBeUpdated = new List<Account>();
        System.debug('scope---->'+scope);
        for(Account thisAccount : scope){                                 
            if(thisAccount.Bookings__r != null && thisAccount.Bookings__r.size() > 0){
                for(Booking__c thisBooking : thisAccount.Bookings__r){
                    boookingIdSet.add(thisBooking.Id);                    
                }                
            }
        }
        System.debug('boookingIdSet--->'+boookingIdSet);
        List<Booking__c> bookingList = [SELECT Id,
                                        (SELECT Id,Account__c
                                         FROM Buyers__r
                                         WHERE Account__c != Null)
                                        FROM Booking__c
                                        WHERE Id IN:boookingIdSet];
        for(Booking__c thisBooking: bookingList){
            if(thisBooking.Buyers__r != null && thisBooking.Buyers__r.size() > 0) {
                for(Buyer__c thisBuyer : thisBooking.Buyers__r) {
                    accountIdSet.add(thisBuyer.Account__c);
                }
            }
        }
        System.debug('accountIdSet-->'+accountIdSet);   
        for(Id thisAccountId : accountIdSet) {
            Account acc = new Account(Id = thisAccountId);
            acc.Secondary_buyer_FAM_Check__c = True;
            acc.FAM_CAUTION__c = 'FAM';
            accountToBeUpdated.add(acc);        
        }
        System.debug('accountToBeUpdated-->'+accountToBeUpdated);   
        update accountToBeUpdated;
        
    }
    public void finish(Database.BatchableContext BC){
    }
}