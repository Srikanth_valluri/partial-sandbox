/*-------------------------------------------------------------------------------------------------
Description: Test class for EHOCancellationController

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 03-01-2018       | Lochana Rajput   | 1. Added test methods for code coverage
   =============================================================================================================================
*/
@isTest
private class EHOCancellationControllerTest {

    @isTest static void test_cancelEHO() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 5);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 20);
        lstBookingUnits[0].Registration_ID__c = '74712';
        insert lstBookingUnits;
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Status='Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = true;
        insert objCase;
        List<Task> lstTasks = new List<Task>();
        lstTasks.add(new Task(WhatId=objCase.ID, Status='Not Working'));
        insert lstTasks;
        List<Payment_Plan__c> lstPaymentPlanInactive = new List<Payment_Plan__c>();
        lstPaymentPlanInactive.add(new Payment_Plan__c(Booking_Unit__c=lstBookingUnits[0].Id, Status__c='InActive'));
        insert lstPaymentPlanInactive;
        List<Payment_Plan__c> lstPaymentPlanActive = new List<Payment_Plan__c>();
        lstPaymentPlanActive.add(new Payment_Plan__c(Booking_Unit__c=lstBookingUnits[0].Id, Status__c='Active', Parent_Payment_Plan__c=lstPaymentPlanInactive[0].Id));
        insert lstPaymentPlanActive;
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        Test.startTest();
        PageReference pageRef = Page.EHOCancellation;
         Test.setCurrentPage(pageRef);
        EHOCancellationController controller = new EHOCancellationController(sc);
        controller.cancelEHO();
        Test.stopTest();
    }

    @isTest static void test_cancelEHOCaseNotSubmitted() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Is_New_Payment_Terms_Applied__c = true;
        insert objCase;
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        Test.startTest();
        PageReference pageRef = Page.EHOCancellation;
        Test.setCurrentPage(pageRef);
        EHOCancellationController controller = new EHOCancellationController(sc);
        controller.cancelEHO();
        Test.stopTest();
    }

    public static testmethod void testPaymentReversal(){
         List<Booking__c> bookingList = new List<Booking__c>();
         List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
         List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
    
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        //Id aoptRecordTypeID = getRecordTypeIdForAOPT();
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC';
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
        }
        system.debug('>>>>>'+bookingUnitList);
        insert bookingUnitList;        

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAccount.ID, caseRTId);
        objCase.Booking_Unit__c = bookingUnitList[0].Id;
        objCase.Status='Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = true;
        insert objCase;
        
        List<Task> lstTasks = new List<Task>();
        lstTasks.add(new Task(WhatId=objCase.ID, Status='Not Working'));
        insert lstTasks;
        
        Payment_Plan__c ObjPaymentPlanOld = new Payment_Plan__c();
        ObjPaymentPlanOld.Status__c = 'InActive';
        ObjPaymentPlanOld.Booking_Unit__c = bookingUnitList[0].id;
        insert ObjPaymentPlanOld;

        Payment_Plan__c ObjPaymentPlan = new Payment_Plan__c();
        ObjPaymentPlan.Status__c = 'Active';
        ObjPaymentPlan.Booking_Unit__c = bookingUnitList[0].id;
        ObjPaymentPlan.Parent_Payment_Plan__c = ObjPaymentPlanOld.id;
        insert ObjPaymentPlan;

        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        PageReference pageRef = Page.EHOCancellation;
        Test.setCurrentPage(pageRef);
        EHOCancellationController objCancel = new EHOCancellationController(stdController);
        
        Test.startTest();           
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));            
            objCancel.cancelEHO();
        Test.stopTest();   
    }
    
    //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
    //SOAPCalloutServiceMockAOPT.returnToMe = new Map<String,paymentPlanService.PaymentPlanReversalMultipleResponse_element>();
    //paymentPlanService.PaymentPlanReversalMultipleResponse_element response_x = new paymentPlanService.PaymentPlanReversalMultipleResponse_element();
    //response_x.return_x = 'S';
    //SOAPCalloutServiceMockAOPT.returnToMe.put('response_x',response_x);
    
    //paymentPlanService.AOPTHttpSoap11Endpoint obj =  new paymentPlanService.AOPTHttpSoap11Endpoint();
    //String  resp = obj.PaymentPlanReversalMultiple('P_REGISTRATION_ID','P_SR_NUMBER');
    //System.assertEquals(resp, 'S');
    //PaymentPlanReversalMultipleService.updatePaymentPlanInIPMS(bookingUnitList[0].Registration_Id__c,'2-001545',objCase);*/
}