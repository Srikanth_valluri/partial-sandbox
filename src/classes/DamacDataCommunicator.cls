// Class to consume the damac website data service
public class DamacDataCommunicator {

    @RemoteAction
    public static String getOffersData(){
        HttpRequest req = new HttpRequest();
        req.setEndpoint(Label.Damac_Offers);
        req.setMethod('GET');
        Http http = new Http();
        HTTPResponse res;
        if (!Test.isRunningTest()) {
            res = http.send(req);
        } else {
            res = new HttpResponse();
        }
        return res.getBody();
    }
}