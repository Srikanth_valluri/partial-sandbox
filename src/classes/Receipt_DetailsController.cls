public with sharing class Receipt_DetailsController {
    public String accId                                        {get;set;}
    public String strResponse;
    public String errorMessage;
    public List<ReceiptDetailsWrapper> lstWrapper              {get;set;}
    public List<Error_Log__c> lstErrorLog;
    public String docNumber                                    {get;set;}
    public String receiptNumber                                {get;set;}

    public Receipt_DetailsController(ApexPages.StandardController controller) {
        //init();
    } 
 
    public void init(){
        accId = ApexPages.currentPage().getParameters().get('id');
        accId = String.valueOf(accId).substring(0, 15); 
        strResponse = null;
        lstWrapper = new List<ReceiptDetailsWrapper>();
        lstErrorLog = new List<Error_Log__c>();
        errorMessage = '';

        Account currentAccount = [Select Id,
                                         Name,
                                         Party_ID__c
                                    From Account
                                   Where Id = :accId 
                                ];
        system.debug('==currentAccount.Party_ID__c===' + currentAccount.Party_ID__c);                       
        if(currentAccount.Party_ID__c != null) {
            ParseReceiptDetails(currentAccount.Party_ID__c);
        }
        system.debug('==lstWrapper===' + lstWrapper);

    }
    public void ParseReceiptDetails(string partyId) {
        lstWrapper = new List<ReceiptDetailsWrapper>();
        strResponse = ReceiptDetailsController.GetReceiptDetails(partyId);
        system.debug('strResponse==='+strResponse);
        if(String.isNotBlank(strResponse)){
            map<String,Object> mapDeserialize = (map<String,Object>)JSON.deserializeUntyped(strResponse);
            if(mapDeserialize.get('status') == 'S'){
                String StringPDCData = JSON.serialize(mapDeserialize.get('data'));
                lstWrapper = 
                    (List<ReceiptDetailsWrapper>)JSON.deserialize(StringPDCData, List<ReceiptDetailsWrapper>.class);
                system.debug('lstWrapper==='+lstWrapper);
                strResponse = 'Success';
            }
            else if(mapDeserialize.get('status') == 'E'){
                errorMessage = 'Error : '+mapDeserialize.get('message');
                Error_Log__c objErr = createErrorLogRecord(accId);
                objErr.Error_Details__c = errorMessage;
                lstErrorLog.add(objErr);
                strResponse = 'Server Down... Please try after sometime.';
            }
        }
        else {
            errorMessage = 'Error : No Response from IPMS for PDC Details';
            Error_Log__c objErr = createErrorLogRecord(accId);
            objErr.Error_Details__c = errorMessage;
            lstErrorLog.add(objErr);
            strResponse = 'Error : No Response from IPMS for PDC Details.';
        }
        if(lstErrorLog.size() > 0 && lstErrorLog != null) {
            insert lstErrorLog;
        }
    }

    public void GenerateURL() {
        //docNumber = ApexPages.CurrentPage().getParameters().get('docNumber');
        //receiptNumber = ApexPages.CurrentPage().getParameters().get('receiptNumber');
        system.debug('===docNumber====' + docNumber );        
        system.debug('===receiptNumber====' + receiptNumber );
        
        List<GenerateReceiptProcessService.APPSXXDC_PROCESS_SERX1794747X1X5> lstRegTerms = 
            new List<GenerateReceiptProcessService.APPSXXDC_PROCESS_SERX1794747X1X5>();

        GenerateReceiptProcessService.APPSXXDC_PROCESS_SERX1794747X1X5 regTerms = new GenerateReceiptProcessService.APPSXXDC_PROCESS_SERX1794747X1X5();
        regTerms.ATTRIBUTE1 = '';
        regTerms.ATTRIBUTE10 = '';
        regTerms.ATTRIBUTE11 = '';
        regTerms.ATTRIBUTE12 = '';
        regTerms.ATTRIBUTE13 = '';
        regTerms.ATTRIBUTE14 = '';
        regTerms.ATTRIBUTE15 = '';
        regTerms.ATTRIBUTE16 = '';
        regTerms.ATTRIBUTE17 = '';
        regTerms.ATTRIBUTE18 = '';
        regTerms.ATTRIBUTE19 = '';
        regTerms.ATTRIBUTE2 = '';
        regTerms.ATTRIBUTE20 = '';
        regTerms.ATTRIBUTE21 = '';
        regTerms.ATTRIBUTE22 = '';
        regTerms.ATTRIBUTE23 = '';
        regTerms.ATTRIBUTE24 = '';
        regTerms.ATTRIBUTE25 = '';
        regTerms.ATTRIBUTE26 = '';
        regTerms.ATTRIBUTE27 = '';
        regTerms.ATTRIBUTE28 = '';
        regTerms.ATTRIBUTE29 = '';
        regTerms.ATTRIBUTE3 = docNumber;
        regTerms.ATTRIBUTE30 = '';
        regTerms.ATTRIBUTE31 = '';
        regTerms.ATTRIBUTE32 = '';
        regTerms.ATTRIBUTE33 = '';
        regTerms.ATTRIBUTE34 = '';
        regTerms.ATTRIBUTE35 = '';
        regTerms.ATTRIBUTE36 = '';
        regTerms.ATTRIBUTE37 = '';
        regTerms.ATTRIBUTE38 = '';
        regTerms.ATTRIBUTE39 = '';
        regTerms.ATTRIBUTE4 = '';
        regTerms.ATTRIBUTE41 = '';
        regTerms.ATTRIBUTE42 = '';
        regTerms.ATTRIBUTE43 = '';
        regTerms.ATTRIBUTE44 = '';
        regTerms.ATTRIBUTE45 = '';
        regTerms.ATTRIBUTE46 = '';
        regTerms.ATTRIBUTE47 = '';
        regTerms.ATTRIBUTE48 = '';
        regTerms.ATTRIBUTE49 = '';
        regTerms.ATTRIBUTE5 = '';
        regTerms.ATTRIBUTE50 = '';
        regTerms.ATTRIBUTE6 = '';
        regTerms.ATTRIBUTE7 = '';
        regTerms.ATTRIBUTE8 = '';
        regTerms.ATTRIBUTE9 = '';
        regTerms.PARAM_ID = docNumber;
        lstRegTerms.add(regTerms);
        List<GenerateReceiptService.ReceiptResponse> lstGenerateReceipt = new List<GenerateReceiptService.ReceiptResponse>();
        lstGenerateReceipt = GenerateReceiptService.getReceiptURL(lstRegTerms);

        if(lstGenerateReceipt != null & lstGenerateReceipt[0].status != 'Exception') {
            system.debug('=size GenerateURL=lstWrapper===: ' + lstWrapper.size());
            for(ReceiptDetailsWrapper wrap : lstWrapper) {
                if(wrap.ATTRIBUTE1 == docNumber) {
                    system.debug('--url--: ' + lstGenerateReceipt[0].url);
                    if(String.isEmpty(lstGenerateReceipt[0].url)) {
                        wrap.url = 'URL doesnot exist';
                    } else {
                        wrap.url = lstGenerateReceipt[0].url;
                        break;  
                    }                    
                }
            }
            for(ReceiptDetailsWrapper wrap : lstWrapper) {
                system.debug('--wrap--'+wrap.ATTRIBUTE1+'----'+wrap.url);
            }
        }
    }
    public class ReceiptDetailsWrapper {
        public String ATTRIBUTE1 {get;set;}
        public String ATTRIBUTE2 {get;set;}
        public String ATTRIBUTE3 {get;set;}
        public String ATTRIBUTE4 {get;set;}
        public String ATTRIBUTE5 {get;set;}
        public String ATTRIBUTE6 {get;set;}
        public String ATTRIBUTE7 {get;set;}
        public String ATTRIBUTE8 {get;set;}
        public String ATTRIBUTE9 {get;set;}
        public String ATTRIBUTE10 {get;set;}
        public String ATTRIBUTE11 {get;set;}
        public String ATTRIBUTE12 {get;set;}
        public String ATTRIBUTE13 {get;set;}
        public String ATTRIBUTE14 {get;set;}
        public String ATTRIBUTE15 {get;set;}
        public String ATTRIBUTE16 {get;set;}
        public String ATTRIBUTE17 {get;set;}
        public String ATTRIBUTE18 {get;set;}
        public String ATTRIBUTE19 {get;set;}
        public String ATTRIBUTE20 {get;set;}
        public String url {get;set;}

        public ReceiptDetailsWrapper(){
            
        }
    }

    public Error_Log__c createErrorLogRecord(String accId) {
        Error_Log__c objErr = new Error_Log__c(Account__c = accId);
        return objErr;
    }
}