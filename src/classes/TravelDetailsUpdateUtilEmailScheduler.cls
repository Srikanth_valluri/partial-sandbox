/**************************************************************************************************
* Name               : TravelDetailsUpdateUtilEmailScheduler
* Description        : Scheduler class for TravelDetailsUpdateUtilEmail batch class.
* Test Class         : TravelDetailsUpdateUtilEmailTest
* Created Date       : 28/02/2020
* Created By         : QBurst
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst          28/02/2020      Initial Draft.
**************************************************************************************************/
public class TravelDetailsUpdateUtilEmailScheduler implements Schedulable {
  public void execute(SchedulableContext SC) {
    TravelDetailsUpdateUtilEmailBatch tdEmailBatchObj = new TravelDetailsUpdateUtilEmailBatch();
    Database.executeBatch(tdEmailBatchObj, 20); 
  }
}