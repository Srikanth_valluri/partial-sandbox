/*
* Description - Test class developed for 'documentGeneration'
*
* Version            Date            Author            Description
* 1.0                17/11/17        Snehil            Initial Draft
*/
@isTest 
public class documentGenerationTest { 
	
    static testMethod void testMethod1() {
        String TemplateName = 'Test TemplateName';
        docgenerationDtoComXsd.DocGenDTO objDocGenDTO = new docgenerationDtoComXsd.DocGenDTO();
        documentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint obj = new documentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint();
        
        test.startTest(); 
    	SOAPCalloutServiceMock.returnToMe = new Map<String, documentGeneration.DocGenerationResponse_element>();
    	documentGeneration.DocGenerationResponse_element response = new documentGeneration.DocGenerationResponse_element();
		response.return_x = 'http://test.txt';
		SOAPCalloutServiceMock.returnToMe.put('response_x', response);
		Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        String resp = obj.DocGeneration(TemplateName,objDocGenDTO);
        test.stopTest();
    }
}