@isTest
private class EsignReservationFormController_Test {
    static testMethod void esignReservationTest() {
        Account acc = insertAccount();
        NSIBPM__Service_Request__c serviceReq = insertServiceRequest();
        Booking__c book = insertBooking(serviceReq.Id);
        Inventory__c inventory = insertInventory();
        Booking_Unit__c bookUnit = insertBookingUnit(book.Id, inventory.Id);
        Buyer__c buyer = insertBuyer(acc.Id, book.Id, bookUnit.Id);
        
        Test.startTest();
        PageReference pageRef = Page.ESignReservationFormPDF;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('recId', String.valueOf(bookUnit.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(bookUnit);
        EsignReservationFormController erfc = new EsignReservationFormController(sc);
        Test.stopTest();
    }
    
    private static Booking_Unit__c insertBookingUnit(Id bookingId, Id inventoryId) {
        Booking_Unit__c bookUnit = new Booking_Unit__c();
        bookUnit.Booking__c = bookingId;
        bookUnit.Inventory__c = inventoryId;
        bookUnit.Payment_Method__c = 'Cash';
        bookUnit.Requested_Token_Amount__c = 8374447;
        insert bookUnit;
        return bookUnit;
    }
    
    private static Booking__c insertBooking(Id servReqId) {
        Booking__c book = new Booking__c();
        book.Deal_SR__c = servReqId;
        book.Booking_Channel__c = 'Office';
        book.Unique_Key__c = 'a0M1w000001Zfnha1A1w000000iAYuEAM';
        insert book;
        return book;
    }
    
    private static NSIBPM__Service_Request__c insertServiceRequest() {
        NSIBPM__Service_Request__c serviceReq = new NSIBPM__Service_Request__c();
        serviceReq.Delivery_Mode__c = 'Print at Sales Office';
        serviceReq.Reinstatement_Status__c = 'Not Applicable';
        serviceReq.Deal_ID__c = '251568';
        serviceReq.Rejection_Status__c = 'Not Applicable';
        insert serviceReq;
        return serviceReq;
    }
    
    private static Inventory__c insertInventory() {
        Inventory__c inventory = new Inventory__c();
        inventory.Property_City__c = 'DUBAI';
        inventory.Unit__c = 'ORB/4/401';
        inventory.Property_Type__c = 'Residential Apartments';
        inventory.Project_Category__c = 'LOW-RISE';
        inventory.IPMS_Bedrooms__c = 'DAMAC CRESCENT PROPERTIES LLC';
        inventory.Price__c = '2502198.6325';
        insert inventory;
        return inventory;
    }
    
    private static Buyer__c insertBuyer(Id accId, Id bookingId, Id bookingUnitId) {
        Buyer__c buyer = new Buyer__c();
        buyer.Account__c = accId;
        buyer.Booking__c = bookingId;
        buyer.Booking_Unit__c = bookingUnitId;
        buyer.Nationality__c = 'Australian';
        buyer.First_Name__c = 'First';
        buyer.Middle_Name__c = 'Middle';
        buyer.Last_Name__c = 'Last';
        buyer.Email__c = 'test@demo.com';
        buyer.Passport_Number__c = '1234567890';
        buyer.Primary_Buyer__c = true;
        buyer.Buyer_Type__c = 'Individual';
        buyer.Address_Line_1__c = 'Address 1';
        buyer.Phone__c = '9876543210';
        insert buyer;
        return buyer;
    }
    
    private static Account insertAccount() {
        Account acc = new Account();
        acc.Name = 'Testing Account';
        acc.Agency_Email__c = 'acc@email.com';
        acc.VAT_Registration_Certificate_Date__c = Date.today();
        insert acc;
        return acc;
    }
}