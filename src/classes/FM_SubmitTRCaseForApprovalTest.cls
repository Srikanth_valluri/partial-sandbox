@isTest
private class FM_SubmitTRCaseForApprovalTest {
    @isTest
    static void itShould() {
        FM_Case__c fmCase = new FM_Case__c(
            Status__c = 'Submitted',
            Origin__c = 'Portal',
            RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName()
                                        .get('Tenant Registration').getRecordTypeId()
        );
        insert fmCase;
        Task fmTask = new Task(
            Subject = Label.Subject_For_Admin_Task,
            OwnerId = UserInfo.getUserId(),
            Priority = 'High',
            WhatId = fmCase.Id,
            Status = 'Not Started',
            ActivityDate = Date.today()
        );
        insert fmTask;
        FM_SubmitTRCaseForApproval.sendTrCaseForApproval(new List<Task>{fmTask});
    }
}