@isTest
public class CloseTaskForCollectionCreSchedulerTest {
    public static testMethod void testCloseTaskForCollectionCreScheduler() {
        Test.startTest();
        String cron = '0 0 12 1/1 * ? *';
        String jobID = system.schedule('CloseTaskForCollectionCreBatch Job', cron, new CloseTaskForCollectionCreScheduler());
        Test.stopTest();
        System.assert(jobID != null);
    }
}