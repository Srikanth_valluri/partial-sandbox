@isTest
public class VirtualHandoverDLDMeetingControllerTest {
    
    private class VirtualHandoverMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            
            HTTPResponse res = new HTTPResponse();
            res.setBody('{"token_type": "Bearer","scope": "Calendars.Read","expires_in": 3599,"ext_expires_in": 3599,'
                                + '"access_token": " accesstoken","refresh_token": "refreshtoken"}');
            res.setStatusCode(200);
            return res;
            
        }
    }
    
    @isTest static void testcreateMeeting() {
      API_Integration_Value__c objApiInt = new API_Integration_Value__c();
        objApiInt.name = 'Virtual Handover Graph API';
        objApiInt.Endpoint_URL__c = 'https://login.microsoftonline.com/';
        objApiInt.Tenant_Id__c = '57e43429-428a-4973-9720-e6907ae94def';
        objApiInt.Client_Id__c = 'b8f6c4e9-d493-4a2f-96f3-7d4ea68c1540';
        objApiInt.Client_Secret__c = 'f18Ocz6Phrng4OyW_iC_.R0z.e85a38qy4';
        objApiInt.Scope__c = 'Offline_access%20Calendars.Read%20Calendars.ReadWrite%20OnlineMeetings.Read%20OnlineMeetings.ReadWrite%20user.Read';
        objApiInt.Refresh_Token__c = 'OAQABAAAAAAAGV_bv21oQQ4ROqh0_1-tA4P4IswFvPEVC1Y6u-NplxxTXklGFdsBBIGMYyccIgg-66bzebSXv-4NR1YEglPxSeUQPojaZyFC8kQDkUdOlXVm9oz9oRxWt1YpJFRqpKnOF1zzK9ZZkaVdn2YpOXgtuNePZZdIOhGD-aHk60havFh8RyTVjIAOU0xMjzRqOrZiz0PQOL0DQmX4PMeyNOe_S6iOlI29kk-OaUvBPwPld1Ux9M_Yva-bzNw7Fs8gGRRE_-babB8oILd8VqWPfF_tQ0CQwoIclK2xOj9OGQpEGfN514fLWw3J9PLG1gL9wb0mnLkky3oVj6powQ-WpwDYLL_6AkM0Ovs4omhjdhzeLYmR_Iblw0uhoV8-Ekpjep10wRQElKoYaMwKrhQ6bntEIXqFJ1IgedeBC3soXuCLFaKqCgP_nHb5NZo5ePOiO16kjSAsV2dnBKkjmHDC52mtm1qlFYP-nVFT2JkTHyNCeLMqNbytTJmtt51vTzzgU3eq2VPx1CPUdjw9vxQsbtbt5c49OkaGw-jszWvlTufife1Gmoy1vdkaN-Ol64dyCscfTF2zhbP4p2BEa2pIutsA8h4rV5ov79y-tj2xpPCacsAB2oz-la_QHa_n5aWP3dIuyXhVQ1_uitj8TWNjR2Nx1Vk2-Lz6AoJneGTHWkYQU1zvLh0cOuiDn1imO3pIiEPmtedqNqYu_bI6xsUnaMov5buNbmMjp5AttQuf9q_gRowJwAhPkWxZuLhdIXDA-bx5QaLFVXYRAH-8ym-giz9-vbYm7rmWm-A45oGY-STHpFPzT-8jiE31GspES6mCbQZ_926_iLm1M7gcK_MdsAkMpq5Y1LnsXO-cz0BGltcMnmXDYcrw5JvWlNF-khfSc_gvnl_0SbLZs-JsJii_10OIl0hLkQsaURecr0YZHDwREHsNcId3kqKW1uTA6PKaoyGStIuUBuXwhGREWpTIeK-TpUpUKqnWDF_i_pzJ2wH5AdGvddGgEy0ghyeaMAo8kCWkM4MEy8C-6v6dXiHMvEVdV0RsQkCbzvnrlB_9MV8MSGUxrNKGmm6ot2_7EMRnG4Ff8Hzn95QJgjLtXIaLMVOOsyeTbdoo0G9h_lcfoA0QPwSAA';
        objApiInt.Meeting_EndPoint_URL__c = 'https://graph.microsoft.com/v1.0/me/onlineMeetings';
        objApiInt.Event_Creation_URL__c = 'https://graph.microsoft.com/v1.0/me/calendar/events';
        objApiInt.Redirect_URI__c = 'https://damacholding--crmuatpro.my.salesforce.com/services/apexrest/DLDTrusteeMeeting/';
        objApiInt.Username__c = 'DLDTrustee.Meeting@damacproperties.com';
        insert objApiInt;
        
        API_Integration_Value__c objApiInt1 = new API_Integration_Value__c();
        objApiInt1.name = 'Virtual Handover Graph API';
        objApiInt1.Endpoint_URL__c = 'https://login.microsoftonline.com/';
        objApiInt1.Tenant_Id__c = '57e43429-428a-4973-9720-e6907ae94def';
        objApiInt1.Client_Id__c = 'b8f6c4e9-d493-4a2f-96f3-7d4ea68c1540';
        objApiInt1.Client_Secret__c = 'f18Ocz6Phrng4OyW_iC_.R0z.e85a38qy4';
        objApiInt1.Scope__c = 'Offline_access%20Calendars.Read%20Calendars.ReadWrite%20OnlineMeetings.Read%20OnlineMeetings.ReadWrite%20user.Read';
        objApiInt1.Refresh_Token__c = 'OAQABAAAAAAAGV_bv21oQQ4ROqh0_1-tA4P4IswFvPEVC1Y6u-NplxxTXklGFdsBBIGMYyccIgg-66bzebSXv-4NR1YEglPxSeUQPojaZyFC8kQDkUdOlXVm9oz9oRxWt1YpJFRqpKnOF1zzK9ZZkaVdn2YpOXgtuNePZZdIOhGD-aHk60havFh8RyTVjIAOU0xMjzRqOrZiz0PQOL0DQmX4PMeyNOe_S6iOlI29kk-OaUvBPwPld1Ux9M_Yva-bzNw7Fs8gGRRE_-babB8oILd8VqWPfF_tQ0CQwoIclK2xOj9OGQpEGfN514fLWw3J9PLG1gL9wb0mnLkky3oVj6powQ-WpwDYLL_6AkM0Ovs4omhjdhzeLYmR_Iblw0uhoV8-Ekpjep10wRQElKoYaMwKrhQ6bntEIXqFJ1IgedeBC3soXuCLFaKqCgP_nHb5NZo5ePOiO16kjSAsV2dnBKkjmHDC52mtm1qlFYP-nVFT2JkTHyNCeLMqNbytTJmtt51vTzzgU3eq2VPx1CPUdjw9vxQsbtbt5c49OkaGw-jszWvlTufife1Gmoy1vdkaN-Ol64dyCscfTF2zhbP4p2BEa2pIutsA8h4rV5ov79y-tj2xpPCacsAB2oz-la_QHa_n5aWP3dIuyXhVQ1_uitj8TWNjR2Nx1Vk2-Lz6AoJneGTHWkYQU1zvLh0cOuiDn1imO3pIiEPmtedqNqYu_bI6xsUnaMov5buNbmMjp5AttQuf9q_gRowJwAhPkWxZuLhdIXDA-bx5QaLFVXYRAH-8ym-giz9-vbYm7rmWm-A45oGY-STHpFPzT-8jiE31GspES6mCbQZ_926_iLm1M7gcK_MdsAkMpq5Y1LnsXO-cz0BGltcMnmXDYcrw5JvWlNF-khfSc_gvnl_0SbLZs-JsJii_10OIl0hLkQsaURecr0YZHDwREHsNcId3kqKW1uTA6PKaoyGStIuUBuXwhGREWpTIeK-TpUpUKqnWDF_i_pzJ2wH5AdGvddGgEy0ghyeaMAo8kCWkM4MEy8C-6v6dXiHMvEVdV0RsQkCbzvnrlB_9MV8MSGUxrNKGmm6ot2_7EMRnG4Ff8Hzn95QJgjLtXIaLMVOOsyeTbdoo0G9h_lcfoA0QPwSAA';
        objApiInt1.Meeting_EndPoint_URL__c = 'https://graph.microsoft.com/v1.0/me/onlineMeetings';
        objApiInt1.Event_Creation_URL__c = 'https://graph.microsoft.com/v1.0/me/calendar/events';
        objApiInt1.Redirect_URI__c = 'https://damacholding--crmuatpro.my.salesforce.com/services/apexrest/virtualhandover/';
        objApiInt1.Username__c = 'virtual.handover@damacproperties.com';
        insert objApiInt1;
        
        TriggerOnOffCustomSetting__c objCustomSetting = new TriggerOnOffCustomSetting__c();
        objCustomSetting.Name = 'CallingListTrigger';
        objCustomSetting.OnOffCheck__c = false;
        insert objCustomSetting;
        
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
      insert objAccount;
        System.debug('Account-->' +objAccount);
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAccount.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;

        Buyer__c buyer = new Buyer__c();
        buyer.Email__c = 'abc@gmail.com';
        buyer.Booking__c = objBooking.Id;
        insert buyer;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC';
        insert objBookingUnit;
        
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Account__c = objAccount.Id;
        objCallingList.MeetingDateTime__c = System.now();
        objCallingList.Booking_Unit__c = objBookingUnit.id;
        objCallingList.Meeting_Slots__c = '9:00-9:15';
        insert objCallingList;
        
        DLD_Trustee_Appointment_Slots__c dldApptSlots = new DLD_Trustee_Appointment_Slots__c();
        dldApptSlots.Appointment_Slot__c = '9:00-9:15';
        dldApptSlots.Slot_Booked__c = false;
        dldApptSlots.Trustee_Email__c = 'xyz@de.com';
        dldApptSlots.Trustee_Order__c = 1;
        dldApptSlots.Name = 'Slot1.1';
        insert dldApptSlots;

        Test.setMock(HttpCalloutMock.class, new VirtualHandoverMock());
        Test.startTest();
            ApexPages.StandardController objStdController = new ApexPages.StandardController(objCallingList);
            VirtualHandoverDLDMeetingController objController = new VirtualHandoverDLDMeetingController(objStdController);
            objController.createMeeting();
        Test.stopTest();
    }
}