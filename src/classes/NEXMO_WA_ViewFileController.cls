global class NEXMO_WA_ViewFileController{
    global String data { get; set; }
    global void loadFile () {
        data = '';
        ID recordId = apexpages.currentpage().getparameters().get('id');
        Nexmo_Whats_App_Message__c obj = new Nexmo_Whats_App_Message__c();
        obj = [SELECT message_body_url__c FROM Nexmo_Whats_App_Message__c WHERE ID =: recordId];
        if (obj.message_body_url__c != NULL) 
            data = download(obj.message_body_url__c);
    }
    global static String download(String mediaUrl) {
        String contentType = '';
        String data = '';
        if ( mediaUrl != null && mediaUrl != '' ) {
            map<string,string> credentials = NEXMO_WA_MESSAGING.getCredentials();
            string JWTToken = PrepareJWTToken();
            if(credentials.size() > 0){ 
                HttpRequest req = new HttpRequest();            
                req.setEndpoint(mediaUrl);
                req.setHeader('Authorization','Bearer '+JWTToken);
                req.setHeader('Content-Type','application/json');
                req.setMethod('GET');        
                Http http = new Http();
                HTTPResponse res;
                if(!test.isRunningTest()){
                    res = http.send(req);
                    System.debug(req);
                    System.debug(res.getBody());
                    System.debug(res.getstatuscode());
                    if( res.getstatuscode() == 200 || res.getstatuscode() == 202){
                        data = EncodingUtil.Base64Encode(res.getBodyAsBlob());
                        for (String key : res.getHeaderKeys()) {
                            if (key.toLowerCase() == 'content-type') {
                                data = 'data:'+res.getHeader(key)+';base64,'+data;
                            }
                        }
                    }
                }
            }
        }
        return data;
        
        
    }
    public static string PrepareJWTToken(){
        string JWTAssertion='';
        map<string,string> credentials = NEXMO_WA_MESSAGING.getCredentials();
        string appId = credentials.get('appid');
        system.debug('APPLICATION ID=='+credentials.get('appid'));
        String JWTHeader = '{"alg": "RS256","typ": "JWT"}';
        JWTHeader = NEXMO_WA_MESSAGING.base64URLencode (Blob.valueOf(JWTHeader));
        Long rightNow = (dateTime.now().getTime()/1000);
        Long expireTime = datetime.now().addHours(24).getTime()/1000;
        String JWTClaim = '{"application_id": "'+appId+'",'
            +'"iat": "'+rightNow+'","jti": "'+rightNow +'0000087", "exp" : "'+expireTime+'"}';
        System.Debug (JWTClaim);
        JWTClaim = NEXMO_WA_MESSAGING.base64URLencode (Blob.valueOf(JWTClaim));
        String privateKey = '';
        
        Document doc = NEW Document ();
        doc = [ SELECT BODY FROM Document WHERE Name = 'JWT_Private_Key'];
        privateKey = (doc.Body).toString ();
        privateKey = privateKey .deleteWhiteSpace ();
        privateKey = privateKey .remove ('\n');
        
        
        String modifiedPrivateKey = NEXMO_WA_MESSAGING.prepareStringForBase64Decoding (privateKey);
        Blob privateKeyBlob = EncodingUtil.base64Decode (modifiedPrivateKey);
        Blob JWTSignature = Crypto.sign('RSA-SHA256', 
                                        Blob.valueof(JWTHeader +'.'+JWTClaim), 
                                        privateKeyBlob);
       JWTAssertion = JWTHeader +'.'+JWTClaim +'.'+NEXMO_WA_MESSAGING.base64URLencode (JWTSignature);
        System.Debug (JWTAssertion);
        return JWTAssertion;        
    }

}