/**
 * Description: Test class for LHOCancellationController
 */
@isTest
private class LHOCancellationControllerTest {

    @isTest static void testcancelLHO() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id leaseHandoverRecordTypeID = getRecordTypeIdForLeaseHO();

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , leaseHandoverRecordTypeID);
        objCase.Status = 'Submitted';
        insert objCase;


        // Creation of Task
        Task objTask = new Task();
        objTask.subject = 'Test Subject';
        objTask.whatId = objCase.Id;
        objTask.status = 'In Progress';
        insert objTask;

        Test.startTest();
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            LHOCancellationController  objController = new LHOCancellationController (standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.LHOCancellation'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            objController.cancelLHO();
        Test.stopTest();
    }

    @isTest static void testcancelLHOWithNoTask() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id leaseHandoverRecordTypeID = getRecordTypeIdForLeaseHO();

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , leaseHandoverRecordTypeID);
        objCase.Status = 'Submitted';
        insert objCase;

        Test.startTest();
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            LHOCancellationController  objController = new LHOCancellationController (standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.LHOCancellation'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            objController.cancelLHO();
        Test.stopTest();
    }
    
    @isTest static void testcancelLHOWithNoSubmit() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id leaseHandoverRecordTypeID = getRecordTypeIdForLeaseHO();

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , leaseHandoverRecordTypeID);
        objCase.Status = 'New';
        insert objCase;

        Test.startTest();
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            LHOCancellationController  objController = new LHOCancellationController (standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.LHOCancellation'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            objController.cancelLHO();
        Test.stopTest();
    }
    /**
     * Method to get the "Lease Handover" record type
     */
    private static Id getRecordTypeIdForLeaseHO() {
      Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id leaseHandoverRecordTypeID = caseRecordTypes.get('Lease Handover').getRecordTypeId();
      return leaseHandoverRecordTypeID;
    }
}