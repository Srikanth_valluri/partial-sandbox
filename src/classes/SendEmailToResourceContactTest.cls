@isTest
private class SendEmailToResourceContactTest{
	@isTest
	static void itShould(){
		

		//EmailTemplate e = new EmailTemplate (developerName = 'Booking_Info', TemplateType= 'Text', Name = 'test'); 
		//insert e;

		account acc = new account();
        acc.Name = 'Test Account1';
        acc.party_ID__C = '103903';
        insert acc;

		insert new Contact(LastName='Test');
    	Resource__c objRes = new Resource__c();
        objRes.Chargeable_Fee_Slot__c = 10;
        objRes.Non_Operational_Days__c = 'Sunday';
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.No_of_advance_booking_days__c = 2;
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.Deposit__c = 3;
		objRes.Contact_Person_Email__c = 'test@test.com';
        objRes.No_of_deposit_due_days__c = 1;
        insert objRes;

		Inquiry__c inq=new Inquiry__c();
        insert inq;
        
        Email_message__c msg=new Email_message__c();
        msg.Inquiry__c=inq.id;
        msg.CC_Address__c='test@test.com';
        msg.BCC_Address__c='Test@test2.com';
        insert msg;

		Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr1 = new NSIBPM__Service_Request__c();
        sr1.NSIBPM__Customer__c = acc.id;
        sr1.RecordTypeId = RecType;
        insert sr1;

        booking__c bk1 = new booking__c();
        bk1.Account__c = acc.id;
        bk1.Deal_SR__c = sr1.id;
        insert bk1;

        booking_unit__c bu1 = new booking_unit__c();
        bu1.Booking__c = bk1.id;
        bu1.Unit_Name__c = 'LSB/10/B1001';
        bu1.Owner__c = acc.id;
        bu1.Property_City__c = 'Dubai';
        insert bu1;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
		fmCaseObj.Booking_Unit__c = bu1.id;
		fmCaseObj.Resource__c = objRes.id;
		fmCaseObj.Booking_Date__c =Date.today();
		fmCaseObj.Tenant_Email__c = 'test@gmail.com';
		fmCaseObj.Booking_Start_Time_p__c = '10:00';
		fmCaseObj.Booking_End_Time_p__c = '11:00';
		fmCaseObj.Amenity_Booking_Status__c = 'Booking Confirmed';
		fmCaseObj.Initiated_by_tenant_owner__c = 'Tenant';
		fmCaseObj.Tenant__c = acc.id;
		fmCaseObj.Account__c = acc.id;
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        //fmCaseObj.Approving_Authorities__c='FM Manager';
        fmCaseObj.Nationality__c ='Pakistani';
        fmCaseObj.Mobile_no__c = '123456789';
        fmCaseObj.Outstanding_service_charges__c = '0';
        fmCaseObj.No_of_Adults__c = '5';
        insert fmCaseObj;
		list<Id> lstFMCaseId = new list<Id>();
		lstFMCaseId.add(fmCaseObj.id);

    	SendEmailToResourceContact controller=new SendEmailToResourceContact();
        SendEmailToResourceContact.sendEmail(lstFMCaseId);
	}

	@isTest
	static void itShouldNot(){
		

		//EmailTemplate e = new EmailTemplate (developerName = 'Booking_Info', TemplateType= 'Text', Name = 'test'); 
		//insert e;

		account acc = new account();
        acc.Name = 'Test Account1';
        acc.party_ID__C = '103903';
        insert acc;

		insert new Contact(LastName='Test');
    	Resource__c objRes = new Resource__c();
        objRes.Chargeable_Fee_Slot__c = 10;
        objRes.Non_Operational_Days__c = 'Sunday';
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.No_of_advance_booking_days__c = 2;
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.Deposit__c = 3;
		objRes.Contact_Person_Email__c = 'test@test.com';
        objRes.No_of_deposit_due_days__c = 1;
        insert objRes;

		Inquiry__c inq=new Inquiry__c();
        insert inq;
        
        Email_message__c msg=new Email_message__c();
        msg.Inquiry__c=inq.id;
        msg.CC_Address__c='test@test.com';
        msg.BCC_Address__c='Test@test2.com';
        insert msg;

		Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr1 = new NSIBPM__Service_Request__c();
        sr1.NSIBPM__Customer__c = acc.id;
        sr1.RecordTypeId = RecType;
        insert sr1;

        booking__c bk1 = new booking__c();
        bk1.Account__c = acc.id;
        bk1.Deal_SR__c = sr1.id;
        insert bk1;

        booking_unit__c bu1 = new booking_unit__c();
        bu1.Booking__c = bk1.id;
        //bu1.Unit_Name__c = 'LSB/10/B1001';
        bu1.Owner__c = acc.id;
        bu1.Property_City__c = 'Dubai';
        insert bu1;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
		fmCaseObj.Booking_Unit__c = bu1.id;
		fmCaseObj.Resource__c = objRes.id;
		//fmCaseObj.Booking_Date__c =Date.today();
		fmCaseObj.Tenant_Email__c = 'test@gmail.com';
		//fmCaseObj.Booking_Start_Time_p__c = '10:00';
		//fmCaseObj.Booking_End_Time_p__c = '11:00';
		//fmCaseObj.Amenity_Booking_Status__c = 'Booking Confirmed';
		fmCaseObj.Initiated_by_tenant_owner__c = 'Owner';
		fmCaseObj.Tenant__c = acc.id;
		fmCaseObj.Account__c = acc.id;
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        //fmCaseObj.Approving_Authorities__c='FM Manager';
        fmCaseObj.Nationality__c ='Pakistani';
        fmCaseObj.Mobile_no__c = '123456789';
        fmCaseObj.Outstanding_service_charges__c = '0';
        fmCaseObj.No_of_Adults__c = '5';
        insert fmCaseObj;
		list<Id> lstFMCaseId = new list<Id>();
		lstFMCaseId.add(fmCaseObj.id);

    	SendEmailToResourceContact controller=new SendEmailToResourceContact();
        SendEmailToResourceContact.sendEmail(lstFMCaseId);
	}
}