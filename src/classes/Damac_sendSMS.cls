//Test class Name: Damac_SecureSMS_Test
/*
Changes:
May 28 2019 - Added Logic to check if the Response has Errors and to capture the Errors.
*/

Public with sharing class Damac_sendSMS {

    @Future (Callout = TRUE)
    public static void sendSecureMessage (set<ID> standInquiries,string objectName) {        
        sendSecureMessageSync (standInquiries, false,objectName, '');        
    }

    
    public static void sendSecureMessageSync (set<ID> standInquiries, Boolean retry, string objectName, string messageReturn) {
        Secure_SMS__mdt msgSettings = new Secure_SMS__mdt ();
        List <Log__c> logsToinsert = new List<log__c> ();
        List <sobject> standInquiriesToUpdate = new List <sobject> ();
        msgSettings = [SELECT User_Name__c, Password__c, Endpoint__c, Sender_Name__c FROM Secure_SMS__mdt WHERE Label = 'SMS_SingleAPI'];
        string queryStr;
        if(objectName == 'Inquiry__c'){
            queryStr = 'SELECT Retry_Count__c, Retried_Date_Time__c, Mobile_CountryCode__c, SMS_Request_Status__c, SMS_Message__c, SMS_MSG_Id__c, Mobile_Phone__c,Mobile_Phone_Encrypt__c  FROM '+objectName+' WHERE ID IN: standInquiries';
        }else{
            queryStr = 'SELECT Retry_Count__c, Retried_Date_Time__c, Mobile_CountryCode__c, SMS_Request_Status__c, SMS_Message__c, SMS_MSG_Id__c, Mobile_Phone_Encrypt__c FROM '+objectName+' WHERE ID IN: standInquiries';
        }
        for(sobject inq: database.query(queryStr)){
        
            //for (sobject inq: [SELECT Retry_Count__c, Retried_Date_Time__c, Mobile_CountryCode__c, SMS_Request_Status__c, SMS_Message__c, SMS_MSG_Id__c, Mobile_Phone_Encrypt__c 
            //FROM Stand_Inquiry__c WHERE ID IN: standInquiries]) {
            
            if (inq.get('Mobile_Phone_Encrypt__c') != NULL) {
                 String mobilePhone;
                if(objectName!='Inquiry__c'){
                    mobilePhone = string.valueof(inq.get('Mobile_CountryCode__c')).split (':')[1].trim ();
                    mobilePhone = mobilePhone.removeStart ('00')+inq.get('Mobile_Phone_Encrypt__c');
                }
                if(objectName =='Inquiry__c'){
                    mobilePhone = string.valueof(inq.get('Mobile_Phone__c'));
                    mobilePhone = mobilePhone.removeStart ('00');
                }
                
                String message = ''+inq.get('SMS_Message__c');
                if (messageReturn != '' && messageReturn != null) {
                    message = messageReturn;
                }
                //Added by Charan 2nd Nov 2020
                string senderName = system.label.Common_Sender_Name;
                if(inq.get('Mobile_CountryCode__c') == 'United Arab Emirates: 00971')
                    senderName = System.Label.UAE_Sender_Name;
                if(inq.get('Mobile_CountryCode__c') == 'Saudi Arabia: 00966')
                    senderName = System.Label.Saudi_Sender_Name;
                
                String reqBody = '{"MobileNumbers" : "'+mobilePhone+'", "Message" : "'+message+'", '
                            +'"SenderName" : "'+senderName +'", "CallbackQueryString" : "InquiryId='+inq.Id+'","ReportRequired":true}';
                 system.debug('reqBody=='+reqBody);           
                String endPoint = msgSettings.Endpoint__c+'?UserName='+msgSettings.User_Name__c+'&Password='+msgSettings.Password__c;
                HttpRequest req = new HttpRequest ();
                req.setEndPoint (endPoint);
                req.setMethod ('POST');
                req.setBody (reqBody);
                req.setTimeout(120000);
                req.setHeader ('Content-Type', 'application/JSON');
                
                HTTp http = new HTTP ();
                
                HttpResponse res = new HttpResponse ();
                if (!Test.isRunningTest ()) {
                    res = http.send (req);
                }
                if (Test.isRunningTest ()) {
                    res.setStatusCode (200);
                    res.setBody ('{"Status": "OK","Data": [{"mobileNo": "971561985082","status": "OK","details": "Message Sent","creditsUsed": "0.054000","msgId": 900223640}]}');
                }
                if (res.getStatusCode () != 200) {
                    Log__c log = new Log__c ();
                    log.Type__c = 'Single SMS Fail';
                    log.Description__c = res.getBody ();
                    log.Notification_Email__c = label.SMS_Log_Email;
                    logsToinsert .add (log);
                } else {
                    system.debug('RESPONSE =='+res.getBody ());
                    Damac_SendSMSResponse response = Damac_SendSMSResponse.parse (res.getBody ());
                    if( response.Status == 'OK' ){ // SMS submitted to Provider successfully
                        for (Damac_SendSMSResponse.Data data: response.Data) {
                            inq.put('SMS_Request_Status__c',data.details);
                            inq.put('SMS_MSG_Id__c',String.valueOf (data.msgId)); 
                        }
                    }else if( response.Status == 'ERROR' ){ // Check if Provider sent any error
                        system.debug(response.ErrorDescription);
                        inq.put('SMS_Request_Status__c',response.ErrorDescription);
                    }
                }
                if (retry) {
                    if (inq.get('Retry_Count__c') == NULL) {
                        inq.put('Retry_Count__c',1);
                    } else {
                        inq.put('Retry_Count__c',integer.valueof(inq.get('Retry_Count__c')) + 1);                        
                    }
                    inq.put('Retried_Date_Time__c',DateTime.Now ());
                }
                standInquiriesToUpdate.add (inq);
                system.debug(standInquiriesToUpdate.size());
            }
        }
        if (standInquiriesToUpdate.size () > 0) {
            update standInquiriesToUpdate;
        }
        if (logsToinsert.size () > 0) {
            insert logsToinsert ;
        }
    }    
}