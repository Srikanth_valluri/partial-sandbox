@isTest
public class DAMAC_AMEYO_CUSTOMER_TEST {
    
    public static testMethod void method1 () {
        Ameyo_Credentials__c credentials = new Ameyo_Credentials__c ();
        credentials.Endpoint__c = 'www.test.com';
        credentials.hash_key__c = 'test';
        credentials.host__c = 'test.com';
        credentials.policy_name__c = 'test';
        credentials.requesting_host__c = 'test.com';
        insert credentials;
        
        
        Inquiry__c inqObj= new Inquiry__c(Is_Meeting_Scheduled__c= true,
                                        Encrypted_Mobile__c='test',
                                        Meeting_Due_Date__c = Date.today (),
                                        Pre_InquiryId__c='123456',
                                        Mobile_Phone_Encrypt__c='456123',
                                        Ameyo_Callback_Id__c = 'test',
                                        Mobile_CountryCode__c='American Samoa: 001684',
                                        mobile_phone__c='1234',email__c='mk@gmail.com',
                                        First_Name__c='Test',Last_Name__c='Last',
                                        CR_Number__c='0987',
                                        Organisation_Name__c = 'Oliver');
        insert inqObj;
        
        Ameyo_log__c log = new Ameyo_log__c ();
        log.inquiry__c = inqObj.id;
        log.CrtObjectId__c = inqObj.id;
        insert log;
        
        List <ID> inqIds = new List <ID> ();
        inqIds.add (inqObj.id);
        
        DAMAC_AMEYO_CUSTOMER.setCustomerManager (inqIds);
        DAMAC_AMEYO_DELETE_CALLBACK.deleteAmeyoCallback (inqIds);
        string s = '{"beanResponse":{"id":"d462-5dbfbbdb-cm-23599","campaignId":12,"callBackTime":"2019-11-27 09:59:51.617","selfCallback":false,"callBackProperties":{"customerId":"8645996","phone":"dcecc9c3a4c0f068e3bacc75b450d45e"},"userId":"apiuser","phone":"dcecc9c3a4c0f068e3bacc75b450d45e","customerId":8645996}}';
        DAMAC_AMEYO_CALLBACK_RESPONSE respcls = DAMAC_AMEYO_CALLBACK_RESPONSE.PARSE(s);
        
        DAMAC_AMEYO_CUSTOMER_CALLBACK.setCustomerManagerCallBack (inqIds);
        
    }
    
    static testMethod void testParse() {
        String json = '{"campaignId":3,"customerAndCallbackRecords":[{"customerRecord": '
                        +'{"id":"123411","phone1":"1234567890"}}'
                        +',{"customerRecord": {"id":"deepak","phone1":"1234567880"}}]'
                        +',"leadId":26,"properties": {"update_customer": true}, "attempts":2}';
        DAMAC_AMEYO_CUSTOMER_JSON obj = DAMAC_AMEYO_CUSTOMER_JSON.parse(json);
    }
    
    static testMethod void testParseCallBack() {
        String json=        '{'+
        '   "campaignId": 2,'+
        '   "customerAndCallbackRecords": [{'+
        '       "callbackRecord": {'+
        '           "id": "sfid",'+
        '           "userId": "user_fedarationid",'+
        '           "callBackPhone": "encrypted_mobile__c",'+
        '           "isSelfCallBack": true,'+
        '           "callBackTime": "2019-10-27 19:30:00 PM",'+
        '           "customerId": "265859"'+
        '       }'+
        '   }],'+
        '   "properties": {'+
        '       "update.customer": true'+
        '   }'+
        '}';
        DAMAC_AMEYO_CALLBACK_JSON obj = DAMAC_AMEYO_CALLBACK_JSON.parse(json);
    }
    
}