/****************************************************************************************************************
* Name                  : ContactTriggerHandler_Test                                                            *
* Trigger               : ContactTrigger                                                                        *
* Class Covered         : ContactTriggerHandler                                                                 *
* Created By            : NSI - Sivasankar K                                                                    *
* Created Date          : 20/02/2017                                                                            *
* Last Modified Date    :                                                                                       *
* Last Modified By      :                                                                                       *
* ------------------------------------------------------------------------------------------------------------  *
*VER    AUTHOR                      DATE        Description                                                     *
*1.0    NSI - Sivasankar K          20/02/2017  Initial developmen                                              *
*2.0    Craig Lobo                  26-07-2018  Updated the Test Coverage                                       *
*****************************************************************************************************************/
@isTest
public class ContactTriggerHandler_Test {
    
    testmethod static void testData() {
        
        List<Account> insertAccounts = new List<Account>();
        Account blacklistAcc = InitialiseTestData.getBlacklistedAccount('Blacklist Account');
        insertAccounts.add(blacklistAcc);
        Account corporateAcc = InitialiseTestData.getCorporateAccount('Damac Test Account');
        insertAccounts.add(corporateAcc);
        Account terminateAcc = InitialiseTestData.getTerminatedAccount('Damac Test Termination');
        
        insertAccounts.add(terminateAcc);
        
        Id businessContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('DAMAC Contact').getRecordTypeId();
        List<Contact> contactList = new List<Contact>();
        Contact corporateContactDamac = InitialiseTestData.getCorporateAgencyContact('Damac');
        corporateContactDamac.RecordTypeid = businessContactRecordTypeId;
        corporateContactDamac.Mobile_Phone_Encrypt__c = '02124587411';
        corporateContactDamac.Mobile_Country_Code__c = 'India: 0091';
        corporateContactDamac.Mobile_Phone_Encrypt_2__c = '02124587411';
        corporateContactDamac.Mobile_Country_Code_2__c = 'India: 0091';
        corporateContactDamac.Mobile_Country_Code_3__c = 'India: 0091';
        corporateContactDamac.Mobile_Phone_Encrypt_3__c = '02124587411';
        corporateContactDamac.Mobile_Country_Code_4__c = 'India: 0091';
        corporateContactDamac.Mobile_Phone_Encrypt_4__c = '02124587411';
        corporateContactDamac.Mobile_Phone_Encrypt_5__c = '02124587411';
        corporateContactDamac.Mobile_Country_Code_5__c = 'India: 0091';
        contactList.add(corporateContactDamac);
        
        ContactTriggerHandler instance = new ContactTriggerHandler();
        Contact corporateContactDamactest = InitialiseTestData.getCorporateAgencyContact('Damactest');
        corporateContactDamactest.Mobile_Phone_Encrypt__c = '02124587411';
        corporateContactDamactest.Mobile_Country_Code__c = '';
        instance.validateMobileNumbers(corporateContactDamactest);
        corporateContactDamactest.Mobile_Phone_Encrypt_2__c = '02124587411';
        corporateContactDamactest.Mobile_Country_Code_2__c = '';
        instance.validateMobileNumbers(corporateContactDamactest);
        corporateContactDamactest.Mobile_Country_Code_3__c = '';
        corporateContactDamactest.Mobile_Phone_Encrypt_3__c = '02124587411';
        instance.validateMobileNumbers(corporateContactDamactest);
        corporateContactDamactest.Mobile_Country_Code_4__c = '';
        corporateContactDamactest.Mobile_Phone_Encrypt_4__c = '02124587411';
        instance.validateMobileNumbers(corporateContactDamactest);
        corporateContactDamactest.Mobile_Phone_Encrypt_5__c = '02124587411';
        corporateContactDamactest.Mobile_Country_Code_5__c = '';
        instance.validateMobileNumbers(corporateContactDamactest);
        corporateContactDamactest.Mobile_Phone_Encrypt__c = '';
        corporateContactDamactest.Mobile_Country_Code__c = 'India: 0091';
        instance.validateMobileNumbers(corporateContactDamactest);
        corporateContactDamactest.Mobile_Phone_Encrypt_2__c = '';
        corporateContactDamactest.Mobile_Country_Code_2__c = 'India: 0091';
        instance.validateMobileNumbers(corporateContactDamactest);
        corporateContactDamactest.Mobile_Country_Code_3__c = 'India: 0091';
        corporateContactDamactest.Mobile_Phone_Encrypt_3__c = '';
        instance.validateMobileNumbers(corporateContactDamactest);
        corporateContactDamactest.Mobile_Country_Code_4__c = 'India: 0091';
        corporateContactDamactest.Mobile_Phone_Encrypt_4__c = '';
        instance.validateMobileNumbers(corporateContactDamactest);
        corporateContactDamactest.Mobile_Phone_Encrypt_5__c = '';
        corporateContactDamactest.Mobile_Country_Code_5__c = 'India: 0091';
        instance.validateMobileNumbers(corporateContactDamactest);
        contactList.add(corporateContactDamactest);
        
        Contact testcorporateContactDamac = InitialiseTestData.getCorporateAgencyContact('Damac');
        contactList.add(testcorporateContactDamac);
        
        insert contactList;
        
        List<User> portalUsersList = new List<User>();
        
        User portalUser = InitialiseTestData.getPortalUser('testUser@damac.com',corporateContactDamac.Id);
        portalUser.isActive = true;
        portalUsersList.add(portalUser);
        
        User portalUser1 = InitialiseTestData.getPortalUser('testUser1@damac.com',corporateContactDamactest.Id);
        portalUser1.isActive = true;
        portalUsersList.add(portalUser1);

        insert portalUsersList;
        
        delete testcorporateContactDamac;
        
        try{
            portalUser1.isActive = false;
            update portalUser1;
        }Catch(Exception ex){
            
        }
        
    } 
}