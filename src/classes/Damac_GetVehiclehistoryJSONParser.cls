public class Damac_GetVehiclehistoryJSONParser{
    public Integer status;  //200
    public cls_results[] results;
    public String message;  //
    public class cls_results {
        public String IMEINo;   //868324020706490
        public String TimeStamp;    //2019-07-02T00:06:43+04:00
        public Double Latitude; //25.22728
        public Double Longitude;    //55.2780516
        public Integer Angle;   //309
        public Integer Speed;   //0
        public boolean IsEngineOn;
        public Integer TotalMilage; //970000
        public String VehicleID;    //20f4ed22-d3f6-4ffb-ac55-63b9efeea9af
        public String VehicleName;  //N 49213 
        public String VehicleGroupID;   //00000000-0000-0000-0000-000000000000
        public String VehicleGroupName; //
        public Integer VehicleType; //2
        public String Driver;   //
        public String LastEngineOn; //0001-01-01T00:00:00
        public String LastRunningOn;    //0001-01-01T00:00:00
        public Integer TrackingStatus;  //3
        public String CurrentLocation;  //26548 91245, 19 Street, Al Satwa Dubai, United Arab Emirates
        public Integer FuelLevel;   //0
        public Integer IsSeatBeltOn;    //0
        public Integer IsDoorOpen;  //-1
        public Integer IsAcOn;  //-1
        public Integer IsEquipmentOn;   //-1
        public Integer EngineTemp;  //0
        public Integer EngineRpm;   //0
        public boolean RemoteImmobilization;
    }
    public static Damac_GetVehiclehistoryJSONParser parse(String json){
        return (Damac_GetVehiclehistoryJSONParser) System.JSON.deserialize(json, Damac_GetVehiclehistoryJSONParser.class);
    }
}