/****************************************************************************************************
* Name          : ScheduleUserActionPlan                                                            *
* Description   : Scheduler to invoke the CreateActionPlanBatch class                               *
* Created Date  : 27/03/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    27/03/2018      Initial Draft.                                          *
****************************************************************************************************/
global class ScheduleUserActionPlan implements Schedulable {
    global void execute(SchedulableContext SC) {
        CreateActionPlanBatch actionPlanBatch = new CreateActionPlanBatch();
       database.executebatch(actionPlanBatch);
   }
}