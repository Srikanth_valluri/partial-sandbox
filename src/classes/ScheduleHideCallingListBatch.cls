public with sharing class ScheduleHideCallingListBatch implements Schedulable {
    public void execute(SchedulableContext SC){
    	HideCallingListBatch batchInst = new HideCallingListBatch();
    	Database.executeBatch(batchInst);
    }
}