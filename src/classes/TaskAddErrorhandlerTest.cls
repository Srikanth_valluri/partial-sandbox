@isTest
public class TaskAddErrorhandlerTest {
	public static testmethod void testmethod1(){
        Profile profRec=[select id from profile where name ='System Administrator'];
        User u= new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profRec.id, country='United States',IsActive =true,
                    timezonesidkey='America/Los_Angeles', username='testermee@noemail.com');
        insert u;

        Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Work Permit').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Task_Id__c=123;
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Work_Permit';
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
        insert fmCaseObj;
        Task taskIns=new Task();
        taskIns.whatId=fmCaseObj.id;
        taskIns.OwnerId=u.id;
        taskIns.status='Open';
        taskIns.subject = 'Generate Work Permit';
        insert taskIns;
        SR_Attachments__c insAttach = new SR_Attachments__c();
        insAttach.name = 'Work Permit';

        insAttach.FM_Case__c =fmCaseObj.id;
        insert insAttach;
        try{
            taskIns.status = 'Closed';
            update taskIns;
        }
        catch(Exception e){

        }
    }

    public static testmethod void test_accessCardTask(){
        Profile profRec=[select id from profile where name ='System Administrator'];
        User u= new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profRec.id, country='United States',IsActive =true,
                    timezonesidkey='America/Los_Angeles', username='testermee@noemail.com');
        insert u;

        Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Request For Access Card').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Task_Id__c=123;
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Work_Permit';
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
        insert fmCaseObj;
        Task taskIns=new Task();
        taskIns.whatId=fmCaseObj.id;
        taskIns.OwnerId=u.id;
        taskIns.process_name__c = 'Request For Access Card';
        taskIns.status='Open';
        taskIns.subject = Label.FM_Access_Card_Issue_task_label;
        insert taskIns;

        try{
            taskIns.status = 'Closed';
            update taskIns;
        }
        catch(Exception e){

        }
    }
}