/**
 * @File Name          : PopulateSOAForDPInvoiceBatch.cls
 * @Description        : This Batch is used to populate SOA 
 *                        for DPInvoice.
 * @Author             : Dipika Rajput
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 7/3/2019, 5:45:08 PM
 * @Modification Log   : 
 *==============================================================================================
 * Ver         Date                     Author                    Modification
 * 
 *==============================================================================================
 * 1.0    6/25/2019                   Dipika Rajput           Initial Version
**/
public without sharing class PopulateTaxInvoiceForDpInvoiceBatch implements Database.Batchable<sObject>,
    Database.AllowsCallouts, Database.Stateful {
        
    private String query; 
    public map <Id,list<string>> mapDPInvoiceWithURLs;
    

    public PopulateTaxInvoiceForDpInvoiceBatch() {
      
      mapDPInvoiceWithURLs = new map <Id,list<string>>();
    }
     public Database.Querylocator start( Database.BatchableContext bc ) {
        query = ' SELECT Id,'+ 
                ' Cover_Letter__c, '+
                ' SOA__c,'+ 
                ' Trxn_Number__c,'+
                ' TAX_Invoice__c,'+
                ' BookingUnits__c,'+
                ' BookingUnits__r.Registration_ID__c,'+
                ' COCD_Letter__c, '+
                ' Other_Language_TAX_Invoice__c '+
                ' FROM DP_Invoices__c '+
                ' Where '+
                ' TAX_Invoice__c = NULL '+
                ' AND Other_Language_TAX_Invoice__c = NULL';
        System.debug('Dp Invoice query ::: '+Database.getQueryLocator(query));
        return Database.getQueryLocator(query);
    }
    public void execute( Database.BatchableContext bc, list<DP_Invoices__c> dpInvoiceRecords ) {
        System.debug('dpInvoiceRecords ::: '+dpInvoiceRecords);
        // Iterate on Dp invoice records
        for(DP_Invoices__c dpInvoiceObj : dpInvoiceRecords){

                // Call SOA Language specific service which will give me all the list of invoice Tax urls
                list<string> strTaxUrlResponseList = FmIpmsRestCoffeeServices.getInvoiceByCustTrxIdInLang(
                    dpInvoiceObj.Trxn_Number__c 
                );
                System.debug('strTaxUrlResponseList ::: '+strTaxUrlResponseList);
                //System.debug('strTaxUrlResponseList ::: '+strTaxUrlResponseList.size());
                if(strTaxUrlResponseList != NULL && !strTaxUrlResponseList.isEmpty() && strTaxUrlResponseList.size()>0){
                    
                    // Send TaxUrl Response list specific to dp Invoice record 
                    calloutToGetBlob(strTaxUrlResponseList, dpInvoiceObj);

                }
                System.debug('dpInvoiceObj ::: '+dpInvoiceObj);
        }
        System.debug('mapDPInvoiceWithURLs = ' + mapDPInvoiceWithURLs);

    }
    public void finish( Database.BatchableContext bc ) {
        System.debug('mapDPInvoiceWithURLs in finish= ' + mapDPInvoiceWithURLs);
        list<DP_Invoices__c> lstDPInvoicesToUpdate = new list<DP_Invoices__c>();
        
        for(DP_Invoices__c objDPInvoice : [SELECT id,
                                                  Other_Language_SOA__c,
                                                  SOA__c 
                                             FROM DP_Invoices__c 
                                            WHERE id in: mapDPInvoiceWithURLs.keyset()]){
            list<string> lstURLs = new list<string>();
            lstURLs = mapDPInvoiceWithURLs.get(objDPInvoice.id);
            System.debug('lstURLs in finish= ' + lstURLs);
            if(!lstURLs.isEmpty()){
                System.debug('lstURLs in finish= ' + lstURLs.size());
                if(lstURLs.size() > 1){
                    objDPInvoice.TAX_Invoice__c = lstURLs[0];
                    objDPInvoice.Other_Language_TAX_Invoice__c = lstURLs[1];
                } else {
                    objDPInvoice.TAX_Invoice__c = lstURLs[0];
                }   
            }   
            lstDPInvoicesToUpdate.add(objDPInvoice);                  
        }
        System.debug('lstDPInvoicesToUpdate in finish= ' + lstDPInvoicesToUpdate);
        if(!lstDPInvoicesToUpdate.isEmpty()){
            update lstDPInvoicesToUpdate;
        }
    }

    /* This Method is used to get the blob of the Tax urls for specific dpInoice record */
    public void calloutToGetBlob ( list<string> lstTaxResponse, DP_Invoices__c dpInvoiceRecord ){
        list<Blob> lstBlob = new list<blob>();
        String accessToken = FmIpmsRestCoffeeServices.accessToken;
        map<DP_Invoices__c,List<Blob>> dpInvoiceToTaxUrlBlobsMap = new map<DP_Invoices__c,List<Blob>>();
        System.debug('AccessToken is' + accessToken);
      
        if(lstTaxResponse != NULL && !lstTaxResponse.isEmpty()){
            for(string objResponse: lstTaxResponse) {
                HttpRequest req = new HttpRequest();
                req.setEndpoint(objResponse);
                req.setHeader('Accept', 'application/json');
                req.setMethod('GET');
                req.setHeader('Authorization','Bearer' + accessToken);
                req.setTimeout(120000);
                HttpResponse response = new Http().send(req);
                System.debug('Status Code = ' + response.getStatusCode());
                System.debug('Pdf Body = ' + response.getBodyAsBlob());

                if(response.getStatusCode() == 200) {
                    blob resBlob =  response.getBodyAsBlob();
                    if(resBlob != NULL){
                        lstBlob.add(resBlob);
                    }                    
                }
            }            
        }
        System.debug('lstBlob = ' + lstBlob);
        if(!dpInvoiceToTaxUrlBlobsMap.containsKey(dpInvoiceRecord)){
            dpInvoiceToTaxUrlBlobsMap.put(
                dpInvoiceRecord,
                lstBlob
            );
        } 
        System.debug('dpInvoiceToTaxUrlBlobsMap = ' + dpInvoiceToTaxUrlBlobsMap);  
        SendContentToCentralRepository.getPublicUrlsOfSOAForDpInvoice(dpInvoiceToTaxUrlBlobsMap);
        System.debug('SendContentToCentralRepository.mapAttachURLwithDPInvoice = ' + 
            SendContentToCentralRepository.mapAttachURLwithDPInvoice); 
        for(Id DPInvoiceId: SendContentToCentralRepository.mapAttachURLwithDPInvoice.keyset()){
            mapDPInvoiceWithURLs.put(
                DPInvoiceId,
                SendContentToCentralRepository.mapAttachURLwithDPInvoice.get(DPInvoiceId));
        }
        System.debug('mapDPInvoiceWithURLs = ' + mapDPInvoiceWithURLs); 
    }
}