/*
 * Description - Class used for creating EmailMessage records for the notifications sent to customer
 *
 * Version        Date            Author            Description
 * 1.0            12/04/18        Vivek Shinde      Initial Draft
 */
public with sharing class AOPTNotificationManager {
    
    //Method called from process builder to create EmailMessage records
    @InvocableMethod
    public static void createEmailMessages(List<Case> lstCase) {
        List<Case> lstCase21DayNotification = new List<Case>();
        List<Case> lstCase30DayNotification = new List<Case>();
        List<Case> lstCaseAddnmNotification = new List<Case>();
        
        for(Case objCase: lstCase) {
            if(objCase.Is_30th_Day__c) {
                lstCase30DayNotification.add(objCase);
            } else if(objCase.Is_21st_Day__c) {
                lstCase21DayNotification.add(objCase);
            } else if(objCase.Offer_Acceptance_Letter_Generated__c) {
                lstCaseAddnmNotification.add(objCase);
            }
        }
        
        List<EmailMessage> lstEmailMesssage = new List<EmailMessage>();
        String strFromEmail = 'no-replysf@damacgroup.com';
        
        if(!lstCase30DayNotification.isEmpty()) {
            String strSubject = 'Payment Extension Cancellation Notification';
            String strTextBody = Label.AOPT30DayNotificationText1;
            
            for(Case objCase: lstCase30DayNotification) {
                EmailMessage objEmailMessage = new EmailMessage(
                    Status = '3',
                    ParentId = objCase.Id,
                    FromAddress = strFromEmail,
                    ToAddress = objCase.Account_Email__c,
                    Subject = strSubject,
                    TextBody = strTextBody);
                lstEmailMesssage.add(objEmailMessage);
            }
        }
        else if(!lstCase21DayNotification.isEmpty()) {
            String strSubject = 'Payment Extension Cancellation Notification';
            String strTextBody = Label.AOPT21DayNotificationText1 + Label.AOPT21DayNotificationText2;
            
            for(Case objCase: lstCase21DayNotification) {
                EmailMessage objEmailMessage = new EmailMessage(
                    Status = '3',
                    ParentId = objCase.Id,
                    FromAddress = strFromEmail,
                    ToAddress = objCase.Account_Email__c,
                    Subject = strSubject,
                    TextBody = strTextBody);
                lstEmailMesssage.add(objEmailMessage);
            }
        }
        else if(!lstCaseAddnmNotification.isEmpty()) {
            String strSubject = 'Payment Extension Notification';
            String strTextBody = Label.AOPTAddendumNotificationText1 + Label.AOPTAddendumNotificationText2;
            
            for(Case objCase: lstCaseAddnmNotification) {
                EmailMessage objEmailMessage = new EmailMessage(
                    Status = '3',
                    ParentId = objCase.Id,
                    FromAddress = strFromEmail,
                    ToAddress = objCase.Account_Email__c,
                    Subject = strSubject,
                    TextBody = strTextBody);
                lstEmailMesssage.add(objEmailMessage);
            }
        }
        
        insert lstEmailMesssage;
    }
}