@isTest
public class HoHotelDocumentHandlerTest {
    private static testMethod void testOne(){
        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        objAcc.Mobile__c ='1234567890';
        insert objAcc;
                
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        
        
        Id  bildingLocRecTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.recordTypeId = bildingLocRecTypeId;
        loc.Location_Name__c = 'test';
        loc.Address__c = 'test';
        loc.Contact__c = 'test';
        loc.Timings__c = 'test';
        loc.Map_URL__c = 'https://test';
        loc.Hospitality_Email__c = 'test2144@tt.com'; 
        insert loc;
        
        Property__c objProperty = TestDataFactory_CRM.createProperty();
        insert objProperty;
        
        Inventory__c objInventory = TestDataFactory_CRM.createInventory(objProperty.Id);
        objInventory.Building_Location__c = loc.id;
        objInventory.property_Name__c = 'akoya oxygen';
        insert objInventory;
        
        Booking_Unit_Active_Status__c buStatusObj = new Booking_Unit_Active_Status__c();
        buStatusObj.Name = 'Agreement Rejected By Sales Admin';
        buStatusObj.Status_Value__c  = 'Agreement Rejected By Sales Admin';
        insert buStatusObj;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        //objBookingUnit.Mortgage__c = true;
        objBookingUnit.Inventory__c = objInventory.Id;
        objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_DateTime__c = System.now().addDays( 185 );
        insert objBookingUnit;

        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', Booking_Unit__c = objBookingUnit.Id, RecordTypeID=caseRecTypeId);
        insert caseObj;

        SR_Attachments__c objAtt = new SR_Attachments__c( case__c = caseObj.id, Name = 'UMA'
            , Attachment_Url__c='https://partial-damacproperties.cs80.force.com/Documents/apex/GetFile?id=01NUEFKAQPBJ2RNKXWAFFI27JM3EPDOOWP');
        insert objAtt;

        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        //Set mock response
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        Test.startTest();
        HoHotelDocumentHandler.sendUMA( new List<Id> { objAtt.Id });
        Test.stopTest();

    }
}