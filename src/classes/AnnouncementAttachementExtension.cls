public class AnnouncementAttachementExtension {
    public Announcement_Section__c announcementSection {get; set;}
    public Attachment newAttachment;
    
    public AnnouncementAttachementExtension(ApexPages.StandardController controller) {
        announcementSection = (Announcement_Section__c)controller.getRecord();
    }
    
    public Attachment getnewAttachment(){
        newAttachment = new Attachment();
        return newAttachment;
    }
    
    public Pagereference AttachFile(){
        try{
            
            Attachment myAttachment = new Attachment();
            myAttachment.ParentId = announcementSection.Id; 
            myAttachment.Name = newAttachment.Name; 
            myAttachment.Body = newAttachment.Body;
            INSERT myAttachment; 
            
            announcementSection.URL__c = URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/servlet.FileDownload?file=' + myAttachment.Id;
            UPDATE announcementSection;
            
        } catch (Exception ex) {
            system.debug('Attachment Exception: ' + ex.getMessage() + ' at line: ' + ex.getLineNumber());
        }
        PageReference returnPage = new PageReference('/' + announcementSection.Id);
        return returnPage;
    }   
    
}