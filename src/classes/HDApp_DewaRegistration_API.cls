/* 

/**********************************************************************************************************************
Description: This service is used for getting the available slots for booking an appointment for Unit Inspection.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By   | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   06-09-2020       |   Jyoti Aher   |   1. Implementation to store DEWA Number on Booking Unit

1.1     |   06-09-2020      |   Jyoti Aher   |   2. Implementation to store the DEWA certificate in SR attachment

1.2     |   20-08-2020      |   Jyoti Aher   |   3. 
***********************************************************************************************************************/
@RestResource(urlMapping='/dewaRegistrationForBookingUnit/*')
global class HDApp_DewaRegistration_API {
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Bad Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };
    @HttpPost
    /**********************************************************************************************************************
    Description : Method to get appointment Slots for Booking Unit Inspection
    Parameter(s )  : NA
    Return Type : Wrapper (FinalReturnWrapper) as responnse which contains data and metadata
    **********************************************************************************************************************/
    global static FinalReturnWrapper dewaRegistrationProcess(){
        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_data objData = new cls_data();
        cls_meta_data objMeta = new cls_meta_data();
        dewa_registration_Wrapper wrapperInstance = new dewa_registration_Wrapper();

        if(!r.params.containsKey('bookingUnitId')) {
            objMeta = ReturnMetaResponse('Missing parameter : bookingUnitId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;      
        }
        
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : bookingUnitId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        else if(r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : accountId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        else if(r.params.containsKey('dewaNumber') && String.isBlank(r.params.get('dewaNumber'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : dewaNumber', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        else if(r.params.containsKey('documentName') && String.isBlank(r.params.get('documentName'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : documentName', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        } 

        else if(r.params.containsKey('fileName') && String.isBlank(r.params.get('fileName'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : fileName', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        if( r.params.containsKey('bookingUnitId') && String.isNotBlank(r.params.get('bookingUnitId')) 
        && r.params.containsKey('dewaNumber') && String.isNotBlank(r.params.get('dewaNumber'))
        && r.params.containsKey('accountId') && String.isNotBlank(r.params.get('accountId'))
        && r.params.containsKey('documentName') && String.isNotBlank(r.params.get('documentName')) 
        && r.params.containsKey('fileName') && String.isNotBlank(r.params.get('fileName'))) {
            
            String bookingUnitId = r.params.get('bookingUnitId');
            String dewaNumber = String.valueOf(r.params.get('dewaNumber'));
            String accountId = r.params.get('accountId');
            String strDocumentName = r.params.get('documentName');
            String fileName = r.params.get('fileName');
            
            map<Id, Booking_Unit__c> bookingUnitMap = HDApp_getAppointmentDetailsHandler.getBookingIds(bookingUnitId);
            System.debug(bookingUnitMap);
            List<Booking_Unit__c> BookingUnitListToUpdate = new List<Booking_Unit__c>();
            Booking_Unit__c bookingUnitInstance = new Booking_Unit__c();
            if(bookingUnitMap.containsKey(bookingUnitId) && bookingUnitMap.get(bookingUnitId) != null){
                bookingUnitInstance = bookingUnitMap.get(bookingUnitId);
                System.debug('bookingUnitInstance'+bookingUnitInstance);
                bookingUnitInstance.DEWA_Utility_Number__c = dewaNumber;
                BookingUnitListToUpdate.add(bookingUnitInstance);
                System.debug('BookingUnitListToUpdate'+BookingUnitListToUpdate);
            }
            if(BookingUnitListToUpdate!= null && BookingUnitListToUpdate.size()>0){
                
                Blob body = r.requestBody;
                //System.debug('body:: ' + body);
                String requestString = EncodingUtil.Base64Encode(body);
                System.debug('requestString: '+requestString);
                //Logic to upload Dewa Certificate and store the same in SR_Attachment
                UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
                list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
                if(string.isNotBlank(requestString)) {
                    lstWrapper.add(FM_Utility.makeWrapperObject(requestString,
                                                                strDocumentName ,
                                                                r.params.get('fileName') ,
                                                                bookingUnitInstance.Name, '1'));
                }
                System.debug('== lstWrapper =='+lstWrapper);
                objResponse = UploadMultipleDocController.getMultipleDocUrl(lstWrapper);
                System.debug(objResponse);
                // Update the Document name to store in specific format on SR Attachment record
                strDocumentName = strDocumentName + '-' + bookingUnitInstance.Unit_Name__c + '-' + bookingUnitInstance.Registration_ID__c;
                System.debug('strDocumentName'+strDocumentName);

                List<SR_Attachments__c> documentList = new List<SR_Attachments__c>();
                // Insert SR Attachment to store the Document details associated to Booking Unit
                if( objResponse != NULL && objResponse.data != NULL ) {
                    documentList = insertCustomAttachmentForDewaRegDocument(bookingUnitInstance, accountId, strDocumentName, objResponse, fileName);
                }
                if(documentList!=null && documentList.size()>0){
                    // Update Dewa Number on Booking Unit record
                    update BookingUnitListToUpdate;
                    System.debug('BookingUnitListToUpdate'+BookingUnitListToUpdate);

                    for(SR_Attachments__c documentInstance : documentList){
                        wrapperInstance.sr_attachment_url = documentInstance.Attachment_URL__c;
                        wrapperInstance.sr_attachment_id = documentInstance.Id;
                        wrapperInstance.status = 'Submitted';
                    }
                    objMeta.message = 'Success';
                    objMeta.status_code = 1;
                    objMeta.title = mapStatusCode.get(1);
                    objMeta.developer_message = null;

                    objData.dewa_registration_details = wrapperInstance;

                    returnResponse.meta_data = objMeta;
                    returnResponse.data = objData;
                    System.debug('returnResponse:: ' + returnResponse); 
                    return returnResponse;
                }
            }    
        }
        return returnResponse;
    }
    /**********************************************************************************************************************
    Description : Method to create the SR Attachment records related to Account and Booking Unit
    Parameter(s )  : Booking_Unit__c, accountId, strDocumentName, UploadMultipleDocController.data, fileName
    Return Type : void
    **********************************************************************************************************************/
    public static List<SR_Attachments__c> insertCustomAttachmentForDewaRegDocument(Booking_Unit__c bookingUnitInstance, String accountId, String strDocumentName, UploadMultipleDocController.data objResponse, String fileName){
        //removed AttachmentId parameter from this function
        list<SR_Attachments__c> lstCustomAttachments = new list<SR_Attachments__c>();

        System.debug('fileName:: ' + fileName);

        String strFileExtension;
        if( String.isNotBlank( fileName ) && fileName.lastIndexOf('.') != -1 ) {
            strFileExtension = fileName.substring( fileName.lastIndexOf('.')+1 , fileName.length() );
            strFileExtension = strFileExtension.substringBefore('&');
        }

        System.debug('strFileExtension:: ' + strFileExtension);
        
        for(UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            SR_Attachments__c objCustAttach = new SR_Attachments__c();

            objCustAttach.Account__c = accountId;
            objCustAttach.Name = strDocumentName;
            objCustAttach.Attachment_URL__c = objFile.url;
            objCustAttach.Booking_Unit__c = bookingUnitInstance.id;
            objCustAttach.Type__c = strFileExtension;
            lstCustomAttachments.add(objCustAttach);
        }

        System.debug('lstCustomAttachments:: ' + lstCustomAttachments);
        if( !lstCustomAttachments.isEmpty() ) {
            upsert lstCustomAttachments;
            System.debug('lstCustomAttachments'+lstCustomAttachments);
        }
        return lstCustomAttachments;
    }

    /**********************************************************************************************************************
    Description : Method to process and return the response for the API request
    Parameter(s )  : Message, statusCode
    Return Type : Wrapper (FinalReturnWrapper) as responnse which contains data and metadata
    **********************************************************************************************************************/
    public static cls_meta_data ReturnMetaResponse(String message, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data(); 
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = null;
        
        return retMeta;
            
    }
    /**********************************************************************************************************************
    Description : Wrapper Class to combine data and metadata for API response 
    Parameter(s )  : NA
    Return Type : NA
    **********************************************************************************************************************/
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }
    /**********************************************************************************************************************
    Description : Wrapper Class to combine and return metadata results for API response 
    Parameter(s )  : NA
    Return Type : NA
    **********************************************************************************************************************/
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }
    /**********************************************************************************************************************
    Description : Wrapper Class to combine and return data results for API response
    Parameter(s )  : NA
    Return Type : NA
    **********************************************************************************************************************/
    public class dewa_registration_Wrapper {
        public String sr_attachment_id;
        public String sr_attachment_url;
        public String status;
    }
    /**********************************************************************************************************************
    Description : Class contains appointment slots data and return it to FinalReturnWrapper wrapper class for API response
    Parameter(s )  : NA
    Return Type : NA
    **********************************************************************************************************************/
    public class cls_data {
        public dewa_registration_Wrapper dewa_registration_details;
    }
}