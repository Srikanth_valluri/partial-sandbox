@isTest
public class Damac_SG_createTemplateTest {
    static Email_Request__c req = new Email_Request__c ();
    static Email_Template_Versions__c version = new Email_Template_Versions__c ();
    public static void init () {
        Sendgrid_Credentials__c credentials = new Sendgrid_Credentials__c ();
        credentials.Endpoint_URL__c = 'test.com';
        credentials.API_Key__c = 'testcom';
        credentials.From_name__c = 'test';
        credentials.From_Email__c = 'test@test.com';
        insert credentials;
        
        
       ContentVersion cv = new Contentversion();
        cv.title = 'ABC.jpeg';
        cv.PathOnClient = 'test';
        Blob b = Blob.valueOf('Unit Test Attachment Body');
        cv.versiondata = EncodingUtil.base64Decode('Unit Test Attachment Body');
        insert cv;
        
        
        req = new Email_Request__c ();
        req.Template_for__c = 'Sales Offer Inquiry';
        req.Substitution_tags__c = '-Name-';
        req.Related_Object_API__c = 'Inquiry__c';
        req.Template_Name__c = 'Test Template';
        req.Subject__c = 'Test';
        req.Email_template__c = 'Test Template';
        req.Template_Version_ID__c = 'Test Template';
        insert req;
        
        version = new Email_Template_Versions__c ();
        version.Email_Request__c = req.id;
        version.Template_Version_ID__c = req.Email_Template__c;
        insert version;
        
    }
    static testmethod void callMethodStandard () {
        Test.startTest ();
        init ();
        ApexPages.StandardController sc = new ApexPages.StandardController (req);        
        Damac_SG_CreateTemplateController obj = new Damac_SG_CreateTemplateController (sc);
        obj.init ();
    }

    static testmethod void callMethods () {
        Test.startTest ();
        init ();
        apexpages.currentpage().getparameters().put ('id', req.id);
        Damac_SG_CreateTemplateController obj = new Damac_SG_CreateTemplateController ();
        obj.init ();
        obj.emailReq = req;
        obj.createTemplate ();
        obj.getEmailTemplate ();
        obj.getContentDocURL ();
        obj.createTemplate ();
        obj.getEmailTemplate ();
        obj.getContentDocURL ();
        Test.stopTest ();
    }

    static testmethod void callMethodVersion () {
        Test.startTest ();
        init ();
        req.Email_template__c = 'test';
        Update req;
        version.Template_Version_ID__c = 'test';
        version.Template_Version_ID__c = req.Email_Template__c;
        update version; 
        apexpages.currentpage().getparameters().put ('id', version.id);
        Damac_SG_CreateTemplateController obj = new Damac_SG_CreateTemplateController ();
        obj.init ();
        obj.emailReq = req;
        obj.getVersionTemplate();
        Test.stopTest ();
    }
}