public with Sharing class GetSNAGInfoREST {
    string key = Label.SNAGR_KEY;
    
    public string getLocId(String buildCode, String unitName) {
        
        string strEndPointURL = 'https://damac.snagrweb.com/'+buildCode+'/api/locations?key='+key+'&ExtRef='+unitName;
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(strEndPointURL);
        req.setMethod('GET');
        req.setHeader('ACCEPT','application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        
        Http http = new Http();
        HTTPResponse res;
        res =  http.send(req);
        system.debug('!!!!!!!!!!res'+res.getBody());
        list<Location> lstLoc = new list<Location>();
        if (!string.isBlank(res.getBody()) && res.getStatusCode() == 200){
            lstLoc = Location.parse(res.getBody());
        }
        system.debug('!!!!lstLoc'+ lstLoc );
        String LocId;
        if (lstLoc != null && !lstLoc.isEmpty() && !string.isBlank(lstLoc[0].LocID) ) {
            LocId = lstLoc[0].LocID;
        }
        system.debug('!!!!LocId'+LocId);
        return LocId;
    }
    
    public string getPlotId(String buildCode, String locId) {
        string strEndPointURL = 'https://damac.snagrweb.com/'+buildCode+'/api/inspections?key='+key+'&LocID='+locId;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(strEndPointURL);
        req.setMethod('GET');
        req.setHeader('ACCEPT','application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        
        Http http = new Http();
        HTTPResponse res;
        res =  http.send(req);
        system.debug('!!!!!!!!!!res'+res.getBody());        
        list<Inspection> lstInsp = new list<Inspection>();
        if (!string.isBlank(res.getBody()) && res.getStatusCode() == 200){
            lstInsp = Inspection.parsePlot(res.getBody());
        }
        system.debug('!!!!lstInsp'+ lstInsp);
        
        String plotId;
        if (lstInsp != null && !lstInsp.isEmpty() && !string.isBlank(lstInsp[0].PlotID) ) {
            plotId = lstInsp[0].PlotID;
        }
        system.debug('!!!!plotId'+plotId);
        return plotId;
    }
    
    public string getSCID(String buildCode, String plotId) {
        string strEndPointURL = 'https://damac.snagrweb.com/'+buildCode+'/api/schedchecks?key='+key+'&PlotID='+plotId; 
        HttpRequest req = new HttpRequest();
        req.setEndpoint(strEndPointURL);
        req.setMethod('GET');
        req.setHeader('ACCEPT','application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        
        Http http = new Http();
        HTTPResponse res;
        res =  http.send(req);
        system.debug('!!!!!!!!!!res'+res.getBody());
        list<Schedchecks> lstSched = new list<Schedchecks>();
        if (!string.isBlank(res.getBody())&& res.getStatusCode() == 200){
            lstSched = Schedchecks.parseSCID(res.getBody());
        }
        system.debug('!!!!lstSched'+ lstSched);
        
        String scId;
        if (lstSched != null && !lstSched.isEmpty() && !string.isBlank(lstSched[0].SCID) ) {
            scId = lstSched[0].SCID;
        }
        system.debug('!!!!scId'+scId);
        return scId;
    }
    
    public string getDefectIds(String buildCode, String scId) {
        string strEndPointURL = 'https://damac.snagrweb.com/'+buildCode+'/api/FormPlotsLinkedSnags?key='+key+'&SCID='+scId;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(strEndPointURL);
        req.setMethod('GET');
        req.setHeader('ACCEPT','application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        
        Http http = new Http();
        HTTPResponse res;
        res =  http.send(req);
        system.debug('!!!!!!!!!!res'+res.getBody());
        
        String defectIds;
        
        if (!string.isBlank(res.getBody())&& res.getStatusCode() == 200){
            String strBody = res.getBody();
            strBody = strBody.remove('[');
            strBody = strBody.remove(']');
            defectIds = strBody;
        }
        system.debug('!!!!!!!!!!defectIds'+defectIds);
        return defectIds;
    }
    
    public list<Defect> getSnags(String buildCode, String defectIds) {
        string strEndPointURL = 'https://damac.snagrweb.com/'+buildCode+'/api/defects?key='+key+'&IDs='+defectIds;
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(strEndPointURL);
        req.setMethod('GET');
        req.setHeader('ACCEPT','application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        
        Http http = new Http();
        HTTPResponse res;
        res =  http.send(req);
        system.debug('!!!!!!!!!!res'+res.getBody());
        
        list<Defect> lstDefects = new list<Defect>();
        if (!string.isBlank(res.getBody())&& res.getStatusCode() == 200){
           lstDefects = Defect.parseDefect(res.getBody());
        }
        system.debug('!!!!lstDefects '+ lstDefects );
        
        return lstDefects;
    }
    
    public InspectionIds getInspectionForms(String buildCode, String plotId) {
        string strEndPointURL = 'https://damac.snagrweb.com/'+buildCode+'/api/inspections/{id}?key='+key+'&ID='+plotId;
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(strEndPointURL);
        req.setMethod('GET');
        req.setHeader('ACCEPT','application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        
        Http http = new Http();
        HTTPResponse res;
        res =  http.send(req);
        system.debug('!!!!!!!!!!res'+res.getBody());
        
        InspectionIds lstInspectionForms = new InspectionIds();
        if (!string.isBlank(res.getBody())&& res.getStatusCode() == 200){
           lstInspectionForms = InspectionIds.parseInspections(res.getBody());
        }
        system.debug('!!!!lstInspectionForms'+ lstInspectionForms );
        
        return lstInspectionForms ;
    }
    
    public class Defect{
        public String ID;
        public String GUID;
        public String DrawingID;
        public String DrawingTitle;
        public String Status;
        public String CurrentStatus;
        public String UserDescription;
        public String LocationID;
        public String Location;
        public String LocationExtRef;
        public String LocationGUID;
        public String XCoord;
        public String YCoord;
        public String PriorityID;
        public String Priority;
        public String DueBy;
        public String DaysLeft;
        public String GroupID;
        public String GroupName;
        public String GroupInitials;
        public String DateClosed;
        public String BigPhoto;
        public String Lattitude;
        public String Longitude;
        public String CreatedByID;
        public String CreatedBy;
        public String CreatedDate;
        public String SDCategoryID;
        public String SDCategory;
        public String SDCode;
        public String SDType;
        public String SDID;
        public String SDDescription;
        public String DateSync;
        public String MPGUID;
        public String FCGUID;
        public String FutureDefectGUID;
        public String PreviousDefectGUID;
        public String ContractNo;
        public String Role;
        public String UGUserID;
        public String HistoryCount;
        public String LastChanged;
        public String PhotoExists;
    }
    
    public class Inspection {
        public String PlotID;
    }
    
    public class Schedchecks {
        public String SCID;
        public String PlotID;
        public String Code;
        public String Description;
        public String srt;
        public String Qty;
        public String Result;
        public String Comment;
        public String UserID;
        public String DateStamp;
        public String DueBy;
        public String PPCGuid;
        public String DateSync;
        public String CantSkip;
        public String Cost;
        public String SCGuid;
        public String FormCode;
        public String Percent;
        public String Categ;
    }
    
    public class Location {
        public String LocID;
        public String DrwgID;
        public String Xcoord;
        public String Ycoord;
        public String Location;
        public String Datestamp;
        public String Lat;
        public String Lng;
        public String LocGUID;
        public String DateSync;
        public String ExtRef;
        public String Closed;
        public String RFDI;
        public String LocationTypeID;
    }
    
    public class FormChecks {
        public String CheckCode;
        public String Categ;
        public String Question;
        public String srt;
        public String Result;
        public String ResultText;
        public String ResultImg;
        public String FIAID;
        public String Username;
        public String UserID;
        public String DateStamp;
        public String AnswerType;
        public String Mandatory;
        public String PageNo;
        public String FCID;
        public String ListCode;
        public String LinkCheckCode;
        public String FCGUID;
        public String AutoSnagged;
    }
    
    public class InspectionForms {
        public String FormName;
        public String FormShortName;
        public String FormID;
        public String FormCode;
        public String IssuedDate;  
        public String ValudUntil;
        public String IssuedTo;
        public String IssuedBy;  
        public String AcceptedBy;
        public String Closed;
        public String Passed;
        public List<FormChecks> FormChecks;
    }
    
    public class InspectionIds {
        public String ProjectTitle;
        public String ContractNo;
        public String DrawingTitle;
        public String DrwgID;
        public String InspectionID;
        public String InspectionName;
        public String InspectionExtRef;
        public String Location;
        public String LocID;
        public String LocationExtRef;
        public String CreatedDate;
        public String CreatedBy;
        public String CreatedByID;
        public String Status;
        public String ClosedDate;
        public String ClosedBy;
        public String ClosedByID;
        public String UpdateDate;
        public List<InspectionForms> InspectionForms;
    }
    
    public static InspectionIds parseInspections (String json) {
        return (InspectionIds) System.JSON.deserialize(json, InspectionIds.class);
    }
    
    public static list<Location> parse(String json) 
    {
        return (list<Location>) System.JSON.deserialize(json, list<Location>.class);
    }
    
    public static list<Inspection> parsePlot(String json) 
    {
        return (list<Inspection>) System.JSON.deserialize(json, list<Inspection>.class);
    }
    
    public static list<Schedchecks> parseSCID(String json) 
    {
        return (list<Schedchecks>) System.JSON.deserialize(json, list<Schedchecks>.class);
    }
    
    public static list<Defect> parseDefect(String json) 
    {
        return (list<Defect>) System.JSON.deserialize(json, list<Defect>.class);
    }
}