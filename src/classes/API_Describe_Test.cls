@isTest
public class API_Describe_Test {

    @isTest 
    static void doGetDescribeObject_Test() {
        RestRequest req = new RestRequest(); 
        
        req.requestURI = '/api_describe/object';  
        req.httpMethod = 'GET';
        
        req.params.put('object', 'Account');
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
        
        Test.startTest();
        
        API_Describe.doGet();
        
        Test.stopTest();
    }
    
    @isTest 
    static void doGetDescribeObjectWithoutObject_Test() {
        RestRequest req = new RestRequest(); 
        
        req.requestURI = '/api_describe/object';  
        req.httpMethod = 'GET';
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
        
        Test.startTest();
        
        API_Describe.doGet();
        
        Test.stopTest();
    }
    
    @isTest 
    static void doGetDescribeObjectFields_Test() {
        RestRequest req = new RestRequest(); 
        
        req.requestURI = '/api_describe/objectfields';  
        req.httpMethod = 'GET';
        
        req.params.put('object', 'Account');
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
        
        Test.startTest();
        
        API_Describe.doGet();
        
        Test.stopTest();
    }

    @isTest 
    static void doGetDescribeObjectFieldsBuyer() {
        RestRequest req = new RestRequest(); 
        
        req.requestURI = '/api_describe/field';  
        req.httpMethod = 'GET';
        
        req.params.put('object', 'Buyer__c');
        req.params.put('field', 'City__c');
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
        
        Test.startTest();
        
        API_Describe.doGet();
        
        Test.stopTest();
    }
    
    @isTest 
    static void doGetDescribeObjectFieldsWithoutObject_Test() {
        RestRequest req = new RestRequest(); 
        
        req.requestURI = '/api_describe/objectfields';  
        req.httpMethod = 'GET';
                
        RestContext.request = req;
        RestContext.response = new RestResponse();
        
        Test.startTest();
        
        API_Describe.doGet();
        
        Test.stopTest();
    }
    
    @isTest 
    static void doGetDescribeField_Test() {
        RestRequest req = new RestRequest(); 
        
        req.requestURI = '/api_describe/field';  
        req.httpMethod = 'GET';
        
        req.params.put('object', 'Account');
        req.params.put('field', 'Industry');
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
        
        Test.startTest();
        
        API_Describe.doGet();
        
        Test.stopTest();
    }
    
    @isTest 
    static void doGetDescribeFieldWithoutField_Test() {
        RestRequest req = new RestRequest(); 
        
        req.requestURI = '/api_describe/field';  
        req.httpMethod = 'GET';
        
        req.params.put('object', 'Account');
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
        
        Test.startTest();
        
        API_Describe.doGet();
        
        Test.stopTest();
    }
    
    @isTest 
    static void doGetDescribe_Test() {
        RestRequest req = new RestRequest(); 
        
        req.requestURI = '/api_describe';  
        req.httpMethod = 'GET';
                
        RestContext.request = req;
        RestContext.response = new RestResponse();
        
        Test.startTest();
        
        API_Describe.doGet();
        
        Test.stopTest();
    }
    
}