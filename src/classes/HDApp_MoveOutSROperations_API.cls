/**********************************************************************************************************************
Description: This API is used for Move Out SR Delete & GetDraft operattions in Damac Living App
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   20-10-2020      | Shubham Suryawanshi | Created initial draft
***********************************************************************************************************************/
@RestResource(urlMapping='/moveOutSrOps/*')

global class HDApp_MoveOutSROperations_API {

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    /***********************************************************************************************
    Method Name : deleteMoveOutSR
    Description : Deletes the Move Out SR (FM Case) for given fmCaseId
    Parameter(s): None
    Return Type : FinalReturnWrapper
    ************************************************************************************************/
    @HttpDelete
    global static FinalReturnWrapper deleteMoveOutSR() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_data objData = new cls_data();
        cls_meta_data objMeta = new cls_meta_data();

        if(!r.params.containsKey('fmCaseId')){
            objMeta = ReturnMetaResponse('Missing parameter : fmCaseId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('fmCaseId') && String.isBlank(r.params.get('fmCaseId'))) {
            objMeta = ReturnMetaResponse('Missing parameter value: fmCaseId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        System.debug('fmCaseId :: ' + r.params.get('fmCaseId'));
        List<FM_Case__c> moveOutCaseToDel = GetTenantRegistrationCase_API.getFMCase(r.params.get('fmCaseId'));
        System.debug('moveOutCaseToDel: ' + moveOutCaseToDel);

        if(moveOutCaseToDel != null && moveOutCaseToDel.size() > 0) {
            System.debug('Deleting moveOutCaseToDel');

            try {
                Delete moveOutCaseToDel;
            }
            catch(exception e) {
                objMeta = ReturnMetaResponse(e.getMessage(), 6);
                returnResponse.meta_data = objMeta;
                return returnResponse;
            }
            
        }else {
            objMeta = ReturnMetaResponse('No FM case found for given fmCaseId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        objMeta = ReturnMetaResponse('Record deleted successfully', 1);
        returnResponse.meta_data = objMeta;
        returnResponse.data = null;
        return returnResponse;

    }

    /***********************************************************************************************
    Method Name : getMoveOutDraftSR
    Description : Returns the Move Out SR (FM Case) details for given fmCaseId
    Parameter(s): None
    Return Type : FinalReturnWrapper (Wrapper)
    ************************************************************************************************/
    @HttpGet
    global static FinalReturnWrapper getMoveOutDraftSR() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_data objData = new cls_data();
        cls_meta_data objMeta = new cls_meta_data();

        if(!r.params.containsKey('fmCaseId')){
            objMeta = ReturnMetaResponse('Missing parameter : fmCaseId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('fmCaseId') && String.isBlank(r.params.get('fmCaseId'))) {
            objMeta = ReturnMetaResponse('Missing parameter value: fmCaseId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        System.debug('fmCaseId :: ' + r.params.get('fmCaseId'));
        List<FM_Case__c> moveOutCaseDetails = GetTenantRegistrationCase_API.getFMCase(r.params.get('fmCaseId'));
        System.debug('moveOutCaseToDel: ' + moveOutCaseDetails);

        if(moveOutCaseDetails != null && moveOutCaseDetails.size() > 0) {
            HDApp_MoveOutSR_API.FmCaseReturnWrapper moveOutCaseWrap = HDApp_MoveOutSR_API.getMoveOutCaseWrapper(moveOutCaseDetails[0]);
            cls_data instData = (cls_data)JSON.deserialize(JSON.serialize(moveOutCaseWrap) ,cls_data.class );
            System.debug('instData:: ' + instData);

            objData = instData;
            objMeta = ReturnMetaResponse('Success', 1);
        }
        else {
            objMeta = ReturnMetaResponse('No FM case found for given fmCaseId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        returnResponse.meta_data = objMeta;
        returnResponse.data = objData;

        System.debug('returnResponse: ' + returnResponse);

        return returnResponse;
    }

    /***********************************************************************************************
    Method Name : ReturnMetaResponse
    Description : Generic method to create meta_data response instance
    Parameter(s): String message, Integer statusCode
    Return Type : cls_meta_data (wrapper)
    ************************************************************************************************/
    public static cls_meta_data ReturnMetaResponse(String message, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data();
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = null;
        return retMeta;
    }
    
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }
    
    public class cls_data {
        public String fm_case_id;   
        public String fm_case_status;   
        public String fm_case_number;
        public String booking_unit_id;

        public String move_out_date;
        public String move_out_type;    //Self move out / Moving company
        public String moving_company_name;
        public String moving_contractor_name;
        public String moving_contractor_phone;
        public String reason_to_move_out;
        public cls_attachment[] attachments;
    }

    public class cls_attachment {
        public String id;
        public String name;
        public String attachment_url;
        public String file_extension;
    }

}