/************************************************************************************************
 * @Name              : DAMAC_Inventory_DependentLookupAPI_Test
 * @Description       : Test Class for DAMAC_Inventory_DependentLookupAPI
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         23/07/2020       Created
***********************************************************************************************/

@isTest
public class DAMAC_Inventory_DependentLookupAPI_Test{

@isTest
    static void testAPI(){
    	Marketing_Documents__c marketDoc = new Marketing_Documents__c();
        marketDoc.Name = 'FIORA';
        insert marketDoc;                                                                                       
        Inventory__c inv = new Inventory__c();
        inv.Property_City__c = 'Dubai';
        inv.District__c = 'AL YUFRAH 2';
        inv.Status__c = 'Released';
        inv.View_Type__c = 'GOLF';
        inv.Marketing_Name_Doc__c = marketDoc.Id;
        inv.IPMS_Bedrooms__c = '1';       
        inv.Space_Type_Lookup_Code__c= 'Hotel';
        inv.Property_Status__c= '';              
        insert inv;
        List<Inventory__c> invList = new List<Inventory__c>();
        invList.add(inv);
        
        Map<String, Map<String, Map<String, List<String>>>> cityMap = new Map<String, Map<String, Map<String, List<String>>>>();
        Map<Id, List<String>> projectNameMap = new Map<Id, List<String>>(); 
        Map<String, Map<String, List<String>>> districtMap = new Map<String, Map<String, List<String>>>();
        List<String> viewTypeList = new List<String>();
        String city;
        String dis;
        String proj;
        String viewT;
        for(Inventory__c invt : invList){
        	city = invt.Property_City__c;
            dis = invt.District__c;
            proj = invt.Marketing_Name_Doc__r.Name;
            viewT = invt.View_Type__c;
            viewTypeList.add(viewT);
            projectNameMap.put(proj,viewTypeList);
            //districtMap.put(dis,projectNameMap);
            cityMap.put(city,districtMap);
            
        }
        
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/queryInventoryDependentLookup';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Inventory_DependentLookupAPI.getResults();
       
        
        Test.stopTest();
    
    }
}