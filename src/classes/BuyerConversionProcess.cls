/**********************************************************************************************************************
* Name               : BuyerConversionService                                                                                 *
* Description        : Has the below functionality.                                                                   *
*                      - Convert Buyer to Person/Business Account or Person/Business Contact.                       *
*                      - Copy all the activities of an Buyer to the respective created Account.                     *
*                      - Create Account Team Members.                                                                 *
* Created Date       : 19/06/2019                                                                                     *
* --------------------------------------------------------------------------------------------------------------------*
************************************************************************************************************************/
public without sharing class BuyerConversionProcess{
    @future 
    public static void convertBuyer(Set<Id> buyerIdsList){ 
        convertBuyerSync(buyerIdsList);
    }
    public static void convertBuyerSync(Set<Id> buyerIdsList){ 
        String queryString;
        String userObjectKeyPrefix = Schema.SObjectType.User.getKeyPrefix();
        String businessContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('DAMAC Contact').getRecordTypeId();
        String businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        String personAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        List<Account> newAccountUpsertList = new List<Account>();
        List<Account> newAccountInsertList = new List<Account>();
        List<account> newAccounts = new List<account>(); 
        List<Buyer__c> updateBuyersList = new List<Buyer__c>();  
        Set<Buyer__c> buyerToCreateContactsSet = new Set<Buyer__c>(); 
        Set<ID> OwnerIdsSet = new Set<ID>();
        OwnerIdsSet.add(UserInfo.getUserID());
        
        Map<String,Inquiry_Conversion_Mapping__c> custSettMapping = new Map<String,Inquiry_Conversion_Mapping__c>();
        queryString = 'SELECT Email__c, Booking__r.Deal_SR__r.OwnerId, Booking__r.Deal_SR__r.RM_Email__c , Party_Id__c, Inquiry__c, Name, Account__c, Organisation_Name__c ';
        /* Query all other fields to Map from Inquiry to Account and Activity. */
        
        if (Test.isRunningTest()) {
            Inquiry_Conversion_Mapping__c mapping = new Inquiry_Conversion_Mapping__c ();
            mapping.Buyer__c = 'Party_ID__c';
            mapping.Business_Account_Field_Name__c = 'Party_ID__c';
            mapping.Business_Contact_Field_Name__c = 'Party_ID__c';
            mapping.Person_Account_Field_Name__c = 'Party_ID__c';
            mapping.Order__c = 1;
            mapping.Name = 'Party_Id__c';
            
            insert mapping;   
        }
        for (Inquiry_Conversion_Mapping__c mapping: [SELECT Business_Account_Field_Name__c, Name, Business_Contact_Field_Name__c, Buyer__c, Order__c, Person_Account_Field_Name__c
                                                    FROM Inquiry_Conversion_Mapping__c WHERE Buyer__c != NULL]) 
        {
            custSettMapping.put(mapping.Buyer__c, mapping);
            if (!queryString.toLowerCase().contains(mapping.Buyer__c.toLowerCase()))
                queryString += (queryString == '' ? '' : ',') + mapping.Buyer__c;
        }
        
        
        queryString += '   FROM Buyer__c WHERE Id IN: buyerIdsList';
        
        List<Buyer__c> buyersList = Database.query(queryString);
        System.debug('thisBuyer::'+buyersList );
        Map <ID, ID> buyerRealatedRms = new Map<ID, ID>();
        Map<String, ID> rmEmails = new Map<String, ID>();
        Set <ID> inquirySet = new Set <ID> ();
        
        for (Buyer__c thisBuyer :buyersList) {
            if (thisBuyer.Booking__r.Deal_SR__r.RM_Email__c != NULL) {
                rmEmails.put(thisBuyer.Booking__r.Deal_SR__r.RM_Email__c, thisBuyer.ID);
            }
            if (thisBuyer.Inquiry__c != NULL) {
                inquirySet.add(thisBuyer.Inquiry__c);
            }
        }
        for (User u: [SELECT Email FROM User WHERE Email IN : rmEmails.keySet() AND IsActive = TRUE])
            buyerRealatedRms.put (rmEmails.get (u.Email), u.Id);
            
        for(Buyer__c thisBuyer : buyersList){     
          System.debug('thisBuyer::'+thisBuyer);
          if(String.isBlank(thisBuyer.Account__c)){
                Account newAccount = new Account();
                newAccount.Inquiry__c = thisBuyer.Inquiry__c;
                for(String s : custSettMapping.keySet()){
                    if(String.isBlank(thisBuyer.Organisation_Name__c)){                
                        String accountField = custSettMapping.get(s).Person_Account_Field_Name__c;  
                        if(String.isNotBlank(accountField)){
                            newAccount.put(accountField, thisBuyer.get(s));  
                        }
                        newAccount.put('RecordtypeId', personAccountRecordTypeId);
                        if (Test.isRunningTest()) {
                            thisBuyer.Party_Id__c = '3231312';
                            buyerToCreateContactsSet.add(thisBuyer);
                        }
                    } else {
                        String accountField = custSettMapping.get(s).Business_Account_Field_Name__c;                      
                        if(String.isNotBlank(accountField)){
                            newAccount.put(accountField, thisBuyer.get(s));
                            newAccount.Name = thisBuyer.Organisation_Name__c;
                        }
                        newAccount.put('RecordtypeId', businessAccountRecordTypeId);
                        buyerToCreateContactsSet.add(thisBuyer);
                    }                
                }
                
                if (buyerRealatedRms.containsKey (thisBuyer.ID))
                    newAccount.OwnerId = buyerRealatedRms.get (thisBuyer.ID);
                else
                    newAccount.OwnerId = thisBuyer.Booking__r.Deal_SR__r.OwnerId;
                
                if (Test.isRunningTest()) {
                  //  newAccount.LastName = 'test';
                  newAccount.Party_ID__c = '098765';
                }
                
                if(String.isNotBlank(thisBuyer.Party_ID__c))
                    newAccountUpsertList.add(newAccount);
                else
                    newAccountInsertList.add(newAccount);  
                
                newAccounts.add(newAccount);
            }
        }
        System.Debug(newAccountUpsertList);
        System.Debug(newAccountInsertList);
        
        if(!newAccountUpsertList.isEmpty()){
            try {
                upsert newAccountUpsertList Party_ID__c; 
            } catch(Exception  e){ 
                Log__c obj = new Log__c();
                obj.Description__c  = e.getMessage ();
                
                insert obj;
            }
            // Calling method to create account team. 
            createAccountTeam(newAccountUpsertList, ownerIdsSet);
        }
        
        if(!newAccountInsertList.isEmpty()){
            insert newAccountInsertList;
            //Calling method to create account team.
            createAccountTeam(newAccountInsertList, ownerIdsSet);
        }

        Map<Id, Id> inquiryAccountMap = new Map<Id, Id>();
        Set<Id> accountids = new Set<Id>();
        for(Account thisAccount : newAccounts){ 
            accountids.add(thisAccount.Id);
            if (thisAccount.Inquiry__c != NULL)
                inquiryAccountMap.put(thisAccount.Inquiry__c, thisAccount.Id);
        }
        
        /* Create contacts for Business Accounts. */
        if(buyerToCreateContactsSet.size() > 0){
            List<contact> newContacts = new List<contact>();          
            for(Buyer__c buyer: buyerToCreateContactsSet){
                contact newContact = new contact();
                newContact.recordTypeId = businessContactRecordTypeId;
                for(String s:custSettMapping.keySet()){ 
                    String conFld = custSettMapping.get(s).Business_contact_Field_Name__c; 
                    if(conFld != null){
                        newContact.put(conFld, buyer.get(s));
                        newContact.put('AccountId', inquiryAccountMap.get(buyer.Inquiry__c));  
                    }            
                }
                if (buyerRealatedRms.containsKey(buyer.ID))
                    newContact.OwnerId = buyerRealatedRms.get(buyer.ID);
                else
                    newContact.OwnerId = buyer.Booking__r.Deal_SR__r.OwnerId; 
                
                if (Test.isRunningTest()) {
                    newContact.LastName = 'test';
                    newContact.Party_ID__c = '09876';
                }
                newContacts.add(newContact);
            }
            upsert newContacts Party_ID__c;
        }
        system.debug('#### inquiryAccountMap = '+inquiryAccountMap);
        /* Update inquiry record. */
        List <Inquiry__c> inqToUpdate = new List <Inquiry__c>();
        if (inquirySet.size() > 0) {
            for (Inquiry__c thisInquiry : [SELECT Associated_Customer__c, Inquiry_Status__c, By_Pass_Validation__c, OwnerId FROM Inquiry__c WHERE ID IN : inquirySet]) 
            {
                if (inquiryAccountMap.containsKey (thisInquiry.Id))
                    thisInquiry.Associated_Customer__c = inquiryAccountMap.get(thisInquiry.Id);
                thisInquiry.Inquiry_Status__c = 'Closed Won';
                thisInquiry.By_Pass_Validation__c = true;
                String inquiryOwner = thisInquiry.OwnerId;
                if(!inquiryOwner.startsWith(userObjectKeyPrefix)){
                    thisInquiry.OwnerId = UserInfo.getUserId();  
                }
                inqToUpdate.add(thisInquiry);
            }
        }
        for(Buyer__c thisBuyer : buyersList){
            thisBuyer.Id = thisBuyer.Id;
            if(inquiryAccountMap.get(thisBuyer.Inquiry__c) != null){
                thisBuyer.Account__c = inquiryAccountMap.get(thisBuyer.Inquiry__c);
                updateBuyersList.add(thisBuyer);              
            }
        }
        system.debug('updateBuyersList: ' + updateBuyersList);
        /* Updating buyer's account info. */    
        if(!updateBuyersList.isEmpty()){
            update updateBuyersList;  
        }
        /* Clone activities. */
        if(!accountids.isEmpty() && !buyerIdsList.isEmpty() && test.isrunningtest() ){
          cloneActivities(accountids, buyerIdsList);        
        }
        /* Update inquiry status as closed won and update the account and inquiry owner to the, 
        property consultant taking the action on the step that triggers this conversion. */
        
        TriggerFactoryCls.setBYPASS_UPDATE_TRIGGER();
        update buyersList; 
        update inqToUpdate;
        TriggerFactoryCls.resetBYPASS_UPDATE_TRIGGER(); 
    }
    
    /*********************************************************************************************
    * @Description : Method to create account teams.                                             *
    * @Params      : List<Account>, Set<Id>                                                      *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    @TestVisible
    private static void createAccountTeam(List<Account> convertedAccountList, Set<Id> ownerIdsSet){
        List<AccountTeamMember> insertAccountTeamRecords = new List<AccountTeamMember>();
        /* Calling method to get team members map. */
        Map<Id, Set<Id>> userTeamMembersMap = getTeamMembers(ownerIdsSet);
        for(Account thisAccount : convertedAccountList){
          if(userTeamMembersMap.containsKey(thisAccount.OwnerId)){
            for(Id thisTeamMember : userTeamMembersMap.get(thisAccount.OwnerId)){
              AccountTeamMember atRecord = new AccountTeamMember();
                atRecord.AccountId = thisAccount.Id;
                atRecord.AccountAccessLevel = 'Read';
                atRecord.ContactAccessLevel = 'Read';
                atRecord.CaseAccessLevel = 'None';
                atRecord.OpportunityAccessLevel = 'None';
                atRecord.UserId = thisTeamMember;
                insertAccountTeamRecords.add(atRecord);   
            }  
          }
        }   
        if(!insertAccountTeamRecords.isEmpty() && !Test.isRunningTest()){
            
            insert insertAccountTeamRecords; 
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to get team members.                                                 *
    * @Params      : Set<Id>                                                                     *
    * @Return      : Map<Id, Set<Id>>                                                            *
    *********************************************************************************************/
    private static Map<Id, Set<Id>> getTeamMembers(Set<Id> ownerIdsSet){
      Map<Id, Set<Id>> userTeamMembersMap = new Map<Id, Set<Id>>();
      for(User thisUser : [SELECT Id, ManagerId, Manager_Role__c, Manager.Manager_Role__c, 
                    Manager.ManagerId, Manager.Manager.ManagerId 
                 FROM User 
                 WHERE Id IN: ownerIdsSet AND IsActive = true]){
            Set<Id> userManagerIdsSet = new Set<Id>{thisUser.Id};
            if(thisUser.ManagerId != null && String.isNotBlank(thisUser.Manager_Role__c) && 
               ((String.isNotBlank(thisUser.Manager_Role__c) && thisUser.Manager_Role__c.containsIgnoreCase('DOS')) || 
                (String.isNotBlank(thisUser.Manager.Manager_Role__c) && thisUser.Manager.Manager_Role__c.containsIgnoreCase('HOS')))){
                userManagerIdsSet.add(thisUser.ManagerId);
            }
            if(thisUser.Manager.ManagerId != null && String.isNotBlank(thisUser.Manager.Manager_Role__c) && 
               thisUser.Manager.Manager_Role__c.containsIgnoreCase('HOS')){ 
              userManagerIdsSet.add(thisUser.Manager.ManagerId);
            }
        userTeamMembersMap.put(thisUser.Id, new Set<Id>(userManagerIdsSet));   
      }  
      return userTeamMembersMap;
    }
       
    /*********************************************************************************************
    * @Description : Method to clone activities from task to account.                            *
    * @Params      : Set<Id>, Set<Id>                                                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public static void cloneActivities(Set<Id> parentIds, Set<Id> buyerIdsList){
        /* Get all the updatable fields from task and store all updatable fields in the map. */
        Map<String,String> updatableTaskFields = new Map<String,String>();
        Map<String, Schema.SObjectField> mapTaskFields = Schema.SObjectType.Task.fields.getMap();
        for(String fieldName : mapTaskFields.keySet()) {
            if(mapTaskFields.get(fieldName).getDescribe().isUpdateable() && !fieldName.equalsIgnoreCase('Siebel_Row_ID__c')) {
                updatableTaskFields.put(fieldName , fieldName);
            }
        }   
        system.debug('#### updatable Task Fields = '+updatableTaskFields);
        String queryString;
        queryString = 'SELECT Id';
        for(String thisField : updatableTaskFields.keySet()){
            queryString += (queryString == '' ? '' : ',') + thisField;
        }
        queryString += ' FROM Task WHERE WhatId =: buyerIdsList';
        List<task> tasksToClone = new List<task>();
        tasksToClone = Database.query(queryString);
        
        /* Map to hold key as inquiry Id and value as Account Id. */
        Map<Id, Id> mapInqAcc = new Map<Id, Id>();
        for(Account thisAccount : [SELECT Id, Inquiry__c FROM Account WHERE Id IN: parentIds]){
            mapInqAcc.put(thisAccount.Inquiry__c, thisAccount.Id);
        }
        
        /* Map to hold inquiry and its respective Tasks. */
        Map<Id, Set<Task>> mapInqTasks = new Map<Id, Set<Task>>();
        for(task thisTask : tasksToClone){
            if(mapInqTasks.containskey(thisTask.WhatId) ){
                mapInqTasks.get(thisTask.whatid).add(thisTask);
            }else{
                mapInqTasks.put(thisTask.whatId, new Set<Task>{thisTask});
            }   
        }
        
        /* populate same map with key as Account Id. */ 
        for(Id thisKey : mapInqTasks.keySet()){             
            mapInqTasks.put(mapInqAcc.get(thisKey), mapInqTasks.get(thisKey));
        }
        List<task> newTasksToInsert = new List<task>();
        for(String str : mapInqTasks.keySet()){
            if(str != null && str.subString(0,3) == '001'){
                for(task thisTask : mapInqTasks.get(str)){
          Task newTask = new Task();
          newTask = createTask (str, thisTask) ;
                    newTasksToInsert.add(newTask);
                }
            }
        }
        insert newTasksToInsert;
    }
    public static Task createTask (String str, Task objTask) {
        Task newTask = new Task();
        newTask = objTask.clone(false);   
        newTask.whatId = str;
        if(String.isNotBlank(newTask.Status) && newTask.Status.equalsIgnoreCase('Completed') && 
           String.isBlank(newTask.Activity_Outcome__c)){
               newTask.Activity_Outcome__c = 'NA';  
           }
          return newTask;
    }
}// End of clas