/*******************************************************************************************************
Description : Class to generate Addedndum document from drawloop for VAT HO SR
--------------------------------------------------------------------------------------------------------
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
--------------------------------------------------------------------------------------------------------
1.0     | 04-02-2020        |Aishwarya Todkar    | Initial Draft
*****************************************************************************************************/
Public Class VAT_AddendumDocGenerator implements Schedulable {

    public VAT_AddendumDocGenerator(ApexPages.StandardSetController controller) {}
    public VAT_AddendumDocGenerator() {}

    Booking_Unit__c objUnit;
/********************************************************************************************************
    Method to generate Addedndum document
*********************************************************************************************************/
    public static void generateAddendumDoc() {
        List<Booking_Unit__c> listBUToUpdate = new List<Booking_Unit__c>();
        for(Booking_Unit__c objBu : [ SELECT 
                                        Id
                                        , Permitted_Use_Type__c
                                        , Handover_Status__c
                                        , VAT_Addendum_Generated__c
                                        , Is_Cover_Letter_Generated__c
                                    FROM
                                        Booking_Unit__c
                                    WHERE
                                        Generate_and_Send_Addendum__c = true
                                    AND
                                        VAT_Email_Sent__c = false
                                    AND
                                        Permitted_Use_Type__c != null
                                    AND 
                                        ( 
                                            VAT_Addendum_Generated__c = false 
                                        OR  
                                            Is_Cover_Letter_Generated__c = false 
                                        )
                                    ] ) {//Handover_Status__c != null
            System.debug('objBu--' + objBu);
            
            String ddpId1 = '';
            String deliveryId1 = '';
            String ddpId2 = '';
            String deliveryId2 = '';

            /**
                Generating SPA Addendum
            */
            //if( !objBu.VAT_Addendum_Generated__c ) {
                
                if( objBu.Permitted_Use_Type__c.equalsIgnoreCase( 'Units With VAT' ) ) {
                    
                    Riyadh_Rotana_Drawloop_Doc_Mapping__c cs 
                        = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( 'Unit with HO VAT Addendum' );
                    
                    System.debug( 'spa cs 1-' + cs);
                    
                    if(cs != null
                    && String.isNotBlank( cs.Drawloop_Document_Package_Id__c ) 
                    && String.isNotBlank( cs.Delivery_Option_Id__c )) {
                        ddpId1 = cs.Drawloop_Document_Package_Id__c;
                        deliveryId1 = cs.Delivery_Option_Id__c;
                    }
                    else {
                        ApexPages.addmessage( 
                            new ApexPages.message(ApexPages.severity.Error
                            ,'Please configure \'Unit with HO VAT Addendum\' DDP details properly.')
                        );
                    }
                }
                else if( objBu.Permitted_Use_Type__c.equalsIgnoreCase( 'Units Without VAT' ) ) {
                    
                    Riyadh_Rotana_Drawloop_Doc_Mapping__c cs 
                        = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( 'Unit w/o HO VAT Addendum' );
                    
                    System.debug( 'spa cs 2-' + cs);
                    
                    if(cs != null
                    && String.isNotBlank( cs.Drawloop_Document_Package_Id__c ) 
                    && String.isNotBlank( cs.Delivery_Option_Id__c )) {
                        ddpId1 = cs.Drawloop_Document_Package_Id__c;
                        deliveryId1 = cs.Delivery_Option_Id__c;
                    }
                    else {
                        ApexPages.addmessage( new ApexPages.message(ApexPages.severity.Error
                            ,'Please configure \'Unit w/o HO VAT Addendum\' DDP details properly.')
                        );
                    }
                }
            //}// End SPA Addendum generation
            

            /**
                Generating Cover Letter
            */
            //if( !objBu.Is_Cover_Letter_Generated__c ) {
                
                if( objBu.Handover_Status__c.equalsIgnoreCase( 'Handed Over' ) 
                || objBu.Handover_Status__c.equalsIgnoreCase( 'Early Handed Over' ) 
                || objBu.Handover_Status__c.equalsIgnoreCase( 'Lease Handed Over' ) ) {

                    Riyadh_Rotana_Drawloop_Doc_Mapping__c cs 
                        = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( 'Handed Over Cover Letter' );

                    System.debug( 'refused cs 1-' + cs);

                    if(cs != null
                    && String.isNotBlank( cs.Drawloop_Document_Package_Id__c ) 
                    && String.isNotBlank( cs.Delivery_Option_Id__c )) {

                        ddpId2 = cs.Drawloop_Document_Package_Id__c;
                        deliveryId2 = cs.Delivery_Option_Id__c;
                    }
                    else {
                        ApexPages.addmessage( new ApexPages.message(ApexPages.severity.Error
                        ,'Please configure \'Handed Over Cover Letter\' DDP details properly.'));
                    }
                }
                else {

                    Riyadh_Rotana_Drawloop_Doc_Mapping__c cs 
                        = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( 'Non Handed Over Cover Letter' );

                    System.debug( 'refused cs 2-' + cs);

                    if(cs != null
                    && String.isNotBlank( cs.Drawloop_Document_Package_Id__c ) 
                    && String.isNotBlank( cs.Delivery_Option_Id__c )) {

                        ddpId2 = cs.Drawloop_Document_Package_Id__c;
                        deliveryId2 = cs.Delivery_Option_Id__c;
                    }
                    else {
                        ApexPages.addmessage( new ApexPages.message(ApexPages.severity.Error
                        ,'Please configure \'Non Handed Over Cover Letter\' DDP details properly.'));
                    }
                }// End Cover letter generation
            //}

            if( String.isNotBlank( ddpId1 ) && String.isNotBlank( deliveryId1 ) 
            && String.isNotBlank( ddpId2 ) && String.isNotBlank( deliveryId2 ) 
            && !Test.isRunningTest() ) {
                Database.ExecuteBatch( new GenerateBUDrawloopDocumentBatch( objBu.Id , ddpId1, deliveryId1 ) );
                Database.ExecuteBatch( new GenerateBUDrawloopDocumentBatch( objBu.Id , ddpId2, deliveryId2 ) );
            }

            listBUToUpdate.add( new Booking_Unit__c(Id = objBU.Id, Generating_VAT_From_Batch__c=true));
        }//End for loop
        ApexPages.addmessage(
            new ApexPages.message( ApexPages.severity.INFO, 
            'Your request was successfully submitted. Please check the documents section for the document in a while.')
        ); 
        if( !listBUToUpdate.isEmpty() )
            update listBUToUpdate; 
    }
    
    public static void generateVAT( Booking_Unit__c objBu ) {
        
        DateTime dt = system.now().addMinutes(1);
        String day = string.valueOf(dt.day());
        String month = string.valueOf(dt.month());
        String hour = string.valueOf(dt.hour());
        String minute = string.valueOf(dt.minute());
        String second = string.valueOf(dt.second());
        String year = string.valueOf(dt.year());
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;



        VAT_AddendumDocGenerator vatGen = new VAT_AddendumDocGenerator();
        vatGen.objUnit = objBu;

        system.schedule('VAT Addendum Upload'+ system.now(), strSchedule, vatGen);    
  
    }

    public void execute(SchedulableContext ctx) {

        Riyadh_Rotana_Drawloop_Doc_Mapping__c cs;

        if( objUnit.Permitted_Use_Type__c.equalsIgnoreCase( 'Units With VAT' ) ) {
            
            cs = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( 'Unit with HO VAT Addendum' );
        }
        else if( objUnit.Permitted_Use_Type__c.equalsIgnoreCase( 'Units Without VAT' ) ) {

            cs = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( 'Unit w/o HO VAT Addendum' );
        }    
            System.debug( 'spa cs 1-' + cs);
        if(cs != null
        && String.isNotBlank( cs.Drawloop_Document_Package_Id__c ) 
        && String.isNotBlank( cs.Delivery_Option_Id__c )
        && !Test.isRunningTest() ) {
            Id jobId = Database.ExecuteBatch( new GenerateBUDrawloopDocumentBatch( objUnit.Id, cs.Drawloop_Document_Package_Id__c, cs.Delivery_Option_Id__c ) );
        }
        
    }
    
    public static void updateBu( Map<Id,Attachment> mapParentToAtt ) {
        
        System.debug('updateBu mapParentToAtt--' + mapParentToAtt);
        if( !mapParentToAtt.isEmpty() ) {
            for( Id buId : mapParentToAtt.keySet() ) {

                uploadVAT( buId, mapParentToAtt.get(buId).Id );
            }
        }
    }

    @future(callout=true)
    public static void uploadVAT( Id  buId, String strAttachmentId  ) {
        List<Booking_Unit__c> listBUToUpdate = new List<Booking_Unit__c>();
        List<SR_Attachments__c> lstSRAttachment = new List<SR_Attachments__c>();

        List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocReq = new  List<UploadMultipleDocController.MultipleDocRequest>();
        
        UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();
        Booking_Unit__c objBu =  [SELECT
                                            Id
                                            , Handover_Notice_URLs__c
                                            , Generating_VAT_From_Batch__c
                                            , VAT_Addendum_Generated__c
                                            , Is_Cover_Letter_Generated__c
                                            , Booking__r.Account__r.Person_Business_Email__c
                                            , Booking__r.Account__r.Name
                                            , Unit_Name__c
                                            , Permitted_Use_Type__c
                                            , VAT_addendum_Email_Sent_Date__c
                                            , Booking__r.Account__c
                                            , Requested_Price_AED__c
                                            , Property_Name__c
                                            , Inventory__r.Seller_Name__c
                                            , Handover_Status__c
                                            , Permitted_Use__c
                                        FROM
                                            Booking_Unit__c
                                        WHERE
                                            Id =: buId];
        //Attachment objAtt = (Attachment)JSON.deserialize(strAttachment, Attachment.Class );
        Attachment objAtt = [ SELECT Id, Body, parentId, name from Attachment WHERE id =: strAttachmentId];
        String  hoNoticeUrl =  '' ;
        Booking_Unit__c objBuUpdate = new Booking_Unit__c( Id = objBu.Id );
        Id attId = objAtt.Id;
        Boolean sendEmail = false;

        if( objAtt.name.contains( 'VAT Addendum' ) ) {
            //List<Attachment> lstVatAtt = [ SELECT Id, Body, parentId, name from Attachment  WHERE  parentId =: objBu.Id AND Name Like : '%Handed Over Cover Letter%' order BY createdDate DESC limit 1];
        
            /*if( !lstVatAtt.isEmpty() ) {
                objBuUpdate.VAT_Addendum_Generated__c = true;
                objBuUpdate.Is_Cover_Letter_Generated__c = true;
                sendEmail = true;
            }*/
            
            objBuUpdate.VAT_Addendum_Generated__c = true;
            sendEmail = true;
            
            if( !objBu.Generating_VAT_From_Batch__c  ) {
                UploadMultipleDocController.MultipleDocRequest reqObjCRF = new UploadMultipleDocController.MultipleDocRequest();
                reqObjCRF.base64Binary = EncodingUtil.base64Encode(objAtt.Body);
                reqObjCRF.category = 'Document';
                reqObjCRF.entityName = 'VAT Addendum.pdf';
                reqObjCRF.fileDescription = 'VAT Addendum.pdf';
                reqObjCRF.fileId = objBU.Id + '-VAT Addendum.'+'pdf';
                reqObjCRF.fileName = objBU.Id +'-VAT Addendum.'+'pdf';
                reqObjCRF.registrationId = objBU.Id;
                reqObjCRF.sourceFileName = 'IPMS-' + objBU.Id + '-' + 'CallTranscript'+'.'+'pdf';
                reqObjCRF.sourceId = 'IPMS-' + objBU.Id + '-' + 'CallTranscript';
                lstMultipleDocReq.add(reqObjCRF);
                
                SR_Attachments__c objAttach = new SR_Attachments__c();
                //objAttach.Case__c  = objCase.Id;
                objAttach.Booking_Unit__c  = objBU.Id;
                objAttach.isValid__c = true;
                objAttach.IsRequired__c = true;
                objAttach.Name = 'VAT Addendum';
                //objAttach.Attachment_URL__c = 'www.salesforce.com';
                objAttach.Need_Correction__c =  false;
                lstSRAttachment.add(objAttach);
                
                System.debug( 'VAT lstSRAttachment == ' + lstSRAttachment );
                    //objBuUpdate.Handover_Notice_URLs__c = hoNoticeUrl;
                
                if( !Test.isRunningTest() ) {
                    MultipleDocData = UploadMultipleDocController.getMultipleDocUrl( lstMultipleDocReq );
                }
                else {
                                
                    List<UploadMultipleDocController.MultipleDocResponse> data = new List<UploadMultipleDocController.MultipleDocResponse>();
                    UploadMultipleDocController.MultipleDocResponse objMultipleDocResponse = new UploadMultipleDocController.MultipleDocResponse();
                    objMultipleDocResponse.PROC_STATUS = 'S';
                    objMultipleDocResponse.url = 'www.google.com';
                    data.add(objMultipleDocResponse);
                    
                    MultipleDocData.Data = data;
                }
                System.debug( 'VAT MultipleDocData == ' + MultipleDocData );

                if( MultipleDocData != null 
                && MultipleDocData.status != 'Exception'
                && (MultipleDocData.Data != null || MultipleDocData.Data.Size() > 0)) {
                    system.debug('MultipleDocData '+ MultipleDocData );
                    for( Integer objInt = 0; objInt < lstSRAttachment.Size(); objInt++ ) {
                        UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[objInt];
                        system.debug('objData '+ objData );
                        if( objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url) ) {
                            
                            lstSRAttachment[objInt].Attachment_URL__c = objData.url;
                            //objBuUpdate.Value__c = objData.url;
                        }
                    }
                }
                UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[0];
                system.debug('in else if' );
                string vatDocURL = '';
                vatDocURL = '<li><a href="'+ objData.url + '"target="_blank">'+ 'VAT Addendum' + '</a></li>';
                system.debug('vatDocURL ='+ vatDocURL );
                String newHooticeUrl = '';
                if( String.isNotBlank( objBu.Handover_Notice_URLs__c ) ) {
                    system.debug('objBu.Handover_Notice_URLs__c ='+ objBu.Handover_Notice_URLs__c );
                    newHooticeUrl = objBu.Handover_Notice_URLs__c;
                    String existingURl = objBu.Handover_Notice_URLs__c;
                    existingURl = existingURl.remove('</ul>');
                    newHooticeUrl = existingURl + vatDocURL + '</ul>';
                }
                else {
                    newHooticeUrl = '<ul>' + vatDocURL + '</ul>';
                }

                System.debug( 'newHooticeUrl -- ' + newHooticeUrl);
                objBuUpdate.Handover_Notice_URLs__c = newHooticeUrl;
            }
        }
        /*else if (objAtt.name.contains( 'Handed Over Cover Letter' ) ) {
            List<Attachment> lstVatAtt = [ SELECT Id, Body, parentId, name from Attachment WHERE  parentId =: objBu.Id  AND Name Like : '%VAT Addendum%' order BY createdDate DESC limit 1];
            
            if( !lstVatAtt.isEmpty() ) {
                objBuUpdate.VAT_Addendum_Generated__c = true;
                objBuUpdate.Is_Cover_Letter_Generated__c = true;
                sendEmail = true;
            }
        }*/
        System.debug( 'VAT listBUToUpdate == ' + listBUToUpdate );
        if( sendEmail && objBu.Generating_VAT_From_Batch__c) {
            Boolean emailSent = VAT_AddendumDocNotifier.prepareSendGridEmail( objBu, null, false, '', false);
            if( emailSent )  {
                objBuUpdate.VAT_addendum_Email_Sent_Date__c = System.Today();
                objBuUpdate.VAT_Email_Sent__c = true;
            }

        }
        listBUToUpdate.add( objBuUpdate );
        if( !listBUToUpdate.isEmpty() )
                update listBUToUpdate;
        insert lstSRAttachment;

    }
}