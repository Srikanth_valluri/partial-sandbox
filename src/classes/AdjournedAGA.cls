/*-------------------------------------------------------------------------------------------------
Description: Invocable class, used in 'FM Access Card' process builder
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 02-01-2019       | Lochana Rajput   | 1. Added method to create schedule for adjourned AGA meeting
=============================================================================================================================
*/
public without sharing class AdjournedAGA {

	@InvocableMethod
	public static void createAdjournedAGASchedule(List<FM_Case__c> lstFMCases) {
		//Create adjourned AGA schedule
		List<AGASchedule> wrapperList = new List<AGASchedule>();
		List<FM_Case__c> lstFMCasesToUpdate = new List<FM_Case__c>();
		List<Task> lstTasks = new List<Task>();
		List<AGA_Schedule__c> lstAGASchedules = new List<AGA_Schedule__c>();
		Set<Id> FMCaseIds = new Set<Id>();
		for(FM_Case__c obj : lstFMCases) {
			FMCaseIds.add(obj.Id);
		}
		// Map<ID, FM_Case__c> mapFMCaseId_Schedule = new Map<ID, FM_Case__c>();
		for(FM_Case__c obj : [SELECT Id, AGA_Schedule__c,Property__c,
									AGA_Schedule__r.Meeting_Date__c,
									AGA_Schedule__r.Meeting_Time__c,
									AGA_Schedule__r.Meeting_Venue__c
							  FROM FM_Case__c
							  WHERE Id IN: FMCaseIds]) {

			AGA_Schedule__c newScheduleObj = new AGA_Schedule__c();
			system.debug('==obj.AGA_Schedule__r.Meeting_Date__c==='+obj.AGA_Schedule__r.Meeting_Date__c);
			system.debug('==obj.AGA_Schedule__r.Meeting_Date__c==='+obj.AGA_Schedule__c);
			newScheduleObj.Meeting_Date__c = obj.AGA_Schedule__r.Meeting_Date__c.addDays(7);
			newScheduleObj.Meeting_Time__c = obj.AGA_Schedule__r.Meeting_Time__c;
			newScheduleObj.Meeting_Venue__c = obj.AGA_Schedule__r.Meeting_Venue__c;
			newScheduleObj.Property__c = obj.Property__c;
			newScheduleObj.Type_of_Meeting__c = 'Adjourned AGA meeting';
			lstAGASchedules.add(newScheduleObj);
			// mapFMCaseId_Schedule.put();
			wrapperList.add(new AGASchedule(newScheduleObj, obj));

			//Create tasks
			Task noticeTaskObj = new Task();
			noticeTaskObj.whatId = obj.Id;
			noticeTaskObj.Subject = 'Send notice for adjourned AGA meeting';//Label.FM_MeetingConductedLabel;
			noticeTaskObj.Status = 'Not Started';
			noticeTaskObj.Priority = 'Normal';
			noticeTaskObj.Process_Name__c = 'AGA Process';
			noticeTaskObj.ActivityDate = Date.today().addDays(2);
			noticeTaskObj.Assigned_User__c = 'Property Manager';

			Task objTask = new Task();
			objTask.whatId = obj.Id;
			objTask.Subject = 'Book venue for adjourned AGA meeting';//Label.FM_MeetingConductedLabel;
			objTask.Status = 'Not Started';
			objTask.Priority = 'Normal';
			objTask.Process_Name__c = 'AGA Process';
			objTask.ActivityDate = Date.today().addDays(2);
			objTask.Assigned_User__c = 'Property Manager';
			lstTasks.add(objTask);
			lstTasks.add(noticeTaskObj);
		}//for
		insert lstAGASchedules;
		insert lstTasks;
		for(AGASchedule obj : wrapperList) {
			system.debug('==obj.objAGA.Id==='+obj.objAGA.Id);
			obj.objFMCase.AGA_Schedule__c = obj.objAGA.Id;
			obj.objFMCase.Meeting_Type__c = 'Adjourned AGA meeting';
			obj.objFMCase.Meeting_Conducted__c = '';
			lstFMCasesToUpdate.add(obj.objFMCase);
		}//for
		update lstFMCasesToUpdate;
	}

	public class AGASchedule {
		AGA_Schedule__c objAGA;
		FM_Case__c objFMCase;
		public AGASchedule (AGA_Schedule__c objAGA, FM_Case__c objFMCase) {
			this.objAGA = objAGA;
			this.objFMCase = objFMCase;
		}
	}

}