@isTest
public class SendEmailToCustomerTest {
    @isTest
    static void testSendEmailNonSalariedClients(){
        List<Account> accountList = new List<Account>();
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        objAccount.Nationality__c = 'UAE';
        objAccount.Email__pc = 'abc@z.com';
        objAccount.Party_ID__c = '12345';
        objAccount.Active_Customer__c = 'Active';      
        insert objAccount ;
        accountList.add(objAccount);
        
        Mortgage_Offer__c objMortgageOffer = new Mortgage_Offer__c();
        objMortgageOffer.Name = 'Test';
        objMortgageOffer.Type_Of_Client__c = 'Non Resident - Salaried Clients';
        insert objMortgageOffer;
        
        TriggerOnOffCustomSetting__c objTrigger = new TriggerOnOffCustomSetting__c();
        objTrigger.Name = 'CallingListTrigger';
        objTrigger.OnOffCheck__c = true;
        insert objTrigger;
        
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Service_Type__c = 'Handover';
        objCallingList.Sub_Purpose__c = 'Unit Viewing';
        objCallingList.Account__c = objAccount.Id;
        objCallingList.Mortgage_Offer__c = objMortgageOffer.Id;
        insert objCallingList;
        
        
                
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(objCallingList);
            SendEmailToCustomer objSendEmail = new SendEmailToCustomer(sc);
            objSendEmail.sendEmailSendGridCL();
            objSendEmail.redirectToDetailPage();
        Test.stopTest();
    }
    
    @isTest
    static void testSendEmailEmployedClients(){
        List<Account> accountList = new List<Account>();
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        objAccount.Nationality__c = 'UAE';
        objAccount.Email__pc = 'abc@z.com';
        objAccount.Party_ID__c = '12345';
        objAccount.Active_Customer__c = 'Active';      
        insert objAccount ;
        accountList.add(objAccount);
        
        Mortgage_Offer__c objMortgageOffer = new Mortgage_Offer__c();
        objMortgageOffer.Name = 'Test';
        objMortgageOffer.Type_Of_Client__c = 'Self Employed Clients';
        insert objMortgageOffer;
        
        TriggerOnOffCustomSetting__c objTrigger = new TriggerOnOffCustomSetting__c();
        objTrigger.Name = 'CallingListTrigger';
        objTrigger.OnOffCheck__c = true;
        insert objTrigger;
        
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Service_Type__c = 'Handover';
        objCallingList.Sub_Purpose__c = 'Unit Viewing';
        objCallingList.Account__c = objAccount.Id;
        objCallingList.Mortgage_Offer__c = objMortgageOffer.Id;
        insert objCallingList;
        
        
                
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(objCallingList);
            SendEmailToCustomer objSendEmail = new SendEmailToCustomer(sc);
            objSendEmail.sendEmailSendGridCL();
            objSendEmail.redirectToDetailPage();
        Test.stopTest();
    }
    
    @isTest
    static void testSendEmailSalariedClients(){
        List<Account> accountList = new List<Account>();
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        objAccount.Nationality__c = 'UAE';
        objAccount.Email__pc = 'abc@z.com';
        objAccount.Party_ID__c = '12345';
        objAccount.Active_Customer__c = 'Active';      
        insert objAccount ;
        accountList.add(objAccount);
        
        Mortgage_Offer__c objMortgageOffer = new Mortgage_Offer__c();
        objMortgageOffer.Name = 'Test';
        objMortgageOffer.Type_Of_Client__c = 'Salaried Clients';
        insert objMortgageOffer;
        
        TriggerOnOffCustomSetting__c objTrigger = new TriggerOnOffCustomSetting__c();
        objTrigger.Name = 'CallingListTrigger';
        objTrigger.OnOffCheck__c = true;
        insert objTrigger;
        
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Service_Type__c = 'Handover';
        objCallingList.Sub_Purpose__c = 'Unit Viewing';
        objCallingList.Account__c = objAccount.Id;
        objCallingList.Mortgage_Offer__c = objMortgageOffer.Id;
        insert objCallingList;
        
        
                
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(objCallingList);
            SendEmailToCustomer objSendEmail = new SendEmailToCustomer(sc);
            objSendEmail.sendEmailSendGridCL();
            objSendEmail.redirectToDetailPage();
        Test.stopTest();
    }
    
    @isTest
    static void testSendEmailSelfEmployed(){
        List<Account> accountList = new List<Account>();
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        objAccount.Nationality__c = 'UAE';
        objAccount.Email__pc = 'abc@z.com';
        objAccount.Party_ID__c = '12345';
        objAccount.Active_Customer__c = 'Active';      
        insert objAccount ;
        accountList.add(objAccount);
        
        Mortgage_Offer__c objMortgageOffer = new Mortgage_Offer__c();
        objMortgageOffer.Name = 'Test';
        objMortgageOffer.Type_Of_Client__c = 'Non Resident - Self Employed';
        insert objMortgageOffer;
        
        TriggerOnOffCustomSetting__c objTrigger = new TriggerOnOffCustomSetting__c();
        objTrigger.Name = 'CallingListTrigger';
        objTrigger.OnOffCheck__c = true;
        insert objTrigger;
        
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Service_Type__c = 'Handover';
        objCallingList.Sub_Purpose__c = 'Unit Viewing';
        objCallingList.Account__c = objAccount.Id;
        objCallingList.Mortgage_Offer__c = objMortgageOffer.Id;
        insert objCallingList;
        
        Riyadh_Rotana_Drawloop_Doc_Mapping__c objRiyadhRotana = new Riyadh_Rotana_Drawloop_Doc_Mapping__c();
        objRiyadhRotana.Name = 'Non Resident - Self Employed';
        objRiyadhRotana.Delivery_Option_Id__c = 'test';
        objRiyadhRotana.Drawloop_Document_Package_Id__c = 'test';
        insert objRiyadhRotana;
        
        Attachment objAttachment = new Attachment();    
        objAttachment.Name = 'Unit Test Attachment';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        objAttachment.body = bodyBlob;
        objAttachment.parentId = objCallingList.id;
        insert objAttachment;
                
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(objCallingList);
            SendEmailToCustomer objSendEmail = new SendEmailToCustomer(sc);
            objSendEmail.sendEmailSendGridCL();
            objSendEmail.redirectToDetailPage();
            SendEmailToCustomer.sendMortgageOfferToCx(objAttachment.Id);
        Test.stopTest();
    }
}