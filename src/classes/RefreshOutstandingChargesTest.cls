@isTest
private class RefreshOutstandingChargesTest
{
	@isTest
	static void updateOutstandingChargesTest() {
		Account customerAccount = TestDataFactoryFM.createAccount();
        insert customerAccount;
        CustomerCommunityUtils.customerAccountId = customerAccount.Id;

		NSIBPM__Service_Request__c objSr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert objSr;

        Booking__c  objBooking = TestDataFactoryFM.createBooking(customerAccount, objSr);
        insert objBooking;

		Booking_Unit__c objBookingUnit = TestDataFactoryFM.createBookingUnit(customerAccount, objBooking);
        insert objBookingUnit;

		FM_Case__c objFMCase = new FM_Case__c();
		objFMCase.Booking_Unit__c = objBookingUnit.id;
		insert objFMCase;

		Test.startTest();
			RefreshOutstandingCharges.updateOutstandingCharges(objFMCase.id);
		Test.stopTest();
	}
}