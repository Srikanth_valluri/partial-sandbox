@isTest
public class Damac_BookingReceiptController_Test {
    public static testMethod void method1() {
        Account acc = new Account ();
            acc.LastName = 'test';
            acc.Agency_Type__c = 'Corporate';
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        insert acc;
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);        
            sr.Eligible_to_Sell_in_Dubai__c = true;
            sr.Agency_Type__c = 'Individual';
            sr.ID_Type__c = 'Passport';
            sr.Token_Amount_AED__c = 40000;
            sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Change Agent').getRecordTypeId();
            sr.Booking_Wizard_Level__c = null;
            sr.Agency_Email_2__c = 'test2@gmail.com';
            sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
            sr.Country_of_Sale__c = 'UAE';
            sr.Mode_of_Payment__c = 'Online_Payment';
            sr.agency__c = acc.id;
            sr.Token_Attachment_Name__c = 'Unit Test Attachment';
        insert sr;
        Attachment attach=new Attachment();     
            attach.Name='Unit Test Attachment';
            Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
            attach.body = bodyBlob;
            attach.parentId = sr.id;
        insert attach;
        List<Booking__c> lstbk = new List<Booking__c>();
            lstbk.add(InitializeSRDataTest.createBooking(sr.id));
            lstbk.add(InitializeSRDataTest.createBooking(sr.id));
            lstbk[0].Deal_SR__c = sr.id;
        insert lstbk;
        buyer__c b = new buyer__c();
            b.Buyer_Type__c =  'Individual';
            b.Address_Line_1__c =  'Ad1';
            b.Country__c =  'United Arab Emirates';
            b.City__c = 'Dubai' ;       
            b.Account__c = acc.id;
            b.dob__c = system.today().addyears(-30);
            b.Email__c = 'test@test.com';
            b.First_Name__c = 'firstname' ;
            b.Last_Name__c =  'lastname';
            b.Nationality__c = 'Indian' ;
            b.Passport_Expiry__c = system.today().addyears(20) ;
            b.Passport_Number__c = 'J0565556' ;
            b.Phone__c = '569098767' ;
            b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
            b.Place_of_Issue__c =  'India';
            b.Title__c = 'Mr';
            b.booking__c = lstbk[0].id;
            b.Primary_Buyer__c =true;
        insert b;
        apexpages.currentpage().getparameters().put('id',sr.id);
        Damac_BookingReceiptController obj = New Damac_BookingReceiptController ();
    }
}