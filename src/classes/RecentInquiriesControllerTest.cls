/****************************************************************************************************
* Name          : RecentInquiriesControllerTest                                                     *
* Description   : Test Class for RecentInquiriesController                                          *
* Created Date  : 25/06/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Twinkle P     25/06/2018      Initial Draft.                                          *
****************************************************************************************************/
@isTest
public class RecentInquiriesControllerTest {

    /**
     * Method to insert records for Object Configuration
     */
    public static Inquiry__c createInquiries() {
        Inquiry__c inquiryInst = new Inquiry__c(
            First_Name__c = 'Test',
            Last_Name__c = 'Last Test',
            Inquiry_Source__c = 'Call Center',
            Inquiry_Status__c = 'Active',
            Email__c = 'test@gmail.com',
            OwnerId = UserInfo.getuserid()
        );
        insert inquiryInst;
        return inquiryInst;
    }
    /**
     * Method to test the records displayed on Load
     */
	static testMethod void testFetchInquiriesDetails() {
        Inquiry__c  inqObj = createInquiries();
        test.startTest();
        	RecentInquiriesController conInst = new RecentInquiriesController();
        test.stopTest();
		System.assert(conInst.inquiryDetails.isEmpty());
		System.assertEquals(conInst.inquiryDetails.size(), 0);
    }
    /**
     * Method to test the records displayed on Load
     */
	static testMethod void testShowRecords() {
        Inquiry__c  inqObj = createInquiries();
        test.startTest();
        	RecentInquiriesController conInst = new RecentInquiriesController();
        	conInst.numberLimit  = 10;
        	conInst.showRecords();
        test.stopTest();
		System.assert(conInst.inquiryDetails.isEmpty());
		System.assertEquals(conInst.inquiryDetails.size(), 0);
    }
    
}