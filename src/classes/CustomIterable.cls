global class CustomIterable implements Iterable<sobject> {
    
    list<sobject> results;
    global CustomIterable(list<sobject> data){
        results = data;
    }
  
    global Iterator<sobject> Iterator(){
        return new CustomIterator(results);
    }
}