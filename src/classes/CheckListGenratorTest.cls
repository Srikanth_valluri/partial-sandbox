/***********************************************************************************************
 * @Name              : CheckListGenratorTest
 * @Description       : Class to test ChecklistGenerator Class.
 * Modification Log
 * VERSION     AUTHOR                   DATE            Update Log
 * 1.0         Aishwarya Todkar         16/08/2020      Created
***********************************************************************************************/
@isTest
public  without sharing class CheckListGenratorTest {
	@TestSetup
	public static void setUpData(){
		Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();

		Location__c objLoc = new Location__c();
		objLoc.Location_Code__c = 'VRD';
		objLoc.Location_ID__c ='23123';
		objLoc.New_Ho_Doc_Type__c = 'Unit';
		insert objLoc;

		Inventory__c objInv = new Inventory__c();
		objInv.Building_Location__c = objLoc.id;
		insert objInv;

		List<Account> accList = new List<Account>();
        for(Integer i=0; i<2; i++){
           Account acc = new Account();
           acc = TestDataFactory_CRM.createPersonAccount();
           acc.Nationality__c = 'UAE'; 
           acc.party_ID__c = acc.party_ID__c + i;
           acc.Email__pc = 'test'+i+'@gmail.com';
           accList.add(acc);
        }
        insert accList;

		NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
		insert Sr;

		Booking__c Booking =  new Booking__c();
		Booking.AWB_Number__c = 'Test AWB';
		Booking.Account__c = accList[0].Id;
		Booking.Deal_SR__c = Sr.Id;
		insert Booking;

		Booking_Unit__c BU =  new Booking_Unit__c();
		BU.Unit_Name__c = 'Test Units';
		BU.Registration_Status__c  = 'Active';
		BU.Registration_ID__c   = '1234';
		BU.Unit_Selling_Price_AED__c  = 100;
		BU.Booking__c = Booking.Id;
		BU.Inventory__c = objInv.Id;
		insert BU;

		Case pCas = TestDataFactory_CRM.createCase(accList[0].Id,CaseRecordTypeId);
		pCas.Booking_Unit__c = BU.Id ;
		pCas.Type = 'NOCVisa';
		pCas.Document_Verified__c = true;
		pCas.Total_Snags_Closed__c = 3;
		insert pCas;
        
        List<Buyer__c> buyerList = new List<Buyer__c>();
        for(Integer i=0; i<2; i++){
            Buyer__c buyObj = new Buyer__c();
            buyObj.Account__c = accList[0].Id;
            buyObj.Booking__c = Booking.Id;
            buyObj.Booking_Unit__c = BU.Id;
            buyObj.Case__c = pCas.Id;
            buyObj.Primary_Buyer__c = false;
            buyerList.add(buyObj);
        }
        buyerList[0].Primary_Buyer__c = true;
        insert buyerList;
        
		List<Riyadh_Rotana_Drawloop_Doc_Mapping__c> newCustomSettingList = new List<Riyadh_Rotana_Drawloop_Doc_Mapping__c>();
		for(Integer i=1;i<12;i++){
			Riyadh_Rotana_Drawloop_Doc_Mapping__c customSettingObj = new Riyadh_Rotana_Drawloop_Doc_Mapping__c();
			customSettingObj.Name = 'CHECKLIST_OP_'+i+'_WITH_AV_E-SIGN';
			customSettingObj.Delivery_Option_Id__c = 'a0T0E000007BesO';
			customSettingObj.Drawloop_Document_Package_Id__c = 'a0V0E000003E0vR';
			customSettingObj.Document_Name__c = 'CheckList OP'+i+'with AV E-sign';
			newCustomSettingList.add(customSettingObj);
		}

		for(Integer i=1;i<12;i++){
			Riyadh_Rotana_Drawloop_Doc_Mapping__c customSettingObj1 = new Riyadh_Rotana_Drawloop_Doc_Mapping__c();
			customSettingObj1.Name = 'CHECKLIST_OP_'+i+'_AV';
			customSettingObj1.Delivery_Option_Id__c = 'a0T0E000007BesO';
			customSettingObj1.Drawloop_Document_Package_Id__c = 'a0V0E000003E0vR';
			customSettingObj1.Document_Name__c = 'CheckList OP'+i+'with AV';
			newCustomSettingList.add(customSettingObj1);
		}

		for(Integer i=1;i<12;i++){
			Riyadh_Rotana_Drawloop_Doc_Mapping__c customSettingObj = new Riyadh_Rotana_Drawloop_Doc_Mapping__c();
			customSettingObj.Name = 'CHECKLIST_OP_'+i+'_E-SIGN';
			customSettingObj.Delivery_Option_Id__c = 'a0T0E000007BesO';
			customSettingObj.Drawloop_Document_Package_Id__c = 'a0V0E000003E0vR';
			customSettingObj.Document_Name__c = 'CheckList OP'+i+'with E-sign';
			newCustomSettingList.add(customSettingObj);
		}

		for(Integer i=1;i<12;i++){
			Riyadh_Rotana_Drawloop_Doc_Mapping__c customSettingObj1 = new Riyadh_Rotana_Drawloop_Doc_Mapping__c();
			customSettingObj1.Name = 'CHECKLIST_OP_'+i;
			customSettingObj1.Delivery_Option_Id__c = 'a0T0E000007BesO';
			customSettingObj1.Drawloop_Document_Package_Id__c = 'a0V0E000003E0vR';
			customSettingObj1.Document_Name__c = 'CheckList OP'+i+'without E-Sign';
			newCustomSettingList.add(customSettingObj1);
		}

		Riyadh_Rotana_Drawloop_Doc_Mapping__c customSettingObj3 = new Riyadh_Rotana_Drawloop_Doc_Mapping__c();
		customSettingObj3.Name = 'DEFAULT_CHECKLIST';
		customSettingObj3.Delivery_Option_Id__c = 'a0T0E000007BesO';
		customSettingObj3.Drawloop_Document_Package_Id__c = 'a0V0E000003E0vR';
		customSettingObj3.Document_Name__c = 'CheckList OP 1 without E-Sign';
		newCustomSettingList.add(customSettingObj3);

		Riyadh_Rotana_Drawloop_Doc_Mapping__c customSettingObj2 = new Riyadh_Rotana_Drawloop_Doc_Mapping__c();
		customSettingObj2.Name = 'DEFAULT_CHECKLIST_E-SIGN';
		customSettingObj2.Delivery_Option_Id__c = 'a0T0E000007BesO';
		customSettingObj2.Drawloop_Document_Package_Id__c = 'a0V0E000003E0vR';
		customSettingObj2.Document_Name__c = 'CheckList OP 1 with E-Sign';
		newCustomSettingList.add(customSettingObj2);

		insert newCustomSettingList;
	}

   @isTest
	static void testGenrateHOCheckList1(){
		Booking_Unit__c BU = [Select Id from Booking_Unit__c Limit 1];
		Case objcase = [Select Id from Case Limit 1];
		System.Test.startTest();
		CheckListGenrator.genrateHOCheckList('Villa', 'Akoya', 'United Arab Emirates', 'Test_Building', 'Test_District','Test_property_City', 'Test_PermittedUse', BU.Id , 'Case', objCase.Id , false);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Akoya', 'United Arab Emirates', 'Test_Building', 'Test_District','Test_property_City', 'Test_PermittedUse',NULL  , 'BU', NULL , false);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'United Arab Emirates', 'Test_Building', 'DIFC','Test_property_City', 'Test_PermittedUse', NULL  , 'BU', NULL , false);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'United Arab Emirates', 'Test_Building', 'Test_District','Abu Dhabi', 'Test_PermittedUse', NULL  , 'BU', NULL , false);
		CheckListGenrator.genrateHOCheckList('PLOT', 'Test_Property', 'United Arab Emirates', 'Test_Building', 'Test_District','Test_property_City', 'Test_PermittedUse', BU.Id  , 'Case', objCase.Id , false);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'UPPER CREST', 'United Arab Emirates', 'Test_Building', 'Test_District','Test_property_City', 'Test_PermittedUse',  NULL  , 'BU', NULL , false);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'United Arab Emirates', 'Test_Building', 'Test_District','Test_property_City', 'Serviced', NULL  , 'BU', NULL , false);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'United Arab Emirates', 'BDW', 'Test_District', 'Test_property_City','Test_PermittedUse',NULL  , 'BU', NULL , false);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'United Arab Emirates', 'BDM', 'Test_District','Test_property_City', 'Test_PermittedUse', NULL  , 'BU', NULL , false);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'United Arab Emirates', 'Test_Building', 'Test_District','Test_property_City', 'Test_PermittedUse', NULL  , 'BU', NULL , false);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'Lebanon', 'Test_Building', 'Test_District', 'Test_property_City','Test_PermittedUse', NULL  , 'BU', NULL , false);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'Qatar', 'Test_Building', 'Test_District','Test_property_City', 'Test_PermittedUse', NULL  , 'BU', NULL , false);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'Jordan', 'Test_Building', 'Test_District','Test_property_City', 'Test_PermittedUse', NULL  , 'BU', NULL , false);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'Saudi', 'DEA', 'Test_District','Test_property_City', 'Test_PermittedUse', NULL  , 'BU', NULL , false);
		System.Test.stopTest();
	}
	@isTest
	static void testGenrateHOCheckList2(){
		System.Test.startTest();
		CheckListGenrator.genrateHOCheckList('Villa', 'Akoya', 'United Arab Emirates', 'Test_Building', 'Test_District','Test_property_City', 'Test_PermittedUse', NULL  , 'BU', NULL , true);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Akoya', 'United Arab Emirates', 'Test_Building', 'Test_District','Test_property_City', 'Test_PermittedUse',NULL  , 'BU', NULL , true);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'United Arab Emirates', 'Test_Building', 'DIFC','Test_property_City', 'Test_PermittedUse', NULL  , 'BU', NULL , true);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'United Arab Emirates', 'Test_Building', 'Test_District','Abu Dhabi', 'Test_PermittedUse', NULL  , 'BU', NULL , true);
		CheckListGenrator.genrateHOCheckList('PLOT', 'Test_Property', 'United Arab Emirates', 'Test_Building', 'Test_District','Test_property_City', 'Test_PermittedUse', NULL  , 'BU', NULL , true);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'UPPER CREST', 'United Arab Emirates', 'Test_Building', 'Test_District','Test_property_City', 'Test_PermittedUse',  NULL  , 'BU', NULL , true);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'United Arab Emirates', 'Test_Building', 'Test_District','Test_property_City', 'Serviced', NULL  , 'BU', NULL , true);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'United Arab Emirates', 'BDW', 'Test_District', 'Test_property_City','Test_PermittedUse',NULL  , 'BU', NULL , true);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'United Arab Emirates', 'BDM', 'Test_District','Test_property_City', 'Test_PermittedUse', NULL  , 'BU', NULL , true);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'United Arab Emirates', 'Test_Building', 'Test_District','Test_property_City', 'Test_PermittedUse', NULL  , 'BU', NULL , true);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'Lebanon', 'Test_Building', 'Test_District', 'Test_property_City','Test_PermittedUse', NULL  , 'BU', NULL , true);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'Qatar', 'Test_Building', 'Test_District','Test_property_City', 'Test_PermittedUse', NULL  , 'BU', NULL , true);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'Jordan', 'Test_Building', 'Test_District','Test_property_City', 'Test_PermittedUse', NULL  , 'BU', NULL , true);
		CheckListGenrator.genrateHOCheckList('Test_Type', 'Test_Property', 'Saudi', 'DEA', 'Test_District','Test_property_City', 'Test_PermittedUse', NULL  , 'BU', NULL , true);
		System.Test.stopTest();
	}

	/************************************************************************
	* @Description : Method to test the creation of Handover Checklist document
	*                for E-sign
	* @Params      : None
	* @Return      : None
	*************************************************************************/
	@isTest
	static void generateAkoyaHandoverChecklistESign(){
		Booking_Unit__c BU = [Select Id from Booking_Unit__c Limit 1];
		Case objcase = [Select Id from Case Limit 1];
		Test.startTest();
		CheckListGenrator.genrateVirtualHOCheckList('Villa', 'Akoya', 'United Arab Emirates',
			 'Test_Building', 'Test_District','Test_property_City', 'Test_PermittedUse',
			  BU.Id  , 'Case', objCase.Id , true);
		Test.stopTest();

		List<Attachment> attList = [SELECT Id, ParentId FROM Attachment WHERE ParentId = :BU.Id];
		System.assertEquals(false, !attList.isEmpty(), 'Handover Checklist document should be created on Booking unit.');
	}

	/************************************************************************
	* @Description : Method to test the creation of Handover Checklist document
	*                without E-sign
	* @Params      : None
	* @Return      : None
	*************************************************************************/
	@isTest
	static void generateAkoyaHandoverChecklistWOESign(){
		Booking_Unit__c BU = [Select Id from Booking_Unit__c Limit 1];
		Case objcase = [Select Id from Case Limit 1];
		Test.startTest();
		CheckListGenrator.genrateHOCheckList('Villa', 'Akoya', 'United Arab Emirates', 'Test_Building',
			 'Test_District','Test_property_City', 'Test_PermittedUse', BU.Id  , 'Case', objCase.Id , true);
		Test.stopTest();

		List<Attachment> attList = [SELECT Id, ParentId FROM Attachment WHERE ParentId = :BU.Id];
		System.assertEquals(false, !attList.isEmpty(), 'Handover Checklist document should be created on Booking unit.');
	}
}