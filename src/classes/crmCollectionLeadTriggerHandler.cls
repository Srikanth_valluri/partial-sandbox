//Test class : crmCollectionLeadTriggerHandlerTest
public class crmCollectionLeadTriggerHandler {

    public void beforeInsert (List <Collection__c> newLeads) {
        if (Trigger.isInsert && Trigger.isBefore) {
            
            Set <ID> accountIds = new Set <ID> ();
            
            for (Collection__c lead :newLeads) {
                if (lead.Account__c != null) {
                    accountIds.add (lead.Account__c);
                }
            }
            
            Map <ID, Account> accounts = new Map <ID, Account> ([SELECT 
                                                                    IsPersonAccount, Mobile_Phone_Encrypt__pc, Mobile__c 
                                                                FROM 
                                                                    Account 
                                                                WHERE Id IN: accountIds]);
            
            List <String> lstMobileCountryCode = new List <String> ();
            lstMobileCountryCode = getDynamicPicklistValues('Inquiry__c', 'Mobile_CountryCode__c');
            
            for (Collection__c lead :newLeads) {
                if (lead.Account__c != null) {
                    String mobileNumber = '';
                    if (accounts.get (lead.Account__c).IsPersonAccount) {
                        mobileNumber = accounts.get (lead.Account__c).Mobile_Phone_Encrypt__pc;                        
                    }
                    else {
                        mobileNumber = accounts.get (lead.Account__c).Mobile__c;
                    }
                    if (mobileNumber != '') {
                        for (String code :lstMobileCountryCode) {
                            if (mobileNumber.startsWith (code.trim())) {
                                mobileNumber = mobileNumber.replace (code, '');
                                break;
                            }
                            if (mobileNumber.startsWith ('00'+code.trim())) {
                                mobileNumber = mobileNumber.replace ('00'+code, '');
                                break;
                            }
                        }
                        lead.Mobile_Phone__c = mobileNumber;
                    }
                }
            }
            
        }
    }
    
    public static List<String> getDynamicPicklistValues(String ObjectApi_name, String Field_name){
        List<String> lstPickvals=new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType();
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : pick_list_values) {
            String countryCode = a.getValue().split (': 00')[1];
            lstPickvals.add(countryCode);
        }
        return lstPickvals;
    }
}