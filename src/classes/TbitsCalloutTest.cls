/**************************************************************************************************
* Name               : TbitsCalloutTest                                                           *
* Description        : Test class for TbitsCallout utility class.                                 *
* Created Date       : 25/05/2017                                                                 *
* Created By         : PWC                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         NSI - Vineet      25/05/2017      Initial Draft.                                    *
**************************************************************************************************/
@isTest
private class TbitsCalloutTest {
	private static Account acc;
    static void setupData() {
        acc = new Account(name='testacc');
        insert acc;
    }
    
    @isTest static void test_method_1() {
    	setupData();
    	TbitsCallout tcObject = new TbitsCallout(new BuyerDetailsController());
        TbitsCallout.cls_errorDescription obj1 = new TbitsCallout.cls_errorDescription('test');
        TbitsCallout.cls_fields obj2 = new TbitsCallout.cls_fields();
        TbitsCallout.cls_ReferenceID obj3 = new TbitsCallout.cls_ReferenceID();
        system.debug('#### acc = '+acc);
        TbitsCallout.getPassportDetails('base64, test====', 'test.jpeg', acc.Id, 'Passport');
        TbitsCallout.getPassportDetails('base64, test=', 'test.jpeg', acc.Id, 'Passport');
        TbitsCallout.getPassportDetails('base64, test', 'test.jpeg', acc.Id, 'Passport');
    }
    
     @isTest static void test_method_2() {
    	setupData();
    	TbitsCallout tcObject = new TbitsCallout(new BuyerDetailsController());
        TbitsCallout.cls_errorDescription obj1 = new TbitsCallout.cls_errorDescription('test');
        TbitsCallout.cls_fields obj2 = new TbitsCallout.cls_fields();
        TbitsCallout.cls_ReferenceID obj3 = new TbitsCallout.cls_ReferenceID();
        system.debug('#### acc = '+acc);
        TbitsCallout.getPassportDetails('base64, test', 'test.jpeg', acc.Id, 'Id');
    }
}// End of class.