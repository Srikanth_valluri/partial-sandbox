@isTest
public class fmcRequestForWorkPermitControllerNewTest {
    public static testmethod void createFMCase(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        PageReference myVfPage = Page.CommunityPortal;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Email__c = 'test@test.com';
        fmCaseObj.Account__c = acctIns.id;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view','ApplyWorkPermit');
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('Id',fmCaseObj.Id);
        ApexPages.currentPage().getParameters().put('SRType','Apply_for_Work_Permits');
        //ApexPages.currentPage().getParameters().put('Id',Opp.Id);
        fmcRequestForWorkPermitControllerNew obj=new fmcRequestForWorkPermitControllerNew();

        Test.startTest();
        obj.objFMCase=fmCaseObj;
        obj.getUnitDetailsNew();
        obj.createRequestForWorkPermit();
        Test.stopTest();
    }

    public static testmethod void submitFMCase(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.fm_role__c ='FM Manager';
        insert fmUser;

        PageReference myVfPage = Page.CommunityPortal;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c = Date.today();
        fmCaseObj.Contact_person__c = 'test';
        fmCaseObj.Description__c = 'test';
        fmCaseObj.Contact_person_contractor__c = 'test';
        fmCaseObj.Email__c = 'test@test.com';
        fmCaseObj.Request_Type_DeveloperName__c = 'Apply_for_Work_Permits';
        insert fmCaseObj;

        Contractor_Information__c contractorObj = new Contractor_Information__c();
        contractorObj.Employee_Name__c = 'test';
        contractorObj.Emirates_ID__c = '123121';
        contractorObj.Employee_Mobile_Number__c = '212';
        contractorObj.Company_Name__c = 'Test';
        insert contractorObj;

        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view','ApplyWorkPermit');
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Apply_for_Work_Permits');
        ApexPages.currentPage().getParameters().put('noOfEmp','1');
        fmcRequestForWorkPermitControllerNew  obj=new fmcRequestForWorkPermitControllerNew();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';

        Test.startTest();
        obj.createCaseShowUploadDoc();
        //obj.uploadDocument();
        obj.strFMCaseId = fmCaseObj.id;
        obj.objFMCase=fmCaseObj;
        obj.createContractorInformation();
        obj.notifyContractorConsultantNew();        //newly added
        obj.submitRequestForWorkPermit();
        Test.stopTest();
    }

    public static testmethod void createFMCaseExistingCase(){
        Id RecTypeFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('NOC For FitOut').getRecordTypeId();
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        PageReference myVfPage = Page.CommunityPortal;
        FM_Case__c fmCaseIns=new Fm_Case__c();
        fmCaseIns.recordtypeid = RecTypeFMCase;
        fmCaseIns.Account__c = acctIns.id;
        fmCaseIns.Booking_Unit__c = buIns.id;
        fmCaseIns.status__c = 'Closed';
        fmCaseIns.Approval_Status__c = 'Approved';
        fmCaseIns.Permit_To_Work_For__c = 'Hot Works;Confined Spaces';
        insert fmCaseIns;
        //Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view','ApplyWorkPermit');
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Apply_for_Work_Permits');
        fmcRequestForWorkPermitControllerNew obj=new fmcRequestForWorkPermitControllerNew();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';

        Test.startTest();
        obj.strAccountId = acctIns.id;
        obj.strFMCaseId = fmCaseObj.id;
        obj.getUnitDetailsNew();
        obj.notifyContractorConsultant();
        obj.createRequestForWorkPermit();
        Test.stoptest();
    }

    public static testmethod void testInsertAttachment(){

        //inserting custom setting
        insert new Credentials_Details__c(Name = 'office365RestServices',
                                          User_Name__c = '418c72c7-e498-4114-a1b2-7fbfd78596a1',
                                          Password__c = 'oBWyXtEbQ2gEjzNZpF.MX/pWOtr-44]8',
                                          Resource__c = 'https://graph.microsoft.com',
                                          grant_type__c = 'client_credentials');

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        PageReference myVfPage = Page.CommunityPortal;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Apply_for_Work_Permits';
        insert fmCaseObj;

        Contractor_Information__c contractorObj = new Contractor_Information__c();
        contractorObj.Employee_Name__c = 'test';
        contractorObj.Emirates_ID__c = '123121';
        contractorObj.Employee_Mobile_Number__c = '212';
        contractorObj.Company_Name__c = 'Test';
        insert contractorObj;

        //Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view','ApplyWorkPermit');
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Apply_for_Work_Permits');
        ApexPages.currentPage().getParameters().put('noOfEmp','1');
        fmcRequestForWorkPermitControllerNew  obj=new fmcRequestForWorkPermitControllerNew();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';

        Test.startTest();
        obj.strFMCaseId=fmCaseObj.id;
        obj.objFMCase=fmCaseObj;
        obj.uploadDocument();
        obj.createContractorInformation();
        obj.submitRequestForWorkPermit();
        Test.stoptest();
    }

    public static testmethod void submitFMCaseNew(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.fm_role__c ='FM Manager';
        insert fmUser;

        PageReference myVfPage = Page.CommunityPortal;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c = Date.today();
        fmCaseObj.Contact_person__c = 'test';
        fmCaseObj.Description__c = 'test';
        fmCaseObj.Contact_person_contractor__c = 'test';
        fmCaseObj.Email__c = 'test@test.com';
        fmCaseObj.Request_Type_DeveloperName__c = 'Apply_for_Work_Permits';
        insert fmCaseObj;

        Contractor_Information__c contractorObj = new Contractor_Information__c();
        contractorObj.Employee_Name__c = 'test';
        contractorObj.Emirates_ID__c = '123121';
        contractorObj.Employee_Mobile_Number__c = '212';
        contractorObj.Company_Name__c = 'Test';
        insert contractorObj;

        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view','ApplyWorkPermit');
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Apply_for_Work_Permits');
        ApexPages.currentPage().getParameters().put('noOfEmp','1');
        fmcRequestForWorkPermitControllerNew  obj=new fmcRequestForWorkPermitControllerNew();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';

        Test.startTest();
        obj.createCaseShowUploadDoc();
        obj.strFMCaseId = fmCaseObj.id;
        obj.objFMCase=fmCaseObj;
        obj.createContractorInformation();
        obj.proceedToSR = true;
        obj.showOTPBlock = false;
        obj.submitRequestForWorkPermit();
        Test.stopTest();
    }

    public static testmethod void createFMCaseNew(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Building_Location__c = locObj.id;
        insert invObj;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        PageReference myVfPage = Page.CommunityPortal;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Email__c = 'test@test.com';
        fmCaseObj.Account__c = acctIns.id;
        fmCaseObj.Submitted__c = true;
        fmCaseObj.Approval_Status__c = 'Approved';
        insert fmCaseObj;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view','ApplyWorkPermit');
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('Id',fmCaseObj.Id);
        ApexPages.currentPage().getParameters().put('SRType','Apply_for_Work_Permits');
        fmcRequestForWorkPermitControllerNew obj=new fmcRequestForWorkPermitControllerNew();

        Test.startTest();
        obj.objFMCase = fmCaseObj;
        obj.strSelectedUnit = buIns.id;
        obj.getUnitDetailsNew();
        obj.createRequestForWorkPermit();
        Test.stopTest();
    }

    public static testmethod void createFMCaseExistingCaseNew(){
        Id RecTypeFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('NOC For FitOut').getRecordTypeId();
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        PageReference myVfPage = Page.CommunityPortal;
        FM_Case__c fmCaseIns=new Fm_Case__c();
        fmCaseIns.recordtypeid=RecTypeFMCase;
        fmCaseIns.Account__c=acctIns.id;
        fmCaseIns.Booking_Unit__c=buIns.id;
        fmCaseIns.status__c='Closed';
        fmCaseIns.Approval_Status__c='Approved';
        //fmCaseIns.Request_Type__c='Work Permit';
        fmCaseIns.Permit_To_Work_For__c='Hot Works;Confined Spaces';
        insert fmCaseIns;
        //Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view','ApplyWorkPermit');
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Apply_for_Work_Permits');
        fmcRequestForWorkPermitControllerNew obj=new fmcRequestForWorkPermitControllerNew();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';

        Test.startTest();
            obj.strFMCaseId=fmCaseObj.id;
            obj.notifyContractorConsultant();
            obj.createRequestForWorkPermit();
        Test.stoptest();
    }
}