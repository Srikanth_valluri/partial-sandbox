@isTest
private class createJSONInquiry_Test{
  static testMethod void test_createInquiry_UseCase1(){
    
string req1='{"title": "Mr",   "firstName": "Ramu",   "lastName": "Kaaka",   "email": "ramu@gmail.com",   "phoneNumber": "019167708340",   "city": "Mumbai",   "country": "India",   "countryCode": "0091",   "preferredLanguage": "English",   "utmSource": "default",   "utmMedium": "default",   "utmCampaign": "default",   "campaignId": "default",   "source": "default",   "heardOfDAMAC": "default",   "message": "MESSAGE",   "mobileCode": "default",   "telephone": "019167708340",   "nationality": "Indian",   "webSource": "default",   "adGroup": " default",   "campaignNameHOD": "default",   "goal": "default",   "digitalSource": "default",   "digitalMedium": "default",   "bannerSize": "default",   "keyword": "default",   "placement": "default",   "adPosition": "default",   "matchType": "default",   "network": "default",   "bidType": "default",   "leadSource": "default",   "lastMileConversion": "default",   "device": "desktop",   "projectName": "default",   "os": "default",   "resolution": "default",   "browser": "default",   "GAUserID": "default",   "GCLID": "default",   "timeSpentbeforeFormSubmit": "default",   "ipAddress": "default",   "landingPageURL": "default",   "websiteLanguage": "default",   "originalWebSource": "default",   "originalMobileNumber": "default",   "originalEmailId": "default" }';

RestRequest req = new RestRequest();
RestResponse res = new RestResponse();
req.requestURI = '/services/apexrest/CreateRestInquiry';  //Request URL
req.httpMethod = 'POST';//HTTP Request Type
req.requestBody = Blob.valueof(req1);
RestContext.request = req;
RestContext.response = res;
createJSONInquiry.createInquiry();

  }
}