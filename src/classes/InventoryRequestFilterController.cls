/***************************************************************************************************
 * @Name              : InventoryRequestFilterController
 * @Test Class Name   : InventoryRequestFilterControllerTest 
 * @Description       : 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         19/08/2020       Created
****************************************************************************************************/
public class InventoryRequestFilterController{
    public Id derId {get;set;}
    public boolean filterExists {get; set;}
    public boolean commentExists {get; set;}
    public boolean restrictedApproval {get; set;}
    public string filters {get; set;}
    public Map<String, String> filterMap {get; set;}
    public List<String> filterTypeList {get; set;}
    public string recordLink {get; set;}
    
    public Deal_Exception_Request__c der{
        get{
        system.debug('Inside GET Der');
        system.debug('derId: ' +derId);
        filterExists = false;
        commentExists = false;
        restrictedApproval = false;
        filterTypeList = new List<String>();
        filterMap = new Map<String, String>();
        der = [SELECT Id, Name, Inventory_Request_Filters__c, RM_Comments__c, RM__r.Name, RM__c,
                    Contains_Restricted_Inventories__c, Status__c, Inventory_Request_Number__c
               FROM Deal_Exception_Request__c WHERE Id =: derId];
        filters = '';
        if(der.Contains_Restricted_Inventories__c && 
                (der.Status__c == 'Awaiting Sales Admin Manager Additional Approval' || der.Status__c == 'Awaiting MIS HOD Approval')){
            restrictedApproval = true;
        }
        system.debug('der: ' +der);
        if(der.Inventory_Request_Filters__c != null && der.Inventory_Request_Filters__c != ''){
            filterExists = true;
            system.debug('Inside if1');
            for(String eachFilter: der.Inventory_Request_Filters__c.split(';')){
                system.debug('Inside for loop');
                if(eachFilter != null && eachFilter != '' && eachFilter.split(':').size() == 2){
                    system.debug('Inside if 2');
                    String filterType = eachFilter.split(':')[0].trim();
                    String filterValue = eachFilter.split(':')[1];
                    if(filterType != '' && filterValue != ''){
                        filterMap.put(filterType, filterValue);
                        filterTypeList.add(filterType);
                        filters += filterType + ': ' + filterValue + '\n';
                    }
                }
            } 
        }
        system.debug('filters: ' +filters);
        
        if(der.RM_Comments__c != null && der.RM_Comments__c != ''){
            //der.RM_Comments__c = der.RM_Comments__c.replace('\n', '<br/>');
            commentExists = true;
        }
        if(restrictedApproval){
            recordLink = System.URL.getSalesforceBaseUrl().toExternalForm() + '/apex/RestrictedInventoryApproval?id=' + der.Id; 
        } else {
            recordLink = System.URL.getSalesforceBaseUrl().toExternalForm() + '/apex/RequestInventory?id=' + der.Id; 
        }
        return der;
        }  set;
    }
}