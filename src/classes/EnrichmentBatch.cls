/****************************************************************************************
Description: Batch Class to call Enrichment Adfolks service.
----------------------------------------------------------------------------------------
Version     Date           Author               Description                                 
1.0       16/9/2020    Ruchika Choudhary       Initial Draft
*****************************************************************************************/
public class EnrichmentBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {

    /* Method Description : start method of Batch to query all records 
    */    
    public Database.QueryLocator start(Database.BatchableContext BC){
        
        String strQuery = 'SELECT Id, Person_Business_Email__c'+
                           ' FROM Account' +
                            ' WHERE Party_ID__c != NULL' +
                            ' AND Active_Customer__c != NULL'+
                            ' AND Active_Customer__c = \'Active\'';

         System.debug('strQuery == ' + strQuery);
         return Database.getQueryLocator(strQuery);
    }
    
    
    /* Method Description : Execute method of Batch to call Enrichment Adfolks 
    *                       service 
    */
    public void execute(Database.BatchableContext BC, List<Account> scope){
       for (Account acc : scope ) {
           if (acc != null && acc.Person_Business_Email__c != null) {
               EnrichmentService.sendHttpRequest(acc.Id, acc.Person_Business_Email__c);
           }
           
       }
    }
    
    
    /* Method Description : Finish method of Batch.
    */
    public void finish(Database.BatchableContext BC){
    }
}