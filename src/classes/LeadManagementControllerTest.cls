/*-------------------------------------------------------------------------------------------------
Description: Test class for controller LeadManagementController

============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 28-03-2018       | Lochana Rajput   | 1. Initial draft
----------------------------------------------------------------------------------------------------------------------------
2.0     | 29-03-2018       | Lochana Rajput   | 1. Added methods to test different scenarios
=============================================================================================================================
*/

@isTest
private class LeadManagementControllerTest {
    
    /*@isTest static void test_bookingUnits() {
        Id RecordTypeIdContact = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.recordtypeid=RecordTypeIdContact;
        SR.Eligible_to_Sell_in_Dubai__c = true;
        SR.Agency_Type__c = 'Individual';
        SR.ID_Type__c = 'Passport';
        // SR.Agency__c = a.id;
        SR.Agency_Email_2__c = 'test2@gmail.com';
        SR.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        SR.Country_of_Sale__c = 'UAE;KSA;Lebanon';
        insert SR;
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = SR.Id;
        insert objBooking;
        List<Booking_Unit__c> lstBUs = new List<Booking_Unit__c>();
        for(Integer i=0; i<5; i++) {
            lstBUs.add(new Booking_Unit__c(Booking__c = objBooking.ID));
        }
        insert lstBUs;
        Test.startTest();
        Test.stopTest();
    }
    @isTest static void test_NobookingUnitSelected() {
        Test.startTest();
        LeadManagementController controller = new LeadManagementController();
        controller.getBuyerDetails();
        Test.stopTest();
        System.assertEquals('error',controller.msgType);
    }*/

    @isTest static void test_NobookingUnitSelected1() {
        list<Lead_Admin_Console__mdt> lstRules = [Select Id from Lead_Admin_Console__mdt];
        System.debug('===test_NobookingUnitSelected1===TestClass====lstRules============='+lstRules);
        Test.startTest();
        LeadManagementController controller = new LeadManagementController();
        controller.getBuyers();
        Test.stopTest();
        System.assertEquals('error',controller.msgType);
    }
    
    /*@isTest static void test_ProspectingInquiry() {
        Id RecordTypeIdContact = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        
        Booking_Unit_Active_Status__c obj = new Booking_Unit_Active_Status__c(
                Status_Value__c='Agreement executed by DAMAC', 
                Name='Agreement executed by DAMAC');
        insert obj;
        SR.recordtypeid=RecordTypeIdContact;
        SR.Eligible_to_Sell_in_Dubai__c = true;
        SR.Agency_Type__c = 'Individual';
        SR.ID_Type__c = 'Passport';
        // SR.Agency__c = a.id;
        SR.Agency_Email_2__c = 'test2@gmail.com';
        SR.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        SR.Country_of_Sale__c = 'UAE;KSA;Lebanon';
        insert SR;
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = SR.Id;
        insert objBooking;
        List<Booking_Unit__c> lstBUs = new List<Booking_Unit__c>();
        for(Integer i=0; i<5; i++) {
            lstBUs.add(new Booking_Unit__c(Booking__c = objBooking.ID, Unit_Name__C='Test'+i,
                                          Registration_Status__c = 'Agreement executed by DAMAC'));
        }
        insert lstBUs;


        lstBUs = [SELECT Id, Unit_Name__C from Booking_Unit__c WHERE Id IN: lstBUs];
        // Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
        Inquiry__c inq = InitialiseTestData.getInquiryDetails('Agent Team',455);
        inq.First_Name__c = 'Test First name';
        inq.Last_Name__c = 'Test Last name';
        inq.Email__c = 'test@gmail.com';
        inq.Inquiry_Source__c = 'Customer Referral';
        insert inq;
        Buyer__c buyerObj = new Buyer__c();
        buyerObj.First_Name__c = 'Test First name';
        buyerObj.Last_Name__c = 'Test Last name';
        buyerObj.Email__c = 'test@gmail.com';
        buyerObj.Inquiry__c = inq.Id;
        buyerObj.Booking__c = objBooking.ID;
        insert buyerObj;
        Inquiry__c inqNew = InitialiseTestData.getInquiryDetails('Agent Team',855);
        //inqNew.Inquiry_Source__c = 'Customer Referral';
        insert inqNew;
        inqNew = [SELECT Id, Name from Inquiry__c where Id=:inqNew.Id];
        buyerObj = [SELECT ID FROM Buyer__c LIMIT 1];
        
        // Implement test code
        Test.startTest();
        LeadManagementController controller = new LeadManagementController();
        controller.selectedbookingUnit = lstBUs[0].Unit_Name__C;
        controller.getBuyers();
        controller.selectedBuyer = buyerObj.Id;
        controller.selectedInquiry = inqNew.Name;
        controller.getBuyerDetails();
        controller.save();
        controller.cancel();
        Test.stopTest();
        //System.assertEquals('success',controller.msgType);
    }*/

    @isTest static void test_ProspectingInquiry1() {
        Id RecordTypeIdContact = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        
        Booking_Unit_Active_Status__c obj = new Booking_Unit_Active_Status__c(
                Status_Value__c='Agreement executed by DAMAC', 
                Name='Agreement executed by DAMAC');
        insert obj;
        SR.recordtypeid=RecordTypeIdContact;
        SR.Eligible_to_Sell_in_Dubai__c = true;
        SR.Agency_Type__c = 'Individual';
        SR.ID_Type__c = 'Passport';
        // SR.Agency__c = a.id;
        SR.Agency_Email_2__c = 'test2@gmail.com';
        SR.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        SR.Country_of_Sale__c = 'UAE;KSA;Lebanon';
        insert SR;
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = SR.Id;
        insert objBooking;
        List<Booking_Unit__c> lstBUs = new List<Booking_Unit__c>();
        for(Integer i=0; i<5; i++) {
            lstBUs.add(new Booking_Unit__c(Booking__c = objBooking.ID, Unit_Name__C='Test'+i,
                                          Registration_Status__c = 'Agreement executed by DAMAC'));
        }
        insert lstBUs;
        
        Campaign__c cmp = InitialiseTestData.createCampaign();
        cmp.Campaign_Name__c = 'Be My Neighbour';
        insert cmp;

        lstBUs = [SELECT Id, Unit_Name__C from Booking_Unit__c WHERE Id IN: lstBUs];
        // Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
        Inquiry__c inq = InitialiseTestData.getInquiryDetails('Agent Team',455);
        inq.First_Name__c = 'Test First name';
        inq.Last_Name__c = 'Test Last name';
        inq.Email__c = 'test@gmail.com';
        inq.Inquiry_Source__c = 'Agent Referral'; 
        //inq.Inquiry_Source__c = 'Customer Referral';
        inq.Campaign__c = cmp.Id;
        insert inq;
        Buyer__c buyerObj = new Buyer__c();
        buyerObj.First_Name__c = 'Test First name';
        buyerObj.Last_Name__c = 'Test Last name';
        buyerObj.Email__c = 'test@gmail.com';
        buyerObj.Inquiry__c = inq.Id;
        buyerObj.Booking__c = objBooking.ID;
        insert buyerObj;
        Inquiry__c inqNew = InitialiseTestData.getInquiryDetails('Agent Team',855);
        //inqNew.Inquiry_Source__c = 'Customer Referral';
        insert inqNew;
        inqNew = [SELECT Id, Name from Inquiry__c where Id=:inqNew.Id];
        buyerObj = [SELECT ID FROM Buyer__c LIMIT 1];
        
        // Implement test code
        Test.startTest();
        LeadManagementController controller = new LeadManagementController();
        controller.selectedbookingUnit = lstBUs[0].Unit_Name__C;
        controller.getBuyers();
        controller.getBuyerDetails();
        controller.selectedBuyer = buyerObj.Id;
        controller.selectedInquiry = inqNew.Name;
        controller.getBuyerDetails();
        controller.leadObj.Same_Buyer_Details__c = 'YES';
        Blob blob1=Blob.valueOf('Test Data');

        controller.otherAttachmentBody = EncodingUtil.base64Encode(blob1);
        controller.otherAttachmentName = 'c://test.pdf';
        controller.additionalAttachmentBody = EncodingUtil.base64Encode(blob1);
        controller.additionalAttachmentName = 'c://test.pdf';
        controller.ccfAttachmentBody = EncodingUtil.base64Encode(blob1);
        controller.ccfAttachmentName = 'c://test.pdf';
        controller.save();
        controller.cancel();
        controller.clear();
        
        LeadManagementController.searchCampaign ('CAM-');
        Test.stopTest();
        //System.assertEquals('success',controller.msgType);
    }

    /*@isTest static void test_NoDetailsMatch() {
        Id RecordTypeIdContact = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.recordtypeid=RecordTypeIdContact;
        SR.Eligible_to_Sell_in_Dubai__c = true;
        SR.Agency_Type__c = 'Individual';
        SR.ID_Type__c = 'Passport';
        // SR.Agency__c = a.id;
        SR.Agency_Email_2__c = 'test2@gmail.com';
        SR.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        SR.Country_of_Sale__c = 'UAE;KSA;Lebanon';
        insert SR;
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = sr.Id;
        insert objBooking;
        List<Booking_Unit__c> lstBUs = new List<Booking_Unit__c>();
        Id campaignRoadshowId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId();
        Campaign__c newCampaign = InitialiseTestData.getCampaignDetails();
        newCampaign.RecordTypeId = campaignRoadshowId;
        newCampaign.Campaign_Name__c = 'Roadshow';
        newCampaign.Credit_Control_Active__c= true;
        newCampaign.Marketing_Active__c = true;
        newCampaign.Sales_Admin_Active__c = true;
        insert newCampaign;
        for(Integer i=0; i<5; i++) {
           
            lstBUs.add(new Booking_Unit__c(Booking__c = objBooking.ID, Unit_Name__C='Test'+i,
                                          Registration_Status__c = 'Agreement executed by DAMAC'));
        
        }
        lstBUs[0].Related_Campaign__c = newCampaign.Id;
        insert lstBUs;
        lstBUs = [SELECT Id, Unit_Name__C from Booking_Unit__c WHERE Id IN: lstBUs];
        Inquiry__c inq = InitialiseTestData.getInquiryDetails('Agent Team',455);
        inq.Inquiry_Source__c = 'Customer Referral';
        insert inq;
        
        Buyer__c buyerObj = new Buyer__c();
        buyerObj.Inquiry__c = inq.Id;
        buyerObj.Booking__c = objBooking.ID;
        insert buyerObj;
        Inquiry__c inqNew = InitialiseTestData.getInquiryDetails('Agent Team',855);
        inqNew.Inquiry_Source__c = 'Customer Referral';
        inqNew.Campaign__c = newCampaign.Id;
        insert inqNew;
        buyerObj = [SELECT ID FROM Buyer__c LIMIT 1];
        inqNew = [SELECT ID,Name FROM Inquiry__c LIMIT 1];
        // Implement test code
        Test.startTest();
        LeadManagementController controller = new LeadManagementController();
        controller.selectedbookingUnit = lstBUs[0].Unit_Name__C;
        controller.getBuyers();
        controller.selectedBuyer = buyerObj.Id;
        controller.selectedInquiry = inqNew.Name;
        controller.getBuyerDetails();
        controller.otherAttachmentBody = 'test body';
        controller.otherAttachmentName = 'c://test.pdf';
        controller.additionalAttachmentBody = 'test body';
        controller.additionalAttachmentName = 'c://test.pdf';
        controller.ccfAttachmentBody = 'test body';
        controller.ccfAttachmentName = 'c://test.pdf';
        controller.save();
        controller.clear();
        controller.cancel();
        Lead_Management__c lead = new Lead_Management__c();
        insert lead;
        Blob blob1=Blob.valueOf('Test Data');
        blob1= controller.extractBody(EncodingUtil.base64Encode(blob1));

        String name = controller.extractName('c://test.pdf');

        Test.stopTest();
        System.assertEquals('error',controller.msgType);
    }*/
    
}