/************************************************************************************************
 * @Name              : DAMAC_Agents_UpdateDeviceTokenId_API
 * @Description       : To update DeviceTokenId to the User for Damac Agents App
 * @Test Class        : DAMAC_Agents_UpdateDeviceTokenIdTest
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         11/08/2020       Created
***********************************************************************************************/

@RestResource(urlMapping='/updateDeviceToken')
/* 
URL to test

POST - /services/apexrest/updateDeviceToken

*/
global class DAMAC_Agents_UpdateDeviceTokenId_API{

    @HttpPost
    global static APIResponse updateDeviceTokenId() {
        APIResponse objResponse = new APIResponse ();                
        String userId = RestContext.request.params.get('userId');     
        String deviceTokenId = RestContext.request.params.get('deviceTokenId');
        String unRegister = RestContext.request.params.get('unRegister');
        List<User> theUser = new List<User>();        
        try {
            
            if(userId != '' && userId != NULL){
                theUser = [SELECT Id,Device_Token_ID__c FROM User WHERE Id =: userId LIMIT 1];
                if(theUser.size() > 0 ){
                    if(!String.isBlank(deviceTokenId) && (unRegister == 'false' || String.isBlank(unRegister))){
                        //Registering
                        theUser[0].Device_Token_ID__c = deviceTokenId;                    
                        update theUser;
                        objResponse.isSuccess = true;                    
                        objResponse.statusMessage = 'DeviceToken Updated successfully.';
                        objResponse.statusCode = 200;
                    }
                    
                    if(String.isBlank(deviceTokenId) && unRegister == 'true'){
                        //Unregistering
                        theUser[0].Device_Token_ID__c = '';                    
                        update theUser;
                        objResponse.isSuccess = true;                    
                        objResponse.statusMessage = 'Unregistered User successfully.';
                        objResponse.statusCode = 200;
                    }
                    if(!String.isBlank(deviceTokenId) && unRegister == 'true'){
                        objResponse.isSuccess = false;
                        objResponse.statusMessage = 'You cannot register and unregister at the same time!';
                        objResponse.statusCode = 401;    
                    
                    }
                    if(String.isBlank(deviceTokenId) && (unRegister == 'false' || String.isBlank(unRegister))){
                        objResponse.isSuccess = false;
                        objResponse.statusMessage = 'Please give any one of the operations to proceed';
                        objResponse.statusCode = 401;                        
                    }                    
                }
                else{
                    objResponse.isSuccess = false;                    
                    objResponse.statusMessage = 'No User is found with the given details';
                    objResponse.statusCode = 204;
                }
            }
            else{
                objResponse.isSuccess = false;                    
                objResponse.statusMessage = 'Add UserId';
                objResponse.statusCode = 401;
            
            }                               
            
        } catch (Exception e) {
            objResponse.isSuccess = false;
            objResponse.statusMessage = 'Error';
            objResponse.statusCode = 400;
        }
        return objResponse;        
        
    }
    
    // To send the response back to API 
    global class APIResponse {
        public Boolean isSuccess {get; set;}
        public String statusMessage {get; set;}
        public Integer statusCode {get; set;}
    }

}