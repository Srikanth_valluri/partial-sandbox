@isTest
private class Agent_LOI_Controller_Test {

    @isTest static void test_method(){   
        NSIBPM__Service_Request__c LOI_SR = new NSIBPM__Service_Request__c();
        LOI_SR.Agency_Corporate_Type__c = '';
        LOI_SR.P_O_Box_Zip_Postal_Code__c = '';
        LOI_SR.Country_of_Sale__c = '';
        LOI_SR.Zip_Postal_Code__c = '';
        LOI_SR.CreatedDate = System.now();
        LOI_SR.First_Name__c = '';
        LOI_SR.Last_Name__c = '';
        LOI_SR.Agency_Type__c = '';
        LOI_SR.Country__c = '';
        LOI_SR.City__c = '';
        LOI_SR.Street__c = '';
        LOI_SR.Street2__c = '';
        LOI_SR.Agency_Name__c = '';
        LOI_SR.P_O_Box_No__c = '';
        LOI_SR.Emirate_State__c = '';
        String JSONString = JSON.serialize(LOI_SR);
        
        PageReference Agent_LOI_Jordan_Page = Page.Agent_LOI_Jordan;
        Test.setCurrentPageReference(Agent_LOI_Jordan_Page);
        Agent_LOI_Jordan_Page.getParameters().put('srJSON', JSONString);
        Agent_LOI_Controller instance = new Agent_LOI_Controller();
    }

}