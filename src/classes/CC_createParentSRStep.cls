/******************************************************************************************************************
* Name               : CC_createParentSRStep 
* Description        : This is the custom code class for creating Sales Audit Step on Parent SR Of Change Agent SR         
* Created Date       : 06/04/2017                                                                 
* Created By         : Alok - DAMAC                                                       
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE                                                              
* 1.0         Alok -DAMAC    01/09/2017 
* 1.1         Alok- DAMAC    12/11/2017 Modified Condition To Include Step Rejection as Open Step
* 1.2         Alok-DAMAC     14/02/2018 Added Condition to Create SA Step.                                              
*******************************************************************************************************************/

global without sharing class CC_createParentSRStep implements NSIBPM.CustomCodeExecutable {
    public string EvaluateCustomCode(NSIBPM__Service_Request__c SR, NSIBPM__Step__c step) 
{
        string retString = 'Success';
        try{    
         if (step !=null)
         {
            String srId ='';
            String srType='INSERT';
            list<NSIBPM__Service_Request__c> getParentSR = new list<NSIBPM__Service_Request__c>();
            getParentSR = [select id,NSIBPM__Parent_SR__c from NSIBPM__Service_Request__c where id=:step.NSIBPM__SR__c limit 1];
            if (getParentSR.size() >0)
            {
            srId= getParentSR[0].NSIBPM__Parent_SR__c;
            }
            System.Debug('ParentSR>>>>'+srId);
            if (srId !=null)
            {
            list<NSIBPM__Step__c> checkSalesAudit = new list<NSIBPM__Step__c>();
            checkSalesAudit = [select id,NSIBPM__SR__c,NSIBPM__Status__c,NSIBPM__Step_Status__c from NSIBPM__Step__c where NSIBPM__Step_Template__r.NSIBPM__Code__c='SALES_AUDIT' and NSIBPM__SR__c=:srId];
            Integer i = checkSalesAudit.size();
            Integer j =0;
            if (i> 0){
            while(j<i)
            {
            if(checkSalesAudit[j].NSIBPM__Step_Status__c=='Sales Audit Review' ||checkSalesAudit[j].NSIBPM__Step_Status__c=='Sales Audit On Hold'||checkSalesAudit[j].NSIBPM__Step_Status__c=='Sales Audit Rejected'){
            srType='UPDATE';
            break;
            }
            else{
            j++;
            }
            }
            }
            
           /* if(checkSalesAudit.size() > 0){
                if (checkSalesAudit[0].NSIBPM__Step_Status__c=='Sales Audit Review' || checkSalesAudit[0].NSIBPM__Step_Status__c=='Sales Audit On Hold')
                {
                srType='UPDATE';
                }  
                }*/
        System.Debug('WhatUpdate>>>>>'+srType);
       if (srType =='INSERT')
       {
            NSIBPM__SR_Template__c template = new NSIBPM__SR_Template__c();
            template = [select id,(select id,NSIBPM__Start_Status__c,NSIBPM__Step_Template__c,NSIBPM__SR_Steps__c.OwnerId,NSIBPM__Step_No__c,NSIBPM__SR_Template__r.Name from  NSIBPM__SR_Steps_SR_Template__r where NSIBPM__Step_Template_Code__c='SALES_AUDIT' and
                        NSIBPM__Start_Status__c!=null and 
                        NSIBPM__Step_Template__c!=null and 
                        ownerid != null and 
                        NSIBPM__Step_No__c!=null LIMIT 1 ) from NSIBPM__SR_Template__c where Name='Deal' limit 1];
                        
            system.debug(template.NSIBPM__SR_Steps_SR_Template__r +'>>>');
            
            if(template.NSIBPM__SR_Steps_SR_Template__r[0] != null){         
                        NSIBPM__Step__c newStep = new NSIBPM__Step__c();
                        newStep.NSIBPM__SR__c = srId;
                        newStep.NSIBPM__SR_Step__c = template.NSIBPM__SR_Steps_SR_Template__r[0].id;
                        newStep.NSIBPM__Status__c =  template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Start_Status__c;
                        newStep.NSIBPM__Step_Template__c = template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_Template__c;
                        newStep.OwnerId = template.NSIBPM__SR_Steps_SR_Template__r[0].ownerid;
                        newStep.NSIBPM__Step_No__c  = template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_No__c;
                        newStep.NSIBPM__Sys_Step_Loop_No__c= template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_No__c+'_1';
              
                insert newStep;
            }
            }//END:if (srType =='INSERT')
            }
            else
             {
            NSIBPM__SR_Template__c template = new NSIBPM__SR_Template__c();
            template = [select id,(select id,NSIBPM__Start_Status__c,NSIBPM__Step_Template__c,NSIBPM__SR_Steps__c.OwnerId,NSIBPM__Step_No__c,NSIBPM__SR_Template__r.Name from  NSIBPM__SR_Steps_SR_Template__r where NSIBPM__Step_Template_Code__c='SALES_AUDIT' and
                        NSIBPM__Start_Status__c!=null and 
                        NSIBPM__Step_Template__c!=null and 
                        ownerid != null and 
                        NSIBPM__Step_No__c!=null LIMIT 1 ) from NSIBPM__SR_Template__c where Name='Deal' limit 1];
                        
            system.debug(template.NSIBPM__SR_Steps_SR_Template__r +'>>>');
            
            if(template.NSIBPM__SR_Steps_SR_Template__r[0] != null){         
                        NSIBPM__Step__c newStep = new NSIBPM__Step__c();
                        newStep.NSIBPM__SR__c = getParentSR[0].Id;
                        newStep.NSIBPM__SR_Step__c = template.NSIBPM__SR_Steps_SR_Template__r[0].id;
                        newStep.NSIBPM__Status__c =  template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Start_Status__c;
                        newStep.NSIBPM__Step_Template__c = template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_Template__c;
                        newStep.OwnerId = template.NSIBPM__SR_Steps_SR_Template__r[0].ownerid;
                        newStep.NSIBPM__Step_No__c  = template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_No__c;
                        newStep.NSIBPM__Sys_Step_Loop_No__c= template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_No__c+'_1';
              
                insert newStep;
            }
            }

            }//END:if (step !=null)
            }//END:Try
        catch(exception e){
        retString =string.valueOf(e.getMessage());
        }
 return(retString);       
        }
    }