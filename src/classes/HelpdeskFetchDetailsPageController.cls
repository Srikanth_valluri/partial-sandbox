public with sharing class HelpdeskFetchDetailsPageController {
  public HelpdeskFetchDetailsPageController(ApexPages.StandardController sc) {}

  public PageReference fetchTaskDetails(){
          String sessionId;
          // try {
              // System.debug('===taskId==='+taskId);
        String FMCaseId = ApexPages.currentPage().getParameters().get('id');
              System.debug('===FMCaseId==='+FMCaseId);
              // Query FM case
              List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
              lstFMCases = [SELECT task_id__c, Id from FM_Case__c
                            WHERE Id =: FMCaseId
                            AND task_id__c != NULL];
            if(lstFMCases.size() > 0) {
                //Get session id
                Integer taskID = Integer.valueOf(lstFMCases[0].task_id__c);
                sessionId = getSessionIDfromCAFM();
                schemasDatacontractOrg200407FsiConc_t32.TaskDtoV3 CAFMTaskObj =
                    new schemasDatacontractOrg200407FsiConc_t32.TaskDtoV3();
                  wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3  obj = new wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3 ();
                CAFMTaskObj = obj.GetTaskById(sessionId,taskID);
                System.debug('===CAFMTaskObj==='+CAFMTaskObj.LongDescription);
                FM_Case__c objFMCase = new FM_Case__c();
                objFMCase.Id = FMCaseId;
                objFMCase.CAFM_Task_Code__c = CAFMTaskObj.Code;
                objFMCase.Long_Description__c = CAFMTaskObj.LongDescription;
                // objFMCase.Task_Priority__c = CAFMTaskObj.Priority;
                 objFMCase.CAFM_Description__c = CAFMTaskObj.Description;
                objFMCase.CAFM_Task_State__c = CAFMTaskObj.State;
                update objFMCase;
        return new PageReference('/'+FMCaseId);
            }

          // }
          // catch(Exception excp) {
              // System.debug('===excp==='+excp);
          // }
      return NULL;
      }
    public String getSessionIDfromCAFM() {
      try{
          wwwFsiCoUkServicesEvolution0409.SecurityService1 obj = new wwwFsiCoUkServicesEvolution0409.SecurityService1();
          obj.timeout_x = 120000;
          schemasDatacontractOrg200407FsiPlat.AuthenticationDto result = obj.Authenticate(Label.CAFM_Username,Label.CAFM_Password);
          system.debug('>>result>>>'+result);
          if(result != NULL) {
              if(result.OperationResult == 'Succeeded') {
                  System.debug('===result.SessionId=='+result);
                  return result.SessionId;
              }
          }

      }
      catch(System.Exception excp) {
          System.debug('==excp==SESSION ID='+excp);
          insert new Error_Log__c(Error_Details__c =excp.getMessage() + ': '+excp.getLineNumber(),
        // FM_Case__c = objFMCase.Id,
        Process_Name__c = 'Helpdesk' );
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,excp.getMessage() + excp.getLineNumber()));
      }
     return NULL;
  }
}