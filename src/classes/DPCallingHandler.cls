/*
* This class is used To Update Dp Percent Value on DP Calling List.
*
*  Revision History: 

*
*  Version     Author               Date            Description 
*  1.0                              18/01/2018      Initial Draft          
*  1.1         Aishwarya Todkar     25/06/2020      Added null checks.            
*/
public class DPCallingHandler {
  //public static Id DPcallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('DP Calling List').RecordTypeId;
    public static Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
     public static List<Calling_List__c> updateCallingList;
    public static Map<Id,List<Calling_List__c>>paymentPlanIdToLstOfCallingMap;
        
   /*public static void onBeforeInsertDpPercent( List<Calling_List__c> lstCallingList ) {
     //system.debug('inside DP '+lstCallingList);
     updateCallingList = new List<Calling_List__c>() ;
      for(Calling_List__c objCall :lstCallingList ){
           //system.debug('inside objCall '+objCall.Payment_Plan__c);
            if(objCall.RecordTypeId == DPcallingRecordTypeId ){
                if(String.isNotBlank(objCall.Payment_Plan__c)){
                    updateCallingList.add(objCall);
                }
            }   
      }
      if(updateCallingList != null && !updateCallingList.isEmpty()){
        //System.debug('call Dp pecnt1');
        populateDpPercent();
      }
    }*/
   public static void UpdateDpPercentOnCallingList(list<Calling_List__c> lstCallingList ) {
       updateCallingList = new List<Calling_List__c>() ;
           for(Calling_List__c objCall :lstCallingList ){
              if(objCall.RecordTypeId == collectioncallingRecordTypeId && objCall.Calling_List_Type__c == 'DP Calling'){
                  if(String.isNotBlank(objCall.Payment_Plan__c)){
                      updateCallingList.add(objCall);
                  }
              }   
          }
          if(updateCallingList != null && !updateCallingList.isEmpty()){
            populateDpPercent();
          }
   }
   public static void populateDpPercent(){
       Set<Id>paymentPlaIdSet = new Set<Id>();
       paymentPlanIdToLstOfCallingMap = new  Map<Id,List<Calling_List__c>>();
       List<Calling_List__c>callingLst = new List<Calling_List__c>();
       Map<Id,Payment_Terms__c>paymentPlanIdToDpPaymentTermMap = new Map<Id,Payment_Terms__c>();
       /*get all payment plans */
       if(updateCallingList != null && !updateCallingList.isEmpty()){
         for(Calling_List__c callInst : updateCallingList){
           if(!paymentPlanIdToLstOfCallingMap.containsKey(callInst.Payment_Plan__c)){
             paymentPlanIdToLstOfCallingMap.put(callInst.Payment_Plan__c,new List<Calling_List__c>{callInst});
           }
           else{
             paymentPlanIdToLstOfCallingMap.get(callInst.Payment_Plan__c).add(callInst);
           }
         }
       }
       /*get DP payment term related to those payment plans*/
       if(paymentPlanIdToLstOfCallingMap != null && !paymentPlanIdToLstOfCallingMap.isEmpty()){
         paymentPlaIdSet.addAll(paymentPlanIdToLstOfCallingMap.keySet());
         List<Payment_Plan__c>paymentPlanLst = [SELECT Id,
                                 Name,
                                 (SELECT Id,
                                       Name,
                                       Installment__c,
                                       Percent_Value__c
                                    FROM Payment_Terms__r
                                   WHERE Installment__c = 'DP'
                                   LIMIT 1)
                             FROM Payment_Plan__c
                             WHERE Id IN :paymentPlaIdSet];
        if(paymentPlanLst != null && !paymentPlanLst.isEmpty()){
            for(Payment_Plan__c paymentPlanObj : paymentPlanLst){
                if( paymentPlanObj.Payment_Terms__r != null && paymentPlanObj.Payment_Terms__r.size() > 0 )//Added by AT
                    paymentPlanIdToDpPaymentTermMap.put(paymentPlanObj.Id,paymentPlanObj.Payment_Terms__r);
            }
        }
           /*update calling list records with Dp percent value*/
            for(Id paymentPlanId : paymentPlanIdToLstOfCallingMap.keySet()){
                callingLst.addAll(paymentPlanIdToLstOfCallingMap.get(paymentPlanId));
                for(Calling_List__c callObj : callingLst){
                    if( paymentPlanIdToDpPaymentTermMap.containsKey(paymentPlanId) // Addec by AT
                    && paymentPlanIdToDpPaymentTermMap.get(paymentPlanId) != null //Added by AT
                    && paymentPlanIdToDpPaymentTermMap.get(paymentPlanId).Percent_Value__c != null ) //Added by AT
                        callObj.DP_Percent__c = paymentPlanIdToDpPaymentTermMap.get(paymentPlanId).Percent_Value__c;
                }
            }
        }
    }   
}