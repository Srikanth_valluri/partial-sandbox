@isTest 
public class Office365RestServiceMock{
    public class Office365RestServiceMockPositive implements HttpCalloutMock {
            public HTTPResponse respond(HTTPRequest req) {
                HTTPResponse res = new HTTPResponse();
                if (req.getEndpoint().endsWith('token')) {
                    res.setBody('{'+
                        '"token_type": "Bearer",'+  
                        '"expires_in": "3600",'+
                        '"ext_expires_in": "3600",'+
                        '"expires_on": "1570628201",'+
                        '"not_before": "1570624301",'+
                        '"resource": "https://graph.microsoft.com",'+
                        '"access_token": "eyJ0eXAiOiJKV1QiLCJub25jZSI6IkN3MVpSbEFnNTZKaUt4Mkd2Tl"'+
                    '}');
                    res.setStatusCode(200);
                    return res;
                } else if (req.getEndpoint().endsWith('preview')) {
                    res.setBody('{'+
                        '"@odata.context": "https://graph.microsoft.com/v1.0/$metadata#microsoft.graph.itemPreviewInfo",'+
                        '"getUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/",'+
                        '"postParameters": null,'+
                    '"postUrl": null'+
                    '}');
                    res.setStatusCode(200);
                    return res;
                } else if(req.getEndpoint().endsWith('content')) {
                    //res.setHeader('Location','https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/download');
                    res.setBody('{'+
                        '"@microsoft.graph.downloadUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/download",'+
                        '"id": "123456789A",'+
                        '"webUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/Documents/test.pdf"'+
                    '}');
                    res.setStatusCode(302);
                    return res;
                }
                else if(req.getEndpoint().endsWith(')')) {
                    res.setBody('{'+
                        '"@microsoft.graph.downloadUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/download",'+
                        '"value" :[{"id": "123456789A"}] '+
                    '}');
                    res.setStatusCode(200);
                    return res;
                }else {
                    System.assert(false, 'unexpected endpoint ' + req.getEndpoint());
                    return null;
                }
            }
        }
    
    public class Office365RestServiceMockPositive2 implements HttpCalloutMock {
            public HTTPResponse respond(HTTPRequest req) {
                HTTPResponse res = new HTTPResponse();
                if (req.getEndpoint().endsWith('token')) {
                    res.setBody('{'+
                        '"token_type": "Bearer",'+  
                        '"expires_in": "3600",'+
                        '"ext_expires_in": "3600",'+
                        '"expires_on": "1570628201",'+
                        '"not_before": "1570624301",'+
                        '"resource": "https://graph.microsoft.com",'+
                        '"access_token": "eyJ0eXAiOiJKV1QiLCJub25jZSI6IkN3MVpSbEFnNTZKaUt4Mkd2Tl"'+
                    '}');
                    res.setStatusCode(200);
                    return res;
                } else if (req.getEndpoint().endsWith('preview')) {
                    res.setBody('{'+
                        '"@odata.context": "https://graph.microsoft.com/v1.0/$metadata#microsoft.graph.itemPreviewInfo",'+
                        '"getUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/",'+
                        '"postParameters": null,'+
                    '"postUrl": null'+
                    '}');
                    res.setStatusCode(200);
                    return res;
                } else if(req.getEndpoint().endsWith('content')) {
                    //res.setHeader('Location','https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/download');
                    res.setBody('{'+
                        '"@microsoft.graph.downloadUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/download",'+
                        '"id": "123456789A",'+
                        '"webUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/Documents/test.pdf"'+
                    '}');
                    res.setStatusCode(200);
                    return res;
                }
                else if(req.getEndpoint().endsWith(')')) {
                    res.setBody('{'+
                        '"@microsoft.graph.downloadUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/download",'+
                        '"value" :[{"id": "123456789A"}] '+
                    '}');
                    res.setStatusCode(200);
                    return res;
                }else {
                    System.assert(false, 'unexpected endpoint ' + req.getEndpoint());
                    return null;
                }
            }
        }

        public class Office365RestServiceMockPositiveForWhtsApp implements HttpCalloutMock {
            public HTTPResponse respond(HTTPRequest req) {
                HTTPResponse res = new HTTPResponse();
                if (req.getEndpoint().endsWith('content')) {
                    res.setBody('{'+
                        '"@microsoft.graph.downloadUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/download",'+
                        '"id": "123456789A",'+
                        '"webUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/Documents/test.pdf"'+
                    '}');
                    res.setStatusCode(302);
                    return res;
                }
                else if (req.getEndpoint().endsWith('token')) {
                    res.setBody('{'+
                        '"token_type": "Bearer",'+  
                        '"expires_in": "3600",'+
                        '"ext_expires_in": "3600",'+
                        '"expires_on": "1570628201",'+
                        '"not_before": "1570624301",'+
                        '"resource": "https://graph.microsoft.com",'+
                        '"access_token": "eyJ0eXAiOiJKV1QiLCJub25jZSI6IkN3MVpSbEFnNTZKaUt4Mkd2Tl"'+
                    '}');
                    res.setStatusCode(200);
                    return res;
                } else if (req.getEndpoint().endsWith('preview')) {
                    res.setBody('{'+
                        '"@odata.context": "https://graph.microsoft.com/v1.0/$metadata#microsoft.graph.itemPreviewInfo",'+
                        '"getUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/",'+
                        '"postParameters": null,'+
                    '"postUrl": null'+
                    '}');
                    res.setStatusCode(200);
                    return res;
                }
                else if(req.getEndpoint().endsWith(')')) {
                    res.setBody('{'+
                        '"@microsoft.graph.downloadUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/download",'+
                        '"value" :[{"id": "123456789A"}] '+
                    '}');
                    res.setStatusCode(200);
                    return res;
                }
                else {
                    //System.assert(false, 'unexpected endpoint ' + req.getEndpoint());
                    return null;
                }
            }
        }
    
    
    public class Office365RestServiceMockNegative implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse res = new HTTPResponse();
            if (req.getEndpoint().endsWith('token')) {
                res.setBody('{'+
                    '"token_type": "Bearer",'+  
                    '"expires_in": "3600",'+
                    '"ext_expires_in": "3600",'+
                    '"expires_on": "1570628201",'+
                    '"not_before": "1570624301",'+
                    '"resource": "https://graph.microsoft.com",'+
                    '"access_token": "eyJ0eXAiOiJKV1QiLCJub25jZSI6IkN3MVpSbEFnNTZKaUt4Mkd2Tl"'+
                '}');
                res.setStatusCode(200);
                return res;
            } else if (req.getEndpoint().endsWith('preview')) {
                res.setBody('{'+
                    '"reason": "bad code"'+  
                        '}');
                res.setStatusCode(400);
                return res;
            } else if(req.getEndpoint().endsWith('content')) {
                res.setBody('{'+
                    '"reason": "bad code"'+  
                        '}');
                res.setStatusCode(400);
            return res;
            }else {
                System.assert(false, 'unexpected endpoint ' + req.getEndpoint());
                return null;
            }
        }
    } 

    public class  Office365RestServiceMockName implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse res = new HTTPResponse();
            if (req.getEndpoint().endsWith('token')) {
                res.setBody('{'+
                    '"token_type": "Bearer",'+  
                    '"expires_in": "3600",'+
                    '"ext_expires_in": "3600",'+
                    '"expires_on": "1570628201",'+
                    '"not_before": "1570624301",'+
                    '"resource": "https://graph.microsoft.com",'+
                    '"access_token": "eyJ0eXAiOiJKV1QiLCJub25jZSI6IkN3MVpSbEFnNTZKaUt4Mkd2Tl"'+
                '}');
                res.setStatusCode(200);
                return res;
            } else if (req.getEndpoint().endsWith(')')) {
                res.setBody('{'+
                        '"@microsoft.graph.downloadUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/download",'+
                        '"id": "123456789A",'+
                        '"webUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/Documents/test.pdf"'+
                    '}');
                res.setStatusCode(200);
                return res;
            } else {
                System.assert(false, 'unexpected endpoint ' + req.getEndpoint());
                return null;
            }
        }
    }      
}