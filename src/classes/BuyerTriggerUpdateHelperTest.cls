/*                                                        *
* Description        : Test Class for BuyerTrigger                                      *
* Created Date       : 30/11/2017                                                                 *
* Created By         : Swapnil Gholap                                                                      *
*/
@isTest
public class BuyerTriggerUpdateHelperTest{
    
    static testMethod void Test1(){
        test.StartTest();
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Buyer__c objBuyer = new Buyer__c();
        objBuyer.First_Name__c = 'Test';
        objBuyer.Last_Name__c  = 'Test';
        objBuyer.Booking__c   = objBooking.id;
        objBuyer.Primary_Buyer__c = true;
        objBuyer.IPMS_Registration_ID__c = '123';
        insert objBuyer;
        
        Map<ID,Buyer__c> OldMapBuyers = new Map<ID,Buyer__c>();
        OldMapBuyers.put(objBuyer.id,objBuyer);
        
                
        List<Buyer__c> lstNewBuyers  = new List<Buyer__c>();
        lstNewBuyers.add(objBuyer);
        
        BuyerTriggerInsertHelper obj = new BuyerTriggerInsertHelper();
        obj.helperMethod(lstNewBuyers);
        
        objBuyer.Account__c = objAcc.id;
        update objBuyer;
                
        obj.helperMethod(lstNewBuyers);
                             
        
        test.StopTest();
    }
    
    static testMethod void Test2(){
        test.StartTest();
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Buyer__c objBuyer = new Buyer__c();
        objBuyer.First_Name__c = 'Test';
        objBuyer.Last_Name__c  = 'Test';
        objBuyer.Booking__c   = objBooking.id;
        objBuyer.Primary_Buyer__c = true;
        objBuyer.IPMS_Registration_ID__c = '123';
        insert objBuyer;
        
        Map<ID,Buyer__c> OldMapBuyers = new Map<ID,Buyer__c>();
        OldMapBuyers.put(objBuyer.id,objBuyer);
        
                
        List<Buyer__c> lstNewBuyers  = new List<Buyer__c>();
        lstNewBuyers.add(objBuyer);
        
        BuyerTriggerUpdateHelper obj = new BuyerTriggerUpdateHelper();
        obj.helperMethod(OldMapBuyers, lstNewBuyers);
        
        objBuyer.Account__c = objAcc.id;
        update objBuyer;
                
        obj.helperMethod(OldMapBuyers, lstNewBuyers);
                                     
        test.StopTest();
    }
}