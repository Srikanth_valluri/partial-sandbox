/*************************************************************************************************************************
* Name               : InquiryGetEligiblePCS                                                                             *
* Description        : This is a InquiryTriggerHandler helper class. It will assign inquiry owner based on Inquiry assignment rules 
                       for inquiry record type                                  
* Test class Name    : InquiryGetEligiblePCSTest                                                                                                    
* Created Date       : 29/05/2018                                                                                        *
* Created By         : CH V Gopinadh                                                                                     *
* --------------------------------------------------------------------------------------------------------------------   */
public class InquiryGetEligiblePCS {
    
    /*************** This method filters the queue users based on tenure and languaage of the user ************/
    public List<Inquiry__c> getEligiblsPCS (List<Inquiry__c> newInquiryList) {
        List<Inquiry__c> finalList = new List<Inquiry__c>();
        Map <Inquiry__c, List<User>> eligibleUsersMap = new Map <Inquiry__c, List<User>> ();
        PreInquiryGetEligiblePCS obj = new PreInquiryGetEligiblePCS ();
        Map<string,List<Inquiry__c>> inquiryAssignmentMap = obj.queueAssignment(newInquiryList, 'Create');
        System.debug('inquiryAssignmentMapinquiryAssignmentMap'+inquiryAssignmentMap);
        if ( inquiryAssignmentMap.get('assigned') != null ) {
            finalList.addAll(inquiryAssignmentMap.get('assigned'));
        } 
        if ( inquiryAssignmentMap.get('notassigned') != null ) {
            newInquiryList = new List<Inquiry__c>();
            newInquiryList.addAll(inquiryAssignmentMap.get('notassigned'));
            
            if ( newInquiryList.size() > 0 ) {
                Map <Inquiry__c, List<User>> eligibleUsers = obj.getEligiblePCSfromQueue(newInquiryList, 'Create', null);
                System.Debug (eligibleUsers.keySet ());
                finalList = checkForCampaign (eligibleUsers);
            }
            List <Inquiry__c> userInquiries = new List <Inquiry__c> ();
            Map <ID, Set <Inquiry__c>> assignmentRulesMap = new map <ID, Set <Inquiry__c>> ();
            for (Inquiry__c inq : finalList) {
                if (inq.Inquiry_Assignment_Rules__c == NULL) {
                    userInquiries.add (inq);
                }
                else {
                    if (assignmentRulesMap.containsKey (inq.Inquiry_Assignment_Rules__c))
                        assignmentRulesMap.get (inq.Inquiry_Assignment_Rules__c).add (inq);
                    else
                        assignmentRulesMap.put (inq.Inquiry_Assignment_Rules__c, new Set <Inquiry__c> {inq});
                }
            }
            if (assignmentRulesMap.size () > 0) {
                
                Set <Inquiry__c> inquriesSet = new Set <Inquiry__c >();
                Map <ID, Inquiry_Assignment_Rules__c> assignmentRules = new Map <ID, Inquiry_Assignment_Rules__c> (
                    [SELECT Assign_to_Created_by__c FROM Inquiry_Assignment_Rules__c WHERE ID IN: assignmentRulesMap.keySet ()]);
                for (ID key :assignmentRulesMap.keySet ()) {
                    for (Inquiry__c inq: assignmentRulesMap.get (key)) {
                        if (assignmentRules.containsKey (key)) {
                            if (assignmentRules.get (key).Assign_to_Created_by__c) {
                                inq.OwnerID = UserInfo.getUserID ();
                                inquriesSet.add (inq);
                            }
                        }
                    }
                }
                if (inquriesSet.size () > 0) {
                    finalList = new List <Inquiry__c> ();
                    finalList.addAll (inquriesSet);
                }
                
            }
            if (userInquiries.size () > 0) {
                inquiryAssignmentMap = obj.userAssignment(userInquiries, 'Create');
                System.debug('inquiryAssignmentMapinquiryAssignmentMap'+inquiryAssignmentMap);
                if ( inquiryAssignmentMap.get('assigned') != null ) {
                    finalList.addAll(inquiryAssignmentMap.get('assigned'));
                } 
            }
        }
        
        System.debug('finalListfinalList'+finalList); 
        return finalList;
    }
    
    public List <Inquiry__c> checkForCampaign (Map <Inquiry__c, List<User>> eligibleUsersMap) {
        Map <Inquiry__c, List<User>> resultMap = new Map <Inquiry__c, List<User>> ();
        List <Inquiry__c> finalList = new List <Inquiry__c> ();
        List <ID> ruleIds = new List <ID>();
        for (Inquiry__c inq: eligibleUsersMap.keySet ()) {
            if (inq.Inquiry_Assignment_Rules__c != NULL) {
                ruleIds.add (inq.Inquiry_Assignment_Rules__c);
            }
        }
        if (ruleIds.size () > 0) {
            Map <ID, Inquiry_Assignment_Rules__c> rulesMap = new Map <ID, Inquiry_Assignment_Rules__c> (
                            [SELECT Is_Campaign_Active__c
                             FROM Inquiry_Assignment_Rules__c
                                WHERE ID IN: ruleIds]);
            
            List <Inquiry__c> directTeamInquiries = new List <Inquiry__c> ();
            List <Inquiry__c> campaignInquiries = new List <Inquiry__c> ();
            List <ID> assignmentRuleIds = new List <ID> ();
            List <ID> campaignIds = new List <ID> ();
            for (Inquiry__c inq: eligibleUsersMap.keySet ()) {
                assignmentRuleIds.add (inq.Inquiry_Assignment_Rules__c);
                if (inq.Inquiry_Assignment_Rules__c != NULL) {
                    
                    if (rulesMap.get (inq.Inquiry_Assignment_Rules__c).Is_Campaign_Active__c == NULL) {
                        resultMap.put (inq, eligibleUsersMap.get (inq));
                    } 
                    System.Debug (rulesMap.get (inq.Inquiry_Assignment_Rules__c).Is_Campaign_Active__c);
                    System.Debug (inq.Inquiry_Assignment_Rules__c); 
                    if (rulesMap.get (inq.Inquiry_Assignment_Rules__c).Is_Campaign_Active__c == 'No') {
                        directTeamInquiries.add (inq);                        
                    }
                    if (rulesMap.get (inq.Inquiry_Assignment_Rules__c).Is_Campaign_Active__c == 'Yes') {
                        campaignInquiries.add (inq);
                        campaignIds.add (inq.Campaign__C);
                    }
                    
                }
            }  
            if (resultMap.size () > 0)  {
                Map<Inquiry__c,Id> ownerIds = InquiryOwnerAssignment.assignOwner(resultMap);
                System.debug('oooooooooooo'+ownerIds);
                for (Inquiry__c inq : ownerIds.keyset() ) {
                    if ( ownerIds.get(inq) != null ) {
                        inq.OwnerId = ownerIds.get(inq);
                    }
                    finalList.add(inq);
                }
            }
                
            System.Debug ('Direct Team'+directTeamInquiries.size ());
            System.Debug (' NOn Direct Team'+campaignInquiries.size ());
            if (directTeamInquiries.size () > 0) {
                finalList = getInquiryOwnersList (directTeamInquiries);
            } 
            if (campaignInquiries.size () > 0) {
                finalList = getInquiryOwnersList (campaignInquiries);
                
            }                
            
        }
        return finalList;
    }
    
    /************* This method gets directteam pcs from assignment rule ***********/
    public Map<Inquiry__c,List<User>> getDirectTeamPcs(List<Inquiry__c> newInquiryList) {
        string fields = getAllfields('Inquiry_Assignment_Rules__c');
        String campaignActive = 'No';
        Id inquiryRecId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        String queryCondition = 'select '+fields+' from Inquiry_Assignment_Rules__c where Fields_to_Apply_Rule__c != null and Active__c = True and Is_Campaign_Active__c = '+'\''+campaignActive+'\''+' order by Priority__c Asc';
        List<Inquiry_Assignment_Rules__c> assignmentRules = database.query(queryCondition);
        System.Debug (assignmentRules.size ());
        List <ID> ownerIds = new List <ID> ();
        List <ID> assignmentRuleId = new List <ID> ();
        if ( assignmentRules.size() > 0 ) {
            for (Inquiry__c inq : newInquiryList) {
                Integer tempFlag = 0;
                for (Inquiry_Assignment_Rules__c inqAssignment : assignmentRules ) {
                    System.Debug (inqAssignment.ID);
                    System.Debug (inqAssignment.Priority__c);
                    set<String> rulesList = DamacUtility.splitMutliSelect(inqAssignment.Fields_to_Apply_Rule__c);
                    Integer flag = 0;
                    for (string field : rulesList) {
                        field = field.trim();
                        string field1 = field;
                        if ( field == 'RecordTypeId' ) {
                            field1 = 'Inquiry_Record_Type__c';
                            
                            if (inqAssignment.get(field1) == 'Inquiry') {
                                inqAssignment.Inquiry_Record_Type__c = InquiryRecId;
                            }
                        }
                        try {
                            if ( inq.get(field) != null && inqAssignment.get(field1) != null) { 
                                if ( inq.get(field) == inqAssignment.get(field1)){
                                    flag = 1;
                                } else {
                                    flag = 0;
                                    break;
                                }
                            } else {
                                flag = 0;
                                break;
                            }
                        } catch ( Exception e ) {
                            break;
                        }
                    }
                    tempFlag = flag;
                    if (flag == 1) {
                        inq.ownerId = inqAssignment.On_Create_Queue_Id__c;
                        inq.Not_Eligible_for_Reassignment__c = inqAssignment.Exclude_from_Re_assignment__c; 
                        if (inq.OwnerID == NULL) 
                            inq.OwnerId = inqAssignment.OwnerID;   
                        inq.Inquiry_Assignment_Rules__c = inqAssignment.id;
                        ownerIds.add (inqAssignment.ownerID);
                        assignmentRuleId.add(inqAssignment.id);
                        System.Debug ('Rule ID '+inqAssignment.id);
                        break;
                    }
                }
            }
        }
        Map<Inquiry__c,List<User>> eligibleUsersMap = new Map<Inquiry__c,List<User>>();
        Map <Id, Inquiry_Assignment_Rules__c> assignmentRulesMap = new Map <Id, Inquiry_Assignment_Rules__c> (
            [select ownerId,name, Skip_Language_Check__c from Inquiry_Assignment_Rules__c 
             where 
             ID IN :assignmentRuleId                                                                  
            ]);
        if ( assignmentRulesMap.size() > 0 ) {
            
            map <ID, List <ID>> groupMembersMap = new Map <ID, List <ID>> ();            
            List <ID> groupMemberIds = new List <ID> ();
            for (GroupMember mem: [select userorgroupid, groupid from groupmember where groupid IN :ownerIds]) {
                groupMemberIds.add (mem.UserOrGroupId);
                if (!groupMembersMap.containsKey (mem.groupId))
                    groupMembersMap.put (mem.groupId, new List <ID> {mem.userOrGroupId});
                else
                    groupMembersMap.get (mem.groupId).add (mem.UserOrGroupId);
            }
            Map <ID, User> assignersMap = new Map <ID, User> ([select Languages_Known__c,Date_of_Joining__c from user where isActive = True and
                                    Is_Blacklisted__c = False and isUserOnLeave__c = False and id in : groupMemberIds]);
                                    
            for( Inquiry__c inq : newInquiryList ) {
                List <User> assigners = new List <User> ();
                if (inq.Inquiry_Assignment_Rules__c != NULL) {
                    ID ownerID = assignmentRulesMap.get (inq.Inquiry_Assignment_Rules__c).ownerID;
                    if (groupMembersMap.containsKey (ownerID)) {
                        for (Id key: groupMembersMap.get (ownerID)) {
                            if (assignersMap.containskey(key)) {
                                User thisUser = assignersMap.get (key);
                                if (assignmentRulesMap.get (inq.Inquiry_Assignment_Rules__c).Skip_Language_Check__c) {
                                    assigners.add (assignersMap.get (key));
                                } else {
                                    if((String.isNotBlank(thisUser.Languages_Known__c) &&
                                        String.isNotBlank(inq.Preferred_Language__c) &&
                                        DamacUtility.splitMutliSelect(thisUser.Languages_Known__c).contains(inq.Preferred_Language__c)) ||
                                       (String.isNotBlank(thisUser.Languages_Known__c) &&
                                        String.isBlank(inq.Preferred_Language__c) &&
                                        thisUser.Languages_Known__c.contains(DAMAC_Constants.DEFAULT_LANGUAGE))) {
    
                                        assigners.add (assignersMap.get (key));
                                    } 
                                }
                                   
                            }
                        } 
                        
                    }
                }
                eligibleUsersMap.put(inq, assigners);
            }
        }
        
        return eligibleUsersMap;
    }
    /**** This method returns all fields of Inquiry Object **********/
    public static string getAllfields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ())
            fields += f+ ', ';
        return fields.removeEnd(', '); 
        
    }
    /*************** This method filters the queue users based on tenure and languaage of the user ************/
    public List<Inquiry__c> getInquiryOwnersList(List<Inquiry__c> newInquiryList) {
        List<Inquiry__c> finalList = new List<Inquiry__c>();
        set<Id> campaignIds = new Set<Id>();
        Map<Inquiry__c,Id> inqCampaignMap = new Map<Inquiry__c,Id>();
        Map<Inquiry__c,List<User>> eligibleUsersMap = new Map<Inquiry__c,List<User>>();
        for ( Inquiry__c inq : newInquiryList) {
            if ( inq.Campaign__C != null ) {
                campaignIds.add(inq.Campaign__C);
            }
        }
        List<Campaign__c> InActiveCampains = [select Active__c from Campaign__c where id in:campaignIds and Active__c = False];
        if ( InActiveCampains.size() > 0 ) {
            eligibleUsersMap = getDirectTeamPcs(newInquiryList);
            
        }
        List<Campaign__c> ActiveCampaigns = [select Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c,
                                                     Active__c,Language__c from Campaign__c where Active__c = True and id in:campaignIds];

        Map<Id,set<Id>> campParentCamsMap = new Map<Id,set<Id>>();
        Map<Id,Campaign__c> campMap = new Map<Id,Campaign__c>();
        
        if ( ActiveCampaigns.size() > 0 ) {
            CampaignPcs obj = getAssignedPcs (campaignIds, ActiveCampaigns, 1);
            List<Assigned_PC__c> assignedPcs = obj.assignedPcs;
            campParentCamsMap = obj.campParentCamsMap;
            campMap = obj.campMap;
            
            if ( assignedPcs.size() == 0 ) {
                Map<Inquiry__c,List<User>> eligibleDirectTeamUsersMap = getDirectTeamPcs(newInquiryList);
                for (Inquiry__c inq : eligibleDirectTeamUsersMap.keyset()) {
                    eligibleUsersMap.put(inq,eligibleDirectTeamUsersMap.get(inq));
                }
            }
            set<Id> userIds = new set<Id>();
            for (Assigned_PC__c pc : assignedPcs) {
                userIds.add(pc.User__c);
            }
            Map<Id,User> usersMap = new Map<Id,User>([select Languages_Known__c,Date_of_Joining__c from user where id in : userIds and IsActive = True
                                                       and Is_Blacklisted__c = False and isUserOnLeave__c = False]);
            
            Map<Id,List<User>> campUsersMap = new Map<Id,List<User>>();
            for (Assigned_PC__c pc : assignedPcs) {
                Id campId = pc.Campaign__c;
                for ( Campaign__c camp : ActiveCampaigns ) {
                    set<Id> parentCampIds = campParentCamsMap.get(camp.Id);
                    if ( parentCampIds != null ) {
                        if ( parentCampIds.contains(campId) ) {
                            Id userid = pc.User__c;
                            if ( usersMap.get(userId) != null) {
                                User user = usersMap.get(userId);
                                if ( campUsersMap.containskey(camp.id)) {
                                    campUsersMap.get(camp.id).add(user);
                                } else {
                                    campUsersMap.put(camp.id,new List<User>{user});
                                }
                            }
                            
                        }
                    }
                }
            }
            List<Inquiry__c> nonAssignedPcsCampaigns = new List<Inquiry__c>();
            for ( Inquiry__c inq : newInquiryList) {
                if ( inq.Campaign__C != null ) {
                
                    Campaign__c camp = campMap.get(inq.Campaign__C);
                    
                    
                    inq.Assignment_Queue_Id__c = 'temp_'+inq.Campaign__c;
                    if ( campUsersMap.get(inq.Campaign__C) != null ) {
                        for ( User thisUSer : campUsersMap.get(inq.Campaign__C)) {
                            if((String.isNotBlank(thisUser.Languages_Known__c) &&
                                String.isNotBlank(inq.Preferred_Language__c) &&
                                DamacUtility.splitMutliSelect(thisUser.Languages_Known__c).contains(inq.Preferred_Language__c)) ||
                               (String.isNotBlank(thisUser.Languages_Known__c) &&
                                String.isNotBlank(camp.Language__c) &&
                                DamacUtility.splitMutliSelect(thisUser.Languages_Known__c).contains(camp.Language__c)) ||
                               (String.isNotBlank(thisUser.Languages_Known__c) && thisUser.Languages_Known__c.contains(DAMAC_Constants.DEFAULT_LANGUAGE))) {
                                   inq.Inquiry_Assignment_Rules__c = Label.Active_Campaign_Rule_Id;
                                   eligibleUsersMap.put(inq,campUsersMap.get(inq.Campaign__C));
                               }
                        }
                    } else {
                        nonAssignedPcsCampaigns.add(inq);
                    }
                    
                }
            } 
            if ( nonAssignedPcsCampaigns.size() > 0 ) {
                Map<Inquiry__c,List<User>> eligibleDirectTeamUsersMap = getDirectTeamPcs(nonAssignedPcsCampaigns);
                for (Inquiry__c inq : eligibleDirectTeamUsersMap.keyset()) {
                    eligibleUsersMap.put(inq,eligibleDirectTeamUsersMap.get(inq));
                }
               
            }
        }
        System.debug('eligibleUsersMapeligibleUsersMap'+eligibleUsersMap);
        Map<Inquiry__c,Id> ownerIds = InquiryOwnerAssignment.assignOwner(eligibleUsersMap);
        System.debug('oooooooooooo'+ownerIds);
        for (Inquiry__c inq : ownerIds.keyset() ) {
            if ( ownerIds.get(inq) != null ) {
                inq.OwnerId = ownerIds.get(inq);
            }
            finalList.add(inq);
        }
        return finalList;
    }
    
    public CampaignPcs getAssignedPcs (Set <ID> campaignIds, List <Campaign__c> ActiveCampaigns, integer Count) {
        CampaignPcs obj = new CampaignPcs ();
        Map <Id, Campaign__c> campMap = new Map <ID, Campaign__c> ();
        Map<Id,set<Id>> campParentCamsMap = new Map<Id,set<Id>> ();
        set<Id> allParentCampIds = new set<Id>();
        if (count == 1)
            allParentCampIds = campaignIds;
            
        for ( Campaign__c camp : ActiveCampaigns ) {
            Set <ID> parentCampIds = new Set <ID> ();
            parentCampIds.add(camp.id);
            if (count == 2 && camp.Parent_Campaign__c != null ) {
                parentCampIds.add(camp.Parent_Campaign__c);
            }
            if (Count == 3 && camp.Parent_Campaign__r.Parent_Campaign__c != null ) {
                parentCampIds.add(camp.Parent_Campaign__r.Parent_Campaign__c);
            }
            if (count == 4 && camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null ) {
                parentCampIds.add(camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c);
            }
            if (count == 5 && camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null) {
                parentCampIds.add(camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c);
            }
            if (count == 6 && camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null) {
                parentCampIds.add(camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c);
            }
            if (count == 7 && camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null ) {
                parentCampIds.add(camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c);
            }
            campParentCamsMap.put (camp.Id, parentCampIds);
            if (count != 1)
                allParentCampIds.addAll(parentCampIds);
            else
                allParentCampIds.add (camp.Id);
            campMap.put(camp.id,camp);
        }
        List<Assigned_PC__c> assignedPcs = New List<Assigned_PC__c> ();
        assignedPcs = [select User__c,Campaign__c from Assigned_PC__c where Campaign__c in : allParentCampIds];
        
        if (assignedPcs.size () == 0) {
            Count = count + 1;
            if (count == 8) {
                obj.assignedPcs = new List <Assigned_PC__c> ();
                return obj;        
            } else
                getAssignedPcs (campaignIds, ActiveCampaigns, count);
        } 
        obj.assignedPcs = assignedPcs; 
        obj.campParentCamsMap = campParentCamsMap;
        obj.campMap = campMap;
        return obj;    
    }
    
    public class CampaignPcs {
        public Map<Id,set<Id>> campParentCamsMap ;
        public Map<Id,Campaign__c> campMap;
        public List<Assigned_PC__c> assignedPcs;
        public campaignPcs () {
            campParentCamsMap = new Map<Id,set<Id>>();
            campMap = new Map<Id,Campaign__c>();
            assignedPcs = new List<Assigned_PC__c> ();        
        }
        
    }
}