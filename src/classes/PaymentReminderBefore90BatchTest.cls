@isTest
public class  PaymentReminderBefore90BatchTest {
  @isTest static void testProcess() {
    Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        Process_Email_Template__mdt emt = [SELECT Process_Name__c, Nationality__c, Email_Template__c
                        FROM Process_Email_Template__mdt
                        WHERE Nationality__c ='Indian'
                        LIMIT 1];
                System.debug('emt !!==' + emt);

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        List<DP_Invoices__c> dpInvoicesListList = new List<DP_Invoices__c>();

        // Creation of Account
        Account objAccount2 = TestDataFactory_CRM.createPersonAccount();
        objAccount2.Nationality__c = 'Saudi';
        objAccount2.Email__pc = 'gsauf2@hjfd.com';
        objAccount2.email__c = 'gsauf2@hjfd.com';
        objAccount2.Mobile_Phone_Encrypt__pc = '009405883798';
        //objAccount2.recordTypeId = '0120Y000000DjCLQA0';
        insert objAccount2;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount2.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property
        Property__c objProp = TestDataFactory_CRM.createProperty();
        objProp.Name = 'DamacHills';
        insert objProp;

        // Create Inventory
        Inventory__c objInv = TestDataFactory_CRM.createInventory( objProp.Id );
        objInv.Building_Code__c = 'PLOT';
        insert objInv;

        //Configure DDP details
        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(Name = 'VAT Letter'
            , Drawloop_Document_Package_Id__c = '432424'
            , Delivery_Option_Id__c = '43424' );

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
            objBookingUnit.Inventory__c = objInv.Id;
        }

        insert bookingUnitList;
        DP_Invoices__c obj=new DP_Invoices__c();
        obj.Name = 'Test_Name';
        
        obj.FinalDate__c = system.today().addDays(90);
        obj.Milestone_Type_for_Dunning__c = 'Date based';
        
        obj.Current_Level__c = 'Level 2';
        obj.Future_Level__c = 'Level 10';
        obj.sixtyninty__c = true;
        obj.Current_percentage__c = 40;
        obj.Expected_percentage__c = 80;
        obj.Accounts__c = objAccount2.id;
        obj.BookingUnits__c = bookingUnitList[0].id;
        obj.Cover_Letter__c = 'test';
        obj.SOA__c= 'test';
        obj.Trxn_Number__c= 'test';
        obj.TAX_Invoice__c= 'test';
        obj.COCD_Letter__c= 'test';
        obj.Other_Language_TAX_Invoice__c = 'test';
        obj.EmailSent__c = false;
        obj.Milestone__c = 'test';
        obj.Type_of_Milestone__c = 'Date Based';
        insert obj;
        
        

        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
        DataBase.executeBatch(new PaymentReminderBefore90Batch(), 1);
        Test.StopTest();
    }

    

}