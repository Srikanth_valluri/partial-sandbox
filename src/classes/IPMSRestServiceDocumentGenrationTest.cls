@isTest
public with sharing class IPMSRestServiceDocumentGenrationTest {
   @TestSetup
    static void customSettingData() {

        insert new IpmsRestServices__c(
                   SetupOwnerId = UserInfo.getOrganizationId(),
                   BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                   Username__c = 'oracle_user',
                   Password__c = 'crp1user',
                   Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                   BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                   Timeout__c = 120000
                   );
                   
         
    }

    @isTest
    static void genrateLUSAIL_SPA_DocTest() {
    
        Id businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(Name = 'Test Account',
                                    recordtypeid = businessRecTypeId,
                                    email__c = 'tt@tfdst.com',
                                    ZBEmailStatus__c = 'Valid');
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].FM_Outstanding_Amount__c = '1000';
        listBUs[0].Property_Country__c = 'United Arab Emirates';
        listBUs[0].Email__c = '321test@test.com';
        listBUs[0].Party_Id__c='1234';
        listBUs[0].Registration_ID__c = '123123';
        listBUs[0].Registration_Status_Code__c= 'MM';
        //listBUs[0].Registration_Status__c= 'executed';
        listBUs[0].SC_From_RentalIncome__c = false;
        insert listBUs;
    
    
         //Create Customer record
        
 
        String jsonStr =  '{' +
            '"responseId":"20112019-112342355",'+
            '"responseTime":"Wed Nov 20 11:23:42 GMT+04:00 2019",'+
            '"status":"S",'+
            '"responseMessage":"Process Completed",'+
            '"elapsedTimeMs":28098,'+
            '"responseLines":[ '+
                '{ '+
                    '"documentName":"LUSAIL_SPA",'+
                   ' "language":"EN",'+
                   ' "url":"https://ptctest.damacgroup.com/COFFEE/apex/document/view/3587f76616df673c64f36e1d8babc2e7"'+
               ' }'+
           ' ],'+
           ' "complete":true'+
        '}';

        

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));
        String url;

        Test.startTest();
            url = IPMSRestServiceDocumentGenration.genrateLUSAIL_SPA_Doc(listBUs[0]);
        Test.stopTest();
        //System.assertEquals('https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a', responseBody);
    }

    @isTest
    static void genrateLUSAIL_SPA_DocTestError() {
    
        Id businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(Name = 'Test Account',
                                    recordtypeid = businessRecTypeId,
                                    email__c = 'tt@tfdst.com',
                                    ZBEmailStatus__c = 'Valid');
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].FM_Outstanding_Amount__c = '1000';
        listBUs[0].Property_Country__c = 'United Arab Emirates';
        listBUs[0].Email__c = '321test@test.com';
        listBUs[0].Party_Id__c='1234';
        listBUs[0].Registration_ID__c = '123123';
        listBUs[0].Registration_Status_Code__c= 'MM';
        //listBUs[0].Registration_Status__c= 'executed';
        listBUs[0].SC_From_RentalIncome__c = false;
        insert listBUs;

        String jsonStr =   '   {  '  +
         '       "responseId": "12112019-175936435",  '  +
         '       "responseTime": "Tue Nov 12 17:59:36 GMT+04:00 2019",  '  +
         '       "status": "N",  '  +
         '       "responseMessage": "Process Completed"  '  +
         '  }  ' ;

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(302, 'N', jsonStr));
        String url;

        Test.startTest();
            url = IPMSRestServiceDocumentGenration.genrateLUSAIL_SPA_Doc(listBUs[0]);
        Test.stopTest();
        //System.assertEquals('https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a', responseBody);
    }

    

}