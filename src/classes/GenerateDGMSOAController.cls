public with sharing class GenerateDGMSOAController {
    
    public static soaResponse getSOADocument(String regId){
        GenerateSOAService.COCDHttpSoap11Endpoint calloutObj = new GenerateSOAService.COCDHttpSoap11Endpoint();
        calloutObj.timeout_x = 120000;
        soaResponse resObj = new soaResponse();

        try{
            String response = calloutObj.GenCustomerStatement( '2-'+String.valueOf(System.currentTimeMillis()), 'DFM', 'SFDC', regId, '', String.valueOf(System.currentTimeMillis()), regId );
            system.debug('== Statement of Account Response =='+response );
            resObj = (soaResponse)JSON.deserialize(response, GenerateDGMSOAController.soaResponse.class);
            system.debug('DGM soa response === '+ resObj);
        } catch (Exception e ){
             resObj.status = 'Exception';
             resObj.url = '';
        }
        
        return resObj;
    }

    public class soaResponse{
        public String status {get;set;}
        public String url {get;set;}
    }
    
}