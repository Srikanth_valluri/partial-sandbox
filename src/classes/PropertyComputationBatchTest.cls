/****************************************************************************************************
* Name          : PropertyComputationBatch                                                          *
* Description   : Class to create Property Computation records                                      *
* Created Date  : 09-08-2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER     AUTHOR            DATE            COMMENTS                                                *
* 1.0     Nikhil Pote       09-08-2018      Initial Draft.                                          *
****************************************************************************************************/
@isTest
private class PropertyComputationBatchTest{
    @isTest
    static void testGetData() {
        Marketing_Documents__c createMarketingDocObj =new Marketing_Documents__c();
        createMarketingDocObj.Name='test';
        insert createMarketingDocObj;
        
        List<Inventory__c> createInventoryList = new List<Inventory__c>();
        for(Integer i=0; i<10; i++){
            Inventory__c createInventoryObj = new Inventory__c();
            createInventoryObj.status__c='Released';
            createInventoryObj.Property_Status__c='Ready';
            createInventoryObj.Unit_Type__c='Hotel';
            createInventoryObj.property_city__c='Dubai';
            createInventoryObj.Marketing_Name_Doc__c=createMarketingDocObj.id;
            createInventoryObj.property_name__c='DAMAC TOWER';
            createInventoryObj.Bedroom_Type__c='Single';
            createInventoryObj.Floor_Package_Name__c='Test';
            createInventoryList.add(createInventoryObj);
        }
        insert createInventoryList;
        
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Party_ID__C='25425';
        insert objAcc ;
            
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        Booking__c bookObj = new booking__c();
        bookObj.Deal_SR__c = objSR.Id;
        bookObj.Booking_Channel__c = 'Web';
        insert bookObj;
        
        list<Booking_Unit__c> lstBookingUnit = new list<Booking_Unit__c >();
        for(integer i=0; i<10; i++){
            Booking_Unit__c bu = new Booking_Unit__c();
            bu.Booking__c = bookObj.id;
            bu.Payment_Method__c = 'Cash';
            bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
            bu.Primary_Buyer_s_Name__c = 'testNSI';
            bu.Primary_Buyer_s_Nationality__c = 'test';
            bu.Inventory__c = createInventoryList[0].id;
            bu.Registration_Status__c = 'TEST';
            lstBookingUnit.add(bu);
        }
        insert lstBookingUnit;
        
        list<Share_Project_Details__c> lstSPD= new list<Share_Project_Details__c>();
        for(Integer i=0; i<5; i++){
            Share_Project_Details__c sp = new Share_Project_Details__c();
            sp.Inventory__c = createInventoryList[0].id;
            sp.No_of_Opens__c = 7;
            sp.Shared_From__c = 'Sales Offer';
            sp.Number_of_Clicks__c = 10;
            lstSPD.add(sp);
        }
        for(Integer i=5; i<10; i++){
            Share_Project_Details__c sp = new Share_Project_Details__c();
            sp.Inventory__c = createInventoryList[0].id;
            sp.No_of_Opens__c = 7;
            sp.Shared_From__c = 'Project Details';
            sp.Number_of_Clicks__c = 10;
            lstSPD.add(sp);
        }
        insert lstSPD;
        
        Property_Computation__c objPropCom = new Property_Computation__c();
        objPropCom.name = 'Test';
        objPropCom.IsActive__c = True;
        objPropCom.Number_of_Booking_Units__c = 10;
        objPropCom.Number_of_Clicks__c = 5;
        objPropCom.Number_of_Days__c = 10;
        objPropCom.Number_of_Opens__c = 5;
        objPropCom.Number_of_Records_for_Project_Details__c = 10;
        objPropCom.Number_of_Records_for_Sales_Offer__c = 5;
        insert objPropCom;
        
        Test.startTest();
            PropertyComputationBatch obj = new PropertyComputationBatch(objPropCom);
            DataBase.executeBatch(obj); 
            String CRON_EXP = '0 0 0 15 3 ? *';
            String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, new PropertyComputationScheduler());
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(CRON_EXP, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
        list<Property_Ranking__c> lstPropRank = new list<Property_Ranking__c>();
        lstPropRank = [select id from Property_Ranking__c where Inventory__c =: createInventoryList[0].id];
        system.assertNotequals(NULL,lstPropRank.size());
            
    }
}