global class Damac_AmeyoDialer2 {
    
    public Damac_AmeyoDialer2 (Apexpages.standardController stdController) {
    }
    public void dialAmeyo() {
        Id inqId = apexpages.currentpage().getparameters().get('id');
        invokeAmeyo(inqId);
    }
    
    webservice static void invokeAmeyo(id inquiryId){
        Inquiry__c inq = [SELECT 
                            Owner_Queue_Name__c, Campaign_Name_Text__c, Full_Name__c,  Ameyo_Campaign_Id_for_CM_API__c,
                            Owner.Name, Is_Owner_Queue__c, Ameyo_Callback_Id__c, Inquiry_Status__c,Ameyo_Date_Time_Now__c,
                            Ameyo_Customer_Id__c, Call_back_Date_time__c, Inquiry_Source__c, Mobile_CountryCode__c,
                            Short_Code_Mobile_Country__c, Short_Code_Mobile_Country_2__c, Ameyo_Name__c, OwnerId, Ameyo_Dialing_Priority_Attribute__c, LastModifiedDate,
                            Inquiry_Created_Date__c, Class__c, Nationality__c, Inquiry_Number__c, City__c, Country_of_Residence__c,
                            Ameyo_Campaign_Id__c, Encrypted_Mobile__c,Inquiry_18_Digit__c,
                            Encrypted_Mobile_2__c, createdDate 
                            FROM Inquiry__c WHERE ID =: inquiryId LIMIT 1];
        
        Ameyo_Credentials__c credentials = Ameyo_Credentials__c.getInstance(UserInfo.getUserID());
        HTTPResponse cmAPIResponse = callCMAPI (inq, credentials );
        string formattedDate = inq.CreatedDate.format('YYYY-MM-dd');
                         
        String endPoint = '';
        //if(System.label.Ameyo_Switcher == 'true')
        endPoint = credentials.Endpoint__c
                        +'?command='+credentials.Command__c+'&data={%22userId%22:%22'+UserInfo.getUserName()+'%22,%22campaignId%22:%22'
                            +credentials.CampaignId__c+'%22,%22phone%22:%22'+inq.Encrypted_Mobile_2__c+'%22,%22searchable%22:%22'+inq.Id
                            +'%22,%22shouldAddCustomer%22:%22false%22,%22additionalParams%22:'
                            +'{%22phone1%22:%22'+inq.Encrypted_Mobile_2__c+'%22,%22id%22:%22'+inq.Id+'%22,%22inquiry_created_date%22:%22'+formattedDate+'%22}} ';
        
                            
        /*
        else
            endpoint = credentials.Endpoint__c
                        +'?command='+credentials.Command__c+'&data={%22userId%22:%22'+UserInfo.getUserName()+'%22,%22campaignId%22:%22'
                            +credentials.CampaignId__c+'%22,%22phone%22:%22'+inq.Encrypted_Mobile_2__c+'%22,%22searchable%22:%22'+inq.Id
                            +'%22,%22shouldAddCustomer%22:%22false%22,%22additionalParams%22:'
                            +'{%22phone1%22:%22'+inq.Encrypted_Mobile_2__c+'%22,%22id%22:%22'+inq.Id+'%22}} ';
        */
        
        system.debug('endpoint='+endPoint);
        
        HTTPRequest req = new HTTPRequest();
        req.setMethod('POST');
        req.setEndpoint(endpoint);
        
        req.setHeader('hash-key', credentials.hash_key__c);
        req.setHeader('host', credentials.host__c);
        req.setHeader('policy-name', credentials.policy_name__c);
        req.setHeader('requesting-host', credentials.requesting_host__c);
        req.setHeader('content-Type', 'application/json');        
        req.setTimeOut(120000);
        
        
        HTTP http = new HTTP();
        HTTPResponse res = new HTTPResponse();
        if (!Test.isRunningTest())
            res = http.send(req);
        
        System.Debug(res.getStatusCode());
        System.Debug(res.getBody());
        
        Ameyo_Log__c log = new Ameyo_Log__c();
        log.CrtObjectId__c = inq.Id+'-'+DateTime.Now().getTime();
        log.srcPhone__c = inq.Encrypted_Mobile_2__c;
        log.Click_to_Dial_Response__c = res.getBody();
        insert log;
        
        
        String cmAPIResponseBody = '';
        
        if (!Test.isRunningTest ()) {
            cmAPIResponseBody = cmAPIResponse.getBody ();
        } else {
            cmAPIResponseBody = '{"beanResponse":[{"inputCustomerRecord":'
                +'{"name":"Amit4","id":"123","phone1":"7838735378"},"inserted":true,'
                +'"customerId":4148793,"resultTypeString":"UPDATED","crmIntegrated":false,'
                +'"crmSuccess":false},{"inputCustomerRecord":{"name":"Ashu2","id":"123",'
                +'"phone1":"9773925451"},"inserted":false,"exception":"Duplicate key 123",'
                +'"resultTypeString":"DUPLICATE_NOT_ADDED","crmIntegrated":false,"crmSuccess":false}]}';
            cmAPIResponse.setStatusCode (200);
        }
        if (cmAPIResponseBody != '' && cmAPIResponse.getStatusCode () == 200) {
        
            Map <String, String> customerIds = new Map <String, String> ();
            DAMAC_AMEYO_CUSTOMER_RESP_JSON respObj = DAMAC_AMEYO_CUSTOMER_RESP_JSON.parse (cmAPIResponseBody);
            
            for (DAMAC_AMEYO_CUSTOMER_RESP_JSON.cls_beanResponse resp : respObj.beanResponse) {
                if (resp.customerId != NULL) {
                    String customerId = String.valueOf (resp.customerId);
                    String inqId = resp.inputCustomerRecord.id;
                    customerIds.put (inqId, customerId);
                }
            }
            
            if (customerIds.keySet ().size () > 0) {
                List <Inquiry__c> inqToUpdate = new List <Inquiry__c> ();
                for (Inquiry__c inquiry :[SELECT Ameyo_Customer_Id_Phone2__c FROM Inquiry__c WHERE ID IN :customerIds.keySet ()]) {
                    inquiry.Ameyo_Customer_Id_Phone2__c = customerIds.get (inquiry.id);
                    inqToUpdate.add (inquiry);
                    
                }
                if (InqToUpdate.size () > 0) {
                    DAMAC_Constants.skip_InquiryTrigger = true;
                    Update inqToUpdate;
                }
            }
        }
       
                 
    }
    
    public static HTTPResponse callCMAPI (Inquiry__c inq, Ameyo_Credentials__c credentials) {
        // For Customer Manager API 
        Ameyo_JSON obj = new Ameyo_JSON();
        
        obj.sessionId = DAMAC_AMEYO_CUSTOMER.checkSession (); // To get the active session from Ameyo
        obj.numAttempts = '1';
        obj.campaignId = credentials.CampaignId__c;

        Attributes prop = new Attributes();
        prop.migrate_customer = true;        
        obj.properties = prop;
        
        Map <ID, User> ownerDetails = new Map <ID, User>([SELECT FederationIdentifier FROM User WHERE ID =: inq.ownerId]);
        obj.customerRecords = new list<RecordData>();
        RecordData rec = new RecordData ();
        rec.id = inq.Inquiry_18_Digit__c;
        rec.phone1 = inq.Encrypted_Mobile_2__c;
        rec.mobilecountrycode2 = inq.Short_Code_Mobile_Country_2__c;
        rec.attribute30 = ''+inq.Ameyo_Date_Time_Now__c; 
        
        if (inq.Is_Owner_Queue__c && inq.Owner.Name == 'Ameyo Dialer Queue' && inq.Ameyo_Campaign_Id__c != NULL) {
            obj.LeadId = Integer.valueOf(inq.Ameyo_Campaign_Id__c);
        }
        obj.customerRecords.add(rec);
        
        String reqBody = JSON.serialize (obj);
        reqBody = reqBody.replace ('migrate_customer', 'migrate.customer');
        reqBody = reqBody.replace ('"leadId": null,', '');

        System.Debug ('::: Customer Insert/Update Body to Send :::'+reqBody);    
        
        // Http Request to get the Customer id based on the inquiry information
        HTTPRequest req = new HTTPRequest ();
        req.setEndpoint (credentials.Endpoint__c+'?command=uploadContacts');
        req.setHeader ('Content-Type', 'application/x-www-form-urlencoded');
        req.setHeader ('hash-key', credentials.hash_key__c);
        req.setHeader ('Host', credentials.host__c);
        req.setHeader ('policy-name', credentials.policy_name__c);
        req.setHeader ('requesting-host', credentials.requesting_host__c);
        req.setTimeout (120000);
        req.setMethod ('POST');
        req.setBody ('data='+EncodingUtil.urlEncode(reqBody, 'UTF-8'));
                
        HTTP http = new HTTP ();
        HTTPResponse res = new HTTPResponse ();
        if (!Test.isRunningTest ()){
            res = http.send (req);
        }
        return res;
    }

    
    public class Ameyo_JSON{
        public String sessionId;
        public Attributes properties;
        public String numAttempts;
        public integer leadId;	
        public list<RecordData> customerRecords;
        public String campaignId;
    }
    public class Attributes {
        public boolean migrate_customer;
    }
    public class RecordData {
        public String phone1;
        public String mobilecountrycode2;
        public String id;
        public String attribute30;
    }
    
}