public without sharing class ProcessUnprocessedRequestQueable implements Queueable {
	list<SObject> lstSObject;
	public Map<String,String> fieldValuesMap;
	
	public ProcessUnprocessedRequestQueable(list<SObject> lstSObjectParam){
		lstSObject = lstSObjectParam;
	}
	
    public void execute(QueueableContext context) {
        List<sObject> lstSObjectToBeUpdated = new List<sObject>();
     	for( SObject sObj : lstSObject ) {
     		sObj.put( 'Is_Batch_Processing_Pending__c', false );
     		sObj.put( 'LOA_Submitted__c', true );
     		sObj.put( 'Status', 'Submitted' );
     		lstSObjectToBeUpdated.add( sObj );
     	}
     	
     	if( !lstSObjectToBeUpdated.isEmpty() ) {
    		update lstSObjectToBeUpdated ;
    	}      
    }
}