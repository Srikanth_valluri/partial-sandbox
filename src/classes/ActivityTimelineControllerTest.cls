/****************************************************************************************************
* Name          : ActivityTimelineControllerTest                                                    *
* Description   : Test Class for ActivityTimelineController                                         *
* Created Date  : 26/06/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Twinkle P     26/06/2018      Initial Draft.                                          *
****************************************************************************************************/
@isTest
public class ActivityTimelineControllerTest {
    /**
     * Method to insert records for Inquiries
     */
    public static Inquiry__c createInquiries() {
        Inquiry__c inquiryInst = new Inquiry__c(
            First_Name__c = 'Test',
            Last_Name__c = 'Last Test',
            Inquiry_Source__c = 'Call Center',
            Inquiry_Status__c = 'Active',
            Email__c = 'test@gmail.com',
            OwnerId = UserInfo.getuserid()
        );
        insert inquiryInst;
        return inquiryInst;
    }
    
    /**
     * Method to insert records for Task
     */
    public static Task createTask(Id inqId) {
        Task objTask = new Task(
            WhatId = inqId,
            Subject = 'Last Test',
            Description = 'Last Test',
            OwnerId = UserInfo.getuserid()
        );
        insert objTask;
        return objTask;
    }

     /**
     * Method to insert records for Sales Tour
     */
    public static Sales_Tours__c createSalesTour(Id inqId) {
        Sales_Tours__c objSalesTour = new Sales_Tours__c(
            Inquiry__c = inqId,
            OwnerId = UserInfo.getuserid(),
            Tour_Outcome__c = 'No Show - Follow up needed',
            Pickup_Location__c = 'Park Towers',
            Check_In_Date__c = System.today(),
            Check_Out_Date__c = System.today(),
            Comments__c = 'Test'
        );
        insert objSalesTour;
        return objSalesTour;
    }
    /**
     * Method to test the records displayed on Load
     */
	static testMethod void testFetchTaskAndSalesTourOnLoad() {
        Inquiry__c  inqObj = createInquiries();
        Task objTask = createTask(inqObj.Id);
        Sales_Tours__c objSalesTour = createSalesTour(inqObj.Id);
        test.startTest();
        	ApexPages.StandardController sc = new ApexPages.StandardController(inqObj);
        	ActivityTimelineController conInst = new ActivityTimelineController(sc);
        test.stopTest();
		System.assert(!conInst.inquiryTaskList.isEmpty());
        System.assert(!conInst.inquirySalesToursList.isEmpty());
    }
    /**
     * Method to test the Save Task functionality
     */
	static testMethod void testSaveTask() {
        Inquiry__c  inqObj = createInquiries();
        Task objTask = createTask(inqObj.Id);
        Sales_Tours__c objSalesTour = createSalesTour(inqObj.Id);
        test.startTest();
        	ApexPages.StandardController sc = new ApexPages.StandardController(inqObj);
        	ActivityTimelineController conInst = new ActivityTimelineController(sc);
        	conInst.taskCounter = 0;
        	conInst.saveTask();
        test.stopTest();
		System.assert(!conInst.inquiryTaskList.isEmpty());
        System.assert(!conInst.inquirySalesToursList.isEmpty());
    }
   /**
     * Method to test the Save Sales Tour functionality
     */
	static testMethod void testSaveSalesTour() {
        Inquiry__c  inqObj = createInquiries();
        Task objTask = createTask(inqObj.Id);
        Sales_Tours__c objSalesTour = createSalesTour(inqObj.Id);
        test.startTest();
        	ApexPages.StandardController sc = new ApexPages.StandardController(inqObj);
        	ActivityTimelineController conInst = new ActivityTimelineController(sc);
        	PageReference pageRef = Page.ActivityTimeline;
        	pageRef.getParameters().put('id', String.valueOf(inqObj.Id));
        	pageRef.getParameters().put('view', 'global');
        	conInst.tourCounter = 0;
        	Boolean isSF = conInst.isSF1;
        	conInst.saveTour();
        	conInst.createTaskRecord();
        test.stopTest();
        System.assert(!isSF);
		System.assert(!conInst.inquiryTaskList.isEmpty());
        System.assert(!conInst.inquirySalesToursList.isEmpty());
    }
}