/**
 * @File Name          : ScheduleFMCaseEscalationBatchTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/22/2019, 11:57:07 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/22/2019, 11:51:06 AM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
 @istest
public without sharing class ScheduleFMCaseEscalationBatchTest {
    @istest
    public static void testschedule() {
        Test.StartTest();
            ScheduleFMCaseEscalationBatch sh1 = new ScheduleFMCaseEscalationBatch();      
            String sch = '0 0 23 * * ?';
                    system.schedule('Test check', sch, sh1);
        Test.stopTest();
    }
}