public virtual class ScheduleAppointment_CC {

    public transient list<AppointmentWrapper> lstAppointmentWrapper {get;set;}
    public String strSelectedDate { get; set; }
    public List<SelectOption> appointmntObjLst {get;set;}
    public transient List<String> slotLst {get;set;}
    public list<Appointment__c> appointmntObj {get;set;}
    public transient set<String> appointmntSlotSet {get;set;}
    public Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
    public Calling_List__c objCalling;
    public String hoTeamEmail;
    public String strCallingId ;
    public Id buildingId ;

    public ScheduleAppointment_CC() { 
            //init();
    }//End constructor

    protected ScheduleAppointment_CC(Boolean shouldCall) {}

    public virtual pagereference init(){
        map<Id, AppointmentWrapper> mapAppIdAppWrapper = new map<Id, AppointmentWrapper>();
        strSelectedDate = formatDate( strSelectedDate );
        objCalling = [  SELECT Id
                            , Name
                            , Booking_Unit_Name__c
                            , RecordTypeId
                            , Account__c
                            , Appointment_Date__c
                            , Appointment_Status__c
                            , Appointment__c
                            , Customer_Name__c
                            , Booking_Unit__c
                            , Booking_Unit__r.Registration_ID__c
                            , Account__r.Email__pc
                            , Account__r.Email__c
                            , Account__r.isPersonAccount
                            , Account__r.Name
                            , Account__r.Primary_Language__c
                            , OwnerId
                            , Owner.Email
                         FROM Calling_List__c
                        WHERE Id = :strCallingId ];
            System.debug('--objCalling---: '+objCalling);
        if(  objCalling != null && String.isNotBlank( objCalling.Booking_Unit_Name__c ) ) {
            String strProjectName = objCalling.Booking_Unit_Name__c.substringBefore('/');
            System.debug('--strProjectName---: '+strProjectName);
            if( String.isNotBlank( strProjectName ) ) {
                Location__c objLoc = [ SELECT Id
                                            , Name
                                            , Handover_Team_notification_Email__c
                                         FROM Location__c
                                        WHERE Name = :strProjectName ];
                                        system.debug('>>>objLoc'+objLoc);
                System.debug('--objLoc---: '+objLoc);
                if( objLoc != null ) {
                    
                    buildingId = objLoc.Id ;
                    System.debug('--buildingId---: '+buildingId);
                    hoTeamEmail = objLoc.Handover_Team_notification_Email__c;

                    //system.debug('== Appointment List =='+getAppointments() );
                    //system.debug('== Date =='+strSelectedDate );

                    /*for( Appointment__c objApp : getAppointments() ) {
                        if( String.isNotBlank( objApp.Sub_Process_Name__c ) && objApp.Appointment_Date__c != null ) {
                            if ( objApp.Sub_Process_Name__c == 'Unit Viewing') {
                                Date newDate = System.today().addDays( Integer.valueOf( Label.Unit_Viewing_Days ) );
                                if ( objApp.Appointment_Date__c > newDate ) {
                                    mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                                }
                            }
                            else if( objApp.Sub_Process_Name__c == 'Key Handover' ) {
                                Date newDate = System.today().addDays( Integer.valueOf( Label.Key_Handover_Days ) );
                                if ( objApp.Appointment_Date__c > newDate ) {
                                    mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                                }
                            }
                            else {
                               mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                            }
                        }
                    }*/
                }
            }
        }

        /*if( mapAppIdAppWrapper != null && !mapAppIdAppWrapper.isEmpty() ) {
            lstAppointmentWrapper = new list<AppointmentWrapper>();
            for( Calling_List__c objCalling : getAppointmentScheduling( mapAppIdAppWrapper.keySet() ) ) {
                if( mapAppIdAppWrapper.containsKey( objCalling.Appointment__c ) ) {
                    mapAppIdAppWrapper.get( objCalling.Appointment__c ).objCL = objCalling;
                }
            }
            lstAppointmentWrapper.addAll( mapAppIdAppWrapper.values() );
        } */

        return null ;
    }

    // method used to format date to yyyy-MM-dd
    public static String formatDate( String dt ) {
        list<String> lstDateComponents;
        system.debug('== Date1 =='+dt );
        if( dt.contains('/') ) {
            lstDateComponents = dt.split('/');

        }
        else if( dt.contains('-') ) {
            lstDateComponents = dt.split('-');
        }
        system.debug('lstDateComponents'+lstDateComponents);
        Date objDateToFormat = date.newinstance( Integer.valueOf( lstDateComponents[2] ), Integer.valueOf( lstDateComponents[1] ), Integer.valueOf( lstDateComponents[0] ));
        system.debug('== Date2 =='+objDateToFormat );
        system.debug('>>Datetime'+DateTime.newInstance(objDateToFormat.year(),objDateToFormat.month(),objDateToFormat.day()).format('yyyy-MM-dd'));
        return DateTime.newInstance(objDateToFormat.year(),objDateToFormat.month(),objDateToFormat.day()).format('yyyy-MM-dd');

    }

    public virtual PageReference getAvailableAppointments() {
        //if(strSelectedProcess == 'Handover'){
            lstAppointmentWrapper = new list<AppointmentWrapper>();
            strSelectedDate = String.valueOf( formatDate( strSelectedDate ) );
            system.debug('== Date3 =='+strSelectedDate );
            map<Id, AppointmentWrapper> mapAppIdAppWrapper = new map<Id, AppointmentWrapper>();
            if( String.isNotBlank( strSelectedDate ) ) {
                for( Appointment__c objApp : getAppointments() ) {
                    system.debug('== objApp =='+ObjApp );
                    if( String.isNotBlank( objApp.Sub_Process_Name__c ) && objApp.Appointment_Date__c != null ) {
                        system.debug('== if =='+objApp.Appointment_Date__c );
                        if ( objApp.Sub_Process_Name__c == 'Unit Viewing') {
                            system.debug('== if2 =='+objApp.Appointment_Date__c );
                            Date newDate = System.today().addDays( Integer.valueOf( Label.Unit_Viewing_Days ) );
                            system.debug('== newDate =='+newDate );
                            if ( objApp.Appointment_Date__c > newDate ) {
                                mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                            }
                        }
                        else if( objApp.Sub_Process_Name__c == 'Key Handover' ) {
                            Date newDate = System.today().addDays( Integer.valueOf( Label.Key_Handover_Days ) );
                            if ( objApp.Appointment_Date__c > newDate ) {
                                mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                            }
                        }
                        else if( objApp.Sub_Process_Name__c == 'Unit Viewing or Key Handover') {
                            Date newDate = System.today().addDays( Integer.valueOf( Label.Unit_Viewing_Days ) );
                            if ( objApp.Appointment_Date__c > newDate ) {
                                mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                            }
                        }
                        else {
                           mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                        }
                    }
                }
            }
            system.debug( '== mapAppIdAppWrapper ==' +mapAppIdAppWrapper );
            if( mapAppIdAppWrapper != null && !mapAppIdAppWrapper.isEmpty() ) {
                for( Calling_List__c objCalling : getAppointmentScheduling( mapAppIdAppWrapper.keySet() ) ) {
                    if( mapAppIdAppWrapper.containsKey( objCalling.Appointment__c ) ) {
                        mapAppIdAppWrapper.get( objCalling.Appointment__c ).objCL = objCalling;
                    }
                }
                lstAppointmentWrapper.addAll( mapAppIdAppWrapper.values() );
                system.debug( '== lstAppointmentWrapper ==' +lstAppointmentWrapper );
            }
        //}
        return NULL;
    }

    public virtual pageReference createAppointment() {
        Group objGroup, objGroupArab, objGroupnNonArab;
        map<Id, list<GroupMember>> mapOwnerIdGroupMember = new map<Id, list<GroupMember>>();
        map<String, Calling_List__c > mapProcessCL = new map<String, Calling_List__c >();
        List<GroupMember> lstGroupMember = new List<GroupMember>();
        list<Calling_List__c> lstAppointmentCL = new list<Calling_List__c>();
        list<Event> lstEvent = new list<Event>();
        List<Group> lstQueues = [select Id,Name
                                     from Group
                                     where Type = 'Queue'
                                     AND (name= 'Non Elite Arabs Queue'
                                     OR Name = 'Non Elite Non Arabs Queue'
                                     OR Name = 'Handover Queue')
                                     LIMIT 3];

        for(Group grp : lstQueues) {
            if(grp.Name == 'Non Elite Arabs Queue') {
                objGroupArab = grp;
            }
            else if(grp.Name == 'Non Elite Non Arabs Queue') {
                objGroupnNonArab = grp;
            } else if (grp.Name == 'Handover Queue') {
                objGroup = grp;
            }
        }

        for (GroupMember objGM : [Select id,GroupId, Group.name, UserOrGroupId from GroupMember where GroupId IN: lstQueues]) {
            if (mapOwnerIdGroupMember.containsKey(objGM.GroupId)) {
                list<GroupMember> lstGM = mapOwnerIdGroupMember.get(objGM.GroupId);
                lstGM.add(objGM);
                mapOwnerIdGroupMember.put(objGM.GroupId, lstGM);
            } else {
                mapOwnerIdGroupMember.put(objGM.GroupId, new list<GroupMember> {objGM});
            }
        }

        for (Calling_List__c objCL : [Select Id, RecordTypeId, Appointment_Date__c, Appointment_Status__c, Calling_List__c,
                                        Sub_Purpose__c
                                        From Calling_List__c
                                        Where RecordTypeId =: devRecordTypeId and (Appointment_Status__c != 'Rejected' and
                                        Appointment_Status__c != 'Cancelled') and Appointment_Date__c > TODAY and
                                        Calling_List__c =: objCalling.Id]) {
             mapProcessCL.put(objCL.Sub_Purpose__c , objCL );
        }
        for (Appointment__c objWrap : appointmntObj) {
            //if (objWrap.isSelected == true) {
                //system.debug('!!!!!!!!!!inside createAppointment'+objWrap.objCL);
                system.debug('objWrap.Sub_Process_Name__c '+objWrap.Sub_Process_Name__c);
                system.debug('mapProcessCL '+mapProcessCL);
                if (mapProcessCL != null && mapProcessCL.containsKey(objWrap.Sub_Process_Name__c )) {
                    lstAppointmentCL.clear();
                    lstEvent.clear();
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Appointment already available for this Unit, please cancel existing appointment if want to reschedule same.'));
                    break;
                } else {
                    Calling_List__c objCallList = new Calling_List__c();
                    objCallList.Account__c = objCalling.Account__c;
                    objCallList.Booking_Unit__c = objCalling.Booking_Unit__c;
                    objCallList.RecordTypeId = devRecordTypeId;
                    if (objCalling.Id != null) {
                        objCallList.Calling_List__c = objCalling.ID;
                    }
                    if (objWrap.Sub_Process_Name__c  == 'Unit Viewing' || objWrap.Sub_Process_Name__c  == 'Key Handover'
                    || objWrap.Sub_Process_Name__c  == 'Unit Viewing or Key Handover' ) {
                        objCallList.ownerId = objGroup.Id;
                    } else if (objWrap.Sub_Process_Name__c  == 'Documentation' && objCalling.Account__r.Primary_Language__c == 'Arabic') {
                        objCallList.ownerId = objGroupArab.Id;
                    } else if (objWrap.Sub_Process_Name__c == 'Documentation' && objCalling.Account__r.Primary_Language__c != 'Arabic') {
                        objCallList.ownerId = objGroupnNonArab.Id;
                    }
                    objCallList.Service_Type__c = objWrap.Process_Name__c;
                    objCallList.Sub_Purpose__c = objWrap.Sub_Process_Name__c;
                    objCallList.Appointment_Date__c = objWrap.Appointment_Date__c;
                    objCallList.Appointment_Status__c = 'Requested';
                    objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList.Registration_ID__c = objCalling.Booking_Unit__r.Registration_ID__c;
                    String Slot = objWrap.Slots__c.substringBefore('-');
                    String EndTime = objWrap.Slots__c.substringAfter('-');
                    objCallList.Appointment_Slot__c = Slot;
                    if (objCalling.Account__r.isPersonAccount == true) {
                        objCallList.Account_Email__c = objCalling.Account__r.Email__pc;
                    } else {
                        objCallList.Account_Email__c = objCalling.Account__r.Email__c;
                    }
                    objCallList.Appointment__c = objWrap.Id;
                    String ownerId = objCalling.ownerId;
                    if (ownerId.startsWith('005')) {
                        objCallList.HO_Calling_list_Owner__c = objCalling.owner.Email;
                    }
                    lstAppointmentCL.add(objCallList);


                    system.debug('!!!!!!!!!!!00'+Slot.substringAfter(':'));
                    system.debug('!!!!!!!!!!!EndTime'+EndTime.substringBefore(':'));
                     if (mapOwnerIdGroupMember != null && mapOwnerIdGroupMember.containsKey(objCallList.ownerId)) {
                        lstGroupMember = mapOwnerIdGroupMember.get(objCallList.ownerId);
                    }
                    Datetime StartDT = datetime.newInstance(objWrap.Appointment_Date__c.year(), objWrap.Appointment_Date__c.month(),
                        objWrap.Appointment_Date__c.day(), integer.Valueof(Slot.substringBefore(':')),
                        Slot.substringAfter(':') == '00' ? integer.Valueof(Slot.substringAfter(':')) : 00, 00);
                    Datetime EndDT = datetime.newInstance(objWrap.Appointment_Date__c.year(), objWrap.Appointment_Date__c.month(),
                        objWrap.Appointment_Date__c.day(), integer.Valueof((EndTime.substringBefore(':')).deleteWhitespace()),
                        EndTime.substringAfter(':') != '00' ? integer.Valueof(EndTime.substringAfter(':')) : 00, 00);
                    Event objEvent = new Event();
                    objEvent.status__c = 'Requested';
                     if(lstGroupMember.Size()>0){
                         objEvent.OwnerId = lstGroupMember[0].UserOrGroupId ;
                     }
                    objEvent.Type = 'Meeting';
                    objEvent.IsVisibleInSelfService = true;
                    objEvent.StartDateTime = StartDT;
                    objEvent.EndDateTime = EndDT;
                    objEvent.Subject = 'Appointment Scheduled with Customer:'+objCalling.Account__r.Name;
                    objEvent.Description= 'Appointment Scheduled with Customer:'+objCalling.Account__r.Name;
                    lstEvent.add(objEvent);

                }
            //}
        }// end of for

        if (lstAppointmentCL != null && lstAppointmentCL.size() > 0) {
            insert lstAppointmentCL;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Appointment Booked'));
        }

         for(Integer i=0; i<lstAppointmentCL.size(); i++) {
            lstEvent[i].WhatId = lstAppointmentCL[i].Id;
        }
        system.debug('!!!!!!!lstEvent'+lstEvent);

        insert lstEvent;

        init();
        return null;
    }
    public virtual class AppointmentWrapper {
        public Appointment__c objApp {get;set;}
        public Calling_List__c objCL {get;set;}
        public Boolean isSelected{get;set;}

        public AppointmentWrapper() {

        }

        public AppointmentWrapper( Appointment__c tempObjApp, Calling_List__c tempObjCL ) {
            objApp = tempObjApp ;
            objCL = tempObjCL ;
        }
    }

    public virtual list<Appointment__c> getAppointments() {

        system.debug('strSelectedDate '+strSelectedDate);
        Date objDate = Date.valueOf( strSelectedDate );
        system.debug('>>>buildingId>>> '+buildingId);
        system.debug('objDate '+objDate);
        appointmntObjLst = new list<SelectOption>();
        slotLst = new list<String>();
        appointmntSlotSet = new Set<String>();
        list<id> appLst = new list<id>();
        list<Calling_List__c> lstCall = [Select id, Appointment__c from Calling_List__c where Appointment__c != NULL ];
        for(Integer i = 0; i<lstCall.size();i++){
            appLst.add(lstCall[i].Appointment__c);
        }
        system.debug('appLst '+appLst);
        appointmntObj = [ SELECT Id
                       , Name
                       , Appointment_Date__c
                       , Building__c
                       , Process_Name__c
                       , Slots__c
                       , Sub_Process_Name__c
                       , Time_Slot_Sub__c
                    FROM Appointment__c
                   WHERE Building__c = :buildingId
                     AND Appointment_Date__c = :objDate
                     AND id NOT IN :appLst] ;
                     system.debug('>>>>appointmntObj'+ appointmntObj);
         if(!appointmntObj.isEmpty()){
            for(Appointment__c ObjApp: appointmntObj ){
                appointmntSlotSet.add(ObjApp.Slots__c);
            }
        }
        else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'No Appointments available for this Appointment Date.Please try for some other day.'));
        }
        if(!appointmntSlotSet.isEmpty()){
            for(string setElement : appointmntSlotSet){
                appointmntObjLst.add(new selectoption(setElement,setElement));
                slotLst.add(setElement);
            }

        }

           return  [ SELECT Id
                       , Name
                       , Appointment_Date__c
                       , Building__c
                       , Process_Name__c
                       , Slots__c
                       , Sub_Process_Name__c
                       , Time_Slot_Sub__c
                    FROM Appointment__c
                   WHERE Building__c = :buildingId
                     AND Appointment_Date__c = : objDate] ;


    }

    public list<Calling_List__c> getAppointmentScheduling( set<Id> setAppointmentIds ) {
        return [ SELECT Id
                      , Name
                      , Booking_Unit_Name__c
                      , RecordTypeId
                      , Account__c
                      , Appointment_Date__c
                      , Appointment_Status__c
                      , Appointment__c
                      , Customer_Name__c
                      , Booking_Unit__c
                      , Booking_Unit__r.Registration_ID__c
                      , Account__r.Email__pc
                      , Account__r.Email__c
                      , Account__r.isPersonAccount
                      , ownerId
                      , owner.Email
                   FROM Calling_List__c
                  WHERE RecordTypeId = :devRecordTypeId
                    AND Appointment_Status__c NOT IN ( 'Rejected', 'Cancelled' )
                    AND Appointment__c IN :setAppointmentIds ] ;
    }

    /*public void sendEmail(List<Calling_List__c> lstAppointment) {
        List<Messaging.SingleEmailMessage> messages =   new List<Messaging.SingleEmailMessage>();
        EmailTemplate reqEmailTemplate =  [SELECT ID, Subject, Body, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Appointment_Notifications_to_Customer' limit 1];
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
        for (Calling_List__c objCL : [Select Id, Name, Customer_Name__c, Service_Type__c, Appointment_Date__c, Appointment_Slot__c,
                                             Handover_Team_Email__c, HO_Calling_list_Owner__c, CRE_Email__c, Account_Email__c,
                                             Appointment_Status__c, Sub_Purpose__c, Booking_Unit_Name__c
                                      From Calling_List__c  Where Id IN: lstAppointment]) {
            Messaging.SingleEmailMessage mail =
                            new Messaging.SingleEmailMessage();
            mail.setWhatId(objCL.Id);
            mail.setSaveAsActivity(true);
            list<String> bccAddress = new list<String>();
            List<String> sendTo = new List<String>();
            String body, sub;
            if (reqEmailTemplate != null) {
                body = reqEmailTemplate.HtmlValue;
                sub = reqEmailTemplate.Subject;
                if (objCL.Customer_Name__c!= null) {
                    body = body.replace('{!Calling_List__c.Customer_Name__c}', objCL.Customer_Name__c);
                } else {
                    body = body.replace('{!Calling_List__c.Customer_Name__c}', '');
                }
                if (objCL.Sub_Purpose__c!= null) {
                    body = body.replace('{!Calling_List__c.Sub_Purpose__c}', objCL.Sub_Purpose__c);
                } else {
                    body = body.replace('{!Calling_List__c.Sub_Purpose__c}', '');
                }
                if (objCL.Booking_Unit_Name__c!= null) {
                    body = body.replace('{!Calling_List__c.Booking_Unit_Name__c}', objCL.Booking_Unit_Name__c);
                } else {
                    body = body.replace('{!Calling_List__c.Booking_Unit_Name__c}', '');
                }
                if (objCL.Appointment_Status__c != null) {
                    body = body.replace('{!Calling_List__c.Appointment_Status__c}', objCL.Appointment_Status__c);
                } else {
                    body = body.replace('{!Calling_List__c.Appointment_Status__c}', '');
                }
                if (objCL.Service_Type__c != null) {
                    body = body.replace('{!Calling_List__c.Service_Type__c}', objCL.Service_Type__c);
                } else {
                    body = body.replace('{!Calling_List__c.Service_Type__c}', '');
                }
                if (objCL.Appointment_Date__c != null) {
                    body = body.replace('{!Calling_List__c.Appointment_Date__c}', String.valueOf(objCL.Appointment_Date__c));
                } else {
                    body = body.replace('{!Calling_List__c.Appointment_Date__c}', null);
                }
                if (objCL.Appointment_Slot__c != null) {
                    body = body.replace('{!Calling_List__c.Appointment_Slot__c}', objCL.Appointment_Slot__c);
                } else {
                    body = body.replace('{!Calling_List__c.Appointment_Slot__c}', '');
                }

                if (objCL.Name != null) {
                    body = body.replace('{!Calling_List__c.Name}', objCL.Name);
                } else {
                    body = body.replace('{!Calling_List__c.Name}', '');
                }

                body = body.replace(']]>', '');
                if (objCL.Service_Type__c != null) {
                    sub = sub.replace('{!Calling_List__c.Service_Type__c}', objCL.Service_Type__c);
                } else {
                    sub = sub.replace('{!Calling_List__c.Service_Type__c}', '');
                }
                if (objCL.Appointment_Status__c != null) {
                    sub = sub.replace('{!Calling_List__c.Appointment_Status__c}', objCL.Appointment_Status__c);
                } else {
                    sub = sub.replace('{!Calling_List__c.Appointment_Status__c}', '');
                }
                mail.setHtmlBody(body);
                mail.setSubject(sub);
            }
            if ((objCL.Sub_Purpose__c == 'Key Handover' || objCL.Sub_Purpose__c == 'Unit Viewing') &&
                objCL.Handover_Team_Email__c != null) {
                bccAddress.add(objCL.Handover_Team_Email__c);
            }
            if (objCL.HO_Calling_list_Owner__c != null) {
                bccAddress.add(objCL.HO_Calling_list_Owner__c);
            }
            if (objCL.CRE_Email__c != null) {
                bccAddress.add(objCL.CRE_Email__c);
            }
            if (bccAddress != null) {
                mail.setBccAddresses(bccAddress);
            }
            sendTo.add(objCL.Account_Email__c);
            mail.setToAddresses(sendTo);
            messages.add(mail);

            if ( owea.size() > 0 ) {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
            }
        }

        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
    }*/
}