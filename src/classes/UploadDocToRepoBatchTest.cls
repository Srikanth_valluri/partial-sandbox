/*
 * Description: Test class for UploadDocToCentralRepoBatch class
 */
@isTest
private class UploadDocToRepoBatchTest {

    /*
     * Test method to check whether the callout is being done successfully. This method also checks
     * if URL of Attachment is changed after callout.
     */
    @isTest static void testCalloutToIPMS() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id titleDeedRecordTypeID = getRecordTypeIdForTitleDeed();

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , titleDeedRecordTypeID);
        insert objCase;

        // Creation of SR Attachment
        SR_Attachments__c objSRAttachment = new SR_Attachments__c();
        objSRAttachment.Name = 'Test Name';
        objSRAttachment.Attachment_URL__c = 'https://sftest.deeprootsurface.com/docs/t/005888-1529486514912.ID';
        objSRAttachment.Case__c = objCase.Id;
        insert objSRAttachment;

        List<SR_Attachments__c> lstAttachments = [SELECT Id,Attachment_URL__c FROM SR_Attachments__c WHERE Case__c =: objCase.Id];
        Set<Id> setAttachmentIds = new Set<Id>();

        setAttachmentIds.add(lstAttachments[0].Id);

        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockAssignment());
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            UploadDocToCentralRepoBatch batchInstance = new UploadDocToCentralRepoBatch(setAttachmentIds);
            Database.executeBatch(batchInstance, 1);
        Test.stopTest();

        // Check whether the attachment URL is changed or not
        List<SR_Attachments__c> lstSRAttachments = [SELECT Id,Attachment_URL__c FROM SR_Attachments__c WHERE Case__c =: objCase.Id];
        System.assert(lstSRAttachments[0].Attachment_URL__c != null);
        System.assert(lstAttachments[0].Attachment_URL__c != lstSRAttachments[0].Attachment_URL__c);
    }

    /**
     * Method to get the "Title Deed" record type
     */
    private static Id getRecordTypeIdForTitleDeed() {
      Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id titleDeedRecordTypeID = caseRecordTypes.get('Title Deed').getRecordTypeId();
      return titleDeedRecordTypeID;
    }

}