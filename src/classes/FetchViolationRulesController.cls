public with sharing class FetchViolationRulesController {
	public FetchViolationRulesController(ApexPages.StandardController sc) { }

	public PageReference fetchViolationRules(){
		String buildingId = Apexpages.currentpage().getparameters().get('id');
		if(String.isNotBlank(buildingId)) {
			Set<String> rules = new Set<String>();
	        Location__c objBuilding = [SELECT Id,
	                                            (SELECT Violation_Of_Rule__c
	                                            FROM Violation_Rules__r)
	                                FROM Location__c
	                            WHERE Id=:buildingId
	                            AND RecordType.DeveloperName = 'Building' LIMIT 1];
	        System.debug('===objBuilding=='+objBuilding);
	        if(objBuilding != NULL) {
	            //To avoid duplicate rule creation
	            if(objBuilding.Violation_Rules__r.size() > 0) {
	                for(Violation_Rule__c obj : objBuilding.Violation_Rules__r) {
	                    rules.add(obj.Violation_Of_Rule__c);
	                }
	            }
	            //Get defined violation rules from metadata
	            List<Violation_Notice__c> lsCS = new List<Violation_Notice__c>();
	            List<Violation_Rule__c> lstRules = new List<Violation_Rule__c>();
	            Map<String,Violation_Notice__c> allCSRules = Violation_Notice__c.getAll();
	            List<Violation_Notice__c> lstCSRules = new List<Violation_Notice__c>();
	            lstCSRules = allCSRules.values();
	            System.debug('===lstCSRules=='+lstCSRules);
	            for(Violation_Notice__c obj : lstCSRules) {
	                if(obj.Active__c && !rules.contains(obj.Violation_Of_Rule__c)) {
	                    lstRules.add(new Violation_Rule__c(
	                        Category__c = obj.Category__c,
	                        Payble_Fine__c = obj.Payble_Fine__c,
	                        Remedial_Period__c = obj.Remedial_Period__c,
	                        Violation_Of_Rule__c = obj.Violation_Of_Rule__c,
	                        Building__c = objBuilding.Id
	                    ));
	                }
	            }//for
	            insert lstRules;
				return new PageReference('/'+buildingId);
	        }
			return NULL;
		}//if
		return NULL;
    }//method
}//Class