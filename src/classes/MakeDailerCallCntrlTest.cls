@IsTest
public with sharing class MakeDailerCallCntrlTest {
    private class Mock implements HttpCalloutMock  {
        public HTTPResponse respond(HTTPRequest req) {
           
            HTTPResponse res = new HTTPResponse();
            res.setBody('{}');
            res.setStatusCode(200);
            return res;
            
        }
    }
    @IsTest
    static void Test1() {

            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            objAcc.Mobile_Encrypt__c='940588';
            objAcc.Mobile_pc_CRM_Encrypt__c = '940588';
            insert objAcc ;
            
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
            
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR ;
            
            
            
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objDealSR.Id, 1 );
            insert lstBooking ;
            
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
            insert lstBookingUnit ;
            
            List<Calling_List__c> lstCalling = TestDataFactory_CRM.createCallingList( 'Appointment_Scheduling', 1 , lstBookingUnit[0] );
            lstCalling[0].Account__c  = objAcc.Id;
             lstCalling[0].Mobile_Number__c  = 940588;

        insert lstCalling ;
            
        //add contact
		Contact contactObj = new Contact();
        contactObj.LastName = 'test';
        insert contactObj;
        
        Test.setMock(HttpCalloutMock.class, new Mock());
        PageReference myVfPage = Page.MakeDailerCall;
        Test.setCurrentPage(myVfPage);

        // Put Id into the current page Parameters
        
        ApexPages.currentPage().getParameters().put('CLId',lstCalling[0].Id);
        ApexPages.currentPage().getParameters().put('objId', lstCalling[0].Id);
        ApexPages.currentPage().getParameters().put('fieldToquery', 'Mobile_Number__c');
        ApexPages.currentPage().getParameters().put('ContactId', contactObj.Id);
        MakeDailerCallCntrl cntrlObj = new MakeDailerCallCntrl();
        Test.startTest();
        MakeDailerCallCntrl.doPost(); 
        cntrlObj.backToDetailPage(); 
        Test.stopTest();
    }
}