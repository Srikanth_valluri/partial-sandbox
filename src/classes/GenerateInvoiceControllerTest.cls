/****************************************************************************************
 * Class : GenerateInvoiceControllerTest 
 * Created By : ES
 -----------------------------------------------------------------------------------
 * Description : Test class for GenerateInvoiceController
 -----------------------------------------------------------------------------------
 * Version History:
 * Version    Developer Name    Date          Detail Features
   1.0        Nikhil Pote       30/08/2018    Initial Development
 **********************************************************************************/
@isTest
private class GenerateInvoiceControllerTest {

	/****************************************************************************************
 	* Method : To check Invoice is getting generated correctly
	****************************************************************************************/
    static testMethod void myUnitTest() {
      	NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
      	SR.Delivery_mode__c='Email';
      	SR.Deal_ID__c='1001';
		SR.Token_Amount_AED__c = 20000;
      	insert SR;
      
      	update SR;
      	List<NSIBPM__Service_Request__c > listSR = new List<NSIBPM__Service_Request__c >();
      	listSR =[select Name
                 from NSIBPM__Service_Request__c 
                 where ID=: SR.ID];                
                 
       	System.debug('...SR...'+listSR[0].Name);

       	ApexPages.currentPage().getParameters().put('id',listSR[0].Id);
		
        PageReference pageRef = Page.GenerateInvoice; 
        pageRef.getParameters().put('srid', String.valueOf(SR.Id));
		pageRef.getParameters().put('rmrk', String.valueOf('test'));
		pageRef.getParameters().put('amt', String.valueOf('test'));
		pageRef.getParameters().put('add', String.valueOf('test'));
        Test.setCurrentPage(pageRef);  

		ApexPages.StandardController sc = new ApexPages.StandardController(SR);
        GenerateInvoiceController objController = new GenerateInvoiceController(sc);        
        //objController.dealSRId = SR.Id;
		objController.saveReceiptDetails();

		Attachment insertedAttachment = new Attachment();
		insertedAttachment = [select id,ParentId from Attachment limit 1];
		system.assertequals(insertedAttachment.ParentId,SR.Id);
        
    }
}