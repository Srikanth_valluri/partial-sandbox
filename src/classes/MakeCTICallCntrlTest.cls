@IsTest
public with sharing class MakeCTICallCntrlTest {
    private class Mock implements HttpCalloutMock  {
        public HTTPResponse respond(HTTPRequest req) {
           
            HTTPResponse res = new HTTPResponse();
            res.setBody('{}');
            res.setStatusCode(200);
            return res;
            
        }
    }
    @IsTest
    static void Test1() {

            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            objAcc.Mobile_Encrypt__c='940588';
            insert objAcc ;
            User u = new User(
            ProfileId = [SELECT Id FROM Profile LIMIT 1].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            
            );


        Test.setMock(HttpCalloutMock.class, new Mock());
        PageReference myVfPage = Page.MakeCTICall;
        Test.setCurrentPage(myVfPage);

        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('Destination','Mobile_Encrypt__c');
        ApexPages.currentPage().getParameters().put('ContactId',objAcc.Id);
        MakeCTICallCntrl cntrlObj = new MakeCTICallCntrl();
        Test.startTest();
        MakeCTICallCntrl.doPost(); 
        cntrlObj.backToDetailPage(); 
        Test.stopTest();
    }
}