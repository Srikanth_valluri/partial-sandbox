@isTest
private class HDApp_ComplaintSR_APITest {

	@isTest
	static void getComplaintsLookupDataTest_HandedOverUnit() {
		NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'Y',
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;

        Test.startTest();

        	RestRequest req = new RestRequest();
	        RestResponse res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        req.addParameter('accountId',account.id);
	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;
	        HDApp_ComplaintSR_API.getComplaintsLookupData();

        Test.stopTest();
	}

	@isTest
	static void getComplaintsLookupDataTest_NonHandedOverUnit() {

		NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'N',
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;

        Test.startTest();

        	RestRequest req = new RestRequest();
	        RestResponse res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        req.addParameter('accountId',account.id);
	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;
	        HDApp_ComplaintSR_API.getComplaintsLookupData();

        Test.stopTest();
	}

	@isTest
	static void getComplaintsLookupDataTest_NoAccParam() {

		Test.startTest();

        	RestRequest req = new RestRequest();
	        RestResponse res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        //req.addParameter('accountId',account.id);
	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;
	        HDApp_ComplaintSR_API.getComplaintsLookupData();

        Test.stopTest();
	}

	@isTest
	static void getComplaintsLookupDataTest_NoAccParamVal() {

		Test.startTest();

        	RestRequest req = new RestRequest();
	        RestResponse res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        req.addParameter('accountId','');
	        req.httpMethod = 'GET';
	        RestContext.request = req;
	        RestContext.response = res;
	        HDApp_ComplaintSR_API.getComplaintsLookupData();

        Test.stopTest();
	}

	/*******************************************************************/
	/*******************************************************************/

	@isTest
	static void submitComplaintSRCaseTest_FMCaseCreation() {

		NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'Y',
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;

        Test.startTest();

        	RestRequest req = new RestRequest();
	        RestResponse res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        req.addParameter('accountId',account.id);
	        req.addParameter('bookingUnitId',bookingUnit.id);
	        req.addParameter('typeId','2');
	        req.addParameter('typeName','Common Area');
	        req.addParameter('subType','Pest Control');
	        req.addParameter('description','Testing');

	        req.httpMethod = 'POST';
	        RestContext.request = req;
	        RestContext.response = res;

	        HDApp_ComplaintSR_API.submitComplaintSRCase();

        Test.stopTest();

	}

	@isTest
	static void submitComplaintSRCaseTest_CaseCreation() {

		NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'N',
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;

        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;

        String fileBody = 'Test String';

        Test.startTest();

        	RestRequest req = new RestRequest();
	        RestResponse res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        req.addParameter('accountId',account.id);
	        req.addParameter('bookingUnitId',bookingUnit.id);
	        req.addParameter('typeId','1');
	        req.addParameter('typeName','Assignment');
	        req.addParameter('subType','Appointment');
	        req.addParameter('description','Testing');
	        req.addParameter('documentName', 'Attachment 1');
	        req.addParameter('fileName', 'test.jpg');
	        req.requestBody = Blob.valueOf(fileBody);

	        req.httpMethod = 'POST';
	        RestContext.request = req;
	        RestContext.response = res;

	        UploadMultipleDocController.strLabelValue ='N';
            Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());

	        HDApp_ComplaintSR_API.submitComplaintSRCase();

        Test.stopTest();

	}

	@isTest
	static void submitComplaintSRCaseTest_NoParams() {

		NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'N',
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;

		Test.startTest();

			String fileBody = 'Test String';

        	RestRequest req = new RestRequest();
	        RestResponse res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        //req.addParameter('accountId','');
	        req.addParameter('bookingUnitId','1234');
	        req.addParameter('typeId','1');
	        req.addParameter('typeName','Assignment');
	        req.addParameter('subType','Appointment');
	        req.addParameter('description','Testing');
	        req.addParameter('documentName', 'Attachment 1');
	        req.addParameter('fileName', 'test.jpg');
	        req.requestBody = Blob.valueOf(fileBody);

	        req.httpMethod = 'POST';
	        RestContext.request = req;
	        RestContext.response = res;

	        HDApp_ComplaintSR_API.submitComplaintSRCase();
	        /*********************************************************/
	        req = new RestRequest();
	        res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        req.addParameter('accountId','1234');
	        //req.addParameter('bookingUnitId','1234');
	        req.addParameter('typeId','1');
	        req.addParameter('typeName','Assignment');
	        req.addParameter('subType','Appointment');
	        req.addParameter('description','Testing');
	        req.addParameter('documentName', 'Attachment 1');
	        req.addParameter('fileName', 'test.jpg');
	        req.requestBody = Blob.valueOf(fileBody);

	        req.httpMethod = 'POST';
	        RestContext.request = req;
	        RestContext.response = res;

	        HDApp_ComplaintSR_API.submitComplaintSRCase();

	        /*********************************************************/
	        req = new RestRequest();
	        res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        req.addParameter('accountId','1234');
	        req.addParameter('bookingUnitId','1234');
	        //req.addParameter('typeId','1');
	        req.addParameter('typeName','Assignment');
	        req.addParameter('subType','Appointment');
	        req.addParameter('description','Testing');
	        req.addParameter('documentName', 'Attachment 1');
	        req.addParameter('fileName', 'test.jpg');
	        req.requestBody = Blob.valueOf(fileBody);

	        req.httpMethod = 'POST';
	        RestContext.request = req;
	        RestContext.response = res;

	        HDApp_ComplaintSR_API.submitComplaintSRCase();

	        /*********************************************************/
	        req = new RestRequest();
	        res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        req.addParameter('accountId','1234');
	        req.addParameter('bookingUnitId','1234');
	        req.addParameter('typeId','1');
	        //req.addParameter('typeName','Assignment');
	        req.addParameter('subType','Appointment');
	        req.addParameter('description','Testing');
	        req.addParameter('documentName', 'Attachment 1');
	        req.addParameter('fileName', 'test.jpg');
	        req.requestBody = Blob.valueOf(fileBody);

	        req.httpMethod = 'POST';
	        RestContext.request = req;
	        RestContext.response = res;

	        HDApp_ComplaintSR_API.submitComplaintSRCase();
	        /*********************************************************/
	        req = new RestRequest();
	        res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        req.addParameter('accountId',account.id);
	        req.addParameter('bookingUnitId',bookingUnit.id);
	        req.addParameter('typeId','1');
	        req.addParameter('typeName','Assignment');
	        req.addParameter('subType','Appointment');
	        req.addParameter('description','Testing');
	        //req.addParameter('documentName', 'Attachment 1');
	        req.addParameter('fileName', 'test.jpg');
	        req.requestBody = Blob.valueOf(fileBody);

	        req.httpMethod = 'POST';
	        RestContext.request = req;
	        RestContext.response = res;

	        HDApp_ComplaintSR_API.submitComplaintSRCase();

	        /*********************************************************/
	        req = new RestRequest();
	        res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	       	req.addParameter('accountId',account.id);
	        req.addParameter('bookingUnitId',bookingUnit.id);
	        req.addParameter('typeId','1');
	        req.addParameter('typeName','Assignment');
	        req.addParameter('subType','Appointment');
	        req.addParameter('description','Testing');
	        req.addParameter('documentName', 'Attachment 1');
	        //req.addParameter('fileName', 'test.jpg');
	        req.requestBody = Blob.valueOf(fileBody);

	        req.httpMethod = 'POST';
	        RestContext.request = req;
	        RestContext.response = res;

	        HDApp_ComplaintSR_API.submitComplaintSRCase();

        Test.stopTest();
	}

	@isTest
	static void submitComplaintSRCaseTest_NoParamValues() {
		Test.startTest();

			NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
	        insert sr;

	        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
	        Account account = new Account( LastName = 'Test Account',
	                                       Party_ID__c = '63062',
	                                       RecordtypeId = rtId,
	                                       Email__pc = 'test@mailinator.com');
	        insert account;

	        Booking__c booking = new Booking__c(
	                             Deal_SR__c = sr.Id,
	                             Account__c = account.Id);
	        insert booking;



	        Booking_Unit__c bookingUnit = new Booking_Unit__c(
	                                      Booking__c = booking.Id,
	                                      Unit_Name__c = 'BD4/12/1204',
	                                      Registration_ID__c = '3901',
	                                      Handover_Flag__c = 'N',
	                                      Dummy_Booking_Unit__c = true);
	        insert bookingUnit;

	        String fileBody = 'Test String';

        	RestRequest req = new RestRequest();
	        RestResponse res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        req.addParameter('accountId','');
	        req.addParameter('bookingUnitId','1234');
	        req.addParameter('typeId','1');
	        req.addParameter('typeName','Assignment');
	        req.addParameter('subType','Appointment');
	        req.addParameter('description','Testing');
	        req.addParameter('documentName', 'Attachment 1');
	        req.addParameter('fileName', 'test.jpg');
	        req.requestBody = Blob.valueOf(fileBody);

	        req.httpMethod = 'POST';
	        RestContext.request = req;
	        RestContext.response = res;

	        HDApp_ComplaintSR_API.submitComplaintSRCase();
	        /*********************************************************/
	        req = new RestRequest();
	        res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        req.addParameter('accountId','1234');
	        req.addParameter('bookingUnitId','');
	        req.addParameter('typeId','1');
	        req.addParameter('typeName','Assignment');
	        req.addParameter('subType','Appointment');
	        req.addParameter('description','Testing');
	        req.addParameter('documentName', 'Attachment 1');
	        req.addParameter('fileName', 'test.jpg');
	        req.requestBody = Blob.valueOf(fileBody);

	        req.httpMethod = 'POST';
	        RestContext.request = req;
	        RestContext.response = res;

	        HDApp_ComplaintSR_API.submitComplaintSRCase();

	        /*********************************************************/
	        req = new RestRequest();
	        res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        req.addParameter('accountId','1234');
	        req.addParameter('bookingUnitId','1234');
	        req.addParameter('typeId','');
	        req.addParameter('typeName','Assignment');
	        req.addParameter('subType','Appointment');
	        req.addParameter('description','Testing');
	        req.addParameter('documentName', 'Attachment 1');
	        req.addParameter('fileName', 'test.jpg');
	        req.requestBody = Blob.valueOf(fileBody);

	        req.httpMethod = 'POST';
	        RestContext.request = req;
	        RestContext.response = res;

	        HDApp_ComplaintSR_API.submitComplaintSRCase();

	        /*********************************************************/
	        req = new RestRequest();
	        res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        req.addParameter('accountId','1234');
	        req.addParameter('bookingUnitId','1234');
	        req.addParameter('typeId','1');
	        req.addParameter('typeName','');
	        req.addParameter('subType','Appointment');
	        req.addParameter('description','Testing');
	        req.addParameter('documentName', 'Attachment 1');
	        req.addParameter('fileName', 'test.jpg');
	        req.requestBody = Blob.valueOf(fileBody);

	        req.httpMethod = 'POST';
	        RestContext.request = req;
	        RestContext.response = res;

	        HDApp_ComplaintSR_API.submitComplaintSRCase();

	        /*********************************************************/
	        req = new RestRequest();
	        res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        req.addParameter('accountId',account.Id);
	        req.addParameter('bookingUnitId',bookingUnit.id);
	        req.addParameter('typeId','1');
	        req.addParameter('typeName','Assignment');
	        req.addParameter('subType','Appointment');
	        req.addParameter('description','Testing');
	        req.addParameter('documentName', '');
	        req.addParameter('fileName', 'test.jpg');
	        req.requestBody = Blob.valueOf(fileBody);

	        req.httpMethod = 'POST';
	        RestContext.request = req;
	        RestContext.response = res;

	        HDApp_ComplaintSR_API.submitComplaintSRCase();


			/*********************************************************/
	        req = new RestRequest();
	        res = new RestResponse();
	        req.requestURI = '/complaintSR';  
	        req.addParameter('accountId',account.id);
	        req.addParameter('bookingUnitId',bookingUnit.id);
	        req.addParameter('typeId','1');
	        req.addParameter('typeName','Assignment');
	        req.addParameter('subType','Appointment');
	        req.addParameter('description','Testing');
	        req.addParameter('documentName', 'Attachment 1');
	        req.addParameter('fileName', '');
	        req.requestBody = Blob.valueOf(fileBody);

	        req.httpMethod = 'POST';
	        RestContext.request = req;
	        RestContext.response = res;

	        HDApp_ComplaintSR_API.submitComplaintSRCase();

        Test.stopTest();	
	}


}