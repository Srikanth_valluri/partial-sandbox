/****************************************************************************************************************************
* Name               : CreateStepOnDPOverDue                                                                                *
* Description        : This class is used to create steps.                                                                  *
*                      - Process Builder                                                                                    * 
*                      - Custom Buttons on SR                                                                               *
* Created Date       : 05/02/2017                                                                                           *
* Created By         : NSI                                                                                                  *
* --------------------------------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                                        *
* 1.0                       05/02/2017      Initial Draft.                                                                  *
* 1.1         Vineet        12/07/2017      Changed the filter condition on createStepFromButton method to only check for   *
*                                           the rejection status on service request to be Not Applicable,                   * 
*                                           Auto Rejection Cancelled, Rejection Cancelled.                                  *
****************************************************************************************************************************/
global class CreateStepOnDPOverDue{
    
    /*********************************************************************************************
    * @Description : To Create Deal Rejection Step on SR from Process Builder.                   *
    * @Params      : List<Id>                                                                    *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    //OLD Create Step
    /*@InvocableMethod
    public static void createStep(List<Id> srIds){
        try{    
            list<NSIBPM__Step__c> checkDealRejection = new list<NSIBPM__Step__c>();
            checkDealRejection = [select id,NSIBPM__SR__c from NSIBPM__Step__c where NSIBPM__Step_Template__r.NSIBPM__Code__c='DEAL_AUTO_REJECTION' and NSIBPM__SR__c IN:srIds];
            map<id,boolean> mapSrDealRejStep = new map<id,boolean>();
            if(checkDealRejection.size() > 0){
                // Prepare map of Sr Id and its Deal rejection setp            
                for(NSIBPM__Step__c step:checkDealRejection){
                    mapSrDealRejStep.put(step.NSIBPM__SR__c,true); //There is already a Deal Rejection Step under SR           
                }
            }
            
            
            NSIBPM__SR_Template__c template = new NSIBPM__SR_Template__c();
            template = [SELECT Id, 
                                (SELECT Id, NSIBPM__Start_Status__c, NSIBPM__Step_Template__c, NSIBPM__SR_Steps__c.OwnerId, 
                                        NSIBPM__Step_No__c, NSIBPM__SR_Template__r.Name 
                                 FROM  NSIBPM__SR_Steps_SR_Template__r 
                                 WHERE NSIBPM__Step_Template_Code__c = 'DEAL_AUTO_REJECTION' AND 
                                       NSIBPM__Start_Status__c != null AND 
                                       NSIBPM__Step_Template__c != null AND OwnerId != null AND 
                                       NSIBPM__Step_No__c != null LIMIT 1 ) 
                        FROM NSIBPM__SR_Template__c 
                        WHERE Name='Deal' limit 1];
            system.debug(template+'>>>');
            system.debug(template.NSIBPM__SR_Steps_SR_Template__r +'>>>');
            
            if(template.NSIBPM__SR_Steps_SR_Template__r  != null){        
                list<NSIBPM__Step__c> newSteps = new list<NSIBPM__Step__c>();
                for(id srId:srIds){
                    if(!mapSrDealRejStep.containskey(srId)){
                        NSIBPM__Step__c newStep = new NSIBPM__Step__c();
                        newStep.NSIBPM__SR__c = srId;
                        newStep.NSIBPM__SR_Step__c = template.NSIBPM__SR_Steps_SR_Template__r[0].id;
                        newStep.NSIBPM__Status__c =  template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Start_Status__c;
                        newStep.NSIBPM__Step_Template__c = template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_Template__c;
                        //newStep.RecordTypeId = 
                        newStep.OwnerId = template.NSIBPM__SR_Steps_SR_Template__r[0].ownerid;
                        newStep.NSIBPM__Step_No__c  = template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_No__c;
                        newStep.NSIBPM__Sys_Step_Loop_No__c= template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_No__c+'_1';
                        newSteps.add(newStep);
                    }
                }
                insert newSteps;
            }
        }catch(exception ex){
            system.debug('Exception at line number = '+ex.getLineNumber()+', Exception message = '+ex.getMessage());
        }
    }*/
    
    //New Create Step
    @InvocableMethod
    public static void createStep(List<Id> srIds){
        try{    
            list<New_Step__c> checkDealRejection = new list<New_Step__c>();
            checkDealRejection = [select id,Service_Request__c from New_Step__c where Step_Type__c ='DEAL_AUTO_REJECTION' and Service_Request__c IN:srIds];
            map<id,boolean> mapSrDealRejStep = new map<id,boolean>();
            if(checkDealRejection.size() > 0){
                // Prepare map of Sr Id and its Deal rejection setp            
                for(New_Step__c step:checkDealRejection){
                    mapSrDealRejStep.put(step.Service_Request__c,true); //There is already a Deal Rejection Step under SR           
                }
            }
            List<Group> groupList = new List<Group>([SELECT ID FROM Group WHERE Type = 'Queue' AND DeveloperName = 'Sales_Admin_Queue']);
            
            /*NSIBPM__SR_Template__c template = new NSIBPM__SR_Template__c();
            template = [SELECT Id, 
                                (SELECT Id, NSIBPM__Start_Status__c, NSIBPM__Step_Template__c, NSIBPM__SR_Steps__c.OwnerId, 
                                        NSIBPM__Step_No__c, NSIBPM__SR_Template__r.Name 
                                 FROM  NSIBPM__SR_Steps_SR_Template__r 
                                 WHERE NSIBPM__Step_Template_Code__c = 'DEAL_AUTO_REJECTION' AND 
                                       NSIBPM__Start_Status__c != null AND 
                                       NSIBPM__Step_Template__c != null AND OwnerId != null AND 
                                       NSIBPM__Step_No__c != null LIMIT 1 ) 
                        FROM NSIBPM__SR_Template__c 
                        WHERE Name='Deal' limit 1];
            system.debug(template+'>>>');
            system.debug(template.NSIBPM__SR_Steps_SR_Template__r +'>>>');
            
            if(template.NSIBPM__SR_Steps_SR_Template__r  != null){   */     
                list<New_Step__c> newSteps = new list<New_Step__c>();
                for(id srId:srIds){
                    if(!mapSrDealRejStep.containskey(srId)){
                        New_Step__c newStep = new New_Step__c();
                        newStep.Service_Request__c = srId;
                        newStep.Step_Type__c = 'DEAL_AUTO_REJECTION';
                        newStep.Step_Status__c =  'Auto Rejection Initiated';
                        if (groupList.size() > 0) {
                            newStep.OwnerId = groupList[0].Id;
                        }
                        newStep.Step_No__c  = 61.0;
                        //newStep.NSIBPM__Sys_Step_Loop_No__c= template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_No__c+'_1';
                        newSteps.add(newStep);
                    }
                }
                insert newSteps;
            //}
        }catch(exception ex){
            system.debug('Exception at line number = '+ex.getLineNumber()+', Exception message = '+ex.getMessage());
        }
    }
    
    /*********************************************************************************************
    * @Description : To Create Deal Rejection Step on SR from Detail Page Button.                *
    * @Params      : Id                                                                          *
    * @Return      : String                                                                      *
    *********************************************************************************************/
    webservice static string createStepFromButton(Id srId){
        List<New_Step__c> checkDealRejection = new List<New_Step__c>();
        try{
            checkDealRejection = [SELECT Id 
                                  FROM New_Step__c 
                                  WHERE Service_Request__c =: srId AND 
                                        Service_Request__r.Rejection_Status__c != 'Not Applicable' AND 
                                        Service_Request__r.Rejection_Status__c != 'Auto Rejection Cancelled' AND 
                                        Service_Request__r.Rejection_Status__c != 'Rejection Cancelled'];
            if(checkDealRejection.size() > 0){
                return 'This deal is not applicable for rejection.';   
            }else{
                List<Group> groupList = new List<Group>([SELECT ID FROM Group WHERE Type = 'Queue' AND DeveloperName = 'Sales_Admin_Queue']);
                
                New_Step__c newStep = new New_Step__c();
                newStep.Service_Request__c = srId;
                newStep.Step_No__c = 60;
                newStep.Step_Status__c = 'Awaiting Rejection';
                newStep.Step_Type__c = 'Deal Rejection';
                if (groupList.size() > 0) {
                    newStep.OwnerId = groupList[0].Id;
                }
                insert newStep;
                return 'success';
                
            }
        }catch(exception ex){
            return 'error';
        }
    }
    
    /*********************************************************************************************
    * @Description : To Create Deal Reinstatement Step on SR from Detail Page Button.            *
    * @Params      : Id                                                                          *
    * @Return      : String                                                                      *
    *********************************************************************************************/
    /*webservice static string reinstatementDeal(Id srId){
        List<NSIBPM__Step__c> checkDealRejection = new List<NSIBPM__Step__c>();
        try{  
            checkDealRejection = [SELECT Id, NSIBPM__SR__c 
                                  FROM NSIBPM__Step__c 
                                  WHERE NSIBPM__Step_Template__r.NSIBPM__Code__c = 'UPLOAD_POP' AND 
                                        NSIBPM__SR__c =:srId ];
            if(checkDealRejection.size() > 0){
                return 'Deal reinstatement step already exist and it is open.';   
            }else{
        
                NSIBPM__SR_Template__c template = new NSIBPM__SR_Template__c();
                template = [select id,(select id,NSIBPM__Start_Status__c,NSIBPM__Step_Template__c,NSIBPM__SR_Steps__c.OwnerId,NSIBPM__Step_No__c,NSIBPM__SR_Template__r.Name from  NSIBPM__SR_Steps_SR_Template__r where NSIBPM__Step_Template_Code__c='UPLOAD_POP' and
                            NSIBPM__Start_Status__c!=null and 
                            NSIBPM__Step_Template__c!=null and 
                            ownerid != null and 
                            NSIBPM__Step_No__c!=null LIMIT 1 ) from NSIBPM__SR_Template__c where Name='Deal' limit 1];
                        
                if(template.NSIBPM__SR_Steps_SR_Template__r  != null){
                              
                        NSIBPM__Step__c newStep = new NSIBPM__Step__c();
                        newStep.NSIBPM__SR__c = srId;
                        newStep.NSIBPM__SR_Step__c = template.NSIBPM__SR_Steps_SR_Template__r[0].id;
                        newStep.NSIBPM__Status__c =  template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Start_Status__c;
                        newStep.NSIBPM__Step_Template__c = template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_Template__c;
                        newStep.OwnerId = template.NSIBPM__SR_Steps_SR_Template__r[0].ownerid;
                        newStep.NSIBPM__Step_No__c  = template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_No__c;
                        newStep.NSIBPM__Sys_Step_Loop_No__c= template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_No__c+'_1';               
                        insert newStep;
                        
                        //update the status on SR to submitted SUBMITTED REINSTATEMENT_SUBMITTED
                        List<NSIBPM__SR_Status__c> lstSRStatus = [select id,name from NSIBPM__SR_Status__c where NSIBPM__Code__c ='REINSTATEMENT_SUBMITTED' limit 1];
                        if(lstSRStatus != null && !lstSRStatus.isempty()){
                            NSIBPM__Service_Request__c srStatustoSubmit = new NSIBPM__Service_Request__c(id=srId);
                            srStatustoSubmit.NSIBPM__External_SR_Status__c = lstSRStatus[0].id;
                            srStatustoSubmit.NSIBPM__Internal_SR_Status__c = lstSRStatus[0].id;
                            update srStatustoSubmit;
                        }
                        return 'success';
                }else {
                    return 'No sufficient data on service request.';
                }
            }
        }catch(exception e){
            return 'error';
        }
    }*/
    
     /*********************************************************************************************
    * @Description : To Create Deal Reinstatement NEW Step on SR from Detail Page Button configured for the New Step.        *
    * @Params      : Id                                                                          *
    * @Return      : String  
    * @By          : Mariam Cloudzlab                                                          *
    *********************************************************************************************/
    webservice static string reinstatementDeal(Id srId){
        List<New_Step__c> checkDealRejection = new List<New_Step__c>();
        try{  
            checkDealRejection = [SELECT Id, Service_Request__c 
                                  FROM New_Step__c 
                                  WHERE Step_Type__c = 'Upload PoP' AND 
                                  Service_Request__c =:srId
                                  AND Is_Closed__c = False];
            if(checkDealRejection.size() > 0){
                return 'Deal reinstatement step already exist and it is open.';   
            }else{
                List<Group> queueList = new List<Group>([SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperName = 'Property_Consultant_Queue']);
                
                if(queueList.size() > 0){
                    New_Step__c newStep = new New_Step__c();
                    newStep.Service_Request__c = srId;
                    newStep.Step_Type__c = 'Upload PoP';
                    newStep.Step_Status__c = 'Awaiting PoP';
                    newStep.OwnerId = queueList[0].Id;
                    newStep.Step_No__c = 70;
                    insert newStep;
                    
                    //update the status on SR to submitted SUBMITTED REINSTATEMENT_SUBMITTED
                    NSIBPM__Service_Request__c srStatustoSubmit = new NSIBPM__Service_Request__c(id = srId);
                    srStatustoSubmit.External_Status__c = 'REINSTATEMENT_SUBMITTED';
                    srStatustoSubmit.Internal_Status__c = 'REINSTATEMENT_SUBMITTED';
                    srStatustoSubmit.nsibpm__finalizeamendmentflg__c = true;
                    update srStatustoSubmit;
                    return 'success';
                }else {
                    return 'No sufficient data on service request.';
                }
            }
        }catch(exception ex){
            system.debug('Exception line number: ' + ex.getLineNumber());
            system.debug('Exception mesaage: ' + ex.getMessage());
            return 'error';
        }
    }
    /*********************************************************************************************
    * @Description : To check various conditions before creating deal reinstatement step on,     * 
    *                service request from detail page button.                                    *
    * @Params      : String                                                                      *
    * @Return      : Boolean                                                                     *
    *********************************************************************************************/
    public static boolean preChecksForReinstatement(string srId){ 
        string sridval = srId.substring(0, 15);           
        //Get all Booking Units for the SR and all units with same area.
        list<Booking_Unit__c> bookingUnits = new list<Booking_Unit__c>();
        bookingUnits = [select id from Booking_Unit__c where SR_Id__c=:sridval LIMIT 50000];
        system.debug(bookingUnits); 
        
        if(bookingUnits.size() >0){    
            List<AggregateResult> bookingUnitsWithSameArea = new List<AggregateResult>();
            integer bookingUnitsWithSameAreaCount;            
            bookingUnitsWithSameArea = [select count(id) countval from Booking_Unit__c where SR_Id__c=:sridval and Is_Same_Area__c=:true and Release_Check__c='N'];
            system.debug(bookingUnitsWithSameArea[0].get('countval'));  
            if(bookingUnitsWithSameArea[0].get('countval') != null) {             
                bookingUnitsWithSameAreaCount = integer.valueof(bookingUnitsWithSameArea[0].get('countval'));
            }  
            system.debug(bookingUnits.size()+'--'+bookingUnitsWithSameAreaCount);              
            if(bookingUnits.size() == bookingUnitsWithSameAreaCount){       
                return true;
            } else {
                return false;
            }              
        }else{
            return false;
        }      
    }
}// End of class.