public virtual class InventoryDesignChangeUtility {
    
    public static boolean approved = false;
    public static void afterhandler(list<Design_Change_Request__c> lstNew, map<id, Design_Change_Request__c> mpOld){
        set<Id> recordIds = new set<Id>();
        for(Design_Change_Request__c dcr : lstNew){
            if(dcr.Status__c == 'Approved' && dcr.Status__c != mpOld.get(dcr.Id).status__c){
                recordIds.add(dcr.Id);
            }
        }

        if(!recordIds.isEmpty()  && !InventoryDesignChangeUtility.approved){
            InventoryDesignChangeUtility.approved = true;
            approvedRecord(recordIds);
        }
    }

    //readFieldSet
    public static list<string> readFieldSet(String fieldSetName, String ObjectName){
        
        list<string> FieldApiNames = new list<string>();
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
       
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        for(Schema.FieldSetMember f : fieldSetObj.getFields()) {
            FieldApiNames.add(f.getFieldPath());
        }
        return FieldApiNames;

    }

    public static String blobToString(Blob input, String inCharset){
        
        String hex = EncodingUtil.convertToHex(input);
        System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() >> 1;
        String[] bytes = new String[bytesCount];
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i << 1, 2);
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);

    }

    public class LineData{
        public string floor {get; set;}
        public string unit_no {get; set;}
        public string bedroom {get; set;}
        public string merge_unit_no {get; set;}
        public string suite_area_sqm {get; set;}
        public string suite_area_sqft {get; set;}
        public string balcony_area_sqm {get; set;}
        public string balcony_area_sqft {get; set;} 
        public string terrace_area_sqm {get; set;}
        public string terrace_area_sqft {get; set;}
        public string common_area_sqm {get; set;}
        public string common_area_sqft {get; set;}
        public string sellable_area_sqm {get; set;}
        public string sellable_area_sqft {get; set;}
        public string project{get; set;}
        public string building{get; set;}
        public string projectId{get; set;}
        public string buildingId{get; set;}
        public string floorId{get; set;}
        public string unit_Code{get; set;}
        public string inventoryId{get; set;}
        public string status{get; set;}
        public string floor_code{get; set;}
        public string merge_unit{get; set;}
        public string basic_price{get; set;}
        public string view_type{get; set;}
        public string space_type{get; set;}
        public string action{get; set;} 
        
        //villa
        public String plotNo { get; set;}
        public String protoType { get; set; }
        public String position { get; set; }
        public String free_parking { get; set; }
        public String plot_area_sqm { get; set; }
        public String plot_area_sqft { get; set; }
        public String max_gfa_sqm { get; set; }
        public String max_gfa_sqft { get; set; }
        public String net_sellablearea_sqm{ get; set; }
        public String net_sellablearea_sqft { get; set; }
        
    }

    public static void approvedRecord(set<Id> requestIds){

        list<Design_Change_Request__c> icdRecords = [Select id, name, Property__c, Property__r.name, Building__c,
                                        Building__r.name, status__c, Building__r.Seller_Name__c,
                                        Property__r.Property_City__c, Property__r.Property_Country__c,
                                        Property__r.Currency_Of_Sale__c, Property__r.UOM__c,
                                        Property__r.Seller_name__c, Property__r.District__c, Property__r.ProjectNum_Connect__c    
                                        from Design_Change_Request__c 
                                        where id in: requestIds]; 

        Design_Change_Request__c icd = icdRecords[0];
        attachment att = [Select id, name, body from Attachment where parentId =: icd.Id];
        string DataAsString = blobToString(att.body,'ISO-8859-1');
        list<string> Lines = DataAsString.split('\n');
        system.debug('fileLines Size: ' + Lines.size());
        list<linedata> csvLines = new list<LineData>();

        set<string> unitKeys = new set<string>();
        for(integer i = 1; i< lines.size(); i++){
            list<string> vals = lines[i].split(',');
            unitKeys.add(icd.Property__r.name+'/'+icd.Building__r.name+'/'+vals[0].replaceAll('Floor','').trim());
        }

        map<string, Inventory__c> mpExisting = new map<string, Inventory__c>();
        for(Inventory__c inv : [Select id, Unit_Key__c from Inventory__c where Unit_Key__c in: unitKeys]){
            mpExisting.put(inv.Unit_Key__c, inv);
        }

        for(integer i = 1; i< lines.size(); i++){
            list<string> vals = lines[i].split(',');
            LineData d = new lineData();
            string key = icd.Property__r.name+'/'+icd.Building__r.name+'/'+vals[0].replaceAll('Floor','').trim();
            d.action = mpExisting.containsKey(key) ? 'Update' : 'Create';
            d.floor = vals[0];
            d.unit_no = vals[1];
            d.bedroom = vals[3];
            d.merge_unit_no = vals[2];
            d.suite_area_sqm = vals[4];
            d.suite_area_sqft = vals[5];
            d.balcony_area_sqm = vals[6];
            d.balcony_area_sqft = vals[7];
            d.terrace_area_sqm = vals[8];
            d.terrace_area_sqft = vals[9];
            d.common_area_sqm = vals[10];
            d.common_area_sqft = vals[11];
            d.sellable_area_sqm = vals[12];
            d.sellable_area_sqft = vals[13];
            d.project = icd.Property__r.name;
            d.building = icd.Building__r.name;
            d.projectId = icd.Property__c;
            d.buildingId = icd.Building__c;
            d.inventoryId = mpExisting.containsKey(key) ? mpExisting.get(key).id : null;
            csvLines.add(d);
        }

        map<string, Bedroom_Mapping__c> bedMappings = Bedroom_Mapping__c.getAll();
        for(lineData line : csvLines){
            if(line.action.toLowerCase() == 'create'){
                Location__c floor = new Location__c(
                    Name = icd.Building__r.name+'/'+line.floor.tolowerCase().replaceAll('floor','').trim(),
                    RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Floor').getRecordTypeId(),
                    Building_Number__c = icd.Building__c,
                    Floor_Name__c = line.floor.tolowerCase().replaceAll('floor','').trim(),
                    Location_Alias__c = line.floor.tolowerCase().replaceAll('floor','').trim(),
                    Location_Code_IFC_Id__c = icd.Building__r.name+line.floor.tolowerCase().replaceAll('floor','').trim(),
                    Location_Code__c =  icd.Building__r.name+'/'+line.floor.tolowerCase().replaceAll('floor','').trim(),
                    Location_Type__c = 'Floor',
                    Maximum_Capacity__c = '0.000000000000000',
                    Optimum_Capacity__c = '0.000000000000000',
                    Rentable_Are__c = '0.000000000000000',
                    UOM_Code__c = icd.Property__r.UOM__c,
                    Usable_Area__c = '0.000000000000000'
                );
                
                database.upsert(floor, Location__c.Location_Code_IFC_Id__c);

                Location__c unit = new Location__c(
                    Name = floor.Location_Code__c+'/'+line.unit_no,
                    RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Unit').getRecordTypeId(),
                    Building_Name__c = icd.Building__r.Name,
                    Floor_Name__c = floor.Location_Code__c,
                    Floor_Number__c = floor.Id,
                    Location_Code_IFC_Id__c = floor.Location_Code_IFC_Id__c+line.unit_no,
                    Location_Code__c = floor.Location_Code__c+'/'+line.unit_no,
                    Location_Type__c = 'Unit',
                    Master_developer__c = icd.Property__r.Seller_Name__c,
                    UOM_Code__c = icd.Property__r.UOM__c,
                    Unit_Area__c = line.sellable_area_sqft,
                    Unit_Description__c = bedMappings.containsKey(line.bedroom)? bedMappings.get(line.bedroom).Description__c : '',
                    Unit_Name__c = line.Unit_no,
                    Unit_Type__c = 'Residential'
                );

                insert unit;

                Inventory__c inventory = new Inventory__c();
                inventory.RecordTypeId = Schema.SObjectType.Inventory__c.getRecordTypeInfosByName().get('Unit').getRecordTypeId();
                inventory.Unit_Categorization__c = 'Villa';//Bedroom Types

                inventory.Area_Sqft__c = line.suite_area_sqft;
                inventory.Balcony_Area__c = line.balcony_area_sqft;
                inventory.Unit_Area__c = (inventory.Unit_Categorization__c == 'Villa' ? '0' : line.sellable_area_sqft);//Sellable Area
                inventory.Current_AC_Area__c = 0.0;
                inventory.Net_area__c = (inventory.Unit_Categorization__c == 'Villa' ? '0' : line.sellable_area_sqft);//Sellable Area
                inventory.Plot_Area__c = (inventory.Unit_Categorization__c == 'Villa' ? line.sellable_area_sqft : '0');//Villa
                inventory.Terrace_Area__c = line.terrace_area_sqft;

                inventory.View_Type__c = line.view_Type;
                inventory.Space_Type_Lookup_Code__c = line.space_Type;
                
                inventory.Bedroom_Type_Code__c = line.bedroom;
                inventory.Bedroom_Type__c = line.bedroom;
                inventory.Building_Location__c = icd.Building__c;
                inventory.Building_Name__c = icd.Building__r.name;
                inventory.Description_Code__c = bedMappings.containsKey(line.bedroom)? bedMappings.get(line.bedroom).Description__c : '';
                inventory.Description_Name__c = bedMappings.containsKey(line.bedroom)? bedMappings.get(line.bedroom).Description__c : '';
                inventory.Floor_Code_IFC__c = floor.Location_Code_IFC_Id__c;
                inventory.Floor_Location__c = floor.Id;
                inventory.Floor__c = floor.name;
                inventory.IPMS_Bedrooms__c = bedMappings.containsKey(line.bedroom)? string.valueOf(bedMappings.get(line.bedroom).No_of_Bedrooms__c) : '';
                //inventory.Level_2_Classification_of_Units__c = 'Residential Apartments';
                inventory.Location_Code_IFC__c = unit.Location_Code_IFC_Id__c;
                inventory.OQOOD_Fee_C__c = 1000.0;
                inventory.Plot_Number__c = unit.Unit_Name__c;
                //Project_Category__c = 'VILLA';//in Sheet
                inventory.Property_City__c = icd.Property__r.Property_City__c;
                inventory.Property_Country__c = icd.Property__r.Property_Country__c;
                inventory.Property_Name__c = icd.Property__r.name;
                //Property_Status__c = 'Ready';
                //Property_Type__c = 'Residential';
                inventory.Property__c = icd.Property__c;
                inventory.Status__c = 'Inventory';
                inventory.UOM_Code__c = icd.Property__r.UOM__c;
                inventory.Unit_Location__c = unit.Id;
                //Unit_Type__c = 'Residential';
                inventory.Unit__c = unit.name;
                inventory.Property_Location__c = 'DP';//Should be added in sheet
                inventory.Building_Code__c = icd.Building__r.name;
                inventory.Currency_of_Sale__c = icd.Property__r.Currency_Of_Sale__c;
                inventory.District__c = icd.Property__r.District__c;
                inventory.Floor_Number__c = floor.Floor_Name__c;
                inventory.Property_Code__c = icd.Property__r.name;
                inventory.Seller_Name__c = icd.Building__r.Seller_Name__c;                    
                
                inventory.Floor_No__c = decimal.valueOf(floor.Floor_Name__c);                    
                inventory.FP_Label__c = 'Floor Plan';
                inventory.PP_Label__c = 'Plot Plan';
                inventory.UP_Label__c = 'Unit Plan';
                inventory.Conn_Project_Code__c = icd.Property__r.ProjectNum_Connect__c;
                inventory.Is_Mixed_Use__c = 'N';

                insert inventory;
            }
            else{
                Inventory__c inventory = new Inventory__c(id = line.inventoryId);
                inventory.Area_Sqft__c = line.suite_area_sqft;
                inventory.Balcony_Area__c = line.balcony_area_sqft;
                inventory.Unit_Area__c = (inventory.Unit_Categorization__c == 'Villa' ? '0' : line.sellable_area_sqft);//Sellable Area
                inventory.Current_AC_Area__c = 0.0;
                inventory.Net_area__c = (inventory.Unit_Categorization__c == 'Villa' ? '0' : line.sellable_area_sqft);//Sellable Area
                inventory.Plot_Area__c = (inventory.Unit_Categorization__c == 'Villa' ? line.sellable_area_sqft : '0');//Villa
                inventory.Terrace_Area__c = line.terrace_area_sqft;
                inventory.Space_Type_Lookup_Code__c = line.space_Type;
                inventory.View_Type__c = line.view_Type;
                update inventory;
            }
        }    
    }
    
}