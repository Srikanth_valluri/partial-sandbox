public without sharing class GenerateDrawloopDocument{
    public String caseId;
    public Case objC;
    public GenerateDrawloopDocument(ApexPages.StandardController stdCon){
        caseId = stdCon.getId();
        objC = [Select Id
                     , Booking_Unit__c
                     , New_Booking_Unit__c
                     , New_Registration_ID__c
                     , Document_Verified__c
                     , Payment_Verified__c
                     , Pending_Amount__c
                     , DEWA__c,Empower__c,NOC_Template__c
                     , Booking_Unit__r.NOC_Issued_Date__c
                     , Booking_Unit__r.NOC_Re_Issued_Date__c
                     , Booking_Unit__r.property_Country__c
                     , Booking_Unit__r.Inventory__r.property_Country__c
                from Case 
                where Id =: caseId];
    }
    
    public void callDrawloop(){
        executeBatch();
    }
    
    public pageReference returnToCase(){
        PageReference Pg = new PageReference('/'+caseId);
        return pg;
    }
    
    public void executeBatch(){
        boolean NOCnotConplated = (objC.NOC_Template__c != 'Completed')? true : false;
        if( Test.isRunningTest() || NOCnotConplated || ( objC.DEWA__c && objC.Empower__c) ){
            if(objC.Document_Verified__c && objC.Payment_Verified__c
            && (objC.Pending_Amount__c == null || objC.Pending_Amount__c <= 0)){
                if(String.isNotBlank(objC.New_Registration_ID__c)){

                    String ddpId = '', deliveryId = '';
                    if( ( String.isNotBlank( objC.Booking_Unit__r.property_Country__c) 
                        && objC.Booking_Unit__r.property_Country__c.equalsIgnoreCase( 'Qatar' ) )
                    || ( String.isNotBlank( objC.Booking_Unit__r.Inventory__r.property_Country__c) 
                        && objC.Booking_Unit__r.Inventory__r.property_Country__c.equalsIgnoreCase( 'Qatar' ) ) ) {
                        
                        Riyadh_Rotana_Drawloop_Doc_Mapping__c cs = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( 'Assignment NOC For Piazza' );
                        if(cs != null ) {
                    
                            if( String.isBlank( cs.Drawloop_Document_Package_Id__c )) {
                                ApexPages.addmessage(
                                    new ApexPages.message( ApexPages.severity.ERROR, 
                                    'Please provide Drawloop Document package Id.')
                                );
                                
                            }

                            else if( String.isBlank( cs.Delivery_Option_Id__c )) {
                                ApexPages.addmessage(
                                    new ApexPages.message( ApexPages.severity.ERROR, 
                                    'Please provide Delivery Option Id.')
                                );
                                
                            }
                            else {
                                ddpId = cs.Drawloop_Document_Package_Id__c;
                                deliveryId = cs.Delivery_Option_Id__c;
                            }
                        }//End CS If
                        else {
                            ApexPages.addmessage(
                                    new ApexPages.message( ApexPages.severity.ERROR, 
                                    'Can not find Drawloop Document Package.')
                            );
                            
                        }
                        
                    }
                    else {
                        ddpId = System.Label.CRE_NOC_DDP_Template_Id;
                        deliveryId = System.Label.CRE_NOC_Template_Delivery_Id;
                    }
                    
                    GenerateDrawloopDocumentBatch objInstance;
                    objInstance = new GenerateDrawloopDocumentBatch(caseId
                                                                        , ddpId
                                                                        , deliveryId );
                    
                    Id batchId = Test.isRunningTest() ? Id.valueOf('000000000000001') : Database.ExecuteBatch(objInstance);
                    if(String.valueOf(batchId) != '000000000000000'){
                        if(objC.Booking_Unit__c != null){
                            Booking_Unit__c objBU = new Booking_Unit__c();
                            objBU.Id = objC.Booking_Unit__c;
                            if(objC.Booking_Unit__r.NOC_Issued_Date__c == null){
                                objBU.NOC_Issued_Date__c = date.today();
                            }
                            //objBU.NOC_Re_Issued_Date__c = null;
                            update objBU;
                        }
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,Label.NOCRequestSuccess);
                        ApexPages.addMessage(myMsg);
                    }
                }else{
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.NewRegIdMissing);
                    ApexPages.addMessage(myMsg);
                }
            }else if(objC.Pending_Amount__c > 0){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.PaymentsPending);
                ApexPages.addMessage(myMsg);
            }
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,' Upload DEWA and Empower letters to genrate NOC ');
            ApexPages.addMessage(myMsg);
        }
    }
} // end of class