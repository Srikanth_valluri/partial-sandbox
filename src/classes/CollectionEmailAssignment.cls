global class CollectionEmailAssignment implements Database.Batchable<sObject>{//AKISHOR

    String query;
    //public static Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
    public static Id EmailRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
    public static Id CollEmailRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Collection Email').getRecordTypeId();
    
    public Id collectionQueueId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName( 'Collection_Queue','Queue' );
    public static Id CollectionCLId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
 
    global Database.QueryLocator start(Database.BatchableContext BC) {
         Set<Id> recTypeIdSet=new Set<Id>();
        for(RecordType rec :[SELECT DeveloperName,Id,Name 
                               FROM RecordType 
                               WHERE SobjectType = 'Case' 
                                 //And Name='Email' ]){
                               And (Name='Collection Email' 
                               OR Name='Email'
                               ) ]){
            recTypeIdSet.add(rec.Id);
        }
        
    //ID id = '0010Y00000Q36Pq';    
           
query= 'SELECT Id,Party_Id__c,RecordTypeId,OwnerId from Case Where RecordTypeId IN :recTypeIdSet AND OwnerId =: collectionQueueId AND Status = \'New\'';        
//AND Id=\'50025000007LHbx\'
        system.debug( ' query : ' + query );        
        return Database.getQueryLocator(query);
    }
    global void execute( Database.BatchableContext BC, List<Case> caseLst) {
        system.debug( ' caseLst : ' + caseLst.size());
        List<Account> lstAcc = new List<Account>();
        Set<String> setPartyId = new Set<String>();
        Set<String> setAcntid = new set<String>();
        Map<String,List<Calling_List__c>> mapPartyIdAcc  = new Map<String,List<Calling_List__c>>();     
        
        system.debug( ' caseLst : ' + caseLst );
        for( Case objCase : caseLst ) {
            system.debug( ' objCase : ' + objCase );
            if( objCase.Party_Id__c != '') {
                setPartyId.add( objCase.Party_ID__c );
                //setAcntid.add(String.valueOf(accObj.id).substring(0, 15));
               // mapPartyIdAcc.put(accObj.Party_ID__c ,accObj);//partyid,account map  
                           
            }
        }
        system.debug('acntidset'+setAcntid+'partyidset='+setPartyId);
        if(setPartyId!=null && setPartyId.size()>0)
        {//query booking unit based party id and then update active flag on cust
            for( Calling_List__c objBU :[select id, party_id__c,OwnerId from Calling_List__c 
																	   where Party_Id__c IN: setPartyId 
                                                                         AND IsHideFromUI__c = false 
																		 AND OwnerId !=:collectionQueueId 
																		 AND RecordTypeId=: CollectionCLId]) {
                                //setPartyId.add( accObj.Party_ID__c );
              //mapPartyIdAcc.put(objBU.party_id__c,objBU);//partyid,BU map 
                system.debug('partyaccmap' + mapPartyIdAcc);            
            if( String.isNotBlank( objBU.party_id__c ) && string.valueOf(objBU.OwnerId).startsWith('005') ) {
					if(mapPartyIdAcc.containsKey(objBU.party_id__c) && mapPartyIdAcc.get(objBU.party_id__c) != null ) {
									List<Calling_List__c> lstCL = mapPartyIdAcc.get(objBU.party_id__c);
									lstCL.add(objBU);
									mapPartyIdAcc.put(objBU.party_id__c,lstCL );
								} else {
									mapPartyIdAcc.put(objBU.party_id__c,new List<Calling_List__c>{objBU});
								}
			}
        } system.debug( 'map: ' + mapPartyIdAcc);
		if( !mapPartyIdAcc.isEmpty() ) {
        list<Case> acntlist=new list<Case> ();
            for(Case accObj : caseLst ) {
            system.debug( 'accObj : ' + accObj );
            if(accObj.Party_ID__c != '') {
                  if(mapPartyIdAcc.containskey(accObj.Party_Id__c)){
                      accObj.OwnerId=mapPartyIdAcc.get(accObj.Party_ID__c)[0].OwnerId;
					  acntlist.add(accObj);					  
                  }   
                  else
                  {
                      //accObj.OwnerId=collectionQueueId;
                  }
                    
            }
        }
		
		system.debug( '<<<<<<<<<<<<<<<<< acntlist : ' + acntlist );
        if(acntlist.size()>0){
        update acntlist;}
       }
	   }
  }
  global void finish(Database.BatchableContext BC) {
    }
 
}