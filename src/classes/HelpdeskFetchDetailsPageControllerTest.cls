@isTest
private class HelpdeskFetchDetailsPageControllerTest {

	@isTest static void test_getTaskDetails() {
        FM_Case__c obj = new FM_Case__c();
		obj.Task_Id__c = 124;
        insert obj;
		PageReference myVfPage = Page.HelpdeskFetchDetailsPage;
		Test.setCurrentPage(myVfPage);
		ApexPages.StandardController sc = new ApexPages.StandardController(obj);
		// Put Id into the current page Parameters
		ApexPages.currentPage().getParameters().put('id',obj.Id);
        Test.setMock(WebServiceMock.class, new TaskMockClass());
				Test.setMock( WebServiceMock.class, new MaintenanceRequestMock() );
				User objUser = [SELECT Id from User
											where IsActive = true
											AND Profile.Name = 'System Administrator' LIMIT 1];
				System.runAs(objUser){
					Test.startTest();
	        new HelpdeskFetchDetailsPageController(sc).fetchTaskDetails();
	        Test.stopTest();
				}

    }

}