/*
Developed By: DAMAC IT Team
Test Class for :DAMAC_CDC_REST_UTILITY
*/
@istest
public class DAMAC_CDC_REST_UTILITY_TEST{

    static testmethod void test_GETMethods(){
    
    
        //test data
        
         NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
         insert srTemplate;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c=null;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        insert sr;
        
        NSIBPM__Step_Template__c stpTemplate =  InitializeSRDataTest.createStepTemplate('Deal','MANAGER_APPROVAL');
        insert stptemplate;
        
        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
        Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);
        
        
        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id,stepStatuses.values()[0].id,stptemplate.id);
        insert stp;
        
        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        insert lstbk;
        
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        insert loc;       
        
        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        bu.Registration_ID__c = '123';
        insert bu;
        
        buyer__c b = new buyer__c();
        b.Buyer_Type__c =  'Individual';
        b.Address_Line_1__c =  'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.City__c = 'Dubai' ;
        //b.Account__c =Acc.Id ;
        //b.Inquiry__c = [select id from Inquiry__c  where Last_Name__c ='Test'].Id ;
        b.Date_of_Birth__c = string.valueof(system.today().addyears(-30)) ;
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname' ;
        b.Last_Name__c =  'lastname';
        b.Nationality__c = 'Indian' ;
        b.Passport_Expiry_Date__c = string.valueof(system.today().addyears(20)) ;
        b.Passport_Number__c = 'J0565556' ;
        b.Phone__c = '569098767' ;
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b.Place_of_Issue__c =  'India';
        b.Title__c = 'Mr';
        b.booking__c = lstbk[0].id;
        b.Primary_Buyer__c =true;
        insert b;
        
        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Booking_Unit__c = bu.id;
        pp.Effective_From__c = system.today().adddays(-4);
        pp.Effective_To__c = system.today().adddays(7); 
        pp.Building_Location__c =  loc.id;      
        insert pp;
        
        Payment_Terms__c pt = new Payment_Terms__c();
        pt.Payment_Plan__c = pp.id;
        pt.Booking_Unit__c = bu.id;
        pt.Percent_Value__c = '5';
        insert pt;
    
    
        // PTerms
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        // pass the req and resp objects to the method     
        req.requestURI = 'https://damacholding--fullcopy.cs80.my.salesforce.com/services/apexrest/sfdetails';  
        req.httpMethod = 'GET';
        RestContext.request = req;
        req.addparameter('regid','123');
        req.addparameter('rtype','pt');
        try{
            DAMAC_CDC_REST_UTILITY.doGet();
        }
        catch(exception e){
        }
        
        // Deatils
        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
    
        // pass the req and resp objects to the method     
        req1.requestURI = 'https://damacholding--fullcopy.cs80.my.salesforce.com/services/apexrest/sfdetails';  
        req1.httpMethod = 'GET';
        RestContext.request = req1;
        req1.addparameter('regid','123');
        req1.addparameter('rtype','dt');
        try{
            DAMAC_CDC_REST_UTILITY.doGet();
        }
        catch(exception e){
        }
        
        
        //SRdetails
        NSIBPM__Service_Request__c srname = [select id,name from NSIBPM__Service_Request__c  where id=:sr.id limit 1];
        RestRequest req2 = new RestRequest(); 
        RestResponse res2 = new RestResponse();
    
        // pass the req and resp objects to the method     
        req2.requestURI = 'https://damacholding--fullcopy.cs80.my.salesforce.com/services/apexrest/sfdetails';  
        req2.httpMethod = 'GET';
        RestContext.request = req2;
        req2.addparameter('srnum',srname.name);
        req2.addparameter('rtype','dt');
        try{
            DAMAC_CDC_REST_UTILITY.doGet();
        }
        catch(exception e){
        }
    
    }
     
    // POST methods
    
    static testmethod void test_POSTMethods(){
    
        //test data
        
         NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
         insert srTemplate;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c=null;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        insert sr;
        
        NSIBPM__Step_Template__c stpTemplate =  InitializeSRDataTest.createStepTemplate('Deal','MANAGER_APPROVAL');
        insert stptemplate;
        
        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
        Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);
        
        
        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id,stepStatuses.values()[0].id,stptemplate.id);
        insert stp;
        
        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        insert lstbk;
        
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        insert loc;       
        
        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        bu.Registration_ID__c = '123';
        insert bu;
        
        buyer__c b = new buyer__c();
        b.Buyer_Type__c =  'Individual';
        b.Address_Line_1__c =  'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.City__c = 'Dubai' ;
        //b.Account__c =Acc.Id ;
        //b.Inquiry__c = [select id from Inquiry__c  where Last_Name__c ='Test'].Id ;
        b.Date_of_Birth__c = string.valueof(system.today().addyears(-30)) ;
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname' ;
        b.Last_Name__c =  'lastname';
        b.Nationality__c = 'Indian' ;
        b.Passport_Expiry_Date__c = string.valueof(system.today().addyears(20)) ;
        b.Passport_Number__c = 'J0565556' ;
        b.Phone__c = '569098767' ;
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b.Place_of_Issue__c =  'India';
        b.Title__c = 'Mr';
        b.booking__c = lstbk[0].id;
        b.Primary_Buyer__c =true;
        insert b;
        
        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Booking_Unit__c = bu.id;
        pp.Effective_From__c = system.today().adddays(-4);
        pp.Effective_To__c = system.today().adddays(7); 
        pp.Building_Location__c =  loc.id;      
        insert pp;
        
        Payment_Terms__c pt = new Payment_Terms__c();
        pt.Payment_Plan__c = pp.id;
        pt.Booking_Unit__c = bu.id;
        pt.Percent_Value__c = '5';
        insert pt;
        
        // executed
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        // pass the req and resp objects to the method     
        req.requestURI = 'https://damacholding--fullcopy.cs80.my.salesforce.com/services/apexrest/sfdetails';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        req.requestBody = (blob.valueof('{  "regid": "123",  "cdcstatus":"executed",  "cdccomments":"cin",  "checkindatetime":"2017-12-10T13:23:32+04:00"}'));       
        try{
            DAMAC_CDC_REST_UTILITY.doPost();
        }
        catch(exception e){
        }
        Date d = Date.newinstance(25,12,2021);
        IPMS_Registration_Status_Update.GetDatetext(d); 
        List<Id> recordids = new List<Id>();
        recordids.add(lstbk[0].id);
        IPMS_Registration_Status_Update.sendRegnUpdate(recordids,'STATUS_UPDATE','SS');
        STRING body = SetResponse(BU.id);
        IPMS_Registration_Status_Update.parseRegnUpdateResponse(body,'SS');
        
    
    
    }
    
    private static string SetResponse(String idval){
        string body = '';
        body+='<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">';
        body+='<env:Header/>';
        body+='<env:Body>';
          body+='<OutputParameters xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/process/">';
             body+='<X_RESPONSE_MESSAGE>';
                body+='<X_RESPONSE_MESSAGE_ITEM>';
                   body+='<PARAM_ID>'+idval+'</PARAM_ID>';
                   body+='<PROC_STATUS>S</PROC_STATUS>';
                   body+='<PROC_MESSAGE>Bank :SFDC Test Bank One Created for Vendor with Id:810117</PROC_MESSAGE>';
                   body+='<ATTRIBUTE1>810117</ATTRIBUTE1>';
                   body+='<ATTRIBUTE2>1274778</ATTRIBUTE2>';
                   body+='<ATTRIBUTE3>123</ATTRIBUTE3>';
                   body+='<ATTRIBUTE4 xsi:nil="true"/>';
                body+='</X_RESPONSE_MESSAGE_ITEM>';
             body+='</X_RESPONSE_MESSAGE>';
             body+='<X_RETURN_STATUS>S</X_RETURN_STATUS>';
             body+='<X_RETURN_MESSAGE>Process Completed successfully...</X_RETURN_MESSAGE>';
          body+='</OutputParameters>';
        body+='</env:Body>';
        body+='</env:Envelope>';
        body=body.trim();
        body= body.replaceAll('null', '');
        body=body.trim();
        
        return body;
    }
    
    static testmethod void test_ObjCls(){   
        
        
        string str= '{"STATUS":"Error","REGID":"92781","MESSAGE":"Cannot update CDC status as SR-66516 Internal Status (Docs Verified) not allowed at this time for VCT/SD94/XM1594B"}';
        DAMAC_CDC_POST_RESP_UNIT.parse(str);
        
        
        
        string pstr = '{"PAYMENT_TERMS":[{"PERCENT":"24.00","PAYMENT_DATE":null,"INSTALLMENT":"DP","EXPECTED_DATE":null,"EVENT":"Immediate"},{"PERCENT":"0.00","PAYMENT_DATE":null,"INSTALLMENT":"I001","EXPECTED_DATE":null,"EVENT":null},{"PERCENT":"5.00","PAYMENT_DATE":null,"INSTALLMENT":"I002","EXPECTED_DATE":null,"EVENT":"Within 180 Days of Sale Date"},{"PERCENT":"5.00","PAYMENT_DATE":null,"INSTALLMENT":"I003","EXPECTED_DATE":null,"EVENT":"Within 360 days of Sale Date"},{"PERCENT":"10.00","PAYMENT_DATE":null,"INSTALLMENT":"I004","EXPECTED_DATE":null,"EVENT":"Within 540 days of Sale Date"},{"PERCENT":"10.00","PAYMENT_DATE":null,"INSTALLMENT":"I005","EXPECTED_DATE":null,"EVENT":"Within 630 days of Sale Date"},{"PERCENT":"10.00","PAYMENT_DATE":null,"INSTALLMENT":"I006","EXPECTED_DATE":null,"EVENT":"Within 720 days of Sale Date"},{"PERCENT":"10.00","PAYMENT_DATE":null,"INSTALLMENT":"I007","EXPECTED_DATE":null,"EVENT":"Within 900 days of Sale Date"},{"PERCENT":"10.00","PAYMENT_DATE":null,"INSTALLMENT":"I008","EXPECTED_DATE":null,"EVENT":"Within 1080 days of Sale Date"},{"PERCENT":"20.00","PAYMENT_DATE":null,"INSTALLMENT":"I009","EXPECTED_DATE":null,"EVENT":"On Completion"}]}';
        DAMAC_CDC_RESPONSE_OBJ_PAYTERMS.parse(pstr);
        
        
        string srstr = '{"SFSRNUMBER":"SR-66335","bookingunits":[{"regId":"92694","cdcstatus":"verified"},{"regId":"92707","cdcstatus":null}]}';
        DAMAC_CDC_RESPONSE_OBJ_SR.parse(srstr);
        
        
        string regstr = '{"X_RETURN_STATUS":"S","X_ERROR_MSG":null,"UNIT_NAME":"VET/SD114/N259B","STATUS":"SA - Agreement Generated","SIZE_OF_APARTMENT":"2696.00","SFSRSTATUS":"Docs Verified","SFSRNUMBER":"SR-66335","SELLER_NAME":null,"REGISTRATION_ID":"92707","REGISTRATION_DATE":"2017-10-26 13:15:49","PROJECT_NAME":null,"PRIMARY_PHONE_NUM":"56664308456","PRESENT_CM_PRICE":"","POSTAL_CODE":"","PHONE_NUMBER":"56664308456","PERMITTED_USE":"Residential","paymentTerms":null,"PASSPORT_NO":"h789021","PARKING_ADDENDUM":"","NATIONALITY":"Indian","FINANCE":"N","FAX_NUMBER":"","EMAIL_ADDRESS":"varun@constancecapital.com","EFF_DATE_OF_CURR_CM_PRICE":"","DOCUMENTS_OK":"true","DISPUTE":null,"DEPOSIT_RECEIVED":"false","CUSTOMER_NAME":"Test Velma","COUNTRY":"India","CM_PRICE":null,"CITY":"Hyderabad","CDCSTATUS":null,"APARTMENT_PRICE":"0.00","ADDRESS4":null,"ADDRESS3":null,"ADDRESS2":null,"ADDRESS1":"AL1"}';
        DAMAC_CDC_RESPONSE_OBJ_UNIT.parse(regstr);
    }


}