@RestResource(urlMapping='/HappinessSurvey/*')
global with sharing class HappinessSurvey
{//*****AKISHOR: 29Nov2019 for happiness survey***************//  
 
 @HttpPatch
    global static ResponseWrapper HappinessSurvey( String UnitName, String Project, String SurveyLocation, String CustomerType,String Source,
                                        String CommonAreaLocation, String LocationName, String Question, String SurveyOutcome,
                                        String VisitorName, String VisitorPhone, String ReasonOfUnSatis, String Comments) 
    {
    
    //String Vstr='N';
        List<Booking_Unit__c> BU;String INVVSTR='N';
        ResponseWrapper objResponseWrapper = new ResponseWrapper();
       
            String Mobile; if(UnitName=='VISITOR'){INVVSTR='Y';}//dummy unit for visitor 20Jan2020
            List <Survey_Taken_CRM__c> ClList = new List<Survey_Taken_CRM__c>();  
            List<Account> Ownr= new List<Account>();String OwTntMobile='';
            List<Account> Tnt= new List<Account>();
        if(UnitName !='')// && INVVSTR=='N')
        {
             BU =[Select Id, Unit_Name__c,Account_Id__c,Tenant__c from Booking_Unit__c where Unit_Name__c=:UnitName AND Unit_Active__c='Active' limit 1];
               System.debug('entered:::'+UnitName + BU);
                  if(BU.size() == 0)
                  {
                  INVVSTR='Y';Mobile=VisitorPhone;
                  }
                  else if(BU.size() > 0)
                   {
                    if(CustomerType=='Owner'|| CustomerType=='OWNER'){
                     //BU =[Select Id, Unit_Name__c,Account_Id__c,Tenant__c from Booking_Unit__c where Unit_Name__c=:UnitName AND Unit_Active__c='Active' limit 1];
                            Ownr=[Select Id,Mobile_Phone_Encrypt__pc from Account where Id= : BU[0].Account_Id__c limit 1];
                             if( Ownr.size() > 0){
                                OwTntMobile=Ownr[0].Mobile_Phone_Encrypt__pc;
                            }
                     }
                     else if(CustomerType=='Tenant'||CustomerType=='TENANT'){
                     //BU =[Select Id, Unit_Name__c,Account_Id__c,Tenant__c from Booking_Unit__c where Unit_Name__c=:UnitName AND Unit_Active__c='Active' limit 1];
                             Tnt=[Select Id,Mobile_Phone_Encrypt__pc from Account where Id=:BU[0].Tenant__c limit 1];
                             System.debug('Tnt:::'+Tnt);
                             if(Tnt.size() > 0){
                                 OwTntMobile=Tnt[0].Mobile_Phone_Encrypt__pc;
                             }
                     }
                     if(OwTntMobile !=''){Mobile=OwTntMobile;}
                     else{Mobile=VisitorPhone;}
                  }
                   System.debug('Mobile:::'+Mobile+INVVSTR);                 
             if(BU.size() > 0 || INVVSTR=='Y'){
             
                 Survey_Taken_CRM__c Survey = new Survey_Taken_CRM__c();
                 Survey.UntName__c=UnitName;
                 Survey.Visitor_Name__c=VisitorName;
                 if(CustomerType=='Owner'||CustomerType=='Tenant'){
                 Survey.Visitor_Phone__c=OwTntMobile;
                 }
                 else{
                    Survey.Visitor_Phone__c=VisitorPhone;
                 }
                 Survey.Type_Of_Customer__c=CustomerType;Survey.Survey_LocationName__c=LocationName;
                 Survey.survey_Location__c=CommonAreaLocation;
                 Survey.Survey_Source__c=Source;
                 Survey.survey_Outcome__c= SurveyOutcome;
                 Survey.Question__c = Question;
                 Survey.ProjName__c=Project;
                 if(INVVSTR== 'N'){
                 Survey.Booking_Unit__c=BU[0].Id;Survey.Reason_of_Unsatisfaction__c=ReasonOfUnSatis;}
                 Survey.Comment__c=Comments;
                 ClList.add(Survey);
                }
               System.debug('List:::'+ClList);  
                    if(Mobile!='')
                    {
                    String SMS='';
                     System.debug('B4 SMS:::'+Mobile);  
                    if(SurveyOutcome=='Unsatisfied'||SurveyOutcome=='unsatisfied'||SurveyOutcome=='UNSATISFIED'){
                    SMS = 'Thanks for your feedback, someone from customer happiness team will contact you shortly.\n'; }
                    else if(SurveyOutcome=='Satisfied'||SurveyOutcome=='satisfied'||SurveyOutcome=='SATISFIED'||SurveyOutcome=='Happy'
                    ||SurveyOutcome=='HAPPY'||SurveyOutcome=='happy'){
                    SMS = 'Thanks for your feedback.\n'; }
                    HttpRequest req = new HttpRequest();
                    HttpResponse res = new HttpResponse();
                    Http http = new Http();
                    String user = ''; String passwd = '';String strSID = '';                
               
                    user = Label.FM_SMS_Service_Username; //'loams.';
                    passwd =  Label.FM_SMS_Service_Password; //'1@#$%qwert';
                    //strSID =  Label.FM_SMS_Service_SenderId; //'LOAMS';
                    strSID = SMSClass.getSenderName(user, Mobile, false);

                    req.setMethod('POST' ); // Method Type
                    req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx'); //SMS WebService                                     
                    req.setBody('user='+ user + '&passwd=' + passwd +'&message=' + SMS + '&mobilenumber=' + Mobile+ '&sid='+strSID+ '&MTYPE=LNG&DR=Y');                   
                    res = http.send(req);
                    System.debug('res :::'+res );
                    }
                    System.debug('After SMS:::'+ClList);     
             
                try{
                    if( ClList!= NULL && ClList.size() > 0)
                    {
                         system.debug('ClList '+ClList);
                         database.Insert (ClList);
                        //SendSMS(Mobile);
                        objResponseWrapper.status = 'Survey Recorded Sucessfully';
                        //objResponseWrapper.errorMessage  = lstInvToUpdate;
                        objResponseWrapper.statusCode = '200';
                        
                        system.debug('objResponseWrapper :'+ objResponseWrapper);
                        system.debug('JSON.serialize(objResponseWrapper):'+ JSON.serialize(objResponseWrapper));
                        system.debug('BLOB JSON.serialize(objResponseWrapper):'+ Blob.valueOf(JSON.serialize(objResponseWrapper)));                        
                                                
                                        } 
                    }                                       
                catch( Exception ex ) {
                        system.debug( ex.getMessage() );
                        objResponseWrapper.errorMessage = ex.getMessage();
                        objResponseWrapper.statusCode = '400';
                        
                        system.debug('objResponseWrapper :'+ objResponseWrapper);
                        system.debug('JSON.serialize(objResponseWrapper):'+ JSON.serialize(objResponseWrapper));
                        system.debug('BLOB JSON.serialize(objResponseWrapper):'+ Blob.valueOf(JSON.serialize(objResponseWrapper)));
                    
                                 
                    } 
                                                
                
                }
          return objResponseWrapper;
 }
     
    
    //for sending response    
    global class ResponseWrapper {
        global String status;
        global String statusCode;
        global String errorMessage;
       
        global ResponseWrapper() {
            this.status = '';
            this.statusCode = '';
            this.errorMessage = '';
        }
    }
   

}