/**********************************************************************************************************************
Description: This class contains reusable SOQL queries & generic methods required in Damac Living App API classes
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   16-12-2020      | Shubham Suryawanshi | Created initial draft
***********************************************************************************************************************/

public class HDApp_Utility{
    /*
     * Static field to check sandbox context */
    public static Boolean IsSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;

    /**********************************************************************************************************************
    Description : Method to get account record for given accountId
    Parameter(s):  String accountId
    Return Type : List<Account> 
    **********************************************************************************************************************/
    public static List<Account> getAccountFromAccountId(String accId) {
        List<Account> lstAccount = [SELECT id 
                                         , Name
                                         , Party_ID__c
                                         , Email__pc
                                         , Email__c 
                                         , IsPersonAccount
                                    FROM Account 
                                    WHERE Id =: accId ];
        return lstAccount;
    }
    
    /**********************************************************************************************************************
    Description : Method to get Booking Unit record for given booking Unit Id
    Parameter(s):  String bookingUnitId
    Return Type : List<Booking_Unit__c> 
    **********************************************************************************************************************/
    public static List<Booking_Unit__c> getUnitsFromBUId(String buId) {
        List<Booking_Unit__c> units = [SELECT Id
                                            , Name
                                            , Registration_ID__c
                                            , Unit_Name__c
                                            , Inventory__c
                                            , Inventory__r.Building_Location__c
                                            , Inventory__r.Building_Location__r.Id
                                            , Inventory__r.Building_Location__r.Name
                                            , Inventory__r.Building_Location__r.Building_Name__c
                                            , Inventory__r.Building_Location__r.Loams_Email__c
                                            , Inventory__r.Building_Location__r.Security_Deposit_Cheque_Amount__c
                                            , Inventory__r.Building_Location__r.Garbage_disposal_Amount__c
                                            , Inventory__r.Building_Location__r.Temporary_power_water_connection_Amount__c
                                       		, Property_City__c
                                       FROM Booking_Unit__c
                                       WHERE id =: buId
                                      ];
        return units;                                     
    }
    
    /**********************************************************************************************************************
    Description : Method to get Case record for given caseId
    Parameter(s):  String CaseId
    Return Type : List<Case> 
    **********************************************************************************************************************/
    public static List<Case> getCaseDetails(String caseId) {
        List<Case> caseList = [SELECT id 
                                   , CaseNumber
                                   , Booking_Unit__c
                                   , Complaint_Type__c
                                   , Complaint_Sub_Type__c
                                   , Description
                                   , Additional_Doc_File_URL__c
                                   , CreatedDate
                                   , OwnerId
                                   , currencyISOcode
                               FROM Case
                               WHERE id =: caseId];
        return caseList;
    }
    
    /**********************************************************************************************************************
    Description : Method to get all handedover, active/dummy booking units for given account, where account can be tenant or Owner to the unit
    Parameter(s):  String accountId
    Return Type : List<Booking_Unit__c> 
    **********************************************************************************************************************/
    public static List<Booking_Unit__c> getAllUnitsFromAccountId(String accountId) {
        //This query is same as used in fetching units in Login Api - API_CommunityPortalLogin
        List<Booking_Unit__c> lstUnits =  [SELECT Id
                                                , Name
                                                , Registration_ID__c
                                                , Unit_Name__c
                                                , Resident_Type__c
                                                , Property_Country__c
                                                , Inventory__c
                                                , Handover_Status__c
                                                , FM_PCC__c
                                                , DP_PCC_Issued__c /*Added on 10-09-2020 for PCC*/
                                                , Eligible_for_Handover_Notice_Date__c 
                                                , Handover_Notice_Sent_Date__c
                                                , Early_Handover_Date__c 
                                                , Handover_Date__c 
                                                , Lease_Handover_Date__c
                                                , Booking__r.Account__c
                                                , Property_Name__c
                                                , ByPassFMReminder__c
                                                , SC_From_RentalIncome__c
                                                , CM_Units__c
                                                , Booking__r.Account__r.Party_ID__c
                                                , Inventory__r.Marketing_Name_Doc__c
                                                , Inventory__r.Building_Location__c
                                                , Inventory__r.Building_Location__r.Property_Name__r.Name
                                                , Inventory__r.Building_Location__r.Property_Name__r.SnagValue__c
                                                , Inventory__r.Building_Location__r.Id
                                                , Inventory__r.Building_Location__r.Name
                                                , Inventory__r.Building_Location__r.Building_Name__c
                                                , Inventory__r.Building_Location__r.Loams_Email__c
                                                , Inventory__r.Building_Location__r.Location_Code__c 
                                                FROM Booking_Unit__c 
                                                WHERE (Dummy_Booking_Unit__c = true  OR Unit_Active__c = 'Active') 
                                                       AND ( Booking__r.Account__c =: accountId  
                                                             OR  Tenant__c =: accountId  
                                                             OR  Resident__c =: accountId  ) 
                                          ];

        System.debug('lstUnits-' + lstUnits);
        return lstUnits;
    }

    /**********************************************************************************************************************
    Description : Method to get FM Case & its details for given FMCaseID
    Parameter(s):  String fmCaseId
    Return Type : List<FM_Case__c> 
    **********************************************************************************************************************/
    public static List<FM_Case__c> getFMCase(String fmCaseId) {

        List<FM_Case__c> lstFmCase = [SELECT id 
                                           , Name
                                           , Status__c
                                           , Tenant__c
                                           , Booking_Unit__c
                                           , First_Name__c
                                           , Last_Name__c
                                           , Gender__c
                                           , Date_of_Birth__c
                                           , Nationality__c
                                           , Passport_Number__c
                                           , Expiry_Date__c
                                           , Emirates_Id__c
                                           , Mobile_Country_Code__c
                                           , Mobile_no__c
                                           , Tenant_Email__c
                                           , No_of_Adults__c
                                           , No_of_Children__c
                                           , Start_Date__c
                                           , End_Date__c
                                           , Ejari_Number__c
                                           , Move_in_date__c
                                           , Move_in_Type__c
                                           , Company__c
                                           , Contractor__c
                                           , Mobile_no_contractor__c
                                           , Submission_Date__c
                                           , isHavingPets__c
                                           , isPersonWithSpecialNeeds__c
                                           , Expected_move_out_date__c
                                           , Move_in_move_out_type__c
                                           , Reason_for_Move_Out__c
                                           , Booking_Unit__r.Unit_Name__c
                                           , Type_of_Access_Card__c
                                           , Access_Card_Permission__c
                                           , Request_Type_DeveloperName__c
                                           , New_Access_Card_Fee__c
                                           , Access_Card_Replacement_Fee__c
                                           , Approval_Status__c
                                           , Unit_Name__c
                                           , Access_Card_Payment_Receipt_No__c
                                           , Access_Card_Payment_Receipt_URL__c
                                           , Type_of_Alteration__c
                                           , Type_of_NOC__c
                                           , Description__c
                                           , Person_To_Collect__c
                                           , Contact_person_contractor__c
                                           , Email_2__c
                                           , Mobile_Country_Code_2__c
                                           , FM_Manager_Email__c
                                           , Admin__c
                                           , Booking_Unit__r.Property_Name__c
                                           , Approving_Authorities__c
                                           , Booking_Unit__r.Property_City__c
                                           , FitOut_Alterations_Payment_Receipt_No__c
                                           , Request_Type__c
                                           , Noc_for_Fitout_Alterations_Fee__c
                                           , Suggestion_Type__c
                                           , Suggestion_Sub_Type__c
                                           , Company_Name__c
                                           , Mobile_Country_Code_3__c
                                           , Office_tel_contractor__c
                                           , Contractor_Type__c
                                           , Number_of_Employees__c
                                           , Work_Permit_Type__c
                                           , Location_of_Work__c
                                           , Permit_To_Work_For__c
                                           , Is_Contractor_Consultant_Notified__c

                                           , (SELECT id
                                                  , Name__c
                                                  , Relationship__c
                                                  , Email__c
                                                  , Mobile__c 
                                              FROM Emergency_Contact_Details__r 
                                              WHERE RecordType.Name = 'Emergency Contact') 
                                           , (SELECT id
                                                   , Vehicle_Number__c
                                                   , Vehicle_Make_Model__c 
                                                   , Vehicle_Sticker_No__c
                                                   , Parking_Slot_Number__c
                                              FROM Vehicle_Details__r 
                                              WHERE RecordType.Name = 'Vehicle' )
                                           , (SELECT id
                                                   , Name__c
                                                   , Nationality__c
                                                   , Gender__c
                                                   , Date_of_Birth__c
                                                   , Passport_Number__c
                                                   , Identification_Number__c
                                                   , Mobile__c
                                                   , Email__c
                                              FROM Resident_Details__r
                                              WHERE RecordType.Name = 'Resident')  
                                           , (SELECT id
                                                   , Attachment_url__c
                                                   , Name 
                                                   , Type__c
                                              FROM Documents__r) 
                                           , (SELECT id
                                                   , Employee_Name__c
                                                   , Emirates_ID__c
                                                   , Company_Name__c
                                                   , Employee_Mobile_Number__c
                                              FROM Contractor_Informations__r
                                             )
                                     FROM FM_Case__c WHERE 
                                     id =: fmCaseId];
        System.debug('lstFmCase:: ' + lstFmCase);
        return lstFmCase;
    }

    /**********************************************************************************************************************
    Description : Method to get user 7 its account details from given username
    Parameter(s):  String username
    Return Type : List<User>
    **********************************************************************************************************************/
    public static List<User> getUserDetailsFromUsername(String userName) {
        List<User> lstUser = [SELECT Name 
                                   , Profile.Name
                                   , username
                                   , is_Reset_Password_Initiated__c
                                   , Login_Invalid_Attempt_Timestamp__c
                                   , Community_User_Invalid_Login_Attempts__c
                                   , AccountId
                                   , ContactId
                                   , Account.Name
                                   , Account.Party_ID__c
                                   , Account.Email__pc
                                   , Account.Email__c 
                                   , Account.IsPersonAccount
                                   , Account.Mobile_Phone_Encrypt__pc
                                   , Account.Mobile_Phone_Encrypt__c
                                   , Account.Mobile_Country_Code__pc
                                   , Account.Mobile_Country_Code__c
                                   , Contact.Name 
                                   , Account.Change_Password_OTP_Attempts__c
                                   , Account.First_Change_Password_OTP_Time__c
                                   , Account.Is_Blocked_for_Change_Password_OTP__c
                                   , Account.Last_Change_Password_OTP_Time__c
                             FROM User 
                             WHERE userName =: userName 
                             AND Profile.Name 
                             LIKE '%COMMUNITY%' 
                             LIMIT 1];
        System.debug('lstUser:: ' + lstUser);                     
        return lstUser;
    }

    /**********************************************************************************************************************
    Description : Method to get GUID which is used in OTP verification
    Parameter(s):  NA
    Return Type : String
    **********************************************************************************************************************/
    public static String getGUID() {

        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        System.debug('guid generated:: ' + guid);
        return guid;
    }
    
    /**********************************************************************************************************************
    Description : Method to get map of picklist values along with its dependent picklist value
    Parameter(s):  Schema.sObjectField
    Return Type : Map<String, List<String>>
    Example: To call this method - getDependentPicklistValues(Case.Complaint_Sub_Type__c)
    **********************************************************************************************************************/
    public static Map<String, List<String>> getDependentPicklistValues(Schema.sObjectField dependToken) {
        Schema.DescribeFieldResult depend = dependToken.getDescribe();
        Schema.sObjectField controlToken = depend.getController();
        if (controlToken == null) {
            return new Map<String, List<String>>();
        }
     
        Schema.DescribeFieldResult control = controlToken.getDescribe();
        List<Schema.PicklistEntry> controlEntries;
        if(control.getType() != Schema.DisplayType.Boolean) {
            controlEntries = control.getPicklistValues();
        }
     
        String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        Map<String,List<String>> dependentPicklistValues = new Map<String,List<String>>();
        for (Schema.PicklistEntry entry : depend.getPicklistValues()) {
            if (entry.isActive() && String.isNotEmpty(String.valueOf(((Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')))) {
                List<String> base64chars =
                        String.valueOf(((Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')).split('');
                for (Integer index = 0; index < (controlEntries != null ? controlEntries.size() : 2); index++) {
                    Object controlValue =
                            (controlEntries == null
                                    ?   (Object) (index == 1)
                                    :   (Object) (controlEntries[index].isActive() ? controlEntries[index].getLabel() : null)
                            );
                    Integer bitIndex = index / 6;
                    if (bitIndex > base64chars.size() - 1) {
                        break;
                    }
                    Integer bitShift = 5 - Math.mod(index, 6);
                    if  (controlValue == null || (base64map.indexOf( base64chars[ bitIndex ] ) & (1 << bitShift)) == 0)
                        continue;
                    if (!dependentPicklistValues.containsKey((String) controlValue)) {
                        dependentPicklistValues.put((String) controlValue, new List<String>());
                    }
                    dependentPicklistValues.get((String) controlValue).add(entry.getLabel());
                }
            }
        }
        return dependentPicklistValues;
    }
    
    /**********************************************************************************************************************
    Description : Method to get MultipleDocRequest instance for document to be uploaded
    Parameter(s):  String attachmentBody, String fileName, String caseNumber
    Return Type : UploadMultipleDocController.MultipleDocRequest
    **********************************************************************************************************************/
    public static UploadMultipleDocController.MultipleDocRequest getCaseUploadDocumentWrapper(String attachmentBody, String fileName, String caseNumber) {

        Integer i=1;
        if(String.isNotBlank( attachmentBody ) && String.isNotBlank( fileName )) {
            UploadMultipleDocController.MultipleDocRequest objMultipleDocRequest =
                                    new UploadMultipleDocController.MultipleDocRequest();
            objMultipleDocRequest.category = 'Document';
            objMultipleDocRequest.entityName = 'Damac Service Requests';
            objMultipleDocRequest.base64Binary = attachmentBody;
            objMultipleDocRequest.fileDescription = extractType(fileName);
            i++;
            objMultipleDocRequest.fileId = caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(fileName);
            objMultipleDocRequest.fileName = caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(fileName);

            objMultipleDocRequest.registrationId = caseNumber;
            objMultipleDocRequest.sourceId = 'IPMS-'+caseNumber+'-'+extractName(fileName);
            objMultipleDocRequest.sourceFileName = 'IPMS-'+caseNumber+'-'+extractName(fileName);
            //attachment1 = objMultipleDocRequest.sourceFileName;
            //lstMultipleDocReq.add(objMultipleDocRequest);

            return objMultipleDocRequest;
        }
        else {
            return null;
        }
    }
    
    /**********************************************************************************************************************
    Description : Method to get filename of attachment being uploaded from above method - getCaseUploadDocumentWrapper
    Parameter(s):  String strName (complete name of file Ex: test.pdf)
    Return Type : String
    **********************************************************************************************************************/
    public static String extractName( String strName ) {
      return strName.substring( strName.lastIndexOf('\\')+1 ) ;
    }

    /**********************************************************************************************************************
    Description : Method to get file extension being uploaded from above method - getCaseUploadDocumentWrapper
    Parameter(s): String fileName (complete name of file Ex: test.pdf)
    Return Type : String
    **********************************************************************************************************************/
    public static String extractType(String fileName) {
        String strFileExtension;
        if( String.isNotBlank( fileName ) && fileName.lastIndexOf('.') != -1 ) {
            strFileExtension = fileName.substring( fileName.lastIndexOf('.')+1 , fileName.length() );
            strFileExtension = strFileExtension.substringBefore('&');
        }
        System.debug('strFileExtension:: ' + strFileExtension);
        return strFileExtension;
    }
    
    /**********************************************************************************************************************
    Description : Declaration of common statusCode map used in every Damac Living API response
    Parameter(s):  NA
    Return Type : NA
    **********************************************************************************************************************/
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong',
        7 => 'Successfully notified',
        8 => 'Invalid username',
        9 => 'OTP Expiry',
        10 => 'Session Timeout',
        11 => 'Incorrect Password',
        12 => 'Too many attempts'
    };
    
    /**********************************************************************************************************************
    Description : Method to get meta_data object response for Damac Living APIs response
    Parameter(s):  String message, String devMsg, Integer statusCode
    Return Type : cls_meta_data
    **********************************************************************************************************************/
    public static cls_meta_data ReturnMetaResponse(String message, String devMsg, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data();
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = devMsg;
        return retMeta;
    }
    
    /**********************************************************************************************************************
    Description : Declaration of common meta_data wrapper class returned in every Damac Living API response
    Parameter(s):  NA
    Return Type : NA
    **********************************************************************************************************************/
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }
    
    /**********************************************************************************************************************
    Description : Method to get picklist options for any field, in any object
    Parameter(s):  String sObjectName, String fieldName
    Return Type : List<String>
    **********************************************************************************************************************/
    public static List<String> getPicklistOptions(String sObjectName, string fieldName) {  
        List<String> allOpts = new List<String>();
        
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(sObjectName);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        
        List<Schema.PicklistEntry> values = fieldMap.get(fieldName).getDescribe().getPickListValues();  
        
        for(Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        
        return allOpts;
    }
}