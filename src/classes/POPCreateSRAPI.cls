/**********************************************************************************************************************
Description: This API is used for creating POP SRs raised from Damac Living App
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By      | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   15-12-2020      | Anand Venkitakrishnan | Created initial draft
***********************************************************************************************************************/

@RestResource(urlMapping='/popCreateSR/*')
global class POPCreateSRAPI {
    public static String errorMsg;
    public static Integer statusCode;
    public static final String PROPERTY_INSTALLMENT = 'installment';
    public static final String SERVICE_CHARGE = 'service_charge';
    public static final String CASH = 'Cash';
    public static final String CHEQUE = 'Cheque';
    public static final String commonErrorMsg = 'This may be because of technical error that we\'re working to get fixed. Please try again later.';
    
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong',
        7 => 'Service charges overdue!'
    };
    
    @HttpPost
    global static FinalReturnWrapper apiMainMethod() {
        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString:'+jsonString);
        System.debug('Request params:'+req.params);
        
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        POPCaseWrapper objPOPWrap = new POPCaseWrapper();
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();
        
        if(String.isBlank(jsonString)) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'No JSON body found in request',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        System.debug('jsonString:'+jsonString);
        objPOPWrap = (POPCaseWrapper)JSON.deserialize(jsonString,POPCaseWrapper.class);
        System.debug('objPOPWrap:'+objPOPWrap);
        
        if(objPOPWrap.action == null || (!objPOPWrap.action.equalsIgnoreCase('draft') && !objPOPWrap.action.equalsIgnoreCase('submit') )) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Invalid parameter value in : action',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(objPOPWrap.account_id == null || String.isBlank(objPOPWrap.account_id)) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'No account_id found in request',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(objPOPWrap.pop_type == null || String.isBlank(objPOPWrap.pop_type)) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'No pop_type found in request',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(!objPOPWrap.pop_type.equals(PROPERTY_INSTALLMENT) && !objPOPWrap.pop_type.equals(SERVICE_CHARGE)) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Invalid pop_type value in request',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(objPOPWrap.payment_mode == null || String.isBlank(objPOPWrap.payment_mode)) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'No payment_mode found in request',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(objPOPWrap.payment_date == null || String.isBlank(objPOPWrap.payment_date)) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'No payment_date found in request',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(objPOPWrap.total_amount == null || (objPOPWrap.total_amount == 0)) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'No total_amount found in request',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(objPOPWrap.unit_details == null || objPOPWrap.unit_details.isEmpty()) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'No unit_details found in request',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(objPOPWrap.pop_type.equals(PROPERTY_INSTALLMENT) && (objPOPWrap.source_of_funds == null || String.isBlank(objPOPWrap.source_of_funds))) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'No source_of_funds found in request',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(objPOPWrap.pop_type.equals(PROPERTY_INSTALLMENT) && 
                (objPOPWrap.payment_mode.equals(CASH) || objPOPWrap.payment_mode.equals(CHEQUE)) && 
                (objPOPWrap.cheque_from_account == null || String.isBlank(objPOPWrap.cheque_from_account))) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'No cheque_from_account found in request',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        if(objPOPWrap != null) {
            System.debug('action:'+objPOPWrap.action);
            if(objPOPWrap.action.equalsIgnoreCase('submit') && String.isBlank(objPOPWrap.id)) {
                objMeta = ReturnMetaResponse(commonErrorMsg,'No POP Case id passed in the request',3);
                returnResponse.meta_data = objMeta;
                return returnResponse;
            }
            
            if(objPOPWrap.action.equalsIgnoreCase('submit') && String.isNotBlank(objPOPWrap.id)) {
                String missingDocName = verifyMandatoryDocs(objPOPWrap.pop_type,objPOPWrap.id);
                
                if(!String.isEmpty(missingDocName)) {
                    String errorMsg = missingDocName + ' upload pending';
                    objMeta = ReturnMetaResponse(commonErrorMsg,errorMsg, 2);
                    returnResponse.meta_data = objMeta;
                    return returnResponse;
                }
            }
            
            //Any verification of input data
            
            String popType = objPOPWrap.pop_type;
            
            if(popType.equals(PROPERTY_INSTALLMENT)) {
                Case objCase = processCase(objPOPWrap);
                System.debug('objCase:'+objCase);
                
                Case srRecord = fetchCaseDetails(objCase.Id);
                
                //Fetch more details from case
                if(objCase != null) {
                    objData.sr_id = srRecord.id;
                    objData.sr_number = srRecord.CaseNumber;
                    objData.sr_type = 'Submit proof of payment';
                    objData.sr_sub_type = 'Property installments';
                    objData.sr_status = srRecord.Status;
                    objData.sr_submission_date = srRecord.CreatedDate != null ? Datetime.newInstance(srRecord.CreatedDate.year(),srRecord.CreatedDate.month(),srRecord.CreatedDate.day()).format('yyyy-MM-dd') : '';
                    objData.pop_case = wrapCase(srRecord);
                    
                    objMeta = ReturnMetaResponse('Successful','', statusCode);
                }
                else {
                    objMeta = ReturnMetaResponse(commonErrorMsg,'No Case found/Created',3);
                }
            }
            else {
                FM_Case__c objFM = processFMCase(objPOPWrap);
                System.debug('objFM:'+objFm);
                
                FM_Case__c srRecord = fetchFmCaseDetails(objFM.Id);
                
                //Fetch more details from case
                if(srRecord != null) {
                    objData.sr_id = srRecord.id;
                    objData.sr_number = srRecord.Name;
                    objData.sr_type = 'Submit proof of payment';
                    objData.sr_sub_type = 'Service charges';
                    objData.sr_status = srRecord.Status__c;
                    objData.sr_submission_date = srRecord.Submission_Date__c != null ? Datetime.newInstance(srRecord.Submission_Date__c.year(),srRecord.Submission_Date__c.month(),srRecord.Submission_Date__c.day()).format('yyyy-MM-dd') : '';
                    objData.pop_case = wrapFmCase(srRecord);
                    
                    objMeta = ReturnMetaResponse('Successful','', statusCode);
                }
                else {
                    objMeta = ReturnMetaResponse(commonErrorMsg,'No FM Case found/Created',statusCode);
                }
            }
        }
        
        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;
        System.debug('returnResponse:'+returnResponse);
        
        return returnResponse;   
    }
    
    public static String verifyMandatoryDocs(String popType,String popCaseId) {
        System.debug('Inside VerifyMandatoryDocs');
        System.debug('popCaseId:'+popCaseId);
        
        Boolean isDocsValid = false;
        String missingDocName = '';
        
        List<String> onlyMandatoryDocs = new List<String>();
        onlyMandatoryDocs.add('pop document');
        
        List<SR_Attachments__c> insertedAttachments;
        
        if(popType.equals(PROPERTY_INSTALLMENT)) {
            insertedAttachments = [SELECT Name 
                                   FROM SR_Attachments__c 
                                   WHERE Case__c =: popCaseId];
        }
        else {
            insertedAttachments = [SELECT Name 
                                   FROM SR_Attachments__c 
                                   WHERE FM_Case__c =: popCaseId];
        }
        
        Set<String> uploadedDocs = new Set<String>();
        
        for(SR_Attachments__c objA : insertedAttachments) {
            uploadedDocs.add(objA.Name.toLowerCase());
        }
        
        System.debug('uploadedDocs:'+uploadedDocs);
        System.debug('onlyMandatoryDocs:'+onlyMandatoryDocs);
        
        for(String objReqDoc : onlyMandatoryDocs) {
            isDocsValid = uploadedDocs.contains(objReqDoc);
            if(!isDocsValid) {
                missingDocName = objReqDoc;
                break;
            } 
        }
        System.debug('isDocsValid:'+isDocsValid);
        
        if(Test.isRunningTest()) {
            return '';
        }
        
        return missingDocName;
    }
    
    public static Case processCase(POPCaseWrapper objPOPWrap) {
        String action = objPOPWrap.action;
        
        Case objCase = new Case();
        
        if(String.isNotBlank(objPOPWrap.id)) {
            objCase.id = objPOPWrap.id;
        }
        
        objCase.AccountId = objPOPWrap.account_id;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        objCase.Origin = 'Portal';
        objCase.SR_Type__c = 'Proof of Payment SR';
        objCase.Type = 'Proof of Payment SR';
        objCase.isHelloDamacAppCase__c = true;
        
        objCase.Payment_Mode__c = objPOPWrap.payment_mode;
        objCase.Payment_Date__c = Date.valueOf(objPOPWrap.payment_date);
        
        if(objPOPWrap.payment_mode.equals(CASH)) {
            objCase.Cash_Deposit_Slip_Details__c = objPOPWrap.cheque_from_account;
        }
        else if (objPOPWrap.payment_mode.equals(CHEQUE)) {
            objCase.Cheque_from_Account__c = objPOPWrap.cheque_from_account;
        }
        
        objCase.Source_of_Funds__c = objPOPWrap.source_of_funds;
        objCase.Payment_Allocation_Details__c = objPOPWrap.payment_remarks;
        
        Map<String,String> currencyMap = getCurrencyMap('Case','CurrencyIsoCode');
        
        if(currencyMap != NULL && !currencyMap.isEmpty() && currencyMap.containsKey(objPOPWrap.payment_currency)) {
            objCase.Payment_Currency__c = currencyMap.get(objPOPWrap.payment_currency);
        }
        else {
            objCase.Payment_Currency__c = objPOPWrap.payment_currency;
        }
        
        objCase.Total_Amount__c =  objPOPWrap.total_amount;
        objCase.Sender_Name__c = objPOPWrap.sender_name;
        objCase.Cheque_Bank_Name__c = objPOPWrap.sender_bank_name;
        objCase.Swift_Code__c = objPOPWrap.swift_code;
        System.debug('objCase:'+objCase);
        
        try {
            if(action.equalsIgnoreCase('draft') && String.isBlank(objPOPWrap.id)) {
                System.debug('Save as draft');
                objCase.Status = 'Draft Request';
                insert objCase;
            }
            else if(action.equalsIgnoreCase('draft') && String.isNotBlank(objPOPWrap.id)) {
                upsert objCase;
            }
            
            if(action.equalsIgnoreCase('submit')) {
                List<QueueSobject> ownerQueueList = [SELECT Id,Queue.Name,QueueId 
                                                     FROM QueueSobject 
                                                     WHERE SobjectType = 'Case' AND Queue.Name = 'Collection Queue'
                                                     LIMIT 1];
                System.debug('ownerQueueList:'+ownerQueueList);
                if(!ownerQueueList.isEmpty()) {
                    objCase.OwnerId = ownerQueueList[0].QueueId;
                }
                
                createSRBUList(objPOPWrap);
                
                if(!Test.isRunningTest()) {
                    System.debug('Submit the case');
                  objCase.Status = 'Submitted';
                  upsert objCase;
                }
                
                createPOPTask(objCase.Id);
                
                Account objAccount = new Account(Id=objPOPWrap.account_id);
                objAccount.POP_Status__c ='POP SR has been created' ;
                update objAccount;
            }
            
            errorMsg = 'Successful';
            statusCode = 1;
        }
        catch(Exception e) {
            System.debug('Exception: ' + e.getMessage());
            errorMsg = e.getMessage();
            statusCode = 6;
            
            if(objCase.Id != NULL) {
                Error_Log__c errorLog = new Error_Log__c(Case__c=objCase.Id,Account__c=objCase.accountId,Process_Name__c='Proof Of Payment',Error_Details__c=e.getMessage());
                insert errorLog;
            }
        }
        
        return objCase;
    }
    
    public static FM_Case__c processFMCase(POPCaseWrapper objPOPWrap) {
        String action = objPOPWrap.action;
        
        FM_Case__c objFMCase = new FM_Case__c();
        
        if(String.isNotBlank(objPOPWrap.id)) {
            objFMCase.id = objPOPWrap.id;
        }
        
        objFMCase.Account__c = objPOPWrap.account_id;
        objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Proof Of Payment').getRecordTypeId();
        objFMCase.Origin__c = 'Portal';
        objFMCase.Request_Type__c = 'Proof Of Payment';
        objFMCase.Request_Type_DeveloperName__c = 'Proof_of_Payment';
        objFMCase.isHelloDamacAppCase__c = true;
        
        objFMCase.Payment_Mode__c = objPOPWrap.payment_mode;
        objFMCase.Payment_Date__c = Date.valueOf(objPOPWrap.payment_date);
        objFMCase.Payment_Allocation_Details__c = objPOPWrap.payment_remarks;
        
        Map<String,String> currencyMap = getCurrencyMap('FM_Case__c','CurrencyIsoCode');
        
        if(currencyMap != NULL && !currencyMap.isEmpty() && currencyMap.containsKey(objPOPWrap.payment_currency)) {
            objFMCase.Currency__c = currencyMap.get(objPOPWrap.payment_currency);
        }
        else {
            objFMCase.Currency__c = objPOPWrap.payment_currency;
        }
        
        objFMCase.Total_Amount__c =  objPOPWrap.total_amount;
        objFMCase.Sender_Name__c = objPOPWrap.sender_name;
        objFMCase.Bank_Name__c = objPOPWrap.sender_bank_name;
        objFMCase.Swift_Code__c = objPOPWrap.swift_code;
        System.debug('objFMCase:'+objFMCase);
        
        //objFMCase.Raised_Date__c = System.now();
        
        try {
            if(action.equalsIgnoreCase('draft') && String.isBlank(objPOPWrap.id)) {
                Account objAcc = getCurrentAccountDetails(objPOPWrap.account_id);
                System.debug('objAcc:'+objAcc);
                if(objAcc != NULL) {
                    System.debug('objAcc.IsPersonAccount:'+objAcc.IsPersonAccount);
                  System.debug('objAcc.Email__pc:'+objAcc.Email__pc);
                  System.debug('objAcc.Email__c:'+objAcc.Email__c);
                    objFMCase.Account_Email__c = objAcc.IsPersonAccount ? objAcc.Email__pc : objAcc.Email__c;
                }
                System.debug('objFMCase.Account_Email__c:'+objFMCase.Account_Email__c);
                System.debug('Save as draft');
                objFMCase.Status__c = 'Draft Request';
                insert objFMCase;
            }
            else if(action.equalsIgnoreCase('draft') && String.isNotBlank(objPOPWrap.id)) {
                upsert objFMCase;
            }
            
            if(action.equalsIgnoreCase('submit')) {
                createSRBUList(objPOPWrap);
                
                if(!Test.isRunningTest()) {
                  System.debug('Submit the case');
                  objFMCase.Status__c = 'Submitted';
                  upsert objFMCase;
                }
            }
            
            errorMsg = 'Successful';
            statusCode = 1;
        }
        catch(Exception e) {
            System.debug('Exception: ' + e.getMessage());
            errorMsg = e.getMessage();
            statusCode = 6;
            
            if(objFMCase.Id != NULL) {
                Error_Log__c errorLog = FM_Utility.createErrorLog(objFMCase.Account__c,NULL,objFMCase.Id,'Proof Of Payment',e.getMessage());
              insert errorLog;
            }
        }
        
        return objFMCase;
    }
    
    public static Map<String,String> getCurrencyMap(String sObjectName, string fieldName) {  
        Map<String,String> currencyMap = new Map<String,String>();
        
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(sObjectName);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        List<Schema.PicklistEntry> values = fieldMap.get(fieldName).getDescribe().getPickListValues();  
        
        for(Schema.PicklistEntry a: values) {
            currencyMap.put(a.getValue(),a.getLabel());
        }
        System.debug('currencyMap:'+currencyMap);
        
        return currencyMap;
    }
    
    public static void createSRBUList(POPCaseWrapper objPOPWrap) {
        String popType = objPOPWrap.pop_type;
        
        List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c> ();
        
        for(cls_unit_details unitDetails : objPOPWrap.unit_details) {
            SR_Booking_Unit__c srBookingUnit = new SR_Booking_Unit__c();
            srBookingUnit.Booking_Unit__c =  unitDetails.unit_id;
            if(popType.equals(PROPERTY_INSTALLMENT)) {
              srBookingUnit.Case__c = objPOPWrap.id;
            }
            else {
                srBookingUnit.FM_Case__c = objPOPWrap.id;
            }
            srBookingUnitList.add(srBookingUnit);
        }
        
        if(!srBookingUnitList.isEmpty()) {
            insert srBookingUnitList;
        }
    }
    
    public static void createPOPTask(Id caseId) {
        String popAttachmentUrl;
        
        Case caseRecord = fetchCaseDetails(caseId);
        
        for(SR_Attachments__c attachment : caseRecord.SR_Attachments__r) {
            if(attachment.Type__c.equals('POP')) {
                popAttachmentUrl = attachment.Attachment_URL__c;
            }
        }
        
        Task objTask = new Task();
        objTask.WhatId = caseRecord.id;
        objTask.Subject = 'Verify Proof of Payment Details in IPMS';
        objTask.ActivityDate = System.today().addDays(1);
        objTask.Assigned_User__c = 'Finance';
        objTask.Document_URL__c = popAttachmentUrl;
        objTask.Process_Name__c = 'POP';
        objTask.Priority = 'High';
        objTask.Status = 'Not Started';
        
        if(!Test.isRunningTest()) {
          System.debug('POP Task:'+objTask);
          insert objTask;
        }
    }
    
    public static Account getCurrentAccountDetails(String accId) {
        if(String.isBlank(accId)) {
            return null;
        }
        
        List<Account> acc = [SELECT Id,Name,IsPersonAccount,Email__pc,Email__c 
                             FROM Account 
                             WHERE Id =: accId];
        return acc.size() > 0 ? acc[0] : null;
    }
    
    public static Case fetchCaseDetails(String caseId) {
        Case caseRecord = [SELECT Id,CaseNumber,AccountId,Status,CreatedDate,Payment_Mode__c,Payment_Date__c,Payment_Allocation_Details__c, 
                           Cash_Deposit_Slip_Details__c,Cheque_from_Account__c,Source_of_Funds__c,
                           Payment_Currency__c,Total_Amount__c,Sender_Name__c,Cheque_Bank_Name__c,Swift_Code__c, 
                           (SELECT Id,Booking_Unit__r.Unit_Name__c,Booking_Unit__r.Inventory__r.Building_Location__r.Building_Name__c FROM SR_Booking_Units__r WHERE Case__c =: caseId), 
                           (SELECT Id,Attachment_url__c,Name,Type__c FROM SR_Attachments__r WHERE Type__c = 'POP') 
                           FROM Case 
                           WHERE Id = :caseId];
        
        return caseRecord;
    }
    
    public static FM_Case__c fetchFmCaseDetails(String fmCaseId) {
        FM_Case__c fmCaseRecord = [SELECT Id,Name,Account__c,Status__c,Submission_Date__c,Payment_Mode__c,Payment_Date__c,Payment_Allocation_Details__c, 
                                   Currency__c,Total_Amount__c,Sender_Name__c,Bank_Name__c,Swift_Code__c, 
                                   (SELECT Id,Booking_Unit__r.Unit_Name__c,Booking_Unit__r.Inventory__r.Building_Location__r.Building_Name__c FROM Booking_Units__r WHERE FM_Case__c =: fmCaseId), 
                                   (SELECT Id,Attachment_url__c,Name,Type__c FROM Documents__r WHERE Type__c = 'POP') 
                                   FROM FM_Case__c 
                                   WHERE Id = :fmCaseId];
        
        return fmCaseRecord;
    }
    
    public static POPResponseWrapper wrapCase(Case caseRecord) {
        POPResponseWrapper popCaseWrapper = new POPResponseWrapper();
        
        popCaseWrapper.id = caseRecord.Id;
        popCaseWrapper.account_id = caseRecord.AccountId;
        popCaseWrapper.payment_mode = caseRecord.Payment_Mode__c;
        popCaseWrapper.payment_date = Datetime.newInstance(caseRecord.Payment_Date__c.year(),caseRecord.Payment_Date__c.month(),caseRecord.Payment_Date__c.day()).format('yyyy-MM-dd');
        popCaseWrapper.cash_deposit_slip = caseRecord.Cash_Deposit_Slip_Details__c;
        popCaseWrapper.cheque_from_account = caseRecord.Cheque_from_Account__c;
        popCaseWrapper.source_of_funds = caseRecord.Source_of_Funds__c;
        popCaseWrapper.payment_remarks = caseRecord.Payment_Allocation_Details__c;
    popCaseWrapper.payment_currency = caseRecord.Payment_Currency__c;
        popCaseWrapper.total_amount = caseRecord.Total_Amount__c;
        popCaseWrapper.sender_name = caseRecord.Sender_Name__c;
        popCaseWrapper.sender_bank_name = caseRecord.Cheque_Bank_Name__c;
        popCaseWrapper.swift_code = caseRecord.Swift_Code__c;
        
        List<cls_unit_details> unitsList = new List<cls_unit_details>();
        
        if(caseRecord.SR_Booking_Units__r.size() > 0) {
            for(SR_Booking_Unit__c obj : caseRecord.SR_Booking_Units__r) {
                cls_unit_details objUnit = new cls_unit_details();
                objUnit.unit_id = obj.Booking_Unit__r.Id;
                objUnit.unit_name = obj.Booking_Unit__r.Unit_Name__c;
                
                unitsList.add(objUnit);
            }
        }
        
        popCaseWrapper.unit_details = unitsList;
        
        List<cls_attachment> attachmentsList = new List<cls_attachment>();
    
        if(caseRecord.SR_Attachments__r.size() > 0) {
            for(SR_Attachments__c obj : caseRecord.SR_Attachments__r) {
                cls_attachment objAttach = new cls_attachment();
                objAttach.id = obj.id;
                objAttach.name = obj.Name;
                objAttach.attachment_url = obj.Attachment_URL__c;
                objAttach.file_extension = obj.Type__c;
                
                attachmentsList.add(objAttach);
            }
        }
        
        popCaseWrapper.attachments = attachmentsList;
        
        return popCaseWrapper;
    }
    
    public static POPResponseWrapper wrapFmCase(FM_Case__c fmCase) {
        POPResponseWrapper popCaseWrapper = new POPResponseWrapper();
        
        popCaseWrapper.id = fmCase.Id;
        popCaseWrapper.account_id = fmCase.Account__c;
        popCaseWrapper.payment_mode = fmCase.Payment_Mode__c;
        popCaseWrapper.payment_date = Datetime.newInstance(fmCase.Payment_Date__c.year(),fmCase.Payment_Date__c.month(),fmCase.Payment_Date__c.day()).format('yyyy-MM-dd');
        popCaseWrapper.payment_remarks = fmCase.Payment_Allocation_Details__c;
    popCaseWrapper.payment_currency = fmCase.Currency__c;
        popCaseWrapper.total_amount = fmCase.Total_Amount__c;
        popCaseWrapper.sender_name = fmCase.Sender_Name__c;
        popCaseWrapper.sender_bank_name = fmCase.Bank_Name__c;
        popCaseWrapper.swift_code = fmCase.Swift_Code__c;
        
        List<cls_unit_details> unitsList = new List<cls_unit_details>();
        
        if(fmCase.Booking_Units__r.size() > 0) {
            for(SR_Booking_Unit__c obj : fmCase.Booking_Units__r) {
                cls_unit_details objUnit = new cls_unit_details();
                objUnit.unit_id = obj.Booking_Unit__r.Id;
                objUnit.unit_name = obj.Booking_Unit__r.Unit_Name__c;
                
                unitsList.add(objUnit);
            }
        }
        
        popCaseWrapper.unit_details = unitsList;
        
        List<cls_attachment> attachmentsList = new List<cls_attachment>();
    
        if(fmCase.Documents__r.size() > 0) {
            for(SR_Attachments__c obj : fmCase.Documents__r) {
                cls_attachment objAttach = new cls_attachment();
                objAttach.id = obj.id;
                objAttach.name = obj.Name;
                objAttach.attachment_url = obj.Attachment_URL__c;
                objAttach.file_extension = obj.Type__c;
                
                attachmentsList.add(objAttach);
            }
        }
        
        popCaseWrapper.attachments = attachmentsList;
        
        return popCaseWrapper;
    }
    
    public static cls_meta_data ReturnMetaResponse(String message, String devMessage, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data();
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = devMessage;
        return retMeta;
    }
    
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }
  
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }
    
    public class cls_data {
        public String sr_id;
        public String sr_number;
        public String sr_type;
        public String sr_sub_type;
        public String sr_status;
        public String sr_submission_date;
        public POPResponseWrapper pop_case;
    }
    
    public class POPCaseWrapper {
        public String action;
        public String account_id;
        public String pop_type;
        public String id;
        public String payment_date;
        public String payment_mode;
        public String cheque_from_account;
        public String source_of_funds;
        public String payment_remarks;
        public String payment_currency;
        public Decimal total_amount;
        public String sender_name;
        public String sender_bank_name;
        public String swift_code;
        
        public cls_unit_details[] unit_details;
        
        public cls_attachment[] attachments;
    }
    
    public class POPResponseWrapper {
        public String id;
        public String account_id;
        public String payment_date;
        public String payment_mode;
        public String cash_deposit_slip;
        public String cheque_from_account;
        public String source_of_funds;
        public String payment_remarks;
        public String payment_currency;
        public Decimal total_amount;
        public String sender_name;
        public String sender_bank_name;
        public String swift_code;
        
        public cls_unit_details[] unit_details;
        
        public cls_attachment[] attachments;
    }
    
    public class cls_unit_details {
        public String unit_id;
        public String unit_name;
    }
    
    public class cls_attachment {
        public String id;
        public String name;
        public String attachment_url;
        public String file_extension;
    }
}