@isTest
public class AP_CompanyAddAgentController_TC {
    @IsTest
    static void  companyAddAgentContlr(){
        Id testUser = [SELECT id FROM User WHERE UserRole.Name != null LIMIT 1].id;
        
        Account accToInsert = new Account();
        accToInsert.Name = 'test';
        accToInsert.OwnerId =  testUser ;
        insert accToInsert;
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = accToInsert.Id;
        insert sr;
        
        Test.startTest();
        PageReference pageRef = Page.AP_CompanyAddAgent;
        pageRef.getParameters().put('id', String.valueOf(sr.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(accToInsert);
        AP_CompanyAddAgentController companyAgent = new AP_CompanyAddAgentController(sc);
        companyAgent.setIsAdd();
        
        Amendment__c currAmend = new Amendment__c();        
        currAmend.Portal_Administrator__c = true;
        currAmend.Agent_Representative__c = true;
        currAmend.Title__c = 'DR.';
        currAmend.First_Name__c = 'Test';
        currAmend.Last_Name__c = 'Test';
        currAmend.Email__c = 'test@test.com';
        currAmend.ID_Type__c = 'Passport';
        currAmend.ID_Number__c = '1234567890';
        currAmend.ID_Issue_Date__c = Date.parse('11/11/2019');
        currAmend.ID_Expiry_Date__c = Date.parse('12/12/2020');
        currAmend.Broker_Card_Number__c = '1234567899';
        currAmend.Broker_Card_Expiry_Date__c = Date.parse('12/12/2020');
        currAmend.Nationality__c = 'India';
        currAmend.Mobile_Country_Code__c = 'India: 0091';
        currAmend.Mobile__c = '1234567899';
        currAmend.Designation__c = 'Testing';
        companyAgent.currAmnd = currAmend;
        
        companyAgent.insertAmendment();
        companyAgent.createDocumentsForAmendments();
        Test.stopTest();
        
    }
}