@isTest
public class RentalPoolEHOCheckControllerTest {
    static Account objAcc;
    //static Case objCase;
    static Id recTypeRPEHOAgreement;
    static NSIBPM__Service_Request__c objDealSR;
    static list<Booking__c> listCreateBookingForAccount;
    //static list<Booking_Unit__c> listCreateBookingUnit;
    
    static void init() {
        objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);

        recTypeRPEHOAgreement = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Agreement Early Handover').getRecordTypeId();
        system.debug('recTypeRPEHOAgreement=='+recTypeRPEHOAgreement);

        objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        
    }

    @isTest static void testWithEHOtrue() {
        init();
        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        listCreateBookingUnit[0].Early_Handover__c = true;
        insert listCreateBookingUnit;

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPEHOAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        insert objCase;

        Task taskObj = TestDataFactory_CRM.createTask(objCase, Label.TaskEHOCompletion, 'CRE', 'Rental Pool Agreement Early Handover', date.today());
        taskObj.OwnerId = UserInfo.getUserId();
        insert taskObj;

        test.StartTest();
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolEHOCheckController objEhoCheck = new RentalPoolEHOCheckController(stdController);
        objEhoCheck.caseId = objCase.Id;
        objEhoCheck.checkEHOStatus();
        test.StopTest();
    }

    @isTest static void testWithEHOfalse() {
        init();
        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        listCreateBookingUnit[0].Early_Handover__c = false;
        insert listCreateBookingUnit;

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPEHOAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        insert objCase;

        Task taskObj = TestDataFactory_CRM.createTask(objCase, Label.TaskEHOCompletion, 'CRE', 'Rental Pool Agreement Early Handover', date.today());
        taskObj.OwnerId = UserInfo.getUserId();
        insert taskObj;

        test.StartTest();
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolEHOCheckController objEhoCheck = new RentalPoolEHOCheckController(stdController);
        objEhoCheck.caseId = objCase.Id;
        objEhoCheck.checkEHOStatus();
        test.StopTest();
    }

     @isTest static void testWithEHOCasetrue() {
        init();
        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        listCreateBookingUnit[0].Early_Handover__c = true;
        insert listCreateBookingUnit;

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPEHOAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Early_Handover__c = true;
        insert objCase;

        Task taskObj = TestDataFactory_CRM.createTask(objCase, Label.TaskEHOCompletion, 'CRE', 'Rental Pool Agreement Early Handover', date.today());
        taskObj.OwnerId = UserInfo.getUserId();
        insert taskObj;

        test.StartTest();
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolEHOCheckController objEhoCheck = new RentalPoolEHOCheckController(stdController);
        objEhoCheck.caseId = objCase.Id;
        objEhoCheck.checkEHOStatus();
        test.StopTest();
    }

    @isTest static void testWithEHONoBookingUnit() {
        init();
        /*list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        listCreateBookingUnit[0].Early_Handover__c = true;
        insert listCreateBookingUnit;*/

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPEHOAgreement);
        objCase.AccountId = objAcc.Id;
        //objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        //objCase.Early_Handover__c = true;
        insert objCase;

        Task taskObj = TestDataFactory_CRM.createTask(objCase, Label.TaskEHOCompletion, 'CRE', 'Rental Pool Agreement Early Handover', date.today());
        taskObj.OwnerId = UserInfo.getUserId();
        insert taskObj;

        test.StartTest();
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolEHOCheckController objEhoCheck = new RentalPoolEHOCheckController(stdController);
        objEhoCheck.caseId = objCase.Id;
        objEhoCheck.checkEHOStatus();
        test.StopTest();
    }
}