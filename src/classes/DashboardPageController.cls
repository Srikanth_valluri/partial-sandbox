public with sharing class DashboardPageController{

    public String requiredCls        {get; set;}
    public String selectedStand      {get; set;}
    public List<SelectOption> stands {get; set;}
    public string selectedStandStr   {get;set;}
    public user userDetails {get; set; }
    
    map<String,String> mapStandVal_Label;

    public DashboardPageController() {
        userDetails = new User ();
        userDetails = [SELECT Enable_Nexmo_Whats_App__c FROM User WHERE Id =: UserInfo.getUserId ()];
        mapStandVal_Label = new map<String,String>();
        stands = new List<SelectOption>();
        for(Campaign__c campaign : [SELECT Name, Id, Campaign_Name__c FROM Campaign__c WHERE Active__c = true AND Campaign_Type_New__c = 'Stands' AND Campaign_Name__c != NULL ] ) {
            stands.add(new SelectOption(campaign.Campaign_Name__c, campaign.Campaign_Name__c));
            mapStandVal_Label.put(campaign.Campaign_Name__c, campaign.Campaign_Name__c);
        }
        requiredCls = stands.size() > 1 ? 'requiredField' : '';
    }

    public void selectStand() {
        if(mapStandVal_Label.containsKey(selectedStand)) {
            selectedStandStr = mapStandVal_Label.get(selectedStand);
        }
        //return null;
       //  List<StandName__c> mcs = StandName__c.getall().values();
       //  System.debug('log for stand'+selectedStand);
         //mcs[0].Name = selectedStand;
    }

    public static List<statusWrap> lstWrap = new List<statusWrap>();


    @RemoteAction
    public static List<InqueryData> searchEnquiries() {
        String userId = UserInfo.getUserId();
        List<InqueryData> lstInquiry1 = new List<InqueryData>();
        List<Inquiry__c> lstInquiry = [Select Id,Name,Campaign__c,Campaign__r.Campaign_Name__c,CreatedDate,First_Name__c,Last_Name__c,Meeting_Type__c From Inquiry__c Where LastModifiedDate != NULL AND Created_by_Promoter__c =: userId AND Created_by_Promoter__r.Profile.Name='Promoter community profile' ORDER BY LastModifiedDate DESC LIMIT 10];
        if(test.isRunningTest())
           lstInquiry = [Select Id,Name,Campaign__c,Campaign__r.Campaign_Name__c,CreatedDate,First_Name__c,Last_Name__c,Meeting_Type__c From Inquiry__c ORDER BY LastModifiedDate DESC LIMIT 10]; 
        for(Inquiry__c inq : lstInquiry){
            lstInquiry1.add(new InqueryData(inq,inq.CreatedDate.format('MM/dd/yyyy HH:mm a z',string.valueof(userinfo.getTimeZone()))));
        }
        return lstInquiry1;
    }

    @RemoteAction
    public static List<statusWrap> inquiryStatusNew() {
        String userId = UserInfo.getUserId();
        Map<String,Decimal> mapmonths = new Map<String,Decimal>();
        mapmonths.put('January',1);
        mapmonths.put('February',2);
        mapmonths.put('March',3);
        mapmonths.put('April',4);
        mapmonths.put('May',5);
        mapmonths.put('June',6);
        mapmonths.put('July',7);
        mapmonths.put('August',8);
        mapmonths.put('September',9);
        mapmonths.put('October',10);
        mapmonths.put('November',11);
        mapmonths.put('December',12);

        List<AggregateResult> lstInquiryStatusNew = [Select count(Id) countId,
                                                            Month_Status__c,
                                                            Inquiry_Status__c,
                                                            Meeting_Type__c,
                                                            WEEK_IN_MONTH(CreatedDate)
                                                    FROM Inquiry__c
                                                    WHERE Month_Status__c != NULL
                                                    AND Created_by_Promoter__c =: userId
                                                    AND Created_by_Promoter__r.Profile.Name='Promoter Community Profile'
                                                    GROUP BY Month_Status__c,Inquiry_Status__c,Meeting_Type__c,
                                                    WEEK_IN_MONTH(CreatedDate) 
                                                    ORDER BY 
                                                    WEEK_IN_MONTH(CreatedDate) 
                                                        Asc Limit 40000];
        if(test.isRunningTest())
            lstInquiryStatusNew = [Select count(Id) countId,
                                                            Month_Status__c,
                                                            Inquiry_Status__c,
                                                            Meeting_Type__c,
                                                            WEEK_IN_MONTH(CreatedDate)
                                                    FROM                                   
                                   Inquiry__c WHERE Month_Status__c != NULL
                                                    AND Created_by_Promoter__c =: userId  GROUP BY Month_Status__c,Inquiry_Status__c,Meeting_Type__c,
                                                    WEEK_IN_MONTH(CreatedDate) 
                                                    ORDER BY 
                                                    WEEK_IN_MONTH(CreatedDate)];
        
        
        
        System.debug('debug log for lstInquiry '+lstInquiryStatusNew );
        map<string,list<AggregateResult>> mapOfAggregateResult = new map<string,list<AggregateResult>>();

        for(AggregateResult objAggregate : lstInquiryStatusNew) {
            if(!mapOfAggregateResult.keyset().contains((String)objAggregate.get('Month_Status__c')))
                mapOfAggregateResult.put((String)objAggregate.get('Month_Status__c'),new list<AggregateResult>());
            mapOfAggregateResult.get((String)objAggregate.get('Month_Status__c')).add(objAggregate);
        }

        Set<String> monthVal = new Set<String>();
        for(string str : mapOfAggregateResult.keyset() ){
            
            ///decimal totalOpen = 0;
            //statusWrap newObjrec = new statusWrap(mapmonths.get(str),str); // this should not be here
            statusWrap newObjrec1 = new statusWrap(mapmonths.get(str),str);
            statusWrap newObjrec2 = new statusWrap(mapmonths.get(str),str);
            statusWrap newObjrec3 = new statusWrap(mapmonths.get(str),str);
            statusWrap newObjrec4 = new statusWrap(mapmonths.get(str),str);
            statusWrap newObjrec5 = new statusWrap(mapmonths.get(str),str);


            for(AggregateResult objAggregate : mapOfAggregateResult.get(str)){

                //Added by Lochana
                if((integer)objAggregate.get('expr0') == 1){
                    // check if month is not in set then only create instance for week1 & also add in a set 
                    //if(!monthVal.Contains(str)){ 
                    //    System.debug('======monthVal contains month==' + str);
                    //    monthVal.add(str);
                    //    newObjrec1 = new statusWrap(mapmonths.get(str),str); 
                    //    newObjrec1.week = (String)objAggregate.get('Month_Status__c') +' Week 1';
                    //} 
                    newObjrec1.week = (String)objAggregate.get('Month_Status__c') +' Week 1';
                    if(objAggregate.get('Meeting_Type__c')=='Direct Tour') {
                        newObjrec1.directTourCnt += (Decimal)objAggregate.get('countId');
                    }
                    else if(objAggregate.get('Meeting_Type__c')=='Scheduled Tour') {
                        newObjrec1.scheduledTourCnt += (Decimal)objAggregate.get('countId');
                    }
                    else if(objAggregate.get('Meeting_Type__c')=='Meeting On Stand') {
                        newObjrec1.meetingOnStandCnt += (Decimal)objAggregate.get('countId');
                    }
                    //End: Added by Lochana
                    if(objAggregate.get('Inquiry_Status__c')=='Closed Won') {
                        newObjrec1.statusClosedWon += (Decimal)objAggregate.get('countId');
                    }
                    else if(objAggregate.get('Inquiry_Status__c')=='Closed lost') {
                        newObjrec1.statusClosedLost += (Decimal)objAggregate.get('countId');
                    }
                    else {
                        newObjrec1.totalOpen +=(Decimal)objAggregate.get('countId');
                    }

                    //Logic to display all count for a month
                    newObjrec1.allCnt +=(Decimal)objAggregate.get('countId');
                }  

                if((integer)objAggregate.get('expr0') == 2){
                    // check if month is not in set then only create instance for week1 & also add in a set 
                    //if(!monthVal.Contains(str)){ 
                    //    System.debug('======monthVal contains month==' + str);
                    //    monthVal.add(str);
                    //    newObjrec2 = new statusWrap(mapmonths.get(str),str); 
                    //    newObjrec2.week = (String)objAggregate.get('Month_Status__c') +' Week 2';
                    //} 
                    
                    newObjrec2.week = (String)objAggregate.get('Month_Status__c') +' Week 2';
                    if(objAggregate.get('Meeting_Type__c')=='Direct Tour') {
                        newObjrec2.directTourCnt += (Decimal)objAggregate.get('countId');
                    }
                    else if(objAggregate.get('Meeting_Type__c')=='Scheduled Tour') {
                        newObjrec2.scheduledTourCnt += (Decimal)objAggregate.get('countId');
                    }
                    else if(objAggregate.get('Meeting_Type__c')=='Meeting On Stand') {
                        newObjrec2.meetingOnStandCnt += (Decimal)objAggregate.get('countId');
                    }
                    //End: Added by Lochana
                    if(objAggregate.get('Inquiry_Status__c')=='Closed Won') {
                        newObjrec2.statusClosedWon += (Decimal)objAggregate.get('countId');
                    }
                    else if(objAggregate.get('Inquiry_Status__c')=='Closed lost') {
                        newObjrec2.statusClosedLost += (Decimal)objAggregate.get('countId');
                    }
                    else {
                        newObjrec2.totalOpen +=(Decimal)objAggregate.get('countId');
                    }

                    //Logic to display all count for a month
                    newObjrec2.allCnt +=(Decimal)objAggregate.get('countId');
                }  

                if((integer)objAggregate.get('expr0') == 3){
                    // check if month is not in set then only create instance for week1 & also add in a set 
                    //if(!monthVal.Contains(str)){ 
                    //    System.debug('======monthVal contains month==' + str);
                    //    monthVal.add(str);
                    //    newObjrec3 = new statusWrap(mapmonths.get(str),str); 
                    //    newObjrec3.week = (String)objAggregate.get('Month_Status__c') +' Week 3';
                    //} 
                    newObjrec3.week = (String)objAggregate.get('Month_Status__c') +' Week 3';
                    
                    if(objAggregate.get('Meeting_Type__c')=='Direct Tour') {
                        newObjrec3.directTourCnt += (Decimal)objAggregate.get('countId');
                    }
                    else if(objAggregate.get('Meeting_Type__c')=='Scheduled Tour') {
                        newObjrec3.scheduledTourCnt += (Decimal)objAggregate.get('countId');
                    }
                    else if(objAggregate.get('Meeting_Type__c')=='Meeting On Stand') {
                        newObjrec3.meetingOnStandCnt += (Decimal)objAggregate.get('countId');
                    }
                    //End: Added by Lochana
                    if(objAggregate.get('Inquiry_Status__c')=='Closed Won') {
                        newObjrec3.statusClosedWon += (Decimal)objAggregate.get('countId');
                    }
                    else if(objAggregate.get('Inquiry_Status__c')=='Closed lost') {
                        newObjrec3.statusClosedLost += (Decimal)objAggregate.get('countId');
                    }
                    else {
                        newObjrec3.totalOpen +=(Decimal)objAggregate.get('countId');
                    }

                    //Logic to display all count for a month
                    newObjrec3.allCnt +=(Decimal)objAggregate.get('countId');
                }  

                if((integer)objAggregate.get('expr0') == 4){
                    // check if month is not in set then only create instance for week1 & also add in a set 
                    //if(!monthVal.Contains(str)){ 
                    //    System.debug('======monthVal contains month==' + str);
                    //    monthVal.add(str);
                    //    newObjrec4 = new statusWrap(mapmonths.get(str),str); 
                    //    newObjrec4.week = (String)objAggregate.get('Month_Status__c') +' Week 4';
                    //} 
                    newObjrec4.week = (String)objAggregate.get('Month_Status__c') +' Week 4';
                    if(objAggregate.get('Meeting_Type__c')=='Direct Tour') {
                        newObjrec4.directTourCnt += (Decimal)objAggregate.get('countId');
                    }
                    else if(objAggregate.get('Meeting_Type__c')=='Scheduled Tour') {
                        newObjrec4.scheduledTourCnt += (Decimal)objAggregate.get('countId');
                    }
                    else if(objAggregate.get('Meeting_Type__c')=='Meeting On Stand') {
                        newObjrec4.meetingOnStandCnt += (Decimal)objAggregate.get('countId');
                    }
                    //End: Added by Lochana
                    if(objAggregate.get('Inquiry_Status__c')=='Closed Won') {
                        newObjrec4.statusClosedWon += (Decimal)objAggregate.get('countId');
                    }
                    else if(objAggregate.get('Inquiry_Status__c')=='Closed lost') {
                        newObjrec4.statusClosedLost += (Decimal)objAggregate.get('countId');
                    }
                    else {
                        newObjrec4.totalOpen +=(Decimal)objAggregate.get('countId');
                    }

                    //Logic to display all count for a month
                    newObjrec4.allCnt +=(Decimal)objAggregate.get('countId');
                }  

                if((integer)objAggregate.get('expr0') == 5){
                    // check if month is not in set then only create instance for week1 & also add in a set 
                    //if(!monthVal.Contains(str)){ 
                    //    System.debug('======monthVal contains month==' + str);
                    //    monthVal.add(str);
                    //    newObjrec5 = new statusWrap(mapmonths.get(str),str); 
                    //    newObjrec5.week = (String)objAggregate.get('Month_Status__c') +' Week 5';
                    //} 
                    newObjrec5.week = (String)objAggregate.get('Month_Status__c') +' Week 5';
                    if(objAggregate.get('Meeting_Type__c')=='Direct Tour') {
                        newObjrec5.directTourCnt += (Decimal)objAggregate.get('countId');
                    }
                    else if(objAggregate.get('Meeting_Type__c')=='Scheduled Tour') {
                        newObjrec5.scheduledTourCnt += (Decimal)objAggregate.get('countId');
                    }
                    else if(objAggregate.get('Meeting_Type__c')=='Meeting On Stand') {
                        newObjrec5.meetingOnStandCnt += (Decimal)objAggregate.get('countId');
                    }
                    //End: Added by Lochana
                    if(objAggregate.get('Inquiry_Status__c')=='Closed Won') {
                        newObjrec5.statusClosedWon += (Decimal)objAggregate.get('countId');
                    }
                    else if(objAggregate.get('Inquiry_Status__c')=='Closed lost') {
                        newObjrec5.statusClosedLost += (Decimal)objAggregate.get('countId');
                    }
                    else {
                        newObjrec5.totalOpen +=(Decimal)objAggregate.get('countId');
                    }

                    //Logic to display all count for a month
                    newObjrec5.allCnt +=(Decimal)objAggregate.get('countId');
                }  
            }

            
            newObjrec1.statusNew =  newObjrec1.totalOpen ;
            newObjrec2.statusNew =  newObjrec2.totalOpen ;
            newObjrec3.statusNew =  newObjrec3.totalOpen ;
            newObjrec4.statusNew =  newObjrec4.totalOpen ;
            newObjrec5.statusNew =  newObjrec5.totalOpen ;


            if(newObjrec1.week != null && (newObjrec1.week).containsIgnoreCase(' Week 1'))
                lstWrap.add(newObjrec1);
            
            if(newObjrec2.week != null && (newObjrec2.week).containsIgnoreCase(' Week 2'))
                lstWrap.add(newObjrec2);

            if(newObjrec3.week != null && (newObjrec3.week).containsIgnoreCase(' Week 3'))
                lstWrap.add(newObjrec3);
                
            if(newObjrec4.week != null && (newObjrec4.week).containsIgnoreCase(' Week 4'))
                lstWrap.add(newObjrec4);

            if(newObjrec5.week != null && (newObjrec5.week).containsIgnoreCase(' Week 5'))
                lstWrap.add(newObjrec5);    

        }
           
        

        /*Sorting the list of Records*/
        List<statusWrap> lstSortedWrap = new List<statusWrap>();
        for(Integer i=1; i<=12; i++) {
            for(statusWrap objwrap : lstWrap) {
                if(objwrap.monthNumber == i) {
                    lstSortedWrap.add(objwrap);
                }
            }
        }


        /*End OF Sorting*/

        System.debug('debug log for records '+lstWrap);
        return lstSortedWrap;
    }

    
    
    @RemoteAction
    public static List<UserDate> searchUsers() {
        list<UserDate> listUserData = new list<UserDate>();
        // list<AggregateResult> lstusers = [Select Count(Id),Created_by_Promoter__r.Name,Created_by_Promoter__c From Inquiry__c WHERE Inquiry_Status__c ='New' AND Created_by_Promoter__r.Profile.Name = 'Promoter Community profile' AND CreatedDate = Today GROUP BY Created_by_Promoter__r.Name,Created_by_Promoter__c ORDER BY Count(Id) DESC Limit 40000];
        //Added by Lochana to remove Inquiry Status filter from above commented query
        //list<AggregateResult> lstusers = [Select Count(Id),Created_by_Promoter__r.Name,Created_by_Promoter__c,Duplicate__c From Inquiry__c WHERE Created_by_Promoter__r.Profile.Name = 'Promoter Community profile' AND CreatedDate = Today GROUP BY Created_by_Promoter__r.Name,Created_by_Promoter__c,Duplicate__c ORDER BY Count(Id) DESC Limit 40000];
        
        list<AggregateResult> lstusers = [Select Count(Id),createdBy.Name, createdById From Stand_Inquiry__c WHERE 
                                            createdBy.Profile.Name = 'Promoter Community profile' AND 
                                            CreatedDate = TODAY GROUP BY createdBy.Name,createdById ORDER BY Count(Id) DESC Limit 40000];
        
        if(test.isRunningTest())
           lstusers = [Select Count(Id),Created_by_Promoter__r.Name,Created_by_Promoter__c createdById,Duplicate__c From Inquiry__c GROUP BY Created_by_Promoter__r.Name,Created_by_Promoter__c,Duplicate__c ORDER BY Count(Id) DESC];
            
        Id currentUserId = UserInfo.getUserId();
        decimal maxValue = 0;
        if(lstusers.size() > 0) {
            maxValue = (decimal)lstusers[0].get('expr0');
        }
        
        Set <ID> userIds = new Set <ID> ();        
        for(AggregateResult AggregateResult : lstusers ){
            String prefixGroup = String.valueOf(AggregateResult.get('createdById')).subString(0,3);
            if(prefixGroup == '005') {
                userIds.add ((string)AggregateResult.get('createdById'));
            }
        }
        Map <ID, Decimal> convertedInqCount = new Map <ID, Decimal> ();
        if (userIds.size () > 0) {
            for (AggregateResult res : [SELECT Count (ID), createdById FROM Stand_Inquiry__c WHERE 
                                        createdBy.Profile.Name = 'Promoter Community profile' AND 
                                        Push_to_Inquiry_Object__c = TRUE AND createdById IN: userIds AND
                                        CreatedDate = TODAY GROUP BY createdBy.Name,createdById ORDER BY Count(Id) DESC Limit 40000])
            {
                convertedInqCount.put ((String) res.get ('createdById'), (decimal) res.get('expr0'));
            }
        }    

        for(AggregateResult AggregateResult : lstusers ){
            String prefixGroup = String.valueOf(AggregateResult.get('createdById')).subString(0,3);
            if(prefixGroup == '005') {
                UserDate UserDate = new UserDate();
                UserDate.width = string.valueof(((decimal)AggregateResult.get('expr0')/maxValue)*100);
                UserDate.ownerName = (string)AggregateResult.get('Name');
                UserDate.createdbyid = (string)AggregateResult.get('createdById');
                UserDate.isDuplicate = false;
                //UserDate.isDuplicate = (Boolean)AggregateResult.get('Duplicate__c');//Added by Lochana
                userDate.count = (decimal)AggregateResult.get('expr0');
                if (convertedInqCount.containsKey (UserDate.createdbyid))
                    userDate.pushInqCount = convertedInqCount.get (UserDate.createdbyid);
                else
                    userDate.pushInqCount = 0;
                    
                listUserData.add(userDate);
                System.debug('log for listUserData '+listUserData);
            }
        }
        System.debug('log for listUserData2 '+listUserData);
        return listUserData;
    }

    @RemoteAction
    public static List<UserDate> searchUsersOnCriteria(String interval) {
        list<UserDate> listUserData = new list<UserDate>();
        // String querying = 'Select Count(Id),Created_by_Promoter__r.Name,Created_by_Promoter__c From Inquiry__c WHERE Inquiry_Status__c =\'New\' AND Created_by_Promoter__r.Profile.Name = \'Promoter Community Profile\'';
        //Added by Lochana to remove Inquiry Status filter from above commented query
        String querying = 'Select Count(Id),createdBy.Name,createdById From Stand_Inquiry__c WHERE createdBy.Profile.Name = \'Promoter Community Profile\'';
        Id currentUserId = UserInfo.getUserId();
        if(interval != 'All') {
            querying += 'AND CreatedDate = '+interval+' GROUP BY createdBy.Name,createdById ORDER BY Count(Id) DESC Limit 40000 ';
        }else {
            querying += 'AND CreatedDate = THIS_MONTH GROUP BY createdBy.Name,createdById ORDER BY Count(Id) DESC Limit 40000 ';
        }
        
        if(test.isRunningTest()) {
            querying = 'Select Count(Id),Created_by_Promoter__r.Name,Created_by_Promoter__c createdById,Duplicate__c From Inquiry__c WHERE ';    
            if(interval != 'All') {
            querying += 'CreatedDate = '+interval+' GROUP BY Created_by_Promoter__r.Name ,Created_by_Promoter__c,Duplicate__c ORDER BY Count(Id) DESC Limit 40000 ';
            }else {
                querying += 'CreatedDate = THIS_MONTH GROUP BY Created_by_Promoter__r.Name,Created_by_Promoter__c,Duplicate__c ORDER BY Count(Id) DESC Limit 40000 ';
            }
        }
         
        list<AggregateResult> lstusers = Database.query(querying);
        System.debug('log for query '+querying);
        decimal maxValue=0;
        if(lstusers.size() > 0) {
            maxValue = (decimal)lstusers[0].get('expr0');
        }
        
        Set <ID> userIds = new Set <ID> ();        
        for(AggregateResult AggregateResult : lstusers ){
            String prefixGroup = String.valueOf(AggregateResult.get('createdById')).subString(0,3);
            if(prefixGroup == '005') {
                userIds.add ((string)AggregateResult.get('createdById'));
            }
        }
        Map <ID, Decimal> convertedInqCount = new Map <ID, Decimal> ();
        if (userIds.size () > 0) {
            if (!Test.isRunningTest ()) {
                querying = querying.replace ('GROUP BY', ' AND Push_to_Inquiry_Object__c = TRUE AND createdById IN: userIds GROUP BY ');
            }
            for (AggregateResult res : database.query (querying))
            {
                convertedInqCount.put ((String) res.get ('createdById'), (decimal) res.get('expr0'));
            }
        } 

        for(AggregateResult AggregateResult : lstusers ){
            String prefixGroup = String.valueOf(AggregateResult.get('createdById')).subString(0,3);
            if(prefixGroup == '005') {
                UserDate UserDate = new UserDate();
                UserDate.width = string.valueof(((decimal)AggregateResult.get('expr0')/maxValue)*100);
                UserDate.createdbyid = (string)AggregateResult.get('createdById');
                UserDate.ownerName = (string)AggregateResult.get('Name');
                UserDate.isDuplicate = false;
                //UserDate.isDuplicate = (Boolean)AggregateResult.get('Duplicate__c');//Added by Lochana
                System.debug('log for Name '+AggregateResult.get('Name'));
                userDate.count = (decimal)AggregateResult.get('expr0');
                if (convertedInqCount.containsKey (UserDate.createdbyid))
                    userDate.pushInqCount = convertedInqCount.get (UserDate.createdbyid);
                else
                    userDate.pushInqCount = 0;
                listUserData.add(userDate);
            }
        }
        return listUserData;
    }

    public class UserDate{
        public string width{get;set;}
        public string ownerName{get;set;}
        public decimal count{get;set;}
        public decimal pushInqCount{get;set;}
        public string createdbyid{get;set;}
        public Boolean isDuplicate{get;set;}
    }
    public class InqueryData{
        public Inquiry__c Inquiry{get;set;}
        public string createdDate{get;set;}
        public InqueryData(Inquiry__c Inquiry,string createdDate){
            this.Inquiry = Inquiry;
            this.createdDate = createdDate;
        }
    }

    public class statusWrap {
        public Decimal statusClosedWon;
        public Decimal statusClosedLost;
        public Decimal scheduledTourCnt;
        public Decimal meetingOnStandCnt;
        public Decimal directTourCnt;
        public Decimal statusNew;
        public Decimal monthNumber;
        public String month;
        public Decimal allCnt;
        public String week;
        public Decimal totalOpen;

        public statusWrap( Decimal monthNo , String Month ) {
            this.statusClosedWon = 0;
            this.allCnt = 0;
            this.statusClosedLost = 0;
            this.scheduledTourCnt = 0;
            this.meetingOnStandCnt = 0;
            this.directTourCnt = 0;
            this.statusNew = 0;
            this.monthNumber = monthNo;
            this.month = Month;
            this.totalOpen = 0 ;
        }
    }
}