/****************************************************************************************
 * Class : ShareFilesWithCommunityUsersHandler
 * Created By : Twinkle P
 -----------------------------------------------------------------------------------
 * Description : Handler for Trigger ShareFilesWithCommunityUsers
 -----------------------------------------------------------------------------------
 * Version History:
 * Version    Developer Name    Date          Detail Features
   1.0        Twinkle P         07/05/2018    Initial Development
 **********************************************************************************/
public with sharing class ShareFilesWithCommunityUsersHandler {
    private static String prefix = getObjectPrefixID('Announcement__c');
    /**
     * Method to share the Files with all Users
     *
     * @param: cdls : Content Document List
     *
     * @return : NA
     */
    public static void shareFiles(List<ContentDocumentLink> cdls) {
    
            for(ContentDocumentLink cdl: cdls){
                Id recordID = cdl.LinkedEntityId;
                String keyPrefix = String.valueOf(recordID).substring(0,3);
                system.debug('prefix----------------'+prefix);
                system.debug('keyPrefix----------------'+keyPrefix);
                system.debug('LinkedEntityId----------------'+cdl.LinkedEntityId);
                if( prefix.equalsIgnoreCase(keyPrefix)) {
                    cdl.ShareType = 'I';
                    cdl.Visibility = 'AllUsers';
                }
            }
    }

    /**
     * Method to fetch the Prefix of the SObject
     *
     * @param: objName : Object Name for the Prefix to be fetched
     *
     * @return: Prefix Value
     */
    private static String  getObjectPrefixID(String objName){
        Map<String, Schema.SObjectType> globalDescribeMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType sObjType = globalDescribeMap.get(objName) ;
        Schema.DescribeSObjectResult descResult = sObjType.getDescribe() ;
        String keyPrefix = descResult.getKeyPrefix();
        return keyPrefix;
    }
}