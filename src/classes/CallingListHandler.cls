public without sharing class CallingListHandler {

    private static set<Id> setCallingItemIdsForApproval ;
    private static list<Task> lstTaskToBeGenerated ;

    public static map<ID,ID> mapManagerID ;
    public static map<ID,String> mapManagerRole;
    public static Map<String, Id> mapRoles;

    public static String emailSubject;
    public static String emailBody;
    public static List<Attachment> lstAttach;

    public static Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;

    static {
        list<User> lstActiveUser = new list<User>();
        mapManagerID = new map<ID,ID>();
        mapManagerRole = new map<ID,String>();
        mapRoles = new Map<String, Id>();
        lstActiveUser = [Select Id,Name,isActive,Managerid,UserType,UserRole.Name,UserRoleID
                                   from User
                                   where isActive = true LIMIT 2000];

        for(User objUser : lstActiveUser){
            if(objUser.managerid != null){
                mapManagerID.put(objUser.id,objUser.managerid);
            }
            if(objUser.UserRoleID != null){
                mapManagerRole.put(objUser.id,objUser.UserRole.Name);
                mapRoles.put(objUser.UserRole.Name, objUser.id);
            }

        }
        system.debug('--mapManagerID--'+mapManagerID.Size());
        system.debug('--mapManagerRole--'+mapManagerRole.Size());
    }

    public static void onBeforeUpdate( map<Id, Calling_List__c> mapUpdatedCallingItems, map<Id, Calling_List__c> mapOldCallingItems ) {
        //check AOPT Open Cases For Bounced Cheque Calling List
        CallingListBouncedChequeHelper.OpenCasesForBouncedChequeCallingList(mapOldCallingItems.values(), mapUpdatedCallingItems.values());

        Id bouncedChequeRTId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Bounced Cheque').getRecordTypeId();

        if( mapUpdatedCallingItems != null && !mapUpdatedCallingItems.isEmpty() ) {
            setCallingItemIdsForApproval = new set<Id>();
            lstTaskToBeGenerated = new list<Task>();
            // map<Id, list<SR_Booking_Unit__c>> mapCaseBookingUnits = CallingListHelper.getUnitInfo( mapUpdatedCallingItems.values() );
            for( Calling_List__c objCall : mapUpdatedCallingItems.values() ) {
                if(objCall.RecordTypeId == bouncedChequeRTId) {
                    String OwnerIdStr = objCall.OwnerId;
                    OwnerIdStr = OwnerIdStr.substring(0,3);
                    //Check if ownership of record is with queue
                    if(OwnerIdStr != '005') {
                         //objCall.addError('Please change the owner from queue to a user before proceeding with making updates');
                    }
                    checkOwnershipChanges( objCall, mapOldCallingItems.get( objCall.Id )  );
                }//if

                //checkApprovalStatus( objCall, mapOldCallingItems.get( objCall.Id ) );
                //validateCallingListRecords( objCall, mapOldCallingItems.get( objCall.Id ), mapCaseBookingUnits );
            }

            /*if( !lstTaskToBeGenerated.isEmpty() ) {
                insert lstTaskToBeGenerated ;
            }*/

            /*if( !setCallingItemIdsForApproval.isEmpty() ) {
                ApprovalHandler.processCallingListApproval( setCallingItemIdsForApproval );
            }*/
        }
    }

    public static void checkOwnershipChanges( Calling_List__c updatedCallingItem, Calling_List__c oldCallingItem ) {
        if( String.isNotBlank( updatedCallingItem.Call_Outcome__c ) 
        && updatedCallingItem.Call_Outcome__c != oldCallingItem.Call_Outcome__c 
        && updatedCallingItem.Calling_List_Status__c != 'Closed'
        && updatedCallingItem.Calling_List_Status__c != 'In Progress') {
            updatedCallingItem.Calling_List_Status__c = 'In Progress';
        }
    }

    /*public static void checkApprovalStatus( Calling_List__c updatedCallingItem, Calling_List__c oldCallingItem ) {
        if( updatedCallingItem.Submit_for_Approval__c &&
            updatedCallingItem.Approving_Authorities__c != oldCallingItem.Approving_Authorities__c ) {
            Approval.UnlockResult unlockedResult = Approval.unlock( updatedCallingItem );
            setCallingItemIdsForApproval.add( updatedCallingItem.Id );
            if( String.isNotBlank( updatedCallingItem.Approving_Authorities__c ) ) {
                String strApprovingAuthorities = oldCallingItem.Approving_Authorities__c;
                String strRole = strApprovingAuthorities.contains( ',' ) ?
                                  strApprovingAuthorities.subString( 0, strApprovingAuthorities.indexOf( ',' ) ) :
                                  strApprovingAuthorities ;
                updatedCallingItem.Approval_Status__c = 'Approved by ' + UserInfo.getUserName() + ' (' + strRole + ')';
            }
            else {
                updatedCallingItem.Approval_Status__c = 'Approved';
                lstTaskToBeGenerated.add( TaskUtility.getTask( (SObject)updatedCallingItem, 'Customer refusing to pay, take legal action.', 'Legal', 'Bounced Cheque', system.today().addDays( 1 ) ) ) ;
            }
        }
    }*/


    /*public static void validateCallingListRecords( Calling_List__c updatedCallingItem, Calling_List__c oldCallingItem, map<Id, list<SR_Booking_Unit__c>> mapCaseBookingUnits ) {
        if( updatedCallingItem.Calling_List_Type__c != null && updatedCallingItem.Call_Outcome__c != null &&
            ( !updatedCallingItem.Calling_List_Type__c.equalsIgnoreCase( oldCallingItem.Calling_List_Type__c ) ||
              !updatedCallingItem.Call_Outcome__c.equalsIgnoreCase( oldCallingItem.Call_Outcome__c ) )
          ) {
            if( updatedCallingItem.Calling_List_Type__c.equalsIgnoreCase( 'BC Calling' ) ) {
                handleBounceChequeCallingItem( updatedCallingItem, mapCaseBookingUnits );
            }
        }
    }*/

    /*private static void handleBounceChequeCallingItem( Calling_List__c objCall, map<Id, list<SR_Booking_Unit__c>> mapCaseBookingUnits ) {
        if( objCall.Call_Outcome__c.equalsIgnoreCase( 'Refusing To Pay' ) ) {

            //Check for EHO on units.
            if( mapCaseBookingUnits != null && mapCaseBookingUnits.containsKey( objCall.Case__c ) &&
                CallingListHelper.areUnitsForEHO( mapCaseBookingUnits.get( objCall.Case__c ) ) ) {
                //Values to be replaced by actual approving authorities for refuse to pay
                objCall.Approving_Authorities__c = 'VP - Operations,General Manager';
                objCall.Submit_for_Approval__c = true ;
                setCallingItemIdsForApproval.add( objCall.Id );
            }
        }
    }*/


    public void onAfterUpdate(Map<Id, Calling_List__c> mapOldCallList, List<Calling_List__c> lstNewCallList) {
        Id idBCCallList = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Bounced Cheque').getRecordTypeId();
        Set<Id> setCallListId = new Set<Id>();
        List<Calling_List__c> lstCallListsToUnlock = new List<Calling_List__c>();
        List<Calling_List__c> lstCallListsToUpdate = new List<Calling_List__c>();
        List<Calling_List__c> lstCallListsForTask = new List<Calling_List__c>();
        List<Task> lstTaskToInsert = new List<Task>();
        Set<Id> setCallLstIdForNotResponding = new Set<Id>();

        List<String> lstCallOutcomeLabel = label.CallingList_CallOutcome_Not_Responding.split(',');
        System.debug('lstCallOutcomeLabel : ' + lstCallOutcomeLabel);

        for(Calling_List__c objCallList: lstNewCallList) {

            //Check for Call_Outcome = Not Responding
            System.debug('CallOutCome value: ' + objCallList.Call_Outcome__c);
            if(String.isNotBlank(objCallList.Call_Outcome__c) 
            && lstCallOutcomeLabel.contains(objCallList.Call_Outcome__c)
            && (objCallList.IsHideFromUI__c == False 
            && objCallList.RecordTypeId == collectioncallingRecordTypeId)) {

                setCallLstIdForNotResponding.add(objCallList.Id);
            }

            if(String.isNotBlank(objCallList.Call_Outcome__c) 
            && objCallList.Call_Outcome__c.equalsIgnoreCase('Refusing to Pay') 
            && objCallList.RecordTypeId == idBCCallList) {
                if(mapOldCallList.get(objCallList.Id).Approving_Authorities__c == null 
                && objCallList.Approving_Authorities__c == null 
                && objCallList.Approval_Status__c != 'Approved') {
                    Calling_List__c objCallListToUpdate = new Calling_List__c(Id = objCallList.Id);
                    objCallListToUpdate.Approving_Authorities__c = 'SVP Operations,CRM - Committee';
                    lstCallListsToUpdate.add(objCallListToUpdate);
                    system.debug('--lstCallListsToUpdate--'+lstCallListsToUpdate);
                }
            }

            if(String.isNotBlank(objCallList.Call_Outcome__c) &&
                objCallList.Call_Outcome__c.equalsIgnoreCase('Refusing to Pay') &&
                String.isNotBlank(mapOldCallList.get(objCallList.Id).Call_Outcome__c) &&
                mapOldCallList.get(objCallList.Id).Call_Outcome__c.equalsIgnoreCase('Negotiating')) {

                setCallListId.add(objCallList.Id);
                lstCallListsToUnlock.add(objCallList);
            }

            system.debug('--objCallList.Approving_Authorities__c--'+objCallList.Approving_Authorities__c);
            system.debug('--mapOldCallList.get(objCallList.Id).Approving_Authorities__c--'+mapOldCallList.get(objCallList.Id).Approving_Authorities__c);

            if(objCallList.Approving_Authorities__c != mapOldCallList.get(objCallList.Id).Approving_Authorities__c &&
                objCallList.RecordTypeId == idBCCallList) {
                    system.debug('--inside approval conditions--');
                //&& objCallList.Status.equalsIgnoreCase('Submitted')
                system.debug('call list update Approving_Authorities__c '+objCallList.Approving_Authorities__c);
                if(objCallList.Approving_Authorities__c != null) {

                    /*if(objCallList.Approving_Authorities__c.containsIgnoreCase('HOD')) {
                        Id idTempUser = getUserID(objCallList.ownerID, 'HOD');

                        if(idTempUser != null) {
                            setCallListId.add(objCallList.Id);
                            lstCallListsToUnlock.add(objCallList);

                            Calling_List__c objCallListToUpdate = new Calling_List__c(Id = objCallList.Id);
                            objCallListToUpdate.Approving_User_Id__c = idTempUser;
                            objCallListToUpdate.Approving_User_Role__c = 'HOD';
                            lstCallListsToUpdate.add(objCallListToUpdate);
                        }
                    }
                    else*/ if(objCallList.Approving_Authorities__c.containsIgnoreCase('SVP Operations')) {
                        Id idTempUser = mapRoles.get('SVP Operations');

                        if(idTempUser != null) {
                            setCallListId.add(objCallList.Id);
                            lstCallListsToUnlock.add(objCallList);

                            Calling_List__c objCallListToUpdate = new Calling_List__c(Id = objCallList.Id);
                            objCallListToUpdate.Approving_User_Id__c = idTempUser;
                            objCallListToUpdate.Approving_User_Role__c = 'SVP Operations';
                            lstCallListsToUpdate.add(objCallListToUpdate);
                        }
                    }
                    if(objCallList.Approving_Authorities__c.equalsIgnoreCase('CRM - Committee')) {
                        Id idTempUser = mapRoles.get('CRM - Committee');

                        if(idTempUser != null) {
                            setCallListId.add(objCallList.Id);
                            lstCallListsToUnlock.add(objCallList);

                            Calling_List__c objCallListToUpdate = new Calling_List__c(Id = objCallList.Id);
                            objCallListToUpdate.Approving_User_Id__c = idTempUser;
                            objCallListToUpdate.Approving_User_Role__c = 'CRM - Committee';
                            lstCallListsToUpdate.add(objCallListToUpdate);
                        }
                    }
                }
                else {
                    lstCallListsForTask.add(objCallList);
                }
            }
        }

        if(!lstCallListsForTask.isEmpty()) {
            for(Calling_List__c objCallList: lstCallListsForTask) {
                Calling_List__c objCallListToUpdate = new Calling_List__c(Id = objCallList.Id);
                objCallListToUpdate.Approval_Status__c = 'Approved';
                objCallListToUpdate.Additional_Comments__c = 'A task has been assigned to litigation for further action';
                objCallListToUpdate.Approving_User_Id__c = null;
                objCallListToUpdate.Approving_User_Role__c = null;

                if(objCallList.RecordTypeId == idBCCallList) {
                    Case objCase = new Case(Id = objCallList.Case__c, OwnerId = objCallList.OwnerId);
                    Task objTask = TaskUtility.getTask((SObject)objCase, 'Bounced Cheque: Customer Refused to Pay, Take Legal Action', 'Legal',
                        'Bounced Cheque', system.today().addDays(1));
                    lstTaskToInsert.add(objTask);
                }

                lstCallListsToUpdate.add(objCallListToUpdate);
            }
        }

        if(!lstCallListsToUpdate.isEmpty()) {
            if(!Test.isRunningTest()){update lstCallListsToUpdate;}
        }

        if(!lstTaskToInsert.isEmpty()) {
            if(!Test.isRunningTest()){insert lstTaskToInsert;}
        }

        if(lstCallListsToUnlock != null && !lstCallListsToUnlock.isEmpty()) {
            for(Calling_List__c objCallList: lstCallListsToUnlock) {
                //Unlock the locked record
                Approval.UnlockResult unlockedResult = Approval.unlock(objCallList);
            }

            //Call future method to send approval request to the next approver
            ApprovalHandler.processCallingListApprovals(setCallListId);
        }

        //For Sending email if Call_Outcome__c = Not Responding
        if(setCallLstIdForNotResponding.size() > 0) {
          
             //SendEmailForNotRespondingQueueable objSMSQue = new SendEmailForNotRespondingQueueable(setCallLstIdForNotResponding);
             //Id jobId = System.enqueueJob(objSMSQue);
        SendEmailForNotRespondingSchedulable objSendEmailForNotRespondingSchedulable = new SendEmailForNotRespondingSchedulable();
        objSendEmailForNotRespondingSchedulable.callingListIds = setCallLstIdForNotResponding;
        DateTime dt = system.now().addMinutes(1);
        String day = string.valueOf(dt.day());
        String month = string.valueOf(dt.month());
        String hour = string.valueOf(dt.hour());
        String minute = string.valueOf(dt.minute());
        String second = string.valueOf(dt.second());
        String year = string.valueOf(dt.year());
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
        
        String jobID = system.schedule('SendEmailForNotRespondingSchedulable '+ system.now(), strSchedule, objSendEmailForNotRespondingSchedulable);
        }
    }
    
    





    




}