/**
 * Ver       Date            Author      		    Modification
 * 1.0    9/30/2019         Arsh Dave              Initial Version  
 * 2.0    10/08/2019        Arsh Dave              Updated the fields to random fields as new fields cannot be deployet on production  
**/
@isTest
public class TaskEscalationCustomerSRBatchTest{

    @testSetup static void setup(){
        
        UserRole objRole = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert objRole;
        System.debug('-->> objRole : ' + objRole );

        User objUserManager = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'Muser000@amamama.com',
            Username = 'Muser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias_M',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = objRole.Id
        );
        insert objUserManager;
        System.debug('-->> objUserManager : ' + objUserManager );

        User objUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = objRole.Id,
            ManagerId = objUserManager.Id
        );
        insert objUser;
        System.debug('-->> objUser : ' + objUser );
    }

    static testMethod  void callConstructor() {
        
        User objUser = [SELECT Id, Email FROM User WHERE Email = 'puser000@amamama.com'];
        System.debug('-->> objUser ## : ' + objUser );

        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        objAccount.OwnerId = objUser.Id;
        insert objAccount;
        System.debug('-->> objAccount ## : ' + objAccount );

        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
        insert objBooking;
        
        Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
        insert objBooking_Unit;

        FM_Case__c objFMCase = new FM_Case__c(
            Account__c = objAccount.Id,
            Category_GE__c = 'Change of Contact Details',
            Subcategory_GE__c = 'Change registered email id',
            RecordTypeId = System.Label.FM_Case_General_Enquiry
        );
        insert objFMCase;
        System.debug('-->> objFMCase ## : ' + objFMCase );

        List<Task> objTask = [
            SELECT Id
                , Status
                , ActivityDate
            FROM Task
            WHERE What.Id = :objFMCase.Id   
        ];
        System.debug('-->> objTask ## : ' + objTask );

        objTask[0].ActivityDate = System.Today();
        update objTask;

        Test.startTest();
            
            TaskEscalationCustomerSRProcessBatch objTaskEscalationCustomerSRProcessBatch = new TaskEscalationCustomerSRProcessBatch();
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            Database.executebatch(objTaskEscalationCustomerSRProcessBatch,1);
            
        Test.stopTest();
        
    }

    // static testMethod  void callBulkSameUser() {
        
    //     User objUser = [SELECT Id, Email FROM User WHERE Email = 'puser000@amamama.com'];
    //     System.debug('-->> objUser ## : ' + objUser );

    //     Account objAccount = TestDataFactoryFM.createAccount();
    //     objAccount.Email__c = 'test@test.com'; 
    //     objAccount.OwnerId = objUser.Id;
    //     insert objAccount;
    //     System.debug('-->> objAccount ## : ' + objAccount );

    //     NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
    //     insert objSR;

    //     Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
    //     insert objBooking;
        
    //     Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
    //     insert objBooking_Unit;

    //     FM_Case__c objFMCase = new FM_Case__c(
    //         Account__c = objAccount.Id,
    //         Category_GE__c = 'Change of Contact Details',
    //         Subcategory_GE__c = 'Change registered email id',
    //         RecordTypeId = System.Label.FM_Case_General_Enquiry
    //     );
    //     insert objFMCase;
    //     System.debug('-->> objFMCase ## : ' + objFMCase );

    //     List<Task> objTask = [
    //         SELECT Id
    //             , Status
    //             , ActivityDate
    //         FROM Task
    //         WHERE What.Id = :objFMCase.Id   
    //     ];
    //     System.debug('-->> objTask ## : ' + objTask );

    //     objTask[0].ActivityDate = System.Today();
    //     update objTask;

    //     List<FM_Case__c> listFM_Case = new List<FM_Case__c>();
    //     for(Integer i=0; i<=2; i++){
    //         FM_Case__c objFMCase1 = new FM_Case__c(
    //             Account__c = objAccount.Id,
    //             Category_GE__c = 'Change of Contact Details',
    //             Subcategory_GE__c = 'Change registered email id',
    //             RecordTypeId = System.Label.FM_Case_General_Enquiry
    //         );
    //         System.debug('-->> objFMCase1 ## 11: ' + objFMCase1 );
    //         listFM_Case.add(objFMCase1);
    //         System.debug('-->> listFM_Case ## 11: ' + listFM_Case );
    //         FM_Case__c objFMCase2 = new FM_Case__c(
    //             Account__c = objAccount.Id,
    //             Category_GE__c = 'Hello Damac',
    //             Subcategory_GE__c = 'Tenant Registration',
    //             RecordTypeId = System.Label.FM_Case_General_Enquiry
    //         );
    //         System.debug('-->> objFMCase1 ## 22: ' + objFMCase1 );
    //         listFM_Case.add(objFMCase2);
    //         System.debug('-->> listFM_Case ## 22: ' + listFM_Case );
    //         FM_Case__c objFMCase3 = new FM_Case__c(
    //             Account__c = objAccount.Id,
    //             Category_GE__c = 'Fund Transfer',
    //             Subcategory_GE__c = 'Service Charge to DAMAC',
    //             RecordTypeId = System.Label.FM_Case_General_Enquiry
    //         );
    //         System.debug('-->> objFMCase1 ## 33: ' + objFMCase1 );
    //         listFM_Case.add(objFMCase3);
    //         System.debug('-->> listFM_Case ## 33: ' + listFM_Case );
    //     }
        
    //     insert listFM_Case;
    //     System.debug('-->> listFM_Case 11 : ' + listFM_Case );
    //     Set<Id> setFMCaseId = new Set<Id>();
    //     for(FM_Case__c objFM_CaseLoop: listFM_Case){
    //         setFMCaseId.add(objFM_CaseLoop.Id);
    //     }
        
    //     List<Task> listTask = new List<Task>();
    //     for(Task objTask1 : [
    //         SELECT Id
    //             , Status
    //             , ActivityDate
    //         FROM Task
    //         WHERE What.Id = :setFMCaseId   
    //     ]){
    //         objTask1.ActivityDate = System.Today();
    //         listTask.add(objTask1);
    //     }
    //     System.debug('-->> listTask 11 : ' + listTask );
    //     update listTask;

    //     Test.startTest();
            
    //         TaskEscalationCustomerSRProcessBatch objTaskEscalationCustomerSRProcessBatch = new TaskEscalationCustomerSRProcessBatch();
    //         FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
    //         Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
    //         Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
    //         Database.executebatch(objTaskEscalationCustomerSRProcessBatch,1);
            
    //     Test.stopTest();
        
    // }

    // static testMethod  void callBulkMethod() {
        
    //     User objUser = [SELECT Id, Email FROM User WHERE Email = 'puser000@amamama.com'];
    //     System.debug('-->> objUser : ' + objUser );

    //     User objUser_1 = [SELECT Id FROM User WHERE Email = 'Muser000@amamama.com'];
    //     System.debug('-->> objUser_1 : ' + objUser_1 );

    //     Account objAccount = TestDataFactoryFM.createAccount();
    //     objAccount.Email__c = 'test@test.com'; 
    //     objAccount.OwnerId = objUser.Id;
    //     insert objAccount;
    //     System.debug('-->> objAccount : ' + objAccount );

    //     Account objAccount_1 = TestDataFactoryFM.createAccount();
    //     objAccount_1.Email__c = 'test123@test.com'; 
    //     objAccount_1.OwnerId = objUser_1.Id;
    //     insert objAccount_1;
    //     System.debug('-->> objAccount_1 : ' + objAccount_1 );

    //     NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
    //     insert objSR;

    //     Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
    //     insert objBooking;
        
    //     Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
    //     insert objBooking_Unit;
        
    //     Test.startTest();
    //     System.runAs(objUser){
    //         System.debug('-->> userinfo: '+ UserInfo.getUserId());
    //         List<FM_Case__c> listFM_Case = new List<FM_Case__c>();
    //         for(Integer i=0; i<=2; i++){
    //             FM_Case__c objFMCase = new FM_Case__c(
    //             Account__c = objAccount.Id,
    //             Category_GE__c = 'Change of Contact Details',
    //             Subcategory_GE__c = 'Change registered email id',
    //             RecordTypeId = System.Label.FM_Case_General_Enquiry
    //             );
    //             listFM_Case.add(objFMCase);
    //             FM_Case__c objFMCase1 = new FM_Case__c(
    //             Account__c = objAccount.Id,
    //             Category_GE__c = 'Hello Damac',
    //             Subcategory_GE__c = 'Tenant Registration',
    //             RecordTypeId = System.Label.FM_Case_General_Enquiry
    //             );
    //             listFM_Case.add(objFMCase1);
    //             FM_Case__c objFMCase2 = new FM_Case__c(
    //             Account__c = objAccount.Id,
    //             Category_GE__c = 'Fund Transfer',
    //             Subcategory_GE__c = 'Service Charge to DAMAC',
    //             RecordTypeId = System.Label.FM_Case_General_Enquiry
    //             );
    //             listFM_Case.add(objFMCase2);
    //         }
            
    //         insert listFM_Case;
    //         System.debug('-->> listFM_Case 11 : ' + listFM_Case );
    //         Set<Id> setFMCaseId = new Set<Id>();
    //         for(FM_Case__c objFM_CaseLoop: listFM_Case){
    //             setFMCaseId.add(objFM_CaseLoop.Id);
    //         }
            
    //         List<Task> listTask = new List<Task>();
    //         for(Task objTask : [
    //             SELECT Id
    //                 , Status
    //                 , ActivityDate
    //             FROM Task
    //             WHERE What.Id = :setFMCaseId   
    //         ]){
    //             objTask.ActivityDate = System.Today();
    //             listTask.add(objTask);
    //         }
    //         System.debug('-->> listTask 11 : ' + listTask );
    //         update listTask;
    //     }

    //     System.runAs(objUser_1){
    //         System.debug('-->> userinfo 2222: '+ UserInfo.getUserId());
    //         List<FM_Case__c> listFM_Case = new List<FM_Case__c>();
    //         for(Integer i=0; i<=2; i++){
    //             FM_Case__c objFMCase3 = new FM_Case__c(
    //             Account__c = objAccount_1.Id,
    //             Category_GE__c = 'Change of Contact Details',
    //             Subcategory_GE__c = 'Change registered email id',
    //             RecordTypeId = System.Label.FM_Case_General_Enquiry
    //             );
    //             listFM_Case.add(objFMCase3);
    //             FM_Case__c objFMCase4 = new FM_Case__c(
    //             Account__c = objAccount.Id,
    //             Category_GE__c = 'Hello Damac',
    //             Subcategory_GE__c = 'Tenant Registration',
    //             RecordTypeId = System.Label.FM_Case_General_Enquiry
    //             );
    //             listFM_Case.add(objFMCase4);
    //             FM_Case__c objFMCase5 = new FM_Case__c(
    //             Account__c = objAccount.Id,
    //             Category_GE__c = 'Fund Transfer',
    //             Subcategory_GE__c = 'Service Charge to DAMAC',
    //             RecordTypeId = System.Label.FM_Case_General_Enquiry
    //             );
    //             listFM_Case.add(objFMCase5);
    //         }
            
    //         insert listFM_Case;
    //         System.debug('-->> listFM_Case : ' + listFM_Case );
    //         Set<Id> setFMCaseId = new Set<Id>();
    //         for(FM_Case__c objFM_CaseLoop: listFM_Case){
    //             setFMCaseId.add(objFM_CaseLoop.Id);
    //         }
            
    //         List<Task> listTask = new List<Task>();
    //         for(Task objTask : [
    //             SELECT Id
    //                 , Status
    //                 , ActivityDate
    //             FROM Task
    //             WHERE What.Id = :setFMCaseId   
    //         ]){
    //             objTask.ActivityDate = System.Today();
    //             listTask.add(objTask);
    //         }
    //         System.debug('-->> listTask : ' + listTask );
    //         update listTask;
    //     }
        
    //         TaskEscalationCustomerSRProcessBatch objTaskEscalationCustomerSRProcessBatch = new TaskEscalationCustomerSRProcessBatch();
    //         FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
    //         Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
    //         Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
    //         Database.executebatch(objTaskEscalationCustomerSRProcessBatch,1);
            
    //     Test.stopTest();
        
    // }

}