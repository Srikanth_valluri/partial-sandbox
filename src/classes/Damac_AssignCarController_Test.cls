@isTest
public class Damac_AssignCarController_Test {
    testMethod static void assignCar(){
        Cars__c car1 = new Cars__c();
        car1.Name = 'V 55698';
        insert car1;
        
        Driver__c driver1 = new Driver__c();
        driver1.Name = 'test';
        driver1.Assigned_Car__c = car1.Id;
        driver1.In_Active__c = FALSE;
        driver1.Mobile__c = '1234567890';
        driver1.User__c = UserInfo.getUserId();
        driver1.Unique_Key__c = String.valueOf(UserInfo.getUserId()+'-'+car1.Name);
        insert driver1;
        PageReference pg = ApexPages.currentPage();
        ApexPages.currentPage().getParameters().put('selectedCar','V 55698');
        ApexPages.currentPage().getParameters().put('selectedDriver',driver1.Id);
        test.startTest();
        Damac_AssignCarController carAllocation = new Damac_AssignCarController();
		Damac_AssignCarController.searchCars('te');
        
        carAllocation.assignCarToDriver();
        carAllocation.deactivateDriver();
        test.stopTest();
    }
}