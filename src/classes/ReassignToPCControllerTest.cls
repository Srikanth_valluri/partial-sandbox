@isTest
public class ReassignToPCControllerTest {
    public static testmethod void testMethod1(){
        Unit_Assignment__c uAObj = new Unit_Assignment__c();
        uAObj.Start_Date__c = Date.parse('11/12/17');
        uAObj.End_Date__c =  Date.parse('11/12/18');
        uAObj.Unit_Assignment_Name__c = 'UA-Test';
        uAObj.Expired__c = false;
        uAObj.Reason_For_Unit_Assignment__c = 'Reason of UA';
        uaObj.Status__c = 'PC Submitted';
        uaObj.PC__c = UserInfo.getUserID ();
        insert uAObj;
        Apexpages.standardController stdController = new Apexpages.standardController (UAobj);
        ReassignToPCController  obj = new ReassignToPCController (stdController);
        ReassignToPCController.reassigntoPC (uaObj.Id);
    }
    
    public static testmethod void testMethod2(){
        Unit_Assignment__c uAObj = new Unit_Assignment__c();
        uAObj.Start_Date__c = Date.parse('11/12/17');
        uAObj.End_Date__c =  Date.parse('11/12/18');
        uAObj.Unit_Assignment_Name__c = 'UA-Test';
        uAObj.Expired__c = false;
        uAObj.Reason_For_Unit_Assignment__c = 'Reason of UA';
        uaObj.Status__c = 'PC Submitted';
        insert uAObj;
        Apexpages.standardController stdController = new Apexpages.standardController (UAobj);
        ReassignToPCController  obj = new ReassignToPCController (stdController);
        ReassignToPCController.reassigntoPC (uaObj.Id);
    }
}