/************************************************************************************************
 * @Name              : DAMAC_ZoomIntegration
 * @Test Class Name   : 
 * @Description       : Class for accessing Team Meetings/Calendar Events from Zoom API 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         Srikanth V    28/01/2021       Created
***********************************************************************************************/
global class DAMAC_ZoomIntegration{
    @Future (callout = TRUE)
    public static void scheduleMeetingInqList  (List<Id> inqIds) {
        Map<Id, User> inqTLEmailMap = new Map<Id, User>();
        for(Inquiry__c inq: [SELECT TSA_TL_of_Owner__c FROM Inquiry__c WHERE ID IN : inqIds]){
            if(inq.TSA_TL_of_Owner__c != null && inq.TSA_TL_of_Owner__c != ''){
                inqTLEmailMap.put(inq.TSA_TL_of_Owner__c, new User());
            }
        }
        for(User tl: [SELECT Id, FirstName, LastName, Email FROM User WHERE Id IN: inqTLEmailMap.keyset()]){
            inqTLEmailMap.put(tl.Id, tl);
        }
        for(Inquiry__c inq: [SELECT Id, Name, Online_Meeting_Start_Date_Time__c, 
                                Owner.Email, Meeting_Due_Date__c, 
                                Inquiry_Owner__c, 
                                Online_Meeting_End_Date_Time__c, Notes__c, 
                                Email__c, First_Name__c, Last_Name__c ,TSA_TL_of_Owner__c
                            FROM Inquiry__c 
                            WHERE Id IN: inqIds]){
            createMeeting (inq, inqTLEmailMap);
        }
    }

    public static void createMeeting(Inquiry__c inq, Map<Id, User> inqTLEmailMap){
        ZoomRequest zoomReq = new ZoomRequest();
        zoomReq.topic = 'DAMAC: Meeting with Property Advisor';
        zoomReq.type = 2;
        zoomReq.start_time = getTimeZoomTime(inq.Online_Meeting_Start_Date_Time__c);
        zoomReq.duration = getMins(inq.Online_Meeting_Start_Date_Time__c, inq.Online_Meeting_End_Date_Time__c);
        zoomReq.agenda = 'Meeting with Property Advisor ref:'+inq.Name;
        
        RequestSettings objReqSettings = new RequestSettings();
        objReqSettings.host_video = true;
        objReqSettings.registration_type = 1;
        objReqSettings.auto_recording = 'cloud';
        zoomReq.settings = objReqSettings;

        string reqBody = JSON.serialize(zoomReq); 
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.zoom.us/v2/users/gDrRyn8oQcm86VZQwluicQ/meetings');
        req.setMethod('POST');
        req.setHeader('ACCEPT','application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6ImhnLVhVVW9tUlRLTXp2LXVxTXE5S1EiLCJleHAiOjE2NDI5NjA5MjAsImlhdCI6MTYxMTQxOTY4OX0.4f4ecu_dbnaIF-J9rkWoe36C14hDJhaINldc4U6wPh8');
        req.setTimeout(120000);
        req.setBody(reqBody);
        Http http = new Http();
        HTTPResponse res;
        res = http.send(req);

        if(res != null && res.getBody() != null){
            map<string, string> response = (map<string, string>)System.JSON.deserialize(res.getBody(), map<string, string>.class);
            System.debug ('::: Meeting ID :::::'+response.get('id'));
            String meetingId = response.get('id');
            // Adding Inquiry related user as a particiant
            addParticipant (inq.Email__c, inq.First_Name__c, inq.Last_Name__c, meetingId);
            // Adding Inquiry Owner as a particiant
            addParticipant (inq.Owner.Email, inq.Inquiry_Owner__c, '', meetingId);
            // Adding logged in user as a particiant
            if(inq.owner.Email != UserInfo.getUserEmail()){
                addParticipant (UserInfo.getUserEmail(), userInfo.getFirstName(), userInfo.getLastName(), meetingId);
            }
            // Adding TSA TL as a participant
            if(inqTLEmailMap.containsKey(inq.TSA_TL_of_Owner__c)){
                User u = inqTLEmailMap.get(inq.TSA_TL_of_Owner__c);
                if (u.Id != null)
                    addParticipant (u.Email, u.firstName, u.lastName, meetingId);
            }
        }
    }
    public static void addParticipant(string email, string firstName, string lastName, string meetingId){
        try{
            
            Registrants objReg = new Registrants();
            objReg.email = email;
            objReg.first_name = firstName;
            objReg.last_name = lastName;

            string reqBody = JSON.serialize(objReg); 
            HttpRequest req = new HttpRequest();
            req.setEndpoint('https://api.zoom.us/v2/meetings/'+meetingId+'/registrants');
            req.setMethod('POST');
            req.setHeader('ACCEPT','application/json');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6ImhnLVhVVW9tUlRLTXp2LXVxTXE5S1EiLCJleHAiOjE2NDI5NjA5MjAsImlhdCI6MTYxMTQxOTY4OX0.4f4ecu_dbnaIF-J9rkWoe36C14hDJhaINldc4U6wPh8');
            req.setTimeout(120000);
            req.setBody(reqBody);
            Http http = new Http();
            HTTPResponse res;
            res = http.send(req);
            System.debug ('Participant Response ::::'+res.getBOdy ());

        }catch(Exception ex){

        }
    }
    public static string getTimeZoomTime(Datetime dt){
        return dt.format('yyyy-MM-dd HH:mm:ss');
    }
    public static Integer getMins(Datetime st, Datetime et){
        Long dt1Long = st.getTime();
        Long dt2Long = et.getTime();
        Long milliseconds = dt2Long - dt1Long;
        Long seconds = milliseconds / 1000;
        Integer minutes = (Integer)(seconds / 60);
        return minutes;
    }

    public class ZoomRequest {
        public string topic;
        public Integer type;
        public string start_time;
        public Integer duration;
        public string schedule_for;
        public string timezone;
        public string password;
        public string agenda;
        public RecurrenceDetails recurrence;
        public RequestSettings settings;
    }
    public class RecurrenceDetails{
        public Integer type;
        public Integer repeat_interval;
        public string weekly_days;
        public Integer monthly_day;
        public Integer monthly_week;
        public Integer monthly_week_day;
        public Integer end_times;
        public string end_date_time;
    }
    public class RequestSettings{
        public boolean host_video;
        public boolean participant_video;
        public boolean cn_meeting;
        public boolean in_meeting;
        public boolean join_before_host;
        public boolean mute_upon_entry;
        public boolean watermark;
        public boolean use_pmi;
        public Integer approval_type;
        public Integer registration_type;
        public string audio;
        public string auto_recording;
        public boolean enforce_login;
        public string enforce_login_domains;
        public string alternative_hosts;
        public list<string> global_dial_in_countries;
        public boolean registrants_email_notification;
    }
    public class Registrants {
        public String email {get;set;}
        public String first_name {get;set;}
        public String last_name {get;set;}
        public String address {get;set;}
        public String city {get;set;}
        public String country {get;set;}
        public String zip {get;set;}
        public String state {get;set;}
        public String phone {get;set;}
        public String industry {get;set;}
        public String org {get;set;}
        public String job_title {get;set;}
        public String purchasing_time_frame {get;set;}
        public String role_in_purchase_process {get;set;}
        public String no_of_employees {get;set;}
        public String comments {get;set;}
        public list<CustomQuestions> custom_questions {get;set;}
    }
    public class CustomQuestions {
        public String title {get;set;}
        public String value {get;set;}
    }


}