public Interface DataProvider {
    DataDisplayConfig getData(DataDisplayConfig config);
}