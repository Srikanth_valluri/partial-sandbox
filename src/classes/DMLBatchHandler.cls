global class DMLBatchHandler implements Database.Batchable<sobject>{
    
    global string DMLAction = '';
    global string queryStr = '';
    global boolean hardDelete = false;
    global string fieldName = '';
    global string fieldValue = '';
    global string val2Append = '';
    
    
    global DMLBatchHandler(string query, string action, boolean hdel, string fieldAPIName, string fieldVal, string value2Append){
        queryStr = query;
        DMLAction = action;
        hardDelete = hDel;
        fieldName = fieldAPIName;
        fieldValue = fieldVal;
        val2Append = value2Append;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        return database.getQueryLocator(querystr);
    }
    
    global void execute(Database.BatchableContext bc, list<sobject> scope){

        if(DMLAction.toLowerCase()=='update'){
            System.debug(fieldName+'>>>>>>'+fieldValue+'>>>>>>>>>>>>'+val2Append);
            if(fieldName != ''){
            for(sObject s: scope){
                if(fieldValue != '')
                    s.put(fieldName, fieldValue);
                if(val2Append != '')
                    s.put(fieldName, string.valueof(s.get(fieldName))+val2Append);  
                }   
            }
            update scope;
        }

        if(DMLAction.toLowerCase()=='delete'){
            delete scope;   
            if(hardDelete)
                Database.emptyRecycleBin(scope);
        }     
    }
    
    global void finish(Database.BatchableContext bc){
    
    }

}