/*
Developed By: DAMAC IT Team
Usage:DAMAC_CDC_REST_UTILITY
*/

public class DAMAC_CDC_RESPONSE_OBJ_SR{
    
    public list<BookingUnits> bookingunits;        
    public string SFSRNUMBER;    
    
    public class BookingUnits{
        public String regId; 
        public string cdcstatus;
    }
    
    public static DAMAC_CDC_RESPONSE_OBJ_SR parse(String json){
        return (DAMAC_CDC_RESPONSE_OBJ_SR) System.JSON.deserialize(json, DAMAC_CDC_RESPONSE_OBJ_SR.class);
    }
    
}