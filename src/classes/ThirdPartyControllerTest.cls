@isTest
public class ThirdPartyControllerTest {
    private static testMethod void testDDP(){

        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', RecordTypeID=caseRecTypeId);
        insert caseObj;

        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c( name='3rd party'
                                                        , Drawloop_Document_Package_Id__c = 'fjafjkaf'
                                                        , Delivery_Option_Id__c ='dasdasd' );
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController( caseObj );
        ThirdPartyController ctrl = new ThirdPartyController( stdCtrl );
        ThirdPartyController.generate3rdParty();
        Test.stopTest();

    }
    
     private static testMethod void testNegativeDDP(){

        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', RecordTypeID=caseRecTypeId);
        //insert caseObj;

        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c( name='3rd party'
                                                        , Drawloop_Document_Package_Id__c = 'fjafjkaf'
                                                        , Delivery_Option_Id__c ='dasdasd' );
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController( caseObj );
        ThirdPartyController ctrl = new ThirdPartyController( stdCtrl );
        ThirdPartyController.generate3rdParty();
        Test.stopTest();

    }
}