/***************************************************************************************************
 * @Name              : ApplySalesMarginToInventoriesController
 * @Test Class Name   : ApplySalesMarginToInventoriesTest
 * @Description       : 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         19/08/2020       Created
****************************************************************************************************/

global class ApplySalesMarginToInventoriesController {
    webservice static String updatePSF(Id salesMarginId ) { 
        List<Id> smIdList = new List<Id>();
        smIdList.add(salesMarginId);
        String retValue = updatePSF(smIdList);
        return retValue;
    }
    
    public static String updatePSF(list<Id> salesMarginIdList ) {
       system.debug('salesMarginIdList>> ' + salesMarginIdList);
       String acd, marketingName, bdType, buildLoc, floorLoc, viewType, propertyName;
       List<String> invStatuses = new List<String>();
       for(String status: Label.Sales_Margin_Calc_Inventory_Status.split(',')){
           invStatuses.add(status.trim());    
       }
       system.debug('invStatuses: ' + invStatuses);
       Map<Id, List<Inventory__c>> invSMMap = new Map<Id, List<Inventory__c>>();
       try{
           for(Inventory__c inv:  [SELECT Id, Sales_Margins__c, Status__c, AC_Area_PSF_on_Current_Standard_Price__c, 
                                       PSF_on_Current_Standard_Price__c, Final_Total_Area__c, 
                                       Current_AC_Area__c, List_Price__c, Current_AC_Area_SFT__c
                       FROM Inventory__c 
                       WHERE Sales_Margins__c IN: salesMarginIdList
                           AND Status__c IN: invStatuses
                           AND List_Price__c != NULL]){
               List<Inventory__c> invList = new List<Inventory__c>();
               if(invSMMap.containsKey(inv.Sales_Margins__c)){
                   invList = invSMMap.get(inv.Sales_Margins__c);
               }
               invList.add(inv);
               invSMMap.put(inv.Sales_Margins__c, invList);
           }
       } catch(Exception e){
           system.debug('Error while fetching Inventories: ' + e);
           return 'Error while fetching Inventories: ' + e;
       }

       system.debug('invSMMap size>> ' + invSMMap.keyset().size());
       String returnMsg = 'Updated PSF Details Successfully';
       Boolean updatePSF = false;
       
       Integer count = 0;
       List<Sales_Margins__c> salesMarginList = new List<Sales_Margins__c>();
       for(Id smId: invSMMap.keyset()){
           Decimal totalListPrice = 0.0;
           Decimal totalACListPrice = 0.0;
           Decimal totalArea = 0.0;
           Decimal totalACArea = 0.0;
           for(Inventory__c inv: invSMMap.get(smId)){
               system.debug('inv.List_Price__c: ' + inv.List_Price__c);
               system.debug('inv.Final_Total_Area__c: ' + inv.Final_Total_Area__c);
               totalListPrice += inv.List_Price__c;
               totalArea += inv.Final_Total_Area__c;
               if(inv.Current_AC_Area_SFT__c != null){
                   totalACArea += inv.Current_AC_Area_SFT__c;
                   totalACListPrice += inv.List_Price__c;
               }
           }
           system.debug('totalACListPrice: ' + totalACListPrice);
           system.debug('totalListPrice: ' + totalListPrice);
           system.debug('totalArea: ' + totalArea);
           system.debug('totalACArea: ' + totalACArea);
           Sales_Margins__c salesMargin = new Sales_Margins__c();
           if(totalArea > 0){
               salesMargin.Avg_System_PSF__c = totalListPrice / totalArea;
           } else {
               salesMargin.Avg_System_PSF__c = 0;
           }
           if(totalACArea > 0){
               salesMargin.Avg_PSF_on_AC_Area__c = totalACListPrice / totalACArea;
           } else {
               salesMargin.Avg_PSF_on_AC_Area__c = 0;
           }
           salesMargin.Id = smId;
           salesMarginList.add(salesMargin);
       }
       
       if(salesMarginList.size() > 0){
           try{
               update salesMarginList;
           } catch(Exception e){
               system.debug('Error while updating Inventories: ' + e);
               return 'Error while updating Inventories: ' + e;
           }
       }
       return returnMsg;
    }


    webservice static String applyToInventories(Id salesMarginId ) {
       system.debug('salesMarginId>> ' + salesMarginId);
       Sales_Margins__c salesMargin = new Sales_Margins__c();
       List<String> acd = new List<String>();
       List<String> marketingName = new List<String>();
       List<String> bdType = new List<String>();
       List<String> buildLoc = new List<String>();
       List<String> floorLoc = new List<String>();
       List<String> viewType = new List<String>();
       List<String> propertyName = new List<String>();
       salesMargin = [SELECT Id, Marketing_Name__c, Bedroom_Type__c, Building_Location__c, ACD__c,
                                   Property_Name__c, View_Type__c, Floor_Location__c,Avg_System_PSF__c, Avg_PSF_on_AC_Area__c
                      FROM Sales_Margins__c WHERE Id=: salesMarginId LIMIT 1];
       String queryString = ' SELECT Id, Sales_Margins__c, Status__c '
                           + ' FROM Inventory__c WHERE Sales_Margins__c !=: salesMarginId ' ;

       if(salesMargin.ACD__c != null && salesMargin.ACD__c != ''){
           for(String val: salesMargin.ACD__c.split(';')){
               acd.add(val);
           }
           queryString += ' AND Anticipated_Completion_Date__c IN: acd ';   
       }
       if(salesMargin.Marketing_Name__c != null && salesMargin.Marketing_Name__c != ''){
           for(String val: salesMargin.Marketing_Name__c.split(';')){
               marketingName.add(val);
           }
           queryString += ' AND Marketing_Name__c IN: marketingName ';   
       }
       if(salesMargin.Bedroom_Type__c != null && salesMargin.Bedroom_Type__c != ''){
           for(String val: salesMargin.Bedroom_Type__c.split(';')){
               bdType.add(val);
           }
           queryString += ' AND Bedroom_Type__c IN: bdType ';   
       }
       if(salesMargin.Building_Location__c != null && salesMargin.Building_Location__c != ''){
           for(String val: salesMargin.Building_Location__c.split(';')){
               buildLoc.add(val);
           }
           queryString += ' AND Building_Location__r.Name IN: buildLoc ';   
       }
       if(salesMargin.Floor_Location__c != null && salesMargin.Floor_Location__c != ''){
           for(String val: salesMargin.Floor_Location__c.split(';')){
               floorLoc.add(val);
           }
           queryString += ' AND Floor_Location__r.Name IN: floorLoc ';   
       }
       if(salesMargin.View_Type__c != null && salesMargin.View_Type__c!= ''){
           for(String val: salesMargin.View_Type__c.split(';')){
               viewType.add(val);
           }
           queryString += ' AND View_Type__c IN: viewType ';   
       }
       if(salesMargin.Property_Name__c != null && salesMargin.Property_Name__c != ''){
           for(String val: salesMargin.Property_Name__c.split(';')){
               propertyName.add(val);
           }
           queryString += ' AND Property_Name__c IN: propertyName ';   
       }
        if(Test.isRunningTest()){
         queryString += 'LIMIT 3';     
       } else{
            queryString += 'LIMIT 10000';
       }
        system.debug('queryString: ' + queryString);
       List<Inventory__c> invList = new List<Inventory__c>();
       try{
           invList = Database.query(queryString);
       } catch(Exception e){
           system.debug('Error while updating Inventories: ' + e);
           return 'Error while updating Inventories: ' + e;
       }

       system.debug('invList size>> ' + invList.size());
       system.debug('invList >> ' + invList);
       String returnMsg = 'No Unapplied Inventories Found';
       if(invList.size() > 0){
           if(invList.size() == 1){
               returnMsg = 'Successfully Applied Sales Margin to 1 Inventory';    
           } else{
               returnMsg = 'Successfully Applied Sales Margin to ' + invList.size() + ' Inventories';
           }
          
           for(Inventory__c inv: invList){
               inv.Sales_Margins__c = salesMargin.Id;
           }
           try{
               update invList;
           } catch(Exception e){
               system.debug('Error while updating Inventories: ' + e);
               return 'Error while updating Inventories: ' + e;
           }
       }
       return returnMsg;
    }
}