/**************************************************************************************************
* Name               : PcConfirmation_Test
* Description        : Test Class for PcConfirmation 
* ------------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0                         24/03/2018      Created
**************************************************************************************************/
@isTest
public class PcConfirmation_Test {
    static NSIBPM__Service_Request__c servceReq;
    static Buyer__c buyer, buyer1, buyer2;
    Static Set <ID> bookingIds;
    static Inquiry__c inq;
    static Account a;
    static void init () {
        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        a = new Account();
        a.LastName= 'Test Account';
        a.RecordTypeId = accountRTId;
        a.Agency_Short_Name__c = 'testShrName';
        a.Party_ID__c = '769875';
        a.Email__c = 'test@gmail.com';
        a.Mobile__c = '7894561230';
        a.Title__c = 'Ms.';
        a.Nationality__c = 'Australian';
        a.Passport_Number__c = '123654';
        insert a;
        
        NSIBPM__SR_Status__c srStatus = new NSIBPM__SR_Status__c();
        srStatus.NSIBPM__Code__c = 'SUBMITTED';
        insert srStatus;

        servceReq = new NSIBPM__Service_Request__c();
        servceReq.Agency__c = a.Id;
        insert servceReq;

        Booking__c bookng = new Booking__c ();
        bookng.Deal_SR__c = servceReq.Id;
        insert bookng;
        bookingIds = new Set <ID> ();
        bookingIds.add (bookng.Id);
        
        Booking_Unit__c bookngUnit = new Booking_Unit__c ();
        bookngUnit.Booking__c = bookng.Id;
        insert bookngUnit; 

        ID recordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry Decrypted').getRecordTypeId();
        inq = new Inquiry__c ();
        inq.Mobile_phone__c = '7894561230';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYUIYRTYU';
        inq.Class__c = 'test';
        inq.First_Name__c = 'test';
        inq.Inquiry_Status__c = 'Active';
        inq.Inquiry_Source__c ='Agent Referral';
        inq.RecordtypeId = recordTypeId;
        inq.Class__c = 'test';
        inq.Email__c = 'test@gmail.com';
        inq.Phone_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Nationality__c = 'Australian';
        insert inq;

        buyer = new buyer__c ();
        buyer.account__c = a.Id;
        buyer.Booking__c = bookng.Id;
        buyer.Phone__c = '7894561230';
        buyer.Country__c = 'India';
        buyer.Primary_buyer__c = true;
        buyer.Email__c = 'test@gmail.com';
        buyer.Phone_Country_Code__c = 'United Arab Emirates: 00971';
        buyer.Address_Line_1__c = 'Testing'; 
        buyer.CR_Registration_Expiry_Date_New__c = Date.today().addDays (60);
        buyer.Passport_Expiry__c = Date.today().addDays (60);
        buyer.DOB__c = Date.today().addYears (-30);
        buyer.Inquiry__c = inq.Id;
        buyer.Buyer_Organisation_Name__c = 'Test Org';
        buyer.Customer_Industry__c = 'Test Industry';
        buyer.Function_Domain__c = 'Test Domain';
        buyer.Designation__c = 'Test Designation';
        buyer.Influencer_Rating__c = '5';
        
        insert buyer;

        buyer1 = new buyer__c ();
        buyer1.account__c = a.Id;
        buyer1.Email__c = 'test@gmail.com';
        buyer1.Booking__c = bookng.Id;
        buyer1.Job_Id__c = bookng.Id;
        buyer1.Address_Changed__c = TRUE;
        buyer1.Party_ID__c = '12345';
        buyer1.Buyer_Type__c = 'Individual';
        buyer1.Passport_Expiry__c = Date.today().addDays (60);
        buyer1.Passport_Expiry_Date__c = String.valueOf (Date.today().addDays (60));
        buyer1.DOB__c = Date.today().addYears (-21);
        buyer1.Phone__c = '789456123';
        buyer1.Country__c = 'India';
        buyer1.City__c = 'Hyderabad';
        buyer1.Nationality__c = 'Indian';
        buyer1.Passport_Number__c = 'K766606';
        buyer1.Place_of_Issue__c = 'K766606';
        buyer1.Address_line_1__c = 'Testing';
        buyer1.Title__c = 'Mr.';
        buyer1.First_Name__c = 'test';
        buyer1.last_Name__c = 'test';
        buyer1.Gender__c = 'Male';
        buyer1.Phone_Country_Code__c = 'India: 0091';
        buyer1.Address_Line_1__c = 'Testing';
        buyer1.Inquiry__c = inq.Id;
        buyer1.Buyer_Organisation_Name__c = 'Test Org';
        buyer1.Customer_Industry__c = 'Test Industry';
        buyer1.Function_Domain__c = 'Test Domain';
        buyer1.Designation__c = 'Test Designation';
        buyer1.Influencer_Rating__c = '5';
        insert buyer1;
    }

    static testMethod void pcCnfirmationTest () {
        init ();
        PageReference pageRef = Page.PCConfirmationStep;
        ApexPages.currentPage().getParameters().put('buyerId', buyer.Id);
        ApexPages.StandardController sc = new ApexPages.standardController (servceReq);
        Test.startTest ();    
            PCConfirmation obj = new PCConfirmation (sc);
            obj.clearPageMessages ();
            obj.init (); 
            obj.jointBuyer = buyer;
            obj.pageMessage = '';
            obj.validateDateOfBirth (System.Today ().addYears (-25));
            obj.formatDate (System.Today ().addYears (-25));
            obj.checkLength ('1');
            obj.createJointBuyer ();
            obj.buyerType = 'Corporate';
            obj.createAccountforBuyer(buyer1);
            obj.jointBuyer = buyer1;
            obj.creatingJointBuyer ();
            PCConfirmation.RegistrationResults (buyer.Id);
            obj.retrievePartyIds ();
            obj.sendToIPMS ();
            apexpages.currentpage().getparameters().put ('buyerId', buyer1.Id);
            obj.deleteBuyer ();
            obj.updateBuyers ();
            obj.confirmStep ();
            PCConfirmation.getCityValues ('India');
            PCConfirmation.checkForAccount ('123654', 'Australian');
            PCConfirmation.checkingForAccount ('123654', 'Australian','Individual');
            
            obj.cancel ();
        Test.StopTest ();
    }

    static testMethod void pcCnfirmationTest2 () {
        test.starttest();
            PageReference pageRef = Page.PCConfirmationStep;
            Test.setCurrentPage(pageRef);
            init ();
            ApexPages.currentPage().getParameters().put('buyerId', buyer.Id);
            ApexPages.StandardController sc = new ApexPages.standardController (servceReq);
            PCConfirmation obj = new PCConfirmation (sc);
            obj.clearPageMessages ();
            obj.init ();
            obj.jointBuyer = buyer;
            obj.pageMessage = '';
            obj.validateDateOfBirth (System.Today ().addYears (-25));
            obj.formatDate (System.Today ().addYears (-25));
            obj.checkLength ('1');
            obj.createJointBuyer ();
            obj.buyerType = 'Individual';
            obj.createAccountforBuyer(buyer1);
            PCConfirmation.RegistrationResults (buyer.Id);
            obj.retrievePartyIds ();
            obj.sendToIPMS ();
            apexpages.currentpage().getparameters().put ('buyerId', buyer1.id);
            PCConfirmation.getCityValues ('India');
            PCConfirmation.checkForAccount ('123654', 'Australian');
            PCConfirmation.checkingForAccount ('123654', 'Australian', 'Corporate');
            obj.cancel ();
            PCConfirmation.getInquiryDetails ('test', 'test');
            PCConfirmation.getInquiryDetails ('test', '');
            PCConfirmation.mapInquiryDetailsToBuyer (inq.Id, 'Inquiry__c');
            PCConfirmation.getAccountDetails ('Test', 'Individual');
            PCConfirmation.mapInquiryDetailsToBuyer (a.Id, 'Account__c');
            AsyncBuyerWebservice.GetDatetext (Date.Today ().addMonths (1));
            AsyncBuyerWebservice.GetDatetext (Date.Today ().addMonths (2));
            AsyncBuyerWebservice.GetDatetext (Date.Today ().addMonths (3));
            AsyncBuyerWebservice.GetDatetext (Date.Today ().addMonths (4));
            AsyncBuyerWebservice.GetDatetext (Date.Today ().addMonths (5));
            AsyncBuyerWebservice.GetDatetext (Date.Today ().addMonths (6));
            AsyncBuyerWebservice.GetDatetext (Date.Today ().addMonths (7));
            AsyncBuyerWebservice.GetDatetext (Date.Today ().addMonths (8));
            AsyncBuyerWebservice.GetDatetext (Date.Today ().addMonths (9));
            AsyncBuyerWebservice.GetDatetext (Date.Today ().addMonths (10));
            AsyncBuyerWebservice.GetDatetext (Date.Today ().addMonths (11));
        test.stoptest();
    }

    static testMethod void pcConfirmationFail () {
        init ();
        servceReq.PC_confirmation_Completed__c = True;
        update servceReq;
        ApexPages.currentPage().getParameters().put('buyerId', buyer.Id);
        ApexPages.StandardController sc = new ApexPages.standardController (servceReq);
        PCConfirmation obj = new PCConfirmation (sc);
        obj.clearPageMessages ();
        obj.init ();
    }
    
        static testMethod void pcCnfirmationTest3 () {
        init ();
        PageReference pageRef = Page.PCConfirmationStep;
        ApexPages.currentPage().getParameters().put('buyerId', buyer.Id);
        ApexPages.StandardController sc = new ApexPages.standardController (servceReq);
        Test.startTest ();    
            PCConfirmation obj = new PCConfirmation (sc);
            obj.clearPageMessages ();
            obj.init (); 
            obj.jointBuyer = buyer1;
            obj.pageMessage = '';
            obj.validateDateOfBirth (System.Today ().addYears (-25));
            obj.formatDate (System.Today ().addYears (-25));
            obj.checkLength ('1');
            obj.createJointBuyer ();
        Test.StopTest ();
    }
}