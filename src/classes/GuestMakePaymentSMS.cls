public class GuestMakePaymentSMS{
@InvocableMethod 
    public static void initiateSMS(List<Id> callogsIdList){
        list<string> phonenumList = new list<string>();
        list<string> callTypelabel=label.GuestMakePaymentSMSLabel.split(',');
        if(callogsIdList.size()>0){
            for(Call_Log__c cl:[select id
                                        ,LOAMS_Call_Type__c
                                        ,Call_Type__c
                                        ,Calling_Number__c
                                        from
                                            Call_Log__c
                                        where   
                                                LOAMS_Call_Type__c =:callTypelabel
                                        AND
                                                ID =:callogsIdList
                                        AND   
                                                Call_Type__c='Inbound'
                                        ]){
                    if(!phonenumList.contains(cl.Calling_Number__c) && string.isNotBlank(cl.Calling_Number__c)){
                        phonenumList.add(cl.Calling_Number__c);
                    }                       
                
                system.debug('phonenumList:::'+phonenumList.size());
            }
        }
        if(phonenumList.size()>0){
            EmailTemplate emailtmp= new EmailTemplate();
            emailtmp=[SELECT Body,name FROM EmailTemplate where name ='GuestMakePaymentSMS'];
            string smsBody=emailtmp.Body;
            smsBody=smsBody.replace('{!url}','https://rebrand.ly/inp7qkr' );
            string MsgContent=smsBody;
            system.debug('MsgContent:::'+MsgContent);
            string accountid='';
            boolean isFMSMS=true; 
            string strAbndId='';
            if(string.isNotBlank(MsgContent) && label.GuestMakePaymentSMSSwitch=='True'){
            SendSMSAccountService.Sendtextmessage(phonenumList,MsgContent,accountid,isFMSMS,strAbndId);
            }
        }
    }

}