/****************************************************************************************************
* Name               : PaymentPlanProjectController
* Description        : Controller class for Payment Plan Project VF Page
* Created Date       : 16/10/2020
* Created By         : QBurst
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         16/10/2020      Initial Draft
****************************************************************************************************/

    global with sharing class PaymentPlanProjectController {
    public String projectId {get; set;}
    public Map<String, Id> planNamesMap {get; set;}
    public List<String> planNamesList {get; set;}
    public Integer buildingCount {get; set;}
    public Integer floorCount {get; set;}
    public Integer unitCount {get; set;}
    public Property__c property {get; set;}
    public static string payplanJSON {get; set;}
    public Map<String, Date> payPlanEndDateMap { get; set;}
    public String payPlanEndDateMapJSON {get; set;}
    public String propertyName;
    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public PaymentPlanProjectController() {
        projectId = ApexPages.currentPage().getParameters().get('Id');
        floorCount = 0;
        unitCount = 0; 
        buildingCount = 0;
        payplanJSON = 'default';
        planNamesMap = new  Map<String, Id>();
        planNamesList = new List<String>();
        propertyName = '';
        payPlanEndDateMap = new Map<String, Date>();
        payPlanEndDateMapJSON = JSON.serialize(payPlanEndDateMap);
		system.Debug(projectId);
        if(projectId != null && projectId != '' ){
            property = [SELECT Id, Name, Property_Name__c
                               FROM Property__c 
                               WHERE  Id =: projectId];
            for(Location__c loc: [SELECT Id, Name, Building_Name__c , Building_Number__c
                               FROM Location__c 
                               WHERE Location_Type__c = 'Floor'
                               AND Building_Number__r.Property_Name__c =: projectId]){
                floorCount++;
            }
            for(Location__c loc: [SELECT Id, Name, Building_Name__c , Building_Number__c
                               FROM Location__c 
                               WHERE Location_Type__c = 'Building'
                               AND Property_Name__c =: projectId]){
                buildingCount++;
            }
            for(Inventory__c inv: [SELECT Id FROM Inventory__c 
                                WHERE Property__c =: projectId]){
                unitCount++;
            }

            for(Payment_Plan__c payPlan: [SELECT id, Name, Building_ID__c, Booking_Unit__c, Payment_Term_Description__c,
                                        Active__c, Effective_To_calculated__c, Effective_from__c,Effective_To__c
                                 FROM Payment_Plan__c
                                 WHERE Effective_To_calculated__c >= TODAY
                                 AND Active__c = TRUE
                                 ORDER BY CreatedDate DESC
                                 LIMIT 1000]){
                String planName = payPlan.Name;
                if(payPlan.Payment_Term_Description__c != null  && payPlan.Payment_Term_Description__c != ''){
                    planName += ' - ' + payPlan.Payment_Term_Description__c;
                }
                planNamesList.add(planName);
                planNamesMap.put(planName, payPlan.Id);
            }
            fetchPayPlans();
        }
    }

    public void fetchPayPlans(){
        List<Payment_Plan__c> payPlanList = new List<Payment_Plan__c>();
        List<Id> payPlanIds = new List<Id>();
        for(Payment_Plan_Association__c planAssoc : [SELECT Id, Name, Payment_Plan__c, 
                                                        Location__c, Effective_From__c,
                                                        Effective_To__c, Property__c,
                                                        Property__r.Property_Name__c
                                                     FROM Payment_Plan_Association__c
                                                     WHERE Property__c =: projectId]){
            payPlanIds.add(planAssoc.Payment_Plan__c);
            if(!payPlanEndDateMap.containsKey(planAssoc.Payment_Plan__c)){
                payPlanEndDateMap.put(String.valueOf(planAssoc.Payment_Plan__c), planAssoc.Effective_To__c);
            }
        }
        payPlanEndDateMapJSON = JSON.serialize(payPlanEndDateMap);
        system.debug('payPlanIds: ' + payPlanIds);
        for(Payment_Plan__c payPlan: [SELECT Id, Building_Location__c, Name, 
                                            Payment_Term_Description__c, Effective_To_calculated__c, 
                                            LastModifiedDate, Effective_To__c,
                                            (SELECT Id, Name,Installment__c,Description__c,Milestone_Event__c, CreatedDate,
                                                Milestone_Event_Arabic__c,Percent_Value__c FROM Payment_Terms__r) 
                                       FROM Payment_Plan__c WHERE Id IN: payPlanIds]){
                payPlanList.add(payPlan);
                system.debug('payPlanList'+payPlan);
            }
            system.debug('payPlanList'+payPlanList);
            payplanJSON = JSON.serialize(payPlanList);
    }

    public void associatePayPlan(){
        String payPlanId = ApexPages.currentPage().getParameters().get('payPlanId');
        String endDate = ApexPages.currentPage().getParameters().get('endDate');
        system.debug('payPlanId'+payPlanId);
        system.debug('endDate'+endDate);
        Payment_Plan_Association__c planAssoc = new Payment_Plan_Association__c();
        planAssoc.Payment_Plan__c = payPlanId;
        //planAssoc.Location__c = projectId;
        planAssoc.Property__c = projectId;
        planAssoc.Effective_From__c = system.today();
        if(endDate != null && endDate != ''){
            planAssoc.Effective_To__c =  Date.parse(endDate);
        }
        insert planAssoc;
        fetchPayPlans();
    }
}