@isTest
public class ProcessUnprocessedRequestTest{
     static testMethod void testMethod1() 
    {
        Account lstAccount= TestDataFactory_CRM.createPersonAccount();
        insert lstAccount;
        Case lstCase = TestDataFactory_CRM.createCase(lstAccount.id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId());
        lstCase.Is_Batch_Processing_Pending__c = true ;
        insert lstCase;
        
        Test.startTest();

            ProcessUnprocessedRequest obj = new ProcessUnprocessedRequest();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }
    
    static testMethod void testMethod2() 
    {
        Account lstAccount= TestDataFactory_CRM.createPersonAccount();
        insert lstAccount;
        Case lstCase = TestDataFactory_CRM.createCase(lstAccount.id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId());
        lstCase.Is_Batch_Processing_Pending__c = true ;
        insert lstCase;
        
        Test.startTest();

            ProcessUnprocessedRequest obj = new ProcessUnprocessedRequest(0, true);
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }
}