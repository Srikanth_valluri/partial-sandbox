global class UpdateCustomerMobileFieldBatch implements Database.Batchable<sObject> {
	
	String query;
	
	global UpdateCustomerMobileFieldBatch() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = ' SELECT Mobile_Phone_Encrypt__pc, Mobile__C, telephone__c, Id from Account where IsPersonAccount = true AND Mobile_Phone_Encrypt__pc = null AND ( Mobile__c != NULL OR Telephone__c != NULL ) ';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Account> scope) {
		for( Account accObj : scope ){
			if (String.isBlank( accObj.Mobile_Phone_Encrypt__pc) && String.isNotBlank( accObj.Mobile__c )   ){
				accObj.Mobile_Phone_Encrypt__pc = accObj.Mobile__c;
			} else if ( String.isBlank( accObj.Mobile_Phone_Encrypt__pc) && String.isBlank( accObj.Mobile__c ) && String.isNotBlank( accObj.telephone__c ) ){
				accObj.Mobile_Phone_Encrypt__pc = accObj.telephone__c;
			}
		}

		update scope;
	}
	
	global void finish(Database.BatchableContext BC) {
			List<String> toAddresses = new List<String>();
	        //toAddresses.add('amit.joshi@eternussolutions.com');
	        toAddresses.add('snehil.karn@eternussolutions.com');
	        //toAddresses.add( caseObj[0].Owner.Email );
	        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	        mail.setToAddresses(toAddresses);

	        mail.setSubject( 'Batch UpdateCustomerMobileFieldBatch() completed');
	        String mailBody = 'FYI,';

	        mail.setHtmlBody( mailBody );
	        system.debug('mail' + mail);
	        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	
}