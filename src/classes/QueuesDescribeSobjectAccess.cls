/*
This class will define the access of SObject based in queues
*/
public virtual class QueuesDescribeSobjectAccess {
    
    public Boolean canEditRecord {get;set;}
    protected String sObjectAPIName {get;set;}
    protected String recordID {get;set;}
    
    public QueuesDescribeSobjectAccess(){
        
    }
    
    //Method will help to identify if the current loged in user has Edit and Delete access or not on current accessing sObject.
    public virtual void getAccess(){
        sObject thisObject;
        canEditRecord = False;
        
        //logged in User Profile Name
        User loggedinUser = [Select Profile.Name From User Where Id =: userinfo.getUserId() Limit 1];
        if(loggedinUser.Profile.Name.containsIgnoreCase('System Administrator')){
            canEditRecord = true;
        }else{
            //get ownerid
            String queryStr = 'Select OwnerId From ' + sObjectAPIName + ' Where Id =: recordID Limit 1';
            thisObject = Database.query(queryStr);
            String recordOwnerId = (String)thisObject.get('OwnerId');
            
            List<GroupMember> userQueues = [SELECT Group.Name 
                                            FROM GroupMember 
                                            WHERE UserOrGroupId =: userinfo.getUserId() 
                                            AND Group.Type = 'Queue' AND GroupId =: recordOwnerId];
            
            if(userQueues.size() > 0){
                canEditRecord = true;
            }
        }
        
        system.debug('thisObject: ' + thisObject);
        system.debug('canEditRecord: ' + canEditRecord);
    }
}