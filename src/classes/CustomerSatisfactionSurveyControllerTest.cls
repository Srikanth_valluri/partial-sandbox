@isTest
private class CustomerSatisfactionSurveyControllerTest {

    @isTest
    static void testSurvey() {
        CustomerSatisfactionSurveyController controller = new CustomerSatisfactionSurveyController();
        Survey_CRM__c survey = new Survey_CRM__c(
            Name = Label.FMCustomerSatisfactionSurveyForm,
            Description__c = 'Test Description',
            Thank_You_Text__c = 'Thank You',
            Is_Active__c = TRUE
        );
        insert survey;

        List<Survey_Question_CRM__c> questions = new List<Survey_Question_CRM__c>();
        /*questions.add(new Survey_Question_CRM__c(
            Survey__c = survey.Id,
            Question__c = 'What is your name',
            OrderNumber__c = 1,
            Order_Number_Displayed__c = '1',
            Type__c = 'Free Text - Single Row Visible'
        ));*/

        questions.add(new Survey_Question_CRM__c(
            Survey__c = survey.Id,
            OrderNumber__c = 1,
            Order_Number_Displayed__c = '1',
            Question__c = 'What is your Name?',
            Required__c = True,
            Choices__c = '1 \n 2\n 3',
            Type__c = 'Single Select--Vertical',
            Choice_for_Additional_Textbox__c = 'Single Select Vertical; Test;'
        ));
        questions.add(new Survey_Question_CRM__c(
            Survey__c = survey.Id,
            OrderNumber__c = 2,
            Order_Number_Displayed__c = '2',
            Question__c = 'What is your Name?',
            Required__c = True,
            Choices__c = '1 \n 2\n 3',
            Type__c = 'Single Select--Horizontal',
            Choice_for_Additional_Textbox__c = 'Single Select Vertical; Test;'
        ));
        questions.add(new Survey_Question_CRM__c(
            Survey__c = survey.Id,
            OrderNumber__c = 3,
            Order_Number_Displayed__c = '3',
            Question__c = 'What is your Name?',
            Choices__c = '1 \n 2\n 3',
            Type__c = 'Multi-Select--Vertical',
            Choice_for_Additional_Textbox__c = 'Single Select Vertical; Test;'
        ));
        questions.add(new Survey_Question_CRM__c(
            Survey__c = survey.Id,
            OrderNumber__c = 4,
            Order_Number_Displayed__c = '4',
            Question__c = 'What is your Name?',
            Required__c = True,
            Choices__c = '1 \n 2\n 3',
            Type__c = 'Free Text'
        ));
        questions.add(new Survey_Question_CRM__c(
            Survey__c = survey.Id,
            OrderNumber__c = 5,
            Order_Number_Displayed__c = '5',
            Question__c = 'What is your Name?',
            Required__c = True,
            Choices__c = '1 \n 2\n 3',
            Type__c = ' Free Text - Single Row Visible'
        ));
        questions.add(new Survey_Question_CRM__c(
            Survey__c = survey.Id,
            OrderNumber__c = 6,
            Order_Number_Displayed__c = '6',
            Question__c = 'What is your Name?',
            Required__c = True,
            Choices__c = '1 \n 2\n 3',
            Type__c = 'Picklist'
        ));
        questions.add(new Survey_Question_CRM__c(
            Survey__c = survey.Id,
            OrderNumber__c = 7,
            Order_Number_Displayed__c = '7',
            Question__c = 'What is your Name?',
            Choices__c = '1 \n 2\n 3',
            Type__c = 'Rating'
        ));
        questions.add(new Survey_Question_CRM__c(
            Survey__c = survey.Id,
            OrderNumber__c = 8,
            Order_Number_Displayed__c = '8',
            Question__c = 'What is your Name?',
            Choices__c = '1 \n 2\n 3',
            Type__c = 'Section'
        ));
        insert questions;

        controller = new CustomerSatisfactionSurveyController();

        System.debug('controller.questions = ' + controller.questions);

        controller.questions[0].selectedOption = '';
        controller.submitSurvey();
        controller.questions[0].selectedOption = '1';

        controller.questions[1].selectedOption = '';
        controller.submitSurvey();
        controller.questions[1].selectedOption = '2';

        controller.questions[2].selectedOptions = new List<String>();
        controller.submitSurvey();
        controller.questions[2].selectedOptions = new List<String>{'1'};

        controller.questions[3].choices = '';
        controller.submitSurvey();
        controller.questions[3].choices = '1';

        controller.questions[4].choices = '';
        controller.submitSurvey();
        controller.questions[4].choices = '1';

        controller.questions[5].selectedOption = '';
        controller.submitSurvey();
        controller.questions[5].selectedOption = '1';

        controller.questions[6].selectedOption = '';
        controller.submitSurvey();
        controller.questions[6].selectedOption = '1';


        controller.questions[1].choiceForAdditionalTextbox = 'Test';
        controller.questions[1].selectedOption = '2';
        controller.questions[2].selectedOptions = new List<String>{'1'};
        //controller.questions[3].renderFreeText = true ;
        controller.questions[3].choices = '1';
        controller.questions[4].choices = '1';
        controller.questions[5].selectedOption = '1';
        controller.questions[6].selectedOption = '1';
        controller.questions[7].selectedOption = 'Test';

        controller.submitSurvey();
    }

    @isTest
    private static void testSearchBookingUnit() {
        System.assert(CustomerSatisfactionSurveyController.searchBookingUnit('test').isEmpty());
    }

}