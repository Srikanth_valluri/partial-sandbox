/*
* Description - Test class developed for 'CODCaseTriggerHandler'
*
* Version            Date            Author            Description
* 1.0                20/11/17        Snehil            Initial Draft
*/
@isTest
public class CODCaseTriggerHandlerTest {
    /*static testMethod void CODtestMethod1() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Type = 'Change of Contact Details';
        objCase.IsPOA__c = false;
        objCase.OCR_verified__c = true;
        objCase.OQOOD_Fee_Applicable__c = false;
        objCase.Approval_Status__c = 'Approved' ;
        objCase.Status = 'Working';
        //insert objCase;
        test.StartTest();
        
        SOAPCalloutServiceMock.returnToMe  = new Map<String,actionCom.UpdateCOCDResponse_element >();
        actionCom.UpdateCOCDResponse_element  obj =  new actionCom.UpdateCOCDResponse_element ();
        obj.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',obj);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());

        insert objCase;
        test.StopTest();
    }
    
    static testMethod void PDUtestMethod1() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Passport Detail Update').getRecordTypeId();
        
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Type = 'Passport Detail Update SR';
        objCase.Passport_Issue_Date__c = system.today();
        objCase.IsPOA__c = false;
        objCase.OCR_verified__c = true;
        objCase.OQOOD_Fee_Applicable__c = false;
        objCase.Approval_Status__c = 'Approved' ;
        objCase.Status = 'Working';

        test.StartTest();
        
        SOAPCalloutServiceMock.returnToMe  = new Map<String,actionCom.UpdateCOCDResponse_element >();
        actionCom.UpdateCOCDResponse_element  obj =  new actionCom.UpdateCOCDResponse_element ();
        obj.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',obj);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());

        insert objCase;
        test.StopTest(); 
    }*/
    
    static testMethod void NNCtestMethod1() {
    
        /*Profile creProfile = [SELECT Id FROM Profile WHERE Name='Non Elite - CRE'];
        User creUser = TestDataFactory_CRM.createUser('Manager', creProfile.Id, null);

        insert creUser;
        system.debug('User details == '+ [SELECT ManagerId,Id from User Where Id =:creUser.id ] );

        System.runAs( creUser ) {*/
            // Insert Accont
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc ;
            
            Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Name Nationality Change').getRecordTypeId();
            
            // insert Case
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
            objCase.Approval_Status__c = 'Approved' ;
            objCase.Status = 'Working';
            objCase.Type = 'Name Nationality Change SR';
            /*
            objCase.Passport_Issue_Date__c = system.today();
            objCase.IsPOA__c = false;
            objCase.OCR_verified__c = true;
            objCase.OQOOD_Fee_Applicable__c = false;
            objCase.Is_Court_Order_Uploaded__c = false;
            objCase.Approving_Authorities__c = null;
            objCase.Submit_for_Approval__c = false;
            objCase.Status = 'Working';*/
            system.debug('objCase======= '+ objCase);
            insert objCase;
            update objCase;
            //System.debug('Case Oqner Id========== '+ [Select OwnerId from Case where id =:objCase.Id ]);
            test.startTest();
            
            SOAPCalloutServiceMock.returnToMe  = new Map<String,NameAndNationalityService.NameAndNationalityProcessResponse_element>();
            NameAndNationalityService.NameAndNationalityProcessResponse_element obj =  new NameAndNationalityService.NameAndNationalityProcessResponse_element();
            obj.return_x = 'S~Successfully Updated.';
            SOAPCalloutServiceMock.returnToMe.put('response_x',obj);
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
    
            //insert objCase;
            test.stopTest(); 
            //insert objCase;
        //}
    }
}