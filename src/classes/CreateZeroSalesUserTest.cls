@isTest 
public class CreateZeroSalesUserTest {
  
    @isTest static void testMethod2 (){
        Profile profileObjNew = [SELECT Id FROM Profile WHERE Name = 'Director of Sales' LIMIT 1];
        Profile profileObjHOS = [SELECT Id FROM Profile WHERE Name = 'Head of Sales' LIMIT 1];
        Profile profileObj = [SELECT Id FROM Profile WHERE Name = 'Property Consultant' LIMIT 1];
        User userObjHOS1 = new User(
            Alias = 'stand', 
            Email = 'userNewttt@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObjHOS.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfdtttNew@testorg.com'
        );
        insert userObjHOS1;
        User userObjHOS = new User(
            Alias = 'stand', 
            Email = 'userNewtt@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObjNew.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfdttNew@testorg.com',
            managerId = userObjHOS1.id
        );
        insert userObjHOS;
        User userObjNew = new User(
            Alias = 'stand', 
            Email = 'userNew@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObjNew.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfdNew@testorg.com',
            managerId = userObjHOS.id
        );
        insert userObjNew;
        
        User userObj = new User(
            Alias = 'stand', 
            Email = 'user@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObj.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfd@testorg.com',
            managerId = userObjNew.id
        );
        insert userObj;
        Zero_Sales_User__c zsObj = new Zero_Sales_User__c();
        zsObj.Employee__c = userObj.id;
        zsObj.HOS_Descision__c = 'Pending';
        zsObj.DOS_Email_Id__c = 'test@testt.com';
        zsObj.HOS_Email_Id__c = 'tt@test.com';
        insert zsObj;
        
        userObj.Email = 'userNewTest@testorg.com';
        userObj.managerId = userObjHOS.id;
        update userObj; 
        
        DateTime todaysDateTime = datetime.now();
        String lastMonthName = todaysDateTime.addmonths(-1).format('MMMMM');
        String secondLastMonthName = todaysDateTime.addmonths(-2).format('MMMMM');    
        Target__c targetObjMonth1 = new Target__c(
            Target__c = 456,
            User__c = userObj.Id,
            Month__c = lastMonthName,
            Year__c = String.valueOf(System.today().year())
        );
        insert targetObjMonth1;

        Target__c targetObjMonth2 = new Target__c(
            Target__c = 123,
            User__c = userObj.Id,
            Month__c = secondLastMonthName,
            Year__c = String.valueOf(System.today().year())
        );
        insert targetObjMonth2;

        Test.startTest();
        ScheduleUserActionPlan schedulerInstance = new ScheduleUserActionPlan();
        String cronStr = '0 0 23 * * ?'; 
        system.schedule('Test ScheduleUserActionPlan', cronStr, schedulerInstance); 
        CreateActionPlanBatch createPlan = new CreateActionPlanBatch();
        ID batchprocessid = Database.executeBatch(createPlan, 200);
        Test.stopTest();
         list<Zero_Sales_User__c> zsList = [SELECT
                                          		id
                                                ,HOS_Email_Id__c
                                           		,DOS_Email_Id__c
                                           FROM
                                          		Zero_Sales_User__c] ;
        System.assertEquals(zsList[0].DOS_Email_Id__c ,'usernewtt@testorg.com');
        System.assertEquals(zsList[0].HOS_Email_Id__c ,'usernewttt@testorg.com');
    } 
    @isTest static void testMethod3 (){
        Profile profileObjNew = [SELECT Id FROM Profile WHERE Name = 'Director of Sales' LIMIT 1];
        Profile profileObjHOS = [SELECT Id FROM Profile WHERE Name = 'Head of Sales' LIMIT 1];
        Profile profileObj = [SELECT Id FROM Profile WHERE Name = 'Property Consultant' LIMIT 1];
        User userObjHOS1 = new User(
            Alias = 'stand', 
            Email = 'userNewttt@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObjHOS.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfdtttNew@testorg.com'
        );
        insert userObjHOS1;
        User userObjHOS = new User(
            Alias = 'stand', 
            Email = 'userNewtt@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObjNew.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfdttNew@testorg.com',
            managerId = userObjHOS1.id
        );
        insert userObjHOS;
        User userObjNew = new User(
            Alias = 'stand', 
            Email = 'userNew@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObjNew.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfdNew@testorg.com',
            managerId = userObjHOS.id
        );
        insert userObjNew;
        
        User userObj = new User(
            Alias = 'stand', 
            Email = 'user@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObj.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfd@testorg.com',
            managerId = userObjNew.id
        );
        insert userObj;
        Zero_Sales_User__c zsObj = new Zero_Sales_User__c();
        zsObj.ownerId = userObj.id;
        zsObj.HOS_Descision__c = 'Pending';
        zsObj.HOS_Email_Id__c = 'test@testt.com';
        insert zsObj;
        
        userObj.Email = 'userNewTest@testorg.com';
        userObj.managerId = userObjHOS.id;
        update userObj; 
        
        DateTime todaysDateTime = datetime.now();
        String lastMonthName = todaysDateTime.addmonths(-1).format('MMMMM');
        String secondLastMonthName = todaysDateTime.addmonths(-2).format('MMMMM');    
        Target__c targetObjMonth1 = new Target__c(
            Target__c = 456,
            User__c = userObj.Id,
            Month__c = lastMonthName,
            Year__c = String.valueOf(System.today().year())
        );
        insert targetObjMonth1;

        Target__c targetObjMonth2 = new Target__c(
            Target__c = 123,
            User__c = userObj.Id,
            Month__c = secondLastMonthName,
            Year__c = String.valueOf(System.today().year())
        );
        insert targetObjMonth2;

        Test.startTest();
        ScheduleUserActionPlan schedulerInstance = new ScheduleUserActionPlan();
        String cronStr = '0 0 23 * * ?'; 
        system.schedule('Test ScheduleUserActionPlan', cronStr, schedulerInstance); 
        CreateActionPlanBatch createPlan = new CreateActionPlanBatch();
        ID batchprocessid = Database.executeBatch(createPlan, 200);
        Test.stopTest();
        list<Zero_Sales_User__c> zsList = [SELECT
                                          		id
                                           		,HOS_Email_Id__c
                                           		,DOS_Email_Id__c
                                           FROM
                                          		Zero_Sales_User__c] ;
         System.assertEquals(zsList[0].DOS_Email_Id__c ,Null);
        System.assertEquals(zsList[0].HOS_Email_Id__c ,'usernewtt@testorg.com');

    }
}