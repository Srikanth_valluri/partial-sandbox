/******************************************************************************
* Description - Test class developed for TaskTriggerHandlerEarlyHandover
*
* Version            Date            Author                    Description
* 1.0                18/12/17        Naresh Kaneriya (Accely)   Initial Draft
********************************************************************************/

@isTest 
public class TaskTriggerHandlerEarlyHandoverTest{


 @isTest static void getTest(){
   
     Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('NOC For Visa').getRecordTypeId();
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
     
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Type = 'NOCVisa';
        Cas.Lease_End_Date__c = system.today();
        Cas.Lease_Commencement_Date__c = system.today();
        Cas.Security_Cheque_Amount__c = 4000;
        Cas.Completed_Milestones_Percent__c = 98;
        Cas.Monthly_Rent__c = 3000;
        Cas.Parking_Details_JSON__c = '[{"TERM_ID":"70315","REGISTRATION_ID":"74365","PAID_PERCENTAGE":"100","PAID_AMOUNT":"523699.2","MILESTONE_EVENT_AR":"فورا","MILESTONE_EVENT":"Immediate","MILESTEON_PERCENT_VALUE":"24","LINE_ID":"152486","INVOICE_AMOUNT":"523699.2","INSTALLMENT":"DP","DUE_DATE":"21/11/2015","DUE_AMOUNT":"0","DESCRIPTION":"DEPOSIT"},'+
                                     '{"TERM_ID":"70315","REGISTRATION_ID":"74365","PAID_PERCENTAGE":"100","PAID_AMOUNT":null,"MILESTONE_EVENT_AR":null,"MILESTONE_EVENT":null,"MILESTEON_PERCENT_VALUE":"0","LINE_ID":"152487","INVOICE_AMOUNT":null,"INSTALLMENT":"I001","DUE_DATE":"","DUE_AMOUNT":"0","DESCRIPTION":"1ST INSTALLMENT"},'+
                                     '{"TERM_ID":"70315","REGISTRATION_ID":"74365","PAID_PERCENTAGE":"100","PAID_AMOUNT":"261849.6","MILESTONE_EVENT_AR":"خلال 180 يوما من تاريخ البيع","MILESTONE_EVENT":"Within 180 Days of Sale Date","MILESTEON_PERCENT_VALUE":"10","LINE_ID":"152488","INVOICE_AMOUNT":"261849.6","INSTALLMENT":"I002","DUE_DATE":"19/5/2016","DUE_AMOUNT":"0","DESCRIPTION":"2ND INSTALLMENT"},'+
                                     '{"TERM_ID":"70315","REGISTRATION_ID":"74365","PAID_PERCENTAGE":"100","PAID_AMOUNT":"261849.6","MILESTONE_EVENT_AR":"خلال 360 يوما من تاريخ البيع","MILESTONE_EVENT":"Within 360 days of Sale Date","MILESTEON_PERCENT_VALUE":"10","LINE_ID":"152489","INVOICE_AMOUNT":"261849.6","INSTALLMENT":"I003","DUE_DATE":"15/11/2016","DUE_AMOUNT":"0","DESCRIPTION":"3RD INSTALLMENT"},'+
                                     '{"TERM_ID":"70315","REGISTRATION_ID":"74365","PAID_PERCENTAGE":"100","PAID_AMOUNT":"261849.6","MILESTONE_EVENT_AR":"خلال 420 يوما من تاريخ البيع","MILESTONE_EVENT":"Within 420 days of Sale Date","MILESTEON_PERCENT_VALUE":"10","LINE_ID":"152490","INVOICE_AMOUNT":"261849.6","INSTALLMENT":"I004","DUE_DATE":"14/1/2017","DUE_AMOUNT":"0","DESCRIPTION":"4TH INSTALLMENT"},'+
                                     '{"TERM_ID":"70315","REGISTRATION_ID":"74365","PAID_PERCENTAGE":"100","PAID_AMOUNT":"261849.6","MILESTONE_EVENT_AR":"خلال 540 يوما من تاريخ البيع","MILESTONE_EVENT":"Within 540 days of Sale Date","MILESTEON_PERCENT_VALUE":"10","LINE_ID":"152491","INVOICE_AMOUNT":"261849.6","INSTALLMENT":"I005","DUE_DATE":"14/5/2017","DUE_AMOUNT":"0","DESCRIPTION":"5TH INSTALLMENT"},'+
                                     '{"TERM_ID":"70315","REGISTRATION_ID":"74365","PAID_PERCENTAGE":"100","PAID_AMOUNT":"261849.6","MILESTONE_EVENT_AR":"خلال 630 يوما من تاريخ البيع","MILESTONE_EVENT":"Within 630 days of Sale Date","MILESTEON_PERCENT_VALUE":"10","LINE_ID":"152492","INVOICE_AMOUNT":"261849.6","INSTALLMENT":"I006","DUE_DATE":"12/8/2017","DUE_AMOUNT":"0","DESCRIPTION":"6TH INSTALLMENT"},'+
                                     '{"TERM_ID":"70315","REGISTRATION_ID":"74365","PAID_PERCENTAGE":"100","PAID_AMOUNT":"261849.6","MILESTONE_EVENT_AR":"خلال 720 يوما من تاريخ البيع","MILESTONE_EVENT":"Within 720 days of Sale Date","MILESTEON_PERCENT_VALUE":"10","LINE_ID":"152493","INVOICE_AMOUNT":"261849.6","INSTALLMENT":"I007","DUE_DATE":"10/11/2017","DUE_AMOUNT":"0","DESCRIPTION":"7TH INSTALLMENT"},'+
                                     '{"TERM_ID":"70315","REGISTRATION_ID":"74365","PAID_PERCENTAGE":"0","PAID_AMOUNT":"0","MILESTONE_EVENT_AR":"خلال 900 يوما من تاريخ البيع","MILESTONE_EVENT":"Within 900 days of Sale Date","MILESTEON_PERCENT_VALUE":"10","LINE_ID":"152494","INVOICE_AMOUNT":"261849.60","INSTALLMENT":"I008","DUE_DATE":"01/08/2018","DUE_AMOUNT":"261849.60","DESCRIPTION":"8TH INSTALLMENT"},'+
                                     '{"TERM_ID":"70315","REGISTRATION_ID":"74365","PAID_PERCENTAGE":"0","PAID_AMOUNT":"0","MILESTONE_EVENT_AR":"خلال 1080 يوما من تاريخ البيع","MILESTONE_EVENT":"Within 1080 days of Sale Date","MILESTEON_PERCENT_VALUE":"10","LINE_ID":"152495","INVOICE_AMOUNT":"261849.60","INSTALLMENT":"I009","DUE_DATE":"01/12/2018","DUE_AMOUNT":"261849.60","DESCRIPTION":"9TH INSTALLMENT"}]';
        insert Cas;
        System.assert(Cas != null);
         
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
        
        Task tsk = new Task();
        tsk.WhatId = Cas.Id;
        tsk.ActivityDate = System.today()+2;
        tsk.Subject = 'Generate EHO docs';
        tsk.Status = 'Not Started';
        tsk.Assigned_User__c = 'Finance'; 
        tsk.Status = 'Not Started';
        tsk.Process_Name__c = 'Early Handover';
        insert tsk ;  
        
        list<Task> lstTask =  new list<Task>();
        lstTask.add(tsk);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();
        TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response1 = new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
        response1.return_x = '{'+
        ''+
        '"message":"message",'+
        '"status":"E"'+
        '}';
       SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
       Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
       new TaskTriggerHandlerEarlyHandover().taskForExternalUser(lstTask);     
       Test.stopTest();
    
 }
}