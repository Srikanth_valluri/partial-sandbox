/* * * * * * * * * * * * * *
*  Class Name:   AOPTDocumentGenerationTest
*  Purpose:      Unit test class for AOPTDocumentGeneration 
*  Author:       Hardik Mehta - ESPL
*  Company:      ESPL
*  Created Date: 21-Nov-2017
*  Type:         Test Class
* * * * * * * * * * * * */
@isTest
public class AOPTDocumentGenerationTest{
  /* * * * * * * * * * * * *
  *  Method Name:  testWSDL
  *  Purpose:      This method is used to check WSDL
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */  
    public static testmethod void testWSDL(){
        AOPTDocumentGeneration.DocGenerationResponse_element objResponse = 
        new AOPTDocumentGeneration.DocGenerationResponse_element();
    
        AOPTDocumentGeneration.DocGeneration_element objGeneration =
        new AOPTDocumentGeneration.DocGeneration_element();
    
        AOPTDocumentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint objEndpoint =
        new AOPTDocumentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint();
        AOPTDocumentGenerationXsd.DocGenDTO objDocAttribute = new AOPTDocumentGenerationXsd.DocGenDTO();
            objDocAttribute.Attribute1 = String.valueOf(Date.today());
            objDocAttribute.Attribute2 = 'Test';
            objDocAttribute.Attribute3 = 'Executive Heights, Barsha Heights, Dubai, UAE';
            objDocAttribute.Attribute4 = 'P.O.Box: 2195, Phone: 971.4.3731000';
            objDocAttribute.Attribute5 = 'Subject: Payment Plan Revision';
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        objEndpoint.DocGeneration('ABC',objDocAttribute);
    }
}