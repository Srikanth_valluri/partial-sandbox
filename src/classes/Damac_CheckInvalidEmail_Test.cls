@isTest
public class Damac_CheckInvalidEmail_Test {
    
    static testmethod void doTest () {
        Test.startTest ();
        Inquiry_Checks__c rec = new Inquiry_Checks__c ();
        rec.Email__c = 'no@test.com';
        insert rec;
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/getInvalidEmails'; 
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        Damac_CheckInvalidEmail.doGet ();
        Test.stopTest ();
    }
    static testmethod void doTest2 () {
        Test.startTest ();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/getInvalidEmails'; 
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        Damac_CheckInvalidEmail.doGet ();
        Test.stopTest ();
    }
}