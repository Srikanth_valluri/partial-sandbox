/**
 * @File Name          : FmcUtils.cls
 * @Description        :
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/28/2019, 5:11:00 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    7/29/2019, 2:26:25 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public without sharing class FmcUtils {

    public  static final String         GUEST_USER_NAME         = 'Community Portal Site Guest User';
    public  static final Set<String>    CLOSED_CASE_STATUSES    = new Set<String>{'Closed', 'Rejected'};

    public static Boolean isOwner       = NULL;
    public static Boolean isTenant      = NULL;
    public static Boolean isBoardMember = NULL;


    public static final String         LOCATION_FIELDS         =
            'Id,Name,RecordTypeId,RecordType.Name,Building_Name__c,Location_ID__c,Location_Code__c,Location_Type__c';
    public static final String         UNIT_FIELDS             =
            'Inventory__c, Inventory__r.Building_Location__c, Inventory__r.Building_Location__r.Id, Name,Unit_Name__c,Property_Name__c,Property_City__c,Property_Country__c,Booking__c,Booking__r.Id,'
            + 'Booking__r.Account__c,Tenant__c,Resident__c,Amount_Paid__c,Anticipated_Completion_Date__c,Area__c,'
            + 'Area_sft__c,Bedroom_Type__c,Booking_Name__c,Completion_Date__c,Construction_RERA__c,'
            + 'Construction_Status__c,DEWA_Utility_Number__c,District__c,DLD_registration__c,DLP_End_Date__c,'
            + 'DLP_Start_date__c,DLP_Validity__c,DP_OK__c,DP_Overdue__c,Early_Handover__c,Email__c,'
            + 'Early_Handover_Date__c,Empower_Utility_Number__c,Expected_date_of_completion_of_Snags__c,'
            + 'Empower_registration_Date__c,External_Parking__c,Finance_Flag__c,Floor_Plan__c,FM_Email__c,FM_PCC__c,'
            + 'Handover_Complete__c,Handover_Date__c,Handover_Flag__c,IPMS_Status__c,Is_Siebel_Migrated_Data__c,'
            + 'Last_Payment_Date__c,Loams_Email__c,OQOOD_Reg_Flag__c,Overdues_as_per_Collectable_amount__c,'
            + 'Owner__r.Name,Owner__r.Party_ID__c,Owner__r.Party_Type__c,PCC_Date__c,Registration_ID__c,'
            + 'Registration_Status__c,Registration_Status_Code__c,Requested_Price__c,Requested_Price_AED__c,'
            + 'RERA_Project__c,Resident__r.Name,Resident__r.Party_ID__c,Resident__r.Party_Type__c,Status__c,'
            + 'Tenant__r.Name,Tenant__r.Party_ID__c,Tenant__r.Party_Type__c,Total_Collections__c,Total_due_Overdue__c,'
            + 'Total_Snags_Closed__c,Total_Snags_Open__c,Unique_Key__c,Unit_Location__c';


    public static Boolean isCurrentView(String view) {
        String currentView = ApexPages.currentPage().getParameters().get('view');
        if (String.isBlank(currentView)) {
            currentView = '';
        }
        return view.equalsIgnoreCase(currentView);
    }

    public static Boolean isOwner() {
        if (isOwner == NULL) {
            isOwner = ![ SELECT  Id
                         FROM    Booking_Unit__c
                         WHERE   Booking__r.Account__c = :CustomerCommunityUtils.customerAccountId
                         LIMIT   1 ].isEmpty();
        }
        return isOwner;
    }

    public static Boolean isTenant() {
        if (isTenant == NULL) {
            isTenant = ![ SELECT  Id
                          FROM    Booking_Unit__c
                          WHERE   Tenant__c = :CustomerCommunityUtils.customerAccountId
                          LIMIT   1 ].isEmpty();
        }
        return isTenant;
    }

    public static Boolean isBoardMember() {
        if (isBoardMember == NULL) {
            isBoardMember = ![ SELECT  Id
                               FROM    Board_Member__c
                               WHERE   Board_Member__c = :CustomerCommunityUtils.customerAccountId
                               LIMIT   1 ].isEmpty();
        }
        return isBoardMember;
    }

    public static String formatAsIpmsDate(Date sfDate) {
        if (sfDate == NULL) {
            return NULL;
        }
        return formatAsIpmsDate(Datetime.newInstance(sfDate.year(), sfDate.month(), sfDate.day()));
    }

    public static String formatAsIpmsDate(Datetime sfDate) {
        if (sfDate == NULL) {
            return NULL;
        }
        return sfDate.format('dd-MMM-yyyy').toUpperCase();
    }

    public static List<Location__c> queryLocationsForAccount(Id accountId) {
        return queryLocationsForAccount(accountId, LOCATION_FIELDS);
    }

    public static List<Location__c> queryLocationsForAccount(Id accountId, String fields) {
        Map<Id, Location__c> mapLocation = new Map<Id, Location__c>();
        String unitFields = 'Id,Inventory__c,Inventory__r.Building_Location__c,';
        for (String field : fields.split(',')) {
            unitFields += 'Inventory__r.Building_Location__r.' + field.trim() + ',';
        }
        if (!unitFields.containsIgnoreCase('Inventory__r.Building_Location__r.Id')) {
            unitFields += 'Inventory__r.Building_Location__r.Id';
        } else {
            unitFields = unitFields.removeEndIgnoreCase(',');
        }

        for (Booking_Unit__c unit : queryUnitsForAccount(accountId, unitFields)) {
            if (unit.Inventory__c == NULL  || unit.Inventory__r.Building_Location__c == NULL) {
                continue;
            }
            Location__c location = unit.Inventory__r.Building_Location__r;
            mapLocation.put(location.Id, location);
        }
        return mapLocation.values();
    }

    public static List<Booking_Unit__c> queryOwnedUnitsForAccount(Id accountId) {
        return queryOwnedUnitsForAccount(accountId, UNIT_FIELDS);
    }

    public static List<Booking_Unit__c> queryOwnedUnitsForAccount(Id accountId, String fields) {
        return queryOwnedUnitsForAccount(accountId, fields, '');
    }

    public static List<Booking_Unit__c> queryOwnedUnitsForAccount(Id accountId, String fields, String filter) {
        return queryUnitsForAccount(accountId, new Set<String>{'Booking__r.Account__c'}, fields, filter);
    }

    public static List<Booking_Unit__c> queryLeasedUnitsForAccount(Id accountId) {
        return queryLeasedUnitsForAccount(accountId, UNIT_FIELDS);
    }

    public static List<Booking_Unit__c> queryLeasedUnitsForAccount(Id accountId, String fields) {
        return queryLeasedUnitsForAccount(accountId, fields, '');
    }

    public static List<Booking_Unit__c> queryLeasedUnitsForAccount(Id accountId, String fields, String filter) {
        return queryUnitsForAccount(accountId, new Set<String>{'Tenant__c'}, fields, filter);
    }

    public static List<Booking_Unit__c> queryUnitsForAccount(Id accountId) {
        return queryUnitsForAccount(accountId, UNIT_FIELDS, '');
    }

    public static List<Booking_Unit__c> queryUnitsForAccount(Id accountId, String fields) {
        return queryUnitsForAccount(accountId, fields, '');
    }

    public static List<Booking_Unit__c> queryUnitsForAccount(Id accountId, String fields, String filter) {
        Set<String> allRelatedFields = new Set<String> {'Booking__r.Account__c', 'Tenant__c', 'Resident__c'};
        return queryUnitsForAccount(accountId, allRelatedFields, fields, filter);
    }

    public static List<Booking_Unit__c> queryUnitsForAccount(
        Id accountId, Set<String> relationToUnit, String fields, String filter
    ) {
        Set<String> setActiveStatuses = Booking_Unit_Active_Status__c.getAll().keySet();

        Set<String> unitFields = new Set<String>();
        for (String field : fields.toLowerCase().split(',')) {
            unitFields.add(field.trim());
        }

        if (!unitFields.contains('id')) {
            fields += ',Id';
        }

        Set<String> invalidHandoverStatus = new Set<String> {NULL, ''};
        invalidHandoverStatus.addAll(Label.FM_Booking_Unit_Handover_Status.split(','));

        String query = ' SELECT ' + fields + ' FROM Booking_Unit__c '
                     + ' WHERE  (Dummy_Booking_Unit__c = true '
                     + ' OR (Handover_Status__c NOT IN :invalidHandoverStatus '
                     + ' AND ((Handover_Flag__c = \'Y\' OR Early_Handover__c = true)'
                        + ' AND Registration_Status__c IN :setActiveStatuses))) ';

        String strAccountId = ' \'' + accountId + '\' ';

        query += ' AND (';
        for (String relatedField : relationToUnit) {
            query += (' ' + relatedField + ' = ' + strAccountId + ' OR ');
        }
        query = query.removeEnd(' OR ');
        query += ' ) ';

        query += (String.isBlank(filter) ? '' : (' AND ' + filter));
        System.debug('units query = ' + query);
        return Database.query(query);
    }

}