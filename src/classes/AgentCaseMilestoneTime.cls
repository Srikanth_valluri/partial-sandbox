global class AgentCaseMilestoneTime implements Support.MilestoneTriggerTimeCalculator {   
     global Integer calculateMilestoneTriggerTime(String caseId, String milestoneTypeId){
         SYSTEM.DEBUG('milestoneTypeId'+milestoneTypeId);
        Case c = [SELECT id,Status FROM Case WHERE Id=:caseId];
        
        MilestoneType mt = [SELECT Name FROM MilestoneType WHERE Id=:milestoneTypeId];
        SYSTEM.DEBUG(mt.Name);
        if (c.status!= null && c.status.equals('Pending with other party')){
              if (mt.Name != null && mt.Name.equals('Agent Case Resolution Pending on Other Party')) { 
                  return integer.valueof(label.Agent_Case_Milestone_Pending_on_Other_Party);
              }
              else { 
                  return 5; 
              }
        }
        else {
            return 18;
        }                
     }
}