public class EscalateSocialMediaBatchScheduler Implements Schedulable  {
     public void execute(SchedulableContext SC){
        System.debug('inside scheduler');
        EscalateSocialMediaBatch batchInst = new EscalateSocialMediaBatch();
        Database.executeBatch(batchInst,50);
    }
}