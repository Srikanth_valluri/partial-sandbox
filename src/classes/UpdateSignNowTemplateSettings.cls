/****************************************************************************************
Description: Class to create SignNow Template Setting with dynamic recipients on click
			 of Send With SignNow Button on Case.
----------------------------------------------------------------------------------------*
Version     Date         Author              Description                                *
1.0        19/08/2020    Akshata Anvekar    Initial Draft                               *
*****************************************************************************************/
global class UpdateSignNowTemplateSettings {
    public String strRecordId;
    public List<Buyer__c> lstBuyers;
    //public final String SIGNNOW_URL = System.Label.Send_With_SignNow_Url;
    public final String EMAIL_SUBJECT = System.Label.SignNow_Email_Subject;
    public final String EMAIL_BODY = System.Label.SignNow_Email_Body;
    
	public UpdateSignNowTemplateSettings(ApexPages.StandardController objController){
    	strRecordId = objController.getId();
        List<Case> lstCase = [Select Booking_Unit__r.Booking__c
                                From Case
                               Where Id =:strRecordId];
        lstBuyers = new List<Buyer__c>();
        lstBuyers = [Select Id
               		, Email__c
               		, Primary_Buyer__c
                    , Account__r.Person_Business_Email__c
                 From Buyer__c
                Where Booking__c =: lstCase[0].Booking_Unit__r.Booking__c
             Order By Name ASC];
        System.debug('####--->lstBuyers'+lstBuyers);
    }
    
     /* method to update recipients Json in 
      * SignNow template settings
      * Return Type : pageReference
      */
    public pageReference updateRecipients(){
    	Integer intJb = 1;
        Integer intOrder = 1;
        String strRolejson = '[';
        String strPbEmail = '';
        
        for(Buyer__c objBuyer : lstBuyers){
        	if(objBuyer.Primary_Buyer__c){
                strPbEmail = objBuyer.Account__r.Person_Business_Email__c;
            	strRolejson += '{"order":"1", "name":"Primary Buyer", "email":"'+strPbEmail+'","allow_forwarding":"yes"}';
                break;
            }   
        }
        
        if(lstBuyers != null && lstBuyers.size() > 1){
         	strRolejson += ',';   
        }
        
        for(Buyer__c objBuyer : lstBuyers){
            if(!objBuyer.Primary_Buyer__c){
                String strJbEmail = String.isNotBlank(objBuyer.Account__r.Person_Business_Email__c)
                    			  ?	objBuyer.Account__r.Person_Business_Email__c
                    			  : strPbEmail;
                if(intJb > 1){
                	strRolejson += ',';   
                }
                intOrder++;
                strRolejson += '{"order":"'+intOrder+'", "name":"Joint Buyer '+intJb+'","email":"'+ strJbEmail+'", "allow_forwarding":"no"}';
                intJb++;
            }
            
        }
        strRolejson += ']';
        
        cuda_signnow__CN_TemplateSetting__c objTemplateSetting = new cuda_signnow__CN_TemplateSetting__c();
        objTemplateSetting.Name = 'HO Checklist';
        objTemplateSetting.cuda_signnow__Roles__c = strRolejson;
        objTemplateSetting.cuda_signnow__subject__c = EMAIL_SUBJECT;
        objTemplateSetting.cuda_signnow__Body__c = EMAIL_BODY;
        objTemplateSetting.cuda_signnow__Automatically_Send__c = true;
        objTemplateSetting.cuda_signnow__Skip_to_End__c = true;
        objTemplateSetting.cuda_signnow__Hide_Adv_Set__c = true;
        
        System.debug('###--->strRolejson'+strRolejson);
        
        Database.SaveResult[] srList = Database.insert(new List<cuda_signnow__CN_TemplateSetting__c>{objTemplateSetting}, false);
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()){
                //String strReturnUrl = SIGNNOW_URL+strRecordId+'&type=case&view=attachments&template_setting='+sr.getId()+'&redirect=true';
                String strReturnUrl = formSignNowBaseUrl()+strRecordId+'&type=case&view=attachments&template_setting='+sr.getId()+'&redirect=true';
                System.debug('Successfully inserted Template Seting ' + sr.getId());
                PageReference pg = new PageReference(strReturnUrl);
                pg.setRedirect(false);
                return pg;
            }else{
                return null;
            }
        }
        
        return null;
    }
    
    /* method to form SignNow base url 
     * Return Type : String
     */
    public String formSignNowBaseUrl(){
    	String sfdcBaseURL = Url.getOrgDomainUrl().toExternalForm();
        String strOrgDomain = sfdcBaseURL.removeEndIgnoreCase('.my.salesforce.com');
        Organization objInstance = [SELECT InstanceName FROM Organization];
        
        String strUrl = strOrgDomain+'--cuda-signnow.';
        
        if(objInstance!=null){
        	strUrl = strUrl+ objInstance.InstanceName; 
        }
        strUrl = strUrl+'.visual.force.com/apex/signnow_templates?id=';
        
        return strUrl;
    }
}