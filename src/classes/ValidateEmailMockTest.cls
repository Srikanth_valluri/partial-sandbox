@isTest
global class ValidateEmailMockTest implements HttpCalloutMock {
     global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"address":"craig.lobo@eternussolutions.com","status":"Valid","sub_status":"","account":"craig.lobo","domain":"eternussolutions.com","disposable":false,"toxic":false,"firstname":null,"lastname":null,"gender":null,"location":null,"creationdate":null,"processedat":"2018-01-22 06:10:15.382"}');
        res.setStatusCode(200);
        return res;
     }  
}