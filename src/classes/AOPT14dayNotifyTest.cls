@isTest
public class AOPT14dayNotifyTest {
    
    public static testmethod void testAOPT14stDayNotification() {
        Case objCase = new Case();
        objCase.Account_Email__c = 'test@test.com';
        insert objCase;
        
        AOPT14dayNotify.checkIfAddendumUploaded(new List<Case>{objCase});
    }

}