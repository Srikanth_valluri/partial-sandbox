public class DAMAC_AMEYO_CUSTOMER_JSON{
    public String campaignId;   //1
    public Integer leadId;  //119
    public String sessionId;    //d721-5da761d7-ses-apiuser-0KhZoZQW-1049
    public cls_properties properties;
    public String numAttempts;  //X
    public String status;   //NOT_TRIED
    public cls_customerRecords[] customerRecords;
    public class cls_properties {
        public boolean update_customer;
        public boolean migrate_customer;
    }
    public class cls_customerRecords {
        public String id;   //123
        public String phone1;   //7838735378
        public String name; //Amit4
        
        
        public String campaign_name_text;
        public String ownerid;
        public String ownername;
        public String inquiry_source;
        public String mobilecountrycode2;
        public String mobilecountrycode1;
        public String attribute1;
        public String attribute3;
        public String attribute2;
        
        public String attribute4;
        public String attribute5;
        public String attribute6;
        public String attribute7;
        public String attribute8;
        
        public String wrapupurl;
        public String inquiry_created_date;
        public String DAMAC_JSON_class;
        public String inquiry_number;
        public String nationality;
        public String city;
        public String country_of_residence;
        
        
        /*public String timezone;
        public String last_name;
        
        public String phone2;
        public String phone3;
        public String phone4;
        public String phone5;
        
        public String timezone2;
        public String phone;
        public String first_name;
        public String email;
        public String attribute4;
        public String attribute5;
        public String attribute6;
        public String attribute7;
        public String attribute8;
        public String attribute9;
        public String attribute10;
        public String attribute11;
        public String attribute12;
        public String attribute13;
        public String attribute14;
        public String attribute15;
        public String attribute16;
        public String attribute17;
        public String attribute18;
        public String attribute19;
        public String attribute20;
        public String attribute21;
        public String attribute22;
        public String attribute23;
        public String attribute24;
        public String attribute25;
        public String attribute26;
        public String attribute27;
        public String attribute28;
        public String attribute29;
        */public String attribute30;
        
        
        
        
    }
    public static DAMAC_AMEYO_CUSTOMER_JSON parse(String json){
        return (DAMAC_AMEYO_CUSTOMER_JSON) System.JSON.deserialize(json, DAMAC_AMEYO_CUSTOMER_JSON.class);
    }   
}