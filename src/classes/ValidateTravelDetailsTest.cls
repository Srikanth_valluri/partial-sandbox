@IsTest
public class ValidateTravelDetailsTest {
 
    @IsTest
    public static void test1() {
        
        Id flyinadminProfile = [SELECT Id FROM Profile WHERE Name = 'Flyin Admin'].Id;
        User flyinadminUser = new User(alias = 'test456', email='xyz1@email.com',
            emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
            localesidkey='en_US', profileid = flyinadminProfile , country='United Arab Emirates',
            IsActive =true,timezonesidkey='America/Los_Angeles', username='xyz1@email.com');
       

        Travel_Details__c td = new Travel_Details__c ();
        insert td;
        
        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=td.id;
        insert attach;
        
        System.Test.StartTest();
        System.RunAs(flyinadminUser ) {
            td.Status__c = 'Token Paid';
            update td;
        }
        System.Test.StopTest();
        
        delete td;
        
   }
}