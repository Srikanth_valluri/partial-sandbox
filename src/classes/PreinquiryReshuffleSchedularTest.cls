/**************************************************************************************************
* Name               : PreinquiryReshuffleSchedular                                              *
* Description        : Test class for PreinquiryReshuffleSchedular    class.                      *
* Created Date       : 11/03/2017                                                                 *
* Created By         : Alok Chauhan                                                                  *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         Alok Chauhan    11/03/2017      Initial Draft.                                    *
**************************************************************************************************/
@isTest
private class PreinquiryReshuffleSchedularTest{

    static testMethod void testScheduler() {
      Test.startTest();
        PreinquiryReshuffleSchedular preInqShuffle= new PreinquiryReshuffleSchedular();
      String sch = '0 0 23 * * ?'; 
      system.schedule('Test PreInquiry Reshuffle', sch, preInqShuffle); 
    Test.stopTest();
    }
}// End of class.