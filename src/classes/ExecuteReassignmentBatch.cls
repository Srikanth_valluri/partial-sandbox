global  class ExecuteReassignmentBatch {
    webservice static string executeBatch (ID recordId,string executeon) {
        List <Id> rules = new List <ID> ();
        for (Inquiry_Assignment_Rules__c rule : [SELECT ID FROM Inquiry_Assignment_Rules__c 
                WHERE Active__c = TRUE 
                AND Execute_on__c =:executeon
                AND filter_logic__c != NULL 
                AND ID !=: recordId]) {
                
            rules.add (rule.ID);
        } 
        if(executeon =='Reassign'){       
            Database.executeBatch(new InquiryOwnerReshufflingBatch (rules), 1);
        }
         if(executeon =='Dialing Priority'){       
            Database.executeBatch(new DAMAC_Dialing_List_Priority_Batch(rules), 1);
        }
        return 'Job started.';

                 
    }
}