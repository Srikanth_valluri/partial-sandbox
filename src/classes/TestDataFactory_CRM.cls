@isTest
public class TestDataFactory_CRM {

    /*
     @ Description : To create person account
     @ Return      : Account instance to insert
    */
   public static Account createPersonAccount() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='65432');
        return objAcc;
    }

    public static list<RuleEngineDocuments__c> createRuleDocs(){
        list<RuleEngineDocuments__c> lstRD = new list<RuleEngineDocuments__c>();
        RuleEngineDocuments__c objD1 = new RuleEngineDocuments__c();
        objD1.Name = 'D1';
        objD1.Document_Name__c = 'passportCopyOftheWitness';
        objD1.Customer_Level_Document__c = true;
        objD1.Document_Name_Displayed__c = 'D1';
        lstRD.add(objD1);

        RuleEngineDocuments__c objD2 = new RuleEngineDocuments__c();
        objD2.Name = 'D2';
        objD2.Document_Name__c = 'utilityClearanceChillerChargeFinalInvoice';
        objD2.Document_Name_Displayed__c = 'D2';
        objD2.Handover_Process_Document__c = true;
        lstRD.add(objD2);

        RuleEngineDocuments__c objD3 = new RuleEngineDocuments__c();
        objD3.Name = 'D3';
        objD3.Document_Name__c = 'validOriginalPassportsOfHeirs';
        objD3.Document_Name_Displayed__c = 'D3';
        objD3.Handover_Process_Document__c = true;
        lstRD.add(objD3);
        
        RuleEngineDocuments__c objD4 = new RuleEngineDocuments__c();
        objD4.Name = 'D4';
        objD4.Document_Name__c = 'corporatePoa';
        objD4.Document_Name_Displayed__c = 'D4';
        objD4.Handover_Process_Document__c = true;
        lstRD.add(objD4);
        
        RuleEngineDocuments__c objD5 = new RuleEngineDocuments__c();
        objD5.Name = 'D5';
        objD5.Document_Name__c = 'ifPoaTakingHandoverColatePoaPassportResidence';
        objD5.Document_Name_Displayed__c = 'D5';
        objD5.Handover_Process_Document__c = true;
        lstRD.add(objD5);
        
        return lstRD;
    }
    /*
     @ Description : To create Business account
     @ Return      : Account instance to insert
    */
    public static Account createBusinessAccount() {
        Id businessAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = businessAccRTId, Name='DAMAC Group', Type='Organization', party_ID__C='18545');
        return objAcc;
    }

    /*
     @ Description : To create OTP record
     @ Return      : OTP instance to insert
    */
    public static OTP__c createOTP(Id caseId, String otpNumber, String phoneNumber) {
        OTP__c objOTP = new OTP__c(Case__c = caseId, OTP_Number__c = otpNumber, Phone_Number__c = phoneNumber);
        return objOTP;
    }

    /*
     @ Description : To create person account
     @ Return      : Account instance to insert
    */
    public static Property__c createProperty(){
        Property__c objProp = new Property__c();
        objProp.Name = 'Damac Hills';
        objProp.DIFC__c = false;
        objProp.Property_ID__c = System.currentTimeMillis();
        objProp.CurrencyISoCode = 'AED';
        return objProp;
    }

    public static Inventory__c createInventory(Id propertyId){
        Id recTypeId = Schema.SObjectType.Inventory__c.getRecordTypeInfosByName().get('Unit').getRecordTypeId();
        Inventory__c objInv = new Inventory__c();
        objInv.Property__c = propertyId;
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Country__c = 'UAE';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = '1 BR';
        objInv.Unit__c = 'ABC/ABC/001';
        objInv.RecordTypeId = recTypeId;
        objInv.Property_Code__c = 'ABC';
        return objInv;
    }

    /*
     @ Description : To create customers(Accounts)
     @ Return      : List of accounts
    */
    public static List<Account> createCustomers(Integer counter) {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        List<Account> lstAccounts = new List<Account>();
        for(Integer i = 0; i < counter; i++){
            lstAccounts.add(new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName'+i, LastName='Test LastName'+i, Type='Person'));
        }
        return lstAccounts;
    }
    /*
     @ Description : To create custom setting for active Registration_Status__c values for Booking unit
    */
    public static List<Booking_Unit_Active_Status__c> createActiveFT_CS() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        for(Integer i=0; i<5; i++) {
            lstActiveStatus.add(new Booking_Unit_Active_Status__c(Status_Value__c='Active Status' + i, Name='Active Status' + i));
        }
        lstActiveStatus.add(new Booking_Unit_Active_Status__c(Status_Value__c='Agreement Executed by Damac', Name='Agreement Executed by Damac'));
        return lstActiveStatus;
    }

    /*
     @ Description : To create custom setting for rejected Registration_Status__c values for Booking unit
    */
    public static List<Fund_Transfer_Unit_Status__c> createRejectedFT_CS() {
        List<Fund_Transfer_Unit_Status__c> lstRejectedStatus = new List<Fund_Transfer_Unit_Status__c>();
        for(Integer i=0; i<5; i++) {
            lstRejectedStatus.add(new Fund_Transfer_Unit_Status__c(Status__c='Rejected Status' + i, Name='Rejected Status' + i));
        }
        return lstRejectedStatus;
    }

    /*
     @ Description : To create Service Request for Booking
     @ Parameters  : AccountId
     @ Return      : Service Request to be created
    */
    public static NSIBPM__Service_Request__c createServiceRequest() {
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;

        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c=null;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        return sr;
    }


    /*
     @ Description : To create Bookings for an account
     @ Parameters  : AccountId
     @ Return      : Bookings to be created
    */
    public static List<Booking__c> createBookingForAccount(Id accId, Id dealSRId,Integer counter) {
        List<Booking__c> lstBookings = new List<Booking__c>();
        for(Integer i=0; i<counter; i++) {
            lstBookings.add(new Booking__c(Account__c=accId, Deal_SR__c=dealSRId));
        }
        return lstBookings;
    }


    /*
     @ Description : To create Custom setting records
     @ Parameters  : list<String>
     @ Return      : Custom setting records to be created
    */
    public static List<Booking_Unit_Active_Status__c> createActiveUnitCustomSetting( List<String> lstStatus ) {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        Integer counter = 0;
        for(String strStatus : lstStatus ) {
            Booking_Unit_Active_Status__c objCS = new Booking_Unit_Active_Status__c();
            objCS.Name = 'Test' + counter;
            objCS.Status_Value__c = strStatus ;
            lstActiveStatus.add(objCS);
            counter++;
        }
        return lstActiveStatus;
    }


    /*
     @ Description : To create Booking Units for list of Bookings
     @ Parameters  : list of Bookings
     @ Return      : Booking units to be created
    */
    public static List<Booking_Unit__c> createBookingUnits(List<Booking__c> lstBookings,Integer counter) {
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        Integer j=1;
        for(Booking__c objBooking : lstBookings) {
            for(Integer i=0; i<counter; i++) {
                if(math.mod(i,2) == 0) {
                    lstBookingUnits.add(new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name' + j,
                        Registration_ID__c = String.valueOf((j*1000)+600), Registration_Status__c = 'Active Status'+j, Unit_Selling_Price_AED__c = 100));
                }
                else {
                    lstBookingUnits.add(new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name' + j,
                        Registration_ID__c = String.valueOf((j*1000)+600), Registration_Status__c = 'Rejected Status'+j,Unit_Selling_Price_AED__c = 100));
                }

                j++;
            }
        }

        return lstBookingUnits;
    }

    /*
     @ Description : To create Fund Transfer Case
     @ Parameters  : AccountId and record type id
     @ Return      : Case to be created
    */
    public static Case createCase(Id accId, Id recodTypeIdCase) {
        return new Case(AccountId = accId, Credit_Note_Amount__c=5000, RecordTypeId = recodTypeIdCase);
    }

    /*
     @ Description : To create Fund Transfer Case
     @ Parameters  : AccountId, Case Id and record type id
     @ Return      : Case to be created
    */
    public static List<Fund_Transfer_Unit__c> createFundTransferUnits(Id accId, Id caseId, List<Booking_Unit__c> lstBookingUnits) {
        List<Fund_Transfer_Unit__c> lstFundTransferUnits = new List<Fund_Transfer_Unit__c>();
        for(Booking_Unit__c objBU : lstBookingUnits) {
            lstFundTransferUnits.add(new Fund_Transfer_Unit__c(New_Account_Holder__c = accId,
                                        New_Unit__c = objBU.Id, Case__c = caseId, Allocated_Amount__c=200));
        }
        return lstFundTransferUnits;
    }

    /*
     @ Description : To create Document(SR_Attachments__c)
     @ Parameters  : Case Id and document type
     @ Return      : Document to be created
    */
    public static SR_Attachments__c createCaseDocument(Id caseId, String docType) {
        return new SR_Attachments__c(Case__c = caseId, Type__c = docType, isValid__c = true );
    }

    /*
     @ Description : To create SR Booking Units
     @ Parameters  : Case Id and list of booking units
     @ Return      : List of SR Booking Units
    */
    public static List<SR_Booking_Unit__c> createSRBookingUnis(Id caseId, List<Booking_Unit__c> lstBookingUnits) {
        List<SR_Booking_Unit__c> lstSRBookingUnits = new List<SR_Booking_Unit__c>();
        for(Booking_Unit__c objBU : lstBookingUnits) {
            lstSRBookingUnits.add(new SR_Booking_Unit__c(Booking_Unit__c = objBU.Id, Case__c = caseId));
        }
        return lstSRBookingUnits;
    }

    public static list<Buyer__c> createBuyer(Id bookingId, Integer counter, Id accId) {
        list<Buyer__c> lstBuyer = new list<Buyer__c>();

        /*Inquiry__c objInq = new Inquiry__c();
        insert objInq ;*/

        Inquiry__c objInquiry = new Inquiry__c();
        objInquiry.RecordtypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        objInquiry.Inquiry_Source__c = 'Assignment';
        objInquiry.First_Name__c = 'New';
        objInquiry.Last_Name__c = 'Inquiry';
        objInquiry.Preferred_Language__c = 'English';
        objInquiry.Primary_Contacts__c = 'Mobile Phone';
        objInquiry.Mobile_CountryCode__c = 'India: 0091';
        objInquiry.Mobile_Phone__c = '9999999';
        objInquiry.Email__c = 'we@test.com';
        objInquiry.Mobile_Phone_Encrypt__c = '78457845';
        insert objInquiry;

        for(Integer i = 0; i < counter; i++){
            lstBuyer.add(new Buyer__c(  Booking__c = bookingId,
                                        //Primary_Buyer__c = false,
                                        First_Name__c='Fname' + i,
                                        Last_Name__c ='Lname' + i,
                                        Inquiry__c = objInquiry.Id,
                                        Account__c=  accId,
                                        IPMS_Registration_ID__c = i+''+String.valueOf(System.currentTimeMillis())
                                        /*Address_Line_1_Arabic__c = 'Test' + i,
                                        City_Arabic__c = 'Test' + i,
                                        Country_Arabic__c = 'Test' + i,
                                        First_Name_Arabic__c = 'Test' + i,
                                        Last_Name_Arabic__c = 'Test' + i,
                                        Nationality_Arabic__c = 'Test' + i,
                                        Buyer_Type__c = 'Individual',
                                        Address_Line_1__c = 'Test' + i,
                                        Country__c = 'Test' + i,
                                        City__c = 'Test' + i,
                                        Date_of_Birth__c = 'Test',
                                        Email__c = 'Test',
                                        Nationality__c = 'Test',
                                        Passport_Expiry_Date__c = 'Test',
                                        Passport_Number__c = 'Test',
                                        Phone__c = 'Test',
                                        Phone_Country_Code__c = 'India: 0091',
                                        Place_of_Issue__c = 'Test',
                                        Title__c = 'DR.'*/
                                        ));
        }
        return lstBuyer;
    }
    /*
     @ Description : To create Options for Booking Units
     @ Parameters  : list of booking units
     @ Return      : List of Options
    */
    public static List<Option__c> createOptions( List<Booking_Unit__c> lstBookingUnits) {
        List<Option__c> lstOptions = new List<Option__c>();
        for(Booking_Unit__c objBU : lstBookingUnits) {
            lstOptions.add(new Option__c(Booking_Unit__c = objBU.Id, OptionsName__c = 'Test', PromotionName__c='Test', SchemeName__c='Test', CampaignName__c='Test'));
        }
        return lstOptions ;
    }

    /*
     @ Description : To create roles
     @ Parameters  : name and developerName(should not contain space)
     @ Return      : UserRole Object
    */
    public static UserRole createRole( String name, String developerName ) {
        UserRole user = new UserRole(
                            DeveloperName = developerName,
                            Name = name
                        );

        return user;
    }

    /*
     @ Description : To create user
     @ Parameters  : lastName, profileId, roleId
     @ Return      : User Object
    */
    public static User createUser(String lastName, String profileId, String roleId){

        User u = new User(Alias = 'standt', Email=lastName+'@testorg.com',
                        EmailEncodingKey='UTF-8', LastName=lastName, LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US', ProfileId = profileId,
                        TimeZoneSidKey='America/Los_Angeles',
                        UserName=lastName+ DateTime.now().getTime()+'@testorg.com',
                        UserRoleId=roleId);
        return u;
    }

    public static Task createTask(sObject objGeneric, String strSubject, String strAssignedUser, String strProcessName,
        Date dtDueDate) {
        Task objTask = new Task();
                objTask.OwnerId = (Id)objGeneric.get('OwnerId');
                objTask.WhatId = objGeneric.Id;
                objTask.Subject = strSubject;
                objTask.ActivityDate = dtDueDate;
                objTask.Assigned_User__c = strAssignedUser;
                objTask.Process_Name__c = strProcessName;
                objTask.Priority = 'High';
                objTask.Status = 'Not Started';
                return objTask;
        }

    /*
     @ Description : To create calling list record
     @ Parameters  : recordType Developer Name, counter , bookingUnitsObj
     @ Return      : list of calling list to be created
    */

    public static List<Calling_List__c> createCallingList( String recordTypeDevName, Integer counter , Booking_Unit__c bookingUnitsObj ) {

        Id RecordTypeIdCollection = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType=:'Calling_List__c'
            AND DeveloperName=:recordTypeDevName
            AND IsActive = TRUE LIMIT 1
        ].Id;

        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        for( Integer i=0; i<counter; i++ ) {
            lstCallingLists.add(new Calling_List__c( Registration_ID__c = bookingUnitsObj.Registration_ID__c , Inv_Due__c = 0, DM_Due_Amount__c = 0 , RecordTypeId = RecordTypeIdCollection ) );
        }
        return lstCallingLists;
    }


}