public class GenericHttpResponseFromService {
/***************************************************************************************************************
Description: Method to HTTPResponse 
Return Type: HTTPResponse 
Parameters : Endpoint, method, userName, authorization
****************************************************************************************************************/     
    public static HTTPResponse returnResponseFromService(String endpoint,String method,String userName,String password,String authorization) {
        HTTP h = new HTTP();
        HTTPRequest request = new HTTPRequest();
        request.setEndpoint(endpoint);
        request.setMethod(method);
        if(authorization == 'Basic') {
          Blob headerValue = Blob.valueOf(userName + ':' +password);
          String authorizationHeader = 'Basic' + EncodingUtil.base64Encode(headerValue);
          request.setHeader('Authorization', authorizationHeader);  
        }      
        request.setTimeout(120000);  
        HTTPResponse resp = h.send(request);
        return resp;
    }
}