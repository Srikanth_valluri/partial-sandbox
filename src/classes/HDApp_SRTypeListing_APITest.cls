@isTest
private class HDApp_SRTypeListing_APITest{

	@isTest
  	static void getSRTypeDetailsTest1(){
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
            Agency_ID__c = '1234');
        insert sr;
        
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                      Party_ID__c = '63062',
                                      RecordtypeId = rtId,
                                      Email__pc = 'test@mailinator.com');
        insert account;
        
        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                             );
        Database.insert(user1);
        
        Booking__c booking = new Booking__c(
            Deal_SR__c = sr.Id,
            Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Villa';
        insert invObj;
        
        
        Booking_Unit__c bookingUnit = new Booking_Unit__c(
            Booking__c = booking.Id,
            Unit_Name__c = 'BD4/12/1204',
            Registration_ID__c = '3901',
            Handover_Flag__c = 'Y',
            Inventory__c = invObj.id,
            Resident__c = account.id,
            Dummy_Booking_Unit__c = true);
        insert bookingUnit;
        
        List<FM_Case__c> lstFmCases = new List<FM_Case__c>();
        //Move-In Case
        Id rtMoveInId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move In').getRecordTypeId();
        FM_Case__c objMoveIn = new FM_Case__c();
        objMoveIn.Origin__c = 'Portal';
        objMoveIn.isHelloDamacAppCase__c = true;
        //objMoveIn.Tenant__c = account.Id;
        objMoveIn.Account__c = account.id;
        objMoveIn.RecordtypeId = rtMoveInId;
        objMoveIn.Status__c = 'Submitted';
        objMoveIn.Request_Type__c = 'Move In';
        objMoveIn.Booking_Unit__c = bookingUnit.id;
        lstFmCases.add(objMoveIn);
        
        //Move-Out Case
        //Id rtMoveOutId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move Out').getRecordTypeId();
        //FM_Case__c objMoveOut = new FM_Case__c();
        //objMoveOut.Origin__c = 'Portal';
        //objMoveOut.isHelloDamacAppCase__c = true;
        ////objMoveOut.Tenant__c = account.Id;
        //objMoveOut.Account__c = account.id;
        //objMoveOut.RecordtypeId = rtMoveOutId;
        //objMoveOut.Status__c = 'Submitted';
        //objMoveOut.Request_Type__c = 'Move Out';
        //objMoveOut.Booking_Unit__c = bookingUnit.Id;
        //lstFmCases.add(objMoveOut);
        
        List<Case> lstCases = new List<Case>();
        //COCD
        Case objCOCD = new Case();
        objCOCD.AccountId = account.Id;
        objCOCD.SR_Type__c = 'Change of Contact Details';
        objCOCD.Origin = 'Portal';
        objCOCD.isHelloDamacAppCase__c = true;
        objCOCD.Status = 'New';
        lstCases.add(objCOCD);
        
        //Passport
        Case objPassport = new Case();
        objPassport.AccountId = account.Id;
        objPassport.SR_Type__c = 'Passport Detail Update SR';
        objPassport.Origin = 'Portal';
        objPassport.isHelloDamacAppCase__c = true;
        objPassport.Status = 'Submitted';
        lstCases.add(objPassport);
        
        Test.startTest();
        
        insert lstCases;
        insert lstFmCases;
        
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getSRTypelist';  
        req.addParameter('accountId',account.id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        HDApp_SRTypeListing_API.getSRTypeDetails();
        HDApp_SRTypeListing_API.isGasClearanceDocApplicable(bookingUnit.Inventory__r.Building_Location__c);
        
        Test.stopTest();
  	}
    
    @isTest
    static void getSRTypeDetailsTestTenant(){
        
        Test.startTest();
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
            Agency_ID__c = '1234');
        insert sr;
        
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account ownerAcc = new Account( LastName = 'Test Account Owner',
                                       Party_ID__c = '1234',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert ownerAcc;
        
        //Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                      Party_ID__c = '63062',
                                      RecordtypeId = rtId,
                                      Email__pc = 'test@mailinator.com');
        insert account;
        
        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Tenant Community Login User%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                             );
        Database.insert(user1);
        
        Booking__c booking = new Booking__c(
            Deal_SR__c = sr.Id,
            Account__c = ownerAcc.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Studio';
        insert invObj;
        
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        Booking_Unit__c bookingUnit1 = new Booking_Unit__c(
            Booking__c = booking.Id,
            Unit_Name__c = 'BD4/12/1204',
            Tenant__c = account.id,
            Resident__c = account.id,
            Registration_ID__c = '3901',
            Handover_Flag__c = 'Y',
            Inventory__c = invObj.Id,
            Dummy_Booking_Unit__c = true);
        bookingUnitList.add(bookingUnit1);
        
        Booking_Unit__c bookingUnit2 = new Booking_Unit__c(
            Booking__c = booking.Id,
            Unit_Name__c = 'BD4/12/1205',
            Tenant__c = account.id,
            Resident__c = account.id,
            Registration_ID__c = '3902',
            Handover_Flag__c = 'Y',
            Inventory__c = invObj.Id,
            Dummy_Booking_Unit__c = true);
        bookingUnitList.add(bookingUnit2);
        
        insert bookingUnitList;
        
        List<FM_Case__c> lstFmCases = new List<FM_Case__c>();
        //Move-In Case
        //Id rtMoveInId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move In').getRecordTypeId();
        //FM_Case__c objMoveIn = new FM_Case__c();
        //objMoveIn.Origin__c = 'Portal';
        //objMoveIn.isHelloDamacAppCase__c = true;
        //objMoveIn.Tenant__c = account.Id;
        //objMoveIn.Account__c = account.id;
        //objMoveIn.RecordtypeId = rtMoveInId;
        //objMoveIn.Status__c = 'New';
        //objMoveIn.Request_Type__c = 'Move In';
        //lstFmCases.add(objMoveIn);
        
        //Move-Out Case
        Id rtMoveOutId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move Out').getRecordTypeId();
        FM_Case__c objMoveOut = new FM_Case__c();
        objMoveOut.Origin__c = 'Portal';
        objMoveOut.isHelloDamacAppCase__c = true;
        objMoveOut.Tenant__c = account.Id;
        objMoveOut.Account__c = account.id;
        objMoveOut.RecordtypeId = rtMoveOutId;
        objMoveOut.Status__c = 'Submitted';
        objMoveOut.Request_Type__c = 'Move Out';
        objMoveOut.Booking_Unit__c = bookingUnit1.id;
        lstFmCases.add(objMoveOut);
        
        Id rtTenantRenewalId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId();
        FM_Case__c objTenantRenewal = new FM_Case__c();
        objTenantRenewal.Origin__c = 'Portal';
        objTenantRenewal.isHelloDamacAppCase__c = true;
        objTenantRenewal.Tenant__c = account.Id;
        objTenantRenewal.Account__c = account.id;
        objTenantRenewal.RecordtypeId = rtTenantRenewalId;
        objTenantRenewal.Status__c = 'Submitted';
        objTenantRenewal.Request_Type__c = 'Tenant Renewal';
        objTenantRenewal.Booking_Unit__c = bookingUnit1.id;
        lstFmCases.add(objTenantRenewal);
        
        insert lstFmCases;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getSRTypelist';  
        req.addParameter('accountId',account.id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        HDApp_SRTypeListing_API.getSRTypeDetails();
        
        Test.stopTest();
    }
    
    @isTest
    static void getSRTypeDetailsTestNew(){
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Villa';
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'N',
                                      Inventory__c = invObj.id,
                                      Resident__c = account.id,
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;

        Test.startTest();

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getSRTypelist';  
        req.addParameter('accountId',account.id);
        req.addParameter('installmentUnits',bookingUnit.Registration_ID__c);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        HDApp_SRTypeListing_API.getSRTypeDetails();

        Test.stopTest();
  	}
    
    @isTest
    static void testSRType(){
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;
		
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;
		
        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];
		
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
		
        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
		
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Villa';
        insert invObj;
		
        List<Booking_Unit__c> listBookingUnits = new List<Booking_Unit__c>();
        
        Booking_Unit__c bookingUnit1 = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'Y',
                                      Inventory__c = invObj.id,
                                      Owner__c = account.id,
            						  Resident__c = account.id,
                                      Dummy_Booking_Unit__c = true);
        listBookingUnits.add(bookingUnit1);
        
        Booking_Unit__c bookingUnit2 = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1205',
                                      Registration_ID__c = '3902',
                                      Handover_Flag__c = 'Y',
                                      Inventory__c = invObj.id,
                                      Owner__c = account.id,
            						  Resident__c = account.id,
                                      Dummy_Booking_Unit__c = true);
        listBookingUnits.add(bookingUnit2);
        
        insert listBookingUnits;
		
        List<FM_Case__c> listFMCases = new List<FM_Case__c>();
        
        Id rtNocFitOutId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('NOC For FitOut').getRecordTypeId();
        FM_Case__c objWPFmCaseNocFitOut = new FM_Case__c();
        objWPFmCaseNocFitOut.Origin__c = 'Portal';
        objWPFmCaseNocFitOut.isHelloDamacAppCase__c = true;
        objWPFmCaseNocFitOut.Account__c = account.Id;
        objWPFmCaseNocFitOut.RecordtypeId = rtNocFitOutId;
        objWPFmCaseNocFitOut.Status__c = 'In Progress';
        objWPFmCaseNocFitOut.Request_Type__c = 'NOC for Fit out/Alterations';
        objWPFmCaseNocFitOut.Type_of_NOC__c = 'NOC for Fit-out';
        objWPFmCaseNocFitOut.Booking_Unit__c = bookingUnit1.Id;
        objWPFmCaseNocFitOut.Work_Permit_Type__c = 'Work Permit - Including Fit Out';
        objWPFmCaseNocFitOut.Person_To_Collect__c = 'Contractor';
        objWPFmCaseNocFitOut.Contact_person_contractor__c = 'Test Contractor';
        objWPFmCaseNocFitOut.Email_2__c = 'testctr@test.com';
        listFMCases.add(objWPFmCaseNocFitOut);
        
        Id rtWPId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Work Permit').getRecordTypeId();
        FM_Case__c objWPFmCaseContractor = new FM_Case__c();
        objWPFmCaseContractor.Origin__c = 'Portal';
        objWPFmCaseContractor.isHelloDamacAppCase__c = true;
        objWPFmCaseContractor.Account__c = account.Id;
        objWPFmCaseContractor.RecordtypeId = rtWPId;
        objWPFmCaseContractor.Status__c = 'Draft Request';
        objWPFmCaseContractor.Request_Type__c = 'Work Permit';
        objWPFmCaseContractor.Booking_Unit__c = bookingUnit1.Id;
        objWPFmCaseContractor.Work_Permit_Type__c = 'Work Permit - Including Fit Out';
        objWPFmCaseContractor.Person_To_Collect__c = 'Contractor';
        objWPFmCaseContractor.Contact_person_contractor__c = 'Test Contractor';
        objWPFmCaseContractor.Email_2__c = 'testctr@test.com';
        listFMCases.add(objWPFmCaseContractor);
        
        insert listFMCases;
        
        Test.startTest();
		
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getSRTypelist';  
        req.addParameter('accountId',account.id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        HDApp_SRTypeListing_API.getSRTypeDetails();
		
        Test.stopTest();
  	}
}