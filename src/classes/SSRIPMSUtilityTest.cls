@isTest
private class SSRIPMSUtilityTest
{
  public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';

  @testSetup()
  private static void testData(){
      List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);    
      settingLst2.add(newSetting1);
      insert settingLst2;
  }

  static testMethod void checkFundTransferTask() {
    List<Booking__c> bookingList = new List<Booking__c>();
    List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
    List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();

    //insert sample data for Account
    Account objAccount = TestDataFactory_CRM.createPersonAccount();
    insert objAccount;
    
    Sales_Service_Request__c objCase = new Sales_Service_Request__c();
    objCase.Account__c = objAccount.Id;
    objCase.Status__c = 'Submitted';
    insert objCase;

    //create Deal SR record
    NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
    insert objDealSR;

    //create Booking record for above created Deal and Account
    bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
    insert bookingList;

    //create Property data
    Property__c objProperty = new Property__c();
    objProperty.Name = 'Test Project';
    objProperty.Property_Name__c = 'Test Property';
    objProperty.Property_ID__c = 3431;
    objProperty.CurrencyIsoCode = 'AED';
    insert objProperty;

    //create data for Inventory
    Inventory__c objInventory = new Inventory__c();
    objInventory.Property__c = objProperty.Id;
    objInventory.Unit__c = '1345';
    insert objInventory;

    //create Booking Units record for above created Bookings
    bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
    for(Booking_Unit__c objBookingUnit : bookingUnitList) {
      objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
      objBookingUnit.Inventory__c = objInventory.Id;
      objBookingUnit.Registration_ID__c = '74712';
    }
    insert bookingUnitList;

    List<SR_Booking_Unit__c> lstSRBookingUnits = new List<SR_Booking_Unit__c>();
    for(Booking_Unit__c objBU : bookingUnitList) {
        lstSRBookingUnits.add(new SR_Booking_Unit__c(Booking_Unit__c = objBU.Id, Sales_Service_Request__c = objCase.Id));
    }
    insert srBookingUnitList;
    objCase.OwnerId = UserInfo.getUserId();
    update objCase;

      
    Task objTask = TaskUtility.getTask((SObject)objCase, 'Update Fund Transfer Details in IPMS', 'Finance','Sales Service Request - Fund Transfer', System.today().addDays(1));
    insert objTask;

    Test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
        SSRIPMSUtility.verifyTaskBeforePushToIPMS(new List<Task> {objTask});
     Test.stopTest();
  }
}