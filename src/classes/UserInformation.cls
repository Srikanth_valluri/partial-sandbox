@RestResource(urlMapping='/user/*')
global class UserInformation{

    @HttpGet 
    global static void getNewFundraiser(){
        RestResponse res = RestContext.response;
        RestRequest req = RestContext.request;
        UserDetails userDetails = new UserDetails();
        userDetails.users= new list<user>();
        List<String> profilesList = System.Label.PC_Profile.split(',');
        System.debug('>>>> profilesList >> ' + profilesList);
        //for(user user : [select id,Name from user where isactive=true and profile.name=:system.label.PC_Profile]){
        for (User userObj : [ SELECT Id, Name, Profile.Name 
                                FROM User 
                               WHERE isActive = true 
                                 AND Profile.Name IN :profilesList
        ]) {
            userDetails.users.add(userObj);
            System.debug('>>>>Profile.Name >> ' + userObj.Id);
            System.debug('>>>>Profile.Name >> ' + userObj.Name);
            System.debug('>>>>Profile.Name >> ' + userObj.Profile.Name);
        }
        res.responseBody = Blob.valueof(JSON.serialize(userDetails));
        res.statusCode = 200;
    }

    global class UserDetails{
        global List<User> users{get;set;}
    }

}