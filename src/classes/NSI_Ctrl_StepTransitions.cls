/***********************************************************************************************************************
* Name               : NSI_Ctrl_StepTransitions                                                                        *
* Description        : Step transition page controller class.                                                          *
* Created Date       : 05/02/2017                                                                                      *
* Created By         : NSI                                                                                             *
* ---------------------------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR            DATE            COMMENTS                                                               *
* 1.0                           05/02/2017      Initial Draft.                                                         *
* 1.1         Subhash           09/05/2017      Added the method to make the docs optional for rejection.              *
* 1.2         Vineet            05/07/2017      Added rejection step template condition check.                         *
* 1.3         Vineet            05/07/2017      Added the code to uncheck the required docs not uploaded flag,         * 
*                                               on service request inside the below check.                             *
***********************************************************************************************************************/
public without sharing class NSI_Ctrl_StepTransitions{
    
    public String CancelLink {get; set;}
    public String StepLink {get; set;}
    public String SRID {get; set;}
    public String StepID {get; set;}
    public String userType {get; set;}
    public Boolean hasAccess {get; set;}
    public Boolean showDPPercentage {get; set;}
    public Integer iListSize {get; set;}
    public User objCurrentUser;
    public NSIBPM__Step__c step {get; set;}
    public NSIBPM__Service_Request__c objSR {get; set;}
    public List<TransitionWrapper> lstTrnsWrap {get; set;}
    public Map<String, String> MapDelegatedUsers;
    public Map<Id, NSIBPM__Step_Transition__c> mapStepTransition;
    
    /*********************************************************************************************
    * @Description : Controller class.                                                           *
    * @Params      : void                                                                        *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public NSI_Ctrl_StepTransitions(ApexPages.StandardController controller){
        lstTrnsWrap = new list<TransitionWrapper>();
        showDPPercentage = false;
        iListSize = 0;
        if(apexpages.currentpage().getParameters().get('Id') != null){
            SRID = apexpages.currentpage().getParameters().get('Id');
        }
        step = new NSIBPM__Step__c();
        if(apexpages.currentpage().getParameters().get('StepId') != null){
            StepID = apexpages.currentpage().getParameters().get('StepId');
            for(NSIBPM__Step__c stp:[SELECT Id, Name, NSIBPM__Summary__c, RecordTypeId, NSIBPM__Step_Status__c,  
                                         RecordType.DeveloperName, OwnerId, Owner.Name, RecordType.Name, NSIBPM__Parent_Step__c, 
                                         NSIBPM__SR__c, NSIBPM__SR__r.NSIBPM__SR_Template__c, NSIBPM__Status__c, NSIBPM__SR_Step__c, 
                                         NSIBPM__Rejection_Reason__c, NSIBPM__Status__r.Name ,NSIBPM__SR_Step__r.NSIBPM__Step_Template_Code__c
                                     FROM NSIBPM__Step__c 
                                     WHERE Id != null AND 
                                           Id =: StepID AND 
                                           NSIBPM__SR__c != null AND 
                                           NSIBPM__SR__r.NSIBPM__SR_Template__c != null AND 
                                           IsDeleted = false]){
                step = stp;
            }
            objSR = SRUtility.getSRDetails(step.NSIBPM__SR__c);
            if(step.NSIBPM__SR_Step__r != null && step.NSIBPM__SR_Step__r.NSIBPM__Step_Template_Code__c != null && step.NSIBPM__SR_Step__r.NSIBPM__Step_Template_Code__c == 'UPLOAD_POP'){
                showDPPercentage = true;
            }
        }
        hasAccess = false;
        objCurrentUser = new User();
        for(User curUser:[SELECT Id, ContactId, ProfileId, Profile.UserLicenseId, Profile.UserLicense.Name, 
                                 Profile.UserLicense.LicenseDefinitionKey, Profile.Name 
                          FROM User 
                          WHERE Id != null AND 
                                Id =: userInfo.getUserId() AND 
                                IsActive = true]){
            objCurrentUser = curUser;
            if(curUser.ContactId==null){
                userType = 'salesforce';
                CancelLink = '/'+SRID;
                StepLink =  '/'+StepId;
            }else{
                userType = 'Community';
                CancelLink = '/agents/'+SRID;
                StepLink =  '/agents/'+StepId;
            }
        }
    }
    
    public void Check_Permissions(){
        MapDelegatedUsers = new map<string,string>();
        MapDelegatedUsers = GetDelegatedUsers(objCurrentUser.Id);
        if(step.OwnerId==userinfo.getUserId() || MapDelegatedUsers.get(step.OwnerId)!=null || (objCurrentUser!=null && objCurrentUser.Profile.Name=='System Administrator')){
            hasAccess = true;
        }else if(userType!=null && userType=='Community'){
            hasAccess = true;
        }else{
            if(string.valueOf(step.OwnerId).substring(0,3)=='00G'){
                getGroupData(step.OwnerId);
                system.debug('#### lstGroupsData = '+lstGroupsData);
                for(GroupDetails GD:lstGroupsData){
                    if(GD.GroupOrUserId==userinfo.getUserId()){
                        hasAccess = true;
                        break;
                    }
                }
            }
        }
        //hasAccess = false;
        Prepare_Transitions();
    }
    
    public void Prepare_Transitions(){
        set<id> setValidSteps = new set<id>();
        mapStepTransition = new map<id,NSIBPM__Step_Transition__c>();
        if(userType=='salesforce'){
            for(NSIBPM__Step_Transition__c trans : [SELECT NSIBPM__From__c, NSIBPM__To__c, NSIBPM__Transition__c, NSIBPM__Transition__r.NSIBPM__To__c, 
                                                           NSIBPM__SR_Step__c, NSIBPM__SR_Status_External__c, NSIBPM__SR_Status_Internal__c 
                                                    FROM NSIBPM__Step_Transition__c 
                                                    WHERE NSIBPM__Transition__c != null AND 
                                                          NSIBPM__From__c =: step.NSIBPM__Status__r.Name AND 
                                                          NSIBPM__SR_Step__c=:step.NSIBPM__SR_Step__c AND 
                                                          IsDeleted = false]){
                setValidSteps.add(trans.NSIBPM__Transition__r.NSIBPM__To__c);
                mapStepTransition.put(trans.NSIBPM__Transition__r.NSIBPM__To__c,trans);
            }
        }else{
            for(NSIBPM__Step_Transition__c trans : [SELECT NSIBPM__From__c, NSIBPM__To__c, NSIBPM__Transition__c, NSIBPM__Transition__r.NSIBPM__To__c, 
                                                           NSIBPM__SR_Step__c, NSIBPM__SR_Status_External__c, NSIBPM__SR_Status_Internal__c 
                                                    FROM NSIBPM__Step_Transition__c 
                                                    WHERE NSIBPM__Transition__c != null AND 
                                                          NSIBPM__From__c =: step.NSIBPM__Status__r.Name AND 
                                                          NSIBPM__SR_Step__c =: step.NSIBPM__SR_Step__c AND 
                                                          NSIBPM__Display_on_Portal__c = true AND 
                                                          IsDeleted = false]){
                setValidSteps.add(trans.NSIBPM__Transition__r.NSIBPM__To__c);
                mapStepTransition.put(trans.NSIBPM__Transition__r.NSIBPM__To__c,trans);
            }
        }
        if(setValidSteps!=null && setValidSteps.size()>0){
            TransitionWrapper objWrap;
            for(NSIBPM__Status__c objstat : [SELECT Id, Name, NSIBPM__Type__c, NSIBPM__Rejection__c, NSIBPM__SR_Closed_Status__c, NSIBPM__Code__c 
                                             FROM NSIBPM__Status__c 
                                             WHERE Id != null AND 
                                                   Id IN: setValidSteps AND 
                                                   IsDeleted = false]){
                objWrap = new TransitionWrapper();
                objWrap.objStatus = objstat;
                objWrap.objSRStepTrans = new NSIBPM__Step_Transition__c();
                if(mapStepTransition.get(objstat.id)!=null){
                    objWrap.objSRStepTrans = mapStepTransition.get(objstat.id);
                }
                lstTrnsWrap.add(objWrap);
            }
            iListSize = lstTrnsWrap.size();
        }
    }
    public string selTransition{get;set;}
    public string RejReason{get;set;}
    public string StepNotes{get;set;}
    public pagereference SaveChanges(){
        pagereference pg;
        if(selTransition != null && mapStepTransition.get(selTransition) != null){
            /* Savepoint can only be set after making the callout */
            Savepoint Stat_svpoint = Database.setSavepoint();
            try{
                pg = new PageReference('/'+SRID);
                pg.setRedirect(true);
                if(mapStepTransition.get(selTransition).NSIBPM__SR_Status_Internal__c != null && mapStepTransition.get(selTransition).NSIBPM__SR_Status_External__c != null){
                    NSIBPM__Service_Request__c objSRloc = new NSIBPM__Service_Request__c(Id = SRID);
                    NSIBPM__Service_Request__c DealSr = new NSIBPM__Service_Request__c(Id = SRID);
                    DealSr = [SELECT Id, Recordtype.Name FROM NSIBPM__Service_Request__c WHERE Id =: SRID];
                    if(DealSr.Recordtype.Name == 'Deal'){// Added on May 15 2017 - For jacob
                        // v1.1 : Added the below method to make the sr docs optional for rejection step.
                        // v1.2 : Added below step template check
                        // v1.3 : Added the code to uncheck the required docs not uploaded flag on service request inside the below check
                        if(step != null && step.NSIBPM__SR_Step__c != null && step.NSIBPM__SR_Step__r.NSIBPM__Step_Template_Code__c != null && 
                           (step.NSIBPM__SR_Step__r.NSIBPM__Step_Template_Code__c.equalsIgnoreCase('DEAL_AUTO_REJECTION') || 
                            step.NSIBPM__SR_Step__r.NSIBPM__Step_Template_Code__c.equalsIgnoreCase('DEAL_REJECTION'))){
                            system.debug('#### invoking the make sr docs optional code.');
                            DealSr.NSIBPM__Required_Docs_not_Uploaded__c = false;
                            update DealSr;
                            /* Calling method to make SR Docs optional for rejection step. */
                            makeSrDocsOptional(SRID);   
                        }
                    }
                    objSRloc.DP_Payment__c = objSR.DP_Payment__c;
                    if(mapStepTransition.get(selTransition).NSIBPM__SR_Status_Internal__c != null){
                        objSRloc.NSIBPM__Internal_SR_Status__c = mapStepTransition.get(selTransition).NSIBPM__SR_Status_Internal__c;
                    }
                    if(mapStepTransition.get(selTransition).NSIBPM__SR_Status_External__c != null){
                        objSRloc.NSIBPM__External_SR_Status__c = mapStepTransition.get(selTransition).NSIBPM__SR_Status_External__c;
                    }
                    update objSRloc;
                }else if(showDPPercentage){
                    NSIBPM__Service_Request__c objSRloc = new NSIBPM__Service_Request__c(Id=SRID);
                    objSRloc.DP_Payment__c = objSR.DP_Payment__c;
                    update objSRloc;
                }
                step.NSIBPM__Status__c = mapStepTransition.get(selTransition).NSIBPM__Transition__r.NSIBPM__To__c;
                step.NSIBPM__Step_Notes__c = StepNotes;
                step.NSIBPM__Rejection_Reason__c = RejReason;                
                NSIBPM__Status__c stpStatus = [Select Id,Name,NSIBPM__Type__c,NSIBPM__Rejection__c,NSIBPM__SR_Closed_Status__c,NSIBPM__Code__c from NSIBPM__Status__c where id!=null and id=:(mapStepTransition.get(selTransition).NSIBPM__Transition__r.NSIBPM__To__c) and IsDeleted=false];                
                if(stpStatus!=null && 
                   (stpStatus.NSIBPM__Code__c=='MORE_INFO_UPDATED' || 
                    stpStatus.NSIBPM__Code__c == 'DOCS_SENT' || 
                    stpStatus.NSIBPM__Code__c == 'POP_UPLOADED') && step.NSIBPM__SR__c != null){     
                    boolean isReuploadRequired = false; 
                    if(stpStatus.NSIBPM__Code__c == 'DOCS_SENT'){
                        NSIBPM__Service_Request__c objSRlocal = SRUtility.getSRDetails(step.NSIBPM__SR__c);
                        if(objSRlocal.Delivery_Mode__c != null && objSRlocal.Delivery_Mode__c == 'Dispatch'){
                            for(Booking__c bk : [SELECT Id, Name FROM Booking__c WHERE Deal_SR__c =: step.NSIBPM__SR__c AND AWB_Number__c = null AND Dispatch_Date__c = null]){
                                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'AWB Number and Dispatch Date cannot be null for the bookings under this deal.'));
                                Database.rollback(Stat_svpoint);
                                return null;
                            }
                        }
                    }
                    if(stpStatus.NSIBPM__Code__c == 'POP_UPLOADED'){
                        NSIBPM__Service_Request__c objSRlocal = SRUtility.getSRDetails(step.NSIBPM__SR__c);
                        if(objSRlocal.DP_Payment__c == null){
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'DP Payment % on deal is mandatory.'));
                            Database.rollback(Stat_svpoint);
                            return null;
                        }
                    }
                    for(NSIBPM__SR_Doc__c srDoc : [SELECT Id FROM NSIBPM__SR_Doc__c WHERE Id != null AND NSIBPM__Service_Request__c =: step.NSIBPM__SR__c AND NSIBPM__Status__c = 'Re-upload' LIMIT 1]){
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'Please Re-upload the required document and click â€œSaveâ€ button in SR Documents screen.'));
                        Database.rollback(Stat_svpoint);
                        return null;
                    }
                }
                update step;
                
            }catch(DMLException e){
                string DMLError = e.getdmlMessage(0)+'';
                if(DMLError==null){
                    DMLError = e.getMessage() +' ';
                }
                selTransition = null;
                Database.rollback(Stat_svpoint);
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLError));
                return null;
            }
        }else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please select the status to proceed.'));
            return null;
        }
        return pg;
    }
    
    /*********************************************************************************************
    * @Description : #v1.2 : Making all SR Docs Optional on Deal rejection.                      *
    * @Params      : Id                                                                          *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void makeSrDocsOptional(Id srid){
        List<NSIBPM__SR_Doc__c> srdocs = new List<NSIBPM__SR_Doc__c>();
        srdocs = [SELECT Id, NSIBPM__Is_Not_Required__c 
                  FROM NSIBPM__SR_Doc__c 
                  WHERE NSIBPM__Service_Request__c =: srid AND 
                        NSIBPM__Is_Not_Required__c = false 
                  LIMIT:LIMITS.getLimitQueryRows()];
        if(srdocs.size() > 0){
            for(NSIBPM__SR_Doc__c doc : srdocs){
                doc.NSIBPM__Is_Not_Required__c = true;
            }   
            update srdocs;
        }   
    }
    
    /*********************************************************************************************
    * @Description : Method to get the delegated user.                                           *
    * @Params      : String                                                                      *
    * @Return      : Map<String, String>                                                         *
    *********************************************************************************************/
    public static Map<String, String> GetDelegatedUsers(String CurrentUserId){
        Map<String, String> mapDelegatedUsers = new Map<String, String>();
        for(User usr : [SELECT Id, DelegatedApproverId FROM User WHERE Id != null AND DelegatedApproverId =: CurrentUserId]){
            mapDelegatedUsers.put(usr.Id,usr.DelegatedApproverId);
        }
        return mapDelegatedUsers;
    }
    
    public static List<GroupDetails> lstGroupsData = new List<GroupDetails>();
    public static List<GroupDetails> getGroupData(string strGroupId){
        if(lstGroupsData.size()==0){
            set<string> setInnerGrps = new set<string>();
            for(GroupMember GrpMem:[select Id,GroupId,UserOrGroupId from GroupMember where id!=null AND GroupId=:strGroupId]){
                GroupDetails objGrp = new GroupDetails();
                objGrp.GroupOrUserId = GrpMem.UserOrGroupId;
                if(string.valueOf(GrpMem.UserOrGroupId).substring(0,3)=='00G')
                    setInnerGrps.add(GrpMem.UserOrGroupId);
                lstGroupsData.add(objGrp);
            }
            if(setInnerGrps!=null && setInnerGrps.size()>0){
                for(GroupMember GrpMem:[select Id,GroupId,UserOrGroupId from GroupMember where id!=null AND GroupId IN:setInnerGrps]){
                    GroupDetails objGrp = new GroupDetails();
                    objGrp.GroupOrUserId = GrpMem.UserOrGroupId;
                    lstGroupsData.add(objGrp);
                }
            }
        }
        return lstGroupsData;
    }
    
    /*********************************************************************************************
    * @Description : Transition Wrapper.                                                         *
    *********************************************************************************************/
    public class TransitionWrapper{
        public NSIBPM__Status__c objStatus{get;set;}
        public NSIBPM__Step_Transition__c objSRStepTrans{get;set;}
    }
    
    /*********************************************************************************************
    * @Description : Group details wrapper.                                                      *
    *********************************************************************************************/
    public class GroupDetails{
        public string GroupOrUserId;
    }
}// End of class.