/******************************************************************************************************************
* Name               : VATUpdateIPMS
* Description        : This code is used for sending VAT Update to IPMS   
* Created Date       : 05/12/2017                                                                 
* Created By         : Alok - DAMAC                                                       
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE                                                              
* 1.0         Alok -DAMAC    05/December/2017                                                     
*******************************************************************************************************************/
public class VATUpdateIPMS{
@invocablemethod
public static void updateVAT(List<ID> ids){ 
   
   System.Debug('VAT IDs>>>>'+ids);
     if (!ids.isEmpty()){
     system.enqueueJob(new AsyncVATUpdate(ids));
     }
    
   }
 }