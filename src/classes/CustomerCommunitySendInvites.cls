global class CustomerCommunitySendInvites{
    
    public List<Account> accList {get;set;}
    public ApexPages.StandardSetController standardController{get;set;}
    private Set<Id> accIds= new Set<Id>();

    public CustomerCommunitySendInvites(ApexPages.StandardSetController standardController){
        this.standardController = standardController;
        accList = new List<Account>();
        for (Account accObj: (List<Account>)standardController.getSelected()){ 
            accIds.add(accObj.Id);
        }
        accList = [SELECT Name, id, Email__pc FROM Account WHERE ID IN: accIds];
    }
       
   public PageReference sendInvitationEmail(){
        list<Messaging.SingleEmailMessage> allmails = new list<Messaging.SingleEmailMessage>();

        //Select email template for user language from custom settings.
        list<Customer_Community_Language_Settings__c> listCustom = [
                                                                    Select language__c
                                                                         , Community_Invitation_Email__c
                                                                      from Customer_Community_Language_Settings__c
                                                                     where Language__c = :UserInfo.getLanguage()
                                                                     LIMIT 1
                                                                ];

        //select template html body from email template
        list<EmailTemplate> emailObj = [
                                        Select Id
                                            , HtmlValue
                                        FROM EmailTemplate
                                        WHERE Name = :listCustom[0].Community_Invitation_Email__c
                                    ];

        for(Account acc: [SELECT Name, id, Email__pc FROM Account WHERE ID IN: accIds]){

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            list<string> toAddresses = new list<string>();
            //set selected accounts' email address as recepient
            toAddresses.add(acc.Email__pc);

            if(!toAddresses.isEmpty()){
                mail.setToAddresses(toAddresses);
                system.debug(toAddresses);
                
                //set email body as template html body
                String html = emailObj[0].HtmlValue;
                String filteredHtml = Html.remove('<![CDATA[');

                system.debug('filteredHtml '+filteredHtml);
                OrgWideEmailAddress[] owea = [SELECT 
                                                    Id 
                                               FROM OrgWideEmailAddress 
                                               WHERE Address = 'no-replysf@damacgroup.com'];
                if ( owea.size() > 0 ) {
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                mail.setHtmlBody(filteredHtml.remove(']]>'));
                mail.setSubject('Welcome to Damac Customer Community Portal');
                //set whatId to set Merge Fields' values
                mail.setWhatId(acc.id);
                mail.setTreatBodiesAsTemplate(true);
                allmails.add(mail);
           }

           system.debug('>>> mail : '+mail);
        }
        // send email to selected accounts.
        try{
            Messaging.sendEmail(allmails);
            PageReference nextPage = new PageReference('/001/o');
            return nextPage;
        }
        catch(System.EmailException e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Sending Failed : '+e.getMessage()));
            return null;
        }
        
    }
}