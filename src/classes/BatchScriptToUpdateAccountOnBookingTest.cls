@isTest
private class BatchScriptToUpdateAccountOnBookingTest {
    static testMethod void test_Method1 () {
        BatchScriptToUpdateAccountOnBooking batchCls = new BatchScriptToUpdateAccountOnBooking ();
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Party_ID__C='25425';
        insert objAcc ;
            
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        Booking__c book = new booking__c();
        book.Deal_SR__c = objSR.Id;
        book.Booking_Channel__c = 'Web';
        insert book;

        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = book.id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'test';
        insert bu;

        buyer__c b = new buyer__c();
        b.Buyer_Type__c =  'Individual';
        b.Address_Line_1__c =  'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.City__c = 'Dubai' ;
        b.Date_of_Birth__c = string.valueof(system.today().addyears(-30)) ;
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname' ;
        b.Last_Name__c =  'lastname';
        b.Nationality__c = 'Indian' ;
        b.Passport_Expiry_Date__c = string.valueof(system.today().addyears(20)) ;
        b.Passport_Number__c = 'J0565556' ;
        b.Phone__c = '569098767' ;
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b.Place_of_Issue__c =  'India';
        b.Title__c = 'Mr';
        b.booking__c = book.id; 
        b.Party_ID__C = '25425';
        b.Primary_Buyer__c = true;
        insert b;
        
        Test.startTest();
            Database.executeBatch(batchCls);
        Test.stopTest();
    }
}