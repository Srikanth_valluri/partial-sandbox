/****************************************************************************************************
* Name          : CreateNewTaskControllerTest                                                                 *
* Description   : Test Class for  CreateNewTaskController                                           *
* Created Date  : 26/06/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Twinkle P     26/06/2018      Initial Draft.                                          *
****************************************************************************************************/
@isTest
public class CreateNewTaskControllerTest {
    /**
     * Method to insert records for Object Configuration
     */
    public static Inquiry__c createInquiries() {
        Inquiry__c inquiryInst = new Inquiry__c(
            First_Name__c = 'Test',
            Last_Name__c = 'Last Test',
            Inquiry_Source__c = 'Call Center',
            Inquiry_Status__c = 'Active',
            Email__c = 'test@gmail.com',
            OwnerId = UserInfo.getuserid()
        );
        insert inquiryInst;
        return inquiryInst;
    }
    /**
     * Method to test the Save Task for Inquiry 
     */
	static testMethod void testSaveTask() {
        Inquiry__c  inqObj = createInquiries();
        Task objTask = new Task();
        test.startTest();
        	ApexPages.StandardController sc = new ApexPages.StandardController(objTask);
        	CreateNewTaskController conInst = new CreateNewTaskController(sc);
        	PageReference pageRef = Page.CreateNewTask;
        	pageRef.getParameters().put('inqId', String.valueOf(inqObj.Id));
        	Test.setCurrentPage(pageRef);
        	conInst.objTask.Subject	= 'Test';
        	conInst.saveTask();
        test.stopTest();
        Task objTaskExpected = [SELECT Id, Subject FROM Task LIMIT 1];
		System.assert(objTaskExpected != null);
		System.assertEquals(objTaskExpected.Subject, 'Test');
        
    }
    /**
     * Method to test Cancel on click
     */
	static testMethod void testCancel() {
        Inquiry__c  inqObj = createInquiries();
        Task objTask = new Task();
        test.startTest();
        	ApexPages.StandardController sc = new ApexPages.StandardController(objTask);
        	CreateNewTaskController conInst = new CreateNewTaskController(sc);
        	PageReference pageRef = Page.CreateNewTask;
        	pageRef.getParameters().put('inqId', String.valueOf(inqObj.Id));
        	Test.setCurrentPage(pageRef);
        	conInst.objTask.Subject	= 'Test';
        	conInst.saveTask();
        	conInst.cancelRecord();
        test.stopTest();
        Task objTaskExpected = [SELECT Id, Subject FROM Task LIMIT 1];
		System.assert(objTaskExpected != null);
		System.assertEquals(objTaskExpected.Subject, 'Test');
    }
}