/*
* Description - Test class developed for 'CREPortalHomeExtensionClone'
*
* Version            Date            Author            Description
* 1.0                21/11/17        Lochan            Initial Draft
*/
@isTest
public class CREPortalHomeExtensionCloneTest {
    static testMethod void testMethod1() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
         
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        insert objCase;
 

        ApexPages.StandardController standardController = new ApexPages.StandardController(objAcc);
    
        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'AdditionalParking');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);

        CREPortalHomeExtensionClone obj = new CREPortalHomeExtensionClone(standardController);
        obj.currentAccountId = objAcc.Id;
        obj.searchTerm = 'Test FirstName';
        List<Account> lstAccount = CREPortalHomeExtensionClone.searchMovie(obj.searchTerm);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'AssignmentRequest');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'BouncedCheque');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'FundTransfer');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'FurniturePackage');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'Mortgage');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'PenaltyWaiver');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'ProofOfPayment');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'Refunds');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'COCD');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'COCD');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'EarlyHandover');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'NOCVisa');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'Complaint');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'Home');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'RentalPool');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'TitleDeed');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'Handover');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'RentalPoolTermination');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);


        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'RentalPoolAssignment');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);

        Test.setCurrentPageReference(new PageReference('CREPortalHomeExtensionClone')); 
        System.currentPageReference().getParameters().put('id', objAcc.Id);
        System.currentPageReference().getParameters().put('SRType', 'PlotHandover');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtensionClone(standardController);
        
    }
}