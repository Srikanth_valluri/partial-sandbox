@isTest
private class LastLoginDetailForCallingListTest{
    private static testMethod void unitTest(){
        
         
        
      //  list<Account> lstA = [Select Id, PersonContactId from Account where Id =: objA.Id];
        list<Profile> lstP =[Select Id from Profile where Name Like 'Customer Community%' limit 1];
        profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        //User objU = TestDataFactory_CRM.createUser('Test', lstP[0].Id, null);
        //objU.ContactId = lstA[0].PersonContactId;
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'test2@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);

        //insert objU;
        
        
        system.runAs(portalAccountOwner1) {
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = false);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        Account objA = TestDataFactory_CRM.createPersonAccount();
        objA.LastName = 'Last';
        insert objA;
            Calling_List__c callObj = new Calling_List__c(Account__c = objA.Id,Registration_ID__c = '12851');
        insert callObj;
            ApexPages.StandardController sc = new ApexPages.StandardController(callObj);
            LastLoginDetailForCallingList objClass = new LastLoginDetailForCallingList(sc);
        }
    }
}