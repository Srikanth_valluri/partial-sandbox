/*********************************************************************************************************
* Name               : StatusQueryController
* Test Class         : 
* Description        : Controller class for StatusQuery VF Page
* Created Date       : 09/11/2020
* -----------------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         24/11/2020      Initial Draft.
**********************************************************************************************************/
global With Sharing class StatusQueryController {
    
    public transient string bookungUnitListJSON {get; set;}
    public string invDetails {get;set;}
    
    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public StatusQueryController() {
        initialSearchQuery();
    }
    
     /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public void initialSearchQuery() {
        String searchValue = ApexPages.currentPage().getParameters().get('search');
        List<Booking_Unit__c> buList = new List<Booking_Unit__c> ();
        buList = [SELECT Id, Name, Registration_ID__c , Dispute__c, Unit_Name__c,
                    Customer_Name__c, Registration_Status__c, Property_Name__c, Doc_OK__c,
                    Registration_Date__c, Bedroom_Type__c, View__c, Permitted_Usage__c
                  FROM Booking_Unit__c 
                  WHERE Inventory__c != null 
                      AND Registration_ID__c != null 
                      AND Registration_ID__c != ''
                      AND Registration_Date__c != NULL
                      AND Unit_Name__c != ''
                      AND Property_Name__c != ''
                  ORDER BY Registration_Date__c DESC
                  LIMIT 1000];
        bookungUnitListJSON = JSON.serialize(buList);
    }
    
    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public void updateSearchQuery() {
        String searchValue = ApexPages.currentPage().getParameters().get('search');
        if(searchValue == null || searchValue  == ''){
            initialSearchQuery();
        } else{
            String key = '%' + searchValue + '%';
            List<Booking_Unit__c> buList = new List<Booking_Unit__c> ();
            buList = [SELECT Id, Name, Registration_ID__c , Dispute__c, Unit_Name__c,
                        Customer_Name__c, Registration_Status__c, Property_Name__c, Doc_OK__c,
                        Registration_Date__c, Bedroom_Type__c, View__c, Permitted_Usage__c
                      FROM Booking_Unit__c 
                      WHERE Inventory__c != null 
                          AND Registration_ID__c != null 
                          AND Registration_ID__c != ''
                          AND Unit_Name__c != ''
                          AND Property_Name__c != ''
                          AND (Registration_ID__c LIKE: key OR Customer_Name__c LIKE: key OR Unit_Name__c LIKE: key )
                      ORDER BY Registration_Date__c DESC
                      LIMIT 5000];
            bookungUnitListJSON = JSON.serialize(buList);
        }
    }
    
     public void updateSelectedUnit() {
        String bookId = ApexPages.currentPage().getParameters().get('unitId');
        system.debug('bookId'+bookId);
      	Booking_Unit__c bu =  [SELECT Inventory__c FROM Booking_Unit__c WHERE id =:bookId];
       Inventory__c selectedInventory = [SELECT Id,Unit_Name__c,BccDate__c,Name, Building_Location__c,Comments__c,
                                   Building_Location__r.Building_Name__c,Special_Price__c, UOM_Code__c,Floor__c,
                                   Bedrooms__c, View_Type__c,Furnished__c,
                                    Building_Name__c, Parking__c,Property_Name_2__c,Floor_2__c,Bedroom_Type__c,
                                   Building_Location__r.Permitted_Use__c,
                                   Marketing_Name__c,Property__c,Permitted_Use__c
                                   FROM Inventory__c 
                               WHERE Id =:  bu.Inventory__c];
        
        invDetails = JSON.serialize(selectedInventory);       
    }
    
}