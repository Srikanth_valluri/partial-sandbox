/************************************************************************************************
 * Description: Batch Class to populate Invoice due and Dm due on Mortgage SR and BU            *
 ===============================================================================================*
 * Ver      Date            Author                  Modification                                *
 *==============================================================================================*
 * 1.0      10/09/2019      Arjun Khatri            Initial Draft                               *
*************************************************************************************************/
global class DuesPopulationBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {
    String query;
    
    public Database.QueryLocator start( Database.BatchableContext BC ) {
    
        Set<Id>     recTypeIdSet    =   new Set<Id>();
        Set<String> setMFlag        =   new Set<String>{'A','C','I'};
        Set<String> setStatus       =   new Set<String>{'Submitted','Closed'};

        for( RecordType rec : [ SELECT DeveloperName
                                     , Id
                                     , Name 
                                  FROM RecordType 
                                 WHERE SobjectType = 'Case' 
                                   AND Name = 'Mortgage' ] ) {
                                //OR Name= 'Handover'  )  ]){
            recTypeIdSet.add(rec.Id);
        }
        
        query = ' SELECT Id,DM_Due_Amount__c,Inv_Due__c,Status,Booking_Unit__c, ';
        query += 'Booking_Unit__r.Registration_ID__c ,Booking_Unit__r.DM_Due_Amount__c  ,Booking_Unit__r.Inv_Due__c  ';
        query += 'FROM Case WHERE RecordTypeId IN :recTypeIdSet AND Mortgage_Flag__c IN: setMFlag AND ';
        query += 'Status IN: setStatus AND Booking_Unit__c != null ';// AND Id = \'50025000005KoZi\' ';
        return Database.getQueryLocator(query);
    }

    public void execute( Database.BatchableContext BC, List<Case> lstCase ) {
        System.debug('lstCase size:: : '+lstCase.size());
        List<Case> lstCaseToUpdate = new List<Case>();
        List<Booking_Unit__c> lstBUToUpdate = new List<Booking_Unit__c>();
        List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();

        if( !lstCase.isEmpty() && lstCase != null && lstCase.size() > 0 ) {
            for( Case objCase : lstCase ) {
                if( objCase.Booking_Unit__c != null && String.isNotBlank( objCase.Booking_Unit__r.Registration_ID__c ) ) {
                    RefreshTokenService.agingBucketResponse agingBucketInst =  RefreshTokenService.getAgingbucket( objCase.Booking_Unit__r.Registration_ID__c );
                    System.debug('agingBucketInst:: : '+agingBucketInst);
                    if( agingBucketInst != null && !agingBucketInst.ATTRIBUTE13.contains( 'Exception' ) ) {
                        Booking_Unit__c objBu = new Booking_Unit__c( Id = objCase.Booking_Unit__c );
                    
                        if( agingBucketInst.ATTRIBUTE5 != null && agingBucketInst.ATTRIBUTE5 != 'null' ) {
                            objCase.DM_Due_Amount__c = Decimal.valueOf( agingBucketInst.ATTRIBUTE5 );
                            objBu.DM_Due_Amount__c = Decimal.valueOf( agingBucketInst.ATTRIBUTE5 );
                        }
                        if( agingBucketInst.ATTRIBUTE6 != null && agingBucketInst.ATTRIBUTE6 != 'null' ) {
                            objCase.Inv_Due__c = Decimal.valueOf( agingBucketInst.ATTRIBUTE6 );
                            objBu.Inv_Due__c = Decimal.valueOf( agingBucketInst.ATTRIBUTE6 );
                        }
                        lstCaseToUpdate.add( objCase );
                        lstBUToUpdate.add( objBu );
                    } else if( agingBucketInst.ATTRIBUTE13.contains( 'Exception' ) ){
                        lstErrorLog.add( GenericUtility.createErrorLog( agingBucketInst.ATTRIBUTE13 , null , null  , null, objCase.Id)) ;
                    }         
                }
            }
        }

        System.debug('lstCaseToUpdate:: '+lstCaseToUpdate.size());
        if( lstCaseToUpdate != null && lstCaseToUpdate.size() > 0 ) {
            update lstCaseToUpdate;
        }

        System.debug('lstBUToUpdate:: '+lstBUToUpdate.size());
        if( lstBUToUpdate != null && lstBUToUpdate.size() > 0 ) {
            update lstBUToUpdate;
        }

        System.debug('lstErrorLog:: '+lstErrorLog.size());
        if( lstErrorLog != null && lstErrorLog.size() > 0 ) {
            insert lstErrorLog;
        }
    }
    
    public void finish(Database.BatchableContext BC){
    }

}