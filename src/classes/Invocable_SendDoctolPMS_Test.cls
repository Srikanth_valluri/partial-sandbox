/**************************************************************************************************
* Name               : Invocable_SendDoctolPMS_Test
* Description        : Test Class for Invable_SendDoctolPMS class
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0                         16/06/2019      Created
**************************************************************************************************/
@isTest 
public class Invocable_SendDoctolPMS_Test {
    @testSetup 
    static void setupData() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;

        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.Id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;

        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.Id;
        nstp1.Step_No__c = 2;
        nstp1.Step_Status__c = 'Awaiting Token Deposit';
        nstp1.Step_Type__c = 'Token Payment';
        insert nstp1;
    }
    
    @isTest
    static void test_method1() {
        New_Step__c stp = [SELECT Id FROM New_Step__c LIMIT 1];
        Invocable_SendDoctolPMS.UpdateBuyer(new List<Id>{stp.Id});
    }
}