@isTest
private class CallingListEmailToCXBatchTest {
    @TestSetup
    static void createTestData(){
        Id welcomeClRecTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
         List<TriggerOnOffCustomSetting__c>settingLst = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
                                                                                    insert newSetting1;
        
        
        Account Acc = new Account();
        Acc.Name ='test';
        //Acc.Person_Business_Email__c ='tes2t@tsl.com';
        insert Acc;
        
         NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
        
        Booking__c book= new Booking__c();
		book.Account__c = Acc.id;
        book.Deal_SR__c = objSR.id;       
        insert book;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        objBookingUnit.DOS_Name__c = 'Admin CRM';
        objBookingUnit.HOS_Name__c = 'Admin CRM';
        objBookingUnit.HOD_Name__c = 'Admin CRM';
        objBookingUnit.Booking__c =book.id;
        insert objBookingUnit;

        case c= new case();
        insert c;
        c = [Select OwnerID, Owner.ID From Case where ID = :c.id];
            
        
        Calling_List__c objCL = new Calling_List__c();
        //objCL.Name;
        objCL.Booking_Unit__c = objBookingUnit.Id;
        objCL.Promise_To_Pay_Date__c= System.today().addDays( 2 );
        objCL.Call_Outcome_Brkr__c='Flyin offer';
        objCL.Call_Outcome__c ='Promise to Pay';
        objCL.Customer_Secondary_Email__c = 'test3@tst.com';
        //objCL.Customer_Email__c = 'test4@tst.com';
        
        objCL.Welcome_Call_Outcome__c = 'Invalid Email';
        objCL.Reminder_Date__c = System.today().addDays( -1 );
        objCL.RecordTypeId = welcomeClRecTypeId;
        objCL.OwnerId = c.OwnerID;
        objCL.Account__c = Acc.id;
        objCL.IsHideFromUI__c = false;
        
        insert objCL;
        
        list<Calling_List__c> callList = new list<Calling_List__c>();
        callList.add(objCL);
    }

    @isTest
    private static void testTestCase() {
        // Test data setup

        // Actual test
        Test.startTest();
        Database.executeBatch( new CallingListEmailToCXBatch() );
        Test.stopTest();

        // Asserts
    }
    
}