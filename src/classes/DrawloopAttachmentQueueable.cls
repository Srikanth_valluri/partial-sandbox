public class DrawloopAttachmentQueueable implements Queueable, database.allowscallouts {
    public List<ID> recordIds = new List<ID> ();
    public DrawloopAttachmentQueueable (List<Id> recIds) {
        recordIds = recIds;
    }
    public void execute(QueueableContext context) {
        String DDP_ID = '';
        String DELIVERY_OPTION_ID = '';
        
        
        String SESSION_ID = '';
        
        if (!Test.isRunningTest()) {
            
            session_id = documentHelper.login();
        } else {
            session_id = 'test';
        }
        
        
        if(String.isNotBlank(SESSION_ID) && SESSION_ID != 'none') {
            Loop.loopMessage lm = new Loop.loopMessage();
            lm.sessionId = SESSION_ID;

            for(Unit_Documents__c objUnitDocument : [SELECT Service_Request__c,
                                                            DDP_Ids__c,
                                                            Id
                                                    FROM Unit_Documents__c
                                                    WHERE Id IN :recordIds])
            {
                System.Debug ('====='+objUnitDocument.DDP_Ids__c);
                if(String.isNotBlank(objUnitDocument.DDP_Ids__c)
                    && objUnitDocument.DDP_Ids__c.contains('-'))
                {
                    System.Debug ('====='+objUnitDocument.DDP_Ids__c);
                    DDP_ID = objUnitDocument.DDP_Ids__c.split('-').get(0);
                    System.Debug ('====='+DDP_ID);
                    DELIVERY_OPTION_ID = objUnitDocument.DDP_Ids__c.split('-').get(1);
                    System.Debug ('====='+DELIVERY_OPTION_ID);
                    if(String.isNotBlank(DDP_ID) && String.isNotBlank(DELIVERY_OPTION_ID)) {
                        System.Debug ('====='+DDP_ID);
                        Map<string, string> variables = new map<String,String>();
                        variables.put('deploy', DELIVERY_OPTION_ID);
                        lm.requests.add(new Loop.loopMessage.loopMessageRequest(
                                    objUnitDocument.Id,
                                    DDP_ID,
                                    variables));
                        System.Debug ('====='+variables);
                    }
                }
            }
            if (!Test.isRunningTest()) {
                String response = lm.sendAllRequests();
                system.debug('== response ==' +response );
            }
         
        }
    }
}