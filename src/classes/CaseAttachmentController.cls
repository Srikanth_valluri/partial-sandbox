/**
 * @File Name          : CaseAttachmentController.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 9/18/2019, 3:14:25 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    9/18/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public without sharing class CaseAttachmentController {
    public String selectedType {get;set;}
    public Boolean selectedAwesomeness {get;set;}
    public Boolean damacSigned {get;set;}
    public String description {get;set;}
    public String attachmentUrl {get;set;}
    public Boolean needCorrection {get;set;}
    public Case objCase {get;set;}
    public String objCaseNumber {get;set;}
    public SR_Attachments__c objSRAttach {get;set;}
    public String fileName {get;set;}
    public String fileType {get;set;}
    public transient Blob fileBody {get;set;}
    public list<selectoption> lstOptions {get;set;}
    Map<String,SR_Attachments__c> mapName_Object;
    list<SR_Attachments__c> lstSrAttachment;
    public Boolean rend {get;set;}
    public Boolean rend1 {get;set;}
    List<SR_Attachments__c> lstSRA = new List<SR_Attachments__c>();
    public String strError;
    public String strChildCaseId;
    String parentRecordId;
    public Boolean isPlotHandoverCase {get; set;}

    public void commandLinkAction(){
        if(rend == true){
            rend = false;
            rend1 = true;
        }else{
            rend = true;
            rend1 = false;
        }
    }

    public CaseAttachmentController(ApexPages.StandardController controller) {
        
        this.objSRAttach = (SR_Attachments__c)controller.getRecord();
        System.debug('-objSRAttach--'+objSRAttach);
        
        System.debug('-Attach ID--'+controller.getId());
        /*if(objSRAttach == NULL ){
            SR_Attachments__c newDoc = new SR_Attachments__c(
                
            );
        }*/
        PageReference currentPage = ApexPages.currentPage();
        if (currentPage != NULL) {
            this.strChildCaseId = currentPage.getParameters().get('strChildCaseId');
        }
       
        this.rend = false;
        this.rend1 = true;
        lstSRA = new List<SR_Attachments__c>();
        lstSRA = [Select Id
                                               , Attachment__c
                                               , IsRequired__c
                                               , Name
                                               , isValid__c
                                               , Description__c
                                               , Attachment_URL__c
                                               , SR_Attachments__c.Case__c
                                               , Case__r.CaseNumber
                                               , Signed_by_DAMAC__c
                                               , Need_Correction__c
                                               , Case__r.Booking_Unit__c
                                               , Case__r.Booking_Unit__r.Registration_ID__c
                                               , Case__r.RecordType.DeveloperName
                                               , Case__r.Status
                                               , FM_Case__c
                                               , FM_Case__r.Name
                                          from SR_Attachments__c
                                          where Id =: this.objSRAttach.Id
                                          limit 1];
       system.debug('*****lstSRA[0] Reg*****'+lstSRA[0].Case__r.Booking_Unit__r.Registration_ID__c);
       system.debug('*****lstSRA[0] Reg*****'+lstSRA[0]);
        this.objSRAttach = lstSRA[0];
        this.selectedAwesomeness = lstSRA[0].isValid__c;
        this.damacSigned = lstSRA[0].Signed_by_DAMAC__c;
        this.needCorrection = lstSRA[0].Need_Correction__c;
        objCase = new Case(Id = lstSRA[0].Case__c);
        isPlotHandoverCase = false;
        if (objCase.Id != null && this.objSRAttach != null && String.isNotBlank(this.objSRAttach.Name)) {
            isPlotHandoverCase = checkCasePlotHandover(objCase, this.objSRAttach);
        }
        if(lstSRA[0].FM_Case__c != null){
            parentRecordId = lstSRA[0].FM_Case__c;
            objCaseNumber = lstSRA[0].FM_Case__r.Name;
        }else{
            parentRecordId = lstSRA[0].Case__c;
        }
        objCaseNumber = lstSRA[0].Case__r.CaseNumber;
        system.debug('>>>>>>>objCase : '+objCase);
        system.debug('>>>>>>>objCaseNumber : '+objCaseNumber);
        lstOptions = new list<selectoption>();
        lstOptions.add(new SelectOption('true','Yes'));
        lstOptions.add(new SelectOption('false','No'));
        this.strError = '';
    }

    public CaseAttachmentController() {
        this.rend = false;
        this.rend1 = true;
        this.strError = '';
    }

    // creates a new SR_Attachments__c record
    private void saveCustomAttachment() {

        objSRAttach.Case__c = objCase.Id;
        objSRAttach.isValid__c = selectedAwesomeness;
        objSRAttach.Signed_by_DAMAC__c = damacSigned;
        objSRAttach.Need_Correction__c = needCorrection;

    }

    private String extractName( String strName ) {
        return strName.substringBefore('.');
    }

    private String extractType( String strName ) {
        strName = strName.substring( strName.lastIndexOf('\\')+1 );
        return strName.substring( strName.lastIndexOf('.')+1 ) ;
    }

    // update CaseAttachment record by calling out and storing response url to view document publically
    public pagereference saveCaseAttachmentURL() {
        //lstSrAttachment = new list<SR_Attachments__c>();
        Pagereference pgRef;
        mapName_Object = new Map<String,SR_Attachments__c>();
        system.debug('>>>>>>>>>>>>>>>>>objCaseNumber : '+this.objCaseNumber);
        saveCustomAttachment();
        system.debug('>>>>>>>>>>>>>>>>>fileName : '+fileName);
        system.debug('>>>>>>>>>>>>>>>>>fileBody : '+fileBody);
        try{
            if(fileName != null && fileBody != null){
                List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocReq = new List<UploadMultipleDocController.MultipleDocRequest>();
                system.debug('this.fileName**************'+fileName);
                String stType = extractType(fileName);
                String strName = extractName(fileName);
                strName = strName.replaceAll(' ','_');
                strName = strName.replaceAll('/','_');
                system.debug('strName************'+strName);
                String encodedBody = EncodingUtil.base64Encode(fileBody);
                system.debug('encodedBody :*****'+encodedBody);
                UploadMultipleDocController.MultipleDocRequest objMultipleDocRequest = new UploadMultipleDocController.MultipleDocRequest();
                String stUniqueName = string.valueOf(System.currentTimeMillis());
                objMultipleDocRequest.category = 'Document';
                objMultipleDocRequest.entityName = 'Damac Service Requests';
                objMultipleDocRequest.base64Binary = encodedBody;
                objMultipleDocRequest.fileDescription = fileName;
                objMultipleDocRequest.fileId = 'IPMS-'+objCaseNumber+'-'+stUniqueName;
                objMultipleDocRequest.fileName = 'IPMS-'+objCaseNumber+'-'+stUniqueName+'.'+stType;
                objMultipleDocRequest.registrationId = objCaseNumber;
                objMultipleDocRequest.sourceId = 'IPMS-'+objCaseNumber+'-'+stUniqueName;
                objMultipleDocRequest.sourceFileName = 'IPMS-'+objCaseNumber+'-'+stUniqueName+'.'+stType;
                system.debug('before list addition*****');
                lstMultipleDocReq.add(objMultipleDocRequest);
                system.debug('*****list addition done*****');
                mapName_Object.put('IPMS-'+this.objCaseNumber+'-'+stUniqueName,this.objSRAttach);

                UploadMultipleDocController.data objResponseData = new UploadMultipleDocController.data();
                    objResponseData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);
                    system.debug('objResponseData : ***** '+objResponseData);
                    if(objResponseData != null && objResponseData.status == 'S'
                    && objResponseData.Data != null && objResponseData.Data.size() > 0){
                        for(UploadMultipleDocController.MultipleDocResponse objData : objResponseData.Data){
                            if(String.isNotBlank(objData.url) && !mapName_Object.isEmpty()
                            && mapName_Object.get(objData.PARAM_ID) != null){
                                this.objSRAttach.isValid__c = true;
                                this.objSRAttach.Attachment_URL__c = objData.url;
                                if(this.objSRAttach.Name.equalsIgnoreCase('PRC Document')){
                                    objCase.PRC_Document__c = true;
                                }
                                if(this.objSRAttach.Name.contains('Signed Rental Pool Agreement')){
                                    objCase.Rental_Pool_Agreement_Signed__c = true;
                                }
                                if (this.objSRAttach.Name.contains('Signed Rental Pool Addendum') || this.objSRAttach.Name.contains('Signed LOA Document')) {
                                    objCase.Addnm_Verified__c = true;
                                }
                                system.debug('this.objSRAttach.Name********'+this.objSRAttach.Name);
                                /*if(this.objSRAttach.Name.contains('Signed Key Release Form')){
                                    if(this.objSRAttach.Case__c != null
                                    && this.objSRAttach.Case__r.RecordTypeId != null
                                    && this.objSRAttach.Case__r.RecordType.DeveloperName.equalsignoreCase('Handover')){
                                        
                                        list<String> lstRegIds = new list<String>();
                                        system.debug('*****REGID*****'+this.objSRAttach.Case__r.Booking_Unit__r.Registration_ID__c);
                                        lstRegIds.add(this.objSRAttach.Case__r.Booking_Unit__r.Registration_ID__c);
                                        CallEarlyHandoverMQService1.CallEarlyHandoverMQService1Name('Y','N',lstRegIds);
                                        Booking_Unit__c objBU = new Booking_Unit__c();
                                        objBU.Handover_Date__c = date.today();
                                        objBU.Id = this.objSRAttach.Case__r.Booking_Unit__c;
                                        objBU.Handover_Flag__c = 'Y';
                                        //objBU.Early_Handover__c = false;
                                        system.debug('*****BU ID*****'+this.objSRAttach.Case__r.Booking_Unit__c);
                                        update objBU;
                                        
                                        // Commented on 05/07/2018
                                        //objCase.PRC_Document__c = true;
                                        objCase.Status = 'Closed';
                                    }
                                }*/
                                //lstSrAttachment.add(this.objSRAttach);
                            }
                        }
                        system.debug('objCase.Status**********'+objCase.Status);
                        update objSRAttach;
                        
                        // 01 Nov 20: below code commented coz to delete Insurance copy start date and Insurance copy end date
                        // field, as we are not using above 2 fields as requested by Aditya.
                        //System.debug('--> '+objCase.Insurance_Copy_Start_Date__c);
                        //System.debug('--> '+objCase.Insurance_Copy_End_Date__c);
                        if(objCase.PRC_Document__c
                        || objCase.Rental_Pool_Agreement_Signed__c
                        || (objCase.Status != null && objCase.Status.equalsIgnoreCase('Closed'))
                        || objCase.Addnm_Verified__c) {
                        //|| objCase.Insurance_Copy_Start_Date__c != null
                        //|| objCase.Insurance_Copy_End_Date__c != null
                            try{
                                update objCase;
                            }catch (exception ex){
                                system.debug('Message*****'+ex.getMessage());
                                system.debug('getCause*****'+ex.getCause());
                                system.debug('getStackTraceString*****'+ex.getStackTraceString());
                                system.debug('getLineNumber*****'+ex.getLineNumber());
                                system.debug('getTypeName*****'+ex.getTypeName());
                            }
                        }
                        pgRef = back();
                    }else{
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,objResponseData.message);
                        ApexPages.addMessage(myMsg);
                        pgRef = null;
                    }
            }
            else{
                system.debug('>>>>Entered!!');
                objSRAttach.Case__c = objCase.Id;
                objSRAttach.isValid__c = selectedAwesomeness;
                objSRAttach.Signed_by_DAMAC__c = damacSigned;
                objSRAttach.Need_Correction__c = needCorrection;
                system.debug('>>>>>lstSRA[0] : '+lstSRA[0]);
                if(objSRAttach != null
                    ){
                        // && (lstSRA[0].isValid__c != selectedAwesomeness
                        // || lstSRA[0].Signed_by_DAMAC__c != damacSigned
                        // || lstSRA[0].Need_Correction__c != needCorrection)

                    update objSRAttach;
                }
                pgRef = back();
            }
        }catch(Exception ex){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
            ApexPages.addMessage(myMsg);
            pgRef = null;
        }
        return pgRef;
    }

    public PageReference back() {
        system.debug('***strChildCaseId***'+strChildCaseId);
        system.debug('***parentRecordId***'+parentRecordId);
        if(string.isNotBlank(strChildCaseId)){
            system.debug('Child redirect');
            return new PageReference('/'+strChildCaseId);
        } else {
            system.debug('***parent redirect***');
            return new PageReference('/'+parentRecordId);
        }
        
    }
    
    /*
     * Description : This method is called from "Task Closure in IPMS" Process Builder & is used to update the PRC Document field on Case.
     */
    @InvocableMethod
    public static void updatePRCDocumentOnCase(List<Task> lstTasks) {

        List<Case> lstCases = new List<Case>();
        List<String> lstCaseIds = new List<String>();

        if (lstTasks.size() > 0) {

            // Iterate and check if the Task is related to Case or not.
            for (Task objTask : lstTasks) {
                if (objTask.WhatId != null) {
                    String strWhatId = objTask.WhatId;
                    if (strWhatId.startsWith('500')) {
                        lstCaseIds.add(objTask.WhatId);
                    }
                }
            }

            // Fetch the Case information
            if (lstCaseIds.size() > 0) {
                lstCases = [SELECT Id, RecordType.Name FROM Case WHERE Id In : lstCaseIds AND RecordType.Name = 'Handover'];
            }

            // Update the PRC Document field on Case with true value
            if (lstCases.size() > 0) {
                for (Case objCase : lstCases) {
                    objCase.PRC_Document__c = true;
                }
                update lstCases;
            }
        }
    }

    /*
     * Description : This method checks if the case is of type "Plot Handover" and the document name is "Insurance Copy"
     */ 
    public Boolean checkCasePlotHandover(Case objCase, SR_Attachments__c objSRAttachment) {
        if (objSRAttachment.Name.equalsIgnoreCase('Insurance Copy')) {
            objCase = [SELECT RecordType.DeveloperName FROM Case WHERE Id =: objCase.Id];
            if (objCase.RecordType.DeveloperName == 'Plot_Handover') {
                return true;
            }
            return false;
        }
        return false;
    }
}