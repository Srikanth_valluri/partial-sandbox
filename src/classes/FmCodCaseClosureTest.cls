@isTest
public class FmCodCaseClosureTest{
    static Account objA;
    static void init(Integer intVal){
        objA = new Account();
        if(intVal == 1){
            objA.RecordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            objA.FirstName = 'Vivian';
            objA.LastName = 'Test';
        }else{
            objA.RecordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            objA.Name = 'Account';
        }
        insert objA;
    }
    
    public static TestMethod void testCocdClosure(){
        init(1);
        FM_Case__c objCase = new FM_Case__c();
        objCase.Account__c = objA.Id;
        objCase.Status__c = 'New';
        objCase.Mobile_Country_Code__c = 'India: 0091';
        objCase.Mobile_no__c = '9879879870';
        objCase.Contact_Email__c = 'Email@email.com';
        objCase.New_CR__c = 'PP123456';
        objCase.Passport_Issue_Date__c = date.today().addyears(-10);
        objCase.Passport_Issue_Place__c = 'Pune';
        objCase.Passport_Issue_Place_Arabic__c = 'Pune Ar';
        objCase.Request_Type__c = 'Change of Contact Details'; // Passport Detail Update
        insert objCase;
        
        objCase.Status__c = 'Submitted';
        update objCase;
        list<FM_Case__c> lstFM = new list<FM_Case__c>();
        lstFM.add(objCase);
        FmCodCaseClosure.updateCustomer(lstFM);
    }
    
    public static TestMethod void testpassportClosure(){
        init(2);
        FM_Case__c objCase = new FM_Case__c();
        objCase.Account__c = objA.Id;
        objCase.Status__c = 'New';
        objCase.Mobile_Country_Code__c = 'India: 0091';
        objCase.Mobile_no__c = '9879879870';
        objCase.Contact_Email__c = 'Email@email.com';
        objCase.New_CR__c = 'PP123456';
        objCase.Passport_Issue_Date__c = date.today().addyears(-10);
        objCase.Passport_Issue_Place__c = 'Pune';
        objCase.Passport_Issue_Place_Arabic__c = 'Pune Ar';
        objCase.Request_Type__c = 'Passport Detail Update'; // 
        insert objCase;
        
        objCase.Status__c = 'Submitted';
        update objCase;
        
        list<FM_Case__c> lstFM = new list<FM_Case__c>();
        lstFM.add(objCase);
        FmCodCaseClosure.updateCustomer(lstFM);
    }
}