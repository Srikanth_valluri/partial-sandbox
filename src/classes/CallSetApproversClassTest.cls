@isTest
/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  20/11/2017  Initial Draft
*/

public class CallSetApproversClassTest {
    static Account  objAcc;
    static Case objCase;
    static Booking__c objBooking;
    static Booking_Unit__c objBookingUnit;
    static NSIBPM__Service_Request__c objSR;
    static Booking_Unit_Active_Status__c objBUActive;
    static Option__c objOptions;
    static Payment_Plan__c objPaymentPlan;
    static SR_Attachments__c objSRatt ;
    static SR_Booking_Unit__c objSRB;
    
    static testMethod void initialMethod(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'Test Unit';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        System.debug('------objBookingUnit 1------');    
        insert objBookingUnit;
        System.debug('------objBookingUnit 2------'+objBookingUnit);
                       
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Token Refund').getRecordTypeId();
        objCase = new Case();
        objCase.AccountID = objAcc.id ;
        objCase.RecordTypeID = devRecordTypeId;
        objCase.Total_Token_Amount__c = 1000;
        objCase.POA_Expiry_Date__c = System.today();
        objCase.IsPOA__c = true;
        objCase.status = 'Submitted';
        //objCase.Roles_from_Rule_Engine__c = 'VP';
        objCase.Active_Unit_Ids__c  = 'test';
        insert objCase;
               
        
        objSRB = new SR_Booking_Unit__c();
        objSRB.Case__c = objCase.id;
        objSRB.Booking_Unit__c = objBookingUnit.id;   
        insert objSRB;
        
        objSRatt = new SR_Attachments__c ();
        objSRatt.name = 'Test NOC';
        objSRatt.Case__c = objCase.Id;         
        objSRatt.type__c = 'NOC' ;
        objSRatt.isValid__c = false;
        insert objSRatt;
    }
    
    static testMethod void Test1(){
        test.StartTest();
        initialMethod();
        
        Task objTask  = new Task();
        objTask.Subject = 'Deactivate Booking Unit';
        objTask.WhatID = objCase.id ; 
        objTask.OwnerId = UserInfo.getUserId();
        objTask.status = 'In Progress';
        objTask.priority= 'High';       
        objTask.ActivityDate = System.today().addDays(1);
        objTask.Process_Name__c = 'Token Refund';       
        insert objTask;
                        
        list<ID> lstBookingUnitIDs = new list<ID>();
        lstBookingUnitIDs.add(objBookingUnit.id);
        
        //objCase.Active_Unit_Ids__c  = objBookingUnit.id;
        //update objCase;
               
        
        System.debug('------CallSetApproversClassTest 1------');
        CallSetApproversClass.checkServiceRequest(lstBookingUnitIDs);
        System.debug('------CallSetApproversClassTest 2------');
        
        test.StopTest();
    }       
}