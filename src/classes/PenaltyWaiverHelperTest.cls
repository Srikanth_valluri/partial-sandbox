/*
* Description - Test class developed for PenaltyWaiverHelper
*
* Version            Date            Author            Description
* 1.0              20/11/2017      Ashish Agarwal     Initial Draft
*/


@isTest
public class PenaltyWaiverHelperTest {
     static testMethod void testRelatedBookingUnits() {
         set<String> setRecordTypesName = new set<String>{'Bounced_Cheque_SR', 'Fund_Transfer', 'Token_Refund', 'Customer_Refund' };
         
         // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        PenaltyWaiverHelper objHelper = new PenaltyWaiverHelper();
        test.startTest();
        PenaltyWaiverHelper.CaseListWrapper objCaseListWrapper = PenaltyWaiverHelper.getCaseAndUnitDetailsWrapper(lstBookingUnits[0], setRecordTypesName);
        test.stopTest();
     }
     
     static testMethod void testPopulateCaseInfoWrapper() {
        set<String> setRecordTypesName = new set<String>{'Bounced_Cheque_SR', 'Fund_Transfer', 'Token_Refund', 'Penalty_Waiver' };
         
         // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            objCase.RecordTypeId = recordTypeId;
            objCase.Approval_Status__c = 'Approved';
            //objCase.Status = 'Closed';
            lstCases.add( objCase );
        }
        System.debug('lstCases===='+lstCases);
        insert lstCases ;
        System.debug('after insert lstCases===='+lstCases);
        
        List<SR_Booking_Unit__c> lstSRBooking = new List<SR_Booking_Unit__c>();
        for(Integer i = 0; i <= 5; i++){
            SR_Booking_Unit__c obj = new SR_Booking_Unit__c(Booking_Unit__c = lstBookingUnits[0].Id, Case__c = lstCases[0].Id);
            lstSRBooking.add(obj);
        }
        insert lstSRBooking;  
        
        List<Case> lstNewCases = [Select Id, Approval_Status__c, Status, Booking_Unit__c, RecordType.Name,Approved_Amount__c ,
                                         RecordTypeId, RecordType.DeveloperName From Case Where Id IN : lstCases];        
        
        PenaltyWaiverHelper objHelper = new PenaltyWaiverHelper();
        PenaltyWaiverHelper.CaseListWrapper objCaseListWrapper = PenaltyWaiverHelper.getCaseAndUnitDetailsWrapper(lstBookingUnits[0], setRecordTypesName); 
        test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() ); 
        Set<Id> setOfID = PenaltyWaiverHelper.populateCaseInfoWrapper(lstNewCases, objCaseListWrapper, true, lstBookingUnits[0], setRecordTypesName);
        test.stopTest();
     }
     
     static testMethod void testPenaltyChargedLessEqual() {
         Boolean checkResult;
         test.startTest();
         checkResult = PenaltyWaiverHelper.isPenaltyChargedLessEqual('70480', 50.50);
         test.stopTest();
         //System.assertEquals(true, checkResult);
     }
     
     /*static testMethod void testGetWaivedAmountAccountLevel() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        Decimal checkResult;
        test.startTest();
        checkResult = PenaltyWaiverHelper.getWaivedAmountAccountLevel(objAcc.Id);
        test.stopTest();
     }*/
     
     static testMethod void testCumulativePercent() {
         set<String> setRecordTypesName = new set<String>{'Bounced_Cheque_SR', 'Fund_Transfer', 'Token_Refund', 'Customer_Refund' };
         
         // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        Decimal checkResult;
        PenaltyWaiverHelper objHelper = new PenaltyWaiverHelper();
        test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() ); 
        checkResult = PenaltyWaiverHelper.getCumulativePercent(lstBookingUnits, objAcc.Id, 50.50);
        test.stopTest();
     }
     
     static testMethod void testMultipleDocRequest() {
         UploadMultipleDocController objCOntroller = new UploadMultipleDocController();
         test.startTest();
         UploadMultipleDocController.MultipleDocRequest objWrapper = PenaltyWaiverHelper.makeWrapperObject('body', 'type', 'Test', '70852', '123', '1');
         //objWrapper = 
         test.stopTest();
         //System.assertEquals(true, checkResult);
     }
     
     
}