/********************************************************************************************************************************
 * Description : A batch to send Call back notification
 *===============================================================================================================================
 * Ver      Date-DD/MM/YYYY     Author              Modification
 *===============================================================================================================================
 * 1.1      06-12-2020          Aishwarya Todkar    Added Mortgage calling list and Mortgage telecalling list check
 * 1.2      11-01-2021          Aishwarya Todkar    Added Welcome calling list check
*********************************************************************************************************************************/
global class CallBackNotificationBatchForCallingList implements Database.Batchable<sObject> {
    String query;
    String recordId;
    Map<String,EmailTemplate> mapEmailTemplate;
     
    global CallBackNotificationBatchForCallingList(){}
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        List<String> lstRecordType = new List<String> {'Collections Calling List','Lease Handover Calling List','Handover Calling List','Early Handover Calling List'};
        recordId =  Label.dailyDpBatchForSingleUser.split('-',2)[1];
        query =   ' SELECT Id,'+
                         ' Name,'+
                         ' RecordTypeId,'+
                         ' Registration_ID__c,'+
                         ' Unit_Name__c,'+
                         ' Inv_Due__c,'+
                         ' DM_Due_Amount__c,'+
                         ' Amount_Pending__c,'+
                         ' Notification_Time_for_outcome__c,'+
                         ' Call_Back_Date__c,'+
                         ' Call_Outcome__c,PTP_Date__c,Calling_List_Type__c,'+ 
                         ' IsHideFromUI__c,DP_OK__c,'+
                         ' Customer_Name__c,'+ 
                         ' OwnerId'+
                   ' FROM Calling_List__c'+
                   ' WHERE ' ;
                   
                if(Label.dailyDpBatchForSingleUser.split('-',2)[0] == 'ON'){
                    query +=' ID = :recordId';
                }
                else{
                     query += ' (Call_Back_Date__c = Today AND Call_Outcome__c = \'Call Back\' AND RecordType.Name IN: lstRecordType )' +
                   ' OR (PTP_Date__c = Today AND Call_Outcome__c = \'Promise to Pay\' AND RecordType.Name IN: lstRecordType )' +
                   ' OR (DP_CL_OutCome_Update_Date__c = LAST_N_DAYS:2 AND Call_Outcome__c = \'Unreachable\' AND RecordType.Name = \'Collections Calling List\' AND Calling_List_Type__c = \'DP Calling\' AND DP_OK__c = false  )' +
                   ' OR (DP_CL_OutCome_Update_Date__c = LAST_N_DAYS:5 AND RecordType.Name = \'Collections Calling List\' AND Calling_List_Type__c = \'DP Calling\' AND DP_OK__c = false)'+
                   ' OR ( Call_Back_Date__c = Today AND Call_Outcome_MortgageCalling__c = \'Call Back\' AND RecordType.Name = \'Mortgage TeleCalling List\')'+
                   ' OR ( Call_Back_Date__c = Today AND Mortgage_Call_Outcome__c = \'Call Back\' AND RecordType.Name = \'Mortgage Calling List\')' + 
                   ' OR ( Call_Back_Date__c = Today AND ( Welcome_Call_Outcome__c = \'Customer requests to Call Back Later\' OR Welcome_Call_Outcome__c = \'Call Back\'  ) AND RecordType.Name = \'Welcome Calling List\')' ;
                }
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Calling_List__c> callingList) {
        System.debug('callingList:***'+callingList);
        
        
        mapEmailTemplate = new Map<String,EmailTemplate>();
         for( EmailTemplate objEmailTemplate : [SELECT Id
                                                        , Subject
                                                        , Body
                                                        , Name
                                                        , HtmlValue
                                                        , TemplateType
                                                        , BrandTemplateId
                                                     FROM EmailTemplate 
                                                    WHERE Name IN ('DP followup Reminder','PTP Reminder','Call back Reminder')

            ] ) {
                
                if( objEmailTemplate != null && objEmailTemplate.Body != null ) {
                    mapEmailTemplate.put( objEmailTemplate.Name , objEmailTemplate );
                }
            }
            system.debug( ' mapEmailTemplate=== ' + mapEmailTemplate);
            
            
        List<OrgWideEmailAddress> orgWideAddress = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com' limit 1];
        List<Messaging.SingleEmailMessage> allMails = new List<Messaging.SingleEmailMessage>();
        
        Set<Id>ownerIdsSet = new Set<Id>();
        if(callingList != null && !callingList.isEmpty()){
            for(Calling_List__c callObj : callingList){
                ownerIdsSet.add(callObj.OwnerId);
            }
        }
        Map<Id,String> ownerIdToEmailMap = new Map<Id,String>();
        List<User>ownerEmailLst = [SELECT Id,
                                     Email,
                                     FirstName,
                                     LastName
                                FROM User 
                               WHERE Id IN :ownerIdsSet];
        if(ownerEmailLst != null && !ownerEmailLst.isEmpty()){
            for(User userObj : ownerEmailLst){
                ownerIdToEmailMap.put(userObj.Id,userObj.Email);
            }
        }
       
        for(Calling_List__c callObj  : callingList){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       
            String SubjectForMail = '';
            String bodyForMail = '';
            EmailTemplate objEmailTemplate;
            System.debug('Call_Outcome__c =='+callObj.Call_Outcome__c);
            
            if(callObj.Call_Outcome__c == 'Call Back' && callObj.Calling_List_Type__c != 'DP Calling' ){
                System.debug('Call Back');
                objEmailTemplate =  mapEmailTemplate.get( 'Call back Reminder' );
                SubjectForMail = objEmailTemplate.Subject;
                bodyForMail = objEmailTemplate.HtmlValue;
                
            }
            
            else if( callObj.Call_Outcome__c == 'Promise to Pay'&& callObj.Calling_List_Type__c != 'DP Calling'  ){
                System.debug('Promise to Pay');
                objEmailTemplate =  mapEmailTemplate.get( 'PTP Reminder');
                SubjectForMail = objEmailTemplate.Subject;
                bodyForMail = objEmailTemplate.HtmlValue;
            }
            else if( callObj.Calling_List_Type__c == 'DP Calling' ){
                System.debug('DP Calling');
                objEmailTemplate =  mapEmailTemplate.get('DP followup Reminder');
                SubjectForMail = objEmailTemplate.Subject;
                bodyForMail = objEmailTemplate.HtmlValue;
            }
           
            bodyForMail = String.isNotBlank(callObj.Customer_Name__c)?bodyForMail.replace('{CxName}', callObj.Customer_Name__c)
                :bodyForMail.replace('{CxName}', '');
                
            bodyForMail = String.isNotBlank(callObj.Name)?bodyForMail.replace('{CLName}', callObj.Name)
                :bodyForMail.replace('{CLName}', '');
                
            bodyForMail = String.isNotBlank(callObj.Unit_Name__c)?bodyForMail.replace('{unitName}', callObj.Unit_Name__c)
                :bodyForMail.replace('{unitName}', '');
            
            if(orgWideAddress.size() > 0){
                mail.setOrgWideEmailAddressId(orgWideAddress.get(0).Id);
            }
            
            mail.setSubject(SubjectForMail);
            mail.setHtmlBody(bodyForMail);
            mail.setToAddresses(new List<string>{ownerIdToEmailMap.get(callObj.OwnerId)});
            System.debug('mail::::::::::!!!!!!!'+mail);
            allMails.add(mail);
        }
        /*sending an email*/
        try{
            System.debug('You have made ' + Limits.getEmailInvocations() + ' email calls out of ' + Limits.getLimitEmailInvocations() + ' allowed');
            System.debug('allMails:::::allMails:::::!!!!!!!'+allMails);
            Messaging.sendEmail(allMails); 
            System.debug('You have made ' + Limits.getEmailInvocations() + ' email calls out of ' + Limits.getLimitEmailInvocations() + ' allowed');
            
        }
        catch(Exception e){
            System.debug('there something problem:while snding mail:'+e.getMessage());
        }
        
    }
    global void finish(Database.BatchableContext BC) {
    }
}