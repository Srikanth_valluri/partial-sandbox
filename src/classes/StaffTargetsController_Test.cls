@istest
public class StaffTargetsController_Test {
    
    @TestSetup
    static void makeData(){
        User HODUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
            UserRoleId = [Select id from UserRole where name = 'HOD14'].Id,
            LastName = 'last',
            Email = 'pc@damac.com',
            Username = 'pc@damac.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'aliasHOD',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            IPMS_Employee_ID__c = 'IHOD1234',
            HR_Employee_ID__c = 'HHODC1234'
        );

        insert HODUser;

        User HOSUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
            UserRoleId = [Select id from UserRole where name = 'HOS57'].Id,
            LastName = 'last',
            Email = 'pc@damac.com',
            Username = 'pc@damac.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'aliasHOS',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            IPMS_Employee_ID__c = 'IHOS1234',
            HR_Employee_ID__c = 'HHOS1234',
            ManagerId = HODUser.Id
        );

        insert HOSUser;

        User DOSUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
            UserRoleId = [Select id from UserRole where name = 'DOS554'].Id,
            LastName = 'last',
            Email = 'pc@damac.com',
            Username = 'pc@damac.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'aliasDOS',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            IPMS_Employee_ID__c = 'IDOS1234',
            HR_Employee_ID__c = 'HDOS1234',
            ManagerId = HOSUser.Id
        );

        insert DOSUser;


        User PCUser1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
            UserRoleId = [Select id from UserRole where name = 'PC464'].Id,
            LastName = 'last',
            Email = 'pc@damac.com',
            Username = 'pc@damac.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'aliaspc1',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            IPMS_Employee_ID__c = 'IPC1234',
            HR_Employee_ID__c = 'HPC1234',
            ManagerId = DOSUser.Id
        );

        insert PCUser1;

        User PCUser2 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
            LastName = 'last',
            Email = 'pc2@damac.com',
            Username = 'pc2@damac.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'aliaspc2',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            IPMS_Employee_ID__c = 'IPC11234',
            HR_Employee_ID__c = 'HPC11234',
            ManagerId = DOSUser.Id
        );

        insert PCUser2;
    }

    static testmethod void m1(){
        StaffTargetsController obj = new StaffTargetsController();
        obj.saveTargets();
        obj.filterData();
        obj.filterTarget.Month__c = System.now().format('MMMM');
        obj.filterTarget.Year__c = string.valueof(System.today().Year());
        obj.filterData();
        list<User> lstusers = [Select id, Name, Email, IPMS_Employee_ID__c, HR_Employee_ID__c,
                                    DOS_IPMS_ID__c, Manager.HR_Employee_ID__c, DOS_Name__c,
                                    HOS_IPMS_ID__c, Manager.Manager.HR_Employee_ID__c, HOS_Name__c,
                                    HOD_IPMS_ID__c, Manager.Manager.Manager.HR_Employee_ID__c, HOD_Name__c 
                                    from User 
                                    where isActive = true and Profile.Name = 'Property Consultant'
                                    order by name asc];

        string csvString = 'SF Identifier,PC_NAME,PC_EMP_ID,PC_ID,DOS_EMP_ID,DOS_ID,DOS_NAME,HOS_EMP_ID,HOS_ID,HOS_NAME,HOD_EMP_ID,HOD_ID,HOD_NAME,Target\n';
        for(User uRec : lstusers){
            csvString += uRec.Id+','+uRec.Name+','+uRec.IPMS_Employee_ID__c+','+(uRec.HR_Employee_ID__c!= null ? uRec.HR_Employee_ID__c:'')+',';
            csvString += uRec.DOS_IPMS_ID__c+','+uRec.Manager.HR_Employee_ID__c+','+uRec.DOS_Name__c+',';
            csvString += uRec.HOS_IPMS_ID__c+','+uRec.Manager.Manager.HR_Employee_ID__c+','+uRec.HOS_Name__c+',';
            csvString += uRec.HOD_IPMS_ID__c+','+uRec.Manager.Manager.Manager.HR_Employee_ID__c+','+uRec.HOD_Name__c+','+'200000'+'\n';
        }
        
        obj.saveTargets();
        
        obj.lstTargets[0].searchText = lstusers[0].Name;
        obj.searchText = lstusers[0].Name +' - '+(lstusers[0].HR_Employee_ID__c == null ? 'NA': lstusers[0].HR_Employee_ID__c);
        obj.rowIndex = '0';
        obj.userSelected();
        obj.lstTargets[0].tar.user__c = lstusers[0].Id;
        obj.selIndex = 0;
        obj.removeRow();
        obj.searchText = lstusers[0].Name +' - '+(lstusers[0].HR_Employee_ID__c == null ? 'NA': lstusers[0].HR_Employee_ID__c);
        obj.rowIndex = '0';
        obj.userSelected();

        obj.readCSVFile();
        obj.fileBody = blob.valueof(csvString);
        obj.readCSVFile();
        obj.saveTargets();
    }

    static testmethod void m2(){
        AllRMUsersController arc = new AllRMUsersController();
    }

}