@RestResource(urlMapping='/cancelBookingForUnitInsp/*')
global class HDApp_cancelBookingSlotForUnitInsp_API {
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Technical error'
    };
                
    @HttpPost
    global static FinalReturnWrapper cancelBookingSlotForUnit(){
        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);
        
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();
        
        
       if(r.params.containsKey('callingListId') && String.isBlank(r.params.get('callingListId'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : callingListId', 3);
            returnResponse.meta_data = objMeta;
            System.debug('Booking Unit 11');
            return returnResponse;
        }
        
        if( r.params.containsKey('callingListId') && String.isNotBlank(r.params.get('callingListId'))){
               System.debug('Once we get CallingListId---->');
               String callingListId = String.valueOf(r.params.get('callingListId'));
               List<Calling_List__c> callingList = HDApp_cancelAppointmentDetailsHandler.cancelAppointment(callingListId);
               System.debug('return callingList----->'+callingList);
               
               if(callingList!=null && callingList.size()>0){
                   if(callingList[0]. Appointment_Status__c == 'Cancelled'){
                       objMeta = ReturnMetaResponse('Your Scheduled Appointment is Cancelled', 1);
                       returnResponse.meta_data = objMeta;
                       return returnResponse;
                   }
                   else{
                       objMeta = ReturnMetaResponse('Your Appointment Cancellation is Failed', 2);
                       returnResponse.meta_data = objMeta;
                       return returnResponse;
                   }    
               }else{
                   objMeta = ReturnMetaResponse('No record found for callingListId', 2);
                   returnResponse.meta_data = objMeta;
                   return returnResponse;    
               }
            }
            returnResponse.data = objData;/*Added by Shubham - 27/09/2020*/
            System.debug('Response'+returnResponse);
            return returnResponse;
    }
    
    public static cls_meta_data ReturnMetaResponse(String message, Integer statusCode) {
        cls_meta_data meta_data = new cls_meta_data(); 
        meta_data.message = message;
        meta_data.status_code = statusCode;
        meta_data.title = mapStatusCode.get(statusCode);
        meta_data.developer_message = null;
        
        return meta_data;
    }
    
    global class FinalReturnWrapper {
        public cls_meta_data meta_data;
        public cls_data data;
    }
    
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }

    public class cls_data {
        //Added by Shubham - 27/09/2020 as discussed with Charith
    }
    
}