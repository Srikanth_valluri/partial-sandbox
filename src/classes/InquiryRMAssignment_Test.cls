@isTest
public with sharing class InquiryRMAssignment_Test {

    static testmethod void testInquiryUserAssignment1 () {
        List <user> userList = new List <User> ();
        userList = [SELECT Name, Email, UserName FROM User WHERE ID !=: UserInfo.getUserID () AND IsActive = TRUE AND Profile.Name = 'Property Consultant' LIMIT 2];
        
        List <Inquiry_User_Assignment_Rules__c> algList = new List <Inquiry_User_Assignment_Rules__c> ();
        Inquiry_User_Assignment_Rules__c alg = new Inquiry_User_Assignment_Rules__c ();
        alg.Daily__c = 1;
        alg.Weekly__c = 2;
        alg.User_Profile__c = 'Property Consultant';
        alg.Monthly__c = 2;
        alg.Net_Direct_Sales_Rank__c = 2;
        alg.setUpOwnerID = userList[0].ID;
        algList.add (alg);
        
        Inquiry_User_Assignment_Rules__c alg1 = new Inquiry_User_Assignment_Rules__c ();
        alg1.Daily__c = 0;
        alg1.Weekly__c = 0;
        alg1.Monthly__c = 2;
        alg1.Net_Direct_Sales_Rank__c = 1;
        alg1.setUpOwnerID = userList[1].id;
        alg1.User_Profile__c = 'Property Consultant';
        algList.add (alg1);
        
        insert algList;
        
        
        Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
        Inquiry__c inqObj = new Inquiry__c(Assignment_Queue_ID__c= 'Temp_TestQueue', RecordTypeId=agenTeamRT,Inquiry_Source__c='Agent Referral',Mobile_Phone_Encrypt__c='4561266553',Mobile_CountryCode__c='American Samoa: 001684',Mobile_Phone__c='1234',Email__c='mk162@gmail.com',First_Name__c='Test162',Last_Name__c='Last162',CR_Number__c='0987',ORN_Number__c='7842',Agency_Type__c='Corporate',Organisation_Name__c = 'Oliver',isDuplicate__c=false);
        insert inqObj ;
        
        Inquiry__c inqObjNew= new Inquiry__c(Assignment_Queue_ID__c= 'Temp_TestQueue', RecordTypeId=agenTeamRT,Inquiry_Source__c='Agent Referral',Mobile_Phone_Encrypt__c='456126553',Mobile_CountryCode__c='American Samoa: 001684',Mobile_Phone__c='1234',Email__c='mk12@gmail.com',First_Name__c='Test12',Last_Name__c='Last12',CR_Number__c='0987',ORN_Number__c='7842',Agency_Type__c='Corporate',Organisation_Name__c = 'Oliver',isDuplicate__c=false);
        Inquiry__c inqObjNew1 = new Inquiry__c(Assignment_Queue_ID__c= 'Temp_TestQueue', RecordTypeId=agenTeamRT,Inquiry_Source__c='Agent Referral',Mobile_Phone_Encrypt__c='42561265353',Mobile_CountryCode__c='American Samoa: 001684',Mobile_Phone__c='12324',Email__c='mk1232@gmail.com',First_Name__c='Test122',Last_Name__c='Last122',CR_Number__c='0987',ORN_Number__c='7842',Agency_Type__c='Corporate',Organisation_Name__c = 'Oliver',isDuplicate__c=false);
        
        List <Inquiry__C> inqList = new List <Inquiry__c> ();
        inqList.add (inqObjNew);
        inqList.add (inqObjNew1);
        InquiryRMAssignment.checkForRM (inqList);
        
    }
}