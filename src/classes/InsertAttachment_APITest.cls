@isTest
public class InsertAttachment_APITest {
    @isTest
    static void testApiDocument(){
        FM_Case__c objFMCase = new FM_Case__c();
        insert objFMCase;
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/uploadDocToSF'; 
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        InsertAttachment_API.apiMethod();
        Test.stopTest();
    }
    
    @isTest
    static void testApiFilename(){
        FM_Case__c objFMCase = new FM_Case__c();
        insert objFMCase;
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/uploadDocToSF';
        req.addParameter('documentName', '');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        InsertAttachment_API.apiMethod();
        Test.stopTest();
    }
    
    @isTest
    static void testApiBlankDoc(){
        FM_Case__c objFMCase = new FM_Case__c();
        insert objFMCase;
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/uploadDocToSF';  
        req.addParameter('documentName', '');
        req.addParameter('filename', 'test');
        //req.addParameter('processName', 'Tenant Registration');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        InsertAttachment_API.apiMethod();
        Test.stopTest();
    }
    
     @isTest
    static void testApiBlankFile(){
        FM_Case__c objFMCase = new FM_Case__c();
        insert objFMCase;
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/uploadDocToSF';
         req.addParameter('documentName', 'test');
        req.addParameter('filename', '');
        //req.addParameter('processName', 'Tenant Registration');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        InsertAttachment_API.apiMethod();
        Test.stopTest();
    }
    
     @isTest
    static void testApiBlankAttach(){
        FM_Case__c objFMCase = new FM_Case__c();
        insert objFMCase;
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/uploadDocToSF';
        req.addParameter('documentName', 'test');
        req.addParameter('filename', 'test');
        req.addParameter('attachmentId', '');
        //req.addParameter('processName', 'Tenant Registration');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        InsertAttachment_API.apiMethod();
        Test.stopTest();
    }
    
    @isTest
    static void testApiFMCaseNotNull(){

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;

        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Tenant_Registration',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId(),
                                           Request_Type__c = 'Tenant Registration',
                                           Booking_Unit__c = bookingUnit.Id);
        insert fmCase;

        SR_Attachments__c srAttach = new SR_Attachments__c();
        srAttach.Name = 'Test';
        srAttach.Attachment_URL__c = 'http://damacproperties.force.com/Documents/apex/GetFile?id=01NUEFKAW4NVD35H4Z7VEKEMYYSTVGULRT';
        srAttach.Type__c = 'JPG';
        srAttach.FM_Case__c = fmCase.Id;
        insert srAttach;    

        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/uploadDocToSF'; 
        req.addParameter('documentName', 'test');
        req.addParameter('filename', 'test.jpg');
        req.addParameter('attachmentId', srAttach.id);
        req.addParameter('fmCaseId', fmCase.Id);
        req.requestBody= Blob.valueOf('test');  
        //req.addParameter('processName', 'Tenant Registration');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        //Set mock response
        
        
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        
        Test.startTest();
        InsertAttachment_API.apiMethod();
        Test.stopTest();
    }
    
    @isTest
    static void testApiSRFMCaseNotNull(){
        Id personAcc_rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc1 = new Account( LastName = 'Test Account1',
                                       Party_ID__c = '63062',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'test1@mailinator.com');
        insert acc1;
        
        Id passDetailChgCase_rtId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Passport Detail Update').getRecordTypeId();
        FM_Case__c testPassDetChgCase1 = new FM_Case__c();
        testPassDetChgCase1.recordTypeId = passDetailChgCase_rtId;
        testPassDetChgCase1.Account__c = acc1.id;
        testPassDetChgCase1.Type__c = 'Passport Detail Update';
        testPassDetChgCase1.Request_Type__c = 'Passport Detail Update';
        testPassDetChgCase1.Origin__c = 'Portal';
        testPassDetChgCase1.Status__c = 'Draft Request';
        testPassDetChgCase1.New_CR__c = 'PL0098RT98765';
        testPassDetChgCase1.Passport_Issue_Date__c = Date.today();
        testPassDetChgCase1.Passport_Issue_Place__c = 'document_issue_place';
        testPassDetChgCase1.Passport_File_URL__c = 'passport_file_url';
        testPassDetChgCase1.Additional_Doc_File_URL__c = 'additional_doc_file_url';
        insert testPassDetChgCase1;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/uploadDocToSF'; 
        req.addParameter('documentName', 'passport');
        req.addParameter('filename', 'test.pdf');
        req.addParameter('fm_case_id', '');
        req.requestBody= Blob.valueOf('');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        
        // set Mock Response for Office 365 service callout
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        
        Test.startTest();
        
        InsertAttachment_API.apiMethod(); /* 1 */
        
        req.addParameter('fm_case_id', 'invalid_id');
        InsertAttachment_API.apiMethod(); /* 2 */
        
        req.addParameter('fm_case_id', acc1.Id);
        InsertAttachment_API.apiMethod(); /* 3 */
        
        req.addParameter('fm_case_id', testPassDetChgCase1.Id);
        InsertAttachment_API.apiMethod(); /* 4 */
        
        req.requestBody= Blob.valueOf('test');
        InsertAttachment_API.apiMethod(); /* 5 */
        
        Test.stopTest();
    }
    
    @isTest
    static void testApiStandardCaseNotNull(){
        Id personAcc_rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc1 = new Account( LastName = 'Test Account1',
                                       Party_ID__c = '63062',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'test1@mailinator.com');
        insert acc1;
        
        Id passDetailChgCase_rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Passport Detail Update').getRecordTypeId();
        Case testPassDetChgCase = new Case();
        testPassDetChgCase.recordTypeId = passDetailChgCase_rtId;
        testPassDetChgCase.AccountId = acc1.id;
        testPassDetChgCase.type = 'Passport Detail Update SR';
        testPassDetChgCase.SR_Type__c = 'Passport Detail Update SR';
        testPassDetChgCase.Origin = 'Portal';
        testPassDetChgCase.status = 'Draft Request';
        testPassDetChgCase.New_CR__c = 'PL0098RT98765';
        testPassDetChgCase.Passport_Issue_Date__c = Date.today();
        testPassDetChgCase.Passport_Issue_Place__c = 'document_issue_place';
        testPassDetChgCase.Passport_File_URL__c = 'passport_file_url';
        testPassDetChgCase.Additional_Doc_File_URL__c = 'additional_doc_file_url';
        insert testPassDetChgCase;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/uploadDocToSF'; 
        req.addParameter('documentName', 'passport');
        req.addParameter('filename', 'test.pdf');
        req.addParameter('case_id', testPassDetChgCase.Id);
        req.requestBody= Blob.valueOf('');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        
        // set Mock Response for Office 365 service callout
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        
        Test.startTest();
        
        InsertAttachment_API.apiMethod(); /* 1 */
        
        req.addParameter('case_id', 'invalid_id');
        InsertAttachment_API.apiMethod(); /* 2 */
        
        req.addParameter('case_id', acc1.Id);
        InsertAttachment_API.apiMethod(); /* 3 */
        
        req.addParameter('case_id', testPassDetChgCase.Id);
        InsertAttachment_API.apiMethod(); /* 4 */
        
        req.requestBody= Blob.valueOf('test');
        InsertAttachment_API.apiMethod(); /* 5 */
        
        Test.stopTest();
    }

    @isTest
    static void testGetPreviewURL(){

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;

        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Tenant_Registration',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId(),
                                           Request_Type__c = 'Tenant Registration',
                                           Booking_Unit__c = bookingUnit.Id);
        insert fmCase;

        SR_Attachments__c srAttach = new SR_Attachments__c();
        srAttach.Name = 'Test';
        srAttach.Attachment_URL__c = 'http://damacproperties.force.com/Documents/apex/GetFile?id=01NUEFKAW4NVD35H4Z7VEKEMYYSTVGULRT';
        srAttach.Type__c = 'JPG';
        srAttach.FM_Case__c = fmCase.Id;
        insert srAttach;    

        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/uploadDocToSF'; 
        req.addParameter('attachmentURL', 'http://damacproperties.force.com/Documents/apex/GetFile?id=01NUEFKAW4NVD35H4Z7VEKEMYYSTVGULRT');
        req.requestBody= Blob.valueOf('test');  
        //req.addParameter('processName', 'Tenant Registration');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        //Set mock response
        
        
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        
        Test.startTest();
        InsertAttachment_API.GetPreviewURL();
        Test.stopTest();
    }
    
    //@isTest
    //static void testTRCase(){
       
    //    FM_Case__c objFMCase = new FM_Case__c();
    //    insert objFMCase;
        
    //    Test.startTest();
    //    InsertAttachment_API.createTRCase();
    //    InsertAttachment_API.getTRCase(objFMCase.Id);
    //    Test.stopTest();
        
    //}
}