@isTest
public class MortgageAutoCloseTaskInSFTest {
    
    private static Id getRecordTypeId(){
        return Schema.SObjectType.Case.getRecordTypeInfosByName().get('Mortgage').getRecordTypeId();
    }

    @isTest static void testMortgage_10(){
        Account accObj = TestDataFactory_CRM.createPersonAccount();
        insert accObj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accObj.Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;
        
        List<Case> lstCase = createCase('Mortgage',1,accObj.Id );
        lstCase[0].Mortgage_Status__c = 'Mortgage Final Offer Letter';
        lstCase[0].Booking_Unit__c =   bookingUnitsObj[0].Id;
        lstCase[0].Type =   'Mortgage';
        lstCase[0].Customer_Eligible_For_Mortgage__c =   true;
        lstCase[0].Approved_by_Bank__c =   true;
        insert lstCase ;
        

        Task taskObj1 = new Task();
        taskObj1.WhatId = lstCase[0].Id;
        taskObj1.Priority = 'High';
        taskObj1.ActivityDate = System.Today();
        taskObj1.Status = 'Closed';
        taskObj1.Subject = System.Label.Mortgage_10;
        taskObj1.Assigned_User__c = 'CRE';
        taskObj1.Process_Name__c = 'Mortgage';
        taskObj1.Task_Due_Date__c = System.Today();
        //taskObj1.Completed_DateTime__c = System.Now();
        insert taskObj1;
        

        List<SR_Attachments__c> lstSR_Attachments  = new List<SR_Attachments__c>();
        
        SR_Attachments__c attachObj =new SR_Attachments__c();
        attachObj = TestDataFactory_CRM.createCaseDocument(lstCase[0].Id, 'Final Offer Letter');
        attachObj.Name='Final Offer Letter';
        lstSR_Attachments.add(attachObj);
        insert lstSR_Attachments;

        MortgageAutoCloseTaskInSF.UpdateTaskInSF(lstSR_Attachments);

    }
 public static List<Case> createCase( String recordTypeDevName, Integer counter ,Id accId  ) {

        Id recodTypeIdCase = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType=:'Case'
            AND DeveloperName=:recordTypeDevName
            AND IsActive = TRUE LIMIT 1
        ].Id;

        List<Case> lstCase = new List<Case>();
        for( Integer i=0; i<counter; i++ ) {
            lstCase.add(new Case( AccountId = accId, Credit_Note_Amount__c=5000, RecordTypeId = recodTypeIdCase ) );
        }
        return lstCase;
    }
    @isTest static void testMortgage_13(){
        Account accObj = TestDataFactory_CRM.createPersonAccount();
        insert accObj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accObj.Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;
        
        List<Case> lstCase = createCase('Mortgage',1,accObj.Id );
        lstCase[0].Mortgage_Status__c = 'Mortgage Final Offer Letter';
        lstCase[0].Booking_Unit__c =   bookingUnitsObj[0].Id;
        lstCase[0].Type =   'Mortgage';
        lstCase[0].Customer_Eligible_For_Mortgage__c =   true;
        lstCase[0].Approved_by_Bank__c =   true;
        insert lstCase ;
        

       // Task taskObj2 = new Task();
        Task taskObj2 = new Task();
        taskObj2.WhatId = lstCase[0].Id;
        taskObj2.Priority = 'High';
        taskObj2.ActivityDate = System.Today();
        taskObj2.Status = 'Closed';
        taskObj2.Subject = System.Label.Mortgage_13;
        taskObj2.Assigned_User__c = 'CRE';
        taskObj2.Process_Name__c = 'Mortgage';
        taskObj2.Task_Due_Date__c = System.Today();
        //taskObj2.Completed_DateTime__c = System.Now();
        insert taskObj2;
        

        List<SR_Attachments__c> lstSR_Attachments  = new List<SR_Attachments__c>();
        
        SR_Attachments__c attachObj =new SR_Attachments__c();
        attachObj = TestDataFactory_CRM.createCaseDocument(lstCase[0].Id, 'Original Letter of Guarantee');
        attachObj.Name='Original Letter of Guarantee';
        lstSR_Attachments.add(attachObj);
        insert lstSR_Attachments;

        MortgageAutoCloseTaskInSF.UpdateTaskInSF(lstSR_Attachments);

    }
    @isTest static void testMortgage_12(){
        Account accObj = TestDataFactory_CRM.createPersonAccount();
        insert accObj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accObj.Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;
        
        List<Case> lstCase = createCase('Mortgage',1,accObj.Id );
        lstCase[0].Mortgage_Status__c = 'Mortgage Final Offer Letter';
        lstCase[0].Booking_Unit__c =   bookingUnitsObj[0].Id;
        lstCase[0].Type =   'Mortgage';
        lstCase[0].Customer_Eligible_For_Mortgage__c =   true;
        lstCase[0].Approved_by_Bank__c =   true;
        insert lstCase ;
        

          Task taskObj2 = new Task();
        taskObj2.WhatId = lstCase[0].Id;
        taskObj2.Priority = 'High';
        taskObj2.ActivityDate = System.Today();
        taskObj2.Status = 'Closed';
        taskObj2.Subject = System.Label.Mortgage_12;
        taskObj2.Assigned_User__c = 'CRE';
        taskObj2.Process_Name__c = 'Mortgage';
        taskObj2.Task_Due_Date__c = System.Today();
        //taskObj2.Completed_DateTime__c = System.Now();
        insert taskObj2;
        

        List<SR_Attachments__c> lstSR_Attachments  = new List<SR_Attachments__c>();
        
        SR_Attachments__c attachObj =new SR_Attachments__c();
        attachObj = TestDataFactory_CRM.createCaseDocument(lstCase[0].Id, 'Original Letter of Guarantee');
        attachObj.Name='Lien Letter';
        lstSR_Attachments.add(attachObj);
        insert lstSR_Attachments;

        MortgageAutoCloseTaskInSF.UpdateTaskInSF(lstSR_Attachments);

    }
    @isTest static void testMortgage_22(){
        Account accObj = TestDataFactory_CRM.createPersonAccount();
        insert accObj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accObj.Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;
        
        List<Case> lstCase = createCase('Mortgage',1,accObj.Id );
        lstCase[0].Mortgage_Status__c = 'Mortgage Final Offer Letter';
        lstCase[0].Booking_Unit__c =   bookingUnitsObj[0].Id;
        lstCase[0].Type =   'Mortgage';
        lstCase[0].Customer_Eligible_For_Mortgage__c =   true;
        lstCase[0].Approved_by_Bank__c =   true;
        insert lstCase ;
        

       // Task taskObj2 = new Task();
        Task taskObj2 = new Task();
        taskObj2.WhatId = lstCase[0].Id;
        taskObj2.Priority = 'High';
        taskObj2.ActivityDate = System.Today();
        taskObj2.Status = 'Closed';
        taskObj2.Subject = System.Label.Mortgage_22;
        taskObj2.Assigned_User__c = 'CRE';
        taskObj2.Process_Name__c = 'Mortgage';
        taskObj2.Task_Due_Date__c = System.Today();
        //taskObj2.Completed_DateTime__c = System.Now();
        insert taskObj2;
        

        List<SR_Attachments__c> lstSR_Attachments  = new List<SR_Attachments__c>();
        
        SR_Attachments__c attachObj =new SR_Attachments__c();
        attachObj = TestDataFactory_CRM.createCaseDocument(lstCase[0].Id, 'Original Letter of Guarantee');
        attachObj.Name='NOC';
        lstSR_Attachments.add(attachObj);
        insert lstSR_Attachments;

        MortgageAutoCloseTaskInSF.UpdateTaskInSF(lstSR_Attachments);

    }
}