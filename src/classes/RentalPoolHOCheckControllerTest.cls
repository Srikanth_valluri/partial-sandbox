@isTest
public class RentalPoolHOCheckControllerTest {
    static Account objAcc;
    //static Case objCase;
    static Id recTypeRPHOAgreement;
    static NSIBPM__Service_Request__c objDealSR;
    static list<Booking__c> listCreateBookingForAccount;
    //static list<Booking_Unit__c> listCreateBookingUnit;
    
    static void init() {
        objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);

        recTypeRPHOAgreement = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Agreement').getRecordTypeId();
        system.debug('recTypeRPHOAgreement=='+recTypeRPHOAgreement);

        objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        
    }

    @isTest static void testWithHOtrue() {
        init();
        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        listCreateBookingUnit[0].Handover_Flag__c = 'Y';
        insert listCreateBookingUnit;

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPHOAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        insert objCase;

        Task taskObj = TestDataFactory_CRM.createTask(objCase, Label.TaskHOCompletion, 'CRE', 'Rental Pool Agreement', date.today());
        taskObj.OwnerId = UserInfo.getUserId();
        insert taskObj;

        test.StartTest();
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolHOCheckController objHoCheck = new RentalPoolHOCheckController(stdController);
        objHoCheck.caseId = objCase.Id;
        objHoCheck.checkHOStatus();
        test.StopTest();
    }

    @isTest static void testWithHOfalse() {
        init();
        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        listCreateBookingUnit[0].Handover_Flag__c = 'N';
        insert listCreateBookingUnit;

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPHOAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        insert objCase;

        Task taskObj = TestDataFactory_CRM.createTask(objCase, Label.TaskHOCompletion, 'CRE', 'Rental Pool Agreement', date.today());
        taskObj.OwnerId = UserInfo.getUserId();
        insert taskObj;

        test.StartTest();
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolHOCheckController objHoCheck = new RentalPoolHOCheckController(stdController);
        objHoCheck.caseId = objCase.Id;
        objHoCheck.checkHOStatus();
        test.StopTest();
    }

     @isTest static void testWithHOCasetrue() {
        init();
        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        listCreateBookingUnit[0].Handover_Flag__c = 'Y';
        insert listCreateBookingUnit;

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPHOAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Handover__c = true;
        insert objCase;

        Task taskObj = TestDataFactory_CRM.createTask(objCase, Label.TaskHOCompletion, 'CRE', 'Rental Pool Agreement', date.today());
        taskObj.OwnerId = UserInfo.getUserId();
        insert taskObj;

        test.StartTest();
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolHOCheckController objHoCheck = new RentalPoolHOCheckController(stdController);
        objHoCheck.caseId = objCase.Id;
        objHoCheck.checkHOStatus();
        test.StopTest();
    }

    @isTest static void testWithHONoBookingUnit() {
        init();
        /*list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        listCreateBookingUnit[0].Early_Handover__c = true;
        insert listCreateBookingUnit;*/

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPHOAgreement);
        objCase.AccountId = objAcc.Id;
        //objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        //objCase.Early_Handover__c = true;
        insert objCase;

        Task taskObj = TestDataFactory_CRM.createTask(objCase, Label.TaskHOCompletion, 'CRE', 'Rental Pool Agreement', date.today());
        taskObj.OwnerId = UserInfo.getUserId();
        insert taskObj;

        test.StartTest();
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolHOCheckController objHoCheck = new RentalPoolHOCheckController(stdController);
        objHoCheck.caseId = objCase.Id;
        objHoCheck.checkHOStatus();
        test.StopTest();
    }
}