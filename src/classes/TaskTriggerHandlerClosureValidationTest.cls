@isTest
public class TaskTriggerHandlerClosureValidationTest{
    private static testmethod void DIFCClosure(){
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Case objC = new Case();
        objC.Status = 'New';
        objC.Origin = 'Walk-In';
        objC.RecordTypeId = recTypeId;
        insert objC;
        
        Task objT = new Task();
        objT.Subject = Label.DIFC_Task_Print_NOC;
        objT.WhatId = objC.Id;
        objT.Status = 'Not Started';
        objT.Process_Name__c = 'Assignment';
        objT.OwnerId = UserInfo.getUserId();
        insert objT;
        
        try{
            objT.Status = 'Completed';
            update objT;
        }catch(Exception ex){
        }
    }
    
    private static testmethod void DubaiClosure(){
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Case objC = new Case();
        objC.Status = 'New';
        objC.Origin = 'Walk-In';
        objC.RecordTypeId = recTypeId;
        insert objC;
        
        Task objT = new Task();
        objT.Subject = Label.Dubai_Task_Print_NOC;
        objT.WhatId = objC.Id;
        objT.Status = 'Not Started';
        objT.Process_Name__c = 'Assignment';
        objT.OwnerId = UserInfo.getUserId();
        insert objT;
        
        try{
            objT.Status = 'Completed';
            update objT;
        }catch(Exception ex){
        }
    }
    
    private static testmethod void ValidClosure(){
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Case objC = new Case();
        objC.Status = 'New';
        objC.Origin = 'Walk-In';
        objC.RecordTypeId = recTypeId;
        insert objC;
        
        Task objT = new Task();
        objT.Subject = Label.Dubai_Task_Print_NOC;
        objT.WhatId = objC.Id;
        objT.Status = 'Not Started';
        objT.Process_Name__c = 'Assignment';
        objT.OwnerId = UserInfo.getUserId();
        insert objT;
        
        SR_Attachments__c objS = new SR_Attachments__c();
        objS.Name = 'NOC Document - '+system.now();
        objS.Case__c = objC.Id;
        objS.Attachment_URL__c = 'ww.google.com';
        objS.IsValid__c = true;
        objS.IsRequired__c = true;
        insert objS;
        
        try{
            objT.Status = 'Completed';
            update objT;
        }catch(Exception ex){
        }
    }
    
    private static testmethod void taskRejection(){
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Case objC = new Case();
        objC.Status = 'New';
        objC.Origin = 'Walk-In';
        objC.RecordTypeId = recTypeId;
        insert objC;
        
        Task objT = new Task();
        objT.Subject = Label.Dubai_Task_Print_NOC;
        objT.WhatId = objC.Id;
        objT.Status = 'Not Started';
        objT.Process_Name__c = 'Assignment';
        objT.Assigned_User__c = 'CRE';
        objT.ActivityDate = date.today();
        objT.OwnerId = UserInfo.getUserId();
        insert objT;
        
        try{
        Task objT1 = new Task();
        objT1.Subject = Label.Dubai_Task_Print_NOC+' - Rejected';
        objT1.WhatId = objC.Id;
        objT1.Status = 'Not Started';
        objT1.Parent_Task_Id__c = objT.Id;
        objT1.Process_Name__c = 'Assignment';
        objT1.Assigned_User__c = 'CRE';
        objT1.ActivityDate = date.today();
        objT1.OwnerId = UserInfo.getUserId();
        insert objT1;
        
        objT1.Status = 'Completed';
        update objT1;
        
        }catch(Exception ex){
        }
    }


    private static testmethod void testRiyadhTask(){
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Riyadh Rotana Conversion').getRecordTypeId();
        Case objC = new Case();
        objC.Status = 'Submitted';
        objC.Origin = 'Walk-In';
        objC.RecordTypeId = recTypeId;
        insert objC;
        
        Task objT = new Task();
        objT.Subject = 'Update whether customer is interested in the hotel operation or not';
        objT.WhatId = objC.Id;
        objT.Status = 'Not Started';
        objT.Process_Name__c = 'Riyadh Rotana Conversion';
        objT.Assigned_User__c = 'CRE';
        objT.ActivityDate = date.today();
        objT.OwnerId = UserInfo.getUserId();
        insert objT;
        
        try{
        objT.Status = 'Completed';
        update objT;
        
        }catch(Exception ex){
        }
    }
}