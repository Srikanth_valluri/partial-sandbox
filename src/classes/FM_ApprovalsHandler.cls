/**
 * Generic class for handling all the approvals in FM Processes 
 *
 * Revision History
 * 19-12-2018   Ashish Agarwal      Extracted the approval code for FMCaseTriggerHelper class.
 * 18-10-2020   Aishwarya Todkar    Added null  check for objFMCase.Approving_Authorities__c
 * 22-10-2020   Aishwarya Todkar    Checked if record is locked before submitting for approval
 * 26-10-2020   Aishwarya Todkar    Added blank checks
 *
 **/

public without sharing class FM_ApprovalsHandler {
    
    public final Id objFMCaseId ;
    
    public static void callApprovalProcess( map<Id,FM_Case__c> mapNewFMCases, map<Id,FM_Case__c> mapOldFMCases ) {
        if( mapNewFMCases != null && !mapNewFMCases.isEmpty() && mapOldFMCases != null && !mapOldFMCases.isEmpty() ) {
            initiateApproval( mapNewFMCases, mapOldFMCases );
        }
    }
    
    public static void initiateApproval( map<Id,FM_Case__c> mapNewFMCases, map<Id,FM_Case__c> mapOldFMCases ) {
        
        list<Id> lstEligibleCasesId = ApprovalHelper.extractEligibleCaseIds( mapNewFMCases, mapOldFMCases );
        system.debug('==lstEligibleCasesId=='+lstEligibleCasesId);
        if( lstEligibleCasesId != NULL && !lstEligibleCasesId.isEmpty() ) {
            system.debug('in if app');
            if( System.isBatch() || System.isFuture() ) {
                submitCaseForApproval( lstEligibleCasesId );
            } else {
                submitCaseForApprovalAsync( lstEligibleCasesId );
            }
        }

        list<Id> lstApprovedCasesId = ApprovalHelper.extractApprovedCaseIds( mapNewFMCases, mapOldFMCases );
        if( lstApprovedCasesId != NULL && !lstApprovedCasesId.isEmpty() ) {
            if ( System.isBatch() || System.isFuture() ) {
                markCaseAsApproved( lstApprovedCasesId );
            } else {
                markCaseAsApprovedAsync( lstApprovedCasesId );
            }
        }
    }
    
    @future
    public static void submitCaseForApprovalAsync( list<Id> lstEligibleCasesId ) {
        submitCaseForApproval( lstEligibleCasesId );
    }
    
    @future
    public static void markCaseAsApprovedAsync( list<Id> lstApprovedCasesId ) {
        markCaseAsApproved( lstApprovedCasesId );
    }

    public static void markCaseAsApproved( list<Id> lstApprovedCasesId ) {
        if( lstApprovedCasesId != NULL && !lstApprovedCasesId.isEmpty() ) {
            list<FM_Case__c> lstApprovedCases = new list<FM_Case__c>();
            for( Id caseId : lstApprovedCasesId ) {
                lstApprovedCases.add( new FM_Case__c( Id = caseId,
                                                      Approval_Status__c = 'Approved',
                                                      Current_Approver__c = '' ) );
            }

            update lstApprovedCases ;
        }
    }
    
    public static void submitCaseForApproval( list<Id> lstEligibleCasesId ) {
        system.debug('==submitCaseForApproval method called=='+lstEligibleCasesId);
        list<FM_Case__c> lstEligibleCases = FM_Utility.getCaseDetails( lstEligibleCasesId );
        map<String, map<String, Id>> mapLocationRoleUser ;
        
        if( lstEligibleCases != NULL && !lstEligibleCases.isEmpty() ) {
            set<String> setLocationNames = ApprovalHelper.extractLocationNames( lstEligibleCases );
            if( !setLocationNames.isEmpty() ) {
                list<FM_User__c> lstApprovingUsers = FM_Utility.getApprovingUsers( setLocationNames );
                if( !lstApprovingUsers.isEmpty() ) {
                    mapLocationRoleUser = ApprovalHelper.getLocationRolesMap( lstApprovingUsers );
                }
            }
        }
        
        
        if( lstEligibleCases != NULL && !lstEligibleCases.isEmpty() &&
            mapLocationRoleUser != NULL && !mapLocationRoleUser.isEmpty() ) {
            List<Approval.ProcessSubmitRequest> lstApprovalRequest = new List<Approval.ProcessSubmitRequest>();
            
            FM_Process__mdt objTenantMetadata ;
            
            for( FM_Case__c objFMCase : lstEligibleCases ) {

                if( String.isNotBlank( objFMCase.Request_Type_DeveloperName__c ) ) {
                    String strProjName = String.isNotBlank( objFMCase.Unit_Name__c ) 
                                        ? objFMCase.Unit_Name__c.subString( 0, objFMCase.Unit_Name__c.indexOf( '/' ) ) : '';
                    system.debug('==strProjName=='+strProjName);
                    if( objFMCase.Request_Type_DeveloperName__c.equalsIgnoreCase( 'Tenant_Registration' ) ) {
                        if( String.isNotBlank( objFMCase.Current_Approver__c ) && 
                          ( objFMCase.Current_Approver__c.containsIgnoreCase('__') ) ) {
                          objFMCase.Recall_Approvals__c = true ;
                        }
                        String strLevel = '';
                        if( String.isNotBlank( objFMCase.Approving_Authorities__c ) ) {
                            strLevel =  objFMCase.Approving_Authorities__c.contains( ',' ) 
                                            ? objFMCase.Approving_Authorities__c.split(',')[0] : objFMCase.Approving_Authorities__c;
                            for( String strRole : strLevel.split('__') ) {
                                if( String.isNotBlank( strProjName ) &&  mapLocationRoleUser.containsKey( strProjName ) ) {
                                    if( mapLocationRoleUser.get( strProjName ).containsKey( strRole ) ) {
                                        if( !Approval.isLocked( objFMCase.Id ) ) { //Added by Aishwarya Todkar on 22-10-2020
                                            lstApprovalRequest.add( ApprovalHelper.createApprovalRequest ( 'Submitting request for approval -' + strRole,
                                                                                                    objFMCase.Id,
                                                                                                    mapLocationRoleUser.get( strProjName ).get( strRole ),
                                                                                                    'FM_Approval_Process' ) );
                                        }
                                    }
                                }
                            }
                        }
                        if( String.isNotBlank( strLevel )  ) { 
                            if( lstApprovalRequest.isEmpty() ) {

                                if( String.isNotBlank( objFMCase.Approving_Authorities__c ) ) {
                                    objFMCase.Approving_Authorities__c = objFMCase.Approving_Authorities__c.removeStart( strLevel )
                                                                                                        .removeStart(',')
                                                                                                        .removeEnd(',');
                                }
                                if( String.isBlank( objFMCase.Approving_Authorities__c ) ) {
                                    objFMCase.Approval_Status__c = 'Approved' ;
                                    objFMCase.Current_Approver__c='' ;
                                }
                            }
                            else {  
                                objFMCase.Current_Approver__c = strLevel ;
                            }
                        }
                        else {
                            objFMCase.Approval_Status__c = 'Approved' ;
                            objFMCase.Current_Approver__c='' ;
                        }
                    }
                    else if( objFMCase.Request_Type_DeveloperName__c.equalsIgnoreCase( 'Penalty_Waiver' ) ) {
                      list<User> lstUsers = [ SELECT Id
                                                FROM User
                                               WHERE Name =: objFMCase.Approving_Authorities__c
                                                 AND IsActive = true ];
                        system.debug('lstUsers==='+lstUsers);
                     if( !lstUsers.isEmpty() ) {
                        if( !Approval.isLocked( objFMCase.Id ) ) { //Added by Aishwarya Todkar on 22-10-2020
                            lstApprovalRequest.add( ApprovalHelper.createApprovalRequest ( 'Submitting request for approval.',
                                                                                           objFMCase.Id,
                                                                                           lstUsers[0].Id,
                                                                                           'FM_Approval_Process' ) );
                            objFMCase.Current_Approver__c = objFMCase.Approving_Authorities__c ;
                        }
                     }
                     else {
                       objFMCase.Approving_Authorities__c = '';
                     }
                      
                    }
                    else if( objFMCase.Request_Type_DeveloperName__c.equalsIgnoreCase( 'Gate_Pass_Request' ) 
                    && objFMCase.booking_unit__c != null 
                    && String.isNotBlank( objFMCase.booking_unit__r.Property_Name__c )
                    && objFMCase.booking_unit__r.Property_Name__c.contains(label.Damac_Hills) 
                    && objFMCase.Is_Task_created_for_Damac_Hills__c == false){
                        system.debug('in damac hills======');
                        objFMCase.Is_Task_created_for_Damac_Hills__c = true;
                    } else  {
                        //system.debug('non damac hills?'+objFMCase.booking_unit__r.Property_Name__c.contains(label.Damac_Hills));
                        system.debug('objFMCase.Request_Type_DeveloperName__c======'+objFMCase.Request_Type_DeveloperName__c);
                        system.debug('strProjName======'+strProjName);
                        String strRole =   String.isNotBlank( objFMCase.Approving_Authorities__c ) ? objFMCase.Approving_Authorities__c.split(',')[0] : '';
                        system.debug('strRole======'+strRole);
                        //system.debug('mapLocationRoleUser.get( strProjName ).containsKey( strRole )======'+mapLocationRoleUser.get( strProjName ).containsKey( strRole ));
                        if( String.isNotBlank( strProjName ) &&  mapLocationRoleUser.containsKey( strProjName )) {
                            if(  String.isNotBlank( strRole ) && mapLocationRoleUser.get( strProjName ) != null 
                            && mapLocationRoleUser.get( strProjName ).containsKey( strRole ) ) {
                                if( !Approval.isLocked( objFMCase.Id ) ) { //Added by Aishwarya Todkar on 22-10-2020
                                    lstApprovalRequest.add( ApprovalHelper.createApprovalRequest ( 'Submitting request for approval.',
                                                                                            objFMCase.Id,
                                                                                            mapLocationRoleUser.get( strProjName ).get( strRole ),
                                                                                            'FM_Approval_Process' ) );
                                    objFMCase.Current_Approver__c = strRole ;
                                }
                            }
                            else {
                                if( String.isNotBlank( objFMCase.Approving_Authorities__c ) && String.isNotBlank( strRole ) ) {
                                    objFMCase.Approving_Authorities__c = objFMCase.Approving_Authorities__c.removeStart( strRole )
                                                                                                    .removeStart(',')
                                                                                                    .removeEnd(',');
                                }
                                else if( String.isBlank( objFMCase.Approving_Authorities__c ) ) {
                                    objFMCase.Approval_Status__c = 'Approved' ;
                                    objFMCase.Current_Approver__c='' ;
                                }
                            }
                        }
                    }
                }//End if Request_Type_DeveloperName__c
            }
            List<FM_Case__c> lstFMCasesToUpdate = new List<FM_Case__c>();
            for( FM_Case__c objFMCase : lstEligibleCases) {
                if( !Approval.isLocked(objFMCase.Id) ) {
                    lstFMCasesToUpdate.add( objFMCase );
                }
            }

            if( !lstFMCasesToUpdate.isEmpty() ) {
                update lstFMCasesToUpdate;
            }

            if( !lstApprovalRequest.isEmpty() ) {
                Approval.ProcessResult[] lstApprovalResult = Approval.process(lstApprovalRequest);
            }
            
        }
    }
    
    /*public static void recallPendingApprovals( Id targetObjectId ) {
        FM_case__c insFMCase = new FM_case__c(id = targetObjectId);
        list<ProcessInstanceWorkitem> lstPendingWorkItems = [ SELECT Id,ProcessInstanceId
                                                            FROM ProcessInstanceWorkitem 
                                                           WHERE ProcessInstance.TargetObjectId = :targetObjectId
                                                             AND ProcessInstance.Status = 'Pending' ];
        if( !lstPendingWorkItems.isEmpty() ) {
            ProcessInstanceStep processInsStep = [ SELECT Comments 
                                 FROM ProcessInstanceStep 
                                WHERE ProcessInstanceId = :lstPendingWorkItems[0].ProcessInstanceId];
            if(processInsStep != Null){
                if(processInsStep.Comments.split('-')[1] == 'FM Manager'){
                    insFMCase.Property_Manager_Email__c    = '';
                }else{
                    insFMCase.FM_Manager_Email__c = '';
                }
            }
            Approval.ProcessWorkitemRequest objWorkItem = new Approval.ProcessWorkitemRequest();
            objWorkItem.setAction( 'Removed' );
            objWorkItem.setWorkItemId( lstPendingWorkItems[0].Id );
            Approval.ProcessResult result = Approval.process( objWorkItem );
        }
        update insFMCase;
    }*/
    
    
}