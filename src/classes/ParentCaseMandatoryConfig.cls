/***********************************************************************************************************************
Description: Class to get Parent Case Configurations.
========================================================================================================================
Version | Date(DD-MM-YYYY)  | Author            | Comments
------------------------------------------------------------------------------------------------------------------------
1.0     | 24-07-2019        | Aishwarya Todkar  | 1.Initial Draft
************************************************************************************************************************/

public with sharing Class ParentCaseMandatoryConfig {

/***********************************************************************************************************************
Description : Method to get parent case component mandatory config once the conditions satisfies in SF
              rule against single process and object 
Parameters  : Process Name, Record Id
Return Type : String ( rule )
************************************************************************************************************************/ 
    public static Boolean checkMandatory( String processName, Id recordId ) {

        String sfRule = !Test.isRunningTest() ? CollectionPromotionConfig.filterSfRuleEngine( processName, recordId ) 
                        : 'rule 1:Case-Overude Rebate Parent Case';
        
        System.debug('====> sfRule >' + sfRule); //IMP
        
        if( String.isNotBlank( sfRule ) && sfRule.contains('-') && sfRule.contains(':') ) {
            String ruleAndObj = sfRule.split('-')[0];
            processName = sfRule.split('-')[1];
            String objectName = ruleAndObj.split(':')[0];
            String ruleNumber = ruleAndObj.split(':')[1];

            System.debug('====processName ' + processName); //IMP
            System.debug('====objectName ' + objectName);   //IMP
            System.debug('====ruleNumber ' + ruleNumber);   //IMP

            List<Parent_Case_Configs__c> listParentCaseConfig = new List<Parent_Case_Configs__c>();
            if( String.isNotBlank( ruleNumber ) 
            && String.isNotBlank( processName )  
            && String.isNotBlank( objectName ) ) {

                listParentCaseConfig = [SELECT
                                        Mandatory__c
                                        , Process_Name__c
                                        , Rule_Number__c
                                        , Object_API_Name__c
                                    FROM
                                        Parent_Case_Configs__c
                                    WHERE
                                        Rule_Number__c =: ruleNumber
                                    AND
                                        Process_Name__c =: processName
                                    AND
                                        Object_API_Name__c =: objectName
                                    ORDER BY
                                        Rule_Number__c ];
                
                System.debug( 'listParentCaseConfig == ' + listParentCaseConfig); //IMP

                return listParentCaseConfig != null && listParentCaseConfig.size() > 0 ? listParentCaseConfig[0].Mandatory__c: false;
            }
        }
        return false;
    }
}