public class CLRebateCalculatorHandler {
    public String currentRecordId {get;set;}
    public Calling_List__c callingList {get;set;}
    public List<FinalRespWrapper> finalResp {get;set;}
    //public FinalRespWrapper curRecord {get;set;}
    public String requestBody {get;set;}
    public String responseBody {get;set;}
    public CLRebateCalculatorHandler(){
        currentRecordId  = ApexPages.CurrentPage().getparameters().get('Id');
        if(!Test.isRunningTest()){
            callingList = [select Booking_Unit__r.Registration_ID__c,Booking_Unit__r.Unit_Name__c,Booking_Unit__r.Requested_Price__c,Name from Calling_List__c where Id =: currentRecordId];
        }
        //finalResp = new List<FinalRespWrapper>();
    }
    
    public void getDetails(){
        
        Credentials_Details__c creds = Credentials_Details__c.getInstance( 'Rebate Calculator' );        
        
        if( creds != null ) {
            String userName = creds.user_Name__c;
            String password = creds.password__c;
            String endPoint = creds.endPoint__c;
            
            HttpRequest req = new HttpRequest();
            //req.setEndpoint('http://83.111.194.181:8033/webservices/rest/XXDC_PROCESS_SERVICE_WS/retrieve/');
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json; charset=UTF-8');
            
            //String username = 'oracle_user';
            //String password = 'crp1user';  
            
            Blob headerValue = Blob.valueOf(username + ':' + password);
            String authorizationHeader = 'Basic ' + 
                EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);
            
            BookingUnitWrapper reqWrapper = new BookingUnitWrapper();
            if(Test.isRunningTest()){
                reqWrapper.RETRIEVE_Input.convertToWrapper('85651');
            }else{
                reqWrapper.RETRIEVE_Input.convertToWrapper(string.valueof(callingList.Booking_Unit__r.Registration_ID__c));
            }
            
            //reqWrapper.convertToWrapper(string.valueof(bookingUnit.Registration_ID__c));
            String jsonString = JSON.serialize(reqWrapper);
            
            req.setBody(jsonString);
            //system.debug(jsonString);
            requestBody = jsonString;
            
            Http http = new Http();        
            HTTPResponse res = http.send(req);
            String respBody = res.getBody();
            responseBody = respBody;
            //BookingUnitWrapper.ResponseWrapper respWrap = (BookingUnitWrapper.ResponseWrapper) JSON.deserialize(respBody, BookingUnitWrapper.ResponseWrapper.class);
            WrapResponse respWrap = (WrapResponse) System.JSON.deserialize(respBody, WrapResponse.class);
            
            if(res.getStatusCode() == 200) {
                List<FinalRespWrapper> respWrapList = new List<FinalRespWrapper>();
                if(respWrap != null
                   && respWrap.OutputParameters != null
                   && respWrap.OutputParameters.X_RETURN_STATUS == 'S'
                   && respWrap.OutputParameters.X_RESPONSE_MESSAGE != null 
                   && respWrap.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM != null
                   && respWrap.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.size() > 0){
                       //List<respWrap.OutputParameters.X_RESPONSE_MESSAGE> listsamp = respWrap.OutputParameters.X_RESPONSE_MESSAGE ;
                       for( X_RESPONSE_MESSAGE_ITEM msgItem : respWrap.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM ) {
                           FinalRespWrapper resWrap = new FinalRespWrapper();
                           resWrap.installment = String.valueOf(msgItem.ATTRIBUTE4);
                           resWrap.SPADate = String.valueOf(msgItem.ATTRIBUTE9);
                           resWrap.dueAmount = String.valueOf(msgItem.ATTRIBUTE12);
                           resWrap.paidAmount = String.valueOf(msgItem.ATTRIBUTE11);
                           resWrap.paidPercent = String.valueOf(msgItem.ATTRIBUTE13);
                           respWrapList.add(resWrap);
                       }
                       if(respWrapList.size() > 0){
                           finalResp = respWrapList;
                           system.debug('finalResp ==> '+finalResp[0].SPADate);
                       }
                   }
            }
        }
    }
    
    public void calculateRebateAmount(){
        //String selId = Apexpages.currentPage().getParameters().get('curRecord');
        //system.debug('wrapRec.SPADate ==> '+wrapRec.SPADate);
        for(FinalRespWrapper wrapRec: finalResp){
            if(Test.isRunningTest()){
                wrapRec.SPADate = '12-DEC-2018';
                wrapRec.paidDateValue = '12/12/2020';
                wrapRec.paidAmount = '12';
                wrapRec.paidAmount = '999999';                
            }
            if(wrapRec.SPADate != '' && wrapRec.SPADate != null && wrapRec.paidDateValue != ''){
                String spa_DateStr = '';
                String[] dateStr = wrapRec.SPADate.split('-');
                spa_DateStr += dateStr[0]+'/';
                switch on dateStr[1] {
                    when 'JAN' {
                        spa_DateStr += '01'+'/';
                    }
                    when 'FEB' {
                        spa_DateStr += '02'+'/';
                    }
                    when 'MAR' {
                        spa_DateStr += '03'+'/';
                    }
                    when 'APR' {
                        spa_DateStr += '04'+'/';
                    }
                    when 'MAY' {
                        spa_DateStr += '05'+'/';
                    }
                    when 'JUN' {
                        spa_DateStr += '06'+'/';
                    }
                    when 'JUL' {
                        spa_DateStr += '07'+'/';
                    }
                    when 'AUG' {
                        spa_DateStr += '08'+'/';
                    }
                    when 'SEP' {
                        spa_DateStr += '09'+'/';
                    }
                    when 'OCT' {
                        spa_DateStr += '10'+'/';
                    }
                    when 'NOV' {
                        spa_DateStr += '11'+'/';
                    }
                    when 'DEC' {
                        spa_DateStr += '12'+'/';
                    }
                    when else {
                        System.debug('INVALID Date');
                    }
                }
                spa_DateStr += dateStr[2];
                Date paid_Date = Date.parse(wrapRec.paidDateValue);
                Date spa_Date = Date.parse(spa_DateStr);
                Integer numberDaysDue = paid_Date.daysBetween(spa_Date);
                wrapRec.noOfPrePaymentDays = String.valueOf(numberDaysDue);
                wrapRec.paidDateValue = String.valueOf(Date.parse(wrapRec.paidDateValue));
                
                //Calculating Rebate Amount
                wrapRec.rebateAmount = String.valueOf((Long.valueOf(wrapRec.paidAmount)*Long.valueOf(wrapRec.noOfPrePaymentDays)*Long.valueOf(wrapRec.rebatePercent))/36500);
            }           
        }
    }
    
    public class FinalRespWrapper {
        public String installment {set;get;}
        public String SPADate {set;get;}
        public String dueAmount {set;get;}
        public String paidAmount {set;get;}
        public String paidDateValue {set;get;}
        public String noOfPrePaymentDays {set;get;}
        public String rebateAmount {set;get;}
        public String rebatePercent {set;get;}
        public String paidPercent {set;get;}
        public FinalRespWrapper() {
            this.installment = '';
            this.SPADate = '';
            this.dueAmount = '';
            this.paidAmount = '';
            this.paidDateValue = '';
            this.rebateAmount = '';
            this.rebatePercent = '12';
            this.noOfPrePaymentDays = '';
            this.paidPercent = '';
        } 
    }
    
    public class WrapResponse{       
        public OutputParameters OutputParameters; 
    }
    
    public class OutputParameters {  
        public String xmlns;
        public String xmlns_xsi;
        public X_RESPONSE_MESSAGE X_RESPONSE_MESSAGE;
        public String X_RETURN_STATUS;
        public String X_RETURN_MESSAGE;
    }
    
    
    public class X_RESPONSE_MESSAGE_ITEM {
        public String ATTRIBUTE1;
        public String ATTRIBUTE2;
        public String ATTRIBUTE3;
        public String ATTRIBUTE4;
        public String ATTRIBUTE5;
        public String ATTRIBUTE6;
        public String ATTRIBUTE7;
        public String ATTRIBUTE8;
        public String ATTRIBUTE9;
        public String ATTRIBUTE10;
        public String ATTRIBUTE11;
        public String ATTRIBUTE12;
        public String ATTRIBUTE13;
        public String ATTRIBUTE14;
        public String ATTRIBUTE15;
        public String ATTRIBUTE16;
        public String ATTRIBUTE17;
        public String ATTRIBUTE18;
        public String ATTRIBUTE19;
        public String ATTRIBUTE20;
        public String ATTRIBUTE21;
        public String ATTRIBUTE22;
        public String ATTRIBUTE23;
        public String ATTRIBUTE24;
        public String ATTRIBUTE25;
        public String ATTRIBUTE26;
        public String ATTRIBUTE27;
        public String ATTRIBUTE28;
        public String ATTRIBUTE29;
        public String ATTRIBUTE30;
        public String ATTRIBUTE31;
        public String ATTRIBUTE32;
        public String ATTRIBUTE33;
        public String ATTRIBUTE34;
        public String ATTRIBUTE35;
        public String ATTRIBUTE36;
        public String ATTRIBUTE37;
        public String ATTRIBUTE38;
        public String ATTRIBUTE39;
        public String ATTRIBUTE40;
    }
    
    public class X_RESPONSE_MESSAGE {
        public List<X_RESPONSE_MESSAGE_ITEM> X_RESPONSE_MESSAGE_ITEM;
    }
}