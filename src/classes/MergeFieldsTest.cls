@istest
public class MergeFieldsTest {
    static testMethod void testMethod1(){
        Account a = new Account();
        a.Name = 'Test Account';  
        a.party_ID__c = '1039032';
        a.Email__c = 'test@test.com';
        a.RecordTypeId  = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Corporate_Agency').getRecordTypeId();
        insert a;
        
        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = a.id;
        sr.RecordTypeId = RecType;
        insert sr;

        Booking__c  bk = new  Booking__c();
        bk.Account__c = a.id;
        bk.Deal_SR__c = sr.id;
        insert bk;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = bk.id;
        bu.Unit_Name__c = 'LSB/10/B1001'; 
        bu.Registration_Status__c = 'Agreement Cancellation Verified';
        bu.Registration_ID__c = '74655';
        insert bu;
        
        Booking_Unit__c bu1 = new Booking_Unit__c();
        bu1.Booking__c = bk.id;
        bu1.Unit_Name__c = 'LSB/10/B1002'; 
        bu1.Registration_Status__c = 'Agreement Executed By Damac';
        bu1.Registration_ID__c = '74656';
        insert bu1;
        
        String templateBody = 'Dear {!Account.Name}, <br/><br/> We are pleased to offer you Early Handover of your unit type{!Booking_Unit__c.Unit_Type__c}, unit no {!Booking_Unit__c.Unit_Name__c}. <br/><br/> ';
        
        map<String, String> mapToMerge = new map<String, String>();
        mapToMerge.put(bu.Id+ '-test', templateBody);
        mapToMerge.put(bu1.Id+ '-test2', templateBody);
        
        map<String, String> mapMergeOutput = MergeFields.parse(mapToMerge);
        
    }

}