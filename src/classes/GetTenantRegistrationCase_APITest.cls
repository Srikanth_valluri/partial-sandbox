@isTest
public class GetTenantRegistrationCase_APITest {
    @isTest
    static void testTenantReg(){
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getTenantRegistrationCase';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetTenantRegistrationCase_API.getTenantRegCaseFromId();
    }
    
    @isTest
    static void testTenantReg2(){
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getTenantRegistrationCase';
        req.addParameter('accessCode','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetTenantRegistrationCase_API.getTenantRegCaseFromId();
    }
    
    @isTest
    static void testTenantRegBlank(){
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getTenantRegistrationCase'; 
        req.addParameter('bookingUnitId','test');
        req.addParameter('accessCode','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetTenantRegistrationCase_API.getTenantRegCaseFromId();
    }
    
    @isTest
    static void testTenantRegBlankBU(){
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getTenantRegistrationCase';  
        req.addParameter('bookingUnitId','');
        req.addParameter('accessCode','test');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetTenantRegistrationCase_API.getTenantRegCaseFromId();
    }
    
     @isTest
    static void testTenantRegNotBlank(){
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.DLP_End_Date__c = Date.today().addDays(2);
        objBU.Registration_ID__c = '3901';
        objBU.Unit_Name__c = 'test/test';
        objBU.Booking__c = objBooking.Id;
        insert objBU;
        
        FM_Case__c objFM = new FM_Case__c();
        objFM.Vehicle_Details__c = 'test123';
        objFM.Status__c = 'Draft Request';
        objFM.Booking_Unit__c = objBU.Id;
        insert objFM;
        
        FM_Additional_Detail__c objFMAdditional = new FM_Additional_Detail__c();
        objFMAdditional.Name__c = 'test1 test2';
        objFMAdditional.Relationship__c = 'test';
        objFMAdditional.Email__c = 'test@xyz.com';
        objFMAdditional.Mobile__c = '12345';
        objFMAdditional.Vehicle_Number__c = 'test1';
        objFMAdditional.Vehicle_Make_Model__c = 'test2';
        //objFMAdditional.member_first_name = 'test';
        //objFMAdditional.member_last_name = 'test';
        objFMAdditional.Date_of_Birth__c = Date.newInstance(2018, 12, 9);
        objFMAdditional.Nationality__c = 'Indian';
        objFMAdditional.Passport_Number__c = 'test';
        objFMAdditional.Identification_Number__c = 'Identify';
        objFMAdditional.Emergency_Contact_Case__c = objFM.Id;
        //objFMAdditional.Vehicle_Case__c = objFM.Id;
        //objFMAdditional.Resident_Case__c = objFM.Id;
        insert objFMAdditional;
        
        FM_Additional_Detail__c objFMAdditional2 = new FM_Additional_Detail__c();
        objFMAdditional2.Vehicle_Number__c = 'test1';
        objFMAdditional2.Vehicle_Make_Model__c = 'test2';
        objFMAdditional2.Vehicle_Case__c = objFM.Id;
        insert objFMAdditional2;
        
        FM_Additional_Detail__c objFMAdditional3 = new FM_Additional_Detail__c();
        objFMAdditional3.Name__c = 'test1 test2';
        objFMAdditional3.Gender__c = 'Male';
        objFMAdditional3.Email__c = 'test@xyz.com';
        objFMAdditional3.Date_of_Birth__c = Date.newInstance(2018, 12, 9);
        objFMAdditional3.Nationality__c = 'Indian';
        objFMAdditional3.Passport_Number__c = 'test';
        objFMAdditional3.Identification_Number__c = 'Identify';
        objFMAdditional3.Resident_Case__c = objFM.Id;
        objFMAdditional3.Mobile__c = '12345678';
        insert objFMAdditional3;
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getTenantRegistrationCase';
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('accessCode', objFM.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        GetTenantRegistrationCase_API.getTenantRegCaseFromId();
        Test.stopTest();
    }
    
     @isTest
    static void testTenantRegVehicle(){
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.DLP_End_Date__c = Date.today().addDays(2);
        objBU.Registration_ID__c = '3901';
        objBU.Unit_Name__c = 'test/test';
        objBU.Booking__c = objBooking.Id;
        insert objBU;
        
        FM_Case__c objFM = new FM_Case__c();
        objFM.Vehicle_Details__c = 'test123';
        objFM.Status__c = 'Draft Request';
        objFM.Booking_Unit__c = objBU.Id;
        insert objFM;
        
        FM_Additional_Detail__c objFMAdditional = new FM_Additional_Detail__c();
        objFMAdditional.Vehicle_Number__c = 'test1';
        objFMAdditional.Vehicle_Make_Model__c = 'test2';
        objFMAdditional.Vehicle_Case__c = objFM.Id;
        insert objFMAdditional;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getTenantRegistrationCase';
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('accessCode', objFM.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        GetTenantRegistrationCase_API.getTenantRegCaseFromId();
        Test.stopTest();
    }
    
     @isTest
    static void testFetchTenantReg(){
        
        FM_Case__c objFM = new FM_Case__c();
        //objFM.Vehicle_Details__c = 'test123';
        insert objFM;
        
        FM_Additional_Detail__c objFMAddition = new FM_Additional_Detail__c();
        objFMAddition.Emergency_Contact_Case__c = objFM.Id;
        objFMAddition.Vehicle_Case__c = objFM.Id;
        objFMAddition.Resident_Case__c = objFM.Id;
        insert objFMAddition;
        
        SR_Attachments__c objSRAttach = new SR_Attachments__c();
        objSRAttach.FM_Case__c = objFM.Id;
        insert objSRAttach;
        
        //GetTenantRegistrationCase_API.fetchTenantRegFmCase(new List<FM_Case__c>());
        //GetTenantRegistrationCase_API.getTenantRegCaseFromId();
        GetTenantRegistrationCase_API.getFMCase(objFM.Id);
        
    }
    
     @isTest
    static void testTenantRegWithStatus(){
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.DLP_End_Date__c = Date.today().addDays(2);
        objBU.Registration_ID__c = '3901';
        objBU.Unit_Name__c = 'test/test';
        objBU.Booking__c = objBooking.Id;
        insert objBU;
        
        FM_Case__c objFM = new FM_Case__c();
        objFM.Vehicle_Details__c = 'test123';
        objFM.Status__c = 'Submitted';
        objFM.Booking_Unit__c = objBU.Id;
        insert objFM;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getTenantRegistrationCase'; 
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('accessCode',objFM.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        GetTenantRegistrationCase_API.getTenantRegCaseFromId();
        Test.stopTest();
    }
    
     @isTest
    static void testTenantVehicleDetails(){
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.DLP_End_Date__c = Date.today().addDays(2);
        objBU.Registration_ID__c = '3901';
        objBU.Unit_Name__c = 'test/test';
        objBU.Booking__c = objBooking.Id;
        insert objBU;
        
        List< FM_Case__c> lstFmCase = new List< FM_Case__c>();
        FM_Case__c objFM = new FM_Case__c();
        objFM.Vehicle_Details__c = 'test123';
        objFM.Status__c = 'Draft Request';
        objFM.Booking_Unit__c = objBU.Id;
        lstFmCase.add(objFM);
        Insert lstFmCase;
        
        //objFM.Vehicle_Details__r = objFM.Id;
        FM_Additional_Detail__c objFMAdditional = new FM_Additional_Detail__c();
        objFMAdditional.Vehicle_Number__c = 'test1';
        objFMAdditional.Vehicle_Make_Model__c = 'Modal';
        objFMAdditional.Vehicle_Sticker_No__c = 'Sticker';
        objFMAdditional.Parking_Slot_Number__c = '21521';
        objFMAdditional.Vehicle_Case__c = objFM.Id;
        insert objFMAdditional;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getTenantRegistrationCase';
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('accessCode', objFM.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        GetTenantRegistrationCase_API.fetchTenantRegFmCase(lstFmCase);
        Test.stopTest();
    }
    
}