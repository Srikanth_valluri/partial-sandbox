/*--------------------------------------------------------------------------------------------------
Description: Controller for PromotorInquiryPage

====================================================================================================
 Version | Date(DD-MM-YYYY) | Last Modified By | Comments                                           
----------------------------------------------------------------------------------------------------
 1.0     | 18-02-2018       | Lochana Rajput   | 1. Initial draft                                   
----------------------------------------------------------------------------------------------------
 2.0     | 19-02-2018       | Lochana Rajput   | 1. Added methods to save Inquiry details           
----------------------------------------------------------------------------------------------------
 3.0     | 26-02-2018       | Lochana Rajput   | 1. Added logic to assign owner based Meeting Type
----------------------------------------------------------------------------------------------------
 4.0     | 05-03-2018       | Craig Lobo       | 1. Commented the Sharing Logic.
----------------------------------------------------------------------------------------------------
 5.0     | 15-03-2018       | Lochana Rajput   | 1. Added logic to retain selected stand
----------------------------------------------------------------------------------------------------
 6.0     | 20-03-2018       | Lochana Rajput   | 1. Added logic to assign Inquiry creation date if meeting
                                                    type is direct tour
   =================================================================================================
*/

public without sharing class PromotorInquiryController {

    public Inquiry__c objInquiry                    {get; set;}
    public String userId                            {get; set;}
    public String msgType                           {get; set;}
    public String requiredCls                       {get; set;}
    public String msg                               {get; set;}
    public String selectedStand                     {get; set;}
    public Boolean showMessage                      {get; set;}
    public Boolean showInqPanel                      {get; set;}
    public List<SelectOption> stands                {get; set;}
    public InquiryShareWithManager inqShareObj;
    public string selectedStandStr{get;set;}
    map<String,String> mapStandVal_Label;

    // Added by Monali 03/04/18
    public String selectedCountryCode{get;set;}
    public String selectedAlternateCode{get;set;}
    public String selectedNationality{get;set;}
    public String selectedCountry{get;set;}
    public String selectedCity{get;set;}
    public String selectedLanguage{get;set;}

    public List<String> lstCountryCode{get;set;}
    public List<String> lstAlternateCode{get;set;}
    public List<String> lstNationality{get;set;}
    public List<String> lstCountry{get;set;}
    public List<String> lstCity{get;set;}
    public List<String> lstLanguage{get;set;}
    public static Map<String, List<String>> controllingInfo;

    @RemoteAction
    public static List<Inquiry__c> queryInquiries() {
         return [SELECT Id, First_Name__c, Last_Name__c, Mobile_CountryCode__c, Mobile_Phone_Encrypt__c, Email__c,Country__c from Inquiry__c ];
    }
    public PromotorInquiryController() {
        showInqPanel = false;
        mapStandVal_Label = new map<String,String>();
        showMessage = false;
        objInquiry = new Inquiry__c();
        stands = new List<SelectOption>();
        System.debug('==getUserId=='+UserInfo.getUserId());
        //stands.add(new SelectOption('None', 'None'));
        for(Campaign__c campaign : [SELECT Name
                                         , Id
                                         , Campaign_Name__c
                                      FROM Campaign__c
                                     WHERE Active__c = true
                                       AND Campaign_Type_New__c = 'Stands'
                                       AND Campaign_Name__c != NULL
        ] ) {
            stands.add(new SelectOption(campaign.Id, campaign.Campaign_Name__c));
            mapStandVal_Label.put(campaign.Id, campaign.Campaign_Name__c);
        }
        requiredCls = stands.size() > 1 ? 'requiredField' : '';
        selectedCountryCode = '';
        selectedAlternateCode = '';
        selectedNationality = '';
        selectedCountry = '';
        selectedCity = '';
        selectedLanguage = '';
        controllingInfo = new Map<String, List<String>>();

        lstCountryCode = new List<String>();
        lstAlternateCode = new List<String>();
        lstNationality = new List<String>();
        lstCountry = new List<String>();
        lstCity = new List<String>();
        lstLanguage = new List<String>();
        
        buildPicklist(); // Added by Monali 05/04/2018
    }

    // Added by Monali 05/04/2018 START
    public void buildPicklist() {

        lstCountryCode = getPicklistValues('Inquiry__c', 'Mobile_CountryCode__c');
        lstAlternateCode = getPicklistValues('Inquiry__c', 'Mobile_Country_Code_2__c');
        lstNationality = getPicklistValues('Inquiry__c', 'Nationality__c');
        lstCountry = getPicklistValues('Inquiry__c', 'Country__c');
        //lstCity = getPicklistValues('Inquiry__c', 'City_new__c');
        lstLanguage = getPicklistValues('Inquiry__c', 'Preferred_Language__c');

    }
    // Added by Monali 05/04/2018 END

    public pageReference cancel() {
        pageReference pg = new pageReference('/home/home.jsp');
        return pg;
    }

     // Added by Monali 03/04/18
    public static List<String> getPicklistValues(String ObjectApi_name,String Field_name){
        List<String> lstPickvals=new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
            lstPickvals.add(a.getValue());//add the value  to our final list
        }
        System.debug('======lstPickvals=====' + lstPickvals);
        return lstPickvals;
    }

    public static void getFieldDependencies(String objectName, String controllingField, String dependentField)
    {
        controllingInfo = new Map<String, List<String>>();

        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);

        Schema.DescribeSObjectResult describeResult = objType.getDescribe();
        Schema.DescribeFieldResult controllingFieldInfo = describeResult.fields.getMap().get(controllingField).getDescribe();
        Schema.DescribeFieldResult dependentFieldInfo = describeResult.fields.getMap().get(dependentField).getDescribe();

        List<Schema.PicklistEntry> controllingValues = controllingFieldInfo.getPicklistValues();
        List<Schema.PicklistEntry> dependentValues = dependentFieldInfo.getPicklistValues();

        for(Schema.PicklistEntry currControllingValue : controllingValues)
        {
            //System.debug('ControllingField: Label:' + currControllingValue.getLabel());
            controllingInfo.put(currControllingValue.getValue(), new List<String>());
        }

        for(Schema.PicklistEntry currDependentValue : dependentValues)
        {
            String jsonString = JSON.serialize(currDependentValue);

            MyPickListInfo info = (MyPickListInfo) JSON.deserialize(jsonString, MyPickListInfo.class);

            String hexString = EncodingUtil.convertToHex(EncodingUtil.base64Decode(info.validFor)).toUpperCase();

            //System.debug('DependentField: Label:' + currDependentValue.getLabel() + ' ValidForInHex:' + hexString + ' JsonString:' + jsonString);

            Integer baseCount = 0;

            for(Integer curr : hexString.getChars())
            {
                Integer val = 0;

                if(curr >= 65)
                {
                    val = curr - 65 + 10;
                }
                else
                {
                    val = curr - 48;
                }

                if((val & 8) == 8)
                {
                    //System.debug('Dependent Field: ' + currDependentValue.getValue() + ' Partof ControllingField:' + controllingValues[baseCount + 0].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 0].getValue()).add(currDependentValue.getValue());
                }
                if((val & 4) == 4)
                {
                    //System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 1].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 1].getValue()).add(currDependentValue.getValue());
                }
                if((val & 2) == 2)
                {
                    //System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 2].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 2].getValue()).add(currDependentValue.getValue());
                }
                if((val & 1) == 1)
                {
                    //System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 3].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 3].getValue()).add(currDependentValue.getValue());
                }

                baseCount += 4;
            }
        }

        System.debug('======ControllingInfo==in method==== ' + controllingInfo);

        //return controllingInfo;
    }

    public void updateCityList() {
        System.debug('======selectedCountry=====' + selectedCountry);
        getFieldDependencies('Inquiry__c','Country__c','City_new__c');
        System.debug('==updateCityList====controllingInfo=====' + controllingInfo);
        if(controllingInfo.containsKey(selectedCountry.trim())){
            System.debug('=contains ===' + selectedCountry);
            lstCity = controllingInfo.get(selectedCountry.trim());
        } else {
            System.debug('=Not ====contains ===' + selectedCountry);
        }
        System.debug('=updateCityList====lstCity===' + lstCity);
    }


    public pageReference selectStand() {
        if(mapStandVal_Label.containsKey(selectedStand)) {
            showInqPanel = true;
            selectedStandStr = mapStandVal_Label.get(selectedStand);
        }
        return null;
    }
    public pageReference submitInquiry() {
        showMessage = false;
        System.debug('==objInquiry==' + objInquiry);
        System.debug('==PC userId==' + userId);

        // Get the list of Promoter Profile Names
        List<String> profileNames = Label.PromoterCommunityProfile.split(',');
        User currentUser = [ SELECT Id
                                  , Profile.Name
                                  , Name
                                  , ContactId
                                  , Manager.Name
                               FROM User
                              WHERE Id = :UserInfo.getUserId()
        ];

        objInquiry.Created_By_Profile__c = currentUser.Profile.Name;

        // Populate Promoter Name if User Profiel is mentioned in the Promoter Label
        if(profileNames != null 
            && !profileNames.isEmpty()
            && profileNames.contains(currentUser.Profile.Name)
        ) {
            objInquiry.Promoter_Name__c = currentUser.Name;
            objInquiry.Stand_Manager__c = currentUser.Manager.Name;
        }

        // Populate Campaign
        /*if(selectedStand != 'None' && String.isNotBlank(selectedStand)) {
            objInquiry.Campaign__c = ;
        }*/

        // List<StandName__c> lstStand = StandName__c.getall().values();
        List<User> lstuser = [Select Id,Stand_Id__c FROM User WHERE Id=:UserInfo.getUserId() limit 1];
        if(lstuser.size() >0) {
            if(lstuser[0].Stand_Id__c != null && lstuser[0].Stand_Id__c != '') {
                    objInquiry.Campaign__c = lstuser[0].Stand_Id__c;
            }
        }else{
            
        }

        // Assign the Inquiry to the Current User and Inquiry RT as Pre-Inquiry 
        if(objInquiry.Meeting_Type__c == 'Scheduled Tour' 
            || String.isBlank(objInquiry.Meeting_Type__c)
        ) {
            objInquiry.RecordTypeId = 
                Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
            objInquiry.OwnerId = UserInfo.getUserId();
        }

        if(objInquiry.Meeting_Type__c == 'Direct Tour' ) {
                objInquiry.Tour_Date_Time__c = System.now();
        }

        if(String.isNotBlank(userId)) {

            // Assign Owner to PC
            objInquiry.Assigned_PC__c = userId;
            if(objInquiry.Meeting_Type__c == 'Direct Tour' 
                || objInquiry.Meeting_Type__c == 'Meeting On Stand'
            ) {
                objInquiry.OwnerId = userId;
                objInquiry.recordTYpeId = 
                    Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
            }
        }

          // Added by Monali 03/04/18 =======START=========

        objInquiry.Mobile_CountryCode__c = selectedCountryCode;
        objInquiry.Mobile_Country_Code_2__c = selectedAlternateCode;
        objInquiry.Nationality__c = selectedNationality;
        objInquiry.Country__c = selectedCountry;
        objInquiry.City_new__c = selectedCity;
        objInquiry.Preferred_Language__c = selectedLanguage;


        // Added by Monali 03/04/18 =======END=========

        System.debug('==objInquiry==: ' + objInquiry.Tour_Date_Time__c);
        showMessage = true;
        if(String.isNotBlank(String.valueOf(objInquiry.Tour_Date_Time__c))
            && (objInquiry.Tour_Date_Time__c > Date.today().addDays(3)
            || objInquiry.Tour_Date_Time__c < Date.today())
            && objInquiry.Meeting_Type__c == 'Scheduled Tour') {
            msgType = 'error';
            msg='Please select Tour Date/Time within 3 days from today';
            return null;
        }
        try {
            // DML Inquiry
            insert objInquiry;
           
            List<Inquiry__c> inqRecLst = [ SELECT Id
                                                , Name
                                                , OwnerId
                                                , CreatedById 
                                            FROM Inquiry__c 
                                            WHERE Id = :objInquiry.Id
            ];

            // v5.0 05-03-2018 Craig - Commented the Sharing Logic.
            // Create a Sharing record for the Inquiry Creator (only for Internal Users) 
            /*if(profileNames.contains(currentUser.Profile.Name)
                && profileNames != null 
                && inqRecLst != null
                && !profileNames.isEmpty()
                && !inqRecLst.isEmpty()
            ) {
                // Pass the Inquiry in a list to be shared with its Managers 
                inqShareObj = new InquiryShareWithManager();
                inqShareObj.InquiryShare(inqRecLst);
                //Inquiry__share objInquiryShare = new Inquiry__share();
                //objInquiryShare.ParentId = objInquiry.Id;
                //objInquiryShare.UserOrGroupId = UserInfo.getUserId();
                //objInquiryShare.RowCause = Schema.Inquiry__Share.RowCause.Promoter__c;
                //objInquiryShare.AccessLevel = 'Read';
                // DML Inquiry Share
                //insert objInquiryShare; 
            } */
            // v5.0 05-03-2018 Craig

            msg = ' Inquiry ' + inqRecLst[0].Name + ' has been created successfully ';
            objInquiry = new Inquiry__c();
            //selectedStand = '';
            userId = null;
            msgType = 'success';
            ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.Info,
                'Inquiry has been created successfully'
            ));
        } catch(System.DmlException excp) {
            msgType = 'error';
            System.debug('===='+excp.getDmlFieldNames(0));
            System.debug('==***=='+excp);
            msg = excp.getMessage() 
                + ': '
                + excp.getLineNumber();
            ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.Error, 
                excp.getMessage() + ': ' + excp.getLineNumber()
            ));
        }

        selectedCountryCode = '';
        selectedAlternateCode = '';
        selectedNationality = '';
        selectedCountry = '';
        selectedCity = '';
        selectedLanguage = '';
        controllingInfo = new Map<String, List<String>>();
     
        //buildPicklist(); // Added by Monali 05/04/2018

        return null;
    }

    public class MyPickListInfo
   {
        public String validFor;
   }
}