public with sharing class PDCReceiptGenerator {
    
    public class generateVariables {
        @InvocableVariable(required=true)
        public String ObjName;
        
        @InvocableVariable(required=true)
        public Id ObjId;
    }
    
    @InvocableMethod
    public static void callingGenerateReceipt(List<generateVariables> dataList){
        Set<Id> idSet = new Set<Id>();
        Map<String,Set<Id>> objIdMap = new Map<String,Set<Id>>();
        if(dataList != null && !dataList.isEmpty()){
            for(generateVariables genVar :dataList){
                if(!objIdMap.containsKey(genVar.objName)){
                    idSet.add(genVar.ObjId);
                    objIdMap.put(genVar.objName, idSet);
                }else{
                    objIdMap.get(genVar.objName).add(genVar.ObjId);
                }
            }
            
            if(objIdMap.containsKey('Case')){
                Map<Id, Case_Post_Dated_Cheque__c> mapCasePDC = 
                    new Map<Id, Case_Post_Dated_Cheque__c>([SELECT Id
                                                            	 , Case__c
                                                              FROM Case_Post_Dated_Cheque__c
                                                             WHERE Case__c IN : objIdMap.get('Case')]);
                if(!mapCasePDC.isEmpty()){
                   futureGeneratePDC(new List<Id>(mapCasePDC.keySet()));
                }
            }else if(objIdMap.containsKey('Calling List')){
                Map<Id, Case_Post_Dated_Cheque__c> mapCasePDC = 
                    new Map<Id, Case_Post_Dated_Cheque__c>([SELECT Id
                                                            	 , Case__c
                                                              FROM Case_Post_Dated_Cheque__c
                                                             WHERE Calling_List__c IN : objIdMap.get('Calling List')]);
                if(!mapCasePDC.isEmpty()){
                   futureGeneratePDC(new List<Id>(mapCasePDC.keySet()));
                }
            }
        }
    }
    
    @future(callout=true)
    public static void futureGeneratePDC( List<Id> CasePdcIds ) {
        generatePDCReceipt(CasePdcIds);
    }
    
  
    public static void generatePDCReceipt( List<Id> CasePdcIds  ){
        
        Credentials_Details__c creds = Credentials_Details__c.getInstance( 'PDCReceiptGenerator' );
        Boolean isValidCreds = (creds != null && String.isNotBlank(creds.Endpoint__c) && String.isNotBlank(creds.User_Name__c) && String.isNotBlank(creds.Password__c)) ? True : False;
        
        String body = '';        
        String rId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Early_Handover').getRecordTypeId();    
        
        List<Case_Post_Dated_Cheque__c> lstCasePDC = [  SELECT id
                                                             , Case__c
                                                             , Case__r.Party_ID__c
                                                             , Case__r.RecordTypeId
                                                             , Reason__c
                                                             , Receipt_Classification__c
                                                             , booking_unit__r.Registration_ID__c
                                                             , Mode_of_Payment__c
                                                             , Account__c
                                                             , Calling_List__c
                                                             , Account__r.Party_ID__c
                                                             , Receipt_Date__c
                                                             , CurrencyIsoCode
                                                             , Case__r.booking_unit__r.Registration_ID__c
                                                             , Booking_Unit__c
                                                             , New_maturity_date__c
                                                             , IPMS_Cash_Receipt_Id__c
                                                             , Receipt_No__c
                                                             , New_Drawer__c
                                                             , New_Bank__c
                                                             , New_Amount__c
                                                             , Owner.Email 
                                                             , Account__r.Name
                                                             , Case__r.CaseNumber
                                                             , Calling_List__r.Name
                                                          FROM Case_Post_Dated_Cheque__c 
                                                         WHERE id IN : CasePdcIds ];
        
        if(isValidCreds && !lstCasePDC.isEmpty()) {
            String requestBody = '';
            List<Case_Post_Dated_Cheque__c> lstCasePDCToUpdate = new List<Case_Post_Dated_Cheque__c>();
            List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
            system.debug('lstCasePDC size==='+lstCasePDC.size());
            for(Case_Post_Dated_Cheque__c objPDC : lstCasePDC ) {
                PDC_Receipt objPdcr = new PDC_Receipt();
                objPdcr.InputParameters = new InputParameters();
                
                objPdcr.InputParameters.P_RECEIPT_NUMBER = String.isNOtBlank(objPDC.Receipt_No__c) ? objPDC.Receipt_No__c : '';
                objPdcr.InputParameters.P_RECEIPT_DATE = String.isNotBlank(String.valueOf(objPDC.Receipt_Date__c)) ? formatDate(String.valueOf(objPDC.Receipt_Date__c)) : '';               
                objPdcr.InputParameters.P_MATURITY_DATE = String.isNotBlank(String.valueOf(objPDC.New_Maturity_date__c)) ? formatDate(String.valueOf(objPDC.New_Maturity_date__c)) : '';
                objPdcr.InputParameters.P_AMOUNT = String.isNOtBlank(String.valueOf(objPDC.New_Amount__c)) ? String.valueOf(objPDC.New_Amount__c) : '';
                objPdcr.InputParameters.P_CURRENCY_CODE = String.isNOtBlank(objPDC.CurrencyIsoCode) ? objPDC.CurrencyIsoCode : '';
                objPdcr.InputParameters.P_COMMENTS = String.isNOtBlank(objPDC.Reason__c) ? objPDC.Reason__c : '';
                objPdcr.InputParameters.P_RECEIPT_METHOD_ID = '1481';//String.isNOtBlank(objPDC.Booking_unit__r.Registration_ID__c) ? objPDC.Booking_unit__r.Registration_ID__c : '';
                objPdcr.InputParameters.P_CASH_RECEIPT_ID = String.isNOtBlank(objPDC.IPMS_Cash_Receipt_Id__c) ? objPDC.IPMS_Cash_Receipt_Id__c : '';
                objPdcr.InputParameters.P_ORG_ID = '81';
                objPdcr.InputParameters.P_RECEIPT_CLASSIFICATION = String.isNOtBlank(objPDC.Receipt_Classification__c) ? objPDC.Receipt_Classification__c : '';
                objPdcr.InputParameters.P_PAYMENT_MODE = String.isNOtBlank(objPDC.Mode_of_Payment__c) ? objPDC.Mode_of_Payment__c : '';
                objPdcr.InputParameters.P_APPROVER_NAME = String.isNOtBlank(objPDC.Account__r.Name) ? objPDC.Account__r.Name : '';
                objPdcr.InputParameters.P_SR_NUMBER = (objPDC.Case__c!=null && String.isNOtBlank(objPDC.Case__r.CaseNumber)) ? objPDC.Case__r.CaseNumber : 
                									  (objPDC.Calling_List__c!=null && String.isNOtBlank(objPDC.Calling_List__r.Name))?objPDC.Calling_List__r.Name :'';
              
               //Need field mapping of P_HOLD_REASON from Aditya
                objPdcr.InputParameters.P_HOLD_REASON = 'test';
                objPdcr.InputParameters.P_PARAMETER1 = '';
                objPdcr.InputParameters.P_PARAMETER2 = '';
                objPdcr.InputParameters.P_PARAMETER3 = '';
                
                if(objPDC.Case__c != Null && objPDC.Case__r.RecordTypeId == rId) {
                    objPdcr.InputParameters.P_PARTY_ID = String.isNOtBlank(objPDC.Case__r.Party_ID__c) ? objPDC.Case__r.Party_ID__c : '';
                    objPdcr.InputParameters.P_REG_ID = String.isNOtBlank(objPDC.Case__r.Booking_Unit__r.Registration_ID__c) ? objPDC.Case__r.Booking_Unit__r.Registration_ID__c : '';
                } else {
                    objPdcr.InputParameters.P_PARTY_ID = String.isNOtBlank(objPDC.Account__r.Party_ID__c) ? objPDC.Account__r.Party_ID__c : '';
                    objPdcr.InputParameters.P_REG_ID = String.isNOtBlank(objPDC.Booking_Unit__r.Registration_ID__c) ? objPDC.Booking_Unit__r.Registration_ID__c : '';
                }
                String JSONData = JSON.serialize(objPdcr);
                body = '{"PDC_Receipt": '+JSONData+'}';
                System.debug('JSONData: '+JSONData);
                System.debug('body: '+body);
                if(String.isNotBlank(body)) {
                    HTTPResponse resp = returnResponseFromService(creds.Endpoint__c, 'POST', creds.User_Name__c, creds.Password__c, body); //h.send(request);
                    
                    JSON2ApexWrapper responseObj = ( JSON2ApexWrapper )System.JSON.deserialize(resp.getBody(), JSON2ApexWrapper.class);
                    System.debug('responseObj: '+responseObj);
                    if(resp.getStatusCode() == 200 && responseObj.OutputParameters.X_RETURN_STATUS == 'S' && responseObj.OutputParameters.X_RECEIPT_ID != Null){
                        lstCasePDCToUpdate.add(new Case_Post_Dated_Cheque__c(Id = objPDC.Id, IPMS_Cash_Receipt_Id__c = responseObj.OutputParameters.X_RECEIPT_ID,Response_Status__c='Success'));
                    }
                                       
                    
                    if( String.isNotBlank(responseObj.OutputParameters.X_RETURN_STATUS) && responseObj.OutputParameters.X_RETURN_STATUS == 'E' ) {
                        requestBody = requestBody +  objPDC.Receipt_No__c + ' - ' + responseObj.OutputParameters.X_ERROR_MSG + '   -  \n\n' ;
                        lstCasePDCToUpdate.add(new Case_Post_Dated_Cheque__c(Id = objPDC.Id,Response_Status__c='Failure'));
                         sendEmailToOwner( lstCasePDC[0].Owner.Email
                            , 'noreply@damacgroup.com'
                            , 'There are one or more receipt creation failure -  please see below details : \n\n'+requestBody);
                    }
                    /*lstErrorLog.add(GenericUtility.createErrorLog(resp.getBody(), objPDC.Account__c,
                                                                  objPDC.Booking_Unit__c, objPDC.Calling_List__c, 
                                                                  objPDC.Case__c));*/
                    
                    Error_Log__c objE = GenericUtility.createErrorLog(resp.getBody(), objPDC.Account__c, 
                                                                      objPDC.Booking_Unit__c, objPDC.Calling_List__c, 
                                                                      objPDC.Case__c);
                    objE.Case_Post_Dated_Cheque__c = objPDC.Id;
                    objE.Service_Request_Body__c = body;
                    objE.Service_Response_Body__c = resp.getBody();
                    lstErrorLog.add(objE);
                    System.debug('Response+++'+resp); // GenericUtility.createErrorLog(); create error log in both condition failure and success
                    System.debug('Response Body+++'+resp.getBody());
                    system.debug('lstErrorLog size==='+lstErrorLog.size());
                }
            }
            
            if(!lstCasePDCToUpdate.isEmpty()){
                Update lstCasePDCToUpdate;
                
            }
            system.debug('lstErrorLog size==='+lstErrorLog.size());
            if(!lstErrorLog.isEmpty()){
                Insert lstErrorLog;
            }
        }     
    }
    
    
    public static void sendEmailToOwner( String strToAddress
                                       , String strFromAddress
                                       , String requestBody){
                                         
        String strEmailBody = requestBody;
        String strSubject = 'PDC Creation Failure';
                                                      
        
        SendGridEmailService.SendGridResponse emailResponse = SendGridEmailService.sendEmailService(  strToAddress
                                                                               , ''
                                                                               , ''
                                                                               , ''
                                                                               , ''
                                                                               , ''
                                                                               , strSubject
                                                                               , ''
                                                                               , strFromAddress
                                                                               , ''
                                                                               , ''
                                                                               , ''
                                                                               , 'Text'
                                                                               , strEmailBody
                                                                               , ''
                                                                               , new List<Attachment>());
        
     
    }
    
    //Method used to convert date format from yyyy-mm-dd to dd-mmm-yyyy to SF format.
    public static  String formatDate( String strDate ){
        Map<String,String> myMap = new Map<String,String>{
        '01' => 'JAN',
        '02' => 'FEB', 
        '03' => 'MAR',
        '04' => 'APR',
        '05' => 'MAY', 
        '06' => 'JUN',
        '07' => 'JUL',
        '08' => 'AUG', 
        '09' => 'SEP',
        '10' => 'OCT',
        '11'=> 'NOV', 
        '12' => 'DEC'
        };
        
        List<String > lstStr = strDate.Split('-');
        return lstStr[2]+'-'+myMap.get(lstStr[1])+'-'+lstStr[0];
        
    }
    
    
    public static HTTPResponse returnResponseFromService(String endpoint,String method,String userName,String password, String body) {
        String authorization  = 'Basic';
        
        HTTP h = new HTTP();
        HTTPRequest request = new HTTPRequest();
        request.setEndpoint(endpoint);
        request.setMethod(method);
        request.setHeader('Accept', 'application/json');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Accept-Language', 'en-US');
        Blob headerValue = Blob.valueOf(userName + ':' +password);
        String authorizationHeader = authorization + EncodingUtil.base64Encode(headerValue);
        request.setHeader('Authorization', authorizationHeader);
        request.setBody(body);
        request.setTimeout(120000);
        System.debug('request: '+request);
        HTTPResponse resp = h.send(request);
        return resp;
    }
    

    public PDC_Receipt PDC_Receipt;

    public class PDC_Receipt {
        public InputParameters InputParameters;
    }

    public class InputParameters {
        public String P_PARTY_ID;
        public String P_RECEIPT_NUMBER;
        public String P_RECEIPT_DATE;
        public String P_MATURITY_DATE;
        public String P_AMOUNT;
        public String P_CURRENCY_CODE;
        public String P_COMMENTS;
        public String P_RECEIPT_METHOD_ID;
        public String P_CASH_RECEIPT_ID;
        public String P_ORG_ID;
        public String P_REG_ID;
        public String P_RECEIPT_CLASSIFICATION;
        public String P_PAYMENT_MODE;
        public String P_PARAMETER1;
        public String P_PARAMETER2;
        public String P_PARAMETER3;
        public String P_SR_NUMBER;
        public String P_APPROVER_NAME;
        public String P_HOLD_REASON;
       
    }
    
    public class JSON2ApexWrapper {
        public OutputParameters OutputParameters;
    }
    
    public class OutputParameters {
        public String xmlns;
        public String xmlnsxsi;
        public String X_RECEIPT_ID;
        public String X_RECEIPT_NO;
        public String X_RETURN_STATUS;
        public String X_ERROR_MSG;
    }

    
}