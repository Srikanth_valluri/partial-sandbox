@isTest 
public class UpdateBookingUnitToIPMSTest {

    @testSetup
    static void createTestdata() {

        Id RecordTypeIdContact = 
        Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();

        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.recordtypeid=RecordTypeIdContact;
        SR.Eligible_to_Sell_in_Dubai__c = true;
        SR.Agency_Type__c = 'Individual';
        SR.ID_Type__c = 'Passport';
        // SR.Agency__c = a.id;
        SR.Agency_Email_2__c = 'test2@gmail.com';
        SR.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        SR.Country_of_Sale__c = 'UAE;KSA;Lebanon';
        insert SR;

        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = sr.Id;
        insert objBooking;
        List<Booking_Unit__c> lstBUs = new List<Booking_Unit__c>();
        for(Integer i=0; i<5; i++) {
        lstBUs.add(new Booking_Unit__c(Booking__c = objBooking.ID));
        }
        insert lstBUs;

        Id agenTeamRT = 
            Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();

        Inquiry__c inquiry = InitialiseTestData.getInquiryDetails('Agent Team',455);
        inquiry.Inquiry_Source__c = 'Prospecting';
        insert inquiry;

        Buyer__c buyerObj = new Buyer__c();
        buyerObj.Inquiry__c = inquiry.Id;
        buyerObj.Booking__c = objBooking.ID;
        insert buyerObj;

        Buyer__c buyerObj1 = new Buyer__c();
        buyerObj1.Inquiry__c = inquiry.Id;
        buyerObj1.Booking__c = objBooking.ID;
        insert buyerObj1;

        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = objBooking.ID;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.DP_Overdue__c = true;
        bu.Registration_Status_Code__c = 'abc';
        bu.tBits_Ready__c = true;
        bu.Receipt_ID__c = '67878';
        insert bu;

        Inquiry__c newInquiry = InitialiseTestData.getInquiryDetails('Agent Team',855);
        newInquiry.Inquiry_Source__c = 'Customer Referral';
        newInquiry.Telesales_Executive__c = UserInfo.getUserId();
        newInquiry.Promoter_Name__c = 'Test Promoter';
        insert newInquiry;
    }


  // Select Inquiry and tag on buyer
    static testMethod void testMethod1() {
        List<Buyer__c> buyer = [Select Id, Inquiry__c From Buyer__c Limit 2];
        Inquiry__c newInquiry = [Select Id 
                                    From Inquiry__c 
                                    Where Inquiry_Source__c = 'Customer Referral' Limit 1];

        list<Lead_Admin_Console__mdt> lstRules = [ SELECT Inquiry_Source__c, Action__c, Action_Code__c,
                                                    Buyer_Details_Same__c, Campaign_Name__c,
                                                    PC_Part_of_Active_RoadShow__c,
                                                    Related_Campaign_on_Booking_Unit__c,
                                                    Related_Inquiry_Owner_Same__c,
                                                    Type__c
                                                FROM Lead_Admin_Console__mdt
                                                WHERE Action_Code__c = 'SEL_INQ'
                                      ];
        System.debug('========lstRules : ' +lstRules.size());

        List<Booking_Unit__c> buList = [SELECT Id FROM Booking_Unit__c 
                                        WHERE Primary_Buyer_s_Name__c = 'testNSI' LIMIT 1];

        Lead_Management__c leadObj = new Lead_Management__c();
        leadObj.Buyer__c = buyer[0].Id;
        leadObj.New_Inquiry__c = newInquiry.Id;
        leadObj.Old_Inquiry__c = buyer[0].Inquiry__c;
        leadObj.Status__c = 'Submitted';
        leadObj.Lead_Admin_Console_Id__c = lstRules[0].Id;
        leadObj.Booking_Unit__c = buList[0].Id;
        leadObj.TSA_Name__c = true;
        leadObj.TSA_Manager_Name__c = true;
        leadObj.Promoter_Name__c = true;
        leadObj.HOD_Name__c = true;
        leadObj.Replace_inquiry_on_buyer__c = 'YES';
        leadObj.Reason_for_HOD_Update__c = 'Name Correction';
        insert leadObj;

        List<Id> pUnitIdsList = new List<Id>(); 
        List<Id> pLeadManagementIdList = new List<Id>();
        pUnitIdsList.add(buList[0].Id);
        pLeadManagementIdList.add(leadObj.Id);

        Test.startTest();
        UpdateBookingUnitToIPMS.updateBookingUnitsInIPMS(pLeadManagementIdList);
        Test.stopTest();
    }
}