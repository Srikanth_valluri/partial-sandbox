/******************************************************************************
* Description - Test class developed for CallEarlyHandoverMQService1
*
* Version            Date            Author                    Description
* 1.0                17/12/17        Naresh Kaneriya (Accely)   Initial Draft
********************************************************************************/

@isTest
public class CallEarlyHandoverMQService1Test{


 @isTest static void getTest(){
     
      Test.startTest();
      List<String> lstregId  = new List<String> {'1234','2341'};  
      SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.UPDATE_EARLY_HO_FLAGResponse_element>();
      EarlyHandoverMQService1.UPDATE_EARLY_HO_FLAGResponse_element response1 = new EarlyHandoverMQService1.UPDATE_EARLY_HO_FLAGResponse_element();
      response1.return_x ='{'+
		'			"lstReg" : ['+
		'			{'+
		'				"ATTRIBUTE1":"HandoverStat",'+
		'            "ATTRIBUTE10":"Test",'+
		'            "ATTRIBUTE11":"Test",'+
		'            "ATTRIBUTE12":"Test",'+
		'            "ATTRIBUTE13":"Test",'+
		'            "ATTRIBUTE14":"Test",'+
		'            "ATTRIBUTE15":"Test",'+
		'            "ATTRIBUTE16":"Test",'+
		'            "ATTRIBUTE17":"Test",'+
		'            "ATTRIBUTE18":"Test",'+
		'            "ATTRIBUTE19":"Test",'+
		'            "ATTRIBUTE2":"EHOStat",'+
		'            "ATTRIBUTE20":"Test",'+
		'            "ATTRIBUTE21":"Test",'+
		'            "ATTRIBUTE22":"Test",'+
		'            "ATTRIBUTE23":"Test",'+
		'            "ATTRIBUTE24":"Test",'+
		'            "ATTRIBUTE25":"Test",'+
		'            "ATTRIBUTE26":"Test",'+
		'            "ATTRIBUTE27":"Test",'+
		'            "ATTRIBUTE28":"Test",'+
		'            "ATTRIBUTE29":"Test",'+
		'            "ATTRIBUTE3":"Test",'+
		'            "ATTRIBUTE30":"Test",'+
		'            "ATTRIBUTE31":"Test",'+
		'            "ATTRIBUTE32":"Test",'+
		'            "ATTRIBUTE33":"Test",'+
		'            "ATTRIBUTE34":"Test",'+
		'            "ATTRIBUTE35":"Test",'+
		'            "ATTRIBUTE36":"Test",'+
		'            "ATTRIBUTE37":"Test",'+
		'            "ATTRIBUTE38":"Test",'+
		'            "ATTRIBUTE39":"Test",'+
		'            "ATTRIBUTE4":"Test",'+
		'            "ATTRIBUTE41":"Test",'+
		'            "ATTRIBUTE42":"Test",'+
		'            "ATTRIBUTE43":"Test",'+
		'            "ATTRIBUTE44":"Test",'+
		'            "ATTRIBUTE45":"Test",'+
		'            "ATTRIBUTE46":"Test",'+
		'            "ATTRIBUTE47":"Test",'+
		'            "ATTRIBUTE48":"Test",'+
		'            "ATTRIBUTE49":"Test",'+
		'            "ATTRIBUTE5":"Test",'+
		'            "ATTRIBUTE50":"Test",'+
		'            "ATTRIBUTE6":"Test",'+
		'            "ATTRIBUTE7":"Test",'+
		'            "ATTRIBUTE8":"Test",'+
		'            "ATTRIBUTE9":"Test",'+
		'            "PARAM_ID":"reg1"'+
		'			}'+
		'			 ],'+
		'			 "P_REQUEST_NUMBER" : "1234",'+
		'			 "P_REQUEST_NAME" : "Test",'+
		'			 "P_SOURCE_SYSTEM" : "SFDC"'+
		'		}';
      SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
      Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
      CallEarlyHandoverMQService1.CallEarlyHandoverMQService1Name('HandoverStat','EHOStat',lstregId);
      Test.stopTest();
    
     CallEarlyHandoverMQService1.EarlyHandoverMQService1Response  objInner =  new CallEarlyHandoverMQService1.EarlyHandoverMQService1Response();
     list<EarlyHandoverMQService2.APPSXXDC_PROCESS_SERX1794747X1X5> lstReg = new  list<EarlyHandoverMQService2.APPSXXDC_PROCESS_SERX1794747X1X5>();
     objInner.P_REQUEST_NUMBER  = '12365';
     objInner.P_REQUEST_NAME  = 'Test';
     objInner.P_SOURCE_SYSTEM  = 'Test';
     objInner.lstReg  = lstReg;
  }
   
}