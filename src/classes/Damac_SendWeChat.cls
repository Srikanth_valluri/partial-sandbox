/*
    Apex Class : Damac_SendWeChat 
    Test Class Name :Damac_SendWeChat_Test
    Functionality : This class is used to display the incoming messages and to send a message to wechat user from an inquiry record.
*/

global class Damac_SendWeChat {
    public String msgText { get; set; }
    public String msgFileText { get; set; }
    public String fileType { get; set; }
    public Id recId { get; set; }
    public Map <Long, List <Wechat_Deliverability__c>> outboundMsgs { get; set; }
    public Map <Long, List <Wechat_Deliverability__c>> inboundMsgs { get; set; }
    public Set <Long> msgReceivedDates { get; set; }
    public List <String> imageURLs { get; set; }
    public Map <String, String> imageURLsMap { get; set; }
    public Map <String, String> documentTypeMap { get; set; }
    public Map <String, String> fileNames { get; set; }
    public SObject inq;
    public String wechatId { get; set; }
    public String msgFileLink { get; set; }
    public Boolean enableWechat { get; set; }
    public Damac_SendWeChat () {
        msgText = '';
        msgFileText = '';
        msgFileLink = '';
        inq = null;
        wechatId = '';
        fileType = 'text';
        enableWechat = true;
        
        fileNames = new Map <String, String> ();
        imageURLs = new List <String> (); 
        
        imageURLsMap = new Map <String, String> ();
        recId = apexpages.currentpage().getparameters().get('id');
        String sObjName = recId.getSObjectType().getDescribe().getName();

        inq = Database.Query ('SELECT OwnerId, WeChat_Id__c FROM '+sObjName+' WHERE Id =: recId');
        wechatId = String.valueOf (inq.get ('WeChat_Id__c'));
        if (wechatId == NULL) {
            wechatId = '';
        }
        // If the looged in user is owner of the inquiry then only we are giving him the option to view/send messages
        if (inq.get ('OwnerId') != UserInfo.getUserId ()) {
            enableWechat = false; 
            
            
        } else {
            init ();
        }
    }
    // To display existing content documents in wechat message page
    @RemoteAction
    global static String getContentDocURL (id currentVersionId) {
        
        ContentDistribution newDistribution = new ContentDistribution(
            ContentVersionId = currentVersionId, Name = 'External Link',
            PreferencesNotifyOnVisit = false);
        insert newDistribution;
        
        ContentDistribution distributionURL = [SELECT Id, DistributionPublicUrl, ContentDownloadUrl, ContentDocumentId
                                           FROM ContentDistribution 
                                           WHERE Id =: newDistribution.ID
                                           order BY LastModifiedDate DESC];
       System.Debug (distributionURL.ContentDownloadUrl);
        return distributionURL.ContentDownloadUrl;
    }
    // Method to display inbound/outbound messages on the wechat vf page based on the record created date/time
    public void init () {
        msgText = '';
        msgFileText = '';
        msgFileLink = '';
        fileType = 'text';
        outboundMsgs = new Map <Long, List <Wechat_Deliverability__c>> ();
        inboundMsgs = new Map <Long, List <Wechat_Deliverability__c>> ();
        msgReceivedDates = new Set <Long> ();
        String uniqueKey = inq.get ('weChat_id__c')+UserInfo.getUserId ();
        
        for (Wechat_Deliverability__c msg :[SELECT Message_Text__c , Message_Media_URL__c, 
                                            Message_Coordinates__c, Message_Type__c,
                                            Message_Media_Type__c, Message_Status__c,
                                            Message_Source_Type__c, LastModifiedDate ,
                                            Message_Coordinates__Latitude__s, Message_Coordinates__Longitude__s
                                            FROM Wechat_Deliverability__c 
                                            WHERE Unique_Id__c =: uniqueKey order by lastModifiedDate Asc LIMIT 500]) 
        {
            
            msgReceivedDates.add (msg.LastModifiedDate.getTime());
            if (msg.Message_Source_Type__c == 'Outbound') {
                if (outboundMsgs.containsKey (msg.LastModifiedDate.getTime ()))
                    outboundMsgs.get (msg.LastModifiedDate.getTime ()).add (msg);
                else
                    outboundMsgs.put (msg.LastModifiedDate.getTime (), new List <Wechat_Deliverability__c> {msg});
            } else {

            }
            if (msg.Message_Source_Type__c == 'Inbound') {
                if (inboundMsgs.containsKey (msg.LastModifiedDate.getTime ()))
                    inboundMsgs.get (msg.LastModifiedDate.getTime ()).add (msg);
                else
                    inboundMsgs.put (msg.LastModifiedDate.getTime (), new List <Wechat_Deliverability__c> {msg});
            }
        }
        for (Long key :msgReceivedDates) {
            if (!outboundMsgs.containsKey(key)) {
                outboundMsgs.put (key, new List <Wechat_Deliverability__c> ());
            }
            if (!inboundMsgs.containsKey(key)) {
                inboundMsgs.put (key, new List <Wechat_Deliverability__c> ());
            }
        }
        System.Debug (msgReceivedDates);
        System.Debug (outboundMsgs);
        System.Debug (inboundMsgs);
       
    }
    // Method to send wechat message to wechat service
    public void sendMessage () {
        String fileTypeLower = fileType.toLowerCase ();
        if (fileTypeLower == 'jpg' || fileTypeLower == 'jpeg' || fileTypeLower == 'png' || fileTypeLower == 'gif') {
            fileType = 'image';   
        } 
        else {
            if (fileType != 'text')
                fileType = 'file';        
        }
        
        if (msgFileLink != '' && msgFileLink != NULL) {
            msgText = msgText.replace (msgFileLink, '');
            msgFileLink = msgFileLink+'.'+fileTypeLower;        
        }
        if (!Test.isRunningTest ())
            Damac_WeChatServices.PostMessage (inq.Id, String.valueOf (inq.get ('weChat_id__c')), msgText, fileType, msgFileLink);
        msgText = '';
        fileType ='text';
        msgFileLink = '';        
        init ();
    }
    
}