@isTest
public class Damac_SalesOfficeOwnerAllocation_Test {
    static testMethod void SalesOfficeOwnerAllocation() {
        Activate_New_Assignment_Process__c process = new Activate_New_Assignment_Process__c ();
        process.Power_Line_Range_Ends_At__c = 25;
        process.Non_Power_Line_Starts_At__c = 25;
        insert process;
        
        List <Inquiry_User_Assignment_Rules__c> algList = new List <Inquiry_User_Assignment_Rules__c> ();
        Inquiry_User_Assignment_Rules__c alg = new Inquiry_User_Assignment_Rules__c ();
        alg.Daily__c = 1;
        alg.Weekly__c = 2;
        alg.Monthly__c = 2;
        alg.Net_Direct_Sales_Rank__c = NULL;
        alg.setUpOwnerID = NULL;
        algList.add (alg);
        
        Inquiry_User_Assignment_Rules__c alg1 = new Inquiry_User_Assignment_Rules__c ();
        alg1.Daily__c = 0;
        alg1.Weekly__c = 0;
        alg1.Monthly__c = 2;
        alg1.Net_Direct_Sales_Rank__c = 1;
        alg1.setUpOwnerID = userInfo.getUserID ();
        algList.add (alg1);
        
        insert algList;
        
        Damac_SalesOfficeOwnerAllocation SalesOfficeOwnerAllocation = new Damac_SalesOfficeOwnerAllocation();
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c='Test';
        inquiry.Last_Name__c='Test';
        inquiry.Allocation_Model__c='Power Line';
        inquiry.Assignment_Queue_ID__c = algList[0].Id;
        insert inquiry;
        Campaign__c campaign = new Campaign__c();
        campaign.Start_Date__c=system.today();
        campaign.End_Date__c = system.today()+2;
        campaign.Marketing_Start_Date__c=system.today();
        campaign.Marketing_End_Date__c=system.today();
        insert campaign;
        
        Assigned_Pc__c pc = new Assigned_pc__c ();
        pc.User__c = UserInfo.getUserID();
        pc.Campaign__c  = campaign.id;
        insert pc;
        
        
        
        SalesOfficeOwnerAllocation.assignOwner(inquiry, campaign.Id,false);
        
    }
    static testMethod void SalesOfficeOwnerAllocation1() {
        Activate_New_Assignment_Process__c process = new Activate_New_Assignment_Process__c ();
        process.Power_Line_Range_Ends_At__c = 25;
        process.Non_Power_Line_Starts_At__c = 25;
        insert process;
        
        List <Inquiry_User_Assignment_Rules__c> algList = new List <Inquiry_User_Assignment_Rules__c> ();
        Inquiry_User_Assignment_Rules__c alg = new Inquiry_User_Assignment_Rules__c ();
        alg.Daily__c = 1;
        alg.Weekly__c = 2;
        alg.Monthly__c = 2;
        alg.Net_Direct_Sales_Rank__c = NULL;
        alg.setUpOwnerID = NULL;
        algList.add (alg);
        
        Inquiry_User_Assignment_Rules__c alg1 = new Inquiry_User_Assignment_Rules__c ();
        alg1.Daily__c = 0;
        alg1.Weekly__c = 0;
        alg1.Monthly__c = 2;
        alg1.User_Profile__c  = 'Property Consultant';
        alg1.Net_Direct_Sales_Rank__c = 1;
        alg1.setUpOwnerID = userInfo.getUserID ();
        algList.add (alg1);
        
        insert algList;
        
        // Rule
        inquiry_assignment_rules__c rule = new inquiry_assignment_rules__c();
        rule.sales_office__c = 'OCEAN HEIGHTS';
        insert rule;
        
        Damac_SalesOfficeOwnerAllocation SalesOfficeOwnerAllocation = new Damac_SalesOfficeOwnerAllocation();
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c='Test';
        inquiry.Last_Name__c='Test';
        inquiry.Allocation_Model__c='Power Line';
        inquiry.Assignment_Queue_ID__c = algList[0].Id;
        inquiry.inquiry_assignment_rules__c = rule.id;
        insert inquiry;

        SalesOfficeOwnerAllocation.assignOwner(inquiry, NULL,true);
        
    }
    
    //Non Power line
    
    static testMethod void SalesOfficeOwnerAllocation2() {
        ID consultantId = [ Select id,UserType from Profile where name = 'Property Consultant'].id;
        User u = new User( email='test-user1@fakeemail.com', profileid = consultantId, 
                  UserName='test-user@fakeemail.com', alias='tuser2', CommunityNickName='tuser1', isActive = true,
                  TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
                  LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User',sales_office__c='OCEAN HEIGHTS');
        insert u;
        
        Activate_New_Assignment_Process__c process = new Activate_New_Assignment_Process__c ();
        process.Power_Line_Range_Ends_At__c = 1;
        process.Non_Power_Line_Starts_At__c = 1;
        insert process;
        
        List <Inquiry_User_Assignment_Rules__c> algList = new List <Inquiry_User_Assignment_Rules__c> ();
        Inquiry_User_Assignment_Rules__c alg = new Inquiry_User_Assignment_Rules__c ();
        alg.Daily__c = 1;
        alg.Weekly__c = 2;
        alg.Monthly__c = 2;
        alg.Net_Direct_Sales_Rank__c = null;
        alg.setUpOwnerID = NULL;
        algList.add (alg);
        
        Inquiry_User_Assignment_Rules__c alg1 = new Inquiry_User_Assignment_Rules__c ();
        alg1.Daily__c = 0;
        alg1.Weekly__c = 0;
        alg1.Monthly__c = 2;
        alg1.User_Profile__c  = 'Property Consultant';
        alg1.Net_Direct_Sales_Rank__c = 1;
        alg1.setUpOwnerID = u.id;
        algList.add (alg1);
        
        insert algList;
        
        // Rule
        inquiry_assignment_rules__c rule = new inquiry_assignment_rules__c();
        rule.sales_office__c = 'OCEAN HEIGHTS';
        insert rule;
        
        Damac_SalesOfficeOwnerAllocation SalesOfficeOwnerAllocation = new Damac_SalesOfficeOwnerAllocation();
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c='Test';
        inquiry.Last_Name__c='Test';
        inquiry.Allocation_Model__c='Non Power Line';
        inquiry.Assignment_Queue_ID__c = algList[0].Id;
        inquiry.inquiry_assignment_rules__c = rule.id;
        inquiry.sales_office__c = 'OCEAN HEIGHTS';
        insert inquiry;
        //inquiry.is_time_based_conversion__c =false;
        //update inquiry;

        SalesOfficeOwnerAllocation.assignOwner(inquiry, NULL,TRUE);
        
    }

}