public class DAMAC_RMManDaysController {
    
    
    public RM_Man_Days__c rmManDays { get; set; }
    
    public List <RM_Man_Days__c> activeRMs { get; set;}
    public String message { get; set; }
    public String userQuery = 'SELECT Name FROM User WHERE IsActive = TRUE AND Profile.name LIKE \'%Property Consultant%\'';

    public List <SelectOption> getRMNames() {
        List <SelectOption> rms = new List <SelectOption> ();
        User currentUser = new User ();
        currentUser = [SELECT New_Team__c FROM User WHERE ID =: UserInfo.getUserId ()];
        
        if (currentUser.New_Team__c != null) {
            String userTeam = currentUser.New_Team__c;        
            userQuery += ' AND New_Team__c =: userTeam ';
        }
        for (User u : Database.query (userQuery)) {
            rms.add (new selectOption (u.id, u.Name));
        }
        return rms; 
    }    
    public DAMAC_RMManDaysController (ApexPages.standardController stdController) {
        message = '';
        rmManDays = new RM_Man_Days__c ();
        activeRMs = new List <RM_Man_Days__c> ();
        
        User currentUser = new User ();
        currentUser = [SELECT New_Team__c FROM User WHERE ID =: UserInfo.getUserId ()];
        
        if (currentUser.New_Team__c != null) {
            String userTeam = currentUser.New_Team__c;        
            userQuery += ' AND New_Team__c =: userTeam ';
        }
        
        for (User u : Database.query (userQuery))
        {
            RM_Man_Days__c rm = new RM_Man_Days__c ();
            rm.Rm_Name__c = u.Id;
            activeRms.add (rm);
        }
        
    }
    
    public pageReference save () {
        message = '';
        List <RM_Man_Days__c> rmsToUpsert = new List <RM_Man_Days__c> ();
        if (rmManDays.Month__c != null && rmManDays.Year__c != null) {
            
            for (RM_Man_Days__c rm :activeRMs) {
                rm.Month__c = rmManDays.Month__c;
                rm.Year__c = rmManDays.Year__c;
                
                //if ( rm.man_days__c != null || rm.Leave_Days__c != null || rm.Working_Days_on_Stands__c != null) {
                    rm.unique_id__c = rm.RM_Name__c+'-'+rm.Year__c+'-'+rm.Month__c;
                    rmsToUpsert.add (rm);
                    
                //}
            }
        } else {
            message = 'Please select month and year.';
        }
        System.debug (message);
        if (message == '') {
            Database.upsert (rmsToUpsert, false);
            message = 'Success';
            return new pageReference ('/'+RM_Man_Days__c.sobjecttype.getDescribe().getKeyPrefix());
        } else {
            return null;
        }
        
       
    }
    
    public pageReference cancel () {
        
        return new pageReference ('/'+RM_Man_Days__c.sobjecttype.getDescribe().getKeyPrefix());
    }
}