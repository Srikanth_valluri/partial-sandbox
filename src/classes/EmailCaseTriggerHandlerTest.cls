// Description - Test class for EmailCaseTriggerHandler
@isTest
private class EmailCaseTriggerHandlerTest {
    static testMethod void afterInsertTest1() {
    
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
    
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.Subject = 'Test email';
        objCase.AccountId = objAcc.id;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        insert objCase;


        Case objCase1 = new Case();
        objCase1.Origin = 'Email';
        objCase1.Subject = 'Test email';
        objCase1.AccountId = objAcc.id;
        objCase1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RentalPool Email').getRecordTypeId();
        insert objCase1;
        
        Case objCase2 = new Case();
        objCase2.Origin = 'Email';
        objCase2.Subject = 'Test email';
        objCase2.AccountId = objAcc.id;
        objCase2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Collection Email').getRecordTypeId();
        insert objCase2;        
        
        EmailMessage[] newEmail1 = new EmailMessage[0];
        newEmail1.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'atyourservice@damacproperties.com;crmrentalpool@damacproperties.com;collection@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase.Id)); 
        insert newEmail1;
        
        EmailMessage[] newEmail2 = new EmailMessage[0];
        newEmail2.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'atyourservice@damacproperties.com;crmrentalpool@damacproperties.com;collection@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase1.Id)); 
        insert newEmail2;
        
        EmailMessage[] newEmail3 = new EmailMessage[0];
        newEmail3.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'atyourservice@damacproperties.com;crmrentalpool@damacproperties.com;collection@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase2.Id)); 
        insert newEmail3;
    }
    
    static testMethod void afterInsertTest2() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
    
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.Subject = 'Test email';
        objCase.AccountId = objAcc.id;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        insert objCase;
        
        objCase.Status = 'Closed';
        update objCase;
        
        EmailMessage[] newEmail1 = new EmailMessage[0];
        newEmail1.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'atyourservice@damacproperties.com',
            CcAddress = '',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase.Id)); 
        insert newEmail1;
    }
    
    static testMethod void afterInsertTest3() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
    
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.AccountId = objAcc.id;
        objCase.Subject = 'Test email';
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RentalPool Email').getRecordTypeId();
        insert objCase;

        objCase.Status = 'Closed';
        update objCase;     
        
        EmailMessage[] newEmail1 = new EmailMessage[0];
        newEmail1.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'crmrentalpool@damacproperties.com',
            CcAddress = '',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase.Id)); 
        insert newEmail1;
    }
    
    static testMethod void afterInsertTest4() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
        
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.AccountId = objAcc.id;
        objCase.Subject = 'Test email';
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Collection Email').getRecordTypeId();
        insert objCase;

        objCase.Status = 'Closed';
        update objCase;
        
        EmailMessage[] newEmail1 = new EmailMessage[0];
        newEmail1.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'collection@damacproperties.com',
            CcAddress = '',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase.Id)); 
        insert newEmail1;
    }
    
    static testMethod void afterInsertTest5() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
        
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.AccountId = objAcc.id;
        objCase.Subject = 'Test email';
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        insert objCase;

        Case objCase1 = new Case();
        objCase1.Origin = 'Email';
        objCase1.AccountId = objAcc.id;
        objCase1.Subject = 'Test email';
        objCase1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RentalPool Email').getRecordTypeId();
        insert objCase1;
        
        Case objCase2 = new Case();
        objCase2.Origin = 'Email';
        objCase2.AccountId = objAcc.id;
        objCase2.Subject = 'Test email';
        objCase2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Collection Email').getRecordTypeId();
        insert objCase2;        
        
        EmailMessage[] newEmail1 = new EmailMessage[0];
        newEmail1.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'atyourservice@damacproperties.com',
            CcAddress = 'crmrentalpool@damacproperties.com',
            BccAddress = 'collection@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase.Id)); 
        insert newEmail1;
        
        EmailMessage[] newEmail2 = new EmailMessage[0];
        newEmail2.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'atyourservice@damacproperties.com',
            CcAddress = 'crmrentalpool@damacproperties.com',
            BccAddress = 'collection@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase1.Id)); 
        insert newEmail2;
        
        EmailMessage[] newEmail3 = new EmailMessage[0];
        newEmail3.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'atyourservice@damacproperties.com',
            CcAddress = 'crmrentalpool@damacproperties.com',
            BccAddress = 'collection@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase2.Id)); 
        insert newEmail3;
    
    }
    
    static testMethod void afterInsertTest6() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
        
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.AccountId = objAcc.id;
        objCase.Subject = 'Test email';
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        insert objCase;

        Case objCase1 = new Case();
        objCase1.Origin = 'Email';
        objCase1.Subject = 'Test email';
        objCase1.AccountId = objAcc.id;
        objCase1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RentalPool Email').getRecordTypeId();
        insert objCase1;
        
        Case objCase2 = new Case();
        objCase2.Origin = 'Email';
        objCase2.AccountId = objAcc.id;
        objCase2.Subject = 'Test email';
        objCase2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Collection Email').getRecordTypeId();
        insert objCase2;
        
        EmailMessage[] newEmail1 = new EmailMessage[0];
        newEmail1.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'crmrentalpool@damacproperties.com',
            CcAddress = 'collection@damacproperties.com',
            BccAddress = 'atyourservice@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase.Id)); 
        insert newEmail1;
        
        EmailMessage[] newEmail2 = new EmailMessage[0];
        newEmail2.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'crmrentalpool@damacproperties.com',
            CcAddress = 'collection@damacproperties.com',
            BccAddress = 'atyourservice@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase1.Id)); 
        insert newEmail2;
        
        EmailMessage[] newEmail3 = new EmailMessage[0];
        newEmail3.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'crmrentalpool@damacproperties.com',
            CcAddress = 'collection@damacproperties.com',
            BccAddress = 'atyourservice@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase2.Id)); 
        insert newEmail3;
        
    }
    
    static testMethod void afterInsertTest7() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
    
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.Subject = 'Test email';
        objCase.AccountId = objAcc.id;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        insert objCase;

        Case objCase1 = new Case();
        objCase1.Origin = 'Email';
        objCase1.Subject = 'Test email';
        objCase1.AccountId = objAcc.id;
        objCase1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RentalPool Email').getRecordTypeId();
        insert objCase1;
        
        Case objCase2 = new Case();
        objCase2.Origin = 'Email';
        objCase2.AccountId = objAcc.id;
        objCase2.Subject = 'Test email';
        objCase2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Collection Email').getRecordTypeId();
        insert objCase2;
        
        EmailMessage[] newEmail1 = new EmailMessage[0];
        newEmail1.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'collection@damacproperties.com',
            CcAddress = 'crmrentalpool@damacproperties.com',
            BccAddress = 'atyourservice@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase.Id)); 
        insert newEmail1;
        
        EmailMessage[] newEmail2 = new EmailMessage[0];
        newEmail2.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'collection@damacproperties.com',
            CcAddress = 'crmrentalpool@damacproperties.com',
            BccAddress = 'atyourservice@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase1.Id)); 
        insert newEmail2;
        
        EmailMessage[] newEmail3 = new EmailMessage[0];
        newEmail3.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'collection@damacproperties.com',
            CcAddress = 'crmrentalpool@damacproperties.com',
            BccAddress = 'atyourservice@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase2.Id)); 
        insert newEmail3;
        
    }
    static testMethod void afterInsertTest8() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
        
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.Subject = 'Test email';
        objCase.AccountId = objAcc.id;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        insert objCase;

        Case objCase2 = new Case();
        objCase2.Origin = 'Email';
        objCase2.Subject = 'Test email';
        objCase2.AccountId = objAcc.id;
        objCase2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Collection Email').getRecordTypeId();
        insert objCase2;
        
        EmailMessage[] newEmail1 = new EmailMessage[0];
        newEmail1.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'atyourservice@damacproperties.com;collection@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase.Id)); 
        insert newEmail1;
        
        EmailMessage[] newEmail3 = new EmailMessage[0];
        newEmail3.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'atyourservice@damacproperties.com;collection@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase2.Id)); 
        insert newEmail3;
        
    }
    
    static testMethod void afterInsertTest9() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
        
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.AccountId = objAcc.id;
        objCase.Subject = 'Test email';
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        insert objCase;

        Case objCase1 = new Case();
        objCase1.Origin = 'Email';
        objCase1.Subject = 'Test email';
        objCase1.AccountId = objAcc.id;
        objCase1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RentalPool Email').getRecordTypeId();
        insert objCase1;
        
        EmailMessage[] newEmail1 = new EmailMessage[0];
        newEmail1.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'atyourservice@damacproperties.com;crmrentalpool@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase.Id)); 
        insert newEmail1;
        
        EmailMessage[] newEmail3 = new EmailMessage[0];
        newEmail3.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'atyourservice@damacproperties.com;crmrentalpool@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase1.Id)); 
        insert newEmail3;
    }
    
    static testMethod void afterInsertTest10() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
        
        Case objCase1 = new Case();
        objCase1.Origin = 'Email';
        objCase1.AccountId = objAcc.id;
        objCase1.Subject = 'Test email';
        objCase1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RentalPool Email').getRecordTypeId();
        insert objCase1;
        
        Case objCase2 = new Case();
        objCase2.Origin = 'Email';
        objCase2.AccountId = objAcc.id;
        objCase2.Subject = 'Test email';
        objCase2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Collection Email').getRecordTypeId();
        insert objCase2;
        
        EmailMessage[] newEmail1 = new EmailMessage[0];
        newEmail1.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'collection@damacproperties.com;crmrentalpool@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase1.Id)); 
        insert newEmail1;
        
        EmailMessage[] newEmail3 = new EmailMessage[0];
        newEmail3.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'collection@damacproperties.com;crmrentalpool@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase2.Id)); 
        insert newEmail3;
        
    }
    
    static testMethod void afterInsertTest11() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
        
        Case objCase1 = new Case();
        objCase1.Origin = 'Email';
        objCase1.AccountId = objAcc.id;
        objCase1.Subject = 'Test email';
        objCase1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RentalPool Email').getRecordTypeId();
        insert objCase1;

        EmailMessage[] newEmail3 = new EmailMessage[0];
        newEmail3.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'crmrentalpool@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase1.Id)); 
        insert newEmail3;
        

        EmailMessage[] newEmail1 = new EmailMessage[0];
        newEmail1.add(new EmailMessage(
            ToAddress = 'test@abc.org', 
            FromAddress = 'crmrentalpool@damacproperties.com',
            Incoming = False,
            Subject = 'Re: Test email', 
            TextBody = '23456 ref:_00QJSDOLFNF:ref ', 
            ParentId = objCase1.Id)); 
        insert newEmail1;
        
    }

    static testMethod void afterInsertTest12() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
        
        Case objCase1 = new Case();
        objCase1.Origin = 'Email';
        objCase1.AccountId = objAcc.id;
        objCase1.Subject = 'Test email';
        objCase1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RentalPool Email').getRecordTypeId();
        insert objCase1;

        Case objCase2 = new Case();
        objCase2.Origin = 'Email';
        objCase2.AccountId = objAcc.id;
        objCase2.Subject = 'RE:Re: Test email';
        objCase2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Collection Email').getRecordTypeId();
        insert objCase2;
        
        EmailMessage[] newEmail3 = new EmailMessage[0];
        newEmail3.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'crmrentalpool@damacproperties.com',
            Incoming = True,
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase1.Id)); 
        insert newEmail3;
        

        EmailMessage[] newEmail1 = new EmailMessage[0];
        newEmail1.add(new EmailMessage(
            ToAddress = 'test@abc.org', 
            FromAddress = 'crmrentalpool@damacproperties.com',
            Incoming = false,
            Subject = 'Re: Test email', 
            TextBody = '23456 ref:_00QJSDOLFNF:ref ', 
            ParentId = objCase1.Id)); 
        insert newEmail1;

        objCase1.Status = 'Closed';
        update objCase1;
        
        EmailMessage[] newEmail2 = new EmailMessage[0];
        newEmail2.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'crmrentalpool@damacproperties.com;collection@damacproperties.com',
            Incoming = True,
            Subject = 'RE:Re: Test email', 
            TextBody = 'Test 23456 ref:_00QJSDOLFNF:ref ', 
            ParentId = objCase1.Id)); 
        insert newEmail2;

        EmailMessage[] newEmail4 = new EmailMessage[0];
        newEmail4.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'crmrentalpool@damacproperties.com;collection@damacproperties.com',
            Incoming = True,
            Subject = 'RE:Re: Test email', 
            TextBody = 'Test 23456 ref:_00QJSDOLFNF:ref ', 
            ParentId = objCase2.Id)); 
        insert newEmail4;
        
    }
    
}