public class CRMNexmoWAReplyCntrl{
    
    public String crmNexmoWhatsappRequestId;
    

    public CRMNexmoWAReplyCntrl(ApexPages.StandardController stdController) {
        crmNexmoWhatsappRequestId = (Id)stdController.getRecord().Id;
    }
    
    public pageReference init(){
        List<CRM_Nexmo_Whats_App_Request__c> lstCRM_Nexmo_Whats_App_Request = new List<CRM_Nexmo_Whats_App_Request__c>();
        List<Account> lstAccount = new List<Account>();
        if( crmNexmoWhatsappRequestId != '' && crmNexmoWhatsappRequestId != null ) {
            lstCRM_Nexmo_Whats_App_Request = [ SELECT Id
                                                    , Last_Message_Sent__c
                                                    , Account__c
                                                    , Account__r.Mobile__c
                                                    , Account__r.Mobile_Phone_Encrypt__pc
                                                    , Last_MTM_Sent_at__c
                                                    , To_number__c
                                                 FROM CRM_Nexmo_Whats_App_Request__c
                                                WHERE Id =: crmNexmoWhatsappRequestId ];
            for( CRM_Nexmo_Whats_App_Request__c objCRM_Nexmo_Whats_App_Request : lstCRM_Nexmo_Whats_App_Request ) {         
                objCRM_Nexmo_Whats_App_Request.Last_Message_Sent__c = system.now();
                objCRM_Nexmo_Whats_App_Request.Last_MTM_Sent_at__c = system.now();
                
                Account objAccount =  new Account();
                objAccount.Id = objCRM_Nexmo_Whats_App_Request.Account__c;
                objAccount.Mobile__c = String.valueOf( objCRM_Nexmo_Whats_App_Request.To_number__c );
                objAccount.Mobile_Phone_Encrypt__pc = String.valueOf( objCRM_Nexmo_Whats_App_Request.To_number__c);
                lstAccount.add(objAccount);
                //lstCRM_Nexmo_Whats_App_Request.add(objCRM_Nexmo_Whats_App_Request);
            }
            
            if(  lstCRM_Nexmo_Whats_App_Request != null && lstCRM_Nexmo_Whats_App_Request.size() > 0 ) {
                update lstCRM_Nexmo_Whats_App_Request;
            }
            if(  lstAccount != null && lstAccount.size() > 0 ) {
                update lstAccount;
            }
            
        }
        
        PageReference nexmoWAPage = new PageReference('/apex/crmNexmoWhatsapp');
        nexmoWAPage.setRedirect(true);
        nexmoWAPage.getParameters().put('Id', lstCRM_Nexmo_Whats_App_Request[0].Account__c);
        nexmoWAPage.getParameters().put('crmNexmoWARequestId', lstCRM_Nexmo_Whats_App_Request[0].Id);
        return nexmoWAPage;
        
    }
    
}