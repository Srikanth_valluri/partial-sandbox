@RestResource(urlMapping='/SendComplainttSubServiceListToMobileApp/*')
 Global class SendComplainttSubServiceListToMobileApp
 {
     @HtTPPost
    Global static list<String> SendComplainttSubServiceTypeListToMobile(String ProcessType)
    {
       List<String>lstSubProcessOptions = new List<String>();
                
             if(String.IsNotBlank(ProcessType)&& ProcessType=='Payment')
             {
                lstSubProcessOptions = new List<String>();
                lstSubProcessOptions.add('Not reflecting in SOA');
                lstSubProcessOptions.add('Others');
              
             }else if(String.IsNotBlank(ProcessType)&& ProcessType=='Handover')
             {
                lstSubProcessOptions = new List<String>();
                lstSubProcessOptions.add('Appointment');
                lstSubProcessOptions.add('Others');
                lstSubProcessOptions.add('Unit Viewing');
                lstSubProcessOptions.add('Delay');
             }else if(String.IsNotBlank(ProcessType)&& ProcessType=='Assignment')
             {
                lstSubProcessOptions = new List<String>();
                lstSubProcessOptions.add('Appointment');
                lstSubProcessOptions.add('Others');
                lstSubProcessOptions.add('NOC related');
                lstSubProcessOptions.add('Process related');
             }else if(String.IsNotBlank(ProcessType)&& ProcessType=='Parking')
             {
                lstSubProcessOptions = new List<String>();
                lstSubProcessOptions.add('Payment related');
                lstSubProcessOptions.add('Others');
             }else if(String.IsNotBlank(ProcessType)&& ProcessType=='Rental Pool')
             {
               lstSubProcessOptions = new List<String>();
                lstSubProcessOptions.add('Service Charge related');
                lstSubProcessOptions.add('Others');
             }else if(String.IsNotBlank(ProcessType)&& ProcessType=='Title Deed')
             {
                lstSubProcessOptions = new List<String>();
                lstSubProcessOptions.add('Process related');
                lstSubProcessOptions.add('Others');
             }else if(String.IsNotBlank(ProcessType)&& ProcessType=='Mortgage')
             {
                lstSubProcessOptions = new List<String>();
                lstSubProcessOptions.add('Process related');
                lstSubProcessOptions.add('Others');
             }else if(String.IsNotBlank(ProcessType)&& ProcessType=='Snagging')
             {
                lstSubProcessOptions = new List<String>();
                lstSubProcessOptions.add('Process related');
                lstSubProcessOptions.add('Others');
             }else if(String.IsNotBlank(ProcessType)&& ProcessType=='Document Related')
             {
                lstSubProcessOptions = new List<String>();
                lstSubProcessOptions.add('SPA related');
                lstSubProcessOptions.add('Others');
                lstSubProcessOptions.add('OQOOD/PRC related');
             }else if(String.IsNotBlank(ProcessType)&& ProcessType=='Others')
             {
                lstSubProcessOptions = new List<String>();
                
                lstSubProcessOptions.add('Others');
             }
            return lstSubProcessOptions;   
     }
 }