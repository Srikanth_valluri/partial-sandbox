public class AsyncDeactivateUnitAssignment implements Queueable, Database.AllowsCallouts  {
    
    public Id unitAssignmentId;
    public AsyncDeactivateUnitAssignment (id unitID) {
        unitAssignmentId = unitId;
    }
    public void execute (QueueableContext context) {
        ActiveUnitAssignmentController.deActivateUA (unitAssignmentId, true);      
    }
}