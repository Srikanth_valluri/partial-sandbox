/***********************************************************************************
* Controller Class : DiassociateInventoryController
* Created By : Tejashree Chavan
-----------------------------------------------------------------------------------
* Description : This is a controller class for DiassociateInventory
*
* Test Data : DiassociateInventoryControllerTest
-----------------------------------------------------------------------------------
* Version History:
* VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
1.0         Tejashree Chavan     14/12/2017      Initial Development
**********************************************************************************/

Public class DiassociateInventoryController {
    public String inventoryId;
    Public String unitAssignmentId {get; set;}
    
    /*******************************************************************************
* @Description : This is Constructor method                                    *
* @Params      :                                                               *
* @Return      :                                                               *
********************************************************************************/
    public DiassociateInventoryController (ApexPages.StandardController controller) {
        inventoryId= String.valueof(controller.getRecord().get('id'));
    }
    
    /*********************************************************************************
* @Description : This method will diassociate the unit assignmnet from inventory *
* @Params      :                                                                 *
* @Return      :PageReference                                                    *
**********************************************************************************/
    public void diassociateInventory() {
        String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm() + '/';
        List<Inventory__c> inventories = [SELECT
                                          Id,Selling_Price__c,Inventory_ID__c,
                                          Name, UA_Proposal_Price__c,
                                          Tagged_To_Unit_Assignment__c,
                                          Unit_Assignment__c,Release_ID__c // getting release id, added by Srikanth
                                          FROM
                                          Inventory__c
                                          WHERE Id = :inventoryId ];
        Unit_Assignment__c assignment = new Unit_Assignment__c ();
        assignment = [SELECT Inventory_Count__c, Proposal_Price__c, Deal_Value__c,
                      CreatedBy.ID, Agency__c, Agency__r.Vendor_id__c,  // getting agency, added by Srikanth
                      createdBy.IPMS_Employee_ID__c,  // getting IMPS employee id, added by Srikanth
                      Reason_For_Unit_Assignment__c,
                      CreatedBy.ManagerID,Name,
                      createdBy.Manager.IPMS_Employee_ID__c // getting manager's IMPS employee id, added by Srikanth
                      FROM Unit_Assignment__c WHERE ID =: inventories[0].Unit_Assignment__c];
        
        /* Modified By : Srikanth Valluri
* Description : decreasing the proposal price, deal values of removed inventory on UA
* Modified on : 19/02/2018
*/
        
        if (assignment.Inventory_Count__c > 0) {
            assignment.Inventory_Count__c = assignment.Inventory_Count__c -1;  //decreasing inventory count, added by Srikanth
        } else {
            assignment.Inventory_Count__c = 0;
        }
        if (assignment.Proposal_Price__c != null && inventories[0].UA_Proposal_Price__c != null) {
            assignment.Proposal_Price__c = assignment.Proposal_Price__c - inventories[0].UA_Proposal_Price__c; // subtracting proposal price of inventory from UA, added by Srikanth 
        }
        if (assignment.Deal_Value__c != null && inventories[0].Selling_Price__c != null) {
            assignment.Deal_Value__c = assignment.Deal_Value__c - inventories[0].Selling_Price__c; // subtracting deal value of inventory from UA, added by Srikanth
        }
        
        if(inventories != null && !inventories .isEmpty()) {
            String status = AsyncWebserviceToIPMS.createUAJSON (inventories, assignment, true, false); // Sending Inventories to IPMS, added by Srikanth
            if(Test.isRunningTest())
                status = 'Success';
            if (status == 'Success') {
                unitAssignmentId = inventories[0].Unit_Assignment__c;
                inventories[0].Unit_Assignment__c  = null;
                inventories[0].Tagged_To_Unit_Assignment__c = false;
                inventories[0].UA_Proposal_Price__c = null;
                update inventories;
                
                List <Inventory_User__c> invUser = new List <Inventory_User__c> ();
                invUser = [SELECT User__c, Inventory__c, Unit_Assignment__c FROM Inventory_User__c WHERE Inventory__c =: inventories[0].id];
                List <Inventory_Users_History__c> invHistory = new List <Inventory_Users_History__c> ();
                for (Inventory_User__c userVal :invUser) {
                    Inventory_Users_History__c invUserHistory = new Inventory_Users_History__c ();
                    invUserHistory.User__c = userVal.User__c;
                    invUserHistory.Inventory__c = userVal.Inventory__c;
                    invUserHistory.Unit_Assignment__c = userVal.Unit_Assignment__c;
                    invHistory.add (invUserHistory);
                }
                update assignment;
                if (invUser.size () > 0) {
                    
                    delete invUser;
                    insert invHistory;
                }
            } else {
                ApexPages.addMessage(new ApexPages.Message(
                    ApexPages.severity.Info, 'Unit Assignment Deactivation Failed, Please Try Again'));
            }
        }
    }
}