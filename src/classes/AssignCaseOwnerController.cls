public without sharing class AssignCaseOwnerController{

    public Case callingList {get;set;}
    List<Case> selectedRecords;
    String filterId;
    public AssignCaseOwnerController(ApexPages.StandardSetController controller) {
        callingList = new Case();
        selectedRecords = controller.getSelected();
        System.debug('selectedRecords:::'+selectedRecords);
        filterId = controller.getFilterId();
        System.debug('filterId:::'+filterId);
    }

    public pagereference saveOwner() {
        for(Case callInst : selectedRecords) {
            callInst.OwnerId = callingList.OwnerId;
            System.debug(' callInst.OwnerId:::'+ callInst.OwnerId);
        }
//        try {
            update selectedRecords;
/*        } catch (Exception ex) {
            ApexPages.addMessage(
                new ApexPages.message(
                    ApexPages.severity.INFO,
                    ex.getMessage()
                )
            );
            return null;
        }
*/
        //System.debug(' ApexPages.currentpage().getParameters()::'+ ApexPages.currentpage().getParameters());
        //String url = ApexPages.currentpage().getParameters().get('fcf');
         // String url = ApexPages.currentpage().getUrl();
        //system.debug('url::'+url);
        return new Pagereference(String.format(Label.Case_Lightning_List_View_URL, new List<String>{filterId}));
    }
}