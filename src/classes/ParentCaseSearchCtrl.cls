/********************************************************************************************************************************
 * Description : Class to search the parent
 *===============================================================================================================================
 * Ver      Date-DD/MM/YYYY     Author              Modification
 *===============================================================================================================================
 * 1.0      09/07/2020          Aishwarya Todkar    Initial Draft
*********************************************************************************************************************************/
public Class ParentCaseSearchCtrl {

    public Static String parentSearchKey {get; set;}
    public static List<case> listParentCases { get; set; }
    public static String parentCaseId { get; set; }
    public ParentCaseSearchCtrl() {
        parentSearchKey = '';
    }
    
/********************************************************************************************************************************
* Method Name : searchParentCase
* Description : method to search the parent
* Return Type : void
* Parameter(s): None
********************************************************************************************************************************/
    public static void searchParentCase( ) {
        System.debug('parentSearchKey=='+parentSearchKey);
        listParentCases = new List<Case>();
        Id recordTypeId = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
        String strQuery = 'Select id,casenumber from case where casenumber like \'%' +parentSearchKey +'%\''
                        + ' And recordType.Id = \'' + recordTypeId + '\'';
        
        System.debug('strQuery=='+strQuery);
        listParentCases =  database.query(strQuery);
        system.debug('parentCaseId--- ' + parentCaseId);
    }   
}