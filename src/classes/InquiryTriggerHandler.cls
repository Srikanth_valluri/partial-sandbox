public class InquiryTriggerHandler extends InquiryService implements TriggerFactoryInterface{


    public void executeBeforeInsertTrigger(List<sObject> lstNewRecords){
       
    }
    public static Map<Id,Date> userToTenureMap      = new Map<Id,Date>();
    /*********************************************************************************************
    * @Description : Method to get available user based on the assigned campaign or property.    *
    * @Params      : Map<Id, List<User>>                                                         *
    * @Return      : Map<Id, List<Id>>                                                           *
    *********************************************************************************************/
    public Map<Id, List<Id>> getUserWithLoad(Map<Id, List<User>> userPreferredLanguageMap){
        Map<Id, List<Id>> parentSortedEligibleUsersMap = new Map<Id, List<Id>>();
        Map<Id, Integer> userIdsScoreMap = new Map<Id, Integer>();
        for(Id thisKey : userPreferredLanguageMap.keySet()){
            for(User thisAssociatedUser : userPreferredLanguageMap.get(thisKey)){
                if(parentSortedEligibleUsersMap.containsKey(thisKey)){
                    parentSortedEligibleUsersMap.get(thisKey).add(thisAssociatedUser.Id);
                }else{
                    parentSortedEligibleUsersMap.put(thisKey, new List<Id>{thisAssociatedUser.Id});
                }
                userIdsScoreMap.put(thisAssociatedUser.Id, 0);
            }
        }
        system.debug('#### parentSortedEligibleUsersMap = '+parentSortedEligibleUsersMap);
        if(!userIdsScoreMap.isEmpty()){
            /* Get users with active lead. */
            //Map<Id, List<Inquiry__c>> userAssociatedInquiryListMap = getAllUserWithLeadLoad(userIdsScoreMap);
            /* Calling method to get the sorted list of available users. */
            if(!parentSortedEligibleUsersMap.isEmpty()){
                parentSortedEligibleUsersMap = new Map<Id, List<Id>>(getLoadSortedUsers(parentSortedEligibleUsersMap));
            }
        }
        return parentSortedEligibleUsersMap;
    }
    
    /*********************************************************************************************
    * @Description : Method to get available user sorted by load.                                *
    * @Params      : Map<Id, Integer>, Map<Id, Set<Id>>                                          *
    * @Return      : Map<Id, List<Id>>                                                           *
    *********************************************************************************************/
    public Map<Id, List<Id>> getLoadSortedUsers(Map<Id, List<Id>> parentSortedEligibleUsersMap){
        Map<Id, Integer> pcWithNewMeetingCount = new Map<Id, Integer>();
        Map<Id, Integer> pcWithCompletedMeetingCount = new Map<Id, Integer>();
        Map<Id, Integer> pcWithNewLeadCount = new Map<Id, Integer>();
        Map<Id, Integer> pcWithAllLeadCount = new Map<Id, Integer>();
        Map<Id, Integer> pcPerformanceBandMap = new Map<Id, Integer>();
        Map<Id, Integer> pcMeetingBandMap = new Map<Id, Integer>();
        Integer highestMeetingCount = 0;
        for(PCInquiryCounter__c thisValue : [SELECT Id, Name, All_Inquiry_Count__c, New_Inquiry_Count__c,
                                             New_Meeting_Count__c, Completed_Meeting_Count__c,
                                             Owner_Name__c
                                             FROM PCInquiryCounter__c
                                             ORDER BY Completed_Meeting_Count__c ASC]){
                                                 highestMeetingCount = Integer.valueOf(thisValue.Completed_Meeting_Count__c);
                                                 pcWithNewMeetingCount.put(thisValue.Name, Integer.valueOf(thisValue.New_Meeting_Count__c));
                                                 pcWithCompletedMeetingCount.put(thisValue.Name, Integer.valueOf(thisValue.Completed_Meeting_Count__c));
                                                 pcWithNewLeadCount.put(thisValue.Name, Integer.valueOf(thisValue.New_Inquiry_Count__c));
                                                 pcWithAllLeadCount.put(thisValue.Name, Integer.valueOf(thisValue.All_Inquiry_Count__c));
                                             }
        system.debug('#### pcWithCompletedMeetingCount = '+pcWithCompletedMeetingCount);
        system.debug('#### pcWithNewMeetingCount = '+pcWithNewMeetingCount);
        system.debug('#### pcWithAllLeadCount = '+pcWithAllLeadCount);
        system.debug('#### userToTenureMap = '+userToTenureMap);
        system.debug('#### pcWithNewLeadCount = '+pcWithNewLeadCount);
        if(parentSortedEligibleUsersMap != null){
            /* Calling method to get the pc and performance band map. */
            pcPerformanceBandMap = getPcPerformanceMap(parentSortedEligibleUsersMap);
            system.debug('#### pcPerformanceBandMap = '+pcPerformanceBandMap);
            /* Calling method to get the pc and meeting band map. */
            pcMeetingBandMap = getPcMeetingBandMap(highestMeetingCount, pcWithCompletedMeetingCount);
            system.debug('#### pcMeetingBandMap = '+pcMeetingBandMap);
            for(Id thisKey : parentSortedEligibleUsersMap.keySet()){
                List<UserLoadWrapper> ulwObjectList = new List<UserLoadWrapper>();
                for(Id thisEligibleUser : parentSortedEligibleUsersMap.get(thisKey)){
                    /* Value hardcoded till the fetching field is decided. */
                    if((!pcWithNewMeetingCount.containsKey(thisEligibleUser) ||
                        (pcWithNewMeetingCount.containsKey(thisEligibleUser) &&
                         pcWithNewMeetingCount.get(thisEligibleUser) < Integer.valueOf(Label.Max_Activities))) &&
                       (!pcWithAllLeadCount.containsKey(thisEligibleUser) ||
                        (pcWithAllLeadCount.containsKey(thisEligibleUser) &&
                         pcWithAllLeadCount.get(thisEligibleUser) < Integer.valueOf(Label.Max_Inquiries)))){
                             ulwObjectList.add(new UserLoadWrapper(thisEligibleUser,
                                                                   pcWithNewLeadCount.containsKey(thisEligibleUser) ? pcWithNewLeadCount.get(thisEligibleUser) : 0,
                                                                   pcPerformanceBandMap.containsKey(thisEligibleUser) ? pcPerformanceBandMap.get(thisEligibleUser) : Integer.valueOf(Label.Performance_Band_Size),
                                                                   userToTenureMap.containsKey(thisEligibleUser) && userToTenureMap.get(thisEligibleUser).monthsBetween(system.today()) > 6 ? 1 : 0,
                                                                   pcMeetingBandMap.containsKey(thisEligibleUser) ? pcMeetingBandMap.get(thisEligibleUser) : Integer.valueOf(Label.Activity_Band_Size),
                                                                   pcWithAllLeadCount.containsKey(thisEligibleUser) ? pcWithAllLeadCount.get(thisEligibleUser) : 0));
                         }
                }
                /* Sorting the wrapper, implemented using the comparable wrapper class. */
                system.debug('#### Wrapper before sorting = '+ulwObjectList);
                ulwObjectList.sort();
                system.debug('#### Wrapper after sorting = '+ulwObjectList);
                List<Id> sortedUserIdsList = new List<Id>();
                for(UserLoadWrapper thisUlwObject : ulwObjectList){
                    sortedUserIdsList.add(thisUlwObject.recordId);
                }
                parentSortedEligibleUsersMap.put(thisKey, new List<Id>(sortedUserIdsList));
            }
        }
        return parentSortedEligibleUsersMap;
    }


    
    public void executeBeforeUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        
    }

    public void executeAfterInsertTrigger(Map<Id, sObject> mapNewRecords){
        
    }
    public void executeAfterUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
    }
    public void executeBeforeInsertUpdateTrigger(list<sObject> lstNewRecords, Map<Id,sObject> mapOldRecords){}
    public void executeBeforeDeleteTrigger(Map<Id, sObject> mapOldRecords){}
    public void executeAfterDeleteTrigger(Map<Id, sObject> mapOldRecords){}
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
}