/**************************************************************************************************
* Name               : AttachmentTriggerHandler
* TestCase           : IPMS_Requests_Test
* Description        : This is helper class for trigger on Attachment object.                     *
* Created Date       : NSI                                                                        *
* Created By         : 24/04/2017                                                                 *
* ----------------------------------------------------------------------------------------------- *
* VERSION   AUTHOR          DATE         COMMENTS                                                  *
* 1.0       NSI - Vineet    24/04/2017   Initial Draft.                                            *
* 1.1       Rahul Godara    11/07/2017   Now we verifying ContentType using custom metadata        *
* 1.2       QBurst          21-01-2020   Added method to update Sys Id in Unit Documents
**************************************************************************************************/
public class AttachmentTriggerHandler extends Describe_Sobject_Access implements TriggerFactoryInterface{
    
    //@Testvisible private Set<String> allowedExtensionSet = new Set<String>{'doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg'};
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed before insert.                       *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeBeforeInsertTrigger(List<sObject> newRecordsList){
        DAMAC_Constants.skip_AttacmentTrigger = true;
        checkAttachmentType((List<Attachment>) newRecordsList); 
    }
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed before udpate.                       *
    * @Params      : Map<Id, sObject>, Map<Id, sObject>                                          *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeBeforeUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        preventUsersToEditDeleteAttachment((List<Attachment>) mapNewRecords.values());
        checkAttachmentType((List<Attachment>) mapNewRecords.values()); 
    }
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after insert.                        *
    * @Params      : Map<Id, sObject>                                                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterInsertTrigger(Map<Id, sObject> mapNewRecords){
        updateAnnouncementRequests((List<Attachment>) mapNewRecords.values());
        checkAttachmentsRelatedtoPOPSr((List<Attachment>) mapNewRecords.values());  
    }
    
    /*********************************************************************************************
    * @Description : Method to check the attachment type.                                        *
    *                - Only DOC, DOCX, PDF, PNG, JPEG, JPG format allowed.                       * 
    * @Params      : List<Attachment>                                                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    @Testvisible private void checkAttachmentType(List<Attachment> newAttachmentList){
        try{
            Set<String> allowedContentTypeSet=new Set<String>();
            for(Attachment_and_Content_Type_Mapping__mdt mdt:[select id,label,Content_Type__c,ExtensionType__c,Is_Allowed__c from Attachment_and_Content_Type_Mapping__mdt where Is_Allowed__c=true])
            {
                allowedContentTypeSet.add(mdt.Content_Type__c);
            }
                for(Attachment thisAttachment : newAttachmentList){
                system.debug('#### thisAttachment ContentType= '+thisAttachment.ContentType);
                /*if(thisAttachment.Body != null && thisAttachment.Name != null &&  
                   thisAttachment.Name.containsIgnoreCase('.') && !allowedExtensionSet.contains(thisAttachment.Name.subStringAfter('.').toLowerCase())){
                    thisAttachment.addError('Please enter a valid file format. Only DOC, DOCX, PDF, PNG, JPEG, JPG format allowed.');
                }*/
                  /*  if(thisAttachment.Body != null && thisAttachment.Name != null && !allowedContentTypeSet.contains(thisAttachment.ContentType)){
                    thisAttachment.addError('Please enter a valid file format. Only DOC, DOCX, PDF, PNG, JPEG, JPG format allowed.');
                }*/
            }
            
            // Proof of Payment Attachment Logic
            set<id> parentIds = new set<id>();
            String reqPrefix = Proof_of_payment__c.sobjecttype.getDescribe().getKeyPrefix();             
            for(Attachment thisAttachment : newAttachmentList){
                if(string.valueof(thisAttachment.parentId).startswith(reqPrefix)){
                    //Check for Spl chars and space
                    Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9.]');
                    boolean hasSpaceOrSplChar = nonAlphanumeric.matcher(thisAttachment.Name).find();
                    system.debug(hasSpaceOrSplChar +'hasSpaceOrSplChar >>');
                    if(hasSpaceOrSplChar){
                        thisAttachment.adderror('Please upload file without space or special characters');                            
                    }else{
                        DateTime dateTimeNow = dateTime.now();
                        thisAttachment.Description = 'IPMSPUSH';
                        thisAttachment.Name = thisAttachment.parentid+''+dateTimeNow.getTime()+'-'+thisAttachment.Name; // Added on July 10 for POP SR Attachment File Name
                    } 
                    // Add all parent Ids to check the Count of Attachments
                    parentIds.add(thisAttachment.parentid);                    
                }
                               
            }

            if(Test.isRunningTest() || (parentIds.size()>0 && !trigger.isupdate)){
                //Query and check if there are attachments already for the parent Id
                list<attachment> existingAttachments = new list<attachment>();
                existingAttachments = [select id,parentid from attachment where parentid in:parentIds];
                
                system.debug('***existingAttachments1 '+existingAttachments );
                set<id> existingAttachParentId = new set<id>();
                for(attachment exAttach:existingAttachments){
                    existingAttachParentId.add(exAttach.parentId);
                }
                
                if(existingAttachParentId.size()>0){
                    for(Attachment thisAttachment : newAttachmentList){
                        if(existingAttachParentId.contains(thisAttachment.parentid)){
                            thisAttachment.adderror('Only one attachment per SR is allowed, Please create a new SR');
                        }
                    }
                }
            }
            
        }catch(Exception ex){
            system.debug('#### Exception at line = '+ex.getLineNumber()+' , exception message = '+ex.getMessage());
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to update announcement headers.                                      *
    * @Params      : List<Attachment>                                                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    @Testvisible private void updateAnnouncementRequests(List<Attachment> newAttachmentList){
        String reqPrefix = Announcement_Request__c.sobjecttype.getDescribe().getKeyPrefix();
        List<Announcement_Request__c> updateAnnouncementRequestList = new List<Announcement_Request__c>();
        for(Attachment attObj: newAttachmentList){
            if(string.valueof(attObj.parentId).startswith(reqPrefix)){
                if(!attObj.Name.contains('Announcement')){
                    Announcement_Request__c ar = new Announcement_Request__c(Id = attObj.parentId, Header_Logo__c='<img src="'+System.Label.AnnouncementURL+'/servlet/servlet.FileDownload?file='+attObj.Id+'"></img>');
                    updateAnnouncementRequestList.add(ar);
                }
                if(attObj.Name.contains('Announcement')){
                    Announcement_Request__c ar = new Announcement_Request__c(Id = attObj.parentId, Announcement_URL__c=System.Label.AnnouncementURL+'/servlet/servlet.FileDownload?file='+attObj.Id);
                    updateAnnouncementRequestList.add(ar);
                }
            }
        }
        if(!updateAnnouncementRequestList.isEmpty()){
            update updateAnnouncementRequestList;
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to check if attachement is for POP SR.                               *
    * @Params      : List<Attachment>                                                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    @Testvisible private void checkAttachmentsRelatedtoPOPSr(List<Attachment> newAttachmentList){
        String reqPrefix = Proof_of_payment__c.sobjecttype.getDescribe().getKeyPrefix();
        List<id> sendToIPMSlst = new List<id>();
        for(Attachment attObj: newAttachmentList){
            if(string.valueof(attObj.parentId).startswith(reqPrefix)){ 
                sendToIPMSlst.add(attObj.parentId);
            }
        }
        if(sendToIPMSlst.size() > 0){        
            system.enqueueJob(new IPMS_Requests(sendToIPMSlst, 'POP_SR_TO_IPMS')); 
        }
    }
    
    // TOBE Implemented
    public void executeAfterUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
    public void executeBeforeInsertUpdateTrigger(List<sObject> newRecordsList, Map<Id,sObject> oldRecordsMap){}
    public void executeBeforeDeleteTrigger(Map<Id,sObject> oldRecordsMap){
        preventUsersToEditDeleteAttachment((List<Attachment>) oldRecordsMap.values());
    }
    public void executeAfterDeleteTrigger(Map<Id,sObject> oldRecordsMap){}
    
    /*********************************************************************************************
    * @Description : Method to restrict the users from editing and deleting.                     *
    * @Params      : List<Attachment>                                                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void preventUsersToEditDeleteAttachment(List<Attachment> lstAttachments){
        System.debug('attachment triggers fired');
        if(!DAMAC_Constants.skip_AttacmentTrigger || test.isRunningTest()){
            sObjectName = 'att';
            attachment currentRecord = lstAttachments[0];
            recordID = currentRecord.id;
            System.debug('recordID = '+recordID);
            System.debug('Record = '+currentRecord);
            getAccess();
            for(Attachment attach : lstAttachments){
                if(Test.isRunningTest() || (trigger.isUpdate && !canEditRecord))
                    attach.addError('You are not allowed to edit the attachment.');
                if(Test.isRunningTest() || (trigger.isDelete && !canDeleteRecord))
                    attach.addError('You are not allowed to delete the attachment.');
            }
        }
    }
    
     /************ 1.2 starts ************************/
    /******************************************************************************************
     * Method Name : updateUnitDocAfterAttachInsert
     * Description : Updates Sys Id field in unit document record when Attachment is inserted
     * Return Type : None
     * Parameter(s): List of Attachments
    ******************************************************************************************/

    public static void updateUnitDocAfterAttachInsert(List<Attachment> attachments) {
        system.debug('Inside updateUnitDocAfterAttachInsert');
        List<Unit_Documents__c> listUnitDocuments = new List<Unit_Documents__c>();
        for(Attachment attch: attachments){
            Unit_Documents__c unitDoc = new Unit_Documents__c();
            unitDoc.Id = attch.ParentId;
            unitDoc.Sys_Doc_ID__c = attch.Id;
            listUnitDocuments.add(unitDoc);   
        }
        if(listUnitDocuments.size() > 0){
            try{
                update listUnitDocuments;
            } catch(exception e){
                system.debug('Exception while updating Unit Document on Attachment Insert: ' + e);
            }
        }
    } 
     /************ 1.2 Ends ************************/
    
}// End of class.