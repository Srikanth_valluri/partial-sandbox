/**********************************************************************************************************************
Description: This is the handler class for HDApp_createHandoverAppointments_API class
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   18-10-2020      | Shubham Suryawanshi | Uncommented Booking Unit check in createAppointment() method
***********************************************************************************************************************/

public without sharing class HDApp_createHandoverAppointmentsHandler {
    public  static Id  devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
    public static map<Id, Booking_Unit__c> mapBookingUnit = new map<Id, Booking_Unit__c>(); 
    public static List<HDApp_getAppointmentDetailsHandler.AppointmentWrapper> handlerWrapper = new List<HDApp_getAppointmentDetailsHandler.AppointmentWrapper>();

    public static final Map<Integer, String> mapCalenderMonth = new Map<Integer, String> {
    
        1 => 'Jan',
        2 => 'Feb',
        3 => 'Mar',
        4 => 'Apr',
        5 => 'May',
        6 => 'Jun',
        7 => 'Jul',
        8 => 'Aug',
        9 => 'Sep',
        10 => 'Oct',
        11 => 'Nov',
        12 => 'Dec'

    };


    public static appointmentHandler_Wrapper createAppointment(String accountId, String strSelectedBookingUnits, String strSelectedDate, String strSelectedProcess, String strSelectedSubProcess, String appointmentSlotId, String inspectionBy) {

        appointmentHandler_Wrapper returnedWrapper = new appointmentHandler_Wrapper();
        

        // If appointments are already booked for this unit, then display message
        if((strSelectedProcess == '01-Handover' || strSelectedProcess == 'Handover')
               /*&& String.isNotBlank( strSelectedBookingUnits )*/
                && (strSelectedSubProcess == 'Unit Viewing'
                    || strSelectedSubProcess == 'Key Handover'
                    || strSelectedSubProcess == 'Unit Viewing or Key Handover')) {
            List<Calling_List__c> lstCl = [ Select Id
                                                  , RecordTypeId
                                                  , Appointment_Date__c
                                                  , Appointment_Status__c
                                                  , Calling_List__c
                                                  , Sub_Purpose__c
                                            From Calling_List__c
                                            Where RecordTypeId =: devRecordTypeId
                                            AND (Appointment_Status__c != 'Rejected'
                                                 AND Appointment_Status__c != 'Cancelled')
                                            AND Appointment_Date__c > TODAY
                                            AND Booking_Unit__c =: strSelectedBookingUnits     //Uncommented this for - 'Appointment already available for this Unit, please cancel existing appointment if want to reschedule same.' issue  
                                            AND Sub_Purpose__c = :strSelectedSubProcess
                                            AND Service_type__c =: (strSelectedProcess.contains('-')
                                                                    ? strSelectedProcess.substringAfter('-')
                                                                    : strSelectedProcess) ];
            System.debug('--- lstCl --- : '+lstCl);
            if (lstCl != null && lstCl.size() > 0) {
                // Append the apppointment SF ids to error message for internal use - Requested by Charith - 29/08/2020
                String callingListToCancel = 'Ids--->';
                for(Calling_List__c callingListInstance: lstCl) {
                    callingListToCancel = callingListToCancel + ',' + callingListInstance.Id; 
                    System.debug('callingListInstance.Id'+callingListInstance.Id);   
                }
                System.debug('callingListToCancel'+callingListToCancel); 
                String errorMsg= 'Appointment already available for this Unit, please cancel'
                           //+' existing appointment if want to reschedule same.' +callingListToCancel;
                            +' existing appointment if want to reschedule same.';
                appointmentHandler_Wrapper objwrap = new appointmentHandler_Wrapper(null, null, null,null,null,errorMsg,null,null,null);
                returnedWrapper = objwrap;
                return returnedWrapper;
            }
        }//If appointments are not booked for that 
        else if( String.isNotBlank(strSelectedDate) && String.isNotBlank( strSelectedBookingUnits )
                            && String.isNotBlank( strSelectedProcess )
                            && strSelectedProcess != 'Handover') {
            Date objSelectedDate = Date.valueOf(formatDate(strSelectedDate));
            List<Calling_List__c> lstCL = [ SELECT Id
                                            FROM Calling_List__c
                                            WHERE RecordTypeId =: devRecordTypeId
                                            AND (Appointment_Status__c != 'Rejected'
                                                 AND Appointment_Status__c != 'Cancelled')
                                            AND Appointment_Date__c =: objSelectedDate
                                            AND Service_type__c =: (strSelectedProcess.contains('-') ? strSelectedProcess.substringAfter('-') : strSelectedProcess)
                                            AND Booking_Unit__c =: strSelectedBookingUnits ];
            System.debug('--- lstCL --- : '+lstCL);
            if (lstCL.size() > 0) {
                appointmentHandler_Wrapper objwrap = new appointmentHandler_Wrapper(null, null, null,null,null,String.format(Label.Error_Message_For_Multiple_Calling_List,
                                new List<String>{strSelectedProcess, formatDate(strSelectedDate)}), null,null,null);
                returnedWrapper = objwrap;
                return returnedWrapper;
            }
        }
        //strSelectedDate = dateFormat(strSelectedDate);
        system.debug('--- strSelectedDate ---:'+strSelectedDate);

        // Retrieving Appointment slots results in Handler wrapper for the wrapper parameter of createAppointments function
        
        handlerWrapper = HDApp_getAppointmentDetailsHandler.getAvailableAppointments(accountId, strSelectedBookingUnits, strSelectedDate, strSelectedSubProcess, strSelectedProcess);
        System.debug('handlerWrapper'+handlerWrapper);
        
        //To set the flag in wrapper as true, so it would create Calling list in further logic
        for(HDApp_getAppointmentDetailsHandler.AppointmentWrapper wrapperInstance: handlerWrapper){
            wrapperInstance.isSelected = true;
        }
        System.debug('handlerWrapper'+handlerWrapper);
        
        Account objAcc = new Account();
        System.debug('--- accountId --- : '+accountId);
        if(string.isNotBlank(accountId)) {
            List<Account> accList = [ SELECT Id
                            , name 
                            , isPersonAccount
                            , Email__pc
                        FROM Account
                        WHERE id = :accountId];
            System.debug('accList'+accList);
            if(accList!=null && accList.size()>0){
                objAcc.id = accList[0].Id;
                objAcc = accList[0];    
            }
        }
        System.debug('--- objAcc --- : '+objAcc);

        list<Calling_List__c> callingList = new list<Calling_List__c>();
        if( handlerWrapper != NULL && handlerWrapper.size() > 0
                    && String.isNotBlank(strSelectedBookingUnits)
                    && String.isNotBlank(strSelectedProcess) && String.isNotBlank(strSelectedDate)
                    && objAcc != NULL ) {
            callingList = createAppointments(handlerWrapper,
                               (String.isNotBlank(strSelectedProcess) ? strSelectedProcess : ''),
                               strSelectedBookingUnits,
                               objAcc,
                               (String.isNotBlank(strSelectedSubProcess)
                                        ? strSelectedSubProcess : ''),
                               (strSelectedProcess.contains('-')
                                        ? strSelectedProcess.substringAfter('-')
                                        : strSelectedProcess),
                               strSelectedDate,
                               //false
                               appointmentSlotId, inspectionBy);
            System.debug('callingList---->'+callingList);
        }
        appointmentHandler_Wrapper objwrap = new appointmentHandler_Wrapper();
        for(Calling_List__c callingListInstance: callingList){
            objwrap.calling_list_id = callingListInstance.Id;
            //objwrap.slot = callingListInstance.Appointment_Slot__c;
            System.debug('callingListInstance-> ' + callingListInstance);
            //objwrap.slot = HDApp_getHandoverCurrentStatusHandler.get12HrsFormattedTime(callingListInstance.Appointment__r.Slots__c);
            
            objwrap.slot = HDApp_getHandoverCurrentStatusHandler.get12HrsFormattedTime(callingListInstance.Appointment_Slot__c);
            System.debug('callingList Name----->'+callingListInstance.Name);
            objwrap.bookingNumber = callingListInstance.Name;
            objwrap.bookingUnitId = callingListInstance.Booking_Unit__c;
            //objwrap.purpose = strSelectedProcess;
            objwrap.purpose =  strSelectedSubProcess == 'Unit Viewing' ? 'Unit Inspection' : 
                                strSelectedSubProcess == 'Key Handover' ? 'Key Handover' :
                                    strSelectedSubProcess == 'Documentation' ? 'Handover Documentation' : null;
            //objwrap.appointmentDate = strSelectedDate;
            //objwrap.appointmentMessage = 'Dear Customer, your appointment for ' + strSelectedSubProcess
            //                    + ' on '+ strSelectedDate +' has been requested at '
            //                    + objwrap.slot +'. You will get notification on confirmation'
            //                    +' of your appointment';
            String strDate =  getFormattedDateWithMonth(strSelectedDate);
            System.debug('strDate: ' + strDate);

            objwrap.appointmentDate = strDate;
            objwrap.appointmentMessage = 'Dear Customer, your appointment for ' + objwrap.purpose
                                + ' on '+ strDate +' has been requested at '
                                + objwrap.slot +'. You will get notification on confirmation'
                                +' of your appointment';

            if(callingListInstance.Appointment_Status__c == 'Requested'){
                objwrap.status = 'Submitted';
            }
            else{
                objwrap.status = callingListInstance.Appointment_Status__c;    
            }
            //if they asked to return bookedby in response:
            //objwrap.bookedBy = callingListInstance.Customer_Name__c;
        }
        System.debug('objwrap'+objwrap);
        System.debug('returnedWrapper'+returnedWrapper);

        returnedWrapper = objwrap;
        return returnedWrapper;
    }
    
    // Logic to create Appointments, Events and send email
    public static list<Calling_List__c> createAppointments (list<HDApp_getAppointmentDetailsHandler.AppointmentWrapper> handlerWrapper,
                                    String purpose,
                                    String unitName,
                                    Account objAccount,
                                    String subProcessName,
                                    String processName,
                                    String selectedDate, 
                                    String appointmentSlotId, String inspectionBy) {
        mapBookingUnit = HDApp_getAppointmentDetailsHandler.getBookingIds(unitName);
        system.debug('!!!!!!!!mapBookingUnit'+mapBookingUnit);
        Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().
                                get('Appointment Scheduling').getRecordTypeId();
        String hoTeamEmail, strProjectName;
        if (mapBookingUnit.containsKey(unitName) && mapBookingUnit.get(unitName) != null) {
            strProjectName = mapBookingUnit.get(unitName).Unit_Name__c.substringBefore('/');
        }
        list<Calling_List__c> lstAppointmentCL = new list<Calling_List__c>();
        list<Event> lstEvent = new list<Event>();
        if(String.isNotBlank(strProjectName)) {
            Location__c objLoc = [ SELECT Id
                                        , Name
                                        , Handover_Team_notification_Email__c
                                   FROM Location__c
                                   WHERE Name = :strProjectName ];
            hoTeamEmail = objLoc.Handover_Team_notification_Email__c;
        }
        system.debug('!!!!!!!!handlerWrapper'+handlerWrapper);
        // Logic to create the Appointment Calling List records
        for (HDApp_getAppointmentDetailsHandler.AppointmentWrapper objWrap : handlerWrapper) {
            if (objWrap.isSelected == true && objWrap.objApp.Id == appointmentSlotId) {
                 Calling_List__c objCallList = new Calling_List__c();
                 objCallList.Account__c = objAccount.Id;
                 objCallList.IsHideFalse_Relationship__c = objAccount.Id;
                 objCallList.Customer_Name__c = objAccount.Name;
                 if( String.isNotBlank(purpose)) {
                    objCallList.Remarks__c = purpose;
                 }
                 if( String.isNotBlank( unitName ) ) {
                    if (mapBookingUnit.containsKey(unitName)
                                && mapBookingUnit.get(unitName) != null){
                        objCallList.Booking_Unit__c = mapBookingUnit.get(unitName).Id;
                        objCallList.Registration_ID__c = mapBookingUnit.get(
                                                        unitName).Registration_ID__c;
                    }
                }
                objCallList.RecordTypeId = devRecordTypeId;
                /*if ( subProcessName == 'Unit Viewing' || subProcessName == 'Key Handover'
                    || subProcessName == 'Unit Viewing or Key Handover') {
                    List<Id> lstOwnerIds = getQueueIds('Handover Queue');
                    objCallList.ownerId = lstOwnerIds[0];
                    //objCallList.Appointment_owner_assigned__c = true;
                } else*/ 
                
                if (objWrap.objApp.Assigned_CRE__c != null) {
                    objCallList.ownerId = objWrap.objApp.Assigned_CRE__c;
                    objCallList.Assigned_CRE__c = objWrap.objApp.Assigned_CRE__c;
                } else if (objWrap.objApp.Group_Name__c != null) {
                    List<Id> lstOwnerIds = getQueueIds(objWrap.objApp.Group_Name__c);
                    if (!lstOwnerIds.isEmpty()) {
                        objCallList.ownerId = lstOwnerIds[0];
                    }
                }
                objCallList.Service_Type__c = processName;
                objCallList.Sub_Purpose__c = subProcessName;
                objCallList.Service_Type1__c = processName;
                objCallList.Sub_Process1__c  = subProcessName;
                objCallList.Amount_Due__c = inspectionBy;
                objCallList.Publish_Flag__c = true;   //For CL created from Damac Living App; IsCreatedFromHelloDamacApp
                System.debug('selectedDate--->'+selectedDate);
                //Commented the logic of converting date from String. Our date is already in expected format
                //objCallList.Appointment_Date__c =  generateDateFromString(selectedDate);
                objCallList.Appointment_Date__c  = Date.valueOf(selectedDate);
                System.debug('Appointment Date on calling list--->'+objCallList.Appointment_Date__c);

                if (processName == 'Handover' && (subProcessName == 'Unit Viewing'
                            || subProcessName == 'Unit Viewing or Key Handover'
                            || subProcessName == 'Key Handover')) {
                    objCallList.Appointment_Status__c = 'Requested';
                    objCallList.Handover_Team_Email__c = hoTeamEmail;
                } else {
                    objCallList.Appointment_Status__c = 'Confirmed';
                }

                List<Calling_List__c> lstExistingHandoverCL = getExistingHOCL(processName,
                                                                               subProcessName,
                                                                               unitName,
                                                                               objAccount.Id);
                if ( !lstExistingHandoverCL.isEmpty()) {
                    objCallList.Calling_List__c = lstExistingHandoverCL[0].Id;
                    String ownerId = lstExistingHandoverCL[0].ownerId;
                    if ( ownerId.startsWith('005') && lstExistingHandoverCL[0].owner.Email != '') {
                        objCallList.HO_Calling_list_Owner__c = lstExistingHandoverCL[0].owner.Email;
                    }
                }
                
                String Slot =  String.isNotBlank(objWrap.objApp.Slots__c.substringBefore('-'))?objWrap.objApp.Slots__c.substringBefore('-'):'';
                String EndTime = String.isNotBlank(objWrap.objApp.Slots__c.substringAfter('-'))?objWrap.objApp.Slots__c.substringAfter('-'):'';
                objCallList.Appointment_Slot__c = objWrap.objApp.Slots__c;
                if (objAccount.isPersonAccount == true) {
                    objCallList.Account_Email__c = objAccount.Email__pc;
                } else {
                    objCallList.Account_Email__c = objAccount.Email__c;
                }
                objCallList.Appointment__c = objWrap.objApp.Id;

                system.debug('!!!!!!!!!selectedDate'+selectedDate);
                List<String> parts = selectedDate.split('-');
                system.debug('parts'+parts);
                Integer intDate, intMonth, intYear;
                if (!parts.isEmpty() && parts.size() == 3){
                    intDate = integer.valueOf(parts[2]);
                    intMonth = integer.valueOf(parts[1]);
                    intYear = integer.valueOf(parts[0]);
                }
                
                Datetime StartDT, EndDT;
                if (intYear != null && intMonth != null && intDate != null) {
                    StartDT = datetime.newInstance(intYear, intMonth, intDate,
                                integer.Valueof(Slot.substringBefore(':')),
                                Slot.substringAfter(':') == '00'
                                    ? integer.Valueof(Slot.substringAfter(':')) : 00, 00);
                    system.debug('!!!!StartDT'+StartDT);

                   EndDT = datetime.newInstance(intYear, intMonth, intDate,
                                integer.Valueof((EndTime.substringBefore(':')).deleteWhitespace()),
                                EndTime.substringAfter(':') != '00'
                                    ? integer.Valueof(EndTime.substringAfter(':')) : 00, 00);
                   system.debug('!!!!EndDT'+EndDT);
                   
               }
               objCallList.Appointment_Start_DateTime__c = StartDT;
               objCallList.Appointment_End_DateTime__c = EndDT;
               lstAppointmentCL.add(objCallList);

              // Logic to create Events for Handover Appointments
               if(!lstAppointmentCL.isEmpty()) {
                      String callListOwnerId = objCallList.ownerId;
                      System.debug('callListOwnerId'+callListOwnerId);
                      Event objEvent = new Event();
                      if (processName == 'Handover' && (subProcessName == 'Unit Viewing'
                                || subProcessName == 'Unit Viewing or Key Handover'
                                || subProcessName == 'Key Handover')) {
                          objEvent.status__c = 'Requested';
                      } else {
                          objEvent.status__c = 'Confirmed';
                      }
                      if(!string.isBlank(callListOwnerId) && callListOwnerId.startsWith('00G')){
                          List<Id> lstGMId = getQueueMemberIds(callListOwnerId);
                            System.debug('lstGMId'+lstGMId);
                          
                            String userName = System.Label.HDApp_OwnerForDigitalHandover;
                            System.debug('userName'+userName);
                            List<User> userList = [SELECT Id
                                                , Name 
                                                FROM 
                                                User 
                                                WHERE Id=:userName];
                            System.debug('userList'+userList);
                            objEvent.OwnerId = userList[0].Id;
                            System.debug('objEvent.OwnerId'+objEvent.OwnerId);
                              
                        
                            objEvent.Type = 'Meeting';
                            objEvent.IsVisibleInSelfService = true;
                            objEvent.StartDateTime = StartDT;
                            objEvent.EndDateTime = EndDT;
                            if (mapBookingUnit.containsKey(unitName) && mapBookingUnit.get(unitName) != null){
                                objEvent.Subject = processName + ' : ' + subProcessName +
                                                        ': Appointment Scheduled with Customer: '
                                                        +objAccount.Name + ' : ' + mapBookingUnit.get(
                                                            unitName).Unit_Name__c;
                                objEvent.Description = processName + ' : ' + subProcessName +
                                                            ': Appointment Scheduled with Customer: '+
                                                            objAccount.Name + ' : ' + mapBookingUnit.get(
                                                                unitName).Unit_Name__c;
                            }
                            System.debug('objEvent'+objEvent);
                            lstEvent.add(objEvent);
                            System.debug('lstEvent'+lstEvent);
                      }
                }// end of if for Events
            }
        }// end of for lstAppointmentWrapper

        System.debug('--- lstAppointmentCL --- : '+lstAppointmentCL);
        if (lstAppointmentCL != null && lstAppointmentCL.size() > 0) {
            //if(!Test.isRunningTest()) {
                insert lstAppointmentCL;
            //}
            
            System.debug('lstAppointmentCL---->'+lstAppointmentCL);
            System.debug('lstAppointmentCL.SIZE---->'+lstAppointmentCL.size());
            //System.debug('lstAppointmentCL[0].id---->'+lstAppointmentCL[0].id);
        }
        if( !lstAppointmentCL.isEmpty()) {
            for(Event eventObj: lstEvent){
                for(Integer i=0; i<lstAppointmentCL.size(); i++) {
                    System.debug('lstAppointmentCL[i].Id'+lstAppointmentCL[i].Id);
                    eventObj.WhatId = lstAppointmentCL[i].Id;
                    System.debug('eventObj.WhatId'+eventObj.WhatId);
                }
                //lstEvent.add(eventObj);
            }
            system.debug('!!!!!!!lstEvent'+lstEvent);
            //if(!Test.isRunningTest()) {
                insert lstEvent;
            //}
            
        }

        // Logic to send emails to Customer
        /*
        if ( lstAppointmentCL != null && processName != 'Handover'
                    && (subProcessName != 'Unit Viewing' || subProcessName != 'Key Handover'
                            || subProcessName != 'Unit Viewing or Key Handover'
                            || subProcessName != 'Documentation' )) {
            ScheduleAppointment objScheduleAppointmentr = new ScheduleAppointment();
            objScheduleAppointmentr.sendEmail(lstAppointmentCL);
        } else */

        /* Commented now as Charith has asked to use Email, sms designed by front end part  - Jyoti 31/08/2020
        if( lstAppointmentCL != null && processName == 'Handover'
                    && subProcessName == 'Documentation' ) {
            List<Calling_List__c> lstCal = getCallingList(lstAppointmentCL[0].id);
            if (lstCal[0].Account_Email__c != null) {
                //HDApp_digitalHandoverEmailUtility handoverEmailUtility = new HDApp_digitalHandoverEmailUtility();
                HDApp_digitalHandoverEmailUtility.sendEmail(lstCal, 'Documentation_Appointments_Confirmed');
                String strMessageBody='Your appointment for <Purpose> at <Project Name>, <Unit'
                                        +' Number>, on <dd/mm/yyyy> at <hh:mm> (UAE time) is'
                                        +' Confirmed. A confirmation email has been sent to your'
                                        +' registered e-mail address. For assistance call'
                                        +' +971 4 2375000 or write to atyourservice@damacproperties.com';
                HDApp_digitalHandoverEmailUtility.sendMessage(lstCal, strMessageBody);
            }
        } else if( lstAppointmentCL != null && processName == 'Handover'
            && subProcessName == 'Unit Viewing' ) {
            List<Calling_List__c> lstCal = getCallingList(lstAppointmentCL[0].id);
            if (lstCal[0].Appointment_Status__c == 'Requested' && lstCal[0].Account_Email__c != null) {
                String strMessageBody='You have requested for an appointment for <Purpose> at '
                                        +'<Project Name>, <Unit Number>, on <dd/mm/yyyy> at <hh:mm>'
                                        +' (UAE time). Please wait for the confirmation e-mail and'
                                        +' SMS before visiting us. For assistance, call '
                                        +'+971 4 2375000 or write to atyourservice@damacproperties.com';
                HDApp_digitalHandoverEmailUtility.sendEmail(lstCal, 'Handover_Unit_Viewing_Appointments_Requested');
                HDApp_digitalHandoverEmailUtility.sendMessage(lstCal, strMessageBody);
            }
        } else if( lstAppointmentCL != null && lstAppointmentCL.size() > 0 &&
                        processName == 'Handover' &&  subProcessName == 'Key Handover'){
            List<Calling_List__c> lstCal = getCallingList(lstAppointmentCL[0].id);
            if (lstCal[0].Appointment_Status__c == 'Requested' && lstCal[0].Account_Email__c != null) {
                String strMessageBody='You have requested for an appointment for <Purpose> at '
                                        +'<Project Name>, <Unit Number>, on <dd/mm/yyyy> at <hh:mm>'
                                        +' (UAE time). Please wait for the confirmation e-mail and '
                                        +' SMS before visiting us. For assistance, call '
                                        +' +971 4 2375000 or write to atyourservice@damacproperties.com';
                HDApp_digitalHandoverEmailUtility.sendEmail(lstCal, 'Handover_Key_Handover_Appointments_Requested');
                HDApp_digitalHandoverEmailUtility.sendMessage(lstCal, strMessageBody);
            }
        }*/
        return lstAppointmentCL;
    }// end of method createAppointments
    
    // method to get Queue Ids for queues
    public static List<Id> getQueueIds (String queueName){
        List<Id> lstGroupId = new List<Id>();
        for (Group objGroup : [ SELECT Id
                                      , Name
                                      , Type
                                FROM Group
                                WHERE Type = 'Queue'
                                AND Name =: queueName ]){
            lstGroupId.add(objGroup.Id);
        }
        return lstGroupId;
    }// end of method getQueueIds

    //Method to get User as QueueMembers from Queue
    public static List<Id> getQueueMemberIds (Id groupId){
        List<Id> lstGMId = new List<Id>();
        for (GroupMember objGM : [ SELECT Id
                                        , GroupId
                                        , UserOrGroupId
                                   FROM GroupMember
                                   WHERE GroupId =: groupId ]){
            String GmId = objGM.UserOrGroupId;
            if (GmId.StartsWith('005')){
                lstGMId.add(objGM.UserOrGroupId);
            }
        }
        return lstGMId;
    }// end of method getQueueMembers

    // method to get all Calling List information
    public static List<Calling_List__c> getCallingList(Id callingListId) {
        List<Calling_List__c> lstCallingList = new List<Calling_List__c>();

        for (Calling_List__c objCallList : [ SELECT id
                                                    , Name
                                                    , OwnerID
                                                    , Account__c
                                                    , Appointment_Date__c
                                                    , Account__r.Name
                                                    , Account__r.Email__c
                                                    , Account__r.PersonEmail
                                                    , Appointment_Slot__c
                                                    , Account_Name_for_Walk_In__c
                                                    , RecordTypeId
                                                    , RecordType.DeveloperName
                                                    , Assigned_CRE__c
                                                    , Service_Type__c
                                                    , Assigned_CRE__r.id
                                                    , CRE_Email__c
                                                    , Account_Email__c
                                                    , Customer_Name__c
                                                    , Handover_Team_Email__c
                                                    , HO_Calling_list_Owner__c
                                                    , Appointment_Status__c
                                                    , Sub_Purpose__c
                                                    , Booking_Unit_Name__c
                                                    , Booking_Unit__r.Property_Name__c
                                                    , Booking_Unit__r.Unit_Name__c
                                                    , Account__r.PersonMobilePhone
                                                    , Account__r.Mobile__c
                                                    , Account__r.Mobile_Phone_Encrypt__pc
                                                    , Account__r.isPersonAccount,Project__c
                                            FROM Calling_List__c
                                            WHERE id=: callingListId ]) {
            lstCallingList.add(objCallList);
        }
        return lstCallingList;
    }// end of method getCallingList

    //Method to check for Handover and Early Handover Calling Lists
    public static List<Calling_List__c> getExistingHOCL (String processName,
                                                  String subProcessName,
                                                  String unitName,
                                                  String strAccId) {
        List<Calling_List__c> lstHandoverCallingList = new List<Calling_List__c>();
        Id handoverCLRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName()
                                        .get('Handover Calling List').getRecordTypeId();
        Id earlyHandoverCLRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName()
                                            .get('Early Handover Calling List').getRecordTypeId();
        if((processName == 'Handover' && (subProcessName == 'Unit Viewing'
                    || subProcessName == 'Key Handover'
                    || subProcessName == 'Unit Viewing or Key Handover')
                    || processName == 'Early Handover')
                    && String.isNotBlank(unitName)) {
            lstHandoverCallingList = [ SELECT Id
                                             , RecordTypeId
                                             , ownerId
                                             , Owner.Email
                                             , Purpose_of_your_visit__c
                                             , Remarks__c
                                       FROM Calling_List__c
                                       WHERE (RecordTypeId =: handoverCLRecordTypeId
                                                OR RecordTypeId =: earlyHandoverCLRecordTypeId)
                                       AND Account__c =: strAccId 
                                       AND Booking_Unit__c =: unitName ORDER BY CreatedDate DESC LIMIT 1];
        }
        return lstHandoverCallingList;
    }// end of method getExistingHOCL

    //method to get Date from string format
    public static date generateDateFromString(String strDate){
        List<String> parts = strDate.split('-');
        System.debug('parts'+parts);
        system.debug('!!!!parts'+parts);
        if (parts.size() == 3){
            return date.newinstance(integer.valueOf(parts[2]), integer.valueOf(parts[0]),
                        integer.valueOf(parts[1]));
        } else {
            return null;
        }
    }// end of method generateDateFromString

    // Method to get Booking Unit and the location of its associated property
    public static List<Booking_Unit__c> GetBookingUnit(String regId) {

        System.debug('regId:: ' + regId);
        List<Booking_Unit__c> fetchBU = [SELECT id 
                                              , Unit_Name__c
                                              , Registration_ID__c
                                              , DLP_End_Date__c
                                              , DLP_Start_date__c
                                              , DLP_Validity__c
                                              , Handover_Status__c
                                              , Inventory__c
                                              , Inventory__r.Property__r.Latitude__c
                                              , Inventory__r.Property__r.Longitude__c
                                        FROM Booking_Unit__c
                                        WHERE Registration_ID__c =: regId];

        System.debug('fetchBU:: ' + fetchBU);
        return fetchBU;
    }

    // method used to format date to yyyy-MM-dd
    public static String formatDate( String dt ) {
        list<String> lstDateComponents;
        system.debug('== Date1 =='+dt );
        if( dt.contains('/') ) {
            lstDateComponents = dt.split('/');
        }
        else if( dt.contains('-') ) {
            lstDateComponents = dt.split('-');
        }
        system.debug('lstDateComponents'+lstDateComponents);
        Date objDateToFormat;
        if(lstDateComponents[2].length() == 4 ) {
            objDateToFormat = date.newinstance( Integer.valueOf( lstDateComponents[2] ),
                                    Integer.valueOf( lstDateComponents[1] ),
                                    Integer.valueOf( lstDateComponents[0] ));
        }
        else {
            objDateToFormat = Date.valueOf(dt);
        }
        system.debug('== Date2 =='+objDateToFormat );
        system.debug('>>Datetime'+DateTime.newInstance(objDateToFormat.year(),
                            objDateToFormat.month(),objDateToFormat.day()).format('yyyy-MM-dd'));
        return DateTime.newInstance(objDateToFormat.year(),objDateToFormat.month(),
                            objDateToFormat.day()).format('yyyy-MM-dd');
    }
    // method used to format date as 'mm/dd/yyyy'
    public static String dateFormat(String strSelectedDate) {
        system.debug('--- strSelectedDate ---:'+strSelectedDate);
        if(String.isNotBlank(strSelectedDate)) {
            List<String> parts = new List<String>();
            if( strSelectedDate.contains('/') ) {
                parts = strSelectedDate.split('/');
            }
            else if( strSelectedDate.contains('-') ) {
                parts = strSelectedDate.split('-');
            }
            Integer intDate, intMonth, intYear;
            if (!parts.isEmpty() && parts.size() == 3){
                intDate = integer.valueOf(parts[2]);
                intMonth = integer.valueOf(parts[1]);
                intYear = integer.valueOf(parts[0]);
            }
            Datetime dt = Datetime.newInstance(intYear, intMonth, intDate);
            strSelectedDate = dt.format('MM/dd/yyyy');
        }
        system.debug('--- strSelectedDate ---:'+strSelectedDate);
        return strSelectedDate;
    }
    public class appointmentHandler_Wrapper {
        public String calling_list_id;
        public String bookingNumber;
        public String purpose;
        public String appointmentDate;
        public String slot;
        public String appointmentMessage;
        public String bookingUnitId;
        public string status;
        //public String bookedBy;
        
        
        public appointmentHandler_Wrapper() {

        }

        public appointmentHandler_Wrapper( String appSFId,
                                   String bookingNum,
                                   String process,
                                   String selectedDate, 
                                   String appSlot, 
                                   String appMessage,
                                   String bookingUnit_Id, 
                                   String appStatus, String callingListId) {
            calling_list_id = appSFId;
            bookingNumber = bookingNum;
            purpose = process;
            appointmentDate = selectedDate;
            slot = appSlot;
            appointmentMessage = appMessage;
            bookingUnitId = bookingUnit_Id;
            status = appStatus;
            //bookedBy = accountName;
        }
    }

    public static String getFormattedDateWithMonth(String strDate) {
        System.debug('strDate: ' + strDate);
        if(String.isBlank(strDate)) {
            return null;
        }

        List<String> lstDateStr = strDate.split('-');
        String month = mapCalenderMonth.get(Integer.valueOf(lstDateStr[1]));
        System.debug('month:: ' + month);

        String finalDate = lstDateStr[2]+'-'+month+'-'+lstDateStr[0];
        System.debug('finalDate :: ' + finalDate);

        return finalDate;
    }
}