@isTest
private class SafetyRegulationsPageControllerTest {

    static testMethod void getCaseDetailsTest() {

        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id accountId = [ SELECT Id
                                , Contact.AccountId
                         FROM User
                         WHERE Id = :portalUser.Id ][0].Contact.AccountId;

        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
	    objAccount.party_ID__C = '1039032';
	    insert objAccount;

        NSIBPM__Service_Request__c objSr = new NSIBPM__Service_Request__c();
        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName()
                        .get('Deal').getRecordTypeId();
		objSr.NSIBPM__Customer__c = accountId;
		objSr.RecordTypeId = RecType;
		insert objSr;

        Booking__c  objBooking = new  Booking__c();
        objBooking.Account__c = accountId;
		objBooking.Deal_SR__c = objSr.id;
		insert objBooking;

        Booking_Unit__c objBu = new Booking_Unit__c();
        objBu.Booking__c = objBooking.id;
		objBu.Unit_Name__c = 'LSB/10/B1001';
		objBu.Owner__c = objAccount.id;
		objBu.Property_City__c = 'Dubai';
		objBu.Property_Name__c = 'DAMAC Hills';
		objBu.Handover_Flag__c = 'Y';
		objBu.Early_Handover__c = true;
		insert objBu;


        FM_Case__c objFmCase = new FM_Case__c();
        objFmCase.Account__c = accountId;
        objFmCase.Booking_Unit__c = objBu.id;
        insert objFmCase;

        PageReference currentPage = Page.SafetyRegulationsPage;
        Test.setCurrentPage(currentPage);
        ApexPages.currentPage().getParameters().put('CaseId', objFmCase.id);

        SafetyRegulationsPageController objSrp = new SafetyRegulationsPageController();
        Test.startTest();
		objSrp.caseId = objFmCase.id;
        objSrp.getCaseDetails();
        Test.stopTest();
    }

     static testMethod void getCaseDetailsTest2() {

        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id accountId = [ SELECT Id
                                , Contact.AccountId
                         FROM User
                         WHERE Id = :portalUser.Id ][0].Contact.AccountId;

        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.party_ID__C = '1039032';
        insert objAccount;

        NSIBPM__Service_Request__c objSr = new NSIBPM__Service_Request__c();
        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName()
                        .get('Deal').getRecordTypeId();
        objSr.NSIBPM__Customer__c = accountId;
        objSr.RecordTypeId = RecType;
        insert objSr;

        Booking__c  objBooking = new  Booking__c();
        objBooking.Account__c = accountId;
        objBooking.Deal_SR__c = objSr.id;
        insert objBooking;

        Booking_Unit__c objBu = new Booking_Unit__c();
        objBu.Booking__c = objBooking.id;
        objBu.Unit_Name__c = 'LSB/10/B1001';
        objBu.Owner__c = objAccount.id;
        objBu.Property_City__c = 'Dubai';
        objBu.Property_Name__c = 'Test';
        objBu.Handover_Flag__c = 'Y';
        objBu.Early_Handover__c = true;
        insert objBu;


        FM_Case__c objFmCase = new FM_Case__c();
        objFmCase.Account__c = accountId;
        objFmCase.Booking_Unit__c = objBu.id;
        insert objFmCase;

        PageReference currentPage = Page.SafetyRegulationsPage;
        Test.setCurrentPage(currentPage);
        ApexPages.currentPage().getParameters().put('CaseId', objFmCase.id);
        ApexPages.currentPage().getParameters().put('tenantName', 'test Name');

        SafetyRegulationsPageController objSrp = new SafetyRegulationsPageController();
        Test.startTest();
        objSrp.caseId = objFmCase.id;
        objSrp.getCaseDetails();
        Test.stopTest();
    }

    static testMethod void getCaseDetailsMoveInTest() {

        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id accountId = [ SELECT Id
                                , Contact.AccountId
                         FROM User
                         WHERE Id = :portalUser.Id ][0].Contact.AccountId;

        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.party_ID__C = '1039032';
        insert objAccount;

        NSIBPM__Service_Request__c objSr = new NSIBPM__Service_Request__c();
        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName()
                        .get('Deal').getRecordTypeId();
        objSr.NSIBPM__Customer__c = accountId;
        objSr.RecordTypeId = RecType;
        insert objSr;

        Booking__c  objBooking = new  Booking__c();
        objBooking.Account__c = accountId;
        objBooking.Deal_SR__c = objSr.id;
        insert objBooking;

        Booking_Unit__c objBu = new Booking_Unit__c();
        objBu.Booking__c = objBooking.id;
        objBu.Unit_Name__c = 'LSB/10/B1001';
        objBu.Owner__c = objAccount.id;
        objBu.Property_City__c = 'Dubai';
        objBu.Property_Name__c = 'Test';
        objBu.Handover_Flag__c = 'Y';
        objBu.Early_Handover__c = true;
        insert objBu;


        FM_Case__c objFmCase = new FM_Case__c();
        objFmCase.Account__c = accountId;
        objFmCase.Booking_Unit__c = objBu.id;
        objFMCase.Request_Type_DeveloperName__c = 'Move_in_Request';
        insert objFmCase;

        PageReference currentPage = Page.SafetyRegulationsPage;
        Test.setCurrentPage(currentPage);
        ApexPages.currentPage().getParameters().put('CaseId', objFmCase.id);

        SafetyRegulationsPageController objSrp = new SafetyRegulationsPageController();
        Test.startTest();
        objSrp.caseId = objFmCase.id;
        objSrp.getCaseDetails();
        Test.stopTest();
    }

    static testMethod void getCaseDetailsMoveInTenantTest() {

      User portalUser = CommunityTestDataFactory.createPortalUser();
      Id accountId = [  SELECT Id
                               , Contact.AccountId
                        FROM User
                        WHERE Id = :portalUser.Id ][0].Contact.AccountId;

      Account objAccount = new Account();
      objAccount.Name = 'Test Account';
      objAccount.party_ID__C = '1039032';
      insert objAccount;

      NSIBPM__Service_Request__c objSr = new NSIBPM__Service_Request__c();
      Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName()
                        .get('Deal').getRecordTypeId();
      objSr.NSIBPM__Customer__c = accountId;
      objSr.RecordTypeId = RecType;
      insert objSr;

      Booking__c  objBooking = new  Booking__c();
      objBooking.Account__c = accountId;
      objBooking.Deal_SR__c = objSr.id;
      insert objBooking;

      Booking_Unit__c objBu = new Booking_Unit__c();
      objBu.Booking__c = objBooking.id;
      objBu.Unit_Name__c = 'LSB/10/B1001';
      objBu.Owner__c = objAccount.id;
      objBu.Property_City__c = 'Dubai';
      objBu.Property_Name__c = 'Test';
      objBu.Handover_Flag__c = 'Y';
      objBu.Early_Handover__c = true;
      insert objBu;


      FM_Case__c objFmCase = new FM_Case__c();
      objFmCase.Tenant__c = accountId;
      objFmCase.Booking_Unit__c = objBu.id;
      objFMCase.Request_Type_DeveloperName__c = 'Move_in_Request_T';
      insert objFmCase;

      PageReference currentPage = Page.SafetyRegulationsPage;
      Test.setCurrentPage(currentPage);
      ApexPages.currentPage().getParameters().put('CaseId', objFmCase.id);

      SafetyRegulationsPageController objSrp = new SafetyRegulationsPageController();
      Test.startTest();
      objSrp.caseId = objFmCase.id;
      objSrp.getCaseDetails();
      Test.stopTest();
  }
}