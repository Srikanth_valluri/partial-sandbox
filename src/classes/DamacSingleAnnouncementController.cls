public with sharing class DamacSingleAnnouncementController {
	public DamacSingleAnnouncementController(){
		
	}
	public List<Notification__c> getSingleNotification(){
		      List<Notification__c> listNot = new  List<Notification__c>();
      if(ApexPages.currentPage().getParameters().containsKey('Id') &&
      null != ApexPages.currentPage().getParameters().get('Id')){
      String NId = ApexPages.currentPage().getParameters().get('Id');
      System.debug('...NId..'+NId);
      listNot =[Select Id,
                       Title__c,
                       Description__c
                from Notification__c
                where id=: NId];
    }
    System.debug('...listNot...'+listNot);
    return listNot;
	}
	   
}