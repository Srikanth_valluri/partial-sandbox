/**
 *==================================================================================================
 * Class Name      : NOCVisaControllerTest
 * Description     : 
 * Developer Name  : Pratiksha Narvekar
 * Created Date    : 29-12-2017
 *==================================================================================================
 */
//pratiksha narvekar
@isTest
private class SubstractNumberOfNOCTest {
    private static testMethod void test() {
        
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('NOC For Visa').getRecordTypeId();
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
     

         
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
        
        Case objCase = new Case();
        objCase.status = 'Not started';
        objCase.RecordTypeId = CaseRecordTypeId;
        objCase.AccountID =Acc.ID;
        objCase.Booking_Unit__c = BU.ID;
       
        insert objCase;
        
        SR_Booking_Unit__c SrbookingUnit = new SR_Booking_Unit__c();
        SrbookingUnit.Agreement_Status__c = 'Test';
        SrbookingUnit.Booking_Unit__c = BU.Id;
        SrbookingUnit.Case__c = objCase.Id;
        SrbookingUnit.Deal_Status__c = 'Test';
        insert SrbookingUnit;
        
        SR_Attachments__c SRattch =  new SR_Attachments__c();
        SRattch.Account__c = Acc.Id;
        SRattch.Booking_Unit__c =BU.Id;
         SRattch.Type__c ='NOC For VISA';
        SRattch.Case__c = objCase.Id;
        SRattch.isValid__c = true;
        //SRattch.Error_Message__c = 'Error';
        
        insert SRattch;
        
        SubstractNumberOfNOC objSubstractNumberOfNOC = new SubstractNumberOfNOC();

        
        map<ID,Case> newmapcase = new map<ID,Case>();
         newmapcase.put(objCase.ID,objCase);
         objCase.status = 'Cancelled';
        update objCase;
        
        map<ID,Case> oldmapcase = new map<ID,Case>();
       
        oldmapcase.put(objCase.ID,objCase);
        objSubstractNumberOfNOC.OnAfterUpdate(newmapcase,oldmapcase);
	}



}