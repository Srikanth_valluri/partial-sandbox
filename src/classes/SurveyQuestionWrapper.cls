global with sharing class SurveyQuestionWrapper {

    public String               name ;
    public String               id ;
    public String               question ;
    public String               orderNumber ;
    public String               choices  ;
    public String               selectedOption ;
    public List<String>         selectedOptions ;
    public Map<String,String>   singleOptions  ;
    public Map<String,String>   multiOptions  ;
    public Boolean              required  ;
    public String               questionType ;
    public String               surveyName ;
    public Boolean              renderFreeText ;
    public Boolean              renderSelectRadio ;
    public Boolean              renderSelectCheckboxes ;
    public Boolean              renderSelectRow ;
    public Boolean              renderPicklist ;
    public Boolean              renderRatings ;
    public List<String>         responses;
    public Map<String,String>   rowOptions ;
    public String               noOfRowsForTextArea ;
    public String               choiceForAdditionalTextbox ;
    public String               additionalTextboxNumber ;
    public String               additionalResponse ;
    public List<SurveyQuestionWrapper> subQuestions ;
    public Survey_Question_CRM__c   surveyQuestion ;
    public Boolean              isSubQuestion ;

    // Fills up the question object
     //  param:    Survey_Question_CRM__c
     //
    public SurveyQuestionWrapper(Survey_Question_CRM__c sq, List<SurveyQuestionWrapper> subQuestionList) {
        name = sq.Name;
        surveyQuestion = sq;
        id = sq.Id;
        question = sq.Question__c;
        isSubQuestion = false;
        if(subQuestionList != null && subQuestionList.size() >0){
            isSubQuestion = true;
        }
        subQuestions = subQuestionList;
        orderNumber = sq.Order_Number_Displayed__c;
        choices = sq.Choices__c;
        required = sq.Required__c;
        questionType = sq.Type__c;
        choiceForAdditionalTextbox = sq.Choice_for_Additional_Textbox__c;
        selectedOption = '';
        additionalResponse = '';
        additionalTextboxNumber = '';
        renderSelectRadio = false;
        renderSelectCheckboxes = false;
        renderPicklist = false;
        renderRatings = false;
        renderFreeText = false;
        renderSelectRow = false;
        selectedOptions = new List<String> ();
        singleOptions = new Map<String,String>();
        if ('Single Select--Vertical'.equalsIgnoreCase(sq.Type__c)) {
            renderSelectRadio = true;
            singleOptions = stringToSelectOptions(choices);

            renderSelectCheckboxes = false;
            renderPicklist = false;
            renderFreeText = false;
            renderSelectRow = false;
            selectedOption = '';
            selectedOptions = new List < String > ();
        } else if ('Multi-Select--Vertical'.equalsIgnoreCase(sq.Type__c)) {
            renderSelectCheckboxes = true;
            multiOptions = stringToSelectOptions(choices);
            renderSelectRadio = false;
            renderPicklist = false;
            renderFreeText = false;
            renderSelectRow = false;
            selectedOption = '';
            selectedOptions = new List < String > ();
        } else if ('Single Select--Horizontal'.equalsIgnoreCase(sq.Type__c)) {
            renderSelectCheckboxes = false;
            rowOptions = stringToSelectOptions(choices);
            renderSelectRadio = false;
            renderPicklist = false;
            renderFreeText = false;
            renderSelectRow = true;
            selectedOption = '';
            selectedOptions = new List < String > ();

        } else if ('Picklist'.equalsIgnoreCase(sq.Type__c)) {
            renderPicklist = true;
            renderRatings = false;
            renderSelectRadio = false;
            singleOptions = new Map<String,String>();
            singleOptions.put('', '');
            singleOptions.putAll(stringToSelectOptions(choices));

            renderSelectCheckboxes = false;
            renderFreeText = false;
            renderSelectRow = false;
            selectedOption = '';
            selectedOptions = new List < String > ();
        } else if ('Rating'.equalsIgnoreCase(sq.Type__c)) {
            renderRatings = true;
            renderPicklist = false;
            renderSelectRadio = false;
            singleOptions = new Map<String,String>();
            singleOptions.put('', '');
            singleOptions.putAll(stringToSelectOptions(choices));

            renderSelectCheckboxes = false;
            renderFreeText = false;
            renderSelectRow = false;
            selectedOption = '';
            selectedOptions = new List < String > ();
        } else if ('Free Text'.equalsIgnoreCase(sq.Type__c) || 'Free Text - Single Row Visible'.equalsIgnoreCase(sq.Type__c)) {
            renderFreeText = true;
            renderSelectRadio = false;
            renderSelectCheckboxes = false;
            renderPicklist = false;
            renderSelectRow = false;
            choices = '';

            //If it's text area but for single row then only show single row even though it's stil text area
            if ('Free Text - Single Row Visible'.equalsIgnoreCase(sq.Type__c)) {
                noOfRowsForTextArea = '1';
            } else {
                noOfRowsForTextArea = '5';
            }

        }
        //responses= getResponses();
    }

    // Splits up the string as given by the user and adds each option
    //  to a list to be displayed as option on the Visualforce page
    // param: str   String as submitted by the user
    // returns the List of SelectOption for the visualforce page
    //
    private map <String,String> stringToSelectOptions(String str) {
        if (str == '') {
            return new map <String,String> ();
        }
        List<String> strList = str.split('\n');

        map <String,String> returnVal = new map <String,String> {};
        Integer i = 0;
        for (String s: strList) {
            if (String.isNotBlank(s)) {
                if (s != 'null') {
                    String sBis = s.replace(' ', '%20');
                    if(choiceForAdditionalTextbox != null){
                        List<String> optionList = choiceForAdditionalTextbox.split(';');
                        if(optionList.size()>0){
                            for (String option : optionList) {
                                if(option != '' && option.equals(s.trim())){
                                    additionalTextboxNumber = additionalTextboxNumber + String.valueOf(i) + ';';
                                    continue;
                                }
                            }
                        }
                    }

                    returnVal.put(String.valueOf(i), s.trim());
                    i++;
                }
            }
        }
        return returnVal;
    }
}