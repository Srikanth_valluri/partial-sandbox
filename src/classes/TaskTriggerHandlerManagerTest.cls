/**
 * @File Name          : TaskTriggerHandlerManagerTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/27/2019, 7:10:37 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/20/2019, 1:00:08 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
public class TaskTriggerHandlerManagerTest {
    public static testmethod void testSubmitFMCaseApproval(){
        insert new Skip_Task_Trigger_for_Informatica__c(
            SetupOwnerId = UserInfo.getOrganizationId(), 
            Enable__c = true
        ); 

        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
       
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        Property__c propObj = TestDataFactoryFM.createProperty();
        insert propObj;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Property_name__c = propObj.id;
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.FM_Role__c = 'FM Admin';
        insert fmUser;
        
        FM_User__c fmUser2= TestDataFactoryFM.createFMUser(u,locObj);
        fmUser2.FM_Role__c = 'Property Manager';
        insert fmUser2;
        
        FM_Case__c fmCaseObj=new FM_Case__c(); 
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Request_for_As_built_drawings';
        fmCaseObj.Booking_Unit__c=buIns.id;
        fmCaseObj.origin__c='Portal';
        fmCaseObj.Notice_Type__c = 'First Notice';
        fmCaseObj.Remedial_Period__c = 'Immediate';
        fmCaseObj.Property_Manager__c = fmUser2.FM_User__c;
        fmCaseObj.Admin__c = fmUser.FM_User__c;
        insert fmCaseObj;
        
        Task ins=new Task();
        ins.status='New';
        ins.subject='Verify All Details';
        ins.WhatId=fmCaseObj.id;
        insert ins;
        
        ins.status='Closed';
        update ins;
        
        Task ins1=new Task();
        ins1.status='New';
        ins1.subject=label.Violation_upload_Incident_Report;
        ins1.WhatId=fmCaseObj.id;
        insert ins1;
        
        ins1.status='Closed';
        update ins1;
        
        Task ins2=new Task();
        ins2.status='New';
        ins2.subject=label.Select_Notice_Type_and_Issue_Violation_Notice_Task;
        ins2.WhatId=fmCaseObj.id;
        insert ins2;
        
        ins2.status='Closed';
        update ins2;
        
        Task ins3=new Task();
        ins3.status='New';
        ins3.subject=label.Violation_Final_Notice_Task;
        ins3.WhatId=fmCaseObj.id;
        insert ins3;
        
        ins3.status='Closed';
        update ins3;

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
       
        List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
        List<Id> caseIdList = new List<Id>();
        System.runAs(u) {
        Contact con = new Contact(LastName = 'TEst');
        insert con;
        Id VIOLATION_NOTICE_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('FM_Case__c', 'Violation Notice');
        FM_Case__c obj1 = new FM_Case__c();
        obj1.Email__c = 'tenant@gmail.com';
        obj1.Tenant_Email__c = 'owner@gmail.com';
        obj1.recordtypeid = VIOLATION_NOTICE_RECORD_TYPE_ID;
        lstFMCases.add(obj1);        
        FM_Case__c obj2 = new FM_Case__c();
        obj2.Tenant_Email__c = 'owner@gmail.com';
        obj2.recordtypeid = VIOLATION_NOTICE_RECORD_TYPE_ID;
       // lstFMCases.add(obj2);        
        insert lstFMCases;
            for(fm_case__c obj: lstFMCases){
                caseIdList.add(obj.id);
            }
        }
        
        Document objDoc = new Document();
        objDoc.id = Label.Violation_List_document_Id;
        objDoc.Body = Blob.valueOf('test body');
        upsert objDoc;
        
        SR_Attachments__c objCustAttach1 = new SR_Attachments__c();
        objCustAttach1.FM_Case__c = lstFMCases[0].Id ;
        objCustAttach1.Name = 'attach 1';
        objCustAttach1.Attachment_URL__c  = 'wwwdamacpropertiescom';
        insert objCustAttach1 ;
        SR_Attachments__c objCustAttach2 = new SR_Attachments__c();
        objCustAttach2.FM_Case__c = lstFMCases[0].Id ;
        objCustAttach2.Name = 'attach 2';
        objCustAttach2.Attachment_URL__c  = 'wwwdamacpropertiescom';
        insert objCustAttach2 ;
        
        SR_Attachments__c objCustAttach3 = new SR_Attachments__c();
        objCustAttach3.FM_Case__c = lstFMCases[0].Id ;
        objCustAttach3.Name = 'attach 2';
        objCustAttach3.Attachment_URL__c  = 'wwwdamacpropertiescom';
        insert objCustAttach3;
        
        Blob b = blob.valueof('Unit.test');
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName('FileName');
        efa.setBody(b);
    
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTemplateId('00X7F000001Jsnb');
        mail.setTargetObjectId('0057F000000r5pG');
        mail.setSaveAsActivity(false);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { efa });
        emailList.add(mail);

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        //ViolationNoticeEmailSendInvocable.initiateEmailAndSMS(caseIdList);
        Test.stopTest();
        
    }
}