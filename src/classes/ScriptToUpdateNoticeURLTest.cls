/******************************************************************************
* Description - Test class developed for ScriptToUpdateNoticeURL
********************************************************************************/
@isTest
private class ScriptToUpdateNoticeURLTest {
    static testMethod void updateBuWithArabicUrlTest() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account( RecordTypeId = personAccRTId
                                    , Email__pc = 'a@w.com'
                                    , FirstName='Test FirstName'
                                    , LastName='Test LastName'
                                    , Type='Person'
                                    , party_ID__C='123456'
                                    , Nationality__pc = 'UAE');
        insert objAcc; 

        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        objSR.SPA_Type__c = 'Lease Option : On Payment plan without PDC';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        List<Booking_Unit_Active_Status__c>bookingUnitSettingLst = new List<Booking_Unit_Active_Status__c>();
        Booking_Unit_Active_Status__c setting1 = new Booking_Unit_Active_Status__c(Name = 'Agreement Cancellation Verified',
                                                                                   Status_Value__c = 'Agreement Cancellation Verified');
        
        bookingUnitSettingLst.add(setting1);
        Booking_Unit_Active_Status__c setting2 = new Booking_Unit_Active_Status__c(Name = 'Agreement executed by DAMAC',
                                                                                   Status_Value__c = 'Agreement executed by DAMAC');
        
        bookingUnitSettingLst.add(setting2);
        insert bookingUnitSettingLst;
        
        List<Booking_Unit__c>bookingUnitLst = new List<Booking_Unit__c>();
        Booking_Unit__c bookingUnit1 = new Booking_Unit__c( Unit_Name__c = 'JNU/SD168/XH2910B',
                                                            Registration_Status__c = 'Agreement executed by DAMAC' ,
                                                            JOPD_Area__c = '3456',
                                                            Booking__c = objBooking.Id,
                                                            Registration_ID__c = '74655',
                                                            Status__c = 'Active',
                                                            Approval_Status__c = 'EHO',
                                                            Property_Name__c = 'Property name',
                                                            Registration_DateTime__c  = datetime.newInstance(2017, 9, 15, 12, 30, 0),

                                                            Handover_Notice_URLs__c = 'Notice of Completion and Possession for your property in'
                                                            //Eligible_For_Early_Handover__c =true
                                                            );
        bookingUnitLst.add(bookingUnit1);
        insert bookingUnitLst;
        ScriptToUpdateNoticeURL.updateBuWithArabicUrl(new List<Id>{bookingUnitLst[0].Id});
    }

}