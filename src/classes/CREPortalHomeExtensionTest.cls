/*
* Description - Test class developed for 'CREPortalHomeExtension'
*
* Version            Date            Author            Description
* 1.0                21/11/17        Lochan            Initial Draft
*/
@isTest
private class CREPortalHomeExtensionTest {
    static testMethod void testMethod1() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
         
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        insert objCase;
        List<Case> lstCase = new List<Case>();
        lstCase.add(objCase);

        ApexPages.StandardSetController cases = new ApexPages.StandardSetController(lstCase);
    
        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'AdditionalParking');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);

        CREPortalHomeExtension obj = new CREPortalHomeExtension(cases);
        obj.searchTerm = 'Test FirstName';
        List<Account> lstAccount = CREPortalHomeExtension.searchMovie(obj.searchTerm);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'AssignmentRequest');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'BouncedCheque');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'FundTransfer');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'FurniturePackage');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'Mortgage');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'PenaltyWaiver');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'ProofOfPayment');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'Refunds');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'COCD');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'COCD');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'Early_Handover');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'NOCVisa');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'Complaint');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'Home');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'AOPT');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'RentalPool');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'TitleDeed');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'Handover');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'RentalPoolTermination');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);


        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'RentalPoolAssignment');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);

        Test.setCurrentPageReference(new PageReference('DummyTitleDeedCREPortal')); 
        System.currentPageReference().getParameters().put('CaseId', objCase.Id);
        System.currentPageReference().getParameters().put('SRType', 'PlotHandover');
        System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
        obj = new CREPortalHomeExtension(cases);
    }
}