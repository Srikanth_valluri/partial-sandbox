/************************************************************************************
* Description - Class to get Offers from IPMS                                       *
*                                                                                   *
* Version   Date            Author              Description                         *
* 1.0       10/07/2019      Arjun Khatri        Initial Draft.                      *
* 1.1       20/11/2019      Aishwarya Todkar    1.Passed Rebate Amount for Offer    *
                                                2.Closed the task                   *
* 1.2       30/12/2019      Aishwarya Todkar    1.Configured End point in CS        *                                             
*************************************************************************************/
Public Class OfferCallout {
    @testVisible private static final String SF_REST_BASEURL = Label.FM_REST_IPMS_BaseUrl;
    @testVisible private static final String OFFER_INSERT = '/booking/offer';
    Static List<Error_Log__c>  listErrorLogs;
    Static List<Case> lstCases;
    
    @InvocableMethod
    public static void updateOfferinIPMS( List<Id> lstCaseId ) {
        if (lstCaseId != null && lstCaseId.size() >0 ) {
            for(Id caseId:lstCaseId) {
                createOffers(caseId, null );
            }
            
        }
    }

    @future (callout=true)
    public static void createOffers(Id caseId, Id taskId ) {
        //query
        String offerText = '', offerCode = '', startDate = '', endDate = '';
        integer offerValue;
        integer registrationId ;
        List<Rebate_Offer__c> lstRebateOffers = new List<Rebate_Offer__c>();
        listErrorLogs = new List<Error_Log__c>();
        
        if( caseId != null ) {
            lstCases = [SELECT
                            Id
                            , Booking_Unit__r.Registration_ID__c
                            , Approved_Amount__c 
                            , IPMS_Updated__c 
                            , Registration_id__c
                            , Advance_Rabte_Amount__c
                            , recordType.Name
                            , AccountId
                            , Booking_Unit__c
                            , Status
                            , OwnerId
                            , Rebate_Amount__c//Offer_value__c
                        FROM
                            Case
                        WHERE
                                Id =: caseId];
            if( lstCases != null && lstCases.size() > 0 ) {
                if( lstCases[0].recordType.Name == 'Rebate On Advance' ) {
                    offerText = 'Rebate On Advance Payment';
                    offerCode = 'CP';
                    //offerValue = 1000;
                }
                else if( lstCases[0].recordType.Name == 'Overdue Rebate/Discount' ) {
                    offerText = 'Rebate On Overdue Payment';
                    offerCode = 'CP';
                    //offerValue = 2000;
                }
                offerValue = Integer.valueOf( lstCases[0].Rebate_Amount__c );
                //offerValue = String.valueOf(lstCases[0].Advance_Rabte_Amount__c);
                registrationId = Integer.valueOf(lstCases[0].Registration_id__c);
                startDate = System.today().day() + '-' + GenericUtility.getMonthName( System.today().month() ) + '-' + System.today().year();
                /*endDate = System.today().addDays(5).day() + '-' + mapOfMonth.get(System.today().addMonths(-1).month()) + '-' + System.today().addDays(5).year();
                */
                OfferResult objOfferResult = getOffers(registrationId
                                                        , offerText
                                                        , offerCode
                                                        , offerValue
                                                        , startDate
                                                        , endDate);
                if(objOfferResult != null ){
                    if(objOfferResult.responseLines != null && objOfferResult.responseLines.size() > 0) {
                        
                        //Creating Offers
                        for(responseLinesWrapper objResLines : objOfferResult.responseLines) {

                            Rebate_Offer__c objRebateOffer = new Rebate_Offer__c();
                            objRebateOffer.Offer_Id__c = String.isNotBlank( String.valueOf( objResLines.offerId ) ) ? String.valueOf( objResLines.offerId ) : '';
                            objRebateOffer.Offer_Value__c = String.isNotBlank( String.valueOf(objResLines.offerValue ) ) ? String.valueOf( objResLines.offerValue ) : '';
                            objRebateOffer.Offer_Text__c = String.isNotBlank( objResLines.offerText ) ? objResLines.offerText : '';
                            objRebateOffer.Offer_Code__c = String.isNotBlank( objResLines.offerCode ) ? objResLines.offerCode : '';
                            objRebateOffer.Case__c = lstCases[0].Id;
                            objRebateOffer.Booking_Unit__c = lstCases[0].Booking_Unit__c;
                            objRebateOffer.Account__c = lstCases[0].AccountId;
                            objRebateOffer.Adjust_In_Sale_Price__c = objResLines.adjustInSalePrice;

                            //objRebateOffer.Start_Date__c = Date.valueOf(objResLines.startDate);
                            if(String.isNotBlank(objResLines.startDate)) {
                                List<String> startDates = objResLines.startDate.split('-');
                                if(startDates!=null && startDates.size() >= 3) {
                                    Integer monthNumber = GenericUtility.getMonthNumber( startDates[1] );
                                    if( monthNumber != null ) {
                                        objRebateOffer.Start_Date__c = 
                                        GenericUtility.convertStringToDate( Integer.valueOf( startDates[0] )
                                                                        , monthNumber
                                                                        , Integer.valueOf( startDates[2] ) );
                                    }
                                }
                            }
                            
                            if(String.isNotBlank(objResLines.endDate)) {
                                List<String> endDates = objResLines.endDate.split('-');
                                if(endDates!=null && endDates.size() >= 3) {
                                    Integer monthNumber = GenericUtility.getMonthNumber( endDates[1] );
                                    if( monthNumber != null ) {
                                        objRebateOffer.End_Date__c = 
                                        GenericUtility.convertStringToDate( Integer.valueOf( endDates[0] )
                                                                        , monthNumber 
                                                                        , Integer.valueOf( endDates[2] ) );
                                    }
                                }
                            }
                            //objRebateOffer.End_Date__c = Date.valueOf(objResLines.endDate);
                            
                            lstRebateOffers.add(objRebateOffer);
                        }
                        
                        if( lstCases[0].recordType.Name == 'Overdue Rebate/Discount' ) {
                            if( taskId != null ) {
                                Task objTask = new Task( Id = taskId
                                                        , Status = 'Completed'
                                                        , Update_IPMS__c = true
                                                        , Process_Name__c = 'Overdue Rebate/Discount'
                                                        );
                                update objTask;
                            }
                            PenaltyWaiverService.updateDetailsInIPMS( lstCases );
                        }
                        else if(lstCases[0].recordType.Name == 'Rebate On Advance') {
                            lstCases[0].status = 'Closed';
                            update lstCases;
                        }
                    }
                    else {
                        Error_Log__c  objError = new Error_Log__c ();
                        objError.Error_Details__c  = objOfferResult.responseMessage != null ? objOfferResult.responseMessage :'Blank ResponseLines.';
                        objError.Account__c = lstCases[0].AccountId;
                        objError.Booking_unit__c = lstCases[0].Booking_Unit__c;
                        objError.Case__c = lstCases[0].Id;
                        listErrorLogs.add(objError);
                    }
                }
                if(lstRebateOffers.size() > 0) {
                insert lstRebateOffers;
                
                /*lstCases[0].status = 'Closed';
                update lstCases;*/
                }
                if(listErrorLogs.size() > 0) {
                    insert listErrorLogs;
                }
            }
        }
        
    }

    public static OfferResult getOffers(Integer registrationId
                                            , String offerText
                                            , String offerCode
                                            , Integer offerValue
                                            , String startDate
                                            , String endDate) {
        String accessToken = FmIpmsRestCoffeeServices.getNewBearerToken();
        HttpRequest req = new HttpRequest();
        OfferResult objOfferResult;

        List<Credentials_Details__c> lstCreds = [ SELECT
                                                    Id
                                                    , Name
                                                    , User_Name__c
                                                    , Password__c
                                                    , Endpoint__c
                                                FROM
                                                    Credentials_Details__c
                                                WHERE
                                                    Name = 'Rebate Offer'];

            System.debug('lstCreds = ' + lstCreds);                                        
            if( lstCreds != null && lstCreds.size() > 0 && lstCreds[0].Endpoint__c != null) {
    
                req.setEndpoint(lstCreds[0].Endpoint__c);//(SF_REST_BASEURL + OFFER_INSERT);
                System.debug(req.getEndpoint());
                req.setMethod('POST');
                req.setHeader('Accept', 'application/json');
                req.setHeader('Content-Type', 'application/json'); 
                req.setHeader('Authorization','Bearer' + accessToken);
                req.setTimeout(120000);
                
                String requestBody =
                    '{' +
                        '"registrationId":'+ registrationId +',' +
                        '"offerText": "' + offerText + '",' +
                        '"offerCode": "' + offerCode +'",' +
                        '"offerValue": ' + offerValue + ','+
                        '"startDate": "' + startDate + '",' + //15-Jun-19
                        '"endDate": "' + endDate + '"' +
                    '}';


                System.debug('requestBody = ' + requestBody);
                req.setBody(requestBody);
                System.debug('req : '+req);
                try {
                    HttpResponse res = new Http().send(req);
                    System.debug('Status Code = ' + res.getStatusCode());
                    System.debug('body = ' + res.getBody());
                    objOfferResult = new OfferResult();
                    if(res.getBody() != null) {
                        objOfferResult = (OfferResult) JSON.deserialize(
                            res.getBody(), OfferResult.class
                        );

                        System.debug('objOfferResult = ' + objOfferResult);
                    }
                }
                catch(Exception e){
                    system.debug('exception = ' + e.getMessage());
                    Error_Log__c  objError = new Error_Log__c ();
                    objError.Error_Details__c  = e.getMessage();
                    objError.Account__c = lstCases[0].AccountId;
                    objError.Booking_unit__c = lstCases[0].Booking_Unit__c;
                    objError.Case__c = lstCases[0].Id;
                    listErrorLogs.add(objError);
                    return null;
                }
            }
        return objOfferResult;
    }
    public class OfferResult {
        public String               responseId                  {get; set;}
        public String               responseTime                {get; set;}
        public String               responseMessage             {get; set;}
        public Integer              elapsedTimeMs               {get; set;} 
        public List<responseLinesWrapper>  responseLines                {get; set;}
        public Boolean                complete                    {get; set;} 
        
        public OfferResult() {
            responseLines = new List<responseLinesWrapper>();
        }
    }
    
    public class responseLinesWrapper {
        public Integer              offerId        {get; set;}
        public Integer              registrationId    {get; set;}
        public String               offerText        {get; set;}
        public String               offerCode        {get; set;} 
        public Decimal              offerValue        {get; set;}
        public String               adjustInSalePrice    {get; set;}
        public String               startDate            {get; set;}
        public String               endDate                {get; set;}
    }
}