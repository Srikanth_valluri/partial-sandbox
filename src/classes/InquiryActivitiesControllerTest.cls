@isTest
public with sharing class InquiryActivitiesControllerTest {


    static testmethod void testInquiryActivitiesInsertCompleted() {

        // Test Data
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABC@test.com');
        insert pcUser;




        Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
        Inquiry__c inqObj= new Inquiry__c(RecordTypeId=agenTeamRT,Inquiry_Source__c='Agent Referral',Mobile_Phone_Encrypt__c='456123',Mobile_CountryCode__c='American Samoa: 001684',Mobile_Phone__c='1234',Email__c='mk@gmail.com',First_Name__c='Test',Last_Name__c='Last',CR_Number__c='0987',ORN_Number__c='7842',Agency_Type__c='Corporate',Organisation_Name__c = 'Oliver',isDuplicate__c=false);
        insert inqObj; 

        List<Amendment__c> ammendmentList = new List<Amendment__c>();
        for (Integer i = 0; i < 5; i++) {
            Amendment__c ammendmentObj = new Amendment__c();
            ammendmentObj.First_Name__c = 'Corporate Agency ' + i;
            ammendmentObj.Inquiry__c = inqObj.Id; 
            ammendmentList.add(ammendmentObj);
        }
        insert ammendmentList ;

        InquiryActivitiesController inquiryActivity;

        // Test the functionality in user context
        System.runAs(pcUser) {
            System.Test.startTest();
            ApexPages.StandardController controller = new ApexPages.StandardController(inqObj);
            PageReference inquiryActivitiesPage = Page.InquiryActivities;
            Test.setCurrentPage(inquiryActivitiesPage);
            //ApexPages.currentPage().getParameters().put('id', inqObj.Id);
            inquiryActivity = new InquiryActivitiesController(controller);
            inquiryActivity.completedtaskObj.Activity_Type__c = 'Calls';
            inquiryActivity.completedtaskObj.End_Date__c = System.Today();
            inquiryActivity.completedtaskObj.Start_Date__c = System.Today();
            inquiryActivity.completedtaskObj.Activity_Sub_Type__c = null;
            inquiryActivity.completedtaskObj.Activity_Outcome__c='Email Delivered';
            inquiryActivity.saveActivity();
            System.Test.stopTest();
        }
    }


    static testmethod void testAccountActivitiesInsertFollowup() {

        // Test Data
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABC@test.com');
        insert pcUser;

         Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
        Inquiry__c inqObj= new Inquiry__c(RecordTypeId=agenTeamRT,Inquiry_Source__c='Agent Referral',Mobile_Phone_Encrypt__c='456123',Mobile_CountryCode__c='American Samoa: 001684',Mobile_Phone__c='1234',Email__c='mk@gmail.com',First_Name__c='Test',Last_Name__c='Last',CR_Number__c='0987',ORN_Number__c='7842',Agency_Type__c='Corporate',Organisation_Name__c = 'Oliver',isDuplicate__c=false);
        insert inqObj; 

        List<Amendment__c> ammendmentList = new List<Amendment__c>();
        for (Integer i = 0; i < 5; i++) {
            Amendment__c ammendmentObj = new Amendment__c();
            ammendmentObj.First_Name__c = 'Corporate Agency ' + i;
            ammendmentObj.Inquiry__c = inqObj.Id; 
            ammendmentList.add(ammendmentObj);
        }
        insert ammendmentList ;

        InquiryActivitiesController inquiryActivity ;       
        // Test the functionality in user context
        System.runAs(pcUser) {
            System.Test.startTest();
            ApexPages.StandardController controller = new ApexPages.StandardController(inqObj);
            PageReference inquiryActiviesPage = Page.InquiryActivities; 
            Test.setCurrentPage(inquiryActiviesPage);
           // ApexPages.currentPage().getParameters().put('id', inqObj.Id);
            inquiryActivity = new InquiryActivitiesController(controller);
           // inquiryActivity.inqId = inqObj.Id;
            inquiryActivity.completedtaskObj.Activity_Type__c = 'Calls';
            inquiryActivity.completedtaskObj.End_Date__c = System.Today();
            inquiryActivity.completedtaskObj.Start_Date__c = System.Today();
            inquiryActivity.completedtaskObj.Activity_Sub_Type__c = null;
            inquiryActivity.isFollowUp = true;
            inquiryActivity.scheduledtaskObj.Status = 'In Progress';
            inquiryActivity.scheduledtaskObj.Activity_Type__c = 'Calls';
            inquiryActivity.scheduledtaskObj.End_Date__c = System.Today();
            inquiryActivity.scheduledtaskObj.Start_Date__c = System.Today();
            inquiryActivity.scheduledtaskObj.Activity_Sub_Type__c = null;
            inquiryActivity.completedtaskObj.Activity_Outcome__c='Email Delivered';
            inquiryActivity.saveActivity();
            
            System.Test.stopTest();
        }
    }

    static testmethod void testAccountActivitiesUpdate() {

        // Test Data
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABC@test.com');
        insert pcUser;

        Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
        Inquiry__c inqObj= new Inquiry__c(RecordTypeId=agenTeamRT,Inquiry_Source__c='Agent Referral',Mobile_Phone_Encrypt__c='456123',Mobile_CountryCode__c='American Samoa: 001684',Mobile_Phone__c='1234',Email__c='mk@gmail.com',First_Name__c='Test',Last_Name__c='Last',CR_Number__c='0987',ORN_Number__c='7842',Agency_Type__c='Corporate',Organisation_Name__c = 'Oliver',isDuplicate__c=false);
        insert inqObj; 

        List<Amendment__c> ammendmentList = new List<Amendment__c>();
        for (Integer i = 0; i < 5; i++) {
            Amendment__c ammendmentObj = new Amendment__c();
            ammendmentObj.First_Name__c = 'Corporate Agency ' + i;
            ammendmentObj.Inquiry__c = inqObj.Id; 
            ammendmentList.add(ammendmentObj);
        }
        insert ammendmentList ;      
        Task taskObj = new Task();
        taskObj.WhatId = inqObj.Id;
        taskObj.ActivityDate = System.Today();
        taskObj.OwnerId = pcUser.Id;
        taskObj.Status = 'Completed';
        taskObj.Priority = 'Normal';
        taskObj.Activity_Type__c = 'Calls';
        taskObj.Activity_Sub_Type__c = '';
        taskObj.Start_Date__c = System.Today();
        taskObj.End_Date__c = System.Today();
        taskObj.Description = 'Hello';
        taskObj.Agency_Contacts_Ids__c = ammendmentList[0].Id;
        insert taskObj;

        InquiryActivitiesController inquiryActivity;

        // Test the functionality in user context
        System.runAs(pcUser) {
            System.Test.startTest();
            ApexPages.StandardController controller = new ApexPages.StandardController(inqObj);
            PageReference inquiryActiviesPage = Page.InquiryActivities;
            Test.setCurrentPage(inquiryActiviesPage);
            ApexPages.currentPage().getParameters().put('id', inqObj.Id);
            ApexPages.currentPage().getParameters().put('taskid', taskObj.Id);
            inquiryActivity = new InquiryActivitiesController(controller);
            inquiryActivity.completedtaskObj.Activity_Outcome__c='Email Delivered';
            //inquiryActivity.updateActivity();
            Task taskObj1 = new Task();
            Boolean valid = inquiryActivity.validateActivity(taskObj1);
            System.Test.stopTest();
        }
    }
}