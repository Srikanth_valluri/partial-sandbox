public without sharing class CommunityPaymentSuccessExtension {

    public  String                              unitNames               {get; set;}

    private List<FM_Unit_Invoice_Payment__c>    lstUnitInvoicePayment = new List<FM_Unit_Invoice_Payment__c>();
    private String                              recordId;

    public CommunityPaymentSuccessExtension(ApexPages.StandardController standardController) {
        recordId = standardController.getId();
        unitNames = '';
        Set<String> setUnitNames = new Set<String>();

        for (FM_Unit_Invoice_Payment__c unitInvoicePayment : [
            SELECT  Id
                    , Name
                    , Booking_Unit__c
                    , Booking_Unit__r.Unit_Name__c
                    , Call_Type__c
                    , TRX_Type__c
            FROM    FM_Unit_Invoice_Payment__c
            WHERE   FM_Receipt__c = :recordId
        ]) {
            setUnitNames.add(unitInvoicePayment.Booking_Unit__r.Unit_Name__c);
            lstUnitInvoicePayment.add(unitInvoicePayment);
        }
        for (String unitName : setUnitNames) {
            unitNames += unitName + ', ';
        }
        unitNames = unitNames.removeEnd(', ');
    }


}