/************************************************************************************************
 * @File Name          : PaymentReminderBefore90Batch.cls                                           *
 * @Description        :             *
 * @Author             : Mosib Khan                                                         *
 * @Group              :                                                                        *
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 05/02/2019, 2:26:21 PM
 * @Modification Log   :                                                                        * 
 *===============================================================================================
 * Ver      Date            Author                  Modification                                *
 *===============================================================================================
 * 1.0                      Mosib Khan           Initial Version                             *
 
*************************************************************************************************/
public without sharing class PaymentReminderBefore90Batch implements Database.Batchable<sObject>
                                                                , Database.AllowsCallouts
                                                                , Database.Stateful { 
    private String query;
    public String recordId;
    List<DP_Invoices__c> lstInvoicesTUpdate;
    List<EmailMessage> lstEmails;
    List<SMS_History__c> smsLst;
    public String regId;
    
    Set<String> setArabicNationality = new Set<String> {'saudi','jordanian','omani', 'uae' , 'kuwaiti', 'qatari', 'lebanese', 'libyan' , 'Kazak', 'egyption', 'egyptianiraqi', 'iranian'};
    
    public List<Attachment> attachmentUrlsForEmailMessage; 
    
    public Database.Querylocator start( Database.BatchableContext bc ) {
        recordId =  Label.dailyDpBatchForSingleUser.split('-',2)[1];
        query = ' SELECT Id,'+ 
                ' Name,'+
                ' Unit_Name__c,'+
                ' Property_Name__c,'+ 
                ' FinalDate__c,Dunning_90_Email_Sent__c,' +
                
                ' Milestone_Type_for_Dunning__c,'+
                ' When2Trigger__c,' +
                ' Current_Level__c,' +
                ' Future_Level__c,' +
                ' sixtyninty__c,' +
                ' PropertyCategory__c,'+
                ' Current_percentage__c,'+
                ' Expected_percentage__c,'+
                ' Expected_Date__c,'+
                ' Accounts__c, '+
                ' Accounts__r.recordTypeId, '+
                ' Accounts__r.email__c, '+
                ' Accounts__r.email__pc, '+
                ' Accounts__r.Nationality__c ,' +
                ' Accounts__r.isPersonAccount ,' +
                ' Accounts__r.Mobile_Phone_Encrypt__pc ,' +
                ' Accounts__r.Mobile__c ,Accounts__r.First_Name_Arabic__c ,' +
                ' Accounts__r.Name , Accounts__r.Nationality__pc,Accounts__r.Last_Name_Arabic__c,'+
				' Accounts__r.First_Name_Arabic__pc,Accounts__r.Last_Name_Arabic__pc'+
                ' FROM DP_Invoices__c '+
                ' Where ';
                if(Label.dailyDpBatchForSingleUser.split('-',2)[0] == 'ON'){
                    query +=' ID = :recordId';
                }
                else{
                     query += ' sixtyninty__c = TRUE AND PropertyCategory__c != NULL  '+
                              ' AND ( When2Trigger__c >= 70 AND When2Trigger__c <= 90 )';
                }
        System.debug('Dp  ::: '+query);       
        System.debug('Dp  ::: '+Database.getQueryLocator(query));
        return Database.getQueryLocator(query);
    }
    public void execute( Database.BatchableContext bc, list<DP_Invoices__c> dpInvoiceRecords ) {
       
        lstEmails = new List<EmailMessage>();
        smsLst = new List<SMS_History__c>();
        lstInvoicesTUpdate = new List<DP_Invoices__c>();
        attachmentUrlsForEmailMessage = new List<Attachment>();
        System.debug('Payment Reminder Before 60  execute dpInvoiceRecords  == ' + dpInvoiceRecords );
        String emailTemplateName;
        string smsTemplateName;

     
        for( DP_Invoices__c objInvoice : dpInvoiceRecords ) { 
            emailTemplateName = 'payment_reminder_90_days_email';
            string nationality = objInvoice.Accounts__r.Nationality__c != NULL ? objInvoice.Accounts__r.Nationality__c : objInvoice.Accounts__r.Nationality__pc;
            System.debug('nationality !!==' + nationality);
            if(string.isnotblank(nationality)) { 
                // Get Email template for UAE
                if(setArabicNationality.contains(nationality.toLowercase())) {
                    smsTemplateName = 'payment_reminder_90_days_sms_arabic' ;  

                }else{
                    smsTemplateName = 'payment_reminder_90_days_sms_eng' ; 
                }
            }
            
            sendEmail(objInvoice,emailTemplateName); 
            sendSMS(objInvoice,smsTemplateName);
        }

        System.debug('lstEmails=== ' + lstEmails);
        if(lstEmails.size() > 0) {
            if( !Test.isRunningTest() ) {
                insert lstEmails; 
            }
            
        }

        system.debug('smsLst = '+ smsLst );
        if( smsLst != null && smsLst.size() > 0 ) {
            if( !Test.isRunningTest() ) {   
                insert smsLst; 
            }
        }

        system.debug('lstInvoicesTUpdate = '+ lstInvoicesTUpdate );
        if( lstInvoicesTUpdate != null && lstInvoicesTUpdate.size() > 0 ) {
            if( !Test.isRunningTest() ) {   
                update lstInvoicesTUpdate; 
            }
        }
        

        
    }
    
    public void finish( Database.BatchableContext bc ) {
    }

    void sendEmail(DP_Invoices__c objDPInvoice,String TemplateName) {
        List<Attachment>attachmentUrls = new List<Attachment>();
        
        Id personRecTypeId, businessRecTypeId;
        personRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        
        
        if(objDPInvoice.Accounts__c != NULL && String.isNotBlank(TemplateName)){
            System.debug('objDPInvoice.Accounts__c !!==' + objDPInvoice.Accounts__c);
            
            String emailTemplateName;Process_Email_Template__mdt emt;EmailTemplate emailTemplateObj;
            String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';String subject, strAccountId, strDPIId, bccAddress ='';
            
            

            emailTemplateObj = [SELECT 
                                ID, Subject, Body, HtmlValue, TemplateType
                            FROM 
                                EmailTemplate 
                            WHERE 
                                DeveloperName =: TemplateName limit 1];
            
            
            strAccountId = objDPInvoice.Accounts__c != null ? String.valueOf(objDPInvoice.Accounts__c ) : objDPInvoice.BookingUnits__r.Booking__r.Account__c;
            
            strDPIId = String.valueOf(objDPInvoice.id);
            bccAddress = Label.Sf_Copy_Bcc_Mail_Id; //SF_Copy_EMail_Address;
            //System.debug('bccAddress !!==' + bccAddress);

            // If Person Acount 
            if(objDPInvoice.Accounts__r.recordTypeId == personRecTypeId && objDPInvoice.Accounts__r.Email__pc != null) {
                 toAddress = objDPInvoice.Accounts__r.Email__pc;
            }

            //If business account
            else if(objDPInvoice.Accounts__r.recordTypeId == businessRecTypeId && objDPInvoice.Accounts__r.Email__c != null){  
                toAddress = objDPInvoice.Accounts__r.Email__c;
            }
            System.debug('toAddress = ' + toAddress);
            fromAddress = Label.AT_Your_Service;
            contentType = 'text/html';
            replyToAddress = Label.AT_Your_Service;
            
            String strCCAddress = '';
           // System.debug('emailTemplateObj = ' + emailTemplateObj);       
            if(toAddress != '' && emailTemplateObj != null) {

                Subject = emailTemplateObj.subject != null ? emailTemplateObj.subject : 'Payment Reminder by Damac ';
                System.debug('SubjectL:::'+Subject);
                
                
                System.debug('after replacement SubjectL:::'+Subject);
                if(emailTemplateObj.body != NULL){
                    contentBody = emailTemplateObj.body;
                }
                if(emailTemplateObj.htmlValue != NULL){
                    contentValue = emailTemplateObj.htmlValue;
                }    
                if(string.isblank(contentValue)){
                    contentValue='No HTML Body';
                }   
                

                 // TODO  add code fetch Attachment related to email template 
                
                
                
                contentValue = contentValue.replace('{Customer Name}', GenericUtility.getCamelCase(objDPInvoice.Accounts__r.Name));
                contentBody = contentBody.replace('{Customer Name}', GenericUtility.getCamelCase(objDPInvoice.Accounts__r.Name));
                /**
                 * {Expected percentage}
                {Current percentage}
                {Property}
                {Customer Name}
                {Due Date}
                
                {Current Level}
                {Expected Level}
                 * Property_Name__c
                 * Current_Level__c
                    Future_Level__c
                    Current_percentage__c
                    Expected_percentage__c
                
                 */
                if(objDPInvoice.Property_Name__c != NULL){ 
                    contentValue = contentValue.replace('{Property}',String.valueOf(objDPInvoice.Unit_Name__c) +' In '+String.valueOf(objDPInvoice.Property_Name__c));
                    contentBody = contentBody.replace('{Property}', String.valueOf(objDPInvoice.Unit_Name__c) +' In '+String.valueOf(objDPInvoice.Property_Name__c));
                }//<a href="'+ objData.url + '"target="_blank">'+ 'Notice of Completion' + '</a>
                
                    String Link  = Label.Voice_Note_Link;
                    //string url = '<a href="' + Link + '"target="_blank"> LINK </a>';
                    contentValue = String.isNotBlank(Link)?contentValue.replace('{Link}',Link ): contentValue.replace('{Link}','' );
                    contentBody = String.isNotBlank(Link)?contentBody.replace('{Link}',Link ): contentBody.replace('{Link}','' );
                

               
                //System.debug('contentValue = ' + contentValue);
                // Callout to sendgrid to send an email
                SendGridEmailService.SendGridResponse objSendGridResponse =  
                    SendGridEmailService.sendEmailService(toAddress,
                    '', 
                    strCCAddress, 
                    '', 
                    bccAddress, 
                    '', 
                    subject,
                    '',
                    fromAddress,
                    '',
                    replyToAddress,
                    '',
                    contentType,
                    contentValue,
                    '',
                    attachmentUrls
                );
                system.debug('toAddress === ' + toAddress);
                system.debug('fromAddress === ' + fromAddress);
                system.debug('subject === ' + subject);
                //system.debug('contentBody === ' + contentBody);
                //system.debug('contentType === ' + contentType);
                system.debug('objSendGridResponse === ' + objSendGridResponse);
                String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                if( responseStatus == 'Accepted' ) {
                    EmailMessage mail = new EmailMessage();
                    mail.Subject = subject;
                    mail.MessageDate = System.Today();
                    mail.Status = '3';//'Sent';
                    mail.RelatedToId = strAccountId;
                    mail.Account__c  = strAccountId;
                    mail.Type__c = 'FM Invoice';
                    mail.ToAddress = toAddress;
                    mail.FromAddress = fromAddress;
                    mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                    mail.Sent_By_Sendgrid__c = true;
                    mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                    mail.DP_Invoices__c = strDPIId;
                    mail.CcAddress = strCCAddress;
                    mail.BccAddress = bccAddress;
                     
                    lstEmails.add(mail);

                   system.debug('Mail obj == ' + mail);

                    
                    lstInvoicesTUpdate.add( new DP_Invoices__c(Id = objDPInvoice.Id, Dunning_90_Email_Sent__c = true));
                }
            }
        }
    }
    

      void sendSMS(DP_Invoices__c objDPInvoice,String TemplateName) {

        
        EmailTemplate emailTemplateObj;
        emailTemplateObj = [SELECT 
                                ID, Subject, Body, HtmlValue, TemplateType
                            FROM 
                                EmailTemplate 
                            WHERE 
                                DeveloperName =: TemplateName limit 1];

        String strPhoneNumber  = '', contentValue,contentBody = '', strDPIId = '';
        String user,passwd,strSID , strAccountId ,customerName ,customerNameArabic,dueAmount,UnitName ;
        
        strDPIId = String.valueOf(objDPInvoice.id);
        strAccountId = objDPInvoice.Accounts__c;
        
        System.debug('objDPInvoice.Accounts__r..isPersonAccount = ' + objDPInvoice.Accounts__r.isPersonAccount);
        System.debug('objDPInvoice.Accounts__r..Mobile_Phone_Encrypt__pc = ' + objDPInvoice.Accounts__r.Mobile_Phone_Encrypt__pc);
        System.debug('objDPInvoice.Accounts__r..Mobile__c = ' + objDPInvoice.Accounts__r.Mobile__c);

        // If Person Acount
        if( objDPInvoice.Accounts__r.isPersonAccount
        && objDPInvoice.Accounts__r.Mobile_Phone_Encrypt__pc != null ) {
            strPhoneNumber = objDPInvoice.Accounts__r.Mobile_Phone_Encrypt__pc;
        }

        //If business account
        else if( objDPInvoice.Accounts__r.Mobile__c != null ) {
            strPhoneNumber = objDPInvoice.Accounts__r.Mobile__c;
        }
        
        if(strPhoneNumber.startsWith('00')) {
            strPhoneNumber = strPhoneNumber.removeStart('00');
        }
        else if(strPhoneNumber.startsWith('0')) {
            strPhoneNumber = strPhoneNumber.removeStart('0');
        }
        System.debug('strPhoneNumber = ' + strPhoneNumber);
        System.debug('emailTemplateObj = ' + emailTemplateObj);

        if( String.isNotBlank(strPhoneNumber) && emailTemplateObj != null ) {

            customerName = objDPInvoice.Accounts__r.Name != null ? objDPInvoice.Accounts__r.Name : '';
         
            if(objDPInvoice.Accounts__r.isPersonAccount == false 
                && String.isNotBlank(objDPInvoice.Accounts__r.First_Name_Arabic__c)
                && String.isNotBlank(objDPInvoice.Accounts__r.Last_Name_Arabic__c)){
                customerNameArabic = objDPInvoice.Accounts__r.First_Name_Arabic__c + ' ' + 
                                    objDPInvoice.Accounts__r.Last_Name_Arabic__c;  
            }         
            else if(objDPInvoice.Accounts__r.isPersonAccount == true
                && String.isNotBlank(objDPInvoice.Accounts__r.First_Name_Arabic__pc)
                && String.isNotBlank(objDPInvoice.Accounts__r.Last_Name_Arabic__pc))   {
                customerNameArabic = objDPInvoice.Accounts__r.First_Name_Arabic__pc + ' ' + 
                objDPInvoice.Accounts__r.Last_Name_Arabic__pc;
            }
            else{
                customerNameArabic = '';
            }      

            if(customerName != null  ) {

                if(emailTemplateObj.body != NULL){
                    contentBody =  emailTemplateObj.body;
                }
                if(emailTemplateObj.htmlValue != NULL){
                    contentValue = emailTemplateObj.htmlValue;
                }
                if(string.isblank(contentValue)){
                    contentValue='No HTML Body';
                }

                customerName = GenericUtility.getCamelCase(customerName);
                
                contentValue = String.isNotBlank(customerName)?contentValue.replace('{Customer Name}', customerName):'';
                contentBody = String.isNotBlank(customerName)?contentBody.replace('{Customer Name}', customerName):'';
                
                contentValue = String.isNotBlank(customerNameArabic)?contentValue.replace('{Customer Name Arabic}', customerNameArabic):'';
                contentBody = String.isNotBlank(customerNameArabic)?contentBody.replace('{Customer Name Arabic}', customerNameArabic):'';
                
                if(objDPInvoice.Property_Name__c != NULL){ 
                    contentValue = contentValue.replace('{Property}',String.valueOf(objDPInvoice.Unit_Name__c) +' In '+String.valueOf(objDPInvoice.Property_Name__c));
                    contentBody = contentBody.replace('{Property}', String.valueOf(objDPInvoice.Unit_Name__c) +' In '+String.valueOf(objDPInvoice.Property_Name__c));
                }
            
                HttpRequest req = new HttpRequest();
                HttpResponse res = new HttpResponse();
                Http http = new Http();
                user = Label.Damac_CRM_SMS_Service_User_name;//'damaccrm';
                passwd = Label.Damac_CRM_SMS_Service_Password;//'D@$al3sF0rc387!';
                //strSID = Label.Damac_CRM_SMS_Service_Sender_Id;//'DAMAC GRP';
                strSID = SMSClass.getSenderName(user, strPhoneNumber, false);

                req.setMethod('POST' ); // Method Type
                req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx'); 

                string msgCont = GenericUtility.encodeChar(contentBody);
                System.debug('msgCont:'+msgCont);
                req.setBody('user='+ user + '&passwd=' + passwd +'&message=' + msgCont + '&mobilenumber=' + strPhoneNumber + '&sid='+strSID+ '&MTYPE=LNG&DR=Y'); // Request Parameters
                try {
                    system.debug('Request Body---'+req.getBody());
                    system.debug('Request Header---'+req.getHeader('Authorization'));
                    system.debug('Request Header---'+req.getHeader('Content-Type'));
                    res = http.send(req);
                    system.debug('Response---'+res.getBody());
                    if( res.getBody() != null ) {

                        if( String.valueOf( res.getBody() ).contains('OK:') ) {
                            system.debug('Message Send Sucessfully');
                            SMS_History__c smsObj = new SMS_History__c();
                            smsObj.Message__c = contentBody;
                            smsObj.Phone_Number__c = strPhoneNumber;
                            if( strAccountId !='' ) {
                                smsObj.Customer__c = strAccountId;
                            }
                            if( String.isNotBlank( strDPIId ) ) {
                                smsObj.DP_Invoices__c = strDPIId;
                            }
                            smsObj.Is_SMS_Sent__c = true;
                            smsObj.isFMSMS__c = false;
                            smsObj.sms_Id__c = res.getBody().substringAfter('OK:');
                            smsObj.Description__c = res.getBody();
                            System.debug('smsObj::::if:'+smsObj);
                            smsLst.add(smsObj);
                        }
                        else{
                            //if failure
                            SMS_History__c smsObj = new SMS_History__c();
                            smsObj.Message__c = contentBody;
                            smsObj.Phone_Number__c = strPhoneNumber;
                            if( strAccountId !='' ) {
                                smsObj.Customer__c = strAccountId;
                            }
                            if( String.isNotBlank( strDPIId ) ) {
                                smsObj.DP_Invoices__c = strDPIId;
                            }
                            smsObj.isFMSMS__c = false;
                            smsObj.Description__c = res.getBody();
                            System.debug('smsObj::::else:'+smsObj);
                            smsLst.add(smsObj);
                        }
                    }
                } catch(Exception e) {
                    System.debug('Error***** '+ e);
                }

            }//End Payment details null check if
        }//End emailTemplateObj IF
    }    
   
    
    
  
}