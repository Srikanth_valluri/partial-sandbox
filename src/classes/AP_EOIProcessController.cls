/****************************************************************************************************
 Name          : AP_EOIProcessController
 Description   : Controller to get basic info for EOI  
 Created Date  : 10-09-2018                                                                        
 Created By    : ESPL                                                                              
 --------------------------------------------------------------------------------------------------
 VER   AUTHOR             DATE        COMMENTS                                                    
 1.0   Nikhil Pote                    Initial Draft.                                     
****************************************************************************************************/
public class AP_EOIProcessController {
    public String BASE_URL = Site.getBaseSecureUrl();
    public static EOI_Process__c objEOI {get; set;}
    public Inquiry__c objIQ {get; set;}
    public string validate {get;set;}
    public String inquiryId  {get;set;}
    public String unitSelected {get;set;}
    public String paymentModeSelected{get;set;}
    public String strSelectedLanguage {get; set;}
    public User objUsr ;
    public list<SelectOption> propertyOptions{get; set;}
    public AP_EOIProcessController() {
        objEOI = new EOI_Process__c();
        objIQ = new Inquiry__c();
        propertyOptions = new list<Selectoption>();
         //inquiryId;
        inquiryId = apexpages.currentPage().getParameters().get('inquiryId');
        strSelectedLanguage = apexpages.currentPage().getParameters().get('langCode');
        if(string.isNotBlank(inquiryId)){
            Inquiry__c existingInq = [SELECT id, First_Name__c, Last_Name__c, Email__c, Mobile_CountryCode__c, Mobile_Phone_Encrypt__c, Mobile_Phone__c
                                      FROM Inquiry__c
                                      WHERE id =: inquiryId];
            system.debug('existingInq-----------'+existingInq); 
            objIQ = existingInq;
        }

        objUsr = [ SELECT Id, ContactId, Contact.Email, Contact.AccountId,
                                Contact.FirstName, Contact.LastName
                            FROM User
                            WHERE Id =: UserInfo.getUserId()];
        list<Property__c> defaultProperty = new list<Property__c>();
        defaultProperty = [Select id, name From Property__c Where EOI_Enabled__c = true];
        propertyOptions.add(new SelectOption('','--Select A Project--'));
        if(defaultProperty != null && defaultProperty.size()>0){
            for(Property__c prop: defaultProperty)
                propertyOptions.add(new SelectOption(prop.Id,prop.Name));
            objEOI.EOI_for_Project__c = defaultProperty[0].Name;
        }
        if(String.isNotBlank(inquiryId)){
                objEOI.Inquiry__c = inquiryId;
        }
        if(objUsr != null){
            objEOI.Account__c = objUsr.Contact.AccountId;
        }
            
        system.debug('inquiryId-----------'+inquiryId);
        system.debug('objUsr-----------'+objUsr);
    }

    /*
    * @return: Redirect to Next section
    */
    public PageReference submitDetails(){ 
        system.debug('in submit');
        try{            
            upsert objIQ;
            system.debug('objIQ---'+objIQ);
            /* 19-12-2018 */
            //PageReference pg = new PageReference('/agents/AP_EOIProcessStep2');
            //PageReference pg = Page.AP_EOIProcessStep2;
            PageReference pg = new PageReference(BASE_URL+'/AP_EOIProcessStep2');
            pg.getParameters().put('inquiryId',objIQ.id);
            if(String.isNotBlank(strSelectedLanguage)) {
                pg.getParameters().put('langCode',strSelectedLanguage);
            }
            
            system.debug('pg----------------------'+pg);
            return pg; 
        } catch(exception e) {
            validate = e.getMessage();
            system.debug('validate'+validate);
            return null;
        }
    }

    public static List<String> getUnits() {
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = EOI_Process__c.No_of_Units__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            String valueOfPicklist = f.getValue();
            options.add( f.getLabel()); 
        }       
        return options;
    } 

    public static List<String> getPaymentMode() {
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = EOI_Process__c.Mode_of_Token_Payment__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            String valueOfPicklist = f.getValue();
            options.add( f.getLabel()); 
        }       
        return options;
    } 

    @remoteAction
    public static String insertEOI(string propertySelected, String unitSelected,String paymentModeSelected, Decimal tokenamtSelected, String comments, String inquiryId){ 
        list<Property__c> defaultProperty = new list<Property__c>();
        defaultProperty = [Select id, name From Property__c Where name =: System.Label.Default_EOI_Property];
        User userRecord = [Select id, accountId from User where id =: userInfo.getuserId()];
        try{    
            objEOI = new EOI_Process__c();
            objEOI.Property__c = propertySelected;
            if(String.isNotBlank(inquiryId)){
                objEOI.Inquiry__c = inquiryId;
            }
            objEOI.No_of_Units__c = unitSelected;
            objEOI.Mode_of_Token_Payment__c = paymentModeSelected;
            objEOI.Token_Amount__c = tokenamtSelected;
            objEOI.EOI_Comments__c = comments;
            objEOI.Agency__c = userRecord.Accountid;
            system.debug('objEOI---'+objEOI);
            insert objEOI;
            system.debug('objEOI---'+objEOI);
            EOI_Process__c obj = [Select id from EOI_Process__c where id = :objEOI.id];
            system.debug('obj------------'+obj);

            return obj.id; 
        } catch(exception e) {
            //validate = e.getMessage();
            system.debug('validate'+e.getMessage());
            return null;
        }
    }

    private static Attachment getAttachment(String attId) {
        list<Attachment> attachments = [SELECT Id, Body
                                        FROM Attachment 
                                        WHERE Id =: attId];
        if(attachments.isEmpty()) {
            Attachment a = new Attachment();
            return a;
        } else {
            return attachments[0];
        }
    }

    @RemoteAction
    public static String doUploadAttachment(String EoiId,String attachmentBody, String attachmentName, String attachmentId) {
        //String EoiId = objEOI.id;
        system.debug('>>>obj'+EoiId);
        if(EoiId != null) {
            EOI_Process__c  objEOI = [Select id,Name from EOI_Process__c WHERE id =:EoiId];
            if(EoiId != null) {
                if(attachmentBody != null) {
                    Attachment att = getAttachment(attachmentId);
                    String newBody = '';
                    if(att.Body != null) {
                        newBody = EncodingUtil.base64Encode(att.Body);
                    }
                    newBody += attachmentBody;
                    att.Body = EncodingUtil.base64Decode(newBody);
                    if(attachmentId == null) {
                        att.Name = attachmentName;
                        att.parentId = objEOI.Id;
                        //att.Description = description;
                    }
                    try{
                        upsert att;
                        //objDoc.NSIBPM__Status__c = 'Uploaded';
                        //upsert objDoc;
                    }catch(Exception e){
                        system.debug('>>>>Exception'+e);
                    }
                    return att.Id;
                } else {
                    return 'Attachment Body was null';
                }
            } else {
                return 'Document could not be found';
            }
        } else {
            return 'Document Id is null';
        }
        return null;
    }
}