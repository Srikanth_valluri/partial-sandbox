public without sharing Class CasePostDatedChequeCntrl {

    public String strCPDCId {get;set;}
    
    public CasePostDatedChequeCntrl(ApexPages.StandardController stdController) {
        strCPDCId = (Id)stdController.getRecord().Id;
    }
    
    public void init() {
        try {
            PDCReceiptGenerator.generatePDCReceipt(new List<Id>{strCPDCId});
            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.INFO 
                , 'Batch executed to to create reciept.Please check after sometime.' 
                )
            );
        }
        catch( Exception e ) {
            ApexPages.addmessage(new ApexPages.message( ApexPages.severity.ERROR , e.getMessage() ) );
        }
        
    }
    
    public pageReference back(){
        pageReference pgr = new pageReference('/'+strCPDCId);
        pgr.setRedirect(true);
        return pgr;
    }
}