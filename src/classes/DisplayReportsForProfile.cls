public class DisplayReportsForProfile {
    public String qlikDashBoardURL { get; set; }
    public String userProfileName  { get; set; }
    //public List<QLIK_Dasboards_URL__c> qlikDashBoardList ;

    public DisplayReportsForProfile(){
       // qlikDashBoardList = new  List<QLIK_Dasboards_URL__c>();
        userProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name;
        system.debug('>>userProfileName>>>>'+userProfileName);
        QLIK_Dasboards_URL__c qlikDashBoardObj = QLIK_Dasboards_URL__c.getValues(userProfileName);                     
        if(qlikDashBoardObj != null){
            qlikDashBoardURL = qlikDashBoardObj.DashBoard_URL__c; 
        }                        
                               
    }
}