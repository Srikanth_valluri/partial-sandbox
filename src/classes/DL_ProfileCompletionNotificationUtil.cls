/**********************************************************************************************************************
Description: Util class with methods, being called from trigger(AccountTrgHandler) to send email notification to 'DAMAC Living App Users' regarding profile completion.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By   | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     | 02-11-2020        | Subin Antony (QBurst)   | Initial Draft
-----------------------------------------------------------------------------------------------------------------------
1.1     | 12-11-2020        | Subin Antony (QBurst)   | Moved push notification method to Queueable apex (DL_SendPromoCouponEmailAsync) : 
														to fix error while Trigger context being called from future method, 'COCDFinalUpdate.UpdateNameNationalityInIpms()' 
***********************************************************************************************************************/
global class DL_ProfileCompletionNotificationUtil {
    public static Set<Account> filterCompletedProfileOwners(Map<ID, Account> newMap, Map<ID, Account> oldMap) {
        Map<ID, Account> partyWithCompletedProfile_Map = new Map<ID, Account>(); /* Map<ID, Account> partyWithIncompleteProfile_Map; */
        Set<Account> completedProfileOwners = new Set<Account>();
        if(NULL == newMap || newMap.values().size() < 1) {
            return completedProfileOwners;
        }
        
        for(Account customerAcc : newMap.values()) {
            Boolean profileCompleted = false;
            if(customerAcc.Party_Type__c == 'PERSON' && customerAcc.Is_DAMAC_Living_App_user__c) { /* checking only person accounts  */
                profileCompleted = 
                    (String.isNotBlank(customerAcc.Passport_Number__pc) && 
                     NULL != customerAcc.Passport_Expiry_Date__pc && (customerAcc.Passport_Expiry_Date__pc >= Date.today()) && 
                     (String.isNotBlank(customerAcc.Address_Line_1__pc) || String.isNotBlank(customerAcc.Address_Line_2__pc) || 
                      String.isNotBlank(customerAcc.Address_Line_3__pc) || String.isNotBlank(customerAcc.Address_Line_4__pc)) && 
                     String.isNotBlank(customerAcc.City__pc) && 
                     String.isNotBlank(customerAcc.Country__pc) && 
                     String.isNotBlank(customerAcc.Mobile_Phone_Encrypt__pc) && 
                     String.isNotBlank(customerAcc.Email__pc) && 
                     (String.isNotBlank(customerAcc.Mobile_Phone_Encrypt_2__pc) || String.isNotBlank(customerAcc.Mobile_Phone_Encrypt_3__pc) || 
                      String.isNotBlank(customerAcc.Mobile_Phone_Encrypt_4__pc) || String.isNotBlank(customerAcc.Mobile_Phone_Encrypt_5__pc)) && 
                     (String.isNotBlank(customerAcc.Email_2__pc) || String.isNotBlank(customerAcc.Email_3__pc) || 
                      String.isNotBlank(customerAcc.Email_4__pc) || String.isNotBlank(customerAcc.Email_5__pc)) && 
                     String.isNotBlank(customerAcc.WhatsApp_Number__c) );
            }
            System.debug('ACCOUNT[' + customerAcc.Party_Type__c + ' : ' + customerAcc.id + ' : ' + customerAcc.name + '] > PROFILE COMPLETION CHECK : ' + profileCompleted);
            
            if(profileCompleted) { /* except for `passport document`, profile is complete  */
                partyWithCompletedProfile_Map.put(customerAcc.id, customerAcc);
            } /* else {partyWithIncompleteProfile_Map.put(customerAcc.id, customerAcc);} */
        }
        
        // Filter out owner accounts only
        if(NULL != partyWithCompletedProfile_Map && partyWithCompletedProfile_Map.size() > 0) {
        	completedProfileOwners = filterOutOwnersAndTenants(partyWithCompletedProfile_Map).owners;
        } else {
            completedProfileOwners = new Set<Account>(); 
        }
        
        Set<ID> completedOwnerIds = new Set<ID>();
        if(NULL != completedProfileOwners && completedProfileOwners.size() > 0) {
            for(Account completedOwner : completedProfileOwners) {
                completedOwnerIds.add(completedOwner.id);
            }
        } else {
            System.debug('NO ACCOUNTS WITH COMPLETED PROFILE DATA');
            return completedProfileOwners;
        }
        
        List<SR_Attachments__c> attachedDocs = new List<SR_Attachments__c>();
        try {
            if(NULL != completedOwnerIds && completedOwnerIds.size() > 0) {
            	attachedDocs = [SELECT id, name, Attachment_URL__c, Account__c FROM SR_Attachments__c 
                                WHERE Name LIKE '%Customer Passport%'  AND Account__c IN :completedOwnerIds];
            }
        }
        catch(Exception ex) {
            system.debug(ex.getMessage() + ' : ' + ex.getStackTraceString());
        }
        
        Set<Account> completedAccSet = new Set<Account>();
        if(NULL != attachedDocs && attachedDocs.size() > 0) {
            for(SR_Attachments__c doc : attachedDocs) {
                if(NULL != newMap.get(doc.Account__c)) 
                    completedAccSet.add(newMap.get(doc.Account__c));
            }
        }
        System.debug('COUNT OF PROFILE COMPLETED ACCOUNTS : ' + completedAccSet.size());
        return completedAccSet;
    }
    
    public static void sendPromoCouponNotification(Set<Account> completedProfileOwners) {
        if(NULL != completedProfileOwners && completedProfileOwners.size() > 0) {
            for(Account ownerAcc : completedProfileOwners) {
                String ownerEmail = (ownerAcc.Party_Type__c == 'PERSON' ? ownerAcc.Email__pc : ownerAcc.Email__c);
                /* Account is updated from 'COCDFinalUpdate.UpdateNameNationalityInIpms()' which is a future method.
                 * Hence using queueable apex instead of future method */
                try {
                    DL_SendPromoCouponEmailAsync queuedNotificationEmail = new DL_SendPromoCouponEmailAsync(ownerAcc.id, ownerEmail);
                    ID queuedNotificationJobID = System.enqueueJob(queuedNotificationEmail);
                    System.debug('PROFILE COMPLETION NOTIFICATION EMAIL SEND JOB ENQUEUED WITH JOB ID : ' + queuedNotificationJobID);
                }
                catch(Exception ex) {
                    system.debug('ERROR WHILE ENQUEUEING PROFILE COMPLETION NOTIFICATION EMAIL SEND JOB');
                    system.debug(ex.getMessage());
                    system.debug(ex.getStackTraceString());
                }
            } 
        }
    }
    
    public static SegregatedCustomerWrap filterOutOwnersAndTenants(Map<ID, Account> customerMap) {
        SegregatedCustomerWrap filteredCustomers = new SegregatedCustomerWrap();
        Set<Account> owners = new Set<Account>();
        Set<Account> tenants = new Set<Account>();
        
        Set<ID> custAccIdSet = customerMap.keySet();
        List<User> lstUser;
        if(NULL != custAccIdSet && custAccIdSet.size() > 0) {
            lstUser = [SELECT id, Username, FullPhotoUrl, Profile.Name, Contact.AccountId 
                       FROM User WHERE Contact.AccountId IN :custAccIdSet];
        }
        if(NULL != lstUser && !lstUser.isEmpty() && !Test.isRunningTest()) {
            for(User usr : lstUser) {
                String usrProfileName = usr.Profile.Name;
                if(String.isNotBlank(usrProfileName) && usrProfileName.equalsIgnoreCase('Tenant Community Login User')) {
                    tenants.add(customerMap.get(usr.Contact.AccountId));
                } else {
                    owners.add(customerMap.get(usr.Contact.AccountId));
                }
            }
        }
        
        if(Test.isRunningTest()) {
            owners = new Set<Account>(customerMap.values());
            tenants = new Set<Account>();
        }
        
        filteredCustomers.owners = owners;
        filteredCustomers.tenants = tenants;
        return filteredCustomers;
    }
    
    public class SegregatedCustomerWrap {
        public Set<Account> owners;
        public Set<Account> tenants;
    }
}