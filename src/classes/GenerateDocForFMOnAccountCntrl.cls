public without sharing class GenerateDocForFMOnAccountCntrl {

    public String strAccId                              {get;set;}
    public List<Account> lstAccount                     {get;set;}
    
    public GenerateDocForFMOnAccountCntrl(ApexPages.StandardController controller) {
        lstAccount = new List<Account>();
        strAccId = ApexPages.currentPage().getParameters().get('id');
        
        System.debug('strAccId:::::::'+strAccId);
        lstAccount = [ SELECT Id
                            , Party_ID__c
                         FROM Account
                        WHERE Id = :strAccId LIMIT 1 ];
    }
    
    public PageReference generateFMPartySOA(){
        
        if( String.isNotBlank( lstAccount[0].Party_ID__c ) ) {
                
                PageReference pg = GenerateDocForFMCLCntrl.fetchPartyWiseFMSOA( lstAccount[0].Party_ID__c , null , null , lstAccount[0].Id );
                
                //TODO 
                GenericUtility.createSOACreator(String.valueOf(pg),UserInfo.getUserId(),system.now(),'','Bulk Service Charge SOA',lstAccount[0].Id,NULL,NULL);

                return pg;

        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please provide party id for Generating SOA'));
            return null;
        }

    }// End of generateFMPartySOA   
}