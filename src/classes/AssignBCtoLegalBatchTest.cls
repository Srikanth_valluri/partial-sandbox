@isTest
public class AssignBCtoLegalBatchTest {
    @isTest
    static void testBatch(){
    
        Id recTypeBouncedChq = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bounced Cheque SR').getRecordTypeId();
        
        List<TriggerOnOffCustomSetting__c>settingLst = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst.add(newSetting1);
        insert settingLst;
        Date caseCreateDate = system.today().addDays(-3);
        Case objCase = new Case();
        objCase.SR_with_Legal__c = true;
        objCase.Status = 'Open';
        objCase.RecordTypeId = recTypeBouncedChq;
        //objCase.CreatedDate = System.today()+1;
        insert objCase;
        Test.setCreatedDate(objCase.Id, caseCreateDate ); 
        
        Task objTask = new Task();
        objTask.Subject = 'Procedd with legal action';
        objTask.WhatId = objCase.Id;
        //insert objTask;
        
        Calling_List__c objCL = new Calling_List__c();
        objCL.Case__c = objCase.Id;
        insert objCL;
        
        Test.startTest();
            AssignBCtoLegalBatch objAssign = new AssignBCtoLegalBatch();
            Database.executeBatch(objAssign);
        Test.stopTest();
    }
}