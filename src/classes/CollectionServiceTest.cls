/********************************************************************************************************************************
 * Description : Test class for collection service 
 *===============================================================================================================================
 * Ver      Date-DD/MM/YYYY     Author              Modification
 *===============================================================================================================================
 * 1.0      06/08/2020          Aishwarya Todkar    Initial Draft
*********************************************************************************************************************************/
@isTest
public class CollectionServiceTest {
    @isTest
    static void getBearerTokenTest(){
        Credentials_details__c objcreds = new Credentials_details__c();
        objcreds.Endpoint__c = 'https://ptctest.damacgroup.com/COFFEE/apex/login?client-id=';
        objcreds.Name ='Collection Service';
        insert objcreds;
        
        Credentials_details__c creds = new Credentials_details__c();
        creds.Endpoint__c = 'https://ptctest.damacgroup.com/COFFEE/apex/login?client-id=';
        creds.Name ='Collection Service Token';
        creds.Password__c = '8SIFW78-R0RXV-H1QZF-RSIRNU6';
        insert creds;
        
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new CollectionServiceMockHttp());
            CollectionResponse collRes = CollectionService.getUnitsDetails('760002');
        Test.stopTest();
        System.assert(collRes != null);
    }
}