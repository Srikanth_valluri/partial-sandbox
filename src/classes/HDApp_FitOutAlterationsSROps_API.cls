/**********************************************************************************************************************
Description: This API is used for getting the Fit Out types applicable for given bookingId
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   11-11-2020      | Anand Venkitakrishnan | Created initial draft
1.1     |   12-11-2020      | Shubham Suryawanshi   | Added method API method for Draft save & Submit
1.1     |   29-11-2020      | Shubham Suryawanshi   | Added accountId in request params & updated isTenant flag for exclusion of Ejari doc for owners
***********************************************************************************************************************/

@RestResource(urlMapping='/fitOutAlterationsOps/*')
global class HDApp_FitOutAlterationsSROps_API {

    public static String errorMsg;
    public static Integer statusCode;
    public static String unitRegId;
    public static String partyId;
    public static Decimal vatRate =  Decimal.valueOf(label.FitOut_Alteration_VAT_Percent);
    public static final String commonErrorMsg = 'This may be because of technical error that we\'re working to get fixed. Please try again later.';

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    public static final String TNC = 'terms & conditions';
    public static final String GENERAL_UNDERTAKING_LETTER = 'general undertaken letter';
    public static final String AUTHORISATION_LETTER = 'authorisation letter';

    public static final String TNC_URL = 'https://damacholding.my.salesforce.com/sfc/p/#0Y000000ZkVQ/a/1n000000x89p/CgRUgb2X8ywrytAXEYhOUsp2TntYXHSok5zzs29R6H8';
    public static final String AUTHORISATION_LETTER_URL = 'https://damacholding--fullcopy.my.salesforce.com/sfc/p/1w0000008avl/a/1w0000004QE1/UNrg16giu51yc6eR9oIaQsQr7OjSMPDzacH2MpBPLPo';
    
    /**********************************************************************************************************************
    Description : Method to get Initial details for FitOut/Alteration SR
    Parameter(s):  NA
    Return Type : FinalReturnWrapper 
    **********************************************************************************************************************/
    @HttpGet
    global static FinalReturnWrapper getSRTypeDetails() {
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        
        RestRequest r = RestContext.request;
        System.debug('Request params:'+r.params);
        
        cls_meta_data objMeta = new cls_meta_data();
        
        List<cls_data> lstData = new List<cls_data>();
        
        if(!r.params.containsKey('bookingUnitId')) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing parameter : bookingUnitId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing parameter value : bookingUnitId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        //Currently we're not checking for this.. As per discussed with Charith.
        /*if(!r.params.containsKey('accountId')) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing parameter : accountId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId'))) {
            objMeta = ReturnMetaResponse('','Missing parameter value : accountId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }*/

        
        String bookingUnitId = r.params.get('bookingUnitId');
        String accountId = r.params.containsKey('accountId') && String.isNotBlank(r.params.get('accountId')) ? r.params.get('accountId') : null;
        System.debug('accountId: ' + accountId);

        List<Booking_Unit__c> lstBU = [SELECT Id, Unit_Name__c, Inventory__r.Building_Location__r.New_Ho_Doc_Type__c, Resident_Type__c, Tenant__c, Owner__c, Resident__c
                                           FROM Booking_Unit__c
                                           WHERE id =: bookingUnitId];
        
        if(lstBU.size() > 0) {
            List<cls_data> foTypeList = getFODetailsListForAccount(lstBU[0], accountId);
            lstData = foTypeList;
            objMeta = ReturnMetaResponse('successful','', 1);
            
            returnResponse.meta_data = objMeta;
            returnResponse.data = lstData;
            
            System.debug('returnResponse: ' + returnResponse);
            return returnResponse;
        }
        else {
            objMeta = ReturnMetaResponse('No Booking Unit found for the given bookingUnitId','', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
    }
    
    public static List<cls_data> getFODetailsListForAccount(Booking_Unit__c objBU, String accountId) {
        
        String bookingUnitType = objBU.Inventory__r.Building_Location__r.New_Ho_Doc_Type__c;
        //Boolean isTenant = String.isNotBlank(objBU.Resident_Type__c) && objBU.Resident_Type__c.equalsIgnoreCase('tenant') ? true : false;
        Boolean isTenant = String.isNotBlank(objBU.Resident_Type__c) && objBU.Resident_Type__c.equalsIgnoreCase('tenant') && accountId != null && objBU.Tenant__c == accountId ? true : false;

        System.debug('isTenant: ' + isTenant);
        System.debug('bookingUnitType: ' + bookingUnitType);
        if(String.isBlank(bookingUnitType)) {
            bookingUnitType = 'Apartment';
        }

        List<cls_data> lstFODetails = new List<cls_data>();

        Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;
        System.debug('IsSandbox is: '+isSandbox);
        
        List<Damac_Living_FitOut_Alterations__mdt> lstFoAlterations = [SELECT  Id
                                                                             , Work_Type_Name__c
                                                                             , Document_List__c
                                                                             , Price__c
                                                                             , NOC_Type__c
                                                                             , Work_Type_Id__c
                                                                             , Undertaking_Letter_URL__c
                                                                      FROM Damac_Living_FitOut_Alterations__mdt WHERE Applicable_Unit_Type__c =: bookingUnitType];
        
        for(Damac_Living_FitOut_Alterations__mdt foAlt: lstFoAlterations) {

            cls_data foRecord = new cls_data();
            foRecord.type_of_work = foAlt.Work_Type_Name__c;
            List<String> docList = foAlt.Document_List__c.replace('\r','').split('\n');
            List<FitOutDocWrapper> lstFODocs = new List<FitOutDocWrapper>();   
            for(String objStr : docList) {
                if(Test.isRunningTest()) {
                    objStr = 'Test doc-1';
                }

                if(objStr.containsIgnoreCase('General undertaken letter') && objStr.containsIgnoreCase('authorization letter from occupier')) {
                    continue;   //to skip doc - General undertaken letter/Authorization letter from occupier-2

                }
                if(!isTenant && objStr.containsIgnoreCase('NOC from the owner')) {
                    continue;   //To skip "NOC from the owner (only for tenant) doc" is Resident is not tenant
                }
                if(!isTenant && objStr.containsIgnoreCase('Ejari')) {
                    continue;   //To skip 'Title deed/Ejari contract' doc for Owner;
                }
                FitOutDocWrapper objFODoc = new FitOutDocWrapper();
                objFODoc.document_name = objStr.split('-')[0];
                objFODoc.doc_id = Integer.valueOf(objStr.split('-')[1]);
                lstFODocs.add(objFODoc);
            }
            foRecord.documents_required = lstFODocs;
            foRecord.price = foAlt.Price__c;
            foRecord.noc_types = foAlt.NOC_Type__c.replace('\r','').split('\n');
            foRecord.type_of_work_id = Integer.valueOf(foAlt.Work_Type_Id__c);

            //Newly added for Undertaking doc list
            List<UndertakingDocWrapper> undertakingDocList = new List<UndertakingDocWrapper>();
            UndertakingDocWrapper objUTDoc = new UndertakingDocWrapper();
            objUTDoc.document_name = TNC;
            objUTDoc.document_url = TNC_URL;
            undertakingDocList.add(objUTDoc);

            objUTDoc = new UndertakingDocWrapper();
            objUTDoc.document_name = AUTHORISATION_LETTER;
            objUTDoc.document_url = AUTHORISATION_LETTER_URL;
            undertakingDocList.add(objUTDoc);

            objUTDoc = new UndertakingDocWrapper();
            objUTDoc.document_name = GENERAL_UNDERTAKING_LETTER;
            objUTDoc.document_url = foAlt.Undertaking_Letter_URL__c;
            undertakingDocList.add(objUTDoc);

            foRecord.undertaking_documents = undertakingDocList;


            lstFODetails.add(foRecord);
        }
        
        return lstFODetails;
    }

    /**********************************************************************************************************************
    Description : Method to create Draft FitOut Alteration SR
    Parameter(s):  NA
    Return Type : FinalCaseReturnWrapper 
    **********************************************************************************************************************/
    @HttpPut
    global static FinalCaseReturnWrapper createDraftFitOutSR() {

        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString'+jsonString);
        System.debug('Request params:' + req.params);

        FinalCaseReturnWrapper returnResponse = new FinalCaseReturnWrapper();
        FmCaseWrapper objFmWrap = new FmCaseWrapper(); 
        cls_meta_data objMeta = new cls_meta_data();
        cls_case_data objData = new cls_case_data();

        if(String.isBlank(jsonString)) {
            objMeta = ReturnMetaResponse('No JSON body found in request','No JSON body found in request', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        System.debug('jsonString:: ' + jsonString);
        objFmWrap = (FmCaseWrapper)JSON.deserialize(jsonString, FmCaseWrapper.class);
        System.debug('objFmWrap: ' + objFmWrap);

        if(objFmWrap.action == null || (!objFmWrap.action.equalsIgnoreCase('draft') )) {
            objMeta = ReturnMetaResponse('Internal processing error','Invalid parameter value in : action', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;

        }

        if( String.isNotBlank(objFmWrap.contractor_email) && !HDAppInsertTenantRegistrationCase.validateEmailPattern(objFmWrap.contractor_email)) {
            objMeta = ReturnMetaResponse('Invalid email format','Please enter a valid contractor email address', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        

        if(objFmWrap != null) {
            System.debug('action in InsertTenantRegCase: ' + objFmWrap.action);

            FM_Case__c objFM = ProcessFMCase(objFmWrap, objFmWrap.action);
            System.debug('objFM: ' + objFm);

            List<FM_Case__c> fitOutCase = objFM != null ? GetTenantRegistrationCase_API.getFMCase(objFM.id) : null;
            objData = fitOutCase != null && !fitOutCase.isEmpty() ? clsDataCreator(fitOutCase[0]) : null;

            System.debug('errorMsg:' + errorMsg);
            System.debug('statusCode:' + statusCode);
            objMeta = errorMsg == 'Successful'? ReturnMetaResponse(errorMsg,errorMsg, statusCode) : ReturnMetaResponse('Something went wrong',errorMsg, statusCode);
        }
        else {
            objMeta = String.isNotBlank(errorMsg) && statusCode != 1 ? ReturnMetaResponse(errorMsg, errorMsg, statusCode) : ReturnMetaResponse('No FM Case found/Created', '', 2);
        }

        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;

        return returnResponse;
    }

    public Static FM_Case__c ProcessFMCase( FmCaseWrapper objFmCaseWrap, String action ) {

        Booking_Unit__c unit = new Booking_Unit__c();
        if(String.isNotBlank(objFmCaseWrap.booking_unit_id)) {
            unit = HDApp_AccessCardSR_API.getUnitDetails(objFmCaseWrap.booking_unit_id);
        }
        else {
            errorMsg = 'Missing parameter value : booking_unit_id in JSON request body';
            statusCode = 3;
            return null;
        }

        if(unit == null) {

            errorMsg = 'No booking unit found for given booking_unit_id';
            statusCode = 3;
            return null;
        }

        if(String.isBlank(objFmCaseWrap.account_id)) {
            errorMsg = 'No account_id found in request';
            statusCode = 3;
            return null;
        }

        Account objAcc = HDApp_MoveOutSR_API.getCurrentAccountDetails(objFmCaseWrap.account_id);
        System.debug('objAcc:: ' + objAcc);

        System.debug('objFmCaseWrap:: ' + objFmCaseWrap);
        FM_Case__c objCase = new FM_Case__c();

        if(String.isNotBlank(objFmCaseWrap.id)) {       //When case is already created
            objCase.id = objFmCaseWrap.id;
        }

        //Populating common entities
        objCase.isHelloDamacAppCase__c = true;
        objCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('NOC For FitOut').getRecordTypeId();
        objCase.Account__c =  objAcc.id;
        objCase.Account_Email__c = objAcc.Email__pc != NULL ? objAcc.Email__pc : '';
        objCase.Booking_Unit__c =  unit.id;
        objCase.Status__c = 'In Progress';
        objCase.Origin__c = 'Portal';
        objCase.Request_Type__c = 'NOC for Fit out/Alterations';
        objCase.Request_Type_DeveloperName__c = 'NOC_for_Fit_out_Alterations';

        //Populating Fit-Out SR specific fields
        Damac_Living_FitOut_Alterations__mdt worktypeMDT = getWorkTypeDetailsFromMDT(objFmCaseWrap.type_of_work_id);
        objCase.Type_of_Alteration__c = worktypeMDT != null ? worktypeMDT.Work_Type_Name__c.trim() : null;
        objCase.Type_of_NOC__c = objFmCaseWrap.noc_type;
        objCase.Description__c = objFmCaseWrap.description;
        objCase.Person_To_Collect__c = objFmCaseWrap.collection_by;
        objCase.Contact_person_contractor__c = objFmCaseWrap.contractor_name != null ? objFmCaseWrap.contractor_name : null;
        objCase.Email_2__c = objFmCaseWrap.contractor_email != null ? objFmCaseWrap.contractor_email : null; 
        objCase.Mobile_Country_Code_2__c = objFmCaseWrap.contractor_mobile_country_code != null ? objFmCaseWrap.contractor_mobile_country_code : null;
        objCase.Mobile_no_contractor__c = objFmCaseWrap.contractor_mobile != null ? objFmCaseWrap.contractor_mobile : null;
        objCase.Noc_for_Fitout_Alterations_Fee__c = getWorkTypeAmountwithVAT(worktypeMDT.Price__c, vatRate); 
        //Get confirmation on this fields also - NOC_Fees_Including_VAT__c

        try {
            System.debug('objCase before insert: ' + objCase);
            upsert objCase;
            errorMsg = 'Successful';
            statusCode = 1;
            System.debug('objCase after insert: ' + objCase);
        }
        catch(Exception e) {
            System.debug('Exception: ' + e.getMessage());
            errorMsg = e.getMessage();
            statusCode = 2;
        }

        return objCase;
    }

    public static Damac_Living_FitOut_Alterations__mdt getWorkTypeDetailsFromMDT(Integer workTypeId) {
        List<Damac_Living_FitOut_Alterations__mdt> lstFoAlterations = [SELECT Id
                                                                             , Work_Type_Name__c
                                                                             , Document_List__c
                                                                             , Price__c
                                                                             , NOC_Type__c 
                                                                             , Work_Type_Id__c
                                                                      FROM Damac_Living_FitOut_Alterations__mdt 
                                                                      WHERE Work_Type_Id__c =: workTypeId
                                                                      ];
        System.debug('lstFoAlterations: ' + lstFoAlterations);                                                              
        return lstFoAlterations.size() > 0 ? lstFoAlterations[0] : null;
    }

    public static cls_case_data clsDataCreator(FM_Case__c objFM) {
        System.debug('objFM in clsDataCreator : ' + objFM);
        if(objFM == null) {
            return null;
        }
        cls_case_data dataObj = new cls_case_data();

        dataObj.booking_unit_id = String.isNotBlank(objFM.Booking_Unit__c) ? objFM.Booking_Unit__c : null;
        dataObj.booking_unit_name = String.isNotBlank(objFM.Unit_Name__c) ? objFM.Unit_Name__c : null;
        dataObj.sr_id = objFM.id;
        dataObj.sr_number = objFM.Name;
        dataObj.sr_type = objFM.Type_of_Alteration__c;
        dataObj.sr_status = objFM.Status__c;
        dataObj.sr_submission_date = objFM.Submission_Date__c != null ? Datetime.newInstance(objFM.Submission_Date__c.year(), objFM.Submission_Date__c.month(), objFM.Submission_Date__c.day()).format('yyyy-MM-dd') : '';
        dataObj.sr_approval_status = objFM.Approval_Status__c != null ? objFM.Approval_Status__c : 'Pending';
        dataObj.amount = objFM.Noc_for_Fitout_Alterations_Fee__c != null ? objFM.Noc_for_Fitout_Alterations_Fee__c : null;

        list<cls_attachment> lstAttachments = new list<cls_attachment>();

        if(objFM.Documents__r.size() > 0) {
            System.debug('objFM.Documents__r'+objFM.Documents__r);
            for(SR_Attachments__c obj : objFM.Documents__r) {
                cls_attachment objAttach = new cls_attachment();
                objAttach.id = obj.id;
                objAttach.name = obj.Name;
                objAttach.file_extension = obj.Type__c;
                objAttach.attachment_url = obj.Attachment_URL__c;

                lstAttachments.add(objAttach);
            }
        }
        dataObj.attachments = lstAttachments;

        System.debug('dataObj:: ' + dataObj);
        return dataObj;
    }

    /**********************************************************************************************************************
    Description : Method to submit the created fitOut/Alteration SR
    Parameter(s):  NA
    Return Type : FinalCaseReturnWrapper 
    **********************************************************************************************************************/
    @httpPatch
    global static FinalCaseReturnWrapper submitFitOutSR() {

        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString'+jsonString);
        //System.debug('Request params:' + req.params);

        FinalCaseReturnWrapper returnResponse = new FinalCaseReturnWrapper();
        FitOutSRSubmitWrap objSubmitWrap = new FitOutSRSubmitWrap(); 
        cls_meta_data objMeta = new cls_meta_data();
        cls_case_data objData = new cls_case_data();

        if(String.isBlank(jsonString)) {
            objMeta = ReturnMetaResponse('No JSON body found in request','No JSON body found in request', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        System.debug('jsonString:: ' + jsonString);
        objSubmitWrap = (FitOutSRSubmitWrap)JSON.deserialize(jsonString, FitOutSRSubmitWrap.class);
        System.debug('objSubmitWrap: ' + objSubmitWrap);

        if(objSubmitWrap.action == null || (!objSubmitWrap.action.equalsIgnoreCase('submit') )) {
            objMeta = ReturnMetaResponse('Internal processing error','Invalid parameter value in : action', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        if(String.isBlank(objSubmitWrap.id)) {
            objMeta = ReturnMetaResponse('Internal processing error','Missing parameter value: id', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        List<FM_Case__c> draftFitOutCase = GetTenantRegistrationCase_API.getFMCase(objSubmitWrap.id);
        if( draftFitOutCase.isEmpty() || draftFitOutCase == null) {
            objMeta = ReturnMetaResponse('No case found','No any FM case found for given fmCaseId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;  
        }

        draftFitOutCase[0].Status__c = 'Submitted';
        draftFitOutCase[0].FitOut_Alterations_Payment_Receipt_No__c = objSubmitWrap.receipt_number;

        //Stamping Admin__c value on FM Case
        if( String.isNotBlank( draftFitOutCase[0].Unit_Name__c ) ) {
            list<FM_User__c> lstUsers = FM_Utility.getApprovingUsers( new set<String> { draftFitOutCase[0].Unit_Name__c.split('/')[0] } );
            for( FM_User__c objUser : lstUsers ) {
                if( String.isNotBlank( objUser.FM_Role__c ) ) {
                    if( objUser.FM_Role__c.containsIgnoreCase( 'Manager') ) {
                        draftFitOutCase[0].FM_Manager_Email__c = objUser.FM_User__r.Email;
                    }
                    else if( objUser.FM_Role__c.containsIgnoreCase( 'Admin') &&
                             draftFitOutCase[0].Admin__c == NULL ) {
                        draftFitOutCase[0].Admin__c = objUser.FM_User__c;
                    }
                }
            }
        }

        //Identifying the building type i.e property type
        //Get MAster Community property names from label
        List<String> lstPropertyNames = new List<String>();
        lstPropertyNames = Label.DAMAC_Hills_Properties.split(',');
        String buildingType = lstPropertyNames.contains(draftFitOutCase[0].Booking_Unit__r.Property_Name__c) ?
                                Label.FM_Master_Community_Label : Label.FM_Other_Building_label;
        //draftFitOutCase[0].Approval_Status__c = 'Pending';
        //draftFitOutCase[0].Submit_for_Approval__c = true ;
        draftFitOutCase[0].Approving_Authorities__c = '';
        if( String.isNotBlank( draftFitOutCase[0].Request_Type_DeveloperName__c ) ) {
            list<FM_Approver__mdt> lstApprovers = [SELECT Id
                                                        , Role__c
                                                        , DeveloperName
                                                        , FM_Process__r.DeveloperName
                                                     FROM FM_Approver__mdt
                                                    WHERE FM_Process__r.DeveloperName = 'NOC_for_Fit_out_Alterations'
                                                      AND Is_Active__c = true
                                                      AND City__c IN ( 'All', :draftFitOutCase[0].Booking_Unit__r.Property_City__c )
                                                      AND Building_Type__c =: buildingType
                                                 ORDER BY Level__c ASC] ;
            for( FM_Approver__mdt objApproMeta : lstApprovers ) {
                draftFitOutCase[0].Approving_Authorities__c += objApproMeta.Role__c + ',';
            }
            draftFitOutCase[0].Approving_Authorities__c = draftFitOutCase[0].Approving_Authorities__c.removeEnd(',');
        }

        //Uploading doc to O365 & creating its corresponding Sr_Attachment record
        UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
        list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
        if(String.isNotBlank(objSubmitWrap.receipt_url)) {
            HttpRequest request = new HttpRequest();
            request.setEndpoint(objSubmitWrap.receipt_url);
            request.setHeader('Accept', 'application/json');
            request.setMethod('GET');
            //request.setHeader('Authorization','Bearer' + accessToken);
            request.setTimeout(120000);

            HttpResponse response = new HttpResponse();
            if(!Test.isRunningTest()) {
                response = new Http().send(request);
            }
            else {
                response = new HttpResponse();   
            }
            //HttpResponse response = new Http().send(request);
            System.debug('Status Code = ' + response.getStatusCode());
            System.debug('Pdf Body = ' + response.getBodyAsBlob());

            if( response.getStatusCode() == 200 && response.getBodyAsBlob() != null ) {
                String requestString = EncodingUtil.Base64Encode(response.getBodyAsBlob());
                if(string.isNotBlank( requestString ) ) {
                    lstWrapper.add( FM_Utility.makeWrapperObject(  requestString,
                                                                   'FitOut/Alteration payment receipt' ,
                                                                   'FitOut/Alteration Pay receipt.pdf' ,
                                                                   draftFitOutCase[0].Name, '1' ) );
                }
                System.debug('== lstWrapper =='+lstWrapper);
            }
        }

        //Inserting SR Attachment
        if( !lstWrapper.isEmpty() ) {
            objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper, 'FM' );
            system.debug('== objResponse document upload =='+objResponse);
            system.debug('== objResponse.data =='+objResponse.data);
            if( objResponse != NULL && objResponse.data != NULL ) {
                InsertAttachment_API.AttachmentWrapper objSRAttach = InsertAttachment_API.insertCustomAttachment( draftFitOutCase[0], 'FitOut/Alteration payment receipt', objResponse, '', 'FitOut/Alteration Pay receipt.pdf');
                System.debug('objSRAttach:: ' + objSRAttach);
            }

        }

        System.debug('draftFitOutCase before submit:: ' + draftFitOutCase);
        update draftFitOutCase;

        //Fetch FM case post update DML 
        List<FM_Case__c> lstSubmittedFmCase = GetTenantRegistrationCase_API.getFMCase(draftFitOutCase[0].id);
        objData = clsDataCreator(lstSubmittedFmCase[0]);
        objMeta = ReturnMetaResponse('Successful','', 1);

        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;

        return returnResponse;

    }

    /**********************************************************************************************************************
    Description : Method to get the amount with calculation details for work type being requested
    Parameter(s):  NA
    Return Type : AmountDetailsReturnWrapper 
    **********************************************************************************************************************/
    @HttpPost
    global static AmountDetailsReturnWrapper getWorkTypeAmountDetails(){

        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString'+jsonString);
        //System.debug('Request params:' + req.params);

        AmountDetailsReturnWrapper returnResponse = new AmountDetailsReturnWrapper();
        cls_get_amount_details_input_wrap objInputWrap = new cls_get_amount_details_input_wrap(); 
        cls_meta_data objMeta = new cls_meta_data();
        cls_amount_data objData = new cls_amount_data();

        if(String.isBlank(jsonString)) {
            objMeta = ReturnMetaResponse('No JSON body found in request','No JSON body found in request', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        System.debug('jsonString:: ' + jsonString);
        objInputWrap = (cls_get_amount_details_input_wrap)JSON.deserialize(jsonString, cls_get_amount_details_input_wrap.class);
        System.debug('objInputWrap: ' + objInputWrap);

        if(String.isBlank(objInputWrap.booking_unit_id)) {
            objMeta = ReturnMetaResponse('Internal processing error','Missing parameter value: booking_unit_id', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        if(objInputWrap.type_of_work_id == null) {
            objMeta = ReturnMetaResponse('Internal processing error','Missing parameter value: type_of_work_id', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        Damac_Living_FitOut_Alterations__mdt worktypeMDT = getWorkTypeDetailsFromMDT(objInputWrap.type_of_work_id);
        if(worktypeMDT != null) {
            cls_work_type_fee_details objWTfeeDetail = new cls_work_type_fee_details();
            objWTfeeDetail.type_of_work = worktypeMDT.Work_Type_Name__c.trim();
            objWTfeeDetail.type_of_work_id = Integer.valueOf(worktypeMDT.Work_Type_Id__c);
            objWTfeeDetail.fee_amount = worktypeMDT.Price__c;

            cls_total_amount objAmt = new cls_total_amount();
            objAmt.subtotal = objWTfeeDetail.fee_amount;
            objAmt.vat = ((objWTfeeDetail.fee_amount * vatRate)/100).setscale(2);
            objAmt.total = getWorkTypeAmountwithVAT(objWTfeeDetail.fee_amount, vatRate);

            cls_amount_data objClsAmt = new cls_amount_data();
            objClsAmt.total_amount = objAmt;
            objClsAmt.work_type_fee_details = objWTfeeDetail;

            objData = objClsAmt;
            objMeta = ReturnMetaResponse('Successful','', 1);

            returnResponse.data = objData;
            returnResponse.meta_data = objMeta;

            return returnResponse;
        }
        else {
            objMeta = ReturnMetaResponse('Internal processing error','Invalid type_of_work_id passed', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }



    }

    public static Decimal getWorkTypeAmountwithVAT(Decimal subTotal, Decimal vat) {
        System.debug('subTotal in getAccessCardsAmountwithVAT: ' + subTotal);
        Decimal total = ( (subtotal * vat)/100 ) + subtotal;

        System.debug('total:: ' + total);
        return total.setscale(2);
    }
    
    public static cls_meta_data ReturnMetaResponse(String message, String devMsg, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data();
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = devMsg;
        return retMeta;
    }
    
    /**********************************************************************************************************************
    //For HttpGet Method
    /**********************************************************************************************************************/
    global class FinalReturnWrapper {
        public cls_data[] data;
        public cls_meta_data meta_data;
    }
    
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }
    
    public class cls_data {
        public String type_of_work;
        public FitOutDocWrapper[] documents_required;
        public Decimal price;
        public String[] noc_types;
        public Integer type_of_work_id;
        public UndertakingDocWrapper[] undertaking_documents;
    }

    public class FitOutDocWrapper {
        public Integer doc_id;
        public String document_name;
    }

    public class UndertakingDocWrapper {
        public String document_name;
        public String document_url;
    }

    /**********************************************************************************************************************
    // For HttpPut Method
    /**********************************************************************************************************************/
    global class FinalCaseReturnWrapper {
        public cls_case_data data;
        public cls_meta_data meta_data;
    }   

    public class cls_case_data {
        public String booking_unit_id;
        public String booking_unit_name;
        public String sr_id;
        public String sr_number;
        public String sr_type;
        public String sr_status;
        public String sr_submission_date;
        public String sr_approval_status;
        public Decimal amount;
        public cls_attachment[] attachments;

    }

    public class FmCaseWrapper {
        public String action;
        public String id;
        public String booking_unit_id;
        public String account_id;
        public Integer type_of_work_id;
        public String type_of_work;
        public String noc_type;
        public String collection_by;
        public String description;
        public String contractor_name;
        public String contractor_email;
        public String contractor_mobile_country_code;
        public String contractor_mobile;
        
    }

    public class cls_attachment {
        public String id;
        public String name;
        public String attachment_url;
        public String file_extension;
    }

    /**********************************************************************************************************************
    // For HttpPatch Method
    /**********************************************************************************************************************/
    public class FitOutSRSubmitWrap {
        public String action;
        public String id;
        public String receipt_number;
        public String receipt_url;
    }

    /*****************************************/
    /* For HttpPost Method */
    /*****************************************/
    global class AmountDetailsReturnWrapper {
        public cls_amount_data data;
        public cls_meta_data meta_data;
    }

    public class cls_get_amount_details_input_wrap {
        public String booking_unit_id;
        public Integer type_of_work_id;
        public String type_of_work;
    }

    public class cls_amount_data{
        public cls_work_type_fee_details work_type_fee_details;
        public cls_total_amount total_amount;
    }

    public class cls_work_type_fee_details {
        public String type_of_work;
        public Integer type_of_work_id;
        public Decimal fee_amount;
    }

    public class cls_total_amount {
        public Decimal subtotal;
        public Decimal vat;
        public Decimal total;
    }
}

/*
JSON request bodies -
Draft save :
{
    "action" : "draft",
    "id" : "0010Y00000Ma7ne",
    "booking_unit_id" : "a0x0Y000002TglN",
    "account_id" : "",
    "type_of_work_id" : "1",
    "type_of_work" : "Swimming pool",
    "noc_type" : "Authority Noc",
    "collection_by" : "Self",
    "description" : "Testing",
    "contractor_name" : "test",
    "contractor_email" : "test@test.com",
    "contractor_mobile_country_code" : "India: 0091",
    "contractor_mobile" : "12345687"
}

***
Submit :

{
    "action" : "submit",
    "id" : "",
    "receipt_number" : "12365456",
    "receipt_url" : "https://ptctest.damacgroup.com/COFFEE/apex/document/view/782cb06e304b3dc91cc45120f75a3b09"
}
*/