/**************************************************************************************************
* Name               : SendEmailToPromotorForInquiriesSchedulerTest                               *
* Description        : Test class for SendEmailToPromotorForInquiriesSchedulerclass.              *
* Created Date       : 24/02/2018                                                                 *
* Created By         : Eternus                                                                    *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         Tejas Naik      24/07/2018      Initial Draft.                                      *
**************************************************************************************************/
@isTest
private class SendEmailToPromotorForInquiriesSchtest {

    static testMethod void testScheduler() {
      Test.startTest();
        SendEmailToPromotorForInquiriesScheduler  schedulerObject = new SendEmailToPromotorForInquiriesScheduler();
        String sch = '0 0 23 * * ?'; 
        System.schedule('Test Send email to promoter inquiry', sch, schedulerObject); 
      Test.stopTest();
    }
}// End of class.