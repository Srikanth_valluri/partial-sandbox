/**************************************************************************************************
* Name               : Test_DamacCasesController                                               
* Description        : An apex page controller for DamacCasesController                                          
* Created Date       : NSI - Diana                                                                        
* Created By         : 02/21/2017                                                                 
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         NSI - Diana          02/21/2017                                                           
**************************************************************************************************/
@isTest
public class Test_DamacCompanyProfileController {
     public static Contact adminContact;
    public static User portalUser;
    public static Account adminAccount;
    public static User portalOnlyAgent;
    
     static void init(){

        adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        insert adminAccount;
        
        Agent_Site__c lstagsites = new Agent_Site__c();
        lstagsites.NAme='UAE';
        lstagsites.Agency__c = adminAccount.id;
        lstagsites.Active__c = true;
        lstagsites.Start_Date__c=system.today()+1;
        lstagsites.End_Date__c = system.today()+1;
        lstagsites.Active__c = false;
        lstagsites.Tax_Registration_Number__c = '214243';
        lstagsites.Registration_Certificate_Date__c = system.today();
        insert lstagsites;

        adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        insert adminContact;
        
        Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
        insert agentContact;
        
        portalUser = InitialiseTestData.getPortalUser('test555@test.com', adminContact.Id, 'Admin');
        portalOnlyAgent = InitialiseTestData.getPortalUser('test155@test.com', agentContact.Id, 'Agent');
         
        InitialiseTestData.createPageFlow(LABEL.Agent_Portal_Registration_Update_Page_Flow_Name);

    }
    
    @isTest static void showCompanyProfileData(){
         UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole66');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'ta56', email='xyzaa1@email.com',
                emailencodingkey='UTF-8', lastname='Usa456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyzaa1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            Test.startTest();
            init();
            
            System.runAs(portalUser){
                DamacCompanyProfileController companyProfile = new DamacCompanyProfileController();
                system.assert(companyProfile.accountDetail != null);
                companyProfile.isError = true;
                companyProfile.init();
            }
            
            Test.stopTest();
        }
    }
    
    
}