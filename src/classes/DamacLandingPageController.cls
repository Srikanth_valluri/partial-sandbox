/**************************************************************************************************
* Name               : DamacLandingPageController                                               
* Description        : An apex page controller for                                              
* Created Date       : NSI - Diana                                                                        
* Created By         : 30/Jan/2017                                                                 
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         NSI - Diana          30/Jan/2017                                                               
**************************************************************************************************/
public class DamacLandingPageController {
    
   
    /**************************************************************************************************
    Method:         redirectToHome
    Description:    - landing page redirection based on logged in or not
**************************************************************************************************/
    public PageReference redirectToHome(){
        if(UserInfo.getUserType() != 'Guest'){
            return new PageReference('/Damac_Home');
        }
        else{
            return new PageReference('/Damac_Login');
        }

    }

}