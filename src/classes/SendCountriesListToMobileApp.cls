@RestResource(urlMapping='/SendCountriesListToMobileApp/*')
 Global class SendCountriesListToMobileApp
 {
     @HtTPGet
    Global static list<String> SendCountriesListToMobile()
    {
       List<String> pickListValuesList= new List<String>();
       Schema.DescribeFieldResult fieldResult = Case.Country__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       for( Schema.PicklistEntry pickListVal : ple){
           pickListValuesList.add(pickListVal.getLabel());
           } 
           
           return pickListValuesList;
    }
 }