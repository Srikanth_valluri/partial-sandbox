@isTest
public class UpdateStaycationTest {
    @isTest
    static void testUnitDetails(){
        Inventory__c objInven = new Inventory__c();
        objInven.Unit_id__c = '98999';
        insert objInven;
         insert new Credentials_Details__c(User_Name__c = 'oracle_user', Password__c = 'crp1user' , Endpoint__c = 'http://151.253.15.117:8050/webservices/rest/XXDC_PROCESS_SERVICE_WS/process/'
                                    ,Name = 'Update Revised Space Type'
        );
        List<Inventory__c> listInvent = new List<Inventory__c>();
        listInvent.add(objInven);
        Test.startTest();
        UpdateStaycation.sendUnitDetails(objInven.Unit_id__c, true);
        UpdateStaycation.sendUbitDetailsFromPB(listInvent);
        Test.stopTest();
    }
}