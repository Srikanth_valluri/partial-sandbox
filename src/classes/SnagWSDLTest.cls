@isTest
private class SnagWSDLTest {

    @isTest
    static void testDrillDown() {
        SOAPCalloutServiceMock.returnToMe = new Map<String,SnagWSDL.drillDownResponse_element>();
        SnagWSDL.drillDownResponse_element response = new SnagWSDL.drillDownResponse_element();
        response.return_x ='S';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        new SnagWSDL.SnagrwebHttpSoap11Endpoint().drillDown(NULL, NULL);
    }

    @isTest
    static void testLocation() {
        SOAPCalloutServiceMock.returnToMe = new Map<String,SnagWSDL.LocationResponse_element>();
        SnagWSDL.LocationResponse_element response = new SnagWSDL.LocationResponse_element();
        response.return_x ='S';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        new SnagWSDL.SnagrwebHttpSoap11Endpoint().Location(NULL, NULL);
    }

    @isTest
    static void testInspections() {
        SOAPCalloutServiceMock.returnToMe = new Map<String,SnagWSDL.InspectionsResponse_element>();
        SnagWSDL.InspectionsResponse_element response = new SnagWSDL.InspectionsResponse_element();
        response.return_x ='S';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        new SnagWSDL.SnagrwebHttpSoap11Endpoint().Inspections(NULL, NULL);
    }

    @isTest
    static void testActivityLogs() {
        SOAPCalloutServiceMock.returnToMe = new Map<String,SnagWSDL.activityLogsResponse_element>();
        SnagWSDL.activityLogsResponse_element response = new SnagWSDL.activityLogsResponse_element();
        response.return_x ='S';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        new SnagWSDL.SnagrwebHttpSoap11Endpoint().activityLogs(NULL, NULL);
    }

    @isTest
    static void testDefects() {
        SOAPCalloutServiceMock.returnToMe = new Map<String,SnagWSDL.defectsResponse_element>();
        SnagWSDL.defectsResponse_element response = new SnagWSDL.defectsResponse_element();
        response.return_x ='S';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        new SnagWSDL.SnagrwebHttpSoap11Endpoint().defects(NULL, NULL);
    }

    @isTest
    static void testLocationExtRef() {
        SOAPCalloutServiceMock.returnToMe = new Map<String,SnagWSDL.LocationExtRefResponse_element>();
        SnagWSDL.LocationExtRefResponse_element response = new SnagWSDL.LocationExtRefResponse_element();
        response.return_x ='S';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        new SnagWSDL.SnagrwebHttpSoap11Endpoint().LocationExtRef(NULL, NULL);
    }

    @isTest
    static void testWrappers() {
        new SnagWSDL.activityLogs_element();
        new SnagWSDL.Inspections_element();
        new SnagWSDL.drillDownResponse_element();
        new SnagWSDL.drillDown_element();
        new SnagWSDL.LocationResponse_element();
        new SnagWSDL.Location_element();
        new SnagWSDL.InspectionsResponse_element();
        new SnagWSDL.activityLogsResponse_element();
    }

}