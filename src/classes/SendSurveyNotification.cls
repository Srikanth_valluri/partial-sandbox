/**********************************************************************************************************************
Description: This class is used for sending survey push notification to users on SR closure
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    	| Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   25-11-2020		| Anand Venkitakrishnan | Created initial draft
***********************************************************************************************************************/

global class SendSurveyNotification {
    @testVisible
    private static final String push_notification_sandbox_baseurl = 'https://ptctest.damacgroup.com/hellodamac';
    @testVisible
    private static final String push_notification_prod_baseurl = 'https://ptc.damacgroup.com/hellodamac-prod';
    @testVisible
    private static final String push_notification_endpoint = '/api/v1/notifications/trigger-push';
    @testVisible
    private static final String api_token_stg = 'c13d9a4e2be67d853d7428f5a9a952d6';
    @testVisible
    private static final String api_token_prd = 'c13d9a4e2be67d853d7428f5a9a952d6';
    @testVisible
    private static final String access_token_stg = 'FE42DF6EBCC6902B26B45D62B567AD19';
    @testVisible
    private static final String access_token_prd = '89BBB3F7241282C534719C2C875B66BD';
    
    @testVisible
    private static Boolean isSandbox;
    static {
        isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    
    global class FMCaseDataWrap {
        @InvocableVariable(label='Account ID' required=true)
        public String accountId;
        @InvocableVariable(label='Unit ID' required=true)
        public String unitId;
        @InvocableVariable(label='Unit Name' required=true)
        public String unitName;
        @InvocableVariable(label='Party ID' required=true)
        public String partyId;
        @InvocableVariable(label='Registration ID' required=true)
        public String regId;
        @InvocableVariable(label='FM Case Id' required=true)
        public String fmCaseId;
        @InvocableVariable(label='FM Case Number' required=true)
        public String fmCaseNumber;
        @InvocableVariable(label='SR Type' required=true)
        public String srType;
    }
    
    @InvocableMethod(label='Send survey notification to user')
    public static void sendPushNotification(List<FMCaseDataWrap> fmCaseList) {
        System.debug('Call push notification callout');
        for(FMCaseDataWrap fmCaseDetails : fmCaseList) {
            pushNotificationServiceCallout(fmCaseDetails.accountId,fmCaseDetails.unitId,fmCaseDetails.unitName,fmCaseDetails.partyId,fmCaseDetails.regId,fmCaseDetails.fmCaseId,fmCaseDetails.fmCaseNumber,fmCaseDetails.srType);
        }
    }
    
    @future(callout = true)
    public static void pushNotificationServiceCallout(String accountId,String unitId,String unitName,String partyId,String regId,String fmCaseId,String fmCaseNumber,String srType) {
        String fmCaseNumberValue;
        if(fmCaseNumber.contains('FMN')) {
            fmCaseNumberValue = fmCaseNumber.substring(4);
        }
        else {
            fmCaseNumberValue = fmCaseNumber;
        }
        
        String title = 'Service Request #' + fmCaseNumberValue + ' approved';
        String srTitle,srMessage;
        String fmCaseNumberKey = 'Service Request #' + fmCaseNumberValue;
        
        if(srType.equals('Move_In')) {
            srTitle = 'Move in request approved';
            srMessage = 'move in request for ' + unitName;
        }
        else if(srType.equals('Passport_Detail_Update') && !unitId.equals('NA')) {
            srTitle = 'Passport detail update approved';
            srMessage = 'passport detail update';
        }
        else if(srType.equals('Change_of_Contact_Details')) {
            srTitle = 'Change of contact details approved';
            srMessage = 'change of contact details';
        }
        else if(srType.equals('NOC_For_FitOut')) {
            srTitle = 'NOC for fit out approved';
            srMessage = 'noc for fit out ' + unitName;
        }
        else if(srType.equals('Move_Out')) {
            srTitle = 'Move out request approved';
            srMessage = 'move out request for ' + unitName;
        }
        else if(srType.equals('Request_For_Access_Card')) {
            srTitle = 'Request for access card approved';
            srMessage = 'request for access card for ' + unitName;
        }
        else if(srType.equals('Tenant_Renewal')) {
            srTitle = 'Tenancy renewal approved';
            srMessage = 'tenancy renewal of ' + unitName;
        }
        else if(srType.equals('Passport_Detail_Update') && unitId.equals('NA')) {
            srTitle = 'Passport detail update approved';
            srMessage = 'passport detail update';
            unitId = '';
            regId = '';
        }
        else if(srType.equals('Change_of_Details')) {
            srTitle = 'Change of contact details approved';
            srMessage = 'change of contact details';
            unitId = '';
            regId = '';
        }
        
        String message = 'Your service request related to ' + srMessage + ' is approved.\\nHelp us improve! Please share your experience about this service request.';
        String screenType = 'survey';
        String surveyType = '';
        
        /*accountId = '0010Y00000MaLroQAF';
        unitId = 'XX0010Y00000MaLroQAFXX';
        partyId = '12335';
        regId = '1004900';
        fmCaseNumber = 'fm-3232';*/
        
        String jsonBody = '   {  '  + 
        '       "email": "'+accountId+'",  '  + 
        '		"template_id": 0,	'	+
        '       "message": "'+message+'",  '  + 
        '       "title": "'+title+'",  '  + 
        '       "payload": {  '  + 
        '           "account_id": "'+accountId+'",  '  + 
        '           "booking_unit_id": "'+unitId+'",  '  + 
        '           "party_id": "'+partyId+'",  '  + 
        '           "screen": "'+screenType+'",  '  + 
        '           "survey_type": "",  '  + 
        '           "reg_id": "'+regId+'",  '  + 
        '           "sr_number": "'+fmCaseNumberKey+'",  '  + 
        '           "fm_case_id": "'+fmCaseId+'",  '  + 
        '           "sr_title": "'+srTitle+'"  '  + 
        '       },  '  + 
        '       "total_pending_count": 0,  '  +
        '       "image_url": "https://services.damacgroup.com/DCAssets/hello-damac/notifications/txn_success.png",  '  +
        '       "source_application_id": 9,  '  + 
        '       "dest_application_id": 2  '  + 
        '  }  ' ; 
        System.debug('jsonBody:'+jsonBody);
        
        String accessToken = isSandbox ? access_token_stg : access_token_prd;
        String apiToken = isSandbox ? api_token_stg : api_token_prd;
        String endpoint = isSandbox ? push_notification_sandbox_baseurl : push_notification_prod_baseurl;
        endpoint += push_notification_endpoint;
        System.debug('accessToken:'+accessToken);
        System.debug('apiToken:'+apiToken);
        System.debug('endpoint:'+endpoint);
        
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        request.setBody(jsonBody);
        request.setHeader('Access-Token', accessToken);
        request.setHeader('API-Token', apiToken);
        request.setHeader('Content-Type', 'application/json');
        
        System.debug('request:'+request);
        
        Http http = new Http();
        HttpResponse response;
        
        if(Test.isRunningTest()) {
            response = new HttpResponse();
            response.setStatus('OK');
            response.setStatusCode(200);
            response.setBody('{"meta_data":{"title":"OK","status_code":1,"message":"Successfully pushed message","developer_message":null}}');
        }
        else {
        	response = http.send(request);
        }
        
        System.debug('requestBody:'+request.getBody());
        System.debug('response:'+response);
        System.debug('response body:'+response.getBody());
    }
}