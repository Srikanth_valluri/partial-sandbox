/**
 * @File Name          : ScheduleGenerateDPInvoiceDrawloopTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 12/9/2019, 4:58:03 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/9/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
public  class ScheduleGenerateDPInvoiceDrawloopTest {
   static testMethod void testExecute() {
        Test.startTest();
         Test.setMock(HttpCalloutMock.class, new RestServiceMock());
            ScheduleGenerateDPInvoiceDrawloop objScheduler = new ScheduleGenerateDPInvoiceDrawloop();
            String schCron = '0 0 0 * * ? *';
            String jobId = System.Schedule('Reccord Updated',schCron,objScheduler);
        Test.stopTest();
        CronTrigger cronTrigger = [
            SELECT Id
                 , CronExpression
                 , TimesTriggered
                 , NextFireTime
            FROM CronTrigger
           WHERE id = :jobId
        ];
        System.assertEquals( schCron,  cronTrigger.CronExpression  , 'Cron expression should match' );
        System.assertEquals( 0, cronTrigger.TimesTriggered  , 'Time to Triggered batch should be 0' );
        List<AsyncapexJob> apexJobs = [
            SELECT Id, ApexClassID ,JobType , Status 
            FROM AsyncapexJob
            WHERE JobType = 'BatchApex'];
        System.assertEquals( false , apexJobs.isEmpty()  , 'Expected at least one Async apex Job' );
        System.assertEquals( 'BatchApex', apexJobs[0].JobType  , 'Job Type should match ' );
    }
}