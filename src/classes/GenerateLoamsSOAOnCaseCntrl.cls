/**
 * @File Name          : GenerateLoamsSOAOnCaseCntrl.cls
 * @Description        : Controller class of visualforce page "GenerateLoamsSOAForCase" and to generate Loams SOA on case.
 * @Author             : Raveena Jain
 * @Group              :
 * @Last Modified By   : Raveena Jain
 * @Last Modified On   : 
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0        27/09/2020            Raveena Jain                 Initial Version
**/

public without sharing class GenerateLoamsSOAOnCaseCntrl {

    public String cseId                                  {get;set;}
    public List<Case> lstCse                             {get;set;}
    public blob urlHPBody;
    public list<SR_Attachments__c> lstCaseAttachment    {get;set;}
    public String custEmailid                           {get;set;}
    
    public GenerateLoamsSOAOnCaseCntrl(ApexPages.StandardController controller) {

        lstCse = new List<Case>();
        cseId = ApexPages.currentPage().getParameters().get('id');
        
        lstCse = [ SELECT Id ,Booking_Unit__r.Party_Id__c
                  	   , Booking_Unit__r.Id
                       , Booking_Unit__r.Unit_Name__c,Booking_Unit__r.Name
                       , Booking_Unit__r.Registration_ID__c
                       , Booking_Unit__r.Booking__r.Account__r.IsPersonAccount
                       , Booking_Unit__r.Booking__r.Account__r.Email__pc
                       , Booking_Unit__r.Booking__r.Account__r.Email__c
                       , Booking_Unit__r.Booking__r.Account__r.Name
                       , Booking_Unit__r.Booking__r.Account__c
                    FROM Case
                   WHERE Id = :cseId LIMIT 1 ];   

        if (lstCse[0].Booking_Unit__r.Booking__r.Account__r.IsPersonAccount == True) {
            custEmailid = lstCse[0].Booking_Unit__r.Booking__r.Account__r.Email__pc;
        } else {
            custEmailid = lstCse[0].Booking_Unit__r.Booking__r.Account__r.Email__c;
        }
                   
    }
    
    public PageReference generateFMSOA(){
        if( String.isNotBlank( lstCse[0].Booking_Unit__r.Registration_ID__c ) ) {
            PageReference  pg = GenerateDocForFMCLCntrl.fetchUnitFMSOA( lstCse[0].Booking_Unit__r.Registration_ID__c , null,  lstCse[0].Booking_Unit__r.Id , null );
            if(pg != Null){
                //TODO  added to create record of soa genrator
                GenericUtility.createSOACreator(String.valueOf(pg) ,UserInfo.getUserId(),system.now(),lstCse[0].Booking_Unit__r.Registration_ID__c,'Bulk service charge SOA',NULL,lstCse[0].Booking_Unit__r.Id,NULL);
            }
            return pg ;
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter registration id for Generating SOA'));
            return null;
        }
    }// End of generateFMSOA
    
    public PageReference generateFMPartySOA(){
        
        if( String.isNotBlank( lstCse[0].Booking_Unit__r.Party_ID__c ) ) {
           PageReference  pg =GenerateDocForFMCLCntrl.fetchPartyWiseFMSOA( lstCse[0].Booking_Unit__r.Party_ID__c , null , lstCse[0].Booking_Unit__r.Id , null );
           if(pg != Null){
                //TODO  added to create record of soa genrator
                GenericUtility.createSOACreator(String.valueOf(pg),UserInfo.getUserId(),system.now(),lstCse[0].Booking_Unit__r.Registration_ID__c,'BULK service charge SOA',NULL,lstCse[0].Booking_Unit__r.Id,NULL);
            }
            return pg ;
                
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please provide party id for Generating SOA'));
            return null;
        }

    }// End of generateFMPartySOA
    
    
    public pagereference generateAndEmailFMUnitSOA(){
        if( String.isNotBlank( lstCse[0].Booking_Unit__r.Registration_ID__c ) ) {
            String soaUrl;
            try {
                soaUrl = FmIpmsRestServices.getUnitSoaByRegistrationId( lstCse[0].Booking_Unit__r.Registration_ID__c );
            } catch(Exception excp) {
                errorLogger( excp.getMessage()+' at ' +String.valueOf( excp.getLineNumber() ) , null ,lstCse[0].Booking_Unit__r.Id ,null  );
            }
            
            if (String.isBlank(soaUrl)) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error : Unit SOA generation failed. Please try again'));
                return null;                
            }else {
                if(!Test.isRunningTest())
                    urlHPBody = getBlob(soaUrl);

                String soaType = 'FM Unit SOA.pdf';
                String userName = UserInfo.getUserName();
                User activeUser = [Select Email From User where Username = : userName limit 1];
                String userEmail = activeUser.Email;
                String custEmail;
                if (lstCse[0].Booking_Unit__r.Booking__r.Account__r.IsPersonAccount == True) { 
                    custEmail = lstCse[0].Booking_Unit__r.Booking__r.Account__r.Email__pc;
                } else {
                    custEmail = lstCse[0].Booking_Unit__r.Booking__r.Account__r.Email__c;
                }
                SendEmail(urlHPBody, custEmail, lstCse[0].Booking_Unit__r.Booking__r.Account__r.Name, soaType, userEmail, lstCse[0].Booking_Unit__r.Unit_Name__c, lstCse[0].Booking_Unit__r.Booking__r.Account__c,soaUrl);

            }
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter registration id for Generating SOA'));
            return null;
        }
        pagereference newpg = new PageReference ('/'+lstCse[0].Booking_Unit__r.Id);
        newpg.setRedirect(false);
        return newpg;
    }

    public pagereference generateAndEmailFMPartySOA(){
   
        if( String.isNotBlank( lstCse[0].Booking_Unit__r.Party_ID__c ) ) {
            String soaUrl;
            try {
                soaUrl = FmIpmsRestServices.getBulkSoaUrlForPartyId( lstCse[0].Booking_Unit__r.Party_ID__c );

            } catch(Exception excp) {
                errorLogger( excp.getMessage()+' at ' +String.valueOf( excp.getLineNumber() ) , null ,lstCse[0].Booking_Unit__r.Id ,null  );
            }

            if (String.isBlank(soaUrl)) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error : Bulk SOA generation failed. Please try again'));
                return null;                
            }else {
                if(!Test.isRunningTest()){
                    urlHPBody = getBlob(soaUrl);
                }
                else {
                    urlHPBody = blob.valueOf('Test');
                }

                String soaType = 'Party SOA.pdf';
                String userName = UserInfo.getUserName();
                User activeUser = [Select Email From User where Username = : userName limit 1];
                String userEmail = activeUser.Email;
                String custEmail;
                if (lstCse[0].Booking_Unit__r.Booking__r.Account__r.IsPersonAccount == True) { 
                    custEmail = lstCse[0].Booking_Unit__r.Booking__r.Account__r.Email__pc;
                } else {
                    custEmail = lstCse[0].Booking_Unit__r.Booking__r.Account__r.Email__c;
                }
                
                
                SendEmail(urlHPBody, custEmail, lstCse[0].Booking_Unit__r.Booking__r.Account__r.Name, soaType, userEmail, lstCse[0].Booking_Unit__r.Unit_Name__c, lstCse[0].Booking_Unit__r.Booking__r.Account__c,soaUrl);

            }
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter party id for Generating SOA'));
            return null;
        }
        pagereference newpg = new PageReference ('/'+lstCse[0].Booking_Unit__r.Id);
        newpg.setRedirect(false);
        return newpg;
    }
    
    
    public pagereference CancelGeneration(){
    
        pagereference newpg = new PageReference ('/'+cseId);
        newpg.setRedirect(false);
        return newpg;
    }       
    
    public static void errorLogger(string strErrorMessage,Id clId, Id buId , Id accId ){
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        objError.Calling_List__c = clId;
        objError.Booking_Unit__c = buId;
        objError.Account__c = accId;
        insert objError;
    } 
    
    public void SendEmail(blob file, String email, String accName, String soaType, String creMail, String buId, Id accId , string strUrl) {
        //New instance of a single email message
        Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
                    
        list<Messaging.SingleEmailMessage> mails = new  list<Messaging.SingleEmailMessage>();

        List<String> sendTo = new List<String>();
        if(email !='' || email != null) {
          sendTo.add(email);
        }
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        list<String> bccAddress = new list<String>();
        //bccAddress.add(creMail);
        /*
        * Updated bcc address on 10/02/2019
        */
        bccAddress.add(Label.SF_Copy_EMail_Address);
        //mail.setSubject('Statement of Account Generated.');
        mail.setToAddresses(sendTo);
        mail.setBccAddresses(bccAddress);
        mail.setUseSignature(false);
               
        string name = nameFormat(accName);
        
        String body = 'Dear ' + name + ',<br/>';
        
        if(soaType.equalsIgnoreCase('FM Unit SOA.pdf')) {
            mail.setSubject('Service Charge Statement of Account for unit '+ buId);
            body += '<br/> As requested, please find attached Service Charge Statement of Account for unit '+ buId +'.<br/>';
        } else if(soaType.equalsIgnoreCase('Party SOA.pdf')) {
            mail.setSubject('Bulk Service charge Statement of Account ');
            body += '<br/> As requested, please find attached the Bulk Service charge Statement of Account.<br/>';
        }
        body += '<br/>Regards,';
        body += '<br/>' + UserInfo.getName();
        body += '<br/> Officer-Client Relations';
        body += '<br/>DAMAC PROPERTIES Co. LLC.';
        body += '<br/> P.O. Box: 2195, Dubai, United Arab Emirates';
        body += '<br/> Telephone: +971 4 237 5000';
        body += '<br/> Fax: +971 4 373 1373';
        body += '<br/> E-mail: atyourservice@damacproperties.com';
        body += '<br/> http://www.damacproperties.com/';


        mail.setHtmlBody(body);

        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
        efa.setFileName(soaType);
        efa.setBody(file);
        fileAttachments.add(efa);
        mail.setFileAttachments(fileAttachments);
        mails.add(mail);
        try {
            Messaging.sendEmail(mails);
            
            EmailMessage emailMsg = new EmailMessage();

            emailMsg.ToAddress=(mails[0].getToAddresses())[0];
            emailMsg.FromAddress = UserInfo.getUserEmail();
            emailMsg.FromName = 'SOAs SR - Document';
            emailMsg.Subject=mails[0].getSubject();
            emailMsg.HtmlBody=mails[0].getHtmlBody();
            emailMsg.Account__c = accId; //Attach with the account
            emailMsg.MessageDate = system.now();
            emailMsg.Status = '3';
            emailMsg.Attachment_URL__c = strUrl;
            emailMsg.BccAddress = (mails[0].getBccAddresses())[0];
            insert emailMsg;
            
        } catch(Exception e) {
            System.debug('======Error in email sending=========' + e);
        }
    }
    
    public static Blob getBlob(String url){
        PageReference pageRef = new PageReference(url);
        Blob ret = pageRef.getContentAsPDF();
        return ret;
    }
    
    public static String nameFormat ( String strName ) {
        system.debug( ' strName : '+ strName );
        strName = strName.toLowerCase();
        List<String> names = strName.split(' ');
        for (Integer i = 0; i < names.size(); i++) {
            names[i] = names[i].capitalize();
        }
        strName = String.join(names, ' ');
        system.debug(strName);
        return strName;
    }
    
}