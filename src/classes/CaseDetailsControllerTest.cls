@isTest
public class CaseDetailsControllerTest {

    @testSetup 
        static void setup() {
            TestUtility utility=new TestUtility();
            List<Case> caseList =  utility.caseInsert(1);            
            CaseComment caseCommntList = new CaseComment(CommentBody ='Hello ABC', ParentId= caseList[0].id) ;
            insert caseCommntList;
            system.debug('caseCommntList' + caseCommntList);
       }
       
    @isTest
        public static void testGetCaseComments(){
            list<Case> caseLst = [Select id from Case];
            CaseDetailsController controller= new CaseDetailsController();
            Test.startTest(); 
            CaseDetailsController.getCaseComments(caseLst[0].id);
            Test.stopTest(); 
            String caseId= caseLst[0].id;
            list<CaseComment> commentList = [SELECT      
                                                        ParentId,
                                                        CommentBody,
                                                        CreatedDate
                                            FROM        CaseComment
                                            WHERE       ParentId = :caseId
                                            ORDER BY    CreatedDate
                                            LIMIT       1]; 
                system.assertEquals(1,commentList.size());
       }
       
    @isTest
        public static void testCreateCaseComments(){
            list<Case> caseLst = [Select id from Case];
            String message = 'Hello ABC';
            Test.startTest(); 
            CaseDetailsController.createCaseComment(caseLst[0].id, message);
            Test.stopTest(); 
            String caseId= caseLst[0].id;
            CaseComment caseCommntList = new CaseComment(CommentBody = message, ParentId= caseId) ;
            insert caseCommntList;
            list<CaseComment> commentList = [SELECT      
                                                        ParentId,
                                                        CommentBody,
                                                        //CreatedById,
                                                        //CreatedBy.Name,
                                                        CreatedDate
                                            FROM        CaseComment
                                            WHERE       ParentId = :caseId
                                            ORDER BY    CreatedDate
                                            LIMIT       1]; 
        }
       
    @isTest
        public static void testgetCaseDetails(){
            list<Case> caseLst = [Select id from Case];
            //String message = 'Hello ABC';
            Test.startTest(); 
            CaseDetailsController.getCaseDetails(caseLst[0].id);
            Test.stopTest(); 
            String caseId= caseLst[0].id;
            //CaseComment caseCommntList = new CaseComment(CommentBody = message, ParentId= caseId) ;
            //insert caseCommntList;
            list<Case> casList = [SELECT      
                                                Id,
                                                CreatedDate
                                    FROM        Case
                                    WHERE       Id =:caseId ];
            system.assertEquals(1,casList.size()); 
        }
}