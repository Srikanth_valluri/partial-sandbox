global class ZIWOCallHistoryScheduler implements Schedulable{
    
    global void execute(SchedulableContext sc){
        executeFollowing();
    }

    global static void executeFollowing(){
        database.executeBatch(new ZiwoTasks2WaybeoQueueable(), integer.valueOf(System.label.Ziwo_Log_Insert_Batch_Count));
    }
}