public with sharing class SendGridWithAttachments {

    public static HTTPResponse sendRequestToSendGrid(String toMail, String fromMail, String subject, 
        String fromName, String textBody, String htmlBody, set<Id> setAttachId, String replyto){

        

        String boundary = '----------------------------741e90d31eff';

        String header = '';
        header += '--'+boundary+'\r\n';
        /*header += 'Content-Disposition: form-data; name="api_user"\r\n\r\nYOUR_LOGIN\r\n';
        header += '--'+boundary+'\r\n';
        header += 'Content-Disposition: form-data; name="api_key"\r\n\r\nYOUR_PASSWORD\r\n';
        header += '--'+boundary+'\r\n';*/
        header += 'Content-Disposition: form-data; name="to"\r\n\r\n'+toMail+'\r\n';
        header += '--'+boundary+'\r\n';
        header += 'Content-Disposition: form-data; name="from"\r\n\r\n'+fromMail+'\r\n';
        header += '--'+boundary+'\r\n';
        header += 'Content-Disposition: form-data; name="fromname"\r\n\r\n'+fromName+'\r\n';
        header += '--'+boundary+'\r\n';
        header += 'Content-Disposition: form-data; name="replyto"\r\n\r\n'+replyto+'\r\n';
        header += '--'+boundary+'\r\n';
        header += 'Content-Disposition: form-data; name="subject"\r\n\r\n'+subject+'\r\n';
        header += '--'+boundary+'\r\n';
        header += 'Content-Disposition: form-data; name="text"\r\n\r\n'+textBody+'\r\n';
        header += '--'+boundary+'\r\n';
        header += 'Content-Disposition: form-data; name="html"\r\n\r\n'+htmlBody+'\r\n';

        String footer = '--'+boundary+'--';

        String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        while(headerEncoded.endsWith('=')) {
            header += ' ';
            headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        }

        String attachmentsStr = '';
        String lastPrepend = '';
        system.debug('!!!HeapSize'+Limits.getHeapSize());
        if (!setAttachId.isEmpty()) {
            for (Attachment d : [Select Id
            						, Body
            						, Name 
            					 From Attachment
            					 Where Id IN: setAttachId]) {
                Blob fileBlob = d.Body;
                String filename = d.Name;
                String fHeader = lastPrepend + '--'+boundary+'\r\n';
                fHeader += 'Content-Disposition: form-data; name="files['+filename+']"; filename="'+filename+'"\r\nContent-Type: application/octet-stream';
                String fHeaderEncoded = EncodingUtil.base64Encode(Blob.valueOf(fheader+'\r\n\r\n'));
                while(fHeaderEncoded.endsWith('=')) {
                    fHeader += ' ';
                    fHeaderEncoded = EncodingUtil.base64Encode(Blob.valueOf(fHeader+'\r\n\r\n'));
                } 
                String fbodyEncoded = EncodingUtil.base64Encode(fileBlob);
                system.debug('!!!fbodyEncoded'+Limits.getHeapSize());
                String last4Bytes = fbodyEncoded.substring(fbodyEncoded.length()-4,fbodyEncoded.length());
                system.debug('!!!last4Bytes'+Limits.getHeapSize());
                if(last4Bytes.endsWith('==')) {
                	system.debug('!!!inside ==');
                    last4Bytes = last4Bytes.substring(0,2) + '0K';
                    fBodyEncoded = fbodyEncoded.substring(0,fbodyEncoded.length()-4) + last4Bytes;
                    lastPrepend = '';
                } else if(last4Bytes.endsWith('=')) {
                	system.debug('!!!inside =');
                    last4Bytes = last4Bytes.substring(0,3) + 'N';
                    fBodyEncoded = fbodyEncoded.substring(0,fbodyEncoded.length()-4) + last4Bytes;
                    lastPrepend = '\n';
                } else { 
                	system.debug('!!!inside else');
                    lastPrepend = '\r\n';                    
                }
                system.debug('lastPrepend'+Limits.getHeapSize());
                attachmentsStr += fHeaderEncoded + fBodyEncoded;
                system.debug('!!!attachmentsStr'+Limits.getHeapSize());
            }
            footer = lastPrepend + footer;
        } else {
            footer = '\r\n' + footer;
        }
		system.debug('!!!Limits.getHeapSize()'+Limits.getHeapSize());  
        String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
        Blob bodyBlob = EncodingUtil.base64Decode(headerEncoded+attachmentsStr+footerEncoded);
        HttpRequest req = new HttpRequest();
        req.setHeader('authorization','Bearer '+ Label.SendGrid_Username);
        req.setHeader('Content-Type','multipart/form-data; boundary='+boundary);
        req.setMethod('POST');
        req.setEndpoint('https://api.sendgrid.com/api/mail.send.json');
        req.setBodyAsBlob(bodyBlob);
        req.setTimeout(120000);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        return res;
    }
}