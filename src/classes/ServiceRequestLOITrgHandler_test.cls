@isTest

private class ServiceRequestLOITrgHandler_test{

    @isTest static void test_method_1() {
        Id loiProfile = [SELECT Id FROM Profile WHERE Name = 'Property Consultant LOI'].Id;
        User loiUser = new User(alias = 'te156', email='x123@email.com',
                emailencodingkey='UTF-8', lastname='User 123', languagelocalekey='en_US',
                localesidkey='en_US', profileid = loiProfile, country='United Arab Emirates',
                IsActive =true,timezonesidkey='America/Los_Angeles', username='x123@email.com');
        System.RunAs(loiUser) {
            NSIBPM__Service_Request__c serviceRequest ;
            
            List<NSIBPM__SR_Template__c> SRTemplateList 
                = InitialiseTestData.createTestTemplateRecords(
                new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
            
            List<NSIBPM__SR_Status__c> createdSRStatus 
                = TestDataFactory.createSrStatusRecords(new List<NSIBPM__SR_Status__c>{
                new NSIBPM__SR_Status__c(),
                new NSIBPM__SR_Status__c(Name='Token Deposit Paid',NSIBPM__Code__c='TOKEN_DEPOSIT_PAID'),
                new NSIBPM__SR_Status__c(Name='Draft',NSIBPM__Code__c='DRAFT'),
                new NSIBPM__SR_Status__c(Name='LOI Submitted',NSIBPM__Code__c='LOI SUBMITTED'),
                new NSIBPM__SR_Status__c(Name='Closed',NSIBPM__Code__c='CLOSED')
            });
            createdSRStatus[0].NSIBPM__Code__c='Submitted';
            createdSRStatus[1].name='Token Deposit Paid'; 
            createdSRStatus[2].NSIBPM__Code__c='DRAFT';
            createdSRStatus[3].NSIBPM__Code__c = 'LOI SUBMITTED';
            createdSRStatus[4].NSIBPM__Code__c = 'CLOSED';
            update createdSRStatus[0];
            update createdSRStatus[2];
            update createdSRStatus[3];
            update createdSRStatus[1];
            
            List<NSIBPM__Service_Request__c> SRList 
                = InitialiseTestData.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                new NSIBPM__Service_Request__c(
                recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration'),
                NSIBPM__SR_Template__c = SRTemplateList[0].Id)
            });
            id corporrateRecordTypeId 
                = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();   
            Account acc = New Account ();
            acc.name = 'Test';
            acc.recordTypeId = corporrateRecordTypeId;
            insert acc;

            serviceRequest = SRList[0];
            serviceRequest.Agency_Name__c = ' AGENCY Real Estate';
            serviceRequest.Country_of_Sale__c = 'UAE;Lebanon;Jordan';
            serviceRequest.Agent_Registration_Type__c = 'LOI';
            serviceRequest.NSIBPM__Internal_SR_Status__c = createdSRStatus[3].id;
            serviceRequest.NSIBPM__External_SR_Status__c = createdSRStatus[3].id;
            serviceRequest.Agency_Corporate_Type__c = 'Real Estate';
            serviceRequest.ORN_Number__c = '54654';
            serviceRequest.RERA_Expiry_Date__c = System.today().addDays(5); 
            serviceRequest.NSIBPM__Customer__c = acc.id;
            serviceRequest.LOI_Profile__c = true;
            serviceRequest.LOI_Profile__c = true;
            update serviceRequest;

            NSIBPM__Document_Master__c LOI_DM = new NSIBPM__Document_Master__c(
                NSIBPM__Code__c = 'LOI_AGREEMENT', NSIBPM__Available_to_client__c = true
            );
            insert LOI_DM;

            serviceRequest = [ Select NSIBPM__Internal_SR_Status__c,
                                    NSIBPM__Internal_Status_Name__c,NSIBPM__Customer__c 
                                from NSIBPM__Service_Request__c 
                            where id = :serviceRequest.id];
            
            Id DEAL_SR_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('NSIBPM__Service_Request__c', 'Deal');
            
            List<NSIBPM__SR_Template__c> createdSRTemplates 
                = TestDataFactory.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{
                new NSIBPM__SR_Template__c(Name='Deal', NSIBPM__SR_RecordType_API_Name__c = 'Deal')
            });
                        
            List<NSIBPM__Service_Request__c> createdSRs 
                = TestDataFactory.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                new NSIBPM__Service_Request__c(
                    OwnerId = Userinfo.getUserId(), 
                    recordTypeId = DEAL_SR_RECORD_TYPE_ID, 
                    NSIBPM__Internal_SR_Status__c = createdSRStatus[0].Id,
                    NSIBPM__External_SR_Status__c = createdSRStatus[0].Id, 
                    NSIBPM__SR_Template__c = createdSRTemplates[0].Id, 
                    Registration_Date__c = Date.newInstance(2017,5,25)
                )
            });

            NSIBPM__Service_Request__c dealserviceRequest ;
            dealserviceRequest = createdSRs[0];
            dealserviceRequest = [Select NSIBPM__Internal_SR_Status__c,
                                    NSIBPM__Internal_Status_Name__c,NSIBPM__External_SR_Status__c 
                                from NSIBPM__Service_Request__c 
                                where id = :createdSRs[0].id];
            dealserviceRequest.NSIBPM__Internal_SR_Status__c = createdSRStatus[0].id;
            dealserviceRequest.NSIBPM__External_SR_Status__c = createdSRStatus[0].id;
            dealserviceRequest.Agency__c = acc.id;
            update dealserviceRequest; 

            dealserviceRequest = [Select NSIBPM__Internal_SR_Status__c,
                                        NSIBPM__Internal_Status_Name__c,
                                        NSIBPM__External_SR_Status__c,
                                        NSIBPM__External_Status_Code__c 
                                    from NSIBPM__Service_Request__c 
                                where id = :createdSRs[0].id];

            test.startTest();
            dealserviceRequest.NSIBPM__Internal_SR_Status__c = createdSRStatus[1].id;
            dealserviceRequest.NSIBPM__External_SR_Status__c = createdSRStatus[1].id;
            update dealserviceRequest;
            ServiceRequestLOITrgHandler.generateLOIAgreements(serviceRequest.Id);
            List<NSIBPM__Service_Request__c> srList1 = new List<NSIBPM__Service_Request__c>();
            srList1.add(serviceRequest);
            ServiceRequestLOITrgHandler.createAgreementGeneratedStep(srList1);
            test.stopTest();  
        } 
    }

    @isTest static void test_method_2() {
        Id loiProfile = [SELECT Id FROM Profile WHERE Name = 'Property Consultant LOI'].Id;
        User loiUser = new User(alias = 'te156', email='x123@email.com',
                emailencodingkey='UTF-8', lastname='User 123', languagelocalekey='en_US',
                localesidkey='en_US', profileid = loiProfile, country='United Arab Emirates',
                IsActive =true,timezonesidkey='America/Los_Angeles', username='x123@email.com');
        System.RunAs(loiUser) {
            NSIBPM__Service_Request__c serviceRequest ;
            
            List<NSIBPM__SR_Template__c> SRTemplateList 
                = InitialiseTestData.createTestTemplateRecords(
                new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
            
            List<NSIBPM__SR_Status__c> createdSRStatus 
                = TestDataFactory.createSrStatusRecords(new List<NSIBPM__SR_Status__c>{
                new NSIBPM__SR_Status__c(),
                new NSIBPM__SR_Status__c(Name='Token Deposit Paid',NSIBPM__Code__c='TOKEN_DEPOSIT_PAID'),
                new NSIBPM__SR_Status__c(Name='Draft',NSIBPM__Code__c='DRAFT'),
                new NSIBPM__SR_Status__c(Name='LOI Submitted',NSIBPM__Code__c='LOI SUBMITTED'),
                new NSIBPM__SR_Status__c(Name='Closed',NSIBPM__Code__c='CLOSED')
            });
            createdSRStatus[0].NSIBPM__Code__c='Submitted';
            createdSRStatus[1].name='Token Deposit Paid'; 
            createdSRStatus[2].NSIBPM__Code__c='DRAFT';
            createdSRStatus[3].NSIBPM__Code__c = 'LOI SUBMITTED';
            createdSRStatus[4].NSIBPM__Code__c = 'CLOSED';
            update createdSRStatus[0];
            update createdSRStatus[2];
            update createdSRStatus[3];
            update createdSRStatus[1];
            
            List<NSIBPM__Service_Request__c> SRList 
                = InitialiseTestData.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                new NSIBPM__Service_Request__c(
                recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration'),
                NSIBPM__SR_Template__c = SRTemplateList[0].Id)
            });
            id individualRecordTypeId 
                = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Agency').getRecordTypeId();   
            Account acc = New Account ();
            //acc.name = 'Test';
            acc.LastName= 'Test';
            acc.FirstName= 'Test';
            acc.recordTypeId = individualRecordTypeId;
            insert acc;

            serviceRequest = SRList[0];
            serviceRequest.Country_of_Sale__c = 'UAE;Lebanon;Jordan';
            serviceRequest.Agent_Registration_Type__c = 'LOI';
            serviceRequest.NSIBPM__Internal_SR_Status__c = createdSRStatus[3].id;
            serviceRequest.NSIBPM__External_SR_Status__c = createdSRStatus[3].id;
            serviceRequest.NSIBPM__Customer__c = acc.id;
            serviceRequest.Agency_Type__c = 'Individual';
            serviceRequest.LOI_Profile__c = true;
            update serviceRequest;

            NSIBPM__Document_Master__c LOI_DM = new NSIBPM__Document_Master__c(
                NSIBPM__Code__c = 'LOI_AGREEMENT', NSIBPM__Available_to_client__c = true
            );
            insert LOI_DM;

            serviceRequest = [ Select NSIBPM__Internal_SR_Status__c,
                                    NSIBPM__Internal_Status_Name__c,NSIBPM__Customer__c 
                                from NSIBPM__Service_Request__c 
                            where id = :serviceRequest.id];
            
            Id DEAL_SR_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('NSIBPM__Service_Request__c', 'Deal');
            
            List<NSIBPM__SR_Template__c> createdSRTemplates 
                = TestDataFactory.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{
                new NSIBPM__SR_Template__c(Name='Deal', NSIBPM__SR_RecordType_API_Name__c = 'Deal')
            });
                        
            List<NSIBPM__Service_Request__c> createdSRs 
                = TestDataFactory.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                new NSIBPM__Service_Request__c(
                    OwnerId = Userinfo.getUserId(), 
                    recordTypeId = DEAL_SR_RECORD_TYPE_ID, 
                    NSIBPM__Internal_SR_Status__c = createdSRStatus[0].Id,
                    NSIBPM__External_SR_Status__c = createdSRStatus[0].Id, 
                    NSIBPM__SR_Template__c = createdSRTemplates[0].Id, 
                    Registration_Date__c = Date.newInstance(2017,5,25)
                )
            });

            NSIBPM__Service_Request__c dealserviceRequest ;
            dealserviceRequest = createdSRs[0];
            dealserviceRequest = [Select NSIBPM__Internal_SR_Status__c,
                                    NSIBPM__Internal_Status_Name__c,NSIBPM__External_SR_Status__c 
                                from NSIBPM__Service_Request__c 
                                where id = :createdSRs[0].id];
            dealserviceRequest.NSIBPM__Internal_SR_Status__c = createdSRStatus[0].id;
            dealserviceRequest.NSIBPM__External_SR_Status__c = createdSRStatus[0].id;
            dealserviceRequest.Agency__c = acc.id;
            update dealserviceRequest; 

            dealserviceRequest = [Select NSIBPM__Internal_SR_Status__c,
                                        NSIBPM__Internal_Status_Name__c,
                                        NSIBPM__External_SR_Status__c,
                                        NSIBPM__External_Status_Code__c 
                                    from NSIBPM__Service_Request__c 
                                where id = :createdSRs[0].id];

            test.startTest();
            dealserviceRequest.NSIBPM__Internal_SR_Status__c = createdSRStatus[1].id;
            dealserviceRequest.NSIBPM__External_SR_Status__c = createdSRStatus[1].id;
            update dealserviceRequest;
            ServiceRequestLOITrgHandler.generateLOIAgreements(serviceRequest.Id);
            test.stopTest();  
        } 
    }

}