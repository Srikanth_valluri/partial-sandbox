/****************************************************************************************************
* Name          : ReceiptChinaController                                                  			*
* Description   : ReceiptChina Page Controller                                            			*
* Created Date  : 28-08-2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE            COMMENTS                                                *
* 1.0   Craig Lobo          28-08-2018      Initial Draft.                                          *
****************************************************************************************************/
public with sharing class ReceiptChinaController {

    public ReceiptChinaController.ReceiptData rcptData                                  {get; set;}
    public String AED_RMB = Label.AED_To_RMB;
    public Boolean isInitialised;
    public String dealSRId;
    public String dealSRName;

    /**
     * Constructor 
     */
    public ReceiptChinaController() {
        rcptData = new ReceiptChinaController.ReceiptData();
        String bookingId = '';

        dealSRId = Apexpages.currentpage().getparameters().get('srid');
        rcptData.address = Apexpages.currentpage().getparameters().get('add');
        rcptData.remarks = Apexpages.currentpage().getparameters().get('rmrk');
        String strChequeDate = Apexpages.currentpage().getparameters().get('date');
        rcptData.totalAmount =  Decimal.valueOf(Apexpages.currentpage().getparameters().get('amount'));

        if (String.isNotBlank(strChequeDate)) {
            List<String> dateList = strChequeDate.split('/');
            if (dateList != null && dateList.size() == 3) {
                rcptData.chequeDate = Date.newInstance(Integer.valueOf(dateList[2]), Integer.valueOf(dateList[0]), Integer.valueOf(dateList[1]));
            }
        }

        if (String.isBlank(dealSRId)) {
            //dealSRId = pDealId;
        }
        List<NSIBPM__Service_Request__c> dealSRList = [ SELECT Id, Name, LastModifiedDate, Token_Amount_AED__c 
                                                          FROM NSIBPM__Service_Request__c 
                                                         WHERE Id = :dealSRId 
                                                         LIMIT 1
        ];

        if (dealSRList != null && !dealSRList.isEmpty()) {

            String invNum = dealSRList[0].Name;
            invNum = invNum.replace('-', '');
            invNum = invNum.replace('SR', '');
            String dateTimeStr = String.valueOf(System.now());
            dateTimeStr = dateTimeStr.replace(':', '');
            dateTimeStr = dateTimeStr.replace(' ', '');
            dateTimeStr = dateTimeStr.replace('-', '');

            // Service Request Details
            rcptData.invoiceNumber = invNum + dateTimeStr;
            rcptData.invoiceDate = System.Today();
            rcptData.invoiceDueDate = System.Today().addDays(7);
            if (String.isNotBlank(AED_RMB)) {
                //rcptData.amount = dealSRList[0].Token_Amount_AED__c * Decimal.valueOf(AED_RMB) ;
                //rcptData.amount = rcptData.amount * Decimal.valueOf(AED_RMB) ;
                rcptData.amount = rcptData.amount;
            }/* else {
                rcptData.amount = dealSRList[0].Token_Amount_AED__c;
            }*/
            System.debug('rcptData.amount >>>>> ' + rcptData.amount);
            /*rcptData.vatAmount = rcptData.amount * 0.06;
            rcptData.totalAmount = rcptData.amount +rcptData.vatAmount;
            rcptData.amountInWords = NumberToWord.english_number(Integer.valueof(rcptData.totalAmount));*/
            
            rcptData.vatAmount =  rcptData.totalAmount * 0.06;
            rcptData.amount = rcptData.totalAmount - rcptData.vatAmount;
            rcptData.amountInWords = NumberToWord.english_number(Integer.valueof(rcptData.totalAmount)); 

            // Booking Details
            for (Booking__c bookObj :  [ SELECT Id, Registration_ID__c
                                           FROM Booking__c 
                                          WHERE Deal_SR__c = :dealSRId
                                            AND Rejected__c = false
            ]) {
                bookingId = bookObj.Id;
            }

            // Buyer Details
            for (Buyer__c buyerkObj :  [ SELECT Id, Primary_Buyer__c, First_Name__c, Last_Name__c, 
                                                Title__c
                                           FROM Buyer__c 
                                          WHERE Booking__c = :bookingId
                                            AND Primary_Buyer__c = true
            ]) {
                String fName = String.isNotBlank(buyerkObj.First_Name__c) ? buyerkObj.First_Name__c : '';
                String lName = String.isNotBlank(buyerkObj.Last_Name__c) ? buyerkObj.Last_Name__c : '';
                rcptData.buyerName = buyerkObj.Title__c + ' ' + fName + ' ' + lName;
            }

            // Booking Unit Details
            for (Booking_Unit__c buObj : [ SELECT Id, Registration_ID__c, Payment_Method__c
                                             FROM Booking_Unit__c
                                            WHERE Booking__c = :bookingId
                                              AND Registration_ID__c != ''
            ]) {
                if (String.isNotBlank(rcptData.buRegIds)) {
                    rcptData.buRegIds = rcptData.buRegIds + ', ' + buObj.Registration_ID__c;
                } else {
                    rcptData.buRegIds = buObj.Registration_ID__c;
                }
                rcptData.modeOfPayment = buObj.Payment_Method__c;
            }
            System.debug('FINAL rcptData >>>>> ' + rcptData);
            isInitialised = false;
        }
    }


    /**
     * Wrapper Class
     */
    public class ReceiptData {
        public String buyerName                                                         {get; set;}
        public String address                                                           {get; set;}
        public String remarks                                                           {get; set;}
        public String buRegIds                                                          {get; set;}
        public String modeOfPayment                                                     {get; set;}
        public String invoiceNumber                                                     {get; set;}
        public Date invoiceDueDate                                                      {get; set;}
        public Date invoiceDate                                                         {get; set;}
        public Decimal totalAmount                                                      {get; set;}
        public Decimal amount                                                           {get; set;}
        public Decimal vatAmount                                                        {get; set;}
        public String amountInWords                                                     {get; set;}
        public Date chequeDate                                                          {get; set;}

    }


}