public class AP_BankInformationController {
    
    public String BASE_URL = Site.getBaseSecureUrl();
    public integer countBank                            {get; set;} 
    public NSIBPM__Service_Request__c objSR             {get;set;}
    public string strSRID                               {get;set;}
    public Boolean isCommunity                          {get;set;}
    public string strRecordType                         {get;set;}
    public boolean isGuestUser                          {get;set;}
    public String agencyType                            {get; set;} 
    public String pageMessage {get; set;}
    public String redirectToPageName {get; set;} 

   // public string strGeneralInstructions{get;set;}
    /*
        Constructor codes when the Page loads
    */
    public AP_BankInformationController(){
    pageMessage ='';
         countBank = 1;
         objSR = new NSIBPM__Service_Request__c();       
         isCommunity = false;
         isGuestUser = false;     
         system.debug('>>>>strSRID>>>'+strSRID);  
         User objUser = new User();
         try{
         //if there is a contact for this logged in user, then it is a community user
            for(User objUsr : [SELECT Id, Profile.UserLicense.Name, ContactId, Contact.Email, Contact.AccountId,
                                      Contact.FirstName, Contact.LastName, Contact.Account.Website, Contact.Account.Agency_Type__c
                               FROM User
                               WHERE Id =: UserInfo.getUserId()]){
                objUser = objUsr;
                if(objUsr.Profile.UserLicense.name == 'Guest User License'){
                    isGuestUser = true;
                }else{
                    if(objUsr.ContactId != null){
                        isCommunity = true;
                    }
                }
            }

            if(apexpages.currentPage().getParameters().get('Id') != null) {
                strSRID = apexpages.currentPage().getParameters().get('Id');
               
            }
        system.debug('>>>>strSRID>>42>'+strSRID);
        if(strSRID!=null && strSRID!=''){

                //query all the fields configured in the page are query here.
                String strQuery = '';
                strQuery += UtilityQueryManager.getAllFields(NSIBPM__Service_Request__c.getsObjecttype().getDescribe()) ;
                strQuery += '  WHERE Id =:strSRID ';
                for(NSIBPM__Service_Request__c SR:database.query(strQuery)){
                    objSR = SR;
                     system.debug('>>>>objSR>>>'+objSR);
                    agencyType = SR.Agency_Type__c;
                    if(SR.Bank_Details_2__c == true && SR.Bank_Details_3__c == false){
                        countBank = 2;
                    }else{
                        if(SR.Bank_Details_3__c == true){
                            countBank = 3;
                        }

                    }

            }
            
            
         }else{
            if(strRecordType!=null && strRecordType!=''){
                for(RecordType rec:[Select Id,Name from RecordType where developerName=:strRecordType and sObjectType='NSIBPM__Service_Request__c']){
                    objSR.RecordTypeId = rec.Id;
                }
            }
         }
    } catch(Exception e){
        System.debug('Error==>'+e.getMessage());
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Please contact DAMAC support. Error - '+ e.getdmlMessage(0)));
        return;
     }
    /* Constructor End*/
    }

    public PageReference save() {
        system.debug('>>>>strSRID>>>'+strSRID); 
        try {
            countBank ++;
             system.debug('>>>>countBank>>>'+countBank);
             
              system.debug('>>>>objSR>>>'+objSR);
            upsert objSR;   
            system.debug('>>>>objSR>>>'+objSR);  
            /*if(agencyType.equals('Corporate')){
                 system.debug('>>>agencyType>>>'+agencyType);
                PageReference pg = new PageReference(Label.CommunityRedirectURL+'AP_DAMAC_CompanyPersonnel');
                pg.getParameters().put('id',objSR.id);
                return pg;
            }else{
                 system.debug('>>>agencyType>>>'+agencyType);
                PageReference pg = new PageReference(Label.CommunityRedirectURL+'AP_UploadDocuments');
                pg.getParameters().put('id',objSR.id);
                return pg;
            }*/
            return null;
        } catch (DMLException  e) {
            system.debug('>>>>>insavecatch');
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getdmlMessage(0)));
            return null;
        }
        //return pg;
    }
    
    public PageReference upsertRequest(){
         system.debug('>>>>strSRID>>>'+strSRID); 
        try{
             PageReference pg;

            if(!(objSR.filled_page_ids__c).contains('Bank-04') ){
                    objSR.filled_page_ids__c += ',Bank-04';
            }
            upsert objSR; 
            system.debug('>>>agencyType>>>'+agencyType);
            system.debug('>>>redirectToPageName>>>'+redirectToPageName);
            if(String.isBlank(redirectToPageName)){
                pg = Page.AP_UploadDocuments;
                /*if(agencyType.equals('Corporate')){
                    system.debug('>>>agencyType>>>'+agencyType);
                     pg = Page.AP_DAMAC_CompanyPersonnel;
                   
                }else{
                    system.debug('>>>agencyType>>>'+agencyType);
                     pg = Page.AP_UploadDocuments;
                } */
            }else {
                    pg = new PageReference(BASE_URL+'/'+redirectToPageName);
            }
            pg.getParameters().put('id',objSR.id);
            pg.getParameters().put('EditCompanySR','false');
            system.debug('>>>pg>>>'+pg);
            return pg;           
        }
        catch(Exception e){
            system.debug('Exception==> '+e);
            pageMessage = e.getMessage();
            system.debug('pageMessage ==> '+pageMessage );
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getdmlMessage(0)));
            return null;
        }
    }


}