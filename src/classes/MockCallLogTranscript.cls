@isTest 
global with sharing class MockCallLogTranscript implements HTTPCalloutMock{
    global HTTPResponse respond(HTTPRequest req){
        HttpResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/JSON');
        res.setBody('{ "status" : "inserted"}');
        res.setStatusCode(201);
        return res;
    }
}