@isTest
private class ViolationNoticeNotificationServiceTest {

    @isTest
    static void testService() {
        Account portalAccount = new Account(
            LastName = 'Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId()
        );
        insert portalAccount;
        FM_Case__c violationCase = new FM_Case__c(
            Account__c = portalAccount.Id,
            Origin__c = 'Walk-In',
            Status__c = 'Submitted',
            RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Violation Notice').getRecordTypeId()
        );
        insert violationCase;
        SR_Attachments__c violationNotice = new SR_Attachments__c(
            FM_Case__c =violationCase.Id,
            Document_Type__c = 'Violation Notice',
            Notifications_Read__c = false
        );
        insert violationNotice;
        NotificationService.Notification readRequest = new NotificationService.Notification();
        readRequest.className = ViolationNoticeNotificationService.class.getName();
        readRequest.recordId = violationNotice.Id;
        readRequest.accountId = portalAccount.Id;
        Test.startTest();
            ViolationNoticeNotificationService violationNotifService = new ViolationNoticeNotificationService();
            violationNotifService.getNotifications();
            System.assertNotEquals(NULL, violationNotifService.getNotifications(readRequest));
            System.assertNotEquals(NULL, violationNotifService.markUnread(readRequest));
            System.assertNotEquals(NULL, ViolationNoticeNotificationService.markRead(
                new List<NotificationService.Notification>{readRequest}
            ));
        Test.stopTest();
    }

}