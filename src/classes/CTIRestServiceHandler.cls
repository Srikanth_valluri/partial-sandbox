// Class to get available user extensions
Public class CTIRestServiceHandler {
    static Set <Id> campaignIdsSet = new Set<Id> ();
    static List <ID> completedUsers = new List <ID> ();    
    static Map <String, ID> userExtensions = new Map <String, ID> ();
    static Map <ID, User> userMap = new Map <ID, User> ();
    static Map <ID, Decimal> userRankingsMap = new map <ID, Decimal> ();
    static List <ID> finalPowerUsers = new List <ID> ();
    static Inquiry__c tempEligibleInquiry = new Inquiry__c(); 
    static ID inqOwnerId = NULL;
    // to ger user details based on user extensions
    public static Map <String, ID> getUserExtensions (List <String> lstPcExt) {
        userExtensions = new Map <String, ID> ();
        for (User u:[SELECT Extension, Languages_Known__c FROM User WHERE Extension IN :lstPcExt 
                    AND ISACTIVE = TRUE 
                    AND Is_Blacklisted__c = False 
                    AND isUserOnLeave__c = False]) {
            userExtensions.put (u.Extension, u.ID);
            userMap.put (u.id, u);
        }
        return userExtensions;        
    }
    // Method to get eligible extensions based on called number and extensions
    public static responseHandler getEligibleExtensions (String calledNumber, List <String> lstPcExt) {
        finalPowerUsers = new List <ID> ();
        List <String> extensions = new List <String> ();
        getUserExtensions (lstPcExt);
        Set <String> setPcExt = new Set <String> (lstPcExt);
        
        extensions = checkCountry (calledNumber, setPcExt);
        //extensions = new List <String> ();
        System.Debug ('Size of Extensions for Country :::'+extensions.size ());
        System.Debug ('Extensions for Country :::'+extensions);
        
        userRankingsMap = new map <ID, Decimal> ();
        for (Inquiry_User_Assignment_Rules__c rule :[SELECT SetupOwnerId, Net_Direct_Sales_Rank__c 
                                                    FROM Inquiry_User_Assignment_Rules__c 
                                                    WHERE setupOwnerId IN :userExtensions.values ()
                                                    AND Net_Direct_Sales_Rank__c  != NULL]) {
            userRankingsmap.put (rule.setupOwnerId, rule.Net_Direct_Sales_Rank__c);
        }
        completedUsers = new List <ID> ();
        try {
            for (Waybeo_CTI_User__c u: [SELECT User__c FROM Waybeo_CTI_User__c ]) {
                completedusers.add (u.User__c);
            }
        }
        catch (Exception e) {}
        
        if (extensions.size () == 0) {
            extensions = checkforExistingOwnerExtension (calledNumber, setPcExt);
            System.Debug ('===== Existing Inq Owner ==='+inqOwnerId );
            if (inqOwnerId == NULL) {
                System.Debug ('Size of Extensions from Duplication:::'+extensions.size ());
                System.Debug ('Extensions from Duplication :::'+extensions);
                if (extensions.size () == 0) {
                    extensions = checkCampaignExtension (setPcExt);
                    
                    System.Debug ('Size of Extensions from Campaign:::'+extensions.size ());
                    System.Debug ('Extensions from Campaign:::'+extensions);
                    if (extensions.size () == 0) {
                        Map <ID, User> pcUsers = new Map <ID, User> ();
                        pcUsers = getCTIPCs (setPcExt);
                        List <ID> userIds = new List <ID> (pcUsers.keySet ());
                        finalPowerUsers = getSortedUsers (userIds, completedusers, userRankingsmap, 0);
                        for (ID key :finalPowerUsers) {
                            if (extensions.size() < 3) {
                                extensions.add(pcUsers.get (key).Extension);
                            } else {
                                break;
                            }
                        }
                        System.Debug ('Size of Extensions from Default:::'+extensions.size ());
                        System.Debug ('Extensions from Default:::'+extensions);
                    }
                }
                
            } else {
                extensions = new List <String> ();
                Task duplicateFollowUpTask = new Task(
                                        OwnerID = inqOwnerId,
                                        Type='Call-Inbound',
                                        subject='Missed Call',
                                        Activity_Type_3__c = 'Call-Inbound',
                                        Task_Due_Date__c = System.now().addHours(2),
                                        WhatID = tempEligibleInquiry.id,
                                        Description = 'You have missed the call',
                                        Status = 'Not Started');
            insert duplicateFollowUpTask;
                
            }
        }
        System.Debug ('Extensions:::'+extensions);
        responseHandler obj = new responseHandler ();
        obj.Extensions = extensions;
        obj.inqId = tempEligibleInquiry.ID;
        return obj;
    }
    
    // method to get the country related extensions
    public static List <String> checkCountry (String calledNumber, Set <String> lstPcExt) {
        Date todayDate = Date.today();
        campaignIdsSet = new Set<Id> ();
        List <String> countryNames = new List <String> ();
        for (JO_Campaign_Virtual_Number__c thisRecord : [SELECT Id, Related_Campaign__c, Related_Virtual_Number__c, 
                                                        Related_Virtual_Number__r.Name, Related_Campaign__r.country__c
                                                        FROM JO_Campaign_Virtual_Number__c 
                                                        WHERE Related_Virtual_Number__r.Name =: calledNumber
                                                            AND Related_Campaign__r.Marketing_Start_Date__c <=: todayDate 
                                                            AND Related_Campaign__r.Marketing_End_Date__c >=: todayDate]){
            campaignIdsSet.add(thisRecord.Related_Campaign__c); 
            countryNames.add (thisRecord.Related_Campaign__r.country__c);
        }
        System.Debug (countryNames);
        System.Debug (campaignIdsSet);
        return checkCountryCodes (countryNames, lstPcExt);
        
    }    
    // Getting country codes 
    public static List <String> checkCountryCodes (List <String> countryNames, Set <String> lstPcExt) {
        List <String> extensions = new List <String> ();
        Set <String> setPcExt = new Set <String> (lstPcExt);
        List <Integer> extensionNumbers = new List <Integer> ();
        for (String val :lstPcExt) {
            extensionNumbers.add (integer.valueOf (val));
        }
        System.Debug ('Extension Numbers :::'+extensionNumbers);
        
        for (CTI_Country_Code_Extensions__mdt countryCodes :[SELECT Extension__c, label, Active__c 
                                                                FROM CTI_Country_Code_Extensions__mdt
                                                                WHERE Label In : countryNames 
                                                                AND Active__c = TRUE AND Extension__c IN :extensionNumbers]) {
                                                                
            if (setPcExt.contains (String.valueOf (countryCodes.Extension__c)))
                extensions.add(String.valueOf (countryCodes.Extension__c));    
        }
        System.Debug ('Extensions from Country Codes :::'+extensions);
        return extensions;        
    }
    
    // method to get the Duplicate record Owner extension
    public static List <String> checkforExistingOwnerExtension (String callingNumber, Set <String> setPcExt) {
        system.debug('callingNumber='+callingNumber);
        system.debug('pc ext from request='+setPcExt);
        List <String> userExtNos = new List <String> ();
        List <String> extensions = new List <String> ();
        Boolean isDuplicateFound = false;
        Map <Id, User> mapPCUsers = getCTIPCs (setPcExt );
        callingNumber = '%'+callingNumber+'%';
        String sInqStatus = System.Label.CTI_Inq_Status;
        List <String> ctiInqStatus = sInqStatus.split (',');
        tempEligibleInquiry = new Inquiry__c();   
        for (Account existingCustomer : [SELECT Id,OwnerId, Owner.Extension
                                             FROM Account 
                                                WHERE IsPersonAccount = true AND Owner.Extension IN: setPcExt
                                            AND (PersonAssistantPhone LIKE:callingNumber OR PersonHomePhone LIKE:callingNumber 
                                                OR PersonMobilePhone LIKE:callingNumber OR PersonOtherPhone LIKE: callingNumber
                                                OR Phone LIKE:callingNumber OR Mobile__c LIKE: callingNumber 
                                                OR Send_SMS_to_Mobile__c LIKE: callingNumber OR Telephone__c LIKE:callingNumber ) ]) 
        {
            System.debug (existingCustomer.id);
            isDuplicateFound = true;
            if (mapPCUsers.containsKey(existingCustomer.OwnerId)) {
            
                if (mapPCUsers.get(existingCustomer.OwnerId).Extension != null 
                && setPcExt.contains(mapPCUsers.get(existingCustomer.OwnerId).Extension))
                    userExtNos.add(mapPCUsers.get(existingCustomer.OwnerId).Extension);
            }
        }
        System.Debug ('Extesnions from Account : '+userExtNos);
        // If account doesn't exist check for Inquiry owner
        if (userExtNos.size () == 0) {
            InquiryService InquiryServiceObj = new InquiryService();  
                   
            Id inquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_RT).getRecordTypeId();
            system.debug(DAMAC_Constants.INQUIRY_RT);
            system.debug('callingNumber='+callingNumber);
            system.debug(ctiInqStatus);
            
            for (Inquiry__c eligibleInqury : [SELECT Id, Inquiry_Status__c, Campaign__c, OwnerID
                                              FROM Inquiry__c 
                                              WHERE Duplicate__c = false AND Is_Owner_Queue__c = false and
                                                 Inquiry_Status__c in: ctiInqStatus
                                                 AND RecordTypeId =: inquiryRecordTypeId
                                                    AND (Mobile_Phone__c LIKE :callingNumber  OR Mobile_Phone_2__c LIKE :callingNumber  
                                                    OR Mobile_Phone_3__c LIKE :callingNumber OR Mobile_Phone_4__c LIKE :callingNumber 
                                                    OR Mobile_Phone_5__c LIKE :callingNumber)  
                                              Order by Last_Activity_Date__c DESC]) 
            {
                if (eligibleInqury.Inquiry_Status__c != null 
                    && (tempEligibleInquiry.Id == null 
                        || (tempEligibleInquiry.Inquiry_Status__c != eligibleInqury.Inquiry_Status__c 
                            && eligibleInqury.Inquiry_Status__c.containsIgnoreCase(DAMAC_Constants.INQUIRY_ACTIVE_STATUS)) 
                        || (InquiryServiceObj.getLatestDate(tempEligibleInquiry) < InquiryServiceObj.getLatestDate(eligibleInqury)) )){

                    tempEligibleInquiry = eligibleInqury;

                }
            }
            System.Debug ('tempEligibleInquiry: '+tempEligibleInquiry);
            if(tempEligibleInquiry != null) { 
                isDuplicateFound = true;
                if (mapPCUsers.containsKey(tempEligibleInquiry.OwnerId)) {
                    if (mapPCUsers.get(tempEligibleInquiry.OwnerId).Extension != null 
                        && setPcExt.contains(mapPCUsers.get(tempEligibleInquiry.OwnerId).Extension))
                    {
                        userExtNos.add(mapPCUsers.get(tempEligibleInquiry.OwnerId).Extension);
                    }
                } else {
                    inqOwnerId = tempEligibleInquiry.OwnerId;
                }
            }
            System.Debug ('Extensions from Inquiry: '+userExtNos);
        }
        
        if (isDuplicateFound 
            && userExtNos.size() == 0 
            && tempEligibleInquiry.id != null) {
            /*
            Task duplicateFollowUpTask = new Task(Subject='Call',
                                        OwnerID = tempEligibleInquiry.OwnerID,
                                        Task_Due_Date__c = System.now().addHours(2),
                                        WhatID = tempEligibleInquiry.id,
                                        Description = 'You have missed the call',
                                        Type = 'Call',
                                        Status = 'Not Started');
            insert duplicateFollowUpTask;
            */
        }
        System.Debug ('Extensions from duplicate record1 :::'+userExtNos);
        if (isDuplicateFound && userExtNos.size() > 0) {
            for (String key :userExtNos) {
                extensions.add (key);
                if (extensions.size () == 3) break;
            }
        }
        System.Debug ('Extensions from duplicate record 2:::'+userExtNos);
        return userExtNos;
    }
    
    // If duplicate record is not found then check with Campaign
    public static List <String> checkCampaignExtension (Set <String> reqExt ) {
        Date todayDate = Date.today();
        List <String> extensions = new List <String> ();
        Map <String, Boolean> mapUsersEligibleForBreak = new Map <String, Boolean>();
        Map <Id, String> pcExtMap = new Map <Id, String>();
        Map <Id, String> mapCampaignLanguage = new Map <Id, String>();
        Map <Id, List <User>> campaignAssociatedPcMap = new Map <Id, List <User>> ();
        Map <Id, List <Id>> campaignAssociatedPcIdsMap = new Map <Id, List <ID>> ();
        Set <ID> alluserIds = new Set <ID> ();
        for(Campaign__c thisCampaign : [SELECT Id, Campaign_Name__c, Language__c,Country__c,
                                               (SELECT Id, User__c, User__r.Name, User__r.Extension,User__r.Eligible_for_break__c,
                                                       User__r.Languages_Known__c, User__r.Date_of_Joining__c
                                                FROM Assigned_PCs__r 
                                                WHERE User__r.Extension IN: reqExt 
                                                    AND Start_Date__c <=: todayDate
                                                    AND End_Date__c >=: todayDate ) 
                                         FROM Campaign__c 
                                         WHERE Id IN: campaignIdsSet]){
            List<User> userList = new List<User>();
            List <ID> useridsList = new List <ID> ();
            System.Debug (thisCampaign.id);
            for(Assigned_PC__c assignedPC : thisCampaign.Assigned_PCs__r){
                System.Debug (assignedPC);
                mapUsersEligibleForBreak.put(assignedPC.User__r.Extension, assignedPC.User__r.Eligible_for_break__c);
                pcExtMap.put(assignedPC.User__c, assignedPC.User__r.Extension); 
                
                User associatedUser = new User();
                associatedUser.Id = assignedPC.User__c;
                associatedUser.Extension = assignedPC.User__r.Extension;
                associatedUser.Languages_Known__c = assignedPC.User__r.Languages_Known__c;
                associatedUser.Date_of_Joining__c = assignedPC.User__r.Date_of_Joining__c;
                
                if(String.isBlank(thisCampaign.Language__c) 
                || (assignedPC.User__r.Languages_Known__c != null 
                && (assignedPC.User__r.Languages_Known__c.containsIgnoreCase(thisCampaign.Language__c)
                || assignedPC.User__r.Languages_Known__c.containsIgnoreCase(DAMAC_Constants.DEFAULT_LANGUAGE)))
                ) {
                    userList.add (associatedUser); 
                    useridsList.add (associatedUser.ID); 
                    alluserIds.add (associateduser.Id); 
                }
            }
            mapCampaignLanguage.put (thisCampaign.Id, thisCampaign.Language__c);
            campaignAssociatedPcMap.put (thisCampaign.Id, userList); 
            campaignAssociatedPcIdsMap.put (thisCampaign.Id, useridsList);
        }
        if(!campaignAssociatedPcMap.isEmpty() && !pcExtMap.isEmpty()) {
            List <String> campaignLanguageUsers = new List <String> ();
            List <String> defaultLanguageUsers = new List <String> ();
            
            
            for(ID campaignID : campaignAssociatedPcMap.keySet()){
                for (ID userID :getSortedUsers (campaignAssociatedPcIdsMap.get (campaignID), completedUsers, userRankingsmap, 0)) {
                    if (usermap.containsKey (userID)) {
                        User campaignUser = userMap.get (userId);
                        if(campaignUser.Languages_Known__c.containsIgnoreCase (mapCampaignLanguage.get(campaignID)) 
                        && pcExtMap.containsKey (campaignUser.Id)){
                            campaignLanguageUsers.add(campaignUser.Extension);
                        }
                        defaultLanguageUsers.add (campaignUser.Extension);
                    }
                }
            }
            System.Debug ('Campaign Users :::::'+campaignLanguageUsers);
            System.Debug ('Default Users ::::::'+defaultLanguageUsers);
            for(String extNumber : (campaignLanguageUsers != null && campaignLanguageUsers.size() > 0 ? campaignLanguageUsers: defaultLanguageUsers)){
                if(reqExt.contains(extNumber) && 
                    !extensions.contains (extNumber) && 
                    mapUsersEligibleForBreak != null && 
                    mapUsersEligibleForBreak.containsKey(extNumber) && 
                    !mapUsersEligibleForBreak.get(extNumber)){
                        
                    extensions.add(extNumber);
                    reqExt.add(extNumber);
                }
                if(extensions.size() == 3) { break;}
            }
            System.Debug ('Extensions----'+extensions);
            if(extensions.size() <= 1) {
                for(String extNumber : (campaignLanguageUsers != null && campaignLanguageUsers.size() > 0 ? campaignLanguageUsers: defaultLanguageUsers)){
                    if(reqExt.contains(extNumber) && !extensions.contains (extNumber)) {
                        extensions.add (extNumber);
                    }
                    if(extensions.size() == 3) { break;}
                }
            }
        }
        System.Debug ('Campaign Extensions----'+extensions);
        return extensions;
    
    }
    // Method to get Users based on CTI enabled profiles.
    public static Map <Id, User> getCTIPCs (Set <String> pcExtensions) {
        Map <Id, User> userIdDetailsMap = new Map <Id, User>();
        List <String> enabledProfiles = new List <String> ();
        for (CTI_Enabled_Profiles__mdt obj : [SELECT label FROM CTI_Enabled_Profiles__mdt WHERE label != NULL]) 
            enabledProfiles.add (obj.label);
            
        System.Debug (pcExtensions );
        for(User aUser : [SELECT Id, Name, Languages_Known__c, Email, Manager.Email, Leave_Start_Date__c, Extension,
                                 Date_of_Joining__c, Leave_End_Date__c, Sales_office__c,ManagerID, 
                                 Manager.UserRole.Name, Manager.Manager.UserRole.Name,Reshuffling_Block_Date__c 
                          FROM User 
                          WHERE Profile.name IN: enabledProfiles AND
                                Extension IN :pcExtensions AND
                                RoadshowCampaignUser__c !='Y' AND
                                isActive = true AND Is_Blacklisted__c = False and isUserOnLeave__c = False]){
            userIdDetailsMap.put (aUser.Id, aUser);
        }
        System.Debug (userIdDetailsMap);
        return userIdDetailsMap;
    }
    // Method to get users based on ranking 
    public static List <ID> getSortedUsers (List <Id> userList, List <ID> completedUsers, 
                                        Map <ID, Decimal> userRankingsMap, Integer counter) {
        System.Debug ('Completed Users :::::'+completedusers);
        System.Debug ('Counter :::::'+counter);
        finalPowerUsers = new List <ID> ();
        List <ID> powerLineUsersWithOrder = new List <ID> ();
        if (counter <= 1) {
            Map <Decimal, Id> powerLineUsersMap = new Map <Decimal, Id> ();
            List <Decimal> sortedUsersList = new List <Decimal> ();
            for (ID key :userList) {
                if (userRankingsMap.containsKey (key)) {
                    powerLineUsersMap.put (userRankingsMap.get (key), key);
                    sortedUsersList.add (userRankingsMap.get (key));
                }
            }
            sortedUsersList.sort ();
            
            for (Decimal val :sortedUsersList )
                powerLineUsersWithOrder.add (powerLineUsersMap.get (val));
                
            System.Debug (' POWER LINE USERS WITH ORDER ');
            System.Debug (powerLineUsersWithOrder);
            
            if (powerLineUsersWithOrder.size () > 0) {
                
                for (ID key :powerLineUsersWithOrder) {
                    if (completedUsers.size () > 0) {
                        if (!completedUsers.contains (key))
                            finalPowerUsers.add (key);
                    } else {
                        finalPowerUsers.add (key);
                    }
                }
            }
            System.Debug ('Final POWER LINE USERS Before '+finalPowerUsers);
            if (finalPowerUsers.size () == 0) {
                delete [SELECT User__c FROM Waybeo_CTI_User__c];
                completedUsers = new List <ID> ();
                getSortedUsers (userList, completedUsers, userRankingsMap, counter+1);
            }
            
            
        }
        finalPowerUsers = checkThreePowerLineusers (powerLineUsersWithOrder, finalPowerUsers);
        System.Debug ('Final POWER LINE USERS'+finalPowerUsers);
        return finalPowerUsers;
    }
    // To get top 3 power line details.
    public static List <ID> checkThreePowerLineusers (List <ID> totalPowerLineUsers, List <ID> finalPowerLineUsers) {
        List <ID> finalUsers = finalPowerLineUsers;
        Integer totalUsersCount = totalPowerLineUsers.size ();
        Integer finalUsersCount = finalPowerLineUsers.size ();
        List <ID> tempUsers = new List <ID> ();
        if (finalUsersCount < 3) {
            String finalPowerLineUserString = '';
            for (ID key :finalPowerLineUsers) {
                finalPowerLineUserString += key+',';
            }
            for (ID key :totalPowerLineUsers) {
                if (!finalPowerLineUserString.contains (key)) {
                    tempUsers.add (key);  
                }
            }
            System.Debug (tempUsers);
            List <ID> recordsToDel = new List <ID> ();
            if (tempUsers.size () >= 1 && finalUsers.size () == 2) {
                finalUsers.add (tempUsers[0]);
                recordsToDel.add (tempUsers[0]);
            }
            if (tempUsers.size () >= 2 && finalUsers.size () == 1) {
                finalUsers.add (tempUsers[1]);
                recordsToDel.add (tempUsers[1]);
            }
            if (recordsToDel.size () > 0) {
                delete [SELECT User__c FROM Waybeo_CTI_User__c WHERE User__c IN: recordsToDel];
            }
        }        
        return finalUsers;
    }
    
    public class responseHandler {
        public String inqId;
        public List <String> extensions;
        public responseHandler () {
            inqID = '';
            extensions = new List <String> ();
        }
    }
    
}