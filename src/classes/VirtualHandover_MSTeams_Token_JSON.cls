public with sharing class VirtualHandover_MSTeams_Token_JSON {
    public String access_token; //o4yG77TqeaerQkCWKC7o1QaiygQ9
    public string refresh_token; //o4yG77TqeaerQkCWKC7o1QaiygQ9
    public String token_type; //Bearer
    public Integer expires_in;  //3600
    public Integer ext_expires_in; //3600
    public String Scope; 

    public static VirtualHandover_MSTeams_Token_JSON parse(String json){
        return (VirtualHandover_MSTeams_Token_JSON) System.JSON.deserialize(json, VirtualHandover_MSTeams_Token_JSON.class);
    }
}