/******************************************************************************
* Description - Test class developed for HO_NoticeGeneration
*
* Version            Date            Author                    Description
* 1.0                06/02/2018                               Initial Draft
********************************************************************************/
@isTest
private class HO_NoticeGenerationTest {
   static HO_NoticeGeneration.HO_NoticeResponse obj = new HO_NoticeGeneration.HO_NoticeResponse();
     Static String resp ;
    @isTest static void testMethod1(){
         Test.startTest();
          SOAPCalloutServiceMock.returnToMe = new Map<String, HoNotice1.HandOverNoticeResponse_element>();
          HoNotice1.HandOverNoticeResponse_element response1 = new  HoNotice1.HandOverNoticeResponse_element();
          response1.return_x ='{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":null,"ATTRIBUTE1":'+
          '"https://sftest.deeprootsurface.com/docs/e/AR_HO_PACK.pdf","PARAM_ID":"76726"}],'+
          '"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"File (Type,Size,Name) : PDF-212622-XXDCHONOTICE_42847328_1.PDF","ATTRIBUTE3":null,"ATTRIBUTE2":null,"ATTRIBUTE1":"http://dxbhoebtstap.damacholding.home:8033/temp/42847328_HON.pdf","PARAM_ID":"13","ATTRIBUTE4":null}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}'; 
          SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
          Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
          obj = HO_NoticeGeneration.GetHONotice('AR_NHO','reqName','Registrationid');
       // System.assert(resp != null);  
       Test.stopTest();
    }
}