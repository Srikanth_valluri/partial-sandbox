@isTest
private class ValidationCallingListTest {
    @isTest 
    public static void testMethod1() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        // Create common test accounts
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        objSR.Agency__c = objAcc.Id ;
        insert objSR ;
        
        Id dealId = objSR.Deal_ID__c;
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
        insert lstBookings ; 
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
        lstBookingUnits[0].Registration_ID__c = '1520';
        insert lstBookingUnits;
        Id recTypeCOD = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Mortgage Calling List').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        List<Calling_List__c> lstCallingList = new List<Calling_List__c>();
        Calling_List__c callingListObj = new Calling_List__c();
        callingListObj.Call_Count__c = 1;
        callingListObj.RecordTypeId = recTypeCOD;
        callingListObj.Booking_Unit__c = lstBookingUnits[0].Id;
        insert callingListObj;
        lstCallingList.add(callingListObj);
        ValidationCallingList.Validate(lstCallingList);
        update callingListObj;
        
    }
    
    @isTest 
    public static void testMethod2() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        // Create common test accounts
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        objSR.Agency__c = objAcc.Id ;
        insert objSR ;
        
        Id dealId = objSR.Deal_ID__c;
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
        insert lstBookings ; 
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
        lstBookingUnits[0].Registration_ID__c = '1520';
        insert lstBookingUnits;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Approving_User_Id__c = Userinfo.getuserId();
        objCase.OwnerId = Userinfo.getuserId();
        objCase.Approving_User_Role__c='Manager';
        objCase.Parking_Details_JSON__c = '{"Penalties waived AMT":["0"],"Penalties charged AMT":["20"],"Total O/S. Incl. Penalties":["20"],"Balance To Pay":["3220160.00"],"Termination - Refund Value":["a0x0Y000001jABnQAM"],"Termination - Retained Value":["a0x0Y000001jABnQAM"],"Paid %":["40.00%"],"Paid Amount":["AED 73840.00"],"Price Drop by Damac on AC Area":["a0x0Y000001jABnQAM"],"Current PSF on AC Area":["0"],"Current PSF on Saleable Area":["0"],"Sold PSF on Saleable Area":["2,400.37"],"JOPD Area":["1372.29"],"AC Area":["0"],"Saleable Area":["1,372.29"],"Unit Value":["AED 3,294,000.00"],"Inventory Sale Classification":[null],"Handover Status":["Handover in Progress – Notice < 30 days"],"Current ACD":["28-FEB-2019"],"Construction Status":["00"],"Unit Typology":["2 BR , Two Bedroom"],"Agreement Date":["2017-07-18"],"Unit Registration ID":["88395"],"Unit Number":["DRZ/79/7903"],"Unit Details":["a0x0Y000001jABnQAM"],"BU SOA":["88395"]}';
        objCase.NewPaymentTermJSON__c = '["BU SOA","Unit Details","Unit Number","Unit Registration ID","Agreement Date","Unit Typology","Construction Status","Current ACD","Handover Status","Inventory Sale Classification","Unit Value","Saleable Area","AC Area","JOPD Area","Sold PSF on Saleable Area","Current PSF on Saleable Area","Current PSF on AC Area","Price Drop by Damac on AC Area","Paid Amount","Paid %","Termination - Retained Value","Termination - Refund Value","Balance To Pay","Total O/S. Incl. Penalties","Penalties charged AMT","Penalties waived AMT"]';
        insert objCase;
        
        
        List<Calling_List__c> lstCallingList = new List<Calling_List__c>();
        
        Id cbrRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Call Back Request').RecordTypeId;
                    Calling_List__c objCL = new  Calling_List__c();
                    objCL.Account__c =objCase.accountId;
                    objCL.Additional_Comments__c = '';
                    objCL.Assigned_CRE__c   = objCase.OwnerId;
                    objCL.OwnerId   = objCase.OwnerId;
                    objCL.Calling_List_Type__c = 'Case Summary';                
                    objCL.Case__c = objCase.Id;
                    objCL.RecordTypeId = cbrRecordTypeId;
                    System.debug('objCL'+objCL);
                    insert objCL;
        
        objCL.Call_Outcome__c = 'Customer want to re-negotiate';
        
        try{
            update objCL;
            lstCallingList.add(objCL);
            ValidationCallingList.ValidateCBR(lstCallingList);
        }
        catch(Exception e)
        {
            String message = e.getMessage();
            system.assert(message.contains('Please'), 'message=' + message);

        }
        
    }
    
    
}