public class DAMAC_PromotionsRESP{
    public String responseId;   //1583834750148
    public String responseTime; //Tue Mar 10 14:05:50 GMT+04:00 2020
    public String status;   //S
    public String responseMessage;  //Number of Promotions Available = 3
    public Integer elapsedTimeMs;   //5
    public cls_responseLines[] responseLines;
    public boolean complete;
    public class cls_responseLines {
        public String promotionId;  //130
        public String promotion;    //10% PTH Net Return (PTH-A-N)
        public String startDate;    //01-Feb-2020
        public String endDate;  //11-Mar-2020
        public String unitCode; //PTH/15/1506
        public String note;
    }
    public static DAMAC_PromotionsRESP parse(String json){
        return (DAMAC_PromotionsRESP) System.JSON.deserialize(json, DAMAC_PromotionsRESP.class);
    }
}