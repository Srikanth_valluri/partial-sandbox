/*****************************************************************************************
* Description - Test class developed for PassportExtractionService
*
* Version            Date            Author            Description
* 1.0                12/12/17        Naresh (accely)           Initial Draft
*******************************************************************************************/

@isTest(seeAllData=false)
public class PassportExtractionServiceTest{
    
    public static testmethod void PassportServiceBase64Test(){
       
       Test.startTest();
        
       PassportExtractionUtility.Base64Binary b =  new PassportExtractionUtility.Base64Binary();
       SOAPCalloutServiceMock.returnToMe = new Map<String,PassportExtractionService.PassportServiceBase64Response_element>();
       PassportExtractionService.PassportServiceBase64Response_element response_x = new PassportExtractionService.PassportServiceBase64Response_element();
       response_x.return_x = 'S';
       SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
       Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
       PassportExtractionService.PassportServiceHttpSoap11Endpoint obj =  new PassportExtractionService.PassportServiceHttpSoap11Endpoint();
       String response = obj.PassportServiceBase64('filename',b);
       System.assert(response!=null);
       
       Test.stopTest();
    }

     public static testmethod void PassportservTest(){
       Test.startTest();
       
       SOAPCalloutServiceMock.returnToMe = new Map<String,PassportExtractionService.PassportservResponse_element>();
       PassportExtractionService.PassportservResponse_element response_x = new PassportExtractionService.PassportservResponse_element();
       response_x.return_x = 'S';
       SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
       Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
       
       PassportExtractionService.PassportServiceHttpSoap11Endpoint obj =  new PassportExtractionService.PassportServiceHttpSoap11Endpoint();
       String response = obj.Passportserv('www.google.com');
       System.assert(response!=null);
       
       Test.stopTest();
    }

}