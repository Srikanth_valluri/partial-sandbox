@isTest
private class Damac_ValidateSRTest {
    private static testMethod void setup1() {
        
        Location__c loc=new Location__c();
        loc.Location_ID__c='123';
        loc.Location_Code__c = '123';
        insert loc;
        
        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        Account a = new Account();
        a.Name = 'Test Account';
        a.RecordTypeId = accountRTId;
        a.Agency_Short_Name__c = 'testShrName';
        insert a;
        
        Agent_Site__c siteObj = new Agent_Site__c();
        siteObj.Name = 'UAE';
        siteObj.Agency__c = a.id;
        insert siteObj;
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = a.id;
        sr.Delivery_Mode__c = 'Email';
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        sr.Agency__c = a.id;
        sr.Booking_Wizard_Level__c = 'Level 4';
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE;KSA;Lebanon';
        
        insert sr;

        Property__c propObjnew = new Property__c();
        propObjnew.Name = 'DAMAC TOWERS BY PARAMOUNT';
        propObjnew.Property_ID__c   = 455546;
        propObjnew.EOI_Enabled__c = true;
        insert propObjnew;

        EOI_Process__c eoObj = new EOI_Process__c();
        eoObj.No_of_Units__c = 'Single Unit – Token AED 40,000';
        eoObj.Agency__c = a.id;
        eoObj.Mode_of_Token_Payment__c = 'Cash';
        eoObj.Property__c = propObjnew.id ;
        insert eoObj;
        
        Inventory__c invobj = new Inventory__c();
        invobj.Building_Location__c = loc.Id;
        invobj.Bedroom_Type__c = '1 BR'; 
        invobj.CM_Price_Per_Sqft__c=1050.00; 
        //invobj.Location_Code__c = 1234;
        invobj.Brand__c = 'Naia'; 
        invobj.EOI__c = eoObj.id ;
        invobj.IPMS_Bedrooms__c = '1'; 
        invobj.Marketing_Name__c = 'DAMAC MAISON\'S DE VILLE TENORA';
        invobj.Building_Name__c = 'TENORA'; 
        invobj.Property_Name__c = 'TENORA'; 
        invobj.Floor__c = 'AR/1';
        invobj.Unit__c = 'AR/1/110'; 
        invobj.Unit_Type__c = 'HOTEL APARTMENTS';
        invobj.Property_Status__c = 'Ready';
        invobj.Special_Price__c = 1026000.00; 
        invobj.MD_Price__c = 0.00; 
        invobj.List_Price__c = 1080000.00; 
        invobj.Property_Country__c = 'United Arab Emirates'; 
        invobj.Status__c = 'AVAILABLE';
        
        invobj.CurrencyIsoCode = 'AED' ;
        invobj.Floor_Package_ID__c = '123456'; 
        invobj.Floor_Package_Type__c = 'Floor';
        invobj.Floor_Price_Special__c  = '2134' ; 
        invobj.Floor_Price_List__c = '100';
        invobj.Is_Assigned__c = false; 
        invobj.Unit_Location__c = loc.id; 
        invobj.Tagged_to_EOI__c = true;
        insert invobj;
        
        invobj = [select Building_Location__c,Bedroom_Type__c,CM_Price_Per_Sqft__c,Brand__c,EOI__c,IPMS_Bedrooms__c,Marketing_Name__c,Special_Price_calc__c,
        Building_Name__c,Property_Name__c,Unit__c,Floor__c,Unit_Type__c,Property_Status__c,Special_Price__c,MD_Price__c,
        List_Price__c,Property_Country__c,Status__c,Selling_Price__c,CurrencyIsoCode,Floor_Package_ID__c,Floor_Package_Type__c,
        Floor_Price_Special__c,Floor_Price_List__c,Unit_Location__c,Is_Assigned__c,Tagged_to_EOI__c,List_Price_calc__c from inventory__c
        WHERE id =: invobj.id];
        
        Set<String> packageIdsSet = new Set<String>();
        packageIdsSet.add('123456');
        Set<id> invIdsSet = new Set<id>();
        invIdsSet.add(invobj.id);

        List<Booking__c> createBookingList = 
        InitialiseTestData.createBookingRecords(
        new List<Booking__c>{
        new Booking__c(Deal_SR__c = sr.ID)});
        
        List<Booking_Unit__c> createBookingUnitList = 
        InitialiseTestData.createBookingUnitRecords(
        new List<Booking_Unit__c>{
        new Booking_Unit__c(Booking__c = createBookingList[0].Id, Inventory__c = invObj.Id)});
        
        NSIBPM__Service_Request__c newSr = new NSIBPM__Service_Request__c(NSIBPM__Parent_SR__c = sr.Id,Agency__c = a.id,Change_Type__c = 'Remove',Agency_Name__c = null);
        insert newSr;
        Damac_ValidateSR.validateCountryOfSale (newSr.ID);
    }
}