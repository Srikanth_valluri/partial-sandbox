/**********************************************************************************************************************
Description: A Service to capture push back response of robo call.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Author            | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     | 24-11-2019        | Aditya Kishor     | 1.Initial Draft

1.1     | 20-04-2019        | Aishwarya Todkar  | 1.Captured response on Primary and non primary calls respectively.

1.2     | 20-07-2019        | Aishwarya Todkar  | 1.Updated status as connected on primary call if non primary 
                                                    calls are connected.
***********************************************************************************************************************/
@RestResource(urlMapping='/RoboCallUpdate/*')
global with sharing class RoboCallUpdate {
    //*****AKISHOR: 24Nov2019 for recording robo call responses**************//    
    @HttpPatch
    global static ResponseWrapper RoboCallUpdate( String CallDt, String URL, String hangupreason, String StartDt,
                                                String PickupDt, String RecordId, String Duration, String CallStatus) {
        
        ResponseWrapper objResponseWrapper = new ResponseWrapper();
        system.debug('id== '+RecordId);
        system.debug('CallStatus== '+CallStatus);
        system.debug('CallDt== '+CallDt);
        system.debug('URL== '+URL);
        system.debug('StartDt== '+StartDt);
        system.debug('PickupDt== '+PickupDt);
        system.debug('Duration== '+Duration);
        
        if(RecordId !='') {
            List <Added_Call__c> callsToUpsert = new List<Added_Call__c>();

            List <Added_Call__c> listPrimaryCall = new List<Added_Call__c>( [ SELECT
                                                                            Recording_URL__c
                                                                            , Call_Duration__c
                                                                            , Hang__c
                                                                            , Status__c
                                                                            , Pick_Up_Date__c
                                                                            , Start_Date__c
                                                                            , Call_Date__c
                                                                            , Response_Captured__c
                                                                            , Input_String_For_Add_Call__c
                                                                            , Calling_List__c
                                                                            , Campaign_Robo__c
                                                                        FROM
                                                                            Added_Call__c
                                                                        WHERE
                                                                            Id =: RecordId
                                                                        OR
                                                                            Parent_Added_Call_Id__c =: RecordId
                                                                            ] );
            

            if( !listPrimaryCall.isEmpty() && listPrimaryCall != null && listPrimaryCall.size() > 0) {
                //Added_Call__c primaryCall = primaryCL[0];
                
                for( Added_Call__c primaryCall : listPrimaryCall ) {
                    Added_Call__c AC = new Added_Call__c();

                    if( !primaryCall.Response_Captured__c 
                    ||( String.isBlank( primaryCall.Recording_URL__c )
                    && String.isBlank( primaryCall.Call_Duration__c )
                    && String.isBlank( primaryCall.Hang__c )
                    && String.isBlank( primaryCall.Status__c )
                    && primaryCall.Pick_Up_Date__c == null
                    && primaryCall.Start_Date__c == null
                    && primaryCall.Call_Date__c == null ) ) {

                        //Updating primary Added Call
                        AC.Id = primaryCall.Id;  
                    }
                    else {

                        //creating new non-primary added call
                        AC.Calling_List__c = primaryCall.Calling_List__c;
                        AC.Input_String_For_Add_Call__c = primaryCall.Input_String_For_Add_Call__c;
                        AC.Campaign_Robo__c = primaryCall.Campaign_Robo__c;
                        
                        // 20-07-2019  | Aishwarya Todkar  | Updating status as connected on primary call
                        if( CallStatus.equalsIgnoreCase('Connected') ) {
                            Added_Call__c objPrimaryCall = new Added_Call__c( Id = primaryCall.Id, Status__c = 'Connected' );
                            callsToUpsert.add( objPrimaryCall );
                        }
                    }

                    AC.Recording_URL__c = URL;
                    AC.Call_Duration__c = Duration;
                    AC.Hang__c = hangupreason;
                    AC.Status__c = CallStatus.equalsIgnoreCase('Connected') ? 'Connected' 
                                : CallStatus.equalsIgnoreCase('Not%20Connected') ? 'Not Connected' : CallStatus;
                    if( !PickupDt.equalsIgnoreCase('None') )
                        AC.Pick_Up_Date__c = date.valueOf( PickupDt );
                    
                    AC.Start_Date__c = date.valueOf( StartDt );
                    AC.Call_Date__c = date.valueOf( CallDt );
                    AC.Response_Captured__c = true;
                    callsToUpsert.add( AC );
                }
            }    
            system.debug('added call list--' + callsToUpsert);           
            if(callsToUpsert.size() > 0){
                try{
                    Database.upsert( callsToUpsert );
                    objResponseWrapper.status = 'Details Updated Sucessfully';
                    //objResponseWrapper.arrayOfInventory = lstInvToUpdate;
                    objResponseWrapper.statusCode = '200';
                    
                    /*RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                    Restcontext.response.statusCode = 200;   */                              
                } 
                catch( Exception ex ) {
                    system.debug( ex.getMessage() );
                    objResponseWrapper.errorMessage = ex.getMessage();
                    objResponseWrapper.statusCode = '400';
                    /*
                    RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                    Restcontext.response.statusCode = 400;
                    System.debug('============= response : ' + JSON.serialize(objResponseWrapper));
                    return;            */         
                }   
            }
        }
        return objResponseWrapper;
    }

    //for sending response    
    global class ResponseWrapper {
        global String status;
        global String statusCode;
        global String errorMessage;
       
        global ResponseWrapper() {
            this.status = '';
            this.statusCode = '';
            this.errorMessage = '';
        }
    }

}