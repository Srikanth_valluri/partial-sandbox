@isTest
public class DAMAC_INVENTORY_MOBILE_TEST {
    @testSetup static void setup() {
        Account acc = new Account ();
        acc.Name = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '969696';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        insert acc;
    
        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c  = 1;
        newProperty.Property_Code__c    = 'VIR' ;
        newProperty.Property_Name__c    = 'VIRIDIS @ AKOYA OXYGEN' ;
        newProperty.District__c = 'AL YUFRAH 2' ;
        newProperty.AR_Transaction_Type__c  = 'INV VIR' ;
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR' ;
        newProperty.Brokerage_Distribution_Set__c   = '11600' ;
        newProperty.Sales_Commission_Dist_Set__c    = '11601' ;
        newProperty.Currency_Of_Sale__c = 'AED' ;
        newProperty.Signature_Col_Customer_Stmt__c  = 'Front Line Investment Management Co. LLC' ;
        newProperty.EOI_Enabled__c = true;
        insert newProperty;
        
        Marketing_Documents__c mrkDoc = new Marketing_Documents__c();
        mrkDoc.Name = 'Aurum Villas';
        mrkDoc.Has_Multiple_Property__c = true;
        mrkDoc.Marketing_Name__c = 'Aurum Villas';

        insert mrkDoc;
    
        Inventory__c invent = new Inventory__c();
        invent.CM_Price_Per_Sqft__c = 20;
        invent.Marketing_Name_Doc__c = mrkDoc.id;
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1' ;
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.ID;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Released';
        invent.special_price__c = 1000000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234'; 
        invent.Floor_Package_ID__c = ''; 
        invent.property_id__c = newProperty.id;
        insert invent;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);        
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        sr.Agent_Name__c = UserInfo.getUserId();
        sr.Token_Amount_AED__c = 40000;
        sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Change Agent').getRecordTypeId();
        sr.Booking_Wizard_Level__c = null;
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE';
        sr.Mode_of_Payment__c = 'Cash';
        sr.agency__c = acc.id;
        insert sr;
        
        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk[0].Deal_SR__c = sr.id;
        insert lstbk;
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        loc.Property_ID__c = '123';
        insert loc; 
        
        Payment_Plan__c pp = new Payment_Plan__c();        
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today(); 
        pp.Building_Location__c =  loc.id;  
        pp.term_id__c = '1234';   
        insert pp; 
        
        List<Booking_Unit__c> BUList = new List<Booking_Unit__c>();
        Booking_Unit__c bu1 = new Booking_Unit__c();
        bu1.Booking__c = lstbk[0].id;
        bu1.Payment_Method__c = 'Cash';
        bu1.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu1.Primary_Buyer_s_Name__c = 'testNSI';
        bu1.Primary_Buyer_s_Nationality__c = 'Russia';
        bu1.Inventory__c = invent.id;
        bu1.Registration_ID__c = '1234'; 
        bu1.Payment_Plan_Id__c = pp.id;      
        BUList.add(bu1);
        insert BUList;
        
    }
    @isTest static void inventory_mobile () {
        Test.StartTest();
        ID invId = [SELECT ID FROM Inventory__c LIMIT 1].ID;
        
        DAMAC_INVENTORY_MOBILE mobObject = new DAMAC_INVENTORY_MOBILE();
        apexpages.currentpage().getparameters().put ('invId', invId);
        mobObject.initUnitDetails();
        mobObject.initPaymentPlans();
        ID recId = DAMAC_INVENTORY_MOBILE.createSalesOffer (invId);
        DAMAC_INVENTORY_MOBILE.salesOfferLink (recId);
        mobObject.initUnits();
        mobObject.getLocation ();
        mobObject.getCities ();
        mobObject.getUnitTypes ();
        mobObject.getBedRooms ();
        mobObject.getViewTypes ();
        mobObject.selectedPropTypes = 'null';
        mobObject.selectedDistrict = 'null';
        mobObject.selectedCity = 'null';
        mobObject.selectedStatus = 'null';
        
        mobObject.selectedAcd = 'null';
        mobObject.selectedMarketNames = 'null';
        
        mobObject.selectedUnitTypes = 'null';
        mobObject.selectedRooms = 'null';
        mobObject.selectedViewTypes = 'null';
        
        mobObject.initFilterDetails();
        mobObject.retrieveInventories();
        mobObject.initProjectDetails();
        
        mobObject.getProjects();
        mobObject.getBedRooms();
        DAMAC_INVENTORY_MOBILE.addOrRemoveFavorite (invId, true);
        apexpages.currentpage().getparameters().put ('type', 'All');
        apexpages.currentpage().getparameters().put ('term', 'paramount');
        
        mobObject.searchInventory ();
        mobObject.loadFavorites();
        mobObject.loadUnits ();
        Test.StopTest();
    }
}