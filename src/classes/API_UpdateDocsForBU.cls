/**********************************************************************************************************************
Class Name : API_UpdateDocsForBU 
Description : To upload invoice POP docs from Agents app to IPMS.
Test class : API_UpdateDocsForBUTest
========================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   15-10-2020      | Subin C Antony	  | Created initial version.
1.0     |   28-10-2020      | Subin C Antony	  | Changes in remote URL for uploading documents to IPMS.
***********************************************************************************************************************/

@RestResource(urlMapping='/API_UpdateDocsForBU')
global class API_UpdateDocsForBU {
    public static final String IPMS_BASE_URL_STG = 'https://ptctest.damacgroup.com/agent-app/api/v1/';
    public static final String IPMS_BASE_URL_PRD = 'https://ptc.damacgroup.com/agentapp-prod/api/v1/';
    public static final String IPMS_UPLD_END_PNT = 'customers/upload';
    
    public static Boolean IsSandbox;
    public static Map<Integer, String> statusCodeMap;
    static {
        statusCodeMap = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Bad Request',
        4 => 'Error in Remote IPMS Server Callout',
        5 => 'Internal Server Error',
        6 => 'Something went wrong'};
         
        IsSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    
    @HttpPost
    global static void doPost(String bookingUnitId, String invoiceReferenceNumber, String invoiceDate, Decimal invoiceAmount, 
    String taxInvoiceAttachmentID, String vatCertificateAttachmentID) {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg, exceptionMsg, successMsg;
        
        if(String.isBlank(bookingUnitId)){
            errorMsg = 'Booking Unit ID for which documents should be updated is not provided.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, NULL);
            return;
        }
        
        if(String.isBlank(invoiceReferenceNumber) || String.isBlank(invoiceDate) || NULL == invoiceAmount){
            errorMsg = 'Please provide values for invoice number, date, and amount.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, NULL);
            return;
        }
        
        Date invDate = NULL;
        try{
            if(String.isNotBlank(invoiceDate)) {
                invDate = Date.valueOf(invoiceDate);
            }
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            errorMsg = 'Please provide a proper date formatted as \'yyyy-mm-dd\'.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, ex.getMessage());
            return;
        }
        
        Booking_Unit__c unit;
        try{
            unit = [SELECT id, name, Unit_Name__c, Registration_ID__c FROM Booking_Unit__c WHERE Id = :bookingUnitId];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            exceptionMsg = ex.getMessage() + ' : ' + ex.getStackTraceString();
            unit = NULL;
        }
        
        if(NULL == unit){
            errorMsg = 'The Booking Unit ID provided does not match any records.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
            return;
        }
        else if(String.isBlank(unit.Registration_ID__c)) {
            errorMsg = 'The Booking Unit does not have an IPMS registration_ID.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
            return;
        }
        
        Set<String> attachmentIds = new Set<String>();
        if(String.isNotBlank(taxInvoiceAttachmentID)) {
            attachmentIds.add(taxInvoiceAttachmentID);
        }
        if(String.isNotBlank(vatCertificateAttachmentID)) {
            attachmentIds.add(vatCertificateAttachmentID);
        }
        
        if(attachmentIds.size() < 1) {
            errorMsg = 'At least one valid attachmentId should be provided.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
            return;
        }
        
        Map<ID, Attachment> attachmentMap;
        try {
            attachmentMap = new Map<ID, Attachment>(
                [SELECT id, name, parentId, contentType, description, body FROM Attachment 
                 WHERE id IN :attachmentIds AND parentId = :bookingUnitId]);
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            exceptionMsg = ex.getMessage() + ' : ' + ex.getStackTraceString();
            attachmentMap = NULL;
        }
        
        if(NULL == attachmentMap || attachmentMap.size() < 1){
            errorMsg = 'The attachmentIDs provided are invalid and does not match any records.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
            return;
        }
        
        /* Building upload request for IPMS API callout : .................BEGIN */
        IPMS_DocUploadRequestWrapper remoteRequest = new IPMS_DocUploadRequestWrapper();
        remoteRequest.registration_id = unit.Registration_ID__c;
        remoteRequest.ext_inv_ref = invoiceReferenceNumber;
        remoteRequest.ext_inv_date = invoiceDate;
        remoteRequest.ext_inv_amount = invoiceAmount;
        remoteRequest.invoices = new List<IPMS_DocumentDataItem>();
        for(String attachmentId : attachmentMap.keySet()) {
            Attachment invoiceDocumentAttached = attachmentMap.get(attachmentId);
            if(NULL != invoiceDocumentAttached) {
                IPMS_DocumentDataItem docDataToUpload = new IPMS_DocumentDataItem();
                docDataToUpload.document_type = (String.isNotBlank(vatCertificateAttachmentID) && 
                                             vatCertificateAttachmentID.equalsIgnoreCase(attachmentId)) ? 
                    'Vat_Certificate' : 'Tax_Invoice';
                docDataToUpload.mime_type = invoiceDocumentAttached.contentType;
                docDataToUpload.file_name = invoiceDocumentAttached.name;
                docDataToUpload.document = EncodingUtil.base64Encode(invoiceDocumentAttached.body);
                
                remoteRequest.invoices.add(docDataToUpload);
            }
        } /* Building upload request for IPMS API callout : .................END */
        
        attachmentMap = NULL; /* TO FREE HEAP !!!!! ?????? */
        
        System.debug('HEAP_Alloc :...................................................: ' + Limits.getHeapSize());
        System.debug('HEAP_Limit :...................................................: ' + Limits.getLimitHeapSize());
        System.debug('HEAP_Balance :.................................................: ' + (Limits.getLimitHeapSize() - Limits.getHeapSize()));
        String requestPayload;
        try {
            requestPayload = JSON.serialize(remoteRequest, false);
        }
        catch(System.LimitException ex) { /* Will not be caught */
            system.debug(ex.getMessage());
            exceptionMsg = ex.getMessage() + ' : ' + ex.getStackTraceString();
            requestPayload = NULL;
        }
        
        cls_meta_data remoteResponse;
        if(String.isNotBlank(requestPayload)) {
            remoteResponse = uploadDocumentsToIPMS(JSON.serialize(remoteRequest, false)); /* HTTP callout to IPMS server */
        	System.debug('IPMS_CALLOUT_RESPONSE_WRAP : ................................... ' + remoteResponse);
        }
        else {
            remoteResponse = new cls_meta_data();
            remoteResponse.status_code = 5;
            remoteResponse.title = statusCodeMap.get(5);
            remoteResponse.message = 'Exception while trying to build request for uploading documents to IPMS.';
            remoteResponse.developer_message = exceptionMsg;
        }
        
        ResponseWrapper responseWrapper = new ResponseWrapper();
        responseWrapper.meta_data = remoteResponse;
        responseWrapper.data = NULL;
        
        // getErrorResponse(2, statusCodeMap.get(2), 'TEST', NULL);
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    } /* doPost() */
    
    private static cls_meta_data uploadDocumentsToIPMS(String requestPayload) {
        ResponseWrapper remoteResponseWrapper;
        String excptMsg;
        
        HttpRequest uploadDocsRequest = new HttpRequest();
        String baseUrl = IsSandbox ? IPMS_BASE_URL_STG : IPMS_BASE_URL_PRD;
        uploadDocsRequest.setEndpoint(baseUrl + IPMS_UPLD_END_PNT);
        uploadDocsRequest.setMethod('POST');
        uploadDocsRequest.setheader('Content-Type','application/json');
        uploadDocsRequest.setbody(requestPayload);
        uploadDocsRequest.setTimeout(120000);
        
        HttpResponse uploadDocsResponse;
        String exceptionMsg;
        try{
            if(!Test.isRunningTest()) {
                uploadDocsResponse = new Http().send(uploadDocsRequest);
            }
            else {
                // for test excecution context
                uploadDocsResponse = new HttpResponse();
                uploadDocsResponse.setStatus('OK');
                uploadDocsResponse.setStatusCode(200);
                uploadDocsResponse.setBody('{"data":null,"meta_data":{"status_code":1,"message":"Success","title":"Success","developer_message":null}}');
            }
                
            system.debug('IPMS_CALLOUT_Response : ............................................ ' + uploadDocsResponse);
            system.debug('IPMS_CALLOUT_ResponseStatus : ............................................ ' + uploadDocsResponse.getStatusCode());
            system.debug('IPMS_CALLOUT_ResponseBody : ............................................ ' + uploadDocsResponse.getBody());
            if(uploadDocsResponse.getStatusCode() == 200 && NULL != uploadDocsResponse.getBody() && 
               !(uploadDocsResponse.getBody().equals(''))){
                   remoteResponseWrapper = (ResponseWrapper)JSON.deserialize(uploadDocsResponse.getBody(), ResponseWrapper.class);
               }
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            system.debug(ex.getStackTraceString());
            remoteResponseWrapper = NULL;
            excptMsg = ex.getMessage() + ex.getStackTraceString();
        }
        
        if(NULL == remoteResponseWrapper || NULL == remoteResponseWrapper.meta_data) {
            cls_meta_data remoteResponse = new cls_meta_data();
            remoteResponse.status_code = 2;
            remoteResponse.title = statusCodeMap.get(2);
            remoteResponse.message = 'Error in Remote IPMS Service Callout for Document Upload.';
            remoteResponse.developer_message = excptMsg;
            
            return remoteResponse;
        }
        else {
            return remoteResponseWrapper.meta_data;
        }
    }
    
    private static void getErrorResponse(Integer statusCode, String title, String responseMessage, String devMessage) {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = statusCode;
        responseMetaData.title = title;
        responseMetaData.message = responseMessage;
        responseMetaData.developer_message = devMessage;
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    public class IPMS_DocUploadRequestWrapper {
        public String registration_id;
        public String ext_inv_ref;
        public String ext_inv_date;
        public Decimal ext_inv_amount;
        public List<IPMS_DocumentDataItem> invoices;
    }
    
    public class IPMS_DocumentDataItem {
        public String document_type;
        public String mime_type;
        public String file_name;
        public String document;
    }

    /* Wrapper classes for returning reponse */
    public class ResponseWrapper {
        public cls_meta_data meta_data;
        public cls_data data;
    }

    public class cls_meta_data {
        public Integer status_code;
        public String message;
        public String title;
        public String developer_message;   
    }

    public class cls_data {}
}