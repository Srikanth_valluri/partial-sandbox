public without sharing class CaseTriggerHandlerRPAgreement {
    public static final map<String, Id> mapRecordTypeId ;
    static {
        mapRecordTypeId = new map<String, Id>();
        for(RecordType recTypeInst : [select Id, DeveloperName from RecordType
                                        where SobjectType = 'Case']) {
            mapRecordTypeId.put(recTypeInst.DeveloperName, recTypeInst.Id);
        }
        system.debug('mapRecordTypeId==='+mapRecordTypeId);
    }

    public void afterUpdate(map<Id,Case> newMap, map<Id,Case> oldMap) {
        Integer countCaseClosed = 1;
        set<Id> setClosedCase = new set<Id>(); 
        Id RPAgreementRecordType = mapRecordTypeId.get('Rental_Pool_Agreement');
        Id RPAgreementEHRecordType = mapRecordTypeId.get('Rental_Pool_Agreement_Early_Handover');
        system.debug('RPAgreementRecordType=='+RPAgreementRecordType);
        for(Case objC : newMap.values()) {
            system.debug('RecordType=='+objC.RecordTypeId);
            system.debug('Status=='+objC.Status);
            system.debug('Booking unit=='+objC.Booking_Unit__c);
            system.debug('Commencement Date=='+objC.Commencement_Date__c);
            system.debug('Reg ID=='+objC.Booking_Unit__r.Registration_ID__c);
             if((objC.RecordTypeId == RPAgreementRecordType || objC.RecordTypeId == RPAgreementEHRecordType)
                    && objC.Status.equalsIgnoreCase('Closed') && objC.Commencement_Date__c != null
                    && objC.Status != oldMap.get(objC.Id).Status && objC.Booking_Unit__c != null ) {
                system.debug('countCaseClosed==='+countCaseClosed);
                if(countCaseClosed <= 100) {
                    setClosedCase.add(objC.Id);
                    system.debug('setClosedCase==='+setClosedCase);
                }
                else {
                    updateRegStatus(setClosedCase);
                    setClosedCase = new set<Id>();
                    setClosedCase.add(objC.Id);
                    countCaseClosed = 1;
                }
                countCaseClosed++;
             }
        }
        system.debug('setClosedCase==='+setClosedCase);
        if(!setClosedCase.isEmpty()) {
            updateRegStatus(setClosedCase);
        }
    }

    @future(callout=true)
    public static void updateRegStatus(set<Id> setClosedCase) {
        List<Error_Log__c> listErrorLog = new List<Error_Log__c>();
        List<Booking_Unit__c> listUpdateBookingUnit = new List<Booking_Unit__c>();
        List<RPDocDetailsWrapper> lstRPDocDetails = new List<RPDocDetailsWrapper>();
        for(Case caseInst: [select Id, CaseNumber, AccountId, Booking_Unit__c, Commencement_Date__c, 
                                Booking_Unit__r.Registration_ID__c 
                                from Case 
                                where Id IN: setClosedCase and Booking_Unit__r.Registration_ID__c != null]) {
            Date commDate = caseInst.Commencement_Date__c;
            DateTime commDateTime = datetime.newInstance(commDate.year(), commDate.month(), commDate.day());
            String strDate = commDateTime.format('dd-MMM-YY');
            //For Agreement RP send (CREATE_RP_RECORD, Y, Date, CaseNo, RegId)
            String rentalDocResponse = RentalPoolUpdateInIPMS.rentalPoolUpdateCallout('CREATE_RP_RECORD','Y',strDate, caseInst.CaseNumber,caseInst.Booking_Unit__r.Registration_ID__c);
            if(String.isNotBlank(rentalDocResponse)) {
                map<String, Object> mapDeserializeRPDoc = (map<String,Object>)JSON.deserializeUntyped(rentalDocResponse);
                system.debug('mapDeserializeRPDoc==='+mapDeserializeRPDoc);
                if(mapDeserializeRPDoc.get('status') == 'S') {
                    String StringRPData = JSON.serialize(mapDeserializeRPDoc.get('data'));
                    system.debug('StringRPData==='+StringRPData);
                    lstRPDocDetails = 
                        (List<RPDocDetailsWrapper>)JSON.deserialize(StringRPData, List<RPDocDetailsWrapper>.class);
                    system.debug('lstRPDocDetails==='+lstRPDocDetails);
                    for(RPDocDetailsWrapper rpaInst: lstRPDocDetails) {
                        if(rpaInst.PROC_STATUS == 'S') {
                            Booking_Unit__c bookingUnitObj = new Booking_Unit__c();
                            bookingUnitObj.Id = caseInst.Booking_Unit__c;
                            bookingUnitObj.Rental_Pool__c = true;
                            bookingUnitObj.Rental_Pool_Start_Date__c = commDateTime.format('dd-MMM-YY');
                            listUpdateBookingUnit.add(bookingUnitObj);
                        }
                        else {
                            Error_Log__c objErr = createErrorLogRecord(caseInst.AccountId, caseInst.Booking_Unit__c, caseInst.Id);
                            objErr.Error_Details__c = 'Error : '+rpaInst.PROC_MESSAGE; 
                            listErrorLog.add(objErr);
                        }
                    }
                }
                else {
                    Error_Log__c objErr = createErrorLogRecord(caseInst.AccountId, caseInst.Booking_Unit__c, caseInst.Id);
                    objErr.Error_Details__c = 'Error : '+(String)mapDeserializeRPDoc.get('message'); 
                    listErrorLog.add(objErr);
                }
            }
            else {
                Error_Log__c objErr = createErrorLogRecord(caseInst.AccountId, caseInst.Booking_Unit__c, caseInst.Id);
                objErr.Error_Details__c = 'Error : No response from IPMS'; 
                listErrorLog.add(objErr);
            }
        }
        if(!listUpdateBookingUnit.isEmpty()) {
            update listUpdateBookingUnit;
        }
        system.debug('listUpdateBookingUnit=='+listUpdateBookingUnit);
        if(!listErrorLog.isEmpty()) {
            insertErrorLog(listErrorLog);
        }
    }
    
    
    
    public static void insertErrorLog(list<Error_Log__c> listObjErr){
        try{
            insert listObjErr;
        }catch(Exception ex){
            //errorMessage = 'Error : '+ ex.getMessage();
            system.debug('Error Log ex*****'+ex);
        }
    }

    public static Error_Log__c createErrorLogRecord(Id accId, Id bookingUnitId, Id caseId){
        Error_Log__c objErr = new Error_Log__c();
        objErr.Account__c = accId;
        objErr.Booking_Unit__c = bookingUnitId;
        objErr.Case__c = caseId;
        //objErr.Process_Name__c = 'Rental Pool Agreement';
        return objErr;
    }
    
    public class RPDocDetailsWrapper {
        public String PROC_STATUS {get;set;}
        public String PROC_MESSAGE {get;set;}
        public String ATTRIBUTE1 {get;set;}
        public String ATTRIBUTE2 {get;set;}
        public String ATTRIBUTE3 {get;set;}
        public String ATTRIBUTE4 {get;set;}
        public String PARAM_ID {get;set;}

        public RPDocDetailsWrapper(){

        }
    }

}