@isTest
public class Test_MobileAppLoginController {

    static void init(){
        cookie Name = new Cookie('username', 'test', null, 365, false);
        //cookie PasswordCokies = new Cookie('password', 'test', null, 365, false);
        ApexPages.currentPage().setCookies(new Cookie[] {Name});
        
        
    }
    
    @isTest static void loginGuestUser() {
        
        Test.StartTest();
        init();
        
        MobileAppLoginController damacLogin = new MobileAppLoginController();
        damacLogin.rememberMe = true;
        damacLogin.username = 'test@damac.com';
        damacLogin.password = 'salesforce1';
        damacLogin.login();
        
        PageReference pg = new PageReference('/Damac_Home');
        //system.assertEquals(damacLogin.redirectToHome(),pg);
        damacLogin.redirectToHome();
        damacLogin.forgotPassword();
        damacLogin.fetchUsername('test');
        ApexPages.currentPage().getParameters().put('EmailAddress', 'test@damac.com');
        damacLogin.forgotPassword();
        damacLogin.Register();
        
        list<user> listUser = [SELECT Id,
                                      Username
                                 FROM User];
        if( listUser.size()>0){
            ApexPages.currentPage().getParameters().put('EmailAddress', listUser[0].Username);
        damacLogin.forgotPassword();
        }                        
        Test.stopTest();
        
    }
}