public without sharing class PenaltyWaiverUtility {

    public static final String PENALTY_WAIVER_RECTYPE = 'Penalty_Waiver';
    public static final String CASE_SOBJ = 'Case';
    public static final String STATUS_NEW = 'New';
    public static final String STATUS_SUBMITTED = 'Submitted';
    public static final String ATTACH_TYPE_SHORT_CRF = 'crf';
    public static final String ATTACH_TYPE_FULL_CRF = 'CRF Form';
    public static final String ATTACH_TYPE_SHORT_POA = 'poa';
    public static final String ATTACH_TYPE_FULL_POA = 'Power of Attorney';
    public static final String ATTACH_TYPE_SHORT_SOA = 'soa';
    public static final String ATTACH_TYPE_FULL_SOA = 'Statement of Account';
    public static final String ATTACH_TYPE_SHORT_SOP = 'sop';
    public static final String ATTACH_TYPE_FULL_SOP = 'Statement of Penalty';
    public static final String SPAN_TICK = '<span class="symbol icon-tick"></span>';
    public static final String SPAN_CROSS = '<span class="symbol icon-cross"></span>';

    public static map<Id, Booking_Unit__c> getAllRelatedBookingUnits( Id accountId ) {

        /*set<Id> setBookingId  = new set<Id>();
        for( Buyer__c objBuy : [ select id, Booking__r.Id from Buyer__c where Primary_Buyer__c = true AND Account__c = :accountId ] ) {
            if( objBuy.Booking__c != null ) {
                setBookingId.add( objBuy.Booking__c );
            }
        }
        */

        set<String> setActiveStatus = new set<String>();
         for(Booking_Unit_Active_Status__c unitStatusInst: [select Id, Status_Value__c
                                                            from Booking_Unit_Active_Status__c
                                                            where Status_Value__c != null]) {
            setActiveStatus.add(unitStatusInst.Status_Value__c);
         }
        system.debug('==setActiveStatus =='+setActiveStatus );
        return new map<Id, Booking_Unit__c>([SELECT Id
                                                  , Registration_ID__c
                                                  , Unit_Details__c
                                                  , Inventory__r.Building_Code__c
                                                  , Registration_status__c
                                                  , Booking__r.Account__r.Nationality__c
                                                  , Booking__r.Account__r.AnnualRevenue
                                                  , Booking__r.Account__r.Email__pc
                                                  , Booking__r.Account__r.Email__c
                                                  , Booking__r.Account__r.IsPersonAccount
                                                  , Unit_Name__c
                                                  , Rental_Pool__c
                                                  , Registration_DateTime__c
                                                  , HOS_Name__c
                                                  , Manager_Name__c
                                                  , Property_Consultant__c
                                                  , CurrencyIsoCode 

                                                  //Unit Details
                                                  , Property_Name_Inventory__c
                                                  , Inventory__r.Property_City__c
                                                  , Inventory__r.Bedroom_Type__c
                                                  , Inventory__r.Unit_Type__c
                                                  , Booking_Unit_Type__c
                                                  , Unit_Type__c
                                                  , Permitted_Use__c
                                                  , Inventory__r.Property__c
                                                  , Inventory__r.Property_Status__c
                                                  , Booking_Type__c
                                                  , Plot_Price__c
                                                  , Booking__r.CreatedDate
                                                  , Agreement_Date__c
                                                  , Inventory__r.Unit_Plan__c
                                                  , Inventory__r.Floor_Plan__c
                                                  , JOPD_Area__c
                                                  , ( SELECT Id
                                                            , OptionsName__c
                                                            , PromotionName__c
                                                            , SchemeName__c
                                                            , CampaignName__c
                                                        FROM Options__r
                                                    ORDER BY CreatedDate DESC
                                                       LIMIT 1 )
                                                  , ( SELECT Id
                                                            , Name
                                                         FROM Payment_Plans__r
                                                        WHERE Status__c IN ( 'Active', null )
                                                     ORDER BY CreatedDate DESC
                                                        LIMIT 1 )
                                                  //Flags
                                                  , Mortgage__c
                                                  , DP_OK__c
                                                  , Doc_OK__c

                                                  //Unit Status
                                                  , OQOOD_Reg_Flag__c
                                                  , Early_Handover__c
                                                  , Handover_Flag__c
                                                  , PCC_Release__c

                                                  //Additional Details
                                                  , Area__c
                                                  , Area_Varied__c
                                                  , Unit_Selling_Price__c
                                                  , Inventory_Area__c

                                                  //?
                                                  , Bedroom_Type__c
                                                  , Token_Amount_val__c
                                                  , Rental_Pool_Agreement__c
                                                  , Token_Amount__c
                                                  , Requested_Token_Amount__c
                                                  , Booking__r.Account__c
                                                  , Penalty_Amount__c
                                               FROM Booking_Unit__c
                                              WHERE Booking__r.Account__c = :accountId
                                                AND Registration_status__c IN :setActiveStatus ] );
    }

    public static Id getRecordTypeId( String strDeveloperName, String strObjectName ) {
        list<RecordType> lstRecordType = [SELECT Id
                                               , Name
                                               , DeveloperName
                                               , SobjectType
                                            FROM RecordType
                                           WHERE DeveloperName = :strDeveloperName
                                             AND SobjectType = :strObjectName
                                             AND IsActive = true ];
        if( lstRecordType != null && lstRecordType.isEmpty() == false ) {
            return lstRecordType[0].Id;
        }
        return null;
    }

   /* public static Case getExistingOpenCase( Id bookingUnitId ) {
        list<SR_Booking_Unit__c> lstexistingOpenCases = [ SELECT Id
                                                               , Booking_Unit__c
                                                               , Case__c
                                                               , Excess_Amount_Refund_Liability_AED__c
                                                               , Is_Bulk_Deal__c
                                                               , SR__c
                                                               , Unit_Status__c
                                                            FROM SR_Booking_Unit__c
                                                           WHERE Booking_Unit__c = :bookingUnitId
                                                             AND Case__r.Approval_Status__c != 'Approved'
                                                             AND Case__r.RecordTypeId = :getRecordTypeId( PENALTY_WAIVER_RECTYPE, CASE_SOBJ )
                                                        ORDER BY CreatedDate DESC ];
        if( lstexistingOpenCases != null && !lstexistingOpenCases.isEmpty() ) {
            return getCaseDetails( lstexistingOpenCases[0].Case__c );
        }
        return new Case();
    }*/

    public static map<Id,SR_Attachments__c> fetchCaseAttachments( Id caseId ) {
        return new map<Id,SR_Attachments__c>([ SELECT Id
                                                  , Case__c
                                                  , Name
                                                  , View__c
                                                  , Attachment__c
                                                  , Type__c
                                                  , IsValid__c
                                                  , Attachment_URL__c
                                               FROM SR_Attachments__c
                                              WHERE Case__c = :caseId
                                           ORDER BY CreatedDate DESC ]);
    }

    /*public static Case getCaseDetails( Id caseId ) {
        list<Case> lstCase = [SELECT Id
                                   , Amount_to_be_waived__c
                                   , Percent_to_be_waived__c
                                   , Status
                                   , Penalty_Category__c
                                   , IsPOA__c
                                   , POA_Name__c
                                   , Additional_Details__c
                                   , POA_Relationship_with_Buyer__c
                                   , POA_Expiry_Date__c
                                   , POA_Issued_By__c
                                   , Is_CRF_Uploaded__c
                                   , Purpose_of_POA__c
                                FROM Case
                               WHERE Id = :caseId];
        if( lstCase != null && !lstCase.isEmpty() ) {
            return lstCase[0];
        }
        return new Case();
    }*/

    public static list<Task> getTasks( set<Id> setCaseIds ) {
        return new list<Task>( [ SELECT WhoId
                                      , WhatId
                                      , Type
                                      , Status
                                      , OwnerId
                                      , Id
                                      , Subject
                                      , CreatedDate
                                      , Description
                                      , Assigned_User__c
                                      , ActivityDate
                                      , Owner.Name
                                   FROM Task
                                  WHERE Id IN : setCaseIds ] );
    }

    public static map<Id, Case> getCaseMap( set<Id> setCaseIds ) {
        return new map<Id, Case>( [ SELECT Id
                                         , RecordTypeId
                                         , IsPOA__c
                                         , Percent_to_be_waived__c
                                         , POA_Name__c
                                         , Approval_Status__c
                                         , Penalty_Category__c
                                         , Status
                                         , Additional_Details__c
                                         , POA_Relationship_with_Buyer__c
                                         , POA_Expiry_Date__c
                                         , POA_Issued_By__c
                                         , Is_CRF_Uploaded__c
                                         , Purpose_of_POA__c
                                         , Approved_Amount__c
                                         , Amount_to_be_waived__c
                                         , CaseNumber
                                         , Actual_Approving_Percent__c
                                         , Penalty_Amount__c
                                         , RecordType.Name
                                         , RecordType.DeveloperName
                                         , AccountId
                                         , Booking_Unit__c
                                         , Booking_Unit__r.Registration_Id__c
                                         , OwnerId
                                         , Booking_Unit__r.Area__c
                                         , Roles_from_Rule_Engine__c
                                         , Reason_for_waiver__c
                                         /*, Booking_Unit__r.Unit_Details__c
                                         , Booking_Unit__r.Inventory__r.Property_City__c
                                         , Buyer__r.First_Name__c
                                         , Booking_Unit__r.Inventory__r.Property_Status__c
                                         , Buyer__r.Last_Name__c
                                         , Booking_Unit__r.Inventory__r.Property_Country__c
                                         , Buyer__r.Nationality__c
                                         , Booking_Unit__r.Inventory__r.Property__r.DIFC__c
                                         , Buyer__r.Passport_Number__c
                                         , Buyer__r.Passport_Expiry_Date__c
                                         , Buyer__r.IPMS_Buyer_ID__c
                                         , Seller__r.Party_ID__c
                                         , Seller__r.FirstName
                                         , Seller__r.LastName
                                         , Seller__r.Nationality__pc*/
                                         , parentId
                                         , Parent.CaseNumber
                                      FROM Case
                                     WHERE Id IN :setCaseIds ] );
    }

    public static list<Case> getAllRelatedCases( Id bookingUnitId ) {
        return new list<Case>( [ SELECT Id
                                      , RecordType.DeveloperName
                                      , RecordType.Name
                                      , RecordTypeId
                                      , IsPOA__c
                                      , Percent_to_be_waived__c
                                      , POA_Name__c
                                      , Approval_Status__c
                                      , Penalty_Category__c
                                      , Status
                                      , Additional_Details__c
                                      , POA_Relationship_with_Buyer__c
                                      , POA_Expiry_Date__c
                                      , POA_Issued_By__c
                                      , Is_CRF_Uploaded__c
                                      , Purpose_of_POA__c
                                      , Approved_Amount__c
                                      , Amount_to_be_waived__c
                                      , CaseNumber
                                   FROM Case
                                  WHERE Booking_Unit__c = :bookingUnitId ] );
    }

    public static list<SR_Booking_Unit__c> getAllRelatedJunctions( Id bookingUnitId ) {
        return new list<SR_Booking_Unit__c>( [ SELECT Id
                                                    , Case__c
                                                    , Case__r.RecordType.Name
                                                    , Case__R.Status
                                                 FROM SR_Booking_Unit__c
                                                WHERE Booking_Unit__c = :bookingUnitId
                                                  AND Case__c != null  ] );
    }

   /* public static list<Case> getAllPenaltyWaiverCase(Id bookingUnitId){
        return new list<Case>([SELECT Id
                                    , Approved_Amount__c
                                    , RecordType.Name
                                    , RecordTypeId
                                    , Status
                                    FROM Case
                                    WHERE Booking_Unit__c =:bookingUnitId
                                    AND RecordTypeId =:getRecordTypeId( PENALTY_WAIVER_RECTYPE, CASE_SOBJ )
                                    AND Approval_Status__c ='Approved'
                                    AND Status ='Closed']);
    }

    public static list<SR_Booking_Unit__c> getAllPenaltyWaivercasesJunctions(Id bookingUnitId){
        return new list<SR_Booking_Unit__c>([SELECT Id,
                                                    Case__r.Approved_Amount__c
                                                    ,Case__c
                                                    FROM SR_Booking_Unit__c
                                                    WHERE Booking_Unit__c = :bookingUnitId
                                                    AND  Case__r.RecordTypeId = :getRecordTypeId( PENALTY_WAIVER_RECTYPE, CASE_SOBJ )
                                                    AND Case__r.Approval_Status__c= 'Approved'
                                                    AND Case__r.Status = 'Closed']);
    }

   public static List<Case> getAllpenaltyWaiverCaseFromAccount(Id Accountid){
        return new List<Case>([Select id
                                    , Approved_Amount__c
                                    FROM Case
                                    WHERE AccountId = :Accountid
                                    AND RecordTypeId =:getRecordTypeId( PENALTY_WAIVER_RECTYPE, CASE_SOBJ )
                                    AND Approval_Status__c ='Approved'
                                    AND Status ='Closed']);

    }*/

    public static list<Case> getAllPWCasesFromAccount( Id accountId ) {
        return new list<Case>( [ SELECT Id
                                      , Amount_Approved__c
                                      , Amount_to_be_waived__c
                                   FROM Case
                                  WHERE AccountId = :accountId
                                    AND RecordType.DeveloperName = 'Penalty_Waiver'
                                    AND Approval_Status__c = 'Approved'
                                    AND Status = 'Closed'
                                    AND IPMS_Updated__c = true
                                    AND Amount_Approved__c != null ] );
    }

    public static list<Buyer__c> getAllJointBuyer( Id bookingId ) {
        return new list<Buyer__c>( [ SELECT Id
                                          , Name
                                          , First_Name__c
                                          , Last_Name__c
                                          , Account__c
                                          , Buyer_ID__c
                                          , Account__r.Party_Id__c
                                       FROM Buyer__c
                                      WHERE Account__c != null
                                        AND Booking__c = :bookingId
                                        AND Primary_Buyer__c = false ] );
    }
    
    public static string fetchHelpDocURL( String docDeveloperName ){
        String strURL = null;
        list<Document> lstDoc = [SELECT Name
                                      , Id
                                      , DeveloperName 
                                   FROM Document  
                                  WHERE DeveloperName = :docDeveloperName limit 1 ];
        if( lstDoc != null && !lstDoc.isEmpty() ){
            strURL = '/servlet/servlet.FileDownload?file='+lstDoc[0].Id;
        }
        return strURL;
    }
}