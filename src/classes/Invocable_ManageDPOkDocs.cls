public class Invocable_ManageDPOkDocs {
    @InvocableMethod
    public static void UpdateBuyer(List<Id> stepId) {
        EvaluateCustomCode(stepId);
    }

    public static String EvaluateCustomCode(List<Id> stepIdList) {
        String retStr = 'Success';
        boolean isMandatoryDocsApproved = false;

        for (New_Step__c stp :[SELECT Id, Service_Request__c
                               FROM New_Step__c
                               WHERE Id IN :stepIdList
                               AND Step_Status__c = 'Docs OK'
                               AND Step_Type__c = 'Document Verification'])
        {
            isMandatoryDocsApproved = isallDocsApproved(stp.Service_Request__c);

            if (!isMandatoryDocsApproved) {
                return 'All mandatory documents status should be approved to proceed.';
            } else {
                SRUtility.callAsyncReceiptWebservice(stp.Service_Request__c);
            }
            break;
        }
        return retstr;
    }

    public static boolean isallDocsApproved(id SRID) {
        //Ch01 start
        /* for(NSIBPM__SR_Doc__c srDoc:[select id from NSIBPM__SR_Doc__c where NSIBPM__Is_Not_Required__c= false and id!=null and NSIBPM__Service_Request__c =: SRID and NSIBPM__Status__c != 'Approved' limit 1]){
          return false;
          }*/
        return true;
    }
    //CH01 End
}