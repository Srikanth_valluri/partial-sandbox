public without sharing class GetFileFromOffice365Cntrl {

    public String fileId{get;set;}
    public string url{get;set;}
    public string url1{get;set;}
    public string url2{get;set;}
    public String strrecId{get;set;}
    String sObjectType;
    Id recId;
    public GetFileFromOffice365Cntrl(){
        fetchFileFromOffice365();
    }
    public Pagereference fetchFileFromOffice365(){
        fileId =String.isNotBlank(ApexPages.currentPage().getParameters().get('id'))?ApexPages.currentPage().getParameters().get('id'):'';
        System.debug(fileId);
        strrecId = ApexPages.currentPage().getParameters().get('recId');
        if(String.isNotBlank(strrecId)){
            recId = (Id) strrecId;
            Schema.sObjectType entityType = recId.getSObjectType();
            sObjectType = String.valueOf(entityType);
            System.debug('-----recordType:'+sObjectType);   
        }
        else{
            sObjectType = '';
            
        }
        If(fileId != null && fileId != ''  ){
            system.debug( 'fileId : ' + fileId );
            switch on sObjectType {
                when 'Account' {
                     
                     Office365RestService.ResponseWrapper objWrap = Office365RestService.downloadFromOffice365ById(fileId ,'',recId,'','','');
                     if( objWrap != null){
                        if( objWrap.Error_log_lst.size() == 0){
                        url1 = String.isNotBlank(objWrap.downloadUrl)?objWrap.downloadUrl:'ERROR' ;
                        }else{
                            if(objWrap.Error_log_lst != null)
                            if( !Test.isRunningTest()){ insert objWrap.Error_log_lst; }
                        }
                    }

                    Office365RestService.ResponseWrapper objWrap2 =Office365RestService.downloadFromOffice365ByIdWhatsapp(fileId ,'',recId,'','','');           
                    system.debug('objWrap --'+ objWrap );
                    if( objWrap2 != null){
                        if( objWrap2.Error_log_lst.size() == 0){
                        url2 = String.isNotBlank(objWrap2.downloadUrl)?objWrap2.downloadUrl:'ERROR' ;
                        system.debug(url2 );
                        }else{
                            if(objWrap2.Error_log_lst != null)
                            if( !Test.isRunningTest()){ insert objWrap.Error_log_lst; }
                        }
                    }
                    
                    
                }
                when 'Booking_Unit__c' {
                    
                    Office365RestService.ResponseWrapper objWrap = Office365RestService.downloadFromOffice365ById(fileId ,'','',recId,'','');
                   
                    if( objWrap != null){
                        if( objWrap.Error_log_lst.size() == 0){
                        url1 = String.isNotBlank(objWrap.downloadUrl)?objWrap.downloadUrl:'ERROR' ;
                        }else{
                            if(objWrap.Error_log_lst != null)
                            if( !Test.isRunningTest()){ insert objWrap.Error_log_lst; }
                        }
                    }

                    
                    Office365RestService.ResponseWrapper objWrap2 =Office365RestService.downloadFromOffice365ByIdWhatsapp(fileId ,'','',recId,'','');           
                    system.debug('objWrap --'+ objWrap );
                    if( objWrap2 != null){
                        if( objWrap2.Error_log_lst.size() == 0){
                        url2 = String.isNotBlank(objWrap2.downloadUrl)?objWrap2.downloadUrl:'ERROR' ;
                        system.debug(url2 );
                        }else{
                            if(objWrap2.Error_log_lst != null)
                            if( !Test.isRunningTest()){ insert objWrap.Error_log_lst; }
                        }
                    }

                    
                    
                }
                when 'Calling_List__c' {
                    Office365RestService.ResponseWrapper objWrap = Office365RestService.downloadFromOffice365ById(fileId ,'','','',recId,'');
                    if( objWrap != null){
                        if( objWrap.Error_log_lst.size() == 0){
                        url1 = String.isNotBlank(objWrap.downloadUrl)?objWrap.downloadUrl:'ERROR' ;
                        }else{
                            if(objWrap.Error_log_lst != null)
                            if( !Test.isRunningTest()){ insert objWrap.Error_log_lst; }
                        }
                    }

                    Office365RestService.ResponseWrapper objWrap2 =Office365RestService.downloadFromOffice365ByIdWhatsapp(fileId ,'','','',recId,'');           
                    system.debug('objWrap --'+ objWrap );
                    if( objWrap2 != null){
                        if( objWrap2.Error_log_lst.size() == 0){
                        url2 = String.isNotBlank(objWrap2.downloadUrl)?objWrap2.downloadUrl:'ERROR' ;
                        system.debug(url2 );
                        }else{
                            if(objWrap2.Error_log_lst != null)
                            if( !Test.isRunningTest()){ insert objWrap.Error_log_lst; }
                        }
                    }
                    
                    
                }
                when 'Case' {
                    Office365RestService.ResponseWrapper objWrap = Office365RestService.downloadFromOffice365ById(fileId ,'','','','',recId);
                    if( objWrap != null){
                        if( objWrap.Error_log_lst.size() == 0){
                        url1 = String.isNotBlank(objWrap.downloadUrl)?objWrap.downloadUrl:'ERROR' ;
                        }else{
                            if(objWrap.Error_log_lst != null)
                            if( !Test.isRunningTest()){ insert objWrap.Error_log_lst; }
                        }
                    }

                    Office365RestService.ResponseWrapper objWrap2 = Office365RestService.downloadFromOffice365ByIdWhatsapp(fileId, '', '', '', '', '');           
                    system.debug('objWrap --'+ objWrap2 );
                    if( objWrap2 != null){
                        if( objWrap.Error_log_lst.size() == 0){
                        url2 = String.isNotBlank(objWrap2.downloadUrl)?objWrap2.downloadUrl:'ERROR' ;
                        system.debug(url1 );
                        }else{
                            if(objWrap2.Error_log_lst != null)
                            if( !Test.isRunningTest()){ insert objWrap.Error_log_lst; }
                        }
                    }
                    
                }
                when else {
                    Office365RestService.ResponseWrapper objWrap = Office365RestService.downloadFromOffice365ById(fileId ,'','','','','');
                    if( objWrap != null){
                        if( objWrap.Error_log_lst.size() == 0){
                        url1 = String.isNotBlank(objWrap.downloadUrl)?objWrap.downloadUrl:'ERROR' ;
                        }else{
                            if(objWrap.Error_log_lst != null)
                            if( !Test.isRunningTest()){ insert objWrap.Error_log_lst; }
                        }
                    }

                    Office365RestService.ResponseWrapper objWrap2 = Office365RestService.downloadFromOffice365ByIdWhatsapp(fileId, '', '', '', '', '');           
                    system.debug('objWrap --'+ objWrap );
                    if( objWrap2 != null){
                        if( objWrap2.Error_log_lst.size() == 0){
                        url2 = String.isNotBlank(objWrap2.downloadUrl)?objWrap2.downloadUrl:'ERROR' ;
                        system.debug(url2 );
                        }else{
                            if(objWrap2.Error_log_lst != null)
                            if( !Test.isRunningTest()){ insert objWrap.Error_log_lst; }
                        }
                    }

                }
            }system.debug(url2);
            return null;
        }else{
            url1 = 'ERROR';
            url2 = 'ERROR';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please Provide file Id along with url.'));
            return null;
        }
        
       
        
    }
    public void doAction(){}
}