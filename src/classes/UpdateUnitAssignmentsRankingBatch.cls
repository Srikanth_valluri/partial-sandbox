/**************************************************************************************************
* Name               : UpdateUnitAssignmentsRankingBatch                                       *
* Description        : This class will update the Ranking of a User on Unit Assignments Object 
                        when a rank is changed.                                                *
* Created Date       : 04/06/2020                                                                 *
* Created By         : QBurst                                                                        *
* Test class         : UpdateUnitAssignmentsRankingBatch_Test                                     *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         QBurst          04/06/2020     Initial Draft.                                      *
**************************************************************************************************/


global with sharing class UpdateUnitAssignmentsRankingBatch implements Database.Batchable <sObject>,Database.Stateful{
    
    String profilesId = [SELECT Id, Name FROM Profile WHERE Name = 'Property Consultant' LIMIT 1].Id;
  //  global Integer totalUnitAssignmentsCount = [SELECT count() FROM User_Assignment__c];
   // global Integer totalUnitAssignmentsCount = [SELECT count() FROM Inquiry_User_Assignment_Rules__c];
    global Decimal highestUnitAssignmentsValue;                                                    
    global Integer rankedUsersCount;
    global Integer powerLineCount;
    global Integer failedPowerLineCount;
    global  DateTime startTime = System.now();
    Time toTheTime = startTime.time();
    global Boolean isEmpty = true;
    global List<Id> InvalidUserList = new List<Id>(); //To contain Users who are not there in Custom Settings
    global Map<Id, User> InvalidUserMap;// = new Map<Id, User>(); //To contain Users who are not there in Custom Settings
    global Database.QueryLocator start(Database.BatchableContext BC) {
        DateTime startTime = System.now();
        if(startTime.minute() >= 30){
            startTime = Datetime.newInstance(startTime.year(), startTime.month(), startTime.day(), startTime.hour(), 30, 0);
        } else{
            startTime = Datetime.newInstance(startTime.year(), startTime.month(), startTime.day(), startTime.hour(), 0, 0);
        }
        DateTime endTime = startTime.addMinutes(30);
        Time toTime = startTime.time();
        Date today = Date.today();
       // String toStartTime = String.valueOf(startTime.hour() + ':' + startTime.minute());
       // String toEndTime = String.valueOf(endTime.time());
       // system.debug('toStartTime: ' + toStartTime);
        system.debug('toTime: ' + toTime);
        System.debug('Start is invoked');
           
        String query =  'SELECT Id,Name,Rank__c, Shift_Day__c, Shift_Time__c, User_Assigned__c, User_Assigned__r.Name';
        query +=' FROM User_Assignment__c WHERE Shift_Day__c =: today ' +  + ' AND Shift_Time__c = ' + toTime;
        //query +=  ' AND User_Assigned__r.ProfileId =  \'' +  String.escapeSingleQuotes(profilesId )  + '\'' ;
        query += ' ORDER BY Rank__c ASC';
        system.debug('query: ' + query);
        return Database.getQueryLocator(query);
    }


    //Execute Method to process all the Inquiry records    
    global void execute(Database.BatchableContext BC, List <SObject> scope) {
        system.debug('scope size: ' + scope.size());
        System.debug('Execute is invoked');
        rankedUsersCount = 0;
        powerLineCount = 0;
        failedPowerLineCount = 0;
        highestUnitAssignmentsValue = 0;
        List<Inquiry_User_Assignment_Rules__c> highestUnitAssignmentsValueList = [SELECT Net_Direct_Sales_Rank__c FROM Inquiry_User_Assignment_Rules__c 
                                                     WHERE User_Profile__c = 'Property Consultant'  
                                                     ORDER BY Net_Direct_Sales_Rank__c DESC NULLS LAST LIMIT 1];
        highestUnitAssignmentsValue =  highestUnitAssignmentsValueList[0].Net_Direct_Sales_Rank__c; 
        try{
        
          List<User_Assignment__c> unitAssignmentsList = (List<sObject>)scope;
          system.debug('unitAssignmentsList '+unitAssignmentsList );
          
          List<Id> usAsgnList = new List<Id>();
          List<Id> assignedUsersList = new List<Id>();
          List<decimal> rankList = new List<decimal>();
          
          for(User_Assignment__c usAsign: unitAssignmentsList){
              usAsgnList.add(usAsign.Id);
              assignedUsersList.add(usAsign.User_Assigned__c); 
              rankList.add(usAsign.rank__c);             
          }
         
          
          List<Decimal> encounteredRanks = new List<Decimal>();
          List<Decimal> encountered = new List<Decimal>();
          //List<User_Assignment__c> totalUnitAssignmentsListDuplicate = new List<User_Assignment__c>();
          List<Inquiry_User_Assignment_Rules__c> totalUnitAssignmentsListDuplicate = new List<Inquiry_User_Assignment_Rules__c>();
          List<User_Assignment__c> unitAssignmentsListUpdate = new List<User_Assignment__c>();
          List<Id> customSettingsListUpdate = new List<Id>();
          // Retrieving records from Custom Settings based on the User Assigned
          List<Inquiry_User_Assignment_Rules__c>  customSettingsList = [SELECT Id, Net_Direct_Sales_Rank__c FROM Inquiry_User_Assignment_Rules__c  WHERE SetupOwnerId IN :assignedUsersList];          
          system.debug('customSettingsList '+customSettingsList );
         
          //List<Id> InvalidUserList = new List<Id>(); //To contain Users who are not there in Custom Settings   
          List<Inquiry_User_Assignment_Rules__c> customSettingsListToUpdate = new List<Inquiry_User_Assignment_Rules__c>();
             
          if(!unitAssignmentsList.isEmpty()){          
             
              system.debug('not empty');
              isEmpty = false;
              rankedUsersCount = unitAssignmentsList.size();
              
             //Updating Custom Setting Powerline for the Users Assigned 
             for(User_Assignment__c userAssignment : unitAssignmentsList){   
                 Inquiry_User_Assignment_Rules__c userAssignmentRules = Inquiry_User_Assignment_Rules__c.getInstance(userAssignment.User_Assigned__c);
                 
                 if(userAssignmentRules.Id != Null){
                     userAssignmentRules.Net_Direct_Sales_Rank__c =   userAssignment.Rank__c;
                     customSettingsListToUpdate.add(userAssignmentRules); 
                     encountered.add(userAssignmentRules.Net_Direct_Sales_Rank__c);
                     customSettingsListUpdate.add(userAssignmentRules.setupOwnerId);
                     
                }
                else{
                    InvalidUserList.add(userAssignmentRules.setupOwnerId);
                   /* for(Integer i; i<InvalidUserList.size();i++){
                        for(User u : [select Id,Name from User where Id =: InvalidUserList[i]]){
                            InvalidUserMap.put(u.Id,u);
                        }
                    }*/
                    InvalidUserMap = new Map<Id, User>([select Id,Name from User where Id In : InvalidUserList]);
                    system.debug('Invalid Users>>'+InvalidUserList);
                
                }
                 
             }
           
             
             update customSettingsListToUpdate;
             powerLineCount = customSettingsListToUpdate.size();           
             failedPowerLineCount = InvalidUserList.size();                                                               
                                                               
             List<Inquiry_User_Assignment_Rules__c> totalUnitAssignmentsList = [SELECT Id, Net_Direct_Sales_Rank__c,SetupOwnerId FROM Inquiry_User_Assignment_Rules__c 
                                                               WHERE User_Profile__c = 'Property Consultant' 
                                                               AND SetupOwnerId NOT IN: customSettingsListUpdate 
                                                               AND Id NOT IN: customSettingsListToUpdate  
                                                               AND Net_Direct_Sales_Rank__c != NULL
                                                               AND Net_Direct_Sales_Rank__c IN: rankList
                                                               ORDER BY Net_Direct_Sales_Rank__c  ASC 
                                                               LIMIT 5000];                                                                                                 
                                                               
                                                               
              system.debug('totalUnitAssignmentsList >>'+totalUnitAssignmentsList );  
              system.debug('totalUnitAssignmentsList size >>'+totalUnitAssignmentsList.size());                                               
                                                               
              //checking for duplicates
              Integer j = 1;
              for(Inquiry_User_Assignment_Rules__c userAssignmentRecords : totalUnitAssignmentsList){
                  if(encounteredRanks.contains(userAssignmentRecords.Net_Direct_Sales_Rank__c)){
                      system.debug('entered>>');
                      userAssignmentRecords.Net_Direct_Sales_Rank__c =  j + highestUnitAssignmentsValue; //totalUnitAssignmentsCount;  
                      totalUnitAssignmentsListDuplicate.add(userAssignmentRecords);
                      j++;
                      system.debug('Going to Update the Users>>'+userAssignmentRecords.SetupOwnerId);
                  }
                  else{
                      encounteredRanks.add(userAssignmentRecords.Net_Direct_Sales_Rank__c);
                      system.debug('encounteredRanks size>>'+encounteredRanks.size());
                      system.debug('encounteredRanks>>'+encounteredRanks);
                      userAssignmentRecords.Net_Direct_Sales_Rank__c =  j + highestUnitAssignmentsValue; //totalUnitAssignmentsCount;  
                      totalUnitAssignmentsListDuplicate.add(userAssignmentRecords);
                      j++;
                      system.debug('Going to Update the Users>>'+userAssignmentRecords.SetupOwnerId);
                  }
              }
              system.debug('Updated Users>>'+totalUnitAssignmentsListDuplicate);
              update totalUnitAssignmentsListDuplicate;
              
  
          } else{
              isEmpty = true;    
          }
          
        }
        
        
        catch(Exception e){
            system.debug(e);
        
        }
        

    }

    //Finish Method    
    global void finish(Database.BatchableContext BC) {
    
        
   
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       //String[] toAddresses = new String[] {'jithesh@qburst.com'};       
       String[] toAddresses = new String[] {label.UpdateUnitAssignmentsRankingBatch_Notification_Email_Id};
       String subject ='';
       subject = 'Power Update at '+toTheTime; 
       mail.setToAddresses(toAddresses);
       mail.setSubject(subject);
       String body ='';
       
       
       if(!isEmpty){
           if(powerLineCount > 0){
               body = 'Powerline update found for '+ powerLineCount +' user(s) and it is updated Successfully at '+toTheTime +'\n'+'\n';
           }
           else{
               body = 'Powerline update was found for no users at '+toTheTime +'\n'+'\n';
           }
           if(failedPowerLineCount > 0){
               body += 'Note : Powerline update failed for the below Users,\n';//+InvalidUserList;
                   for (ID idKey : InvalidUserMap.keyset()) {
                        String userName = InvalidUserMap.get(idKey).Name;
                        body += userName +',';
                        
                   }
                body = body.removeEnd(','); 
               
           }
           mail.setPlainTextBody(body);       
       } else{
           body = 'Powerline update was found for no users at '+toTheTime ;           
           mail.setPlainTextBody(body);
           //mail.setPlainTextBody('Powerline update was not found for no users at 10:00 AM ');   
       }
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}