/***********************************************************************************
*   Description :  Test class developed for FMServiceChargesCalloutBatchSched.     *
*----------------------------------------------------------------------------------*
*   Revision History:                                                              *
*   Version     Author              Date            Description                    *
*   1.0         Aishwarya Todkar    06/03/2019      Initial Draft                  *
************************************************************************************/
@isTest
public Class FMServiceChargesCalloutBatchSchedTest {
    
    @isTest
    public static void testSchedule() {
        FMServiceChargesCalloutBatchSched objSched = new FMServiceChargesCalloutBatchSched();
        
        String sch = '0 0 23 * * ?'; 
        Test.StartTest();
        system.schedule('Test FM Outstanding Amount', sch, objSched );
        Test.stopTest();
    }
}