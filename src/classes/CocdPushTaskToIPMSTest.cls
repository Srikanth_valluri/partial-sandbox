/**************************************************************************************
* Description - Test class developed for 'CaseTriggerController'
*
* Version            Date            Author                    Description
* 1.0               21/11/17         Naresh(Accely)            Initial Draft
***************************************************************************************/
@isTest
public class CocdPushTaskToIPMSTest{
    //Initial Test Data
	public static Account objAccount;
	public static Case objCase;
	public static Task objTask;
	
	@testsetup
	public static void TestData(){
	    
		Id POPcase=Schema.SObjectType.case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
		
		Account objAccount=TestDataFactory_CRM.createPersonAccount();
		objAccount.PersonEmail='pratiksha.n@accely.com';
		objAccount.FirstName='pratiksha';
		Insert objAccount;
		System.assert(objAccount!=null);
		
		Case objCase=TestDataFactory_CRM.createCase(objAccount.ID,POPcase);
		Insert objCase;
		System.assert(objCase!=null);
		
		Task objTask=new  Task();
		objTask.WhatID=objCase.ID;
		objTask.OwnerId=Userinfo.getUserID();
		objTask.ActivityDate=Date.Today();
		objTask.Assigned_User__c='Finance';
		objTask.Process_Name__c='POP';
		objTask.Priority='High';
		objTask.Status='Not Started';
		objTask.Subject='Verify Proof of Payment Details in IPMS';
		Insert objTask;
		System.assert(objTask!=null);
	}
	
    // Test Method: getTasks
	public static testmethod void Test_getTasks(){
	
		set<string> settaskids=new  set<string>();
		set<string> setcaseids=new  set<string>();
		
		test.StartTest();
		Task tsk=getTask();
		System.assert(tsk!=null);
		
		Case createCase=getCase();
		System.assert(createCase!=null);
		
		CocdPushTaskToIPMS.getCaseMap(setcaseids);
		System.Assert(CocdPushTaskToIPMS.getCaseMap(setcaseids)!=null);
		CocdPushTaskToIPMS.getTasks(settaskids);
		System.Assert(CocdPushTaskToIPMS.getTasks(settaskids)!=null);
		test.StopTest();
	}
	
	
	// Inner Class , 
	public static testmethod void InnerTest(){
	    
	    CocdPushTaskToIPMS.innerClass obj = new  CocdPushTaskToIPMS.innerClass();
	    obj.message = 'Test';
	    obj.status = 'status';
	    
	    Test.startTest();
	    Account Acc = getAccount();
	    Case createCase = getCase();
	    CocdPushTaskToIPMS.createErrorLogRecord(Acc.Id,createCase.Id);
	    
	    
	    Error_Log__c Elog = new Error_Log__c();
	    Elog.Account__c = Acc.Id;
	    Elog.Case__c = createCase.Id;
	    insert Elog;
	    System.assert(Elog!=null);
	    
	    list<Error_Log__c> listobjerr = new list<Error_Log__c>();
	    listobjerr.add(Elog);
	    CocdPushTaskToIPMS.insertErrorLog(listobjerr);     
	    Test.stopTest();  
	    
	}
	
	// Test Method: createCocdFeeTaskInIPMS
	public static testmethod void Test_createCocdFeeTaskInIPMS(){
		List<Task> tasklist = new List<Task>();
		Set<string> cocdfeetaskidset = new Set<string>();
		Set<string> cocdcaseidset = new Set<string>();
		
		Task tsk=getTask();
		tsk.Subject = 'Verify COCD Fee' ;
		update tsk;
		tasklist.add(tsk);
		cocdfeetaskidset.add(tsk.Id);
		
		Case createCase=getCase();
		cocdcaseidset.add(createCase.Id);  
		
		test.StartTest();
		SOAPCalloutServiceMock.returnToMe = new Map<String,TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();
		TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response_x =  new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
	    response_x.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"SR Header Data Created for SR # :2-005057","PARAM_ID":"2-005057"},{"PROC_STATUS":"S","PROC_MESSAGE":"Created SR Task Data for SR # :2-005057 Task :Verify POA Documents","PARAM_ID":"2-005057"},{"PROC_STATUS":"E","PROC_MESSAGE":"Error: Param Id Not Passed...","PARAM_ID":"NULL"}],"message":"[WARNING] Check the PROC_MESSAGE attribute for Actual Error Message(s), Error Message Count = 1","status":"S"}';
		   
		SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
		
		Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
		
		CocdPushTaskToIPMS.createCocdFeeTaskInIPMS(cocdfeetaskidset, cocdcaseidset);
		CocdPushTaskToIPMS.PushTask(tasklist);
		test.StopTest();
	}
	
    
    // Test Method: createPoaTaskInIPMS
	public static testmethod void Test_createPoaTaskInIPMS(){
	    List<Task> tasklist = new List<Task>();
		Set<string> poataskidset = new Set<string>();
		Set<string> poacaseidset = new Set<string>();
		
		Task tsk=getTask();
		tsk.Subject = 'Verify POA Documents' ;
		update tsk;
		tasklist.add(tsk);
		poataskidset.add(tsk.Id);
		
		Case createCase=getCase();
		createCase.Purpose_of_POA__c = 'Test';
		createCase.POA_Issued_By__c = 'Test';
		createCase.POA_Expiry_Date__c = System.today() +56 ;
		update createCase ;
		          
		poacaseidset.add(createCase.Id);   
		  
		test.StartTest(); 
		// CocdPushTaskToIPMS.innerClass obj = new  CocdPushTaskToIPMS.innerClass();
	   	SOAPCalloutServiceMock.returnToMe = new Map<String,TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();
		TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response_x =  new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
	 	
	 	response_x.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"SR Header Data Created for SR # :2-005057","PARAM_ID":"2-005057"},{"PROC_STATUS":"S","PROC_MESSAGE":"Created SR Task Data for SR # :2-005057 Task :Verify POA Documents","PARAM_ID":"2-005057"},{"PROC_STATUS":"E","PROC_MESSAGE":"Error: Param Id Not Passed...","PARAM_ID":"NULL"}],"message":"[WARNING] Check the PROC_MESSAGE attribute for Actual Error Message(s), Error Message Count = 1","status":"S"}';
	 	SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);         
		
		Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());    
		CocdPushTaskToIPMS.createPoaTaskInIPMS(poataskidset, poacaseidset);  
		CocdPushTaskToIPMS.PushTask(tasklist);
		test.StopTest();
	}

	// Test Method: createCourtOrderTaskInIPMS
	public static testmethod void createCourtOrderTaskInIPMS(){
	    List<Task> tasklist = new List<Task>();
		Set<string> poataskidset = new Set<string>();
		Set<string> poacaseidset = new Set<string>();
		
		Task tsk=getTask();
		tsk.Subject = 'Verify POA Documents' ;
		update tsk;
		tasklist.add(tsk);
		poataskidset.add(tsk.Id);
		
		Case createCase=getCase();
		createCase.Purpose_of_POA__c = 'Test';
		createCase.POA_Issued_By__c = 'Test';
		createCase.POA_Expiry_Date__c = System.today() +56 ;
		update createCase ;
		          
		poacaseidset.add(createCase.Id);   
		  
		test.StartTest(); 
		// CocdPushTaskToIPMS.innerClass obj = new  CocdPushTaskToIPMS.innerClass();
	   	SOAPCalloutServiceMock.returnToMe = new Map<String,TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();
		TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response_x =  new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
	 	
	 	response_x.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"SR Header Data Created for SR # :2-005057","PARAM_ID":"2-005057"},{"PROC_STATUS":"S","PROC_MESSAGE":"Created SR Task Data for SR # :2-005057 Task :Verify POA Documents","PARAM_ID":"2-005057"},{"PROC_STATUS":"E","PROC_MESSAGE":"Error: Param Id Not Passed...","PARAM_ID":"NULL"}],"message":"[WARNING] Check the PROC_MESSAGE attribute for Actual Error Message(s), Error Message Count = 1","status":"S"}';
	 	SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);         
		
		Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());    
		CocdPushTaskToIPMS.createCourtOrderTaskInIPMS(poataskidset, poacaseidset);  
		CocdPushTaskToIPMS.PushTask(tasklist);
		test.StopTest();
	}
	
	
    // get Task
	public static Task getTask(){
		return [SELECT WhoId , WhatId, Type, Status, OwnerId, Id, Subject, CreatedDate, Description, 
		        Assigned_User__c, ActivityDate, Owner.Name  FROM Task WHERE Process_Name__c='POP'];
	}
    
	// get Account
	public static Account getAccount(){
		return [SELECT id,PersonEmail,FirstName FROM Account WHERE PersonEmail='pratiksha.n@accely.com'];  
	}
    
	// get Case
	public static Case getCase(){
		return [SELECT Id,Status,Account.Party_ID__c,POA_Name__c,POA_Expiry_Date__c,POA_Relation_With_Owner__c,
		         CaseNumber,POA_Issued_By__c,Purpose_of_POA__c,OQOOD_Fee_Applicable__c,OQOOD_Fee_Payment_Mode__c,
		         OQOOD_Fee__c,POA_File_URL__c FROM Case WHERE Credit_Note_Amount__c=5000];
	}
}