global class Damac_AnnouncementStatus implements Database.Batchable<sObject>{

    Set <String> countries;
   global Damac_AnnouncementStatus () {
        countries = new Set <String> ();
        for (Announcement__c acc: [SELECT Countries_Include__c 
                                           FROM Announcement__c WHERE Start_Date__c <= today AND End_Date__c >= today 
                                            AND Active__c = true And Createddate = Today Order By LastModifiedDate Desc ]) 
        {
            List <String> countriesInclude = new List <String> ();
            if (acc.Countries_Include__c != NULL) {
                if (acc.Countries_Include__c.contains (';'))
                    countriesInclude = acc.Countries_Include__c.split (';');
                else
                    countriesInclude.add (acc.Countries_Include__c);
            }
            countries.addAll (countriesInclude);
        }
   }
   
   global Database.QueryLocator start(Database.BatchableContext BC){
       String unread = 'Unread';
       
       String query = 'SELECT ContactId, Account.Country_of_Incorporation_New__c FROM User '
                       +' WHERE Account.Country_of_Incorporation_New__c IN: countries '
                       +' and isactive = true AND ContactId != NULL AND contact.Announcement_Status__c !=: unread';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<User> scope){
        
        List <Contact> contactList = new List <Contact> ();
        for (User u :Scope) {
            Contact con = new Contact ();
            con.Announcement_Status__c = 'Unread';
            con.id = u.contactId;
            contactList.add (con);
        }
        if (contactList.size () > 0) {
            database.update (contactList, false);
        }
    }
    
    global void finish(Database.BatchableContext BC){
    }
}