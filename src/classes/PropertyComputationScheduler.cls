/****************************************************************************************************
* Name          : PropertyComputationScheduler                                                      *
* Description   : Class to schedule the batch with custom setting record                            *
* Created Date  : 09-08-2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER     AUTHOR            DATE            COMMENTS                                                *
* 1.0     Twinkle Panjabi   12-08-2018      Initial Draft.                                          *
****************************************************************************************************/
public class PropertyComputationScheduler implements Schedulable {

    public void execute(SchedulableContext SC) {
      List<Property_Computation__c>  proCompCustomSet = new List<Property_Computation__c>();
        proCompCustomSet = [ SELECT
                                Number_of_Booking_Units__c,
                                Number_of_Clicks__c,
                                Number_of_Days__c,
                                Number_of_Opens__c,
                                Number_of_Records_for_Project_Details__c,
                                Number_of_Records_for_Sales_Offer__c
                            FROM
                                Property_Computation__c
                            WHERE isActive__c = true
                            LIMIT 1
                            ];
       database.executeBatch(new PropertyComputationBatch(proCompCustomSet[0]),100);  
   }

}