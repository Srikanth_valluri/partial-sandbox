@isTest

/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  12/02/2018  Initial Draft
*/

public class CRE_Plot_Handover_Controller_Test{
    static Account  objAcc;
    static Case objCase;
    static Booking__c objBooking;
    static Booking_Unit__c objBookingUnit;
    static NSIBPM__Service_Request__c objSR;
    static Booking_Unit_Active_Status__c objBUActive;
    static Option__c objOptions;
    static Payment_Plan__c objPaymentPlan;
    static SR_Attachments__c objSRatt ;
    static SR_Booking_Unit__c objSRB;
    
        static testMethod void initialMethod(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'Test Unit';
        objBookingUnit.Unit_Type__c  = 'PLOT';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        objBUActive = new Booking_Unit_Active_Status__c ();
        objBUActive.name = 'Test Active Status';
        objBUActive.Status_Value__c = 'Agreement executed by DAMAC'; 
        insert objBUActive; 
        
        objOptions = new Option__c();
        objOptions.Booking_Unit__c = objBookingUnit.id;
        objOptions.PromotionName__c = 'PromotionName__c';
        objOptions.SchemeName__c = 'SchemeName__c';
        objOptions.OptionsName__c = 'OptionsName__c';
        objOptions.CampaignName__c = 'CampaignName__c';
        insert objOptions;
        
        objPaymentPlan = new Payment_Plan__c();
        objPaymentPlan.Booking_Unit__c = objBookingUnit.id;
        objPaymentPlan.Status__C = '';
        insert objPaymentPlan;
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Plot Handover').getRecordTypeId();
        objCase = new Case();
        objCase.AccountID = objAcc.id ;
        objCase.RecordTypeID = devRecordTypeId;
        objCase.Total_Token_Amount__c = 1000;
        objCase.Excess_Amount__c = 5000;
        objCase.POA_Expiry_Date__c = System.today();
        objCase.IsPOA__c = false;
        insert objCase;
        
        objSRatt = new SR_Attachments__c ();
        objSRatt.name = 'Test NOC';
        objSRatt.Case__c = objCase.Id;         
        objSRatt.type__c = 'NOC' ;
        //insert objSRatt;
        
        objSRB = new SR_Booking_Unit__c();
        objSRB.Case__c = objCase.id;
        objSRB.Booking_Unit__c = objBookingUnit.id;   
        insert objSRB;
    }
    
     static testMethod void Test1(){
        test.StartTest();
        initialMethod();
        
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id);
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id);
        ApexPages.currentPage().getParameters().put('SRType', 'PlotHandover');  
        
        CRE_Plot_Handover_Controller objClass = new  CRE_Plot_Handover_Controller();
        objClass.strSelectedAccount = objAcc.id;
        objClass.getName();
        objClass.init();
                        
        objCase.POA_Expiry_Date__c = SYstem.today();
        objCase.Task_for_CDC_Completed__c = true;
        objCase.Create_Task_for_Obtain_Building_Design__c = true;
        objCase.Create_Task_for_CRE_to_Generate_NOC__c = true;
        objCase.Possession_Date__c = SYstem.today();
        objCase.Construction_Commencement_Date__c = SYstem.today();
        
        update objCase ;
        
        objClass.init();     
        
        test.StopTest();
     
     }  
     
      static testMethod void Test2(){       
        initialMethod(); 
      
        test.StartTest();
        CRE_Plot_Handover_Controller objClass = new  CRE_Plot_Handover_Controller();
        objClass.bookingUnitDetails();  
        
        objClass.strCaseID = objCase.id;
        objClass.strSelectedBookingUnit = objBookingUnit.id; 
        objClass.strSelectedAccount = objAcc.id;
        
        objClass.init();  
        
        //Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock());
        //objClass.bookingUnitDetails();   
        
        objClass.blnIsLitigationFlag = false;
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(1)); // Litigation Flag and strPaidPercent < 20
        objClass.bookingUnitDetails(); 
              
        test.StopTest();
        
      }
      
     static testMethod void Test3(){       
        initialMethod(); 
      
        test.StartTest();
        CRE_Plot_Handover_Controller objClass = new  CRE_Plot_Handover_Controller();
        objClass.bookingUnitDetails();  
        
        objClass.strCaseID = objCase.id;
        objClass.strSelectedBookingUnit = objBookingUnit.id; 
        objClass.strSelectedAccount = objAcc.id;
        
        objClass.init();  
        
       
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(1)); // Litigation Flag and strPaidPercent < 50 and strPDCCoverage = null
        objClass.blnShowExtraDocSection = true;
        objClass.bookingUnitDetails(); 
     
        test.StopTest();
        
      }
      
     static testMethod void Test4(){       
        initialMethod(); 
      
        test.StartTest();
        CRE_Plot_Handover_Controller objClass = new  CRE_Plot_Handover_Controller();
        objClass.bookingUnitDetails();  
        
        objClass.strCaseID = objCase.id;
        objClass.strSelectedBookingUnit = objBookingUnit.id; 
        objClass.strSelectedAccount = objAcc.id;
        
        objClass.init();  
        
       
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(3)); // Litigation Flag and strPaidPercent < 50 and strPDCCoverage < 50
        objClass.blnShowExtraDocSection = true;
        objClass.bookingUnitDetails(); 
     
        test.StopTest();
        
      }
      
     static testMethod void Test5(){       
        initialMethod(); 
      
        test.StartTest();
        CRE_Plot_Handover_Controller objClass = new  CRE_Plot_Handover_Controller();
        objClass.bookingUnitDetails();  
        
        objClass.strCaseID = objCase.id;
        objClass.strSelectedBookingUnit = objBookingUnit.id; 
        objClass.strSelectedAccount = objAcc.id;
        
        objClass.init();  
        
       
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(4));  // getDepositAmountInformation - community charges null
        objClass.blnShowExtraDocSection = true;
        objClass.bookingUnitDetails(); 
        objClass.getDepositAmountInformation();
     
        test.StopTest();
        
      }            
      
      static testMethod void Test7(){       
        initialMethod(); 
      
        test.StartTest();
        CRE_Plot_Handover_Controller objClass = new  CRE_Plot_Handover_Controller();
        objClass.bookingUnitDetails();  
        
        objClass.strCaseID = objCase.id;
        objClass.strSelectedBookingUnit = objBookingUnit.id; 
        objClass.strSelectedAccount = objAcc.id;
        
        objClass.init();  
        
     
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(2)); // strPaidPercent = null
        objClass.bookingUnitDetails();                         
        
        test.StopTest();
        
      }
      
      static testMethod void Test8(){          
      
           test.StartTest();
            
           initialMethod(); 
        
           objSRatt = new SR_Attachments__c ();
           objSRatt.name = 'Test Power of Attorney';
           objSRatt.Case__c = objCase.Id;         
           objSRatt.type__c = 'Power of Attorney' ;
           insert objSRatt;
           
           PageReference pageRef = Page.DummyTitleDeedCREPortal;
           Test.setCurrentPage(pageRef); 
           ApexPages.currentPage().getParameters().put('CaseID', objCase.id);
            ApexPages.currentPage().getParameters().put('AccountID', objAcc.id);
            ApexPages.currentPage().getParameters().put('SRType', 'PlotHandover');  
        
           CRE_Plot_Handover_Controller objClass = new  CRE_Plot_Handover_Controller();
           objClass.strSelectedAccount = objAcc.id;
           objClass.strCaseID = objCase.id;
           objClass.init();
           
           objClass.deletePowerAttornyDoc();
           
           objCase.IsPOA__c = true;
           update objCase;
           
           objClass.init();
           
           objClass.deletePowerAttornyDoc();
           
           test.StopTest();
      }
      
    static testMethod void Test9(){          
    
        test.StartTest();
        
        initialMethod();
        
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id);
            ApexPages.currentPage().getParameters().put('SRType', 'PlotHandover'); 
        
        CRE_Plot_Handover_Controller objClass = new  CRE_Plot_Handover_Controller();
        objClass.strSelectedAccount = objAcc.id;
        objClass.strCaseID = objCase.id;
        objClass.init();
        
        list<SR_Attachments__c> lstSRatt = new list<SR_Attachments__c>();
        SR_Attachments__c objSRatt2 = new SR_Attachments__c ();
        objSRatt2.name = 'Test Power of Attorney';
        objSRatt2.Case__c = objCase.Id;         
        objSRatt2.type__c = 'Power of Attorney' ;
        lstSRatt.add(objSRatt2);   
        
        SR_Attachments__c objSRatt3 = new SR_Attachments__c ();
        objSRatt3.name = 'Test NOC';
        objSRatt3.Case__c = objCase.Id;         
        objSRatt3.type__c = 'Construction Commencement NOC' ;        
        lstSRatt.add(objSRatt3);  
        
        SR_Attachments__c objSRatt4 = new SR_Attachments__c ();
        objSRatt4.name = 'Test CRF';
        objSRatt4.Case__c = objCase.Id;         
        objSRatt4.type__c = 'CRF Form' ;
        lstSRatt.add(objSRatt4); 
        
        SR_Attachments__c objSRatt5 = new SR_Attachments__c ();
        objSRatt5.name = 'Test CRF';
        objSRatt5.Case__c = objCase.Id;         
        objSRatt5.type__c = 'Design Change NOC' ;
        lstSRatt.add(objSRatt5);
        
        SR_Attachments__c objSRatt6 = new SR_Attachments__c ();
        objSRatt6.name = 'Test CRF';
        objSRatt6.Case__c = objCase.Id;         
        objSRatt6.type__c = 'Undertaking Document' ;
        lstSRatt.add(objSRatt6);
        
        SR_Attachments__c objSRatt7 = new SR_Attachments__c ();
        objSRatt7.name = 'Test CRF';
        objSRatt7.Case__c = objCase.Id;         
        objSRatt7.type__c = 'Building Permit' ;
        lstSRatt.add(objSRatt7);
        
        SR_Attachments__c objSRatt8 = new SR_Attachments__c ();
        objSRatt8.name = 'Test CRF';
        objSRatt8.Case__c = objCase.Id;         
        objSRatt8.type__c = 'Insurance Document' ;
        lstSRatt.add(objSRatt8);
        
        SR_Attachments__c objSRatt9 = new SR_Attachments__c ();
        objSRatt9.name = 'Test CRF';
        objSRatt9.Case__c = objCase.Id;         
        objSRatt9.type__c = 'Other Document' ;
        lstSRatt.add(objSRatt9);
        
        SR_Attachments__c objSRatt10 = new SR_Attachments__c ();
        objSRatt10.name = 'Test CRF';
        objSRatt10.Case__c = objCase.Id;         
        objSRatt10.type__c = 'Other Document' ;
        lstSRatt.add(objSRatt10);
        
        SR_Attachments__c objSRatt11 = new SR_Attachments__c ();
        objSRatt11.name = 'Test CRF';
        objSRatt11.Case__c = objCase.Id;         
        objSRatt11.type__c = 'Building Design Plan' ;
        lstSRatt.add(objSRatt11);
         
        insert lstSRatt;
        
        objClass.RemoveSelected = lstSRatt[0].id;
        objClass.removeAttachment();
        
        objClass.RemoveSelected = lstSRatt[1].id;
        objClass.removeAttachment();
        
        objClass.RemoveSelected = lstSRatt[2].id;
        objClass.removeAttachment();
        
        objClass.RemoveSelected = lstSRatt[3].id;
        objClass.removeAttachment();
        
        objClass.RemoveSelected = lstSRatt[4].id;
        objClass.removeAttachment();
        
        objClass.RemoveSelected = lstSRatt[5].id;
        objClass.removeAttachment();
        
         objClass.RemoveSelected = lstSRatt[6].id;
        objClass.removeAttachment();
        
        objClass.RemoveSelected = lstSRatt[7].id;
        objClass.removeAttachment();
        
        objClass.RemoveSelected = lstSRatt[8].id;
        objClass.removeAttachment();
        
        objClass.RemoveSelected = lstSRatt[9].id;
        objClass.removeAttachment();
        
        test.StopTest();
    }
    
    // generateCRF Callout
    static testMethod void Test10(){          
                   
        initialMethod();
        
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id);
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id);
            ApexPages.currentPage().getParameters().put('SRType', 'PlotHandover');  
        
        CRE_Plot_Handover_Controller objClass = new  CRE_Plot_Handover_Controller();
        objClass.strSelectedAccount = objAcc.id;
        objClass.strCaseID = objCase.id;
        objClass.init();
        
        test.StartTest();
        
        SOAPCalloutServiceMock.returnToMe  = new Map<String, documentGeneration.DocGenerationResponse_element>();
        documentGeneration.DocGenerationResponse_element response1 = new documentGeneration.DocGenerationResponse_element();
        response1.return_x = 'DocGenerationResponse_element:[apex_schema_type_info=(http://generation.doc.com, true, true), field_order_type_info=(return_x), return_x=http://34.227.23.86:8080/10745_PlotHandoverCustomerRequestForm_14-Feb-2018_09-46-08.pdf, return_x_type_info=(return, http://generation.doc.com, null, 0, 1, true)]';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        
        objClass.blnExceptionalCase = true;
                  
        objClass.generateCRF();       
        objClass.generateUndertakingDoc();
        objClass.generateNocDoc();
        objClass.generateNocDocExceptional();  
         
        test.StopTest();
    }  
        // generateCRF Exception
        static testMethod void Test11(){          
        test.StartTest();
                    
        initialMethod();
        
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id);
            ApexPages.currentPage().getParameters().put('SRType', 'PlotHandover'); 
        
        CRE_Plot_Handover_Controller objClass = new  CRE_Plot_Handover_Controller();
        objClass.strSelectedAccount = objAcc.id;
        objClass.strCaseID = objCase.id;
        objClass.init();               
        
        SOAPCalloutServiceMock.returnToMe  = new Map<String, documentGeneration.DocGenerationResponse_element>();
        documentGeneration.DocGenerationResponse_element response1 = new documentGeneration.DocGenerationResponse_element();
        response1.return_x = 'DocGenerationResponse_element:[apex_schema_type_info=(http://generation.doc.com, true, true), field_order_type_info=(return_x), return_x=http://34.227.23.86:8080/10745_PlotHandoverCustomerRequestForm_14-Feb-2018_09-46-08.pdf, return_x_type_info=(return, http://generation.doc.com, null, 0, 1, true)]';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        
        objClass.blnExceptionalCase = false;
                
        objClass.generateCRF();
        objClass.generateUndertakingDoc();
        objClass.generateNocDoc();
        objClass.generateNocDocExceptional();        
         
        test.StopTest();
    }
    
    static testMethod void Test12(){          
                   
        initialMethod();
        
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id);
            ApexPages.currentPage().getParameters().put('SRType', 'PlotHandover'); 
        
        CRE_Plot_Handover_Controller objClass = new  CRE_Plot_Handover_Controller();
        objClass.strSelectedAccount = objAcc.id;
        objClass.strCaseID = objCase.id;
        objClass.strSelectedBookingUnit = objBookingUnit.id; 
        objClass.init();
        
        test.StartTest();
        
        objClass.OtherDocAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.OtherDocAttachmentName = 'C/fakepath/Document_1.txt';
                          
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        objClass.addAttachment();
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(2));
        objClass.addAttachment();
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(3));
        objClass.addAttachment();
        
        test.StopTest();
   }   
   
       static testMethod void Test13(){          
                   
        initialMethod();
        
        objCase.IsPOA__c = true;
        update objCase;
        
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id);
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id);
            ApexPages.currentPage().getParameters().put('SRType', 'PlotHandover');  
        
        CRE_Plot_Handover_Controller objClass = new  CRE_Plot_Handover_Controller();
        objClass.strSelectedAccount = objAcc.id;
        objClass.strCaseID = objCase.id;
        objClass.strSelectedBookingUnit = objBookingUnit.id; 
        objClass.init();
        
       
                
        test.StartTest();
        
        objClass.strPOAExpiryDate = System.today().format();
        objClass.strCCDate = System.today().format();
        objClass.strPossessionDate = System.today().format();
        
        objClass.crfAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.crfAttachmentName = 'C/fakepath/Document_8.txt';
        /*objClass.PoaAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.PoaAttachmentName = 'C:/fakepath/Document_7.txt';
        objClass.NocAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.NocAttachmentName = 'C:/fakepath/Document_6.txt';  
        objClass.UnderTakingDocAttachmentName = 'C:/fakepath/Document_5.txt';
        objClass.UnderTakingDocAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';    
        objClass.InsuranceDocAttachmentName = 'C:/fakepath/Document_4.txt'; 
        objClass.InsuranceDocAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.BuildingPermitDocAttachmentName = 'C:/fakepath/Document_3.txt'; 
        objClass.BuildingPermitDocAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.AttachmentNameDesignNoc = 'C:/fakepath/Document_2.txt'; 
        objClass.AttachmentBodyDesignNoc = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.BuildingDesignAttachmentName = 'C:/fakepath/Document_1.txt'; 
        objClass.BuildingDesignAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        */
      
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));        
        
        objClass.saveDraft();
        
        test.StopTest();
   } 
   static testMethod void Test14(){          
                   
        initialMethod();
        
        objCase.IsPOA__c = true;
        update objCase;
        
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id);
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id);
            ApexPages.currentPage().getParameters().put('SRType', 'PlotHandover');  
        
        CRE_Plot_Handover_Controller objClass = new  CRE_Plot_Handover_Controller();
        objClass.strSelectedAccount = objAcc.id;
        objClass.strCaseID = objCase.id;
        objClass.strSelectedBookingUnit = objBookingUnit.id; 
        objClass.init();
        
       
                
        test.StartTest();
        
        objClass.strPOAExpiryDate = System.today().format();
        objClass.strCCDate = System.today().format();
        objClass.strPossessionDate = System.today().format();
               
        objClass.PoaAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.PoaAttachmentName = 'C:/fakepath/Document_7.txt';
        /*objClass.NocAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.NocAttachmentName = 'C:/fakepath/Document_6.txt';  
        objClass.UnderTakingDocAttachmentName = 'C:/fakepath/Document_5.txt';
        objClass.UnderTakingDocAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';    
        objClass.InsuranceDocAttachmentName = 'C:/fakepath/Document_4.txt'; 
        objClass.InsuranceDocAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.BuildingPermitDocAttachmentName = 'C:/fakepath/Document_3.txt'; 
        objClass.BuildingPermitDocAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.AttachmentNameDesignNoc = 'C:/fakepath/Document_2.txt'; 
        objClass.AttachmentBodyDesignNoc = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.BuildingDesignAttachmentName = 'C:/fakepath/Document_1.txt'; 
        objClass.BuildingDesignAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        */
      
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));        
        
        objClass.saveDraft();
        
        test.StopTest();
   } 
   static testMethod void Test15(){          
                   
        initialMethod();
        
        objCase.IsPOA__c = true;
        update objCase;
        
        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id);
            ApexPages.currentPage().getParameters().put('SRType', 'PlotHandover'); 
        
        CRE_Plot_Handover_Controller objClass = new  CRE_Plot_Handover_Controller();
        objClass.strSelectedAccount = objAcc.id;
        objClass.strCaseID = objCase.id;
        objClass.strSelectedBookingUnit = objBookingUnit.id; 
        objClass.init();
        
       
                
        test.StartTest();
        
        objClass.strPOAExpiryDate = System.today().format();
        objClass.strCCDate = System.today().format();
        objClass.strPossessionDate = System.today().format();
                      
        objClass.UnderTakingDocAttachmentName = 'C:/fakepath/Document_5.txt';
        objClass.UnderTakingDocAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';    
        /*objClass.InsuranceDocAttachmentName = 'C:/fakepath/Document_4.txt'; 
        objClass.InsuranceDocAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.BuildingPermitDocAttachmentName = 'C:/fakepath/Document_3.txt'; 
        objClass.BuildingPermitDocAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.AttachmentNameDesignNoc = 'C:/fakepath/Document_2.txt'; 
        objClass.AttachmentBodyDesignNoc = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.BuildingDesignAttachmentName = 'C:/fakepath/Document_1.txt'; 
        objClass.BuildingDesignAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        */
      
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));        
        
        objClass.saveDraft();
        objClass.SubmitSR();
        
        test.StopTest();
   } 
}