public without sharing class CustomerController {

    public String srType                                {get;set;}
    public String pageTitle                             {get;set;}
    public Boolean createAdditionalParking              {get;set;}
    public Boolean createAssignmentRequest              {get;set;}
    public Boolean createAOPT                           {get;set;}
    public Boolean createBouncedCheque                  {get;set;}
    public Boolean createFundTransfer                   {get;set;}
    public Boolean createFurniturePackage               {get;set;}
    public Boolean createMortgage                       {get;set;}
    public Boolean createPenaltyWaiver                  {get;set;}
    public Boolean createProofOfPayment                 {get;set;}
    public Boolean createRefunds                        {get;set;}
    public Boolean createCOCD                           {get;set;}
    public Boolean createEarlyHandover                  {get;set;}
    public Boolean createNOCforVisa                     {get;set;}
    public Boolean createComplaint                      {get;set;}
    public Boolean createRentalPool                     {get;set;}
    public Boolean createTitleDeed                      {get;set;}
    public Boolean createPlotHandover                   {get;set;}
    public Boolean isMyProfileView                      {get;set;}
    public Boolean isUnitDetail                         {get;set;}
    public Boolean isCaseComments                       {get;set;}
    public Boolean isCaseDetail                         {get;set;}
    public Boolean isAppointmentView                    {get;set;}
    public Boolean isAppointmentDetailView              {get;set;}
    public Boolean isUtilityRegistration                {get;set;}
    public Boolean createRentalPoolTermination          {get;set;}
    public Boolean createRentalPoolAssignment           {get;set;}
    public Boolean createChequeReplacement              {get;set;}
    public Boolean createPromotions                     {get;set;}

    public Boolean home                                 {get;set;}

    public String codType                               {get;set;}
    public String recordId                              {get;set;}
    public String tab                                   {get;set;}

    public List<Account> accountsList                   {get;set;}
    public List<SelectOption> listAccountOptions        {get;set;}
    public Id selectedAccountId                         {get;set;}
    public Boolean isAccountSelected                    {get;set;}
    public Boolean createHandover                       {get;set;}
    public Map<Id,Account> mapIdAccount                 {get;set;}
    public Account account                              {get;set;}
    public string strAccountId                          {get;set;}
    public Account objAccount                           {get;set;}// account object to display details for AOPT process

    public Boolean hasHandedOverUnits                   {get;set;}

    public SupportDetails creDetails                    {get;set;}
    public Integer closedCaseCount                      {get;set;}
    public Integer openCaseCount                        {get;set;}

    public class SupportDetails {
        public String supportName                       {get;set;}
        public String supportContactNumber              {get;set;}
        public String supportContactEmail               {get;set;}
    }

    public PageReference authenticateUser() {
        if ('Guest'.equalsIgnoreCase(UserInfo.getUserType())) {
            return Network.forwardToAuthPage(ApexPages.currentPage().getUrl());
        } else {
            /*String currentUserProfile = query the profile;*/
            /*String currentUserprofile =
            if currentUserProfile == LoamsCommunitySettings__c.getInstance().TenantProfileName__c
                redirect to '/community';*/

            List<User> currentUserLst = [SELECT id
                                              , Profile.Name
                                        FROM User
                                        WHERE Id =:UserInfo.getUserId()
                                        LIMIT 1 ];
            if(currentUserLst != null && !currentUserLst.isEmpty() ) {
                System.debug('currentUser Profile : ' + currentUserLst[0].Profile.Name);
                string currentUserProfile = currentUserLst[0].Profile.Name;
                if(currentUserProfile == LoamsCommunitySettings__c.getInstance().TenantProfileName__c) {
                    String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
                    System.debug('Base URL: ' + sfdcBaseURL );

                    PageReference communityPage = new PageReference(sfdcBaseURL+'/community');
                    return communityPage;
                }
            }


            if (CustomerCommunityUtils.needsToTakeSurvey) {
                PageReference surveyPage = Page.CustomerSurvey;
                surveyPage.getParameters().put('type', 'portal');
                return surveyPage;
            } else {
                return null;
            }
        }
    }

    public CustomerController() {

        selectedAccountId = CustomerCommunityUtils.customerAccountId;

        srType = ApexPages.currentPage().getParameters().get('SRType');
        strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
        //system.debug('SRType'+SRType);
        //system.debug('strAccountId'+strAccountId);

        populateCreateSRBoolean();
        setPageTitle();

        accountsList = new List<Account>();
        mapIdAccount= new Map<Id,Account>();
        account = new Account();
        isAccountSelected = false;

        // fetch account details for AOPT process
        getCustomerPortfolio();
        //system.debug('cc objAccount : '+objAccount);

        Set<String> setActiveStatuses = Booking_Unit_Active_Status__c.getAll().keySet();
        hasHandedOverUnits = ![
            SELECT  Id
            FROM    Booking_Unit__c
            WHERE   Booking__r.Account__c = :CustomerCommunityUtils.customerAccountId
                    AND (Handover_Flag__c = 'Y' OR Early_Handover__c = true)
                    AND Registration_Status__c IN :setActiveStatuses
            LIMIT   1
        ].isEmpty();

        creDetails = getSupportDetails(strAccountId);
        fetchCaseDetails();
    }

    public void fetchCaseDetails(){
        closedCaseCount = 0;
        openCaseCount = 0;
        Account customerAccount = new Account();
        Id customerAccountId = CustomerCommunityUtils.customerAccountId;
        System.debug('customerAccountId = ' + customerAccountId);
        if (customerAccountId != NULL) {
            List<Account> lstAccount = [ SELECT  Id,
                                                (SELECT IsClosed
                                                   FROM Cases
                                                ORDER BY    CreatedDate)
                                        FROM    Account
                                        WHERE   Id = :customerAccountId];
            if (!lstAccount.isEmpty()) {
                customerAccount = lstAccount[0];
                for (Case sr : customerAccount.Cases) {
                    if (sr.IsClosed) {
                        closedCaseCount++;
                    } else {
                        openCaseCount++;
                    }
                }
            }

        }
    }

    public void populateCreateSRBoolean() {
        createAdditionalParking = false;
        createAssignmentRequest = false;
        createNOCforVisa = false;
        isUtilityRegistration = false;
        createAOPT = false;
        createBouncedCheque = false;
        createFundTransfer = false;
        createFurniturePackage = false;
        createMortgage = false;
        createPenaltyWaiver = false;
        createProofOfPayment = false;
        createRefunds = false;
        createCOCD = false;
        createEarlyHandover = false;
        createComplaint = false;
        createRentalPool = false;
        createTitleDeed = false;
        createHandover = false;
        createRentalPoolTermination = false;
        createRentalPoolAssignment = false;
        createChequeReplacement = false;
        createPromotions = false;
        createPlotHandover = false;
        home = false;
        isMyProfileView = false;
        isUnitDetail = false;
        isCaseDetail = false;

        System.debug('srType = ' + srType);

        srType = String.isNotBlank(srType) ? srType : ApexPages.currentPage().getParameters().get('view');
        srType = String.isBlank(srType) ? '' : srType.toLowerCase();
        recordId = String.isNotBlank(recordId) ? recordId : ApexPages.currentPage().getParameters().get('id');
        tab = String.isNotBlank(tab) ? tab : ApexPages.currentPage().getParameters().get('tab');

        System.debug('srType = ' + srType);

        if(String.isNotBlank(srType)){
            if(srType.equalsIgnoreCase('AdditionalParking')){
                createAdditionalParking = true;
            } else if(srType.equalsIgnoreCase('AssignmentRequest')){
                createAssignmentRequest = true;
            } else if(srType.equalsIgnoreCase('AOPT')){
                createAOPT = true;
            } else if(srType.equalsIgnoreCase('BouncedCheque')){
                createBouncedCheque = true;
            } else if(srType.equalsIgnoreCase('FundTransferActiveUnits')){
                createFundTransfer = true;
            } else if(srType.equalsIgnoreCase('FurniturePackage')){
                createFurniturePackage = true;
            } else if(srType.equalsIgnoreCase('Mortgage')){
                createMortgage = true;
            } else if(srType.equalsIgnoreCase('PenaltyWaiver')){
                createPenaltyWaiver = true;
            } else if(srType.equalsIgnoreCase('ProofOfPayment')){
                createProofOfPayment = true;
            } else if(srType.equalsIgnoreCase('Refunds')){
                createRefunds = true;
            } else if(srType.equalsIgnoreCase('COCD')){
                createCOCD = true;
            } else if(srType.equalsIgnoreCase('EarlyHandover')){
                createEarlyHandover = true;
            } else if(srType.equalsIgnoreCase('NOCVisa')){
                createNOCforVisa = true;
            } else if(srType.equalsIgnoreCase('Complaint')){
                createComplaint = true;
            } else if(srType.equalsIgnoreCase('Home')){
                home = true;
            } else if(srType.equalsIgnoreCase('RentalPool')){
                createRentalPool = true;
            } else if(srType.equalsIgnoreCase('TitleDeed')){
                createTitleDeed = true;
            } else if(srType.equalsIgnoreCase('Handover')){
                createHandover = true;
            } else if(srType.equalsIgnoreCase('RentalPoolTermination')){
                createRentalPoolTermination = true;
            } else if(srType.equalsIgnoreCase('RentalPoolAssignment')){
                createRentalPoolAssignment = true;
            } else if(srType.equalsIgnoreCase('ChequeReplacement')){
                createChequeReplacement = true;
            } else if(srType.equalsIgnoreCase('Promotions')){
                createPromotions = true;
            } else if(srType.equalsIgnoreCase('PlotHandover')){
                createPlotHandover = true;
            } else if(srType.equalsIgnoreCase('MyProfileView')){
                isMyProfileView = true;
            } else if(srType.equalsIgnoreCase('UnitDetail')){
                isUnitDetail = true;
            } else if(srType.equalsIgnoreCase('CaseComments')){
                isCaseComments = true;
            } else if(srType.equalsIgnoreCase('CaseDetails')){
                isCaseDetail = true;
            } else if(srType.equalsIgnoreCase('UtilityRegistration')){
                isUtilityRegistration= true;
            }else if(srType.equalsIgnoreCase('AppointmentView')){
                isAppointmentView = true;
            } else if(srType.equalsIgnoreCase('AppointmentDetail')){
                isAppointmentDetailView = true;
            }
        } else {
            home = true;
        }

    }

    private void setPageTitle() {
        pageTitle = 'Customers - ';
        String suffix = (new Map<String, String> {
            ''                              =>  'Dashboard',
            'home'                          =>  'Dashboard',
            'additionalparking'             =>  'Purchase Additional Parking',
            'assignmentrequest'             =>  'Assignment Request',
            'aopt'                          =>  'AOPT',
            'bouncedcheque'                 =>  'Bounced Cheque',
            'fundtransferactiveunits'       =>  'Move Excess Funds to Service Charge',
            'furniturepackage'              =>  'Furniture Package',
            'mortgage'                      =>  'Request for Mortgage',
            'refunds'                       =>  'Refunds',
            'proofofpayment'                =>  'Proof of Payment',
            'penaltywaiver'                 =>  'Penalty Waiver',
            'cocd'                          =>  'Change of Details',
            'earlyhandover'                 =>  'Early Handover',
            'nocvisa'                       =>  'Apply for Visa NOC',
            'complaint'                     =>  'Complaints',
            'rentalpool'                    =>  'Join Rental Pool',
            'titledeed'                     =>  'Apply for Title Deed',
            'handover'                      =>  'Handover',
            'rentalpooltermination'         =>  'Rental Pool Termination',
            'rentalpoolassignment'          =>  'Transfer Rental Pool',
            'chequereplacement'             =>  'Replace Cheque',
            'promotions'                    =>  'Promotional Offers',
            'plothandover'                  =>  'Plot Handover',
            'myprofileview'                 =>  'My Portfolio',
            'unitdetail'                    =>  'Unit Details',
            'casedetails'                   =>  'Case Details',
            'utilityregistration'           =>  'Utility Registration',
            'appointmentview'               =>  'Schedule Appointment',
            'appointmentdetail'             =>  'Appointment Details',
            ' sr '                          =>  'My Service Requests'
        }).get(srType);

        if (String.isBlank(suffix)) {
            suffix = srType;
        }
        pageTitle += suffix;
    }

    // method used to retrieve customer portfolio details
    public void getCustomerPortfolio() {
        if(selectedAccountId != null){
            objAccount = [ SELECT Id,Name,Title__c
                          ,PersonTitle,PersonMobilePhone
                          ,PersonEmail,FirstName,LastName
                          ,MiddleName,Party_ID__c,Nationality__pc
                          ,Party_Type__c,Passport_Number__c,CR_Number__c
                          ,Email__pc,Email__c,Mobile_Phone_Encrypt__pc,Mobile__c
                          FROM Account WHERE ID = : selectedAccountId ];
        }
        //system.debug('>>objAccount : '+objAccount);
    }

    public String getFullPhotoUrl() {
        return CustomerCommunityUtils.getFullPhotoUrl();
    }

    public static SupportDetails getSupportDetails(Id strAccountId){

        SupportDetails supportDetails = new SupportDetails();

        List<Account> accountList = [
            SELECT Primary_CRE__r.Phone
                 , Primary_CRE__r.Email
                 , Primary_CRE__r.Name
              FROM Account
             WHERE Id = :strAccountId
               AND Primary_CRE__c != null
        ];

        if( accountList.isEmpty() ){
            supportDetails.supportName          = 'Customer Service';
            supportDetails.supportContactNumber = '+9714 2375000';
            supportDetails.supportContactEmail  = 'atyourservice@damacproperties.com';
        } else {
            supportDetails.supportName          =  accountList[0].Primary_CRE__r.Name;
            supportDetails.supportContactNumber =  accountList[0].Primary_CRE__r.Email;
            supportDetails.supportContactEmail  =  accountList[0].Primary_CRE__r.Phone;
        }

        return supportDetails;
    }

}