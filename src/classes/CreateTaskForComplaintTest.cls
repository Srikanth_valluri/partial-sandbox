/**
 * @File Name          : CreateTaskForComplaintTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 12/17/2019, 12:53:15 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/17/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
public class CreateTaskForComplaintTest {
    public static testMethod void unitTest() {
         Account objA = TestDataFactoryFM.createAccount();
        insert objA;
        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objA);
        insert objSR;
        Booking__c objBooking = TestDataFactoryFM.createBooking(objA,objSR);
        insert objBooking;

        Location__c locObj=TestDataFactoryFM.createLocation();
        insert locObj;
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        Booking_Unit__c objBU = TestDataFactoryFM.createBookingUnit(objA,objBooking);
        objBU.Inventory__c=invObj.id;
        insert objBU;
        list<FM_Case__c> lstC = TestDataFactoryFM.createFMCase(2);
        lstC[0].Account__c = objA.Id;
        lstC[0].Booking_Unit__c = objBU.Id;
        lstC[0].Request_Type__c = Label.Service_Charge_Enquiry;
        lstC[0].status__c='Submitted';
        lstC[0].submitted__c=true;

        lstC[1].Account__c = objA.Id;
        lstC[1].Booking_Unit__c = objBU.Id;
        lstC[1].Request_Type__c = Label.Service_Charge_Enquiry;
        lstC[1].status__c='Submitted';
        lstC[1].submitted__c=true;

        insert lstC;
        list<Id> lstId = new list<Id>();
        lstId.add(lstC[0].Id);
        lstId.add(lstC[1].Id);
        CreateTaskForComplaint.onTaskClosureForComplaint(lstId);

    }
}