global without sharing class FmcUnitDetailsController {

    public String           unitId      {get; set;}
    public Booking_Unit__c  unit        {get; set;}
    public String           srFilter    {get; set;}
    public String           partyId     {get; set;}
    public Boolean          isOwner     {get; set;}

    public FmcUnitDetailsController() {
        if (FmcUtils.isCurrentView('UnitDetails')) {
            initializeVariables();
        }
    }

    private void initializeVariables() {
        if (FmcUtils.isCurrentView('UnitDetails')) {
            partyId = CustomerCommunityUtils.getPartyId();
            isOwner = FmcUtils.isOwner();
            unit = new Booking_Unit__c();
            unitId = ApexPages.currentPage().getParameters().get('id');
            List<Booking_Unit__c> lstUnit = [
                SELECT  Id,
                        Registration_ID__c,
                        Name,
                        Unit_Name__c,
                        Booking__r.Account__c,
                        Inventory__c,
                        Inventory__r.Building_Location__c,
                        Inventory__r.Building_Location__r.Location_Code__c,
                        Inventory__r.Property_Code__c,
                        Registration_Status__c,
                        Property_Name__c,
                        Owner__r.Name,
                        Tenant__r.Name,
                        Resident__r.Name,
                        DLP_End_Date__c,
                        Property_City__c,
                        NOC_Issued_Date__c,
                        CurrencyIsoCode,
                        Bedroom_Type__c,
                        Unit_Type__c,
                        Permitted_Use__c,
                        Agreement_Date__c,
                        Unit_Plan__c,
                        Floor_Plan__c,
                        Area_sft__c,
                        JOPD_Area__c,
                        Handover_Flag__c,
                        Early_Handover__c
                FROM    Booking_Unit__c
                WHERE   Id = :unitId
            ];
            if (!lstUnit.isEmpty()) {
                unit = lstUnit[0];
            }

            srFilter = ' WHERE Booking_Unit__c = ' + (unitId == NULL ? ' NULL ' : '\'' + unitId + '\'') + ' ';
            if (unit.Booking__r.Account__c != CustomerCommunityUtils.customerAccountId) {
                srFilter += ' AND Account__c = \'' + CustomerCommunityUtils.customerAccountId + '\' ';
            }
        }
    }

    @RemoteAction
    public static UnitDetailsService.BookinUnitDetailsWrapper getUnitDetailsFromIPMS(String registrationId) {
        UnitDetailsService.BookinUnitDetailsWrapper wrapper = UnitDetailsService.getBookingUnitDetails(registrationId);
        System.debug('wrapper = ' + JSON.serialize(wrapper));
        return wrapper;
    }

    @RemoteAction
    public static String generateUnitSoa(String registrationId) {
        if (String.isBlank(registrationId)) {
            return registrationId;
        }
        String soaUrl = FmIpmsRestServices.getUnitSoaByRegistrationId(registrationId);
        //System.debug('soaUrl = ' + soaUrl);
        if (String.isBlank(soaUrl)) {
            throw new LoamsCommunityException('Error : Unit SOA generation failed. Please try again');
        }
        return soaUrl;
    }

    @RemoteAction
    public static String generateLocationSoa(String partyId, String projectCode) {
        if (String.isBlank(partyId) || String.isBlank(projectCode)) {
            return NULL;
        }
        FmIpmsRestServices.Response response = FmIpmsRestServices.getBulkSoaForPartyIdAndProjectCode(
            partyId, projectCode
        );

        if (response == NULL || !response.complete || response.actions == NULL || response.actions.isEmpty()) {
            throw new LoamsCommunityException(
                'Error : Location SOA generation failed. Please try again'
                + ((response != NULL && response.errorMessage != NULL && !response.errorMessage.isEmpty())
                    ? ' ' + response.errorMessage[0].code + ': ' + response.errorMessage[0].message : '')
            );
        }
        return response.actions[0].url;
    }

    @RemoteAction
    public static String generateBulkSoa(String partyId) {
        if (String.isBlank(partyId)) {
            return partyId;
        }
        FmIpmsRestServices.Response response = FmIpmsRestServices.getBulkSoaForPartyId(partyId);
        //System.debug('soaUrl = ' + soaUrl);
        if (response == NULL || !response.complete || response.actions == NULL || response.actions.isEmpty()) {
            throw new LoamsCommunityException(
                'Error : Bulk SOA generation failed. Please try again'
                + ((response != NULL && response.errorMessage != NULL && !response.errorMessage.isEmpty())
                    ? ' ' + response.errorMessage[0].code + ': ' + response.errorMessage[0].message : '')
            );
        }
        return response.actions[0].url;
    }

    webservice static string doPay(ID buID, Decimal amt, String url){
        //ID accntID,
        if(buID != null && amt != null){
            /*for(Receipt__c r : [select id,name from Receipt__c where Booking_Unit__c =: buID]){
                return 'Receipt already generated';
            }*/
            string message = initiateTransaction(buID,amt, url);
            System.debug('Final URL = '
                + 'https://uat.timesofmoney.com/direcpay/secure/PaymentTransactionServlet?requestParameter='
                + message
            );
            return message;
        }
        return null;
    }

    webservice static String initiateTransaction(string buID, Decimal amt, String url) {
        try {
            Booking_Unit__c unit = [
                SELECT Id, Name, Unit_Name__c, Booking__c, Booking__r.Deal_SR__c, Handover_Flag__c
                FROM Booking_Unit__c
                WHERE Id = :buID
            ];
            Receipt__c R = new Receipt__c();
            R.Amount__c=amt;
            R.Receipt_Type__c='Card';
            R.Transaction_Date__c=system.now();
            R.Booking_Unit__c = buID != null ? buID : null;
            Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Card').getRecordTypeId();
            R.RecordTypeId=rt;
            insert R;

            Receipt__c newR;

            //Query the newly created receipt record
            newR =[select id,name,Payer__c,Amount__c,Booking_Unit__c,Booking_Unit__r.Booking__c from Receipt__c where id=:R.id];
            system.debug('--check->'+newR.Payer__c+'---->'+newR.Booking_Unit__r.Booking__c+'-----'+newR.Booking_Unit__c);
            system.debug('--Name->'+newR.name);
            String payertype='Self';
            if(newR.Payer__c=='Self')
                payertype='Self';
            else
                payertype='Third Party';
            Account Payer = [SELECT Id,
                                    Name,
                                    FirstName,
                                    LastName,
                                    Email__pc,
                                    Address_Line_1__pc,
                                    City__pc,
                                    Country__pc
                            FROM    Account
                            WHERE   Id = :CustomerCommunityUtils.customerAccountId
                            LIMIT   1];

            System.debug('Payer = ' + JSON.serializePretty(Payer));

            //Getting the country code
            String payercountry=DamacUtility.getCountryCode(Payer.Country__pc);

            //Setting the default state and PO values
            Map<String,String> StateMap = new Map<String,String>();
            Map<String,String> POMap = new Map<String,String>();
            for(NI_State_PO_values__mdt StatePOval :[select id,DeveloperName,State__c,PO__c from NI_State_PO_values__mdt]){
                StateMap.put(StatePOval.DeveloperName,StatePOval.State__c);
                POMap.put(StatePOval.DeveloperName,StatePOval.PO__c);
            }

            string cc,state,PO='';
            if(payercountry=='US'||payercountry=='CA'){
                cc=payercountry;
            } else {
                cc='Others';
            }

            state=StateMap.get(cc);
            PO = POMap.get(cc);
            if(payertype=='Third Party'){
                NSIBPM__Document_Master__c DM = [select id,NSIBPM__Code__c from NSIBPM__Document_Master__c where NSIBPM__Code__c ='Third party consent'];

                NSIBPM__SR_Doc__c SRDoc = new NSIBPM__SR_Doc__c();
                SRDoc.NSIBPM__Service_Request__c = unit.Booking__r.Deal_SR__c;
                SRDoc.Booking_Unit__c=newR.Booking_Unit__c;
                SRDoc.NSIBPM__Generate_Document__c=true;
                SRDoc.NSIBPM__Document_Master__c=DM.id;
                SRDoc.name='Third Party Consent';
                SRDoc.NSIBPM__Status__c='Generated';
                insert SRDoc;
            }
            Id ntwrkId = Network.getNetworkId();
            list<PaymentGateway__c> lstCustmSettng = new list<PaymentGateway__c>();
            lstCustmSettng = [SELECT AccessCode__c, EncryptionKey__c, MerchantId__c,Url__c
                              FROM PaymentGateway__c
                              WHERE Name = 'Customer Portal'];
            system.debug('>>>>lstCustmSettng>>>>'+lstCustmSettng);

            String encRequest = CCAvenuePaymentGateway.getInstance(
                LoamsCommunityController.GATEWAY_NAME
            ).initiateTransaction(
                newR.name + '/' + unit.Unit_Name__c ,
                (newR.Amount__c == NULL ? 0 : newR.Amount__c),
                url + '/' + 'CustomerPaymentStatusUpdate',
               (String.isBlank(Payer.FirstName) ? '' : Payer.FirstName),
               (String.isBlank(Payer.Address_Line_1__pc) ? '' : Payer.Address_Line_1__pc),
               (String.isBlank(Payer.City__pc) ? '' : Payer.City__pc),
               (String.isBlank(state) ? '' : state),
               (String.isBlank(PO) ? '' : PO),
               (String.isBlank(payercountry) ? '' : payercountry)
            );
            return encRequest;
        }
        catch(Exception e){
            System.debug('>>>>>>>>>>>>>>'+e.getLineNumber() +'--->'+e.getmessage());
            return e.getMessage()+'-'+e.getLineNumber();
        }
    }

}