@IsTest(SeeAllData=true)
private class ProfilePhotoUploadControllerTest {

    @isTest
    static void testPhotoUpload() {
        String base64Data = 'iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAAM1BMVEUAAAD///////////////////////////////////////////////////////////////+3leKCAAAAEHRSTlMAaMwztIcOBKVUSD0k23sbzKrGSwAAAFhJREFUOMvtzjkOgDAMRNGMTfYAvv9pcUOHY4kOlGlS/CfF4ZM7O9DsPEh0m9nzLnPQxQHQeug7BXgPMnNUEJnHM0hyL3qgGV8ARWsBknvkAg6oRFTDD3cBCJQFslHv0TwAAAAASUVORK5CYII=';
        ContentVersion cv = new ContentVersion();
        cv.ContentLocation = 'S';
        cv.VersionData = EncodingUtil.base64Decode(base64Data);
        cv.Title = 'fileName';
        cv.PathOnClient = 'file.png';
        insert cv;

        System.assertNotEquals(NULL, ProfilePhotoUploadController.setPhotoFromVersion(cv.Id));
        System.debug(new ProfilePhotoUploadController().getUserPhotoUrl());

    }

}