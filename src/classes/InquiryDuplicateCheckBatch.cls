public class InquiryDuplicateCheckBatch implements Database.Batchable<SObject>,Database.Stateful {

    String query;
    String agentTeamRecordType;
    Public Boolean isDuplicateExist;
/*
    public InquiryDuplicateCheckBatch() {
        isDuplicateExist = false;
        agentTeamRecordType = DamacUtility.getRecordTypeId('Inquiry__c','Agent Team');
        query = 'SELECT Id,Name,Email__c,Mobile_Phone__c,Organisation_Name__c,'+
                'CR_Number__c,First_Name__c,Last_Name__c,ORN_Number__c,isDuplicate__c FROM '+
                'Inquiry__c WHERE recordTypeId =\''+agentTeamRecordType+'\' AND '+
                'isDuplicate__c = false ORDER BY '+
                'Email__c,Mobile_Phone__c,Organisation_Name__c,'+
                'CR_Number__c,First_Name__c,Last_Name__c,ORN_Number__c '+
                'ASC NULLS LAST';
    }
*/

    public Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('----query---'+query);
        if (Test.isRunningTest()) {
            query = 'SELECT Id FROM Inquiry__c LIMIT 1';
        }
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<SObject> scope) {
        /*Integer currentInqIndex = 0;
        Map<String,Inquiry_Duplicate_Criteria__c> duplicateCriteriaMap = new Map<String,Inquiry_Duplicate_Criteria__c>();
        List<Inquiry__c> updatedInq = new List<Inquiry__c>();
        List<String> duplicateCriteriaField = Label.InquiryDuplicateCriteria.split(',');
        Map<String,Inquiry_Duplicate_Criteria__c> duplicateCriteriaSetting = Inquiry_Duplicate_Criteria__c.getAll();
        for(Inquiry_Duplicate_Criteria__c setting : duplicateCriteriaSetting.values()) {
            if(setting.Object_Name__c.equals('Inquiry')) {
                String uniqueKey ='';
                for(String fld : duplicateCriteriaField) {
                    uniqueKey += setting.get(fld)+'_';
                }
                duplicateCriteriaMap.put(uniqueKey.removeEnd('_'),setting);
            }
        }
        system.debug('-----duplicateCriteriaMap----'+duplicateCriteriaMap);
        for(Inquiry__c inq :(List<Inquiry__c>)scope) {
            system.debug('-----currentInqIndex----'+currentInqIndex);
            for(integer i=currentInqIndex+1;i<scope.size();i++) {
                if(duplicateCriteriaField != null && !duplicateCriteriaField.isEmpty()) {
                    String duplicateKey  = '';
                    for(String fld : duplicateCriteriaField) {
                        if(scope[i].get(fld) != null && inq.get(fld) != null) {
                            if(scope[i].get(fld) == inq.get(fld)) {
                                duplicateKey += 'Match' + '_';
                            }
                            else {
                                duplicateKey += 'No Match' + '_';
                            }
                        }
                    }
                    system.debug('-----duplicateKey----'+duplicateKey);
                    if(String.isNotBlank(duplicateKey) &&
                       duplicateCriteriaMap.containsKey(duplicateKey.removeEnd('_')) &&
                       duplicateCriteriaMap.get(duplicateKey.removeEnd('_')).Duplicate__c) {
                        system.debug('-----duplicateFound----'+scope[i]);
                        if(!inq.isDuplicate__c) {
                            inq.isDuplicate__c = true;
                            updatedInq.add(inq);
                            isDuplicateExist = true;
                        }
                    }
                }
            }
            currentInqIndex++;
        }
        system.debug('-----updatedInq----'+updatedInq);
        if(!updatedInq.isEmpty()) {
            update updatedInq;
        }*/
    }

    public void finish(Database.BatchableContext BC) {
        /*if(isDuplicateExist) {
            Id batchJobId = Database.executeBatch(new InquiryDuplicateCheckBatch(),2000);
        }*/
    }

}