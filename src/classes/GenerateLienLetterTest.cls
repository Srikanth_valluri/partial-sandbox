@isTest
private class GenerateLienLetterTest
{
    private static Id getRecordTypeId(){
        return Schema.SObjectType.Case.getRecordTypeInfosByName().get('Mortgage').getRecordTypeId();
    }
    /*
    @isTest static void itShould() {
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
         
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;

        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id; 
        BU.Registration_ID__c = '123135'; 
        BU.Seller_Name__c = 'test';
        BU.Unit_Location_AR__c = 'test';
        BU.Plot_Number__c = 'test';
        BU.Property_Name__c = 'test';
        BU.Property_Name_Arabic__c = 'test';
        BU.Seller_Name_AR__c = 'test';
        BU.Requested_Price__c = 435531;
        insert BU; 

        Buyer__c buyer =  new Buyer__c();
        buyer.Account__c  = Acc.Id;
        buyer.Booking__c   = Booking.Id;
        buyer.Primary_Buyer__c   = false;
        buyer.First_Name__c  = 'Test';      
        buyer.Last_Name__c  = 'Test Last';
        buyer.IPMS_Registration_ID__c ='123456';
        insert buyer;

        Case caseObj = TestDataFactory_CRM.createCase(Acc.Id, getRecordTypeId());
        caseObj.Booking_Unit__c = BU.Id;
        caseObj.Mortgage_Status__c = 'Mortgage Original lien Letter';
        caseObj.Mortgage_Bank_Name__c = 'Test Bank';
        caseObj.Mortgage_Value__c = 21313332;
        caseObj.Mortgage_Start_Date__c = System.today();
        caseObj.Mortgage_End_Date__c = System.today();
        insert caseObj;

        PageReference pr = new PageReference('/'+caseObj.Id);

        ApexPages.StandardController sc = new ApexPages.StandardController(caseObj);
        GenerateLienLetter gn = new GenerateLienLetter(sc);
        
        PageReference pageRef = Page.Generate_Lien_Letter;
        pageRef.getParameters().put('id', String.valueOf(caseObj.Id));
        Test.setCurrentPage(pageRef);

        
        gn.init();

        Test.startTest();
            Test.setMock(WebServiceMock.class, new MortgageWebServiceCalloutMock());
        Test.stopTest();
    }
    @isTest static void itShouldNot() {
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
         
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;

        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id; 
        BU.Registration_ID__c = '123135'; 
        BU.Seller_Name__c = 'test';
        BU.Unit_Location_AR__c = 'test';
        BU.Plot_Number__c = 'test';
        BU.Property_Name__c = 'test';
        BU.Property_Name_Arabic__c = 'test';
        BU.Seller_Name_AR__c = 'test';
        BU.Requested_Price__c = 435531;
        insert BU; 

        Buyer__c buyer =  new Buyer__c();
        buyer.Account__c  = Acc.Id;
        buyer.Booking__c   = Booking.Id;
        buyer.Primary_Buyer__c   = false;
        buyer.First_Name__c  = 'Test';      
        buyer.Last_Name__c  = 'Test Last';
        buyer.IPMS_Registration_ID__c ='123456';
        insert buyer;

        Case caseObj = TestDataFactory_CRM.createCase(Acc.Id, getRecordTypeId());
        caseObj.Booking_Unit__c = BU.Id;
        caseObj.Mortgage_Status__c = 'Mortgage Generate Lien Letter';
        caseObj.Mortgage_Bank_Name__c = 'Test Bank';
        caseObj.Mortgage_Value__c = 21313332;
        caseObj.Mortgage_Start_Date__c = System.today();
        caseObj.Mortgage_End_Date__c = System.today();
        insert caseObj;

        PageReference pr = new PageReference('/'+caseObj.Id);

        ApexPages.StandardController sc = new ApexPages.StandardController(caseObj);
        GenerateLienLetter gn = new GenerateLienLetter(sc);
        
        PageReference pageRef = Page.Generate_Lien_Letter;
        pageRef.getParameters().put('id', String.valueOf(caseObj.Id));
        Test.setCurrentPage(pageRef);

        
        gn.init();

        Test.startTest();
            Test.setMock(WebServiceMock.class, new MortgageWebServiceCalloutMock());
        Test.stopTest();
    }*/
    
    
    static testMethod void TestPositive(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Inventory__c objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].Agreement_Date__c = System.today();
        lstBookingUnits[0].Project_Address_ENU__c = 'Dubai';
        lstBookingUnits[0].Property_Name__c = 'Burj Khalifa';
        insert lstBookingUnits;
        
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Mortgage').RecordTypeId;
        objCase.Cheque_Bank_Name__c = 'UBC';
        insert objCase;
        
        
        insert new SR_Attachments__c(Name = 'Mortgage request form'
                                                        , Attachment_URL__c = 'testsdas.com'
                                                        , Case__c = objCase.Id);
        
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateLienLetter controller = new GenerateLienLetter(sc);
        controller.callDrawloop();
        controller.returnToCase();
        Test.StopTest();
   }
   
   static testMethod void TestBlankUrl(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Inventory__c objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].Agreement_Date__c = System.today();
        lstBookingUnits[0].Project_Address_ENU__c = 'Dubai';
        lstBookingUnits[0].Property_Name__c = 'Burj Khalifa';
        insert lstBookingUnits;
        
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Mortgage').RecordTypeId;
        objCase.Cheque_Bank_Name__c = 'UBC';
        insert objCase;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateLienLetter controller = new GenerateLienLetter(sc);
        controller.callDrawloop();
        controller.returnToCase();
        Test.StopTest();
   }
}