/*-------------------------------------------------------------------------------------------------
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 10-01-2019       | Lochana Rajput   | 1. Updated code to handle multiple SMS
=============================================================================================================================
*/
public class SendSMSQueueable implements Queueable, Database.AllowsCallouts {
    // String SMSHistoryId;
    Set<String> SMSHistorySet;
    Set<Id> fmCaseIdSet;
    public SendSMSQueueable(String smsId) {
        // SMSHistoryId = smsId;
        this.SMSHistorySet = new Set<String>{smsId};
    }
    public SendSMSQueueable(Set<Id> fmCaseIdSet) {
        this.fmCaseIdSet = new Set<Id>();
        this.fmCaseIdSet = fmCaseIdSet;
    }
    public SendSMSQueueable(Set<String> SMSHistorySet) {
        this.SMSHistorySet = new Set<String>();
        this.SMSHistorySet = SMSHistorySet;
    }
    public void execute(QueueableContext context) {
        //Added below logic to handle multiple SMS
        if(SMSHistorySet != NULL && SMSHistorySet.size()>0) {
            // String smsId =
            List<String> lstSmsHistoryId = new List<String>(SMSHistorySet);
            String SMSHistoryId = lstSmsHistoryId.remove(0);
            sendSMS(SMSHistoryId);
            SMSHistorySet = new Set<String>(lstSmsHistoryId);
            if(Test.isRunningTest()) {

            }else {
                System.enqueueJob(new SendSMSQueueable(SMSHistorySet));
            }
        }
        else if(fmCaseIdSet != NULL && fmCaseIdSet.size() >0) {
            getRejectionComments(fmCaseIdSet);
        }

    }

    public void getRejectionComments(Set<Id> pfmCaseIdSet) {
        List<FM_Case__c> fmCaseList = [ SELECT Id,Description__c ,Manager_Comments__c,Approval_Status__c,
                                          (SELECT Id, IsPending, ProcessInstanceId,
                                                  TargetObjectId, StepStatus, OriginalActorId ,
                                                  ActorId, RemindersSent, Comments,
                                                  IsDeleted, CreatedDate, CreatedById,
                                                  SystemModstamp
                                            FROM ProcessSteps
                                            WHERE StepStatus ='Rejected'
                                            ORDER BY  SystemModstamp DESC LIMIT 1)
                                        FROM FM_Case__c
                                        WHERE Id IN :pfmCaseIdSet];
        // update fmCaseList;
        if (fmCaseList.size()>0 && fmCaseList[0].ProcessSteps.size() >0) {
            for(FM_Case__c fmCaseObj : fmCaseList){
                fmCaseObj.Manager_Comments__c = fmCaseObj.ProcessSteps[0].comments;
                system.debug('>>>>>fmCaseObj.Manager_Comments__c'+fmCaseObj.Manager_Comments__c);
            }
            update fmCaseList;
            system.debug('>>>>>fmCaseList'+fmCaseList);

        }
    }

    public void sendSMS(String SMSHistoryId) {
        String strUserName= Label.FM_SMS_service_username;
        String strPassword= Label.FM_SMS_service_password;
        
        List<SMS_History__c> lstSMSHist = new List<SMS_History__c>();
        lstSMSHist = [SELECT Message__c, Phone_Number__c from SMS_History__c where Id=:SMSHistoryId];
        SMS_History__c objSMSHist = new SMS_History__c();
        objSMSHist = lstSMSHist[0];
        String PhoneNumber = objSMSHist.Phone_Number__c;
        if(PhoneNumber.startsWith('00')) {
            PhoneNumber = PhoneNumber.removeStart('00');
        }else if(PhoneNumber.startsWith('0')) {
            PhoneNumber = PhoneNumber.removeStart('0');
        }
        String strSID = SMSClass.getSenderName(strUserName, PhoneNumber, false);

        System.debug('===objSMSHist=='+objSMSHist);
        HttpRequest req = new HttpRequest();
        req.setMethod('POST' ); // Method Type
        req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx');
        req.setBody('user='+ strUserName + '&passwd=' + strPassword +'&message=' 
                + GenericUtility.encodeChar(objSMSHist.Message__c) + '&mobilenumber=' 
                + PhoneNumber + '&sid=' + strSID+ '&MTYPE=LNG&DR=Y'); // Request Parameters
        System.debug('req body = ' + req.getBody());
        Http http = new Http();
        try {
            HTTPResponse res = http.send( req );
            System.debug('res.getBody() = ' + res.getBody());
            if( String.isNotBlank( res.getBody() ) ) {
                objSMSHist.Description__c = res.getBody();
                objSMSHist.Is_SMS_Sent__c = true ;
                 if(String.valueOf(res.getBody()).contains('OK:')){
                     objSMSHist.sms_Id__c = res.getBody().substringAfter('OK:');
                 }
                System.debug('===sms send zala==');
                // return objSMSHist ;
            }
        }
        catch( Exception e ) {
            objSMSHist.Description__c = e.getMessage();
            //objSMSHist.Is_SMS_Sent__c = true ;
            // return objSMSHist ;
        }
        update objSMSHist;
        System.debug('===update  objSMSHist=='+objSMSHist);
    }
}