@isTest
private class DAMAC_CREATE_DIGITAL_INQUIRY_TEST {

    static testMethod void testResults() { 
        
        Campaign__c camp = new Campaign__c();
        camp.End_Date__c = system.today().addmonths(10);
        camp.Marketing_End_Date__c = system.today().addmonths(10);
        camp.Marketing_Start_Date__c = system.today().addmonths(-10);
        camp.Start_Date__c =  system.today().addmonths(-10);
        camp.PCAssignment__c = true;
        insert camp;
        
        String reqBody = '{"adGroup" : "test", "adPosition": "test", "campaignid" : "'+camp.Id+'"}';
        
        
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri ='/services/apexrest/createdigitalinquiry';
        request.httpMethod = 'PUT';
        request.requestBody = Blob.valueof(reqBody);
        RestContext.request = request;
        RestContext.response = response;
        DAMAC_CREATE_DIGITAL_INQUIRY.createInquiry();
    
    }
    
    static testMethod void testResultFail () { 
        
        String reqBody = '{"adGroup" : "test", "adPosition": "test"}';        
        
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri ='/services/apexrest/createdigitalinquiry';
        request.httpMethod = 'PUT';
        request.requestBody = Blob.valueof(reqBody);
        RestContext.request = request;
        RestContext.response = response;
        DAMAC_CREATE_DIGITAL_INQUIRY.createInquiry();
    
    }
}