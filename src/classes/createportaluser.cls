public without sharing class createportaluser{//AKISHOR: for portal login on BU status change
@InvocableMethod

    public static void createuser(List<String> accountid){
         system.debug(' ghusa:  '+ accountid);          
        list<user> userList= new list<user>();
        account acnt=[SELECT Party_ID__c,(Select Id,FirstName,LastName,Email From Contacts) FROM ACCOUNT WHERE id=: accountid];
        Id profileid=[select id from profile where name = 'Customer Community Login User(Use this)'].id;
        system.debug('prof id:  '+ profileid); 
        List<User> userexist = [SELECT IsActive, IsPortalEnabled ,Name,UserRoleId,Username,Title,localesidkey 
                                       , Party_Id__c,Phone,ManagerId,EmployeeNumber
                                       , IPMS_Employee_ID__c,Address,email ,emailencodingkey ,languagelocalekey
                                       , ContactId ,timezonesidkey ,FirstName ,LastName ,ProfileId,Alias
                                    FROM User where Party_Id__c =: acnt.Party_ID__c];
             system.debug(' ghusa:  '+ userexist); 
             if(userexist.size()>0){
             //userexist[0].id.adderror('user alreday exists');
             }
             else if(userexist.size()==0){
             try
             {
             User objUserClone = new User();
             objUserClone.IsActive = true;
             String email=acnt.Contacts[0].Email;
            String aliasVal = String.valueOf(acnt.Contacts[0].Id);
            objUserClone.alias = aliasVal.substring(0, 5);
            objUserClone.Email = acnt.Contacts[0].Email != null ? acnt.Contacts[0].Email : 'test@test.com';
            objUserClone.emailencodingkey = 'UTF-8';
            objUserClone.timezonesidkey = 'Asia/Dubai';
            objUserClone.FirstName =acnt.Contacts[0].FirstName !=''? acnt.Contacts[0].FirstName:'Test First Name';
            objUserClone.LastName = acnt.Contacts[0].LastName != '' ? acnt.Contacts[0].LastName : 'Test Last Name';
            objUserClone.ProfileId = profileId;
            objUserClone.languagelocalekey = 'en_US';
            objUserClone.localesidkey = 'en_GB';
            objUserClone.Party_Id__c = acnt.Party_ID__c;
            objUserClone.IPMS_Employee_ID__c = acnt.Party_ID__c;
            objUserClone.ContactId = acnt.Contacts[0].Id;
            objUserClone.username = acnt.Contacts[0].Email;
            objUserClone.CommunityNickname= acnt.Party_ID__c;         
            system.debug(' objUserClone:  '+ objUserClone);     
            userList.add(objUserClone);
            insert userList;
            //fnSendEmail();
              system.debug(' created:  '+ userList);
            }//end try
            catch(exception e){System.debug('Issue while creating portal user:'+e.getMessage());}           
            }          
     }   
}