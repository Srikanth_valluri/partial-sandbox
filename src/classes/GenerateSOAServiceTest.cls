/*-------------------------------------------------------------------------------------------------
Description: Test class for GenerateSOAService

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 21-11-2017       | Lochana Rajput    | 1. To test SOA document
        1.1     | 21-11-2017       | Naresh (Accely)   | Increase coverage from 11 %
                                                         Comments Lochana Rajput Code.
   =============================================================================================================================
*/
@isTest
private class GenerateSOAServiceTest {
    // variable Created By Naresh
    static GenerateSOAService.COCDHttpSoap11Endpoint  obj = new GenerateSOAService.COCDHttpSoap11Endpoint();
    static String resp ;
   /*
	@testSetup
	static void createSetupDate() {
		Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
		Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;
		NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
		dealSR = TestDataFactory_CRM.createServiceRequest();
		insert dealSR;
		Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
		insert objBooking;
		Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
                        Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
		insert BUObj;
  
	}

	@isTest static void test_generateSOADoc() {
		Booking_Unit__c BUObj = [SELECT Registration_ID__c FROM Booking_Unit__c LIMIT 1];
		Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );
		Test.startTest();
		GenerateSOAController.getSOADocument(BUObj.Registration_ID__c);
		Test.stopTest();
	}

	@isTest static void test_generateSOADocNullResponse() {
		Booking_Unit__c BUObj = [SELECT Registration_ID__c FROM Booking_Unit__c LIMIT 1];
		Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(2) );
		Test.startTest();
		GenerateSOAController.getSOADocument(BUObj.Registration_ID__c);
		Test.stopTest();
		// Implement test code
	}
	*/
	
	
	/****** From here : Naresh *******************/
	
    public static testmethod void COCDFinancials(){
    
    Test.startTest();
    SOAPCalloutServiceMock.returnToMe =  new Map<String, GenerateSOAService.COCDFinancialsResponse_element>();
    GenerateSOAService.COCDFinancialsResponse_element response_x = new GenerateSOAService.COCDFinancialsResponse_element();
    response_x.return_x = 'S';
    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
        resp = obj.COCDFinancials('PartyId','SourceofRequest','ProcessName','SubProcesName','ProjectCity','Project','BuildingCode','PermittedUse',
                                   'BedroomType','ReadyOffPlan','CustomerType','ApplicableNationality','POA');
    System.assertEquals(resp,'S');
    Test.stopTest();
    
  } 
  
   public static testmethod void COCDDocumentation(){
    
    Test.startTest();
    SOAPCalloutServiceMock.returnToMe =  new Map<String, GenerateSOAService.COCDDocumentationResponse_element >();
    GenerateSOAService.COCDDocumentationResponse_element  response_x = new GenerateSOAService.COCDDocumentationResponse_element ();
    response_x.return_x = 'S';
    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
    resp = obj.COCDDocumentation('RegistrationId','SourceofRequest','ProcessName','SubProcesName',
                                        'ProjectCity','Project','BuildingCode','PermittedUse','BedroomType','ReadyOffPlan',
                                       'CustomerType','ApplicableNationalityNew','UnderAssignment','POA') ;
    System.assertEquals(resp,'S');
    Test.stopTest();
    
  } 

	
 public static testmethod void UpdateCOCD(){
    
    Test.startTest();
    GenerateSOAUtility.COCDRequestMessage objTest =  new GenerateSOAUtility.COCDRequestMessage();
    SOAPCalloutServiceMock.returnToMe =  new Map<String, GenerateSOAService.UpdateCOCDResponse_element >();
    GenerateSOAService.UpdateCOCDResponse_element response_x = new GenerateSOAService.UpdateCOCDResponse_element();
    response_x.return_x = 'S';
    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
    resp = obj.UpdateCOCD(objTest);
    System.assertEquals(resp,'S');
    Test.stopTest();
    
  } 
  
   public static testmethod void generateCOCD(){
    
    Test.startTest();
    SOAPCalloutServiceMock.returnToMe =  new Map<String, GenerateSOAService.generateCOCDResponse_element  >();
    GenerateSOAService.generateCOCDResponse_element response_x = new GenerateSOAService.generateCOCDResponse_element();
    response_x.return_x = 'S';
    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
    resp = obj.generateCOCD('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM',
                                  'IPMSPartyID','Address1','Address2','Address3','Address4','city','state','postalCode','country','MobileCountryCode','MobileAreaCode','MobileNumber','PhoneCountryCode','PhoneAreaCode','PhoneNumber',
                                   'FaxCountryCode','FaxAreaCode','FaxNumber','EmailAddress','PassportIssueDate','PassportNumnber');
    System.assertEquals(resp,'S');
    Test.stopTest();
    
  } 
	
   // To cover COCDApprovalsRequired()
   public static testmethod void COCDApprovalsRequired(){
    
    Test.startTest();
    SOAPCalloutServiceMock.returnToMe =  new Map<String, GenerateSOAService.COCDApprovalsRequiredResponse_element>();
    GenerateSOAService.COCDApprovalsRequiredResponse_element response_x = new GenerateSOAService.COCDApprovalsRequiredResponse_element   ();
    response_x.return_x = 'S';
    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
    resp = obj.COCDApprovalsRequired('RegistrationId','SourceofRequest',
                                        'ProcessName','SubProcesName','ProjectCity','Project','BuildingCode',
                                        'PermittedUse','BedroomType','ReadyOffPlan','CustomerType',
                                        'ApplicableNationality','UnderAssignment','POA');

    System.assertEquals(resp,'S');
    Test.stopTest();
    
  } 
  
    // To cover GenCustomerStatement()
 public static testmethod void GenCustomerStatement(){
    
    Test.startTest();
    SOAPCalloutServiceMock.returnToMe =  new Map<String, GenerateSOAService.GenCustomerStatementResponse_element>();
    GenerateSOAService.GenCustomerStatementResponse_element  response_x = new GenerateSOAService.GenCustomerStatementResponse_element    ();
    response_x.return_x = 'S';
    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
    resp = obj.GenCustomerStatement('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','ParamId','stageID','batchname','RegistrationId');
    System.assertEquals(resp,'S');
    Test.stopTest();
    
  } 
  
    // To cover CreateDebitCreditMemo()
   public static testmethod void CreateDebitCreditMemo(){
    
    Test.startTest();
    SOAPCalloutServiceMock.returnToMe =  new Map<String, GenerateSOAService.CreateDebitCreditMemoResponse_element>();
    GenerateSOAService.CreateDebitCreditMemoResponse_element response_x = new GenerateSOAService.CreateDebitCreditMemoResponse_element     ();
    response_x.return_x = 'S';
    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
    resp = obj.CreateDebitCreditMemo('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','RegistrationID','Amount','TRANS_TYPEClass','CALL_TYPE','UniqueTransactionNumber','DESCRIPTION');
    System.assertEquals(resp,'S');
    Test.stopTest();
    
  } 
	
}