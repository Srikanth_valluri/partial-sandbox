@isTest
private class Damac_NewTripController_Test {

    static testMethod void testTripController() {
        Trip__c trip = new Trip__c ();
        trip.Trip_Requester_Comments__c = 'Testing';
        trip.start_time__c = DateTime.Now().addMinutes(5);
        trip.end_time__c = DateTime.Now().addMinutes(10);
        trip.Status__c = 'Test';
        
        Inquiry__c newInquiry = InitialiseTestData.getInquiryDetails('CIL',1);
        insert newInquiry;
        apexpages.currentpage().getparameters().put('id', newInquiry.Id);
        
        
        Damac_NewTripController obj = new Damac_NewTripController();
        obj.trip  = trip;
        obj.save();
    }
    
    static testMethod void testTripController2() {
        Inquiry__c newInquiry = InitialiseTestData.getInquiryDetails('CIL',1);
        insert newInquiry;
        
        newInquiry = [SELECT NAME FROM Inquiry__c WHERE ID =: newInquiry.ID] ;
        
        Trip__c trip = new Trip__c ();
        trip.Trip_Requester_Comments__c = 'Testing';
        trip.start_time__c = DateTime.Now().addMinutes(5);
        trip.end_time__c = DateTime.Now().addMinutes(10);
        trip.Status__c = 'Test';
        
        Damac_NewTripController obj = new Damac_NewTripController();
        Damac_NewTripController.searchInquiries ('IQ-1');
        
        obj.inqId = newInquiry.Name;
        obj.trip  = trip;
        obj.save();
    }
    
    static testMethod void testTripController3() {
        
        Trip__c trip = new Trip__c ();
        trip.Trip_Requester_Comments__c = 'Testing';
        trip.start_time__c = DateTime.Now().addMinutes(5);
        trip.end_time__c = DateTime.Now().addMinutes(10);
        trip.Status__c = 'Test';
        
        Damac_NewTripController obj = new Damac_NewTripController();
        Damac_NewTripController.searchInquiries ('IQ-1');
        
        obj.trip  = trip;
        obj.save();
        System.debug (obj.result);
    }
}