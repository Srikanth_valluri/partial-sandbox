public class Invocable_createParentSRStep {
    @InvocableMethod
    public static void createParentSRStep(List<Id> stepId) {
        for (New_Step__c step :[SELECT Id, Service_Request__c FROM New_Step__c WHERE Id IN :stepId]) {
            EvaluateCustomCode(step);
        }
    }

    public static String EvaluateCustomCode(New_Step__c step) {
        string retString = 'Success';
        try {
            if (step != null)
            {
                String srId = '';
                String srType = 'INSERT';
                list<NSIBPM__Service_Request__c> getParentSR = new list<NSIBPM__Service_Request__c> ();
                getParentSR = [select id, NSIBPM__Parent_SR__c from NSIBPM__Service_Request__c where id = :step.Service_Request__c limit 1];
                Group salesAuditQueue = [SELECT Id FROM Group WHERE Type = 'Queue' AND Name = 'Sales Audit Queue' LIMIT 1];
                if (getParentSR.size() > 0)
                {
                    srId = getParentSR[0].NSIBPM__Parent_SR__c;
                }
                System.Debug('ParentSR>>>>' + srId);
                if (srId != null)
                {
                    list<New_Step__c> checkSalesAudit = new list<New_Step__c> ();
                    checkSalesAudit = [SELECT Id, Service_Request__c, Step_Status__c FROM New_Step__c WHERE Step_Type__c = 'Sales Audit' AND Service_Request__c = :srId];
                    Integer i = checkSalesAudit.size();
                    Integer j = 0;
                    if (i > 0) {
                        while (j<i)
                        {
                            if (checkSalesAudit[j].Step_Status__c == 'Sales Audit Review' || checkSalesAudit[j].Step_Status__c == 'Sales Audit On Hold' || checkSalesAudit[j].Step_Status__c == 'Sales Audit Rejected') {
                                srType = 'UPDATE';
                                break;
                            }
                            else {
                                j++;
                            }
                        }
                    }

                    /* if(checkSalesAudit.size() > 0){
                      if (checkSalesAudit[0].NSIBPM__Step_Status__c=='Sales Audit Review' || checkSalesAudit[0].NSIBPM__Step_Status__c=='Sales Audit On Hold')
                      {
                      srType='UPDATE';
                      }  
                      }*/
                    System.Debug('WhatUpdate>>>>>' + srType);
                    if (srType == 'INSERT')
                    {
                        New_Step__c newStep = new New_Step__c();
                        newStep.Service_Request__c = srId;
                        newStep.Step_Type__c = 'Sales Audit';
                        newStep.Step_Status__c = 'Sales Audit Review';
                        newStep.Step_No__c = 40;
                        newStep.OwnerId = salesAuditQueue.Id;

                        insert newStep;
                    } //END:if (srType =='INSERT')
                }
                else
                {
                    New_Step__c newStep = new New_Step__c();
                    newStep.Service_Request__c = getParentSR[0].Id;
                    newStep.Step_Type__c = 'Sales Audit';
                    newStep.Step_Status__c = 'Sales Audit Review';
                    newStep.Step_No__c = 40;
                    newStep.OwnerId = salesAuditQueue.Id;

                    insert newStep;
                }

            } //END:if (step !=null)
        } //END:Try
        catch(exception e) {
            retString = string.valueOf(e.getMessage());
        }
        return(retString);
    }
}