/* * * * * * * * * * * * * *
*  Class Name:   AdditionalParkingUtilityTest
*  Purpose:      Unit test class for AdditionalParkingUtility Controller
*  Author:       Hardik Mehta - ESPL
*  Company:      ESPL
*  Created Date: 28-Dec-2017
*  Updated Date: 28-Dec-2017
*  Type:         Test Class
* * * * * * * * * * * * */
@isTest
public with sharing class AdditionalParkingUtilityTest 
{
  public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';

  /* * * * * * * * * * * * *
  *  Method Name:  init_test_scenario1
  *  Purpose:      This method is used to unit test functionality for document generation methods
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 28-Dec-2017
  * * * * * * * * * * * * */
  static testMethod void init_test_scenario1()
  {
    List<Booking__c> bookingList = new List<Booking__c>();
    List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
    List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();

    //insert Booking Unit Active Status custom setting record
    insertBookingUnitActiveStatus();

    //insert sample data for Account
    Account objAccount = TestDataFactory_CRM.createPersonAccount();
    insert objAccount;

    Id parkingRecordTypeID = getRecordTypeIdForParking();

    Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , parkingRecordTypeID);
    objCase.Status = 'Submitted';
    objCase.POA_Expiry_Date__c = System.today();
    objCase.Approval_Status__c = 'Approved';
    objCase.Status = 'Submitted';
    objCase.IsPOA__c = true;
    objCase.Parking_Details_JSON__c = '[{"STATUS_CODE":"A","STATUS":"Available","PROPERTY_NAME":"DAMAC HILLS - ARTESIA","PROPERTY_ID":3491,"PRICE":8000,"PARKING_TYPE_DESC":"Visitor","PARKING_TYPE":"VISITOR","PARKING_BAY_NUMBER":null,"ORG_ID":81,"DETAIL_ID":88,"BUILDING_NAME":"ARTESIA - A","BUILDING_ID":64547,"blnIsSelected":true}]';
    objCase.Pending_Amount__c = 500;
    objCase.Parking_Type_Of_Payment__c = 'Full Payment';
    objCase.Parking_Total_Payment_Terms__c = 3;
    objCase.Parking_Payment_Mode__c = 'Cash Payment';
    insert objCase;

    //create Deal SR record
    NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
    insert objDealSR;

    //create Booking record for above created Deal and Account
    bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
    insert bookingList;

    //create Property data
    Property__c objProperty = new Property__c();
    objProperty.Name = 'Test Project';
    objProperty.Property_Name__c = 'Test Property';
    objProperty.Property_ID__c = 3431;
    objProperty.CurrencyIsoCode = 'AED';
    insert objProperty;

    //create data for Inventory
    Inventory__c objInventory = new Inventory__c();
    objInventory.Property__c = objProperty.Id;
    objInventory.Unit__c = '1345';
    insert objInventory;

    //create Booking Units record for above created Bookings
    bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
    for(Booking_Unit__c objBookingUnit : bookingUnitList)
    {
      objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
      objBookingUnit.Inventory__c = objInventory.Id;
      objBookingUnit.Registration_ID__c = '74712';
      objBookingUnit.Re_Assigned__c = false;
      objBookingUnit.Property_Name__c = 'Test Property';
      objBookingUnit.Bedroom_Type__c = 'STD';
    }
    insert bookingUnitList;

    srBookingUnitList = TestDataFactory_CRM.createSRBookingUnis(objCase.Id , bookingUnitList);
    insert srBookingUnitList;

    insertParkingLimits();

    Test.startTest();

      String strParkingPaymentTerms = '[{"strInstallment":"1- INSTALLMENT","strDate":"14/12/2017","strAmount":"3333.33"},{"strInstallment":"2- INSTALLMENT","strDate":"15/12/2017","strAmount":"3333.33"},{"strInstallment":"3- INSTALLMENT","strDate":"16/12/2017","strAmount":"3333.33"}]';
      List<additionalParkingCREController.PaymentTermsWrapper> lstPaymentTerms 
      = (List<additionalParkingCREController.PaymentTermsWrapper>)JSON.deserialize(strParkingPaymentTerms,List<additionalParkingCREController.PaymentTermsWrapper>.class);

      String strParkingInfo = '[{"STATUS_CODE":"A","STATUS":"Available","PROPERTY_NAME":"DAMAC HILLS - ARTESIA","PROPERTY_ID":3491,"PRICE":8000,"PARKING_TYPE_DESC":"Visitor","PARKING_TYPE":"VISITOR","PARKING_BAY_NUMBER":null,"ORG_ID":81,"DETAIL_ID":88,"BUILDING_NAME":"ARTESIA - A","BUILDING_ID":64547,"blnIsSelected":true}]';
      List<AdditionalParkingDetailWrapper.Data> lstParkingInformation 
      = (List<AdditionalParkingDetailWrapper.Data>)JSON.deserialize(strParkingInfo,List<AdditionalParkingDetailWrapper.Data>.class);

      Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );

      AdditionalParkingUtility.generateParkingRequestOffer(objCase,lstPaymentTerms,lstParkingInformation,'Installment');
      AdditionalParkingUtility.generateAllocationParkingAcceptance(objCase);

      AdditionalParkingUtility.calculatePaymentTermsUtility(3,30000);

      AdditionalParkingUtility.checkParkingLimt(bookingUnitList[0]);

    Test.stopTest();
  }

  /* * * * * * * * * * * * *
  *  Method Name:  createParkingPaymentTerms_test
  *  Purpose:      This method is used to unit test functionality for createParkingPaymentTerms
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 28-Dec-2017
  * * * * * * * * * * * * */
  static testMethod void createParkingPaymentTerms_test()
  {
    List<Booking__c> bookingList = new List<Booking__c>();
    List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
    List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();

    //insert Booking Unit Active Status custom setting record
    insertBookingUnitActiveStatus();

    //insert sample data for Account
    Account objAccount = TestDataFactory_CRM.createPersonAccount();
    insert objAccount;

    Id parkingRecordTypeID = getRecordTypeIdForParking();

    Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , parkingRecordTypeID);
    objCase.Status = 'Submitted';
    objCase.POA_Expiry_Date__c = System.today();
    objCase.Approval_Status__c = 'Approved';
    objCase.Status = 'Submitted';
    objCase.IsPOA__c = true;
    objCase.Parking_Details_JSON__c = '[{"STATUS_CODE":"A","STATUS":"Available","PROPERTY_NAME":"DAMAC HILLS - ARTESIA","PROPERTY_ID":3491,"PRICE":8000,"PARKING_TYPE_DESC":"Visitor","PARKING_TYPE":"VISITOR","PARKING_BAY_NUMBER":null,"ORG_ID":81,"DETAIL_ID":88,"BUILDING_NAME":"ARTESIA - A","BUILDING_ID":64547,"blnIsSelected":true}]';
    objCase.Pending_Amount__c = 500;
    objCase.Parking_Type_Of_Payment__c = 'Full Payment';
    objCase.Parking_Total_Payment_Terms__c = 3;
    objCase.Parking_Payment_Mode__c = 'Cash Payment';
    objCase.Parking_Payment_Terms__c = '[{"strInstallment":"1-INSTALLMENT","strDate":"01/02/2018","strAmount":"5000"},{"strInstallment":"2-INSTALLMENT","strDate":"03/02/2018","strAmount":"5000"}]';
    insert objCase;

    //create Deal SR record
    NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
    insert objDealSR;

    //create Booking record for above created Deal and Account
    bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
    insert bookingList;

    //create Property data
    Property__c objProperty = new Property__c();
    objProperty.Name = 'Test Project';
    objProperty.Property_Name__c = 'Test Property';
    objProperty.Property_ID__c = 3431;
    objProperty.CurrencyIsoCode = 'AED';
    insert objProperty;

    //create data for Inventory
    Inventory__c objInventory = new Inventory__c();
    objInventory.Property__c = objProperty.Id;
    objInventory.Unit__c = '1345';
    insert objInventory;

    //create Booking Units record for above created Bookings
    bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
    for(Booking_Unit__c objBookingUnit : bookingUnitList)
    {
      objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
      objBookingUnit.Inventory__c = objInventory.Id;
      objBookingUnit.Registration_ID__c = '74712';
      objBookingUnit.Re_Assigned__c = false;
    }
    insert bookingUnitList;

    srBookingUnitList = TestDataFactory_CRM.createSRBookingUnis(objCase.Id , bookingUnitList);
    insert srBookingUnitList;

    Test.startTest();

      String strParkingPaymentTerms = '[{"strInstallment":"1- INSTALLMENT","strDate":"14/12/2017","strAmount":"3333.33"},{"strInstallment":"2- INSTALLMENT","strDate":"15/12/2017","strAmount":"3333.33"},{"strInstallment":"3- INSTALLMENT","strDate":"16/12/2017","strAmount":"3333.33"}]';
      List<additionalParkingCREController.PaymentTermsWrapper> lstPaymentTerms 
      = (List<additionalParkingCREController.PaymentTermsWrapper>)JSON.deserialize(strParkingPaymentTerms,List<additionalParkingCREController.PaymentTermsWrapper>.class);

      String strParkingInfo = '[{"STATUS_CODE":"A","STATUS":"Available","PROPERTY_NAME":"DAMAC HILLS - ARTESIA","PROPERTY_ID":3491,"PRICE":8000,"PARKING_TYPE_DESC":"Visitor","PARKING_TYPE":"VISITOR","PARKING_BAY_NUMBER":null,"ORG_ID":81,"DETAIL_ID":88,"BUILDING_NAME":"ARTESIA - A","BUILDING_ID":64547,"blnIsSelected":true}]';
      List<AdditionalParkingDetailWrapper.Data> lstParkingInformation 
      = (List<AdditionalParkingDetailWrapper.Data>)JSON.deserialize(strParkingInfo,List<AdditionalParkingDetailWrapper.Data>.class);

      Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );

      AdditionalParkingUtility.createParkingPaymentTerms(objCase.Id);

    Test.stopTest();
  }


  /* * * * * * * * * * * * *
  *  Method Name:  updateParkingStatusInIPMS_test
  *  Purpose:      This method is used to unit test functionality for updateParkingStatusInIPMS
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 01-Feb-2018
  * * * * * * * * * * * * */
  static testMethod void updateParkingStatusInIPMS_test()
  {
    List<Booking__c> bookingList = new List<Booking__c>();
    List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
    List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();

    //insert Booking Unit Active Status custom setting record
    insertBookingUnitActiveStatus();

    //insert sample data for Account
    Account objAccount = TestDataFactory_CRM.createPersonAccount();
    insert objAccount;

    Id parkingRecordTypeID = getRecordTypeIdForParking();

    Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , parkingRecordTypeID);
    objCase.Status = 'Submitted';
    objCase.POA_Expiry_Date__c = System.today();
    objCase.Approval_Status__c = 'Approved';
    objCase.Status = 'Submitted';
    objCase.IsPOA__c = true;
    objCase.Parking_Details_JSON__c = '[{"STATUS_CODE":"A","STATUS":"Available","PROPERTY_NAME":"DAMAC HILLS - ARTESIA","PROPERTY_ID":3491,"PRICE":8000,"PARKING_TYPE_DESC":"Visitor","PARKING_TYPE":"VISITOR","PARKING_BAY_NUMBER":null,"ORG_ID":81,"DETAIL_ID":88,"BUILDING_NAME":"ARTESIA - A","BUILDING_ID":64547,"blnIsSelected":true}]';
    objCase.Pending_Amount__c = 500;
    objCase.Parking_Type_Of_Payment__c = 'Full Payment';
    objCase.Parking_Total_Payment_Terms__c = 3;
    objCase.Parking_Payment_Mode__c = 'Cash Payment';
    objCase.Parking_Payment_Terms__c = '[{"strInstallment":"1-INSTALLMENT","strDate":"01/02/2018","strAmount":"5000"},{"strInstallment":"2-INSTALLMENT","strDate":"03/02/2018","strAmount":"5000"}]';
    insert objCase;

    //create Deal SR record
    NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
    insert objDealSR;

    //create Booking record for above created Deal and Account
    bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
    insert bookingList;

    //create Property data
    Property__c objProperty = new Property__c();
    objProperty.Name = 'Test Project';
    objProperty.Property_Name__c = 'Test Property';
    objProperty.Property_ID__c = 3431;
    objProperty.CurrencyIsoCode = 'AED';
    insert objProperty;

    //create data for Inventory
    Inventory__c objInventory = new Inventory__c();
    objInventory.Property__c = objProperty.Id;
    objInventory.Unit__c = '1345';
    insert objInventory;

    //create Booking Units record for above created Bookings
    bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
    for(Booking_Unit__c objBookingUnit : bookingUnitList)
    {
      objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
      objBookingUnit.Inventory__c = objInventory.Id;
      objBookingUnit.Registration_ID__c = '74712';
      objBookingUnit.Re_Assigned__c = false;
    }
    insert bookingUnitList;

    srBookingUnitList = TestDataFactory_CRM.createSRBookingUnis(objCase.Id , bookingUnitList);
    insert srBookingUnitList;

    Test.startTest();

      String strParkingPaymentTerms = '[{"strInstallment":"1- INSTALLMENT","strDate":"14/12/2017","strAmount":"3333.33"},{"strInstallment":"2- INSTALLMENT","strDate":"15/12/2017","strAmount":"3333.33"},{"strInstallment":"3- INSTALLMENT","strDate":"16/12/2017","strAmount":"3333.33"}]';
      List<additionalParkingCREController.PaymentTermsWrapper> lstPaymentTerms 
      = (List<additionalParkingCREController.PaymentTermsWrapper>)JSON.deserialize(strParkingPaymentTerms,List<additionalParkingCREController.PaymentTermsWrapper>.class);

      String strParkingInfo = '[{"STATUS_CODE":"A","STATUS":"Available","PROPERTY_NAME":"DAMAC HILLS - ARTESIA","PROPERTY_ID":3491,"PRICE":8000,"PARKING_TYPE_DESC":"Visitor","PARKING_TYPE":"VISITOR","PARKING_BAY_NUMBER":null,"ORG_ID":81,"DETAIL_ID":88,"BUILDING_NAME":"ARTESIA - A","BUILDING_ID":64547,"blnIsSelected":true}]';
      List<AdditionalParkingDetailWrapper.Data> lstParkingInformation 
      = (List<AdditionalParkingDetailWrapper.Data>)JSON.deserialize(strParkingInfo,List<AdditionalParkingDetailWrapper.Data>.class);

      Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );

      AdditionalParkingUtility.updateParkingStatusInIPMS(objCase.Id,strParkingInfo,'Submitted');
      AdditionalParkingUtility.updateParkingStatusInIPMS(objCase.Id,strParkingInfo,'Closed');
      AdditionalParkingUtility.updateParkingStatusInIPMS(objCase.Id,strParkingInfo,'Rejected');

      AdditionalParkingUtility.updateParkingStatusOnSRRejection(new List<Case>{objCase});

      AdditionalParkingUtility.validateParkingSelection(Decimal.valueOf('64547'),strParkingInfo);

      AdditionalParkingUtility.postParkingFeeToIPMS(objCase.Id);

    Test.stopTest();
  }


  /* * * * * * * * * * * * *
  *  Method Name:  getRecordTypeIdForParking
  *  Purpose:      This method is used to get record type ID for Parking
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 28-Dec-2017
  * * * * * * * * * * * * */
  private static Id getRecordTypeIdForParking()
  {
    Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
    Id parkingRecordTypeID = caseRecordTypes.get('Parking').getRecordTypeId();
    return parkingRecordTypeID;
  }

  /* * * * * * * * * * * * *
  *  Method Name:  insertBookingUnitActiveStatus
  *  Purpose:      This method is used to insert custom setting record for Booking Unit Active Status.
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 28-Dec-2017
  * * * * * * * * * * * * */
  private static void insertBookingUnitActiveStatus()
  {
    List<Booking_Unit_Active_Status__c> buActiveStatusList = new List<Booking_Unit_Active_Status__c>();

    Booking_Unit_Active_Status__c objBUActiveStatus = new Booking_Unit_Active_Status__c();
    objBUActiveStatus.Name = strBookingUnitActiveStatus;
    objBUActiveStatus.Status_Value__c = strBookingUnitActiveStatus;
    buActiveStatusList.add(objBUActiveStatus);

    insert buActiveStatusList;
  }

  /* * * * * * * * * * * * *
  *  Method Name:  insert#ParkingLimits
  *  Purpose:      This method is used to insert custom setting record for # Parking Limits.
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 28-Dec-2017
  * * * * * * * * * * * * */
  private static void insertParkingLimits()
  {
    List<Parking_Limit__c> lstParkingLimits = new List<Parking_Limit__c>();

    Parking_Limit__c objParkingLimit = new Parking_Limit__c();
    objParkingLimit.Name = 'BU-12345';
    objParkingLimit.parking_no_limit__c = 2;
    objParkingLimit.Bedroom_Type__c = 'STD';
    objParkingLimit.Building_Name__c = 'Test Property';
    objParkingLimit.Project_Name__c = 'Test Property';
    lstParkingLimits.add(objParkingLimit);

    insert lstParkingLimits;
  }
}