public without sharing class HandoverKeyReleaseNotification {
    
    @InvocableMethod
    public static void sendKeyHandoverReadinessNotification( list<Booking_Unit__c> lstUnits ) {
        system.debug(' == sendKeyHandoverReadinessNotification Method called ==');
        system.debug(' == lstUnits == '+lstUnits);
        if( lstUnits != NULL && !lstUnits.isEmpty() ) {
            Booking_Unit__c objUnit = [ SELECT Id
                                             , Inventory__r.Building_Location__r.Handover_Team_notification_Email__c
                                             , Inventory__r.Building_Location__r.FM_Email__c
                                             , Inventory__r.Building_Location__r.Loams_Email__c
                                             , Unit_Name__c
                                             , Property_Name_Inventory__c
                                             , Primary_Buyer_s_Name__c
                                             , Booking__r.Account__c                                             
                                          FROM Booking_Unit__c
                                         WHERE Id = :lstUnits[0].Id ];
            Id unitId = objUnit.Id;
            Set<Id> setCases = new Set<Id>();
            for (Case objCase : [Select Id
                                        , CaseNumber
                                        , RecordTypeId
                                        , RecordType.DeveloperName
                                        , Booking_Unit__c
                                        , Status
                                 From Case
                                 Where Booking_Unit__c =: unitId
                                 And ( RecordType.DeveloperName = 'Handover'
                                 OR RecordType.DeveloperName = 'Early_Handover')
                                 And (Status != 'Closed' OR Status != 'Rejeted'
                                 OR Status != 'Cancelled')]) {
                setCases.add(objCase.Id);            
            }
            
            list<SR_Attachments__c> lstAttach = new list<SR_Attachments__c>();
            if (setCases != null && setCases.size() > 0) {
                for (SR_Attachments__c objAttach : [Select Id
                                                           , Case__c
                                                           , Name
                                                           , Attachment_URL__c
                                                           , Type__c
                                                    From SR_Attachments__c
                                                    Where Case__c IN: setCases
                                                    And Attachment_URL__c != null]) {
                    lstAttach.add(objAttach);                                                    
                }
            }
            String urlDocuments;
            if (lstAttach  != null && lstAttach.size() > 0) {
                for (SR_Attachments__c objAttach : lstAttach) {
                    String docURL = '<a href="' + objAttach.Attachment_URL__c + '">' + objAttach.Name + '</a>';
                    if (string.isBlank(urlDocuments)) {
                        urlDocuments = '<ul><li>'+docURL+'</li></ul>';
                    } else {
                        urlDocuments = urlDocuments.remove('</ul>');
                        urlDocuments = urlDocuments + '<li>'+docURL+'</li></ul>';
                    }
                }
            }
            
            list<EmailTemplate> lstTemplates = [ SELECT Id
                                                      , DeveloperName
                                                      , Markup
                                                      , Body
                                                      , Subject
                                                      , HtmlValue                                                      
                                                   FROM EmailTemplate 
                                                  WHERE DeveloperName = 'Ready_for_Key_Release_Notification' ] ;
            string strBody = lstTemplates[0].Markup ;
            string strSubject = lstTemplates[0].Subject;
            string strDate = String.valueOf(System.Today());
            List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();

            if( objUnit != NULL && 
                String.isNotBlank( objUnit.Inventory__r.Building_Location__r.Handover_Team_notification_Email__c ) && 
                lstTemplates != NULL && !lstTemplates.isEmpty() ) {
                
                    list<String> lstToAddresses = new list<String>();
                    lstToAddresses.add(objUnit.Inventory__r.Building_Location__r.Handover_Team_notification_Email__c);
                    //lstToAddresses.add('lochank58@gmail.com');
                    list<String> lstBccAddresses = new list<String>();
                     if( String.isNotEmpty( objUnit.Inventory__r.Building_Location__r.FM_Email__c) ) {
                        lstBccAddresses.add( objUnit.Inventory__r.Building_Location__r.FM_Email__c);          
                    }
                    if( String.isNotEmpty( objUnit.Inventory__r.Building_Location__r.Loams_Email__c ) ) {
                        lstBccAddresses.add( objUnit.Inventory__r.Building_Location__r.Loams_Email__c );          
                    }
                    strSubject = strSubject + ' ' + objUnit.Unit_Name__c;
                     if (objUnit.Unit_Name__c!= null) {
                        strBody = strBody.replace('{!relatedTo.Unit_Name__c}', objUnit.Unit_Name__c);
                    } else {
                        strBody = strBody.replace('{!relatedTo.Unit_Name__c}', '');
                    }
                    if (objUnit.Property_Name_Inventory__c!= null) {
                        strBody = strBody.replace('{!relatedTo.Property_Name_Inventory__c}', objUnit.Property_Name_Inventory__c);
                    } else {
                        strBody = strBody.replace('{!relatedTo.Property_Name_Inventory__c}', '');
                    }
                    if (objUnit.Primary_Buyer_s_Name__c!= null) {
                        strBody = strBody.replace('{!relatedTo.Primary_Buyer_s_Name__c}', objUnit.Primary_Buyer_s_Name__c);
                    } else {
                        strBody = strBody.replace('{!relatedTo.Primary_Buyer_s_Name__c}', '');
                    }
                    if (objUnit.Id!= null) {
                        strBody = strBody.replace('{!relatedTo.Id}', objUnit.Id);
                    } else {
                        strBody = strBody.replace('{!relatedTo.Id}', '');
                    }
                    if (objUnit.Booking__r.Account__c!= null) {
                        strBody = strBody.replace('{!relatedTo.Booking__r.Account__c}', objUnit.Booking__r.Account__c);
                    } else {
                        strBody = strBody.replace('{!relatedTo.Booking__r.Account__c}', '');
                    }
                    if (!string.isBlank(urlDocuments)) {
                        strBody = strBody.replace('{!relatedTo.Handover_Notice_URLs__c}', urlDocuments);
                    } else {
                        strBody = strBody.replace('{!relatedTo.Handover_Notice_URLs__c}', '');
                    }
                    strBody = strBody.replace('<apex:outputText value="{0,date,dd-MMM-yyyy}"><apex:param value="{!TODAY()}"/></apex:outputText>', strDate);
                    system.debug('!!!!!!strBody'+strBody);
                    OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
                    Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
                    //String[] sendingTo = new String[]{'lochank58@gmail.com'};
                    semail.setToAddresses(lstToAddresses); 
                    if (lstBccAddresses != null) {                
                        semail.setBccAddresses(lstBccAddresses);
                    }
                    semail.setSubject(strSubject);
                    semail.setHTMLBody(strBody);
                    if ( owea.size() > 0 ) {
                        semail.setOrgWideEmailAddressId(owea.get(0).Id);
                    }
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail});
            }
        }
    }
    
}