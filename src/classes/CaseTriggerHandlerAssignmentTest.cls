/*
* Description - Test class developed for 'CaseTriggerHandlerAssignment'
*
* Version            Date            Author            Description
* 1.0                22/11/17        Rohit            Initial Draft
*/

@isTest
public class CaseTriggerHandlerAssignmentTest {
    public static testMethod void assignmentCaseOTP() {
         // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);

        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        insert objCase;

        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        list<Booking__c> listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(null, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        list<Buyer__c> listBuyer = TestDataFactory_CRM.createBuyer(listCreateBookingForAccount[0].Id, 1, null);
        insert listBuyer;

        OTP__c objOTP = TestDataFactory_CRM.createOTP(objCase.Id, '1234', '9856748596');
        insert objOTP;

        objCase.OTP_Number__c = '1234';
        objCase.Buyer__c = listBuyer[0].Id;
        update objCase;
    }

    public static testMethod void assignmentCaseTransferPerson() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;

        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();

        ServiceRequestAgentSharing.BYPASS_UPDATE_SR = true;
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        list<Booking__c> listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        insert listCreateBookingUnit;
        
        list<Booking__c> listCreateBookingForBuyer = TestDataFactory_CRM.createBookingForAccount(null, objDealSR.Id, 1);
        insert listCreateBookingForBuyer;

        list<Buyer__c> listBuyer = TestDataFactory_CRM.createBuyer(listCreateBookingForBuyer[0].Id, 1, null);
        listBuyer[0].Title__c = 'ENG.';
        listBuyer[0].Title_Arabic__c = '??????';
        listBuyer[0].First_Name__c = 'Test First';
        listBuyer[0].First_Name_Arabic__c = 'Test First Arabic';
        listBuyer[0].Last_Name__c = 'Test Last';
        listBuyer[0].Last_Name_Arabic__c = 'Test Last Arabic';
        listBuyer[0].Nationality__c = 'Indian';
        listBuyer[0].Nationality_Arabic__c = 'Indian';
        listBuyer[0].Buyer_Type__c = 'Individual';
        listBuyer[0].Passport_Number__c = 'TT-12345';
        listBuyer[0].Passport_Expiry_Date__c = '25/12/2055';
        listBuyer[0].Place_of_Issue__c = 'Pune';
        listBuyer[0].Place_of_Issue_Arabic__c = 'Pune';
        listBuyer[0].Address_Line_1__c = 'Street 1';
        listBuyer[0].Address_Line_1_Arabic__c = 'Street 1';
        listBuyer[0].Address_Line_2__c = 'Street 2';
        listBuyer[0].Address_Line_2_Arabic__c = 'Street 2';
        listBuyer[0].Address_Line_3__c = 'Street 3';
        listBuyer[0].Address_Line_3_Arabic__c = 'Street 3';
        listBuyer[0].Address_Line_4__c = 'Street 4';
        listBuyer[0].Address_Line_4_Arabic__c = 'Street 4';
        listBuyer[0].Email__c = 'test@test.com';
        listBuyer[0].Date_of_Birth__c = '25/12/1982';
        listBuyer[0].Phone__c = '9674858963';
        listBuyer[0].Country__c = 'India';
        listBuyer[0].Country_Arabic__c = 'India';
        listBuyer[0].City__c = 'Pune';
        listBuyer[0].City_Arabic__c = 'Pune';
        listBuyer[0].Phone_Country_Code__c = 'India: 0091';
        listBuyer[0].Primary_Buyer__c = true;
        insert listBuyer;

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objCase.Seller__c = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Buyer__c = listBuyer[0].Id;
        insert objCase;
        
        ServiceRequestAgentSharing.BYPASS_UPDATE_SR = true;
        objCase.status = 'Submitted';
        update objCase;
        
        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        test.StopTest();

        Case caseObj = [select AccountId from Case where Id =: objCase.Id];
        system.debug('caseObj=='+caseObj.AccountId); 
    }

    public static testMethod void assignmentCaseTransferBusiness() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;

        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();

        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        list<Booking__c> listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        insert listCreateBookingUnit;
        
        list<Booking__c> listCreateBookingForBuyer = TestDataFactory_CRM.createBookingForAccount(null, objDealSR.Id, 1);
        insert listCreateBookingForBuyer;

        list<Buyer__c> listBuyer = TestDataFactory_CRM.createBuyer(listCreateBookingForBuyer[0].Id, 1, null);
        listBuyer[0].Organisation_Name__c = 'DAMAC Group';
        listBuyer[0].Organisation_Name_Arabic__c = 'DAMAC Group';
        listBuyer[0].CR_Registration_Expiry_Date__c = '25/12/2055';
        listBuyer[0].CR_Number__c = 'CR-123';
        listBuyer[0].CR_Registration_Place__c = 'Pune';
        listBuyer[0].CR_Registration_Place__c = 'Pune';
        listBuyer[0].Title__c = 'ENG.';
        listBuyer[0].Title_Arabic__c = '??????';
        listBuyer[0].First_Name__c = 'Test First';
        listBuyer[0].First_Name_Arabic__c = 'Test First Arabic';
        listBuyer[0].Last_Name__c = 'Test Last';
        listBuyer[0].Last_Name_Arabic__c = 'Test Last Arabic';
        listBuyer[0].Nationality__c = 'Indian';
        listBuyer[0].Nationality_Arabic__c = 'Indian';
        listBuyer[0].Buyer_Type__c = 'Corporate';
        listBuyer[0].Passport_Number__c = 'TT-12345';
        listBuyer[0].Passport_Expiry_Date__c = '25/12/2055';
        listBuyer[0].Place_of_Issue__c = 'Pune';
        listBuyer[0].Place_of_Issue_Arabic__c = 'Pune';
        listBuyer[0].Address_Line_1__c = 'Street 1';
        listBuyer[0].Address_Line_1_Arabic__c = 'Street 1';
        listBuyer[0].Address_Line_2__c = 'Street 2';
        listBuyer[0].Address_Line_2_Arabic__c = 'Street 2';
        listBuyer[0].Address_Line_3__c = 'Street 3';
        listBuyer[0].Address_Line_3_Arabic__c = 'Street 3';
        listBuyer[0].Address_Line_4__c = 'Street 4';
        listBuyer[0].Address_Line_4_Arabic__c = 'Street 4';
        listBuyer[0].Email__c = 'test@test.com';
        listBuyer[0].Date_of_Birth__c = '25/12/1982';
        listBuyer[0].Phone__c = '9674858963';
        listBuyer[0].Country__c = 'India';
        listBuyer[0].Country_Arabic__c = 'India';
        listBuyer[0].City__c = 'Pune';
        listBuyer[0].City_Arabic__c = 'Pune';
        listBuyer[0].Phone_Country_Code__c = 'India: 0091';
        listBuyer[0].Primary_Buyer__c = true;
        insert listBuyer;

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objCase.Seller__c = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Buyer__c = listBuyer[0].Id;
        insert objCase;
        Test.StartTest();
        
        objCase.status = 'Submitted';
        update objCase;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        test.StopTest();

        Case caseObj = [select AccountId from Case where Id =: objCase.Id];
        system.debug('caseObj=='+caseObj.AccountId); 
    }
    
    public static testMethod void assignmentCaseClosure() {
        Account objPerAcc = TestDataFactory_CRM.createPersonAccount();
        insert objPerAcc ;

        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();

        Property__c objProperty = TestDataFactory_CRM.createProperty();
        insert objProperty;

        Inventory__c objInventory = TestDataFactory_CRM.createInventory(objProperty.Id);
        insert objInventory;

        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        list<Booking__c> listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(objPerAcc.Id, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        listCreateBookingUnit[0].Registration_ID__c = '854746';
        listCreateBookingUnit[0].Inventory__c = objInventory.Id;
        listCreateBookingUnit[0].Booking_Unit_Type__c = 'One Bedroom';
        insert listCreateBookingUnit;

        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        objAcc.Name = 'DAMAC Group';
        objAcc.Name_Arabic__c = 'DAMAC Group';
        objAcc.CR_Registration_Expiry_Date__c = date.newInstance(2055,12,25);
        objAcc.CR_Number__c = 'CR-123';
        objAcc.CR_Registration_Place__c = 'Pune';
        objAcc.CR_Registration_Place_Arabic__c = 'Pune';
        objAcc.Title__c = 'ENG.';
        objAcc.Title_Arabic__c  = '??????';
        objAcc.First_Name__c = 'Test First';
        objAcc.First_Name_Arabic__c = 'Test First Arabic';
        objAcc.Last_Name__c = 'Test Last';
        objAcc.Last_Name_Arabic__c = 'Test Last Arabic';
        objAcc.Nationality__c = 'Indian';
        objAcc.Nationality_Arabic__c = 'Indian';
        //objAcc.Buyer_Type__c = 'Individual';
        objAcc.Passport_Number__c  = 'TT-12345';
        objAcc.Passport_Expiry_Date__c = date.newInstance(2055,12,25);
        objAcc.Passport_Issue_Place__c = 'Pune';
        objAcc.Passport_Issue_Place_Arabic__c = 'Pune';
        objAcc.Address_Line_1__c = 'Street 1';
        objAcc.Address_Line_1_Arabic__c = 'Street 1';
        objAcc.Address_Line_2__c = 'Street 2';
        objAcc.Address_Line_2_Arabic__c = 'Street 2';
        objAcc.Address_Line_3__c = 'Street 3';
        objAcc.Address_Line_3_Arabic__c = 'Street 3';
        objAcc.Address_Line_4__c = 'Street 4';
        objAcc.Address_Line_4_Arabic__c = 'Street 4';
        objAcc.Email__c = 'test@test.com';
        objAcc.Date_Of_Birth__c = date.newInstance(1982,12,25);
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Encrypt__c = '9674858963';
        objAcc.Country__c = 'India';
        objAcc.Country_Arabic__c = 'India';
        objAcc.City__c = 'Pune';
        objAcc.City_Arabic__c = 'Pune';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Party_ID__c = '1265589';
        insert objAcc;

        system.debug('objAcc RecordType=='+objAcc.RecordTypeId);

        list<Buyer__c> listBuyer = TestDataFactory_CRM.createBuyer(listCreateBookingForAccount[0].Id, 1, null);
        listBuyer[0].Organisation_Name__c = 'DAMAC Group';
        listBuyer[0].Organisation_Name_Arabic__c = 'DAMAC Group';
        listBuyer[0].CR_Registration_Expiry_Date__c = '25/12/2055';
        listBuyer[0].CR_Number__c = 'CR-123';
        listBuyer[0].CR_Registration_Place__c = 'Pune';
        listBuyer[0].CR_Registration_Place__c = 'Pune';
        listBuyer[0].Title__c = 'ENG.';
        listBuyer[0].Title_Arabic__c = '??????';
        listBuyer[0].First_Name__c = 'Test First';
        listBuyer[0].First_Name_Arabic__c = 'Test First Arabic';
        listBuyer[0].Last_Name__c = 'Test Last';
        listBuyer[0].Last_Name_Arabic__c = 'Test Last Arabic';
        listBuyer[0].Nationality__c = 'Indian';
        listBuyer[0].Nationality_Arabic__c = 'Indian';
        listBuyer[0].Buyer_Type__c = 'Corporate';
        listBuyer[0].Passport_Number__c = 'TT-12345';
        listBuyer[0].Passport_Expiry_Date__c = '25/12/2055';
        listBuyer[0].Place_of_Issue__c = 'Pune';
        listBuyer[0].Place_of_Issue_Arabic__c = 'Pune';
        listBuyer[0].Address_Line_1__c = 'Street 1';
        listBuyer[0].Address_Line_1_Arabic__c = 'Street 1';
        listBuyer[0].Address_Line_2__c = 'Street 2';
        listBuyer[0].Address_Line_2_Arabic__c = 'Street 2';
        listBuyer[0].Address_Line_3__c = 'Street 3';
        listBuyer[0].Address_Line_3_Arabic__c = 'Street 3';
        listBuyer[0].Address_Line_4__c = 'Street 4';
        listBuyer[0].Address_Line_4_Arabic__c = 'Street 4';
        listBuyer[0].Email__c = 'test@test.com';
        listBuyer[0].Date_of_Birth__c = '25/12/1982';
        listBuyer[0].Phone__c = '9674858963';
        listBuyer[0].Country__c = 'India';
        listBuyer[0].Country_Arabic__c = 'India';
        listBuyer[0].City__c = 'Pune';
        listBuyer[0].City_Arabic__c = 'Pune';
        listBuyer[0].Phone_Country_Code__c = 'India: 0091';
        listBuyer[0].Primary_Buyer__c = true;
        listBuyer[0].Party_ID__c = '1265589';
        listBuyer[0].Account__c = objAcc.Id;
        insert listBuyer;

        Case objBusCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objBusCase.Seller__c = objPerAcc.Id;
        objBusCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objBusCase.Buyer__c = listBuyer[0].Id;
        objBusCase.AccountId = objAcc.Id;
        insert objBusCase;

        objBusCase.status = 'Closed';
        update objBusCase;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        test.StopTest(); 
    }
    
    public static testMethod void assignmentCompleted() {
        Account objPerAcc = TestDataFactory_CRM.createPersonAccount();
        insert objPerAcc ;

        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();

        Property__c objProperty = TestDataFactory_CRM.createProperty();
        insert objProperty;

        Inventory__c objInventory = TestDataFactory_CRM.createInventory(objProperty.Id);
        insert objInventory;

        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        list<Booking__c> listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(objPerAcc.Id, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        listCreateBookingUnit[0].Registration_ID__c = '854746';
        listCreateBookingUnit[0].Inventory__c = objInventory.Id;
        listCreateBookingUnit[0].Booking_Unit_Type__c = 'One Bedroom';
        insert listCreateBookingUnit;

        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        objAcc.Name = 'DAMAC Group';
        objAcc.Name_Arabic__c = 'DAMAC Group';
        objAcc.CR_Registration_Expiry_Date__c = date.newInstance(2055,12,25);
        objAcc.CR_Number__c = 'CR-123';
        objAcc.CR_Registration_Place__c = 'Pune';
        objAcc.CR_Registration_Place_Arabic__c = 'Pune';
        objAcc.Title__c = 'ENG.';
        objAcc.Title_Arabic__c  = '??????';
        objAcc.First_Name__c = 'Test First';
        objAcc.First_Name_Arabic__c = 'Test First Arabic';
        objAcc.Last_Name__c = 'Test Last';
        objAcc.Last_Name_Arabic__c = 'Test Last Arabic';
        objAcc.Nationality__c = 'Indian';
        objAcc.Nationality_Arabic__c = 'Indian';
        //objAcc.Buyer_Type__c = 'Individual';
        objAcc.Passport_Number__c  = 'TT-12345';
        objAcc.Passport_Expiry_Date__c = date.newInstance(2055,12,25);
        objAcc.Passport_Issue_Place__c = 'Pune';
        objAcc.Passport_Issue_Place_Arabic__c = 'Pune';
        objAcc.Address_Line_1__c = 'Street 1';
        objAcc.Address_Line_1_Arabic__c = 'Street 1';
        objAcc.Address_Line_2__c = 'Street 2';
        objAcc.Address_Line_2_Arabic__c = 'Street 2';
        objAcc.Address_Line_3__c = 'Street 3';
        objAcc.Address_Line_3_Arabic__c = 'Street 3';
        objAcc.Address_Line_4__c = 'Street 4';
        objAcc.Address_Line_4_Arabic__c = 'Street 4';
        objAcc.Email__c = 'test@test.com';
        objAcc.Date_Of_Birth__c = date.newInstance(1982,12,25);
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Encrypt__c = '9674858963';
        objAcc.Country__c = 'India';
        objAcc.Country_Arabic__c = 'India';
        objAcc.City__c = 'Pune';
        objAcc.City_Arabic__c = 'Pune';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Party_ID__c = '1265589';
        insert objAcc;

        system.debug('objAcc RecordType=='+objAcc.RecordTypeId);

        list<Buyer__c> listBuyer = TestDataFactory_CRM.createBuyer(listCreateBookingForAccount[0].Id, 1, null);
        listBuyer[0].Organisation_Name__c = 'DAMAC Group';
        listBuyer[0].Organisation_Name_Arabic__c = 'DAMAC Group';
        listBuyer[0].CR_Registration_Expiry_Date__c = '25/12/2055';
        listBuyer[0].CR_Number__c = 'CR-123';
        listBuyer[0].CR_Registration_Place__c = 'Pune';
        listBuyer[0].CR_Registration_Place__c = 'Pune';
        listBuyer[0].Title__c = 'ENG.';
        listBuyer[0].Title_Arabic__c = '??????';
        listBuyer[0].First_Name__c = 'Test First';
        listBuyer[0].First_Name_Arabic__c = 'Test First Arabic';
        listBuyer[0].Last_Name__c = 'Test Last';
        listBuyer[0].Last_Name_Arabic__c = 'Test Last Arabic';
        listBuyer[0].Nationality__c = 'Indian';
        listBuyer[0].Nationality_Arabic__c = 'Indian';
        listBuyer[0].Buyer_Type__c = 'Corporate';
        listBuyer[0].Passport_Number__c = 'TT-12345';
        listBuyer[0].Passport_Expiry_Date__c = '25/12/2055';
        listBuyer[0].Place_of_Issue__c = 'Pune';
        listBuyer[0].Place_of_Issue_Arabic__c = 'Pune';
        listBuyer[0].Address_Line_1__c = 'Street 1';
        listBuyer[0].Address_Line_1_Arabic__c = 'Street 1';
        listBuyer[0].Address_Line_2__c = 'Street 2';
        listBuyer[0].Address_Line_2_Arabic__c = 'Street 2';
        listBuyer[0].Address_Line_3__c = 'Street 3';
        listBuyer[0].Address_Line_3_Arabic__c = 'Street 3';
        listBuyer[0].Address_Line_4__c = 'Street 4';
        listBuyer[0].Address_Line_4_Arabic__c = 'Street 4';
        listBuyer[0].Email__c = 'test@test.com';
        listBuyer[0].Date_of_Birth__c = '25/12/1982';
        listBuyer[0].Phone__c = '9674858963';
        listBuyer[0].Country__c = 'India';
        listBuyer[0].Country_Arabic__c = 'India';
        listBuyer[0].City__c = 'Pune';
        listBuyer[0].City_Arabic__c = 'Pune';
        listBuyer[0].Phone_Country_Code__c = 'India: 0091';
        listBuyer[0].Primary_Buyer__c = true;
        listBuyer[0].Party_ID__c = '1265589';
        listBuyer[0].Account__c = objAcc.Id;
        insert listBuyer;

        Case objBusCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objBusCase.Seller__c = objPerAcc.Id;
        objBusCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objBusCase.Buyer__c = listBuyer[0].Id;
        objBusCase.AccountId = objAcc.Id;
        insert objBusCase;

        objBusCase.status = 'Closed';
        update objBusCase;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        test.StopTest(); 
    }
        public static testMethod void createNewRegId(){
            Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc;
            NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
            insert objSR ;
            
            Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Inventory__c objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
            
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings;
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        insert lstBookingUnits;
        
        List<Booking_Unit__c> lstBookingUnits1 = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits1[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits1[0].Previous_Booking_Unit__c = lstBookingUnits[0].Id;
        lstBookingUnits1[0].Inventory__c = objInv.Id;
        lstBookingUnits1[0].Registration_Id__c = null;
        lstBookingUnits1[0].Area__c = 35;
        insert lstBookingUnits1;
        
        list<Buyer__c> lstJB = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 2, objAcc.Id);
        insert lstJB;
        
        Case objCase = new Case();
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        objCase.Origin = 'Walk-In';
        objCase.Buyer__c = lstJB[0].Id;
        objCase.Seller__c = objAcc.Id;
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;
        
        objCase.Buyer_Verified__c = true;
        update objCase;
    }
    
    public static testMethod void generateErrors(){
        list<Error_Log__c> lstERLog = new list<Error_Log__c>();
        Error_Log__c erLog = CaseTriggerHandlerAssignment.createErrorLogRecord(null,null,null);
        lstErLog.add(erLog);
        CaseTriggerHandlerAssignment.insertErrorLog(lstErLog);
    }
    
    public static testMethod void spaExecution(){
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc;
            NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
            insert objSR ;
            
            Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Inventory__c objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
            
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings;
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        insert lstBookingUnits;
        
        List<Booking_Unit__c> lstBookingUnits1 = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits1[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits1[0].Previous_Booking_Unit__c = lstBookingUnits[0].Id;
        lstBookingUnits1[0].Inventory__c = objInv.Id;
        lstBookingUnits1[0].Registration_Id__c = null;
        lstBookingUnits1[0].Area__c = 35;
        insert lstBookingUnits1;
        
        list<Buyer__c> lstJB = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 2, objAcc.Id);
        insert lstJB;
        
        Case objCase = new Case();
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        objCase.Origin = 'Walk-In';
        objCase.Buyer__c = lstJB[0].Id;
        objCase.Seller__c = objAcc.Id;
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.New_Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;
        
        objCase.Buyer_Verified__c = true;
        objCase.SPA_Execution__c = true;
        update objCase;
    }
}