public without sharing class UtillityForUpload {
    public Static String strOfcURl ;
    public Static String getOfficeURL (String Url ,String fileName){
        
        Blob pdfBlob ;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(Url);
        req.setHeader('Accept', 'application/json');
        req.setMethod('GET');
        //req.setHeader('Authorization','Bearer' + accessToken);
        req.setTimeout(120000);
        HttpResponse response = (!Test.isRunningTest())?new Http().send(req) :NULL;
        //System.debug('Header='+ response.getHeaderKeys());
       
        if(response != NULL &&response.getStatusCode() == 200) {
             
                System.debug('Pdf Body = ' + response.getBodyAsBlob());
            pdfBlob =  response.getBodyAsBlob();
        }
        if(Test.isRunningTest()){
            pdfBlob = Blob.valueOf('Test Data') ;  
        }

        


        UploadMultipleDocController.data respObj = new UploadMultipleDocController.data();
        List<UploadMultipleDocController.MultipleDocRequest> lstReq = new List<UploadMultipleDocController.MultipleDocRequest>();
        UploadMultipleDocController.MultipleDocRequest reqObjDoc = new UploadMultipleDocController.MultipleDocRequest();
        reqObjDoc.category = 'Document';
        reqObjDoc.entityName = 'Damac Service Requests';
        reqObjDoc.base64Binary = EncodingUtil.base64Encode(EncodingUtil.base64Decode(EncodingUtil.base64Encode(pdfBlob)));
        reqObjDoc.fileDescription = 'Handover Pack';
        reqObjDoc.fileId = fileName;
        reqObjDoc.fileName = fileName;
        reqObjDoc.registrationId = '';
        reqObjDoc.sourceFileName =  fileName;
        reqObjDoc.sourceId = fileName;
        lstReq.add(reqObjDoc);

        if( lstReq != null && lstReq.Size()>0 ){
            UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();
            MultipleDocData = UploadMultipleDocController.getMultipleDocUrl(lstReq);
            if(MultipleDocData != NULL){
                for(integer objInt=0; objInt<lstReq.Size(); objInt++ ) {
                    UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[objInt];
                    if(objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)) {
                        strOfcURl = objData.url;
                        
                    }
                }
            }
            

        }

        System.debug('strOfcURl=='+ strOfcURl);
        return strOfcURl;

    }

}

//System.debug(UtillityForUpload.getOfficeURL('https://damacholding.my.salesforce.com/sfc/p/#0Y000000ZkVQ/a/1n0000007eU9/rIwwZ135k.rnS8RZBIvdi3R1UIn.5My4hdeCgRuQbmA','test.pdf');