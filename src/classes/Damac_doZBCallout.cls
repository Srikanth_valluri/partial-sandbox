public class Damac_doZBCallout {
    @InvocableMethod
    public static void doCallout (List <ID> inquiryIds) {
        List <ID> inqIds = new List <ID> ();
        Set <ID> inqIdSetForSMSValidation = new Set <ID> ();
        List <ID> phone2InqIds = new List <ID> ();
        
        List <ID> email2InqIds = new List <ID> ();
            
        for (Inquiry__c inq: [SELECT Mobile_Phone__c, Mobile_Phone_2__c, Email_2__c, Email__c,
                            BVEmailStatus__c,BVSecondaryStatus__c,ZB_Email2_Status__c,ZB_Secondary_Status__c
                            FROM Inquiry__c WHERE ID IN :inquiryIds and Has_show_task__c = TRUE])
        {
            
            if (inq.Mobile_Phone__c != null) {
                inqIdSetForSMSValidation.add (inq.Id);
            }
            if (inq.Mobile_Phone_2__c != null) {
                phone2InqIds.add (inq.Id);
            }
            //if (inq.BVEmailStatus__c == null && inq.BVSecondaryStatus__c == null) {
            if (inq.Email__c != null) {
                inqIds.add (inq.id);
            }
            //if (inq.ZB_Email2_Status__c == null && inq.ZB_Secondary_Status__c == null) {
            if (inq.Email_2__c != null) {
                email2InqIds.add (inq.id);
            }
        }
        if (inqIds.size () > 0 && !System.isFuture() && !System.isBatch()) {
            InquiryTriggerHandler_New.doZBCallout (inqIds, 'Email__c');
        }
        if (email2InqIds.size () > 0 && !System.isFuture() && !System.isBatch()) {
            InquiryTriggerHandler_New.doZBCallout (email2InqIds, 'Email_2__c');
        }
        if (inqIdSetForSMSValidation.size () > 0 && !System.isFuture() && !System.isBatch()) {
            Damac_sendSMS.sendSecureMessage(inqIdSetForSMSValidation, 'Inquiry__c');
        }
        if (phone2InqIds.size () > 0 && !System.isFuture() && !System.isBatch()) {
            InquiryTriggerHandler_New.sendSMSMobile2 (phone2InqIds);
        }
    }
}