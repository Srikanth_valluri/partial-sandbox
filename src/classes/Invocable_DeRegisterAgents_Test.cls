@isTest
public class Invocable_DeRegisterAgents_Test {
    
    @isTest static void ccAgentRegistered() {
        
        Account newAcc ;
        List<Account> accList = new List<Account>();
        for(Integer i=0;i<10;i++){
            
            newAcc = new Account();
            newAcc.name = 'TestCCDeReg' + i;
            accList.add(newAcc);
            
        }
        insert accList;
        Contact newCon;
        List<Contact> conList = new List<Contact>();
        List<Id> conIdList = new List<Id>();
        for(Integer i=0;i<accList.Size();i++){
            newCon = new Contact();
            newCon.FirstName = 'FirstName' + i;
            newCon.LastName = 'LastName' + i;
            newCon.Email = 'TestCCDeReg' + i + '@gmail.com';
            newCon.Accountid = accList[i].Id;
            conIdList.add(newCon.Id);
            conList.add(newCon);
        }
        insert conList;
        
        Account agency1 = InitialiseTestData.getCorporateAccount('Agency1256');
        insert agency1; 
        
        Contact newCont = new Contact();
        newCont.LastName = 'Agency1256';
        newCont.AccountId = agency1.Id ;
        insert newCont ; 
        
        List<NSIBPM__Service_Request__c> SRList = InitialiseTestData.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
            new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration'))});
        
        
        NSIBPM__Service_Request__c serviceRequestObject = SRList[0] ; 
        serviceRequestObject.NSIBPM__Customer__c = agency1.Id ;
        serviceRequestObject.Last_Name__c = 'test User' ;
        update serviceRequestObject ; 
        
        Amendment__c newAmd;
        List<Amendment__c> amdList = new List<Amendment__c>();
        for(integer i=0;i<conList.Size();i++){
            newAmd =  new Amendment__c();
            newAmd.Account__c = conList[0].AccountId;
            newAmd.Contact__c = conList[0].Id;
            if(i==0){
                newAmd.Owner__c = true;
                newAmd.Shareholding__c =100;
            }
            newAmd.Authorised_Signatory__c = true;
            newAmd.Agent_Representative__c =true;
            newAmd.Portal_Administrator__c = true;
            newAmd.Email__c ='TestCCDeReg'+i+'@test.com';
            newAmd.ID_Expiry_Date__c = System.Today().addDays(100);
            newAmd.ID_Type__c = 'Visa';
            newAmd.Mark_For_Deletion__c = true;
            newAmd.Service_Request__c = SRList[0].Id ;
            amdList.add(newAmd);
        }
        insert amdList;
        
        List<New_Step__c> createStepList = new List<New_Step__c>();
        createStepList.add(new New_Step__c(Service_Request__c = SRList[0].id, Step_No__c  = 1, Step_Type__c = 'Agent Executive Review', Step_Status__c = 'Awaiting Additional Info'));
        createStepList.add(new New_Step__c(Service_Request__c = SRList[0].id, Step_No__c = 2, Step_Type__c  = 'Agreement Signing', Step_Status__c = 'More Info Updated'));
        
        
        
        insert createStepList;
        
        
        List<Id> stepIdList = new List<Id>();
        stepIdList.add(createStepList[0].Id);
        
        Invocable_DeRegisterAgents.deregisterAgents(stepIdList);
        
    }
}