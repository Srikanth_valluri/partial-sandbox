/*
Test Class : FmTaskInvocableClassTest
*/
public without sharing class FM_PushTaskToIPMS{
    public string pushTaskToIpms(list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5> listObjBeans){
        TaskCreationWSDL.TaskHttpSoap11Endpoint objClass = new TaskCreationWSDL.TaskHttpSoap11Endpoint();
        objClass.timeout_x = 120000;
        String response = objClass.SRDataToIPMSMultiple( '2-'+string.valueOf(System.currentTimeMillis()), 'CREATE_SR', 'SFDC', listObjBeans);
        system.debug('resp*****'+response);
        if(String.isNotBlank(response)
        && !response.contains('SOAException')){
            innerClass IC = (innerClass)JSON.deserialize(response, innerClass.class);
            system.debug('IC*****'+IC);
            if(IC.status.EqualsIgnoreCase('S')){
                return 'Success';
            }else{
                return IC.status;
            } 
        }else{
            if(response.contains('SOAException')){
                return response;
            }else{
                return 'Error : No Response from IPMS for Task Creation';
            }
        }  
    }
    
    public class innerClass{
        public string message;
        public string status;
        
        public innerClass(){
            
        }
    }

    /*
    public static void insertErrorLog(list<Error_Log__c> listObjErr){
        try{
            insert listObjErr;
        }catch(Exception ex){
            //errorMessage = 'Error : '+ ex.getMessage();
            system.debug('Error Log ex*****'+ex);
        }
    }
    */

    public static Error_Log__c createErrorLogRecord(Id accId, Id bookingUnitId, Id caseId){
        Error_Log__c objErr = new Error_Log__c();
        objErr.Account__c = accId;
        objErr.Booking_Unit__c = bookingUnitId;
        objErr.FM_Case__c = caseId;
        return objErr;
    }
}