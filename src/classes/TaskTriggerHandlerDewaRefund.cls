public with sharing class TaskTriggerHandlerDewaRefund {
    public static  boolean firstRun = true;
    public void validationForRefundAmount (List<Task> listTask) {
        Set<Id> caseIds = new Set<Id>();       
        for(Task taskObj : listTask){
            if((taskObj.Status == 'Completed' || taskObj.Status == 'Closed') && (taskObj.WhatId != Null)){
                caseIds.add(taskObj.WhatId);
            }
        }
        System.debug('Case Ids--->' +caseIds);
         if(!caseIds.isEmpty()){
           List<Case>  caseList =  [Select Id,Refund_Amount__c,Dispatch_Status__c FROM Case where Id IN: caseIds];
             if(caseList != Null && caseList.size() > 0) {
                 Set<Id> refundAmountNullCaseIds = new Set<Id>();
                 Set<Id> dewaIssueResovedNullCaseIds = new Set<Id>();
                  for(Case caseObj : caseList){               
                    if(caseObj.Refund_Amount__c == null){
                        refundAmountNullCaseIds.add(caseObj.Id);
                    }
                    if(caseObj.Dispatch_Status__c == null){
                        dewaIssueResovedNullCaseIds.add(caseObj.Id);
                    }
                }
                for(Task objTask : listTask){
                    if(refundAmountNullCaseIds.contains(objTask.WhatId) && (objTask.Subject == System.Label.Calculate_Verify_Refund_Amount || objTask.Subject == System.Label.Calculate_Verify_Refund_Amount_Bills_Unpaid) ){
                        objTask.addError('Please enter refund amount before closing this task');
                    }
                    if(dewaIssueResovedNullCaseIds.contains(objTask.WhatId) && objTask.Subject == System.Label.Inform_Customer_And_Provide_Next_Month_DEWA_Bill ){
                        objTask.addError('Please select if DEWA issue is resolved');
                    }
                }                 
             }               
         }        
    }
    
    public void uploadDocsValidation(List<Task> listTask) {
        Set<Id> caseIds = new Set<Id>();       
        for(Task taskObj : listTask){
            if((taskObj.Subject == System.Label.Attach_DEWA_Ejari_and_Bank_Documents || 
            taskObj.Subject == System.Label.Attach_DEWA_Bills_and_Ejari_Bank_Documents ||
            taskObj.Subject == System.Label.Inform_Customer_And_Provide_Next_Month_DEWA_Bill) 
            && (taskObj.Status == 'Completed' || taskObj.Status == 'Closed')){
                caseIds.add(taskObj.WhatId);
            }
        }
        System.debug('Case Ids--->' +caseIds);
         if(!caseIds.isEmpty()){
             list<SR_Attachments__c> listDocument = new list<SR_Attachments__c>();
                
                listDocument = [Select Id
                                From SR_Attachments__c
                                Where Case__c IN :caseIds
                                And (Attachment_URL__c = ''
                                    OR Attachment_URL__c = NULL)
                               ];
                if (!listDocument.isEmpty()) {
                    if(!Test.isRunningTest()) {
                        listTask[0].addError('Please Upload Documents on placeholders of DEWA Refund Case.');
                    }
                } 
              
         }
        
    }
    
    public void checkDEWATaskCompletion(map<Id, Task> mapNewTask, map<Id, Task> mapOldTask) {
        Map<Id,List<Task>> mapOfCaseToListTask = new Map<Id,List<Task>>();
        Set<Id> caseIds = new Set<Id>();
        List<Case> updateCaseStatusLst = new List<Case>();
        List<Task> taskListToInsert = new List<Task>();
             for(Task taskObj : mapNewTask.values()){
                     System.debug('taskObjInside'+taskObj);
                    if((taskObj.Status == 'Completed' || taskObj.Status == 'Closed') && (taskObj.WhatId != Null) 
                    && (taskObj.Status != mapOldTask.get(taskObj.Id).Status) && (taskObj.Subject == System.Label.Book_Refund_Amount_As_Per_Inputs_From_Claim
                    || taskObj.Subject == System.Label.Pay_Customer_Disburse_Cheque_Fund_Transfer_DEWA)){
                    System.debug('taskObj>>'+taskObj);
                        caseIds.add(taskObj.WhatId);
                    }
                }
                for(Task objTask : [Select Id,
                                            Subject,
                                            Status,
                                            WhatId
                                    FROM Task
                                    Where WhatId IN:caseIds
                                    AND (Subject =: Label.Book_Refund_Amount_As_Per_Inputs_From_Claim OR Subject =: Label.Pay_Customer_Disburse_Cheque_Fund_Transfer_DEWA)]) {
                    if(objTask.Status == 'Completed' || objTask.Status == 'Closed'){
                        if(mapOfCaseToListTask.containsKey(objTask.WhatId) && mapOfCaseToListTask.get(objTask.WhatId) != Null && mapOfCaseToListTask.get(objTask.WhatId).size() > 0) {
                            List<Task> taskList = mapOfCaseToListTask.get(objTask.WhatId);
                            taskList.add(objTask);
                            mapOfCaseToListTask.put(objTask.WhatId,taskList);
                        }
                        else {
                            List<Task> taskList = new list<Task>();
                            taskList.add(objTask);
                            mapOfCaseToListTask.put(objTask.WhatId,taskList);
                        }
                        
                        
                    }
               }                    
                System.debug('mapOfCaseToListTask>>'+mapOfCaseToListTask);
                System.debug('caseIds>>'+caseIds);
                for(Case objCase : [Select Id,
                                           Status,
                                           OwnerId,
                                           CurrencyIsoCode
                                    from Case where Id IN:caseIds]) {
                                    if(mapOfCaseToListTask.containsKey(objCase.Id) && 
                                       mapOfCaseToListTask.get(objCase.Id) != Null && 
                                       mapOfCaseToListTask.get(objCase.Id).size() > 0 && mapOfCaseToListTask.get(objCase.Id).size() == 2) {
                                       System.debug('caseList>>'+objCase.Id);
                                       //objCase.Status = 'Closed';
                                       //updateCaseStatusLst.add(objCase);
                                         Task objTask = new Task();
                                        objTask.Subject = System.Label.Inform_Customer_to_pick_up_cheque;
                                        objTask.Assigned_User__c = 'DLP Team';
                                        objTask.ActivityDate = Date.today()+2;
                                        objTask.OwnerId = objCase.OwnerId;
                                        objTask.Priority = 'Normal';
                                        objTask.CurrencyIsoCode = objCase.CurrencyIsoCode;
                                        objTask.Status = 'Not Started';
                                        objTask.WhatId = objCase.Id;
                                        taskListToInsert.add(objTask);
                                    }
                }
                System.debug('Before Update>>'+updateCaseStatusLst);
                /*if(updateCaseStatusLst != Null && updateCaseStatusLst.size() > 0) {
                    update updateCaseStatusLst;
                    System.debug('After Update>>'+updateCaseStatusLst);
                }*/
                if(taskListToInsert != Null && taskListToInsert.size() > 0) {
                    insert taskListToInsert;
                    System.debug('After Insert Task>>'+taskListToInsert);
                }

    }
}