@isTest(SeeAllData=true)
//@isTest
public class Damac_ziwo_listener_Test {
    Static testMethod void testMethod1() {  
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@test.com',
            Username = 'puser000@test.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            Ziwo_DID__c = 'test'
        );
        insert u;
        
        id recTypeid = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        Inquiry__c inq = new Inquiry__c ();
        inq.Is_Meeting_scheduled__c = false;
        inq.RecordTypeId = recTypeid;
        inq.ownerId = u.Id;
        insert inq;
        
        DAMAC_ziwo_listener obj = new DAMAC_ziwo_listener ();
        obj.getObjects ();
        obj.getUserDetails ();
        DAMAC_ziwo_listener.getFieldApis ('Inquiry__c');
        DAMAC_ziwo_listener.searchName ('test', 'Inquiry__c');
        try {
        DAMAC_ziwo_listener.dialCustomer(inq.id, '');
        }catch (Exception e) {}
        try {
            DAMAC_ziwo_listener.getTaskId ('123' , 'test');
        } catch (Exception e) {}
        DAMAC_ziwo_listener_Inbound.ReadStreamingEventinbound('weasd32', '7889988999', u.Ziwo_DID__c, 'Inbound');
         
    }
}