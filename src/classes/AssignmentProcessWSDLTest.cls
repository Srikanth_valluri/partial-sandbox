@isTest
public class AssignmentProcessWSDLTest {
    @isTest
    public static void instanceForClass_getQuarterlyDueResponse_element () {
        Test.startTest();
        AssignmentProcessWSDL.getQuarterlyDueResponse_element getQuarterlyDueResponse_element_Obj = 
            new AssignmentProcessWSDL.getQuarterlyDueResponse_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_customerCreation_element () {
        Test.startTest();
        AssignmentProcessWSDL.customerCreation_element customerCreation_element_Obj = 
            new AssignmentProcessWSDL.customerCreation_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_AssignmentDocument_element () {
        Test.startTest();
        AssignmentProcessWSDL.AssignmentDocument_element AssignmentDocument_element_Obj = 
            new AssignmentProcessWSDL.AssignmentDocument_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_getPDCAvaliable_element () {
        Test.startTest();
        AssignmentProcessWSDL.getPDCAvaliable_element getPDCAvaliable_element_Obj = 
            new AssignmentProcessWSDL.getPDCAvaliable_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_CreateAssignment_element () {
        Test.startTest();
         AssignmentProcessWSDL.CreateAssignment_element CreateAssignment_element_Obj = 
             new AssignmentProcessWSDL.CreateAssignment_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_AssignmentFee_element () {
        Test.startTest();
        AssignmentProcessWSDL.AssignmentFee_element AssignmentFee_element_Obj = 
            new AssignmentProcessWSDL.AssignmentFee_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_PDCOutstanding_element () {
        Test.startTest();
        AssignmentProcessWSDL.PDCOutstanding_element PDCOutstanding_element_Obj = 
            new AssignmentProcessWSDL.PDCOutstanding_element ();
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_getQuarterlyDue_element () {
        Test.startTest();
        AssignmentProcessWSDL.getQuarterlyDue_element getQuarterlyDue_element_Obj = 
            new AssignmentProcessWSDL.getQuarterlyDue_element ();
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_AssignmentFeeResponse_element () {
        Test.startTest();
        AssignmentProcessWSDL.AssignmentFeeResponse_element AssignmentFeeResponse_element_Obj = 
            new AssignmentProcessWSDL.AssignmentFeeResponse_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_getPendingDues_element () {
        Test.startTest();
        AssignmentProcessWSDL.getPendingDues_element getPendingDues_element_Obj = 
            new AssignmentProcessWSDL.getPendingDues_element ();
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_getPenaltyValue_element () {
        Test.startTest();
        AssignmentProcessWSDL.getPenaltyValue_element getPenaltyValue_element_Obj = 
            new AssignmentProcessWSDL.getPenaltyValue_element () ;
        Test.stopTest();
    } 
    
    @isTest
    public static void instanceForClass_getPDCDetailsResponse_element () {
        Test.startTest();
        AssignmentProcessWSDL.getPDCDetailsResponse_element getPDCDetailsResponse_element_Obj = 
            new AssignmentProcessWSDL.getPDCDetailsResponse_element () ;
        Test.stopTest();
    } 
    
    @isTest
    public static void instanceForClass_PDCOutstandingResponse_element () {
        Test.startTest();
        AssignmentProcessWSDL.PDCOutstandingResponse_element PDCOutstandingResponse_element_Obj = 
            new AssignmentProcessWSDL.PDCOutstandingResponse_element () ;
        Test.stopTest();
    } 
    
   
    @isTest
    public static void instanceForClass_UpdateRegistrationStatus_element () {
        Test.startTest();
        AssignmentProcessWSDL.UpdateRegistrationStatus_element UpdateRegistrationStatus_element_Obj = 
            new AssignmentProcessWSDL.UpdateRegistrationStatus_element () ;
        Test.stopTest();
    } 
    
    @isTest
    public static void instanceForClass_customerCreationResponse_element () {
        Test.startTest();
        AssignmentProcessWSDL.customerCreationResponse_element customerCreationResponse_element_Obj = 
            new AssignmentProcessWSDL.customerCreationResponse_element () ;
        Test.stopTest();
    } 
    
    @isTest
    public static void instanceForClass_getFacilityDue_element () {
        Test.startTest();
        AssignmentProcessWSDL.getFacilityDue_element getFacilityDue_element_Obj = 
            new AssignmentProcessWSDL.getFacilityDue_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_getPDCDetails_element () {
        Test.startTest();
        AssignmentProcessWSDL.getPDCDetails_element getPDCDetails_element_Obj = 
            new AssignmentProcessWSDL.getPDCDetails_element () ;
        Test.stopTest();
    } 
     
    @isTest
    public static void instanceForClass_AssignmentApprovalResponse_element () {
        Test.startTest();
        AssignmentProcessWSDL.AssignmentApprovalResponse_element getPDCDetails_element_Obj = 
            new AssignmentProcessWSDL.AssignmentApprovalResponse_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_getPenaltyValueResponse_element () {
        Test.startTest();
        AssignmentProcessWSDL.getPenaltyValueResponse_element getPenaltyValueResponse_element_Obj = 
            new AssignmentProcessWSDL.getPenaltyValueResponse_element () ;
        Test.stopTest();
    }  
    
    @isTest
    public static void instanceForClass_getPendingDuesResponse_element () {
        Test.startTest();
        AssignmentProcessWSDL.getPendingDuesResponse_element getPendingDuesResponse_element_Obj = 
            new AssignmentProcessWSDL.getPendingDuesResponse_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_getPDCAvaliableResponse_element () {
        Test.startTest();
        AssignmentProcessWSDL.getPDCAvaliableResponse_element getPDCAvaliableResponse_element_Obj = 
            new AssignmentProcessWSDL.getPDCAvaliableResponse_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_getFacilityDueResponse_element_element () {
        Test.startTest();
        AssignmentProcessWSDL.getFacilityDueResponse_element getFacilityDueResponse_element_Obj = 
            new AssignmentProcessWSDL.getFacilityDueResponse_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_AssignmentApproval_element () {
        Test.startTest();
        AssignmentProcessWSDL.AssignmentApproval_element AssignmentApproval_element_Obj = 
            new AssignmentProcessWSDL.AssignmentApproval_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_AssignmentDocumentResponse_element () {
        Test.startTest();
        AssignmentProcessWSDL.AssignmentDocumentResponse_element AssignmentDocumentResponse_element_Obj = 
            new AssignmentProcessWSDL.AssignmentDocumentResponse_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_UpdateRegistrationStatusResponse_element () {
        Test.startTest();
        AssignmentProcessWSDL.UpdateRegistrationStatusResponse_element UpdateRegistrationStatusResponse_element_Obj = 
            new AssignmentProcessWSDL.UpdateRegistrationStatusResponse_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_getDueandOverDue_element () {
        Test.startTest();
        AssignmentProcessWSDL.getDueandOverDue_element getDueandOverDue_element_Obj = 
            new AssignmentProcessWSDL.getDueandOverDue_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_getDueandOverDueResponse_element () {
        Test.startTest();
        AssignmentProcessWSDL.getDueandOverDueResponse_element getDueandOverDueResponse_element_Obj = 
            new AssignmentProcessWSDL.getDueandOverDueResponse_element () ;
        Test.stopTest();
    } 
    
    @isTest
    public static void instanceForClass_CreateAssignmentResponse_element () {
        Test.startTest();
        AssignmentProcessWSDL.CreateAssignmentResponse_element CreateAssignmentResponse_element_Obj = 
            new AssignmentProcessWSDL.CreateAssignmentResponse_element () ;
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_AssignmentHttpSoap11Endpoint () {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint AssignmentHttpSoap11Endpoint_Obj = 
            new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint () ;
        AssignmentHttpSoap11Endpoint_Obj .getDueandOverDue('Test 1','Test 2','Test 3','Test 4','Test 5');
        Test.stopTest();
    }
    
    @isTest
    public static void instanceForClass_AssignmentHttpSoap11Endpoint_UpdateRegistrationStatus () {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint AssignmentHttpSoap11Endpoint_Obj = 
            new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint () ;
        AssignmentHttpSoap11Endpoint_Obj .UpdateRegistrationStatus('Test 1','Test 2','Test 3','Test 4','Test 5');
        Test.stopTest();
    } 
    
    @isTest
    public static void instanceForClass_AssignmentHttpSoap11Endpoint_CreateAssignment () {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint AssignmentHttpSoap11Endpoint_Obj = 
            new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint () ;
        AssignmentHttpSoap11Endpoint_Obj .CreateAssignment('Test 1','Test 2','Test 3','Test 4','Test 5');
        Test.stopTest();
    }
    
     @isTest
     public static void instanceForClass_AssignmentHttpSoap11Endpoint_PDCOutstanding () {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint AssignmentHttpSoap11Endpoint_Obj = 
            new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint () ;
        AssignmentHttpSoap11Endpoint_Obj .PDCOutstanding('Test 1','Test 2','Test 3','Test 4');
        Test.stopTest();
    }
    
    @isTest
     public static void instanceForClass_AssignmentHttpSoap11Endpoint_getFacilityDue () {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint AssignmentHttpSoap11Endpoint_Obj = 
            new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint () ;
        AssignmentHttpSoap11Endpoint_Obj .getFacilityDue('Test 1','Test 2','Test 3','Test 4','Test 5');
        Test.stopTest();
    } 
    
    @isTest
     public static void instanceForClass_AssignmentHttpSoap11Endpoint_getPDCAvaliable () {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint AssignmentHttpSoap11Endpoint_Obj = 
            new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint () ;
        AssignmentHttpSoap11Endpoint_Obj .getPDCAvaliable('Test 1','Test 2','Test 3','Test 4');
        Test.stopTest();
    } 
    
    @isTest
     public static void instanceForClass_AssignmentHttpSoap11Endpoint_getPDCDetails () {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint AssignmentHttpSoap11Endpoint_Obj = 
            new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint () ;
        AssignmentHttpSoap11Endpoint_Obj .getPDCDetails('Test 1','Test 2','Test 3','Test 4');
        Test.stopTest();
    }
    
    @isTest
     public static void instanceForClass_AssignmentHttpSoap11Endpoint_AssignmentFee () {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint AssignmentHttpSoap11Endpoint_Obj = 
            new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint () ;
        AssignmentHttpSoap11Endpoint_Obj .AssignmentFee('Test 1','Test 2','Test 3','Test 4','Test 5','Test 6','Test 7','Test 8','Test 9','Test 10','Test 11','Test 12','Test 13','Test 14','Test 15','Test 16','Test 17','Test 18');
        Test.stopTest();
    }
    
    @isTest
     public static void instanceForClass_AssignmentHttpSoap11Endpoint_AssignmentDocument () {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint AssignmentHttpSoap11Endpoint_Obj = 
            new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint () ;
        AssignmentHttpSoap11Endpoint_Obj .AssignmentDocument('Test 1','Test 2','Test 3','Test 4','Test 5','Test 6','Test 7','Test 8','Test 9','Test 10','Test 11','Test 12','Test 13','Test 14','Test 15','Test 16','Test 17','Test 18','Test 19');
        Test.stopTest();
    }
    
    @isTest
     public static void instanceForClass_AssignmentHttpSoap11Endpoint_AssignmentApproval () {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint AssignmentHttpSoap11Endpoint_Obj = 
            new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint () ;
        AssignmentHttpSoap11Endpoint_Obj .AssignmentApproval('Test 1','Test 2','Test 3','Test 4','Test 5','Test 6','Test 7','Test 8','Test 9','Test 10','Test 11','Test 12','Test 13','Test 14','Test 15','Test 16','Test 17','Test 18');
        Test.stopTest();
    }
    
    @isTest
     public static void instanceForClass_AssignmentHttpSoap11Endpoint_getPendingDues () {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint AssignmentHttpSoap11Endpoint_Obj = 
            new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint () ;
        AssignmentHttpSoap11Endpoint_Obj .getPendingDues('Test 1','Test 2');
        Test.stopTest();
    }
    
    @isTest
     public static void instanceForClass_AssignmentHttpSoap11Endpoint_getPenaltyValue () {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint AssignmentHttpSoap11Endpoint_Obj = 
            new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint () ;
        AssignmentHttpSoap11Endpoint_Obj .getPenaltyValue('Test 1','Test 2','Test 3','Test 4','Test 5');
        Test.stopTest();
    }
    
    @isTest
     public static void instanceForClass_AssignmentHttpSoap11Endpoint_getQuarterlyDue () {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint AssignmentHttpSoap11Endpoint_Obj = 
            new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint () ;
        AssignmentHttpSoap11Endpoint_Obj .getQuarterlyDue('Test 1','Test 2','Test 3','Test 4','Test 5');
        Test.stopTest();
    }
    
}