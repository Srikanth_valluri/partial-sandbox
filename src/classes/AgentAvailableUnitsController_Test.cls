/*

pratiksha narvekar

*/
@isTest
public class AgentAvailableUnitsController_Test {
    public static Account a = new Account();
    public static Id RecordTypeIdAGENT;
    public static Id RecordTypeIdContact;
    public static Id RSRecordTypeId;
    public static Campaign__c camp = new Campaign__c();
    public static Assigned_Agent__c aa = new Assigned_Agent__c();
    public static Assigned_PC__c assignedPcs = new Assigned_PC__c();
    public static Contact con = new Contact();
    public static User portalUser = new User(); 
    public static NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
    public static Location__c loc = new Location__c();
    public static Inventory__c  inv = new Inventory__c ();
    public static Campaign_Inventory__c  cinv = new Campaign_Inventory__c(); 
    public static Map<Id, String> inventoryCampaignIdsMap = new Map<Id, String>(); 
    public static List<Id> campaignIdsList = new List<Id>(); 
    public static Inventory_User__c iu = new Inventory_User__c ();
    public static List<NSIBPM__SR_Template__c> SRTemplateList = new List<NSIBPM__SR_Template__c>();  
    public static NSIBPM__Step__c stp = new NSIBPM__Step__c();
    public static AgentAvailableUnitsController auc = new AgentAvailableUnitsController();
    public static UtilityWrapperManager um = new UtilityWrapperManager ();
    public static AgentAvailableUnitsController.FilterWrapper fw; 
    public static Agent_Site__c objAS = new Agent_Site__c();
    public static Property__c onjprop = new Property__c();
    public static Booking_Unit__c bu = new Booking_Unit__c();
    public static Payment_Plan__c pp = new Payment_Plan__c();
    public static buyer__c b = new buyer__c();
    public static List<Inventory__c> lstInv = new List<Inventory__c>();
    public static void setup(){  
    	RSRecordTypeId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId();
        RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        RecordTypeIdContact = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});

        a.recordtypeid=RecordTypeIdAGENT;
        a.Name = 'TestAccount';
        a.Agency_Short_Name__c = 'testShrName';
        insert a;
        
        objAS.Org_ID__c = '12345';
        objAS.Name='UAE';
        objAS.Agency__c = a.ID;
        insert objAS;  
        
        //campaign
        camp.RecordTypeId = RSRecordTypeId;
        camp.Campaign_Name__c='Test Campaign';
        camp.start_date__c = System.today();
        camp.end_date__c = System.Today().addDays(30);
        camp.Marketing_start_date__c = System.today();
        camp.Marketing_end_date__c = System.Today().addDays(30);
        camp.Language__c = 'English';
        camp.Marketing_Active__c = true;
        camp.Credit_Control_Active__c  = true;
        camp.Sales_Admin_Active__c  = true;
        insert camp;
        Campaign__c objc = [select Active__c from Campaign__c where id=: camp.id];
        System.debug('...select query campign...'+objc);
        //Assigned_Agent__c
        aa.Campaign__c = camp.id;
        insert aa;
        
        //Assigned_PC__c
        assignedPcs.Campaign__c = camp.id;
        assignedPcs.User__c = userinfo.getuserid();
        insert assignedPcs;
        
        List<Assigned_PC__c> listAssigned = new List<Assigned_PC__c>();
        listAssigned = [select id,
                               Campaign__r.Marketing_Start_Date__c, 
                               Campaign__r.Marketing_End_Date__c,
                               Start_Date__c,
                               Campaign__r.Active__c
                        from Assigned_PC__c
                        where id=:assignedPcs.ID];
         System.debug('...select query listAssigned...'+listAssigned);
        //contact 
        con.LastName = 'testlnme';
        con.AccountId = a.Id ;
        con.Agent_Representative__c=true;
        insert con;
        
         profile p= [select Id from Profile where name='Customer Community - Admin'];
         //user 
         portalUser = new User(alias = 'test456', email='testusr1@test.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.id, country='United Arab Emirates',IsActive =true,
                ContactId = con.Id,timezonesidkey='America/Los_Angeles', username='testusr1@test.com');
                
         insert portalUser;
         
         sr.recordtypeid=RecordTypeIdContact;
         sr.NSIBPM__SR_Template__c = SRTemplateList[0].Id;
         sr.Eligible_to_Sell_in_Dubai__c = true;
         sr.Agency_Type__c = 'Individual';
         sr.ID_Type__c = 'Passport';
         sr.Agency__c = a.id;
         sr.Agency_Email_2__c = 'test2@gmail.com';
         sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
         sr.Country_of_Sale__c = 'UAE;KSA;Lebanon';
         insert sr;
         
         stp = InitializeSRDataTest.createStep(sr.id,null,null);
         insert stp;
         
         loc.Location_Code__c =  'test';
         loc.Location_ID__c = 'testloc2010';
         insert loc;
        
     
        onjprop.name='DAMAC HEIGHTS';
        onjprop.Property_ID__c =2971;
        insert onjprop;
        Booking__c book = new booking__c();
        book.Deal_SR__c = sr.id;
        book.Booking_Channel__c = 'Web';
        insert book;
        

        //inventory
        inv.Unit_Location__c = loc.id;
        inv.Property_Country__c = 'UAE';
        inv.Property__c = onjprop.id;
        inv.Marketing_Name__c ='DAMAC HEIGHTS';
        inv.Status__c = 'Released';
        inv.Is_Assigned__c = false;
        inv.List_Price__c = 900;
        upsert inv;
        
        
        bu.Booking__c = book.id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'test';
        bu.Inventory__c = inv.id;
        insert bu;
        Location__c objLoc = InitializeSRDataTest.createLocation('123','Building');
        insert objLoc;   
        Payment_Terms__c pt = new Payment_Terms__c();
        pp.Booking_Unit__c = bu.id;
        pp.Effective_From__c = system.today().adddays(-4);
        pp.Effective_To__c = system.today().adddays(7); 
        pp.Building_Location__c =  objLoc.id;      
        insert pp;
        lstInv.add(InitializeSRDataTest.createInventory(objLoc.id));
        lstInv.add(InitializeSRDataTest.createInventory(objLoc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[1].Property_Country__c = 'Lebanon';
        lstInv[1].Inventory_ID__c = '345wer';
        lstInv[0].Special_Price__c = 121;
        lstInv[1].Special_Price__c = 2212;
        insert lstInv;
       
        pt.Payment_Plan__c = pp.id;
        pt.Booking_Unit__c = bu.id;
        pt.Percent_Value__c = '5';
        pt.Modified_Percent_Value__c = '2'; 
        insert pt;
        
        Inquiry__c inquiryRecord = new Inquiry__c();
        inquiryRecord.By_Pass_Validation__c = true;
        inquiryRecord.Party_ID__c = '12345';
        inquiryRecord.Title__c = 'MR.';
        inquiryRecord.Title_Arabic__c ='MR.';
        inquiryRecord.First_Name__c = 'Test';
        inquiryRecord.First_Name_Arabic__c ='Test';
        inquiryRecord.Last_Name__c ='Test';
        inquiryRecord.Last_Name_Arabic__c = 'Test';
        insert inquiryRecord;
                
        b.Buyer_Type__c =  'Individual';
        b.Address_Line_1__c =  'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.City__c = 'Dubai' ;
        b.Inquiry__c =inquiryRecord.Id ;
        b.Date_of_Birth__c = string.valueof(system.today().addyears(-30)) ;
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname' ;
        b.Primary_Buyer__c = true;
        b.Last_Name__c =  'lastname';
        b.Nationality__c = 'Indian' ;
        b.Passport_Expiry_Date__c = string.valueof(system.today().addyears(20)) ;
        b.Passport_Number__c = 'J0565556' ;
        b.Phone__c = '569098767' ;
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b.Place_of_Issue__c =  'India';
        b.Title__c = 'Mr';
        b.booking__c = book.id;
        insert b;
        
        UtilityWrapperManager.InventoryBuyerWrapper umi = new UtilityWrapperManager.InventoryBuyerWrapper(true, false,false, 100, 100, 'test', 'test', 'test','test', 2, inv, null, null,null);
        
        list<UtilityWrapperManager.InventoryBuyerWrapper> listumi = new list<UtilityWrapperManager.InventoryBuyerWrapper>();
        listumi.add(umi);
        
        fw = new AgentAvailableUnitsController.FilterWrapper('GeneralInventories', 'test', 'test', 'test', 'test', 'test','', 100, 100);
        
        //campaign
        
        cinv.Campaign__c = camp.id;
        cinv.Inventory__c = inv.id;
        cinv.Start_Date__c = system.today()-1;
        cinv.End_Date__c = system.today()+1;
        insert cinv;
        
        inventoryCampaignIdsMap.put(cinv.Inventory__c , cinv.Campaign__c);
        campaignIdsList.add(camp.id);
        iu.Inventory__c = inv.id;
        iu.user__c = portalUser.id;
        iu.Start_Date__c = system.today() - 1;
        iu.End_Date__c = system.today() + 1;
        
        
        insert iu;
        
        Map<Id, UtilityWrapperManager.InventoryBuyerWrapper> mp_um = new Map<Id, UtilityWrapperManager.InventoryBuyerWrapper>();
        mp_um.put(camp.id, umi);
        
        um.dealRecord = sr;
        um.ibwList = listumi;
        
        Map<String, Map<Id, UtilityWrapperManager.InventoryBuyerWrapper>> mptest_campaignInventoryBuyerWrapperMap = new Map<String, Map<Id, UtilityWrapperManager.InventoryBuyerWrapper>>();
        mptest_campaignInventoryBuyerWrapperMap.put('GeneralInventories', mp_um);
            
           }
    public static testmethod void AgentAvailableUnitsController_testMethod1() {
        setup();
        system.runAs(portalUser){    
        	test.starttest();
       // auc.objUserInfo = con;
        auc.priceRangeStartValue = 1000;
        auc.priceRangeEndValue = 2000;
        auc.availableInventoryList = um;
        auc.fwObject = fw;
        //auc.campaignInventoryBuyerWrapperMap = mptest_campaignInventoryBuyerWrapperMap;
        List<UtilityWrapperManager.InventoryBuyerWrapper> objl = new List<UtilityWrapperManager.InventoryBuyerWrapper>();
        auc.getCampaignList();
        //auc.getPromotionList();
        auc.getProjectList();
        auc.getBuildingList();
        auc.getBedroomTypeList();
        //auc.getPriceList();
        auc.refreshthePropertyFilters();
        
        auc.refreshtheBedroomFilters();
        auc.getSpaMethods();
        auc.getProperties();
        auc.getRelatedInventories();
        auc.showNextRecords();
        auc.getAgencyRelatedInventories();
        
        AgentAvailableUnitsController.getInventoryUsersList(a.id);
        auc.getCampaignDetails(campaignIdsList);
        //auc.getAssociatedProperties(campaignIdsList);
        auc.getAssociatedProperties(camp.id);
        //auc.getInventoryDetails(inventoryCampaignIdsMap.keySet(), inventoryCampaignIdsMap, 'campaign');
        AgentAvailableUnitsController.getInventoryDetails('test');
        auc.agencyName='test';
        auc.agencyType='test';
        auc.agentName='test';
        auc.loggedInUserId='test';
        auc.showNext=true;
        auc.showprevious=false;
             
        auc.getGeneralInventories();
        AgentAvailableUnitsController.getCampaignRelatedInventories();
        AgentAvailableUnitsController.getTeamUsers();
        auc.sortToggle();
        auc.getSortedList();
        AgentAvailableUnitsController.InventoryWrapper objA = new AgentAvailableUnitsController.InventoryWrapper();
        objA.sortField = 'test';
        objA.inventoryId= 'test';
        objA.sellingPrice= 1234;
        objA.specialPrice= 1234;
        auc.getFloorList();
        auc.getPackageList();
        auc.selectInventory();
        auc.refreshtheBuildingFilters();
        auc.deleteInventory();
        
        AgentAvailableUnitsController.getInventoryDetails('test');
        AgentAvailableUnitsController.getAgencyDetails(a.name);
        AgentAvailableUnitsController.getCorporateAgents(a.id);
        AgentAvailableUnitsController.validateSelectedUnits(a.id, inv.id); 
    	test.stoptest();
        
        }
    }
        public static testmethod void AgentAvailableUnitsController_testMethod2() {
        inv.List_Price__c = null;
        inv.Floor_Package_Type__c = 'Floor';
        inv.Floor_Package_ID__c='45';
        inv.Property_Country__c = 'UAE';
        inv.Marketing_Name__c ='DAMAC HEIGHTS';
        inv.Status__c = 'Released';
        inv.Is_Assigned__c = false;
        insert inv;
        setup();
        
        system.runAs(portalUser){    
       // auc.objUserInfo = con;
       test.startTest();
        auc.priceRangeStartValue = 1000;
        auc.priceRangeEndValue = 2000;
        auc.availableInventoryList = um;
        auc.fwObject = fw;
        //auc.campaignInventoryBuyerWrapperMap = mptest_campaignInventoryBuyerWrapperMap;
        
        auc.getCampaignList();
        //auc.getPromotionList();
        auc.getProjectList();
        auc.getBuildingList();
        auc.getBedroomTypeList();
        //auc.getPriceList();
        auc.refreshthePropertyFilters();
   
 
        auc.refreshtheBedroomFilters();
        auc.getSpaMethods();
        auc.getProperties();
        auc.getRelatedInventories();
        auc.showNextRecords();
        auc.getAgencyRelatedInventories();
        
        AgentAvailableUnitsController.getInventoryUsersList(a.id);
        auc.getCampaignDetails(campaignIdsList);
        //auc.getAssociatedProperties(campaignIdsList);
        auc.getAssociatedProperties(camp.id);
        //auc.getInventoryDetails(inventoryCampaignIdsMap.keySet(), inventoryCampaignIdsMap, 'campaign');
        AgentAvailableUnitsController.getInventoryDetails('test');
        auc.agencyName='test';
        auc.agencyType='test';
        auc.agentName='test';
        auc.loggedInUserId='test';
        auc.showNext=true;
        auc.showprevious=false;
        auc.getGeneralInventories();
        AgentAvailableUnitsController.getCampaignRelatedInventories();
        AgentAvailableUnitsController.getTeamUsers();
        auc.sortToggle();
        auc.getSortedList();
        AgentAvailableUnitsController.InventoryWrapper objA = new AgentAvailableUnitsController.InventoryWrapper();
        objA.sortField = 'test';
        objA.inventoryId= 'test';
        objA.sellingPrice= 1234;
        objA.specialPrice= 1234;
        auc.getFloorList();
        auc.getPackageList();
        
        
        auc.countTotalPages();
        Map<String, Payment_Plan__c> mappayment = new Map<String, Payment_Plan__c>();
        mappayment.put(inv.id,pp);
        List<Buyer__c> listB =new List<Buyer__c>();
        listB.add(b);
        list<UtilityWrapperManager.InventoryBuyerWrapper> wrplst = new list<UtilityWrapperManager.InventoryBuyerWrapper>();
        UtilityWrapperManager.InventoryBuyerWrapper ibwn= new UtilityWrapperManager.InventoryBuyerWrapper(false, 
										                                                                  false,
										                                                                  false,
										                                                                  40000.00,
										                                                                  inv.special_Price_calc__c,
										                                                                  '',
										                                                                  '',
										                                                                  '',
										                                                                  '',
										                                                                  0,
										                                                                  inv,
										                                                                  bu,
										                                                                  mappayment,
										                                                                  listB);
        
        
        
        wrplst.add(ibwn);
        
        auc.refreshtheBuildingFilters();
       
        AgentAvailableUnitsController.getInventoryDetails('test');
        AgentAvailableUnitsController.getAgencyDetails(a.name);
        AgentAvailableUnitsController.getCorporateAgents(a.id);
        AgentAvailableUnitsController.validateSelectedUnits(a.id, inv.id); 
        auc.selectInventory();
         auc.deleteInventory();
        
    	test.stopTest();
        
        }
    }
   
    
}