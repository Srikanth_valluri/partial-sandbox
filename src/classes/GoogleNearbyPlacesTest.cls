@isTest
private class GoogleNearbyPlacesTest {

    @testSetup
    static void setupTestData() {
        insert new GoogleApiKey__c(
            Name = 'Places',
            ApiKey__c = 'testkey'
        );
    }

    @isTest
    static void testNearbyPlacesNullRequest() {
        Test.startTest();
            System.assertEquals(NULL, GoogleNearbyPlaces.getNearbyPlaces(NULL));
        Test.stopTest();
    }

    @isTest
    static void testNearbyPlacesWithoutApiKey() {
        delete [SELECT Id FROM GoogleApiKey__c];
        Test.startTest();
            GoogleNearbyPlaces.Response response = GoogleNearbyPlaces.getNearbyPlaces(
                new GoogleNearbyPlaces.Request(0, 0)
            );
            System.assertNotEquals(NULL, response);
            System.assertEquals(GoogleNearbyPlaces.API_KEY_UNAVAILABLE, response.error_message);
        Test.stopTest();
    }

    @isTest
    static void testNearbyPlacesInvalidRequest() {
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
            200, 'success', JSON.serialize('success'), NULL
        ));

        Test.startTest();
            GoogleNearbyPlaces.Response response = GoogleNearbyPlaces.getNearbyPlaces(
                new GoogleNearbyPlaces.Request(0, 0)
            );
            System.assertNotEquals(NULL, response);
            System.assertEquals('error', response.status);
        Test.stopTest();
    }

    @isTest
    static void testNearbyPlacesValidRequest() {
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
            200, 'success', JSON.serialize(new GoogleNearbyPlaces.Response()), NULL
        ));

        Test.startTest();
            GoogleNearbyPlaces.Response response = GoogleNearbyPlaces.getNearbyPlaces(
                new GoogleNearbyPlaces.Request(0, 0)
            );
            System.assertNotEquals(NULL, response);
            System.assertNotEquals('error', response.status);
            System.assertNotEquals(GoogleNearbyPlaces.API_KEY_UNAVAILABLE, response.error_message);
        Test.stopTest();
    }

    @isTest
    static void testPlaceDetailsNullId() {
        Test.startTest();
            System.assertEquals(NULL, GoogleNearbyPlaces.getPlaceDetails(NULL));
        Test.stopTest();
    }

    @isTest
    static void testPlaceDetailsWithoutApiKey() {
        delete [SELECT Id FROM GoogleApiKey__c];
        Test.startTest();
            GoogleNearbyPlaces.Place placeDetails = GoogleNearbyPlaces.getPlaceDetails('place_id');
            System.assertNotEquals(NULL, placeDetails);
            System.assertEquals(GoogleNearbyPlaces.API_KEY_UNAVAILABLE, placeDetails.error_message);
        Test.stopTest();
    }

    @isTest
    static void testPlaceDetailsInvalidRequest() {
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
            200, 'success', JSON.serialize('success'), NULL
        ));

        Test.startTest();
            GoogleNearbyPlaces.Place placeDetails = GoogleNearbyPlaces.getPlaceDetails('place_id');
            System.assertNotEquals(NULL, placeDetails);
            System.assertEquals('error', placeDetails.status);
        Test.stopTest();
    }

    @isTest
    static void testPlaceDetailsValidRequest() {
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
            200, 'success', JSON.serialize(new GoogleNearbyPlaces.Place()), NULL
        ));

        Test.startTest();
            GoogleNearbyPlaces.Place placeDetails = GoogleNearbyPlaces.getPlaceDetails('place_id');
            System.assertNotEquals(NULL, placeDetails);
            System.assertNotEquals('error', placeDetails.status);
            System.assertNotEquals(GoogleNearbyPlaces.API_KEY_UNAVAILABLE, placeDetails.error_message);
        Test.stopTest();
    }

}