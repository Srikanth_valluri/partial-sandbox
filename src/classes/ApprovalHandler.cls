/* Description: Class used to send approval request to the next approver dynamically
 */
public without sharing class ApprovalHandler {
    
    //Method used to send approval request to the next approver dynamically based on the
    //comma separated roles defined in the Approving_Authorities__c field on Case
    public static map<ID,ID> mapManagerID ;                        
    public static map<ID,String> mapManagerRole;
    public static integer intCounter = 0;
    public static String custLabelVal = System.Label.Dynamic_Approval_Additional_Logic_On_Off  != '' ? System.Label.Dynamic_Approval_Additional_Logic_On_Off  : 'OFF';
    
    public static void CallApprovalHandler(){
        list<User> lstActiveUser = new list<User>();
        mapManagerID = new map<ID,ID>();
        mapManagerRole = new map<ID,String>();
    set<Id> setUserIdTORestirct = new set<Id>{'0050Y000002c9nSQAQ','0050Y000002c5p8QAA'}; 
        lstActiveUser = [SELECT Id,Name,isActive,Managerid,UserType,UserRole.Name,UserRoleID
               FROM User
                          WHERE isActive = true AND IsPortalEnabled = false AND Id NOT IN: setUserIdTORestirct
              AND Managerid NOT IN: setUserIdTORestirct
            ];
        
        for(User objUser : lstActiveUser){
            if(objUser.managerid != null){
                mapManagerID.put(objUser.id,objUser.managerid);
            }
            if(objUser.UserRoleID != null){
                mapManagerRole.put(objUser.id,objUser.UserRole.Name);
            }
            
        }
        system.debug('--mapManagerID--'+mapManagerID.Size());
        system.debug('--mapManagerRole--'+mapManagerRole.Size());
    }
    
    @future
    public static void processApprovals(Set<Id> setCaseId) {
        system.debug('--*processApprovals*--');
        List<Case> lstCaseToUpdate = new List<Case>();
        CallApprovalHandler();
    set<Id> setUserIdTORestirct = new set<Id>{'0050Y000002c9nSQAQ','0050Y000002c5p8QAA'};
        map<Id,Case> mapCase = new map<Id,Case>([Select Id,Ownerid, Approving_Authorities__c, RecordTypeId,
                              Approving_User_Role__c, Approving_User_Id__c, LoA_Submitted__c,Booking_Unit__r.Property_Name_Inventory__c,
                              Percent_to_be_waived__c, Assignment_Fee_Approval__c,Approving_User_Name__c,
                              RecordType.DeveloperName,Customer_Category__c
                              , (Select Id,Property_Name__c From SR_Booking_Units__r where Property_Name__c LIKE '%AKOYA OXYGEN%' LIMIT 1)
                              From Case 
                              Where Id IN: setCaseId
                              And Approving_Authorities__c != null
                AND Approving_User_Id__c NOT IN: setUserIdTORestirct 
                AND Approving_User_Name__c != 'Ali Sajwani' ]);
        
        if(mapCase != null && !mapCase.isEmpty()) {
            List<ApprovalWrapper> lstApprovalWrapper = new List<ApprovalWrapper>();
            Set<String> setRolesToQuery = new Set<String>();
            Set<Id> setOwnerId = new Set<Id>{ System.Label.Dynamic_Approval_User_1
                                            , System.Label.Dynamic_Approval_User_2
                                            , System.Label.Dynamic_Approval_User_3
                                            , System.Label.Dynamic_Approval_User_4
                                            , System.Label.Dynamic_Approval_User_5
                                            , System.Label.Dynamic_Approval_User_6 };
            
            
            
            for(Case objCase: mapCase.values()) {
                if(String.isNotBlank(objCase.Approving_Authorities__c)) {
                    String strApprovingUserRole;
                    Id idApprovingUser;
                    
                    String buPropertyName = objCase.SR_Booking_Units__r.size() > 0 ? objCase.SR_Booking_Units__r[0].Property_Name__c : objCase.Booking_Unit__r.Property_Name_Inventory__c;
                    
                    
                    if( custLabelVal.equalsIgnoreCase('ON') ) { 
                        /*if( setOwnerId.contains( objCase.OwnerId ) 
                         && String.isNotBlank ( buPropertyName ) 
                         && buPropertyName.contains('AKOYA OXYGEN')
                         && objCase.Approving_User_Id__c == System.Label.User_Amira_SF_Id 
                         && objCase.Approving_User_Name__c == 'Amira Sajwani'
                         && objCase.Customer_Category__c != 'Elite' 
                         && objCase.Customer_Category__c != 'Top Broker' ) {
                            strApprovingUserRole = 'Committee';
                            idApprovingUser = System.Label.User_Ali_Sajwani_SF_Id;
                            objCase.Approving_User_Name__c = 'Ali Sajwani';
                         } else*/ 
                         if( ( String.isNotBlank ( buPropertyName ))// || !buPropertyName.contains('AKOYA OXYGEN') ) 
                                 && objCase.Approving_User_Id__c == System.Label.User_Amira_SF_Id 
                                 && objCase.Approving_User_Name__c == 'Amira Sajwani' ) {
                                        strApprovingUserRole = 'Committee';
                                        idApprovingUser = System.Label.User_Amira_SF_Id;
                         } else if( objCase.Approving_User_Id__c != System.Label.User_Amira_SF_Id 
                                 && objCase.Approving_User_Name__c != 'Amira Sajwani' ) {
                                strApprovingUserRole = objCase.Approving_User_Role__c;
                                idApprovingUser = objCase.Approving_User_Id__c;
                         }
                    }else if( custLabelVal.equalsIgnoreCase('OFF') ) {
                        strApprovingUserRole = objCase.Approving_User_Role__c;
                        idApprovingUser = objCase.Approving_User_Id__c;
                    }
                    
                    if(idApprovingUser != null && objCase.Approving_Authorities__c.containsIgnoreCase(strApprovingUserRole)) {
                        ApprovalWrapper objApprovalWrapper = new ApprovalWrapper();
                        objApprovalWrapper.strUserRole = strApprovingUserRole;
                        objApprovalWrapper.objCase = objCase;
                        objApprovalWrapper.approverUserId = idApprovingUser;
                        system.debug('--objApprovalWrapper.approverUserId 1--'+objApprovalWrapper.approverUserId);
                        lstApprovalWrapper.add(objApprovalWrapper);
                    
                        //objCase.Approving_User_Id__c = idTempUserId;
                        //objCase.Approving_User_Role__c = strTempRole;
                    
                        //lstCaseToUpdate.add(objCase);
                    }
                            
                    /*Id idApprovingUserId = String.isNotBlank(objCase.Approving_User_Id__c) ? 
                        objCase.Approving_User_Id__c : objCase.OwnerId;
                    if(mapManagerID.get(idApprovingUserId) != null) {
                        String strApprovingAuthorities = objCase.Approving_Authorities__c;
                        String strRole = strApprovingAuthorities.contains(',') ? 
                        strApprovingAuthorities.subString(0, strApprovingAuthorities.indexOf(',')) :
                        strApprovingAuthorities;
                        system.debug('--strRole--'+strRole);
                        
                        Id idTempUserId = getUserID(objCase.ownerID, strRole);
                        
                        if(idTempUserId != null) {
                            String strTempRole = mapManagerRole.get(idTempUserId);
                            strTempRole = RoleUtility.getSalesforceRole(strTempRole);
                            
                            if(objCase.Approving_Authorities__c.containsIgnoreCase(strTempRole)) {
                                ApprovalWrapper objApprovalWrapper = new ApprovalWrapper();
                                objApprovalWrapper.strUserRole = strTempRole;
                                objApprovalWrapper.objCase = objCase;
                                objApprovalWrapper.approverUserId = idTempUserId;
                                system.debug('--objApprovalWrapper.approverUserId 1--'+objApprovalWrapper.approverUserId);
                                lstApprovalWrapper.add(objApprovalWrapper);
                            
                                objCase.Approving_User_Id__c = idTempUserId;
                                objCase.Approving_User_Role__c = strTempRole;
                            
                                lstCaseToUpdate.add(objCase);
                            }
                        }
                    }*/
                    /*else {
                        objCase.Approving_User_Role__c = 'Approved';
                        lstCaseToUpdate.add(objCase);
                    }*/
                }
                    
                /*if(String.isBlank(objCase.Approving_User_Role__c)) {
                    if(mapManagerID.get(objCase.OwnerId) != null) {
                        objCase.Approving_User_Id__c = mapManagerID.get(objCase.OwnerId);
                        objCase.Approving_User_Role__c = mapManagerRole.get(objCase.Approving_User_Id__c);
                        
                        lstCaseToUpdate.add(objCase);
                    }
                    else {
                        objCase.Approving_Authorities__c = null;
                        lstCaseToUpdate.add(objCase);
                    }
                }*/
                
                /*System.debug('--objCase.Approving_Authorities__c--'+objCase.Approving_Authorities__c);
                String strApprovingAuthorities = objCase.Approving_Authorities__c;
                String strRole = strApprovingAuthorities.contains(',') ? 
                    strApprovingAuthorities.subString(0, strApprovingAuthorities.indexOf(',')) :
                    strApprovingAuthorities;
                system.debug('--strRole--'+strRole);
                setRolesToQuery.add(strRole);
                
                ApprovalWrapper objApprovalWrapper = new ApprovalWrapper();
                objApprovalWrapper.strUserRole = strRole;
                objApprovalWrapper.objCase = objCase;
                                   
                objApprovalWrapper.approverUserId = getUserID(objCase.ownerID,strRole);
                system.debug('--objApprovalWrapper.approverUserId 1--'+objApprovalWrapper.approverUserId);
                lstApprovalWrapper.add(objApprovalWrapper);
                */
            }
            
            /*list<User> lstApprovers = [Select Id, UserRole.Name From User Where UserRole.Name IN: setRolesToQuery
                                And IsActive = true];
            
            for(ApprovalWrapper objApproverWrapper: lstApprovalWrapper) {
                if(String.isBlank(objApproverWrapper.approverUserId)){ // if Manager-User logic failed then only
                    for(User objUser: lstApprovers) {
                        if(objApproverWrapper.strUserRole.equals(objUser.UserRole.Name)) {
                            objApproverWrapper.approverUserId = objUser.Id;
                            system.debug('--objApproverWrapper.approverUserId 2--'+objApproverWrapper.approverUserId);
                            break;
                        }
                    }
                }
            }*/
            system.debug('--lstApprovalWrapper post approver population--'+lstApprovalWrapper);
            
            if(!lstCaseToUpdate.isEmpty()) {
                update lstCaseToUpdate;
            }
            
            Id idPenaltyWaiverRecord = PenaltyWaiverUtility.getRecordTypeId( 'Penalty_Waiver','Case' );
            //Id CustomerRefundRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Refund').getRecordTypeId();
            //Id TokenRefundRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Token Refund').getRecordTypeId();
            
            List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
            for(ApprovalWrapper objApproverWrapper: lstApprovalWrapper) {
                // Create an approval request for the account
                if(objApproverWrapper.approverUserId != null){
                    Approval.ProcessSubmitRequest objApprovalSubmitRequest =
                        new Approval.ProcessSubmitRequest();
                    objApprovalSubmitRequest.setComments('Submitting request for approval.');
                    objApprovalSubmitRequest.setObjectId(objApproverWrapper.objCase.Id);
                    // Submit on behalf of a specific submitter
                    //objApprovalSubmitRequest.setSubmitterId(lstApprover[0].Id);
                    // Submit the record to specific process and skip the criteria evaluation
                    Case objC = mapCase.get(objApproverWrapper.objCase.Id);
                    if( objApproverWrapper.objCase.RecordTypeId == idPenaltyWaiverRecord ) {
                        objApprovalSubmitRequest.setProcessDefinitionNameOrId('Penalty_Waiver_Approval');
                    }
                    /*else if(objApproverWrapper.objCase.RecordTypeId == CustomerRefundRecordTypeId ||
                            objApproverWrapper.objCase.RecordTypeId == TokenRefundRecordTypeId ){
                        objApprovalSubmitRequest.setProcessDefinitionNameOrId('Refund_Process_Approval');
                    }*/
                    else{
                        system.debug('*****objC.Percent_to_be_waived__c*****'+objC.Percent_to_be_waived__c);
                        system.debug('*****objC.Assignment_Fee_Approval__c*****'+objC.Assignment_Fee_Approval__c);
                        if(objC.RecordType.DeveloperName != null 
                        && objC.RecordType.DeveloperName.contains('Assignment') 
                        && (objC.Percent_to_be_waived__c == null
                        || (objC.Percent_to_be_waived__c != null 
                            && objC.Assignment_Fee_Approval__c != null 
                            && objC.Assignment_Fee_Approval__c.equalsIgnoreCase('Completed')))){
                            system.debug('inside dynamic approval process*****');
                            objApprovalSubmitRequest.setProcessDefinitionNameOrId('Dynamic_Approval_Process');
                        }else if(objC.RecordType.DeveloperName != null 
                              && objC.RecordType.DeveloperName.contains('Assignment') 
                              && objC.Percent_to_be_waived__c != null 
                                  && objC.Assignment_Fee_Approval__c == null){
                            system.debug('inside dynamic assignment fee waiver approval process*****');
                            objApprovalSubmitRequest.setProcessDefinitionNameOrId('Dynamic_Assignment_Fee_Waiver_Approval');
                        }else{
                            system.debug('inside COMMON dynamic approval process*****');
                            objApprovalSubmitRequest.setProcessDefinitionNameOrId('Dynamic_Approval_Process');
                        }
                    }
                    objApprovalSubmitRequest.setSkipEntryCriteria(true);
                    objApprovalSubmitRequest.setNextApproverIds(new Id[] {objApproverWrapper.approverUserId});
                    
                    // Submit the approval request for the account
                    Approval.ProcessResult result = Approval.process(objApprovalSubmitRequest);
                    system.debug('--result--'+result+'--for case--'+objApproverWrapper.objCase.Id);
                }
                /*else{
                    Error_Log__c objError = new Error_Log__c();
                    objError.Error_Details__c = 'error: REQUIRED_FIELD_MISSING, : [nextApproverIds], Message = No user ID present for Role:'+objApproverWrapper.strUserRole;                    
                    objError.Case__c = objApproverWrapper.objCase.id;
                    lstErrorLog.add(objError);                                              
                }*/
               
            }
            if(lstErrorLog.Size()>0){
                insert lstErrorLog;
            }
        }
    }
    
    @future
    public static void processCallingListApprovals(Set<Id> setCallListId) {
        system.debug('--*processApprovals*--');
        CallApprovalHandler();
    set<Id> setUserIdTORestirct = new set<Id>{'0050Y000002c9nSQAQ','0050Y000002c5p8QAA'};
        List<Calling_List__c> lstCallList = [Select Id,Ownerid, Approving_Authorities__c, RecordTypeId,
                              Approving_User_Role__c, Approving_User_Id__c
                              From Calling_List__c 
                              Where Id IN: setCallListId
                              And Approving_Authorities__c != null
                AND Approving_User_Id__c NOT IN: setUserIdTORestirct ];
        
        if(lstCallList != null && !lstCallList.isEmpty()) {
            List<ApprovalWrapper> lstApprovalWrapper = new List<ApprovalWrapper>();
            
            for(Calling_List__c objCallList: lstCallList) {
                if(String.isNotBlank(objCallList.Approving_Authorities__c)) {
                    
                    String strApprovingUserRole = objCallList.Approving_User_Role__c;
                    Id idApprovingUser = objCallList.Approving_User_Id__c;
                    
                    if(idApprovingUser != null && objCallList.Approving_Authorities__c.containsIgnoreCase(strApprovingUserRole)) {
                        ApprovalWrapper objApprovalWrapper = new ApprovalWrapper();
                        objApprovalWrapper.strUserRole = strApprovingUserRole;
                        objApprovalWrapper.objCallList = objCallList;
                        objApprovalWrapper.approverUserId = idApprovingUser;
                        system.debug('--objApprovalWrapper.approverUserId 1--'+objApprovalWrapper.approverUserId);
                        lstApprovalWrapper.add(objApprovalWrapper);
                    }
                }
            }
            
            system.debug('--lstApprovalWrapper post approver population--'+lstApprovalWrapper);
            List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
            
            for(ApprovalWrapper objApproverWrapper: lstApprovalWrapper) {
                // Create an approval request for the account
                if(objApproverWrapper.approverUserId != null){
                    Approval.ProcessSubmitRequest objApprovalSubmitRequest =
                        new Approval.ProcessSubmitRequest();
                    objApprovalSubmitRequest.setComments('Submitting request for approval.');
                    objApprovalSubmitRequest.setObjectId(objApproverWrapper.objCallList.Id);
                    // Submit on behalf of a specific submitter
                    //objApprovalSubmitRequest.setSubmitterId(lstApprover[0].Id);
                    // Submit the record to specific process and skip the criteria evaluation
                    objApprovalSubmitRequest.setProcessDefinitionNameOrId('Bounced_Cheque_Calling_List_Approval');
                    objApprovalSubmitRequest.setSkipEntryCriteria(true);
                    objApprovalSubmitRequest.setNextApproverIds(new Id[] {objApproverWrapper.approverUserId});
                    
                    // Submit the approval request for the account
                    Approval.ProcessResult result = Approval.process(objApprovalSubmitRequest);
                    system.debug('--result--'+result+'--for calling list--'+objApproverWrapper.objCallList.Id);
                }
                else{
                    /*Error_Log__c objError = new Error_Log__c();
                    objError.Error_Details__c = 'error: REQUIRED_FIELD_MISSING, : [nextApproverIds], Message = No user ID present for Role:'+objApproverWrapper.strUserRole;                    
                    objError.Case__c = objApproverWrapper.objCase.id;
                    lstErrorLog.add(objError);*/                                              
                }
            }
            if(lstErrorLog.Size()>0){
                insert lstErrorLog;
            }
        }
    }
    
    /*public static ID getUserID(ID strUserID,string strRoleName ){
        try{
            ID strManagerID = null;           
            intCounter++;
            system.debug('--intCounter--'+intCounter);
            system.debug('--mapManagerID approval--'+mapManagerID.Size());
            system.debug('--mapManagerRole approval--'+mapManagerRole.Size());
            if(mapManagerID.containsKey(strUserID)){
                strManagerID = mapManagerID.get(strUserID);
                if(mapManagerRole.containsKey(strManagerID)){                    
                    //if(strRoleName == mapManagerRole.get(strManagerID))                    
                    System.debug('--mapManagerRole Name--'+mapManagerRole.get(strManagerID));                   
                    if(mapManagerRole.get(strManagerID).contains(strRoleName)){                        
                        return strManagerID;
                    }
                    else{                       
                        system.debug('-- again getUserID(strManagerID,strRoleName)--'+strManagerID+' , '+strRoleName);
                        ID strMGRID = null;
                        strMGRID = getUserID(strManagerID, strRoleName);
                        if(strMGRID != null){
                            return strMGRID;
                        }
                    }
                }
                else{                    
                    return null;
                }
                return null;
            }
            else{                
                return strManagerID;
            }
            return null;
        }
        catch(exception ex){
            System.debug('--exception.getNumber--'+ex.getLineNumber()+'---'+ex.getMessage());
            return null;
        }
     }*/
    
    @future 
    public static void processCallingListApproval( set<Id> setCallingListIds ) {
        /*system.debug('=== Method processCallingListApproval Called ===');
        list<Calling_List__c> lstCallingListForApproval = CallingListUtility.getCallingLists( setCallingListIds );
        set<String> setRoleNames = new set<String>();
        list<ApprovalWrapper> lstApprovalWrapper = new list<ApprovalWrapper>();
        for( Calling_List__c objCall : lstCallingListForApproval ) {
            if( String.isNotBlank( objCall.Approving_Authorities__c ) ) {
                String strApprovingAuthorities = objCall.Approving_Authorities__c ;
                String strRoleName = strApprovingAuthorities.contains( ',' ) ? 
                                     strApprovingAuthorities.subString( 0, strApprovingAuthorities.indexOf(',') ) :
                                     strApprovingAuthorities ;
                setRoleNames.add( strRoleName );
                lstApprovalWrapper.add( new ApprovalWrapper( strRoleName, objCall ) );  
            }
        }
        if( !setRoleNames.isEmpty() && !lstApprovalWrapper.isEmpty() ) {
            list<User> lstUsers = CallingListUtility.getUserFromRoles( setRoleNames );
            for( User objUser : lstUsers ) {
                for( ApprovalWrapper objWrap : lstApprovalWrapper ) {
                    if( objUser.UserRole.Name != null && objUser.UserRole.Name.equalsIgnoreCase( objWrap.strUserRole ) ) {
                        objWrap.approverUserId = objUser.Id ;
                    }
                }
            }
            system.debug( '=== lstApprovalWrapper ==='+lstApprovalWrapper );
            for( ApprovalWrapper objWrap : lstApprovalWrapper ) {
                if( objWrap.objCallingList != null && objWrap.approverUserId != null ) {
                    Approval.ProcessSubmitRequest objApprovalSubmitRequest = new Approval.ProcessSubmitRequest();
                    objApprovalSubmitRequest.setComments( 'Submitting request for approval.' );
                    objApprovalSubmitRequest.setObjectId( objWrap.objCallingList.Id );
                    objApprovalSubmitRequest.setProcessDefinitionNameOrId( 'Bounced_Cheque_Calling_List_Approval' );
                    objApprovalSubmitRequest.setSkipEntryCriteria( true );
                    objApprovalSubmitRequest.setNextApproverIds( new list<Id>{ objWrap.approverUserId } );
                    Approval.ProcessResult result = Approval.process( objApprovalSubmitRequest );
                }
            }
        }*/
    }
    
    public class ApprovalWrapper {
        public String strUserRole;
        public Id approverUserId;
        public Case objCase;
        public Calling_List__c objCallList;
        //public Calling_List__c objCallingList ;
        
        ApprovalWrapper() {
            
        }
        
        /*ApprovalWrapper( String strUserRole, Calling_List__c objCallingList ) {
            this.strUserRole = strUserRole ;
            this.objCallingList = objCallingList ;
        }*/
    }
}