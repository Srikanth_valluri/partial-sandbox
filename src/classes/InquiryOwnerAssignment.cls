/*************************************************************************************************************************
* Name               : InquiryOwnerAssignment                                                                            *
* Description        : To check the user ranking and limits                                                              *             
* Test class Name    : InquiryOwnerAssignment_Test                                                                       *
* Created Date       : 22/05/2018                                                                                        *
* Created By         : Srikanth V                                                                                        *
* --------------------------------------------------------------------------------------------------------------------   */
public class InquiryOwnerAssignment {
    static Map <Inquiry__c, List <User>> matchingUsers;
    static Inquiry_User_Assignment_Rules__c defaultValues;
    static Map <ID, Inquiry_User_Assignment_Rules__c> userRulesMap;
    static Map <ID, Map <Integer, Integer>> userConsumedInqMap;
    static Set <ID> userIds;
    static Map <Inquiry__c, ID> resultMap;
    static Map <String, List <String>> OwnersFromInquiryHistory;
    static Map <String, String> existingUsersInQueueIds;
    static set <String> queueIds;
    
    public static void checkAlreadyAssignedUsers () {

        existingUsersInQueueIds = new Map <String, String> ();
        
        for (Inquiry_Assignment_Algorithm__c inq : [SELECT User_ID__c, User__c, Assigned_Queue_Ids__c 
                                                    FROM Inquiry_Assignment_Algorithm__c 
                                                    WHERE User_ID__c IN : userIds]) 
        {
            existingUsersInQueueIds.put (inq.User_ID__c, inq.Assigned_Queue_Ids__c);
        }
    }
    // Method to check for the daily, weekly, monthly consumed inquires related to the Inquiries
    public static Map <Inquiry__c, ID> assignOwner (Map <Inquiry__c, List <User>> mathchingUsersFromRuleMap) {
        queueIds = new Set <String> ();
        existingUsersInQueueIds = new Map <String, String> ();
        matchingUsers = new Map <Inquiry__c, List <User>> ();
        matchingUsers = mathchingUsersFromRuleMap;
        defaultValues = new Inquiry_User_Assignment_Rules__c ();
        userRulesMap = new Map <ID, Inquiry_User_Assignment_Rules__c> ();
        userConsumedInqMap = new Map <ID, Map <Integer, Integer>> ();
        resultMap = new Map <Inquiry__c, ID> ();
        userIds = new Set <ID> ();
        defaultValues = [ SELECT Daily__c, Monthly__c, Weekly__c FROM Inquiry_User_Assignment_Rules__c 
                            WHERE Net_Direct_Sales_Rank__c = NULL LIMIT 1 ];
                            
        
        for (Inquiry__c inq : matchingUsers.keySet ()) {
            
            for (User u :matchingUsers.get (inq)) {
                existingUsersInQueueIds.put (u.ID, ''); 
                userIds.add (u.id);
                Map <Integer, Integer> tempMap = new Map <Integer, Integer> ();
                tempMap.put (1, 0); // 1 == Daily
                tempMap.put (2, 0); // 2 == Weekly
                tempMap.put (3, 0); // 3 == Monthly
                
                userConsumedInqMap.put (u.id, tempMap);
            }
        }
        System.Debug (userIds);
        
        for (Inquiry_User_Assignment_Rules__c rule :[SELECT 
                                SetupOwnerId, Activities_Rank__c, 
                                Alphabetic_Al_Rank__c, 
                                Daily__c, Direct_Sales_Rank__c,
                                Doc_Yes_Rank__c, DP_Yes_Rank__c, Monthly__c, 
                                Net_Direct_Sales_Rank__c, Weekly__c
                            FROM 
                                Inquiry_User_Assignment_Rules__c
                            WHERE setupOwnerId IN :userIds Order By Net_Direct_Sales_Rank__c]) {
            userRulesMap.put (rule.setupOwnerId, rule);            
        }                   
        
        for (AggregateResult res : [SELECT Count (ID) total, OwnerID FROM Inquiry__c 
                                    WHERE CreatedDate = TODAY
                                        AND OwnerID IN :userIds
                                        AND Assignment_Queue_ID__c != NULL
                                    GROUP BY OwnerID]) {
            Map <Integer, Integer> dailyLimits = new Map <Integer, Integer> ();
            dailyLimits.put (1, integer.valueOf (res.get ('total')));
            userConsumedInqMap.put (String.valueOf (res.get ('OwnerId')), dailyLimits);

        }

        for (AggregateResult res : [SELECT Count (ID) total, OwnerID FROM Inquiry__c 
                                    WHERE CreatedDate = THIS_WEEK
                                        AND OwnerID IN :userIds
                                        AND Assignment_Queue_ID__c != NULL
                                    GROUP BY OwnerID]) {
            Map <Integer, Integer> weeklyLimits = userConsumedInqMap.get (String.valueOf (res.get ('OwnerId')));
                
            weeklyLimits.put (2, integer.valueOf (res.get ('total')));
            userConsumedInqMap.put (String.valueOf (res.get ('OwnerId')), weeklyLimits);
        }

        for (AggregateResult res : [SELECT Count (ID) total, OwnerID FROM Inquiry__c 
                                    WHERE CreatedDate = THIS_MONTH
                                        AND OwnerID IN :userIds
                                        AND Assignment_Queue_ID__c != NULL
                                    GROUP BY OwnerID]) {
            Map <Integer, Integer> monthlyLimits = userConsumedInqMap.get (String.valueOf (res.get ('OwnerId')));
            monthlyLimits.put (3, integer.valueOf (res.get ('total')));
            userConsumedInqMap.put (String.valueOf (res.get ('OwnerId')), monthlyLimits);
        }
        checkAlreadyAssignedUsers ();
        for (Inquiry__c inq : matchingUsers.keySet ()) {
            resultMap.put (inq, NULL);
            System.Debug ('===='+inq.Assignment_Queue_Id__c );
            List <User> totalUsers = new List <User> ();
            for (User u :matchingUsers.get (inq)) {
                totalUsers.add (u);
            }  
            checkUser (inq, totalUsers, 1);  
        }
        System.Debug (' RESULT ');
        for (Inquiry__c inq :resultMap.keyset ())
            System.Debug (resultMap.get (inq));
        List <Inquiry_Assignment_Algorithm__c> algToUpdate = new List <Inquiry_Assignment_Algorithm__c> ();
        for (String userId :existingUsersInQueueIds.keySet ()) {
            Inquiry_Assignment_Algorithm__c alg = new Inquiry_Assignment_Algorithm__c ();
            alg.User_ID__c = userId;
            alg.User__c = userId;
            alg.Assigned_Queue_Ids__c = existingUsersInQueueIds.get (userId);
            if (alg.Assigned_Queue_Ids__c == ',')
                alg.Assigned_Queue_Ids__c = NULL;
            algToUpdate.add (alg);
        }
        Upsert algToUpdate User_ID__c;
        return resultMap;
    }
    // Method to check for the daily, weekly, monthly Limits based on user ranking from Assignment rule Custom setttings.
    public static void checkUser (Inquiry__c inq, List <User> totalUsers, Integer threshHoldType) {
        Map <Decimal, Id> powerLineUsersMap = new Map <Decimal, Id> ();
        List <Decimal> sortedUsers = new List <Decimal> ();
        System.Debug ('THRESH HOLD COUNT --1 = Daily, 2 = Weekly, 3 = Weekly --'+threshHoldType);
        System.Debug (totalUsers);
        for (User u :totalUsers) {
            if (userRulesMap.containsKey (u.Id)) {
            
                Decimal availableLimit = 0;
                if (threshHoldType == 1) {
                    availableLimit = userRulesMap.get (u.Id).Daily__c;
                    if (availableLimit == NULL) 
                        availableLimit = defaultValues.Daily__c;    
                }
                if (threshHoldType == 2) {
                    availableLimit = userRulesMap.get (u.Id).Weekly__c;
                    if (availableLimit == NULL) 
                        availableLimit = defaultValues.Weekly__c;    
                }
                if (threshHoldType == 3) {
                    availableLimit = userRulesMap.get (u.Id).Monthly__c;
                    if (availableLimit == NULL) 
                        availableLimit = defaultValues.Monthly__c;
                }
                System.Debug (u.ID);
                if (userConsumedInqMap.get (u.ID).get (threshHoldType) < availableLimit) {
                    powerLineUsersMap.put (userRulesMap.get (u.ID).Net_Direct_Sales_Rank__c, u.ID);
                    sortedUsers.add (userRulesMap.get (u.ID).Net_Direct_Sales_Rank__c);
                } 
                
            } 
        }
        sortedUsers.sort ();
        List <ID> powerLineUsersWithOrder = new List <ID> ();
        for (Decimal val :sortedUsers)
            powerLineUsersWithOrder.add (powerLineUsersMap.get (val));
            
        System.Debug (' POWER LINE USERS WHO HAS LIMITS ');
        System.Debug (powerLineUsersWithOrder);
        if (threshHoldType == 3 && powerLineUsersWithOrder.size () == 0) {
            for (User u : totalUsers) {
                String exitingIds = existingUsersInQueueIds.get (u.Id);
                if (exitingIds != '' && exitingIds != NULL) {
                    if (exitingIds.contains (inq.Assignment_Queue_ID__c))
                        exitingIds = exitingIds.remove (inq.Assignment_Queue_ID__c);
                }
                existingUsersInQueueIds.put (u.Id, exitingIds);
            }
        }
        if (powerLineUsersWithOrder.size () == 0 && threshHoldType < 3) {
            checkUser (inq, totalUsers, threshHoldType+1);
        }
        
        if (powerLineUsersWithOrder.size () > 0) {
            List <ID> finalPowerUsers = new List <ID> ();
            for (Id userId :powerLineUsersWithOrder) {
                String exitingIds = existingUsersInQueueIds.get (userId);
                Boolean flag = false;
                if (exitingIds != NULL && exitingIds != '') {
                    if (exitingIds.contains (inq.Assignment_Queue_ID__c)) {
                        flag = true;
                    }
                }
                System.Debug (flag);
                if (!flag) {
                    finalPowerUsers.add (userID);
                }
            }
            System.Debug (finalPowerUsers);
            if (finalPowerUsers.size () > 0) {
                ID powerLineUserID = finalPowerUsers[0];
                System.Debug (powerLineUserID);
                String exitingIds = existingUsersInQueueIds.get (powerLineUserID);
                if (exitingIds == NULL)
                    exitingIds = '';
                existingUsersInQueueIds.put (powerLineUserID, exitingIds +inq.Assignment_Queue_ID__c+',');
                
                if (finalPowerUsers.size () == 1) {                
                    for (User u : totalUsers) {
                        exitingIds = existingUsersInQueueIds.get (u.Id);
                        if (exitingIds != '' && exitingIds != NULL) {
                            if (exitingIds.contains (inq.Assignment_Queue_ID__c))
                                exitingIds = exitingIds.remove (inq.Assignment_Queue_ID__c);
                        }
                        existingUsersInQueueIds.put (u.Id, exitingIds);
                    }
                }
                    
                if (powerLineUserID != NULL) {
                    Map <Integer, Integer> dailyLimits = userConsumedInqMap.get (powerLineUserID);
                    
                    dailyLimits.put (1, dailyLimits.get (1)+1);
                    dailyLimits.put (2, dailyLimits.get (2)+1);
                    dailyLimits.put (3, dailyLimits.get (3)+1);
                    userConsumedInqMap.put (powerLineUserID, dailyLimits);
                    
                    System.Debug (' Daily available = '+userRulesMap.get (powerLineUserID).Daily__c+' Consumed = '+dailyLimits.get (1));
                    System.Debug (' Weekly available = '+userRulesMap.get (powerLineUserID).Weekly__c+' Consumed = '+dailyLimits.get (2));
                    System.Debug (' Monthly available = '+userRulesMap.get (powerLineUserID).Monthly__c+' Consumed = '+dailyLimits.get (3));
                }
                resultMap.put (inq, powerLineUserID);
                
            } else {
                for (User u : totalUsers) {
                    String exitingIds = existingUsersInQueueIds.get (u.Id);
                    if (exitingIds != '' && exitingIds != NULL) {
                        if (exitingIds.contains (inq.Assignment_Queue_ID__c))
                            exitingIds = exitingIds.remove (inq.Assignment_Queue_ID__c);
                    }
                    existingUsersInQueueIds.put (u.Id, exitingIds);
                }
                checkUser (inq, totalUsers, 1);
            }
        }
    }
    // Method to check for the reshufffling logic when Skip power line is TRUE in Rules object and taking the owner from Queue.
    public static Map <Inquiry__c, ID> reshuffleOwner (Map <Inquiry__c, List <User>> mathchingUsersFromRuleMap) {
        matchingUsers = new Map <Inquiry__c, List <User>> ();
        OwnersFromInquiryHistory = new Map <String, List <String>> ();
        matchingUsers = mathchingUsersFromRuleMap;
        resultMap = new Map <Inquiry__c, ID> ();
        Set <ID> inquiryIds = new Set <ID> ();
        for (Inquiry__c inq: matchingUsers.keySet()) {
            inquiryIds.add (inq.ID);
            resultMap.put (inq, null);
            OwnersFromInquiryHistory.put (inq.ID, new List <String> ());
        }
        System.Debug (matchingUsers);
        for (Inquiry__History his : [SELECT ParentId, OldValue, NewValue, Field, CreatedById, CreatedDate 
                                     FROM Inquiry__History 
                                     WHERE parentId IN: inquiryIds 
                                     AND Field = 'Owner'
                                    ]) 
        {
            if (his.oldValue != NULL) {
                if (String.valueOf (his.oldValue).startsWith ('005'))
                    OwnersFromInquiryHistory.get (his.parentId).add (String.valueOf (his.oldValue));
            }
            if (his.NewValue != NULL) {
                if (String.valueOf (his.NewValue).startsWith ('005'))
                    OwnersFromInquiryHistory.get (his.parentId).add (String.valueOf (his.NewValue)); 
            }
        }
        for (Inquiry__c inq: matchingUsers.keySet ()) {
            List <String> alreadyAssignedOwners = new List <String> ();
            alreadyAssignedOwners = OwnersFromInquiryHistory.get (inq.ID);
            System.Debug (alreadyAssignedOwners);
            for (User u :matchingUsers.get (inq)) {
                System.Debug (u.ID);
                System.Debug (alreadyAssignedOwners.contains (u.ID));
                if (!alreadyAssignedOwners.contains (u.ID)) {
                    resultMap.put (inq, u.ID);
                    break;
                }
                
            }
        }
        System.Debug (resultMap);
        for (Inquiry__c inq: resultMap.keySet()) {
            if (resultMap.get (inq) == NULL) {
                if (matchingUsers.get (inq).size () > 0)
                    resultMap.put (inq, matchingUsers.get (inq)[0].ID);
            }
        }
        System.Debug (resultMap);
        return resultMap;
    }
}