public with sharing class MollakService implements Schedulable  {
    
    Static String STR_ENDPOINT      = Label.Mollak_COD_Endpoint;//'https://qagate.dubailand.gov.ae/mollak/external/contact';
    Static String STR_CONSUMERKEY   = Label.Mollak_Consumer_Key;//'KX4mcWElUHodtBInNxNUPqyqrab1TS2V';

    public List<Id> caseList;

    @InvocableMethod
    public Static void getCases( List<Id> caseList ) {

        system.debug('--caseList:--'+caseList);
        MollakService objMollakService = new MollakService();
        objMollakService.caseList = caseList;
        DateTime dt = system.now().addMinutes(1);
        String day = string.valueOf(dt.day());
        String month = string.valueOf(dt.month());
        String hour = string.valueOf(dt.hour());
        String minute = string.valueOf(dt.minute());
        String second = string.valueOf(dt.second());
        String year = string.valueOf(dt.year());
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
        
        String jobID = system.schedule('COCD Mollak Service Callout '+ system.now(), strSchedule, objMollakService);

    }

    public void execute(SchedulableContext ctx) {
        if( caseList != null && caseList.size() > 0 ) {
            callMollakService( caseList );
        }
    }

    @Future(callout=true)
    public Static void callMollakService(List<Id> caseList) {
        if( caseList != null && caseList.size() > 0 ) {
            Set<Id> bookingIdSet = new Set<Id>();
            Set<String> acctIdSet = new Set<String>();
            Map<Id,String> mapBookingIdMollakId = new Map<Id,String>();

            List<Case> lstCase = [Select Id 
                                      , AccountId
                                      , Account.Party_ID__c
                                      , Account.Title__c
                                      , Account.First_Name__c
                                      , Account.Last_Name__c
                                      , Account.IsPersonAccount
                                      , CaseNumber
                                      , Account.Passport_Number__c
                                      , Account.Email__pc
                                      , Account.Title_Arabic__c
                                      , Account.First_Name_Arabic__c
                                      , Account.Last_Name_Arabic__c
                                      , Account.Email__c
                                      , Account.Mobile__c
                                      , Account.Mobile_Phone_Encrypt__pc
                                      , Contact_Mobile__c
                                    
                                   FROM Case
                                  Where Id IN: caseList
                                    AND AccountId != null ];

            for( Case objCase : lstCase  ) {
                if( objCase != null && objCase.AccountId != null ) {
                    acctIdSet.add( String.ValueOf ( objCase.AccountId ).substring(0,15) );
                }
            }
            system.debug('acctIdSet : '+ acctIdSet);
            if( acctIdSet != null && acctIdSet.size() > 0 ) {
                
                List<CRMAllinOne__c> lstCRMAllinOne = [SELECT Id,
                                                                Account__c,
                                                                Account__r.Name,
                                                                Account__r.First_Name_Arabic__c,
                                                                Account__r.Middle_Name_Arabic__c,
                                                                Account__r.Last_Name_Arabic__c,
                                                                Account__r.Mobile_Person_Business__c,
                                                                Account__r.Person_Business_Email__c,
                                                                Account__r.Passport_Number__c,
                                                                Unit__c,
                                                                Inventory__c,
                                                                JB2_Mollak_OwnerNum__c,
                                                                JB2_Party_Id__c,
                                                                JB3_Mollak_OwnerNum__c,
                                                                JB3_Name__c,
                                                                JB3_Party_Id__c,    
                                                                JB_Mollak_OwnerNum__c,
                                                                JB_Name__c,
                                                                JB_Name_2__c,
                                                                JB_Party_Id__c,
                                                                MollakOwnerNum__c,
                                                                MollakUnitId__c,
                                                                Party_Id__c,
                                                                PropertyGrpId__c,   
                                                                Registration_Id__c, 
                                                                Unit_Name__c
                                                        FROM CRMAllinOne__c 
                                                       WHERE Account__c IN : acctIdSet];
             String reqBody;
            
            String primaryBuyerReq=''; 
            String secondaryBuyersReq ='';
            String strEmail ='';
            String strMobile = '';
            String strMollakId = '';
            String strEmiratesId = '';

        if(lstCRMAllinOne != Null && lstCRMAllinOne.size() >0) {
            System.debug('lstCRMAllinOne = '+lstCRMAllinOne );
            System.debug('lstCRMAllinOne[0].MollakOwnerNum__c = '+lstCRMAllinOne[0].MollakOwnerNum__c);
       
            primaryBuyerReq += '{"mollakPropertyId":' +lstCRMAllinOne[0].MollakUnitId__c+ ',';

            primaryBuyerReq += '"name":"' + lstCRMAllinOne[0].Account__r.Name+ '",';

            primaryBuyerReq += '"nameAr":"' + lstCRMAllinOne[0].Account__r.First_Name_Arabic__c +' '+ lstCRMAllinOne[0].Account__r.Middle_Name_Arabic__c +' '+ lstCRMAllinOne[0].Account__r.Last_Name_Arabic__c + '",'; 
        
            primaryBuyerReq += '"Mobile":"' + lstCRMAllinOne[0].Account__r.Mobile_Person_Business__c + '",';

            primaryBuyerReq += '"email":"' + lstCRMAllinOne[0].Account__r.Person_Business_Email__c + '",';
            
            //primaryBuyerReq += '"emirateId": "' + strEmiratesId + '",';
            
            primaryBuyerReq +=  '"passportNumber":"' + lstCRMAllinOne[0].Account__r.Passport_Number__c + '",';
            
            primaryBuyerReq +=  '"ownerNumber":' + lstCRMAllinOne[0].MollakOwnerNum__c ;
            
            primaryBuyerReq += '},';

           } 
            Map<string , string> mapPartyIdMolakId = new Map<string, string>();
            
            for( CRMAllinOne__c objCRMAllinOne : lstCRMAllinOne  ) {
                //TODO  ad null 
                if(String.isNotBlank(objCRMAllinOne.JB_Party_Id__c) 
                    && String.isNotBlank(objCRMAllinOne.MollakUnitId__c) 
                    &&  String.isNotBlank(objCRMAllinOne.JB_Mollak_OwnerNum__c) ){
                        mapPartyIdMolakId.put(objCRMAllinOne.JB_Party_Id__c , 
                            objCRMAllinOne.MollakUnitId__c + '-' +  objCRMAllinOne.JB_Mollak_OwnerNum__c  );
                }
                if(String.isNotBlank(objCRMAllinOne.JB2_Party_Id__c) 
                    && String.isNotBlank(objCRMAllinOne.MollakUnitId__c) 
                    &&  String.isNotBlank(objCRMAllinOne.JB2_Mollak_OwnerNum__c) ){
                    mapPartyIdMolakId.put(objCRMAllinOne.JB2_Party_Id__c ,
                        objCRMAllinOne.MollakUnitId__c + '-' +  objCRMAllinOne.JB2_Mollak_OwnerNum__c  );
                }
                if(String.isNotBlank(objCRMAllinOne.JB3_Party_Id__c) 
                    && String.isNotBlank(objCRMAllinOne.MollakUnitId__c) 
                    &&  String.isNotBlank(objCRMAllinOne.JB3_Mollak_OwnerNum__c) ){
                    mapPartyIdMolakId.put(objCRMAllinOne.JB3_Party_Id__c ,
                        objCRMAllinOne.MollakUnitId__c + '-' +  objCRMAllinOne.JB3_Mollak_OwnerNum__c );
                }
            }
          

            for( Account objAccount : [SELECT Id,
                                                Name,
                                                First_Name_Arabic__c ,
                                                Middle_Name_Arabic__c,
                                                Last_Name_Arabic__c,
                                                Mobile_Person_Business__c,
                                                Person_Business_Email__c,
                                                Passport_Number__c,
                                                Party_ID__c 
                                            FROM Account 
                                            WHERE Party_ID__c IN : mapPartyIdMolakId.keySet() ] ){

                secondaryBuyersReq += '{"mollakPropertyId":' + mapPartyIdMolakId.get(objAccount.Party_ID__c).substringBefore('-') + ',';
                
                secondaryBuyersReq += '"name":"' + objAccount.Name+ + '",';

                secondaryBuyersReq += '"nameAr":"' + objAccount.First_Name_Arabic__c +' '+ objAccount.Middle_Name_Arabic__c +' '+ objAccount.Last_Name_Arabic__c + '",'; 
                
                secondaryBuyersReq += '"Mobile":"' + objAccount.Mobile_Person_Business__c + '",';

                secondaryBuyersReq += '"email":"'+ objAccount.Person_Business_Email__c + '",';
                    
                //secondaryBuyersReq += '"emirateId": "' + strEmiratesId + '",';
                    
                secondaryBuyersReq += '"passportNumber":"' + objAccount.Passport_Number__c + '",';

                secondaryBuyersReq +=  '"ownerNumber":' + mapPartyIdMolakId.get(objAccount.Party_ID__c).substringAfter('-') ;
                
                secondaryBuyersReq += '},';
    

            }

            if( String.isNotBlank( secondaryBuyersReq ) ) {
            secondaryBuyersReq = secondaryBuyersReq.removeEnd(',');
            } else if(  String.isNotBlank( primaryBuyerReq ) ) {
                primaryBuyerReq = primaryBuyerReq.removeEnd(',');
            }

            System.debug('secondaryBuyersReq = ' + secondaryBuyersReq); 
            

            reqBody = '[' + primaryBuyerReq + secondaryBuyersReq  + ']';

            System.debug('reqBody = ' + reqBody ); 

            if(String.isNotBlank(reqBody)){
                makeHttpCallout(reqBody, caseList[0]);
            }
               

                
                
                
                /**
                for( Booking_Unit__c objBu  : [SELECT Id
                                                    , Booking__c 
                                                    , MollakId__c
                                                 FROM Booking_Unit__c
                                                WHERE Account_Id__c IN: acctIdSet
                                                  AND Unit_Active__c = 'Active' 
                                                  AND  MollakId__c != Null ] ) {
                    if( objBu != null  
                     && objBu.Booking__c != null 
                     && String.isNotBlank( objBu.MollakId__c )) {
                        mapBookingIdMollakId.put( objBu.Booking__c,objBu.MollakId__c );
                    }
                }

                System.debug('mapBookingIdMollakId =' + mapBookingIdMollakId);
                if( mapBookingIdMollakId != null && mapBookingIdMollakId.size() > 0 ) {
                    List<Buyer__c> lstBuyers = [SELECT
                                                    Id
                                                    , Primary_Buyer__c
                                                    , Title__c
                                                    , Title_Arabic__c
                                                    , First_Name__c
                                                    , First_Name_Arabic__c
                                                    , Last_Name__c
                                                    , Last_Name_Arabic__c
                                                    , Email__c
                                                    , Mobile_Phone_Encrypt__c
                                                    , Passport_Number__c
                                                    , Party_ID__c //Emirates Id
                                                    , Account__c
                                                    , Account__r.IsPersonAccount
                                                    , Account__r.Email__pc
                                                    , Account__r.Email__c
                                                    , Account__r.Mobile__c
                                                    , Account__r.Mobile_Phone_Encrypt__pc
                                                    , Address__c
                                                    , Booking__c
                                                FROM
                                                    Buyer__c
                                               WHERE Booking__c IN : mapBookingIdMollakId.keySet() ];
                    
                    System.debug('lstBuyers =' + lstBuyers);
                    
                    if( lstBuyers != null && lstBuyers.size() > 0 ) {
                        String reqBody = buildRequestBody(mapBookingIdMollakId,lstBuyers,lstCase);
                        makeHttpCallout(reqBody, caseList[0]);
                    }
                }
            */

            }
        }        
    }

    // Method to get cx present is joint buyer in other units
    /**
    public static string getCxJointBuyer(  List<Case> caseList ) {
        
        String strCxJointBuyersReq ='';

        if( caseList != null && caseList.size() > 0 ) {
            String strEmail ='';
            String strMobile = '';
            String strEmiratesId = '';
            Map<Id,List<Buyer__c>> mapBookingJointBuyers = new Map<Id,List<Buyer__c>>();
            List<Buyer__c> lstBuyers = [SELECT
                                            Id
                                            , Primary_Buyer__c
                                            , Title__c
                                            , Title_Arabic__c
                                            , First_Name__c
                                            , First_Name_Arabic__c
                                            , Last_Name__c
                                            , Last_Name_Arabic__c
                                            , Email__c
                                            , Mobile_Phone_Encrypt__c
                                            , Passport_Number__c
                                            , Party_ID__c //Emirates Id
                                            , Account__c
                                            , Account__r.IsPersonAccount
                                            , Account__r.Email__pc
                                            , Account__r.Email__c
                                            , Account__r.Mobile__c
                                            , Account__r.Mobile_Phone_Encrypt__pc
                                            , Address__c
                                            , Booking__c
                                        FROM
                                            Buyer__c
                                        WHERE Primary_Buyer__c != true
                                          AND First_Name__c != Null
                                          AND Last_Name__c != Null
                                          AND First_Name__c =: caseList[0].Account.First_Name__c 
                                          AND Last_Name__c =: caseList[0].Account.Last_Name__c  ];

            for( Buyer__c objBuyer : lstBuyers ) {
                if( objBuyer != null && objBuyer.Booking__c != null ) {
                    if( mapBookingJointBuyers.containsKey( objBuyer.Booking__c ) 
                     && mapBookingJointBuyers.get( objBuyer.Booking__c ) != null ) {
                        List<Buyer__c> lstJointBuyers = mapBookingJointBuyers.get( objBuyer.Booking__c );
                        lstJointBuyers.add(objBuyer);
                        mapBookingJointBuyers.put( objBuyer.Booking__c,lstJointBuyers );
                    } else {
                        mapBookingJointBuyers.put( objBuyer.Booking__c,new List<Buyer__c>{objBuyer});
                    }
                }
            }
            //Map<String,List<Buyer__c>> mapMollakIdBuyer = new Map<String,List<Buyer__c>>();
            if( mapBookingJointBuyers != null && mapBookingJointBuyers.size() > 0 ) {
                
                for( Booking_Unit__c objBu : [ SELECT Id 
                                                    , MollakId__c
                                                    , Booking__c
                                                 FROM Booking_Unit__c
                                                WHERE Booking__c IN: mapBookingJointBuyers.keySet() 
                                                  AND Unit_Active__c = 'Active' 
                                                  AND MollakId__c != Null
                                              ] ) {
                    if( objBu != null && String.IsNotBlank( objBu.MollakId__c ) ) {
                        if( mapBookingJointBuyers.containskey( objBu.Booking__c ) ) {
                            //List<Buyer__c> lstBuyers = mapBookingJointBuyers.get(objBU.Booking__c);
                            //mapMollakIdBuyer.put(objBu.MollakId__c, lstBuyers);

                            for( Buyer__c objBuyer : mapBookingJointBuyers.get( objBU.Booking__c ) ) {
                                
                                if( objBuyer.Account__c != null ) {
                                    if( objBuyer.Account__r.Email__pc != null || objBuyer.Account__r.Email__c != null  ){
                                        if( objBuyer.Account__r.IsPersonAccount ) {
                                            strEmail = objBuyer.Account__r.Email__pc;  
                                        } else {
                                            strEmail = objBuyer.Account__r.Email__c;
                                        }
                                    }
                                    if( objBuyer.Account__r.Mobile__c != null || objBuyer.Account__r.Mobile_Phone_Encrypt__pc != null  ){
                                        if( objBuyer.Account__r.IsPersonAccount ) {
                                            strMobile = objBuyer.Account__r.Mobile_Phone_Encrypt__pc;
                                        } else {
                                            strMobile = objBuyer.Account__r.Mobile__c;
                                        }
                                    }   
                                }else {
                                    if( String.isNotBlank( objBuyer.Mobile_Phone_Encrypt__c)) {
                                        strMobile = objBuyer.Mobile_Phone_Encrypt__c;
                                    }else {
                                        strMobile = '';
                                    }

                                    if( String.isNotBlank( objBuyer.Email__c)) {
                                        strEmail = objBuyer.Email__c;
                                    }else {
                                        strEmail = '';
                                    }
                                    
                                }


                                
                                strCxJointBuyersReq += '{"mollakPropertyId":' + objBu.MollakId__c + ',';
                                
                                String strTitle =String.isNotBlank(objBuyer.Title__c) ? objBuyer.Title__c : '';
                                String strFirst_Name = String.isNotBlank(objBuyer.First_Name__c) ? objBuyer.First_Name__c : '';
                                String strLast = String.isNotBlank(objBuyer.Last_Name__c) ? objBuyer.Last_Name__c : '';
                                strCxJointBuyersReq +=  '"name":"' + strTitle +' '+ strFirst_Name +' '+ strLast + '",';

                                String strTitle_Arabic =String.isNotBlank(objBuyer.Title_Arabic__c) ? objBuyer.Title_Arabic__c : '';
                                String strFirst_Name_Arabic = String.isNotBlank(objBuyer.First_Name_Arabic__c) ? objBuyer.First_Name_Arabic__c : '';
                                String strLast_Name = String.isNotBlank(objBuyer.Last_Name_Arabic__c) ? objBuyer.Last_Name_Arabic__c : '';
                                strCxJointBuyersReq +=  '"nameAr":"' + strTitle_Arabic +' '+ strFirst_Name_Arabic +' '+ strLast_Name + '",';  

                            
                                strCxJointBuyersReq += '"Mobile":"' + strMobile + '",';

                                strCxJointBuyersReq += '"email":"' + strEmail + '",';
                                
                                strCxJointBuyersReq += '"emirateId": "' + strEmiratesId + '",';
                                
                                String strpassportNumber = String.isNotBlank(objBuyer.Passport_Number__c) ? objBuyer.Passport_Number__c : '';
                                strCxJointBuyersReq +=  '"passportNumber":"' + strpassportNumber + '"';
                                
                                strCxJointBuyersReq += '},';
                            }

                        }
                    }
                }
                

            }

        }
        return strCxJointBuyersReq;
    } 
    */
    /**
    public static string buildRequestBody( Map<Id,String> mapBookingIdMollakId 
                                         , List<Buyer__c> lstBuyers
                                         , List<Case> lstCase ) {

        String reqBody;
        
        String primaryBuyerReq=''; 
        String secondaryBuyersReq ='';
        String strEmail ='';
        String strMobile = '';
        String strMollakId = '';
        String strEmiratesId = '';
        /*for( Case objCase : lstCase ) {

            if(objCase != null ) {

                if( objCase.Account.Email__pc != null || objCase.Account.Email__c != null  ){
                    if(objCase.Account.IsPersonAccount) {
                        strEmail = objCase.Account.Email__pc;  
                    } else {
                        strEmail = objCase.Account.Email__c;
                    }
                }
                if( objCase.Account.Mobile__c != null || objCase.Account.Mobile_Phone_Encrypt__pc != null  ){
                    if(objCase.Account.IsPersonAccount) {
                        strMobile = objCase.Account.Mobile_Phone_Encrypt__pc;
                    } else {
                        strMobile = objCase.Account.Mobile__c;
                    }
                }   


                primaryBuyerReq = '{"mollakPropertyId":' + '10215507' + ',';

                primaryBuyerReq += '"name":"' + objCase.Account.Title__c + objCase.Account.First_Name__c + objCase.Account.Last_Name__c + '",';

                primaryBuyerReq += '"nameAr":"' + objCase.Account.Title_Arabic__c + objCase.Account.First_Name_Arabic__c + objCase.Account.Last_Name_Arabic__c + '",'; 
               
                primaryBuyerReq += '"Mobile":"' + strMobile + '",';

                primaryBuyerReq += '"email":"' + strEmail + '",';

                primaryBuyerReq += '"emirateId": "' + '5345' + '",';

                primaryBuyerReq += '"passportNumber":"' + objCase.Account.Passport_Number__c + '"';

                primaryBuyerReq += '},';
            }
        }*/
        /**
        for( Buyer__c objBuyer: lstBuyers ) {

            if( mapBookingIdMollakId != null && mapBookingIdMollakId.containsKey(objBuyer.Booking__c) ) {
                strMollakId = mapBookingIdMollakId.get(objBuyer.Booking__c);
            }

            if( objBuyer.Account__c != null ) { 
                if( objBuyer.Account__r.Email__pc != null || objBuyer.Account__r.Email__c != null  ){
                    if(objBuyer.Account__r.IsPersonAccount) {
                        strEmail = objBuyer.Account__r.Email__pc;  
                    } else {
                        strEmail = objBuyer.Account__r.Email__c;
                    }
                }
                if( objBuyer.Account__r.Mobile__c != null || objBuyer.Account__r.Mobile_Phone_Encrypt__pc != null  ){
                    if(objBuyer.Account__r.IsPersonAccount) {
                        strMobile = objBuyer.Account__r.Mobile_Phone_Encrypt__pc;
                    } else {
                        strMobile = objBuyer.Account__r.Mobile__c;
                    }
                }   
            }else {
                if( String.isNotBlank( objBuyer.Mobile_Phone_Encrypt__c)) {
                    strMobile = objBuyer.Mobile_Phone_Encrypt__c;
                }else {
                    strMobile = '';
                }

                if( String.isNotBlank( objBuyer.Email__c)) {
                    strEmail = objBuyer.Email__c;
                }else {
                    strEmail = '';
                }
                
            }

            if( objBuyer.Primary_Buyer__c ) {


                primaryBuyerReq += '{"mollakPropertyId":' +strMollakId+ ',';

                String strTitle =String.isNotBlank(objBuyer.Title__c) ? objBuyer.Title__c : '';
                String strFirst_Name = String.isNotBlank(objBuyer.First_Name__c) ? objBuyer.First_Name__c : '';
                String strLast = String.isNotBlank(objBuyer.Last_Name__c) ? objBuyer.Last_Name__c : '';

                primaryBuyerReq += '"name":"' + strTitle +' '+ strFirst_Name +' '+ strLast + '",';

                String strTitle_Arabic =String.isNotBlank(objBuyer.Title_Arabic__c) ? objBuyer.Title_Arabic__c : '';
                String strFirst_Name_Arabic = String.isNotBlank(objBuyer.First_Name_Arabic__c) ? objBuyer.First_Name_Arabic__c : '';
                String strLast_Name = String.isNotBlank(objBuyer.Last_Name_Arabic__c) ? objBuyer.Last_Name_Arabic__c : '';
                
                primaryBuyerReq += '"nameAr":"' + strTitle_Arabic +' '+ strFirst_Name_Arabic +' '+ strLast_Name + '",'; 
               
                primaryBuyerReq += '"Mobile":"' + strMobile + '",';

                primaryBuyerReq += '"email":"' + strEmail + '",';
                
                primaryBuyerReq += '"emirateId": "' + strEmiratesId + '",';
                
                String strpassportNumber = String.isNotBlank(objBuyer.Passport_Number__c) ? objBuyer.Passport_Number__c : '';
                primaryBuyerReq +=  '"passportNumber":"' + strpassportNumber + '"';
                
                primaryBuyerReq += '},';

            } else {

                if( mapBookingIdMollakId != null && mapBookingIdMollakId.containsKey(objBuyer.Booking__c) ) {
                    strMollakId = mapBookingIdMollakId.get(objBuyer.Booking__c);
                }
                secondaryBuyersReq += '{"mollakPropertyId":' + strMollakId + ',';

                String strTitle =String.isNotBlank(objBuyer.Title__c) ? objBuyer.Title__c : '';
                String strFirst_Name = String.isNotBlank(objBuyer.First_Name__c) ? objBuyer.First_Name__c : '';
                String strLast = String.isNotBlank(objBuyer.Last_Name__c) ? objBuyer.Last_Name__c : '';

                secondaryBuyersReq += '"name":"' + strTitle +' '+ strFirst_Name +' '+ strLast + '",';

                String strTitle_Arabic =String.isNotBlank(objBuyer.Title_Arabic__c) ? objBuyer.Title_Arabic__c : '';
                String strFirst_Name_Arabic = String.isNotBlank(objBuyer.First_Name_Arabic__c) ? objBuyer.First_Name_Arabic__c : '';
                String strLast_Name = String.isNotBlank(objBuyer.Last_Name_Arabic__c) ? objBuyer.Last_Name_Arabic__c : '';

                secondaryBuyersReq += '"nameAr":"' + strTitle_Arabic +' '+ strFirst_Name_Arabic +' '+ strLast_Name + '",'; 
               
                secondaryBuyersReq += '"Mobile":"' + strMobile + '",';

                secondaryBuyersReq += '"email":"' + strEmail + '",';
                
                secondaryBuyersReq += '"emirateId": "' + strEmiratesId + '",';
                
                String strpassportNumber = String.isNotBlank(objBuyer.Passport_Number__c) ? objBuyer.Passport_Number__c : '';
                secondaryBuyersReq += '"passportNumber":"' + strpassportNumber + '"';
                
                secondaryBuyersReq += '},';
            }
        }
        String strCxJointBuyersReq = getCxJointBuyer(lstCase);

        if( String.isNotBlank( strCxJointBuyersReq ) ) {
            strCxJointBuyersReq = strCxJointBuyersReq.removeEnd(',');
        } else if( String.isNotBlank( secondaryBuyersReq ) ) {
            secondaryBuyersReq = secondaryBuyersReq.removeEnd(',');
        } else if(  String.isNotBlank( primaryBuyerReq ) ) {
            primaryBuyerReq = primaryBuyerReq.removeEnd(',');
        }


        System.debug('strCxJointBuyersReq = ' + strCxJointBuyersReq); 
        System.debug('secondaryBuyersReq = ' + secondaryBuyersReq); 
        System.debug('primaryBuyerReq = ' + primaryBuyerReq); 



        reqBody = '[' + primaryBuyerReq + secondaryBuyersReq +  strCxJointBuyersReq + ']';
        
        System.debug('built request body = ' + reqBody);
        return reqBody;
    } */

    public Static void makeHttpCallout(String reqBody, Id CaseId) {

        HttpRequest req = new HttpRequest();            
        req.setEndpoint(STR_ENDPOINT);
        req.setMethod('POST');
        req.setBody(reqBody);  
        req.setHeader('Consumer-id', STR_CONSUMERKEY);
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        Http http = new Http();
        HTTPResponse res;
        //res = http.send(req);
        //HTTPResponse res;
        if(Test.isRunningTest()) {
           res = new HttpResponse();
        } else {
           res = http.send(req);
        }
        if( res != null && res.getBody() != null ) {
            Mollak_Details__c objMollak = new Mollak_Details__c ();
            objMollak.Response__c =  res.getBody();
            objMollak.Request__c = reqBody;
            objMollak.Case__c = CaseId;

            if( objMollak != null ){
                insert objMollak;
            }
            System.debug('objMollak - ' + objMollak);
        } else {
            Error_Log__c objErrLog = GenericUtility.createErrorLog( 'Could not found response' , null , null, null , CaseId);

            if( objErrLog != null ){
                insert objErrLog;
            }
        }


    }

    public Class Mollak_Request {
        String mollakPropertyId, name, nameAr, email, emirateId, passportNumber, address;
        Integer ownerNumber;
        
    }

    public Class Mollak_Response {
        String timeStamp,errorMessage, validationErrorsList,additionalData;
        Integer responseCode;
        Mollak_Response_File response;
    }

    public Class Mollak_Response_File {
        String fileURL;
        List<Mollak_Request> contacts;
    }
}