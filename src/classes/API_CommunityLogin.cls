/*
Class Name : API_CommunityLogin
Description : To login the Community user with username and password, sending sessionId, profileName, accountId, contactId, agency name and agent Name
Sample URL : /services/apexrest/login?username=*********@******.com&password=***********&domain=******************* 
Method     : POST   
Test Class : API_Community_Test      
    
*/

@RestResource(urlMapping='/login/*')
global without sharing class API_CommunityLogin{ 

    
    @HttpPost
    global static LoginResponse login() {
         
        LoginResponse objResponse = new LoginResponse();
        String username = RestContext.request.params.get('username');
        User u = new User ();
        try {
            // To check whether requested user is community user or not
            u = [SELECT Profile.Name, AccountId, ContactId,Account.Name,Contact.Name FROM User WHERE userName =: userName AND Profile.Name LIKE '%COMMUNITY%' LIMIT 1];
        } catch (Exception e) {
            objResponse.isSuccess = false;
            objResponse.sessionId = '';
            objResponse.statusMessage = 'UserName is not a valid Community user.';
            objResponse.statusCode = 400;
        }
        if (u.Id != null) {
            String password = RestContext.request.params.get('password');
            string profilesName = 'Customer Community - Super User';
            if (Test.isRunningTest ()) {
                password = 'test@123';
            }
            String domain = RestContext.request.params.get('domain'); 
            
            try{
                // XML String to prepare the request body
                String loginXML = '<?xml version="1.0" encoding="utf-8"?>';  
                loginXML += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:enterprise.soap.sforce.com">';  
                loginXML += '<soapenv:Header>';  
                loginXML += '<urn:LoginScopeHeader>';  
                loginXML += '<urn:organizationId>'+ UserInfo.getOrganizationId() +'</urn:organizationId>';  
                loginXML += '</urn:LoginScopeHeader>';  
                loginXML += '</soapenv:Header>';  
                loginXML += '<soapenv:Body>'; 
                loginXML += '<urn:login>';
                loginXML += '<urn:username>'+ username +'</urn:username>';
                loginXML += '<urn:password>'+ password +'</urn:password>';
                loginXML += '</urn:login>'; 
                loginXML += '</soapenv:Body>';  
                loginXML += '</soapenv:Envelope>';
                 
                System.debug (loginXML);
                
                // HTTP callcout to salesforce Soap API to login user
                HttpRequest request = new HttpRequest();
                request.setEndpoint(domain+'/services/Soap/c/44.0');
                request.setTimeout(60000); 
                request.setMethod('POST');
                request.setHeader('SOAPAction', 'login');
                request.setHeader('Accept','text/xml');
                request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
                request.setBody(loginXML);       
                HttpResponse response = new HTTPResponse ();
                
                if (!Test.isRunningTest ())
                    response = new Http().send(request);
                else {
                    
                    String xml = '<sessionId>test</sessionId><userId>'+userInfo.getUserId ()+'</userId><accountId></accountId>'
                                +'<contactId></contactId>';
                    response.setBody (xml);   
                }
                 
                String responseBody = response.getBody();
                String sessionId = getValueFromXMLString(responseBody, 'sessionId');
                 
                objResponse.statusMessage = response.getStatus();
                objResponse.statusCode = response.getStatusCode();
                 
                if (string.isNotBlank(sessionId)) {
                    objResponse.isSuccess = true;
                    objResponse.sessionId = sessionId;
                    objResponse.userId = u.id;
                    objResponse.accountId = u.accountId;
                    objResponse.contactId = u.contactId;
                    objResponse.profileName = u.profile.Name;
                    objResponse.agentName = u.contact.Name;
                    objResponse.agencyName = u.Account.Name;
                    objResponse.isSuperUser = u.profile.Name.Contains(profilesName)?true:false;//checks whether a Super User or not
                     
                }else{
                    objResponse.isSuccess = false;
                }
            }
            catch(System.Exception ex){
                objResponse.isSuccess = false;
                objResponse.statusMessage = ex.getMessage();
            }
        }
        system.debug('objResponse-' + objResponse);
        
        return objResponse;
    }
    
    
    // Method to parse the response XML 
    public static string getValueFromXMLString(string xmlString, string keyField){
        String xmlKeyValue = '';
        if(xmlString.contains('<' + keyField + '>')){
            try{
                xmlKeyValue = xmlString.substring(xmlString.indexOf('<' + keyField + '>')+keyField.length() + 2, xmlString.indexOf('</' + keyField + '>'));   
            }catch (exception e){
                 
            }            
        }
        return xmlKeyValue;
    }
    
    
    // To send the response back to API  
    global class LoginResponse {
        public String sessionId {get; set;}
        public Boolean isSuccess {get; set;}
        public String statusMessage {get; set;}
        public Integer statusCode {get; set;}
        public ID userId { get; set; }
        public ID accountId { get; set; }
        public ID contactId { get; set; }
        public String profileName { get; set; }
        // Added on April 2
        public String agencyName{get; set;}
        public String agentName{get; set;}
         public Boolean isSuperUser {get; set;}
    }
}