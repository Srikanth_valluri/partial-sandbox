public class tBitsJSON{
    public string result;
    public string errorDescription;
    public string ReferenceID;
    public integer errorCode;
    public list<fieldData> fields;
    public string imageResolution;
    public string Description;
    
    public class fieldData {
        public boolean valid;
        public String name;
        public String text;
        public string text2;
    }
    public static tBitsJSON parse(String json){
        return (tBitsJSON) System.JSON.deserialize(json, tBitsJSON.class);
    }
}