@isTest
public class DAMAC_Unit_PreBooking_TC {
     
   
    static testMethod void DAMAC_Unit_PreBooking_Test() {
    ID recordTypeId = Schema.SObjectType.inventory__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
    Location__c loc = new Location__c ();
        loc.Name = 'India';
        loc.Location_ID__c  = '123456';
    insert loc;
    ID recordTypeIdServiceReq = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();      
        inventory__c inv = new inventory__c ();
            inv.RecordtypeId = recordTypeId;
            inv.CurrencyIsoCode = 'EUR';
        insert inv;
    inventory__c inv1 = new inventory__c ();
        inv1.RecordtypeId = recordTypeId;
        inv1.CurrencyIsoCode = 'EUR';
        inv1.status__c = 'Released';
        inv1.building_location__c = loc.id;
        inv1.id = inv.id;
    upsert inv1;
    String ids = inv.id+',';
    Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
       Account a = new Account();
        a.LastName= 'Test Account';
        a.RecordTypeId = accountRTId;
        a.Agency_Short_Name__c = 'testShrName';
        a.Party_ID__c = '789456';
        a.Email__c = 'test@gmail.com';
        a.Mobile__c = '7894561230';
        a.Title__c = 'Ms.';
        a.Nationality__c = 'Australian';
        a.Passport_Number__c = '123654';
        insert a;
    NSIBPM__Service_Request__c servceReq = new NSIBPM__Service_Request__c ();
        servceReq.Agency__c = a.id;
        servceReq.RecordtypeId = recordTypeIdServiceReq;  
        insert servceReq;
         Booking__c bookng = new Booking__c ();
        bookng.Deal_SR__c = servceReq.id;
        insert bookng;
       set<id> bookingIds = new Set <ID> ();
        bookingIds.add (bookng.Id);
        
        Booking_Unit__c bookngUnit = new Booking_Unit__c ();
        bookngUnit.Booking__c = bookng.id;
        insert bookngUnit; 
    
     DAMAC_Unit_PreBooking prebookng = new DAMAC_Unit_PreBooking ();
         prebookng.selectedInvIds = inv.id;
     DAMAC_Unit_PreBooking.SendPreBookId (ids, '', false, '');
    
    DAMAC_Unit_PreBooking.createpaymentPlanHeader (ids);
    }
}