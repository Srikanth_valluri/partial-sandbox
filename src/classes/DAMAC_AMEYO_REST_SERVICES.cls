/*
Custom Rest Resource for Changing the owner of the Inquiry based on the Fedaration Id
Creating Pre Inquiry for an Inbound Call and Tagging the Campaign
*/

@RestResource(urlMapping='/ameyoAPI/*')
global without sharing class DAMAC_AMEYO_REST_SERVICES{   
    
    @HttpPOST
    global static void doPOST() 
    { 
        try {
            
            RestRequest req = RestContext.request; 
            String reqBody = req.requestBody.toString().trim();
            RestContext.response.addHeader('Content-Type', 'application/json');
            Map<String, Object> reqJson = (Map<String, Object>) JSON.deserializeUntyped(reqBody);  
            
            String inqId = String.valueOf(reqJson.get('inqId'));
            String userID = String.valueOf(reqJson.get('ssoId'));
            
            System.Debug('Inquiry ID::::'+inqId+'::: User ID:::'+userId);            
            
            
            if(inqId != '' && inqId !=null && userID != null && userID !=''){  
                list<user> u =[select id, Profile.name, FederationIdentifier from user where FederationIdentifier=:userID LIMIT 1];  
                if(u.size() > 0){  
                    Inquiry__c inq = [Select id, RecordType.Name from Inquiry__c where id=: inqId];
                    if(System.label.Ameyo_Profile_Check == 'true'){
                        if(inq.RecordType.Name != 'Inquiry' && 
                            u[0].Profile.Name != 'Telesales Team TL' && u[0].Profile.Name != 'Telesales Team TL Lightning'){
                            inq.OwnerId = u[0].id;
                            update inq;
                        }
                    }
                    else{
                        if(inq.RecordType.Name != 'Inquiry'){ 
                            inq.OwnerId = u[0].id;
                            update inq;
                        }
                    }
                    RestContext.response.responseBody = Blob.valueOf('{"Status" : "Success" , "Message" : "Owner update Success."}');
                }
                else{
                    RestContext.response.responseBody = Blob.valueOf('{"Status" : "Error" , "Message" : "No user found with the ssoid"}');
                }
            }
            else{
                RestContext.response.responseBody = Blob.valueOf('{"Status" : "Error" , "Message" : "inqid and ssoid cannot be blank"}');
                
                Ameyo_log__c log = new Ameyo_log__c();
                log.userAssociations__c = 'inqid and ssoid cannot be blank';
                log.CrtObjectId__c = 'inqid and ssoid cannot be blank -'+string.valueof(system.now())+'-'+math.random();
                insert log;
            }            
            
        } Catch(Exception e){
            System.Debug(e.getMessage());            
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf('{"Status" : "Failure" , "Message" : "'+e.getMessage()+' at line Number :'+e.getLineNumber()+'"}');
            
            Ameyo_log__c log = new Ameyo_log__c();
            log.userAssociations__c = '{"Status" : "Failure" , "Message" : "'+e.getMessage()+' at line Number :'+e.getLineNumber()+'"}';
            log.CrtObjectId__c = '{"Status" : "Failure" , "Message" : "'+e.getMessage()+' at line Number :'+e.getLineNumber()+'"}' + '-'+ string.valueof(system.now())+'-'+math.random();
            insert log;
            
        }
    }
    
    
    
    @HttpGET
    global static void doGET(){
        String customerNumber = RestContext.request.params.get('cnum');
        string dialedNumber = RestContext.request.params.get('cdnum');
        string returnResponse = '';
        
        if(customerNumber != null && dialedNumber != null){
            try{
                // Check if the Encrypted Number already Exist in Salesforce
                string checkForMatch = FindInquiry(customerNumber);
                if(checkForMatch =='No Match'){
                    // Get the Campaign Id based on the Dialednumber                
                    list<JO_Campaign_Virtual_Number__c> vNumber = new list<JO_Campaign_Virtual_Number__c>();
                    vNumber = [SELECT Related_Campaign__c, Related_Campaign__r.Campaign_Type_New__c FROM JO_Campaign_Virtual_Number__c WHERE Related_Virtual_Number__r.Name =:dialedNumber LIMIT 1];
                    
                    // Create a Pre inquiry
                    Id preInquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
                    inquiry__c inq = new inquiry__c();
                    inq.RecordTypeId = preInquiryRecordTypeId;
                    //inq.Mobile_phone__c = customerNumber;
                    inq.Mobile_phone__c = Damac_PhoneEncrypt.decryptPhoneNumber(customerNumber);
                    inq.encrypted_mobile__c = customerNumber;
                    if(vNumber.size() > 0){
                        inq.campaign__c = vNumber[0].Related_Campaign__c;
                    } else {
                        inq.Campaign__c = Label.default_Ameyo_Campaign;
                    }
                    
                    inq.Mobile_Phone_Encrypt__c = customerNumber;
                    inq.Primary_Contacts__c = 'Mobile Phone';
                    inq.email__c = 'noemail@ameyoinb.com';
                    
                    inq.Mobile_CountryCode__c = MobileCountryShortCode.MobileCountryShortCodeOutput(Damac_PhoneEncrypt.decryptPhoneNumber(customerNumber));
                    inq.First_Name__c = 'New Incoming Number';
                    inq.last_name__c = 'Ameyo';
                    //inq.ownerid = '00G1w000001m5cn';
                    inq.ownerid = label.Ameyo_Dialer_Queue_Id;
                    inq.ameyo__c = true;  
                              
                    insert inq;
                    system.debug('------->'+inq);
                    returnResponse =  '{"SUCCESS" : "'+inq.id+'"}';
                    RestContext.response.addHeader('Content-Type', 'application/json');        
                    RestContext.response.responseBody = Blob.valueOf(returnResponse);
                }else{ // Match Found
                    returnResponse =  checkForMatch;
                    RestContext.response.addHeader('Content-Type', 'application/json');        
                    RestContext.response.responseBody = Blob.valueOf(returnResponse);
                }
            }catch(exception e){
                returnResponse =  '{"ERROR" : "'+e.getMessage()+'"}';
                RestContext.response.addHeader('Content-Type', 'application/json');        
                RestContext.response.responseBody = Blob.valueOf(returnResponse);
                
                Ameyo_log__c log = new Ameyo_log__c();
                log.userAssociations__c = e.getMessage();
                log.phone__c = customerNumber;
                log.campaignId__c = dialedNumber;
                log.CrtObjectId__c = string.valueof(system.now())+'-'+customerNumber+'-'+dialedNumber;
                insert log;

            }
            
        }else{
            returnResponse =  '{"ERROR" : "'+'Customer Number or Customer Dialed Number cannot be blank'+'"}';
            RestContext.response.addHeader('Content-Type', 'application/json');        
            RestContext.response.responseBody = Blob.valueOf(returnResponse);
            
                Ameyo_log__c log = new Ameyo_log__c();
                log.userAssociations__c = returnResponse;
                log.phone__c = customerNumber;
                log.campaignId__c = dialedNumber;
                log.CrtObjectId__c = string.valueof(system.now())+'-'+customerNumber+'-'+dialedNumber+'-'+returnResponse;
                insert log;
        }
    }
    
    
    public static string FindInquiry(string encryptedMobile){
        system.debug('Entered match check');
        string matchFoundResult = 'No Match';
        list<inquiry__c> foundInq = new list<inquiry__c>();
        String searchQuery = 'FIND \'' + encryptedMobile + '\' IN ALL FIELDS RETURNING  Inquiry__c(Id,Name,Encrypted_Mobile__c,Inquiry_18_Digit__c)';
        
        List<List <sObject>> searchList = search.query(searchQuery);
        system.debug(searchList);
        foundInq = ((List<inquiry__c>)searchList[0]);
        if(foundInq.size() > 0){
            matchFoundResult = '{"SUCCESS" : "'+foundInq[0].id+'"}'; 
        }
        system.debug('----->'+matchFoundResult);
        return matchFoundResult;
    
    }
}