public without sharing class FmcProofOfPaymentController extends FM_ProofOfPaymentController {

    public FmcProofOfPaymentController() {
        super(false);
        if (FmcUtils.isCurrentView('ProofOfPayment')) {
            super();
        }
    }

    protected override void initializeVariables() {
        Map<String, String> params = ApexPages.currentPage().getParameters();
        strCaseId = params.get('id');
        if(String.isBlank(strCaseId)) {
            strAccountId = CustomerCommunityUtils.customerAccountId;
        } else {
            objFMCase = FM_Utility.getCaseDetails( strCaseId );
            strAccountId = objFMCase.Account__c ;
        }
        strSRType = 'Proof_of_Payment';
    }

    @testVisible
    protected override void initializeFMCase() {
        objFMCase = new FM_Case__c();
        objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Proof Of Payment').getRecordTypeId();
        objFMCase.Account__c = strAccountId ;
        objFMCase.Status__c = 'New';
        objFMCase.Request_Type__c = 'Proof Of Payment';
        objFMCase.Origin__c = 'Portal';
        objFMCase.Request_Type_DeveloperName__c = strSRType ;
        //objFMCase.Email__c =
    }

    public void saveAsDraft() {
        PageReference nextPage = savePopCase();
        if (nextPage != NULL) {
            strCaseId = nextPage.getUrl().substringAfter('/');
        }
    }

    public void submitSr() {
        PageReference nextPage = submitPopCase();
        if (nextPage != NULL) {
            strCaseId = nextPage.getUrl().substringAfter('/');
        }
    }

}