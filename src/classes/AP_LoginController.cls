/****************************************************************************************************
 Name          : AP_LoginController
 Description   : Controller for Login Page of Agent Portal  
 Created Date  : 10-12-2018                                                                        
 Created By    : ESPL                                                                              
 --------------------------------------------------------------------------------------------------
 VER   AUTHOR             DATE                  COMMENTS                                                    
 1.0   Bhanu Gupta      10-12-2018            Initial Draft.                                     
****************************************************************************************************/

global without sharing class AP_LoginController {

    public string username                                                              {set; get;}
    public string password                                                              {set; get;}
    public string useremail                                                             {set; get;}
    public boolean rememberMe                                                           {set; get;}
    public string toastrError                                                           {set; get;}
    public string toastrSuccess                                                         {set; get;}
    public string EmailAddress                                                          {set; get;}   
    public string wrongDetails                                                          {set; get;}
    public string noUserName                                                            {set; get;}
    public string wrongUserName                                                         {set; get;}
    public AP_LoginController() {
        rememberMe = false;
        toastrError = '';
        toastrSuccess = '';
        if (ApexPages.currentPage().getCookies().get('username') != NULL)
            username = ApexPages.currentPage().getCookies().get('username').getValue();
        if (ApexPages.currentPage().getCookies().get('password') != NULL) {
            string codedpassword = ApexPages.currentPage().getCookies().get('password').getValue();
            system.debug('codedpassword ::' + codedpassword );
            blob blobcodedpassword =  EncodingUtil.base64Decode(codedpassword);
            password = blobcodedpassword.toString();
            system.debug('password::' + password);
        }
    }

    public PageReference redirectToHome(){
        if(UserInfo.getUserType() != 'Guest'){
            return Page.AP_Dashboard;
        }
        return null;
    }

    global PageReference login() {
        blob blobPassword = blob.valueof(password);
        PageReference pageRef;
        string codedPassword = EncodingUtil.base64Encode(blobPassword);
        //if (rememberMe == true) {
            /*cookie Name = new Cookie('username', Username, null, 365, false);
            cookie PasswordCokies = new Cookie('password', codedPassword, null, 365, false);
            ApexPages.currentPage().setCookies(new Cookie[] {Name, PasswordCokies});*/
        //} 
            System.debug('==username=='+username);
            System.debug('==password=='+password);
            string startURL = ApexPages.CurrentPage().getParameters().get('startURL');
            pageRef = Site.login(username,password,startURL == null ? '/AP_Dashboard' : startURL);
            System.debug('==pageRef=='+pageRef);
            if(pageRef==null){
               //wrongDetails = 'Username or Password is wrong, Please enter a correct details';
               toastrError = 'Username or Password is wrong, Please enter correct details';
            }
        return pageRef;
    }
    
    @RemoteAction
    public static String loginCheck (String userName, String password) {
        
        String contactId = '';
        User  u = new User ();
        system.debug('userName: ' + userName);
        u = [SELECT ContactId FROM User WHERE Username =: userName];
        contactId = u.ContactId;
        system.debug('contactId: ' + contactId);    
        return contactId;
    }

    public PageReference forgotPassword() {
        system.debug('EmailAddress'+EmailAddress);
        toastrError = '';
        useremail = '';
        boolean success = false;
        string strUsername;
        System.debug('==username=='+username);
        if(EmailAddress == null || string.isempty(EmailAddress)){
          //noUserName = 'Please enter username';
          toastrError = 'Please enter username';
          return null;
        }
        if(EmailAddress != null){
            //if(EmailAddress.contains('@')){
                if(!checkUsername(EmailAddress)){
                    toastrError = 'The entered username is wrong. Please enter correct username';
                    //wrongUserName = 'The entered username is wrong. Please enter a correct username';
                    return null;
                }
                //username = model.username;
            /*}else{
                username = fetchUsername(username);
            }*/
            if(EmailAddress!= null){
                toastrError = 'An email is sent to your register email Id';
                system.debug('EmailAddress---'+EmailAddress);
                success = Site.forgotPassword(EmailAddress);
                system.debug('success'+success);
            }
        }
        if (success) {
            /*ApexPages.addMessage(new ApexPages.message(
                ApexPages.Severity.CONFIRM, 'An email is sent to your registered email id'
            ));*/
            toastrError = '';
            toastrSuccess = 'An email is sent to your registered email id';
        }else{
           toastrError = 'Please enter a valid username/PartyId.';
        }
        return null;
    }//End of forgotPassword

    /*public string fetchUsername(String strPartyId){
        list<user> listuser = [SELECT Id,
                                      Username,
                                      Email,
                                      Contact.Account.Party_ID__c
                                FROM  User
                                WHERE IsActive = TRUE
                                  AND Contact.Account.Party_ID__c = :strPartyId];
        system.debug('listuser--'+listUser);
        if(listuser.isEmpty()){
            return null;
        }
        else{
            return listuser[0].Username;
        }
    }*///End of fetchUsername

    public Boolean checkUsername(string username){
        system.debug('username-----'+username);
        list<user> listUser = [SELECT Id,
                                      Email,
                                      Username
                                 FROM User
                                WHERE Username=:username];
        if(listUser.isEmpty()){

            return false;
        }else{
            useremail = listUser[0].email; 
            system.debug('model.listUser[0]----'+listUser[0]);
            system.debug('model.useremail----'+useremail);
            system.debug('model.username----'+listUser);
            return true;
        }
    }//End of checkUsername
}