@isTest
global Class CRM_WA_Message_HttpMock implements HttpCalloutMock{
    
    global HttpResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type','application/json');
        String strResponse = '{"message_uuid":"016930d1-2eb8-48d1-b1fe-011f217ee129"}';


        res.setBody(strResponse );
        res.setStatusCode(200);
        return res;
    }   
}