/*
Created By : Srikanth V
Created Date : 06/02/2020
Description : REST API class to search for the Inventories with campaign type as Villa configuration
                To fetch the payment plans based on invnetory selected
                To search the RM based on the email parameter
                To search the agency, agent based on the contact email address.
*/

@RestResource(urlMapping='/PortalInventoryService/*')
global with sharing class DAMAC_InventoryVillaService{
    global static final String campaignType = 'Villa Configuration';
    
    
    @HttpGET
    global static void getInventory() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        
        // To fetch the payment plans based on Inventory id parameter
        if (req.requestURI == '/PortalInventoryService/plans') {
            String invId = req.params.get('invId');
            Inventory__c inventory = [SELECT Building_Location__c FROM Inventory__c WHERE ID =: invId];
            ID locationId = inventory.Building_Location__c;
            String query = 'SELECT '+getAllFields('Payment_plan__c')
                                +', (SELECT '+getAllFields ('Payment_Terms__c')+' FROM Payment_Terms__r) '
                                +' FROM Payment_plan__c'
                                +' WHERE Active__c = TRUE AND Building_Location__c =:locationId';
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeFieldName('PaymentPlan');
            gen.writeStartArray();
            
            System.debug(query);
            for (Payment_plan__c plan :Database.query (query)) {
                gen.writeStartObject();
                for (String field : getAllFields ('Payment_Plan__c').split (',')) {  
                    try {                
                        gen.writeStringField(field.trim(), ''+plan.get (field.trim()));                    
                    } catch (Exception e) {
                        System.debug (e.getMessage());
                    }
                }

                    gen.writeFieldName('PaymentTerms');
                    gen.writeStartArray();
                    for (Payment_Terms__c term :plan.Payment_Terms__r) {
                         gen.writeStartObject();
                         for (String field : getAllFields ('Payment_Terms__c').split (',')) {  
                            try {                
                                gen.writeStringField(field.trim(), ''+term.get (field.trim()));                    
                            } catch (Exception e) {
                                System.debug (e.getMessage());
                            }
                        }
                        gen.writeEndObject(); 
                    }
                    gen.writeEndArray();
                    gen.writeEndObject();
                
            }
            gen.writeEndArray();
            gen.writeEndObject();
            String jsonData = gen.getAsString();     
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(jsonData);
        }
        
        
        // To search the inventories whose campaign type is Villa configuration
        if(req.requestURI == '/PortalInventoryService/searchInventory') {
            
            SET <ID> campaignInventories = new SET <ID> ();
            for (Campaign_Inventory__c inv :[SELECT Inventory__c FROM Campaign_Inventory__c WHERE Campaign__r.Campaign_Type_New__c =: campaignType ]) {
                campaignInventories.add (inv.Inventory__c );
            }
            System.debug (campaignInventories);
            
            List <inventory__c> invReleased = [SELECT Name FROM Inventory__c WHERE Status__c='Released' AND ID IN :campaignInventories];
            List <inventory_user__c> activeInvUser = [SELECT Inventory__c FROM Inventory_user__c WHERE Inventory__c IN :invReleased];
            Set <Id> excludeInvIds = new Set <Id>();
            for(inventory_user__c i :activeInvUser ){
                excludeInvIds.add (i.inventory__c);
            }
            
            List <Inventory__c> results = new List <Inventory__c> ();
            results = Database.query('SELECT '+getAllFields ('Inventory__c') 
                        +' FROM '
                           +' Inventory__c'
                        +' WHERE '                           
                            +' Id not IN:excludeInvIds AND ID IN: campaignInventories '
                        +' Order BY Unit_Released_Date_from_IPMS__c DESC, '
                            +' Marketing_Name__c, Unit_Name__c, District__c, '
                            +' Property_City__c, Price__c, Area_sft__c DESC ');
            
            JSONGenerator gen = JSON.createGenerator(true);
            
            gen.writeStartArray();
            for (Inventory__c inv :results) {
                gen.writeStartObject();
                for (String field : getAllFields ('Inventory__c').split (',')) {  
                    try {                
                        gen.writeStringField(field.trim(), ''+inv.get (field.trim()));                    
                    } catch (Exception e) {
                        System.debug (e.getMessage());
                    }
                }
                gen.writeEndObject();
                
            }
            gen.writeEndArray();
            
            String jsonData = gen.getAsString();  
            
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(jsonData);             
        }
        
        
        // To search for the RM user based on the email address.
        if(req.requestURI == '/PortalInventoryService/searchRM') {
            String emailId = req.params.get('email');
            String error = '';
            try {
                User userRMObj = [SELECT Id FROM user WHERE Email =:emailId 
                                            AND IsActive = TRUE 
                                            AND ( Profile.Name LIKE '%Property Consultant%' OR Profile.Name LIKE '%Agent Executive Team%' )
                                            LIMIT 1];
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody = Blob.valueOf('{"userId" : "'+userRMObj.id+'"}');
            } catch (Exception e) {
                error = Label.Validate_RM_Email;
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody = Blob.valueOf('{"userId" : "", "Error" : "'+error+'"}');
            }
            
        }
        
        // To search for the agent based on the email address provided.
        if(req.requestURI == '/PortalInventoryService/searchAgent') {
            String emailId = req.params.get('email');
           
            Contact contact = new Contact ();
            for (Contact con :[SELECT Email, AccountId FROM Contact WHERE Email =: emailId]) {
                contact = con;
            }
            Boolean isActive = false;
            for (User u :[ SELECT Name, IsActive FROM User WHERE contactId =: contact.Id AND IsActive = TRUE LIMIT 1]) {
                if (!u.isActive) {
                    isActive = false;
                } else 
                    isActive = true;
            }
            if (isActive) {
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody = Blob.valueOf('{"agentId" : "'+contact.id+'", "agencyId" : "'+contact.accountId+'"}'); 
                
            }
            else {
                
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody = Blob.valueOf('{"agentId" : "", "agencyId" : "", "Error" : "There is no agency or agent found with this Email."}'); 
            }
        }
    }
    
    
    //Method to fetch the accessible fields based on the object name parameter
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);        
        if (objectType == null)
            return fields;        
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();        
        for (Schema.SObjectField sfield : fieldMap.Values()) { 
            if(sfield.getDescribe().isAccessible() ){           
                fields += sfield.getDescribe().getName ()+ ', '; 
            }  
            
              
        }
        return fields.removeEnd(', '); 
    }
    
    
}