public class Damac_ResetRMSchedular implements Schedulable {
	public void execute (SchedulableContext SC) {
        List <Inquiry_Assignment_Algorithm__c> algToUpdate = new List <Inquiry_Assignment_Algorithm__c> ();
        for (Inquiry_Assignment_Algorithm__c alg : [SELECT RM_Assigned__c, RM_User_ID__c 
                                                    FROM Inquiry_Assignment_Algorithm__c 
                                                    WHERE RM_Assigned__c = TRUE]) {
            alg.RM_Assigned__c = false;
            algToUpdate.add (alg);
        }        
        Upsert algToUpdate RM_User_ID__c;

        Damac_ResetRMSchedular resetPowerLine = new Damac_ResetRMSchedular();
        date nextScheduledDate = System.today().addDays(5);
        integer day = nextScheduledDate.day();
        integer month = nextScheduledDate.month();
        integer year = nextScheduledDate.year();
        String strSchedule = '0 0 12 '+ day + ' ' + month + ' ?' + ' *';
        
        if(!test.isRunningTest()){
            System.schedule('Reset RM Powerline - '+day+'/'+month+'/'+year, strSchedule, resetPowerLine);
            System.abortJob(sc.getTriggerId());
        }
                    
    }
}