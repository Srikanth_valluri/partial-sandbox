/************************************************************************************************************************
* Name               : ReshufflePreinquiries                                                                            *
* Description        : Assign the TSA Team Inquiries                                                                    *
* Created Date       : 04-03-2018                                                                                       *
* Created By         : ESPL                                                                                             *
* ----------------------------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                                    *
* 1.0         Craig Lobo    04-03-2018      Initial Draft.                                                              *
************************************************************************************************************************/
global class ScheduleTSATeamInquiriesAssignment implements Schedulable {
    global void execute(SchedulableContext SC) {
        /*AssignTSATeamInquiriesBatch assignTSABatch = new AssignTSATeamInquiriesBatch();
        Database.executeBatch(assignTSABatch,100);*/
    }
}// End of class.