/*
    Apex Class : Damac_WeChatServices 
    Test Class Name :Damac_WeChatServices_Test
    Functionality : Rest resource class to be used as webhook between smooch and salesforce.
*/

@RestResource(urlMapping='/wechatResponse/*')
global class Damac_WeChatServices {
    
    @HttpPOST
    global static void SMSPOSTResponse () {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        System.Debug (req.requestBody.toString());
        String reqBody = req.requestBody.toString();
        reqBody = reqBody.replace ('"trigger"', '"res_trigger"');
        reqBody = reqBody.replace ('"_id"', '"res_id"');
        reqBody = reqBody.replace ('"id"', '"res_id"');
        reqBody = reqBody.replace ('"lat"', '"res_lat"');
        reqBody = reqBody.replace ('"long"', '"res_long"');
        WeChat_RESP obj = WeChat_RESP.parse (reqBody);
        System.Debug (obj);
        WeChat_API_Settings__c settings = WeChat_API_Settings__c.getInstance (UserInfo.getUserId ());
        // To create/update an Inquiry when user follows the Wechat
        if (obj.appUser.conversationStarted) {
            String wechatUserId = obj.appUser.res_id;
            String ownerId = [select Id from Group where  Type = 'Queue' AND NAME = 'Wechat Inquiries'].ID;
            userDetails details = getUserDetails (wechatUserId);
            try {
                updateMessagedeliveryStatus (obj.message.res_id, obj.res_trigger);
            } catch (Exception e) {}
            //String wechatOpenId = details.openId;
            try {
                Inquiry__c inq = new Inquiry__c ();
                try {
                    inq = [SELECT ID, Name, Wechat_followup_Sent__c, Owner.Email from Inquiry__c where Wechat_ID__c =: wechatUserId Order By CreatedDate DESC LIMIT 1];                    
                } catch (Exception e) {}
                inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
                inq.Campaign__c = settings.Campaign__c;
                inq.Last_Name__c = details.surname;
                inq.OwnerId = ownerId;
                inq.First_Name__c = details.givenName;
                inq.Email__c = details.email;
                inq.Wechat_ID__c = wechatUserId;
                if (inq.Id == NULL) {
                    insert inq;
                    sendInquiryCreationMsg  (weChatUserId, inq.ID);
                }
                else {
                    if (inq.Wechat_followup_Sent__c == false) {
                        sendInquiryCreationMsg  (weChatUserId, inq.ID);
                        createCustomWeChatMenu ();
                    }
                }
                
                
            } catch (Exception e) {
                System.debug (e.getMessage());
            }
        }
        // To create inbound message record when user replies
        if (obj.messages != NULL) {
            if (obj.messages[0].source != NULL) {
                if (obj.messages[0].source.type != 'api') {
                    System.Debug (obj.appUser.res_id);
                    Wechat_Message__c msg = new Wechat_Message__c ();
                    String ownerId = [select Id from Group where  Type = 'Queue' AND NAME = 'Wechat Inquiries'].ID;
                    Inquiry__c inq = new Inquiry__c ();
                    try {
                        msg = [SELECT OwnerId, Last_Message_Sent__c FROM Wechat_Message__c WHERE Wechat_ID__c =: obj.appUser.res_id order By Last_Message_Sent__c DESC LIMIT 1];
                        ownerId = msg.OwnerID;
                    }catch (Exception e) {
                        
                        
                        try {
                            inq = [SELECT Name, Wechat_followup_Sent__c , Wechat_ID__c, OwnerID, Owner.Email from Inquiry__c where Wechat_ID__c =: obj.appUser.res_id];
                            ownerId = inq.OwnerID;
                        } catch (Exception ex) {
                            // Creating an inquiry if there is no inquiry with the wechat id
                            inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
                            inq.OwnerId = ownerId;
                            inq.Campaign__c = settings.Campaign__c;
                            inq.Wechat_ID__c = obj.appUser.res_id;
                            insert inq;
                            if (inq.Wechat_followup_Sent__c == false) {
                                sendInquiryCreationMsg(obj.appUser.res_id, inq.ID);
                                createCustomWeChatMenu ();
                            }
                        }
                        
                        msg = new Wechat_Message__c ();
                        msg.Last_Message_Sent__c = DateTime.Now ();
                        msg.Outbound_Success__c = false;
                        msg.Record_Id__c = inq.Id;
                        msg.Inquiry__c = inq.Id;
                        msg.OwnerId = ownerid;
                        msg.Wechat_ID__c = obj.appUser.res_id;
                        msg.Unique_Id__c = obj.appUser.res_id+ownerId;
                        upsert msg Unique_Id__c ;
                        
                    }
                    Wechat_Deliverability__c delivery = new Wechat_Deliverability__c ();
                    delivery.Message_Source_Type__c = 'Inbound';
                    delivery.Message_Author_Id__c = obj.messages[0].authorId;
                    delivery.Message_Id__c = obj.messages[0].res_Id;
                    delivery.Message_Text__c = obj.messages[0].text;
                    delivery.Message_Type__c = obj.messages[0].type;
                    System.Debug (ownerId);
                    if (Test.isRunningTest ())
                        ownerId = Userinfo.getUserId ();
                    delivery.OwnerId = OwnerId;
                    delivery.Message_Media_Type__c = obj.messages[0].mediaType;
                    delivery.Message_Media_URL__c = obj.messages[0].mediaUrl;
                    try {
                        delivery.Message_Coordinates__Latitude__s = obj.messages[0].coordinates.res_lat;
                        delivery.Message_Coordinates__Longitude__s = obj.messages[0].coordinates.res_lat;
                    } catch (Exception e) {}
                    delivery.Response__c = reqBody;
                    delivery.Wechat_Message__c = msg.Id;
            
                    insert delivery;
                    try {
                        // Sending an email notification to inquiry owner when ever user sends a message in wechat
                        inq = [SELECT Name, Wechat_ID__c, OwnerID, Owner.Email from Inquiry__c where Wechat_ID__c =: obj.appUser.res_id];
                        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        
                        List<String> sendTo = new List<String>();
                        sendTo.add(inq.Owner.Email);
                        mail.setToAddresses(sendTo);
                        
                        mail.setSubject('Wechat Inbound Notification.');
                        String body = 'Hi, <br/> You have recived an incoming we chat message for '+inq.Name+'<br/>'+obj.messages[0].text;
                        mail.setHtmlBody(body);
                        mails.add(mail);
                        if (inq.Owner.Email != NULL)
                            Messaging.sendEmail(mails);
                    } catch (Exception e) {}
  
                }
            }
        }
    }
    
    public static void updateMessagedeliveryStatus (String messageId, String status) {
        Wechat_Deliverability__c delivery = new Wechat_Deliverability__c ();
        try {
            if (status == 'message:delivery:failure') 
                status = 'Failed';
            if (status == 'message:delivery:user') 
                status = 'Delivered';
            if (status == 'message:appUser')
                status = 'Read';
            
            delivery = [SELECT Message_Id__c, Message_Status__c FROM Wechat_Deliverability__c WHERE Message_Id__c =: messageId LIMIT 1];
            delivery.Message_Status__c = status;
            update delivery;
        } catch (Exception e) {
        
        }
    }
    
    
    // To send automated message with the public vf page link to fill the inquiry details, when user follows in wechat
    @Future (CallOut = TRUE)
    public static void sendInquiryCreationMsg (String userweChatId, String inqId) {
         
        Inquiry__c inq = new Inquiry__c ();
        inq = [SELECT Wechat_followup_Sent__c FROM Inquiry__c WHERE Id =: inqId ];
        if (inq.Wechat_followup_Sent__c == false) {
            WeChat_API_Settings__c settings = WeChat_API_Settings__c.getInstance (UserInfo.getUserId ());
            String msg = settings.Automated_Message_Text__c;
        
            String url = settings.Automated_Message_URL__c+'?id='+InqId;
            String jwtAuthorization = 'Bearer '+getAuthentication (settings);
            HTTPRequest req = new HTTPRequest ();
            req.setEndpoint(settings.Endpoint_URL__c+'/v1.1/apps/'
                            +settings.app_Id__c+'/appusers/'
                            +userWechatId+'/messages');
            req.setMethod ('POST');
            String msgBody = msg+' '+url;
            String reqBody = '';
            
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('text', msgBody);
            gen.writeEndObject();
            String textVal = gen.getAsString();
            textVal = textVal.removeStart ('{').removeEnd ('}');
            
            
            reqBody = '{'+textval+', "role": "appMaker", "type": "text", "destination" : {"integrationType" : "wechat"}}';
            System.Debug (reqBody);
            req.setBody (reqBody);
            req.setHeader('Authorization', jwtAuthorization);
            req.setHeader('Content-Type', 'application/json');
            
            Http http = new Http ();
            HttpResponse res = new HttpResponse ();
            if (!Test.isRunningTest())
                res = http.send (req);
            if (res.getStatusCode () == 201) {
                inq.Wechat_followup_Sent__c = true;
                update inq;
            }
        } 
    }
    
    // Method to send custom menu display on wechat page, it will refresh every time when user follows and salesforce receives first wechat message
    @Future (CallOut = TRUE)
    public static void createCustomWeChatMenu () {
        List <Decimal> menuOrderList = new List <Decimal> ();
        Map <Decimal, WeChat_Custom_Menu__mdt> mainMenuMap = new Map <Decimal, WeChat_Custom_Menu__mdt> ();
        Map <Decimal, List <Map <Decimal, WeChat_Custom_Menu__mdt>>> customMenuMap = new Map <Decimal, List <Map <Decimal, WeChat_Custom_Menu__mdt>>> ();
        for (WeChat_Custom_Menu__mdt menu : [SELECT Main_Menu_Order__c, Sub_Menu_Order__c, Sub_Menu_Text__c, Main_Menu_Text__c, Type__c, URI__c FROM WeChat_Custom_Menu__mdt WHERE URI__c != NULL Order BY Main_Menu_Order__c DESC]) {
            mainMenuMap.put (menu.Main_Menu_Order__c, menu);
            if (!menuOrderList.contains (menu.Main_Menu_Order__c))
                menuOrderList.add (menu.Main_Menu_Order__c);
            if (menu.Sub_Menu_Text__c != NULL) {
                Map <Decimal, WeChat_Custom_Menu__mdt> innerMap = new Map <Decimal, WeChat_Custom_Menu__mdt> ();
                innerMap.put (menu.Sub_Menu_Order__c, menu);
                if (customMenuMap.containsKey (menu.Main_Menu_Order__c))
                    customMenuMap.get (menu.Main_Menu_Order__c).add (innerMap);
                else
                    customMenuMap.put (menu.Main_Menu_Order__c, new List <Map <Decimal, WeChat_Custom_Menu__mdt>> {innerMap});
            }
        
        }
        menuOrderList.sort ();
        Map <Decimal, String> mainMenuJsonMap = new Map <Decimal, String> ();
        for (Decimal key :customMenuMap.keySet ()) {
            String innerMsg = '"items": [';
            for (Map <Decimal, WeChat_Custom_Menu__mdt> innerMap : customMenuMap.get (key)) {
               
                for (Decimal innerKey :innerMap.keySet ()) {
                    WeChat_Custom_Menu__mdt menu = innerMap.get (innerKey);
                    String url = menu.URI__c;
                    
                    JSONGenerator gen = JSON.createGenerator(true);
                    gen.writeStartObject();
                    
                    if (menu.Sub_Menu_Text__c != NULL) {
                        gen.writeStringField('text', menu.Sub_Menu_Text__c);                       
                    } else {
                        gen.writeStringField('text', menu.Main_Menu_Text__c);                        
                    }
                    gen.writeStringField('type', menu.Type__c);
                    gen.writeStringField('uri', url);
                    gen.writeEndObject();
                    String textVal = gen.getAsString();
                    
                    innerMsg += textVal+',';        
                }                
            }
            innerMsg = innerMsg.removeEnd (',');
            innerMsg += ']}';
            mainMenuJsonMap.put (key, '{"type": "submenu", "text" :"'+mainMenuMap.get (key).Main_Menu_Text__c+'",'+innerMsg);
        }
        
        String msgBody = '{"items": [';
        
        for (Decimal key :menuOrderList) {
            if (mainMenuJsonMap.containsKey (key))
                msgBody += mainMenuJsonMap.get (key)+',';
            else {
             
                String url = mainMenuMap.get (key).URI__c;
                msgBody += '{"text" : "'+mainMenuMap.get (key).Main_Menu_Text__c+'", "type" : "'+mainMenuMap.get (key).Type__c+'", "uri": "'+url+'"},';
            }

        }
        msgBody = msgBody.removeEnd (',');
        msgBody += ']}';
        System.Debug (msgBody);
        WeChat_API_Settings__c settings = WeChat_API_Settings__c.getInstance (UserInfo.getUserId ());
        String jwtAuthorization = 'Bearer '+getAuthentication (settings);
        HTTPRequest req = new HTTPRequest ();
        req.setEndpoint(settings.Endpoint_URL__c+'/v1.1/apps/'
                        +settings.app_Id__c+'/menu');
        req.setMethod ('PUT');
        System.Debug (jwtAuthorization);
        req.setHeader('Authorization', jwtAuthorization);
        req.setHeader('Content-Type', 'application/json');
        req.setBody (msgBody);
        Http http = new Http ();
        HttpResponse res = new HttpResponse ();
        if (!Test.isRunningTest ())
            res = http.send (req);
        System.Debug (res.getBody());
        
    }
    
     public class userDetails {
        public String surname;
        public String givenName;
        public String email;
    }
    // To get user details when user follows in wechat
    public static userDetails getUserDetails (String userWechatId) {
        WeChat_API_Settings__c settings = WeChat_API_Settings__c.getInstance (UserInfo.getUserId ());
        String jwtAuthorization = 'Bearer '+getAuthentication (settings);
        HTTPRequest req = new HTTPRequest ();
        req.setEndpoint(settings.Endpoint_URL__c+'/v1.1/apps/'
                        +settings.app_Id__c+'/appusers/'
                        +userWechatId);
        req.setMethod ('GET');
        req.setHeader('Authorization', jwtAuthorization);
        req.setHeader('Content-Type', 'application/json');
        Http http = new Http ();
        HttpResponse res = new HttpResponse ();
        if (!Test.isRunningTest ())
            res = http.send (req);
        else {
            
            res.setStatusCode (200);
            res.setBody ('{"id" : "test", "_id" : "test", "appUser" : {"givenName" : "test", "surName" : "test", "email" : "test@test.com"}}');
        }
        System.Debug (res.getBody());
        userDetails details = new userDetails ();
        if (res.getStatusCode () == 200) {
            String reqBody = res.getBody();
            reqBody = reqBody.replace ('"id"', '"res_id2"');
            reqBody = reqBody.replace ('"_id"', '"res_id"');
            WeChat_User_RESP obj = WeChat_User_RESP.parse (reqBody);
            details.givenName = obj.appUser.givenName;
            details.surname = obj.appUser.surName;
            details.email = obj.appUser.email;
            
        }
        return details;
        
    }
    // Method to send message to wechat user from salesforce
    public static void PostMessage (String recordId, String userWechatId, String msgBody, String msgType, String msgLink) {
        WeChat_API_Settings__c settings = WeChat_API_Settings__c.getInstance (UserInfo.getUserId ());
        String jwtAuthorization = 'Bearer '+getAuthentication (settings);
        HTTPRequest req = new HTTPRequest ();
        req.setEndpoint(settings.Endpoint_URL__c+'/v1.1/apps/'
                        +settings.app_Id__c+'/appusers/'
                        +userWechatId+'/messages');
        req.setMethod ('POST');
        String reqBody = '';
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        msgBody = msgBody.replace (msgLink, '');
        gen.writeStringField('text', msgBody);
        gen.writeEndObject();
        String textVal = gen.getAsString();        
        textVal = textVal.removeStart ('{').removeEnd ('}');
        
        reqBody = '{'+textVal+', "role": "appMaker", "type": "'+msgType+'", ';
        if (msgLink != '' && msgLink != NULL) 
            reqBody += '"mediaUrl": "'+msgLink+'", ';
        
        reqBody += '"destination" : {"integrationType" : "wechat"}}';
        
        System.Debug (':::: Request Body ::::'+reqBody);
        req.setBody (reqBody);
        req.setHeader('Authorization', jwtAuthorization);
        req.setHeader('Content-Type', 'application/json');
        
        Http http = new Http ();
        HttpResponse res = new HttpResponse ();
        if (!Test.isRunningTest())
            res = http.send (req);
        else {
            res.setStatusCode (201);
            res.setBody ('{"message" : {"mediaType" : "image"}}');
        }
        System.Debug (res.getBody());
        if (res.getStatusCode () == 201) {
            String resBody = res.getBody ();
            resBody = resBody.replace ('"_id"', '"res_id"');
            Damac_MSG_RESP obj = Damac_MSG_RESP.parse(resBody);
            String mediaType = obj.message.mediaType;
            Wechat_Message__c msg = new Wechat_Message__c ();
            msg.Last_Message_Sent__c = DateTime.Now ();
            msg.Outbound_Success__c = true;
            msg.Record_Id__c = recordId;
            msg.Inquiry__c = recordId;
            msg.Wechat_ID__c = userWechatId;
            msg.Unique_Id__c = userWeChatId+UserInfo.getUserId ();
            upsert msg Unique_Id__c ;
            
            Wechat_Deliverability__c delivery = new Wechat_Deliverability__c ();
            delivery.Message_Source_Type__c = 'Outbound';
            delivery.Message_Author_Id__c = userWeChatId;
            delivery.Message_Id__c = obj.message.res_id;
            delivery.Message_Status__c = 'Sent';
            delivery.Message_Text__c = msgBody;
            delivery.Message_Type__c  = msgType;
            delivery.Message_Media_Type__c = mediaType;
            delivery.Message_Media_URL__c = msgLink;
            if (mediaType != '' && mediaType != NULL) {
                if (mediaType.contains ('image'))
                    delivery.Message_Type__c = 'image';
            }
            delivery.Wechat_Message__c = msg.Id;

            insert delivery;
        }
        
        
    }
    // To generate JWT token for authentication
    public static String getAuthentication (WeChat_API_Settings__c settings) {
        String JWTAssertion = '';        
        String JWTHeader = '{"alg": "HS256","typ": "JWT", "kid" : "'+settings.API_Key__c+'"}';
        JWTHeader = base64URLencode (Blob.valueOf(JWTHeader));        
        String JWTClaim = '{"scope": "app"}';
        JWTClaim = base64URLencode (Blob.valueOf(JWTClaim));
        
        /*Blob JWTSignature = Crypto.sign('RSA-SHA256', 
                                        Blob.valueof(JWTHeader +'.'+JWTClaim), 
                                        Blob.valueof(settings.Secret_Key__c));
                                        
        System.Debug (base64URLencode (JWTSignature));
        JWTAssertion = JWTHeader +'.'+JWTClaim +'.'+base64URLencode (JWTSignature);*/
        System.Debug (JWTAssertion);
        JWTAssertion = settings.JWT_Token__c;
        //'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImFwcF81YzE3NGZjNTgwN2NlMDAwMjNhZmYwOTcifQ.eyJzY29wZSI6ImFwcCJ9.kDtcKr5Zkt-kqcN6EhsjKmby2pd6p5Rua12bHv7BXqk';
        return JWTAssertion;
        
    }

    public static String prepareStringForBase64Decoding(String stringToPrepare) {
        String last4Bytes = stringToPrepare.substring(stringToPrepare.length() - 4, stringToPrepare.length());
        if (last4Bytes.endsWith('==')) {
            last4Bytes = last4Bytes.substring(0, 2) + '0K';
            stringToPrepare = stringToPrepare.substring(0, stringToPrepare.length() - 4) + last4Bytes;
        } else if (last4Bytes.endsWith('=')) {
            last4Bytes = last4Bytes.substring(0, 3) + 'N';
            stringToPrepare = stringToPrepare.substring(0, stringToPrepare.length() - 4) + last4Bytes;
        }
        return stringToPrepare;
    }
    
    public static String base64URLencode (Blob input){ 
        return EncodingUtil.base64Encode(input).replace('+', '-').replace('/', '_');
    }
}