@isTest
public class DAMAC_RMManDaysController_Test {
	static testMethod void rmManDays () {
        RM_Man_Days__c rmDays = new RM_Man_Days__c ();
        rmDays.Month__c = 'January';
        rmDays.Year__c = '2020';
        
        DAMAC_RMManDaysController obj = new DAMAC_RMManDaysController (null);
        obj.getRMNames();
        obj.rmManDays = rmDays;
        obj.save();
        obj.cancel();
    }
}