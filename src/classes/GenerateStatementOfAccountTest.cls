/*
* Description - Test class developed for GenerateStatementOfAccount
*
* Version            Date            Author            Description
* 1.0              21/11/2017      Ashish Agarwal     Initial Draft
*/

@isTest
private class GenerateStatementOfAccountTest {

    static testMethod void myUnitTest() {
       // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.AccountId = objAcc.Id ;
        insert objCase ;
        
        test.startTest();
	        ApexPages.StandardController sc = new ApexPages.standardController( objCase );
	        GenerateStatementOfAccount objController = new GenerateStatementOfAccount(sc);
	        SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOAService.GenCustomerStatementResponse_element>();
            GenerateSOAService.GenCustomerStatementResponse_element response = new GenerateSOAService.GenCustomerStatementResponse_element();
            response.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338397 and Request Id :40187735 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"92061","REQUEST_ID":"40187735","STAGE_ID":"338397","URL":"https://sftest.deeprootsurface.com/docs/e/40187735_92061_SOA.pdf"}';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
	        objController.generatestataccount();
	        
	        response.return_x = '{"Status":"E","PROC_STATUS":"E","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338397 and Request Id :40187735 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"92061","REQUEST_ID":"40187735","STAGE_ID":"338397","URL":"google"}';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            objController.generatestataccount();
        test.stopTest();
        
    }
    
    static testMethod void myUnitTest2() {
       // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase ;
        
        test.startTest();
	        ApexPages.StandardController sc = new ApexPages.standardController( objCase );
	        GenerateStatementOfAccount objController = new GenerateStatementOfAccount(sc);
	        SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOPService.GeneratePenaltyStatmentResponse_element>();
            GenerateSOPService.GeneratePenaltyStatmentResponse_element response1 = new GenerateSOPService.GeneratePenaltyStatmentResponse_element();
            response1.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338396 and Request Id :40185767 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"78152","REQUEST_ID":"40185767","STAGE_ID":"338396","URL":"https://sftest.deeprootsurface.com/docs/e/40185767_78152_PENALTY.pdf"}';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
	        objController.generatestatofpenalty();
	        	        
	        response1.return_x = '{"Status":"E","PROC_STATUS":"E","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338396 and Request Id :40185767 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"78152","REQUEST_ID":"40185767","STAGE_ID":"338396","URL":""}';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            objController.generatestatofpenalty();
            
            objController.cancelrequest();
	        
        test.stopTest();
    }
    
    static testMethod void myUnitTest3() {
       // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC';
           objUnit.Registration_Id__c = '';
        }
        insert lstBookingUnits;
        
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.IPMS_Updated__c = true ;
        insert objCase ;
        
        test.startTest();
        
        ApexPages.StandardController sc = new ApexPages.standardController( objCase );
        GenerateStatementOfAccount objController = new GenerateStatementOfAccount(sc);
        objController.generatestataccount();
        objController.generatestatofpenalty();
        objController.cancelrequest();
        
    	test.stopTest();
    }
    
}