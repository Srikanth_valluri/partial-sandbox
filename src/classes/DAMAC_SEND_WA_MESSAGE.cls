public with sharing class DAMAC_SEND_WA_MESSAGE {
    
    @testvisible
    private id sfRecId;
    
    public sobject sfRec {get;set;}
    
    public string waMessage {get;set;}
    
    public list<Whats_App_Deliverability__c> msgHistory {get;set;}
    
    private WA_Fields__mdt waFldMapping;
    
    private WA_Credentials__mdt creds;
    
    public boolean showPage {get;set;}
    
    public boolean showUpload {get;set;}
    
    public List<SelectOption> mobileNumbersAvaliable {get;set;}
    
    public string selectedWhatsAppNumber {get;set;}
    
    public user u;
    
    
    public DAMAC_SEND_WA_MESSAGE() {
        u = new user();
        showPage  = false;
        showUpload = false;
        
        u = [select id,Enable_WhatsApp__c,Enable_WhatsApp_Upload__c,WA_Account__c from user where id=:userinfo.getuserid() LIMIT 1];
        if(u.Enable_WhatsApp__c && u.WA_Account__c  != null){
            showPage  = true;
            if(u.Enable_WhatsApp_Upload__c){
                showUpload = true;
            }
        }
        
        string soqlQuery;  
        String sObjName = '';        
        sfRecId = apexpages.currentpage().getparameters().get('id');
        system.debug(sfRecId );
        system.debug(sObjName);
        
        sObjName = sfRecId.getSObjectType().getDescribe().getName();
        creds = new WA_Credentials__mdt();
        
        if (!Test.isRunningTest ())
            creds = [select Endpoint_URL__c,API_Key__c,WA_Account__c from WA_Credentials__mdt where WA_Account__c=:u.WA_Account__c LIMIT 1];
        
        getWhatsAppAPINumbers(sfRecId);
        getRecordName(sfRecId,selectedWhatsAppNumber);
        
        
        if(sfRec.get(waFldMapping.API_Name__c) !=null ){            
            messageHistory();
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Message or Whats App Number cannot be blank'));
            showPage  = false;
            showUpload = false;
        }
    }
    
    public void getWhatsAppAPINumbers(id recordId){ 
        string sObjName = recordId.getSObjectType().getDescribe().getName();        
        waFldMapping = new WA_Fields__mdt();
        System.Debug (sObjName);
        mobileNumbersAvaliable = new List<SelectOption>();
        waFldMapping = [select id,Whats_App_Number_API_Names__c,API_Name__c, Object_Name__c from WA_Fields__mdt where Object_Name__c =:sObjName.trim() LIMIT 1];
        if (waFldMapping.Whats_App_Number_API_Names__c != NULL) {
            string mobileApi = waFldMapping.Whats_App_Number_API_Names__c;
            //Split API Names
                 
            for(string s: mobileApi.split(';')){            
                mobileNumbersAvaliable.add(new SelectOption(s,s.replaceall('__c','').replaceall('_',' ')));            
            }
        } else {
            if (waFldMapping.API_Name__c != NULL) {
                mobileNumbersAvaliable.add (new selectOption (waFldMapping.API_Name__c, waFldMapping.API_Name__c.replaceall('__c','').replaceall('_',' ')));            
                selectedWhatsAppNumber = waFldMapping.API_Name__c;
            }
        }
        system.debug('mobileNumbersAvaliable====='+mobileNumbersAvaliable);
    }
    public void sendWAMessage(){
        if(!string.isblank(waMessage) && sfRec.get(waFldMapping.API_Name__c) !=null ){
            string UniqueId = EncodingUtil.urlEncode(sfRec.get('id')+'-'+userinfo.getuserid()+'-'+string.valueof(system.now()),'UTF-8');
            string apiKey = creds.API_Key__c;
            string AccountPhoneNum = creds.WA_Account__c;        
            string toMobNum  = string.valueof(sfRec.get(waFldMapping.API_Name__c)).removeStart('00');
            string encodedwaMessage = EncodingUtil.urlEncode(waMessage, 'UTF-8');
            string endpoint = creds.Endpoint_URL__c+'token='+apiKey+'&uid='+AccountPhoneNum+'&to='+toMobNum+'&custom_uid='+UniqueId+'&text='+encodedwaMessage;
            HttpRequest req = new HttpRequest();            
            req.setEndpoint(endpoint);
            req.setMethod('POST');        
            Http http = new Http();
            HTTPResponse res;
            if(!test.isRunningTest()){
                res = http.send(req);
                System.debug(req);
                System.debug(res.getBody());
                System.debug(res.getstatuscode());
                
                if(res.getstatuscode() == 200){
                    createDeliveryRec(sfRec,id.valueof(userinfo.getuserid()),waMessage,toMobNum ,UniqueId ,true);
                }else{
                    createDeliveryRec(sfRec,id.valueof(userinfo.getuserid()),waMessage,toMobNum ,UniqueId ,false);
                }
            }
        }else{        
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Message or Whats App Number cannot be blank'));
        }        
    }
    
    public void createDeliveryRec(sobject sobj, id userid,string message,string to,string UniqueId,boolean success){
        //Create Parent
        Whats_App_Message__c mess = new Whats_App_Message__c();
        mess.Unique_Id__c = to+userinfo.getuserid();
        mess.Number__c = to;
        mess.Last_Message_Sent__c= message;
        mess.Outbound_Success__c = success;  
        mess.record_id__c = string.valueof(sobj.get('id'));
        mess.Record_Name__c = string.valueof(sobj.get('Name'));
        upsert mess Unique_Id__c;
        // Create Child
        Whats_App_Deliverability__c del = new Whats_App_Deliverability__c();
        del.Whats_App_Message__c = mess.id;
        del.messagebodytext__c = message;
        del.messagedir__c = 'o';
        del.messagecuid__c = EncodingUtil.urlDecode(UniqueId,'UTF-8');
        if(success){
            del.event__c = 'success';
        }else{
            del.event__c = 'fail';
        }
        insert del;
        
        messageHistory();
        waMessage = null;
    }
    public string getRecordName(id recordId,string APIName){
        string sObjName = recordId.getSObjectType().getDescribe().getName();        
        waFldMapping = new WA_Fields__mdt ();
        waFldMapping = [select id,API_Name__c,Object_Name__c from WA_Fields__mdt where Object_Name__c =:sObjName LIMIT 1];
        if(APIName == null){
            APIName = waFldMapping.API_Name__c;
        }
        string soqlQuery;
        soqlQuery = 'SELECT Id,Name,'+APIName+' from '+sObjName+ ' WHERE ID=:recordId';
          
        SYSTEM.debug('soqlQuery'+soqlQuery);
        string valToreturn;
        sfRec = database.query(soqlQuery);
        if(sfRec.get(APIName) != null){
            valToreturn = string.valueof(sfRec.get('Name'))+';'+string.valueof(sfRec.get(APIName));
        }else{
            valToreturn = string.valueof(sfRec.get('Name'))+';'+string.valueof(0);
        }
        return valToreturn;
    }
    public void messageHistory(){
        string uniqueId = '';
        //string.valueof(sfRec.get(waFldMapping.API_Name__c)).removeStart('00');        
        msgHistory = new list<Whats_App_Deliverability__c>();
        
        
        uniqueId = getRecordName(sfrecid, selectedWhatsAppNumber).substringafter(';').removeStart('00');
        System.Debug (uniqueId );
        //if(!u.Enable_Ownership_Control__c){
            msgHistory = [select id,
                                lastmodifieddate,
                                messagebodytext__c,
                                Location_Link__c,
                                messagetype__c,
                                messagedir__c,
                                messageack__c,
                                message_body_url__c,
                                message_body_caption__c,
                                Type_of_File__c from                                 
                                Whats_App_Deliverability__c 
                                where Whats_App_Message__r.Number__c=:uniqueId
                                and messagetype__c!='vcard' 
                                //and CREATEDDATE =LAST_N_DAYS:30 order by CREATEDDATE];        
                                ORDER BY createddate asc LIMIT 500];
       // }else{
            /*msgHistory = [select id,
                                lastmodifieddate,
                                messagebodytext__c,
                                Location_Link__c,
                                messagetype__c,
                                messagedir__c,
                                messageack__c,
                                message_body_url__c,
                                message_body_caption__c,
                                Type_of_File__c from                                 
                                Whats_App_Deliverability__c where Whats_App_Message__r.Unique_Id__c=:uniqueId and messagetype__c!='vcard' and ownerid=:userinfo.getuserid() and CREATEDDATE =LAST_N_DAYS:30 order by CREATEDDATE]; //uncomment this and comment below line to control by owner
        
        }  */                 
                           
    }
    
}