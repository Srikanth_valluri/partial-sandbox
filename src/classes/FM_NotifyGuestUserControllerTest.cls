@isTest
private class FM_NotifyGuestUserControllerTest{
    public static testmethod void createFMCase(){
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
       
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='Contractor';
        fmCaseObj.Person_to_collect__c = 'Contractor';
        insert fmCaseObj;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(fmCaseObj);
        FM_NotifyGuestUserController obj=new FM_NotifyGuestUserController(sc);
        obj.createSMSHistoryRecord();
        obj.redirect();
    }
    
    public static testmethod void negativeTest(){
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
       
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='TEST';
        insert fmCaseObj;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(fmCaseObj);
        FM_NotifyGuestUserController obj=new FM_NotifyGuestUserController(sc);
        obj.createSMSHistoryRecord();
    }
}