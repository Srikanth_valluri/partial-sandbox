/***************************************************************************************************
 * @Name              : DAMAC_Inventory_External_Pagination 
 * @Test Class Name   : DAMAC_Inventory_External_Pagination_Test
 * @Description       : Pagination API For Inventories Listing with(out) Pagination and Apply Filters
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         25/06/2020       Created
****************************************************************************************************/
@RestResource(urlMapping='/queryInventoryPagination/*')
global class DAMAC_Inventory_External_Pagination{
    
    @HttpPOST
    global static void getResults(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String reqBody = req.requestBody.toString();
        System.debug (reqBody);
        String pageSize = req.params.get('pageSize');
        String pageOffset = req.params.get('pageOffset');
        String IsTotalCountNeeded = req.params.get('IsTotalCountNeeded'); 
        Integer TotalCountNeeded = 0;
        Integer pageSizeInteger;
        Integer pageNumberInteger;
        Integer pageOffsetInteger;
        // Parameters for Search API and List of Available Units
        String type = req.params.get('type');                    
        
        if(pageSize != NULL && pageSize != '' && pageOffset != NULL && pageOffset != ''){
            pageSizeInteger = Integer.valueOf(pageSize);
            pageNumberInteger = Integer.valueOf(pageOffset);
            pageOffsetInteger = pageSizeInteger * pageNumberInteger; //calculating Offset
        }
        
         List<inventory__c> inventoryList = new List<inventory__c >();
        // URL to send the Inventories based on the filters received from the inventory app and send the filtered Inventories
        //if(req.requestURI == '/queryInventoryPagination/applyFilters'){ 
        if(req.requestURI == '/queryInventoryPagination'){
            Map<String, Object> jsonResponse = (Map<String, Object>) JSON.deserializeUntyped(reqBody);
            System.debug('jsonResponse>>'+jsonResponse);
            String status = 'Released';
            String propertyType = 'Storage';
            String sortQuery = '';
            set<Id>otherInventoryIds = new set<id>();
            set<id> thisPcinvIds = new set<id>();
            for(inventory_user__c thisPcInv:[select id,inventory__c,Campaign__r.name,Campaign__c 
                                                from inventory_user__c 
                                                where user__c=:userinfo.getuserid() 
                                                and inventory__r.status__c='Released' and inventory__r.Tagged_To_Unit_Assignment__c = false]){
                thisPcinvIds.add(thisPcInv.inventory__c);
            }
            for(inventory_user__c invUs:[select id,inventory__c 
                                            from inventory_user__c 
                                            where User__c!=:userinfo.getuserid() and inventory__r.Tagged_To_Unit_Assignment__c = false 
                                            and inventory__r.status__c='Released']){
                if(thisPcinvIds.size()>0){
                    if(!thisPcinvIds.contains(invUs.inventory__c)){
                        otherInventoryIds.add(invUs.inventory__c);
                    }
                }else{
                    otherInventoryIds.add(invUs.inventory__c);
                }
            }
            String orderQuery = ' ORDER BY Country_Ordering__c ASC, Unit_Released_Date_from_IPMS__c DESC, Marketing_Name__c, ' 
                                + ' Unit_Name__c, District__c, '
                                +' Property_City__c, Price__c, Area_sft__c DESC '; 
            
            list<string> lookupCodes = new list<string>{'VILLA', 'Apartment'};
            /* String query = 'SELECT '
                        +'Unit_Plan_SF_Link__c,Floor_Plan_SF_Link__c, Plot_Plan_SF_Link__c, promotion_unit__c,'
                        +'Marketing_Name_Doc__c, Marketing_Name_Doc__r.Name, Property_Name__c, Unit_Id__c, '
                        +'Unit_Name__c, Bedroom_Type__c, Bedrooms__c,IPMS_Bedrooms__c, Area_sft__c,'
                        +'Unit_Type__c, View_Type__c, Property_Status__c, Price_Per_Sqft__c,App_Property_Status__c,'
                        +'ACD_Date__c, Plot_Area__c, Floor_Plan__c, Unit_Plan__c, Plot_Plan__c,'
                        +'Sales_Offer_Additional_Charges__c, Unit_Catagory__c, convertCurrency(Total_Price_inc_VAT__c),'
                        +'Rera_Percentage__c, Special_Price__c, Special_Price_Tax_Amount__c, Marketing_Plan__c,'
                        +'Property_country__c,Property_City__c, Property_Type__c, Space_type_lookup_code__c '
                        +'from inventory__c '
                        +'WHERE Tagged_To_Unit_Assignment__c = FALSE AND Marketing_name__c != NULL '
                        +'AND Status__c =: status AND Is_Assigned__c = false AND Property_Type__c !=: propertyType '
                        +'AND Id not in: otherInventoryIds AND '; */
            String query = 'SELECT '
                        +'Unit_Plan_SF_Link__c,Floor_Plan_SF_Link__c, Plot_Plan_SF_Link__c, promotion_unit__c,'
                        +'Marketing_Name_Doc__c, Marketing_Name_Doc__r.Name, Property_Name__c, Unit_Id__c, '
                        +'Unit_Name__c, Bedroom_Type__c, Bedrooms__c,IPMS_Bedrooms__c, Area_sft__c,'
                        +'Unit_Type__c, View_Type__c, Property_Status__c, Price_Per_Sqft__c,App_Property_Status__c,'
                        +'ACD_Date__c, Plot_Area__c, Floor_Plan__c, Unit_Plan__c, Plot_Plan__c,'
                        +'Sales_Offer_Additional_Charges__c, Unit_Catagory__c, convertCurrency(Total_Price_inc_VAT__c),'
                        +'Rera_Percentage__c, Special_Price__c, Special_Price_Tax_Amount__c, Marketing_Plan__c,'
                        +'Property_country__c,Property_City__c, Property_Type__c, Space_type_lookup_code__c '
                        +'from inventory__c '
                        +'WHERE Tagged_To_Unit_Assignment__c = FALSE AND Marketing_name__c != NULL '
                        +'AND Status__c =: status AND Property_Type__c !=: propertyType '
                        +'AND ((Is_Assigned__c = false AND Id not in: otherInventoryIds) OR (Is_Assigned__c = true AND Id IN :thisPcinvIds)) AND ';
            
            
            //checking type parameter
            if(type != '' && type != NULL){            
                if(type == 'New'){                
                    query += ' ' + 'IS_NEW__C = TRUE AND ';                
                }
                if(type == 'Popular'){                
                    query += ' ' + 'IS_Focus__c = TRUE AND ';                
                }
                if(type == 'general'){                
                    query += ' ' + 'IS_New__c = FALSE AND IS_Focus__c = FALSE AND ';                
                }
            }
            
            
            Map<String, List<String>> valuesMap = new Map<String, List<String>>();
            
            for (String key:jsonResponse.keySet()){
                List<String> values = new List<String>();
                system.debug('jsonResponse.get(key): ' + jsonResponse.get(key));
                if(jsonResponse.get(key) != null && jsonResponse.get(key) != ''){
                    String resp = String.valueOf(jsonResponse.get(key));
                    system.debug('resp: ' + resp);
                    if (resp != '()' && resp !=''){
                        if (resp.contains(',')) {
                            for (String val : resp.split(',')) {
                                values.add('\''+val.removeStart('(').removeEnd(')').trim()+'\'');
                            }
                        } else {
                            values.add('\'' + resp.removeStart('(').removeEnd(')').trim() + '\'');
                        }
                    }
                    system.debug('values: ' + values);
                    valuesMap.put(key, values);
                }
            }
            
            for (String key:jsonResponse.keySet()){
                System.debug('>>>>>>key>>>>>'+key);
                System.debug('>>>>>>value>>>>>'+jsonResponse.get(key));
                if (key.startsWith('No_of_bedrooms__c')) {
                    if(String.valueOf(jsonResponse.get(key)) != '' && String.valueOf(jsonResponse.get(key)) != '()'){
                        string bedRooms = String.valueOf(jsonResponse.get(key)).replace('BR','').trim();
                        bedRooms = bedRooms.replace('(','').trim();
                        bedRooms = bedRooms.replace(')','').trim();
                        bedRooms = (bedRooms.contains('STUDIO') ? bedRooms.replace('STUDIO','0').trim() : bedRooms.trim());
                        list<string> bedVals = bedRooms.split(',');
                        list<string> bedVals1 = new list<string>();
                        for(string str : bedvals)
                            bedVals1.add(str.trim());
                        System.debug('>>>>>>bedvals>>>>>>'+bedVals1);   
                        if(bedVals1.size() > 0) 
                            query += ' Bedroom_Filter__c in: bedVals1 AND ';
                    }
                }

                else if (key.startsWith ('max_')) {
                    if(String.valueOf(jsonResponse.get(key)) != ''){ 
                        Decimal maxValue = Decimal.valueOf(String.valueOf(jsonResponse.get(key)));
                        if(key != 'max_Price_Per_Sqft__c')
                            query += ' '+key.removeSTART('max_') +' <= '+ maxValue +' AND ';
                        else
                            query += ' Total_Price_inc_VAT__c <= '+ maxValue +' AND ';
                    }
                }
                else if (key.startsWith ('min_')) {
                    if(String.valueOf(jsonResponse.get(key)) != ''){
                        Decimal minValue = Decimal.valueOf(String.valueOf(jsonResponse.get(key)));
                        query += ' '+key.removeSTART('min_') +' >= '+ minValue +' AND ';
                    }
                }  
                else if (key.startsWith ('sort')) {
                    if(String.valueOf(jsonResponse.get(key)) == 'area_low_to_high'){                        
                        sortQuery = ' ORDER BY Country_Ordering__c ASC, No_of_Promotions__c desc, Area_sft__c ASC ';
                    }
                    else if(String.valueOf(jsonResponse.get(key)) == 'area_high_to_low'){                        
                        sortQuery = ' ORDER BY Country_Ordering__c ASC, No_of_Promotions__c desc, Area_sft__c DESC ';
                    }
                    else if(String.valueOf(jsonResponse.get(key)) == 'price_low_to_high'){                        
                        sortQuery = ' ORDER BY Country_Ordering__c ASC, No_of_Promotions__c desc, Total_Price_inc_VAT__c ASC';
                    }
                    else if(String.valueOf(jsonResponse.get(key)) == 'price_high_to_low'){                        
                        sortQuery = ' ORDER BY Country_Ordering__c ASC, No_of_Promotions__c desc, Total_Price_inc_VAT__c DESC ';
                    }
                    else if(String.valueOf(jsonResponse.get(key)) == 'bedroom_low_to_high'){                        
                        sortQuery = ' ORDER BY Country_Ordering__c ASC, No_of_Promotions__c desc, Bedroom_filter__c ASC ';
                    }
                    else if(String.valueOf(jsonResponse.get(key)) == 'bedroom_high_to_low'){                        
                        sortQuery = ' ORDER BY Country_Ordering__c ASC, No_of_Promotions__c desc, Bedroom_filter__c desc ';
                    }
                    else{
                        sortQuery = orderQuery;
                    }
                } 
                
                /*
                else if (key.startsWith('villa_') && String.valueOf(jsonResponse.get(key)) != '') {
                   // String villaPropertyType = '%villa%'; 
                   String villaPropertyType = '%' +  String.valueOf(jsonResponse.get(key)) +'%'; 
                    query += ' '+ key.removeSTART('villa_') + ' LIKE : villaPropertyType AND ';
                
                }
                else if(key.startsWith('apartments_') && String.valueOf(jsonResponse.get(key)) != ''){
                    String apartmentPropertyType = '%apartment%'; 
                    query += ' '+ key.removeSTART('apartments_') + ' LIKE : apartmentPropertyType AND ';
                } */
                else {
                    List<String> valuesList = new List<String>();
                    if (valuesMap.containsKey(key)){
                        
                        if(key == 'Space_type_lookup_code__c' && valuesMap.get(key) != null && valuesMap.get(key).size() > 0){
                            
                            query += '(';
                            Integer i = 0;
                            for(String val: valuesMap.get(key)){
                                if(i  != 0){
                                    query += ' OR ';
                                }
                                val = '\'%' +val.removeStart('\'').removeEnd('\'') + '%\'';
                                //valuesList.add('\'%' + val + '%\'');
                                query += ' Space_type_lookup_code__c LIKE ' + val;
                                i++;
                            }
                            query += ') AND ';
                          
                        }
                        if(key == 'Property_type__c' && valuesMap.get(key) != null && valuesMap.get(key).size() > 0){
                            
                            query += '(';
                            Integer i = 0;
                            for(String val: valuesMap.get(key)){
                                if(i  != 0){
                                    query += ' OR ';
                                }
                                val = '\'%' +val.removeStart('\'').removeEnd('\'') + '%\'';
                                //valuesList.add('\'%' + val + '%\'');
                                query += ' Unit_categorization__c LIKE ' + val;
                                i++;
                            }
                            query += ') AND ';
                          
                        } else{

                            valuesList = valuesMap.get(key) ;
                            if(valuesList != NULL && valuesList.size() > 0){                  
                                query += ' ' + key + ' IN ' + valuesList + ' AND ';
                            }
                        }
                        System.debug('valuesList: ' +valuesList);  
                        
                    }
                }
                           
            }
            query = query.removeEND('AND ');
            query = query.removeEND('WHERE ');
            /*if(sortQuery != '' && sortQuery != NULL){
                query = query + sortQuery ; 
                       
            }*/
            System.Debug ('query>>'+query);
            inventoryList = Database.query(query);   
            
            if(sortQuery != '' && sortQuery != NULL) 
                query = query + sortQuery ;
            else
                query = query + orderQuery;          
            
                                      
            // Adding pagination based on parameters
            if(pageSize != NULL && pageSize != '' && pageOffset != NULL && pageOffset != ''){                                
                //query = query + orderQuery + ' LIMIT : pageSizeInteger OFFSET : pageOffsetInteger';
                query = query + ' LIMIT : pageSizeInteger OFFSET : pageOffsetInteger';                               
            }
            // No pagination, if no parameters
            else{
                query = query + ' LIMIT 50000';                                     
            }
            System.Debug ('query>>'+query);
            //inventoryList = Database.query(query);
            TotalCountNeeded = inventoryList.size(); //total count is calculated here
            
            list<Inventory__c> inventories = database.query(query);
            for(Inventory__c inv: inventories){
                if(inv.IPMS_Bedrooms__c != null && (inv.IPMS_Bedrooms__c).isNumeric())
                    inv.IPMS_Bedrooms__c = inv.IPMS_Bedrooms__c+' BR';
                    
                if(inv.Bedroom_Type__c != null && (inv.Bedroom_Type__c).isNumeric())
                    inv.Bedroom_Type__c = inv.Bedroom_Type__c+' BR';    
            }
            //checks if IsTotalCountNeeded param is entered. Shows full data if false
            if(IsTotalCountNeeded != 'true'  || (IsTotalCountNeeded == '' && IsTotalCountNeeded == NULL)){
                
                    String jsonBody = '';
                    jsonBody = '{'+ '"Total_Records":{' + '"Total_Records_Count":"'+TotalCountNeeded+'"}, "Data":'+JSON.serialize(inventories)+'}';
                    RestContext.response.addHeader('Content-Type', 'application/json');
                    RestContext.response.responseBody = Blob.valueOf(jsonBody ); 
                
            }
            //checks if IsTotalCountNeeded param is entered. Shows just the count with empty data, if true
            if(IsTotalCountNeeded == 'true'){
                String jsonBody = '';
                jsonBody = '{' + '"Total_Records":{' + '"Total_Records_Count":"'+TotalCountNeeded+'"},"Data":[]}';               
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody = Blob.valueOf(jsonBody);
             
            }
     
         
        }
        else {
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf('{"Error" : "URL Not found"}');    
        }
    } //End of POST method
}