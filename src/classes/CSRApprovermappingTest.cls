@IsTest
public class CSRApprovermappingTest{
 @IsTest
    static void positiveTest4(){
        // User Insert 
          User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABC@test.com');
         insert pcUser;
        // Create common test accounts
       
        
       
        // Create common test accounts
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Parking_Details_JSON__c ='[{"AggrementStatus":"Agreement executed by DAMAC","Area":"518.00","PaidAmmount":"1200693","PaidPersentage":"","Price":"1295000.00","PropertyName":"","PSF":"2500.00","RegDate":"2013-09-29","UnitNumber":"DTPC/32/3213"}]';
         objCase.Approving_User_Id__c=pcUser.id;
        insert objCase;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        objSR.Agency__c = objAcc.Id ;
        insert objSR ;

        Id dealId = objSR.Deal_ID__c;
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
        insert lstBookings ; 

        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
        lstBookingUnits[0].Registration_ID__c = '1520';
        insert lstBookingUnits;

        String str ='{"CRE_Comments__c":"Test","Booking_Unit__c":"a0x25000000BDbiAAG","CRE_Request_History__c":"% of Area Variation to be waived=>550,Amount=>,","Case__c":"5002500000Bg6xMAAR","RequestType__c":"Area Variation Waiver","Per_Area_Variation_to_be_waived__c":"550","Approving_User_Id__c":"0050Y000001SVkiQAG","Approving_User_Name__c":"Waleed Elkady","Approving_User_Role__c":"HOD"}}';
        
        CaseSummaryRequest__c objcsr = (CaseSummaryRequest__c)JSON.deserialize(str, CaseSummaryRequest__c.class);
                                system.debug('obj :'+objcsr);
                                objcsr.case__c = objCase.Id;
                                objcsr.Booking_Unit__c = lstBookingUnits[0].Id;
                                objcsr.Manager_Request_History__c ='% of Area Variation to be waived=>550';
                                objcsr.Director_Request_History__c ='% of Area Variation to be waived=>550';
                                objcsr.HOD_Request_History__c ='% of Area Variation to be waived=>550';
                                
        insert objcsr;

        PageReference myVfPage = Page.CSRApprovalPage;
        Test.setCurrentPage(myVfPage);

        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('caseId',objCase.Id);
        ApexPages.currentPage().getParameters().put('approver','Committee');
        ApexPages.currentPage().getParameters().put('Name','Test');
        ApexPages.currentPage().getParameters().put('Status','Need More Info');
        Test.startTest();
        // CSRApprovalController obj = new CSRApprovalController();
        //obj.submitCSR();
              
        
        Test.stopTest();


        
        
        
    }
}