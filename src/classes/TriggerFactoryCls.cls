/**************************************************************************************************
* Name               : TriggerFactoryCls                                                          *
* Description        : This is a trigger factory interface to handle trigger executions.          *
* Created Date       : 03/01/2017                                                                 *
* Created By         : PWC                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR      DATE                                                                    *
* 1.0         Ravibabu    03/01/2017                                                              *
* 1.1         Craig       27/03/2018    Added User_Action_Plan__c handler to the class            *
* 1.2         QBurst      17/12/2019    Added BYPASS_AsynReceiptWebServiceClone_Job and related method to the class 
**************************************************************************************************/
public class TriggerFactoryCls { 
    
    public static Boolean BYPASS_UPDATE_TRIGGER = false; 
    public static Boolean BYPASS_ON_TEST = false;
    public static Boolean BYPASS_AsynReceiptWebServiceClone_Job=false; // 1.2
    
    /*** ******************************************************************************************
* @Description : Method to create and execute a trigger handler.                             *
* @Params      : Schema.sObjectType                                                          *
* @Return      : void                                                                        *
*********************************************************************************************/
    public static void createHandler(Schema.sObjectType soType){
        Boolean skipTrigger = false;
        if (soType == Assigned_PC__c.sObjectType && Test.isRunningTest()) {
            skipTrigger = true;
        }
        if (soType ==  Campaign__c.sObjectType && Test.isRunningTest()) {
            skipTrigger = true;
        }
        if(skipTrigger == FALSE) {
            TriggerFactoryInterface handler = getHandler(soType);
            try{
                if(!BYPASS_ON_TEST){
                    if (handler != null){
                        execute(handler);
                    }else{
                        throw new CustomException('No Trigger Handler registered for Object Type: ' + soType);   
                    }    
                }
            }catch(Exception ex){
                if (!Test.isRunningTest())
                    throw new CustomException('Exception in Handler at line = '+ex.getLineNumber()+' , exception message = '+ex.getMessage()); 
            }
        }
        
    }
    
    /*********************************************************************************************
* @Description : Method to control the execution of the handler.                             *
* @Params      : TriggerFactoryInterface                                                     *
* @Return      : void                                                                        *
*********************************************************************************************/ 
    public static void execute(TriggerFactoryInterface handler){
        if(trigger.isBefore){
            if(trigger.isInsert){
                handler.executeBeforeInsertTrigger(trigger.new);
            }else if(trigger.isUpdate){
                if(!BYPASS_UPDATE_TRIGGER){
                    handler.executeBeforeUpdateTrigger(trigger.newMap, trigger.oldMap);
                }
            }else if(trigger.isDelete){
                handler.executeBeforeDeleteTrigger(trigger.oldMap);
            }else if(trigger.isInsert || trigger.isUpdate){
                handler.executeBeforeInsertUpdateTrigger(trigger.new, trigger.oldMap);
            }else{
                
            }
        }else{
            if(trigger.isInsert){
                handler.executeAfterInsertTrigger(trigger.newMap);
            }else if(trigger.isUpdate){
                if(!BYPASS_UPDATE_TRIGGER){                    
                    handler.executeAfterUpdateTrigger(trigger.newMap, trigger.oldMap);
                    // 1.2 starts
                    System.debug('setBYPASS_AsynReceiptWebServiceClone_Job from trigger='
                                        + BYPASS_AsynReceiptWebServiceClone_Job);
                    if(!BYPASS_AsynReceiptWebServiceClone_Job){
                        setBYPASS_AsynReceiptWebServiceClone_Job();
                    }
                    // 1.2 ends
                    System.debug('After Update Trigger being called from here');
                }
            }else if(trigger.isDelete){
                handler.executeAfterDeleteTrigger(trigger.oldMap);
            }else if(trigger.isInsert || trigger.isUpdate){
                handler.executeAfterInsertUpdateTrigger(trigger.newMap, trigger.oldMap);
            }else{
                
            }
        }
    }
    
    /*********************************************************************************************
* @Description : Method to to get the appropriate handler for the object type.               *
*                Modify this method to add any additional handlers.                          *
* @Params      : Schema.sObjectType                                                          *
* @Return      : TriggerFactoryInterface                                                     *
*********************************************************************************************/ 
    private static TriggerFactoryInterface getHandler(Schema.sObjectType soType){
        if(soType == NSIBPM__Service_Request__c.sObjectType){
            return new ServiceRequestTrgHandler();
        }else if(soType == Location__c.sObjectType){
            return new LocationTrgHandler();
        }else if(soType == Inventory__c.sObjectType){
            return new InventoryTrgHandler();
        }else if(soType == Inquiry__c.sObjectType){
            Activate_New_Assignment_Process__c activateNewProcess = Activate_New_Assignment_Process__c.getInstance(UserInfo.getUserID());
            if (activateNewProcess.Activate_New_Inquiry_Code__c) {
                return new InquiryTriggerHandler_New ();
            } else
                return new InquiryTriggerHandler();
        }else if(soType == Account.sObjectType){
            return new AccountTrgHandler(); 
        }else if(soType == Event.sObjectType){
            return new EventTriggerHandler(); 
        }else if(soType == Task.sObjectType){
            return new TaskTriggerHandler(); 
        }else if(soType == JO_Campaign_Virtual_Number__c.sObjectType){
            return new CampaignVirtualNumberTriggerHandler(); 
        }else if(soType == User.sObjectType){ 
            return new UserTriggerHandler(); 
        }else if(soType == Assigned_PC__c.sObjectType){
            
            return new AssignedPcTriggerHandler(); 
        }else if(soType == Agency_PC__c.sObjectType){
            return new AgencyPCTriggerHandler(); 
        }else if(soType == Inventory_Release__c.sObjectType){
            return new InventoryReleaseTriggerHandler(); 
        }else if(soType == Address__c.sObjectType){
            return new AddressTriggerHandler(); 
        }else if(soType == Inventory_User__c.sObjectType){
            return new InventoryUserTriggerHandler(); 
        }else if(soType == Assigned_Agent__c.sObjectType){
            return new AssignedAgentTriggerHandler(); 
        }else if(soType == Attachment.sObjectType){
            return new AttachmentTriggerHandler(); 
        }else if(soType == Contact.sObjectType){
            return new ContactTriggerHandler(); 
        }else if(soType == Team_Building__c.sObjectType){
            return new TeamBuildingTriggerHandler(); 
        }else if(soType == Payment_Terms__c.sObjectType){
            return new PaymentTermsTriggerHandler(); 
        }else if(soType == Campaign_Inventory__c.sObjectType){
            return new CampaignInventoryTriggerHandler();
        }else if(soType == Agent_Commission__c.sObjectType){
            return new CommissionTriggerHandler();
        }else if(soType == Buyer__c.sObjectType){
            return new BuyerTrgHandler();
        }else if(soType == deal_team__c.sObjectType){
            return new DealTeamTrgHandler();
        }else if(soType == NSIBPM__SR_Doc__c.sObjectType){
            return new SRDocTrgHandler();
        }else if(soType == Proof_of_Payment__c.sObjectType){
            return new proofOfPaymentTrgHandler();
        }else if(soType == Target__c.sObjectType){
            return new TargetTriggerHandler();
        }else if(soType == Campaign__c.sObjectType){
            return new CampaignTriggerHandler();
        }else if(soType == NSIBPM__Step__c.sObjectType){
            return new StepTriggerHandler();
        }else if(soType == Unit_Assignment__c.sObjectType){
            return new UnitAssignmentTriggerHandler();
        }else if(soType == Amendment__c.sObjectType){
            return new AmendmentTriggerHandler();
        }else if(soType == User_Action_Plan__c.sObjectType){
            return new UserActionPlanTriggerHandler();
        }else if(soType == Travel_Details__c.sObjectType){
            return new TravelDetailsTriggerHandler();
        }
        else if(soType == ContentVersion.sObjectType){
            return new ContentVersionTriggerHandler();
        }else if(soType == New_Step__c.sObjectType){
            return new NewStepTrgHandler();
        }
        /*
else if(soType == Receipt__c.sObjectType){
return new ReceiptTriggerHandler();
}*/
        return null;
    }
    
    /*Trigger Handler for Inquiry.*/
    public static Boolean getBYPASS_UPDATE_TRIGGER(){
        return BYPASS_UPDATE_TRIGGER;
    }
    
    public static void setBYPASS_UPDATE_TRIGGER(){
        BYPASS_UPDATE_TRIGGER = true;   
    }
    
    public static void resetBYPASS_UPDATE_TRIGGER(){
        BYPASS_UPDATE_TRIGGER = false;  
    }
    
    
    public static void setBYPASS_AsynReceiptWebServiceClone_Job(){ // 1.2
        BYPASS_AsynReceiptWebServiceClone_Job= true;   
    }
    
    
    
}// End of class.