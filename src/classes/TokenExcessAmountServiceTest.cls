/*
 * Description - Test class for TokenExcessAmountService class
 *
 * Version        Date            Author            Description
 * 1.0            16/04/18        Vivek Shinde      Initial Draft
 */
@isTest
private class TokenExcessAmountServiceTest {

     static testMethod void tokenRefundPositiveTest(){
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new RefundsMock());
        TokenExcessAmountService.getTokenRefund('81965');
    }
    
    static testMethod void tokenRefundNegativeTest(){
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new RefundsMock.RefundsMock1());
        TokenExcessAmountService.getTokenRefund('81965');
    }
}