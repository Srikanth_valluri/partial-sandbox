@isTest 
global class MockHttpSecureSMSGenericCRM implements HttpCalloutMock{

    global HTTPResponse respond(HTTPRequest req){
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type','application/json');
        res.setStatusCode(200);
        res.setBody('{"Status":"OK","Data":[{"status":"OK","details":"Message Sent","bulkId":6336800}]}');
        return res;
    }
}

//{"Status":"OK","Data":[{"status":"Error","details":"NO VALID NUMBER","bulkId":6336800}]}