@isTest
/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  20/11/2017  Initial Draft
*/
public class CallCreateStepClassTest {
     
     static testMethod void Test1(){
        test.StartTest();
        
        UserRole role1 = new UserRole();
        role1.Name = 'Sales Admin';
        role1.DeveloperName =  'Sale_Admin';
        insert role1; 
        
        User objUser = new User();
        objUser.LastName = 'Sales Admin_User';
        objUser.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1].Id;
        //String RoleVp = 'VP';
       
        objUser.Email = 'Vpr123@amamama.com';
        objUser.Username = 'Vpr123@amamama.com';
        objUser.CompanyName = 'TEST02';
        objUser.Title = 'titlef2';
        objUser.Alias = 'aVli12';
        objUser.TimeZoneSidKey = 'Asia/Kolkata';
        objUser.EmailEncodingKey = 'UTF-8';
        objUser.LanguageLocaleKey = 'en_US';
        objUser.LocaleSidKey = 'en_US';
        objUser.IsActive = true;      
        objUser.UserRoleId = role1.Id;        
        insert objUser;
            
        System.runAs(objUser){ 
            List<ID> lstCaseID = new List<ID>();
            Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Token Refund').getRecordTypeId();
            Case objCase1 = new Case();        
            objCase1.Status = 'New';        
            objCase1.RecordTypeID = devRecordTypeId;
            
            insert objCase1;
            lstCaseID.add(objCase1.id) ;        
            
            CallCreateStepClass.checkServiceRequestID(lstCaseID);
        }
        test.StopTest();
     
     }   
}