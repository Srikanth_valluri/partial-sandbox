public class SSRTriggerHandler {

    // Method called After Update which will send the SSR for approval
    public void sendForApprovalOnUpdate(Map<Id, Sales_Service_Request__c> mapNewSSRCases, Map<Id, Sales_Service_Request__c> mapOldSSRCases) {
        List<Sales_Service_Request__c> lstEligibleSSR = new List<Sales_Service_Request__c>();
        List<Sales_Service_Request__c> lstApprovedSSR = new List<Sales_Service_Request__c>();
        List<Sales_Service_Request__c> lstSSRToUpdate = new List<Sales_Service_Request__c>();
        List<Sales_Service_Request__c> lstApprovedSSRToUpdate = new List<Sales_Service_Request__c>();
        Set<Id> setSSR = new Set<Id>();
        Set<Id> setSSRUnlockIds = new Set<Id>();

        for(Sales_Service_Request__c objSSRCase : mapNewSSRCases.values()) {
            if(String.isBlank(objSSRCase.Approving_User_Ids__c) &&
                String.isNotBlank(objSSRCase.Approval_Status__c) &&
                objSSRCase.Approval_Status__c.equalsIgnoreCase('Pending') &&
                objSSRCase.Submit_for_Approval__c == true &&
                (objSSRCase.Approving_User_Ids__c != mapOldSSRCases.get(objSSRCase.Id).Approving_User_Ids__c) &&
                String.isNotBlank(mapOldSSRCases.get(objSSRCase.Id).Approving_User_Ids__c) &&
                objSSRCase.Status__c == 'Submitted'
            ) {
                lstApprovedSSR.add(objSSRCase);
            } else if (String.isNotBlank(objSSRCase.Approving_User_Ids__c) &&
                String.isNotBlank(objSSRCase.Approval_Status__c) &&
                objSSRCase.Approval_Status__c.equalsIgnoreCase('Pending') &&
                objSSRCase.Submit_for_Approval__c == true &&
                (objSSRCase.Approving_User_Ids__c != mapOldSSRCases.get(objSSRCase.Id).Approving_User_Ids__c) &&
                objSSRCase.Status__c == 'Submitted'
            ) {
                system.debug('inside else if');
                lstEligibleSSR.add(objSSRCase);
            }
        }
        
        if (!lstEligibleSSR.isEmpty()) {
            for (Sales_Service_Request__c objSSRCase : lstEligibleSSR) {
                if (String.isNotBlank(objSSRCase.Approving_User_Ids__c)) {
                    List<String> lstApprovingUserIds = objSSRCase.Approving_User_Ids__c.split(',');
                    Sales_Service_Request__c objSSR = new Sales_Service_Request__c(Id = objSSRCase.Id);
                    objSSR.Current_Approving_User_Id__c = lstApprovingUserIds[0];
                    Approval.UnlockResult unlockedResult = Approval.unlock(objSSRCase);
                    lstSSRToUpdate.add(objSSR);
                    setSSR.add(objSSR.Id);
                }
            }
            if (!lstSSRToUpdate.isEmpty()) {
                update lstSSRToUpdate;
            }
            
            if (!setSSR.isEmpty()) {
                processApproval(setSSR);    
            }
        }

        if (!lstApprovedSSR.isEmpty()) {
            for (Sales_Service_Request__c objSSRCase : lstApprovedSSR) {
                Approval.UnlockResult unlockedResult = Approval.unlock(objSSRCase.Id);
                Sales_Service_Request__c objSSR = new Sales_Service_Request__c(Id = objSSRCase.Id);
                objSSR.Approval_Status__c = 'Approved';
                lstApprovedSSRToUpdate.add(objSSR);
                setSSRUnlockIds.add(objSSRCase.Id);
                //Approval.UnlockResult unlockedResult = Approval.unlock(objSSR.Id);

            }
            if (!lstApprovedSSRToUpdate.isEmpty()) {
                update lstApprovedSSRToUpdate;
            }
            processUnlock(setSSRUnlockIds);
        }
    }
    
    @future 
    public static void processUnlock(Set<Id> setCaseIds) {
        List<Sales_Service_Request__c> lstSalesServiceRequests = [SELECT Id FROM Sales_Service_Request__c WHERE ID IN: setCaseIds];
            
        if (!lstSalesServiceRequests.isEmpty()) {
            for (Sales_Service_Request__c objSSR: lstSalesServiceRequests) {
                Approval.UnlockResult unlockedResult = Approval.unlock(objSSR.Id);
                system.debug('-->unlockedResult '+unlockedResult);
            }
        }
    }

    @future
    public static void processApproval(Set<Id> setSSR) {
        List<Sales_Service_Request__c> lstSSRNewList = [SELECT Id,Approving_User_Ids__c, Approval_Status__c, Submit_for_Approval__c, Status__c FROM Sales_Service_Request__c WHERE ID IN: setSSR];
        List<Approval.ProcessSubmitRequest> lstApprovalRequests = new List<Approval.ProcessSubmitRequest>();

        for (Sales_Service_Request__c objSSRCase : lstSSRNewList) {
            if (String.isNotBlank(objSSRCase.Approving_User_Ids__c)) {
                List<String> lstApprovingUserIds = objSSRCase.Approving_User_Ids__c.split(',');
                Approval.ProcessSubmitRequest objApprovalSubmitRequest = new Approval.ProcessSubmitRequest();
                objApprovalSubmitRequest.setComments('Submitting request for approval.');
                objApprovalSubmitRequest.setObjectId(objSSRCase.Id);
                objApprovalSubmitRequest.setProcessDefinitionNameOrId('SSR_Fund_Transfer_Rejected_Unit_AP');
                objApprovalSubmitRequest.setSkipEntryCriteria(true);
                objApprovalSubmitRequest.setNextApproverIds(new Id[] {lstApprovingUserIds[0]});

                Approval.UnlockResult unlockedResult = Approval.unlock(objSSRCase.Id);
                lstApprovalRequests.add(objApprovalSubmitRequest);
                // Submit the approval request for the account
                //Approval.ProcessResult result = Approval.process(objApprovalSubmitRequest);
            }
        }
        if (!lstApprovalRequests.isEmpty()) {
            Approval.ProcessResult[] result = Approval.process(lstApprovalRequests, true);
        }
    }

    public void createFinanceTask(Map<Id, Sales_Service_Request__c> mapNewSSRCases, Map<Id, Sales_Service_Request__c> mapOldSSRCases) {
        List<Sales_Service_Request__c> lstSSR = new List<Sales_Service_Request__c>();
        List<Task> lstTask = new List<Task>();

        for (Sales_Service_Request__c objSSR : mapNewSSRCases.values()) {
            if (objSSR.Approval_Status__c != null &&
            objSSR.Approval_Status__c == 'Approved' &&
            objSSR.Approval_Status__c != mapOldSSRCases.get(objSSR.Id).Approval_Status__c) {
                lstSSR.add(objSSR);
            }
        }
        
        if (!lstSSR.isEmpty()) {
            for (Sales_Service_Request__c objSSR : lstSSR) {
                Task objTask = TaskUtility.getTask((SObject)objSSR, 'Update Fund Transfer Details in IPMS', 'Finance',
                'Sales Service Request - Fund Transfer', System.today().addDays(1));
                lstTask.add(objTask);
            }
        }
        
        if (!lstTask.isEmpty()) {
            insert lstTask;
        }
    }
}