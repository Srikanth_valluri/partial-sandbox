@IsTest
Public class DAMAC_TrainingServicesTest {
    
    @isTest static void doPostTest() {
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri =
            '/services/apexrest/DAMACTrainingServices/agencies';
        
        request.httpMethod = 'POST';
        RestContext.request = request;
        Map<String, String> params = new Map<String, String>();
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
        RestContext.response = res;
        
        test.starttest();
        DAMAC_TrainingServices.doPost();
        test.stoptest();
    }
     @isTest static void doPostCatchTest() {
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri =
            '/services/apexrest/DAMACTrainingServices/agencies';
        
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = res;
        
        test.starttest();
        DAMAC_TrainingServices.doPost();
        test.stoptest();
    }
    @isTest static void doPostTrining() {
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri = '/services/apexrest/DAMACTrainingServices/trainings';
        
        request.httpMethod = 'POST';
        RestContext.request = request;
        Map<String, String> params = new Map<String, String>();
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
        RestContext.response = res;
        Subject__c sub1 = new Subject__c();
        sub1.name = 'DAMAC Communities 1';
        sub1.Duration__c = '2h';
        sub1.Level__c = 'Level 1 - Silver';
        insert sub1;
        
        Subject__c sub2 = new Subject__c();
        sub2.name = 'DAMAC Communities 2';
        sub2.Duration__c = '2h';
        sub2.Level__c = 'Level 1 - Silver';
        insert sub2;
        
        Training__c train1 = new Training__c();
        train1.Name__c = 'dubai';
        train1.Date_of_Training__c = system.today().addYears(1);
        train1.From_Time_Slot__c = '4:30 PM';
        train1.To_Time_Slot__c = '5:30 PM';
        train1.Subject__c = sub1.id;
        insert train1;
        
        Training__c train2 = new Training__c();
        train2.Name__c = 'dubai';
        train2.Date_of_Training__c = system.today().addYears(1);
        train2.From_Time_Slot__c = '4:30 PM';
        train2.To_Time_Slot__c = '5:30 PM';
        train2.Subject__c = sub2.id;
        insert train2;
        
        test.starttest();
        DAMAC_TrainingServices.doPost();
        test.stoptest();
    }
    @isTest static void doPostattendee() {
        Subject__c sub1 = new Subject__c();
        sub1.name = 'DAMAC Communities 1';
        sub1.Duration__c = '2h';
        sub1.Level__c = 'Level 1 - Silver';
        insert sub1;
        
        Subject__c sub2 = new Subject__c();
        sub2.name = 'DAMAC Communities 2';
        sub2.Duration__c = '2h';
        sub2.Level__c = 'Level 1 - Silver';
        insert sub2;
        
        Training__c train1 = new Training__c();
        train1.Name__c = 'dubai';
        train1.Date_of_Training__c = system.today().addYears(1);
        train1.From_Time_Slot__c = '4:30 PM';
        train1.To_Time_Slot__c = '5:30 PM';
        train1.Subject__c = sub1.id;
        train1.Total_Slots__c = 1;
        insert train1;
        
        Training__c train2 = new Training__c();
        train2.Name__c = 'dubai';
        train2.Date_of_Training__c = system.today().addYears(1);
        train2.From_Time_Slot__c = '4:30 PM';
        train2.To_Time_Slot__c = '5:30 PM';
        train2.Subject__c = sub2.id;
        train2.Total_Slots__c = 1;
        insert train2;
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri =
            '/services/apexrest/DAMACTrainingServices/attendee';
        
        request.httpMethod = 'POST';
        RestContext.request = request;
        String param = '{"data":[{"firstName":"test1","lastName":"test1","email":"test1@test.com","countrycode":"United Arab Emirates: 00971","mobileNo":"123455666","trainingId":"'+train1.Id+'"},{"firstName":"test2","lastName":"test2","email":"test2@test.com","countrycode":"United Arab Emirates: 00971","mobileNo":"234234234","trainingId":"'+train1.Id+'"}]}';
        Map<String, object> params =  (Map<String, object>)JSON.deserializeUntyped(param);
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
        RestContext.response = res;
        DAMACTrainingUtils.Subject sub = new DAMACTrainingUtils.Subject('gap', 'aditya', 'year');
        Date d = Date.newInstance(system.today().year(),system.today().month(),system.today().day());
        set<Id> subjects = new set<Id>();
        subjects.add('a8M1w0000008uxDEAQ');
        DAMACTrainingUtils.DateSlots DateSl  = new DAMACTrainingUtils.DateSlots(d, subjects );
        DAMACTrainingUtils.Training Trn  = new DAMACTrainingUtils.Training('recId','subId','fromTime','toTime',d,'subjName','subjDuration','test', 'code');
        test.starttest();
        DAMAC_TrainingServices.doPost();
        param = '{"data":[{"firstName":"test1","lastName":"test1","email":"test1@test.com","countrycode":"United Arab Emirates: 00971","mobileNo":"123455666","trainingId":"'+train1.Id+'"},{"firstName":"test2","lastName":"test2","email":"test2@test.com","countrycode":"United Arab Emirates: 00971","mobileNo":"234234234","trainingId":"'+train2.Id+'"}]}';
        params =  (Map<String, object>)JSON.deserializeUntyped(param);
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
        RestContext.response = res;
        DAMAC_TrainingServices.doPost();
        test.stoptest();
    }
    
}