Global class MapServiceJsonToApex{

    
        public Meta meta {get;set;} 
        public Response response {get;set;} 

        public MapServiceJsonToApex(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'meta') {
                            meta = new Meta(parser);
                        } else if (text == 'response') {
                            response = new Response(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    
    public static void consumeObject(JSONParser parser) {
        Integer depth = 0;
        do {
            JSONToken curr = parser.getCurrentToken();
            if (curr == JSONToken.START_OBJECT || 
                curr == JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == JSONToken.END_OBJECT ||
                curr == JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
    
    public class Meta {
        public Integer code {get;set;} 
        public String requestId {get;set;} 

        public Meta(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'code') {
                            code = parser.getIntegerValue();
                        } else if (text == 'requestId') {
                            requestId = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Meta consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    public class Contact{
        public String phone {get; set;}
        public Contact(JSONParser parser){
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL){
                        if (text == 'phone') {
                            phone = parser.getText();
                        }else {
                            System.debug(LoggingLevel.WARN, 'Location consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }        
        }      
    }
    public class Hours{
        public String status {get; set;}
        public Hours(JSONParser parser){
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    system.debug('>>>>text>>>'+text);
                    if (parser.nextToken() != JSONToken.VALUE_NULL){
                        if (text == 'status') {
                            status = parser.getText();
                             system.debug('>>>>status>>>'+status);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Location consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }        
        }      
    }
    public class Location {
        public String address {get;set;} 
        public String crossStreet {get;set;} 
        public Double lat {get;set;} 
        public Double lng {get;set;} 
        public Integer distance {get;set;} 
        public String postalCode {get;set;} 
        public String cc {get;set;} 
        public String city {get;set;} 
        public String state {get;set;} 
        public String country {get;set;} 
        public List<String> formattedAddress {get;set;} 
        


        public Location(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'address') {
                            address = parser.getText();
                        } else if (text == 'crossStreet') {
                            crossStreet = parser.getText();
                        } else if (text == 'lat') {
                            lat = parser.getDoubleValue();
                        } else if (text == 'lng') {
                            lng = parser.getDoubleValue();
                        } else if (text == 'distance') {
                            distance = parser.getIntegerValue();
                        } else if (text == 'postalCode') {
                            postalCode = parser.getText();
                        } else if (text == 'cc') {
                            cc = parser.getText();
                        } else if (text == 'city') {
                            city = parser.getText();
                        } else if (text == 'state') {
                            state = parser.getText();
                        } else if (text == 'country') {
                            country = parser.getText();
                        } else if (text == 'formattedAddress') {
                            formattedAddress = new List<String>();
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                                //formattedAddress.add(new String(parser));
                                formattedAddress.add(parser.getText());
                            }
                        } else {
                            System.debug(LoggingLevel.WARN, 'Location consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Venue {
        public Location location {get;set;}
        public Contact contact {get;set;}
        public Hours hours {get;set;}
        public String name {get;set;} 
        public Venue(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'location'){
                            location = new Location(parser);
                        }else if (text == 'contact') {
                            contact = new Contact(parser);
                        } else if (text == 'hours') {
                            hours = new Hours(parser);
                        }else if (text == 'name') {
                            name = parser.getText();
                        }
                    }
                }
            }
        }
    }
    
    public class Items {
        public Venue venue {get;set;} 
        public Items(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'venue'){
                            venue = new Venue(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Groups {
        public List<Items> items {get;set;} 

        public Groups(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'items') {
                            items = new List<Items>();
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                                items.add(new Items(parser));
                            }
                        }
                    }
                }
            }
        }
    }
    

    public class Response {
        public List<Groups> groups {get;set;} 

        public Response(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if(text == 'groups'){
                            groups = new List<Groups>();
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                                groups.add(new Groups(parser));
                            }   
                        }
                    }
                }
            }
        }
    }
}