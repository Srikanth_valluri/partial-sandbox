@isTest
public without sharing Class CreateShipmentCaseLstViewCtrlTest {
    public class FirstFlightServiceMock implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req) {
            req.setMethod('POST');
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String Json = '{ "AirwayBillTrackList": [{ "AirWayBillNo": "12345", "Destination": "ABU DHABI-UNITED ARAB EMIRATES", "ForwardingNumber": "", "Origin": "DUBAI-UNITED ARAB EMIRATES", "ShipmentProgress": 3, "ShipperReference": "AE5248222", "TrackingLogDetails": [{ "ActivityDate": "Saturday 02 November 2019", "ActivityTime": "12:21", "DeliveredTo": "", "Location": "DUBAI", "Remarks": "Location Information Received", "Status": "LC" }, { "ActivityDate": "Saturday 02 November 2019", "ActivityTime": "11:43", "DeliveredTo": "", "Location": "DUBAI", "Remarks": "Whatsapp To Customer", "Status": "SM" } ], "Weight": "1.000" }], "Code": 1, "Description": "Success" }';
            res.setBody(Json);
            res.setStatusCode(200);
            return res;
        }
    }
    
      public class FirstFlightAirwayBillPDFMock implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req) {
            req.setMethod('POST');
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String Json = '{ "Code": 1, "Description": "Success", "ReportDoc": "JVBERi0xLjIgCiXi48/TIAoxIDAgb2JqIAo8PCAKL1R5cGUgL0NhdGFsb2cgCi9QYWdlcyAyIDAgUiAKL1BhZ2VNb2RlIC9Vc2VOb25lIAovVmlld2VyUHJlZmVyZW5jZXMgPDwgCi9GaXRXaW5kb3cgdHJ1ZSAKL1BhZ2VMYXlvdXQgL1NpbmdsZVBhZ2UgCi9Ob25GdWxsUzIAo1MDAgCjI3OCAKNTU2IAo1MDAgCjcyMiAKMCAKNTAwIApdIAplbmRvYmogCjE1IDAgb2JqIAovQUFBQUFBK0FyaWFsIAplbmRvYmogCjE3IDAgb2JqIAo8PCAKL1R5cGUgL0ZvbnREZXNjcmlwdG9yIAovQXNjZW50IDkwNSAKL0NhcEhlaWdodCA1MDAgCi9EZXNjZW50IC0yMTIgCi9GbGFnDAwMDAwIG4gCjAwMDAwNDUyOTggMDAwMDAgbiAKdHJhaWxlciAKPDwgCi9TaXplIDI5IAovUm9vdCAxIDAgUiAKL0luZm8gMjggMCBSIAo+PiAKc3RhcnR4cmVmIAo0NTM4NiAKJSVFT0YgCg==" }';
            res.setBody(Json);
            res.setStatusCode(200);
            return res;
        }
    }
   
    @isTest
    public static void createRequestTest() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.FirstName = 'Test';
        objAcc.LastName = 'Account';
        //objAcc.Master_Address__c = 'Test Address';
        objAcc.Zip_Postal_Code__c = '123456';
        objAcc.City__c = 'Dubai';
        objAcc.Country__c = 'Oman';
        objAcc.Phone_1__c = '123456';
        objAcc.Phone_2__c = '123456';
        objAcc.Fax__c = '123';
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
        /*Account objAcc1 = TestDataFactory_CRM.createPersonAccount();
        objAcc1.FirstName = 'Test';
        objAcc1.LastName = 'Account';
        //objAcc.Master_Address__c = 'Test Address';
        objAcc1.Zip_Postal_Code__c = '123456';
        objAcc1.City__c = 'Dubai';
        objAcc1.Country__c = 'Oman';
        objAcc1.Phone_1__c = '123456';
        objAcc1.Phone_2__c = '123456';
        objAcc1.Fax__c = '123';
        objAcc1.Email__c ='test@test.com';
        objAcc1.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc1 ;*/
        
        List<Case> csList = new List<Case>();
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        csList.add(objCase);
        
        Case objCase1 = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        csList.add(objCase1);
        
        Case objCase2 = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase2.Courier_Request_Already_Created__c = true;
        csList.add(objCase2);
        
        Case objCase3 = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase3.Courier_Request_Already_Created__c = true;
        csList.add(objCase3);
        
        insert csList;
        Credentials_Details__c objCred = new Credentials_Details__c();
        Test.setMock(HttpCalloutMock.class, new FirstFlightServiceMock ());
        objCred.Name = 'First Flight Create AirwayBill';
        objCred.Endpoint__c = 'https://ontrack.firstflightme.com/FFCService.svc/CreateAirwayBill';
        objCred.User_Name__c = '3000';
        objCred.Password__c = 'fftes';
        insert objCred;   
        
        Credentials_Details__c objCred1 = new Credentials_Details__c();
        objCred1.Name = 'First Flight AWB PDF';
        objCred1.Endpoint__c = 'https://ontrack.firstflightme.com/FFCService.svc/Tracking';
        objCred1.User_Name__c = '3000';
        objCred1.Password__c = 'fftes';
        objCred1.Resource__c = '3000';
        insert objCred1; 
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;   
        
        Test.setMock(HttpCalloutMock.class, new FirstFlightAirwayBillPDFMock ());
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(csList);
        stdSetController.setSelected(csList);
        CreateShipmentCaseLstViewCtrl lstController = new CreateShipmentCaseLstViewCtrl(stdSetController);
        //lstController.selectedCS = 'First Flight';
        Test.startTest();
            lstController.createRequest();
        Test.stopTest();
    }
}