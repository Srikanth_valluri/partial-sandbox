/****************************************************************************************************
* Name          : PropertyComputationBatch                                                          *
* Description   : Class to create Property Computation records                                      *
* Created Date  : 09-08-2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER     AUTHOR            DATE            COMMENTS                                                *
* 1.0     Twinkle Panjabi   09-08-2018      Initial Draft.                                          *
****************************************************************************************************/
global class PropertyComputationBatch implements Database.Batchable<sObject>, Database.Stateful{

    //Variables related to storing calculation
    global Map<Id,Decimal> invAndSalesOfferMap = new  Map<Id,Decimal>();
    global Map<Id,Decimal> invAndSalesOfferMapRecords = new  Map<Id,Decimal>();
    global Map<Id,Decimal> invAndProjectDetailMap = new  Map<Id,Decimal>();
    global Map<Id,Decimal> invAndProjectDetailMapRecords = new  Map<Id,Decimal>();
    global Map<Id,Decimal> invAndBookingUnitMap = new  Map<Id,Decimal>();
    global Map<Id,Decimal> invScoreMap = new  Map<Id,Decimal>();
    
    //Get the Custom Setting Record 
    global Property_Computation__c propertyCustomSettingObj;
    
    //Constructor 
    global PropertyComputationBatch(Property_Computation__c propertyCustomSetting) {
        propertyCustomSettingObj = propertyCustomSetting;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = ' Select Id,No_of_Opens__c,Number_of_Clicks__c,Shared_From__c,Inventory__c FROM Share_Project_Details__c WHERE CreatedDate = LAST_N_DAYS:'+Integer.valueOf(propertyCustomSettingObj.Number_of_Days__c);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Share_Project_Details__c> shareProjectList){
        system.debug('shareProjectList------'+shareProjectList);
        Set<Id> invIdSet = new Set<Id>();
        List<Inventory__c> lstInv = new List<Inventory__c>(); 
        for(Share_Project_Details__c shareObj : shareProjectList) {
            if(shareObj.Inventory__c != null) {
                invIdSet.add(shareObj.Inventory__c);
                //Add the No Of Opens and Record Count for Sales Offer
                if(shareObj.Shared_From__c == 'Sales Offer'){
                    //Add the No Of Opens for Sales Offer
                    if(invAndSalesOfferMap.containsKey(shareObj.Inventory__c)){
                        Decimal noOfOpensTemp = shareObj.No_of_Opens__c != null  ? shareObj.No_of_Opens__c : 0 ;
                        Decimal noOfOpens = invAndSalesOfferMap.get(shareObj.Inventory__c) + noOfOpensTemp;
                        invAndSalesOfferMap.put(shareObj.Inventory__c, noOfOpens );
                    } else{
                        Decimal noOfOpens = shareObj.No_of_Opens__c != null  ? shareObj.No_of_Opens__c : 0 ;
                        invAndSalesOfferMap.put(shareObj.Inventory__c, noOfOpens );
                    }  
                    //Add the No Of Record Count for Sales Offer
                    if(invAndSalesOfferMapRecords.containsKey(shareObj.Inventory__c)){
                        Decimal noOfRecords = invAndSalesOfferMap.get(shareObj.Inventory__c)  + 1;
                        invAndSalesOfferMapRecords.put(shareObj.Inventory__c, noOfRecords );
                    } else{
                        invAndSalesOfferMapRecords.put(shareObj.Inventory__c, 1 );
                    }  
                }
                //Add the No Of Clicks and Record Count for Project Details
                if(shareObj.Shared_From__c == 'Project Details'){
                    //Add the No Of Clicks for Project Details
                    if(invAndProjectDetailMap.containsKey(shareObj.Inventory__c)){
                        Decimal noOfClicksTemp = shareObj.Number_of_Clicks__c != null  ? shareObj.Number_of_Clicks__c : 0 ;
                        Decimal noOfClicks = invAndProjectDetailMap.get(shareObj.Inventory__c) + noOfClicksTemp;
                        invAndProjectDetailMap.put(shareObj.Inventory__c, noOfClicks );
                    } else{
                        Decimal noOfClicks = shareObj.Number_of_Clicks__c != null ? shareObj.Number_of_Clicks__c : 0 ;
                        invAndProjectDetailMap.put(shareObj.Inventory__c, noOfClicks );
                    } 
                    //Add the No Of Record Count for Project Details
                    if(invAndProjectDetailMapRecords.containsKey(shareObj.Inventory__c)){
                        Decimal noOfRecords = invAndProjectDetailMapRecords.get(shareObj.Inventory__c)  + 1;
                        invAndProjectDetailMapRecords.put(shareObj.Inventory__c,noOfRecords );
                    } else{
                        invAndProjectDetailMapRecords.put(shareObj.Inventory__c, 1 );
                    }    
                }
            }
        }
        Set<String> regNameToExclude = new Set<String>();
        for(Registration_Status_to_Exclude__mdt objMetaData: [SELECT MasterLabel, QualifiedApiName FROM Registration_Status_to_Exclude__mdt LIMIT 1]){
            regNameToExclude.add(objMetaData.MasterLabel);
        }
        lstInv = [  SELECT Id, Name, 
                        ( 
                            SELECT Id, Name FROM Booking_Units__r 
                            WHERE  Registration_Status__c != '' 
                            AND Registration_Status__c NOT in: regNameToExclude
                        ) 
                    FROM Inventory__c 
                    WHERE ID IN:invIdSet];

        for (Inventory__c objInv: lstInv) {
            invAndBookingUnitMap.put(objInv.Id,objInv.Booking_Units__r.size());
        }

        for(Id  invId : invIdSet) {
            Decimal noOfOpens          = invAndSalesOfferMap.containsKey(invId) ?  invAndSalesOfferMap.get(invId) : 0;
            Decimal noOfClicks         = invAndProjectDetailMap.containsKey(invId) ?  invAndProjectDetailMap.get(invId) : 0;
            Decimal noOfBooking        = invAndBookingUnitMap.containsKey(invId) ?  invAndBookingUnitMap.get(invId) : 0;
            Decimal noOfShareRecords   = invAndSalesOfferMapRecords.containsKey(invId) ?  invAndSalesOfferMapRecords.get(invId) : 0;
            Decimal noOfProjectRecords = invAndProjectDetailMapRecords.containsKey(invId) ?  invAndProjectDetailMapRecords.get(invId) : 0;
            Decimal totalScore         = (noOfOpens* propertyCustomSettingObj.Number_of_Opens__c) + 
                                         (noOfShareRecords * propertyCustomSettingObj.Number_of_Records_for_Sales_Offer__c) +
                                         (noOfClicks* propertyCustomSettingObj.Number_of_Clicks__c) + 
                                         (noOfProjectRecords * propertyCustomSettingObj.Number_of_Records_for_Project_Details__c) +
                                         (noOfBooking* propertyCustomSettingObj.Number_of_Booking_Units__c);
            if(invScoreMap.containsKey(invId)) {
                Decimal totalScoreTemp = invScoreMap.get(invId);
                invScoreMap.put(invId, totalScore+totalScoreTemp);

            } else {
                invScoreMap.put(invId, totalScore);
            }
        }
        system.debug('========invScoreMap'+invScoreMap);

    }
    
    global void finish(Database.BatchableContext BC){
        //After the values are filled with Computation then the records need to be sorted
        //Add that Map Values to List of Wrapper to be Sorted
        List<InvWithComputation> invComputedWrapperList = new List<InvWithComputation>();
        for(Id invId :invScoreMap.keySet()){
               invComputedWrapperList.add(new InvWithComputation
                                                    ( 
                                                      invId,
                                                      invScoreMap.get(invId)
                                                    ));               
                                                
        }
        invComputedWrapperList.sort();
        //Implemented Bubble Sort as Comparable Interface is not working for Decimal Value
        /*for(Integer i = 0; i < invComputedWrapperList.size(); i++){
            for(Integer j = 1; j < (invComputedWrapperList.size()-i); j++){
                Decimal firstValue = Decimal.valueof(String.valueof(invComputedWrapperList[j-1].propertyComputation));
                Decimal nextValue = Decimal.valueof(String.valueof(invComputedWrapperList[j].propertyComputation));
                //if firstValue < nextValue, swap the elements
                if(firstValue < nextValue){
                    InvWithComputation tmpValue = invComputedWrapperList[j-1];
                    invComputedWrapperList[j-1]=invComputedWrapperList[j];
                    invComputedWrapperList[j]=tmpValue;
                }
            }
        }*/
        system.debug('========invComputedWrapperList'+invComputedWrapperList);
        List<Property_Ranking__c> propertyRankingList = new  List<Property_Ranking__c>();
        Integer rank = 1;
        for(InvWithComputation invCompWrap :invComputedWrapperList) {
            if(rank <= 20){
                Property_Ranking__c propertyRankingObj = new Property_Ranking__c();
                propertyRankingObj.Inventory__c = invCompWrap.invId;
                propertyRankingObj.Rank__c = rank;
                propertyRankingObj.Computed_Score__c = invCompWrap.propertyComputation;
                propertyRankingList.add(propertyRankingObj);
                rank++;
            }
                    
        }
        if(!propertyRankingList.isEmpty()) {
            insert propertyRankingList;
        }
        system.debug('========propertyRankingList'+propertyRankingList);
    }

    //Wrapper for Inventory
    public class InvWithComputation implements Comparable{ 
        public String invId;
        public Decimal propertyComputation;
        public InvWithComputation(String invId, Decimal propertyComputation) { 
            this.invId = invId; 
            this.propertyComputation = propertyComputation; 
        } 
        public Integer compareTo(Object other) {
            // for descending order
            return Integer.valueOf(((InvWithComputation)other).propertyComputation - propertyComputation) ;
        }
    }
    
}