/**
 * Ver       Date            Author      		    Modification
 * 1.0    9/30/2019         Arsh Dave              Initial Version 
 * 2.0    10/08/2019        Arsh Dave              Updated the fields to random fields as new fields cannot be deployet on production  
**/
@isTest
public class TaskOwnerIdEscalationSetTest{

    static List<Task> listTask = new List<Task>();
    static List<Task> listTask_1 = new List<Task>();

    @testSetup static void setup(){
        
        UserRole objRole = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert objRole;

        User objUserManager = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'Muser000@amamama.com',
            Username = 'Muser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias_M',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = objRole.Id
        );
        insert objUserManager;

        User objUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = objRole.Id,
            ManagerId = objUserManager.Id
        );
        insert objUser;
    }

    static testMethod  void callConstructor() {
        
        User objUser = [SELECT Id FROM User WHERE Email = 'puser000@amamama.com'];

        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        objAccount.OwnerId = objUser.Id;
        insert objAccount;

        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
        insert objBooking;
        
        Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
        insert objBooking_Unit;

        FM_Case__c objFMCase = new FM_Case__c(
            Account__c = objAccount.Id,
            Category_GE__c = 'Change of Contact Details',
            Subcategory_GE__c = 'Change registered email id',
            RecordTypeId = System.Label.FM_Case_General_Enquiry
        );
        insert objFMCase;

        List<Task> objTask = [
            SELECT Id
                , Status
                , ActivityDate
                , WhatId
                , OwnerId
                , Age__c
                , CreatedDate
                , Subject
             FROM Task
            WHERE What.Id = :objFMCase.Id   
        ];

        objTask[0].ActivityDate = System.Today();
        update objTask;
        // static List<Task> listTaskInsert = new List<Task>();
        listTask_1.add(objTask[0]);

        Test.startTest();
            
            // TaskEscalationCustomerSRProcessBatch objTaskEscalationCustomerSRProcessBatch = new TaskEscalationCustomerSRProcessBatch();
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            // Database.executebatch(objTaskEscalationCustomerSRProcessBatch,1);
            TaskOwnerIdEscalationSet objTaskOwnerIdEscalationSet = new TaskOwnerIdEscalationSet();
            TaskOwnerIdEscalationSet.updateDueDate(listTask_1);
            
        Test.stopTest();
        
    }

    static testMethod  void callBulkMethod() {
        
        User objUser = [SELECT Id FROM User WHERE Email = 'puser000@amamama.com'];

        User objUser_1 = [SELECT Id FROM User WHERE Email = 'Muser000@amamama.com'];

        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        objAccount.OwnerId = objUser.Id;
        insert objAccount;

        Account objAccount_1 = TestDataFactoryFM.createAccount();
        objAccount_1.Email__c = 'test123@test.com'; 
        objAccount_1.OwnerId = objUser_1.Id;
        insert objAccount_1;

        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
        insert objBooking;
        
        Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
        insert objBooking_Unit;

        List<FM_Case__c> listFM_Case = new List<FM_Case__c>();
        for(Integer i=0; i>=100; i++){
            FM_Case__c objFMCase = new FM_Case__c(
            Account__c = objAccount.Id,
            Category_GE__c = 'Change of Contact Details',
            Subcategory_GE__c = 'Change registered email id',
            RecordTypeId = System.Label.FM_Case_General_Enquiry
            );
            listFM_Case.add(objFMCase);
            FM_Case__c objFMCase_1 = new FM_Case__c(
            Account__c = objAccount_1.Id,
            Category_GE__c = 'Change of Contact Details',
            Subcategory_GE__c = 'Change registered email id',
            RecordTypeId = System.Label.FM_Case_General_Enquiry
            );
            listFM_Case.add(objFMCase_1);
        }

        
        insert listFM_Case;
        Set<Id> setFMCaseId = new Set<Id>();
        for(FM_Case__c objFM_CaseLoop: listFM_Case){
            setFMCaseId.add(objFM_CaseLoop.Id);
        }
        
        
        for(Task objTask : [
            SELECT Id
                , Status
                , ActivityDate
                , WhatId
                , OwnerId
                , Age__c
                , CreatedDate
                , Subject
            FROM Task
            WHERE What.Id = :setFMCaseId   
        ]){
            objTask.ActivityDate = System.Today();
            listTask.add(objTask);
        }

        update listTask;
        
        Test.startTest();
            
            // TaskEscalationCustomerSRProcessBatch objTaskEscalationCustomerSRProcessBatch = new TaskEscalationCustomerSRProcessBatch();
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            // Database.executebatch(objTaskEscalationCustomerSRProcessBatch,1);
            TaskOwnerIdEscalationSet objTaskOwnerIdEscalationSet = new TaskOwnerIdEscalationSet();
            for(Task objTaskloop: listTask){
                TaskOwnerIdEscalationSet.ReturnWrapper objReturnWrapperTest;
                objReturnWrapperTest = TaskOwnerIdEscalationSet.callFromBatch(objTaskloop);
            }
            
        Test.stopTest();
        
    }

    static testMethod  void callConstructorBatchSLA() {
        
        User objUser = [SELECT Id FROM User WHERE Email = 'puser000@amamama.com'];

        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        objAccount.OwnerId = objUser.Id;
        insert objAccount;

        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
        insert objBooking;
        
        Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
        insert objBooking_Unit;

        FM_Case__c objFMCase = new FM_Case__c(
            Account__c = objAccount.Id,
            Category_GE__c = 'Change of Contact Details',
            Subcategory_GE__c = 'Change registered email id',
            RecordTypeId = System.Label.FM_Case_General_Enquiry
        );
        insert objFMCase;

        List<Task> objTask = [
            SELECT Id
                , Status
                , ActivityDate
                , WhatId
                , OwnerId
                , Age__c
                , CreatedDate
                , Subject
             FROM Task
            WHERE What.Id = :objFMCase.Id   
        ];

        objTask[0].ActivityDate = System.Today();
        update objTask;
        // static List<Task> listTaskInsert = new List<Task>();

        Test.startTest();
            TaskOwnerIdEscalationSet.ReturnWrapper objReturnWrapperTest;
            // TaskEscalationCustomerSRProcessBatch objTaskEscalationCustomerSRProcessBatch = new TaskEscalationCustomerSRProcessBatch();
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            // Database.executebatch(objTaskEscalationCustomerSRProcessBatch,1);
            TaskOwnerIdEscalationSet objTaskOwnerIdEscalationSet = new TaskOwnerIdEscalationSet();
            objReturnWrapperTest = TaskOwnerIdEscalationSet.callFromBatch(objTask[0]);
            
        Test.stopTest();
        
    }

    static testMethod  void callConstructorBatchLevel1() {
        
        User objUser = [SELECT Id FROM User WHERE Email = 'puser000@amamama.com'];

        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        objAccount.OwnerId = objUser.Id;
        insert objAccount;

        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
        insert objBooking;
        
        Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
        insert objBooking_Unit;

        FM_Case__c objFMCase = new FM_Case__c(
            Account__c = objAccount.Id,
            Category_GE__c = 'Change of Contact Details',
            Subcategory_GE__c = 'Change registered email id',
            RecordTypeId = System.Label.FM_Case_General_Enquiry
        );
        insert objFMCase;

        List<Task> objTask = [
            SELECT Id
                , Status
                , ActivityDate
                , WhatId
                , OwnerId
                , Age__c
                , CreatedDate
                , Subject
             FROM Task
            WHERE What.Id = :objFMCase.Id   
        ];

        objTask[0].ActivityDate = System.Today();
        objTask[0].Age__c = 'Level 1';
        update objTask;
        // static List<Task> listTaskInsert = new List<Task>();

        Test.startTest();
            TaskOwnerIdEscalationSet.ReturnWrapper objReturnWrapperTest;
            // TaskEscalationCustomerSRProcessBatch objTaskEscalationCustomerSRProcessBatch = new TaskEscalationCustomerSRProcessBatch();
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            // Database.executebatch(objTaskEscalationCustomerSRProcessBatch,1);
            TaskOwnerIdEscalationSet objTaskOwnerIdEscalationSet = new TaskOwnerIdEscalationSet();
            objReturnWrapperTest = TaskOwnerIdEscalationSet.callFromBatch(objTask[0]);
            
        Test.stopTest();
        
    }

    static testMethod  void callConstructorBatchLevel2() {
        
        User objUser = [SELECT Id FROM User WHERE Email = 'puser000@amamama.com'];

        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        objAccount.OwnerId = objUser.Id;
        insert objAccount;

        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
        insert objBooking;
        
        Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
        insert objBooking_Unit;

        FM_Case__c objFMCase = new FM_Case__c(
            Account__c = objAccount.Id,
            Category_GE__c = 'Change of Contact Details',
            Subcategory_GE__c = 'Change registered email id',
            RecordTypeId = System.Label.FM_Case_General_Enquiry
        );
        insert objFMCase;

        List<Task> objTask = [
            SELECT Id
                , Status
                , ActivityDate
                , WhatId
                , OwnerId
                , Age__c
                , CreatedDate
                , Subject
             FROM Task
            WHERE What.Id = :objFMCase.Id   
        ];

        objTask[0].ActivityDate = System.Today();
        objTask[0].Age__c = 'Level 2';
        update objTask;
        // static List<Task> listTaskInsert = new List<Task>();

        Test.startTest();
            TaskOwnerIdEscalationSet.ReturnWrapper objReturnWrapperTest;
            // TaskEscalationCustomerSRProcessBatch objTaskEscalationCustomerSRProcessBatch = new TaskEscalationCustomerSRProcessBatch();
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            // Database.executebatch(objTaskEscalationCustomerSRProcessBatch,1);
            TaskOwnerIdEscalationSet objTaskOwnerIdEscalationSet = new TaskOwnerIdEscalationSet();
            objReturnWrapperTest = TaskOwnerIdEscalationSet.callFromBatch(objTask[0]);
            
        Test.stopTest();
        
    }

    static testMethod  void callConstructorBatchLevel3() {
        
        User objUser = [SELECT Id FROM User WHERE Email = 'puser000@amamama.com'];

        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        objAccount.OwnerId = objUser.Id;
        insert objAccount;

        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
        insert objBooking;
        
        Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
        insert objBooking_Unit;

        FM_Case__c objFMCase = new FM_Case__c(
            Account__c = objAccount.Id,
            Category_GE__c = 'Change of Contact Details',
            Subcategory_GE__c = 'Change registered email id',
            RecordTypeId = System.Label.FM_Case_General_Enquiry
        );
        insert objFMCase;

        List<Task> objTask = [
            SELECT Id
                , Status
                , ActivityDate
                , WhatId
                , OwnerId
                , Age__c
                , CreatedDate
                , Subject
             FROM Task
            WHERE What.Id = :objFMCase.Id   
        ];

        objTask[0].ActivityDate = System.Today();
        objTask[0].Age__c = 'Level 3';
        update objTask;
        // static List<Task> listTaskInsert = new List<Task>();

        Test.startTest();
            TaskOwnerIdEscalationSet.ReturnWrapper objReturnWrapperTest;
            // TaskEscalationCustomerSRProcessBatch objTaskEscalationCustomerSRProcessBatch = new TaskEscalationCustomerSRProcessBatch();
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            // Database.executebatch(objTaskEscalationCustomerSRProcessBatch,1);
            TaskOwnerIdEscalationSet objTaskOwnerIdEscalationSet = new TaskOwnerIdEscalationSet();
            objReturnWrapperTest = TaskOwnerIdEscalationSet.callFromBatch(objTask[0]);
            
        Test.stopTest();
        
    }

    static testMethod  void callConstructorBatchLevelZero() {
        
        User objUser = [SELECT Id FROM User WHERE Email = 'puser000@amamama.com'];

        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        objAccount.OwnerId = objUser.Id;
        insert objAccount;

        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
        insert objBooking;
        
        Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
        insert objBooking_Unit;

        FM_Case__c objFMCase = new FM_Case__c(
            Account__c = objAccount.Id,
            Category_GE__c = 'Change of Contact Details',
            Subcategory_GE__c = 'Change registered email id',
            RecordTypeId = System.Label.FM_Case_General_Enquiry
        );
        insert objFMCase;

        List<Task> objTask = [
            SELECT Id
                , Status
                , ActivityDate
                , WhatId
                , OwnerId
                , Age__c
                , CreatedDate
                , Subject
             FROM Task
            WHERE What.Id = :objFMCase.Id   
        ];

        objTask[0].ActivityDate = System.Today();
        objTask[0].Age__c = '';
        update objTask;
        // static List<Task> listTaskInsert = new List<Task>();

        Test.startTest();
            TaskOwnerIdEscalationSet.ReturnWrapper objReturnWrapperTest;
            // TaskEscalationCustomerSRProcessBatch objTaskEscalationCustomerSRProcessBatch = new TaskEscalationCustomerSRProcessBatch();
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            // Database.executebatch(objTaskEscalationCustomerSRProcessBatch,1);
            TaskOwnerIdEscalationSet objTaskOwnerIdEscalationSet = new TaskOwnerIdEscalationSet();
            objReturnWrapperTest = TaskOwnerIdEscalationSet.callFromBatch(objTask[0]);
            
        Test.stopTest();
        
    }


}