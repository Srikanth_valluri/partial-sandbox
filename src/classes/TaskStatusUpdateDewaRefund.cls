/***********************************************************************************************************
Description: Class to update Task Status
-----------------------------------------------------------------------------------------------------------*
Version     Date                Author              Description                                 
1.0         10 Nov 2020         Komal Shitole       Initial Draft
************************************************************************************************************/


@RestResource(urlMapping='/TaskStatusUpdateDewaRefund/*')
global with sharing class TaskStatusUpdateDewaRefund {  

 /********************************************************************************************************
     Method Description :  Method to update Task Status
    * Return Type : ResponseWrapper - Response with either Success or Error status.
 ********************************************************************************************************/
 
    @HttpPatch
    global static ResponseWrapper TaskStatusUpdateDewaRefund(String CaseNum, String TaskSubject, String ApprovalStatus, String Comments) {
        ResponseWrapper objResponseWrapper = new ResponseWrapper();
        
        //if(objTaskWrap != null) {
             if(String.isNotBlank(CaseNum)) {
                List<Case> caseList = [SELECT Id,
                                          CaseNumber
                                  FROM Case
                                  WHERE CaseNumber =: CaseNum];
                if(caseList == Null && caseList.isEmpty()) {
                     objResponseWrapper.status = 'No Case exists, please select correct caseNumber.';
                     objResponseWrapper.statusCode = '400';
                }
            
                if(!caseList.isEmpty()) {
                    List<Task> taskList = [SELECT Id,
                                                Subject,
                                                WhatId                              
                                        FROM   Task
                                        Where WhatId =: caseList[0].Id
                                        AND Subject =: TaskSubject];
                    if(taskList != Null && taskList.size() >0) {
                        List<Task> tasksToBeUpdated = new List<Task>();
                        for(Task objTask : taskList) {
                            Task taskObj = new Task();
                            taskObj.Id = objTask.Id;
                           if(String.isNotBlank(ApprovalStatus)) {
                               taskObj.Status = ApprovalStatus;
                           }
                           if(String.isNotBlank(Comments)) {
                               taskObj.Description = Comments;
                           }
                            tasksToBeUpdated.add(taskObj);
                        }
                        if(tasksToBeUpdated.size() > 0) {
                            try{
                                Database.upsert( tasksToBeUpdated );
                                objResponseWrapper.status = 'Details Updated Sucessfully';
                                objResponseWrapper.statusCode = '200';
                                
                            } 
                            catch( Exception ex ) {
                                system.debug( ex.getMessage() );
                                objResponseWrapper.errorMessage = ex.getMessage();
                                objResponseWrapper.statusCode = '400';         
                            }   
                        }
                    }
                    else {
                            objResponseWrapper.errorMessage = 'No Task Exists';
                            objResponseWrapper.statusCode = '400';     
                        
                    }
                }
            }
            else {
                     objResponseWrapper.status = 'CaseNumber not provided';
                     objResponseWrapper.statusCode = '400';
            }

        //}
        return objResponseWrapper;
        
    }
    
    global class ResponseWrapper {
        global String status;
        global String statusCode;
        global String errorMessage;
        
        global ResponseWrapper() {
            this.status = '';
            this.statusCode = '';
            this.errorMessage = '';
        }
    }
    
}