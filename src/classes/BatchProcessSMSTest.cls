@isTest
private class BatchProcessSMSTest {

    @isTest
    static void myUnitTestOne() {

        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
        
        SMS_History__c objSMS = new SMS_History__c();
        objSMS.Message__c = 'Test';
        objSMS.Is_SMS_Sent__c = false ;
        objSMS.Customer__c  = acctIns.Id;
        objSMS.Phone_Number__c = '0097154856556';
        insert objSMS;       

        test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpAgentOTPController());                       
            Database.executeBatch( new BatchProcessSMS('', new list<Id>{objSMS.Id}) );
        test.stopTest();
    }
}