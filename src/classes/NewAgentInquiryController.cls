/**********************************************************************************************************************
* VERSION   AUTHOR            DATE          COMMENTS                                                                  *
* CH01      CRAIG             22/02/2018    Added default values to Agency Type and Inquiry Status.                   *
**********************************************************************************************************************/
public class NewAgentInquiryController {

    public Inquiry__c inquiryObj        {set; get;}
    public Boolean saveRecordFlag       {get; set;}
    public Boolean fieldValidation      {get; set;}
    public Boolean isInquiryInsert      {get; set;}
    public String inquirySuccessMsg     {get; set;}
    public String inquiryOwner          {get; set;}
    public String inquiryCampaign       {get; set;}

    public NewAgentInquiryController() {
        inquirySuccessMsg = '';
        inquiryOwner = '';
        inquiryCampaign = '';
        fieldValidation = false;
        saveRecordFlag = false;
        Date todayDate = Date.today();
        inquiryObj = new Inquiry__c();
        inquiryObj.Preferred_Language__c = '';
        inquiryObj.Mobile_CountryCode__c = '';
    }

    public void saveInquiry() {
        String actualNumber = '';
        String trimmedNumber = '';
        if (true) {
            actualNumber = inquiryObj.Mobile_Phone_Encrypt__c;
            if (String.isNotBlank(actualNumber)) {
                integer first = 0;
                integer temp = 0;
                integer offset = 9;
                integer last = actualNumber.length();
                if(actualNumber.length() > 9) {
                    while((first+offset)<last){
                        /*system.debug(trimmedNumber);
                        system.debug('## ' + first + '## ' + offset + '## ' + last);
                        system.debug(actualNumber.substring(first, first+offset));
                        system.debug(Integer.valueOf(actualNumber.substring(first,first+offset)));*/
                        trimmedNumber += String.valueOf(Integer.valueOf(actualNumber.substring(first, first + offset)));
                        first+=offset;
                    }
                    trimmedNumber += String.valueOf(Integer.valueOf(actualNumber.substring(first, last)));
                    inquiryObj.Mobile_Phone_Encrypt__c = trimmedNumber;
                } else{
                    inquiryObj.Mobile_Phone_Encrypt__c = String.valueOf(Integer.valueOf(actualNumber));
                }
            }
            //system.debug(inquiryObj.Mobile_Phone_Encrypt__c);
            inquiryObj.Inquiry_Source__c = 'Digital';
            
            // CH01  CRAIG  22/02/2018
            // Added default values to Agency Type and Inquiry Status.
            inquiryObj.Inquiry_Status__c = 'Potential Agent';
            inquiryObj.Agency_Type__c = 'Corporate';
            inquiryObj.Inquiry_Source__c = 'DAMAC Website';
            // Added the Authorized_Signatory__c values to the First Name and Last Name Fields
            inquiryObj.Last_Name__c = ''; 
            String inquiryName = inquiryObj.Authorized_Signatory__c;
            List<String> splitInquiryName = inquiryName.split(' ');
            inquiryObj.Last_Name__c = ''; 
            for (Integer i = 0; i < splitInquiryName.size(); i++) {
                if(i == 0) {
                    inquiryObj.First_Name__c = splitInquiryName[i];
                } else {
                    inquiryObj.Last_Name__c += ' ' 
                                            + splitInquiryName[i];
                }
            }
            system.debug('## inquiryObj.First_Name__c >>>>>  ' + inquiryObj.First_Name__c);
            system.debug('## inquiryObj.Last_Name__c >>>>>  ' + inquiryObj.Last_Name__c);
            if (String.isBlank(inquiryObj.Last_Name__c)) {
                inquiryObj.Last_Name__c = splitInquiryName[0];
            }
            system.debug('## inquiryObj.First_Name__c >>>>>  ' + inquiryObj.First_Name__c);
            system.debug('## inquiryObj.Last_Name__c >>>>>  ' + inquiryObj.Last_Name__c);

            // CH01  CRAIG  22/02/2018.

            Id inquiryRecordTypeId =
                Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_Agent_Team_RT).getRecordTypeId();
            if (inquiryRecordTypeId != null) {
                inquiryObj.RecordTypeId = inquiryRecordTypeId;
            }

            //Issue 237  changes : Tejashree Chavan
            // Commented by Craig - Bad Values as Country is not populated
            //checkCityValue();
            
            try{
                system.debug('## inquiryObj BEFORE INSERT >>>>>  ' + inquiryObj);
                insert inquiryObj;
                system.debug('## inquiryObj INSERTED >>>>>  ' + inquiryObj);
                isInquiryInsert = true;
                if (inquiryObj.Id != null) {
                    List<Inquiry__c> inquiryList = [SELECT Id, Name FROM Inquiry__c WHERE Id = :inquiryObj.Id];
                    if (!inquiryList.isEmpty()) {
                        inquirySuccessMsg = '  Inquiry Saved with inquiry number: ' + inquiryList[0].Name;
                    }

                    if (String.isNotBlank(inquiryOwner)) {
                        inquiryObj.OwnerId = inquiryOwner;
                        inquiryObj.Assigned_To__c = inquiryOwner;
                        update inquiryObj;
                        system.debug('## inquiryObj UPDATED >>>>>  ' + inquiryObj);
                    }
                }

                inquiryObj = new Inquiry__c();
                inquiryOwner = '';
                inquiryCampaign = '';
                saveRecordFlag = true;
            } catch(Exception ex){
                system.debug('ex.getStackTraceString >>>> ' +  ex.getStackTraceString());
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error ,ex.getMessage()));
            }
        }
    }


    public void checkCityValue() {
        system.debug('----inquiryObj1----'+inquiryObj);
        system.debug('----inquiryObj.City__c2----'+inquiryObj.City__c);
        if(String.isNotBlank(inquiryObj.City__c)) {
            Schema.DescribeFieldResult fieldResult = Inquiry__c.City_new__c.getDescribe();
            List<PicklistEntry> entries = fieldResult.getPicklistValues();
            system.debug('----entries----'+entries);
            for(Schema.picklistEntry f:entries) {
                if(inquiryObj.City__c.equalsIgnoreCase(f.getValue())) {
                    inquiryObj.City_new__c = inquiryObj.City__c;
                }
            }
            if(String.isBlank(inquiryObj.City_new__c)) {
                inquiryObj.City_new__c = 'Others';
            }
        }
    }

}