/*
Description:  Rest Response Class for WHATSAPP CLIENT to read data from WHATSAPP
Developed By: DAMAC IT Team
*/


@RestResource(urlMapping='/whatsapphook/*')
global without sharing class DAMAC_WA_REST_SERVICES{   


    @HttpPost
    global static void doPost(){    
        try{      
            system.debug(RestContext.request);
            system.debug(RestContext.request.params);
            system.debug(RestContext.request.params.size());
            
            
            if (RestContext.request.params.size() > 0) {             
                map<string,string> respMap = new map<string,string>();
                respMap = RestContext.request.params;
                
                
                
                 
                // Event Ack
                if(respMap.containskey('event') && respMap.get('event') == 'ack'){
                    string cuid = respMap.get('cuid');
                    Whats_App_Deliverability__c delInSys = [select id,messagecuid__c from Whats_App_Deliverability__c where messagecuid__c =:cuid LIMIT 1];
                    for(string s:respMap.keyset()){
                        if(s == 'muid'){
                            delInSys.messageuid__c = respMap.get(s);                        
                        }
                        if(s == 'cuid'){
                            delInSys.messagecuid__c = respMap.get(s);                        
                        }
                        if(s == 'ack'){
                            delInSys.messageack__c = respMap.get(s);                        
                        }
                        if(s == 'event'){
                            delInSys.Event__c = respMap.get(s);                        
                        }
                        if(s == 'token'){
                            delInSys.token__c = respMap.get(s);                        
                        }                    
                    }   
                    delInSys.response__c = string.valueof(RestContext.request);
                    upsert delInSys messagecuid__c;             
                }  
                
                // Event Message Incoming in to Salesforce 
                if(respMap.containskey('event') && respMap.get('event') == 'message' && respMap.get('message[dir]') == 'i'){
                    string cuid = respMap.get('contact[uid]');
                    system.debug('num>>>'+cuid);
                    Whats_App_Message__c mes = [select id,Unique_Id__c,lastmodifiedbyid from Whats_App_Message__c where Number__c=:cuid limit 1];
                    
                    Whats_App_Deliverability__c delInSys = new Whats_App_Deliverability__c();
                    delInSys.ownerid = mes.lastmodifiedbyid;
                    delInSys.Whats_App_Message__c = mes.id;
                    
                    
                    
                    for(string s:respMap.keyset()){
                        
                        if(s == 'message[uid]'){
                            delInSys.messageuid__c = respMap.get(s);                        
                        }
                        if(s == 'message[dtm]'){
                            delInSys.messagedtm__c = respMap.get(s);                        
                        }
                        if(s == 'message[body][text]'){
                            delInSys.messagebodytext__c = respMap.get(s);                        
                        }
                        if(s == 'contact[name]'){
                            delInSys.contactname__c = respMap.get(s);                        
                        }
                        if(s == 'token'){
                            delInSys.token__c = respMap.get(s);                        
                        }
                        if(s == 'uid'){
                            delInSys.uid__c = respMap.get(s);                        
                        }  
                        if(s == 'message[cuid]'){
                            delInSys.messagecuid__c =  cuid+mes.lastmodifiedbyid+string.valueof(system.now());                      
                        }
                        if(s == 'message[type]'){
                            delInSys.messagetype__c=   respMap.get(s);
                            
                        }
                        if(s == 'contact[uid]'){
                            delInSys.contactuid__c =   respMap.get(s);  
                        }
                        if(s == 'event'){
                            delInSys.event__c =   respMap.get(s);  
                        }
                        if(s == 'message[dir]'){
                            delInSys.messagedir__c =   respMap.get(s);  
                        }
                        if(s == 'message[ack]'){
                            delInSys.messageack__c =   respMap.get(s);  
                        }
                        if(s == 'contact[type]'){
                            delInSys.contacttype__c  =   respMap.get(s);  
                        }
                        if(s == 'message[body][caption]'){
                            delInSys.message_body_caption__c = respMap.get(s);  
                        }
                        if(s == 'message[body][contact]'){
                            delInSys.message_body_contact__c = respMap.get(s);                     
                        }
                        
                        if(s == 'message[body][duration]'){
                            delInSys.message_body_duration__c = respMap.get(s);                      
                        }
                        if(s == 'message[body][lat]'){
                            delInSys.message_body_lat__c = respMap.get(s);                     
                        }
                        if(s == 'message[body][lng]'){
                            delInSys.message_body_lng__c = respMap.get(s);                     
                        }
                        if(s == 'message[body][mimetype]'){
                            delInSys.message_body_mimetype__c = respMap.get(s);                     
                        }
                        if(s == 'message[body][name]'){
                            delInSys.message_body_name__c = respMap.get(s);                     
                        }
                        if(s == 'message[body][size]'){
                            delInSys.message_body_size__c  = respMap.get(s);                    
                        }
                        if(s == 'message[body][text]'){
                            delInSys.messagebodytext__c  = respMap.get(s);                    
                        }
                        if(s == 'message[body][thumb]'){
                            //delInSys.message_body_thumb__c  = respMap.get(s);                    
                        }
                        if(s == 'message[body][url]'){
                            delInSys.message_body_url__c = respMap.get(s);                     
                        }
                        if(s == 'message[body][vcard]'){
                            delInSys.message_body_vcard__c = respMap.get(s);                     
                        }                   
                        
                        if(s == 'message[dtm]'){
                            delInSys.messagedtm__c = respMap.get(s);                     
                        }                  
                        if(s == 'muid'){
                            delInSys.muid__c  = respMap.get(s);                    
                        }
                                          
                    }   
                    //delInSys.response__c = delInSys.response__c+string.valueof(RestContext.request);
                    upsert delInSys messagecuid__c;                 
                              
                }
                
                
            }  
            }catch(exception e){
                map<string,string> respMap = new map<string,string>();
                respMap = RestContext.request.params;
                list<string> emails =  new list<string>{Label.SF_Notifications_Email};
                Messaging.singleEmailmessage Email = new Messaging.singleEmailmessage();
                email.setsubject('Error from WA ');
                email.SetPlainTextbody(string.valueof(respMap)+'-->'+e.getmessage());            
                Email.SetToAddresses(emails);    
                Messaging.sendemailResult[] r = Messaging.sendemail(new Messaging.singleEmailmessage[]{email});
            } 
                /*
                map<string,string> respMap = new map<string,string>();
                respMap = RestContext.request.params;
                list<string> emails =  new list<string>{'sfsupport@damacgroup.com'};
                Messaging.singleEmailmessage Email = new Messaging.singleEmailmessage();
                email.setsubject('Error from WA ');
                email.SetPlainTextbody(string.valueof(respMap));            
                Email.SetToAddresses(emails);    
                Messaging.sendemailResult[] r = Messaging.sendemail(new Messaging.singleEmailmessage[]{email});   
                */  
        
        }
    
    
   
}