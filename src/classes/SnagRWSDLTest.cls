/**********************************************************************************************
* Description - Test class developed for SnagRWSDL
*
* Version            Date            Author                    Description
* 1.0                19/12/17        Naresh Kaneriya (Accely)   Initial Draft
**********************************************************************************************/

@isTest
public class SnagRWSDLTest{
    
    static SnagRWSDL.SnagrwebHttpSoap11Endpoint   obj = new SnagRWSDL.SnagrwebHttpSoap11Endpoint();
    Static String resp ;


    @isTest static void TestMethod1(){
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,SnagRWSDL.drillDownResponse_element>();
        SnagRWSDL.drillDownResponse_element response1 = new SnagRWSDL.drillDownResponse_element();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.drillDown('DrawingID');
      System.assert(resp != null);  
     Test.stopTest();
  }
  
   @isTest static void TestMethod2(){
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,SnagRWSDL.defectsResponse_element>();
        SnagRWSDL.defectsResponse_element response1 = new SnagRWSDL.defectsResponse_element();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.defects('LocationID');
      System.assert(resp != null);  
     Test.stopTest();
  }
  
  @isTest static void TestMethod3(){
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,SnagRWSDL.LocationResponse_element>();
        SnagRWSDL.LocationResponse_element response1 = new SnagRWSDL.LocationResponse_element();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.Location('LocationIDs');
      System.assert(resp != null);  
     Test.stopTest();
  }
  
    @isTest static void TestMethod4(){
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,SnagRWSDL.InspectionsResponse_element>();
        SnagRWSDL.InspectionsResponse_element response1 = new SnagRWSDL.InspectionsResponse_element();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.Inspections('LocID');
      System.assert(resp != null);  
     Test.stopTest();
  }

  @isTest static void TestMethod5(){
      
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,SnagRWSDL.activityLogsResponse_element>();
        SnagRWSDL.activityLogsResponse_element response1 = new SnagRWSDL.activityLogsResponse_element();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.activityLogs('LocID');
      System.assert(resp != null);  
     Test.stopTest();
  }
}