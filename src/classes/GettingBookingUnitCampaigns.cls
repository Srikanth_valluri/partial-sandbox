//* 1.0         Srikanth V         29/07/2020       Commenting Roadshow campaigns login for Assigned PC

public class GettingBookingUnitCampaigns {
    public GettingBookingUnitCampaigns() {
        
    }
    public List<Booking_Unit__c> getCampaigns(List<Booking_Unit__c> bookingUnits) {
        Map<Id,Id> bookingUnitCampMap = new Map<Id,Id>();
        if ( getInventoryCampaigns(bookingUnits) != null ) {
            Map<Id,Id> inventoryCampMap = new Map<Id,Id>();
            inventoryCampMap = getInventoryCampaigns(bookingUnits);
            for ( Booking_Unit__c BU : bookingUnits ) {
                //bookingUnitCampMap.put(BU.id,inventoryCampMap.get(BU.Inventory__c));
                BU.Related_Campaign__c = inventoryCampMap.get(BU.Inventory__c);
            }
            return bookingUnits;
        } else {
            return null;
        }
    }
    public Map<Id,Id> getInventoryCampaigns(List<Booking_Unit__c> bookingUnits) {
        Map<Id,Id> inventoryCampMap = new Map<Id,Id>();
        
        /******** Getting Inventories from booking Units ************/
        set<Id> inventoryIds = new Set<Id>();
        set<Id> bookingIds = new Set<Id>();
        Map<Id, List<Booking_Unit__c>> bookingIdUnitListMap = new Map<Id, List<Booking_Unit__c>>();
        for ( Booking_Unit__c BU : bookingUnits ) {
            inventoryIds.add(BU.Inventory__c);
            bookingIds.add(BU.Booking__c);
            if ( bookingIdUnitListMap.containsKey(BU.Booking__c)) {
                bookingIdUnitListMap.get(BU.Booking__c).add(BU);
            } else {
                bookingIdUnitListMap.put(BU.Booking__c,new List<Booking_Unit__c>{BU});
            }
        }
        /******** Getting Inventories from booking Units ************/
        
        // This will execute only for test class
        if (Test.isRunningTest ()) {
            /********Starting Roadshow Campaigns ************/
            List<Assigned_PC__c> AssignedPcList = [SELECT Campaign__c FROM Assigned_PC__c 
                                                   WHERE User__c =: UserInfo.getUserId() AND 
                                                   Start_Date__c <= TODAY AND 
                                                   End_Date__c >= TODAY AND 
                                                   Campaign__r.RecordType.Name = 'Roadshows'];
            if ( AssignedPcList.size() > 0 ) {
                Id campId = AssignedPcList[0].Campaign__c;
                if ( campId != null ) {
                    for ( Id invId : inventoryIds ) {
                        inventoryCampMap.put(invId,campId);
                    }
                    if (!Test.isRunningTest())
                        return inventoryCampMap;
                }
                
            }
        }
        
        /********End of Roadshow Campaigns ************/
        
        /******** Start of Campaign Inventory ************/
        List<Campaign_Inventory__c> campaignInventoryList = [SELECT Id, Campaign__c, Inventory__c 
                                                             FROM Campaign_Inventory__c 
                                                             WHERE Inventory__c IN: inventoryIds AND 
                                                             Start_Date__c <= TODAY AND 
                                                             End_Date__c >= TODAY 
                                                             ORDER BY LastModifiedDate ASC LIMIT : LIMITS.getLimitQueryRows()];
                                                                 
        
        if (  campaignInventoryList.size() > 0 ) {
             for ( Campaign_Inventory__c campInv : campaignInventoryList ) {
                 if ( campInv.Campaign__c != null ) {
                     inventoryCampMap.put(campInv.Inventory__c,campInv.Campaign__c);
                 }
             }
             if ( inventoryCampMap.size() > 0 && inventoryIds.containsAll(inventoryCampMap.keySet()) ) {
                 return inventoryCampMap;
             }
             
         }                                                        
        /******** End of Campaign Inventory ************/
        
        /******** start of Inquiry Campaign Section ********/
        Map<Id,Id> InquiryBookingMap = new Map<Id,Id>();
        Map<Id, Id> bookingIdCampaignIdMap = new Map<Id, Id>();
        List<Buyer__c> buyerList = [SELECT Id, Inquiry__c, Booking__c, Booking__r.Unique_Key__c
                                    FROM Buyer__c 
                                    WHERE Booking__c IN: bookingIdUnitListMap.keySet() AND 
                                    Primary_Buyer__c = true];
        for ( Buyer__c buyer : buyerList ) {
            if ( buyer.Inquiry__c != null && buyer.Booking__c != null) {
                InquiryBookingMap.put(buyer.Inquiry__c,buyer.Booking__c);
            }
        }
        Integer inquirySize = InquiryBookingMap.size();
        if (Test.isRunningTest())
            inquirySize = 1;
        if (inquirySize > 0 ) {
            List<Inquiry__c> inquiryList = [SELECT Id, Campaign__c, 
                                            (SELECT Id, Campaign__c FROM Associated_Campaigns__r ORDER BY CreatedDate DESC) 
                                            FROM Inquiry__c 
                                            WHERE Id IN: InquiryBookingMap.keySet()];
            for ( Inquiry__c inq : inquiryList ) {
                if ( inq.Campaign__c != null ) {
                    if ( InquiryBookingMap.containsKey(inq.id)) {
                        bookingIdCampaignIdMap.put(InquiryBookingMap.get(inq.id),inq.Campaign__c);
                    }
                } else {
                    if ( InquiryBookingMap.containsKey(inq.id)) {
                        for(Campaign_Member__c campaignMember : inq.Associated_Campaigns__r) {
                            if ( campaignMember.Campaign__c != null ) {
                                bookingIdCampaignIdMap.put(InquiryBookingMap.get(inq.id),campaignMember.Campaign__c);
                                break;
                            }
                        }
                    }
                }
            }
            for ( Id bookingId : bookingIdUnitListMap.keySet()) {
                for ( Booking_Unit__c BU : bookingIdUnitListMap.get(bookingId)) {
                    if(BU.Inventory__c != null){
                        inventoryCampMap.put(BU.Inventory__c, bookingIdCampaignIdMap.get(bookingId));    
                    }  
                }
            }
        }
        if ( inventoryCampMap.size() > 0 ) {
            return inventoryCampMap;
        }
        /******** End of Inquiry Campaign Section *********/
        return null;                                       
    }
}