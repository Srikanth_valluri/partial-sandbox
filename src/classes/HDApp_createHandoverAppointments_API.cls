/* 

/**********************************************************************************************************************
Description: This service is used to create Appointments for booking an Unit Inspection.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By   | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   8-08-2020       |   Jyoti Aher   |   1. Initial implementation to return available Appointment Dates on Mobile App

1.1     |   18-08-2020      |   Jyoti Aher   |   2. Implementation to return Appointment slots based on selected Appointment Date

1.2     |   20-08-2020      |   Jyoti Aher   |   3. Added accountId and Process parameter in the request implementation. Also, added processName in the implementation.
***********************************************************************************************************************/

@RestResource(urlMapping='/createHandoverAppointments/*')
global class HDApp_createHandoverAppointments_API {
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };
    
    @HttpPost
    /**********************************************************************************************************************
    Description : Method to create Appointments for Booking Unit Inspection
    Parameter(s )  : NA
    Return Type : Wrapper (FinalReturnWrapper) as response which contains data and metadata
    **********************************************************************************************************************/
    global static FinalReturnWrapper createAppointmentForUnit(){
        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_data objData = new cls_data();
        cls_meta_data objMeta = new cls_meta_data();
        //List<appointment_slots_Wrapper> lstAppointmentSlots = new List<appointment_slots_Wrapper>();
        
        if(!r.params.containsKey('bookingUnitId')) {
            objMeta = ReturnMetaResponse('Missing parameter : bookingUnitId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;      
        }
        
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : bookingUnitId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        else if(r.params.containsKey('bookingUnitDate') && String.isBlank(r.params.get('bookingUnitDate'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : bookingUnitDate', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        else if(r.params.containsKey('bookingType') && String.isBlank(r.params.get('bookingType'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : bookingType', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        else if(r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : accountId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        else if(r.params.containsKey('process') && String.isBlank(r.params.get('process'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : process', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        else if(r.params.containsKey('appointmentSlotId') && String.isBlank(r.params.get('appointmentSlotId'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : appointmentSlotId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        else if(r.params.containsKey('inspectionBy') && String.isBlank(r.params.get('inspectionBy'))) {
            objMeta = ReturnMetaResponse('Please select Inspection By', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        string inspectionBy = r.params.get('inspectionBy');
        if( r.params.containsKey('accountId') && String.isNotBlank(r.params.get('accountId'))
        && r.params.containsKey('bookingUnitId') && String.isNotBlank(r.params.get('bookingUnitId')) 
        && r.params.containsKey('bookingUnitDate') && String.isNotBlank(r.params.get('bookingUnitDate')) 
        && r.params.containsKey('bookingType') && String.isNotBlank(r.params.get('bookingType'))
        && r.params.containsKey('process') && String.isNotBlank(r.params.get('process'))
        && r.params.containsKey('appointmentSlotId') && String.isNotBlank(r.params.get('appointmentSlotId'))
        && r.params.containsKey('inspectionBy') && String.isNotBlank(r.params.get('inspectionBy')) && inspectionBy == 'Owner'){
            //Main logic to create appointments with inspectionBy as Owner
            String strSelectedAccount = r.params.get('accountId');
            String bookingUnitId = String.valueOf(r.params.get('bookingUnitId'));
            String bookingType = r.params.get('bookingType');
            String selectedDate = String.valueOf(r.params.get('bookingUnitDate'));
            String processName = r.params.get('process');
            String appointmentId = r.params.get('appointmentSlotId');
            Date fromDate;
            Date toDate;
            System.debug('bookingUnitDate---->'+selectedDate);

            HDApp_createHandoverAppointmentsHandler.appointmentHandler_Wrapper handlerWrapper = new HDApp_createHandoverAppointmentsHandler.appointmentHandler_Wrapper();
            handlerWrapper = HDApp_createHandoverAppointmentsHandler.createAppointment(strSelectedAccount,bookingUnitId,selectedDate,processName,bookingType,appointmentId,inspectionBy);
            System.debug('handlerWrapper'+handlerWrapper);

            appointment_Wrapper appointmentWrapper = new appointment_Wrapper();
            appointmentWrapper.calling_list_id = handlerWrapper.calling_list_id;
            //As AutoNumber field of Calling List 'Name' is not returning value, so retrirving it from database again
            Calling_List__c callingListRec = getCallingList(handlerWrapper.calling_list_id);
            appointmentWrapper.booking_number = callingListRec.Name;
            appointmentWrapper.purpose = handlerWrapper.purpose;
            appointmentWrapper.appointment_date = handlerWrapper.appointmentDate;
            appointmentWrapper.slot = handlerWrapper.slot;
            appointmentWrapper.appointment_message = handlerWrapper.appointmentMessage;
            appointmentWrapper.booking_unit_id = handlerWrapper.bookingUnitId;
            appointmentWrapper.appointment_status = handlerWrapper.status;
            //appointmentWrapper.bookedBy = handlerWrapper.bookedBy;

            if(handlerWrapper.calling_list_id == null && handlerWrapper.bookingNumber == null 
                && handlerWrapper.purpose == null && handlerWrapper.appointmentDate == null 
                && handlerWrapper.slot == null && String.isNotBlank(handlerWrapper.appointmentMessage) 
                && handlerWrapper.bookingUnitId == null){
                objMeta.message = handlerWrapper.appointmentMessage;
                objMeta.status_code = 2;
                objMeta.title = mapStatusCode.get(2);
                objMeta.developer_message = null;
                
                returnResponse.meta_data = objMeta;
                return returnResponse;
            }
            
            objMeta.message = 'Success';
            objMeta.status_code = 1;
            objMeta.title = mapStatusCode.get(1);
            objMeta.developer_message = null;

            objData.unit_inpsection_booking_details = appointmentWrapper;
            
            returnResponse.meta_data = objMeta;
            returnResponse.data = objData; 
            return returnResponse;
        }
        else if(r.params.containsKey('accountId') && String.isNotBlank(r.params.get('accountId'))
            && r.params.containsKey('bookingUnitId') && String.isNotBlank(r.params.get('bookingUnitId')) 
            && r.params.containsKey('bookingUnitDate') && String.isNotBlank(r.params.get('bookingUnitDate')) 
            && r.params.containsKey('bookingType') && String.isNotBlank(r.params.get('bookingType'))
            && r.params.containsKey('process') && String.isNotBlank(r.params.get('process'))
            && r.params.containsKey('appointmentSlotId') && String.isNotBlank(r.params.get('appointmentSlotId'))
            && r.params.containsKey('inspectionBy') && String.isNotBlank(r.params.get('inspectionBy')) && inspectionBy == 'Third Party'
            && r.params.containsKey('documentName') /*&& String.isNotBlank(r.params.get('documentName'))*/ 
            && r.params.containsKey('fileName') /*&& String.isNotBlank(r.params.get('fileName'))*/){
                
                //if document is missing in request
                if(r.params.containsKey('documentName') && String.isBlank(r.params.get('documentName'))) {
                    objMeta = ReturnMetaResponse('Missing parameter value : documentName', 3);
                    returnResponse.meta_data = objMeta;
                    return returnResponse;

                }
                //if document is missing in request
                if(r.params.containsKey('fileName') && String.isBlank(r.params.get('fileName'))) {
                objMeta = ReturnMetaResponse('Missing parameter value : fileName', 3);
                returnResponse.meta_data = objMeta;
                return returnResponse;
                }
                
                //Logic to create the appointment 
                String strSelectedAccount = r.params.get('accountId');
                String bookingUnitId = String.valueOf(r.params.get('bookingUnitId'));
                String bookingType = r.params.get('bookingType');
                String selectedDate = String.valueOf(r.params.get('bookingUnitDate'));
                String processName = r.params.get('process');
                String appointmentId = r.params.get('appointmentSlotId');
                
                HDApp_createHandoverAppointmentsHandler.appointmentHandler_Wrapper handlerWrapper = new HDApp_createHandoverAppointmentsHandler.appointmentHandler_Wrapper();
                handlerWrapper = HDApp_createHandoverAppointmentsHandler.createAppointment(strSelectedAccount,bookingUnitId,selectedDate,processName,bookingType,appointmentId,inspectionBy);
                System.debug('handlerWrapper'+handlerWrapper);

                appointment_Wrapper appointmentWrapper = new appointment_Wrapper();
                appointmentWrapper.calling_list_id = handlerWrapper.calling_list_id;
                //As AutoNumber field of Calling List 'Name' is not returning value, so retrieving it from database again
                Calling_List__c callingListRecord = getCallingList(handlerWrapper.calling_list_id);
                appointmentWrapper.booking_number = callingListRecord.Name;
                appointmentWrapper.purpose = handlerWrapper.purpose;
                appointmentWrapper.appointment_date = handlerWrapper.appointmentDate;
                appointmentWrapper.slot = handlerWrapper.slot;
                appointmentWrapper.appointment_message = handlerWrapper.appointmentMessage;
                appointmentWrapper.booking_unit_id = handlerWrapper.bookingUnitId;
                appointmentWrapper.appointment_status = handlerWrapper.status;
                //appointmentWrapper.bookedBy = handlerWrapper.bookedBy;
                
                if(handlerWrapper.calling_list_id == null && handlerWrapper.bookingNumber == null 
                && handlerWrapper.purpose == null && handlerWrapper.appointmentDate == null 
                && handlerWrapper.slot == null && String.isNotBlank(handlerWrapper.appointmentMessage) 
                && handlerWrapper.bookingUnitId == null){
                    objMeta.message = handlerWrapper.appointmentMessage;
                    objMeta.status_code = 2;
                    objMeta.title = mapStatusCode.get(2);
                    objMeta.developer_message = null;
                    
                    returnResponse.meta_data = objMeta;
                    return returnResponse;
                }

                /*                
                objMeta.message = 'Success';
                objMeta.status_code = 1;
                objMeta.title = mapStatusCode.get(1);
                objMeta.developer_message = null;

                objData.unit_inpsection_booking_details = appointmentWrapper;
                
                returnResponse.meta_data = objMeta;
                returnResponse.data = objData; 
                */
                
                //Logic to upload the documents if the Unit is inspected by third party
                String callingListObj = appointmentWrapper.calling_list_id;
                if(callingListObj!=null){

                    Calling_List__c callingListRec = getCallingList(callingListObj);
                    System.debug('callingListRec'+callingListRec);
                    Blob body = r.requestBody;
                    //System.debug('body:: ' + body);
                    String requestString = EncodingUtil.Base64Encode(body);
                    System.debug('requestString: '+requestString);

                    String strDocumentName = r.params.get('documentName');
                    String fileName = r.params.get('fileName');
                    /*
                    String attachmentId = r.params.containsKey('attachmentId') ? r.params.get('attachmentId') : '';
                    System.debug('attachmentId:: ' + attachmentId);
                    */

                    UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
                    list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
                    if(string.isNotBlank( requestString ) ) {
                        lstWrapper.add( FM_Utility.makeWrapperObject(  requestString,
                                                                    strDocumentName ,
                                                                    r.params.get('fileName') ,
                                                                    callingListRec.Name, '1' ) );
                    }
                    System.debug('== lstWrapper =='+lstWrapper);
                    String wrapperStr = JSON.serialize(lstWrapper);
                    calloutToDocUpload(wrapperStr, callingListObj, strDocumentName,fileName);
                    
                    objMeta.message = 'Success';
                    objMeta.status_code = 1;
                    objMeta.title = mapStatusCode.get(1);
                    objMeta.developer_message = null;

                    objData.unit_inpsection_booking_details = appointmentWrapper;

                    returnResponse.meta_data = objMeta;
                    returnResponse.data = objData;
                    System.debug('returnResponse:: ' + returnResponse); 
                    return returnResponse;
                }
                else {
                    objMeta.message = 'No Calling List record found for given callingListId';
                    objMeta.status_code = 2;
                    objMeta.title = mapStatusCode.get(2);
                    objMeta.developer_message = null;

                    returnResponse.meta_data = objMeta;
                    System.debug('returnResponse:: ' + returnResponse);
                    return returnResponse;
                }
                
            }
        return returnResponse;  
    }

    public static void insertCustomAttachment(Calling_List__c callingListRec, String docName , UploadMultipleDocController.data objResponse,String fileName) {
        
        //removed AttachmentId parameter from this function
        list<SR_Attachments__c> lstCustomAttachments = new list<SR_Attachments__c>();

        System.debug('fileName:: ' + fileName);

        String strFileExtension;
        if( String.isNotBlank( fileName ) && fileName.lastIndexOf('.') != -1 ) {
            strFileExtension = fileName.substring( fileName.lastIndexOf('.')+1 , fileName.length() );
            strFileExtension = strFileExtension.substringBefore('&');
        }

        System.debug('strFileExtension:: ' + strFileExtension);
        
        for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            SR_Attachments__c objCustAttach = new SR_Attachments__c();

            /*
            if(String.isNotBlank(attachmentId)) {
                objCustAttach.id = attachmentId;
            }*/
            //objCustAttach.Account__c = strAccountId ;
            objCustAttach.Name = docName;
            objCustAttach.Attachment_URL__c = objFile.url;
            //objCustAttach.Booking_Unit__c = bookingUnitId;
            objCustAttach.Calling_List__c = callingListRec.Id;
            objCustAttach.Type__c = strFileExtension;
            lstCustomAttachments.add(objCustAttach);
        }

        System.debug('lstCustomAttachments:: ' + lstCustomAttachments);
        if( !lstCustomAttachments.isEmpty() ) {
            upsert lstCustomAttachments;
                /*
                objAttachWrap.fm_case_id = fmCase.Id;
                objAttachWrap.fm_case_number =fmCase.Name;
                objAttachWrap.id = lstCustomAttachments[0].id; 
                objAttachWrap.attachment_url = lstCustomAttachments[0].Attachment_URL__c;
                objAttachWrap.file_extension = lstCustomAttachments[0].Type__c;
                */
        }
    }

    @future(callout=true)
    public static void calloutToDocUpload(String wrapperStr, String callingListId, String strDocumentName, String fileName){
        if(wrapperStr!=null && wrapperStr.length()>0) {
            
            System.debug('wrapperStr'+wrapperStr);

            list<UploadMultipleDocController.MultipleDocRequest> lstWrapperFromStr = (list<UploadMultipleDocController.MultipleDocRequest>)JSON.deserialize(wrapperStr , list<UploadMultipleDocController.MultipleDocRequest>.class);
            System.debug('lstWrapperFromStr'+ lstWrapperFromStr);

            UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
            if(!Test.isRunningTest()) {
                objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo(lstWrapperFromStr);
            } else {
                objResponse.data = new List<UploadMultipleDocController.MultipleDocResponse>();
                UploadMultipleDocController.MultipleDocResponse objFile = new UploadMultipleDocController.MultipleDocResponse();
                objFile.url = 'test_URL';// TODO
            }
            system.debug('== objResponse document upload =='+objResponse);
            system.debug('== objResponse.data =='+objResponse.data);
            
            //retrive CallingList record to insert SR Attachment
            Calling_List__c callingListRec = getCallingList(callingListId);
            System.debug('callingListRec'+callingListRec);

            FinalReturnWrapper returnResponse = new FinalReturnWrapper();
            cls_data objData = new cls_data();
            cls_meta_data objMeta = new cls_meta_data();

            if( objResponse != NULL && objResponse.data != NULL ) {
                insertCustomAttachment( callingListRec, strDocumentName, objResponse, fileName);
            }
        }
    }

    public static Calling_List__c getCallingList(String callingListObj){
        
        List<Calling_List__c> callingList = [SELECT Id
                                                , Name 
                                                , Appointment_Status__c
                                                FROM 
                                                Calling_List__c 
                                                WHERE 
                                                Id=:callingListObj];
        System.debug('callingList'+callingList);
        Calling_List__c callingListInstance = new Calling_List__c();
        for(Calling_List__c iterator: callingList){
            callingListInstance.Id = iterator.Id;
            callingListInstance = iterator;   
        }
        return callingListInstance;
    }
    /**********************************************************************************************************************
    Description : Method to process and return the response for the API request
    Parameter(s )  : Message, statusCode
    Return Type : Wrapper (FinalReturnWrapper) as responnse which contains data and metadata
    **********************************************************************************************************************/
    public static cls_meta_data ReturnMetaResponse(String message, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data(); 
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = null;
        
        return retMeta;
            
    }
    /**********************************************************************************************************************
    Description : Wrapper Class to combine data and metadata for API response 
    Parameter(s )  : NA
    Return Type : NA
    **********************************************************************************************************************/
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }
    /**********************************************************************************************************************
    Description : Wrapper Class to combine and return metadata results for API response 
    Parameter(s )  : NA
    Return Type : NA
    **********************************************************************************************************************/
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }
    /**********************************************************************************************************************
    Description : Wrapper Class to combine and return data results for API response
    Parameter(s )  : NA
    Return Type : NA
    **********************************************************************************************************************/
    public class appointment_Wrapper {
        public String calling_list_id;
        public String booking_number;
        public String purpose;
        public String appointment_date;
        public String slot;
        public String appointment_message;
        public String booking_unit_id;
        public String appointment_status;
        //public String documentURL;
        //public string bookedBy;
    }
    /**********************************************************************************************************************
    Description : Class contains appointment slots data and return it to FinalReturnWrapper wrapper class for API response
    Parameter(s )  : NA
    Return Type : NA
    **********************************************************************************************************************/
    public class cls_data {
        public appointment_Wrapper unit_inpsection_booking_details;
    }
    
}