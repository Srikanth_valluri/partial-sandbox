@isTest
private class UpdateDpAgingCLBatchTest {

    static testMethod void test_UpdateDpAgingCLBatch () {
        Id collectionQueueId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName( 'Collection_Queue','Queue' );
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        UpdateDpAgingCLBatch batchCls = new UpdateDpAgingCLBatch();
        
        Id RecordTypeIdCollection = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Collections_Calling_List'
            AND IsActive = TRUE LIMIT 1
        ].Id;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        List<Calling_List__c> lstCallingLists1 = createCallingList( RecordTypeIdCollection , 1 ,objAcc.Id, 'Collection Calling' );
        lstCallingLists1[0].Calling_List_Status__c = 'New';
        lstCallingLists1[0].Customer_Flag__c = true;
        insert lstCallingLists1;
        
        List<Calling_List__c> lstCallingLists = createCallingList( RecordTypeIdCollection , 1 ,objAcc.Id, 'DP Calling' );
        lstCallingLists[0].OwnerId = collectionQueueId;
        lstCallingLists[0].Calling_List_Status__c = 'New';
        insert lstCallingLists;
        
        Test.startTest();
            Database.executeBatch(batchCls);
        Test.stopTest();
    }

    /*
     @ Description : To create calling list reocord
     @ Return      : list of calling list to be created
    */
    public static List<Calling_List__c> createCallingList( Id RecordTypeIdCollection, Integer counter , Id acctId, String strType ) {

        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        for( Integer i=0; i<counter; i++ ) {
            lstCallingLists.add(new Calling_List__c(IsHideFromUI__c = false, Account__c = acctId  , Customer_Flag__c  = false
            ,RecordTypeId = RecordTypeIdCollection  ,Calling_List_Type__c = strType) );
        }
        return lstCallingLists;
    }

}