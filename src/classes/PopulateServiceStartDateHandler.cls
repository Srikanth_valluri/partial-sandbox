public with sharing class PopulateServiceStartDateHandler {
    public String callingListId {get;set;}
    public List<Calling_List__c>callingList {get;set;}
    //public static Id walkInRecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Walk In Calling List').RecordTypeId;
        
    public List<Calling_List__c>updateCallingList {get;set;}
    public PopulateServiceStartDateHandler(ApexPages.StandardController controller) {
    //  updateServiceDate();
    }
    public Pagereference updateServiceDate(){
     callingList = new List<Calling_List__c>();
     updateCallingList = new List<Calling_List__c>();
     callingListId = ApexPages.currentPage().getParameters().get('id');
     callingList = [SELECT Id,
                        RecordTypeId,
                        Case__c,Calling_List_Status__c ,
                        Name,
                        Service_start__c,CreatedDate,
                        Account__r.Name,
                        Account__c
                   FROM Calling_List__c
                  WHERE Id = :callingListId
                   // AND RecordTypeId = :walkInRecordTypeId
                     ];
       for(Calling_List__c callObj : callingList){
            callObj.Service_start__c = System.Now();
            callObj.Calling_List_Status__c = 'In Progress';
            updateCallingList.add(callObj);
       }
       update updateCallingList;
       PageReference ReturnPage = new PageReference('/' +callingListId); 
       ReturnPage.setRedirect(true); 
       return ReturnPage;
      // return new Pagereference(String.format(Label.Lightning_Calling_Detail_Page_Url, new List<String>{callingListId}));             
    }
}