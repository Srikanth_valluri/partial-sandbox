/*
Service Name : getAmenityBookingSlots

This service is used for getting the available slots for booking an Amenity.
Request URL sample : https://partial-servicecloudtrial-155c0807bf-1580afc5db1.cs80.force.com/Customer/services/apexrest/getAmenityBookingSlots?bookingDate=2020-06-15&resourceId=a4b250000013QV0

bookingDate format is : yyyy-mm-dd 
*/

@RestResource(urlMapping='/getAmenityBookingSlots/*')
global class GetSlotsforAmenityBooking_API {

    public static List<FM_Case__c> already_booked_amenity;

    public static final String NO_BOOKINGS = 'Advance booking is closed for the chosen date for given amenityId';
    public static final String NO_RESOURCE = 'No Amenity found for the given amenityId';
    public static final String AMENITY_ALREADY_BOOKED = 'You have an active booking available on the selected date for the chosen amenity';
    public static final String SUCCESS = 'Successful';
    public static String responseMessage;

    public static Integer statusCode;

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    @HttpPost
    global static FinalReturnWrapper getAmenitySlots() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_data objData = new cls_data();
        cls_meta_data objMeta = new cls_meta_data();

        SlotsCompleteWrapper finalSlotResponse = new SlotsCompleteWrapper();
        List<SlotWrapper> lstSlots = new List<SlotWrapper>();
        cls_already_booked_slot objBookedAmenity = new cls_already_booked_slot();

        if(!r.params.containsKey('bookingDate')) {

            objMeta.message = 'Missing parameter : bookingDate';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;

        }
        else if(!r.params.containsKey('amenityId')) {
            objMeta.message = 'Missing parameter : amenityId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(!r.params.containsKey('bookingUnitId')) {
            objMeta.message = 'Missing parameter : bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) {
            objMeta.message = 'Missing parameter value: bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;

        }
        else if(r.params.containsKey('bookingDate') && String.isBlank(r.params.get('bookingDate'))) {
            objMeta.message = 'Missing parameter value: bookingDate';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('amenityId') && String.isBlank(r.params.get('amenityId'))) {
            objMeta.message = 'Missing parameter value: amenityId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('isModify') && r.params.get('isModify').equalsIgnoreCase('true') && !r.params.containsKey('fmCaseId')) {
            objMeta.message = 'Missing parameter: fmCaseId in modify call';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;   
        }
        else if(r.params.containsKey('isModify') && r.params.get('isModify').equalsIgnoreCase('true') && r.params.containsKey('fmCaseId') && String.isBlank(r.params.get('fmCaseId'))) {

            objMeta.message = 'Missing parameter value: fmCaseId in modify call';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        if(String.isNotBlank(r.params.get('bookingUnitId')) && String.isNotBlank(r.params.get('amenityId'))) {

            list<Booking_Unit__c> BULst = [SELECT id FROM Booking_Unit__c WHERE id =: r.params.get('bookingUnitId')];
            List<Resource__c> res = [SELECT id FROM Resource__c WHERE id=: r.params.get('amenityId') ];

            if(BULst.isEmpty() || BULst == null) {
                objMeta.message = 'Invalid bookingUnitId passed';
                objMeta.status_code = 3;
                objMeta.title = mapStatusCode.get(3);
                objMeta.developer_message = null;

                returnResponse.meta_data = objMeta;
                return returnResponse;

            }
            else if(res.isEmpty() || res == null) {
                objMeta.message = 'Invalid amenityId passed';
                objMeta.status_code = 3;
                objMeta.title = mapStatusCode.get(3);
                objMeta.developer_message = null;

                returnResponse.meta_data = objMeta;
                return returnResponse;
            }
        }

        /*Below is prev existing code*/ /*Main logic starts here*/
        if( String.isNotBlank(r.params.get('bookingDate')) && String.isNotBlank(r.params.get('amenityId')) && String.isNotBlank(r.params.get('bookingUnitId')) ) {

            Date dateOfBooking = date.valueOf(r.params.get('bookingDate'));
            System.debug('dateOfBooking: ' + dateOfBooking);
            System.debug('amenityId: ' + r.params.get('amenityId'));

            //Check for non-operational days & dates
            List<Resource__c> resLst = GetAmenitiesByBookingUnit_API.getAvailableResources('','', true, r.params.get('amenityId'));
            if(resLst.size() > 0) {
                List<String> non_operational_days = String.isNotBlank(resLst[0].Non_Operational_Days__c) ? resLst[0].Non_Operational_Days__c.split(';') : new List<String>();
                System.debug('non_operational_days: ' + non_operational_days);

                List<Date> non_operational_dates = new List<Date>();
                if(resLst[0].Non_operational_day__r.size() > 0) {
                    for(Non_operational_days__c objOp : resLst[0].Non_operational_day__r) {
                        if(objOp != null) {
                            non_operational_dates.add(objOp.Non_operational_date__c);    
                        }
                    }
                }

                Datetime dt = DateTime.newInstance(dateOfBooking, Time.newInstance(0, 0, 0, 0));
                String dayOfWeek=dt.format('EEEE');
                System.debug('Day : ' + dayOfWeek);

                if(non_operational_days.contains(dayOfWeek) || non_operational_dates.contains(dateOfBooking)) {
                    objMeta.message = 'Amenity Booking is not available for chosen date';
                    objMeta.status_code = 2;
                    objMeta.title = mapStatusCode.get(2);
                    objMeta.developer_message = null;

                    returnResponse.meta_data = objMeta;
                    return returnResponse;
                }

            }
            //Check for non-operational days & dates END

            //Customer cannot book for amenity if already booked any slot for same amenity & same day
            already_booked_amenity = new List<FM_Case__c>();
            String fmCaseId;
            if(r.params.containsKey('fmCaseId') && String.isNotBlank(r.params.get('fmCaseId'))) {
                fmCaseId = r.params.get('fmCaseId');
            }
            System.debug('fmCaseId:: ' + fmCaseId);

            if( CheckCustomerEligibiltyForBooking(dateOfBooking, r.params.get('bookingUnitId') , r.params.get('amenityId'), fmCaseId) || (r.params.containsKey('isModify') && r.params.get('isModify').equalsIgnoreCase('true')) ) {

                lstSlots = fetchAvailableSlots(dateOfBooking, r.params.get('amenityId'));
                if(lstSlots.size() > 0) {
                    responseMessage = 'successful';
                    statusCode = 1;
                }
                else {
                    // no slots found
                    responseMessage = 'No slots available';
                    statusCode = 1;
                }

                //new object for already booked slot
                if(already_booked_amenity.size() > 0) {
                    List<Resource_Slot__c> lstBookedSlot = [SELECT id
                                                                 , Start_Time_p__c
                                                                 , End_Time_p__c
                                                            FROM Resource_Slot__c
                                                            WHERE id=:already_booked_amenity[0].Resource_SlotID__c];
                    
                    System.debug('lstBookedSlot:: ' + lstBookedSlot);
                    if(lstBookedSlot.size() > 0) {
                        //cls_already_booked_slot objBookedAmenity = new cls_already_booked_slot();
                        objBookedAmenity.start_time = lstBookedSlot[0].Start_Time_p__c;
                        objBookedAmenity.end_time = lstBookedSlot[0].End_Time_p__c;
                        objBookedAmenity.id = lstBookedSlot[0].id;
                    }
                    
                }
               

                
            }
            else {
                //responseMessage = 'Already an Amenity is booked for given date';
                responseMessage = AMENITY_ALREADY_BOOKED;
                statusCode = 1;
            }

            

            System.debug('responseMessage: ' + responseMessage);
            System.debug('lstSlots: ' + lstSlots);
        }

        //
        objMeta.title = mapStatusCode.get(statusCode);
        objMeta.status_code = statusCode;
        objMeta.message = responseMessage;
        //objMeta.is_success = statusCode == 200 ? true : false;
        objMeta.developer_message = null; 

        objData.slots = lstSlots;
        objData.already_booked_slot_details = objBookedAmenity;

        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;

        System.debug('returnResponse: ' + returnResponse);

        return returnResponse;

    }

    public static boolean CheckCustomerEligibiltyForBooking(Date bookingDate, String bookingUnitId, String resourceId, String fmCaseId) {
        boolean isEligible;

        System.debug('bookingDate: ' + bookingDate);
        System.debug('bookingUnitId: ' + bookingUnitId);
        System.debug('resourceId: ' + resourceId);

        String queryFetchBooking =  'SELECT id, Resource__c, Booking_Date__c, Account__c, Tenant__c, Booking_Unit__c, Resource_SlotID__c '  + 
        'FROM FM_Case__c  ';
        if(String.isNotBlank(fmCaseId)) {
            queryFetchBooking += 'WHERE id =: fmCaseId ';
        }
        else {
            queryFetchBooking += 'WHERE RecordType.Name = \'Amenity Booking\'  '  + 
            'AND Origin__c = \'Portal\' '  + //Need to change as per APP 
            'AND Amenity_Booking_Status__c NOT IN (\'Cancelled\', \'Rejected\') ' +   //added to include amenity booking is cancelled
            'AND (Resource__c =: resourceId '  + 
            'AND Booking_Unit__c =: bookingUnitId '  + 
            'AND Booking_Date__c =: bookingDate)' ;
        }
         
        System.debug('queryFetchBooking:' + queryFetchBooking);

        List<FM_Case__c> lstAmenityFM  = new List<FM_Case__c>();
        lstAmenityFM = Database.query(queryFetchBooking);
        //List<FM_Case__c> lstAmenityFM = [SELECT id
        //                                      , Resource__c
        //                                      , Booking_Date__c
        //                                      , Account__c
        //                                      , Tenant__c
        //                                      , Booking_Unit__c
        //                                      , Resource_SlotID__c
        //                                 FROM FM_Case__c
        //                                 WHERE RecordType.Name = 'Amenity Booking'
        //                                 AND Origin__c = 'Portal'   //Need to confirm
        //                                 AND (Resource__c =: resourceId
        //                                      AND Booking_Unit__c =: bookingUnitId 
        //                                      AND Booking_Date__c =: bookingDate)
        //                                 ORDER BY CreatedDate DESC];

        System.debug('lstAmenityFM:: ' + lstAmenityFM);

        if(lstAmenityFM.size() > 0) {
            //already_booked_amenity = new List<FM_Case__c>();
            already_booked_amenity.add(lstAmenityFM[0]);
            System.debug('already_booked_amenity:: ' + already_booked_amenity);

            isEligible = false;
        }else {
            isEligible = true;
        }
        System.debug('isEligible:: ' + isEligible);

        //if(lstAmenityFM.size() > 0) {

        //    if(lstAmenityFM[0].Booking_Date__c == bookingDate) {
        //        isEligible = false;
        //    }
        //    else {
        //        isEligible = true;
        //    }
        //}
        //else {
        //    isEligible = true;
        //}

        //System.debug('lstAmenityFM:: ' + lstAmenityFM);

        return isEligible;        

    }

    public static list<SlotWrapper> fetchAvailableSlots(Date bookingDate, String resourceId) {

        List<SlotWrapper> lstResourceSlots = new List<SlotWrapper>();

        List<SlotWrapper> lstResourceSlotOptions2 = new List<SlotWrapper>();

        String devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                            get('Amenity Booking').getRecordTypeId();

        Integer daysDifference = Date.today().daysBetween(bookingDate);
        System.debug('daysDifference: ' + daysDifference);
        //Get previously booked slots
        List<FM_Case__c> lstBookedAmenityCases = new List<FM_Case__c>();

        DateTime now = DateTime.now();
        Integer hours = now.hour(), minutes = now.minute();
        System.debug('==hours==' + hours);
        System.debug('==minutes==' + minutes);
        String currentTime = String.valueOf(hours) + ':' + String.valueOf(minutes);
        System.debug('bookingDate:: ' + bookingDate);
        System.debug('resourceId:: ' + resourceId);

        lstBookedAmenityCases = [SELECT ID,
                                        Booking_End_Time_p__c,
                                        Booking_Start_Time_p__c,
                                        Resource_SlotID__c
                                FROM FM_Case__c
                                WHERE Booking_Date__c =: bookingDate
                                AND Booking_Date__c >= Today
                                AND Resource_Booking_Type__c = 'FM Amenity'
                                AND RecordTypeId =: devRecordTypeId
                                AND Resource__r.Resource_Type__c = 'FM Amenity'
                                AND Amenity_Booking_Status__c != 'Cancelled'
                                AND Amenity_Booking_Status__c != 'Rejected'
                                AND Resource__c =: resourceId];//v4.0
        
        System.debug('==lstBookedAmenityCases==' + lstBookedAmenityCases);
        List<String> slotStartTime = new List<String>();
        List<String> slotEndTime = new List<String>();
        Set<String> fmCaseResSlotIdSet = new Set<String>(); //CDBS

        for(FM_Case__c obj : lstBookedAmenityCases) {
            slotStartTime.add(obj.Booking_Start_Time_p__c);
            slotEndTime.add(obj.Booking_End_Time_p__c);

            //The following if & else is for allowing multiple booking for same timing slots
            if(String.isNotBlank(obj.Resource_SlotID__c) && obj.Resource_SlotID__c.contains('-')) {
                list<String> lstStr = obj.Resource_SlotID__c.split('-');
                System.debug('lstStr:::: ' + lstStr);
                fmCaseResSlotIdSet.addAll(lstStr);
            }
            else {
                fmCaseResSlotIdSet.add(obj.Resource_SlotID__c);
            }

        }
        System.debug('==slotStartTime==' + slotStartTime);
        System.debug('==slotEndTime==' + slotEndTime);
        System.debug('==fmCaseResSlotIdSet==' + fmCaseResSlotIdSet);

        //Get slots to block from custom label V.3.0
        //Format is: Tuesday=18:00-19:30,19:30-21:00;Sunday=18:00-19:30,19:30-21:00
        List<String> lstSlots = new List<String>();

        List<Resource_Sharing__c> lstResourceSharing = new List<Resource_Sharing__c>();

        List<Resource__c> lstResource = [SELECT id
                                              , Name 
                                              , No_of_advance_booking_days__c
                                              , No_of_advance_closing_days__c
                                         FROM Resource__c WHERE id =: resourceId];
        System.debug('lstResource: ' + lstResource);
        
        if(lstResource.size() > 0 && lstResource[0].Name == 'Tennis Court') {
            lstResourceSharing = [SELECT Property__r.Name from Resource_Sharing__c
                                    WHERE Property__r.Name = 'WAVES TOWER'
                                    AND Resource__c =: resourceId];
        }
        System.debug('lstResourceSharing: ' + lstResourceSharing);

        if(lstResourceSharing.size() > 0) {
            List<String> lstSlotsToBlock = new List<String>();
            lstSlotsToBlock = label.FMAmenitySlotsToBlock.split(';');
            Map<String, set<String>> mapDay_BlockedSlots = new Map<String, set<String>>();
            //Get day of booking date
            Datetime dt = DateTime.newInstance(bookingDate, Time.newInstance(0, 0, 0, 0));
            String dayOfWeek=dt.format('EEEE');
            System.debug('Day : ' + dayOfWeek);
            for(String str : lstSlotsToBlock) {
                List<String> sObjectList = new List<String>();
                sObjectList = str.split('=');
                if(sObjectList.size()> 1 && dayOfWeek == sObjectList[0]) {
                    lstSlots.addAll((sObjectList[1]).split(','));
                }
            }
        }
        //v3.0 End
        System.debug('==lstSlots=='+lstSlots);

        System.debug('Resource slots ===='+[SELECT Id, Start_Time_p__c, End_Time_p__c
                             FROM Resource_Slot__c
                             WHERE Resource__c =: resourceId
                             AND Start_Date__c <=: bookingDate
                             AND End_Date__c >=: bookingDate
                             ORDER BY Start_Time_p__c ASC]);


        Set<String> setSlots = new Set<String>();
        if(lstResource.size() > 0) {

            if((lstResource[0].No_of_advance_booking_days__c != NULL
                        && lstResource[0].No_of_advance_booking_days__c >= daysDifference)
                        && (lstResource[0].No_of_advance_closing_days__c == NULL ||
                        (lstResource[0].No_of_advance_closing_days__c != NULL
                        && lstResource[0].No_of_advance_closing_days__c < = daysDifference))) {

                for(Resource_Slot__c obj : [SELECT Id
                                                 , Name
                                                 , Resource__c
                                                 , Start_Time_p__c
                                                 , End_Time_p__c
                                                 , Start_Date__c
                                                 , End_Date__c
                                            FROM Resource_Slot__c
                                            WHERE Resource__c =: resourceId
                                            AND Start_Date__c <=: bookingDate
                                            AND End_Date__c >=: bookingDate
                                            ORDER BY Start_Time_p__c ASC]) {

                    Time startT = getTimeInstanceFromText(obj.Start_Time_p__c);
                    Time tmCurrentTime = getTimeInstanceFromText(currentTime);
                    system.debug('tmCurrentTime'+tmCurrentTime);
                    Time endT = getTimeInstanceFromText(obj.End_Time_p__c);
                   
                    String startTimeTxt = getFormattedTime(startT);
                    String endTimeTxt = getFormattedTime(endT);
                    if(!slotStartTime.contains(startTimeTxt) && !slotEndTime.contains(endTimeTxt)) {
                        system.debug('in if');
                        system.debug('in if startT'+startT);
                        system.debug('in if booking date'+bookingDate);
                        if(bookingDate == Date.Today() && startT < tmCurrentTime){
                            system.debug('in inner if');
                            continue;
                        }
                        else{
                            system.debug('in else');
                            String slot = startTimeTxt + '-' + endTimeTxt;
                            if(! lstSlots.contains(slot)) {
                                system.debug('in else - if');
                                //lstResourceSlotOptions.add(new SelectOption(
                                //slot,slot));
                                String jsonStr = JSON.serialize(obj);
                                jsonStr = jsonStr.replaceAll('__c','');
                                jsonStr = jsonStr.replaceAll('_p','');
                                System.debug('jsonStr: ' + jsonStr);

                                SlotWrapper objWrap = (SlotWrapper)JSON.deserialize(jsonStr, SlotWrapper.class);
                                System.debug('objWrap:: ' + objWrap);

                                System.debug('setSlots: ' + setSlots);

                                if(!setSlots.contains(slot)) {
                                    setSlots.add(slot);
                                    
                                    lstResourceSlots.add(objWrap);
                                }
  
                                responseMessage = SUCCESS;
                            }
                        }
                    }
                    //This Else-If logic is for allowing Slot booking for multiple same timings slots - Shubham 31/05/2020
                    else if(label.AllowMultipleSameSlotAmenityBooking.equalsIgnoreCase('Y')) { //CDBS
                        List<Resource_Slot__c> lstResSlot = [SELECT id
                                                                  , Start_Time_p__c
                                                                  , End_Time_p__c
                                                                  , Resource__c 
                                                             FROM Resource_Slot__c
                                                             WHERE Resource__c =: resourceId
                                                             AND Start_Time_p__c =: startTimeTxt
                                                             AND End_Time_p__c =: endTimeTxt
                                                             AND (Start_Date__c <=: bookingDate
                                                             AND End_Date__c >=: bookingDate)
                                                             /*ORDER BY CreatedDate ASC*/];
                        System.debug('lstResSlot:: ' + lstResSlot);

                        Boolean bool = fmCaseResSlotIdSet.remove(null);
                        System.debug('bool: ' + bool);

                        System.debug('fmCaseResSlotIdSet: ' +fmCaseResSlotIdSet);

                        if(!fmCaseResSlotIdSet.isEmpty()) {
                            System.debug('inside if of label check');
                            for(Resource_Slot__c objResSlot : lstResSlot) {
                                System.debug('objResSlot: ' + objResSlot.Id);
                                if(!fmCaseResSlotIdSet.contains(objResSlot.Id)) {
                                    String slot = objResSlot.Start_Time_p__c + '-' + objResSlot.End_Time_p__c;
                                    System.debug('slot*::'+slot);

                                    if(lstResourceSlotOptions2.isEmpty()) {

                                        String jsonStr = JSON.serialize(obj);
                                        jsonStr = jsonStr.replaceAll('__c','');
                                        jsonStr = jsonStr.replaceAll('_p','');
                                        System.debug('jsonStr: ' + jsonStr);

                                        SlotWrapper objWrap = (SlotWrapper)JSON.deserialize(jsonStr, SlotWrapper.class);
                                        System.debug('objWrap:: ' + objWrap);

                                        lstResourceSlotOptions2.add(objWrap);
                                        break;
                                    }
                                    
                                }
                            }

                        } 
                        else {
                            System.debug('inside else if of label check');
                            
                            if(lstResSlot.size() > 1) {
                                Resource_Slot__c res = lstResSlot.remove(0);
                                System.debug('lstResSlot after removing index 0 :' + lstResSlot);

                                for(Resource_Slot__c objResSlot : lstResSlot) {
                                    System.debug('objResSlot: ' + objResSlot.Id);
                                    if(!fmCaseResSlotIdSet.contains(objResSlot.Id)) {
                                        String slot = objResSlot.Start_Time_p__c + '-' + objResSlot.End_Time_p__c;
                                        System.debug('slot*::'+slot);
                                       
                                        if(lstResourceSlotOptions2.isEmpty()) {

                                            String jsonStr = JSON.serialize(obj);
                                            jsonStr = jsonStr.replaceAll('__c','');
                                            jsonStr = jsonStr.replaceAll('_p','');
                                            System.debug('jsonStr: ' + jsonStr);

                                            SlotWrapper objWrap = (SlotWrapper)JSON.deserialize(jsonStr, SlotWrapper.class);
                                            System.debug('objWrap:: ' + objWrap);

                                            lstResourceSlotOptions2.add(objWrap);
                                            break;
                                        }
                                    }
                                }
                            }

                        }

                    }//END of CDBS

                }//END of FOR

                System.debug('lstResourceSlotOptions2======>>'+lstResourceSlotOptions2);
                if(lstResourceSlotOptions2.size() > 0) {    //CDBS
                    lstResourceSlots.addAll(lstResourceSlotOptions2);
                    lstResourceSlotOptions2.clear();
                }

            }//No_of_advance_booking_days__c If check ends
            else {

                responseMessage = NO_BOOKINGS;
                statusCode = 2;
            }

        }//lstResource.size IF ends
        else {
            responseMessage = NO_RESOURCE;
            statusCode = 2;
        }

        System.debug('lstResourceSlots:: ' + lstResourceSlots);
        return lstResourceSlots;

    }

    public static String getFormattedTime(Time tobj) {
        return String.valueOf(tobj.hour()).leftPad(2, '0') + ':'
            + String.valueOf(tobj.minute()).leftPad(2, '0');
    }

    public static Time getTimeInstanceFromText(String timeInText) {
        String [] timeTxt = timeInText.split(':');
        Time timeInstance = Time.newInstance(Integer.valueOf(timeTxt[0]),
                            Integer.valueOf(timeTxt[1]), 0, 0);
        return timeInstance;
    }

    //Wrapper class for Resource_Slots__c
    global class SlotsCompleteWrapper {
        public String status;
        public String message;
        public SlotWrapper[] slots;
    }

    global class SlotWrapper {
        public String id;   //a4a25000000FKNbAAO
        //public String Name;   //RS-00026
        //public String Resource;   //a4b250000013QV0AAM
        public String start_time;   //17:00
        public String end_time; //18:00
        //public String Start_Date; //2020-01-01
        //public String End_Date;   //2020-12-31
    }

    public class cls_already_booked_slot {
        public String id;
        public String start_time;
        public String end_time;
    }

    //Wrapper classes for returning reponse
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        public SlotWrapper[] slots;
        public cls_already_booked_slot already_booked_slot_details;
    }

    public class cls_meta_data {

        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
        //public String status;
        //public String message;   
        //public Boolean is_success {get; set;}
        //public String status_message {get; set;}
        //public Integer status_code {get; set;}
        //public String message_title {get; set;}
        //public String developer_message {get; set;}
    }

}