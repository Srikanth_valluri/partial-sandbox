@isTest
public class FMCaseSubmissionTest {
    private static testMethod void positiveOthersTest(){
        Account objA = TestDataFactoryFM.createAccount();
        insert objA;
        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objA);
        insert objSR;
        Booking__c objBooking = TestDataFactoryFM.createBooking(objA,objSR);
        insert objBooking;

        Location__c locObj=TestDataFactoryFM.createLocation();
        insert locObj;
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        Booking_Unit__c objBU = TestDataFactoryFM.createBookingUnit(objA,objBooking);
        objBU.Inventory__c=invObj.id;
        insert objBU;
        list<FM_Case__c> lstC = TestDataFactoryFM.createFMCase(2);
        lstC[0].Account__c = objA.Id;
        lstC[0].Booking_Unit__c = objBU.Id;
        lstC[0].Request_Type__c = 'Service Charge Enquiry';
        lstC[0].status__c='Submitted';
        lstC[0].submitted__c=true;

        lstC[1].Account__c = objA.Id;
        lstC[1].Booking_Unit__c = objBU.Id;
        lstC[1].Request_Type__c = 'Suggestion';
        lstC[1].status__c='Submitted';
        lstC[1].submitted__c=true;

        insert lstC;
        list<Id> lstId = new list<Id>();
        lstId.add(lstC[0].Id);
        lstId.add(lstC[1].Id);
        FMCaseSubmission.caseSubmitted(lstId);

        test.StartTest();

        test.StopTest();
    }
}