public class NEXMO_INB_RESPONSE_PARSER{
    public String message_uuid; //d226530c-42cb-49ed-b2cf-4b1a5ac1d233
    public cls_to WA_to;
    public cls_from WA_from;
    public String timestamp;    //2019-10-01T08:29:26.913Z
    public String direction;    //inbound
    public cls_message message;
    public class cls_to {
        public String WA_number;   //447418342149
        public String type; //whatsapp
    }
    public class cls_from {
        public String WA_number;   //918977365305
        public String type; //whatsapp
    }
    public class cls_message {
        public cls_content content;
    }
    Public class cls_content {
        public String type; //text
        public String text; //Hello
        public cls_image image;
        public cls_file file;
        public cls_audio audio;
        public cls_video video;
        public cls_location location;
    }
    
    public class cls_location {
        public String lat;
        public String WA_long;
    }
    Public class cls_audio {
        public String url;  //https://api.nexmo.com/v3/media/09061c8d-93ef-455a-bc2f-892137c0a17f
        public String caption;
    }
    Public class cls_video {
        public String url;  //https://api.nexmo.com/v3/media/09061c8d-93ef-455a-bc2f-892137c0a17f
        public String caption;
    }
    Public class cls_image {
        public String url;  //https://api.nexmo.com/v3/media/09061c8d-93ef-455a-bc2f-892137c0a17f
        public String caption;
    }
    Public class cls_file {
        public String url;  //https://api.nexmo.com/v3/media/09061c8d-93ef-455a-bc2f-892137c0a17f
        public String caption;
    }
    public static NEXMO_INB_RESPONSE_PARSER parse(String json){
        return (NEXMO_INB_RESPONSE_PARSER) System.JSON.deserialize(json, NEXMO_INB_RESPONSE_PARSER.class);
    }

    
}