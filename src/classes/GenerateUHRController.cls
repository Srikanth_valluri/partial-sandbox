public without sharing class GenerateUHRController{
    public String caseId;
    public Case objC;
    public GenerateUHRController(ApexPages.StandardController stdCon){
        caseId = stdCon.getId();
        objC = [Select Id
                     , Booking_Unit__c 
                     , Booking_Unit__r.Unit_Type__c                    
                     , Registration_ID__c
                from Case 
                where Id =: caseId];
    }
    
    public void callDrawloop(){
        executeBatch();
    }
    
    public pageReference returnToCase(){
        PageReference Pg = new PageReference('/'+caseId);
        return pg;
    }
    
    public void executeBatch(){
        if(String.isNotBlank(objC.Registration_ID__c)){
            GenerateDrawloopDocumentBatch objInstance;
            if (objC.Booking_Unit__r.Unit_Type__c != null 
                && objC.Booking_Unit__r.Unit_Type__c == 'PLOT'){
                objInstance = new GenerateDrawloopDocumentBatch(caseId
                     , System.Label.PHR_DDP_Id, System.Label.PHR_Template_Id);
             } else {
                objInstance = new GenerateDrawloopDocumentBatch(caseId
                     , System.Label.UHR_DDP_Id, System.Label.UHR_Template_Id);
             }
            Id batchId = Database.ExecuteBatch(objInstance);
            if(String.valueOf(batchId) != '000000000000000'){
                SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                 objCaseAttachment.Case__c = caseId ;
                 objCaseAttachment.Name = 'Signed UHR Document';
                 objCaseAttachment.Booking_Unit__c = objC.Booking_Unit__c;
                 insert objCaseAttachment;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Your request for UHR was successfully submitted. Please check the documents section for the document in a while.');
                ApexPages.addMessage(myMsg);
            }else{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Your request for UHR could not be completed. Please try again later.');
                ApexPages.addMessage(myMsg);
            }
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Regid for the new unit has not been generated yet. Please try again later.');
            ApexPages.addMessage(myMsg);
        }
    }
} // end of class