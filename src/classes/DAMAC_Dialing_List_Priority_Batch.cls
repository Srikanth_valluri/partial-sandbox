global class DAMAC_Dialing_List_Priority_Batch implements Database.Batchable<sObject>, Database.stateFul {

    Inquiry_Assignment_Rules__c assignmentRule;
    List <ID> completedRules = new List <ID> ();
    global DAMAC_Dialing_List_Priority_Batch(List <ID> ruleIds) {
        completedRules = ruleIds;
    }
    public static string getAllfields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ())
            fields += f+ ', ';
        return fields.removeEnd(', ');         
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String fields = getAllfields('Inquiry_Assignment_Rules__c');
        String executeOn = 'Dialing Priority';
        string queryCondition = 'SELECT '+fields+' FROM Inquiry_Assignment_Rules__c '
                            +' WHERE Active__c = True and Execute_on__c =:executeOn '
                            +' AND ID NOT IN: completedRules AND filter_logic__c != NULL '
                            +' order by Priority__c Asc LIMIT 1';
        system.debug('queryCondition =='+queryCondition);
        assignmentRule = Database.query (queryCondition);
        System.Debug (assignmentRule.ID);
        ID parentID = assignmentRule.ID;
        completedRules.add (assignmentRule.ID);
        List <Inquiry_Assignment_Rules__c> childRules = new List <Inquiry_Assignment_Rules__c> ();
        childRules = Database.query ('SELECT '+fields+' FROM Inquiry_Assignment_Rules__c '
                            +' WHERE Active__c = True AND Parent_Reassign_Rule__c =: parentID Order by Order__c Asc');
                            
        String userLogic = assignmentRule.filter_logic__c;
        
        String query = 'SELECT '+getAllfields('Dialing_list__c')+' FROM Dialing_list__c WHERE Dial_Date__c = TODAY AND ';
        
        String output;
        Map <String, Inquiry_Assignment_Rules__c> rulesMap = new Map <String, Inquiry_Assignment_Rules__c> ();
        for (Inquiry_Assignment_Rules__c con: childRules){
            rulesMap.put (String.valueOf(con.order__c), con);
        }
        System.Debug (rulesMap);
        
        if (String.isNotBlank (userLogic)) {            
            query += +' ( '+ formatQuery (userLogic, rulesMap)+' ) ';
            query += +' ( '+ formatQuery (userLogic, rulesMap)+' ) ';
            if(assignmentRule.query_Limit__c != null){
                query += ' LIMIT '+assignmentRule.query_Limit__c;
            }            
        } 
        if (Test.isRunningTest()) {
            query = ' SELECT OwnerId FROM Dialing_list__c';
        }
        System.debug(query+'>>>>>>>>>>>.');
        assignmentRule.Rule_Running_Status__c = 'Started';
        assignmentRule.Rule_Executed_Date__c = System.now ();
        Update assignmentRule;
        return Database.getQueryLocator(query);
        
    }
    public String formatQuery (String queryLogic, Map <String, Inquiry_Assignment_Rules__c> rules) {
        String query = '';
        System.debug (queryLogic);
        System.debug (rules);
            for (String key : queryLogic.split (' ')) {
                if (key.isNumeric ()) {
                    System.Debug (key );
                    query += +' '+rules.get (key).Query_Condition__c+' ';
                } else
                    query += key;
            }
        
        System.debug (query);
        return query;
    }
    global void execute (Database.BatchableContext BC, List <Dialing_list__c> scope) {
    
        assignmentRule.Rule_Running_Status__c = 'Processing';
        Update assignmentRule;
         List <Dialing_list__c> DialList = new List <Dialing_list__c> ();
        
        Decimal DialingPriority = assignmentRule.Dialing_Priority__c;
        id ruleId = assignmentRule.id;
        for (Dialing_list__c d: scope) {
            d.Priority__c = DialingPriority;  
            d.Priority_rule__c = ruleId;          
            DialList.add (d);
        }    
        if (DialList.size () > 0) {
            
            Update DialList;
        }
        
    }
    
    global void finish (Database.BatchableContext BC) {
        System.Debug (completedRules);
        assignmentRule.Rule_Running_Status__c = 'Completed';
        Update assignmentRule;

        if ([SELECT ID FROM Inquiry_Assignment_Rules__c 
                WHERE Active__c = TRUE 
                AND Execute_on__c = 'Dialing Priority' 
             AND filter_logic__c != NULL AND ID Not IN : completedRules].size () > 0){
                 if(!test.isRunningTest())
                    Database.executeBatch (new DAMAC_Dialing_List_Priority_Batch(completedRules), 1);
         }
        
    }
}