@isTest
public class AssignmentOtpSenderTest {
    @isTest
    static void testSendOtp(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        objAccount.Mobile__c = '12345698545';
        //objAccount.Mobile_Person_Business__c = 'test';
        insert objAccount;
        
        Case objCase = new Case();
        objCase.AccountId = ObjAccount.Id;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        insert objCase;
        
        Test.setMock(HttpCalloutMock.class, new MockHttpAgentOTPController());
        Test.startTest();
        AssignmentOtpSender.sendOtp(new List<Id>{objCase.Id});
        Test.stopTest();
        
    }
}