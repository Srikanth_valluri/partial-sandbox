public class AllRMUsersController {
    
    public string csvString{get; set;}
    public AllRMUsersController(){
        list<User> promoterUsers = [Select id, Name, Email, IPMS_Employee_ID__c, HR_Employee_ID__c,
                                    DOS_IPMS_ID__c, Manager.HR_Employee_ID__c, DOS_Name__c,
                                    HOS_IPMS_ID__c, Manager.Manager.HR_Employee_ID__c, HOS_Name__c,
                                    HOD_IPMS_ID__c, Manager.Manager.Manager.HR_Employee_ID__c, HOD_Name__c 
                                    from User 
                                    where isActive = true and Profile.Name = 'Property Consultant'
                                    order by name asc];
        
        csvString = 'SF Identifier,PC_NAME,PC_EMP_ID,PC_ID,DOS_EMP_ID,DOS_ID,DOS_NAME,HOS_EMP_ID,HOS_ID,HOS_NAME,HOD_EMP_ID,HOD_ID,HOD_NAME,Target\n';
        
        for(User uRec: promoterUsers){
            csvString += uRec.Id+','+uRec.Name+','+uRec.IPMS_Employee_ID__c+','+(uRec.HR_Employee_ID__c!= null ? uRec.HR_Employee_ID__c:'')+',';
            csvString += uRec.DOS_IPMS_ID__c+','+uRec.Manager.HR_Employee_ID__c+','+uRec.DOS_Name__c+',';
            csvString += uRec.HOS_IPMS_ID__c+','+uRec.Manager.Manager.HR_Employee_ID__c+','+uRec.HOS_Name__c+',';
            csvString += uRec.HOD_IPMS_ID__c+','+uRec.Manager.Manager.Manager.HR_Employee_ID__c+','+uRec.HOD_Name__c+','+''+'\n';
        }
        System.debug(csvString);
        csvString = csvString.trim();
        
        System.debug(csvString);
    }


}