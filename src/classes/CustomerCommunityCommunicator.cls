global class CustomerCommunityCommunicator{ 
    private final Account acct;
    public CustomerCommunityCommunicator (ApexPages.StandardController controller){
        this.acct = (Account)controller.getRecord();
    }
  
    public PageReference getInvites() {
        list<Messaging.SingleEmailMessage> allmails = new list<Messaging.SingleEmailMessage>();
        //Select email template for user language from custom settings.
        list<Customer_Community_Language_Settings__c> listCustom = [Select language__c
                                                                     , Community_Invitation_Email__c
                                                                from Customer_Community_Language_Settings__c
                                                                where Language__c = :UserInfo.getLanguage()
                                                                LIMIT 1
                                                                ];

        //select template html body from email template
        list<EmailTemplate> emailObj = [
                                        Select Id
                                            , HtmlValue
                                        FROM EmailTemplate
                                        WHERE Name = :listCustom[0].Community_Invitation_Email__c
                                        ];

        // fetchig attachment from db
        /*Attachment attchmntFileObj = new Attachment();
        attchmntFileObj = [Select id,
                                  Name,
                                  Body,
                                  Contenttype
                            From Attachment
                            Where id = '00P25000002H8OG'];

        // Create the email attachment
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName(attchmntFileObj.Name);
        efa.setBody(attchmntFileObj.Body);
*/
        //send email to selected account ids
        for(Account acc: [
                      Select Name
                          , Email__pc
                      from Account
                      where Id = :acct.id
                    ]){

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            list<string> toAddresses = new list<string>();
            //set selected accounts' email address as recepient
            toAddresses.add(acc.Email__pc);
            if(!toAddresses.isEmpty()){
                mail.setToAddresses(toAddresses);
                system.debug(toAddresses);
                
                String html = emailObj[0].HtmlValue;
                String filteredHtml = Html.remove('<![CDATA[');
                //set email body as template html body
                mail.setHtmlBody(emailObj[0].HtmlValue);
                
                OrgWideEmailAddress[] owea = [SELECT 
                                                    Id 
                                               FROM OrgWideEmailAddress 
                                               WHERE Address = 'no-replysf@damacgroup.com'];
                if ( owea.size() > 0 ) {
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                 mail.setHtmlBody(filteredHtml.remove(']]>'));
                mail.setSubject('Welcome to Damac Customer Community Portal');
                //set whatId to set Merge Fields' values
                mail.setWhatId(acc.id);
                mail.setTreatBodiesAsTemplate(true);
               /* mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});*/
                allmails.add(mail);
            }
            system.debug('>>> mail : '+mail);
        }
        // send email to selected accounts.
         try{
            Messaging.sendEmail(allmails);
            PageReference nextPage = new PageReference('/' + acct.Id);
            return nextPage;
        }
        catch(System.EmailException e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Sending Failed : '+e.getMessage()));
            return null;
        }
        
    }
}