@isTest
public class API_Community_Test {
    
    @testSetup static void setup () {
        Account adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        adminAccount.Agency_Type__c = 'Individual';
        insert adminAccount;
        
        Contact adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        adminContact.Owner__c = true;
        adminContact.Agent_Representative__c= true;
        adminContact.Portal_Administrator__c= true;
        adminContact.FirstName = 'test1';
        adminContact.LastName = 'test2';
        insert adminContact;
        
        User portalUser = InitialiseTestData.getPortalUser('agentEmail@damacgroup.com.fullcopy48', adminContact.Id, 'Admin');
        insert portalUser;
        /*
        system.runAs(portalUser){
        	API_CommunityChangePassword.changepassword(); 
        }*/
    }
    
    @isTest static void login() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/login';  
        req.addParameter('username', 'agentEmail@damacgroup.com.fullcopy48');
        req.addParameter ('password', 'test@123');
        req.addParameter ('domain', 'test.salesforce.com');
        req.httpMethod = 'Post';
        RestContext.request = req;
        RestContext.response = res;
        
        API_CommunityLogin.login(); 
    
    }
    
    @isTest static void resetPassword() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/resetpassword';  
        req.addParameter('username', 'agentEmail@damacgroup.com.fullcopy48');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        API_CommunityResetPassword.resetPassword(); 
    
    }
    @isTest static void resetPasswordException () {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/resetpassword';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        API_CommunityResetPassword.resetPassword(); 
    
    }
    
    @isTest static void changePassword() {        
       
        Id UserRoleId = [select Id from UserRole where name = 'Chairman' LIMIT 1].Id;	
        Id ProfileID = [Select Id From Profile Where Name='Customer Community - Admin'].id;
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		thisUser.UserRoleId = UserRoleId;         
		update thisUser;

        System.runAs(thisUser){
            Account a = new Account(Name='Test Account Name');
            insert a;
        
            Contact c = new Contact(LastName = 'Contact Last Name', AccountId = a.id);
            insert c;
        
            User user = new User();
            user.ProfileID = [Select Id From Profile Where Name='Customer Community - Admin'].id;
            user.EmailEncodingKey = 'ISO-8859-1';
            user.LanguageLocaleKey = 'en_US';
            user.TimeZoneSidKey = 'America/New_York';
            user.LocaleSidKey = 'en_US';
            user.FirstName = 'first';
            user.LastName = 'last';
            user.Username = 'agentEmail@damacgroup.com.fullcopy';
            user.CommunityNickname = 'testUser123';
            user.Alias = 't1';
            user.Email = 'no@email.com';
            user.IsActive = true;
            user.ContactId = c.Id;
        
            insert user;
            
            
            
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/changepassword';  
            req.addParameter('username', 'agentEmail@damacgroup.com.fullcopy');
            req.addParameter('newpassword', 'damac@testUAT');
            
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
        	API_CommunityChangePassword.changepassword();
            
            /*
            User user2 = new User();
            user2.ProfileID = [Select Id From Profile Where Name='system administrator'].id;
            user2.EmailEncodingKey = 'ISO-8859-1';
            user2.LanguageLocaleKey = 'en_US';
            user2.TimeZoneSidKey = 'America/New_York';
            user2.LocaleSidKey = 'en_US';
            user2.FirstName = 'first';
            user2.LastName = 'last';
            user2.Username = 'notAgentEmail@damacgroup.com.fullcopy';
            user2.CommunityNickname = 'testUser1234';
            user2.Alias = 't1';
            user2.Email = 'no@emaila.com';
            user2.IsActive = true;
           // user.ContactId = c.Id;
        
            insert user2;
            */
            
            
            //RestRequest req = new RestRequest();
           // RestResponse res = new RestResponse();
            req.requestURI = '/changepassword';  
            req.addParameter('username', 'agentEmail@damacgroup.com.fullcopy');
            req.addParameter('newpassword', 'damac@testingUAT');
            
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
        	API_CommunityChangePassword.changepassword();
            
    	}
        	
    
    }
    
    @isTest static void changePasswordException() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/changepassword';  
        
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        API_CommunityChangePassword.changepassword(); 
    
    }
}