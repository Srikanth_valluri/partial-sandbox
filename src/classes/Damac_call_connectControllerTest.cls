@isTest 
public class Damac_call_connectControllerTest
{
    static testMethod void testMethod1() {
        Inquiry__c inq = New Inquiry__c ();
        inq.First_Name__c = 'testInq';
        inq.Preferred_Language__c = 'English';
        inq.Last_Name__c ='inqLast';
        inq.Inquiry_Source__c = 'Prospecting';
        inq.Primary_Contacts__c = 'Mobile Phone';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.Mobile_Phone__c = '1236547890';
        inq.Mobile_Phone_Encrypt__c = '1223467886';
        inq.Meeting_Due_Date__c = Date.Today ();
        inq.Email__c = 'test@gmail.com';
        insert inq;
        Task t = new Task ();
            t.Subject='Donni';
            t.Status='New';
            t.Priority='Normal';
            t.CallType='Outbound';
        insert t;
            
        ApexPages.currentPage().getParameters().put('InqId',inq.id);
        ApexPages.currentPage().getParameters().put('tID',t.id);
        ApexPages.currentPage().getParameters().put('isNew','true');
        apexpages.currentpage().getparameters().put ('taskId', t.id);
        ApexPages.standardController std = new ApexPages.standardController (inq);
        Damac_call_connectController objStd = New Damac_call_connectController (std);
        Damac_call_connectController obj = New Damac_call_connectController ();
        obj.getUserExtension();
        Damac_call_connectController.checkDetails ('123','abc','7008', inq.id,'Inbound', 'test', '8977365');
        Damac_call_connectController.getAllFields ('Account');
        
        obj.displayDetails ();
        obj.callDetailsOutcome = 'test';
        obj.updateTask ();
        obj.updateInquiry ();
        obj.clearValues ();
        obj.createFollowup ();
        obj.createMeetingFollowup ();
    }
    
    static testMethod void testMethod2() {
        Inquiry__c inq = New Inquiry__c ();
        inq.First_Name__c = 'testInq';
        inq.Preferred_Language__c = 'English';
        inq.Last_Name__c ='inqLast';
        inq.Inquiry_Source__c = 'Prospecting';
        inq.Primary_Contacts__c = 'Mobile Phone';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.Mobile_Phone__c = '1236547890';
        inq.Mobile_Phone_Encrypt__c = '1223467886';
        inq.Meeting_Due_Date__c = Date.Today ();
        inq.Email__c = 'test@gmail.com';
        inq.Interested_Location__c = 'Dubai';
        insert inq;
        Task t = new Task ();
            t.Subject='Donni';
            t.Status='New';
            t.Priority='Normal';
            t.CallType='Outbound';
        insert t;
            
        ApexPages.currentPage().getParameters().put('InqId',inq.id);
        ApexPages.currentPage().getParameters().put('tID',t.id);
        ApexPages.currentPage().getParameters().put('isNew','true');
        apexpages.currentpage().getparameters().put ('taskId', t.id);
        ApexPages.standardController std = new ApexPages.standardController (inq);
        Damac_call_connectController objStd = New Damac_call_connectController (std);
        Damac_call_connectController obj = New Damac_call_connectController ();
        obj.getUserExtension();
        Damac_call_connectController.checkDetails ('123','abc','7008', '','Inbound', 'test', '8977365');
        Damac_call_connectController.getAllFields ('Account');
        
        obj.getLocation();
        obj.getDevelopement();
        
        obj.displayDetails ();
        obj.callDetailsOutcome = 'test';
        obj.updateTask ();
        obj.updateInquiry ();
        obj.clearValues ();
        obj.createFollowup ();
        obj.createMeetingFollowup ();
    }
}