@isTest
private class UpdateCustomerMobileFieldBatchTest
{
    @isTest static void itShould() {
        Test.startTest();
        List<Account> accList = new List<Account>();
        for( Integer i= 0; i< 100; i++){
            Account accObj = TestDataFactory_CRM.createPersonAccount();
            accObj.Mobile__c = '11111111111';
            accObj.Party_Id__c = String.valueOf( (i * 100) + 200 );
            accList.add( accObj );
        }
        for( Integer i= 100; i< 200; i++){
            Account accObj = TestDataFactory_CRM.createPersonAccount();
            accObj.Telephone__c = '11111111111';
            accObj.Party_Id__c = String.valueOf( (i * 100) + 300 );
            accList.add( accObj );
        }
        insert accList;
        
        Database.executeBatch(new UpdateCustomerMobileFieldBatch(), 200);
        Test.stopTest();
    }
}