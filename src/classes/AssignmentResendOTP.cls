public without sharing class AssignmentResendOTP {
    String caseId;
    public AssignmentResendOTP(ApexPages.StandardController sc){
        caseId = sc.getId();
        //caseId = ApexPages.currentPage().getParameters().get('Id');
        system.debug('caseId*************'+caseId);
    }
    
    public pagereference reSendOTP(){
        if(String.IsNotBlank(caseId)){
            list<String> setPhoneNumbers = new list<String>();
            list<Case> lstCase = [Select c.Id
                                       , c.Buyer__c
                                       , c.Buyer__r.Phone_Country_Code__c
                                       , c.Buyer__r.Phone__c
                                       , c.Buyer__r.Phone_with_Country_Code__c
                                       , c.Buyer_POA_Country_Code__c
                                       , c.Buyer_POA_Phone__c
                                       , c.Seller_POA_Country_Code__c
                                       , c.Seller_POA_Phone__c
                                  From Case c
                                  where c.Id =: caseId limit 1];
            boolean blnValid = true;
            if(lstCase != null && !lstCase.isEmpty()){
                String str = lstCase[0].Id + ':';
                if(String.isNotBlank(lstCase[0].Buyer__r.Phone_with_Country_Code__c)){
                    Configuration_Control__c mc = Configuration_Control__c.getOrgDefaults();
                    if(mc.OTP_Test_Service_Enabled__c && !String.isBlank(mc.OTP_Test_Service_Number__c)){
                        str = str + mc.OTP_Test_Service_Number__c+','+lstCase[0].Buyer__r.Phone_with_Country_Code__c;
                    }else{   
                        str = str + lstCase[0].Buyer__r.Phone_with_Country_Code__c;
                    }
                }else{
                    blnValid = false;
                }
                if(blnValid){
                    if(lstCase[0].Buyer_POA_Country_Code__c != null
                        && lstCase[0].Buyer_POA_Phone__c != null){
                            String st = lstCase[0].Buyer_POA_Country_Code__c.substringAfter(':').trim();
                            st = st.substringAfter('00');
                            st = st + lstCase[0].Buyer_POA_Phone__c;
                            str = str +','+st;
                        }
                        if(lstCase[0].Seller_POA_Country_Code__c != null
                        && lstCase[0].Seller_POA_Phone__c != null){
                            String st = lstCase[0].Seller_POA_Country_Code__c.substringAfter(':').trim();
                            st = st.substringAfter('00');
                            st = st + lstCase[0].Seller_POA_Phone__c;
                            str = str +','+st;
                        }
                    //setPhoneNumbers.add(str);
                    OTPService.Sendtextmessage(str);
                }
            }
        }
        Pagereference pgRef = new Pagereference('/'+caseId);
        pgRef.setRedirect(true);
        return pgRef;
    }
}