@isTest
private class FetchFMNotesFromAccOnCLCntrlTest {
    
    static testMethod void test_FetchFMNotesFromAccOnCallingListCntrl () {
        
        
        TriggerOnOffCustomSetting__c objSetting = new TriggerOnOffCustomSetting__c();
        objSetting.Name = 'CallingListTrigger';
        objSetting.OnOffCheck__c = false ;
        insert objSetting ;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
        
        Id FMCollectionRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
        Calling_List__c callObj = new Calling_List__c( Account__c = objAcc.Id,Registration_ID__c = '12851'
                                                     , RecordTypeId = FMCollectionRecordTypeId ,Calling_List_Type__c = 'FM Calling List');
        insert callObj;
        
        FM_Notes__c objFM_Notes = new FM_Notes__c( Account__c = objAcc.Id , FMOutcome__c = 'Call Answered   ', FM_Remarks__c = 'Test Please ignore'
                                                 , Result__c  = 'Not Reachable', Type__c  = 'Note');
        insert objFM_Notes;
        
        FM_Notes__c objFM_NotesCL = new FM_Notes__c( FMCallingList__c = callObj.Id , FMOutcome__c = 'Call Answered  ', FM_Remarks__c = 'Test Please ignore'
                                                 , Result__c  = 'Not Reachable', Type__c  = 'Note');
        insert objFM_NotesCl;
        
        Test.startTest();
            PageReference fmNotePage = Page.FetchFMNotesFromAccOnCallingList;
            Test.setCurrentPage(fmNotePage);
            fmNotePage.getParameters().put('Id',String.valueOf(callObj.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(callObj);
            FetchFMNotesFromAccOnCallingListCntrl controllerObj = new FetchFMNotesFromAccOnCallingListCntrl(sc);
        Test.stopTest();
        
    }
}