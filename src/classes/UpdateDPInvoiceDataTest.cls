@isTest
private class UpdateDPInvoiceDataTest{
	@isTest
	static void itShould(){
		List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
		objAccount.Nationality__c = 'Indian';
		objAccount.Email__pc = 'gsauf@hjfd.com';
        insert objAccount;

    	//create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;
        
        DP_Invoices__c obj=new DP_Invoices__c();
      	obj.Accounts__c = objAccount.id;
    	obj.BookingUnits__c = bookingUnitList[0].id;
        obj.Cover_Letter__c = 'test';
        obj.SOA__c= 'test';
        obj.Trxn_Number__c= 'test';
        obj.TAX_Invoice__c= 'test';
        obj.COCD_Letter__c= 'test';
        obj.Other_Language_TAX_Invoice__c = 'test';
		obj.EmailSent__c = false;
		obj.Registration_Id__c ='543346';
        insert obj;
		list<id> lstIds = new list<id>();
		lstIds.add(obj.id);
		UpdateDPInvoiceData.updateInvoiceURLs(lstIds);
	}
}