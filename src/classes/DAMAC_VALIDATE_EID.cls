public class DAMAC_VALIDATE_EID {

    public static DAMAC_VALIDATE_EID_RESP checkEID (String idNumber) {
        Integration_Settings__mdt dldcreds = new Integration_Settings__mdt ();
        dldCreds = [select Endpoint_URL__c,user_name__c from Integration_Settings__mdt where DeveloperName='DLD_EID_settings'];        
        String reqBody = '{"RequestType" : "21", "RequestDetails" :"'+idNumber+'"}';
        System.Debug (reqBody);
        HttpRequest req = new HttpRequest ();
        req.setMethod('POST');
        req.setEndpoint (dldcreds.Endpoint_URL__c);
        req.setHeader('X-ApiKey', dldcreds.user_name__c);
        req.setHeader ('Content-Type', 'application/json');
        req.setBody (reqBody);
        req.setTimeout(120000);
        Http http = new Http ();
        HttpResponse res = new HttpResponse ();
        if (!Test.isRunningTest ()) 
            res = http.send (req);
        System.Debug (res.getBody ());
        if (Test.isRunningTest ()) {
            DAMAC_VALIDATE_EID_RESP obj = new DAMAC_VALIDATE_EID_RESP ();
            res.setBody ('{"EmiratesId" : "784198875106977"}');
            
        }
        return DAMAC_VALIDATE_EID_RESP.parse (res.getBody ());
    }
}