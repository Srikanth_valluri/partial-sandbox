/************************************************************************************************
 * @Name              : BuyerTrgHandlerTest
 * @Description       : Test Class for BuyerTrgHandler
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0                        27/05/2017       Created
 * 1.1         QBurst         07/04/2020       Modified the SOAP to REST changes
***********************************************************************************************/
@isTest 
private class BuyerTrgHandlerTest{
    private static List<Account> accountList = new List<Account>();
    private static Inquiry__c inquiry = new Inquiry__c();
    private static void init(){
        accountList = TestDataFactory.createAccountRecords(new List<Account>{new Account()});
        Inquiry_Conversion_Mapping__c icmObject = new Inquiry_Conversion_Mapping__c();
        icmObject.Name = 'First_Name__c';
        icmObject.Business_Account_Field_Name__c = 'Name';
        icmObject.Business_Contact_Field_Name__c = 'FirstName';
        icmObject.Buyer__c = 'First_Name__c';
        icmObject.Order__c = 1;
        icmObject.Person_Account_Field_Name__c = '';
        insert icmObject;
        inquiry = new Inquiry__c(
            First_Name__c = 'Test',
            Last_Name__c = 'Last Test',
            Inquiry_Source__c = 'Call Center',
            Inquiry_Status__c = 'Active',
            Email__c = 'test@gmail.com',
            OwnerId = UserInfo.getuserid()
        );
        insert inquiry;
        Account acc = new Account(Id = accountList[0].Id);
        acc.Inquiry__c = inquiry.Id;
        update acc;
    }

     @isTest static void Test1() {
        init();
        system.debug('accountList: ' + accountList);
        Location__c loc = new Location__c();
        loc.Location_ID__c = '123';
        insert loc;
        Inventory__c inv = new Inventory__c();
        inv.Unit_Location__c = loc.id;
        insert inv;

        NSIBPM__Service_Request__c serviceReq = new NSIBPM__Service_Request__c();
        serviceReq.Delivery_mode__c = 'Email';
        serviceReq.Deal_ID__c = '1001';
        serviceReq.Registration_Date__c = system.today();
        insert serviceReq;

        List<id> serviceReqIds = new List<id>();
        serviceReqIds.add(serviceReq.id);
        NSIBPM__SR_Doc__c srdoc = new NSIBPM__SR_Doc__c();
        srdoc.NSIBPM__Service_Request__c = serviceReq.id;
        insert srdoc;

        Booking__c bk = new Booking__c();
        bk.Deal_SR__c = serviceReq.id;
        bk.Booking_channel__c = 'Office';
        insert bk;

        Booking_Unit__c objBook = new Booking_Unit__c();
        objBook.Inventory__c = inv.id;
        objBook.Booking__c = bk.id;
        insert objBook;

        Buyer__c primaryBuyer = new Buyer__c();
        primaryBuyer.Primary_Buyer__c = true;
        primaryBuyer.Buyer_Type__c = 'Individual';
        primaryBuyer.Booking__c = bk.id;
        primaryBuyer.Phone_Country_Code__c = 'India: 0091';
        primaryBuyer.Passport_Expiry_Date__c = '25/03/2017';
        primaryBuyer.CR_Registration_Expiry_Date__c = '25/12/2017';
        primaryBuyer.City__c = 'Dubai';
        primaryBuyer.Country__c = 'United Arab Emirates';
        primaryBuyer.Address_Line_1__c = 'street1';
        primaryBuyer.Address_Changed__c = true;
        primaryBuyer.Date_of_Birth__c = '25/12/1990';
        primaryBuyer.Email__c = 'test@test.com';
        primaryBuyer.First_Name__c = 'Buyer';
        primaryBuyer.Last_Name__c = 'test';
        primaryBuyer.Nationality__c = 'Indian'; 
        primaryBuyer.Passport_Number__c = 'PP123';
        primaryBuyer.Phone__c = '53532255';
        primaryBuyer.Place_of_Issue__c = 'Delhi';
        primaryBuyer.Title__c = 'Mr.';
        primaryBuyer.Account__c = accountList[0].Id;
        insert primaryBuyer;

        primaryBuyer.First_Name__c = 'Buyer1';
        primaryBuyer.Passport_Number__c = 'PP1234';
        primaryBuyer.Phone__c = '535322556';
        primaryBuyer.Address_Line_1__c = 'street2';
        update primaryBuyer;

        Buyer__c jointBuyer = new Buyer__c();
        jointBuyer.Primary_Buyer__c = false;
        jointBuyer.Buyer_type__c = 'Individual';
        jointBuyer.Booking__c = bk.id;
        jointBuyer.Passport_Expiry_Date__c = '25/11/2017';
        jointBuyer.CR_Registration_Expiry_Date__c = '25/06/2017';
        jointBuyer.status__c = 'New';
        jointBuyer.Date_of_Birth__c = '25/12/1990';
        jointBuyer.City__c = 'Dubai';
        jointBuyer.Country__c = 'United Arab Emirates';
        jointBuyer.Address_Line_1__c = 'street1';
        jointBuyer.Email__c = 'test@test.com';
        jointBuyer.First_Name__c = 'Buyer';
        jointBuyer.Last_Name__c = 'test';
        jointBuyer.Nationality__c = 'Indian';
        jointBuyer.Passport_Number__c = 'PP123';
        jointBuyer.Phone__c = '53532255';
        jointBuyer.Phone_Country_Code__c = 'India: 0091';
        jointBuyer.Place_of_Issue__c = 'Delhi'; 
        jointBuyer.Title__c = 'Mr.';
        jointBuyer.Inquiry__c = inquiry.Id;
        jointBuyer.Organisation_Name__c = 'Test ORG';
        insert jointBuyer;
        jointBuyer.Status__c = 'Active';
        update jointBuyer;
        jointBuyer.First_Name__c = 'BuyerJB';
        update jointBuyer;
     }
}// End of test class.