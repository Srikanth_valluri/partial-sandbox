/******************************************************************************
* Description - Test class developed for HandoverSendEmailBatch
*
* Version            Date            Author                    Description
* 1.0               29/04/18        Lochan Karle            Initial Draft
********************************************************************************/

@isTest
private class HandoverCheckEligibilityBatchTest {
     static CheckEligibleHandover.EligibleHandover  obj = new CheckEligibleHandover.EligibleHandover();
    static testMethod void UnitTest() {
        
        List<Milestones_For_Handover__c>milestonesSettingLst = new List<Milestones_For_Handover__c>();
        Milestones_For_Handover__c milestoneSetting1 = new Milestones_For_Handover__c(Name = 'On 100% of Villa Completion');
        milestonesSettingLst.add(milestoneSetting1);
        insert milestonesSettingLst;
        
        List<Booking_Unit_Active_Status__c>bookingUnitSettingLst = new List<Booking_Unit_Active_Status__c>();
        Booking_Unit_Active_Status__c setting1 = new Booking_Unit_Active_Status__c(Name = 'Agreement Cancellation Verified',
                                                                                   Status_Value__c = 'Agreement Cancellation Verified');
        
        bookingUnitSettingLst.add(setting1);
        Booking_Unit_Active_Status__c setting2 = new Booking_Unit_Active_Status__c(Name = 'Agreement executed by DAMAC',
                                                                                   Status_Value__c = 'Agreement executed by DAMAC');
        
        bookingUnitSettingLst.add(setting2);
        insert bookingUnitSettingLst;
        
        Account Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        Acc.Email__pc = 'abc@z.com';
        insert Acc;
        System.assert(Acc != null);
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.EHO_Email_Template__c = 'EHO';
        loc.HO_Email_Template__c = 'NHO';
        loc.Finance_Confirmation_Complete__c = true;
        insert loc;
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'JNU/WE/12';  
        BU.Registration_Status__c  = 'Agreement executed by DAMAC';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;
        BU.Approval_Status__c = 'NO';  
        BU.JOPD_Area__c = '1020';
        insert BU;
        System.assert(BU != null);  
          
        Payment_Plan__c objPaymentPlan = new Payment_Plan__c();
        objPaymentPlan.Booking_Unit__c = BU.Id;
        objPaymentPlan.Status__c = 'Active';
        insert objPaymentPlan;
        
        //method call to insert data for payment terms
        insertPaymentTermsComplete(objPaymentPlan.Id);
        
        Booking_Unit__c BU1 =  new Booking_Unit__c();
        BU1.Unit_Name__c = 'Test Units 123';  
        BU1.Registration_Status__c  = 'Agreement executed by DAMAC';
        BU1.Registration_ID__c   = '5678';  
        BU1.Unit_Selling_Price_AED__c  = 100;  
        BU1.Booking__c = Booking.Id;
        BU1.Approval_Status__c = 'NO'; 
        BU1.JOPD_Area__c = '2010'; 
        insert BU1;
 
        List<String> lstBUID= new List<String>();
        lstBUID.add(BU.Id);
        lstBUID.add(BU1.Id);
        
        test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,EligibleForHandoverUpdated.EligibleForHandoverNoticeResponse_element>();
        EligibleForHandoverUpdated.EligibleForHandoverNoticeResponse_element response1 = 
            new EligibleForHandoverUpdated.EligibleForHandoverNoticeResponse_element ();
            
        response1.return_x ='{"allowed":null,"message":"BCC not available","mortgageNOCfromB'+
        'ank":null,"ifPoaTakingHandoverColatePoaPassportResidence":null,"corporateValidTra'+
        'deLicence":null,"corporateArticleMemorandumOfAssociation":null,"corporateBoard'+
        'Resolution":null,"corporatePoa":null,"signedForm":null,"clearAndValidPassp'+
        'ortCopyOfOwner":null,"clearAndValidPassportCopyOfJointOwner":null,"visaOrE'+
        'ntryStampWithUid":null,"copyofValidEmiratesId":null,"copyofValidGccId":null,"handoverCh'+
        'ecklistAndLod":null,"keyReleaseForm":null,"checkOriginalSpaAndtakeCopyOfFirstFourP'+
        'agesOfSpa":null,"areaVariationAddendum":null,"tempOne":null,"tempTwo":null,"tem'+
        'pThree":null,"handoverNoticeAllowed":"EHO","approvalQueueOne":null,"approvalQueu'+
        'eTwo":null,"approvalQueueThree":null,"eligibleforRentalPool":null}';  
          SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
          Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
         obj = CheckEligibleHandover.getEligibleHandover('1234', 
                                   'processName',
                                   'subProcessName',
                                   'modeOfRequest',
                                    'noOfMajorSnagsInApartment',
                                    'accessPresent',
                                     'utilitiesAvailable',
                                     'percAptsSnagged',
                                     'EHO',
                                     'DaysToEarliestViewing' );
            HandoverCheckEligibilityBatch obj1 = new HandoverCheckEligibilityBatch();
            DataBase.executeBatch(obj1);     
        test.stopTest();
        
    }
    
    
    private static List<Payment_Terms__c> insertPaymentTermsComplete(Id paymentPlanID)
  {
    List<Payment_Terms__c> lstPaymentTerms = new List<Payment_Terms__c>();

    //insert data for payment terms
    Payment_Terms__c objPaymentTerm1 = new Payment_Terms__c();
    objPaymentTerm1.Payment_Plan__c = paymentPlanID;
    objPaymentTerm1.Installment__c = 'DP';
    objPaymentTerm1.Description__c = 'DEPOSIT';
    objPaymentTerm1.Percent_Value__c = '24';
    objPaymentTerm1.Milestone_Event__c = 'Immediate';
    lstPaymentTerms.add(objPaymentTerm1);

    Payment_Terms__c objPaymentTerm2 = new Payment_Terms__c();
    objPaymentTerm2.Payment_Plan__c = paymentPlanID;
    objPaymentTerm2.Installment__c = 'I001';
    objPaymentTerm2.Description__c = '1ST INSTALLMENT';
    objPaymentTerm2.Percent_Value__c = '0';
    objPaymentTerm2.Milestone_Event__c = '';
    lstPaymentTerms.add(objPaymentTerm2);

    Payment_Terms__c objPaymentTerm3 = new Payment_Terms__c();
    objPaymentTerm3.Payment_Plan__c = paymentPlanID;
    objPaymentTerm3.Installment__c = 'I002';
    objPaymentTerm3.Description__c = '2ND INSTALLMENT';
    objPaymentTerm3.Percent_Value__c = '10';
    objPaymentTerm3.Milestone_Event__c = 'On or Before';
    objPaymentTerm3.Payment_Date__c = System.today().addDays(1);
    lstPaymentTerms.add(objPaymentTerm3);

    Payment_Terms__c objPaymentTerm4 = new Payment_Terms__c();
    objPaymentTerm4.Payment_Plan__c = paymentPlanID;
    objPaymentTerm4.Installment__c = 'I003';
    objPaymentTerm4.Description__c = '3RD INSTALLMENT';
    objPaymentTerm4.Percent_Value__c = '10';
    objPaymentTerm4.Milestone_Event__c = 'On or Before';
    objPaymentTerm4.Payment_Date__c = System.today().addDays(2);
    lstPaymentTerms.add(objPaymentTerm4);

    Payment_Terms__c objPaymentTerm5 = new Payment_Terms__c();
    objPaymentTerm5.Payment_Plan__c = paymentPlanID;
    objPaymentTerm5.Installment__c = 'I004';
    objPaymentTerm5.Description__c = '4TH INSTALLMENT';
    objPaymentTerm5.Percent_Value__c = '10';
    objPaymentTerm5.Milestone_Event__c = 'On or Before';
    objPaymentTerm5.Payment_Date__c = System.today().addDays(3);
    lstPaymentTerms.add(objPaymentTerm5);

    Payment_Terms__c objPaymentTerm6 = new Payment_Terms__c();
    objPaymentTerm6.Payment_Plan__c = paymentPlanID;
    objPaymentTerm6.Installment__c = 'I005';
    objPaymentTerm6.Description__c = '5TH INSTALLMENT';
    objPaymentTerm6.Percent_Value__c = '10';
    objPaymentTerm6.Milestone_Event__c = 'On or Before';
    lstPaymentTerms.add(objPaymentTerm6);
    
    Payment_Terms__c objPaymentTerm7 = new Payment_Terms__c();
    objPaymentTerm7.Payment_Plan__c = paymentPlanID;
    objPaymentTerm7.Installment__c = 'I006';
    objPaymentTerm7.Description__c = '6TH INSTALLMENT';
    objPaymentTerm7.Percent_Value__c = '20';
    objPaymentTerm7.Milestone_Event__c = 'On or Before';
    lstPaymentTerms.add(objPaymentTerm7);
    
    Payment_Terms__c objPaymentTerm8 = new Payment_Terms__c();
    objPaymentTerm8.Payment_Plan__c = paymentPlanID;
    objPaymentTerm8.Installment__c = 'I007';
    objPaymentTerm8.Description__c = '7TH INSTALLMENT';
    objPaymentTerm8.Percent_Value__c = '20';
    objPaymentTerm8.Milestone_Event__c = 'On or Before';
    lstPaymentTerms.add(objPaymentTerm8);
    
    insert lstPaymentTerms;
    return lstPaymentTerms;
  }
}