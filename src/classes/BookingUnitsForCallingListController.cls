/*
    *   Class to fetch units related to customer and make callout to fetch invoice dues related to each units
    */
    public class BookingUnitsForCallingListController {
        
        public String recordId;
        public Set<Id> bookingIdSet;
        public List<Booking_Unit__c> bookingUnitList       {get;set;}
        public Calling_List__c callingObj;
        public Map<String,InvDueWrap> mapRegIdTotalInvDue   {get;set;}
        
        public BookingUnitsForCallingListController(ApexPages.StandardController controller) {
        }
        
        public void init() {
            
            bookingIdSet = new Set<Id>();
            bookingUnitList = new List<Booking_Unit__c>();
            mapRegIdTotalInvDue = new Map<String,InvDueWrap>();
            
            recordId = ApexPages.currentPage().getParameters().get('id');
            System.debug('callingRec:::::recordId:::::!'+recordId);
            callingObj = [SELECT Id,Party_Id__c,Account__c
                            FROM Calling_List__c
                           WHERE Id =:recordId];

            
            if(String.isNotBlank(callingObj.Account__c)){
                for (Booking__c objBooking : [Select Id, 
                                                 Account__c
                                            From Booking__c 
                                           Where Account__c =: callingObj.Account__c]) {
                 bookingIdSet.add(objBooking.Id);                     
                }

                if(bookingIdSet != null && !bookingIdSet.isEmpty()) {
                  bookingUnitList = [ SELECT Id,
                                           Name,
                                           Booking__c,
                                           Status__c,
                                           Account_Id__c,
                                           Registration_ID__c,
                                           Handover_Flag__c,
                                           Dispute_Flag__c,
                                           Property_Name__c,
                                           Requested_Price__c,
                                           Unit_Name__c,
                                           Selling_Price__c,
                                           Price_per_sq_ft__c,
                                           Party_Id__c,
                                           Area__c,
                                           Registration_Status__c,
                                           DP_OK__c,
                                           Doc_OK__c,
                                           Mortgage__c,
                                           Anticipated_Completion_Date__c,
                                           Onsite_Construction__c
                                      FROM Booking_Unit__c
                                     Where Booking__c IN :bookingIdSet 
                                     AND Registration_ID__c != ''
                                     AND Unit_Name__c != ''
                                     Limit 950
                                     ];
                    for( Booking_Unit__c objBu : bookingUnitList ) {
                        if( String.isNotBlank(objBu.Registration_ID__c )) {
                            mapRegIdTotalInvDue.put(objBu.Registration_ID__c,new InvDueWrap(0.00,0.00,0.00,''));
                        }
                    }
                }
                
                if( bookingUnitList.size() <= 0 ){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No Booking Units to display.'));
                }
                
                mapRegIdTotalInvDue = getUnitWiseFmDues(callingObj.Party_Id__c , callingObj.Account__c,mapRegIdTotalInvDue );
                system.debug('<<<<<<<<<<< mapRegIdTotalInvDue : '+mapRegIdTotalInvDue);
            } else{
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please Populate Account on Calling List'));
            }
        }
        
        public class InvDueWrap{
            
            public Decimal totalDueRemaining        {get;set;}
            public Decimal totalCharges             {get;set;}
            public Decimal totalPaidAmount          {get;set;}
            public String   oldestDueDate           {get;set;}
            public InvDueWrap( Decimal totalDueRemaining , Decimal totalCharges , Decimal totalPaidAmount, String oldestDueDate) {
                this.totalDueRemaining = totalDueRemaining;
                this.totalCharges = totalCharges;
                this.totalPaidAmount = totalPaidAmount;
                this.oldestDueDate = oldestDueDate;
            }
        }
        
        public static Map<String,InvDueWrap> getUnitWiseFmDues( String strPartyId , String acctId , Map<String,InvDueWrap> mapRegIdTotalInvDues ) {
            
            if( String.isBlank( strPartyId ) ) {
                return null;
            }
            
            FmIpmsRestServices.DueInvoicesResult dues = fetchFmDues(strPartyId,acctId);
            Decimal totalCharges = 0.00;
            Decimal totalDueRemaining = 0.00;
            Decimal totalPaidAmount = 0.00;
            
            Map<String,String> mapCombRegIdDueDaysDueDate = new Map<String,String>();
            //Map<String,InvDueWrap> mapRegIdTotalInvDues = new Map<String,InvDueWrap>();
            
            system.debug('<<<<<<<<<<< dues : '+dues);
            system.debug('<<<<<<<<<<< dues.lstDueInvoice  : '+dues.lstDueInvoice );
            
            if( dues.lstDueInvoice != null && dues.lstDueInvoice.size() > 0 ) {
                for( FmIpmsRestServices.DueInvoice objDueInv : getSortedList( dues.lstDueInvoice ) ) {
                    system.debug('<<<<<<<<<<< dues.objDueInv  : '+objDueInv );
                    if( String.isNotBlank( objDueInv.registrationId ) && objDueInv.dueDays != null && String.isNotBlank(objDueInv.dueDate)) {
                        mapCombRegIdDueDaysDueDate.put(objDueInv.registrationId + '-'+ objDueInv.dueDays,objDueInv.dueDate);
                    }
                    
                    if( mapRegIdTotalInvDues.containsKey(objDueInv.registrationId ) ) {
                        if( objDueInv.dueRemaining != null ) {
                            totalDueRemaining = mapRegIdTotalInvDues.get(objDueInv.registrationId).totalDueRemaining + objDueInv.dueRemaining;
                        }
                        if( objDueInv.chargeAmount != null ) {
                            totalCharges = mapRegIdTotalInvDues.get(objDueInv.registrationId).totalCharges + objDueInv.chargeAmount;
                        }
                        if( objDueInv.paidAmount != null ) {
                            totalPaidAmount = mapRegIdTotalInvDues.get(objDueInv.registrationId).totalPaidAmount + objDueInv.paidAmount;
                        }                   

                        mapRegIdTotalInvDues.put(objDueInv.registrationId ,new InvDueWrap(totalDueRemaining,totalCharges,totalPaidAmount,getUnitWiseOldestDate(mapCombRegIdDueDaysDueDate,objDueInv)));
                    } else {
                        if( objDueInv.dueRemaining != null ) {
                            totalDueRemaining += objDueInv.dueRemaining;
                        }
                        if( objDueInv.chargeAmount != null ) {
                            totalCharges += objDueInv.chargeAmount;
                        }
                        if( objDueInv.paidAmount != null ) {
                            totalPaidAmount += objDueInv.paidAmount;
                        }                       
                        totalDueRemaining += objDueInv.dueRemaining;
                        mapRegIdTotalInvDues.put(objDueInv.registrationId ,new InvDueWrap(totalDueRemaining,totalCharges,totalPaidAmount,getUnitWiseOldestDate(mapCombRegIdDueDaysDueDate,objDueInv)));
                    }
                    
                }
            }
            return mapRegIdTotalInvDues;
        }
        
        public static FmIpmsRestServices.DueInvoicesResult fetchFmDues( String strPartyId , String acctId ) {
            FmIpmsRestServices.DueInvoicesResult dues;
            
            if( String.isNotBlank( strPartyId ) ) {
                try {
                    dues = FmIpmsRestServices.getDueInvoices('', strPartyId, '');
                } catch(Exception excp) {
                    System.debug('excp = ' + excp);
                    errorLogger( excp.getMessage()+' at ' +String.valueOf( excp.getLineNumber() ) , acctId );
                }
            }
            return dues;
        }
        
        public static List<FmIpmsRestServices.DueInvoice> getSortedList( List<FmIpmsRestServices.DueInvoice> lstDueInv ) {
            
            Map<Integer,FmIpmsRestServices.DueInvoice> fmDueMap = new Map<Integer,FmIpmsRestServices.DueInvoice>();
            Map<Integer,FmIpmsRestServices.DueInvoice> fmDueMapSorted = new Map<Integer,FmIpmsRestServices.DueInvoice>();
            List<Integer> lstDueDays = new List<Integer>();
            List<FmIpmsRestServices.DueInvoice> sortedList = new List<FmIpmsRestServices.DueInvoice>();

            if( lstDueInv != null  && lstDueInv.size() > 0 ) {
                for ( FmIpmsRestServices.DueInvoice objFmIpms : lstDueInv ) {
                    fmDueMap.put( objFmIpms.dueDays , objFmIpms );
                }
                //Add all elements of Set into a list
                if( !fmDueMap.isEmpty() ) {
                    lstDueDays.addAll(fmDueMap.keyset());
                    //Sort the List
                    lstDueDays.sort();
                }
                 
                for ( Integer i= 0;i < lstDueDays.size(); i++){
                    fmDueMapSorted.put(lstDueDays[i],fmDueMap.get(lstDueDays[i]));
                }
                 
                //Get the sorted list
                if( !fmDueMapSorted.isEmpty() ) {
                    sortedList = fmDueMapSorted.values();
                }
            }
            return sortedList;
        }
        
        public static String getUnitWiseOldestDate( Map<String,String> mapCombRegIdDueDaysDueDate , FmIpmsRestServices.DueInvoice objDueInv ) {
            String oldDueDate ;
            List<Integer> lstDueDays = new List<Integer> {objDueInv.dueDays};
            lstDueDays.sort();
            system.debug('lstDueDays >>>>>>>> '+lstDueDays);                        
            List<Integer> lstDueDaysReversed = new List<Integer> ();
            for(Integer i = lstDueDays.size() - 1; i >= 0; i--){
                lstDueDaysReversed.add(lstDueDays[i]);
            }
            system.debug('lstDueDaysReversed >>>>>>>> '+lstDueDaysReversed);
            system.debug('mapCombRegIdDueDaysDueDate >>>>>>>> '+mapCombRegIdDueDaysDueDate);
            
            if( mapCombRegIdDueDaysDueDate.containsKey( objDueInv.registrationId + '-' +lstDueDaysReversed[0]) ) {
                oldDueDate =  mapCombRegIdDueDaysDueDate.get(objDueInv.registrationId + '-' + lstDueDaysReversed[0]);
            }
            system.debug('oldDueDate >>>>>>>> '+oldDueDate);
            
            return oldDueDate;
        }
        
        private static void errorLogger( string strErrorMessage,String acctId ) {
            Error_Log__c objError = new Error_Log__c();
            objError.Error_Details__c = strErrorMessage;
            if( String.isNotBlank( acctId ) ){
                objError.Account__c = acctId;
            }
            insert objError;
        }
    }