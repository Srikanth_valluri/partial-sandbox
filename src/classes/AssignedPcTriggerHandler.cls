/**************************************************************************************************
* Name               : AssignedPcTriggerHandler                                                   *
* Description        : This is a handler class to manage Assigned PC object.                      *
*                      - Used to share records with PC and PC's Manager.                          *
* Created Date       : 17/01/2017                                                                 *
* Created By         : NSI - Vineet                                                               *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR                    DATE                                                      *
* 1.0         NSI - Vineet              17/01/2017
* 1.1         NSI - Rahul Godara        03/07/2017          code clean up and cpu limit fix.
* 1.3         NSI - Rahul Godara        04/07/2017          Inventory shared with campaign PC
* 1.4         Tejashree Chavan          07/11/2017          Added isRecursion flag
* 1.5         Tejashree Chavan          09/11/2017          Added code for Assigned PC history
                                                            tracking.
**************************************************************************************************/
public class AssignedPcTriggerHandler extends UtilitySharingManager implements TriggerFactoryInterface{ 
    public static Boolean isRecursion = false;
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after insert.                        *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterInsertTrigger(Map<Id, sObject> newRecordsMap){
        List<Campaign_Inventory_History__c> campaignHistoryList = new List<Campaign_Inventory_History__c>();
        for(Assigned_PC__c assignPCObj : (List<Assigned_PC__c>)newRecordsMap.values())
        {
            Campaign_Inventory_History__c campaignHistoryObj = new Campaign_Inventory_History__c();
            campaignHistoryObj.Marketing_Campaign__c = assignPCObj.Campaign__c;
            campaignHistoryObj.Type__c = 'Assigned PC History';
            campaignHistoryObj.Assign_PC__c = assignPCObj.User__c;
            campaignHistoryList.add(campaignHistoryObj);
        }
        insert campaignHistoryList;

        try{
            if(isRecursion == false) {
                /*Calling method to update inquiry status when a activity is created. */
                provideAccess((Map<Id, Assigned_PC__c>) newRecordsMap);

                //createInventoryUsers
                Map<ID,List<Assigned_PC__c>> mapCampainAssignedPc = new Map<ID,List<Assigned_PC__c>>();

                for(Assigned_PC__c thisCamp_PC : (List<Assigned_PC__c>)newRecordsMap.values()){
                    if(mapCampainAssignedPc.containsKey(thisCamp_PC.Campaign__c))
                        mapCampainAssignedPc.get(thisCamp_PC.Campaign__c).add(thisCamp_PC);
                    else
                        mapCampainAssignedPc.put(thisCamp_PC.Campaign__c,new List<Assigned_PC__c>{thisCamp_PC});
                }

                if(!mapCampainAssignedPc.isEmpty()){
                    system.debug('####creating Inventory Users...');
                    createInventoryUsers(mapCampainAssignedPc);
                }
                isRecursion = true;
            }
        }catch(exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
    }    
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after delete.                        *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    
    @TestVisible private void deleteInventoryUsers(List<Assigned_PC__c> oldCampaignAssignedPc){
        Set<ID> CampaignIDs = new Set<ID>();
        Set<ID> userIds = new Set<ID>();
        
        for(Assigned_PC__c thisCamp_Pc :  oldCampaignAssignedPc){
            if(thisCamp_Pc.Campaign__c != null)
                CampaignIDs.add(thisCamp_Pc.Campaign__c);
            if(thisCamp_Pc.User__c != null)
                userIds.add(thisCamp_Pc.User__c);
        }
        database.delete([Select id from Inventory_User__c where Campaign_ID__c IN: CampaignIDs AND User__c IN:userIds]);
        
    }
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after insert.                        *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    
    private void createInventoryUsers(Map<Id, List<Assigned_PC__c>> mapOfCampAssignedPcs){
        List<Inventory_User__c> upsertInventoryUser = new List<Inventory_User__c>();
        //TODO : add campaign PC in inventory user
        
       // map<id,Campaign__c> cmpMap=new map<id,Campaign__c>([select id,Start_Date__c,End_Date__c,(select id,User__c from Assigned_PCs__r) from Campaign__c where id IN:mapOfCampInventories.keySet()]);
        for(Campaign__c cmp:[select id,Start_Date__c,End_Date__c,(select id,Inventory__c,Campaign__c from Campaign_Inventories__r) from Campaign__c where id IN:mapOfCampAssignedPcs.keySet()])
        {
           
            for(Campaign_Inventory__c inv:cmp.Campaign_Inventories__r)
            {
               
                for(Assigned_PC__c pc : mapOfCampAssignedPcs.get(cmp.id)){
                
                upsertInventoryUser.add(new Inventory_User__c(Inventory__c = inv.Inventory__c,
                                                          User__c = pc.user__c,
                                                          Campaign__c = inv.Campaign__c,
                                                          Campaign_ID__c = inv.Campaign__c,
                                                          Unique_Key__c = pc.User__c+'###'+inv.Inventory__c+'###'+inv.Campaign__c,
                                                          Start_Date__c = cmp.Start_Date__c,
                                                          End_Date__c = cmp.End_Date__c
                                                        ));
             }   
            }
             
        }
        if(!upsertInventoryUser.isEmpty() && upsertInventoryUser.size() > 0){
            Database.UpsertResult[] upsertResults = Database.upsert(upsertInventoryUser,Inventory_User__c.unique_key__c.getDescribe().getSObjectField(),false);
       for (Database.UpsertResult sr : upsertResults) {
                if (sr.isSuccess()) {
                    System.debug('#### Successfully11 inserted record. ' + sr.getId());
                }else {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('#### The following error has occurred.');                   
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('#### Fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
    }
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after delete.                        *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterDeleteTrigger(Map<Id, sObject> oldRecordsMap){ 
        try{
            if(isRecursion == false) {
                /*Calling method to update inquiry status when a activity is created. */
                revokeAccess((Map<Id, Assigned_PC__c>) oldRecordsMap);
                deleteInventoryUsers((List<Assigned_PC__c>)oldRecordsMap.values());
                isRecursion = true;
            }
        }catch(exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
    }  
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed before insert.                       *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/ 
    public void executeBeforeInsertTrigger(List<sObject> newRecordsList){
        if(isRecursion == false) {
            Set<ID> userIDs = new Set<ID>();
            for(Assigned_PC__c aPC : (List<Assigned_PC__c>)newRecordsList){
                aPC.Unique_ID__c = aPC.User__c+'###'+aPC.Campaign__c;
                if(aPC.User__c != null){
                    userIDs.add(aPC.User__c);
                }
            }
            Map<ID,User> mapUsers = new Map<ID,User>([Select Id,Email FROM User WHERE ID IN:userIDs]);
            for(Assigned_PC__c aPC : (List<Assigned_PC__c>)newRecordsList){
                if(aPC.User__c != null && mapUsers.containsKey(aPC.User__c)){
                    aPC.PC_Email__c = mapUsers.get(aPC.User__c).Email;
                }
            }
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed before update.                       *
    * @Params      : Map<Id, sObject>, Map<Id, sObject>                                          *
    * @Return      : void                                                                        *
    *********************************************************************************************/ 
    public void executeBeforeUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){
        if(isRecursion == false) {
            Set<ID> userIDs = new Set<ID>();
            for(Assigned_PC__c aPC : (List<Assigned_PC__c>)newRecordsMap.values()){
                aPC.Unique_ID__c = aPC.User__c+'###'+aPC.Campaign__c;
                if(aPC.User__c != null){
                    userIDs.add(aPC.User__c);
                }
            }
            Map<ID,User> mapUsers = new Map<ID, User>([Select Id,Email FROM User WHERE ID IN:userIDs]);
            for(Assigned_PC__c aPC : (List<Assigned_PC__c>)newRecordsMap.values()){
                if(aPC.User__c != null && mapUsers.containsKey(aPC.User__c)){
                    aPC.PC_Email__c = mapUsers.get(aPC.User__c).Email;
                }
            }
        }
    }
    
    
    
    /*********************************************************************************************
    * @Description : Method to provide access to the PC and PC's Manager.                        *
    * @Params      : Map<Id, Assigned_PC__c>                                                     *
    * @Return      : void                                                                        *
    *********************************************************************************************/ 
    @TestVisible  private void provideAccess(Map<Id, Assigned_PC__c> newRecordsMap){
        List<SObject> shareRecordsList = new List<SObject>();
        Map<Id, List<Assigned_PC__c>> ownerCampaignMap = new Map<Id, List<Assigned_PC__c>>();  
        for(Assigned_PC__c thisAssignedPc : newRecordsMap.values()){
            if(thisAssignedPc.User__c != null){
                if(ownerCampaignMap.containsKey(thisAssignedPc.User__c)){
                    ownerCampaignMap.get(thisAssignedPc.User__c).add(thisAssignedPc);       
                }else{
                    ownerCampaignMap.put(thisAssignedPc.User__c, new List<Assigned_PC__c>{thisAssignedPc});     
                }
            }
        }   
        if(!ownerCampaignMap.isEmpty()){
            for(Assigned_PC__c thisAssignedPc : newRecordsMap.values()){
                if(thisAssignedPc.User__c != null){ 
                    /* Calling sharing utility class to grant access to user hierarchy. */
                    shareRecordsList.addAll(invokeAccessWithoutHierarchy(thisAssignedPc.User__c, 
                                                                         thisAssignedPc.Campaign__c.getSobjectType().getDescribe().getName(), 
                                                                         thisAssignedPc.Campaign__c, 
                                                                         ''));
                }
            }
            if(!shareRecordsList.isEmpty()){
                /* Calling queable method to insert sharing records in the sharing table. */
                system.enqueueJob(new UtilitySharingManager(shareRecordsList, new List<sObject>()));
            }   
        }
    }    
    
    /*********************************************************************************************
    * @Description : Method to revoke access after the PC is removed from the campaign.          *
    * @Params      : Map<Id, Assigned_PC__c>                                                     *
    * @Return      : void                                                                        *
    *********************************************************************************************/ 
    public void revokeAccess(Map<Id, Assigned_PC__c> deletedRecordsMap){
        Set<Id> campaignIdsSet = new Set<Id>();
        Set<Id> assignedPcIdsSet = new Set<Id>();
        List<Campaign__Share> deleteSharingRecordList = new List<Campaign__Share>();
        
        for(Assigned_PC__c thisDeletedPc : deletedRecordsMap.values()){
            campaignIdsSet.add(thisDeletedPc.Campaign__c);  
            assignedPcIdsSet.add(thisDeletedPc.User__c);
        }
        if(!campaignIdsSet.isEmpty()){
            /* Calling method to get records from campaign sharing table. */
            Map<Id, List<Campaign__Share>> campaignShareRecordsMap = getSharingRecords(campaignIdsSet, assignedPcIdsSet, '');   
            if(!campaignShareRecordsMap.isEmpty()){
                for(Assigned_PC__c thisDeletedPc : deletedRecordsMap.values()){
                    if(campaignShareRecordsMap.containsKey(thisDeletedPc.Campaign__c)){
                        for(Campaign__Share thisShareRecord : campaignShareRecordsMap.get(thisDeletedPc.Campaign__c)){
                            if(thisDeletedPc.User__c == thisShareRecord.UserOrGroupId){
                                deleteSharingRecordList.add(thisShareRecord);
                            }
                        }
                    }
                }
                if(!deleteSharingRecordList.isEmpty()){
                    database.deleteResult[] result = database.delete(deleteSharingRecordList, false);
                }
            }
        }   
    }
    
    /*********************************************************************************************
    * @Description : Method to get sharing records from the campaign sharing table.              *
    * @Params      : Set<Id>                                                                     *
    * @Return      : Map<Id, List<Campaign__Share>>                                              *
    *********************************************************************************************/  
    @TestVisible private Map<Id, List<Campaign__Share>> getSharingRecords(Set<Id> campaignIds, Set<Id> assignedPcIdsSet, String customRowCauseName){
        Map<Id, List<Campaign__Share>> campaignShareRecordsMap = new Map<Id, List<Campaign__Share>>();
        String rowCauseName = String.isNotBlank(customRowCauseName) ? customRowCauseName : 'Property_Consultant_Access__c';
        for(List<Campaign__Share> ShareRecordList : [SELECT Id, ParentId, AccessLevel, UserOrGroupId, RowCause 
                                                     FROM Campaign__Share 
                                                     WHERE ParentId IN: campaignIds AND 
                                                           UserOrGroupId IN: assignedPcIdsSet AND 
                                                           RowCause =: rowCauseName LIMIT 50000]){
            for(Campaign__Share thisShareRecord:ShareRecordList){
                if(campaignShareRecordsMap.containsKey(thisShareRecord.ParentId)){
                    campaignShareRecordsMap.get(thisShareRecord.ParentId).add(thisShareRecord);     
                }else{
                    campaignShareRecordsMap.put(thisShareRecord.ParentId, new List<Campaign__Share>{thisShareRecord});
                }
            }
        }
        return campaignShareRecordsMap; 
    }
    
    //TOBE Implemented
    public void executeBeforeInsertUpdateTrigger(List<sObject> newRecordsList, map<Id,sObject> oldRecordsMap){}
    public void executeAfterUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){}

    public void executeBeforeDeleteTrigger(Map<Id,sObject> oldRecordsMap){
        List<Campaign_Inventory_History__c> campaignPCHistoryList = new List<Campaign_Inventory_History__c>();
        Set<Id> assignUsers = new Set<Id>();
        Set<Id> marketingCampigns = new Set<Id>();
        for(Assigned_PC__c assignPc : (List<Assigned_PC__c>)oldRecordsMap.values()) {
            assignUsers.add(assignPc.User__c);
            marketingCampigns.add(assignPc.Campaign__c);
        }
        for(Campaign_Inventory_History__c campHisObj : [SELECT
                                                            Inventory__c
                                                          FROM
                                                            Campaign_Inventory_History__c
                                                          WHERE
                                                            Assign_PC__c IN :assignUsers
                                                            AND
                                                            Marketing_Campaign__c IN :marketingCampigns])
        {
            campHisObj.Deleted_On__c = System.now();
            campHisObj.Deleted_By__c = UserInfo.getName();
            campaignPCHistoryList.add(campHisObj);
        }
        update campaignPCHistoryList;
    }
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){}
}// End of class