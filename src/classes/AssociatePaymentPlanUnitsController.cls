/****************************************************************************************************
* Name               : AssociatePaymentPlanUnitsController
* Description        : Controller class for Associate Payment Plan VF Page
* Created Date       : 16/10/2020
* Created By         : QBurst
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         16/10/2020      Initial Draft
****************************************************************************************************/

    global with sharing class AssociatePaymentPlanUnitsController {
    public String floorId {get; set;}
    public Location__c floor {get; set;}
    public static string unitDetails {get;set;}

    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public AssociatePaymentPlanUnitsController() {
        floorId = ApexPages.currentPage().getParameters().get('Id');
        system.debug('floorId'+floorId);
        if(floorId != null && floorId != ''){
            floor = [SELECT Id, Name, Building_Name__c, Floor_Name__c, Location_ID__c,
                                Property_Name__r.Property_Name__c, Property_Name__c,
                                Building_Number__c
                               FROM Location__c 
                               WHERE Location_Type__c = 'Floor'
                               AND Id =: floorId];    
               fetchFloorDetails();
           }
    }

    public void fetchFloorDetails(){
         Map<Id, Location__c> locationMap = new Map<Id, Location__c>();
         Map<Id, Boolean> locationPayPlanMap = new Map<Id, Boolean>();
         Map<Id, Inventory__c> locationInventoryMap = new Map<Id, Inventory__c>();
         List<UnitDetailsWrapper> wrapperList = new List<UnitDetailsWrapper>();
         for(Location__c loc: [SELECT Id, Name, Unit_Name__c, Location_Code__c
                               FROM Location__c 
                               WHERE RecordType.Name = 'Unit'
                               AND Floor_Number__c =: floorId
                               ORDER BY Unit_Name__c ASC]){
             locationMap.put(loc.Id, loc);
             locationPayPlanMap.put(loc.Id, false);
         }
         system.debug('locationMap: '+ locationMap);

         for(Payment_Plan_Association__c planAssoc : [SELECT Id, Payment_Plan__c, Location__c
                                                     FROM Payment_Plan_Association__c
                                                     WHERE Location__c IN: locationMap.keyset()]){
             locationPayPlanMap.put(planAssoc.Location__c , true);
         }

         for(Inventory__c inv: [SELECT Id, Status__c, Unit_Location__c,
                                 Gross_Area_sft__c, Plot_Area_sft__c,
                                 Terrace_Area_sft__c, Balcony_Area_sft__c,
                                 Garage_Area_sft__c, Selling_Price__c
                                FROM Inventory__c 
                                WHERE Unit_Location__c IN: locationMap.keyset()]){
             locationInventoryMap.put(inv.Unit_Location__c, inv);
         }
         system.debug('locationInventoryMap: '+ locationInventoryMap);
         for(Location__c loc: locationMap.values()){
             if(locationInventoryMap.containsKey(loc.Id)){
                 Decimal gra = 0.0;
                 Decimal plotArea = 0.0;
                 Decimal sellableArea = 0.0;
                 Decimal suiteArea = 0.0;
                 Decimal balconyArea = 0.0;
                 Decimal garageArea = 0.0;
                 Decimal terraceArea = 0.0;
                 Decimal unitPrice = 0.0;
                 Boolean payPlan = false;
                 if(locationPayPlanMap.containsKey(loc.Id)){
                     payPlan = locationPayPlanMap.get(loc.Id);
                 }             
                 Inventory__c inv = locationInventoryMap.get(loc.Id);
                 system.debug('inv: ' + inv);
                 if(inv.Gross_Area_sft__c != null && inv.Gross_Area_sft__c > 0){
                     gra = inv.Gross_Area_sft__c;
                 }
                 if(inv.Plot_Area_sft__c != null && inv.Plot_Area_sft__c  > 0){
                     plotArea = inv.Plot_Area_sft__c;
                 }
                 //sellableArea = inv.Plot_Area_sft__c;
                // suiteArea = inv.Plot_Area_sft__c;
                 if(inv.Balcony_Area_sft__c != null && inv.Balcony_Area_sft__c  > 0){
                     balconyArea = inv.Balcony_Area_sft__c;
                 }
                 if(inv.Garage_Area_sft__c != null && inv.Garage_Area_sft__c  > 0){
                     garageArea = inv.Garage_Area_sft__c;
                 }if(inv.Terrace_Area_sft__c != null && inv.Terrace_Area_sft__c  > 0){
                     terraceArea = inv.Terrace_Area_sft__c;
                 }
                 if(inv.Selling_Price__c != null && inv.Selling_Price__c  > 0){
                     unitPrice = inv.Selling_Price__c;
                 }
                 UnitDetailsWrapper details = new UnitDetailsWrapper(loc.Location_Code__c, loc.Id, payPlan, gra, 
                                                                 plotArea, sellableArea, suiteArea, balconyArea,
                                                                 garageArea, terraceArea, unitPrice);
                 wrapperList.add(details);
                    
             }
             
         }    
         unitDetails = JSON.serialize(wrapperList);
         system.debug('floorDetails: ' + unitDetails);
    }

    public class UnitDetailsWrapper{
        public string unit;
        public string unitId;
        public string plan;
        public Decimal gra;
        public Decimal plot_area;
        public Decimal sellable_area;
        public Decimal suite_area;
        public Decimal balcony_area;
        public Decimal garage_area;
        public Decimal terrace_area;
        public Decimal unit_price;
        public UnitDetailsWrapper(){
        }
        public UnitDetailsWrapper(String unitName, String uId,
                                Boolean planPresent, Decimal gra,
                                Decimal plotArea,
                                Decimal sellableArea, Decimal suiteArea,
                                Decimal balconyArea, Decimal garageArea,
                                Decimal terraceArea, Decimal unitPrice){
            unit = unitName;
            unitId = uId;
            if(planPresent){
                plan = 'plan available';
            } else{
                plan = 'plan NOT available';
            }
            this.gra = gra;
            plot_area = plotArea;
            sellable_area = sellableArea;
            suite_area = suiteArea;
            balcony_area = balconyArea;
            garage_area = garageArea;
            terrace_area = terraceArea;
            unit_price = unitPrice;
        }
    }
}