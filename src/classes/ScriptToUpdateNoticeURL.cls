/*
    This Class will call from PB "Booking Unit Requested Price" To update Notice URL in Arabic when Nationality of cx is Arabic
*/
public with sharing class ScriptToUpdateNoticeURL {
    // pb = Handover_Notice_URLs__c != null && Handover_Notice_URLs__c isChanged && Account_Nationality = Arabic
    @InvocableMethod
    public Static void updateBuWithArabicUrl( List<Id> lstBuId ) {
        if( lstBuId != null  && lstBuId.size() > 0 ) {
            Set<String> arabicCountriesSet = new Set<String> {'Lebanese', 'Libyan', 'Kazak'
                                                                    , 'Kuwaiti', 'Qatari', 'UAE'
                                                                    , 'Omani', 'Jordanian', 'Iraqi'
                                                                    , 'Iranian', 'Egyptian', 'Saudi'
                                                                    , 'Bahraini', 'Moroccan'};    
            List<Booking_Unit__c> lstBuToUpdate = new List<Booking_Unit__c>();

            String url1 = 'Notice of Completion and Possession for your property in';
            String url2 = 'Statement of account for your property';
            String url3 = 'Statement of account for Owners Association expenses';
            String url4 = 'Handover Pack';
            String url5 = 'Service Charge Invoice';
            String url6 = 'Mortgage Info';
            String url7 = 'Handover Checklist';

            String arabicUrl1 = 'إشعار الإنجاز وملكية عقارك';
            String arabicUrl2 = 'كشف حساب عقارك';
            String arabicUrl3 = 'كشف حساب نفقات اتحاد الملاك';
            String arabicUrl4 = 'حزمة التسليم';
            String arabicUrl5 = 'فاتورة رسوم الخدمة';
            String arabicUrl6 = 'معلومات عن الرهن العقاري';
            String arabicUrl7 = 'قائمة التحقق من تسليم العقار';

            for(Booking_Unit__c objBu : [Select 
                                            Id
                                            , Handover_Notice_URLs__c
                                            , Handover_Notice_URLs_Clone__c
                                            , Booking__r.Account__r.Nationality__pc

                                        from 
                                            Booking_Unit__c 
                                        where 
                                            Id IN: lstBuId
                                            AND Booking__r.Account__r.Nationality__pc != null ]) {
                    
                if( String.isNotBlank(objBu.Handover_Notice_URLs__c) && objBu.Booking__r.Account__r.Nationality__pc != null
                &&  arabicCountriesSet.contains(objBU.Booking__r.Account__r.Nationality__pc)) {
                    String tempHOUrl = objBu.Handover_Notice_URLs__c;
                    objBu.Handover_Notice_URLs_Clone__c = objBu.Handover_Notice_URLs__c;
                    if(objBu.Handover_Notice_URLs__c.contains(url1)) {
                        tempHOUrl = tempHOUrl.replace(url1, arabicUrl1);
                    }
                    if(objBu.Handover_Notice_URLs__c.contains(url2)) {
                        tempHOUrl = tempHOUrl.replace(url2, arabicUrl2);
                    }
                    if(objBu.Handover_Notice_URLs__c.contains(url3)) {
                        tempHOUrl = tempHOUrl.replace(url3, arabicUrl3);
                    }
                    if(objBu.Handover_Notice_URLs__c.contains(url4)) {
                        tempHOUrl = tempHOUrl.replace(url4, arabicUrl4);
                    }
                    if(objBu.Handover_Notice_URLs__c.contains(url5)) {
                        tempHOUrl = tempHOUrl.replace(url5, arabicUrl5);
                    }
                    if(objBu.Handover_Notice_URLs__c.contains(url6)) {
                        tempHOUrl = tempHOUrl.replace(url6, arabicUrl6);
                    }
                    if(objBu.Handover_Notice_URLs__c.contains(url7)) {
                        tempHOUrl = tempHOUrl.replace(url7, arabicUrl7);
                    }
                    objBu.Handover_Notice_URLs__c = tempHOUrl;

                    lstBuToUpdate.add(objBu);
                }

            }

            system.debug('lstBuToUpdate = ' + lstBuToUpdate);
            if(lstBuToUpdate != null && lstBuToUpdate.size() > 0 ) {
                update lstBuToUpdate;
            }

        }
    }
}