public class MyPaymentsController {

    public Id personAccountId{get; set{
        personAccountId = value;
        BookingUnitList();
    }}
    public Id UnitId{get; set{
        UnitId = value;
        BookingUnitList();
    }}
    public transient List<Booking_Unit__c> bookingUnitList {get;set;}
    public transient List<String> fieldLabels {get;set;}
    public transient List<String> fieldNames {get;set;}

    public MyPaymentsController() {
        bookingUnitList = new List<Booking_Unit__c>();
        fieldLabels = new List<String>();
        fieldNames = new List<String>();
        //bookingUnitList();
        FieldLabels();
        FieldNames();
    }
    public void bookingUnitList() {
         Map<String,Booking_Unit_Active_Status__c> mapBookingUnitStatus = Booking_Unit_Active_Status__c.getAll();
          SET<String> keys = mapBookingUnitStatus.keyset();
        bookingUnitList = new List<Booking_Unit__c>();
        if (personAccountId == NULL) {
            return;
        }
        String paymentTermFields = '';
        for(Schema.FieldSetMember fieldMember : Schema.SObjectType.Payment_Terms__c.fieldSets.My_Payment_FieldSet.getFields()) {
            if(paymentTermFields != '')
                paymentTermFields += ',';
            paymentTermFields += fieldMember.getFieldPath();
        }
        String query = 'SELECT Id, Name, Unit_Name__c, Registration_ID__c, ' +
                        '(SELECT ' + paymentTermFields + ' FROM Payment_Terms_del__r) FROM Booking_Unit__c WHERE Booking__r.Account__c = :personAccountId AND Registration_Status__c IN :keys';
        bookingUnitList = (List<Booking_Unit__c>)Database.query(query);
        //return bookingUnitList;
    }
    public void FieldLabels() {
        List<String> fieldLabelList = new List<String>();
        for(Schema.FieldSetMember fieldMember : Schema.SObjectType.Payment_Terms__c.fieldSets.My_Payment_FieldSet.getFields())
            fieldLabelList.add(fieldMember.getLabel());
        //return fieldLabelList;
    }
    public void FieldNames() {
        List<String> fieldNameList = new List<String>();
        for(Schema.FieldSetMember fieldMember : Schema.SObjectType.Payment_Terms__c.fieldSets.My_Payment_FieldSet.getFields())
            fieldNameList.add(fieldMember.getFieldPath());
        //return fieldNameList;
    }

    @RemoteAction
    public static MileStonePaymentDetailsWrapper.MileStonePaymentDetails retrievePaymentTerms(String registrationId) {
        return AOPTMQService.getMilestonePaymentDetails(registrationId);
    }

}