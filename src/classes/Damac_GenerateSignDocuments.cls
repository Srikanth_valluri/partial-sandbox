public class Damac_GenerateSignDocuments implements Queueable, Database.allowsCallouts {
    
    Set <ID> uDocIds = new Set <ID> ();
    public Damac_GenerateSignDocuments (Set <ID> unitDocIds) {
        uDocIds = unitDocIds;
    }
    public void execute(QueueableContext context) {
        Signnow_credentials__c credentials = Signnow_credentials__c.getInstance(UserInfo.getUserId());
        String accessToken = Damac_SignNowAPI.authenticate(credentials);
        FFAorAADocuments (accessToken);
        
    }
    public void FFAorAADocuments (String accessToken) {
        Map <ID, Map <String, String>> srUnitDocs = new Map <ID, Map <String, String>> ();
        Map <ID, Unit_Documents__c> unitDocuments = new Map <ID, Unit_Documents__c> ([SELECT Document_Name__c, Service_Request__c,
                                                                                      Service_Request__r.Name,      
                                                                                      Service_Request__r.Agency_Type__c,
                                                                                            Service_Request__r.NSIBPM__Customer__c,
                                                                                            Service_Request__r.NSIBPM__Internal_SR_Status__r.Name,
                                                                                            Service_Request__r.Agency_Email__c 
                                                                                    FROM Unit_Documents__c
                                                                                    WHERE ID IN: uDocIds]);
                                                                                    //AND Service_Request__r.NSIBPM__Internal_SR_Status__r.Name = 'Awaiting FFA/AA']);
        
        
        Set <ID> customerIds = new Set <ID> ();
        for (Unit_documents__c doc :unitDocuments.values ()) {
            if (doc.Service_Request__r.NSIBPM__Customer__c != null)
                customerIds.add (doc.Service_Request__r.NSIBPM__Customer__c);
        }
        
        Map <ID, String> corporateEmails = new map <Id, String> ();
        for (Contact con : [SELECT AccountId, Email FROM Contact WHERE AccountId IN :customerIds AND Authorised_Signatory__c = TRUE]) {
            corporateEmails.put (con.AccountId, con.Email);
        }
        Map<String, List<Agent_Sign_Positions__mdt>> signer1Positions = new Map<String, List<Agent_Sign_Positions__mdt>>();
        for(Agent_Sign_Positions__mdt mtd : [SELECT MasterLabel, Document_Name__c, Page_Number__c,Signer__c, Type__c,
                                                                      X_Position__c, Y_Position__c, Width__c, Height__c,Required__c
                                                               FROM Agent_Sign_Positions__mdt
                                                               WHERE Signer__c = '1' ORDER BY Page_Number__c]) {
                                                               
             if(signer1Positions.containsKey(mtd.Document_Name__c)) {
                 signer1Positions.get(mtd.Document_Name__c).add(mtd);
             } else{                                               
                 signer1Positions.put(mtd.Document_Name__c, new List<Agent_Sign_Positions__mdt>{mtd});   
             }                                                      
        }
        
                                                                                                          
        for (Attachment att : [SELECT Name, Body, parentId FROM Attachment WHERE ParentId IN: uDocIds]) {
            //if (unitDocuments.get (att.parentId).Service_Request__r.NSIBPM__Internal_SR_Status__r.Name == 'Awaiting FFA/AA') {
                String docuName = unitDocuments.get (att.parentId).Document_Name__c;
                
                String signer1Email = unitDocuments.get (att.parentId).Service_Request__r.Agency_Email__c;
                if (unitDocuments.get (att.ParentId).Service_Request__r.Agency_Type__c == 'Corporate') {
                    if (corporateEmails.containsKey (unitDocuments.get (att.ParentId).Service_Request__r.NSIBPM__Customer__c)) {
                        signer1Email = corporateEmails.get (unitDocuments.get (att.ParentId).Service_Request__r.NSIBPM__Customer__c);
                    }
                }
                
                System.debug(':::DocumentName::::'+unitDocuments.get (att.parentId).Document_Name__c);
                List<Agent_Sign_Positions__mdt> signersPositions = signer1Positions.get(docuName);
                Signnow_credentials__c credentials = Signnow_credentials__c.getInstance(UserInfo.getUserId());
                
                String documentId = Damac_SignNowAPI.uploadAttachment(credentials, att, accessToken, null, signersPositions );
                
                List <String> documentIds = new List <String> ();
                documentIds.add (documentId);
                
                Map <String, HttpResponse> res = new Map <String, HttpResponse> ();
               
                res = Damac_SignNowAPI.sendInvitieToSign (credentials, documentIds, accessToken, 
                                                          signer1Email , 
                                                          '', '', 
                                                          unitDocuments.get (att.parentId).Service_Request__r.Name+'-'+att.Name);

                System.debug (res.get (documentId).getBody());
                
                if (res.get(documentId).getStatusCode () == 200) {
                    String docName = unitDocuments.get (att.ParentId).Document_Name__c.split (' ')[0];
                    Map <String, String> signNowDocumentIds = new Map <String, String> ();
                    if (srUnitDocs.containsKey (unitDocuments.get (att.parentId).Service_Request__c))
                        signNowDocumentIds = srUnitDocs.get (unitDocuments.get (att.parentId).Service_Request__c);
                        
                    signNowDocumentIds.put (docName, documentId);
                    
                    srUnitDocs.put (unitDocuments.get (att.parentId).Service_Request__c, signNowDocumentIds);
                }
           // }
        }
        //AND Service_Request__r.NSIBPM__Internal_SR_Status__r.Name = 'Awaiting FFA/AA']) {
        List <ESign_Details__c> eSignRecords = new List <ESign_Details__c> ();
        for (Unit_Documents__c doc :[SELECT Document_Name__c, Service_Request__c FROM Unit_Documents__c WHERE Service_Request__c IN : srUnitDocs.keySet () 
                                    AND Status__c = 'Pending Upload' ]) {
                                    
            if (srUnitDocs.containsKey(doc.Service_Request__c)) {
                
                Map <String, String> documentNames = srUnitDocs.get (doc.Service_Request__c);
                for (String key : documentNames.keySet()) {
                    if (doc.Document_Name__c.startsWith (key)) {
                        ESign_Details__c sDoc = new ESign_Details__c ();
                        sDoc.Document_Id__c = documentNames.get (key);
                        //sDoc.To_Email__c = documentNames.get (key).split ('###')[1];
                        sDoc.Unit_Documents__c = doc.Id;
                        sDoc.Status__c = 'Email Sent';
                        eSignRecords.add (sDoc);
                    }
                }                      
            }
        }
        if (eSignRecords.size () > 0) {
            Database.insert(eSignRecords, false);
        }
    }
}