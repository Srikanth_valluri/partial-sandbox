public without sharing class FmcGatePassRequestController extends FMGatePassProcessController {

    public  List<SelectOption>  lstUnit             {get; set;}
    public  Integer             attchmentIndex      {get; set;}

    public  Integer             attachCounter = 0;

    public FmcGatePassRequestController() {
        super(false);
        if (FmcUtils.isCurrentView('GatePassRequest')) {
            strSelectedUnit = '';
            instanceList = new List<FM_Case__c>();
            minimumOutstandingCharge = 0;
            valueListOfDrawings = new List<SelectOption>();
            valuesOfDrawings = new List<String>();
            isEdittable = true;
            valueList = new SelectOption[0];
            values = new String[0];
            isMandatory = false;
            strAccountId = CustomerCommunityUtils.customerAccountId;
            strFMCaseId = ApexPages.currentPage().getParameters().get('id');
            strSRType = 'Gate_Pass_Request';

            contractorConsultantSuccessMessage += '<br/><br/>An email with a link for uploading the'
                                                +' below required documents is send to the email id'
                                                +' provided above for contractor/ consultant. Please'
                                                +' inform the contractor/ consultant about the same.'
                                                +' <br/><br/>Please note that the request will be'
                                                +' submitted only once all mandatory details and'
                                                +' documents are uploaded.';

            FM_process__mdt fmProcessMetadata = [ SELECT Minimum_Outstanding_Charge__c
                                                  FROM FM_process__mdt
                                                  WHERE DeveloperName = 'Gate_Pass_Request' ];
            if (fmProcessMetadata != NULL) {
                minimumOutstandingCharge = fmProcessMetadata.Minimum_Outstanding_Charge__c != NULL
                    ? fmProcessMetadata.Minimum_Outstanding_Charge__c : 0;
            }

            if (String.isNotBlank(strFMCaseId)) {
                objFmCase = FM_Utility.getCaseDetails(strFMCaseId);
                if (objFmCase != NULL && objFmCase.Id != NULL) {
                    strSelectedUnit = objFmCase.Booking_Unit__c;
                    instanceList = FM_Utility.getExistingFMCase(
                                        'Work_Permit', strAccountId, strSelectedUnit );
                    System.debug('instanceList===='+instanceList);
                    if ((String.isNotBlank(strSelectedUnit) || String.isNotBlank(strFMCaseId))
                                && instanceList.isEmpty() ) {
                        init();
                    }
                    //System.debug('Outstanding_service_charges__c = ' + objFMCase.Outstanding_service_charges__c);

                    objUnit = getUnitDetails(strSelectedUnit);

                    if (objUnit != NULL) {
                        List<Location__c> lstLocation = new List<Location__c>();
                        lstLocation = [ SELECT As_Built_Drawing_Fee__c
                                              , Long_Duration_Gate_Pass_Fee__c
                                              , Short_Duration_Gate_Pass_Fee__c
                                        FROM Location__c
                                        WHERE Id = :objUnit.Inventory__r.Building_Location__c ];
                        if (!lstLocation.isEmpty()) {
                            System.debug('lstLocation = ' + lstLocation);
                            longDurationGatePassFee = !String.isBlank(String.valueof(
                                    lstLocation[0].Long_Duration_Gate_Pass_Fee__c))
                                    ? String.valueof(lstLocation[ 0].Long_Duration_Gate_Pass_Fee__c) : '0';
                            shortDurationGatePassFee = !String.isBlank(String.valueof(
                                    lstLocation[0].Short_Duration_Gate_Pass_Fee__c ))
                                    ? String.valueof(lstLocation[0].Short_Duration_Gate_Pass_Fee__c) : '0';
                            System.debug('longDurationGatePassFee = ' + longDurationGatePassFee);
                            System.debug('shortDurationGatePassFee = ' + shortDurationGatePassFee);
                        }
                    }
                    if (objUnit != NULL) {
                        try {
                            FmIpmsRestServices.DueInvoicesResult objResponse =
                                    FmIpmsRestServices.getDueInvoices( objUnit.Registration_Id__c,
                                                        '',objUnit.Inventory__r.Property_Code__c );
                          System.debug('== objResult =='+objResponse);
                          if(objResponse != NULL) {
                            objFMCase.Outstanding_service_charges__c = !String.isBlank(
                                    objResponse.totalDueAmount) ? objResponse.totalDueAmount : '0';
                          }
                        }
                        catch( Exception e ) {
                            System.debug( '----Exception while fetching oustanding balance----'
                                                                                +e.getMessage() );
                        }
                    }

                    lstUnit = new List<SelectOption> {
                        new SelectOption(objFmCase.Booking_Unit__c, objUnit.Unit_Name__c, true)};
                } else {
                    lstUnit = getUnitOptions();
                }
            } else {
                List<FM_Case__c> lstFmCase = [ SELECT Id
                                                     , Name
                                               FROM FM_Case__c
                                               WHERE CreatedById = :UserInfo.getUserId()
                                               AND Origin__c = 'Portal'
                                               AND Request_Type__c = 'Gate Pass Request'
                                               AND Status__c IN (NULL, '', 'Draft Request', 'In Progress', 'New')
                                               ORDER BY Status__c
                                               LIMIT 1 ];
                if (lstFmCase.isEmpty()) {
                    objFMCase = new FM_Case__c();
                    lstUnit = getUnitOptions();
                } else {
                    strFMCaseId = lstFmCase[0].Id;
                    objFMCase = FM_Utility.getCaseDetails(strFMCaseId);

                    strSelectedUnit = objFmCase.Booking_Unit__c;
                    instanceList = FM_Utility.getExistingFMCase( 'Work_Permit', strAccountId, strSelectedUnit );
                    System.debug('instanceList===='+instanceList);
                    if ((String.isNotBlank(strSelectedUnit) || String.isNotBlank(strFMCaseId))
                                && instanceList.isEmpty() ) {
                        init();
                    }
                    //System.debug('Outstanding_service_charges__c = ' + objFMCase.Outstanding_service_charges__c);

                    objUnit = getUnitDetails(strSelectedUnit);

                    if (objUnit != NULL) {
                        List<Location__c> lstLocation = new List<Location__c>();
                        lstLocation = [ SELECT As_Built_Drawing_Fee__c
                                              , Long_Duration_Gate_Pass_Fee__c
                                              , Short_Duration_Gate_Pass_Fee__c
                                        FROM Location__c
                                        WHERE Id = :objUnit.Inventory__r.Building_Location__c ];
                        if (!lstLocation.isEmpty()) {
                            System.debug('lstLocation = ' + lstLocation);
                            longDurationGatePassFee = !String.isBlank(String.valueof(
                                    lstLocation[0].Long_Duration_Gate_Pass_Fee__c))
                                    ? String.valueof(lstLocation[0].Long_Duration_Gate_Pass_Fee__c) : '0';
                            shortDurationGatePassFee = !String.isBlank(String.valueof(
                                lstLocation[0].Short_Duration_Gate_Pass_Fee__c ))
                                ? String.valueof(lstLocation[0].Short_Duration_Gate_Pass_Fee__c) : '0';
                            System.debug('longDurationGatePassFee = ' + longDurationGatePassFee);
                            System.debug('shortDurationGatePassFee = ' + shortDurationGatePassFee);
                        }
                    }
                    if (objUnit != NULL) {
                        try {
                            FmIpmsRestServices.DueInvoicesResult objResponse =
                                    FmIpmsRestServices.getDueInvoices( objUnit.Registration_Id__c,
                                                        '',objUnit.Inventory__r.Property_Code__c );
                          System.debug('== objResult =='+objResponse);
                          if(objResponse != NULL) {
                            objFMCase.Outstanding_service_charges__c = !String.isBlank(
                                    objResponse.totalDueAmount) ? objResponse.totalDueAmount : '0';
                          }
                        }
                        catch( Exception e ) {
                            System.debug( '----Exception while fetching oustanding balance----'+e.getMessage() );
                        }
                    }

                    lstUnit = getUnitOptions();
                }
            }

            if (objUnit != NULL) {
                processDocuments();
            }

            System.debug('objFMCase = ' + objFMCase);
            System.debug('strSelectedUnit = ' + strSelectedUnit);

        }
    }

    protected override SR_Attachments__c setDocumentValidity(SR_Attachments__c doc) {
        doc.isValid__c = false;
        return doc;
    }

    protected override FM_Case__c setCaseOrigin(FM_Case__c fmCase) {
        fmCase.Origin__c='Portal';
        return fmCase;
    }

    public override void processDocuments() {
        //System.debug('objFMCase.Person_To_Collect__c-----'+objFMCase.Person_To_Collect__c);
        //System.debug('strSRType====='+strSRType);
        //System.debug('objFMCase.Duration_For_Gate_Pass__c-----'+objFMCase.Duration_For_Gate_Pass__c);
        if(String.isBlank(strSRType)){
            strSRType=ApexPages.currentPage().getParameters().get('SRType');
        }
        lstUploadedDocs = NULL;
        lstDocuments = NULL;
        Set<FM_Documents__mdt> listSpecificDocs = new Set<FM_Documents__mdt>();
        List<String> lstOfString = new List<String>();
        if (objFMCase != NULL) {
            if (objFMCase.Id == NULL) {
                try {
                    createMethod();
                } catch(Exception e) {
                    System.debug(e.getMessage());
                }
            }
            mapUploadedDocs = new map<String, SR_Attachments__c>();
            for ( SR_Attachments__c objAttach : fetchUploadedDocs()) {
                mapUploadedDocs.put(objAttach.Name, objAttach);
            }
            if( mapUploadedDocs != NULL && !mapUploadedDocs.isEMpty() ) {
                lstUploadedDocs = new list<SR_Attachments__c>();
                lstUploadedDocs.addAll(mapUploadedDocs.values());
            }
        }
        mapProcessDocuments = new Map<String, FM_Documents__mdt>();
        lstDocuments = new List<FM_Documents__mdt>();
        System.debug('strSRType======' + strSRType);
        System.debug('objUnit++++++' + objUnit);
        //System.debug('objUnit.Property_City__c = ' + objUnit.Property_City__c);
        for (FM_Documents__mdt objDocMeta : FM_Utility.getDocumentsList(
                                                strSRType, objUnit.Property_City__c)) {
            System.debug('objDocMeta = ' + objDocMeta);
            mapProcessDocuments.put(objDocMeta.DeveloperName, objDocMeta);
            System.debug('mapProcessDocuments = ' + mapProcessDocuments);
            System.debug('mapUploadedDocs = ' + mapUploadedDocs);
            if (mapUploadedDocs != NULL && !mapUploadedDocs.containsKey(objDocMeta.MasterLabel)) {
                lstDocuments.add( objDocMeta );
            }
            if (objDocMeta.Mandatory__c == true) {
                lstOfString.add('true');
            }
        }
        System.debug('lstDocuments+++++' + lstDocuments);
        if (lstOfString.size() > 0) {
            isMandatory=true;
        }
        for (FM_Documents__mdt ins:lstDocuments) {
           if (!mapUploadedDocs.keyset().contains(ins.Document_Name__c)) {
                if (objFMCase.Duration_For_Gate_Pass__c=='2 Days' && ins.Duration__c=='Short Term') {
                    listSpecificDocs.add(ins);
                }
                if ((objFMCase.Duration_For_Gate_Pass__c=='3 Months'
                            || objFMCase.Duration_For_Gate_Pass__c=='12 Months'
                            || objFMCase.Duration_For_Gate_Pass__c=='6 Months')
                                && ins.Duration__c=='Long Term') {
                    listSpecificDocs.add(ins);
                }
           }
        }
        lstDocuments.clear();
        lstDocuments.addAll(listSpecificDocs);
        System.debug('lstDocuments+++++' + lstDocuments);
    }

    public PageReference saveAsDraft() {
        createRequestForWorkPermit();
        String nextSteps = 'You can click on "Submit Request" button to submit your request.';
        ApexPages.addMessage(new ApexPages.message(
            ApexPages.Severity.INFO, 'Your request has been saved successfully.' + ' ' + nextSteps
        ));
        return NULL;
    }

    public PageReference submitRequest() {
        PageReference nextPage = submitRequestForWorkPermit();
        if (nextPage != NULL) {
            String unitId = nextPage.getUrl().substringAfterLast('/');
            nextPage = new PageReference(ApexPages.currentPage().getUrl());
            nextPage.getParameters().clear();
            nextPage.getParameters().put('view', 'CaseDetails');
            nextPage.getParameters().put('id', unitId);
            nextPage.setRedirect(true);
        }
        System.debug('nextPage = ' + nextPage);
        return nextPage;
    }

    public void selectUnit() {
        System.debug('strSelectedUnit = ' + strSelectedUnit);
        instanceList = FM_Utility.getExistingFMCase('Work_Permit', strAccountId, strSelectedUnit);
        System.debug('instanceList = ' + instanceList);


        if (!instanceList.isEmpty()) {
            objFMCase = new FM_Case__c();
            objFMCase.Account__c = instanceList[0].Account__c;
            objFMCase.status__c ='Draft Request';
            objFMcase.origin__c ='Portal';
            objFMCase.Booking_Unit__c = instanceList[0].Booking_unit__c;
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName()
                                        .get('Gate Pass Request').getRecordTypeId();
            objFMCase.Request_Type_DeveloperName__c = strSRType;
            objFMCase.Request_Type__c = 'Gate Pass Request';
            objFMCase.Description__c = instanceList[0].Description__c;
            objFMCase.Person_To_Collect__c = instanceList[0].Person_To_Collect__c;
            objFMCase.Contact_person__c = instanceList[0].Contact_person__c;
            objFMCase.Contact_person_contractor__c = instanceList[0].Contact_person_contractor__c;
            objFMCase.Mobile_no__c = instanceList[0].Mobile_no__c;
            objFMCase.Mobile_no_contractor__c = instanceList[0].Mobile_no_contractor__c;
            objFMCase.Contact_Email__c = instanceList[0].Contact_Email__c;
            objFMCase.Office_tel__c = instanceList[0].Office_tel__c;
            objFMCase.Duration_For_Gate_Pass__c = instanceList[0].Duration_For_Gate_Pass__c;
            objFMCase.Office_tel_contractor__c = instanceList[0].Office_tel_contractor__c;
            objFMCase.Mobile_Country_Code__c = instanceList[0].Mobile_Country_Code__c;
            objFMCase.Mobile_Country_Code_2__c = instanceList[0].Mobile_Country_Code_2__c;
            objFMCase.Mobile_Country_Code_3__c = instanceList[0].Mobile_Country_Code_3__c;
            objFMCase.Mobile_Country_Code_4__c = instanceList[0].Mobile_Country_Code_4__c;
            objFMCase.Parent_case__c = instanceList[0].Id;
            objFMCase.Email__c = instanceList[0].Email__c;
            objFMCase.Email_2__c = instanceList[0].Email_2__c;
            objUnit = getUnitDetails(strSelectedUnit);

            if (objUnit != NULL) {
                List<Location__c> lstLocation = new List<Location__c>();
                lstLocation = [ SELECT As_Built_Drawing_Fee__c
                                      , Long_Duration_Gate_Pass_Fee__c
                                      , Short_Duration_Gate_Pass_Fee__c
                                FROM Location__c
                                WHERE Id = :objUnit.Inventory__r.Building_Location__c ];
                if (!lstLocation.isEmpty()) {
                    System.debug('lstLocation = ' + lstLocation);
                    longDurationGatePassFee = !String.isBlank(String.valueof(
                                lstLocation[0].Long_Duration_Gate_Pass_Fee__c))
                                ? String.valueof(lstLocation[0].Long_Duration_Gate_Pass_Fee__c) : '0';
                    shortDurationGatePassFee = !String.isBlank(String.valueof(
                                lstLocation[0].Short_Duration_Gate_Pass_Fee__c ))
                                ? String.valueof(lstLocation[0].Short_Duration_Gate_Pass_Fee__c) : '0';
                    System.debug('longDurationGatePassFee = ' + longDurationGatePassFee);
                    System.debug('shortDurationGatePassFee = ' + shortDurationGatePassFee);
                }
            }
            if (objUnit != NULL) {
                try {
                    FmIpmsRestServices.DueInvoicesResult objResponse =FmIpmsRestServices.getDueInvoices( objUnit.Registration_Id__c,'',objUnit.Inventory__r.Property_Code__c );
                  System.debug('== objResult =='+objResponse);
                  if(objResponse != NULL) {
                    objFMCase.Outstanding_service_charges__c = !String.isBlank(
                                    objResponse.totalDueAmount) ? objResponse.totalDueAmount : '0';
                  }
                }
                catch( Exception e ) {
                    System.debug( '----Exception while fetching oustanding balance----'+e.getMessage() );
                }
            }
        }

        if ((String.isNotBlank(strSelectedUnit)
            || String.isNotBlank(strFMCaseId))
            && instanceList.isEmpty()
            /* && continueInit*/
        ) {
            init();
        }
    }

    private static List<SelectOption> getUnitOptions() {
        List<SelectOption> lstOptions = new List<SelectOption>{new SelectOption('', 'Select', true)};
        for (Booking_Unit__c unit : FmcUtils.queryUnitsForAccount(
                    CustomerCommunityUtils.customerAccountId, 'Id, Name, Unit_Name__c' )) {
            lstOptions.add(new SelectOption(unit.Id, unit.Unit_Name__c));
        }
        return lstOptions;
    }

    public void removeDocument() {
        System.debug('===attchmentIndex======' + attchmentIndex);
        if(attchmentIndex != NULL) {
            lstDocuments.remove(attchmentIndex);
        }
    }

    public void addDocument() {
        attachCounter++;
        String attName = 'Attachment '+ attachCounter;
        lstDocuments.add(new FM_Documents__mdt(Document_Name__c=attName, Mandatory__c=false));
        // showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
    }
}