/**********************************************************************************************************************
Class Name : DAMACTrainingUtils 
Description: util class for APIs related to Agent Training.
Test Class : DAMAC_TrainingServicesTest
========================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.1     |   21-01-20201     | Subin C Antony	  | Modification: Bug fix: disable showing all subjects in response for trainings. 
		|					|					  | Subjects related to available training slots are only to be returned.
***********************************************************************************************************************/

global class DAMACTrainingUtils {
    
    global static RestContextHandler getAgencies(){
        RestContextHandler handler = new RestContextHandler(true);   
        list<Account> agencies = [Select id, name from Account where RecordType.name = 'Corporate Agency'];
        handler.response.data = agencies;
        handler.response.success = true;
        handler.response.message = '';
        return handler;
    }

    global static RestContextHandler getTrainings(){ 
        
        RestContextHandler handler = new RestContextHandler(true); 
        
        Set<String> subjectIDs = new Set<String>();
        /*  commented on 21 JAN 2021 to fix bug(subjects being listed without available training slots). Placed this block further below in code. */
        /*list<Subject> subjects = new list<Subject>();
        for(Subject__c sub :[select id, name, Duration__c from Subject__c]){
            subjects.add(new Subject(sub.Id, sub.name, sub.Duration__c));
        }*/
         
        list<Training__c> trainings = [Select id, name, subject__c, Name__c, Date_of_Training__c, App_Message__c, App_Color_Code__c,
                                        From_Time_Slot__c, To_Time_Slot__c, Subject__r.name, subject__r.Duration__c  
                                        from Training__c 
                                        where Date_of_Training__c >= TODAY and Ready_for_bookings__c = true
                                        order by Date_of_Training__c asc];
        map<Date, set<Id>> possibleDates = new map<Date, set<Id>>();
        list<Training> trainingRecs = new list<training>();
        for(Training__c trg : trainings){
            //possibleDates.add(trg.Date_of_Training__c);
            subjectIDs.add(trg.subject__c); // added on 21 JAN 2021 to fix bug(subjects being listed without available training slots) 
            
            if(!possibleDates.containsKey(trg.Date_of_Training__c))
                possibleDates.put(trg.Date_of_Training__c,new set<ID>{trg.Subject__c});
            else{
                set<ID> existing = possibleDates.get(trg.Date_of_Training__c);
                existing.add(trg.Subject__c);
                possibleDates.put(trg.Date_of_Training__c,existing);
            } 
            
            trainingRecs.add(new Training(trg.Id, trg.subject__c, trg.From_Time_Slot__c, trg.To_Time_Slot__c,
                                          trg.Date_of_Training__c, trg.Subject__r.name, trg.Subject__r.Duration__c,
                                         trg.App_Message__c, trg.App_Color_Code__c));
        }
        
        list<DateSlots> slots = new list<DateSlots>();
        for(date dt : possibleDates.keyset()){
            slots.add(new DateSlots(dt, possibleDates.get(dt)));
        }
        
        /* Added on 21 JAN 2021:  modified block to query subjects based on available slots: BEGIN */
        list<Subject> subjects = new list<Subject>();
        if(NULL != subjectIDs && subjectIDs.size() > 0) {
            for(Subject__c sub :[select id, name, Duration__c from Subject__c WHERE id IN :subjectIDs]){
                subjects.add(new Subject(sub.Id, sub.name, sub.Duration__c));
            }
        }/* Added on 21 JAN 2021:  modified block to query subjects based on available slots: END */
        
        handler.response.data = new TrainingData(subjects, slots, trainingRecs, describeField('Attendee__c', 'Mobile_country_code__c'));
        handler.response.success = true;
        handler.response.message = '';
        
        return handler;
    }
    
    global static list<String> describeField(String objectName, String fieldName) {
        Map<String, Schema.SObjectField> SObjectFieldMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        Schema.DescribeFieldResult describeFieldResult = SObjectFieldMap.get(fieldName).getDescribe();
        list<string> codes = new list<string>();
        for(Schema.PicklistEntry ple :describeFieldResult.getPicklistValues()){
            codes.add(ple.getValue());
        }
        return codes;
    }
    
    public class Attendee{
        public string firstName;
        public string lastName;
        public string countryCode;
        public string mobileNo;
        public string email;
        public string trainingId;
    }
    
    public class TrainingData{
        public list<subject> subjects;
        public list<DateSlots> subjectDates;
        public list<Training> trainings;
        public list<String> countryCodes;
        public string calendarURL;
        public string prospectURL;
        public TrainingData(list<subject> lst1, list<DateSlots> slots, list<Training> lst2, list<String> result){
            subjects = lst1;
            subjectDates = slots;
            trainings = lst2;
            countryCodes = result;
            calendarURL = System.label.Site_Label+''+PageReference.forResource('AA_Calendar').getUrl();
            prospectURL = System.label.Site_Label+''+PageReference.forResource('AA_Prospectus').getUrl();
        }
    }
    public class DateSlots{
        public date trainingDate;
        public set<ID> subjectIds;
        public DateSlots(Date dt, set<Id> subjects){
            subjectIds = subjects;
            trainingDate = dt;
        }
        
    }
    public class Training{
        public string recorId;
        public string subjectId;
        public string fromSlot;
        public string toSlot;
        public string subjectName;
        public string subjectDuration;
        public Date trainingDate;
        public string appMessage;
        public string appColorCode;
        public Training(string recId, string subId, string fromTime, string toTime, 
                            Date trgDate, string subjName, string subjDuration, string msg, string code){
            recorId = recId;
            subjectId = subId;
            fromSlot = fromTime;
            toSlot = toTime;
            trainingDate = trgDate;
            subjectName = subjName;
            subjectDuration = subjDuration;
            appMessage = msg;
            appColorCode = code;
        }
    }
    
    public class Subject{
        public string subjectId;
        public string subjectDuration;
        public string subjectName;
        public Subject(string recId, string name, string dur){
            subjectId = recId;
            subjectName = name;
            subjectDuration = dur;
        }
    }
    
    public static RestContextHandler createAttendees(string jsonString){
        RestContextHandler handler = new RestContextHandler(true);
        list<Attendee> attendees = (List<Attendee>) JSON.deserialize(jsonString, list<Attendee>.class);
        list<Attendee__c> lstAttendees = new list<Attendee__c>();
        User u = [Select id, accountId from User where id =: UserInfo.getUserId()];
        map<Id, integer> mpRequestedcount = new map<Id, integer>();
        for(Attendee attnd : attendees){
            mpRequestedcount.put(attnd.trainingId, (!mpRequestedcount.containsKey(attnd.trainingId) ? 1 : mpRequestedcount.get(attnd.trainingId)+1));
        }
        
        boolean bool = false;
        for(Training__c tr : [Select id, Slots_Available__c from Training__c where Id in: mpRequestedCount.keyset()]){
            if(tr.Slots_Available__c < mpRequestedCount.get(tr.Id))
                bool = true;  
        }
        
        
        for(Attendee attnd : attendees){
            Attendee__c att = new Attendee__c();
            att.First_name__c = attnd.firstName;
            att.Last_Name__c = attnd.lastName;
            att.Email__c = attnd.email;
            att.Mobile_country_code__c = attnd.countryCode;
            att.Mobile_Number__c = attnd.mobileNo;
            att.training__c = attnd.trainingId;
            att.Agency__c = u.accountId != null? u.accountId : null;
            att.Registration_Status__c = (bool ? 'Rejected' : 'Confirmed');
            lstAttendees.add(att);
        }
        if(!lstAttendees.isEmpty())
            insert lstAttendees;
        if(bool){
            handler.response.message = System.label.Training_ERROR;
            handler.response.data = '';
            handler.response.success = false;
            handler.response.statuscode = WebserviceResponse.STATUS_ALLOTMENT_ERROR;
        }
        else{
            handler.response.data = '';
            handler.response.success = true;
            handler.response.statusCode = WebserviceResponse.STATUS_CODE_SUCCESS;
            handler.response.message = System.label.Training_SUCCESS;
        }    
        return handler;
    }


}