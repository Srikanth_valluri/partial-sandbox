/**
 * @File Name          : ReplaceMergeFieldsOfEmailTemplate.cls
 * @Description        : 
 * @Author             : Dipika Rajput
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 7/9/2019, 11:28:57 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0                                  Dipika Rajput            Initial Version
**/
public without sharing class ReplaceMergeFieldsOfEmailTemplate {
    
    /*
    * @Description : This method is used to replace email content with proper field values
    * @Params      :  
    * @Return      : replaced value
    */
    public static String replaceHtmlContent(
        String emailTemplate, 
        String objectName, 
        String recordId
    ) {
        
        // Defined the regex for bind expression
        Pattern regex = Pattern.compile('\\{!([^}]*)\\}');
        Matcher regexMatcher = regex.matcher(emailTemplate);
        Set<String> tokenFields = new Set<String>();
        
        while(regexMatcher.find()){ 

            String bindVariableStr = regexMatcher.group();
            
            // Extract curly braces and exclamation marks
            String variable = bindVariableStr.substring(2,bindVariableStr.length()-1);
            
            //keep only object fields
            if(variable.startsWith(objectName + '.')){
                //remove self-referencing in fieldnames
                tokenFields.add(variable.replace(objectName + '.', ''));
            }
            
            //Build dynamic query
            String queryStr = 'SELECT Id,';
            for(String objFieldInstance : tokenFields){
                queryStr += objFieldInstance + ',';
            }
            //remove last ","
            queryStr = queryStr.substring(0, queryStr.length()-1);
            
           
            queryStr = queryStr + ' FROM ' + objectName + ' WHERE ID = :recordId';
            
            //Do the query
            List<SObject> objectRecordList = Database.query(
               queryStr
            );
            
            
            // For corresponding sObject record
                for(Sobject sobjInstnace : objectRecordList){
                    System.debug('tokenFields::::::::::'+tokenFields); 
                    //Replace values
                    for(String fieldName : tokenFields){
                        
                        emailTemplate = emailTemplate.replace(
                                '{!' + objectName + '.' + fieldName + '}',
                                getValue(sobjInstnace, fieldName)
                        );
                     }
                }   
            System.debug('replaced content in email template::::::::::'+emailTemplate); 
            }
            return emailTemplate;
        }
    
    /*
    * @Description : This method is used to return value by providing object and field name
    * @Params      :  
    * @Return      : replaced value
    */
    public static String getValue(SObject objectInstance, String fieldName){
        
        return (
            objectInstance.get(fieldName) != null) ?
            String.valueOf(objectInstance.get(fieldName)) :
            '';
    }
}