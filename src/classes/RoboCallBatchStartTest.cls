@isTest
public class RoboCallBatchStartTest {

    @isTest
    static void TestMethodwithCampId() {
        Robocall_Campaign__c objRbc = new Robocall_Campaign__c(Name = 'test', Campaign_Id__c = '1324');
        insert objRbc;

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account(LastName= 'Test Account',
                                      Phone = '1234567890',
                                      RecordtypeId = rtId,
                                      Phone_Country_Code__c = ' India: 0091',
                                      Mobile_Country_Code__c = 'India: 0091',
                                      Mobile_Phone_Encrypt__pc = '8149071353',
                                      Mobile_Phone_Encrypt__c = '1324567890',
                                      Phone_Encrypt__c = 'adswsfds',
                                      Mobile__c = '1234567890'
                                      );
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        //String fmAmount = String.valueOf(Decimal.valueOf(Label.Robo_Call_FM_Outstanding_Amount) + 100.00);
        Booking_unit__c objBU = new Booking_Unit__c (Booking__c = booking.Id,
                                                     Owner__c = account.Id,
                                                     FM_Outstanding_Amount__c = Label.Robo_Call_FM_Outstanding_Amount,
                                                     FM_Outstanding_Date__c = System.today(),
                                                     Unit_Name__c = 'Abc/10/1010',
                                                     Property_Name__c = 'xyz',
                                                     SC_From_RentalIncome__c = false,
                                                     CM_Units__c = NULL
                                                     , Registration_Status_Code__c = 'lxxa'
                                                     , Early_Handover__c = true
                                                     );
        insert objBU;

        Robocall_Campaign__c roboCampaign = new Robocall_Campaign__c(Campaign_Id__c = '5678',
                                                                     Priority__c = '7'
                                                                     );
        insert roboCampaign;

        Test.startTest();
        PageReference pgRef = Page.RoboCallBatchStartPage;
        Test.setCurrentPage(pgRef);
        //pgRef.getParameters().put('acct', objRbc);
        ApexPages.StandardController sc = new ApexPages.standardController(objRbc);

        RoboCallBatchStart obj = new RoboCallBatchStart(sc);
        obj.addCallsToCampaign();
        
        obj.placeRoboCall();
        Test.stopTest();
    }

    @isTest
    static void TestMethodwithoutCampId() {
        Robocall_Campaign__c objRbc = new Robocall_Campaign__c(Name = 'test');
        insert objRbc;

        Test.startTest();
        PageReference pgRef = Page.RoboCallBatchStartPage;
        Test.setCurrentPage(pgRef);
        //pgRef.getParameters().put('acct', objRbc);
        ApexPages.StandardController sc = new ApexPages.standardController(objRbc);

        RoboCallBatchStart obj = new RoboCallBatchStart(sc);
        obj.addCallsToCampaign();

        Test.stopTest();
    }
}