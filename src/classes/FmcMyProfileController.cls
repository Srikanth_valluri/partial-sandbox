public without sharing class FmcMyProfileController {

    public Account                          customerAccount     {get; set;}
    public Boolean                          isOwner             {get; set;}

    //public Integer                          vehicleCount        {get; set;}
    //public List<FM_Additional_Detail__c>    vehiclesDetails     {get; set;}
    //public List<FM_Additional_Detail__c>    emergencyContacts   {get; set;}
    //public List<FM_Additional_Detail__c>    petDetails          {get; set;}
    //public List<FM_Additional_Detail__c>    residentDetails     {get; set;}

    //public String                           detailType          {get; set;}
    //public Integer                          indexToRemove       {get; set;}


    public FmcMyProfileController() {
        if (FmcUtils.isCurrentView('Profile')) {
            initializeVariables();
        }
    }

    private void initializeVariables() {
        isOwner = FmcUtils.isOwner();

        //vehiclesDetails     = new List<FM_Additional_Detail__c>();
        //emergencyContacts   = new List<FM_Additional_Detail__c>();
        //petDetails          = new List<FM_Additional_Detail__c>();
        //residentDetails     = new List<FM_Additional_Detail__c>();

        if (CustomerCommunityUtils.customerAccountId != NULL) {
            customerAccount = [
                SELECT  Id
                        , Name
                        , Salutation
                        , SLA__c
                        , Nationality__c
                        , Nationality__pc
                        , Passport_Number__pc
                        , Passport_Number__c
                        , Passport_Expiry_Date__c
                        , Passport_Expiry_Date__pc
                        , PersonEmail
                        , Phone
                        , Organisation_Name__c
                        , Party_ID__c
                        , Customer_Type__c
                        , Email__c
                        , Email__pc
                        , Mobile_Phone_Encrypt__pc
                        , Mobile__c
                        , PersonMailingStreet
                        , PersonMailingPostalCode
                        , PersonMailingCity
                        , PersonMailingState
                        , PersonMailingCountry
                        , Address_Line_1__pc
                        , Address_Line_2__pc
                        , Address_Line_3__pc
                        , Address_Line_4__pc
                        , Address_Line_1__c
                        , Address_Line_2__c
                        , Address_Line_3__c
                        , Address_Line_4__c
                        , City__pc
                        , City__c
                        , Country__pc
                        , Country__c
                        , Party_Type__c
                        , Mobile_Phone_Encrypt_2__pc
                        , Mobile_Phone_Encrypt_3__pc
                        , Mobile_Phone_Encrypt_4__pc
                        , Mobile_Phone_Encrypt_5__pc
                        , Email_2__pc
                        , Email_3__pc
                        , Email_4__pc
                        , Email_5__pc
                        , Status__c
                        , Industry
                        , Designation__pc
                FROM    Account
                WHERE   Id = :CustomerCommunityUtils.customerAccountId
            ];
            /*for (FM_Additional_Detail__c details : [
                SELECT  Id
                        , RecordType.Name
                        , RecordType.DeveloperName
                        , Emergency_Contact_Case__c
                        , Resident_Case__c
                        , Vehicle_Case__c
                        , Vehicle_Number__c
                        , Name__c
                        , Relationship__c
                        , Phone__c
                        , Age__c
                        , Nationality__c
                        , Passport_Number__c
                        , Identification_Number__c
                        , Vehicle_Make_Model__c
                        , Vehicle_Colour__c
                        , Email__c
                        , Parking_Slot_Number__c
                        , People_of_Determination__c
                        , Date_of_Birth__c
                        , Gender__c
                        , Registration_Country__c
                        , Type_of_Vehicle__c
                        , Mobile__c
                        , Vehicle_Sticker_No__c
                        , Pet_Case__c
                        , Pet_Name__c
                        , Pet_Type__c
                        , Description_Of_Pet__c
                FROM    FM_Additional_Detail__c
                WHERE   Emergency_Contact_Account__c    = :CustomerCommunityUtils.customerAccountId
                        OR Pet_Account__c               = :CustomerCommunityUtils.customerAccountId
                        OR Resident_Account__c          = :CustomerCommunityUtils.customerAccountId
                        OR Vehicle_Account__c           = :CustomerCommunityUtils.customerAccountId
            ]) {
                switch on details.RecordType.Name {
                    when 'Vehicle' {
                        vehiclesDetails.add(details);
                    }
                    when 'Emergency Contact' {
                        emergencyContacts.add(details);
                    }
                    when 'Pet Details' {
                        petDetails.add(details);
                    }
                    when 'Resident' {
                        residentDetails.add(details);
                    }
                }
            }

            vehicleCount = vehiclesDetails.size();*/
        }
    }

    /*public void addDetails() {
        if(String.isNotBlank( detailType )) {
            FM_Additional_Detail__c details = new FM_Additional_Detail__c();
            details.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get(detailType).getRecordTypeId();
            if (detailType.equals('Emergency Contact')) {
                 details.Emergency_Contact_Account__c = customerAccount.Id;
                 emergencyContacts.add(details);
            } else if (detailType.equals('Vehicle')){
                 details.Vehicle_Account__c = customerAccount.Id;
                 vehiclesDetails.add(details);
            } else if (detailType.equals('Resident')){
                details.Resident_Account__c = customerAccount.Id;
                residentDetails.add(details);
            }
        }
    }

    public void removeDetails() {
        if (String.isNotBlank(detailType )) {
            switch on detailType {
                when 'Vehicle' {
                    if (vehiclesDetails.size() > 1) {
                        vehiclesDetails.remove(indexToRemove);
                    }
                }
                when 'Emergency Contact' {
                    if (emergencyContacts.size() > 1) {
                        emergencyContacts.remove(indexToRemove);
                    }
                }
                when 'Pet Details' {
                    if (petDetails.size() > 1) {
                        petDetails.remove(indexToRemove);
                    }
                }
                when 'Resident' {
                    if (residentDetails.size() > 1) {
                        residentDetails.remove(indexToRemove);
                    }
                }
            }
        }
    }*/

    @RemoteAction
    public static FmIpmsRestServices.DueInvoicesResult fetchDueInvoiceData() {
        return FmIpmsRestServices.getDueInvoices('', CustomerCommunityUtils.getPartyId(), '');
    }

    //@RemoteAction
    //public static void updateAdditionalContact(Map<String, String> mapAdditionalDetails) {
    //    Account customerAccount = new Account(Id = CustomerCommunityUtils.customerAccountId);
    //    for (String fieldName : mapAdditionalDetails.keySet()) {
    //        customerAccount.put(fieldName, mapAdditionalDetails.get(fieldName));
    //    }
    //    update customerAccount;
    //}

}