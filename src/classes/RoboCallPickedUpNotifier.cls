/**********************************************************************************************************************
Description: Class to notify CRE when robo call picked by customer
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     | 20-04-2020        | Aishwarya Todkar  | 1. Initial Draft
***********************************************************************************************************************/
public class RoboCallPickedUpNotifier {

/***********************************************************************************************************************
Description : Method to notify CRE when robo call picked by customer
Parameter(s): List of Added_Call__c IDs
Return Type : void
P.B         : Robo Call Picked Up Notification
************************************************************************************************************************/
    @InvocableMethod
    public static void notifyCre(List<Id> addedCallIds) {

        List<EmailMessage> emailList = new List<EmailMessage>();    

        for( Id callId : addedCallIds ) {
            SendEmail( callId );
        }

    }//End method

/***********************************************************************************************************************
Description : Send Email to CREs Via. SendGrid
Parameter(s): Added Call Id
Return Type : void
************************************************************************************************************************/
    @future( Callout = true )
    public static void SendEmail( Id callId) {

        List<Added_Call__c> lstAddCalls = [SELECT 
                                                Id
                                                , Calling_List__c
                                                , Calling_List__r.Name
                                                , Calling_List__r.Owner.Email
                                                , Calling_List__r.Owner.Name
                                                , Calling_List__r.Account__r.Name
                                            FROM 
                                                Added_Call__c
                                            WHERE 
                                                Id =: callId
                                            AND
                                                Calling_List__c != null
                                            AND
                                                Calling_List__r.Account__c != null ];

        System.debug('lstAddCalls: '+lstAddCalls);
        
        if( lstAddCalls != null && lstAddCalls.size() > 0 ) {

            Added_Call__c objAddCall = lstAddCalls[0];

            //Get Sendgrid Email details from custom metadata
            List<SendGrid_Email_Details__mdt> listSendGridMdt 
                = new List<SendGrid_Email_Details__mdt>( [SELECT
                                                            From_Address__c
                                                            , From_Name__c
                                                            , To_Address__c
                                                            , To_Name__c
                                                            , CC_Address__c
                                                            , CC_Name__c
                                                            , Bcc_Address__c
                                                            , Bcc_Name__c
                                                            , Reply_To_Address__c
                                                            , Reply_To_Name__c
                                                            , Email_Template__c
                                                        FROM
                                                            SendGrid_Email_Details__mdt
                                                        WHERE
                                                            MasterLabel = 'Robo call picked up notification'
                                                        AND
                                                            Email_Template__c != null
                                                        LIMIT 1
                                                        ] );
            System.debug('listSendGridMdt: '+listSendGridMdt);

            if( listSendGridMdt != null && listSendGridMdt.size() > 0) {
                
                SendGrid_Email_Details__mdt objSendGridMdt = listSendGridMdt[0];
                                    
                //Get Email Template
                List<EmailTemplate> listRoboCallEmailTemplate 
                    = new List<EmailTemplate>( [SELECT 
                                                    ID
                                                    , Subject
                                                    , Body
                                                    , HtmlValue
                                                    , TemplateType
                                                FROM 
                                                    EmailTemplate 
                                                WHERE 
                                                    Name =: objSendGridMdt.Email_Template__c
                                                Limit 1 ] );
                system.debug('listRoboCallEmailTemplate  === ' + listRoboCallEmailTemplate );
                    

                //Prepare SendGrid Email Details
                if( listRoboCallEmailTemplate != null && listRoboCallEmailTemplate.size() > 0 ) {

                    EmailTemplate objRoboCallEmailTemplate = listRoboCallEmailTemplate[0];

                    String toAddress = String.isNotBlank( objAddCall.Calling_List__r.Owner.Email ) ? objAddCall.Calling_List__r.Owner.Email : '';

                    String toName = String.isNotBlank( objSendGridMdt.To_Name__c ) ? objSendGridMdt.To_Name__c : '';

                    String ccAddress = String.isNotBlank( objSendGridMdt.CC_Address__c ) ? objSendGridMdt.CC_Address__c : '';

                    String ccName = String.isNotBlank( objSendGridMdt.CC_Name__c ) ? objSendGridMdt.CC_Name__c : '';

                    String bccAddress = String.isNotBlank( objSendGridMdt.Bcc_Address__c ) ? objSendGridMdt.Bcc_Address__c : '';

                    String bccName = String.isNotBlank( objSendGridMdt.Bcc_Name__c ) ? objSendGridMdt.Bcc_Name__c : '';

                    String fromAddress = String.isNotBlank( objSendGridMdt.From_Address__c ) ? objSendGridMdt.From_Address__c : '';

                    String fromName = String.isNotBlank( objSendGridMdt.From_Name__c ) ? objSendGridMdt.From_Name__c : '';

                    String replyToAddress = String.isNotBlank( objSendGridMdt.Reply_To_Address__c ) ? objSendGridMdt.Reply_To_Address__c : '';

                    String replyToName = String.isNotBlank( objSendGridMdt.Reply_To_Name__c ) ? objSendGridMdt.Reply_To_Name__c : '';

                    String contentType = 'text/html';

                    String contentValue = replaceMergeFields( objRoboCallEmailTemplate.HtmlValue, objAddCall );

                    String contentBody = replaceMergeFields( objRoboCallEmailTemplate.Body, objAddCall );

                    String subject = replaceMergeFields( objRoboCallEmailTemplate.Subject, objAddCall );

                    List<Attachment> lstAttach = new List<Attachment>();
                    
                    system.debug('toAddress === ' + toAddress);
                    system.debug('fromAddress === ' + fromAddress);
                    system.debug('contentValue === ' + contentValue);
                    system.debug('ccAddress === ' + ccAddress);

                    //Call method to send an email via SendGrid
                    if( String.isNotBlank( toAddress ) && String.isNotBlank( fromAddress ) ) {
                                
                        SendGridEmailService.SendGridResponse objSendGridResponse = 
                            SendGridEmailService.sendEmailService(
                                                        toAddress, ''
                                                        , ccAddress, ''
                                                        , bccAddress, ''
                                                        , subject, ''
                                                        , fromAddress, ''
                                                        , replyToAddress, ''
                                                        , contentType
                                                        , contentValue, ''
                                                        , lstAttach );
                        
                        system.debug('objSendGridResponse === ' + objSendGridResponse);
                        
                        String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                        System.debug('responseStatus== ' + responseStatus);

                        //Create reportable activity 
                        if (responseStatus == 'Accepted') {
                            EmailMessage mail = new EmailMessage();
                            mail.Subject = subject;
                            mail.MessageDate = System.Today();
                            mail.Status = '3';//'Sent';
                            mail.RelatedToId = objAddCall.Id;
                            mail.Type__c = 'Robo call picked up notification';
                            mail.ToAddress = toAddress;
                            mail.FromAddress = fromAddress;
                            mail.TextBody = contentBody;//contentValue.replaceAll('\\<.*?\\>', '');
                            mail.Sent_By_Sendgrid__c = true;
                            mail.SentGrid_MessageId__c = objSendGridResponse.MessageId;
                            mail.Calling_List__c = objAddCall.Calling_List__c;
                            mail.CcAddress = ccAddress;
                            mail.BccAddress = bccAddress;
                            insert mail;
                            System.debug('Mail obj == ' + mail);
                        }
                    } //End Address if
                } // END listRoboCallEmailTemplate if
            }// End listSendGridMdt if
        }// End lstAddCalls if
    }//End of Method

/***********************************************************************************************************************
Description : Replaces merge fields with actual values
Parameter(s): Contents, Added Call instance
Return Type : String
************************************************************************************************************************/
    public Static String replaceMergeFields( String strContents, Added_Call__c objAddCall ) {
        if( strContents.contains( '{!Calling_List__c.Name}' ) ) {
            strContents = strContents.replace( '{!Calling_List__c.Name}', objAddCall.Calling_List__r.Name );
        }

        if( strContents.contains( '{!Calling_List__c.OwnerFullName}' ) ) {
            strContents = strContents.replace( '{!Calling_List__c.OwnerFullName}', objAddCall.Calling_List__r.Owner.Name );
        }

        if( strContents.contains( '{!Account.Name}' ) ) {
            strContents = strContents.replace( '{!Account.Name}', objAddCall.Calling_List__r.Account__r.Name );
        }

        if( strContents.contains( '{!Calling_List__c.Id}' ) ) {
            strContents = strContents.replace( '{!Calling_List__c.Id}', objAddCall.Calling_List__c );
        }

        return strContents;
    }
}//End Of Class