/************************************************************************************************
 * @Name              : DAMAC_SubmitBuyerDetails_API_Test
 * @Description       : Test Class for DAMAC_SubmitBuyerDetails_API
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         03/08/2020       Created
***********************************************************************************************/
@isTest(seeAllData = true) 
public class DAMAC_SubmitBuyerDetails_API_Test{
   
    @isTest
    static void testAPI(){
        list<AggregateResult> result = [SELECT count(id), Booking__c bookId 
                                            from Buyer__c 
                                            group by Booking__c 
                                            HAving count(id)>2
                                            limit 1];
        string bookingId = string.valueOf(result[0].get('bookId'));
        Booking__c book = [Select id, deal_SR__C from booking__c where id=:bookingId  limit 1];
        list<Buyer__c> buyers = [Select id, name, primary_Buyer__c from Buyer__c where Booking__c =: book.Id];
        string primaryBuyerId = '';
        string jointBuyerId = '';
        for(Buyer__c buy : buyers){
            if(buy.Primary_buyer__c)
                primaryBuyerId = buy.Id;
            else 
                jointBuyerId = buy.Id;
        }
        User portalUser = [select id from User 
                                    where AccountId != null and profile.name = 'Customer Community - Super User' and isactive = true limit 1];    
        System.runAs(portalUser){
            Test.startTest();
                RestRequest request = new RestRequest();
                RestResponse res = new RestResponse();
                request.requestUri ='/submitBuyerDetails';
                request.requestBody = Blob.valueOf('{"PrimaryBuyerUnitDoc":[{"Booking__c":"'+book.Id+'","Status__c":"Pending Upload","Service_Request__c":"'+book.Deal_SR__c+'","Document_Name__c":"Passport"},{"Booking__c":"'+book.Id+'","Status__c":"Pending Upload","Service_Request__c":"'+book.Deal_SR__c+'","Document_Name__c":"Entry Stamp"}],"jointBuyerList":[{"jointBuyer":{"Booking__c":"'+book.Id+'","DOB__c":"1988-01-26","Damac_Agent_Action_Type__c":"Saved as Draft","Id":"'+jointBuyerId+'","Passport_Expiry__c":"2028-03-31","Primary_Buyer__c":false},"UnitDoc":[]}],"primaryBuyer":{"Damac_Agent_Action_Type__c":"Saved as Draft","Primary_Buyer__c":true,"Id":"'+primaryBuyerId+'"}}');
                request.httpMethod = 'POST';      
                RestContext.request = request;
                RestContext.response = res;
                DAMAC_SubmitBuyerDetails_API.doPost();
            Test.stopTest();
        }                   
    }
    /*
    @isTest
    static void testAPI2(){
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];     
        thisUser.UserRoleId =  [select Id from UserRole where name = 'Chairman'].Id;
        update thisUser;
        
        system.runAs(thisUser){
            Id superUserProfileId = [select Id from Profile where name = 'Customer Community - Super User'].Id;
            Id UserRoleId = [select Id from UserRole where name = 'Chairman'].Id;
           
            Id RecordTyId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
            Account superUserAccount = new Account(Name = 'Test Super User',  RecordTypeId = RecordTyId);
            insert superUserAccount;
            
            Contact superUserCon = new Contact();
            superUserCon.LastName = 'testSuperUser';
            superUserCon.AccountId = superUserAccount.Id;
            superUserCon.Email = 'superuser@test.com';
            superUserCon.Portal_Administrator__c = true;
            superUserCon.Owner__c = true;
            insert superUserCon;
            
            Id AccId = [select Id from Account Limit 1].Id;
            Contact ContId = [select Id from Contact where AccountId =: AccId limit 1];
           
            User portalUser = new User(alias = 'test457', email= 'superuser@test.com',
                                           emailencodingkey='UTF-8', lastname='User 457', languagelocalekey='en_US',
                                           localesidkey='en_US', profileid = superUserProfileId, country='United Arab Emirates',
                                           IsActive =true, ContactId = ContId.Id, timezonesidkey='America/Los_Angeles', 
                                           username = 'superuser@test.com');
            insert portalUser;
               
            Account acc = InitialiseTestData.getCorporateAccount('TestAgency');
            Id RecordTyIds = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
            acc.RecordTypeId = RecordTyIds;        
            insert acc ;
    
            Contact con = new Contact();
            con.LastName = 'Corporate Agency test';
            con.AccountId = acc.Id;       
            insert con ;
            
            System.runAs(portalUser){
                Test.startTest();
                    RestRequest request = new RestRequest();
                    RestResponse res = new RestResponse();
                    request.requestUri ='/submitBuyerDetails';
                    request.requestBody = Blob.valueOf('{ "jointBuyerList": [{ "jointBuyer": { "Booking__c": "a0y1w000002rqWmAAI", "DOB__c": "1988-01-26", "Damac_Agent_Action_Type__c": "Saved as Draft", "Id": "a1m1w000000aClkAAE", "Inquiry__c": "a1A1w000000ibtvEAA", "Passport_Expiry__c": "2028-03-31", "Primary_Buyer__c": false } , "UnitDoc":[] }], "primaryBuyer": { "Damac_Agent_Action_Type__c": "Saved as Draft", "Primary_Buyer__c": true, "Id": "a1m1w000000aClaAAE" }, "PrimaryBuyerUnitDoc": [] }');
                    request.httpMethod = 'POST';      
                    RestContext.request = request;
                    RestContext.response = res;
                    DAMAC_SubmitBuyerDetails_API.doPost();
                Test.stopTest();
            }
        }
    }*/
}