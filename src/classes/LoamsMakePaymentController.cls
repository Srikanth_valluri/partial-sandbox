/**
 * @File Name          : LoamsMakePaymentController.cls
 * @Description        :
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/29/2019, 7:44:32 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    08/29/2019, 7:44:32 PM   ChangeMeIn@UserSettingsUnder.SFDoc                                     Initial Version
 * 1.1    12/08/2020               Excluded the Damac Living App's picklist values in Other Payment Types
**/
public without sharing class LoamsMakePaymentController {


    public  static final    String  FM_GUEST_PAYMENT    = Label.FMGuestPayment;
    public  static final    String  OTHERS              = Label.Others;

    public  PayerDetails    payer                {get; set;}
    public  String          baseUrl              {get; set;}
    public  String          retUrl               {get; set;}
    public  Boolean         isContratorPageValue {get; set;}


    private Location__c     project;
    private Booking_Unit__c unit;


    public LoamsMakePaymentController() {
        System.debug('-->> Inside LoamsMakePaymentController: ');
        initializeVariables();
        retUrl = ApexPages.currentPage().getParameters().get('retUrl');
        retUrl = String.isBlank(retUrl) ? '' : retUrl;
        System.debug('retUrl = ' + retUrl);
        String strContract = ApexPages.currentPage().getParameters().get('isContractorPage');
        System.debug('-->> strContract: '+ strContract);
        if(strContract == 'true' || strContract == 'True'){
            isContratorPageValue = true;
            System.debug('-->> isContratorPageValue: '+ isContratorPageValue);
            payer.paymentType = 'Others';
            payer.otherPaymentType = 'Work Permit';
            System.debug('-->> Yaha payer.paymentType: '+ payer.paymentType);
            System.debug('-->> Yaha payer.otherPaymentType: '+ payer.otherPaymentType);
        } else {
            isContratorPageValue = false;
        }

    }

    private void initializeVariables() {
        payer = new PayerDetails();
        baseUrl = '';
    }

    public List<SelectOption> getPhoneCountryCodes() {
        List<SelectOption> lstPhoneCodes = new List<SelectOption>{
            new SelectOption('', 'Select / اختار ', true)
        };
        for (Schema.PicklistEntry option : FM_Case__c.Mobile_Country_Code__c.getDescribe().getPicklistValues()) {
            lstPhoneCodes.add(new SelectOption(option.getValue(), option.getLabel()));
        }
        return lstPhoneCodes;
    }

    public List<SelectOption> getCountryList() {
        List<SelectOption> lstCountry = new List<SelectOption>{
            new SelectOption('', 'Select Country / اختيار البلد', true)
        };
        for (Schema.PicklistEntry option : FM_Case__c.Country__c.getDescribe().getPicklistValues()) {
            lstCountry.add(new SelectOption(option.getValue(), option.getLabel()));
        }
        return lstCountry;
    }

    public PageReference initiateTransaction() {
        if (!validatePayerDetails(payer)) {
            return NULL;
        }
        CCAvenuePaymentGateway gateway = CCAvenuePaymentGateway.getInstance(LoamsCommunityController.GATEWAY_NAME);
        if (gateway == NULL) {
            return NULL;
        }
        System.debug('retUrl = ' + baseUrl + '/CommunityPaymentStatus?retUrl=' + retUrl);
        System.debug('OrderId = ' + payer.unit);

        System.debug('payer = ' + JSON.serialize(payer));

        List<Booking_Unit__c> lstBookingUnit = [
            SELECT  Id
                    , Booking__r.Account__c
                    , Booking__r.Account__r.Email__c
                    , Booking__r.Account__r.Email__pc
                    , Booking__r.Account__r.IsPersonAccount
                    , Registration_ID__c
            FROM    Booking_Unit__c
            WHERE   Id = :payer.unit
        ];

        FM_Receipt__c fmReceipt = new FM_Receipt__c(
            Account__c = (lstBookingUnit.isEmpty() ? NULL : lstBookingUnit[0].Booking__r.Account__c),
            Payment_Type__c = FM_GUEST_PAYMENT,
            Guest_Payment_Type__c = payer.paymentType,
            Guest_Other_Payment_Type__c = OTHERS.equalsIgnoreCase(payer.paymentType) ? payer.otherPaymentType : NULL,
            Amount__c = payer.amount,
            Billing_Name__c = (payer.firstName + ' ' +payer.lastName).trim(),
            Billing_Email__c = payer.email,
            Billing_Phone__c = payer.phonecode + ' ' + payer.phone,
            Billing_Address__c = payer.address,
            Billing_City__c = payer.city,
            Billing_State__c = payer.state,
            Billing_Country__c = payer.country,
            Billing_Zip__c = payer.zip,
            Booking_Unit__c = payer.unit,
            CustomerAcceptedTermsNConditions__c = true,
            CustomerEmail__c = (lstBookingUnit.isEmpty()
                                ? NULL
                                : (lstBookingUnit[0].Booking__r.Account__r.IsPersonAccount
                                    ? lstBookingUnit[0].Booking__r.Account__r.Email__pc
                                    : lstBookingUnit[0].Booking__r.Account__r.Email__c))
        );
        insert fmReceipt;
        fmReceipt = [
            SELECT  Id
                    , Name
                    , Amount__c
                    , Billing_Name__c
                    , Billing_Email__c
                    , Billing_Address__c
                    , Billing_City__c
                    , Billing_Country__c
                    , Billing_Phone__c
                    , Billing_State__c
                    , Billing_Zip__c
                    , Booking_Unit__c
            FROM    FM_Receipt__c
            WHERE   Id = :fmReceipt.Id
        ];

        String paymentSubType = OTHERS.equalsIgnoreCase(payer.paymentType)
                                ? payer.paymentType + ' - ' + payer.otherPaymentType : payer.paymentType;

        return new PageReference(
            gateway.initiateTransaction(
                fmReceipt.Name, payer.amount, baseUrl + '/CommunityPaymentStatus?retUrl=' + retUrl,
                fmReceipt.Billing_Name__c, payer.email, fmReceipt.Billing_Phone__c, payer.address, payer.city,
                payer.state, payer.zip, payer.country, FM_GUEST_PAYMENT, paymentSubType
            )
        );
    }

    public Boolean validatePayerDetails(PayerDetails payer) {
        if (!requiredDetailsFilled(payer)) {
            return false;
        } else if (!validateFieldDataIntegrity(payer)) {
            return false;
        }

        return true;
    }

    public Boolean requiredDetailsFilled(PayerDetails payer) {
        if (String.isBlank(payer.firstName)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter First Name'));
            return false;
        } else if (String.isBlank(payer.lastName)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter Last Name'));
            return false;
        } else if (String.isBlank(payer.phone)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter Phone Number'));
            return false;
        } else if (String.isBlank(payer.phoneCode)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter Country code for Phone Number'));
            return false;
        } else if (String.isBlank(payer.email)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter Email address'));
            return false;
        } else if (String.isBlank(payer.country)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter Country'));
            return false;
        } else if (String.isBlank(payer.state)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter State'));
            return false;
        } else if (String.isBlank(payer.city)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter City'));
            return false;
        } else if (String.isBlank(payer.zip)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter ZIP code'));
            return false;
        } else if (String.isBlank(payer.address)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter Address'));
            return false;
        } else if (String.isBlank(payer.project)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please select a Project'));
            return false;
        } else if (String.isBlank(payer.unit)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please select a Unit'));
            return false;
        } else if (payer.amount == NULL) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter Amount to be paid'));
            return false;
        }

        return true;
    }

    public Boolean validateFieldDataIntegrity(PayerDetails payer) {
        if (!(payer.amount > 0)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Amount should be greater than 0'));
            return false;
        }
        List<Location__c> lstLocation = [SELECT Id FROM Location__c WHERE Id = :payer.project];
        if (lstLocation.isEmpty()) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please select a valid Project'));
            return false;
        } else {
            List<Booking_Unit__c> lstUnit = [
                SELECT  Id
                FROM    Booking_Unit__c
                WHERE   Id = :payer.unit
                        AND Inventory__r.Building_Location__c = :payer.project
            ];
            if (lstUnit.isEmpty()) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please select a valid Unit from the selected Project'));
                return false;
            }
            project = lstLocation[0];
            unit = lstUnit[0];
        }

        return true;
    }

    @RemoteAction
    public static List<Location__c> lookupBuilding(String name) {
        if (String.isBlank(name)) {
            return NULL;
        }
        name = '%' + String.escapeSingleQuotes(name) + '%';
        return [
            SELECT  Id
                    , Name
                    , Building_Name__c
                    , Location_Code__c
            FROM    Location__c
            WHERE   (Building_Name__c LIKE :name OR  Name LIKE :name)
                    AND (NOT Building_Name__c  LIKE '%STORAGE%')
                    AND (Location_Type__c = 'Building' OR Location_Type__c = NULL)
            LIMIT   200
        ];
    }

    @RemoteAction
    public static List<Booking_Unit__c> lookupUnit(String name, String locationId) {
        if (String.isBlank(name) || String.isBlank(locationId)) {
            return NULL;
        }
        name = '%' + String.escapeSingleQuotes(name) + '%';
        Set<String> setActiveStatuses = Booking_Unit_Active_Status__c.getAll().keySet();
        System.debug('locationId: ' + locationId);
        System.debug('name: ' + name);
        return [
            SELECT  Id
                    , Unit_Name__c
                    , Booking__r.Account__c
                    , Booking__r.Account__r.Name
                    , Registration_ID__c
            FROM    Booking_Unit__c
            WHERE   Inventory__r.Building_Location__c = :locationId
                    AND Unit_Name__c LIKE :name
                    AND Registration_Status__c IN :setActiveStatuses
                    //AND (Property_Status__c = 'Ready' OR Inventory__r.Property_Status__c = 'Ready')
                    AND Property_Country__c = 'United Arab Emirates'
                    AND Handover_Status__c NOT IN (
                            'Handover Notice not sent', 'Handover Notice Not Sent – Litigation'
                        )
                    AND (Handover_Flag__c = 'Y' OR Early_Handover__c = true OR Inventory__r.Building_Location__r.Finance_Confirmation_Complete__c = TRUE)
                    //AND Unit__r.Finance_Confirmation_Complete__c = TRUE
            LIMIT   200
        ];
    }

    public List<SelectOption> getPaymentTypes() {
        List<SelectOption> lstPaymentTypes = new List<SelectOption>();
        for (Schema.PicklistEntry ple : SobjectType.FM_Receipt__c.fields.Guest_Payment_Type__c.getPicklistValues()) {
            lstPaymentTypes.add(new SelectOption(ple.getValue(), ple.getLabel()));

        }
        System.debug('-->> Yaha 1: ');
        if(isContratorPageValue){
                payer.paymentType = 'Others';
                System.debug('-->> payer.paymentType: '+ payer.paymentType);
        }
        return lstPaymentTypes;
    }

    public List<SelectOption> getOtherPaymentTypes() {
        
        List<SelectOption> lstOtherPaymentTypes = new List<SelectOption>();
        List<String> lstOtherPaymentMDT = new List<String>();
        for(HD_App_Other_Payment_Types__mdt objMdt : [SELECT id
                                                           , MasterLabel
                                                           , Description__c 
                                                      FROM HD_App_Other_Payment_Types__mdt]) {
            lstOtherPaymentMDT.add(objMdt.MasterLabel);
        }
        System.debug('lstOtherPaymentMDT:: ' + lstOtherPaymentMDT);

        List<String> lstCommonOtherPayment = new List<String>{'Tawtheeq charge',
                                                'Drawing review charge',
                                                'Banquet hall booking',
                                                'Lounge booking',
                                                'BBQ charges',
                                                'Cheques bounced charge'};

        System.debug('lstCommonOtherPayment:: ' + lstCommonOtherPayment);
                                 
        for (Schema.PicklistEntry ple :
                SobjectType.FM_Receipt__c.fields.Guest_Other_Payment_Type__c.getPicklistValues()
        ) {
            if( lstOtherPaymentMDT.contains(ple.getLabel()) && !lstCommonOtherPayment.contains(ple.getLabel()) ) {
                continue;
            }
            lstOtherPaymentTypes.add(new SelectOption(ple.getValue(), ple.getLabel()));
        }

        /*Added the Metadata records which are used in HD APP*/
        //List<HD_App_Other_Payment_Types__mdt> lstPaymentMdt = [SELECT id
        //                                                            , MasterLabel
        //                                                            , Description__c 
        //                                                       FROM HD_App_Other_Payment_Types__mdt];
        //System.debug('lstPaymentMdt:: ' + lstPaymentMdt);
        //for(HD_App_Other_Payment_Types__mdt objMdt : lstPaymentMdt) {
        //    lstOtherPaymentTypes.add(new SelectOption(objMdt.MasterLabel, objMdt.MasterLabel));
          
        //}

        System.debug('-->> Yaha 2: ');
        if(isContratorPageValue){
                payer.otherPaymentType = 'Work Permit';
                System.debug('-->> payer.otherPaymentType: '+ payer.otherPaymentType);
        }
        return lstOtherPaymentTypes;
    }
    
    @RemoteAction
    public static duesWrapper fetchFmDues(String unitName) {
        //System.assert(false,'In method ' + unitName );
        Booking_Unit__c bookingUnit = [
                                        SELECT Id,
                                        Registration_ID__c,
                                        Booking__r.Account__r.Party_Id__c,
                                        CurrencyIsoCode
                                        FROM Booking_Unit__c
                                        WHERE Unit_Name__c = :unitName
                                    ];
        System.debug('Test Id' + bookingUnit.Booking__r.Account__r.Party_Id__c);
        String partyId = bookingUnit.Booking__r.Account__r.Party_Id__c ;
        if (String.isBlank(partyId)) {
            throw new LoamsCommunityException('Error: Could not fetch due amount. (Invalid Party Id)');
        }

        FmIpmsRestServices.DueInvoicesResult dues;
        try {
            System.debug('partyId:: ' + partyId);
            System.debug('Reg Is :: ' + bookingUnit.Registration_ID__c);
            dues = FmIpmsRestServices.getDueInvoices(bookingUnit.Registration_ID__c, partyId, '',true);
            //dues = FmIpmsRestServices.getDueInvoices('80682', '1112794', '',true);
            System.debug('dues from IPMS : ' + dues);

        } catch(Exception excp) {
            System.debug('excp = ' + excp);
            //throw new LoamsCommunityException('Could not fetch due amount. (Timeout error)');
        }
        System.debug('Dues ' + dues);
        Decimal totalCharges = 0.0;
        //Commented so as to Sync with Prod copy - 08-12-2020
        //duesWrapper  dueWInstnace = new duesWrapper ();
        //if (dues != null) {
        //   dueWInstnace.totalAmt = totalCharges;
        //    for (FmIpmsRestServices.DueInvoice due : dues.lstDueInvoice) {
        //        totalCharges += due.dueRemaining;
        //    }
            
        //    dueWInstnace.totalAmt = totalCharges;
           
        //} else {
        //        dueWInstnace.totalAmt = totalCharges ;
                
        //}
        for (FmIpmsRestServices.DueInvoice due : dues.lstDueInvoice) {
            totalCharges += due.dueRemaining;
        }
        duesWrapper  dueWInstnace = new duesWrapper ();
        dueWInstnace.totalAmt = totalCharges;
         dueWInstnace.currencyType = bookingUnit.CurrencyIsoCode;
        System.debug('Total remaining charges ' + totalCharges);
        return dueWInstnace ;
    }
    
    public class PayerDetails {
        public String   firstName           {get; set;}
        public String   lastName            {get; set;}
        public String   phoneCode           {get; set;}
        public String   phone               {get; set;}
        public String   email               {get; set;}
        public String   country             {get; set;}
        public String   state               {get; set;}
        public String   city                {get; set;}
        public String   zip                 {get; set;}
        public String   address             {get; set;}
        public String   paymentType         {get; set;}
        public String   otherPaymentType    {get; set;}
        public String   project             {get; set;}
        public String   unit                {get; set;}
        public Decimal  amount              {get; set;}

        public PayerDetails() {
            phoneCode   = '';
            country     = '';
        }
    }
    public class duesWrapper {
        public String currencyType {get; set;}
        public Decimal totalAmt {get; set;}
    }
}