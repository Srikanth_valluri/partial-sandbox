global class FM_PreLegalNoticeSmsBatch implements Database.Batchable<sObject>
                                        , Database.Stateful
                                        , Database.AllowsCallouts {
    
    Static String user = Label.FM_SMS_Service_Username, passwd = Label.FM_SMS_Service_Password, strSID = Label.FM_SMS_Service_SenderId;// 'epms.test@damacgroup.com';
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('***FM_PreLegalNoticeBatch  start****');
        String strQuery = 'SELECT Id, FM_Pre_Legal_Notice_sent_date__c, Send_FM_Pre_Legal_Notice__c, ' +
                        ' Booking__r.Account__r.Mobile_Phone_Encrypt__pc,Building_Name__c,Unit_Name__c,Inventory__r.Building_Name__c, ' +
                        ' Booking__r.Account__r.Name, Booking__r.Account__r.Mobile__c, Booking__r.Account__r.IsPersonAccount FROM Booking_Unit__c '+
                        ' WHERE Send_FM_Pre_Legal_Notice__c = true ';
                        /*' WHERE Unit_Active__c = \'Active\' AND Registration_ID__c != Null' +
                        ' AND Booking__r.Account__c != NULL AND ( Booking__r.Account__r.Mobile_Phone_Encrypt__pc !=NULL ' +
                        ' OR Booking__r.Account__r.Mobile__c != NULL ) AND Unit_Name__c != NULL AND Building_Name__c != NULL' +
                        ' AND Send_FM_Pre_Legal_Notice__c = true';*/
        
        System.debug('strQuery :: ' + strQuery);
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<Booking_Unit__c> listBUs){ 
        List<SMS_History__c> smsLstFinal  = new List<SMS_History__c>();
        System.debug('***FM_PreLegalNoticeBatch execute****');
        for(Booking_Unit__c  objBU : listBUs) {
            smsLstFinal = sendSms(objBU);    
        }
        
        
        if(smsLstFinal.size() > 0 ) {
            upsert smsLstFinal;
        }
        
    }

    global void finish(Database.BatchableContext BC){
        System.debug('***FM_PreLegalNoticeBatch finish****');
    }
    
    /*********************************************************************************
 * Method Name : sendSms
 * Description : send SMS to units
 * Return Type : void
 * Parameter(s): Booking Unit
**********************************************************************************/     
    List<SMS_History__c> sendSms(Booking_Unit__c objBU) {
        List<SMS_History__c> smsLst  = new List<SMS_History__c>();
            String contentValue = 'Dear <Customer>,\n\n' + 
                                'Pre legal notice has been issued to your registered email id,' +
                                ' with reference to your unpaid service charges at <Tower name>-<Unit number>.\n\n'+
                                'For more details or payment instructions contact LOAMS 0097147049000\n\n'+
                                'LOAMS';
            if(contentValue.contains('<Customer>')) {
                String custName = GenericUtility.getCamelCase(objBU.Booking__r.Account__r.Name);
                contentValue = contentValue.replace('<Customer>', custName);
            }
            if(contentValue.contains('<Tower name>')) {
                contentValue = contentValue.replace('<Tower name>', objBU.Inventory__r.Building_Name__c);
            }
            if(contentValue.contains('<Unit number>')) {
                contentValue = contentValue.replace('<Unit number>', objBU.Unit_Name__c);
            }
            System.debug('contentValue==' + contentValue);
            String PhoneNumber = objBU.Booking__r.Account__r.IsPersonAccount ? objBU.Booking__r.Account__r.Mobile_Phone_Encrypt__pc : objBU.Booking__r.Account__r.Mobile__c;
            //contentBody = contentBody.substringBefore('LOAMS');

            if(PhoneNumber.startsWith('00')) {
                PhoneNumber = PhoneNumber.removeStart('00');
            }else if(PhoneNumber.startsWith('0')) {
                PhoneNumber = PhoneNumber.removeStart('0');
            }

            strSID = SMSClass.getSenderName(user, PhoneNumber, false);

            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            req.setMethod('POST' ); // Method Type
            req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx'); 
            req.setBody('user='+ user + '&passwd=' + passwd +'&message=' 
                        + GenericUtility.encodeChar(contentValue) + '&mobilenumber=' + PhoneNumber 
                        + '&sid='+ strSID + '&MTYPE=LNG&DR=Y'); // Request Parameters
            res = http.send(req);
            system.debug('req Body---'+req.getBody());
            system.debug('Response---'+res);
            system.debug('Response body---'+res.getBody());
            
            if(res.getBody() != null){
                // Parse Response
                SMS_History__c smsObj = new SMS_History__c();
                smsObj.Message__c = contentValue;
                smsObj.Phone_Number__c = PhoneNumber;
                smsObj.Customer__c = objBU.Booking__r.Account__c;
                //smsObj.Booking_Unit__c = objBU.Id;
                smsObj.Description__c = res.getBody();
                if(String.valueOf(res.getBody()).contains('OK:')){
                     smsObj.sms_Id__c = res.getBody().substringAfter('OK:');
                     smsObj.Is_SMS_Sent__c = true;
                }
                System.debug('smsObj::::else:'+smsObj);
                
                smsLst.add(smsObj);
            }
            
        return smsLst;
    }
}