/******************************************************************************
* Description - Test class developed for CustomerNotificationBatch
*
* Version            Date            Author                    Description
* 1.0                11/12/17        Naresh Kaneriya (Accely)   Initial Draft
* 2.0				 13/12/2017		 Ashish Agarwal				Updated Test_start method
********************************************************************************/
@isTest
private class CustomerNotificationBatchTest
{
    
   static CustomerNotificationBatch instance = new CustomerNotificationBatch();
    
    public static testmethod void Test_AllContructors(){
        CustomerNotificationBatch.CustomerNotificationInfoWrapper obj = new CustomerNotificationBatch.CustomerNotificationInfoWrapper();
        System.assert(obj != null);
    } 
    
    // Test Method: start
    public static testmethod void Test_start(){
       
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        insert Acc;
        System.assert(Acc != null);
        
        List<Booking__c> Booking =  TestDataFactory_CRM.createBookingForAccount(Acc.Id,Sr.Id,2);
        insert Booking;
        System.assert(Booking != null);
        
        List<Booking_Unit__c> BU =  TestDataFactory_CRM.createBookingUnits(Booking,2);
        insert BU;
        System.assert(BU != null);
        
        List<Calling_List__c> ObjList =  new  List<Calling_List__c>();
        
        Calling_List__c CallObj = new Calling_List__c();
        CallObj.RecordTypeId  = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        CallObj.Registration_ID__c = BU[0].Registration_ID__c ;
        CallObj.Inv_Due__c  =4 ;   
        CallObj.DM_Due_Amount__c  = 0;  
        CallObj.Account__c   =Acc.Id;  
        CallObj.Due_Date__c   = system.today()+30;   
        CallObj.Mobile_Phone__c   = '9856214598';  
        CallObj.Email__c   = 'abc@gmail.com';   
       
        ObjList.add(CallObj);
        insert ObjList; 
        System.assert(ObjList[0].Registration_ID__c != null);
       
        // To cover Custome metadata
        list<Collection_Customer_Notification_Setting__mdt> Mdt = new list<Collection_Customer_Notification_Setting__mdt>();
        Map<Integer, list<Collection_Customer_Notification_Setting__mdt>> mapDaysNotifications = new Map<Integer, list<Collection_Customer_Notification_Setting__mdt>>();
        Map<String, EmailTemplate> mapDNEmailTemplate = new  Map<String, EmailTemplate>();
        set<String> setEmailTemplateNames = new set<String>();
        
        Collection_Customer_Notification_Setting__mdt omt = [select id ,
                                                                Email_Template_API_Name__c ,Notification_Type__c ,
                                                                Number_of_Days__c from Collection_Customer_Notification_Setting__mdt where Notification_Type__c = 'Email' Limit 1];
        
         Mdt.add(omt);
         mapDaysNotifications.put(3,Mdt);
         mapDNEmailTemplate.put('String',new EmailTemplate());
         setEmailTemplateNames.add(omt.Email_Template_API_Name__c);
         
         CustomerNotificationBatch.CustomerNotificationInfoWrapper objmdt = new CustomerNotificationBatch.CustomerNotificationInfoWrapper();
         objmdt.mapDaysNotifications  = mapDaysNotifications;
         objmdt.mapDNEmailTemplate   = mapDNEmailTemplate;
         objmdt.setEmailTemplateNames   = setEmailTemplateNames ;
        
         Test.startTest();
         
          SOAPCalloutServiceMock.returnToMe = new Map<String,GenerateSOAService.GenCustomerStatementResponse_element>();
          GenerateSOAService.GenCustomerStatementResponse_element response_x = new GenerateSOAService.GenCustomerStatementResponse_element();
          response_x.return_x = '{'+
        ' "status" : "s",'+
        ' "url" : "www.google.com",'+
        ' "PROC_MESSAGE" : "s",'+
        ' "PROC_STATUS" : "s"'+  
        ' '+
          '}'; 
          SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
          Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
          Test.setMock(HttpCalloutMock.class, new MockHttpAgentOTPController());
 
          Database.executeBatch(instance);
          Test.stopTest();
    }  
    // Test Method: createErrorLogRecord
    public static testmethod void Test_createErrorLogRecord(){
         Test.startTest();
         Calling_List__c objcall = new Calling_List__c();
         string strmessage = 'strmessage';
         instance.createErrorLogRecord(objcall, strmessage);
         System.assert((instance.createErrorLogRecord(objcall, strmessage)) != null);
         instance.calculateDateDifference(System.today() + 10);  
         Test.stopTest();
    }
}