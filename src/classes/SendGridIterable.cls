public with sharing class SendGridIterable implements Iterator<sendGridEmail>,Iterable<sendGridEmail>{

    List<SendGridEmail> lstWrapper {get; set;}
    Integer i {get; set;}

    public SendGridIterable(List<SendGridEmail> plstWrapper){
        lstWrapper = plstWrapper;
        i = 0;
    }

    public boolean hasNext(){
        if(i >= lstWrapper.size())
            return false;
        else
            return true;
    }

    public Iterator<SendGridEmail> iterator() {
        return this;
    }

    public SendGridEmail next(){
        if(i == lstWrapper.size()){
            return null;
        }
        i = i + 1;
        return lstWrapper[i-1];
    }

    public class sendGridEmail {
        public String toAddress;
        public String toName;
        public String ccAddress;
        public String ccName;
        public String bccAddress;
        public String bccName;
        public String subject;
        public String substitutions;
        public String fromAddress;
        public String fromName;
        public String replyToAddress;
        public String replyToName;
        public String contentType;
        public String contentValue;
        public String templateId;
        public List<Attachment> lstAttach;
        public String relatedId;
        public String buId;
        public String accountId;
    }

}