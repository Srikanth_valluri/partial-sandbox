@isTest
public class Aopt7DayNotifyTest {
    public static testmethod void testAopt7DayNotify() {
        Case objCase = new Case();
        objCase.Account_Email__c = 'test@test.com';
        insert objCase;
        
        Aopt7DayNotify.checkIfAddendumUploaded(new List<Case>{objCase});
    }

}