public class ScheduleCallBackNotificationBatch Implements Schedulable  {
     public void execute(SchedulableContext SC){
        System.debug('inside scheduler');
        CallBackNotificationBatchForCallingList batchInst = new CallBackNotificationBatchForCallingList();
        Database.executeBatch(batchInst);
    }
}