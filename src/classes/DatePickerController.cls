public with sharing class DatePickerController {
    
    /*public DatePickerController () {
    }*/
    public Object targetDate {get;set;}
    public Integer randomJsId {get;set;}
    public DatePickerController() {
        randomJsId = getRandomNumber(1000000);
    }

    //Random number generator to change the js function name if multiple components us
    
    public  Integer getRandomNumber(Integer size){
        Double d = Math.random() * size;
        return d.intValue();
    }

    public void setTargetDate(Object targetDate){
        if(targetDate == null){
            this.targetDate = targetDate;
        }
    }
}