public class CreateEligibilityCriteriaController {
    public Promotion_Eligibility_Criteria__c criteria { get; set; }
    public String selectedField { get; set; }
    public String jsonResult { get; set; }
    public String promotionId { get; set; }
    public Boolean showHeader { get; set; }
    String relatedObjAPI;
    public CreateEligibilityCriteriaController (APexPages.StandardController stdController) {
        criteria = new Promotion_Eligibility_Criteria__c ();
        selectedField = '';
        jsonResult = '';
        relatedObjAPI = '';
        showHeader = false;
        if (stdController.getId() != NULL) {
            criteria = [SELECT Promotion_Setup__c, Object__c, Value_Type__c, 
                                Condition__c, Order__c, Value__c, Parameter__c
                        FROM Promotion_Eligibility_Criteria__c 
                        WHERE Id =:stdController.getId()];
            relatedObjAPI = criteria.Object__c;
        } else {
            promotionId = apexpages.currentpage().getparameters().get('promotionId');
            showHeader = true;
            List <Promotion_Eligibility_Criteria__c> existingCriteria 
                    = new List <Promotion_Eligibility_Criteria__c> ();
            try {
              existingCriteria = [SELECT Name 
                                  FROM Promotion_Eligibility_Criteria__c 
                                  WHERE Promotion_Setup__c =: promotionId];
            } catch (Exception e) {}
            relatedObjAPI = 'NSIBPM__Service_Request__c';
            criteria.Order__c = existingCriteria.size () + 1;
        }
    }

    public void updateCriteria () {
       System.Debug ('====' + criteria);
        if (criteria.Parameter__c != NULL) {
            criteria.Value_Type__c = [SELECT Value_Type__c FROM Promotion_Criteria_Field__mdt 
                                  WHERE Object_API__c =: relatedObjAPI
                                  AND Field_API__c =: criteria.Parameter__c 
                                  AND Promotion_Eligibility_Criteria__c = TRUE
                                  LIMIT 1].Value_Type__c;
        }
        if (promotionId != NULL && promotionId != '') {
          criteria.Promotion_Setup__c = promotionId;
            insert criteria;
        } else {
          update criteria;  
      }      
    }
    
    public void updateRelatedObj(){
        String obj = apexpages.currentpage().getparameters().get('objAPI');
        system.debug('obj: '+ obj);
        if(obj != null && obj != ''){
            relatedObjAPI = obj;
            getFieldAPIs();
        }
        
    }
 

    public Map <String, String> getFieldAPIs () {
        Map <String, String> options = new Map <String, String> ();
        system.debug('relatedObjAPI: ' + relatedObjAPI);
        for (Promotion_Criteria_Field__mdt u: [SELECT Label, Field_API__c 
                                               FROM Promotion_Criteria_Field__mdt 
                                               WHERE Object_API__c =: relatedObjAPI
                                                AND Promotion_Eligibility_Criteria__c = TRUE]) {
            options.put (u.label, u.Field_API__c);
        }
        jsonResult = JSON.serialize(options);
        system.debug('jsonResult: '+ jsonResult);
        return options;
    }
}