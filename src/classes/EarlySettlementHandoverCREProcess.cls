public without sharing class EarlySettlementHandoverCREProcess {
     public String accountName {get;set;}
     public String unitName {get;set;}
     public String strSelectedCategory {get;set;} 
     public string strPaymentPlan {get;set;}
     public String strPayPlanId {get;set;}
     public string rowToRemove {get;set;} 
     public String CurrencyCode {get;set;}
     public String updatedPT {get;set;}
     public string errorPDCMessage{get;set;}
     public decimal percentValueTotalCurPP {get;set;}
     public decimal percentValueTotalNewPP {get;set;}
     public boolean blnIsCurrentPP {get;set;}
     public boolean blnReadOnly {get;set;}
     public Booking_Unit__c objBookingUnit {get;set;}
     public UnitDetailsService.BookinUnitDetailsWrapper objBookinUnitDetailsWrapper {get; set;}
     public list<SelectOption> lstCategories {get;set;}
     public list<Buyer__c> lstJointBuyers {get;set;}
     public list<Case> lstOpenCase {get;set;}
     public list<SelectedNewPaymentTermsByCRE> lstCurrentPaymentTerms {get;set;}
     public list<SelectedNewPaymentTermsByCRE> lstNewPaymentTermsDisplay {get;set;}     
     
     private Map<String,String> mapMileStoneEvents;
     private boolean isDraftCase;
     Case caseobj;
     
     public EarlySettlementHandoverCREProcess (ApexPages.StandardController controller) {        
        caseobj= (Case)controller.getrecord();
        caseobj = [Select Id
                         , Accountid
                         , Booking_Unit_Name__c
                         , Account.Name
                         , RecordType.DeveloperName
                         , RecordTypeId
                         , Status 
                         , Booking_Unit__c
                         , Booking_Unit__r.Registration_ID__c
                         , ParentId
                         , CurrencyIsoCode
                    From Case 
                    where id = :caseobj.id limit 1];
        accountName = caseobj.Account.Name;
        unitName = caseobj.Booking_Unit_Name__c;
        objBookinUnitDetailsWrapper = new UnitDetailsService.BookinUnitDetailsWrapper();
        blnIsCurrentPP = false;
        isDraftCase = false;
        CurrencyCode = caseobj.CurrencyIsoCode;        
        UnitDetails(caseobj.Booking_Unit__c);
        if (caseobj.RecordType.DeveloperName == 'Handover' && 
            (caseobj.Status == 'Submitted' || caseobj.Status == 'Closed' 
                || caseobj.Status == 'Rejected' || caseobj.Status == 'Cancelled')) {
            blnReadOnly = true; 
            //blnIsCurrentPP = true;
            currentPaymentPlan();
        } else {
            blnReadOnly = false;
        }
    }

    public void UnitDetails(String buId) {
        bookingUnitInfo(buId);
        lstJointBuyers = new list<Buyer__c>(); 
        lstOpenCase = new list<Case>();
        strSelectedCategory = 'Unit Details';
        lstCategories = new list<SelectOption>();
        lstCategories.add(new selectOption('Unit Details', 'Unit Details'));
        lstCategories.add(new selectOption('Flags', 'Flags'));
        lstCategories.add(new selectOption('Unit Status', 'Unit Status'));
        lstCategories.add(new selectOption('Joint Buyers', 'Joint Buyers'));
        lstCategories.add(new selectOption('Open SRs', 'Open SRs'));
        objBookinUnitDetailsWrapper =
                UnitDetailsService.getBookingUnitDetails(objBookingUnit.Registration_ID__c);
        system.debug('!!!!!!!objBookinUnitDetailsWrapper'+objBookinUnitDetailsWrapper);
                
        for (Buyer__c objBuyer : [SELECT Id
                                          , Name
                                          , First_Name__c
                                          , Last_Name__c
                                          , Account__c
                                          , Buyer_ID__c
                                          , Account__r.Party_Id__c
                                       FROM Buyer__c
                                      WHERE Account__c != null
                                        AND Booking__c = :objBookingUnit.Booking__c
                                        AND Primary_Buyer__c = false]) {
            lstJointBuyers.add(objBuyer);                           
        }
        lstOpenCase.addAll(checkExistingSRExists());
    }
    
    public void bookingUnitInfo(String BuId) {
        if (!string.isBlank(BuId)) {
            objBookingUnit = [select ID,
                                Bedroom_Type__c,                                  
                                Registration_DateTime__c,
                                Permitted_Use__c,
                                Unit_Details__c,                        
                                Unit_Selling_Price__c,
                                Mortgage__c,
                                Under_Assignment__c,
                                Agreement_Date__c,
                                HOS_Name__c,
                                Manager_Name__c,
                                Property_Consultant__c,
                                Booking__c,
                                Property_Name__c,
                                Property_City__c,
                                Building_ID__c,                                                               
                                Inventory__r.Building_Location__c,
                                Inventory__r.Property__r.Name,
                                Inventory__r.Property__r.Property_City__c,
                                Inventory__r.Property__r.Property_Code__c,
                                Inventory__r.Property_City__c,
                                Inventory__r.Project_Category__c,
                                Booking__r.Account__r.Nationality__pc,
                                Booking__r.Account__r.Nationality__c,
                                Booking__r.Account__r.Party_Type__c,
                                Booking__r.Account__r.Country__c,
                                Booking__r.Account__r.IsPersonAccount,
                                Booking__r.Account__r.Country__pc,
                                Booking__r.Account__c,
                                Inventory__r.Unit_Plan__c,
                                Inventory__r.Floor_Plan__c,
                                Registration_ID__c,
                                Title_Deed__c,
                                Requested_Price__c,
                                Anticipated_Completion_Date__c,
                                Handover_Notice_Sent__c,
                                PCC_Release__c,
                                Rental_Pool__c,
                                Booking_Type__c,
                                Unit_Name__c,
                                Unit_Type__c,
                                (Select Id, Name From Payment_Plans__r)
                            from Booking_Unit__c
                            where ID =: BuId];                
        }
        
        if (objBookingUnit.Payment_Plans__r.size() > 0) {
            strPaymentPlan = objBookingUnit.Payment_Plans__r[0].Name;
            strPayPlanId = objBookingUnit.Payment_Plans__r[0].Id;
        }
    }// end of method bookingUnitInfo
    
    public list<Case> checkExistingSRExists(){
        set<String> setAllowedSRTypes = new set<String>();
        setAllowedSRTypes.add('Parking');
        setAllowedSRTypes.add('Complaint');
        setAllowedSRTypes.add('Promotion_Package');
        setAllowedSRTypes.add('POP');
        setAllowedSRTypes.add('Customer_Refund');
        setAllowedSRTypes.add('Token_Refund');
        setAllowedSRTypes.add('Utility_Registration_SR');
        
        list<Case> lstExistingCase = new list<Case>(); 
        map<Id,Case> mapId_Case = new map<Id,Case>([Select c.Id
                                                         , c.Booking_Unit__c
                                                         , c.AccountId
                                                         , c.CaseNumber
                                                         , c.RecordType.DeveloperName
                                                         , c.RecordType.Name
                                                    From Case c
                                                    where c.Booking_Unit__c =: objBookingUnit.Id
                                                    and c.Status != 'Closed'
                                                    and c.Status != 'Rejected'
                                                    and c.Status != 'Cancelled'                                                                                                       
                                                    and c.RecordType.DeveloperName NOT IN : setAllowedSRTypes
                                                    and c.Id !=: caseobj.id]);
        if(mapId_Case != null && !mapId_Case.isEmpty()){
            lstExistingCase.addAll(mapId_Case.values());
        }
        
        for(SR_Booking_Unit__c objSBU : [Select s.Id
                                              , s.Case__c
                                              , s.Case__r.Status
                                              , s.Case__r.CaseNumber
                                              , s.Case__r.RecordType.DeveloperName
                                              , s.Case__r.RecordType.Name
                                              , s.Booking_Unit__c 
                                         From SR_Booking_Unit__c s
                                         where s.Booking_Unit__c =:objBookingUnit.Id
                                         and s.Case__r.Status != 'Closed'
                                         and s.Case__r.Status != 'Rejected'
                                         and s.Case__r.Status != 'Cancelled'                                                                             
                                         and (s.Case__r.RecordType.DeveloperName = 'AOPT' or s.Case__r.RecordType.DeveloperName = 'Handover'
                                            or s.Case__r.RecordType.DeveloperName = 'Fund_Transfer' 
                                            or s.Case__r.RecordType.DeveloperName = 'Fund_Transfer_Active_Units')
                                         and s.Case__c !=: caseobj.Id]){
            if(!mapId_Case.containsKey(objSBU.Case__c)){
                Case objCase = objSBU.Case__r;
                lstExistingCase.add(objCase);
                mapId_Case.put(objSBU.Case__c, objSBU.Case__r);
            }
        }
        
        for (Account objAccount : [Select Id, 
                                        (Select RecordTypeId,
                                                RecordType.Name,
                                                RecordType.DeveloperName,
                                                CaseNumber,
                                                Status
                                        From Cases where (RecordType.DeveloperName = 'Change_of_Details' or
                                            RecordType.DeveloperName = 'Change_of_Joint_Buyer' or
                                            RecordType.DeveloperName = 'Name_Nationality_Change' or
                                            RecordType.DeveloperName = 'Passport_Detail_Update') and
                                            Status != 'Closed'
                                            and Status != 'Rejected'
                                            and Status != 'Cancelled'                                                                                   
                                            )
                                   From Account
                                   Where Id=:caseobj.AccountId]){
            lstExistingCase.addAll(objAccount.Cases);           
        }
        return lstExistingCase;
    }
    
    Public PageReference currentPaymentPlan() {
        blnIsCurrentPP = true;
        CustomerPaymentService.Payment objCurrTerms = new CustomerPaymentService.Payment();
        if (objBookingUnit.Registration_ID__c != null) {
            objCurrTerms = CustomerPaymentService.getExistingPT(objBookingUnit.Registration_ID__c);
        }
        lstCurrentPaymentTerms = new list<SelectedNewPaymentTermsByCRE>();
        mapMileStoneEvents = new map<String, String>();
        percentValueTotalCurPP = 0;
        if (objCurrTerms != null && objCurrTerms.data != null) {
            for (CustomerPaymentService.data objTerm : objCurrTerms.data) {
                SelectedNewPaymentTermsByCRE objNewPaymentTerms = new SelectedNewPaymentTermsByCRE();
                objNewPaymentTerms.installment = objTerm.ATTRIBUTE10;
                objNewPaymentTerms.description = objTerm.ATTRIBUTE4;
                objNewPaymentTerms.mileStoneEvent = objTerm.ATTRIBUTE8;
                objNewPaymentTerms.percentValue = objTerm.ATTRIBUTE5;
                objNewPaymentTerms.paymentAmount = objTerm.ATTRIBUTE9;
                objNewPaymentTerms.mileStoneArabic = objTerm.ATTRIBUTE12;
                if (!string.isBlank(objTerm.ATTRIBUTE6) ) {
                    objNewPaymentTerms.paymentDate = getConvertDateTime(objTerm.ATTRIBUTE6);
                }                
                lstCurrentPaymentTerms.add(objNewPaymentTerms);
                mapMileStoneEvents.put(objNewPaymentTerms.description , objNewPaymentTerms.paymentDate);
                percentValueTotalCurPP += Decimal.valueOf(!string.isBlank(objNewPaymentTerms.percentValue) ? objNewPaymentTerms.percentValue : '0');
            }
        }
        NewPaymentPlan();
        return null;
    }
    
    public void NewPaymentPlan() {
        lstNewPaymentTermsDisplay = new List<SelectedNewPaymentTermsByCRE>();
        MileStonePaymentDetailsWrapper.MileStonePaymentDetails objPayTerms = new MileStonePaymentDetailsWrapper.MileStonePaymentDetails();
        if (objBookingUnit.Registration_ID__c != null) {
            objPayTerms = AOPTMQService.getMilestonePaymentDetails(objBookingUnit.Registration_ID__c);
            system.debug('!!!!!!!!!objPayTerms'+objPayTerms);
        }
        Decimal remainingPercent;
        if (objPayTerms != null && objPayTerms.REG_TERM_PYMNT_TABLE != null){
             for (MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE objTerm : objPayTerms.REG_TERM_PYMNT_TABLE ) {
                 if (decimal.valueOf(objTerm.PAID_PERCENTAGE) == 100  || (objTerm.INSTALLMENT == 'I001' 
                     && (objTerm.MILESTEON_PERCENT_VALUE == '0' || objTerm.MILESTEON_PERCENT_VALUE == 'null')) || objTerm.INSTALLMENT == 'DP') {
                     SelectedNewPaymentTermsByCRE objNewPaymentTerms = new SelectedNewPaymentTermsByCRE();
                     objNewPaymentTerms.installment = objTerm.INSTALLMENT;
                     objNewPaymentTerms.description = objTerm.DESCRIPTION;
                     objNewPaymentTerms.mileStoneEvent = objTerm.MILESTONE_EVENT;
                     objNewPaymentTerms.percentValue = objTerm.MILESTEON_PERCENT_VALUE;
                     objNewPaymentTerms.paymentAmount = objTerm.INVOICE_AMOUNT;
                     objNewPaymentTerms.mileStoneArabic = objTerm.MILESTONE_EVENT_AR;
                     if (mapMileStoneEvents != null && mapMileStoneEvents.containsKey(objNewPaymentTerms.installment) ) {
                         if (!string.isBlank(mapMileStoneEvents.get(objNewPaymentTerms.installment))) {
                             objNewPaymentTerms.paymentDate = mapMileStoneEvents.get(objNewPaymentTerms.installment);
                         }
                     }
                     objNewPaymentTerms.lineId = objTerm.LINE_ID;
                     objNewPaymentTerms.isReceiptPresent = true;
                     objNewPaymentTerms.termId = objTerm.TERM_ID;
                     lstNewPaymentTermsDisplay.add(objNewPaymentTerms);
                 } else if (objTerm.INSTALLMENT == 'I001' 
                     && !string.isBlank(objTerm.MILESTEON_PERCENT_VALUE) && (decimal.valueOf(objTerm.MILESTEON_PERCENT_VALUE) > 0 )) {
                     SelectedNewPaymentTermsByCRE objNewPaymentTerms = new SelectedNewPaymentTermsByCRE();
                     objNewPaymentTerms.installment = objTerm.INSTALLMENT;
                     objNewPaymentTerms.description = objTerm.DESCRIPTION;
                     objNewPaymentTerms.mileStoneEvent = objTerm.MILESTONE_EVENT;
                     objNewPaymentTerms.percentValue = objTerm.MILESTEON_PERCENT_VALUE;
                     objNewPaymentTerms.paymentAmount = objTerm.INVOICE_AMOUNT;
                     objNewPaymentTerms.mileStoneArabic = objTerm.MILESTONE_EVENT_AR;
                     if (mapMileStoneEvents != null && mapMileStoneEvents.containsKey(objNewPaymentTerms.installment) ) {
                         if (!string.isBlank(mapMileStoneEvents.get(objNewPaymentTerms.installment))) {
                             objNewPaymentTerms.paymentDate = mapMileStoneEvents.get(objNewPaymentTerms.installment);
                         }
                     }
                     objNewPaymentTerms.lineId = objTerm.LINE_ID;
                     objNewPaymentTerms.isReceiptPresent = false;
                     objNewPaymentTerms.termId = objTerm.TERM_ID;
                     lstNewPaymentTermsDisplay.add(objNewPaymentTerms);
                 } else if (decimal.valueOf(objTerm.PAID_PERCENTAGE) > 0 && decimal.valueOf(objTerm.PAID_PERCENTAGE) < 100) {
                     Decimal percentPaid = (decimal.valueOf(objTerm.PAID_PERCENTAGE)/100) * decimal.valueOf(objTerm.MILESTEON_PERCENT_VALUE);
                     SelectedNewPaymentTermsByCRE objNewPaymentTerms = new SelectedNewPaymentTermsByCRE();
                     objNewPaymentTerms.installment = objTerm.INSTALLMENT;
                     objNewPaymentTerms.description = objTerm.DESCRIPTION;
                     objNewPaymentTerms.mileStoneEvent = objTerm.MILESTONE_EVENT;
                     objNewPaymentTerms.percentValue = String.valueOf(percentPaid.setScale(2, RoundingMode.HALF_UP)) ;
                     objNewPaymentTerms.paymentAmount = objTerm.INVOICE_AMOUNT;
                     objNewPaymentTerms.mileStoneArabic = objTerm.MILESTONE_EVENT_AR;                    
                     if (mapMileStoneEvents != null && mapMileStoneEvents.containsKey(objNewPaymentTerms.installment) ) {
                         if (!string.isBlank(mapMileStoneEvents.get(objNewPaymentTerms.installment))) {
                             objNewPaymentTerms.paymentDate = mapMileStoneEvents.get(objNewPaymentTerms.installment);
                         }
                     }
                     objNewPaymentTerms.lineId = objTerm.LINE_ID;
                     objNewPaymentTerms.isReceiptPresent = true;
                     objNewPaymentTerms.termId = objTerm.TERM_ID;
                     lstNewPaymentTermsDisplay.add(objNewPaymentTerms);
                     
                     Decimal remaining = 100 - decimal.valueOf(objTerm.PAID_PERCENTAGE);
                     if (remainingPercent == null){
                         if (!String.isBlank(objTerm.MILESTEON_PERCENT_VALUE)) {
                             remainingPercent = (remaining/100) * decimal.valueOf(objTerm.MILESTEON_PERCENT_VALUE);
                         }
                     } else {
                         if (!String.isBlank(objTerm.MILESTEON_PERCENT_VALUE)) {
                             remainingPercent = remainingPercent + ((remaining/100) * decimal.valueOf(objTerm.MILESTEON_PERCENT_VALUE));
                         }
                     }
                 } else if (decimal.valueOf(objTerm.PAID_PERCENTAGE) == 0) {
                      if (remainingPercent == null ){
                          if (!string.isBlank(objTerm.MILESTEON_PERCENT_VALUE)) {
                              remainingPercent = decimal.valueOf(objTerm.MILESTEON_PERCENT_VALUE);
                          }
                      } else {
                          if (!String.isBlank(objTerm.MILESTEON_PERCENT_VALUE)) {
                              remainingPercent = remainingPercent + decimal.valueOf(objTerm.MILESTEON_PERCENT_VALUE);
                          }
                      }
                     /*SelectedNewPaymentTermsByCRE objNewPaymentTerms = new SelectedNewPaymentTermsByCRE();
                     objNewPaymentTerms.installment = objTerm.INSTALLMENT;
                     objNewPaymentTerms.description = objTerm.DESCRIPTION;
                     objNewPaymentTerms.mileStoneEvent = objTerm.MILESTONE_EVENT;
                     objNewPaymentTerms.percentValue = objTerm.MILESTEON_PERCENT_VALUE;
                     objNewPaymentTerms.paymentAmount = objTerm.INVOICE_AMOUNT;
                     objNewPaymentTerms.mileStoneArabic = objTerm.MILESTONE_EVENT_AR;
                     objNewPaymentTerms.lineId = objTerm.LINE_ID;                
                     objNewPaymentTerms.isReceiptPresent = false;
                     objNewPaymentTerms.termId = objTerm.TERM_ID;                    
                     if(mapMileStoneEvents != null && mapMileStoneEvents.containsKey(objNewPaymentTerms.installment) ) {
                         if (!string.isBlank(mapMileStoneEvents.get(objNewPaymentTerms.installment))) {
                             objNewPaymentTerms.paymentDate = mapMileStoneEvents.get(objNewPaymentTerms.installment);
                         }
                     }
                     lstNewPaymentTermsDisplay.add(objNewPaymentTerms);*/
                 }
             }// end of for
             if (remainingPercent != null){
                 Long counter = lstNewPaymentTermsDisplay.size();
                 SelectedNewPaymentTermsByCRE objNewPaymentTerm = new SelectedNewPaymentTermsByCRE();
            
                 if (counter == 2) {
                     objNewPaymentTerm.description = counter+'ND INSTALLMENT';
                 } else if (counter == 3) {
                     objNewPaymentTerm.description = counter+'RD INSTALLMENT';
                 } else {
                     objNewPaymentTerm.description = counter+'TH INSTALLMENT';
                 }
                 objNewPaymentTerm.Id = '';
                 if (counter <=9) {
                     objNewPaymentTerm.installment = 'I00'+counter;
                 } else if (counter > 9) {
                     objNewPaymentTerm.installment = 'I0'+counter;
                 }
                 objNewPaymentTerm.mileStoneEvent = 'On or Before';                
                 objNewPaymentTerm.percentValue = String.valueOf(remainingPercent.setScale(2, RoundingMode.HALF_UP));
                 Date dueDate = system.today();
                 objNewPaymentTerm.paymentDate = dueDate.format();
                 objNewPaymentTerm.isReceiptPresent = false;
                 lstNewPaymentTermsDisplay.add(objNewPaymentTerm);
            }  

        }
    }
    
    public void removeInstallment() {
        system.debug('rowToRemove '+rowToRemove);
    
        lstNewPaymentTermsDisplay.remove(integer.valueOf(rowToRemove));        
        
        for(integer i = 0 ; i < lstNewPaymentTermsDisplay.size() ; i++) {
            if(i > 0) {
                if (i <=9) {
                    lstNewPaymentTermsDisplay[i].installment = 'I00'+i;
                } else if (i > 9) {
                    lstNewPaymentTermsDisplay[i].installment = 'I0'+i;
                }
                if(i == 1) {
                    lstNewPaymentTermsDisplay[i].description = i+'ST INSTALLMENT';
                } else if(i == 2) {
                    lstNewPaymentTermsDisplay[i].description = i+'ND INSTALLMENT';
                } else if(i == 3) {
                    lstNewPaymentTermsDisplay[i].description = i+'RD INSTALLMENT';
                } else {
                    lstNewPaymentTermsDisplay[i].description = i+'TH INSTALLMENT';
                }                  
            }
        }
    }
    
    public void addInstallment() {
        system.debug('!!!!!!!rowToRemove'+rowToRemove);
        List<SelectedNewPaymentTermsByCRE> existingPT = lstNewPaymentTermsDisplay;
        lstNewPaymentTermsDisplay = new List<SelectedNewPaymentTermsByCRE>();
        Integer rowNo = 0;
        for (SelectedNewPaymentTermsByCRE objSelectNewPaymentTerm : existingPT ) {
            rowNo ++;
            if (objSelectNewPaymentTerm.isReceiptPresent == true) {
                lstNewPaymentTermsDisplay.add(objSelectNewPaymentTerm);
             } else if (rowNo == Integer.valueOf(rowToRemove)-1) {
                 SelectedNewPaymentTermsByCRE objNewPaymentTerms = new SelectedNewPaymentTermsByCRE();
                 objNewPaymentTerms.installment = objSelectNewPaymentTerm.installment ;
                 objNewPaymentTerms.description = objSelectNewPaymentTerm.description ;
                 objNewPaymentTerms.mileStoneEvent = objSelectNewPaymentTerm.mileStoneEvent;
                 objNewPaymentTerms.percentValue = objSelectNewPaymentTerm.percentValue;
                 objNewPaymentTerms.paymentAmount = objSelectNewPaymentTerm.paymentAmount;
                 objNewPaymentTerms.mileStoneArabic = objSelectNewPaymentTerm.mileStoneArabic;                   
                 objNewPaymentTerms.paymentDate = objSelectNewPaymentTerm.paymentDate;                 
                 objNewPaymentTerms.lineId = objSelectNewPaymentTerm.lineId;                
                 objNewPaymentTerms.isReceiptPresent = false;
                 objNewPaymentTerms.termId = objSelectNewPaymentTerm.termId;
                 lstNewPaymentTermsDisplay.add(objNewPaymentTerms);      
            } else if (rowNo == Integer.valueOf(rowToRemove)) {
                 Integer updateRowNo = rowNo - 1;
                 SelectedNewPaymentTermsByCRE objNewPaymentTerms = new SelectedNewPaymentTermsByCRE();
                 if (updateRowNo <=9) {
                     objNewPaymentTerms.installment = 'I00'+updateRowNo ;
                 } else if (updateRowNo > 9) {
                     objNewPaymentTerms.installment = 'I0'+updateRowNo ;
                 }
                 if(updateRowNo == 1)
                 {
                     objNewPaymentTerms.description = updateRowNo +'ST INSTALLMENT';
                 }
                 else if(updateRowNo == 2)
                 {
                     objNewPaymentTerms.description = updateRowNo +'ND INSTALLMENT';
                 }
                 else if(updateRowNo == 3)
                 {
                     objNewPaymentTerms.description = updateRowNo +'RD INSTALLMENT';
                 }
                 else
                 {
                     objNewPaymentTerms.description = updateRowNo +'TH INSTALLMENT';
                 }
                 objNewPaymentTerms.mileStoneEvent = 'On or Before';
                 objNewPaymentTerms.percentValue = '0';                           
                 objNewPaymentTerms.isReceiptPresent = false;
                 Date dueDate = system.today();
                 objNewPaymentTerms.paymentDate = dueDate.format();                 
                 lstNewPaymentTermsDisplay.add(objNewPaymentTerms);
                 SelectedNewPaymentTermsByCRE objNewPaymentTerms1 = new SelectedNewPaymentTermsByCRE();
                 if (rowNo <=9) {
                     objNewPaymentTerms1.installment = 'I00'+rowNo ;
                 } else if (rowNo > 9) {
                     objNewPaymentTerms1.installment = 'I0'+rowNo ;
                 }
                 if(rowNo == 1)
                 {
                     objNewPaymentTerms1.description = rowNo +'ST INSTALLMENT';
                 }
                 else if(rowNo == 2)
                 {
                     objNewPaymentTerms1.description = rowNo +'ND INSTALLMENT';
                 }
                 else if(rowNo == 3)
                 {
                     objNewPaymentTerms1.description = RowNo+'RD INSTALLMENT';
                 }
                 else
                 {
                     objNewPaymentTerms1.description = rowNo +'TH INSTALLMENT';
                 }
                 objNewPaymentTerms1.mileStoneEvent = objSelectNewPaymentTerm.mileStoneEvent;
                 objNewPaymentTerms1.percentValue = objSelectNewPaymentTerm.percentValue;
                 objNewPaymentTerms1.paymentAmount = objSelectNewPaymentTerm.paymentAmount;
                 objNewPaymentTerms1.mileStoneArabic = objSelectNewPaymentTerm.mileStoneArabic;                   
                 objNewPaymentTerms1.paymentDate = objSelectNewPaymentTerm.paymentDate;
                 objNewPaymentTerms1.lineId = objSelectNewPaymentTerm.lineId;                
                 objNewPaymentTerms1.isReceiptPresent = false;
                 objNewPaymentTerms1.termId = objSelectNewPaymentTerm.termId;
                 lstNewPaymentTermsDisplay.add(objNewPaymentTerms1);
            } else if (rowNo > Integer.valueOf(rowToRemove)) {
                 //Integer updatedRowNo = rowNo + 1;
                 SelectedNewPaymentTermsByCRE objNewPaymentTerms = new SelectedNewPaymentTermsByCRE();
                 if (rowNo <=9) {
                     objNewPaymentTerms.installment = 'I00'+rowNo ;
                 } else if (rowNo >9) {
                     objNewPaymentTerms.installment = 'I0'+rowNo ;
                 }
                 if(rowNo == 1)
                 {
                     objNewPaymentTerms.description = rowNo +'ST INSTALLMENT';
                 }
                 else if(rowNo == 2)
                 {
                     objNewPaymentTerms.description = rowNo +'ND INSTALLMENT';
                 }
                 else if(rowNo == 3)
                 {
                     objNewPaymentTerms.description = RowNo+'RD INSTALLMENT';
                 }
                 else
                 {
                     objNewPaymentTerms.description = rowNo +'TH INSTALLMENT';
                 }
                 objNewPaymentTerms.mileStoneEvent = objSelectNewPaymentTerm.mileStoneEvent;
                 objNewPaymentTerms.percentValue = objSelectNewPaymentTerm.percentValue;
                 objNewPaymentTerms.paymentAmount = objSelectNewPaymentTerm.paymentAmount;
                 objNewPaymentTerms.mileStoneArabic = objSelectNewPaymentTerm.mileStoneArabic;                   
                 objNewPaymentTerms.paymentDate = objSelectNewPaymentTerm.paymentDate;
                 objNewPaymentTerms.lineId = objSelectNewPaymentTerm.lineId;                
                 objNewPaymentTerms.isReceiptPresent = false;
                 objNewPaymentTerms.termId = objSelectNewPaymentTerm.termId;
                 lstNewPaymentTermsDisplay.add(objNewPaymentTerms);
            }
        }
    }
    
    public pageReference validationOnPaymentTerm() {
        system.debug('!!!!!!!updatedPT'+updatedPT);
        errorPDCMessage = '';
        map<String, SelectedNewPaymentTermsByCRE> mapPT = new map<String, SelectedNewPaymentTermsByCRE>();
        for (SelectedNewPaymentTermsByCRE objSelectNewPaymentTerm : lstNewPaymentTermsDisplay) {
            mapPT.put(objSelectNewPaymentTerm.installment, objSelectNewPaymentTerm);
        }
        SelectedNewPaymentTermsByCRE objTerm;
        
        if (!String.isBlank(updatedPT) && mapPT != null && mapPT.containsKey(updatedPT)) {
            objTerm = mapPT.get(updatedPT);
            system.debug('!!!!!!!!!objTerm'+objTerm);
            Date PaymentDate;
            Date EndDate = system.today().addDays(30);
            system.debug('!!!!!EndDate'+EndDate);
            if (objTerm != null && !String.isBlank(objTerm.paymentDate)) {
                system.debug('!!!!!!!!objTerm.paymentDate'+objTerm.paymentDate);
                PaymentDate = generateDateFromString(objTerm.paymentDate);
                //PaymentDate = objTerm.paymentDate;
                system.debug('!!!!!!!!PaymentDate'+PaymentDate);
                if (PaymentDate < system.today()) {
                    errorPDCMessage = 'Payment Date can not be smaller than Today';
                    system.debug('!!!!!!errorPDCMessage'+errorPDCMessage );
                } else if (PaymentDate > EndDate ) {
                    errorPDCMessage = 'Payment Date can not be greater than 30 days from Today';
                }
            } 
        }
        return null;
    }
    
    public PageReference submitCase() {
        list<Case> lstCase = new list<case>();
        Id EHOCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        Id HOCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Id ParentHOCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover Parent').getRecordTypeId();        
        
        //Parent Handover case
        Case parentHO = new Case();
        if (caseObj.parentId != null && caseObj.RecordTypeId == HOCaseRecordTypeId) {
            parentHO.Id = caseObj.parentId;
        }
        parentHO.AccountId = objBookingUnit.Booking__r.Account__c;
        parentHO.RecordTypeId = ParentHOCaseRecordTypeId;
        parentHO.Booking_Unit__c = objBookingUnit.Id;
        if (isDraftCase == true) {
            parentHO.Status = 'Draft';
        } else {
            parentHO.Status = 'Submitted';
            parentHO.Early_Settlement_Handover__c = true;
        }
        upsert parentHO;
        
        Case caseHO = new Case();
        if (caseObj.RecordTypeId == HOCaseRecordTypeId) {
            caseHO.Id = caseObj.Id;
        }
        caseHO.AccountId = objBookingUnit.Booking__r.Account__c;
        caseHO.RecordTypeId = HOCaseRecordTypeId;
        caseHO.Booking_Unit__c = objBookingUnit.Id;
        caseHO.ParentId = parentHO.Id;
        if (lstNewPaymentTermsDisplay != null) {
            system.debug('!!!!!lstNewPaymentTermsDisplay'+lstNewPaymentTermsDisplay);
            list<MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE > lstPT = paymentPlan();
            if (lstPT != null) {
                caseHO.NewPaymentTermJSON__c = JSON.serialize(lstPT);
            }
        }
        if (isDraftCase == true) {
            caseHO.Status = 'Draft';
        } else {
            caseHO.Status = 'Submitted';
            caseHO.Early_Settlement_Handover__c = true;
        }
        upsert caseHO;
        
        if (caseObj.RecordTypeId == EHOCaseRecordTypeId ) {
            Case EhoCase = new Case (Id = caseObj.Id);
            EhoCase.Status = 'Cancelled';
            update EhoCase;
        }
        Pagereference pgRef;
        if (caseHO.Id != null) {
            pgRef = new Pagereference('/'+caseHO.Id);
        } else {
            pgRef = new Pagereference('/500');
        }
        return pgRef;
    }
    
    public PageReference draftCase() {
        isDraftCase = true;
        submitCase();
        return null;
    }
    
     public list<MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE> paymentPlan() {
          list<MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE> lstPaymentTerm = new list<MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE>();
           for (SelectedNewPaymentTermsByCRE PT : lstNewPaymentTermsDisplay) {
               MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE objPaymentTerm = new MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE();
               objPaymentTerm.REGISTRATION_ID = objBookingUnit.Registration_ID__c;
               objPaymentTerm.DUE_DATE = PT.paymentDate;
               objPaymentTerm.MILESTEON_PERCENT_VALUE = PT.percentValue;
               objPaymentTerm.PAID_AMOUNT = PT.paymentAmount;
               if (!string.isBlank(PT.paymentAmount)) {
                   objPaymentTerm.INVOICE_AMOUNT = PT.paymentAmount;
               } else if (!string.isBlank(PT.percentValue) && objBookingUnit.Requested_Price__c!= null) {
                   objPaymentTerm.INVOICE_AMOUNT = String.valueOf((decimal.valueOf(PT.percentValue)/100)*objBookingUnit.Requested_Price__c);
               }
               objPaymentTerm.LINE_ID = PT.lineId;
               objPaymentTerm.MILESTONE_EVENT_AR = PT.mileStoneArabic;
               objPaymentTerm.DESCRIPTION = PT.description;
               objPaymentTerm.MILESTONE_EVENT = PT.mileStoneEvent;            
               objPaymentTerm.INSTALLMENT = PT.installment;
               objPaymentTerm.TERM_ID = PT.termId;
               lstPaymentTerm.add(objPaymentTerm);
           }
          return lstPaymentTerm;
     }
     public string getConvertDateTime(string strDT) {
        Map<string,integer> MapMonthList = new Map<string,integer>();
        MapMonthList.put('JAN',01);
        MapMonthList.put('FEB',02);
        MapMonthList.put('MAR',03);
        MapMonthList.put('APR',04);
        MapMonthList.put('MAY',05);
        MapMonthList.put('JUN',06);
        MapMonthList.put('JUL',07);
        MapMonthList.put('AUG',08);
        MapMonthList.put('SEP',09);
        MapMonthList.put('OCT',10);
        MapMonthList.put('NOV',11);
        MapMonthList.put('DEC',12);
        String[] strDTDivided = strDT.split('-');
        string month = String.ValueOf(MapMonthList.get(strDTDivided.get(1)));
        string day = strDTDivided.get(0).replace('-', '');
        string year = strDTDivided.get(2);
        string stringDate = day + '/' + month + '/' + year;
        return stringDate;
    }
    
    public date generateDateFromString(String strDate){
        List<String> parts = strDate.split('/');
        system.debug('!!!!integer.valueOf(parts[2])'+integer.valueOf(parts[2]));
        system.debug('!!!!integer.valueOf(parts[1])'+integer.valueOf(parts[1]));
        system.debug('!!!!integer.valueOf(parts[0])'+integer.valueOf(parts[0]));
        return date.newinstance(integer.valueOf(parts[2]), integer.valueOf(parts[1]), integer.valueOf(parts[0]));
    }
    
    public class SelectedNewPaymentTermsByCRE {
        public String installment {get;set;}
        public String Id {get;set;}
        public Boolean isReceiptPresent {get;set;}
        public String description {get;set;}
        public String mileStoneEvent {get;set;}
        public String mileStoneArabic {get;set;}
        public String percentValue {get;set;}
        public String paymentAmount {get;set;}
        public String paymentDate {get;set;}
        public String termId {get;set;}
        public String lineId {get;set;}
        public Boolean isPDC {get;set;}
        //public List<SelectOption> lstStructureBasedPP {get;set;}
    }
}