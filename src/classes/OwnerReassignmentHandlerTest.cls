@isTest
public class OwnerReassignmentHandlerTest {
    @isTest
    static void testUpdateCase(){
        
        Case objCase = new Case();
        objCase.Customer_First_Name__c = 'test';
        objCase.Customer_Last_Name__c = 'test';
       
        insert objCase;
        
        TriggerOnOffCustomSetting__c objTrigger = new TriggerOnOffCustomSetting__c();
        objTrigger.Name = 'CallingListTrigger';
        objTrigger.OnOffCheck__c = true;
        insert objTrigger;
        
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Unit_Name__c = 'test/test';
        objCallingList.Service_Type__c = 'Handover';
        objCallingList.Sub_Purpose__c = 'Unit Viewing';
        insert objCallingList;
        
        Test.startTest();
            objCase.OwnerId = Label.Admin_User_Id;
            update objCase;
            
            objCallingList.OwnerId = Label.Admin_User_Id;
            update objCallingList;
        Test.stopTest();
    }
    
}