/************************************************************************************************
 * @Name              : EsignReservationFormController 
 * @Test Class Name   : EsignReservationFormController_Test 
 * @Description       : Controller for Esign Reservation Form PDF
 * Modification Log
 * 1.0    Srikanth    03/08/2020        Created Class
 ************************************************************************************************/

public without sharing class EsignReservationFormController { 
    public string bookingUnitId{get; set;}
    public Booking_Unit__c bookingUnit {get; set;}
    public List<Buyer__c> buyersDetails {get; set;}
    public Buyer__c primaryBuyer {get; set;}
    public Inventory__c selInventory {get; set;} 
    public String sellingPriceinWords {get; set;}
    public String grossPriceinWords {get; set;}
    public String currentUser {get;set;}
    public String currentUserEmail {get; set;}
    public String userCurrentTime { get; set; }
    public Integer dd { get; set; }
    public Integer mon { get; set; }
    public Integer year { get; set; }
    
    public EsignReservationFormController(ApexPages.StandardController controller) {
        try {
            buyersDetails = new List<Buyer__c>();
            primaryBuyer = new Buyer__c();
            selInventory = new Inventory__c(); 
            sellingPriceinWords = '';
            grossPriceinWords = '';
            bookingUnitId = Apexpages.currentPage().getParameters().get('recId');
            if(bookingUnitId != null && bookingUnitId != ''){
                fetchMappings(bookingUnitId);
            }
        } catch (Exception e) {
            system.debug('Exception: ' + e);
        }
    }
    public void fetchMappings(String bookingUnitId) {
        bookingUnit = [SELECT Booking__c, Booking__r.Total_Booking_Amount__c, Inventory__c, Payment_Method__c,
                              Token_Amount__c, Token_Paid_Date_and_Time__c, Booking_Type__c,Selling_Price__c
                              
                       FROM Booking_Unit__c WHERE Id =: bookingUnitId];
        system.debug('bookingUnit : ' + bookingUnit);
        sellingPriceinWords = getNumberTOWordConvertion(bookingUnit.Selling_Price__c);
        grossPriceinWords =  getNumberTOWordConvertion(bookingUnit.Token_Amount__c);

        for(Buyer__c buyerRec : [SELECT Account__c, Nationality__c, First_Name__c, Middle_Name__c, 
                                        Last_Name__c, Email__c, Passport_Number__c, Primary_Buyer__c, Buyer_Type__c,
                                        Address_Line_1__c, City__c, Country__c, Phone__c, Buyer_Solicitor_Mobile_Number__c,
                                        CR_Registration_Place__c, CR_Number__c, Account__r.Name, 
                                        Account__r.Agency_Email__c, Account__r.VAT_Registration_Certificate_Date__c 
                                 FROM Buyer__c WHERE Booking__c =:bookingUnit.Booking__c]) 
        {
            buyersDetails.add(buyerRec); 
            if(buyerRec.Primary_Buyer__c) { 
                primaryBuyer = buyerRec;
            }
        }
        
        selInventory = [SELECT Property_City__c, Unit__c, Area_Sqft_2__c, Property_Type__c, Project_Category__c, IPMS_Bedrooms__c
                        FROM Inventory__c WHERE Id =: bookingUnit.Inventory__c];
        
        
        userCurrentTime = System.Now().format()+':'+System.Now().Second();
        currentUser = UserInfo.getName();
        currentUserEmail = UserInfo.getUserEmail();
        dd = System.today().day();
        mon = System.today().month();
        year = System.today().year();
               
    }
    
        public String getNumberTOWordConvertion(Decimal num) {

        Decimal junkVal = num;
        Decimal junkValPaisa = junkVal - Math.floor(junkVal);
        junkVal = Math.floor(junkVal);

        String obStr = junkVal.toPlainString();
        String[] numReversed = obStr.split('');
        String[] actnumber = reverse(numReversed);
        String firstHalf = convertInWords(numReversed, actnumber);

        Integer tmp = Math.round(junkValPaisa * 100);
        junkValPaisa = (Decimal)tmp / 100; System.debug('jj :' + junkValPaisa);
        String paisaStr = junkValPaisa.toPlainString();
        String secondHalf;
        if (paisaStr == '0') {
            secondHalf = '';
        } else if (paisaStr.length() != 4) {
            paisaStr = paisaStr + '0';
            paisaStr = paisaStr.substring(2);
            String [] numReversedPaisa = paisaStr.split('');
            String[] actnumberPaisa = reverse(numReversedPaisa);
            secondHalf = convertInWords(numReversedPaisa, actnumberPaisa);
        } else {
            paisaStr = paisaStr.substring(2);
            String [] numReversedPaisa = paisaStr.split('');
            String[] actnumberPaisa = reverse(numReversedPaisa);
            secondHalf = convertInWords(numReversedPaisa, actnumberPaisa);
        }

        String SumOFHalves = '';

        if (secondHalf.length() > 4) {
            firstHalf = firstHalf.replace('Only', 'AED And ');
            secondHalf = secondHalf.replace('Only', 'Paisa Only');
            SumOFHalves = firstHalf + secondHalf;
        } else {
            firstHalf = firstHalf.replace('Only', 'AED Only');
            SumOFHalves = firstHalf;
        }

        // IF amount has any value
        if (SumOFHalves.length() > 5) {
            return SumOFHalves;
        } else {
            return '';
        }
    }
    // Method reverse the number
    public List<String> reverse(List<String> strToRev) {
        List<String> revList = new List<String>();
        for (Integer i = strToRev.size() - 1; i >= 0; i--) {
            revList.add(strToRev.get(i));
        }
        revList.add('');
        return revList;
    }

    public String convertInWords(String[] numRev, String[] actnum) {
        List<String> iWords = new List<String> {'Zero', ' One', ' Two', ' Three', ' Four', ' Five', ' Six', ' Seven', ' Eight', ' Nine'};
        List<String> ePlace = new List<String> {' Ten', ' Eleven', ' Twelve', ' Thirteen', ' Fourteen', ' Fifteen', ' Sixteen', ' Seventeen', ' Eighteen', ' Nineteen'};
        List<String> tensPlace = new List<String> {'dummy', ' Ten', ' Twenty', ' Thirty', ' Forty', ' Fifty', ' Sixty', ' Seventy', ' Eighty', ' Ninety' };

        Integer iWordsLength = numRev.size();
        String totalWords = '';
        List<String> inWords = new List<String>();
        for (Integer k = 0; k < iWordsLength; k++) {
            inWords.add('');
        }
        String finalWord = '';
        Integer j = 0;

        // Main For loop
        for (Integer i = 0; i < iWordsLength; i++) {

            if (i == 0) {
                if (actnum[i] == '0' || actnum[i + 1] == '1') {
                    inWords[j] = '';
                } else {
                    inWords[j] = iWords[Integer.valueof(actnum[i])];
                }
                inWords[j] = inWords[j] + ' Only';
            } else if (i == 1) {

                if (actnum[i] == '0') {
                    inWords[j] = '';
                } else if (actnum[i] == '1') {
                    inWords[j] = ePlace[Integer.valueof(actnum[i - 1])];
                } else {
                    inWords[j] = tensPlace[Integer.valueof(actnum[i])];
                }
            } else if (i == 2) {
                if (actnum[i] == '0') {
                    inWords[j] = '';
                } else if (actnum[i - 1] != '0' && actnum[i - 2] != '0') {
                    inWords[j] = iWords[Integer.valueof(actnum[i])] + ' Hundred and';
                } else {
                    inWords[j] = iWords[Integer.valueof(actnum[i])] + ' Hundred';
                }
            } else if (i == 3) {
                if (actnum[i] == '0' || actnum[i + 1] == '1') {
                    inWords[j] = '';
                } else {
                    inWords[j] = iWords[Integer.valueof(actnum[i])];
                }
                if (actnum[i + 1] != '0' || Integer.valueof(actnum[i]) > 0) {
                    inWords[j] = inWords[j] + ' Thousand';
                }
            } else if (i == 4) {

                if (actnum[i] == '0') {
                    inWords[j] = '';
                } else if (actnum[i] == '1') {
                    inWords[j] = ePlace[Integer.valueof(actnum[i - 1])];
                } else {
                    inWords[j] = tensPlace[Integer.valueof(actnum[i])];
                }

            } else if (i == 5) {
                if (actnum[i] == '0' || actnum[i + 1] == '1') {
                    inWords[j] = '';
                } else {
                    inWords[j] = iWords[Integer.valueof(actnum[i])];
                }
                if (actnum[i + 1] != '0' || Integer.valueof(actnum[i]) > 0) {
                    inWords[j] = inWords[j] + ' Lakh';
                }
            } else if (i == 6) {

                if (actnum[i] == '0') {
                    inWords[j] = '';
                } else if (actnum[i] == '1') {
                    inWords[j] = ePlace[Integer.valueof(actnum[i - 1])];
                } else {
                    inWords[j] = tensPlace[Integer.valueof(actnum[i])];
                }

            } else if (i == 7) {
                if (actnum[i] == '0' || actnum[i + 1] == '1' ) {
                    inWords[j] = '';
                } else {
                    inWords[j] = iWords[Integer.valueof(actnum[i])];
                }
                inWords[j] = inWords[j] + ' Crore';
            } else if (i == 8) {

                if (actnum[i] == '0') {
                    inWords[j] = '';
                } else if (actnum[i] == '1') {
                    inWords[j] = ePlace[Integer.valueof(actnum[i - 1])];
                } else {
                    inWords[j] = tensPlace[Integer.valueof(actnum[i])];
                }

            }

            j++;
        }
        // End of For loop

        // Reverse the List
        inWords = reverse(inWords);

        for (Integer i = 0; i < inWords.size(); i++) {
            finalWord += inWords[i];
        }

        return finalWord;
    }

    
}