public without sharing class CustomerNotificationSendSMSBatch implements Database.Batchable<SObject>,
                                                                         Database.AllowsCallouts,
                                                                         Database.Stateful {

  private List<SMS_History__c> lstSMSHistory ;
  private List<SMS_History__c> lstSMSHistoryIds;

  public CustomerNotificationSendSMSBatch(List<SMS_History__c> plstSMSHistoryIds) {
    lstSMSHistoryIds = new list<SMS_History__c>();
    lstSMSHistoryIds.addAll(plstSMSHistoryIds);
    lstSMSHistory = new list<SMS_History__c>();
  }
  public CustomerNotificationSendSMSBatch() {
      lstSMSHistoryIds = new list<SMS_History__c>();
      lstSMSHistory = new list<SMS_History__c>();
  }

    public Database.Querylocator start( Database.BatchableContext bc ) {
       system.debug('== Inside the CustomerNotificationSendSMSBatch start ==');
       if( ! lstSMSHistoryIds.isEmpty()) {
           return Database.getQueryLocator( 'SELECT Id, Phone_Number__c,FM_Case__c , Message__c, Is_SMS_Sent__c FROM SMS_History__c WHERE Is_SMS_Sent__c = false AND ID IN : lstSMSHistoryIds' );
       }
       else {
           return Database.getQueryLocator( 'SELECT Id, Phone_Number__c,FM_Case__c , Message__c, Is_SMS_Sent__c FROM SMS_History__c WHERE Is_SMS_Sent__c = false AND FM_Case__c != NULL AND FM_Case__r.RecordType.DeveloperName = \'Tenant_Registration\' AND CreatedDate = TODAY AND (NOT Name LIKE \'Portal Tenant Registration Verification%\')' );
       }
    }

  public void execute( Database.BatchableContext bc, list<SMS_History__c> lstSMSToSend ) {
      //system.debug('== lstSMSToSend =='+lstSMSToSend);
      if( lstSMSToSend != null && !lstSMSToSend.isEmpty() ) {
        for( SMS_History__c objSMS : lstSMSToSend ) {
          system.debug('== objSMS =='+objSMS);
          system.debug('== objSMS.Is_SMS_Sent__c =='+objSMS.Is_SMS_Sent__c);
          system.debug('== objSMS.Message__c =='+objSMS.Message__c);
          if( objSMS.Is_SMS_Sent__c == false && String.isNotBlank( objSMS.Message__c ) ) {
            system.debug('== objSMS =='+objSMS);
            lstSMSHistory.add( CustomerNotificationBatchHelper.sendSMSToCustomer( objSMS ) ) ;
          }
        }
      }
  }

  public void finish( Database.BatchableContext bc ) {
    if( lstSMSHistory != null && lstSMSHistory.size() >0) {
        system.debug('!!!!lstSMSHistory'+lstSMSHistory);
      Database.SaveResult [] updateResult = Database.update(lstSMSHistory, false);
    }
    if (!Test.isRunningTest()) {
      System.scheduleBatch( new CustomerNotificationSendSMSBatch(), 'TR Sending SMS Batch ' + system.now(), 5, 100);
    } else {
      return;
    }
  }
}