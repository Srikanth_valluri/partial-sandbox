@isTest
public class GetMyUnitBookingsTest {
      public  testmethod static void method()
    {
        Account a=new Account();
        a.Name='test';
        insert a;
        NSIBPM__Service_Request__c cs=new NSIBPM__Service_Request__c();
        insert cs;
        Booking__c bo=new Booking__c();
        bo.Account__c=a.id;
        bo.Deal_SR__c=cs.id;
        insert bo;
        
        Booking_Unit__c b=new Booking_Unit__c();
        b.Booking__c=bo.id;
        b.Unit_Name__c='test';
        insert b;
       GetMyUnitBookings.GetMyUnitBooking(a.id);

    }

}