@isTest
global class RESTCalloutServiceMockCallingList implements HttpCalloutMock {

   public integer intResponseNumber ;
    
    public RESTCalloutServiceMockCallingList( integer intNum ) {
        intResponseNumber = intNum ;
    }

    public RESTCalloutServiceMockCallingList() 
    {
    }

    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('http://api.smscountry.com/SMSCwebservice_bulk.aspx', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('SMS message(s) sent');
        if(intResponseNumber == 1)
        {
          res.setBody('OK:1213');
        }
        if(intResponseNumber == 2)
        {
          res.setBody('Error while sending SMS');
        }
        res.setStatusCode(200);
        return res;
    }

}