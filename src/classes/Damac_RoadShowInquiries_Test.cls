@isTest
public with sharing class Damac_RoadShowInquiries_Test {

    static testmethod void testRoadShow () {
        String digitalRecordType = DamacUtility.getRecordTypeId('Campaign__c', 'Roadshows');
        Campaign__c campaignObject = new Campaign__c(RecordTypeId=digitalRecordType,
                                        End_Date__c=Date.Today(), 
                                        Marketing_Active__c = true,
                                        Campaign_Name__c = 'Test',
                                        Credit_Control_Active__c = true,
                                        Sales_Admin_Active__c = true,
                                        Marketing_End_Date__c=Date.Today(), 
                                        Marketing_Start_Date__c=Date.Today(), 
                                        Start_Date__c=Date.Today());
        insert campaignObject;
         
        Assigned_PC__c assign = new Assigned_PC__c ();
        assign.User__c = userinfo.getUserId();
        assign.Campaign__c = campaignObject.Id;
        insert assign;
        
        ApexPages.standardController stdController = new ApexPages.standardController(new Inquiry__c());
        Damac_RoadShowInquiries obj = new Damac_RoadShowInquiries (stdController);
        obj.save();
        obj.cancel();
    }
}