/***********************************************************************************************
* Name               : PromotionSetupController
* Test Class         : PromotionSetupControllerTest
* Description        : Controller class for PromotionSetup VF Page
* Created Date       : 11/01/2021
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         11/01/2021      Initial Draft.
**************************************************************************************************/
global With Sharing class PromotionSetupController {

    public string promoListJSON {get; set;}
    public Promotion_Setup__c promo {get; set;}
    public String jsonResult { get; set; }
    public String jsonResultExec { get; set; }
    public String fieldJsonResult { get; set; }
    public String fieldJsonResultExec { get; set; }
    public Map<String, Boolean> isValueListEmpty {get; set;}
    public Map<String, Boolean> isValueListEmptyExec {get; set;}
    public Promotion_Setup__c selectedPromo {get; set;}
    public List<Promotion_Eligibility_Criteria__c> eligCriteriaList {get; set;}
    public List<Promotion_Execution_Criteria__c> execCriteriaList {get; set;}
    public Map<String, List<String>> valueMap {get; set;}
    public Map<String, List<String>> criteriaValueMap {get; set;}
    public Map<String, List<String>> criteriaValueMapExec {get; set;}
    public Map<String, List<SelectOption>> valueOptionMap {get; set;}
    public Map<String, List<SelectOption>> valueOptionMapExec {get; set;}
    public Map<String, List<String>> valueMapExec {get; set;}
    public string msg {get; set;}
    public map<String, String> valueTypeMap {get; set;}
    public map<String, String> valueTypeMapExec {get; set;}
    public map<String, List<String>> invFilterValueMap {get; set;}
    public map<String, List<String>> apiinvFilterValueMap {get; set;}
    public map<String, List<String>> apiinvFilterValueMapExec {get; set;}
    public string msgType {get; set;}
    public Blob attachmentBody {get; set;}
    public String attachmentName {get; set;}
    public Map<String, Map <String, String>> objFieldAPIMap {get; set;}
    public Map<String, Map <String, String>> objFieldAPIMapExec {get; set;}

    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public PromotionSetupController() {}
    public PromotionSetupController(ApexPages.StandardController controller) {
        msg = '';
        msgType = '';
        jsonResult = '';
        jsonResultExec = '';
        fieldJsonResult = '';
        fieldJsonResultExec = '';
        valueOptionMap = new Map<String, List<SelectOption>>();
        valueOptionMapExec = new Map<String, List<SelectOption>>();
        isValueListEmpty = new Map<String, Boolean>();
        isValueListEmptyExec = new Map<String, Boolean>();
        objFieldAPIMap = new Map<String, Map <String, String>>();
        objFieldAPIMapExec = new Map<String, Map <String, String>>();
        valueMap = new Map<String, List<String>>();
        valueMapExec = new Map<String, List<String>>();
        invFilterValueMap  = new Map<String, List<String>>();
        apiinvFilterValueMap  = new Map<String, List<String>>();
        apiinvFilterValueMapExec  = new Map<String, List<String>>();
        criteriaValueMap = new Map<String, List<String>>(); 
        criteriaValueMapExec  = new Map<String, List<String>>(); 
        valueTypeMap = new Map<String, String>(); 
        valueTypeMapExec = new Map<String, String>(); 
        eligCriteriaList = new List<Promotion_Eligibility_Criteria__c>();
        execCriteriaList = new List<Promotion_Execution_Criteria__c>();
        promo = new Promotion_Setup__c(Signed_Promotion_Letter_Required__c='Yes');
        selectedPromo = new Promotion_Setup__c();
        List<Promotion_Setup__c> promoList = new List<Promotion_Setup__c> ();
        promoListJSON = JSON.serialize(promoList);
        for(Inventory_Filter__c filter: [SELECT Id, Name, Filter_Values__c
                                        FROM Inventory_Filter__c ]){
            if(filter.Filter_Values__c != null && filter.Filter_Values__c != ''){
                invFilterValueMap.put(filter.Name, filter.Filter_Values__c.split(';'));
            }
        }
        initialSearchQuery();
    }

    /*********************************************************************************************
    * @Description : Selecting a given Promotion Setup record using Record Id
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public void selectPromo() {
        String promoId = ApexPages.currentPage().getParameters().get('selPromoId'); 
        if(promoId != null && promoId != ''){
            eligCriteriaList = new List<Promotion_Eligibility_Criteria__c>();
            execCriteriaList = new List<Promotion_Execution_Criteria__c>();
            selectedPromo = [SELECT Id, Name, Cost_to_DAMAC__c,
                                Eligibility_Criteria_Filter_Logic__c,
                                End_Date__c, Execution_Criteria_Filter_Logic__c, 
                                Promotion_Name__c, Type__c,Status__c,
                                Promotion_Type__c, Start_Date__c, Sub_Type__c, 
                                Active__c, LastModifiedBy.Name, LastModifiedDate,
                                Template_Id__c, Adjust_Cost_to_Price__c,
                                Eligibility_Inventory_Filter_Logic__c
                              FROM Promotion_Setup__c 
                              WHERE Id  =: promoId
                              LIMIT 1];
            for(Promotion_Eligibility_Criteria__c 
                    criteria:  [SELECT Id, Name, Condition__c,
                                    Object__c, Order__c,
                                    Parameter__c, Promotion_Setup__c,
                                    Value__c, Value_Type__c
                                 FROM Promotion_Eligibility_Criteria__c
                                 WHERE Promotion_Setup__c =: promoId
                                 ORDER BY Order__c]){
                eligCriteriaList.add(criteria);
                fetchobjFieldAPIMap(criteria.Object__c, String.valueOf(criteria.Order__c));
                List<String> valueList = new List<String>();
                List<String> criteriaValueList = new List<String>();
                if(criteria.Value__c != null && criteria.Value__c != ''){
                    for(String criteriaVal: criteria.Value__c.split(';')){
                        criteriaValueList.add(criteriaVal);
                    }
                }
                if(apiinvFilterValueMap.containsKey(criteria.Parameter__c) 
                        && apiinvFilterValueMap.get(criteria.Parameter__c) != null){
                    valueList = apiinvFilterValueMap.get(criteria.Parameter__c);
                    valueList.sort();
                } 
                if(valueList.size() > 0){
                    isValueListEmpty.put(String.valueOf(criteria.Order__c), false);
                } else {
                    isValueListEmpty.put(String.valueOf(criteria.Order__c), true);
                }
                criteriaValueMap.put(String.valueOf(criteria.Order__c), criteriaValueList);
                valueMap.put(String.valueOf(criteria.Order__c), valueList);
                setValueOptions(valueMap);
            }
            for(Promotion_Execution_Criteria__c 
                    criteria:  [SELECT Id, Name, Condition__c,
                                    Object__c, Order__c,
                                    Parameter__c, Promotion_Setup__c,
                                    Value__c, Value_Type__c
                                 FROM Promotion_Execution_Criteria__c
                                 WHERE Promotion_Setup__c =: promoId
                                 ORDER BY Order__c]){
                execCriteriaList.add(criteria);
                fetchobjFieldAPIMapExec(criteria.Object__c, String.valueOf(criteria.Order__c));
                List<String> valueList = new List<String>();
                if(apiinvFilterValueMapExec.containsKey(criteria.Parameter__c) 
                        && apiinvFilterValueMapExec.get(criteria.Parameter__c) != null){
                    valueList = apiinvFilterValueMapExec.get(criteria.Parameter__c);
                    valueList.sort();
                } 
                List<String> criteriaValueList = new List<String>();
                if(criteria.Value__c != null && criteria.Value__c != ''){
                    for(String criteriaVal: criteria.Value__c.split(';')){
                        criteriaValueList.add(criteriaVal);
                    }
                }
                if(valueList.size() > 0){
                    isValueListEmptyExec.put(String.valueOf(criteria.Order__c), false);
                } else {
                    isValueListEmptyExec.put(String.valueOf(criteria.Order__c), true);
                }
                criteriaValueMapExec.put(String.valueOf(criteria.Order__c), criteriaValueList);
                valueMapExec.put(String.valueOf(criteria.Order__c), valueList);
                setValueOptionsExec(valueMapExec);
            }
            fieldJsonResult = JSON.serialize(valueMap);
            system.debug('fieldJsonResult: '+ fieldJsonResult);
            system.debug('execCriteriaList: '+ execCriteriaList);
            system.debug('objFieldAPIMap:' + objFieldAPIMap);
            system.debug('valueMap: ' + valueMap);
            fieldJsonResultExec = JSON.serialize(valueMapExec);
            system.debug('fieldJsonResultExec: '+ fieldJsonResultExec);
            system.debug('objFieldAPIMapExec:' + objFieldAPIMapExec);
            system.debug('valueMapExec: ' + valueMapExec);
        }
    }

    /************************************************************************************
    * @Description : Adding new Criteria to the list of Eligibility Criterias
    * @Params      : void
    * @Return      : void
    ***************************************************************************************/
    public void addNewEligCriteria() {
        if(eligCriteriaList.size() > 0){
            system.debug('eligCriteriaList:'  + eligCriteriaList);
            system.debug('valueTypeMap:' + valueTypeMap);
            for(Promotion_Eligibility_Criteria__c criteria: eligCriteriaList){
                 if (criteria.Parameter__c != NULL) {
                    String key = criteria.Object__c + '-' + criteria.Parameter__c;
                     system.debug('key:' + key);
                    if(valueTypeMap.containsKey(key) && valueTypeMap.get(key) != null){
                        criteria.Value_Type__c = valueTypeMap.get(key);
                    }
                }
            }
            upsert eligCriteriaList;
            eligCriteriaList = new List<Promotion_Eligibility_Criteria__c>();
            for(Promotion_Eligibility_Criteria__c 
                criteria:  [SELECT Id, Name, Condition__c,
                                Object__c, Order__c,
                                Parameter__c, Promotion_Setup__c,
                                Value__c, Value_Type__c
                             FROM Promotion_Eligibility_Criteria__c
                             WHERE Promotion_Setup__c =: selectedPromo.Id
                             ORDER BY Order__c]){
                eligCriteriaList.add(criteria);
                List<String> valueList = new List<String>();
                if(apiinvFilterValueMap.containsKey(criteria.Parameter__c) 
                        && apiinvFilterValueMap.get(criteria.Parameter__c) != null){
                    valueList = apiinvFilterValueMap.get(criteria.Parameter__c);
                    valueList.sort();
                } 
                if(valueList.size() > 0){
                    isValueListEmpty.put(String.valueOf(criteria.Order__c), false);
                } else {
                    isValueListEmpty.put(String.valueOf(criteria.Order__c), true);
                }
                valueMap.put(String.valueOf(criteria.Order__c), valueList);
            }
        }
        Promotion_Eligibility_Criteria__c criteria = new Promotion_Eligibility_Criteria__c();
        criteria.Order__c = eligCriteriaList.size() + 1;
        criteria.Promotion_Setup__c  = selectedPromo.Id;
        fetchobjFieldAPIMap('', String.valueOf(eligCriteriaList.size() + 1));
        isValueListEmpty.put(String.valueOf(eligCriteriaList.size() + 1), true);
        valueMap.put(String.valueOf(eligCriteriaList.size() + 1), new List<String>());
        criteriaValueMap.put(String.valueOf(eligCriteriaList.size() + 1), new List<String>());
        setValueOptions(valueMap);
        system.debug('valueMap: ' + valueMap);
        fieldJsonResult = JSON.serialize(valueMap);
        system.debug('fieldJsonResult: '+ fieldJsonResult);
        eligCriteriaList.add(criteria);
    }

    /*********************************************************************************************
    * @Description : Adding new Criteria to the list of Execution Criterias
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public void addNewExecCriteria() {
        if(execCriteriaList.size() > 0){
            system.debug('execCriteriaList:'  + execCriteriaList);
            system.debug('valueTypeMapExec:' + valueTypeMapExec);
            for(Promotion_Execution_Criteria__c criteria: execCriteriaList){
                 if (criteria.Parameter__c != NULL) {
                    String key = criteria.Object__c + '-' + criteria.Parameter__c;
                     system.debug('key:' + key);
                    if(valueTypeMapExec.containsKey(key) && valueTypeMapExec.get(key) != null){
                        criteria.Value_Type__c = valueTypeMapExec.get(key);
                    }
                }
            }
            upsert execCriteriaList;
            execCriteriaList = new List<Promotion_Execution_Criteria__c>();
            for(Promotion_Execution_Criteria__c 
                    criteria:  [SELECT Id, Name, Condition__c,
                                    Object__c, Order__c,
                                    Parameter__c, Promotion_Setup__c,
                                    Value__c, Value_Type__c
                                 FROM Promotion_Execution_Criteria__c
                                 WHERE Promotion_Setup__c =: selectedPromo.Id
                                 ORDER BY Order__c]){
                execCriteriaList.add(criteria);
                List<String> valueList = new List<String>();
                if(apiinvFilterValueMapExec.containsKey(criteria.Parameter__c) 
                        && apiinvFilterValueMapExec.get(criteria.Parameter__c) != null){
                    valueList = apiinvFilterValueMapExec.get(criteria.Parameter__c);
                    valueList.sort();
                } 
                if(valueList.size() > 0){
                    isValueListEmptyExec.put(String.valueOf(criteria.Order__c), false);
                } else {
                    isValueListEmptyExec.put(String.valueOf(criteria.Order__c), true);
                }
                valueMapExec.put(String.valueOf(criteria.Order__c), valueList);
            }
        }
        Promotion_Execution_Criteria__c criteria = new Promotion_Execution_Criteria__c();
        criteria.Order__c = execCriteriaList.size() + 1;
        criteria.Promotion_Setup__c  = selectedPromo.Id;
        fetchobjFieldAPIMapExec('', String.valueOf(execCriteriaList.size() + 1));
        isValueListEmptyExec.put(String.valueOf(execCriteriaList.size() + 1), true);
        valueMapExec.put(String.valueOf(execCriteriaList.size() + 1), new List<String>());
        criteriaValueMapExec.put(String.valueOf(execCriteriaList.size() + 1), new List<String>());
        setValueOptionsExec(valueMapExec);
        system.debug('valueMapExec: ' + valueMapExec);
        fieldJsonResultExec = JSON.serialize(valueMapExec);
        system.debug('fieldJsonResultExec: '+ fieldJsonResultExec);
        execCriteriaList.add(criteria);
    }

    /*********************************************************************************************
    * @Description : Updating related Eligibility Criteria and filter logic
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public void updateEligCriteria() {
        for(Promotion_Eligibility_Criteria__c criteria: eligCriteriaList){
             if (criteria.Parameter__c != NULL) {
                String key = criteria.Object__c + '-' + criteria.Parameter__c;
                 system.debug('key:' + key);
                if(valueTypeMap.containsKey(key) && valueTypeMap.get(key) != null){
                    criteria.Value_Type__c = valueTypeMap.get(key);
                }
                if(criteriaValueMap.containsKey(String.valueOf(criteria.Order__c)) 
                        && criteriaValueMap.get(String.valueOf(criteria.Order__c)) != null){
                    criteria.Value__c = String.join(criteriaValueMap.get(String.valueOf(criteria.Order__c)), ';');
                }
            }
        }
        system.debug('criteriaValueMap:' + criteriaValueMap);
        
        upsert eligCriteriaList;
        upsert selectedPromo;
    }

    /*********************************************************************************************
    * @Description : Updating related Eligibility Criteria and filter logic
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public void updateExecCriteria() {
        for(Promotion_Execution_Criteria__c criteria: execCriteriaList){
             if (criteria.Parameter__c != NULL) {
                String key = criteria.Object__c + '-' + criteria.Parameter__c;
                 system.debug('key:' + key);
                if(valueTypeMap.containsKey(key) && valueTypeMap.get(key) != null){
                    criteria.Value_Type__c = valueTypeMap.get(key);
                }
                if(criteriaValueMapExec.containsKey(String.valueOf(criteria.Order__c)) 
                        && criteriaValueMapExec.get(String.valueOf(criteria.Order__c)) != null){
                    criteria.Value__c = String.join(criteriaValueMapExec.get(String.valueOf(criteria.Order__c)), ';');
                }
                system.debug('criteriaValueMapExec:' + criteriaValueMapExec);
            }
        }
        upsert execCriteriaList;
        upsert selectedPromo;
    }        

    /*********************************************************************************************
    * @Description : Updating Promotion Record status to Ready
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public void updateStatusToReady() {
       selectedPromo.Status__c = 'Ready';
       update selectedPromo;
    }

    /*********************************************************************************************
    * @Description : Initial Search of Promotion Setup records from the syste,
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public void initialSearchQuery() {
        List<Promotion_Setup__c> promoList = new List<Promotion_Setup__c> ();
        promoList = [SELECT Id, Name, Cost_to_DAMAC__c, Eligibility_Criteria_Filter_Logic__c,
                            End_Date__c, Execution_Criteria_Filter_Logic__c, Promotion_Name__c,
                            Promotion_Type__c, Start_Date__c, Sub_Type__c, Type__c,Status__c,
                            Active__c, LastModifiedBy.Name, LastModifiedDate, Duration__c,
                            Fees_to_be_deducted__c, of_Rental_Guarantee__c, Waived__c,
                            Capital_Paid_or_Unit_Value__c, Carat__c, Adjust_Cost_to_Price__c,
                            Gross_or_Net__c, Payment_outcome_on_Default__c, Rental_guarantee_Start__c,
                            Service_Charges_to_be_deducted__c, Signed_Promotion_Letter_Required__c,
                            Value_Cap__c, Waiver_Months__c, Weight__c, Pay_Frequency__c, Template_Id__c,
                            Eligibility_Inventory_Filter_Logic__c
                      FROM Promotion_Setup__c 
                      ORDER BY CreatedDate DESC
                  LIMIT 1000];
        promoListJSON = JSON.serialize(promoList);
        system.debug('promoListJSON: ' + promoListJSON);
    }
    
     /*********************************************************************************************
    * @Description : Inserting new Promotion Setup Record
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public void insertPromotion() {
         String fileString =  system.ApexPages.currentPage().getParameters().get('templateFile');
         String fileName =  system.ApexPages.currentPage().getParameters().get('templateFileName');
        Blob contentFile = EncodingUtil.base64Decode(fileString);
        if(promo.Sub_Type__c == 'Car Promotion' || promo.Sub_Type__c == 'Watch'){
            String brand = ApexPages.currentPage().getParameters().get('brand');
            String model = ApexPages.currentPage().getParameters().get('model');
            system.debug('brand: ' + brand + ', model: ' + model);
            if(brand  != null && brand  != ''){
               promo.Brand__c  = brand ;
            }
            if(model != null && model != ''){
              promo.Model__c  = model;
            }
        } else if(promo.Sub_Type__c == 'Gift Card'){
            String brand = ApexPages.currentPage().getParameters().get('brand2');
            String value = ApexPages.currentPage().getParameters().get('value');
            system.debug('brand: ' + brand + ', value: ' + value);
            if(brand  != null && brand  != ''){
               promo.Brand__c  = brand ;
            }
            if(value != null && value != ''){
              promo.Value__c  = value;
            }
        } else if(promo.Sub_Type__c == 'Gold'){
            String carat = ApexPages.currentPage().getParameters().get('carat');
            String weight = ApexPages.currentPage().getParameters().get('weight');
            system.debug('carat: ' + carat + ', weight: ' + weight);
            if(carat  != null && carat  != ''){
               promo.Carat__c  = carat ;
            }
            if(weight != null && weight != ''){
              promo.Weight__c  = weight;
            }
        } else if(promo.Sub_Type__c == 'Free Stay' || promo.Sub_Type__c == 'Free Ticket'){
            String value = ApexPages.currentPage().getParameters().get('value2');
            system.debug('value: ' + value );
            if(value  != null && value  != ''){
               promo.Value__c  = value ;
            }
        } else if(promo.Sub_Type__c == 'VAT Promotion' || promo.Sub_Type__c == 'DLD Promotion'){
            String waived = ApexPages.currentPage().getParameters().get('waived');
            String valueCap = ApexPages.currentPage().getParameters().get('valueCap');
            system.debug('waived: ' + waived + ', valueCap: ' + valueCap);
            if(waived  != null && waived  != ''){
               promo.Waived__c  = waived ;
            }
            if(valueCap != null && valueCap != ''){
              promo.Value_Cap__c  = valueCap;
            }
        } else if(promo.Sub_Type__c == 'Exterior' || promo.Sub_Type__c == 'Furniture'
                    || promo.Sub_Type__c == 'Interior' || promo.Sub_Type__c == 'Membership'
                    || promo.Sub_Type__c == 'Swimming Pool' || promo.Sub_Type__c == 'Unit Features'){
            String brand = ApexPages.currentPage().getParameters().get('brand3');
            system.debug('brand: ' + brand );
            if(brand  != null && brand  != ''){
               promo.Brand__c  = brand ;
            }
        } else if(promo.Sub_Type__c == 'Service Charge Waiver'){
            String brand = ApexPages.currentPage().getParameters().get('brand4');
            String valueCap = ApexPages.currentPage().getParameters().get('valueCap2');
            String waived = ApexPages.currentPage().getParameters().get('waived2');
            String waiverMonths = ApexPages.currentPage().getParameters().get('waiverMonths');
            system.debug('brand: ' + brand + ', valueCap: ' + valueCap  
                + ', waived: ' + waived  + ', waiverMonths: ' + waiverMonths);
            if(brand  != null && brand  != ''){
               promo.Brand__c  = brand ;
            }
            if(valueCap != null && valueCap != ''){
              promo.Value_Cap__c  = valueCap;
            }
            if(waived  != null && waived  != ''){
               promo.Waived__c  = waived ;
            }
            if(waiverMonths != null && waiverMonths != ''){
              promo.Waiver_Months__c  = waiverMonths;
            }
        }  else if(promo.Sub_Type__c == 'Rental Guarantee'){
            String brand = ApexPages.currentPage().getParameters().get('brand5');
            String rentalGuarantee = ApexPages.currentPage().getParameters().get('rentalGuarantee');
            String grossOrNet = ApexPages.currentPage().getParameters().get('grossOrNet');
            String capitalPaid = ApexPages.currentPage().getParameters().get('capitalPaid');
            String duration = ApexPages.currentPage().getParameters().get('duration');
            String rentalGuaranteeStart = ApexPages.currentPage().getParameters().get('rentalGuaranteeStart');
            String payFrequency = ApexPages.currentPage().getParameters().get('payFrequency');
            String payOutcome = ApexPages.currentPage().getParameters().get('payOutcome');
            String serviceCharges = ApexPages.currentPage().getParameters().get('serviceCharges');
            String fees = ApexPages.currentPage().getParameters().get('fees');
            
            if(brand  != null && brand  != ''){
               promo.Carat__c  = brand ;
            }
            if(rentalGuarantee != null && rentalGuarantee != ''){
              promo.of_Rental_Guarantee__c  = rentalGuarantee;
            }
            if(grossOrNet  != null && grossOrNet  != ''){
               promo.Gross_or_Net__c  = grossOrNet ;
            }
            if(capitalPaid != null && capitalPaid != ''){
              promo.Capital_Paid_or_Unit_Value__c  = capitalPaid;
            }
            if(duration  != null && duration  != ''){
               promo.Duration__c  = duration ;
            }
            if(rentalGuaranteeStart != null && rentalGuaranteeStart != ''){
              promo.Rental_guarantee_Start__c  = rentalGuaranteeStart;
            }
            if(payFrequency  != null && payFrequency  != ''){
               promo.Pay_Frequency__c  = payFrequency ;
            }
            if(payOutcome != null && payOutcome != ''){
              promo.Payment_outcome_on_Default__c  = payOutcome;
            }
            if(serviceCharges  != null && serviceCharges  != ''){
               promo.Service_Charges_to_be_deducted__c  = serviceCharges ;
            }
            if(fees != null && fees != ''){
              promo.Fees_to_be_deducted__c  = fees;
            }
        } 
        insert promo;
        Attachment tempalteAttach = new Attachment(parentId = promo.Id, name=fileName, body = contentFile);
        insert tempalteAttach;
        promo.Template_Id__c = tempalteAttach.Id;
        update promo;
        promo = new Promotion_Setup__c(Signed_Promotion_Letter_Required__c='Yes');
        initialSearchQuery();
    }

    /*********************************************************************************
    * @Description : Searching the Promotion Record
    * @Params      : void
    * @Return      : void
    **********************************************************************************/
    public void updateRelatedObj(){
        String obj = apexpages.currentpage().getparameters().get('objAPI');
        String order = apexpages.currentpage().getparameters().get('order2');
        system.debug('obj: '+ obj);
        if(obj != null && obj != ''){
            fetchobjFieldAPIMap(obj, order);
        }
    }

    /*********************************************************************************
    * @Description : Searching the Promotion Record
    * @Params      : void
    * @Return      : void
    **********************************************************************************/
    public void updateRelatedObjExec(){
        String obj = apexpages.currentpage().getparameters().get('objAPIExec');
        String order = apexpages.currentpage().getparameters().get('order2Exec');
        system.debug('obj: '+ obj);
        if(obj != null && obj != ''){
            fetchobjFieldAPIMapExec(obj, order);
        }
    }

    /*********************************************************************************
    * @Description : Searching the Promotion Record
    * @Params      : void
    * @Return      : void
    **********************************************************************************/
    public void getValues(){
        String fieldAPI = ApexPages.currentPage().getParameters().get('fieldAPI');
        String order = ApexPages.currentPage().getParameters().get('order');
        system.debug('fieldAPI2: ' + fieldAPI);
        system.debug('apiinvFilterValueMap: ' + apiinvFilterValueMap);
        List<String> valueList = new List<String>(); 
        if(apiinvFilterValueMap.containsKey(fieldAPI) 
                && apiinvFilterValueMap.get(fieldAPI) != null){
            valueList = apiinvFilterValueMap.get(fieldAPI);
            valueList.sort();
        }
        if(valueList.size() > 0){
            isValueListEmpty.put(order, false);
        } else {
            isValueListEmpty.put(order, true);
        }
        for(Promotion_Eligibility_Criteria__c criteria: eligCriteriaList){
            if(String.valueOf(criteria.Order__c) == order){
                criteria.Value__c = '';
            }
        }
        valueMap.put(order, valueList);
        setValueOptions(valueMap);
        system.debug('valueMap: ' + valueMap);
        fieldJsonResult = JSON.serialize(valueMap);
        system.debug('fieldJsonResult: '+ fieldJsonResult);
    }

    /*********************************************************************************
    * @Description : Searching the Promotion Record
    * @Params      : void
    * @Return      : void
    **********************************************************************************/
    public void getValuesExec(){
        String fieldAPI = ApexPages.currentPage().getParameters().get('fieldAPIExec');
        String order = ApexPages.currentPage().getParameters().get('orderExec');
        system.debug('fieldAPI2: ' + fieldAPI);
        system.debug('apiinvFilterValueMapExec: ' + apiinvFilterValueMapExec);
        List<String> valueList = new List<String>(); 
        if(apiinvFilterValueMapExec.containsKey(fieldAPI) 
                && apiinvFilterValueMapExec.get(fieldAPI) != null){
            valueList = apiinvFilterValueMapExec.get(fieldAPI);
            valueList.sort();
        }
        if(valueList.size() > 0){
            isValueListEmptyExec.put(order, false);
        } else {
            isValueListEmptyExec.put(order, true);
        }
        for(Promotion_Execution_Criteria__c criteria: execCriteriaList){
            if(String.valueOf(criteria.Order__c) == order){
                criteria.Value__c = '';
            }
        }
        valueMapExec.put(order, valueList);
        setValueOptionsExec(valueMapExec);
        system.debug('valueMapExec: ' + valueMapExec);
        fieldJsonResultExec = JSON.serialize(valueMap);
        system.debug('fieldJsonResultExec: '+ fieldJsonResultExec);
    }

    /*********************************************************************************
    * @Description : Searching the Promotion Record
    * @Params      : void
    * @Return      : void
    **********************************************************************************/
    public Map <String, String> fetchobjFieldAPIMapExec (String obj, String order) {
        Map <String, String> options = new Map <String, String> ();
        system.debug('obj: ' + obj);
        system.debug('order: ' + order);
        for (Promotion_Criteria_Field__mdt 
            criteriaField: [SELECT Label, Field_API__c, Inventory_Filter_Name__c,
                                Object_API__c, Value_Type__c 
                           FROM Promotion_Criteria_Field__mdt 
                           WHERE Object_API__c =: obj
                            AND Promotion_Execution_Criteria__c = TRUE]) {
            options.put (criteriaField.label, criteriaField.Field_API__c);
            if(criteriaField.Inventory_Filter_Name__c != null 
                && criteriaField.Inventory_Filter_Name__c != ''
                    && invFilterValueMap.containsKey(criteriaField.Inventory_Filter_Name__c)
                    && invFilterValueMap.get(criteriaField.Inventory_Filter_Name__c) != null){
                apiinvFilterValueMapExec.put(criteriaField.Field_API__c, 
                    invFilterValueMap.get(criteriaField.Inventory_Filter_Name__c));
            }
            valueTypeMapExec.put(criteriaField.Object_API__c + '-' + criteriaField.Field_API__c, 
                    criteriaField.Value_Type__c);
        }
        objFieldAPIMapExec.put(order, options);
        jsonResultExec = JSON.serialize(objFieldAPIMapExec);
        system.debug('jsonResultExec: '+ jsonResultExec);
        return options;
    }

    /*********************************************************************************
    * @Description : Searching the Promotion Record
    * @Params      : void
    * @Return      : void
    **********************************************************************************/
    public Map <String, String> fetchobjFieldAPIMap (String obj, String order) {
        Map <String, String> options = new Map <String, String> ();
        system.debug('obj: ' + obj);
        system.debug('order: ' + order);
        for (Promotion_Criteria_Field__mdt 
            criteriaField: [SELECT Label, Field_API__c, Inventory_Filter_Name__c,
                                Object_API__c, Value_Type__c 
                           FROM Promotion_Criteria_Field__mdt 
                           WHERE Object_API__c =: obj
                            AND Promotion_Eligibility_Criteria__c = TRUE]) {
            options.put (criteriaField.label, criteriaField.Field_API__c);
            if(criteriaField.Inventory_Filter_Name__c != null 
                && criteriaField.Inventory_Filter_Name__c != ''
                    && invFilterValueMap.containsKey(criteriaField.Inventory_Filter_Name__c)
                    && invFilterValueMap.get(criteriaField.Inventory_Filter_Name__c) != null){
                apiinvFilterValueMap.put(criteriaField.Field_API__c, 
                    invFilterValueMap.get(criteriaField.Inventory_Filter_Name__c));
            }
            valueTypeMap.put(criteriaField.Object_API__c + '-' + criteriaField.Field_API__c, 
                    criteriaField.Value_Type__c);
        }
        objFieldAPIMap.put(order, options);
        jsonResult = JSON.serialize(objFieldAPIMap);
        system.debug('jsonResult: '+ jsonResult);
        return options;
    }

    /*********************************************************************************
    * @Description : Searching the Promotion Record
    * @Params      : void
    * @Return      : void
    **********************************************************************************/
    public void setValueOptions(Map<String, List<String>> valueMap){
        valueOptionMap = new Map<String, List<SelectOption>>();
        for(String order: valueMap.keyset()){
            List<String> citiesList = new List<String>();
            List<SelectOption> options = new List<SelectOption>();
            if(valueMap.containsKey(order) && valueMap.get(order) != null){
                for(string value: valueMap.get(order)){
                    options.add(new SelectOption(value, value));
                }
            }
            valueOptionMap.put(order, options);
        }
        system.debug('valueOptionMap: ' + valueOptionMap);
    }

    /*********************************************************************************
    * @Description : Searching the Promotion Record
    * @Params      : void
    * @Return      : void
    **********************************************************************************/
    public void setValueOptionsExec(Map<String, List<String>> valueMap){
        valueOptionMapExec = new Map<String, List<SelectOption>>();
        for(String order: valueMap.keyset()){
            List<String> citiesList = new List<String>();
            List<SelectOption> options = new List<SelectOption>();
            if(valueMap.containsKey(order) && valueMap.get(order) != null){
                for(string value: valueMap.get(order)){
                    options.add(new SelectOption(value, value));
                }
            }
            valueOptionMapExec.put(order, options);
        }
        system.debug('valueOptionMapExec: ' + valueOptionMapExec);
    }
}