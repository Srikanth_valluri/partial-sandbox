/**************************************************************************************************
* Name               : DamacProjectController                                               
* Description        : An apex page controller for                                              
* Created Date       : NSI - Diana                                                                        
* Created By         : 29/Jan/2017                                                                 
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         NSI - Diana          29/Jan/2017       
* 1.5         NSI - Diana          16/Jul/2-17 - commented price slider for temp fix                                                        
**************************************************************************************************/

public without sharing class DamacProjectUnitsController {

    /**************************************************************************************************
            Variables used in the class
    **************************************************************************************************/
    public List<String> projectType{set;get;} // show type filter in VF
    public List<String> bedrooms{set;get;}
    public List<Inventory__c> inventories{set;get;}
    public String inventoryPrefix{set;get;}
    public Inventory_Area_range__c inventoryAreaRange{set;get;}
    public List<Inventory_Price__c> priceRangeValues{set;get;}
    public String areaSqftMin{set;get;}
    public String areaSqftMax{set;get;}
    public String priceMin{set;get;}
    public String priceMax{set;get;}
    public Decimal priceMinRange{set;get;}
    public Decimal priceMaxRange{set;get;}
    public Inventory_Price_Configuration__c inventoryPriceConfiguration{set;get;}
    public String propertyId{set;get;}
    public String userId{set;get;}
    private String inventoryIdQuery{set;get;}
    private Set<Id> InventoryIDs;
    private String generalInventoryQuery;
    public String bedroomsSelectedFromURL{set;get;}
    public String typeSelectedFromURL{set;get;}
    public String locationSelectedFromURL{set;get;}
    public String propertyName{set;get;}
    public string tabName{set;get;}
    public List<String> floorPackageType{set;get;}
    public String floorPackageTypeSelected{set;get;}
    public String floorPackageNameSelected{set;get;}
    public List<String> floorPackageName{set;get;}
    public List<String> villaType{Set;get;}
    public List<String> propertyStatus{set;get;}
    public String villaTypeSelected{set;get;}
    public String propertyStatusSelected{set;get;}

    private String MarketingName;
    private String District;

    /**************************************************************************************************
    Method:         DamacProjectController
    Description:    Constructor executing model of the class 
    **************************************************************************************************/
    public DamacProjectUnitsController() {
        
        projectType = UtilityQueryManager.getAllProjectTypes();
        bedrooms = UtilityQueryManager.getAllBedRooms();
        floorPackageType = new List<String>();
        villaType = new List<String>();
        propertyStatus = new List<String>();

        floorPackageType = DamacUtility.getPicklistValue(Inventory__c.SobjectType,'Floor_Package_Type__c');
        getVillaType();
        getPropertyStatus();

        if(ApexPages.currentPage().getParameters().containsKey('floorPkgName') && null != ApexPages.currentPage().getParameters().get('floorPkgName'))
            floorPackageNameSelected = ApexPages.currentPage().getParameters().get('floorPkgName');
        else 
            floorPackageNameSelected = ' ';


        inventories = new List<Inventory__c>();
        inventoryPrefix = DamacUtility.getObjectPrefix('Inventory__c');

        if(ApexPages.currentPage().getParameters().containsKey('Location') && ApexPages.currentPage().getParameters().get('Location') != ''
            && ApexPages.currentPage().getParameters().get('Location') != 'All'){
            locationSelectedFromURL = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('Location'));
        }

         if(ApexPages.currentPage().getParameters().containsKey('sfdc.tabName')){
            tabName = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('sfdc.tabName'));
        }

        inventoryAreaRange = Inventory_Area_range__c.getValues(DAMAC_Constants.INVENTORY_AREA_RANGE);
        //priceRangeValues = UtilityQueryManager.getPriceRange();
        //inventoryPriceConfiguration = Inventory_Price_Configuration__c.getValues(DAMAC_Constants.INVENTORY_STEP);

        inventoryIdQuery ='SELECT Inventory__c from Inventory_User__c where Inventory__r.Status__c = \'RELEASED\' AND Inventory__r.Unit_Location__c != null'+ 
                                     ' AND Inventory__r.Address__c != null AND End_Date__c >= TODAY AND User__c=\''+UserInfo.getUserId()+'\'';
        
        generalInventoryQuery = 'SELECT Id from Inventory__c where Status__c=\'RELEASED\' AND Unit_Location__c != null'+
                                ' AND Address__c != null AND Is_Assigned__c = false';
                            
        InventoryIDs = UtilityQueryManager.getInventoryIDs(inventoryIdQuery);
        
        
        
        //get all inventories from the general inventory pool
        Set<ID> generalInventoryId = UtilityQueryManager.getAllGeneralInventories(generalInventoryQuery );
        InventoryIDs.addAll(generalInventoryId);
        
        propertyId = null;
        if(ApexPages.currentPage().getParameters().containsKey('Id')){
            propertyId = ApexPages.currentPage().getParameters().get('Id');
        }

        MarketingName = '';
        District = '';

        if(ApexPages.currentPage().getParameters().containsKey('District') &&
             ApexPages.currentPage().getParameters().get('District') != '' && 
             null != ApexPages.currentPage().getParameters().get('District')
                ){
            District = ApexPages.currentPage().getParameters().get('District');
        }

         if(ApexPages.currentPage().getParameters().containsKey('MarketingName') &&
             ApexPages.currentPage().getParameters().get('MarketingName') != '' && 
             null != ApexPages.currentPage().getParameters().get('MarketingName')
                ){
            MarketingName =  ApexPages.currentPage().getParameters().get('MarketingName');
         }

         //v1.5 :start
        //Map<String,Decimal> minMaxPriceMap = UtilityQueryManager.getMinMaxPrice(propertyId);

        /*if(ApexPages.currentPage().getParameters().containsKey('MinPrice') && null != ApexPages.currentPage().getParameters().get('MinPrice')){
            priceMin = ApexPages.currentPage().getParameters().get('MinPrice');
        }

        if(ApexPages.currentPage().getParameters().containsKey('MaxPrice') && null != ApexPages.currentPage().getParameters().get('MaxPrice')){
            priceMax = ApexPages.currentPage().getParameters().get('MaxPrice');
        }
        
        if( null !=  minMaxPriceMap){
            priceMinRange = minMaxPriceMap.get('min');
            priceMaxRange =  minMaxPriceMap.get('max');
            priceMin = (priceMin ==  null)?String.valueOf(priceMinRange):priceMin;
            priceMax= (priceMax == null)?String.valueOf(priceMaxRange):priceMax;
        }*/
        //v1.5 end
        
        userId = UserInfo.getUserId();
        floorPackageName = new List<String>();
        floorPackageName = UtilityQueryManager.getAllFloorPackageName(InventoryIDs,propertyId);
        getAllInventories();
        
        //
       
    }

    public void getAllInventories(){

        if(null != propertyId){
            String condition= 'Id,Property_Name_2__c,Property__r.Property_Name__c FROM Inventory__c WHERE Unit_Type__c != null AND Unit_Location__c != null '+
                        ' AND Id IN :InventoryIDs AND Property__r.Id = \''+propertyId+'\''+
                ' AND Status__c = \'Released\' ';

            
            if(ApexPages.currentPage().getParameters().containsKey('UnitBedrooms')){

                if(ApexPages.currentPage().getParameters().get('UnitBedrooms') != '' 
                    && ApexPages.currentPage().getParameters().get('UnitBedrooms') != 'All')
                    bedroomsSelectedFromURL = ApexPages.currentPage().getParameters().get('UnitBedrooms');
               
            }else if(ApexPages.currentPage().getParameters().containsKey('Bedrooms')){
                if(ApexPages.currentPage().getParameters().get('Bedrooms') != '' 
                    && ApexPages.currentPage().getParameters().get('Bedrooms') != 'All')
                    bedroomsSelectedFromURL = ApexPages.currentPage().getParameters().get('Bedrooms');
            }  

            if(null != bedroomsSelectedFromURL && bedroomsSelectedFromURL != ''){
                List<String> bedroomsSelectedLists = bedroomsSelectedFromURL.split(',');
                    
                if(null != bedroomsSelectedLists){
                  condition+= ' AND IPMS_Bedrooms__c IN (\''+String.join(bedroomsSelectedLists,'\',\'')+'\')'; 
                 }
            }

            if(ApexPages.currentPage().getParameters().containsKey('UnitType')){
                if(ApexPages.currentPage().getParameters().get('Type') != ''
                && ApexPages.currentPage().getParameters().get('Type') != 'All')
               typeSelectedFromURL= ApexPages.currentPage().getParameters().get('UnitType');
                       
            }else if(ApexPages.currentPage().getParameters().containsKey('Type')){
                if(ApexPages.currentPage().getParameters().get('Type') != ''
                && ApexPages.currentPage().getParameters().get('Type') != 'All')
                typeSelectedFromURL= ApexPages.currentPage().getParameters().get('Type');
            }

            if(null != typeSelectedFromURL && typeSelectedFromURL != ''){
                List<String> typeSelectedLists= typeSelectedFromURL.split(',');
                
                if(null != typeSelectedLists){
                   condition+= ' AND Unit_Type__c IN (\''+String.join(typeSelectedLists,'\',\'')+'\')'; 
                }
            }

            //v1.5 start
            /*if(String.isNotEmpty(priceMin)){
                condition += ' AND ( List_Price_calc__c >=' + priceMin +' OR Special_Price_calc__c >= ' +priceMin + ') ';
            }
            
            if(String.isNotEmpty(priceMax)){
                condition += ' AND ( List_Price_calc__c <='+ priceMax +' OR Special_Price_calc__c <= ' +priceMax+ ') ';
            }*/
            //v1.5 end

             if(String.isNotEmpty(floorPackageNameSelected.trim())){
                condition += ' AND Floor_Package_Name__c =\''+floorPackageNameSelected.trim()+'\'';
            }

          if(ApexPages.currentPage().getParameters().containsKey('floorPkgType') && 
            null != ApexPages.currentPage().getParameters().get('floorPkgType') &&
            '' != ApexPages.currentPage().getParameters().get('floorPkgType') &&
            'All' != ApexPages.currentPage().getParameters().get('floorPkgType')){
                floorPackageTypeSelected= ApexPages.currentPage().getParameters().get('floorPkgType');
                List<String> floorPackageTypeLst= floorPackageTypeSelected.split(',');
                
                if(null != floorPackageTypeLst){
                   condition+= ' AND Floor_Package_Type__c IN (\''+String.join(floorPackageTypeLst,'\',\'')+'\')'; 
                }
          }

            if(ApexPages.currentPage().getParameters().containsKey('villaType') && 
            null != ApexPages.currentPage().getParameters().get('villaType') &&
            '' != ApexPages.currentPage().getParameters().get('villaType') &&
            'All' != ApexPages.currentPage().getParameters().get('villaType')){
                villaTypeSelected= ApexPages.currentPage().getParameters().get('villaType');
                List<String> villaTypeLst= villaTypeSelected.split(',');
                
                if(null != villaTypeLst){
                   condition+= ' AND  Project_Category__c IN (\''+String.join(villaTypeLst,'\',\'')+'\')'; 
                }
              }

              if(ApexPages.currentPage().getParameters().containsKey('propertyStatus') && 
                null != ApexPages.currentPage().getParameters().get('propertyStatus') &&
                '' != ApexPages.currentPage().getParameters().get('propertyStatus') &&
                'All' != ApexPages.currentPage().getParameters().get('propertyStatus')){
                    propertyStatusSelected= ApexPages.currentPage().getParameters().get('propertyStatus');
                    List<String> propertyStatusLst= propertyStatusSelected.split(',');
                    
                    if(null != propertyStatusLst){
                       condition+= ' AND Property_Status__c IN (\''+String.join(propertyStatusLst,'\',\'')+'\')'; 
                    }
              }

              if(String.isNotEmpty(MarketingName) && null != MarketingName){
                condition += ' AND Marketing_Name__c =\''+MarketingName+'\'';
              }

              if(String.isNotEmpty(District) && null != District){
                condition += ' AND Property__r.District__c =\''+District+'\'';
              }
            

            condition += ' LIMIT 10000'; 
        
            inventories = UtilityQueryManager.getInventoryList(condition,InventoryIDs);
            
            if(null != inventories && inventories.size()>0){
                propertyName = inventories[0].Property__r.Property_Name__c;
            }
        }
    }
    
    public void filterInventories(){
        
        if(null != propertyId){
             String condition = 'Id FROM Inventory__c WHERE Unit_Type__c != null AND Unit_Location__c != null AND Id IN :InventoryIDs AND Property__r.Id = \''+propertyId+'\''+
                    ' AND Status__c = \'Released\'';
                    
             String BedroomsSelected = ApexPages.currentPage().getParameters().get('BedroomsSelected');
             String TypeSelected = ApexPages.currentPage().getParameters().get('TypeSelected');
             
             //BedroomsSelected Selected is blank or All then dont include it in query
            if(null != BedroomsSelected && '\'\'' != BedroomsSelected && BedroomsSelected != '\'All\'' ){
                condition += ' AND IPMS_Bedrooms__c IN ('+BedroomsSelected+')';
                system.debug('**location query '+condition);
            }
            
            if(null != TypeSelected && '\'\'' != TypeSelected && TypeSelected != '\'All\''){
                condition += ' AND Unit_Type__c IN '+
                                '('+TypeSelected+')';
                system.debug('**type query '+condition);
            }
            
            //v1.5 start
            /*if(String.isNotEmpty(areaSqftMin) && String.isNotEmpty(areaSqftMax)){
                condition += ' AND Area_Sqft_2__c>='+areaSqftMin+' AND Area_Sqft_2__c<='+areaSqftMax;
            }*/
    
            /*if(String.isNotEmpty(priceMin)){
                condition += ' AND ( List_Price_calc__c >=' + priceMin +' OR Special_Price_calc__c >= ' +priceMin + ') ';
            }
            
            if(String.isNotEmpty(priceMax)){
                condition += ' AND ( List_Price_calc__c <='+ priceMax +' OR Special_Price_calc__c <= ' +priceMax+ ') ';
            }*/
            //v1.5 end

            if(String.isNotEmpty(floorPackageNameSelected)){
                condition += ' AND Floor_Package_Name__c =\''+floorPackageNameSelected+'\'';
            }

            if(String.isNotEmpty(floorPackageTypeSelected) && null != floorPackageTypeSelected && '\'\'' != floorPackageTypeSelected && floorPackageTypeSelected != '\'All\'')
                condition += 'AND Floor_Package_Type__c IN '+
                                '('+floorPackageTypeSelected+')';

             if(String.isNotEmpty(villaTypeSelected) && null != villaTypeSelected && '\'\'' != villaTypeSelected && villaTypeSelected != '\'All\'')
                condition += 'AND Project_Category__c IN '+
                                '('+villaTypeSelected+')';

             if(String.isNotEmpty(propertyStatusSelected) && null != propertyStatusSelected && '\'\'' != propertyStatusSelected && propertyStatusSelected != '\'All\'')
                condition += 'AND Property_Status__c IN '+
                                '('+propertyStatusSelected+')';

             if(String.isNotEmpty(MarketingName) && null != MarketingName){
                condition += ' AND Marketing_Name__c =\''+MarketingName+'\'';
              }

             if(String.isNotEmpty(District) && null != District){
                condition += ' AND Property__r.District__c =\''+District+'\'';
              }
            
            system.debug('==condition=='+condition);
        
            inventories = UtilityQueryManager.getInventoryList(condition+' LIMIT 10000',InventoryIDs);
            
        }
    
    }

    /*********************************************************************************************
    * @Description : Get all villas and property status                                         *                      
    * @Params      : void                                                                       *
    * @Return      : void                                                                       *
    *********************************************************************************************/
    public void getVillaType(){

        for(AggregateResult thisInventory:[SELECT Project_Category__c FROM Inventory__c where Project_Category__c != NULL
                                        AND Status__c = 'Released' GROUP BY Project_Category__c ]){
            villaType.add((String)thisInventory.get('Project_Category__c'));
        }
    }

    public void getPropertyStatus(){
        for(AggregateResult thisInventory:[SELECT Property_Status__c FROM Inventory__c where Property_Status__c != null
                                        AND Status__c = 'Released' GROUP BY Property_Status__c]){
            propertyStatus.add((String)thisInventory.get('Property_Status__c'));
        }
    }
}