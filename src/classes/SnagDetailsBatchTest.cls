/*********************************************************************************
* Description - Test Class for class SnagDetailsBatch                            *
*                                                                                *
* Version            Date            Author         Description                  *
* 1.0                23/01/2019      Arjun Khatri   Initial Draft.               *
**********************************************************************************/
@isTest
public class SnagDetailsBatchTest {
    
    @isTest
    public static void test1() {
        
        //Create Customer record
        Account objAcc = new Account(Name = 'Test Account');
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        //listBUs[0].unit_Name__c = 'DTPB/37/3701';
        listBUs[0].Dont_Get_Snags__c = false;
        listBUs[0].Registration_status_code__c = 'fsdf';
        insert listBUs;
        
        /*System.debug('test bu == ' +  [SELECT Id , Booking__r.Account__c, Get_snags__c,Booking__r.Account__r.Mobile_Phone_Encrypt__pc
                        , Booking__r.Account__r.Name, Booking__r.Account__r.Email__pc, Property_Name_Inventory__c
                        , SNAGs_Completed__c, Unit_Name__c, inventory__r.Building_Location__r.Is_New_Snagger__c
                        , (select Id,Snags_Reported_at_first__c,SNAGs_Completed__c FROM Cases__r) FROM Booking_unit__c]);*/
        //EHO Case record type Id
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        
        //Create Case
        Case  objCase = TestDataFactory_CRM.createCase(objAcc.Id, caseRecTypeId);
        objCase.Booking_Unit__c = listBUs[0].Id;
        insert objCase;
        
        Test.startTest();
        Database.executeBatch(new SnagDetailsBatch());
        Test.stopTest();
    }
}