/*
 * Description: This batch is used to populate the Count of Call Log on Calling List.
 */

public class UpdateCountOfCallLogOnCLBatch implements Database.Batchable<sObject>, Database.Stateful {

    String query = 'SELECT Id, Calling_List__c FROM Call_Log__c';
    public Database.QueryLocator start(Database.BatchableContext BC) {
        if(Test.isRunningTest()) {
            query += ' Limit 1';
        } else {
            query += ' WHERE Calling_List__c != NULL';
        }
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<Call_Log__c> scope) {
        Set<Id> setCLIds = new Set<Id>();       
        for (Call_Log__c objCallLog : scope) {
            if (objCallLog.Calling_List__c != null) {
                setCLIds.add(objCallLog.Calling_List__c);
            }
        }
        if (!setCLIds.isEmpty()) {
            List<Call_Log__c> lstCallLog = [SELECT Id, Calling_List__c FROM Call_Log__c Where Calling_List__c In: setCLIds];
            if (!lstCallLog.isEmpty()) {
                new PopulateCountOfCallLogUtility().stampCountOfCallLogOnCL(lstCallLog);    
            }
        }       
    }

    public void finish(Database.BatchableContext BC) {
    }

}