/**
 * @File Name          : SchedulePopulateTaxInvoiceForDpInvoice.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 12/12/2019, 12:04:40 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/9/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class SchedulePopulateTaxInvoiceForDpInvoice implements Schedulable {
     public void execute(SchedulableContext SC){
    	PopulateTaxInvoiceForDpInvoiceBatch batchInst = new PopulateTaxInvoiceForDpInvoiceBatch();
    	Database.executeBatch(batchInst,1);
    }
}