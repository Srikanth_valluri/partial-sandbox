/*****************************************************************************************************
* Description - Test class developed for 'Refunds'
*
* Version            Date            Author               Description
* 1.0                21/11/17        Naresh(Accely)               Initial Draft
 *******************************************************************************************************/


@isTest(SeeAllData=false)
public class RefundsTest{
    
    static Refunds.CustomerFundAndTokenHttpSoap11Endpoint  obj = new Refunds.CustomerFundAndTokenHttpSoap11Endpoint();
    static String resp;
    
    // To Cover getTokenRefunds()
    public static testmethod void getTokenRefunds(){
        
        Test.startTest();
        
        SOAPCalloutServiceMock.returnToMe =  new Map<String,Refunds.getTokenRefundsResponse_element >();
        Refunds.getTokenRefundsResponse_element  response_x = new Refunds.getTokenRefundsResponse_element();
        response_x.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
        resp = obj.getTokenRefunds('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','RegistrationId');
        System.assertEquals('S',resp);
        Test.stopTest();
    }


   // To Cover getExcessAmount()
    public static testmethod void getExcessAmount(){
        
         Test.startTest();
        
        SOAPCalloutServiceMock.returnToMe =  new Map<String,Refunds.getExcessAmountResponse_element >();
        Refunds.getExcessAmountResponse_element  response_x = new Refunds.getExcessAmountResponse_element ();
        response_x.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
        resp = obj.getExcessAmount('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','RegistrationId');
        System.assertEquals('S',resp);
        
        Test.stopTest();
    } 
      
    // To Cover CustomerPaymentMade()
    public static testmethod void CustomerPaymentMade(){
        
        Test.startTest();
        
        SOAPCalloutServiceMock.returnToMe =  new Map<String,Refunds.CustomerPaymentMadeResponse_element >();
        Refunds.CustomerPaymentMadeResponse_element  response_x = new Refunds.CustomerPaymentMadeResponse_element ();
        response_x.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
        resp = obj.CustomerPaymentMade('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','RegistrationId');
        System.assertEquals('S',resp);
        Test.stopTest();
    }
}