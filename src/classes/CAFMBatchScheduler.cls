/*----------------------------------------------------------------------------
Description: Scheduler for bacth CAFMBatch to run every midnight

========================================================================================
    Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------
    2.0     | 26-07-2018       | Lochana Rajput   | Added logic for callout and update FM case
==============================================================================================
*/

public class CAFMBatchScheduler implements Schedulable {
	public static String cronExp = '0 0 0 ? * * *';
    public static String jobName = 'CAFMBatchScheduler';
	public void scheduleMyJob() {
		CAFMBatchScheduler batchObj = new CAFMBatchScheduler();
        System.schedule(jobName, cronExp, batchObj);
	}
	public void execute(SchedulableContext sc) {
		CAFMBatch batchObj = new CAFMBatch();
        Database.executeBatch(batchObj, 10);
	}
}