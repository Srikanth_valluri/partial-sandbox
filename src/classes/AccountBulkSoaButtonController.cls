public class AccountBulkSoaButtonController {

    public Id recordId {get; set;}
    public String responseUrl {get; set;}
    public String accessToken {get; set;}
    public String pdfFile {get; set;}
    public Blob pdfBlob {get; set;}
    //public String base64Response {get; set;}

    public AccountBulkSoaButtonController(ApexPages.StandardController controller) {
        recordId = controller.getId();
        System.debug('Account Id in consturctor: '+recordId);
    }

    public pageReference getBulkSoaUrl() {

        FmIpmsRestCoffeeServices.updateCustomSetting = false;

        Account account = [SELECT Id, Party_ID__c
                           FROM Account
                           WHERE Id =: recordId];
        String partyId = account.Party_ID__c;
        System.debug('partyId is: '+partyId);

        responseUrl = FmIpmsRestCoffeeServices.getBulkSoa(partyId);
        System.debug('responseUrl is: '+responseUrl);

        if(String.isBlank(responseUrl) || responseUrl == null || responseUrl == 'Null') {
            System.debug('In ApexMessage Method');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'There was an error processing your request');
            ApexPages.addMessage(msg);
            return null;
        }

        //Http callout
        //accessToken = FmIpmsRestCoffeeServices.getAccessToken();
        accessToken = FmIpmsRestCoffeeServices.accessToken;
        System.debug('AccessToken is' + accessToken);

        HttpRequest req = new HttpRequest();
        req.setEndpoint(responseUrl);
        req.setHeader('Accept', 'application/json');
        req.setMethod('GET');
        req.setHeader('Authorization','Bearer' + accessToken);
        req.setTimeout(120000);
        HttpResponse response = new Http().send(req);
        System.debug('Status Code = ' + response.getStatusCode());
        System.debug('Pdf Body = ' + response.getBodyAsBlob());


        if(response.getStatusCode() == 200) {
            //String base64Response = (String)JSON.deserialize(response.getBody(), String.class );
            //blob pdfFile = EncodingUtil.base64Decode(base64Response);
            //System.debug('pdfFile: '+pdfFile);
            pdfBlob =  response.getBodyAsBlob();

            //pdfFile = Blob.toPDF(EncodingUtil.base64Encode(pdfBlob));
            pdfFile = EncodingUtil.base64Encode(pdfBlob);
            //TODO 
            GenericUtility.createSOACreator(responseUrl,UserInfo.getUserId(),system.now(),'','Generate Bulk SOA',account.Id,NULL,NULL);
            //FmUploadDocumentsController.Document.uploadDocument(EncodingUtil.base64Encode(pdfBlob));
            //pdfFile = response.getBody();
            //return EncodingUtil.Base64Encode(pdfFile);
        }

        FmIpmsRestCoffeeServices.updateCustomSetting = true;
        FmIpmsRestCoffeeServices.updateBearerToken();      //calling updateMethod of Custom Setting after all callouts execution

        return null;
    }

}