/*********************************************************************************
*   Description :  Executes FMServiceChargesSMSBatch class.                      *
*--------------------------------------------------------------------------------*
*   Revision History:                                                            *
*   Version     Author              Date            Description                  *
*   1.0         Arjun Khatri        05/03/2019      Initial Draft                *
*********************************************************************************/
global class FMServiceChargesSmsSched implements Schedulable {
    global void execute(SchedulableContext sc) {

         database.executebatch(new FMServiceChargesSMSBatch(),1);
    }
}