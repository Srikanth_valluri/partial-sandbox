//Generated by wsdl2apex

public class GenerateCRFService {
    public class GetCustomerRequestForm_element {
        public String P_REQUEST_NUMBER;
        public String P_REQUEST_NAME;
        public String P_SOURCE_SYSTEM;
        public processXxdcProcessServiceWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5[] regTerms;
        private String[] P_REQUEST_NUMBER_type_info = new String[]{'P_REQUEST_NUMBER','http://action.com',null,'0','1','true'};
        private String[] P_REQUEST_NAME_type_info = new String[]{'P_REQUEST_NAME','http://action.com',null,'0','1','true'};
        private String[] P_SOURCE_SYSTEM_type_info = new String[]{'P_SOURCE_SYSTEM','http://action.com',null,'0','1','true'};
        private String[] regTerms_type_info = new String[]{'regTerms','http://action.com',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://action.com','true','true'};
        private String[] field_order_type_info = new String[]{'P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','regTerms'};
    }
    public class GetCustomerRequestFormResponse_element {
        public String return_x;
        private String[] return_x_type_info = new String[]{'return','http://action.com',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://action.com','true','true'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class CRFHttpSoap11Endpoint {
        public String endpoint_x = Label.WSDLBaseUrl + ':8080/CRM_SR_NEW/services/CRF.CRFHttpSoap11Endpoint/';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://action.com', 'GenerateCRFService', 'http://process.xxdc_process_service_ws.plsql.soaprovider.ont.apps.xmlns.oracle.com/xsd', 'processXxdcProcessServiceWsPlsqlSo'};
        public String GetCustomerRequestForm(String P_REQUEST_NUMBER,String P_REQUEST_NAME,String P_SOURCE_SYSTEM,processXxdcProcessServiceWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5[] regTerms) {
            GenerateCRFService.GetCustomerRequestForm_element request_x = new GenerateCRFService.GetCustomerRequestForm_element();
            request_x.P_REQUEST_NUMBER = P_REQUEST_NUMBER;
            request_x.P_REQUEST_NAME = P_REQUEST_NAME;
            request_x.P_SOURCE_SYSTEM = P_SOURCE_SYSTEM;
            request_x.regTerms = regTerms;
            GenerateCRFService.GetCustomerRequestFormResponse_element response_x;
            Map<String, GenerateCRFService.GetCustomerRequestFormResponse_element> response_map_x = new Map<String, GenerateCRFService.GetCustomerRequestFormResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:GetCustomerRequestForm',
              'http://action.com',
              'GetCustomerRequestForm',
              'http://action.com',
              'GetCustomerRequestFormResponse',
              'GenerateCRFService.GetCustomerRequestFormResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
    }
}