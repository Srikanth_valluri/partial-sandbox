/*
* Description - Test class developed for 'RentalPoolTerminationController'
*
* Version            Date            Author            Description
* 1.0              29/02/2018        Ashish           Initial Draft
*/

@isTest 
private class RentalPoolTerminationControllerTest {
    
    static testMethod void testInitMethod() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId();
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id; 
            lstCases.add( objCase );
        }
        insert lstCases ;
                
        PageReference pageRef = Page.RentalPoolTerminationProcessPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('AccountId', objAcc.Id);
        ApexPages.currentPage().getParameters().put('caseID', lstCases[0].Id);
        ApexPages.currentPage().getParameters().put('SRType', 'RentalPoolTermination');
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination() );
        test.startTest();
            RentalPoolTerminationController objContr = new RentalPoolTerminationController();
        test.stopTest();
    }
    
    static testMethod void testGettingUnitsList() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC';
           objUnit.Rental_Pool__c = true;
        }
        insert lstBookingUnits;
        
        List<Booking_Unit_Active_Status__c> lstStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstStatus ;
        
        test.startTest();
            RentalPoolTerminationController objContr = new RentalPoolTerminationController();
            objContr.getlstBookingUnits();
            objContr.accountId = objAcc.Id;
            objContr.strSelectedSRType = 'RentalPoolTermination';
            objContr.getlstBookingUnits();
        test.stopTest();
    }
    
    static testMethod void testGetBookinUnitDetails() {
         Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC';
           objUnit.Rental_Pool__c = true;
        }
        insert lstBookingUnits;
        
        List<Booking_Unit_Active_Status__c> lstStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstStatus ;
        
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId();
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
        insert lstOptions ;
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination() );
        test.startTest();
            RentalPoolTerminationController objContr = new RentalPoolTerminationController();
            objContr.accountId = objAcc.Id;
            objContr.mapAllBookingUnits = RentalPoolTerminationUtility.getAllRelatedBookingUnits( objContr.accountId );
            objContr.objUnitWrap = new WrapperBookingUnit();
            objContr.objUnitWrap.objIPMSDetailsWrapper = new UnitDetailsService.BookinUnitDetailsWrapper();
            objContr.getBookinUnitDetails();
        test.stopTest();
    }
    
    
    static testMethod void testCRFGeneration() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId();
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination() );       
        test.startTest();
            RentalPoolTerminationController objContr = new RentalPoolTerminationController();
            objContr.selectedUnit = lstBookingUnits[0]; 
            objContr.objUnitWrap = new WrapperBookingUnit();
            objContr.objUnitWrap.objIPMSDetailsWrapper = new UnitDetailsService.BookinUnitDetailsWrapper(); 
            objContr.objCase = lstCases[0];
            
            SOAPCalloutServiceMock.returnToMe = new Map<String, AOPTDocumentGeneration.DocGenerationResponse_element>();
            AOPTDocumentGeneration.DocGenerationResponse_element response = new AOPTDocumentGeneration.DocGenerationResponse_element();
            response.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=403501 and Request Id :40815712 ...","ATTRIBUTE3":"403501","ATTRIBUTE2":"40815712","ATTRIBUTE1":"https://sftest.deeprootsurface.com/docs/e/40815712_74428_CRF.pdf","PARAM_ID":"74428"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            objContr.generateCRForm();
            
            response.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            objContr.generateCRForm();
            
        test.stopTest();
    }
    
    static testMethod void testSOAGeneration() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId();
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            lstCases.add( objCase );
        }
        insert lstCases ;
                
        test.startTest();
        
            RentalPoolTerminationController objController = new RentalPoolTerminationController();
            objController.selectedUnit = lstBookingUnits[0];
            objController.objCase = lstCases[0];
            
            SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOAService.GenCustomerStatementResponse_element>();
            GenerateSOAService.GenCustomerStatementResponse_element response = new GenerateSOAService.GenCustomerStatementResponse_element();
            response.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338397 and Request Id :40187735 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"92061","REQUEST_ID":"40187735","STAGE_ID":"338397","URL":"https://sftest.deeprootsurface.com/docs/e/40187735_92061_SOA.pdf"}';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            objController.insertStatementOfAccount();
            
            response.return_x = '{"Status":"E","PROC_STATUS":"E","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338397 and Request Id :40187735 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"92061","REQUEST_ID":"40187735","STAGE_ID":"338397","URL":""}';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            objController.insertStatementOfAccount();
           
            objController.selectedUnit.Registration_ID__c = '';
            objController.insertStatementOfAccount(); 
        test.stopTest();
    }
    
    static testMethod void testFetchRPAgreement() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId();
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        SR_Attachments__c objAttach1 = TestDataFactory_CRM.createCaseDocument( lstCases[0].Id, Label.RP_Agreement );
        insert objAttach1;
        
        test.startTest();
            RentalPoolTerminationController objController = new RentalPoolTerminationController();
            objController.fetchRPAgreement();
            
            objController.objAgreementCase = lstCases[0];
            objController.objCase = lstCases[0];
            
            objController.fetchRPAgreement();
            
            SR_Attachments__c objAttach2 = TestDataFactory_CRM.createCaseDocument( lstCases[0].Id, Label.RP_Agreement );
            objAttach2.Name = 'Signed Rental Pool Agreement Document';
            insert objAttach2;
            objController.fetchRPAgreement();
        test.stopTest();
        
    }
    
    static testMethod void testInitiateTerminationSR1() {
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId();
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        
        test.startTest();
            RentalPoolTerminationController objController = new RentalPoolTerminationController();
            objController.crfAttachmentBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objController.crfAttachmentName = 'Tony.txt';
            objController.poaAttachmentBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objController.poaAttachmentName = 'Stark.txt';
            objController.rpAgreementBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objController.rpAgreementName = 'Bruce.txt';
            objController.selectedUnit = lstBookingUnits[0];
            objController.objCase = lstCases[0];
            
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(1) );
            objController.initiateTerminationSR();
            
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(2) );
            objController.initiateTerminationSR();
        test.stopTest();
    }
    
    static testMethod void testInitiateTerminationSR2() {
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId();
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        
        test.startTest();
            RentalPoolTerminationController objController = new RentalPoolTerminationController();
            objController.crfAttachmentBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objController.crfAttachmentName = 'Tony.txt';
            objController.poaAttachmentBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objController.poaAttachmentName = 'Stark.txt';
            objController.rpAgreementBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objController.rpAgreementName = 'Bruce.txt';
            objController.selectedUnit = lstBookingUnits[0];
            objController.objCase = lstCases[0];
        
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(3) );
            objController.initiateTerminationSR();
        test.stopTest();
    }
    
    static testMethod void testSubmitTerminationSR() {
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId();
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        
        test.startTest();
            RentalPoolTerminationController objController = new RentalPoolTerminationController();
            objController.crfAttachmentBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objController.crfAttachmentName = 'Tony.txt';
            objController.poaAttachmentBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objController.poaAttachmentName = 'Stark.txt';
            objController.rpAgreementBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objController.rpAgreementName = 'Bruce.txt';
            objController.selectedUnit = lstBookingUnits[0];
            objController.objCase = lstCases[0];
        
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(1) );
            objController.submitTerminationSR();
            
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(2) );
            objController.submitTerminationSR();
            
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(3) ); 
            objController.submitTerminationSR();
            
        test.stopTest();
    }
    
    static testMethod void testGetAttachments() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        NSIBPM__Service_Request__c objInq = TestDataFactory_CRM.createServiceRequest();
        insert objInq ;
        
        list<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objInq.Id,1);
        insert lstBookings ;
        
        List<Booking_Unit__c> lstUnits = TestDataFactory_CRM.createBookingUnits(lstBookings,1);
        insert lstUnits ;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        insert objCase ;
        
        SR_Attachments__c objAttach = TestDataFactory_CRM.createCaseDocument( objCase.Id, 'CRF Form');
        insert objAttach;
        
        SR_Attachments__c objAttach1 = TestDataFactory_CRM.createCaseDocument( objCase.Id, 'Power of Attorney');
        insert objAttach1;
        
        SR_Attachments__c objAttach2 = TestDataFactory_CRM.createCaseDocument( objCase.Id, Label.RP_Agreement );
        insert objAttach2;
        
        SR_Attachments__c objAttach3 = TestDataFactory_CRM.createCaseDocument( objCase.Id, Label.RP_Agreement );
        insert objAttach3;
        
        RentalPoolTerminationController objContr = new RentalPoolTerminationController();
        test.startTest();
            objContr.getAttachments( objCase.Id );
        test.stopTest();
    }
    
    static testMethod void testMapCaseData() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        NSIBPM__Service_Request__c objInq = TestDataFactory_CRM.createServiceRequest();
        insert objInq ;
        
        list<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objInq.Id,1);
        insert lstBookings ;
        
        List<Booking_Unit__c> lstUnits = TestDataFactory_CRM.createBookingUnits(lstBookings,1);
        insert lstUnits ;
        
        RentalPoolTerminationController objContr = new RentalPoolTerminationController(); 
        objContr.objCase = new Case();
        objContr.selectedUnit = lstUnits[0] ;
        test.startTest();
            objContr.mapCaseData();
        test.stopTest();
    }
    
    static testMethod void testCreateTaskForCRE() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        NSIBPM__Service_Request__c objInq = TestDataFactory_CRM.createServiceRequest();
        insert objInq ;
        
        list<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objInq.Id,1);
        insert lstBookings ;
        
        List<Booking_Unit__c> lstUnits = TestDataFactory_CRM.createBookingUnits(lstBookings,1);
        insert lstUnits ;
        
        RentalPoolTerminationController objContr = new RentalPoolTerminationController(); 
        objContr.objCase = new Case();
        objContr.selectedUnit = lstUnits[0] ;
        test.startTest();
            objContr.createTaskForCRE();
        test.stopTest();
    }
}