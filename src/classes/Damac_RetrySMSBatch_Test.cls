@isTest
public class Damac_RetrySMSBatch_Test {

    public static testMethod void method1 () {
        Stand_Inquiry__c inq = new Stand_Inquiry__c ();
        inq.Mobile_Phone_Encrypt__c = '8977365305';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.SMS_MSG_Id__c = '9876543210';
        inq.SMS_Response_Status__c = 'Undelivered';
        insert inq;  
        Damac_RetrySMSBatch obj = new Damac_RetrySMSBatch('', 'Stand_inquiry__c', '');
        DataBase.executeBatch(obj, 50);
        
        Damac_RetrySMSBatchSchedule sh1 = new Damac_RetrySMSBatchSchedule ();
        sh1.execute(null); 
        

        Database.executeBatch (new DummyUpdateInquiryBatch (), 1);
    }
}