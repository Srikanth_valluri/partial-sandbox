public class Damac_ObjectFieldsController {
    public Email_Request_Rule_Criteria__c rule { get; set; }
    public String selectedField { get; set; }
    public String jsonResult { get; set; }
    public String emailReqId { get; set; }
    public Boolean showHeader { get; set; }
    String relatedObjAPI;
    public Damac_ObjectFieldsController (APexPages.StandardController stdController) {
        rule = new Email_Request_Rule_Criteria__c ();
        selectedField = '';
        jsonResult = '';
        relatedObjAPI = '';
        showHeader = false;
        if (stdController.getId() != NULL) {
            rule = [SELECT Email_Request__c, Email_Request__r.Related_Object_API__c, Value_Type__c, 
                    Condition__c, Order__c, Value__c, Parameter__c
                    FROM Email_Request_Rule_Criteria__c 
                    WHERE ID =:stdController.getId()];
            relatedObjAPI = rule.Email_Request__r.Related_Object_API__c;
        } else {
            emailReqId = apexpages.currentpage().getparameters().get('emailReqId');
            showHeader = true;
            Email_request__c req = [select Related_Object_API__c FROM Email_request__c WHERE Id =: emailReqId];
            List <Email_Request_Rule_Criteria__c> existingRules = new List <Email_Request_Rule_Criteria__c> ();
            try {
            	existingRules = [SELECT Name FROM Email_Request_Rule_Criteria__c WHERE Email_Request__c =: emailReqId];
            } catch (Exception e) {}
            relatedObjAPI = req.Related_Object_API__c;
            rule.Order__c = existingRules.size () + 1;
        }
    }
    public void updateRule () {
       System.Debug ('===='+rule);
        if (rule.Parameter__c != NULL) {
            rule.Value_Type__c = [SELECT Value_Type__c FROM Object_Specific_Fields__mdt 
                                  WHERE Object_API__C =: relatedObjAPI
                                  AND Field_API__c =: rule.Parameter__c LIMIT 1].Value_Type__c;
        }
        if (emailReqId != NULL && emailReqId != '') {
        	rule.Email_Request__c = emailReqId;
            insert rule;
        }
        else
        	update rule;        
    }
    public Map <String, String> getFieldAPIs () {
        Map <String, String> options = new Map <String, String> ();
        for (Object_Specific_Fields__mdt u: [SELECT Label, Field_API__c FROM Object_Specific_Fields__mdt 
                                             where Object_API__C =: relatedObjAPI])
        {
            options.put (u.label, u.Field_API__c);
        }
        
        jsonResult = JSON.serialize(options);
        return options;
    }
}