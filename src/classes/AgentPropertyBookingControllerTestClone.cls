/**
pratiksha
 */
@isTest(seealldata=false)
private class AgentPropertyBookingControllerTestClone {

    static testMethod void myUnitTest1() { 
    	    	
        Id RSRecordTypeId = null;
         if(null != Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows')) {
            RSRecordTypeId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId();
        }
        ID ProfileID = [ Select id,UserType from Profile where name = 'Customer Community - Admin'].id;
        ID consultantId = [ Select id,UserType from Profile where name = 'Property Consultant'].id;
        ID adminProfileID = [ Select id,UserType from Profile where name = 'System Administrator'].id;
        ID adminRoleId = [ Select id from userRole where name = 'Chairman'].id;
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Property Consultant']; 
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        String milli = String.valueOf(System.now().millisecond());
        User u1 = new User(Alias = 'standt1', Email='standarduser1@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p1.Id, 
            TimeZoneSidKey='America/Los_Angeles', isActive = true,UserName= 'testUser1'+milli+'@damac.ae');
            
        User u2 = new User(Alias = 'standt2', Email='standarduser2@testorg.com', UserRoleId = adminRoleId,
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p2.Id, 
            TimeZoneSidKey='America/Los_Angeles',isActive = true,  UserName= 'testUser2'+milli+'@damac.ae');
        
        System.runAs(u2) {

            if(null != RSRecordTypeId){
                Campaign__c camp = new Campaign__c();
                camp.RecordTypeId = RSRecordTypeId;
                camp.Campaign_Name__c='Test Campaign';
                camp.start_date__c = System.today();
                camp.end_date__c = System.Today().addDays(30);
                camp.Marketing_start_date__c = System.today();
                camp.Marketing_end_date__c = System.Today().addDays(30);
                insert camp;
            }
            
            Address__c addressobj1 = new Address__c();
            addressobj1.Address_ID__c = 987654;
            addressobj1.ADDRESS_LINE1__c='Saba Tower 1';
            addressobj1.ADDRESS_LINE2__c='Cluster E';
            addressobj1.ADDRESS_LINE3__c='Jumeirah Lake Towers';
            addressobj1.City__c = 'Dubai';
            addressobj1.Country__c = 'AE';
            insert addressobj1;
            
            Address__c addressobj2 = new Address__c();
            addressobj2.Address_ID__c = 12345;
            addressobj2.ADDRESS_LINE1__c='Saba Tower 1';
            addressobj2.ADDRESS_LINE2__c='Cluster E';
            addressobj2.ADDRESS_LINE3__c='Jumeirah Lake Towers';
            addressobj2.City__c = 'Dubai';
            addressobj2.Country__c = 'LB';
            insert addressobj2;
            
            Address__c addressobj3 = new Address__c();
            addressobj3.Address_ID__c = 12346;
            addressobj3.ADDRESS_LINE1__c='Saba Tower 1';
            addressobj3.ADDRESS_LINE2__c='Cluster E';
            addressobj3.ADDRESS_LINE3__c='Jumeirah Lake Towers';
            addressobj3.City__c = 'Dubai';
            addressObj3.State__c = 'Dubai';
            addressobj3.Country__c = 'QA';
            insert addressobj3;
            
            addressobj3.Latitude__c = '';
            update addressobj3;
            
            Address__c addressobj4 = new Address__c();
            addressobj4.Address_ID__c = 12347;
            addressobj4.ADDRESS_LINE1__c='Saba Tower 1';
            addressobj4.ADDRESS_LINE2__c='Cluster E';
            addressobj4.ADDRESS_LINE3__c='Jumeirah Lake Towers';
            addressobj4.City__c = 'Dubai';
            addressobj4.Country__c = 'JO';
            insert addressobj4;
            
            Address__c addressobj5 = new Address__c();
            addressobj5.Address_ID__c = 12348;
            addressobj5.ADDRESS_LINE1__c='Saba Tower 1';
            addressobj5.ADDRESS_LINE2__c='Cluster E';
            addressobj5.ADDRESS_LINE3__c='Jumeirah Lake Towers';
            addressobj5.City__c = 'Riyadh';
            addressobj5.Country__c = 'SA';
            insert addressobj5;
            
            Property__c prop = new Property__c();
            prop.Property_Name__c='Discovery gardens';
            prop.Property_ID__c = 12345;
            prop.Active_Property__c = true;
            insert prop;
            
            Location__c loc1 = new Location__c();
            loc1.Name = 'DGB56';
            loc1.Building_Name__c = 'Discovery Gardens';
            loc1.Address_ID__c = '987654';
            loc1.Location_ID__c = '0001';
            loc1.Property_ID__c = '12345';
            loc1.location_type__c = 'Building';
            insert loc1;
            
            Location__c loc2 = new Location__c();
            loc2.Name = 'DGB562';
            loc2.Building_Name__c = 'Discovery Gardens';
            loc2.Address_ID__c = '987654';
            loc2.Location_ID__c = '0002';
            loc2.Property_ID__c = '12345';
            loc2.location_type__c = 'Floor';
            loc2.Parent_id__c = loc1.Location_ID__c;
            insert loc2;
            
            Location__c loc3 = new Location__c();
            loc3.Name = 'DGB562214';
            loc3.Building_Name__c = 'Discovery Gardens';
            loc3.Address_ID__c = '987654';
            loc3.Location_ID__c = '0003';
            loc3.Property_ID__c = '12345';
            loc3.location_type__c = 'Unit';
            loc3.Parent_id__c = loc2.Location_ID__c;
            insert loc3;
            
            Inventory_Release__c ir = new Inventory_Release__c();
            ir.Property_ID__c = '12345';
            ir.Floor_ID__c = '0002';
            ir.Building_ID__c = '0001';
            ir.Unit_ID__c = '0003';
            ir.Release_ID__c = '12345';
            insert ir;
            
            Inventory__c inv = new Inventory__c();
            inv.Inventory_ID__c = '0003';
            inv.Address_Id__c = '987654';
            inv.Building_ID__c = '0001';
            inv.Floor_ID__c = '0002';
            inv.Unit_ID__c = '0003';
            inv.Property_ID__c = '12345';
            inv.Release_ID__c='12345';
            inv.IPMS_Bedrooms__c='1';
            inv.Floor_Package_Name__c = 'test.pkg';
            inv.Floor_Package_Type__c = 'Package';
            inv.Floor_Package_ID__c='12345';
            inv.Master_Developer_EN__c = 'Damac Hills';
            inv.ACD_Date__c = '2018-06-30';
            inv.Bedroom_Type__c = '1BR';
            inv.Property_Status__c = 'Ready';
            inv.Project_Category__c = 'High-rise';
            inv.Package_Bulk_Price_List__c = 12345;
            insert inv; 
            
            ApexPages.currentPage().getParameters().put('id',inv.ID);
            
            List<Inventory__c> objINT = new List<Inventory__c>();
	        objINT = [select id from Inventory__c Limit 5];
	        set<ID> setIDINT = new set<ID>();
	        setIDINT.add(inv.ID);
	        
	        List<Inventory__c> listIDINT = new List<Inventory__c>();
	        listIDINT.add(inv); 
	        //floor package ids
	        set<String> setFloorPackage = new set<String>();
	        setFloorPackage.add(inv.Floor_Package_ID__c);
	        
            System.debug('....set objINT....'+objINT);
            System.debug('...listIDINT....'+listIDINT);
            
            Payment_Plan__c pp1 = new Payment_Plan__c();
            pp1.Term_ID__c = '12346'; 
            insert pp1;
            
            Payment_Plan__c pp2 = new Payment_Plan__c();
            pp2.Term_ID__c = '12345';
            insert pp2;
            
            Map<String, Payment_Plan__c> mapPP = new Map<String, Payment_Plan__c>();
            mapPP.put(inv.ID,pp1);
            
            
            
            Payment_Terms__c pterms = new Payment_Terms__c();
            
            pterms.Term_ID__c ='12345';
            insert pterms;
            
            pterms.Term_ID__c = '12346';
            update pTerms;
            
            List<NSIBPM__SR_Template__c> SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
	        Id RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
	        Account A1 = new Account();
	        A1.recordtypeid=RecordTypeIdAGENT;
	        A1.Name = 'Test Account';
	        A1.Agency_Short_Name__c = 'testShrName';
	        insert A1;
        
            Id RecordTypeIdContact = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
            NSIBPM__Service_Request__c sr = InitializeSRDataTest.getSerReq('Agent Registration',true,null);
	        sr.recordtypeid=RecordTypeIdContact;
	        sr.NSIBPM__SR_Template__c = SRTemplateList[0].Id;
	        sr.Delivery_mode__c='Email';
            sr.Deal_ID__c='1001';
	        sr.Agency__c = A1.id;
	        sr.ID_Type__c = 'Passport';
	        insert sr;
            

	        
	        Inquiry__c onjINQ = new Inquiry__c();
	        onjINQ.By_Pass_Validation__c = true;
	        onjINQ.Party_ID__c = '12345';
	        onjINQ.Title__c = 'MR.';
	        onjINQ.Title_Arabic__c ='MR.';
	        onjINQ.First_Name__c = 'Test';
	        onjINQ.First_Name_Arabic__c ='Test';
	        onjINQ.Last_Name__c ='Test';
	        onjINQ.Last_Name_Arabic__c = 'Test';
	        insert onjINQ;
	        
	        booking__c objbook = new booking__c();
            objbook.Deal_SR__c = sr.id;
            objbook.Booking_Channel__c = 'Web';
            insert objbook;
	        
	        Booking_Unit__c bu = new Booking_Unit__c();
            bu.Payment_Method__c = 'Cash';
	        bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
	        bu.Primary_Buyer_s_Name__c = 'testNSI';
	        bu.Primary_Buyer_s_Nationality__c = 'test';
	        bu.Inventory__c = listIDINT[0].id;
	        bu.Booking__c = objbook.ID;
	        insert bu;
	        
            buyer__c b = new buyer__c();
            b.Buyer_Type__c =  'Individual';
	        b.Address_Line_1__c =  'Ad1';
	        b.Country__c =  'United Arab Emirates';
	        b.City__c = 'Dubai' ;
	        b.Inquiry__c =onjINQ.Id ;
	        b.Date_of_Birth__c = string.valueof(system.today().addyears(-30)) ;
	        b.Email__c = 'test@test.com';
	        b.First_Name__c = 'firstname' ;
	        b.Last_Name__c =  'lastname';
	        b.Nationality__c = 'Indian' ;
	        b.Passport_Expiry_Date__c = string.valueof(system.today().addyears(20)) ;
	        b.Passport_Number__c = 'J0565556' ;
	        b.Phone__c = '569098767' ;
	        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
	        b.Place_of_Issue__c =  'India';
	        b.Title__c = 'Mr';
	        b.booking__c = objbook.id;
	        insert b;
	        
            List<Buyer__c>  listBU = new List<Buyer__c>();
            listBU.add(b);
            
            Contact C1 = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User1', 
            email = 'test-user@fakeemail.com' );
            insert C1; 
            
            Contact C2 = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User2', 
            email = 'test-user1@fakeemail.com' );
            insert C2;
            
            Contact C3 = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User3', 
            email = 'test-user2@fakeemail.com' );
            insert C3;
            
            User u4 = new User( email='test-user5@fakeemail.com', contactid = c1.id, profileid = profileID, 
                      UserName= 'testUser4'+milli+'@damac.ae', alias='tuser1', CommunityNickName='tuser5', isActive = true,
                      TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
                      LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User');
            insert u4;
            
            Agent_Site__c objAS = new Agent_Site__c();
            objAS.Agency__c = A1.ID;
            objAS.Name='UAE';
            objAS.Org_ID__c = '81';
            insert objAS;
        System.runAs(u4) {    
        Test.startTest();
        UtilityWrapperManager UWM = new UtilityWrapperManager();


        AgentPropertyBookingController objAP = new AgentPropertyBookingController();
        objAP.availableInventoriesList =UWM;
	    objAP.selectedCampaignId='test';
	    objAP.errorMessage='test';
	    objAP.errorPrefix ='test';
	    objAP.mode='test';
	    objAP.showScreen1 = true;
	    objAP.showScreen2 = true;
	    objAP.showScreen3= true;
	    objAP.showScreen4= true;
	    objAP.isError= true;
	    objAP.isSuccess= true;
	    objAP.isSubmitted= true;
	    objAP.showsubmitbooking=true;
	    objAP.getPackageRelatedInventories(setFloorPackage);
	    AgentPropertyBookingController.validateSelectedUnits(A1.ID,setIDINT);
	    //objAP.submitApprovalProcess(A1.ID,'Level 4');
	   
        objAP.getAgencyName(); 
        objAP.selectUnits();
        objAP.getAgencyIdConId();
	    objAP.checkInventoryAvailable(setIDINT);
	    AgentPropertyBookingController.validateEmailAddress('pratikshanrvkr@gmail.com');
	    AgentPropertyBookingController.validatePhone('9049665087');
	    AgentPropertyBookingController.validateDateOfBirth(Date.Today());
	    objAP.getRoadshowCampaign();
	    objAP.verifyOPT();
	            System.debug('UWM = '+UWM.value);
        System.debug('UWM = '+UWM.isSubmitted);
        System.debug('UWM = '+UWM.dealRecord); 
        System.debug('UWM = '+UWM.ibwList);
        UtilityWrapperManager.SharingWrapper ser = new UtilityWrapperManager.SharingWrapper('test', 'test', 'test', 'test', 'test');
        UtilityWrapperManager.CampaignWrapper cam = new UtilityWrapperManager.CampaignWrapper(false, new Campaign__c (), new List<Promotion__c> ());
        UtilityWrapperManager.BookingWrapper BW = new UtilityWrapperManager.BookingWrapper('Damac','Start');
        UtilityWrapperManager.BuildingInorWrapper BInfo = new UtilityWrapperManager.BuildingInorWrapper('1',1,Double.valueOf('10.2'),12.1,15.3,13.3,43.2,'ABC');
        UtilityWrapperManager.PriceRageWrapper pt = new UtilityWrapperManager.PriceRageWrapper(12.3, 23.2);
        UtilityWrapperManager.InventoryDetailsWrapper idw = new UtilityWrapperManager.InventoryDetailsWrapper( '1233245324','1231','test','123432423','Damac','dsfw','test','one','2','2','marketing',12.1,23.2,21.2);
        UtilityWrapperManager.InventoryBuyerWrapper ibw = new UtilityWrapperManager.InventoryBuyerWrapper();
        UtilityWrapperManager.InventoryBuyerWrapper ibwn = new UtilityWrapperManager.InventoryBuyerWrapper(false,false, false, 32.2,3432432.234,'test','test','test','test',3, inv ,bu,mapPP,listBU);
         objAP.createPortfolio();
        Test.stopTest();
        }
        
    }
    
}
static testMethod void myUnitTest() { 
    	    	
        Id RSRecordTypeId = null;
         if(null != Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows')) {
            RSRecordTypeId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId();
        }
        ID ProfileID = [ Select id,UserType from Profile where name = 'Customer Community - Admin'].id;
        ID consultantId = [ Select id,UserType from Profile where name = 'Property Consultant'].id;
        ID adminProfileID = [ Select id,UserType from Profile where name = 'System Administrator'].id;
        ID adminRoleId = [ Select id from userRole where name = 'Chairman'].id;
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Property Consultant']; 
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        String milli = String.valueOf(System.now().millisecond());
        User u1 = new User(Alias = 'standt1', Email='standarduser1@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p1.Id, 
            TimeZoneSidKey='America/Los_Angeles', isActive = true,UserName= 'testUser1'+milli+'@damac.ae');
            
        User u2 = new User(Alias = 'standt2', Email='standarduser2@testorg.com', UserRoleId = adminRoleId,
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p2.Id, 
            TimeZoneSidKey='America/Los_Angeles',isActive = true,  UserName= 'testUser2'+milli+'@damac.ae');
        
        System.runAs(u2) {

            if(null != RSRecordTypeId){
                Campaign__c camp = new Campaign__c();
                camp.RecordTypeId = RSRecordTypeId;
                camp.Campaign_Name__c='Test Campaign';
                camp.start_date__c = System.today();
                camp.end_date__c = System.Today().addDays(30);
                camp.Marketing_start_date__c = System.today();
                camp.Marketing_end_date__c = System.Today().addDays(30);
                insert camp;
            }
            
            Address__c addressobj1 = new Address__c();
            addressobj1.Address_ID__c = 987654;
            addressobj1.ADDRESS_LINE1__c='Saba Tower 1';
            addressobj1.ADDRESS_LINE2__c='Cluster E';
            addressobj1.ADDRESS_LINE3__c='Jumeirah Lake Towers';
            addressobj1.City__c = 'Dubai';
            addressobj1.Country__c = 'AE';
            insert addressobj1;
            
            Address__c addressobj2 = new Address__c();
            addressobj2.Address_ID__c = 12345;
            addressobj2.ADDRESS_LINE1__c='Saba Tower 1';
            addressobj2.ADDRESS_LINE2__c='Cluster E';
            addressobj2.ADDRESS_LINE3__c='Jumeirah Lake Towers';
            addressobj2.City__c = 'Dubai';
            addressobj2.Country__c = 'LB';
            insert addressobj2;
            
            Address__c addressobj3 = new Address__c();
            addressobj3.Address_ID__c = 12346;
            addressobj3.ADDRESS_LINE1__c='Saba Tower 1';
            addressobj3.ADDRESS_LINE2__c='Cluster E';
            addressobj3.ADDRESS_LINE3__c='Jumeirah Lake Towers';
            addressobj3.City__c = 'Dubai';
            addressObj3.State__c = 'Dubai';
            addressobj3.Country__c = 'QA';
            insert addressobj3;
            
            addressobj3.Latitude__c = '';
            update addressobj3;
            
            Address__c addressobj4 = new Address__c();
            addressobj4.Address_ID__c = 12347;
            addressobj4.ADDRESS_LINE1__c='Saba Tower 1';
            addressobj4.ADDRESS_LINE2__c='Cluster E';
            addressobj4.ADDRESS_LINE3__c='Jumeirah Lake Towers';
            addressobj4.City__c = 'Dubai';
            addressobj4.Country__c = 'JO';
            insert addressobj4;
            
            Address__c addressobj5 = new Address__c();
            addressobj5.Address_ID__c = 12348;
            addressobj5.ADDRESS_LINE1__c='Saba Tower 1';
            addressobj5.ADDRESS_LINE2__c='Cluster E';
            addressobj5.ADDRESS_LINE3__c='Jumeirah Lake Towers';
            addressobj5.City__c = 'Riyadh';
            addressobj5.Country__c = 'SA';
            insert addressobj5;
            
            Property__c prop = new Property__c();
            prop.Property_Name__c='Discovery gardens';
            prop.Property_ID__c = 12345;
            prop.Active_Property__c = true;
            insert prop;
            
            Location__c loc1 = new Location__c();
            loc1.Name = 'DGB56';
            loc1.Building_Name__c = 'Discovery Gardens';
            loc1.Address_ID__c = '987654';
            loc1.Location_ID__c = '0001';
            loc1.Property_ID__c = '12345';
            loc1.location_type__c = 'Building';
            insert loc1;
            
            Location__c loc2 = new Location__c();
            loc2.Name = 'DGB562';
            loc2.Building_Name__c = 'Discovery Gardens';
            loc2.Address_ID__c = '987654';
            loc2.Location_ID__c = '0002';
            loc2.Property_ID__c = '12345';
            loc2.location_type__c = 'Floor';
            loc2.Parent_id__c = loc1.Location_ID__c;
            insert loc2;
            
            Location__c loc3 = new Location__c();
            loc3.Name = 'DGB562214';
            loc3.Building_Name__c = 'Discovery Gardens';
            loc3.Address_ID__c = '987654';
            loc3.Location_ID__c = '0003';
            loc3.Property_ID__c = '12345';
            loc3.location_type__c = 'Unit';
            loc3.Parent_id__c = loc2.Location_ID__c;
            insert loc3;
            
            Inventory_Release__c ir = new Inventory_Release__c();
            ir.Property_ID__c = '12345';
            ir.Floor_ID__c = '0002';
            ir.Building_ID__c = '0001';
            ir.Unit_ID__c = '0003';
            ir.Release_ID__c = '12345';
            insert ir;
            
            Inventory__c inv = new Inventory__c();
            inv.Inventory_ID__c = '0003';
            inv.Address_Id__c = '987654';
            inv.Building_ID__c = '0001';
            inv.Floor_ID__c = '0002';
            inv.Unit_ID__c = '0003';
            inv.Property_ID__c = '12345';
            inv.Release_ID__c='12345';
            inv.IPMS_Bedrooms__c='1';
            inv.Floor_Package_Name__c = 'test.pkg';
            inv.Floor_Package_Type__c = 'Package';
            inv.Floor_Package_ID__c='12345';
            inv.Master_Developer_EN__c = 'Damac Hills';
            inv.ACD_Date__c = '2018-06-30';
            inv.Bedroom_Type__c = '1BR';
            inv.Property_Status__c = 'Ready';
            inv.Project_Category__c = 'High-rise';
            inv.Package_Bulk_Price_List__c = 12345;
            insert inv; 
            
            ApexPages.currentPage().getParameters().put('id',inv.ID);
            
            List<Inventory__c> objINT = new List<Inventory__c>();
	        objINT = [select id from Inventory__c Limit 5];
	        set<ID> setIDINT = new set<ID>();
	        setIDINT.add(inv.ID);
	        
	        List<Inventory__c> listIDINT = new List<Inventory__c>();
	        listIDINT.add(inv); 
	        //floor package ids
	        set<String> setFloorPackage = new set<String>();
	        setFloorPackage.add(inv.Floor_Package_ID__c);
	        
            System.debug('....set objINT....'+objINT);
            System.debug('...listIDINT....'+listIDINT);
            
            Payment_Plan__c pp1 = new Payment_Plan__c();
            pp1.Term_ID__c = '12346'; 
            insert pp1;
            
            Payment_Plan__c pp2 = new Payment_Plan__c();
            pp2.Term_ID__c = '12345';
            insert pp2;
            
            Map<String, Payment_Plan__c> mapPP = new Map<String, Payment_Plan__c>();
            mapPP.put(inv.ID,pp1);
            
            
            
            Payment_Terms__c pterms = new Payment_Terms__c();
            
            pterms.Term_ID__c ='12345';
            insert pterms;
            
            pterms.Term_ID__c = '12346';
            update pTerms;
            
            List<NSIBPM__SR_Template__c> SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
	        Id RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
	        Account A1 = new Account();
	        A1.recordtypeid=RecordTypeIdAGENT;
	        A1.Name = 'Test Account';
	        A1.Agency_Short_Name__c = 'testShrName';
	        insert A1;
        
            Id RecordTypeIdContact = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
            NSIBPM__Service_Request__c sr = InitializeSRDataTest.getSerReq('Agent Registration',true,null);
	        sr.recordtypeid=RecordTypeIdContact;
	        sr.NSIBPM__SR_Template__c = SRTemplateList[0].Id;
	        sr.Delivery_mode__c='Email';
            sr.Deal_ID__c='1001';
	        sr.Agency__c = A1.id;
	        sr.ID_Type__c = 'Passport';
	        insert sr;
            

	        
	        Inquiry__c onjINQ = new Inquiry__c();
	        onjINQ.By_Pass_Validation__c = true;
	        onjINQ.Party_ID__c = '12345';
	        onjINQ.Title__c = 'MR.';
	        onjINQ.Title_Arabic__c ='MR.';
	        onjINQ.First_Name__c = 'Test';
	        onjINQ.First_Name_Arabic__c ='Test';
	        onjINQ.Last_Name__c ='Test';
	        onjINQ.Last_Name_Arabic__c = 'Test';
	        insert onjINQ;
	        
	        booking__c objbook = new booking__c();
            objbook.Deal_SR__c = sr.id;
            objbook.Booking_Channel__c = 'Web';
            insert objbook;
	        
	        Booking_Unit__c bu = new Booking_Unit__c();
            bu.Payment_Method__c = 'Cash';
	        bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
	        bu.Primary_Buyer_s_Name__c = 'testNSI';
	        bu.Primary_Buyer_s_Nationality__c = 'test';
	        bu.Inventory__c = listIDINT[0].id;
	        bu.Booking__c = objbook.ID;
	        insert bu;
	        
            buyer__c b = new buyer__c();
            b.Buyer_Type__c =  'Individual';
	        b.Address_Line_1__c =  'Ad1';
	        b.Country__c =  'United Arab Emirates';
	        b.City__c = 'Dubai' ;
	        b.Inquiry__c =onjINQ.Id ;
	        b.Date_of_Birth__c = string.valueof(system.today().addyears(-30)) ;
	        b.Email__c = 'test@test.com';
	        b.First_Name__c = 'firstname' ;
	        b.Last_Name__c =  'lastname';
	        b.Nationality__c = 'Indian' ;
	        b.Passport_Expiry_Date__c = string.valueof(system.today().addyears(20)) ;
	        b.Passport_Number__c = 'J0565556' ;
	        b.Phone__c = '569098767' ;
	        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
	        b.Place_of_Issue__c =  'India';
	        b.Title__c = 'Mr';
	        b.booking__c = objbook.id;
	        insert b;
	        
            List<Buyer__c>  listBU = new List<Buyer__c>();
            listBU.add(b);
            
            Contact C1 = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User1', 
            email = 'test-user@fakeemail.com' );
            insert C1; 
            
            Contact C2 = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User2', 
            email = 'test-user1@fakeemail.com' );
            insert C2;
            
            Contact C3 = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User3', 
            email = 'test-user2@fakeemail.com' );
            insert C3;
            
            User u4 = new User( email='test-user5@fakeemail.com', contactid = c1.id, profileid = profileID, 
                      UserName= 'testUser4'+milli+'@damac.ae', alias='tuser1', CommunityNickName='tuser5', isActive = true,
                      TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
                      LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User');
            insert u4;
            
            Agent_Site__c objAS = new Agent_Site__c();
            objAS.Agency__c = A1.ID;
            objAS.Name='UAE';
            objAS.Org_ID__c = '81';
            insert objAS;
        System.runAs(u4) {    
        Test.startTest();
        UtilityWrapperManager UWM = new UtilityWrapperManager();


        AgentPropertyBookingController objAP = new AgentPropertyBookingController();
        objAP.availableInventoriesList =UWM;
	    objAP.selectedCampaignId='test';
	    objAP.errorMessage='test';
	    objAP.errorPrefix ='test';
	    objAP.mode='test';
	    objAP.showScreen1 = true;
	    objAP.showScreen2 = true;
	    objAP.showScreen3= true;
	    objAP.showScreen4= true;
	    objAP.isError= true;
	    objAP.isSuccess= true;
	    objAP.isSubmitted= true;
	    objAP.showsubmitbooking=true;
	    objAP.getPackageRelatedInventories(setFloorPackage);
	    AgentPropertyBookingController.validateSelectedUnits(A1.ID,setIDINT);
	    //objAP.submitApprovalProcess(A1.ID,'Level 4');
	   
        objAP.getAgencyName(); 
        objAP.selectUnits();
        objAP.getAgencyIdConId();
        
	    objAP.checkInventoryAvailable(new set<ID>());
	    AgentPropertyBookingController.validateEmailAddress('pratikshanrvkr@gmail.com');
	    AgentPropertyBookingController.validatePhone('9049665087');
	    AgentPropertyBookingController.validateDateOfBirth(Date.Today());
	    objAP.getRoadshowCampaign();
	    objAP.verifyOPT();
	            System.debug('UWM = '+UWM.value);
        System.debug('UWM = '+UWM.isSubmitted);
        System.debug('UWM = '+UWM.dealRecord); 
        System.debug('UWM = '+UWM.ibwList);
        UtilityWrapperManager.SharingWrapper ser = new UtilityWrapperManager.SharingWrapper('test', 'test', 'test', 'test', 'test');
        UtilityWrapperManager.CampaignWrapper cam = new UtilityWrapperManager.CampaignWrapper(false, new Campaign__c (), new List<Promotion__c> ());
        UtilityWrapperManager.BookingWrapper BW = new UtilityWrapperManager.BookingWrapper('Damac','Start');
        UtilityWrapperManager.BuildingInorWrapper BInfo = new UtilityWrapperManager.BuildingInorWrapper('1',1,Double.valueOf('10.2'),12.1,15.3,13.3,43.2,'ABC');
        UtilityWrapperManager.PriceRageWrapper pt = new UtilityWrapperManager.PriceRageWrapper(12.3, 23.2);
        UtilityWrapperManager.InventoryDetailsWrapper idw = new UtilityWrapperManager.InventoryDetailsWrapper( '1233245324','1231','test','123432423','Damac','dsfw','test','one','2','2','marketing',12.1,23.2,21.2);
        UtilityWrapperManager.InventoryBuyerWrapper ibw = new UtilityWrapperManager.InventoryBuyerWrapper();
        UtilityWrapperManager.InventoryBuyerWrapper ibwn = new UtilityWrapperManager.InventoryBuyerWrapper(false,false, false, 32.2,3432432.234,'test','test','test','test',3, inv ,bu,mapPP,listBU);
         objAP.createPortfolio();
        Test.stopTest();
        }
        
    }
    
}
}