@isTest
public with sharing class UpdatePortalUsersTest {

    static testmethod void UpdatePortalUsersTest1() {
        UserRole userRoleObj = [SELECT Id FROM UserRole LIMIT 1];
        Id executiveProfile = [SELECT Id FROM Profile WHERE Name = 'Agent Executive Manager'].Id;
        User executiveUser = new User(alias = 'test456', email='zxc@email.com', emailencodingkey='UTF-8', 
            lastname='User 456', languagelocalekey='en_US', localesidkey='en_US', profileid = executiveProfile, 
            country='United Arab Emirates',IsActive =true, timezonesidkey='America/Los_Angeles', 
            username='zxc@email.com', UserRoleId = userRoleObj.Id);
        Contact adminContact;
        System.RunAs(executiveUser) {
            Account adminAccount = InitialiseTestData.getCorporateAccount('Test agency');
            adminAccount.Country_of_Sale__c = 'UAE';
            insert adminAccount;

            Test.startTest();
            adminContact = InitialiseTestData.getAdminContact('Test Contact', adminAccount.Id);
            adminContact.Agent_Representative__c = true;
            adminContact.Portal_Administrator__c = false;
            insert adminContact;
            Test.stopTest();
        }

    }

    static testmethod void UpdatePortalUsersTest2() {
        UserRole userRoleObj = [SELECT Id FROM UserRole LIMIT 1];
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='zxc@email.com', emailencodingkey='UTF-8', 
            lastname='User 456', languagelocalekey='en_US', localesidkey='en_US', profileid = adminProfile, 
            country='United Arab Emirates',IsActive =true, timezonesidkey='America/Los_Angeles', 
            username='zxc@email.com', UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            Account adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            adminAccount.Country_of_Sale__c = 'UAE';
            insert adminAccount;
            Contact adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            insert adminContact;
            User portalUser = InitialiseTestData.getPortalUser('t12345@test.com', adminContact.Id, 'Admin');
            insert portalUser;
            Contact contactInstance=new Contact();
            contactInstance=[select FirstName,Email,LastName,Portal_Administrator__c from Contact limit 1];
            contactInstance.FirstName='ChangeTest';
            contactInstance.LastName='ChangeLastName';
            contactInstance.Email='test@test.com';
            contactInstance.Authorised_Signatory__c = true;
            contactInstance.Portal_Administrator__c = true;
            contactInstance.Owner__c = true;
            Test.startTest();
            update contactInstance;
            Test.stopTest();
        }
    }

}