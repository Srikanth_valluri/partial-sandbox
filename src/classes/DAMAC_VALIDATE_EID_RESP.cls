public class DAMAC_VALIDATE_EID_RESP {
	public String EmiratesId;	//784198875106977
	public String EnglishName;	//MOHAMED MOIN AZFAR SHERIFF
	public String ArabicName;	//محمد معين اظفر شريف
	public String BirthDate;	//21/10/1988
	public String PlaceOfBirth;	//تاميل نادو
	public String Mobile;	//0502553669
	public String OfficePhone;	//042539206
	public String HomePhone;	//0563571969
	public String Email;	//
	public String Address;	// ,  ,  ,  , دبي
	public String PoBox;	//1080
	public String Fax;	//
	public String Sex;	//1
	public String PassportIssueDate;	//27/04/2013
	public String PassportExpiryDate;	//26/04/2023
	public String PassportNumber;	//L1016150
	public String PassportIssuePlace;	//الهند
	public String EnglishNationality;	//INDIAN
	public String ArabicNationality;	//هندى
	public String ResidencyNumber;	//20120162100977
	public String ResidencyIssueDate;	//27/03/2018
	public String ResidencyExpiryDate;	//26/03/2020
	public String UnifiedNumber;	//40560681
	public String DnrdCode;	//21
	public boolean IsSuccess;
	public cls_ValidationErrors[] ValidationErrors;
	class cls_ValidationErrors {
	}
	public static DAMAC_VALIDATE_EID_RESP parse(String json){
		return (DAMAC_VALIDATE_EID_RESP) System.JSON.deserialize(json, DAMAC_VALIDATE_EID_RESP.class);
	}

	
}