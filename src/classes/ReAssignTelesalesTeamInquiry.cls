/************************************************************************************************************************
* Name               : ReAssignTelesalesTeamInquiry                                                                     *
* Description        : Assign Inquiries to TSA's                                                                        *
* Created Date       : 04-03-2018                                                                                       *
* Created By         : ESPL                                                                                             *
* ----------------------------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                                    *
* 1.0         Craig Lobo    04-03-2018      Initial Draft.                                                              *
************************************************************************************************************************/
public class ReAssignTelesalesTeamInquiry {
    

    public List<Inquiry__c> updateInquiryOwnerList = new List<Inquiry__c>();

    public void updateInquiryOwner(List<Inquiry__c> inquiryList){
        /*List<Inquiry__c> inquiryStandList = new List<Inquiry__c>();
        List<Inquiry__c> inquiryRoadShowList = new List<Inquiry__c>();
        List<Id> tsaUserStandList = new  List<Id>();
        List<Id> tsaUserRoadShowList = new  List<Id>();

        for(TS_Assignment_Settings__c teleSalesCustomSetting : TS_Assignment_Settings__c.getall().values()
        ) {
            if(teleSalesCustomSetting.Type__c == 'Stands'){
                tsaUserStandList.add(teleSalesCustomSetting.Owner_Id__c);
            }
            if(teleSalesCustomSetting.Type__c == 'Roadshows'){
                tsaUserRoadShowList.add(teleSalesCustomSetting.Owner_Id__c);
            }
        }
        system.debug('>>>tsaUserStandList>>'+ tsaUserStandList); 
        system.debug('>>>tsaUserRoadShowList>>'+ tsaUserRoadShowList);

        for(Inquiry__c inquiryObj : inquiryList) {
            if(inquiryObj.TS_Type__c == 'Stands'){
                inquiryStandList.add(inquiryObj);
            }
            if(inquiryObj.TS_Type__c == 'Roadshows'){
                inquiryRoadShowList.add(inquiryObj);
            }
        } 

        system.debug('>>>inquiryStandList>>'+ inquiryStandList);
        system.debug('>>>inquiryRoadShowList>>'+ inquiryStandList);

        Integer standsBatchSize = 0;
        Integer roadShowBatchSize = 0;

        // Stands Inquiries
        if(inquiryStandList.Size() > 0 
            && tsaUserStandList.Size() > 0 
        ) {
             Integer inquiryCount = inquiryStandList.Size();
             Integer tsaCount = tsaUserStandList.Size();
             standsBatchSize = inquiryCount/tsaCount;
        }

        system.debug('>>>standsBatchSize>>'+ standsBatchSize);
        system.debug('>>>tsaUserStandList>>'+ tsaUserStandList);
        system.debug('>>>inquiryStandList>>'+ inquiryStandList);

        // Method to assign the Stands Inquiries to the TSA's 
        assignTSAToInquiry(inquiryStandList, tsaUserStandList, standsBatchSize);

        // RoadShow Inquiries
        if (inquiryRoadShowList.Size() > 0 
            && tsaUserRoadShowList.Size() > 0 
        ) {
            Integer inquiryCount = inquiryRoadShowList.Size();
            Integer tsaCount = tsaUserRoadShowList.Size();
            roadShowBatchSize = inquiryCount/tsaCount;
        }

        // Method to assign the RoadShow Inquiries to the TSA's 
        assignTSAToInquiry(inquiryRoadShowList, tsaUserRoadShowList, roadShowBatchSize);

        system.debug('>>>updateInquiryOwnerList>>'+ updateInquiryOwnerList);

        // DML
        update updateInquiryOwnerList;
        */
     }


    public void assignTSAToInquiry(
        List<Inquiry__c> pInquiryList, 
        List<Id> pUserIdList, 
        Integer pBatchSize
    ) {

        /*Integer inquiryCount = 0;
        if (pBatchSize == 0 
            && pInquiryList.size() > 0 
            && pUserIdList.size() > 0 
        ) {
            pBatchSize = 1;
        }

        if(pBatchSize > 0) {
            do{
                for(Id userId : pUserIdList) {
                    System.Debug('userId>>>>' + userId);
                    System.Debug('pBatchSize>>>>' + pBatchSize);
                    inquiryCount = pBatchSize;
                    for (Integer i = pInquiryList.size() - 1; i >= 0 ; --i) {
                        if (inquiryCount <= pBatchSize) {
                            System.Debug('userId>>>>' + userId);
                            pInquiryList[i].OwnerId = userId;
                            updateInquiryOwnerList.add(pInquiryList[i]);
                            pInquiryList.remove(i);
                            inquiryCount ++;
                        } else{
                            System.Debug('Break>> ' + updateInquiryOwnerList);
                            inquiryCount = 0;
                            break;
                        }
                    } 
                } // User loop
            } while(pInquiryList.size()>0);
        } // batchsize check
        */
    }

}