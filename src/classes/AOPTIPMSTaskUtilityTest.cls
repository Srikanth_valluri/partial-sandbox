/*
 * Description - Test class for AOPTIPMSTaskUtility
 *
 * Version        Date            Author            Description
 * 1.0            04/04/18        Vivek Shinde      Initial Draft
 */
@isTest
private class AOPTIPMSTaskUtilityTest {
    private static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';
    
    private static Id getRecordTypeIdForAOPT() {
        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id aoptRecordTypeID = caseRecordTypes.get('AOPT').getRecordTypeId();
        return aoptRecordTypeID;
    }
    
    static testMethod void AOPTTaskCreationTest() {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();

        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
        Id aoptRecordTypeID = getRecordTypeIdForAOPT();
        
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , aoptRecordTypeID);
        objCase.Status = 'Submitted';
        insert objCase;
    
          //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;
    
        //create Property data
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;
    
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
        }
        insert bookingUnitList;
    
        srBookingUnitList = TestDataFactory_CRM.createSRBookingUnis(objCase.Id , bookingUnitList);
        insert srBookingUnitList;
    
        objCase.Booking_Unit__c = bookingUnitList[0].Id;
        objCase.OwnerId = UserInfo.getUserId();
        update objCase;
    
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        List<Task> taskList = createTasks(objCase);
        Test.startTest();
        AOPTIPMSTaskUtility.createTaskInIPMS(taskList);
        //AOPTIPMSTaskUtility objUtility = new AOPTIPMSTaskUtility();
        //objUtility.lstTask = taskList;
        //String strCron = '0 0 23 * * ?'; 
        //system.schedule('AOPT Task Creation', strCron, objUtility);
        Test.stopTest();
    }
    
    private static List<Task> createTasks(Case objCase) {
        List<Task> taskList = new List<Task>();
        User objUser = createTestUser();
        insert objUser;
        Task objTask4 = TaskUtility.getTask((SObject)objCase, 'Update AOPT Details in IPMS', 'CRE', 
                                'AOPT', system.today().addDays(1));
        objTask4.OwnerId = objUser.Id;
        taskList.add(objTask4);
        insert taskList;
      
        return taskList;
    }
  
    public static User createTestUser() {
          String orgId = UserInfo.getOrganizationId();
          String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
          Profile objProfile = [Select Id from Profile where Name='System Administrator' LIMIT 1];
         
          Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
          String uniqueName = orgId + dateString + randomInt;
          User tuser = new User(  firstname = 'fName',
                                  lastName = 'lName',
                                  email = uniqueName + '@test' + orgId + '.org',
                                  Username = uniqueName + '@test' + orgId + '.org',
                                  EmailEncodingKey = 'ISO-8859-1',
                                  Alias = uniqueName.substring(18, 23),
                                  TimeZoneSidKey = 'America/Los_Angeles',
                                  LocaleSidKey = 'en_US',
                                  LanguageLocaleKey = 'en_US',
                                  ProfileId = objProfile.Id
                                  );
          return tuser;
    }
}