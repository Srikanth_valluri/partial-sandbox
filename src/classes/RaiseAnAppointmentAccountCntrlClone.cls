// Class to raise an appointment from Account details page
public virtual without sharing class RaiseAnAppointmentAccountCntrlClone {

    public RaiseAnAppointmentAccountCntrlClone(){} 

    public list<AppointmentWrapper> lstAppointmentWrapper {get;set;}
    public String strSelectedDate { get; set; }
    public String strPurpose { get; set; }
    public String strAccId ;
    public Account objAccount {get;set;}
    private Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
    private Id handoverCLRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Handover Calling List').getRecordTypeId();
    private Id earlyHandoverCLRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Early Handover Calling List').getRecordTypeId();    
    public Boolean processSelected {get; set;}
    public Boolean subProcessSelected {get; set;}
    public string strSelectedProcess {get;set;}
    public String strSelectedSubProcess {get;set;}
    public List<SelectOption> lstSRs {get;set;}
    public List<SelectOption> lstSubProcessOptions {get;set;}
    public List<SelectOption> lstBookingUnitOptions {get;set;}
    public map<String,booking_unit__c> mapBookingUnit {get; set;}
    public String strSelectedBookingUnits {get;set;}
    public Boolean unitSelected {get; set;}
    private Id buildingId ;
    private String hoTeamEmail;
    public Boolean blnIsBookingUnit {get;set;}
    public String strHolidayMsg     {get;set;}
    public Boolean isEnableBtn {get;set;}
    
    
    public RaiseAnAppointmentAccountCntrlClone( ApexPages.standardController controller ){
        
        strAccId = ApexPages.currentPage().getParameters().get('id');
        if( String.isNotBlank( strAccId ) ) {
            strHolidayMsg = '';
            //strSelectedDate = String.valueOf( system.today() );
            blnIsBookingUnit = false;
            init();
        }
    } //End constructor
    
    public pagereference init(){
        isEnableBtn = false;
        lstBookingUnitOptions = new List<SelectOption>();
        mapBookingUnit = new map<String,booking_unit__c>();
        system.debug(' strAccId : '+strAccId);
        if( String.isNotBlank(strAccId) ) {
            objAccount = [  SELECT Id
                                , Name 
                                , Primary_CRE__c
                                , Secondary_CRE__c
                                , Tertiary_CRE__c
                                , Primary_Language__c
                                , Email__pc
                                , Email__c,isPersonAccount
                             FROM Account
                            WHERE Id = :strAccId ];
        }

        List<Booking_Unit__c> lstBookingUnit = new List<Booking_Unit__c>();
        lstBookingUnit = [Select Id,name,Property_Name__c,Inventory__c,Booking__c,Manager_Name__c, HOS_Name__c,Okay_to_release_keys__c,Handover_Notice_Sent__c ,
                          Property_Consultant__c,Unit_Name__c,CurrencyIsoCode,Dispute_Flag__c,Handover_Notice_Sent_Date__c,EHO_Notice_Sent__c,
                          Registration_ID__c,Property_City__c,District__c,Construction_Status__c,Property_Country__c,Handover_Flag__c
                          from Booking_Unit__c where Booking__r.Account__c =:
                          strAccId AND Registration_ID__c != null
                          AND Unit_Name__c != null
                          ];

        if(lstBookingUnit.Size()>0){
            lstBookingUnitOptions.add(new SelectOption('','--None--' ));
            for(Booking_Unit__c objBookingUnit : lstBookingUnit){
                lstBookingUnitOptions.add(new SelectOption
                                         (objBookingUnit.id,objBookingUnit.Unit_Name__c ));
                mapBookingUnit.put(objBookingUnit.id,objBookingUnit);
            }
        }
        System.debug('-----mapBookingUnit---'+mapBookingUnit);
        
        map<Id, AppointmentWrapper> mapAppIdAppWrapper = new map<Id, AppointmentWrapper>();
        lstSRs = new List<SelectOption>();
        lstSRs.add(new selectOption('', '--None--'));
        
        List<Active_Process_on_Portal_Meta__mdt> lstActiveProcess = new List<Active_Process_on_Portal_Meta__mdt>();
        lstActiveProcess = [Select id,MasterLabel,CRE_MasterLabel__c,DeveloperName from Active_Process_on_Portal_Meta__mdt ];
        
        for(Active_Process_on_Portal_Meta__mdt obj : lstActiveProcess){
            if( String.isNotBlank( obj.CRE_MasterLabel__c ) ) {
                lstSRs.add(new selectoption(obj.CRE_MasterLabel__c,obj.CRE_MasterLabel__c));
            }
        }
        return null ;
        
    } //End Of Init

    public pageReference subProcess() {
        isEnableBtn = true;
        system.debug( 'strSelectedProcess : ' + strSelectedProcess );
        system.debug( 'strSelectedDate : ' + strSelectedDate );
        
        strSelectedSubProcess = '';
        if( blnIsBookingUnit == false ) {
            strSelectedBookingUnits = '';
        }
        
        if(String.isNotBlank(strSelectedProcess)) {
            processSelected = String.isNotBlank(strSelectedProcess);
            lstSubProcessOptions = new List<SelectOption>();
            lstSubProcessOptions.add(new selectOption('', '--None--'));
            List<Sub_Process_for_Portal__mdt> lstSubProcess = new List<Sub_Process_for_Portal__mdt>();
            lstSubProcess = [Select id, MasterLabel, DeveloperName,Active_Process_on_Portal__c
                            from Sub_Process_for_Portal__mdt
                            where Active_Process_on_Portal__r.CRE_MasterLabel__c  =: strSelectedProcess];
            system.debug( 'lstSubProcess : ' + lstSubProcess );
            if(lstSubProcess.Size()>0){
                for(Sub_Process_for_Portal__mdt objSubProcess : lstSubProcess ){
                    lstSubProcessOptions.add(new selectoption(objSubProcess.MasterLabel,objSubProcess.MasterLabel ));
                }
            }
            system.debug( 'lstSubProcessOptions : ' + lstSubProcessOptions );
            getAvailableAppointments();
        }else {
            lstSubProcessOptions = new List<SelectOption>();
            lstSubProcessOptions.add(new selectOption('', '--None--'));
        }
        return null;
    } // End of subProcess
    
    public pageReference createAppointment() {
        
        if( String.isNotBlank(strSelectedProcess) ) {
            list<AppointmentSelectionHandler.AppointmentWrapper> lstwrappers = new 
            list<AppointmentSelectionHandler.AppointmentWrapper>();
        
            for (AppointmentWrapper objWrap : lstAppointmentWrapper) {
                if (objWrap.isSelected == true) {
                AppointmentSelectionHandler.AppointmentWrapper newWrap = new 
                    AppointmentSelectionHandler.AppointmentWrapper(objWrap.objApp, objWrap.objCL, '', true);
                lstwrappers.add(newWrap);
                }
            }
            if( String.isNotBlank(strSelectedDate)) {
                AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
                handler.createAppointments(lstwrappers, strPurpose, strSelectedBookingUnits,
                    objAccount, strSelectedSubProcess, strSelectedProcess, strSelectedDate);
                    
                if( blnIsBookingUnit == false ) {
                    PageReference nextPage = new PageReference('/' + strAccId);
                    return nextPage;
                } else {
                    PageReference nextPage = new PageReference('/' + strSelectedBookingUnits);
                    return nextPage;
                }
            } else {
                ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.Error,'Please select Appointment Date'));
            }
             
        } else {
            ApexPages.addmessage(new ApexPages.message(
            ApexPages.severity.Error,'Please select Process Name'));
    
        }
        return null; 
    } //End of createAppointment
    
    // method used to format date to yyyy-MM-dd
    public static String formatDate( String dt ) {
        if ( String.isNotBlank( dt ) ) {
            list<String> lstDateComponents;
            system.debug('== Date1 =='+dt );
            if( dt.contains('/') ) {
                lstDateComponents = dt.split('/');
            }
            else if( dt.contains('-') ) {
                lstDateComponents = dt.split('-');
            }
            
            Date objDateToFormat = date.newinstance( Integer.valueOf( lstDateComponents[2] ), Integer.valueOf( lstDateComponents[0] ), Integer.valueOf( lstDateComponents[1] ));
            system.debug('== Date2 =='+dt );
            return DateTime.newInstance(objDateToFormat.year(),objDateToFormat.month(),objDateToFormat.day()).format('yyyy-MM-dd');
        }
        return null;
    } //End of formatDate
  
    public pageReference getAvailableAppointments() {

        lstAppointmentWrapper = new list<AppointmentWrapper>();

        
        map<Id, AppointmentWrapper> mapAppIdAppWrapper = new map<Id, AppointmentWrapper>();
        

        subProcessSelected = String.isNotBlank(strSelectedSubProcess);
        unitSelected = String.isNotBlank(strSelectedBookingUnits);

        /*if (unitSelected) {
            mapBookingUnit.put(strSelectedBookingUnits, new Booking_Unit__c());
        }*/
        
        strSelectedDate = String.valueOf( formatDate( strSelectedDate ) );
        system.debug('== Date3 =='+strSelectedDate );           
            
        if( String.isNotBlank( strSelectedDate ) ) {
            system.debug( 'strSelectedProcess : '+ strSelectedProcess );
            system.debug( 'strSelectedSubProcess : '+ strSelectedSubProcess );
            system.debug( 'strSelectedBookingUnits : '+ strSelectedBookingUnits );
            AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
            for (AppointmentSelectionHandler.AppointmentWrapper objWrap : 
                handler.availableSlots(objAccount.Id, strSelectedBookingUnits, 
                strSelectedProcess, strSelectedSubProcess, 'CRE', strSelectedDate,false)) {
                system.debug('!!!!!!objWrap'+objWrap);            
                if (!string.isBlank(objWrap.ErrorMessage) ){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 
                    objWrap.ErrorMessage));
                    return null;
                } else {
                    AppointmentWrapper objSelectedWrap = 
                    new AppointmentWrapper(objWrap.objApp, objWrap.objCL);
                    lstAppointmentWrapper.add(objSelectedWrap);
                 }
            }
        }
        return null;
    } //End of getAvailableAppointments    

    public class AppointmentWrapper {
        public Appointment__c objApp {get;set;}
        public Calling_List__c objCL {get;set;}
        public Boolean isSelected{get;set;}
        
        public AppointmentWrapper() {}
        
        public AppointmentWrapper( Appointment__c tempObjApp, Calling_List__c tempObjCL ) {
            objApp = tempObjApp ;
            objCL = tempObjCL ;
        }
    } //End of AppointmentWrapper
    
}