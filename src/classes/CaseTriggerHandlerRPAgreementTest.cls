@isTest
public class CaseTriggerHandlerRPAgreementTest {
    static Account objAcc;
    static Id recTypeRPAgreement;
    static NSIBPM__Service_Request__c objDealSR;
    static list<Booking__c> listCreateBookingForAccount;
    static list<Booking_Unit__c> listCreateBookingUnit;
    
    static void init() {
        objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);

        recTypeRPAgreement = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Agreement').getRecordTypeId();
        system.debug('recTypeRPAgreement=='+recTypeRPAgreement);

        objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        insert listCreateBookingUnit;
    }

    @isTest static void testCaseClosureRPAgreementSuccess() {
        init();

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Commencement_Date__c = date.today().addDAys(2);
        insert objCase;

        objCase.status = 'Closed';
        update objCase;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(1));
        test.StopTest();
    }

    @isTest static void testCaseClosureRPAgreementNoResponse() {
        init();

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Commencement_Date__c = date.today().addDAys(2);
        insert objCase;

        objCase.status = 'Closed';
        update objCase;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(2));
        test.StopTest();
    }

    @isTest static void testCaseClosureRPAgreementOuterFailure() {
        init();

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Commencement_Date__c = date.today().addDAys(2);
        insert objCase;

        objCase.status = 'Closed';
        update objCase;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(3));
        test.StopTest();
    }

    @isTest static void testCaseClosureRPAgreementInnerFailure() {
        init();

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Commencement_Date__c = date.today().addDAys(2);
        insert objCase;

        objCase.status = 'Closed';
        update objCase;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(4));
        test.StopTest();

        CaseTriggerHandlerRPAgreement.RPDocDetailsWrapper caseRpObj = new CaseTriggerHandlerRPAgreement.RPDocDetailsWrapper();
        caseRpObj.ATTRIBUTE1 = 'test';
        caseRpObj.ATTRIBUTE2 = 'test';
        caseRpObj.ATTRIBUTE3 = 'test';
        caseRpObj.ATTRIBUTE4 = 'test';
        caseRpObj.PARAM_ID = 'test';
    }
}