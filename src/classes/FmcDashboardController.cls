public without sharing class FmcDashboardController {

    public List<SR_Attachments__c>  lstNotice           {get; set;}
    public List<SR_Attachments__c>  lstNewsletter       {get; set;}
    public List<SR_Attachments__c>  lstPhoto            {get; set;}
    public Id                       customerAccountId   {get; set;}
    public Boolean                  isOwner             {get; set;}
    public Date                     passportExpiryDate  {get; set;}


    public FmcDashboardController() {
        if (FmcUtils.isCurrentView('Home') || FmcUtils.isCurrentView('')) {
            customerAccountId = CustomerCommunityUtils.customerAccountId;
            isOwner = FmcUtils.isOwner();
            System.debug('isOwner = ' + isOwner);
            lstNotice = fetchUserDocuments('Notice', 2);
            lstNewsletter = fetchUserDocuments('Newsletter', 2);
            lstPhoto = fetchPhotos();
            passportExpiryDate = CustomerCommunityUtils.getPassportExpiryDate();
        }
    }

    public Integer getOpenServiceRequestsCount() {
        return (Integer) [
            SELECT  COUNT(Id) total
            FROM    FM_Case__c
            WHERE   Account__c = :CustomerCommunityUtils.customerAccountId
                    AND Status__c IN  ('Submitted', 'Awaiting Correction')
        ][0].get('total');
    }

    public Integer getClosedServiceRequestsCount() {
        return (Integer) [
            SELECT  COUNT(Id) total
            FROM    FM_Case__c
            WHERE   Account__c = :CustomerCommunityUtils.customerAccountId
                    AND Status__c IN :FmcUtils.CLOSED_CASE_STATUSES
        ][0].get('total');
    }

    public Integer getViolationNoticesCount() {
        Set<Id> unitIds = new Set<Id>();
        Set<Id> buildingIds = new Set<Id>();
        for (Booking_Unit__c unit : FmcUtils.queryUnitsForAccount(
                CustomerCommunityUtils.customerAccountId,
                'Id, Inventory__c, Inventory__r.Building_Location__c'
        )) {
            unitIds.add(unit.Id);
            if (unit.Inventory__c != NULL) {
                buildingIds.add(unit.Inventory__r.Building_Location__c);
            }
        }

        return (Integer) [
            SELECT  COUNT(Id) total
            FROM    SR_Attachments__c
            WHERE   Document_Type__c = 'Violation Notice'
                    AND FM_Account__c = :CustomerCommunityUtils.customerAccountId
                    AND (Building__c IN :buildingIds
                        OR Booking_Unit__c IN :unitIds)
        ][0].get('total');
    }

    private static List<SR_Attachments__c> fetchUserDocuments(String documentType, Integer docLimit) {
        List<SR_Attachments__c> docs = new List<SR_Attachments__c>();
        for (List<SR_Attachments__c> lstDoc : FmcDocumentsController.fetchDocumentsOfType(
                documentType, docLimit
            ).values()
        ) {
            docs.addAll(lstDoc);
        }
        return docs;
    }

    private static List<SR_Attachments__c> fetchPhotos() {
        Set<Id> setLocationIds = new Set<Id> {NULL};
        setLocationIds.addAll(new Map<Id, Location__c>(
            FmcUtils.queryLocationsForAccount(CustomerCommunityUtils.customerAccountId)
        ).keySet());
        return [
            SELECT      Id
                        , Building__c
                        , Building__r.Name
                        , Building__r.Building_Name__c
                        , Name
                        , Description__c
                        , LastModifiedDate
                        , Attachment_URL__c
                        , Parent_Id__c
                        , Parent_Id__r.Building__c
                        , Parent_Id__r.Building__r.Name
                        , Parent_Id__r.Building__r.Building_Name__c
            FROM        SR_Attachments__c
            WHERE       Document_Type__c = 'Photo'
                        AND Parent_Id__r.Building__c IN: setLocationIds
            ORDER BY    Document_Type__c DESC, Building__c
            LIMIT       1000
        ];

    }

    @RemoteAction
    public static FmIpmsRestServices.DueInvoicesResult fetchDueInvoiceData() {
        return FmIpmsRestServices.getDueInvoices('', CustomerCommunityUtils.getPartyId(), '');
    }
}