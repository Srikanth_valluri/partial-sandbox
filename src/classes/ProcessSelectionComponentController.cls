/*
 * Description - Controller class for ProcessSelectionComponent visualforce component
 *
 * Version        Date            Author            Description
 * 1.0            28/01/18        Vivek Shinde      Initial Draft
 */
public without sharing class ProcessSelectionComponentController {

    public ProcessSelectionComponentController(){
    }
    
    // JS Remoting action called when searching for a movie name
    @RemoteAction
    public static List<Account> searchMovie(String searchTerm) {
        System.debug('Account Name is: '+searchTerm );
        List<Account> lstAccount = Database.query('Select Id, Name from Account where name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'');
        return lstAccount;
    }
}