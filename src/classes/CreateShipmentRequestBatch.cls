global class CreateShipmentRequestBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful{
    Map<Id, List<Case>> mapAcountToCases = new map<id, list<Case>>();
    String strCourierService;
    List<Account> lstAccountToProcess;
    List<Courier_Delivery__c> lstCourier;
    List<Id> courierIds = new List<Id>();

    map<Id, list<CreateShipmentBookingUnitsListCtrl.CourierDetails>> mapAccountToBUs = 
                                    new map<Id, list<CreateShipmentBookingUnitsListCtrl.CourierDetails>>();

    
    global CreateShipmentRequestBatch(Map<Id, List<CreateShipmentBookingUnitsListCtrl.CourierDetails>> mapAccToBU, String strCourierService){
        this.mapAccountToBUs = mapAccToBU;
        this.strCourierService = strCourierService;  
    }

    global CreateShipmentRequestBatch(Map<Id, List<Case>> mapAcountToCases, String strCourierService){
        this.mapAcountToCases = mapAcountToCases;
        this.strCourierService = strCourierService;  
    }
    
    global List<Account> start(Database.BatchableContext BC){
        lstAccountToProcess = [Select id
                                    , Name
                                    , Master_Address__c
                                    , Zip_Postal_Code__c
                                    , City__c
                                    , Country__c
                                    , Phone_1__c
                                    , Phone_2__c
                                    , Mobile_Person_Business__c
                                    , Fax__c
                                    , IsPersonAccount 
                                    , City__pc 
                                    , Country__pc
                                    , Zip_Postal_Code_Acc__pc
                                    , Fax_Acc__pc
                                    , Mobile_Phone_Encrypt__pc
                                    , Mobile__c 
                                 From Account 
                                Where Id IN :mapAcountToCases.keySet() OR Id IN : mapAccountToBUs.keySet()];
        
        return lstAccountToProcess;
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope){
        List<Courier_Delivery__c> lstCourierToProcess = new List<Courier_Delivery__c>();
        List<Courier_Item__c> lstCourierItemsToInsert = new List<Courier_Item__c>();
        List<Case> lstCasesToUpdate = new List<Case>();
        List<String> unitNameslst;
        System.debug('===>scope'+scope);
        lstCourier = new List<Courier_Delivery__c>();
        for(Account objAccount : scope){
            Courier_Delivery__c objCD = createCourier(objAccount);
            if(mapAccountToBUs.containsKey(objAccount.Id)){
                if(mapAccountToBUs.get(objAccount.Id).size() == 1) {
                    objCD.UnitName__c = mapAccountToBUs.get(objAccount.Id)[0].BookingUnit.Id;
                }
                if(mapAccountToBUs.get(objAccount.Id).size() > 0) {
                    unitNameslst = new List<String>();
                    for(CreateShipmentBookingUnitsListCtrl.CourierDetails objCourierD : mapAccountToBUs.get(objAccount.Id)) {
                        unitNameslst.add(objCourierD.BookingUnit.Unit_Name__c);
                    }
                    System.debug('unitNameslst>>>>'+unitNameslst);
                    if(unitNameslst != null && unitNameslst.size() > 0) {
                        String strUnitName = String.join( unitNameslst, ', ' ); 
                        objCD.Unit_Names__c = strUnitName;
                    }
                    
                }
                objCD.Type_of_Document__c = mapAccountToBUs.get(objAccount.Id)[0].CourierDelivery.Type_of_Document__c;
            }
            lstCourierToProcess.add(objCD);
        }
        
        if(!lstCourierToProcess.isEmpty()){
            if(strCourierService == 'First Flight'){
                lstCourier = FirstFlightService.generateAirwayBill(lstCourierToProcess);
                System.debug('===>lstCourier'+lstCourier);
            }else if(strCourierService == 'Aramex' || strCourierService == 'Aramex CDS'){  
                lstCourier = AramexService.createShipping(strCourierService,lstCourierToProcess);   
                System.debug('===>lstCourier'+lstCourier);
            }
                    
        }
        
        if(!lstCourier.isEmpty()){
            Insert lstCourier;    
        }
        
        for(Courier_Delivery__c courier : lstCourier){
            courierIds.add(courier.Id);
            if(mapAcountToCases.containsKey(courier.Account__c)){
                for(Case objCase : mapAcountToCases.get(courier.Account__c)){
                    Courier_Item__c courierItem = new Courier_Item__c();
                    courierItem.Courier_Delivery__c = courier.Id;
                    courierItem.Case__c = objCase.Id; 
                    lstCourierItemsToInsert.add(courierItem);
                    
                    if(courier.Courier_Status__c == 'Success'){
                        objCase.Courier_Request_Already_Created__c = true;
                        //objCase.Airway_Bill_Number__c = courier.Airway_Bill__c;
                        lstCasesToUpdate.add(objCase);
                    }
                }
            }

            if(mapAccountToBUs.containsKey(courier.Account__c)){
                for(CreateShipmentBookingUnitsListCtrl.CourierDetails objCD : mapAccountToBUs.get(courier.Account__c)){
                    Courier_Item__c courierItem = new Courier_Item__c();
                    courierItem.Courier_Delivery__c = courier.Id;
                    //courierItem.Case__c = objCase.Id; 
                    courierItem.Booking_Unit__c = objCD.BookingUnit.Id;
                    lstCourierItemsToInsert.add(courierItem);
                }
            }
        }
        
        if(!lstCourierItemsToInsert.isEmpty()){
            Insert lstCourierItemsToInsert;    
        }
        
        if(!lstCasesToUpdate.isEmpty()){
            Update lstCasesToUpdate;    
        }
            
    }
    
    global void finish(Database.BatchableContext BC){
      system.debug('courierIds>>>>>>'+courierIds);
      GenerateAirwayBillBatch batchInst = new GenerateAirwayBillBatch(courierIds);
      Id jobId = Database.executeBatch(batchInst,1);       
    }
    
    public Courier_Delivery__c createCourier(Account objAccount){
        Courier_Delivery__c objCourier = new Courier_Delivery__c();
        objCourier.Account__c = objAccount.Id;
        if(objAccount.IsPersonAccount) {
            //objCourier.Consignee_Name__c = objAccount.Name;            
            objCourier.Postal_Code__c = objAccount.Zip_Postal_Code_Acc__pc;
            objCourier.City__c = objAccount.City__pc;
            objCourier.Country__c = objAccount.Country__pc;
            objCourier.Phone_1__c = objAccount.Mobile_Phone_Encrypt__pc;
            objCourier.Phone_2__c = objAccount.Mobile__c;
            objCourier.Fax__c = objAccount.Fax_Acc__pc;
        } else {
            //objCourier.Company_Name__c = objAccount.Name;
            objCourier.Postal_Code__c = objAccount.Zip_Postal_Code__c;
            objCourier.City__c = objAccount.City__c;
            objCourier.Country__c = objAccount.Country__c;
            objCourier.Phone_1__c = objAccount.Phone_1__c;
            objCourier.Phone_2__c = objAccount.Phone_2__c;
            objCourier.Fax__c = objAccount.Fax__c;
        }
        
        objCourier.Address_1__c = objAccount.Master_Address__c;
        objCourier.Company_Name__c = objAccount.Name;
        objCourier.Consignee_Name__c = objAccount.Name;
        objCourier.Courier_Service__c = strCourierService;

        /*objCourier.Address_1__c = objAccount.Master_Address__c;
        objCourier.Postal_Code__c = objAccount.Zip_Postal_Code__c;
        objCourier.City__c = objAccount.City__c;
        objCourier.Country__c = objAccount.Country__c;
        objCourier.Phone_1__c = objAccount.Mobile_Person_Business__c;
        objCourier.Phone_2__c = objAccount.Phone_2__c;
        objCourier.Fax__c = objAccount.Fax__c;*/
        
        if(mapAcountToCases.get(objAccount.Id) != null)
            objCourier.Number_of_Pieces__c = String.valueOf(mapAcountToCases.get(objAccount.Id).size());
        else if(mapAccountToBUs.get(objAccount.Id) != null)
            objCourier.Number_of_Pieces__c = String.valueOf(mapAccountToBUs.get(objAccount.Id).size());
        else
            objCourier.Number_of_Pieces__c = '1';
        return objCourier;
    }
}