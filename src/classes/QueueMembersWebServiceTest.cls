/*
* Test class for QueueMemberWebService Class
*/
@isTest
public class QueueMembersWebServiceTest {

    /**
     * Method to test request with blank calling number
     */
    public static testMethod void testBlankQueueExtension() {
        QueueMembersLogic.QueueMembersRequestBody queueExtensionRequest = new QueueMembersLogic.QueueMembersRequestBody();
        queueExtensionRequest.queueExtension = '';
        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestUri = '/queueMembers';
        req.requestBody = Blob.valueOf(JSON.serializePretty(queueExtensionRequest));
        RestContext.request = req;
        RestContext.response = new RestResponse();
        Test.startTest();
        QueueMembersWebService.doPost();
        Test.stopTest();

        String queueExtensionResponse = RestContext.response.responseBody.toString();
        System.assert(queueExtensionResponse != null);
        QueueMembersLogic.QueueMembersResponseBody response =
            (QueueMembersLogic.QueueMembersResponseBody) JSON.deserializeStrict(
                queueExtensionResponse,
                QueueMembersLogic.QueueMembersResponseBody.Class
            );
        System.assertEquals(response.errorCode, '400');
        System.assertEquals(response.errorMessage, 'No extension found');
    }

    /**
     * Method to test request with no cre pool
     */
    public static testMethod void testNoCrePool() {
        CreRoutingTest.createCrePoolDetails();
        CreRoutingTest.createCollectionPaymentPool();
        QueueMembersLogic.QueueMembersRequestBody queueExtensionRequest = new QueueMembersLogic.QueueMembersRequestBody();
        queueExtensionRequest.queueExtension = '0000';
        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestUri = '/queueMembers';
        req.requestBody = Blob.valueOf(JSON.serializePretty(queueExtensionRequest));
        RestContext.request = req;
        RestContext.response = new RestResponse();
        Test.startTest();
        QueueMembersWebService.doPost();
        Test.stopTest();

        String queueExtensionResponse = RestContext.response.responseBody.toString();
        System.assert(queueExtensionResponse != null);
        QueueMembersLogic.QueueMembersResponseBody response =
            (QueueMembersLogic.QueueMembersResponseBody) JSON.deserializeStrict(
                queueExtensionResponse,
                QueueMembersLogic.QueueMembersResponseBody.Class
            );
        System.assertEquals(response.errorCode, '400');
        System.assertEquals(response.errorMessage, 'No pool found for requested extension');
    }

    /**
     * Method to test request with no users in cre pool
     */
    public static testMethod void testNoUsersInCrePool() {
        CreRoutingTest.createCrePoolDetails();
        CreRoutingTest.createCollectionPaymentPool();
        QueueMembersLogic.QueueMembersRequestBody queueExtensionRequest = new QueueMembersLogic.QueueMembersRequestBody();
        queueExtensionRequest.queueExtension = '6101';
        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestUri = '/queueMembers';
        req.requestBody = Blob.valueOf(JSON.serializePretty(queueExtensionRequest));
        RestContext.request = req;
        RestContext.response = new RestResponse();

        Test.startTest();
        QueueMembersWebService.doPost();
        Test.stopTest();

        String queueExtensionResponse = RestContext.response.responseBody.toString();
        System.assert(queueExtensionResponse != null);
        QueueMembersLogic.QueueMembersResponseBody response =
            (QueueMembersLogic.QueueMembersResponseBody) JSON.deserializeStrict(
                queueExtensionResponse,
                QueueMembersLogic.QueueMembersResponseBody.Class
            );
        System.assertEquals(response.errorCode, '400');
        System.assertEquals(response.errorMessage, 'No users found in pool');
    }

    /**
     * Method to test request with user in cre pool
     */
    public static testMethod void testUserInCrePool() {
        CreRoutingTest.createCrePoolDetails();
        CreRoutingTest.createCollectionPaymentPool();
        User creUser = CreRoutingTest.createCREUsers('creTest@primaryUser.com', '7000');
        System.assert(creUser != null);
        Group publicGroup = CreRoutingTest.createPublicGroup('CRE Pool (Arabic) Test');
        System.assert(publicGroup != null);

        System.runAs(new User(Id = UserInfo.getUserID())) {
            CreRoutingTest.addUserToPublicGroup('CRE Pool (Arabic) Test', 'creTest@primaryUser.com');
        }

        QueueMembersLogic.QueueMembersRequestBody queueExtensionRequest = new QueueMembersLogic.QueueMembersRequestBody();
        queueExtensionRequest.queueExtension = '6121';
        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestUri = '/queueMembers';
        req.requestBody = Blob.valueOf(JSON.serializePretty(queueExtensionRequest));
        RestContext.request = req;
        RestContext.response = new RestResponse();

        Test.startTest();
        QueueMembersWebService.doPost();
        Test.stopTest();

        String queueExtensionResponse = RestContext.response.responseBody.toString();
        System.assert(queueExtensionResponse != null);
        QueueMembersLogic.QueueMembersResponseBody response =
            (QueueMembersLogic.QueueMembersResponseBody) JSON.deserializeStrict(
                queueExtensionResponse,
                QueueMembersLogic.QueueMembersResponseBody.Class
            );
        System.assertEquals(1, response.extensions.size());
        System.assertEquals('7000', response.extensions[0]);
    }
}