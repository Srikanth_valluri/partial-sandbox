public withOut sharing class AP_CompanyAddAgentController {
     
     public Id arRTId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get(
        'Agent Registration').getRecordTypeId();
    public Id auRTId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get(
        'Agents Portal SR').getRecordTypeId();
    public Id auRTId1 = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get(
        'Agent Update').getRecordTypeId();
    public Amendment__c currAmnd {get; set;}
    ApexPages.StandardController controller {get; set;}
    public integer countPerson {get; set;}
    public Boolean EditCompanySR  { get; set; }
    public Boolean isGuestUser  {get; set;}
    public Boolean isCommunity {get; set;}
    public Boolean isAdd {get; set;}
    public NSIBPM__Service_Request__c objSR {get; set;}
    public String recordTypeName  {get; set;}
    public Map<String, String> mapParameters;
    public NSIBPM__Service_Request__c objFinSR  {get; set;}
    public String strAppId {get; set;}
    public List<Amendment__c> amendmentList {get; set; }
    public Boolean addAgentSR {get; set;}
    public Boolean showToaster {get; set;}

    public AP_CompanyAddAgentController(ApexPages.StandardController controller) {
        amendmentList = new List<Amendment__c>();
        showToaster = false;
        Id  SRId = apexpages.currentPage().getParameters().get('Id');
        if (srId != NULL) {
            initRef (srId);
        }
    }
    public void initRef (Id srId) {
        isAdd = false;
        isGuestUser = false;
        countPerson = 1;
        iscommunity = false;
        this.controller = controller;
        
        system.debug('objSR Const==>'+objSR);
        objSR = [SELECT Id,City_Of_Incorporation_New__c, CurrencyIsoCode,RecordTypeId,
                        Agent_Registration_Type__c,Agency_Corporate_Type__c,NSIBPM__SR_Template__r.Name,NSIBPM__Customer__c
                   FROM NSIBPM__Service_Request__c
                 WHERE id=: srid];
        recordTypeName = objSR.NSIBPM__SR_Template__r.Name;
        system.debug('objSR NSIBPM__SR_Template__c==>'+objSR.NSIBPM__SR_Template__r.Name);  
        objSR.Agency_Corporate_Type__c = 'Real Estate';
        system.debug('objSR Const new==>'+objSR);      
        //if there is a contact for this logged in user, then it is a community user
        for(User objUsr:[SELECT Id, Profile.UserLicense.Name, ContactId, Contact.Email,
                                Contact.AccountId, Contact.FirstName, Contact.LastName, 
                                Contact.Account.Website 
                           FROM User 
                          WHERE Id = :UserInfo.getUserId()
        ]) {
            if(objUsr.Profile.UserLicense.Name == 'Guest User License'){
                isGuestUser = true;
            } else{
                if(objUsr.ContactId!=null)
                    isCommunity = true;
             }
        }

        if (apexpages.currentPage().getParameters() != null) {
            mapParameters = apexpages.currentPage().getParameters();
        }
        EditCompanySR = false;
        addAgentSR = true;
        if(mapParameters.get('EditAgentSR') != null){
            addAgentSR = Boolean.valueOf(mapParameters.get('EditAgentSR'));
        }
        strAppId = srId;
        system.debug('strAppId'+strAppId);
        objFinSR = new NSIBPM__Service_Request__c();
        if (strAppId != null) {
            if (mapParameters != null ) {
                //query all the fields configured in the page are query here.
                String strQuery = '';
                strQuery += UtilityQueryManager.getAllFields(NSIBPM__Service_Request__c.getsObjecttype().getDescribe()) ;
                strQuery += '  WHERE Id =:strAppId ';
                system.debug('strQuery>>> '+strQuery);
                for (NSIBPM__Service_Request__c SR : database.query(strQuery)) {
                    objFinSR = SR;
                }
            }
        }
        String amdQuery = UtilityQueryManager.getAllFields(Amendment__c.getsObjectType().getDescribe());
        amdQuery += ' WHERE Service_Request__c =: strAppId ';
        amendmentList = Database.query(amdQuery); 
        currAmnd = new Amendment__c(); 
    }
    /* to display fields of new agent */
    public void setIsAdd(){
        isAdd = true;
        showToaster = false;
    }
    /* create new amendment for new contact(Agent) */
    public void insertAmendment(){
        System.Debug(currAmnd);
       String validate = validateAmendment(currAmnd); 
       if (validate != 'Success'){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, validate));
        }
        else{
            //Id contactId = insertContact(currAmnd);
            currAmnd.Account__c  = objSR.NSIBPM__Customer__c;
           // currAmnd.Contact__c = contactId;
            currAmnd.Service_Request__c = objFinSR.Id;
            insert currAmnd;
            amendmentList.add(currAmnd);
            isAdd = false;
            showToaster = true;
            currAmnd = new Amendment__c();
            countPerson ++;
        }
        
    }
    /* insert new contact and portal user with same new contact details */ 
    /*public Id insertContact(Amendment__c currAmndEach){
        TriggerFactoryCls.BYPASS_ON_TEST = true;
        Contact contactForAmendment = new Contact();
        contactForAmendment.LastName = currAmndEach.Last_Name__c;
        contactForAmendment.FirstName = currAmndEach.First_Name__c;
        contactForAmendment.Email = currAmndEach.Email__c;
        contactForAmendment.ID_Type__c = currAmndEach.ID_Type__c;
        contactForAmendment.ID_Number__c = currAmndEach.ID_Number__c;
        contactForAmendment.ID_Issue_Date__c = currAmndEach.ID_Issue_Date__c;
        contactForAmendment.ID_Expiry_Date__c = currAmndEach.ID_Expiry_Date__c;*/
        /*if(objSR.Agency_Corporate_Type__c == 'Real Estate'){
            contactForAmendment.Broker_Card_Number__c = currAmndEach.Broker_Card_Number__c;
        }*/
       /* contactForAmendment.Mobile_Country_Code__c = currAmndEach.Mobile_Country_Code__c;
        contactForAmendment.Phone = currAmndEach.Mobile__c;
        contactForAmendment.Designation__c = currAmndEach.Designation__c;
        contactForAmendment.AccountId = objSR.NSIBPM__Customer__c;
        contactForAmendment.Portal_Administrator__c = currAmndEach.Portal_Administrator__c;
        contactForAmendment.Agent_Representative__c = currAmndEach.Agent_Representative__c;
        insert contactForAmendment;

        Profile profile1;
        if(contactForAmendment.Portal_Administrator__c == true && contactForAmendment.Agent_Representative__c != true)
            profile1 = [Select Id from Profile where name = 'Customer Community - Admin'];
        else if(contactForAmendment.Portal_Administrator__c != true && contactForAmendment.Agent_Representative__c == true)
            profile1 = [Select Id from Profile where name = 'Customer Community - Agent'];
        else if(contactForAmendment.Portal_Administrator__c == true && contactForAmendment.Agent_Representative__c == true)
            profile1 = [Select Id from Profile where name = 'Customer Community - Agent + Admin'];
            
        
        User portalContactOwner = new User();
            portalContactOwner.ProfileId = profile1.Id;
            portalContactOwner.Username = contactForAmendment.Email;
            portalContactOwner.Alias = contactForAmendment.LastName.Length() > 8 ?contactForAmendment.LastName.subString(0,8): contactForAmendment.LastName;
            portalContactOwner.Email = contactForAmendment.Email;
            portalContactOwner.EmailEncodingKey='UTF-8';
            portalContactOwner.Firstname = contactForAmendment.FirstName;
            portalContactOwner.Lastname = contactForAmendment.LastName;
            portalContactOwner.ContactId = contactForAmendment.Id;
            portalContactOwner.LanguageLocaleKey='en_US';
            portalContactOwner.LocaleSidKey='en_US';
            portalContactOwner.TimeZoneSidKey='America/Chicago';
            
        Database.insert(portalContactOwner);
        
        Contact con = new Contact ();
        con.Salesforce_User__c = portalContactOwner.Id;
        con.Id = contactForAmendment.Id;
        Update con;
       
        return contactForAmendment.Id;

    }*/
    /* create unit documents for new amendments */
    public pagereference createDocumentsForAmendments(){
        objFinSR.Filled_Page_Ids__c = 'Company-05';
        if(!(objFinSR.Filled_Page_Ids__c).contains('Company-05')){
           objFinSR.Filled_Page_Ids__c += ',Company-05';
        }
        objFinSR.RecordTypeId = auRTId1;
        TriggerFactoryCls.BYPASS_UPDATE_TRIGGER = true;
        update objFinSR;
        String strQuery = '';
        strQuery += UtilityQueryManager.getAllFields(NSIBPM__Service_Request__c.getsObjecttype().getDescribe()) ;
        strQuery += '  WHERE Id =:strAppId ';
        system.debug('strQuery>>> '+strQuery);
        for (NSIBPM__Service_Request__c SR : database.query(strQuery)) {
            objFinSR = SR;
        }
        System.Debug(amendmentList);
        
        if(amendmentList.size() > 0){
            
            for(Amendment__c currAmnd : amendmentList){
                    if(SR_Process__c.getInstance(userinfo.getProfileId()).New_Document_Process__c) {
                        AP_SRUtility.createUnitDocuments(new List<Amendment__c>{currAmnd}, objFinSR, ''); 
                    }
                    else {
                        AP_SRUtility.createSRDocuments(currAmnd, objFinSR); 
                    }
            }
        }
        PageReference pg = Page.AP_UploadDocuments;
            pg.getParameters().put('id',objSR.id);
            pg.getParameters().put('EditAgentSR','true');
            return pg;
    }
    /* validating new amendments to be created */
    public String validateAmendment(Amendment__c amd) { 
        String errorStr = 'Success';
        if (!(amd.Owner__c  || amd.Portal_Administrator__c  
            || amd.Agent_Representative__c  || amd.Authorised_Signatory__c)) {
            errorStr = 'Please select at least one role';
        }
        
        
        
        if (objSR.Agent_Registration_Type__c != 'LOI') {
            if(amd.Authorised_Signatory__c && (amd.Designation__c == NULL || amd.Designation__c == '')){
                errorStr = 'Designation is required for Authorised Signatory';
            }
           /* if (amd.Agent_Representative__c && objFinSR.City_Of_Incorporation_New__c == 'Dubai' 
                && objFinSR.Agency_Corporate_Type__c == 'Real Estate' 
                && (amd.Broker_Card_Number__c == NULL || amd.Broker_Card_Expiry_Date__c == NULL)
            ) {
                //errorStr = 'Broker Card No. and Broker Card Expiry Date is required for Agents.';
            }*/
        }

        system.debug('>>>amd.amd.ID_Issue_Date__c>>>'+amd.ID_Issue_Date__c);
        system.debug('>>>amd.amd.ID_Issue_Date__c>>>'+date.today());
        if (amd.ID_Issue_Date__c > date.today()) {
            errorStr = 'ID Issue Date cannot be in the future';
        }
        if (amd.ID_Expiry_Date__c < date.today()) {
            errorStr = 'ID Expiry Date cannot be in the past' ;
        }
        if (amd.Broker_Card_Expiry_Date__c < date.today()) {
            //errorStr = 'Broker Expiry Date cannot be in the past';
        }
        if (amd.Email__c != null) {
            for (User u : [SELECT EMail FROM User WHERE Email =: amd.Email__c LIMIT 1]) {
                errorStr = 'Email already exists.';
            }
        }
        return errorStr;
    }
}