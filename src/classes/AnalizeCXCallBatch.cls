global class AnalizeCXCallBatch implements Database.Batchable<sObject> {
    /*
    
    Account objAcc = [
        SELECT Id ,(SELECT Id,Hours__c,Call_Type__c FROM Call_Logs__r WHERE Call_Type__c ='Outbound' AND CreatedDate = LAST_N_DAYS:180)  FROM Account WHERE Id ='0011n00002GJk3H' LIMIT 1];

        System.debug(objAcc);
        System.debug(objAcc.Call_Logs__r );
    */
    String query;
    public static Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
     
    global Database.QueryLocator start(Database.BatchableContext BC) {
    
        String recordId =  Label.dailyDpBatchForSingleUser.split('-',2)[1];
        
        query = 'SELECT Id ,Best_Time_To_Call__c,Second_Best_Time_To_Call__c ,(SELECT Id,Hours__c,Call_Type__c  FROM Call_Logs__r WHERE Call_Type__c =\'Outbound\' AND CreatedDate = LAST_N_DAYS:180)' ;
        query +=' FROM Account WHERE RecordTypeId =: personAccRTId AND Active_Customer__c = \'Active\' AND  Best_Time_To_Call__c = NULL ';
        if(Label.dailyDpBatchForSingleUser.split('-',2)[0] == 'ON'){
            query +=' AND ID = :recordId';
        }
     
               
        system.debug( ' query : ' + query );        
        return Database.getQueryLocator(query);
    }
     global void execute( Database.BatchableContext BC, List<Account> accLst ) {
        system.debug( ' accLst : ' + accLst.size());
        system.debug( ' accLst : ' + accLst );
        List<Account> accToUpdate = new List<Account>();
        
        for( Account accObj : accLst ) {
            map<Decimal,Decimal> mapHoursFrequecy = new map<Decimal,Decimal> ();
            for( Call_Log__c clObj : accObj.Call_Logs__r ) {
                if(mapHoursFrequecy.containsKey(clObj.Hours__c) ){
                    Decimal Count  = mapHoursFrequecy.get(clObj.Hours__c) + 1;
                    mapHoursFrequecy.put(clObj.Hours__c,Count);
                } else {
                    mapHoursFrequecy.put(clObj.Hours__c,1 );
                }
            }
            system.debug('mapHoursFrequecy='+mapHoursFrequecy);
            if(!mapHoursFrequecy.values().isEmpty()){
                list<Decimal> lst =  mapHoursFrequecy.values();
                lst.sort();
                system.debug('mapHoursFrequecy sorted=' + lst );    
                Decimal maxVal = (lst.size()>0)? lst.get(lst.size()-1) : NULL;
                Decimal SecondmaxVal = (lst.size()>1)?lst.get(lst.size()-2) : NULL;
                system.debug('maxVal=' + maxVal);
                system.debug('maxVal=' + SecondmaxVal);
                Decimal maxValhour =0;
                Decimal SecondmaxValhour =0;
                for(Decimal hours :mapHoursFrequecy.keySet()){
                    if(mapHoursFrequecy.get(hours) == maxVal){
                        system.debug('In side if ' + hours);
                        maxValhour = hours;
                    }else if(mapHoursFrequecy.get(hours) == SecondmaxVal){
                        system.debug('In side  else if ' + hours);
                        SecondmaxValhour = hours;
                    }
                        
                }
                system.debug('maxValhour='+maxValhour);
                system.debug('SecondmaxValhour='+SecondmaxValhour);
                
                //8 hr, 20 ; 12 hr, 15
                String strToStamp = String.ValueOf(maxValhour)+' hr, '+String.ValueOf(maxVal);
                strToStamp = (String.isNotBlank(String.ValueOf(SecondmaxValhour)) && String.isNotBlank(String.ValueOf(SecondmaxVal)) ) ?
                    strToStamp +' ; '+String.ValueOf(SecondmaxValhour)+' hr, '+String.ValueOf(SecondmaxVal):strToStamp;
                    
                system.debug('strToStamp='+strToStamp);
                
                //accObj.Preferred_Time_To_Call__c = strToStamp;
                accObj.Best_Time_To_Call__c = String.isNotBlank(String.ValueOf(maxValhour))?String.ValueOf(maxValhour):'';
                accObj.Second_Best_Time_To_Call__c = (String.isNotBlank(String.ValueOf(SecondmaxValhour))&& SecondmaxValhour != 0)?String.ValueOf(SecondmaxValhour):'-';
                accToUpdate.add(accObj);
            }
            
        }
        
        
        
        if( accToUpdate != NULL && accToUpdate.size()>0){
            update accToUpdate;
        }
    }
    global void finish(Database.BatchableContext BC) {

    }
 }