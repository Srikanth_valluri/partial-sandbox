global class DAMAC_CancelMeetingLightning {
    
    @AuraEnabled
    public static String updateInquiryWithCancellationDetails(String inquiryId){
        
        String message = '';
        Inquiry__c inqToUpdate = new Inquiry__c() ;
        system.debug('inquiryId: ' + inquiryId);
        inqToUpdate.Id = inquiryId;
        inqToUpdate.Is_Meeting_Confirmed__c = false;
        inqToUpdate.Confirmation_Conditions__c = false;
        inqToUpdate.Confirmation_Mail_Sent__c = false;
        inqToUpdate.Schedule_Mail_Sent__c = false;
        inqToUpdate.Schedule_Conditions__c = false;
        inqToUpdate.Is_Meeting_scheduled__c = false;
        inqToUpdate.Sales_Office__c = null;
        inqToUpdate.Sales_Office_Location_Url__c = null;
        inqToUpdate.Meeting_Due_Date__c = null;
        try{
            update inqToUpdate;
            message = 'Meeting Cancelled Successfully';
        } catch (exception e){
            system.debug('Exception while updating Inquiry: ' + e);
            message = 'Error while updating: ' + e;
            return message;
        }
        return message;
    }
    
}