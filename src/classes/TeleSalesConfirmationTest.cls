/****************************************************************************************************
* Name          : TeleSalesConfirmation Test class                                                  *
* Description   :                                                                                   *
* Created Date  :                                                                                   *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE            COMMENTS                                                *
* 1.0   Monali Nagpure      13-05-2018      Initial Draft                                           *
* 1.1   Craig Lobo          26-08-2018      Updated the Coverage as per new Module                  *
****************************************************************************************************/

@isTest
public class TeleSalesConfirmationTest {
    @testSetup static void setup() {
        //Creaing test data
        User PCuser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        insert PCuser;

        User receptionalistuser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Receptionist'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            IsActive = true
        );
        insert receptionalistuser;
        
        User telesalesUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Telesales Team'].Id,
            LastName = 'telesalesUser last',
            Email = 'telesalesUser@amamama.com',
            Username = 'telesalesUser@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            IsActive = true,
            managerId = receptionalistuser.Id,
            TSA_TL__c = PCuser.Id
        );
        insert telesalesUser;
        
		Campaign__c c = new Campaign__c();
        c.Campaign_Name__c = 'Standalone';
        C.End_Date__c = system.today();
        C.Marketing_End_Date__c = system.today();
        C.Marketing_Start_Date__c = system.today();
        C.Start_Date__c = system.today();
        insert c;
        
        Id devRecordTypeId1 = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        Inquiry__c preInq = new Inquiry__c();
        preInq.Mobile_Phone_Encrypt__c = '05789088';
        preInq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        preInq.Preferred_Language__c = 'English';
        preInq.Inquiry_Status__c = 'New' ;
        preInq.First_Name__c = 'Test First name';
        preInq.Last_Name__c = 'Test Last name';
        preInq.Email__c = 'test@gmail.com';
        preInq.Inquiry_Source__c = 'Customer Referral';
        preInq.Mobile_Phone__c = '456789';
        preInq.RecordTypeId = devRecordTypeId1;
        preInq.Assigned_PC__c = PCuser.Id;
        preInq.Sales_Office_Location_Url__c  = 'www.google.com/Damac';
        preInq.Sales_Office__c = 'AKOYA';
        preInq.Meeting_Due_Date__c = system.now();
        preInq.OwnerId = telesalesUser .Id;
        preInq.Type_of_Property_Interested_in__c = 'Hotel';
        preInq.No_Of_Bed_Rooms_Availble_in_SF__c = '6';
        preInq.Country_of_Permanent_Residence__c = 'India';
        preInq.City_of_Permanent_Residence__c = 'Kochi';
        preInq.Interested_Location__c = 'ABU DHABI';
        preInq.Country__c = 'India';
        preInq.City_new__c = 'Kochi';
        preInq.Campaign__c = c.id;
        preInq.For_how_long__c = '7 days';
        preInq.Have_you_purchased_a_home_before__c = 'Yes';
        preInq.Project_Status__c = 'Ready To Move';
        preInq.Interested_Development__c = 'Abu Dhabi';
        preInq.Willing_to_meet_this_week__c = 'Yes';
        preInq.Accompanied_by__c = 'Spouse';
        preInq.Would_you_like_assistance_on_mortgage__c = 'Yes';
        preInq.Notes__c = 'Test Class';
        preInq.Completed__c = 'Yes';
        preInq.Budget__c = 'Below 500K';
        preInq.Project__c = 'DAMAC'; 
        preInq.Joint_Income_To_Be_Considered__c = 'Yes';
        System.runAs(telesalesUser) {
            insert preInq;
        }
    }
    
    @isTest static void TestConfirmationEmail() {
        Sales_Office_Locations__c objLocations = new Sales_Office_Locations__c(
            Name='AKOYA',Location_URL__c='www.google.com');
        insert objLocations;
        Inquiry__c inq = [SELECT Id, 
                                 Inquiry_Status__c,
                                 RecordType.Name,
                                 TSA_TL_of_Owner__c,
                                 First_Name__c,
                                 Last_Name__c,
                                 Email__c,
                                 Meeting_Due_Date__c,
                                 Sales_Office__c,
                                 Mobile_Phone__c,
                                 Sales_Office_Location__c
                            FROM Inquiry__c
                           WHERE RecordType.Name = 'Pre Inquiry'
                           LIMIT 1
        ];
        system.debug('====Test class===inq=======' +inq);
        List<Task> lstTasks = new List<Task>();
        lstTasks.add(new Task(WhatId=inq.Id,
            ActivityDate = System.today().addDays(2),
            Status = 'Not Started',
            Type = 'Meeting at Office',
            Activity_Type_3__c = 'Meeting at Office',
            Activity_Outcome__c = 'Show - Further Meeting Scheduled',
            Subject = 'TSA Task',
            is_Owner_changed__c = false,
            Is_meeting_scheduled__c = true
        ));
        
        Test.startTest();
        TeleSalesConfirmation.SendTeleSalesConfirmationEmail(new Set<ID> {inq.id});
        insert lstTasks;
        Test.stopTest();
    }

    @isTest static void TestScheduledEmail() {
        Sales_Office_Locations__c objLocations = new Sales_Office_Locations__c(
            Name='AKOYA',Location_URL__c='www.google.com');
        insert objLocations;
        Inquiry__c inq = [SELECT Id, 
                                 Inquiry_Status__c,
                                 RecordType.Name,
                                 TSA_TL_of_Owner__c,
                                 First_Name__c,
                                 Last_Name__c,
                                 Email__c,
                                 Meeting_Due_Date__c,
                                 Sales_Office__c,
                                 Mobile_Phone__c,
                                 Sales_Office_Location__c
                            FROM Inquiry__c
                           WHERE RecordType.Name = 'Pre Inquiry'
                           LIMIT 1
                             ];
        system.debug('====Test class===inq=======' +inq);
        List<Task> lstTasks = new List<Task>();
        lstTasks.add(new Task(WhatId=inq.Id,
            ActivityDate = System.today().addDays(2),
            Status = 'Not Started',
            Type = 'Meeting at Office',
            Activity_Type_3__c = 'Meeting at Office',
            Activity_Outcome__c = 'Show - Further Meeting Scheduled',
            Subject = 'TSA Task',
            is_Owner_changed__c = true,
            Is_meeting_scheduled__c = true,
            
            Is_meeting_confirmed_reminder_sent__c = false
        ));

        Test.startTest();

        insert lstTasks;
        TeleSalesConfirmation.SendTeleSalesConfirmationEmail(new Set<ID> {inq.id});
        
        Inquiry__c inqInst = new Inquiry__c(Id = inq.Id);
        inqInst.Mobile_Phone__c = '21212323234';
        inqInst.Inquiry_Status__c = 'Meeting Scheduled';
        update inqInst;
        
        
        Test.stopTest();
    }
}