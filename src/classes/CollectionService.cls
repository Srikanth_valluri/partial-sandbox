/********************************************************************************************************************************
 * Description : Class to make an api callout to get unit details 
 *===============================================================================================================================
 * Ver      Date-DD/MM/YYYY     Author              Modification
 *===============================================================================================================================
 * 1.0      07/06/2020          Aishwarya Todkar    Initial Draft
*********************************************************************************************************************************/
public Class CollectionService {
    

/********************************************************************************************************************************
* Method Name : getBearerToken
* Description : method to get bearer token
* Return Type : String
* Parameter(s): None
********************************************************************************************************************************/
    public static String getBearerToken() {
        Credentials_details__c creds = Credentials_details__c.getInstance('Collection Service Token');
        if( creds != null && String.isNotBlank( creds.Endpoint__c ) && String.isNotBlank( creds.Password__c ) ) { 

            HttpRequest req = new HttpRequest();
            req.setEndpoint( creds.Endpoint__c +  creds.Password__c ); // endpoint + client id
            req.setMethod('POST');
            req.setTimeout(120000);
            HttpResponse response = new Http().send(req);

            System.debug(' response: ' + response.getBody());
            Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            return String.valueOf(m.get('token'));
        } 
        return null;
    }
    
/********************************************************************************************************************************
* Method Name : getUnitsDetails
* Description : method to get unit details
* Return Type : CollectionResponse
* Parameter(s): String partyId
********************************************************************************************************************************/

    public static CollectionResponse getUnitsDetails( String partyId ) {
        System.debug( 'partyId--'+ partyId);
        if( String.isNotBlank( partyId )) {
            Credentials_details__c creds = Credentials_details__c.getInstance('Collection Service');
            if( creds != null && String.isNotBlank( creds.Endpoint__c) ) { 
            
                String accessToken = getBearerToken();
                if( String.isNotBlank( accessToken ) ) {
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint( creds.Endpoint__c + partyId );
                    req.setMethod('GET');
                    req.setHeader('Accept', 'application/json');
                    req.setHeader('Authorization','Bearer' + accessToken );
                    req.setTimeout(120000);
                    System.debug('req : '+req);
                    HttpResponse response = new Http().send(req);
                    system.debug('response--'+response);
                    system.debug('response--'+ response.getBody());

                    if( response.getStatusCode() == 200 && response.getBody() != null ) {
                        /*Map<String, Object> parsedMap = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                        List<cls_unitLines> collRes = (List<cls_unitLines>) System.JSON.deserialize( Json.serialize( parsedMap.get('unitLines') ), List<cls_unitLines>.class);
                        
                        //String temp = '{"partyId":760002,"processStatus":"S","processMessage":"Total Unit Lines = 106",
                        "processingTimeInMs":19,"bouncedChequeValue":1.591102E7,"dataAsOf":"02-Jun-2020 11:51:48",
                        "unitLines":[{"registrationId":89098,"unitNumber":"DTPC/55/5505","reservationPrice":1385100.0,
                        "amountPaid":1108080.0,"amountPaidDld":1108080.0,"invoicedRaised":1398951.0,"amountPaidOnInstallment":
                        1108080.0,"balanceToPay":277020.0,"soaBalance":390773.77,"totalDueAmount":290871.0,"totalOverDueAmount":290871
                        .0,"overDue180To365Days":290871.0,"penaltiesCharged":27398.0,"penaltiesWaived":0.0,
                        "lastPaidDate":"2015-11-24T20:00:00.000+00:00","areaVariation":14.2,"areaVariationCharged":0.0,
                        "areaVariationWaived":0.0,"liborAmount":0.0,"orgId":81,"unitId":50892,"amountCredited":0.0},
                        {"registrationId":29898,"unitNumber":"DTPC/55/5501","reservationPrice":1344600.0,"amountPaid":1075680.0,
                        "amountPaidDld":1075680.0,"invoicedRaised":1358046.0,"amountPaidOnInstallment":1075680.0,"balanceToPay":1344600.0,"soaBalance":380088.77,"totalDueAmount":282366.0,"totalOverDueAmount":282366.0,"overDue180To365Days":282366.0,"penaltiesCharged":26588.0,"penaltiesWaived":0.0,"lastPaidDate":"2015-11-24T20:00:00.000+00:00","areaVariation":19.11,"areaVariationCharged":0.0,"areaVariationWaived":0.0,"liborAmount":0.0,"orgId":81,"unitId":50888,"amountCredited":0.0},{"registrationId":29899,"unitNumber":"DTPC/55/5502","reservationPrice":1819800.0,"amountPaid":1455840.0,"amountPaidDld":1455840.0,"invoicedRaised":1837998.0,"amountPaidOnInstallment":1464855.3,"balanceToPay":363960.0,"soaBalance":498846.47,"totalDueAmount":373142.7,"totalOverDueAmount":373142.7,"overDue180To365Days":373142.7,"penaltiesCharged":35811.0,"penaltiesWaived":0.0,"lastPaidDate":"2015-11-24T20:00:00.000+00:00","areaVariation":36.88,"areaVariationCharged":-8586.0,"areaVariationWaived":0.0,"liborAmount":0.0,"orgId":81,"unitId":50889,"amountCredited":0.0},{"registrationId":74123,"unitNumber":"DTPC/55/5503","reservationPrice":2921400.0,"amountPaid":2337120.0,"amountPaidDld":2337120.0,"invoicedRaised":2950614.0,"amountPaidOnInstallment":2337120.0,"balanceToPay":2921400.0,"soaBalance":805518.77,"totalDueAmount":613494.0,"totalOverDueAmount":613494.0,"overDue180To365Days":613494.0,"penaltiesCharged":58068.0,"penaltiesWaived":0.0,"lastPaidDate":"2015-11-24T20:00:00.000+00:00","areaVariation":23.48,"areaVariationCharged":0.0,"areaVariationWaived":0.0,"liborAmount":0.0,"orgId":81,"unitId":50890,"amountCredited":0.0},{"registrationId":2512,"unitNumber":"DTPC/55/5504","reservationPrice":1404000.0,"amountPaid":1123200.0,"amountPaidDld":1123200.0,"invoicedRaised":1418040.0,"amountPaidOnInstallment":1123200.0,"balanceToPay":280800.0,"soaBalance":395875.77,"totalDueAmount":294840.0,"totalOverDueAmount":294840.0,"overDue180To365Days":294840.0,"penaltiesCharged":27775.0,"penaltiesWaived":0.0,"lastPaidDate":"2015-11-24T20:00:00.000+00:00","areaVariation":-1.19,"areaVariationCharged":0.0,"areaVariationWaived":0.0,"liborAmount":0.0,"orgId":81,"unitId":50891,"amountCredited":0.0},{"registrationId":39210,"unitNumber":"DTPC/55/5506","reservationPrice":1341900.0,"amountPaid":1073520.0,"amountPaidDld":1073520.0,"invoicedRaised":1355319.0,"amountPaidOnInstallment":1073520.0,"balanceToPay":1341900.0,"soaBalance":379110.77,"totalDueAmount":281799.0,"totalOverDueAmount":281799.0,"overDue180To365Days":281799.0,"penaltiesCharged":26535.0,"penaltiesWaived":0.0,"lastPaidDate":"2015-11-24T20:00:00.000+00:00","areaVariation":20.7,"areaVariationCharged":0.0,"areaVariationWaived":0.0,"liborAmount":0.0,"orgId":81,"unitId":50893,"amountCredited":0.0},{"registrationId":39211,"unitNumber":"DTPC/55/5507","reservationPrice":1328400.0,"amountPaid":1062720.0,"amountPaidDld":1062720.0,"invoicedRaised":1341684.0,"amountPaidOnInstallment":1062720.0,"balanceToPay":1328400.0,"soaBalance":375466.77,"totalDueAmount":278964.0,"totalOverDueAmount":278964.0,"overDue180To365Days":278964.0,"penaltiesCharged":26266.0,"penaltiesWaived":0.0,"lastPaidDate":"2015-11-24T20:00:00.000+00:00","areaVariation":18.93,"areaVariationCharged":0.0,"areaVariationWaived":0.0,"liborAmount":0.0,"orgId":81,"unitId":50894,"amountCredited":0.0},{"registrationId":88400,"unitNumber":"DTPC/55/5512","reservationPrice":1736100.0,"amountPaid":1388880.0,"amountPaidDld":1388880.0,"invoicedRaised":1753461.0,"amountPaidOnInstallment":1388880.0,"balanceToPay":347220.0,"soaBalance":485530.77,"totalDueAmount":364581.0,"totalOverDueAmount":364581.0,"overDue180To365Days":364581.0,"penaltiesCharged":34405.0,"penaltiesWaived":0.0,"lastPaidDate":"2015-11-24T20:00:00.000+00:00","areaVariation":22.68,"areaVariationCharged":0.0,"areaVariationWaived":0.0,"liborAmount":0.0,"orgId":81,"unitId":50899,"amountCredited":0.0},{"registrationId":39213,"unitNumber":"DTPC/55/5511","reservationPrice":1347300.0,"amountPaid":1077840.0,"amountPaidDld":1077840.0,"invoicedRaised":1360773.0,"amountPaidOnInstallment":1077840.0,"balanceToPay":1347300.0,"soaBalance":380567.77,"totalDueAmount":282933.0,"totalOverDueAmount":282933.0,"overDue180To365Days":282933.0,"penaltiesCharged":26642.0,"penaltiesWaived":0.0,"lastPaidDate":"2015-11-24T20:00:00.000+00:00","areaVariation":10.96,"areaVariationCharged":0.0,"areaVariationWaived":0.0,"liborAmount":0.0,"orgId":81,"unitId":50898,"amountCredited":0.0},{"registrationId":2513,"unitNumber":"DTPC/55/5510","reservationPrice":1350000.0,"amountPaid":1080000.0,"amountPaidDld":1080000.0,"invoicedRaised":1363500.0,"amountPaidOnInstallment":1080000.0,"balanceToPay":270000.0,"soaBalance":381297.77,"totalDueAmount":283500.0,"totalOverDueAmount":283500.0,"overDue180To365Days":283500.0,"penaltiesCharged":26697.0,"penaltiesWaived":0.0,"lastPaidDate":"2015-11-24T20:00:00.000+00:00","areaVariation":18.75,"areaVariationCharged":0.0,"areaVariationWaived":0.0,"liborAmount":0.0,"orgId":81,"unitId":50897,"amountCredited":0.0},{"registrationId":3952,"unitNumber":"DTPC/55/5509","reservationPrice":1350000.0,"amountPaid":1080000.0,"amountPaidDld":1080000.0,"invoicedRaised":1363500.0,"amountPaidOnInstallment":1080000.0,"balanceToPay":1350000.0,"soaBalance":381297.77,"totalDueAmount":283500.0,"totalOverDueAmount":283500.0,"overDue180To365Days":283500.0,"penaltiesCharged":26697.0,"penaltiesWaived":0.0,"lastPaidDate":"2015-11-24T20:00:00.000+00:00","areaVariation":17.99,"areaVariationCharged":0.0,"areaVariationWaived":0.0,"liborAmount":0.0,"orgId":81,"unitId":50896,"amountCredited":0.0},{"registrationId":3953,"unitNumber":"DTPC/55/5508","reservationPrice":1344600.0,"amountPaid":1075680.0,"amountPaidDld":1075680.0,"invoicedRaised":1358046.0,"amountPaidOnInstallment":1075680.0,"balanceToPay":268920.0,"soaBalance":379838.77,"totalDueAmount":282366.0,"totalOverDueAmount":282366.0,"overDue180To365Days":282366.0,"penaltiesCharged":26588.0,"penaltiesWaived":0.0,"lastPaidDate":"2015-11-24T20:00:00.000+00:00","areaVariation":20.51,"areaVariationCharged":0.0,"areaVariationWaived":0.0,"liborAmount":0.0,"orgId":81,"unitId":50895,"amountCredited":0.0},{"registrationId":3954,"unitNumber":"DTPC/55/5513","reservationPrice":1377000.0,"amountPaid":1101600.0,"amountPaidDld":1101600.0,"invoicedRaised":1390770.0,"amountPaidOnInstallment":1101656.7,"balanceToPay":1377000.0,"soaBalance":388528.07,"totalDueAmount":289113.3,"totalOverDueAmount":289113.3,"overDue180To365Days":289113.3,"penaltiesCharged":27234.0,"penaltiesWaived":0.0,"lastPaidDate":"2015-11-24T20:00:00.000+00:00","areaVariation":25.52,"areaVariationCharged":-54.0,"areaVariationWaived":0.0,"liborAmount":0.0,"orgId":81,"unitId":50900,"amountCredited":0.0},{"registrationId":3955,"unitNumber":"DTPC/55/5515","reservationPrice":1255500.0,"amountPaid":1004400.0,"amountPaidDld":1004400.0,"invoicedRaised":1268055.0,"amountPaidOnInstallment":1004400.0,"balanceToPay":1255500.0,"soaBalance":363501.77,"totalDueAmount":263655.0,"totalOverDueAmount":263655.0,"overDue180To365Days":263655.0,"penaltiesCharged":32526.0,"penaltiesWaived":0.0,"lastPaidDate":"2015-11-24T20:00:00.000+00:00","areaVariation":13.02,"areaVariationCharged":0.0,"areaVariationWaived":0.0,"liborAmount":0.0,"orgId":81,"unitId":50902,"amountCredited":0.0}]}';*/
                        CollectionResponse collRes = (CollectionResponse)JSON.deserialize( response.getBody() ,CollectionResponse.class );
                        System.debug('deserialized response -- ' + collRes );
                        return collRes;
                    }
                }
            }
        }
        return null;
    }
}