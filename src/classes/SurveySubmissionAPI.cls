/**********************************************************************************************************************
Description: This API is used for creating app survey response record
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    	| Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   24-11-2020		| Anand Venkitakrishnan | Created initial draft
1.1		|	09-12-2020		| Anand Venkitakrishnan | Added handling for unsatisfied survey response
***********************************************************************************************************************/

@RestResource(urlMapping='/SubmitSurvey/*')
global class SurveySubmissionAPI {
    public static String errorMsg;
    public static Integer statusCode;
	public static final List<String> sNamesList = new List<String>{'transaction','amenity','sr','profile'};
    public static final String TRANSACTION_TYPE = 'transaction';
    public static final String AMENITY_TYPE = 'amenity';
    public static final String SR_TYPE = 'sr';
    public static final String PROFILE_TYPE = 'profile';
    public static final String TRANSACTION_SN = 'App Payment Transaction Survey';
    public static final String AMENITY_SN = 'App Amenity Booking Survey';
    public static final String SR_SN = 'App SR Completion Survey';
    public static final String PROFILE_SN = 'App Profile Survey';
    
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong',
        7 => 'Thank you for your feedback'
    };
    
    @HttpPost
    global static FinalReturnWrapper submitSurveyResponse() {
        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString:'+jsonString);
        System.debug('Request params:'+req.params);
        
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        ResponseWrapper objResWrap = new ResponseWrapper(); 
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();
        
        if(String.isBlank(jsonString)) {
            objMeta = ReturnMetaResponse('No JSON body found in request', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        System.debug('jsonString:'+jsonString);
        objResWrap = (ResponseWrapper)JSON.deserialize(jsonString,ResponseWrapper.class);
        System.debug('objResWrap:'+objResWrap);
        System.debug('response_details:'+objResWrap.response_details);
        
        if(objResWrap.account_id == NULL) {
            objMeta = ReturnMetaResponse('Missing parameter : accountId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(String.isBlank(objResWrap.account_id)) {
            objMeta = ReturnMetaResponse('Missing parameter value : accountId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(objResWrap.survey_type == NULL) {
            objMeta = ReturnMetaResponse('Missing parameter : surveyType', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(String.isBlank(objResWrap.survey_type)) {
            objMeta = ReturnMetaResponse('Missing parameter value : surveyType', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(!sNamesList.contains(objResWrap.survey_type)) {
            objMeta = ReturnMetaResponse('Invalid surveyType', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if((objResWrap.survey_type.equals(AMENITY_TYPE) || objResWrap.survey_type.equals(SR_TYPE)) && objResWrap.fm_case_id == NULL) {
            objMeta = ReturnMetaResponse('Missing parameter : fmCaseId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if((objResWrap.survey_type.equals(AMENITY_TYPE) || objResWrap.survey_type.equals(SR_TYPE)) && String.isBlank(objResWrap.fm_case_id)) {
            objMeta = ReturnMetaResponse('Missing parameter value : fmCaseId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(objResWrap.response_details.isEmpty() || objResWrap.response_details.size() == 0) {
            objMeta = ReturnMetaResponse('Missing responses', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        String userId;
        Boolean isTenant;
        
        if(String.isNotBlank(objResWrap.account_id)) {
            List<User> usr = [SELECT Id,Name,Contact.AccountId,Profile.Name 
                              FROM User
                              WHERE Contact.AccountId =: objResWrap.account_id];
            System.debug('usr:'+usr);
            if(!usr.isEmpty()) {
                userId = usr[0].Id;
                isTenant = usr[0].Profile.Name.contains('Tenant Community') ? true : false;
        		System.debug('isTenant:'+isTenant);
            }
            else {
                objMeta = ReturnMetaResponse('No valid user for the accountId', 3);
                returnResponse.meta_data = objMeta;
                return returnResponse;
            }
        }
        else {
            objMeta = ReturnMetaResponse('No account_id passed in request body', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        String surveyType = objResWrap.survey_type;
        String surveyName;
        String surveyId;
        Boolean isSubmitted = false;
        
        try {
            String accountId = objResWrap.account_id;
            String accountName;
            
            List<Account> acc = [SELECT Id,Name  
                              	 FROM Account
                              	 WHERE Id =: accountId];
            System.debug('acc:'+acc);
            if(!acc.isEmpty()) {
                accountName = acc[0].Name;
            }
            
            //Creating the main survey response record linked to the user
            Survey_Taken_CRM__c surveyTakenRecord = new Survey_Taken_CRM__c();
            surveyTakenRecord.Account__c = objResWrap.account_id;
            surveyTakenRecord.Customer_taking_Survey__c = objResWrap.account_id;
            surveyTakenRecord.Customer_Name__c = accountName;
            surveyTakenRecord.User_taking_Survey__c = userId;
            
            if(isTenant) {
                surveyTakenRecord.Type_Of_Customer__c = 'Tenant';
            }
            else {
                surveyTakenRecord.Type_Of_Customer__c = 'Owner';
            }
            
            //Link booking unit to survey record if the data is available
            if(objResWrap.booking_unit_id != NULL && !String.isBlank(objResWrap.booking_unit_id)) {
                List<Booking_Unit__c> unit = [SELECT Id,Unit_Name__c,Booking__r.Account__c 
                                              FROM Booking_Unit__c
                                              WHERE Id =: objResWrap.booking_unit_id
                                              LIMIT 1];
                
                if(!unit.isEmpty() && unit.size() > 0) {
                    surveyTakenRecord.Booking_Unit__c = unit[0].Id;
                }
            }
            else {
                List<Booking_Unit__c> unit = [SELECT Id,Unit_Name__c,Booking__r.Account__c 
                                              FROM Booking_Unit__c
                                              WHERE Booking__r.Account__c =: objResWrap.account_id OR Resident__c =: objResWrap.account_id OR Tenant__c =: objResWrap.account_id
                                              LIMIT 1];
                
                if(!unit.isEmpty() && unit.size() > 0) {
                    surveyTakenRecord.Booking_Unit__c = unit[0].Id;
                }
            }
            
            if(surveyType.equals(TRANSACTION_TYPE)) {
                surveyName = TRANSACTION_SN;
            }
            else if(surveyType.equals(AMENITY_TYPE)) {
                surveyName = AMENITY_SN;
            }
            else if(surveyType.equals(SR_TYPE)) {
                surveyName = SR_SN;
            }
            else if(surveyType.equals(PROFILE_TYPE)) {
                surveyName = PROFILE_SN;
            }
            
            //Fetch the survey record
            List<Survey_CRM__c> sList = [SELECT Id,Name 
                                         FROM Survey_CRM__c 
                                         WHERE Name =: surveyName];
            
            if(!sList.isEmpty()) {
                surveyId = sList[0].Id;
                surveyTakenRecord.Survey__c = surveyId;
            }
            else {
                objMeta = ReturnMetaResponse('No survey found', 2);
                returnResponse.meta_data = objMeta;
                return returnResponse;
            }
            
            if(surveyType.equals(SR_TYPE)) {
                //Check if SR survey is already submitted
                List<FM_Case__c> fmCaseData = [SELECT Id,Name,QRSurvey__c 
                                               FROM FM_Case__c 
                                               WHERE Id =: objResWrap.fm_case_id];
                
                if(!fmCaseData.isEmpty()) {
                    FM_Case__c fmCase = fmCaseData[0];
                    
                    if(fmCase.QRSurvey__c != NULL) {
                        objMeta = ReturnMetaResponse('We appreciate your feedback! We have however recorded your earlier submitted response.', 7);
                        returnResponse.meta_data = objMeta;
                        return returnResponse;
                    }
                }
            }
            
            Map<Integer,Id> sqMap = new Map<Integer,Id>();
            Integer sqCount = 1;
            
            //Fetch the survey questions
            for(Survey_Question_CRM__c sq : [SELECT Id,Name FROM Survey_Question_CRM__c WHERE Survey__c =: surveyId ORDER BY OrderNumber__c]) {
                sqMap.put(sqCount,sq.Id);
                sqCount++;
            }
            
            //1.1
            Integer srSurveyRating = 0,mandatoryRating;
            String srSurveyComment,mandatoryComment;
            
            for(cls_response_details resDet : objResWrap.response_details) {
                if(sqMap.containsKey(Integer.valueOf(resDet.question_id))) {
                    if(Integer.valueOf(resDet.question_id) == 1) {
                        if(surveyType.equals(SR_TYPE)) {
                            srSurveyRating = resDet.rating;
                        	srSurveyComment = resDet.comments;
                        }
                        
                        if(!surveyType.equals(PROFILE_TYPE)) {
                            mandatoryRating = resDet.rating;
                        	mandatoryComment = resDet.comments;
                        }
                    }
                    else if(surveyType.equals(PROFILE_TYPE) && Integer.valueOf(resDet.question_id) == 2) {
                        mandatoryRating = resDet.rating;
                        mandatoryComment = resDet.comments;
                    }
                }
            }
            
            System.debug('mandatoryRating:'+mandatoryRating);
            System.debug('mandatoryComment:'+mandatoryComment);
            if((mandatoryRating == 1 || mandatoryRating == 2 || mandatoryRating == 3) && !String.isEmpty(mandatoryComment)) {
                surveyTakenRecord.Reason_of_Unsatisfaction__c = mandatoryComment;
                surveyTakenRecord.survey_Outcome__c = 'Unsatisfied';
            }
            else if(mandatoryRating == 4) {
                surveyTakenRecord.survey_Outcome__c = 'Satisfied';
            }
            else if(mandatoryRating > 4) {
                surveyTakenRecord.survey_Outcome__c = 'Happy';
            }
            
            insert surveyTakenRecord;
            
            List<Survey_Question_Response_CRM__c> sqrList = new List<Survey_Question_Response_CRM__c>();
            
            //Collect responses from the request body
            for(cls_response_details resDet : objResWrap.response_details) {
                if(sqMap.containsKey(Integer.valueOf(resDet.question_id))) {
                    Survey_Question_Response_CRM__c sqr = new Survey_Question_Response_CRM__c();
                    
                    sqr.Survey_Taker__c = surveyTakenRecord.Id;
                    sqr.Survey_Question__c = sqMap.get(Integer.valueOf(resDet.question_id));
                    sqr.Response__c = String.valueOf(resDet.rating);
                    
                    if(resDet.comments != NULL && !String.isBlank(resDet.comments)) {
                        sqr.Additional_Response__c = resDet.comments;
                    }
                    
                    sqrList.add(sqr);
                }
            }
            
            //Insert the question response records
            if(sqrList.size() > 0) {
                System.debug('sqrList:'+sqrList);
                insert sqrList;
                
                isSubmitted = true;
            }
            
            if(surveyType.equals(AMENITY_TYPE) || surveyType.equals(SR_TYPE)) {
                //Update SR with the main survey response record
                List<FM_Case__c> fmCaseList = [SELECT Id,Name,QRSurvey__c 
                                               FROM FM_Case__c
                                               WHERE Id =: objResWrap.fm_case_id];
                
                if(!fmCaseList.isEmpty()) {
                    FM_Case__c srRecord = fmCaseList[0];
                    srRecord.QRSurvey__c = surveyTakenRecord.Id;
                    
                    if (srSurveyRating == 5){
                        srRecord.Customer_Experience__c = 'Happy';
                    }
                    else if (srSurveyRating == 4){
                        srRecord.Customer_Experience__c = 'Satisfied';
                    }
                    else if(srSurveyRating > 0) {
                        srRecord.Customer_Experience__c = 'Not Satisfied';
                    }
                    
                    if(!String.isEmpty(srSurveyComment)) {
                        srRecord.Move_In_experience__c = srSurveyComment;
                    }
                    
                    update srRecord;
                }
            }
        }
        catch(Exception e) {
            System.debug('Exception:'+e.getMessage());
        }
        
        objData.is_submitted = isSubmitted;
        
        if(isSubmitted) {
            errorMsg = 'Successful';
            statusCode = 1;
        }
        else {
            errorMsg = 'Unable to submit';
            statusCode = 2;
        }
        
        System.debug('errorMsg:'+errorMsg);
        System.debug('statusCode:'+statusCode);
        objMeta = ReturnMetaResponse(errorMsg, statusCode);
        
        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;
        System.debug('returnResponse:'+returnResponse);
        
        return returnResponse;
    }
    
    public class ResponseWrapper {
        public String booking_unit_id;
        public String account_id;
        public String survey_type;
        public String fm_case_id;
        
        public cls_response_details[] response_details;
    }
    
    public class cls_response_details {
        public Integer question_id;
        public Integer rating;
        public String comments;
    }
    
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }
    
    public static cls_meta_data ReturnMetaResponse(String message, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data();
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = null;
        return retMeta;
    }
	
    public class cls_data {
        public Boolean is_submitted;
    }
    
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }
}