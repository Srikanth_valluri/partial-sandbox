/**********************************************************************************************************************
Description: Scheduler for the batch(DL_ProfileCompletionNotificationBatch) for profile completion notification.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By   | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     | 03-11-2020        | Subin Antony (QBurst)   | Initial Draft
***********************************************************************************************************************/
global class DL_ProfileCompletionNotificationSched implements Schedulable {
    global void execute(SchedulableContext sc) {
        Id batchinstanceid = database.executeBatch(new DL_ProfileCompletionNotificationBatch(),50);
    }
}