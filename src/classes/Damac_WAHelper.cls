Global class Damac_WAHelper {

    public List <Nexmo_Whats_App_Request__c> waReqs { get; set; }
    public List <Nexmo_Whats_App_Message__c> conversations { get; set; }
    public Inquiry__c inquiryDetails { get; set; }
    public String waReqId { get; set; }
    public String message { get; set; }
    
    public void init () {
        waReqs = new List <Nexmo_Whats_App_Request__c> ();
        waReqs = [SELECT Name, Record_Name__c, Record_Id__c, Last_Message_Sent__c, Read_At__c, Customer_Name__c, Last_Message__c, Latest_Inbound_Message_Received_on__c,
                            Inquiry__c, Inquiry__r.Name, Inquiry__r.Full_Name__c FROM Nexmo_Whats_App_Request__c
                            WHERE (Inquiry__c != NULL OR Record_Name__c LIKE : 'IQ%')
                            AND Inquiry__r.ownerId=:UserInfo.getUserID()
                             Order By Latest_Inbound_Message_Received_on__c DESC LIMIT 100];
    }
    @remoteAction
    public static List<Inquiry__c> searchInquiry (String pSearchKey) {
        String searchString = '%' +pSearchKey +'%';
        ID userId = UserInfo.getUserID();
        User userCheck = new User();
        userCheck = [SELECT FederationIdentifier FROM User WHERE ID =: UserInfo.getUserId()];
        if (userCheck.FederationIdentifier == 'venkatasubhash.k') {
            return [SELECT Full_Name__c, Name, Pre_Inquiry_Number__c FROM Inquiry__c
                    WHERE NAME != NULL
                    AND (First_Name__c LIKE: searchString OR Last_Name__c LIKE: searchString OR Name LIKE: searchString )
                    ORDER By Name DESC
                    LIMIT 10
                   ];
        } else {
            return [SELECT Full_Name__c, Name, Pre_Inquiry_Number__c FROM Inquiry__c
                    WHERE NAME != NULL
                    AND ownerId =:userId
                    AND (First_Name__c LIKE: searchString OR Last_Name__c LIKE: searchString OR Name LIKE: searchString )
                    ORDER By Name DESC
                    LIMIT 10
                   ];
        }
    }
    
    public void assignInquiryId () {
        waReqId = apexpages.currentpage().getparameters().get('inqid');
    }
    
    public void loadMessages () {
        conversations = new List <Nexmo_Whats_App_Message__c> ();
        System.debug (waReqId);
        waReqId = apexpages.currentpage().getParameters().get('waRecId');
        for (Nexmo_Whats_App_Message__c msg :[SELECT Nexmo_Whats_App_Request__r.Inquiry__r.Name, CreatedDate, 
                                                Nexmo_Whats_App_Request__c, Nexmo_Message_Body__c, Direction__c,
                                                Message_Read__c,Message_Submitted__c, Message_Delivered__c, Message_Failed__c,
                                                Nexmo_Whats_App_Request__r.Inquiry__c
                                                FROM Nexmo_Whats_App_Message__c
                                                WHERE Nexmo_Whats_App_Request__c =: waReqId 
                                                AND nexmo_Whats_App_Request__c != null
                                              order by CreatedDate ASC]) 
        {
            msg.message_body_url__c = msg.CreatedDate.format();
            conversations.add (msg);
        }
        inquiryDetails = new Inquiry__c ();
        if (!conversations.isEmpty ())
            inquiryDetails = [SELECT Name FROM Inquiry__c WHERE Id =: conversations[0].Nexmo_Whats_App_Request__r.Inquiry__c];
        else
            message = 'There is no conversations. Please initiate.';
    }
    
    @RemoteAction
    global static void updateReadMsg (ID waReqId) {
        Nexmo_Whats_App_Request__c req = new Nexmo_Whats_App_Request__c();
        req.id = waReqId;
        req.Last_Read_by_Agent_At__c = System.Now();
        update req;
    }
}