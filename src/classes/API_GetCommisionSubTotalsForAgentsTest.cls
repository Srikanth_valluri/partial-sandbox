@isTest
public class API_GetCommisionSubTotalsForAgentsTest {
    private static Contact adminContact;
    private static User portalUser;
    // private static User portalOnlyAgent;
    private static Account adminAccount;
    private static Contact agentContact;
    private static NSIBPM__Service_Request__c srRecord;
    private static Booking__c bookingRecord;
    private static String BU_ID;
    private static String AgentCommisionID;
    
    private static void testData_setup() {
        Id adminRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = 'Chairman' LIMIT 1].Id;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't156', email='x1234@email.com',
                emailencodingkey='UTF-8', lastname='User 56', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='x1234@email.com',UserRoleId = adminRoleId);
        
        System.RunAs(adminUser) {
            adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;
            
            adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            insert adminContact;
            
            agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            
            portalUser = InitialiseTestData.getPortalUser('test45@test.com', adminContact.Id, 'Admin');
            // portalOnlyAgent = InitialiseTestData.getPortalUser('test54@test.com', agentContact.Id, 'Agent');
            
            srRecord = 
                InitialiseTestData.createBookingServiceRequest(true, true, adminContact.AccountId , System.today());

            bookingRecord = 
                InitialiseTestData.createBookingRecords(adminContact.AccountId, srRecord, 30000000,'Pending');

            Booking_Unit__c bookingUnitObject = new Booking_Unit__c();
            bookingUnitObject.Booking__c = bookingRecord.Id;
            //bookingUnitObject.Inventory__c = bookingRecord.Inventory__c;
            bookingUnitObject.Unit_Selling_Price__c = 15000000.00;
            bookingUnitObject.Requested_Price__c = 10000000.00;
            bookingUnitObject.Requested_Token_Amount__c = 40000.00;
            bookingUnitObject.Payment_Method__c = 'Online_Payment';
            bookingUnitObject.Online_Payment_Party__c = 'Third Party';
            bookingUnitObject.No_of_parking__c = 1.00;
            bookingUnitObject.Primary_Buyer_s_Email__c = 'test@test.com';
            bookingUnitObject.Primary_Buyer_s_Name__c = 'Damac Test';
            bookingUnitObject.Primary_Buyer_Country__c = 'United Arab Emirates';
            bookingUnitObject.Primary_Buyer_s_Nationality__c ='Emirati';
            bookingUnitObject.Unique_Key__c = 'BOOKING_UNIT ' + String.valueOf(System.now().millisecond());
            bookingUnitObject.Registration_ID__c = '1234';
            bookingUnitObject.Registration_DateTime__c = System.NOW();
            insert bookingUnitObject;
        	BU_ID = bookingUnitObject.id;
        
            Agent_Commission__c ac1 = new Agent_Commission__c();
            ac1.Agency__c = adminAccount.id;
            ac1.Booking__c = bookingRecord.id;
            ac1.Booking_Unit__c = BU_ID;
            ac1.Amount__c = 1200.00;
            ac1.DP_Amount__c = 40000.00;
            ac1.DP_Amount_Received__c = 40000.00;
            insert ac1;
            AgentCommisionID = ac1.id;
        }
    }
    
    private static void testData_setup_alt() {
        Account acc = new Account ();
        acc.Name = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '969696';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        insert acc;
        
        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c  = 1;
        newProperty.Property_Code__c    = 'VIR' ;
        newProperty.Property_Name__c    = 'VIRIDIS @ AKOYA OXYGEN' ;
        newProperty.District__c = 'AL YUFRAH 2' ;
        newProperty.AR_Transaction_Type__c  = 'INV VIR' ;
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR' ;
        newProperty.Brokerage_Distribution_Set__c   = '11600' ;
        newProperty.Sales_Commission_Dist_Set__c    = '11601' ;
        newProperty.Currency_Of_Sale__c = 'AED' ;
        newProperty.Signature_Col_Customer_Stmt__c  = 'Front Line Investment Management Co. LLC' ;
        newProperty.EOI_Enabled__c = true;
        insert newProperty;
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        loc.Property_ID__c = '123';
        insert loc;   
        
        Inventory__c invent = new Inventory__c();
        invent.CM_Price_Per_Sqft__c = 20;
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1' ;
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.ID;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Released';
        invent.special_price__c = 1000000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234'; 
        invent.Floor_Package_ID__c = ''; 
        invent.building_location__c = loc.id;
        invent.property_id__c = newProperty.id;
        insert invent;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);        
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        sr.Agent_Name__c = UserInfo.getUserId();
        sr.Token_Amount_AED__c = 40000;
        sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Change Agent').getRecordTypeId();
        sr.Booking_Wizard_Level__c = null;
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE';
        sr.Mode_of_Payment__c = 'Cash';
        sr.agency__c = acc.id;
        insert sr;
        
        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk[0].Deal_SR__c = sr.id;
        insert lstbk;
        
        buyer__c b = new buyer__c();
        b.Buyer_Type__c =  'Individual';
        b.Address_Line_1__c =  'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.City__c = 'Dubai' ;       
        b.dob__c = system.today().addyears(-30);
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname' ;
        b.Last_Name__c =  'lastname';
        b.Nationality__c = 'Indian' ;
        b.Passport_Expiry__c = system.today().addyears(20) ;
        b.Passport_Number__c = 'J0565556' ;
        b.Phone__c = '569098767' ;
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b.Place_of_Issue__c =  'India';
        b.Title__c = 'Mr';
        b.booking__c = lstbk[0].id;
        b.Primary_Buyer__c =true;
        b.Account__c = acc.id;
        b.Unique_Key__c = lstbk[0].id+' '+acc.id;
        insert b;
        
        Payment_Plan__c pp = new Payment_Plan__c();        
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today(); 
        pp.Building_Location__c =  loc.id;  
        pp.term_id__c = '1234';   
        insert pp; 
        
        DAMAC_Constants.skip_BookingUnitTrigger = true;
        List<Booking_Unit__c> BUList = new List<Booking_Unit__c>();
        Booking_Unit__c bu1 = new Booking_Unit__c();
        bu1.Booking__c = lstbk[0].id;
        bu1.Payment_Method__c = 'Cash';
        bu1.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu1.Primary_Buyer_s_Name__c = 'testNSI';
        bu1.Primary_Buyer_s_Nationality__c = 'Russia';
        bu1.Inventory__c = invent.id;
        bu1.Registration_ID__c = '1234';
        bu1.Registration_DateTime__c = System.NOW();
        bu1.Payment_Plan_Id__c = pp.id;      
        BUList.add(bu1);
        insert BUList;
        
        BU_ID = BUList[0].id;
    }
    
    @isTest 
    static void getCommisionTileDataTest() {
        testData_setup();
        Test.startTest();
        
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/API_GetAgentCommisionTileData';
        request.httpMethod = 'GET';
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        System.runAs(portalUser) {
            API_GetCommisionSubTotalsForAgents.doGET(); /* 0 */
            
            request.addParameter('graph_type', 'DEFAULT');
            API_GetCommisionSubTotalsForAgents.doGET(); /* 1 */
            
            request.addParameter('graph_type', 'DEFAULT');
            request.addParameter('period', 'LAST_1_YEARS');
            API_GetCommisionSubTotalsForAgents.doGET(); /* 2 */
            
            request.addParameter('period', 'LAST_4_QUARTERS');
            API_GetCommisionSubTotalsForAgents.doGET(); /* 3i */
            
            request.addParameter('period', 'LAST_6_MONTHS');
            API_GetCommisionSubTotalsForAgents.doGET(); /* 3j */
            
            request.addParameter('graph_type', 'TOP_AGENTS');
            request.addParameter('period', 'LAST_1_YEARS');
            API_GetCommisionSubTotalsForAgents.doGET(); /* 4 */
            
            request.addParameter('graph_type', 'TOP_PROJECTS');
            API_GetCommisionSubTotalsForAgents.doGET(); /* 5 */
            
            request.addParameter('period', 'LAST_12_MONTHS');
            API_GetCommisionSubTotalsForAgents.doGET(); /* 6i */
            
            request.addParameter('period', 'LAST_6_MONTHS');
            API_GetCommisionSubTotalsForAgents.doGET(); /* 6j */
            
            request.addParameter('period', 'LAST_3_MONTHS');
            API_GetCommisionSubTotalsForAgents.doGET(); /* 6k */
            
            request.addParameter('period', 'LAST_MONTH');
            API_GetCommisionSubTotalsForAgents.doGET(); /* 6l */
            
            request.addParameter('page_size', 'INVALID');
            API_GetCommisionSubTotalsForAgents.doGET(); /* 7i */
            
            request.addParameter('page_size', '10');
            request.addParameter('page_number', 'INVALID');
            API_GetCommisionSubTotalsForAgents.doGET(); /* 7j */
            
            request.addParameter('page_number', '1');
            API_GetCommisionSubTotalsForAgents.doGET(); /* 7k */
        }
        
        User thisUser = [SELECT Id from User where Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Agent_Commission__c ac1 = new Agent_Commission__c();
            ac1.id = AgentCommisionID;
            delete ac1;
        }
        
        System.runAs(portalUser) {
        	API_GetCommisionSubTotalsForAgents.doGET(); /* 8 */
        }      
        
        Test.stopTest();
    }
    
    @isTest 
    static void getCommisionPlotDataTest() {
        testData_setup();
        Test.startTest();
        
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/API_GetAgentCommisionSubTotalPlots';
        request.httpMethod = 'GET';
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        System.runAs(portalUser) {
            API_GetCommisionSubTotalsPlotData.doGET(); /* 0 */
            
            request.addParameter('graph_type', 'DEFAULT');
            API_GetCommisionSubTotalsPlotData.doGET(); /* 1 */
            
            request.addParameter('period', 'LAST_1_YEAR');
            API_GetCommisionSubTotalsPlotData.doGET(); /* 2 */
            
            request.addParameter('period', 'LAST_4_QUARTERS');
            API_GetCommisionSubTotalsPlotData.doGET(); /* 3i */
            
            request.addParameter('period', 'LAST_6_MONTHS');
            API_GetCommisionSubTotalsPlotData.doGET(); /* 3j */
            
            request.addParameter('graph_type', 'TOP_PROJECTS');
            request.addParameter('period', 'LAST_1_YEAR');
            API_GetCommisionSubTotalsPlotData.doGET(); /* 4 */
            
            request.addParameter('period', 'LAST_12_MONTHS');
            API_GetCommisionSubTotalsPlotData.doGET(); /* 5i */
            
            request.addParameter('period', 'LAST_6_MONTHS');
            API_GetCommisionSubTotalsPlotData.doGET(); /* 5j */
            
            request.addParameter('period', 'LAST_3_MONTHS');
            API_GetCommisionSubTotalsPlotData.doGET(); /* 5k */
            
            request.addParameter('period', 'LAST_MONTH');
            API_GetCommisionSubTotalsPlotData.doGET(); /* 5l */
            
            request.addParameter('graph_type', 'TOP_AGENTS');
            request.addParameter('period', 'LAST_1_YEAR');
            API_GetCommisionSubTotalsPlotData.doGET(); /* 6 */
        }
        
        System.runAs(portalUser) {
            Agent_Commission__c ac1 = new Agent_Commission__c();
            ac1.id = AgentCommisionID;
            delete ac1;
        }
        
        System.runAs(portalUser) {
        	API_GetCommisionSubTotalsPlotData.doGET(); /* 7 */
        }
        
        Test.stopTest();
    }
}