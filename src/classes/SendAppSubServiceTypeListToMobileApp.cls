@RestResource(urlMapping='/SendAppointMEntSubServiceTypeListToMobileApp/*')
 Global class SendAppSubServiceTypeListToMobileApp
 {
     @HtTPPost
    Global static list<String> SendAppointMEntSubServiceTypeListToMobile(String ProcessType)
    {
       List<String>lstSubProcessOptions = new List<String>();
                lstSubProcessOptions.add('--None--');

              
                List<Sub_Process_for_Portal__mdt> lstSubProcess = new List<Sub_Process_for_Portal__mdt>();
                lstSubProcess = [Select id, MasterLabel, DeveloperName,Active_Process_on_Portal__c
                                from Sub_Process_for_Portal__mdt
                                where Active_Process_on_Portal__r.MasterLabel =: ProcessType];

                if(lstSubProcess.Size()>0){
                    
                    for(Sub_Process_for_Portal__mdt objSubProcess : lstSubProcess ){
                        lstSubProcessOptions.add(objSubProcess.MasterLabel);
                    }
                }
                Return lstSubProcessOptions;
    }
 }