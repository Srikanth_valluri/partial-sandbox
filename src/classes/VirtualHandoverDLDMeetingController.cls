public with sharing class VirtualHandoverDLDMeetingController {
    public string callingListId {get;set;}
    public Calling_List__c callinglist {get; set;}
    public String message {get; set;}
    public String subject {get; set;}
    public String messageBody {get; set;}
    public Boolean success;
    public String meetingId;
    //public API_Integration_Value__c apiValue ;
    ID recordId;
    public List<Buyer__c> buyerList = new List<Buyer__c>();
    public List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
    public String emailList = '';
    public Id vatContract = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('VAT Contract Variance').getRecordTypeId();
    
    public VirtualHandoverDLDMeetingController(ApexPages.StandardController controller) {
        recordId = controller.getId();

        message = '';
        success = false;
        messageBody = '';
        
        callingListId = recordId;
        
        // apiValue = [SELECT Id, Refresh_Token__c, Client_Id__c, Client_Secret__c,
        //                                             Endpoint_URL__c, Redirect_URI__c, Scope__c,
        //                                             Tenant_Id__c, Username__c, Password__c
        //             FROM API_Integration_Value__c WHERE name = 'Virtual Handover Graph API' 
        //             AND  Username__c = 'DLDTrustee.Meeting@damacproperties.com' LIMIT 1];

                    
    }

    /************************************************************************************************
    * @Description : Invokes createTeamsMeeting() in DAMAC_MSTeamsIntegration with the required
    *                    inputs from the VF Page
    * @Params      : None
    * @Return      : None 
    ************************************************************************************************/
    public pageReference createMeeting(){
       
        String meetingURL;
        if( recordId != null ) {
            callinglist = [SELECT 
                                Id
                                , Name
                                , Appointment_Start_DateTime__c
                                , Appointment_End_DateTime__c  
                                , Account__r.Name
                                , Booking_Unit__r.Unit_Name__c
                                , Virtual_Meeting_Link__c
                                , Remarks_Pratik__c
                                , Booking_Unit__r.SnagValue__c
                                , Remarks_Vivek__c
                                , Account__r.Person_Business_Email__c
                                , MeetingDateTime__c
                                , Booking_Unit__c
                                , CL_Owner_Email__c
                                , Booking_Unit__r.Booking__c
                                , Customer_Name__c
                                , Meeting_Slots__c
                            FROM 
                                Calling_List__c 
                            WHERE 
                                Id =: recordId 
                            AND 
                                Booking_Unit__r.Booking__c != null ];

            System.debug('Calling List-->' +callingList);

            if( callinglist.Meeting_Slots__c == null ) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select appointment slot before sending meeting invite !!'));
                return null;
            }
            else {
                List<DLD_Trustee_Appointment_Slots__c> listAvailableSlots = getAvailableTrustee( callinglist.Meeting_Slots__c );
                System.debug( 'listAvailableSlots--' + listAvailableSlots);
                if( listAvailableSlots != null && listAvailableSlots.size() > 0 ) {
                    String dldTrusteeEmail = listAvailableSlots[0].Trustee_Email__c;

                    buyerList = [SELECT 
                            Email__c 
                        FROM 
                            Buyer__c 
                        WHERE 
                            Booking__c =: callingList.Booking_Unit__r.Booking__c 
                        AND 
                            Is_Joint_Buyer__c = true];
            
                    if(!buyerList.isEmpty()) {
                        System.debug('Buyer List-->' +buyerList);

                        for(Buyer__c buyer: buyerList){
                            System.debug('Buyer Email-->' +buyer.Email__c);
                            emailList += buyer.Email__c;
                            emailList += ',';
                        }
                    }

                    emailList += callingList.Account__r.Person_Business_Email__c;
                    emailList += ',';
                    emailList += callingList.CL_Owner_Email__c;
                    emailList += ',';
                    emailList += dldTrusteeEmail;

                    String startTimeStr = callinglist.Meeting_Slots__c.substringBefore('-');
                    String endTimeStr = callinglist.Meeting_Slots__c.substringAfter('-');
                    
                    String startHour = startTimeStr.substringBefore(':');
                    String startMinute = startTimeStr.substringAfter(':');
                    String endHour = endTimeStr.substringBefore(':');
                    String endMinute = endTimeStr.substringAfter(':');

                    DateTime startTime = DateTime.newInstance(System.today().year(), System.today().month(), System.today().day(), Integer.ValueOf(startHour), Integer.ValueOf(startMinute), 0);
                    System.debug('Start Date Time-->' +startTime);

                    DateTime endTime = DateTime.newInstance(System.today().year(), System.today().month(), System.today().day(), Integer.ValueOf(endHour), Integer.ValueOf(endMinute), 0);
                    System.debug('End Date Time-->' +endTime);

                    System.debug('Email List-->' +emailList);
                    System.debug('callinglist.MeetingDateTime__c-->' +callinglist.MeetingDateTime__c);
                    subject = 'Video Call Appointment with Dubai Land Department (DLD) Certified' +'\n'
                    + 'Registration Trustee for your unit ' + callingList.Booking_Unit__r.Unit_Name__c + ' : '+ callingList.Id + ' : ' ;
                    
                    EmailTemplate emailTemplate = [SELECT Body from EmailTemplate where DeveloperName = 'Acknowledgement_Email_Park'];
                    String timeZone = UserInfo.getTimezone().getDisplayName().subStringBefore(' ');
                    if(messageBody == ''){
                        messageBody = 'Dear ' + callinglist.Customer_Name__c  + ',' + '\n\n'
                        + ' We are pleased to confirm the appointment with Dubai Land Department(DLD) ' + '\\n'
                        + ' Certified Registration Trustee on ' + startTime.format('dd/MM/YYYY HH:mm') + ' ' + timeZone + '\n\n' 
                        + ' To ensure a smooth verification, we would request you to ensure the following ' + '\n\n'
                        + ' a. Please check the Audio and Video on your Computer/Phone, 5 mins prior to the meeting time.' + '\n'
                        + ' b. All the buyers of the unit should be present on the call.' + '\n'
                        + ' c. The buyers should ensure they have the passport with them during the video call for ' + '\n'
                        + ' verification by the Registration Trustee on the Video Call' + '\n'
                        + ' d. Keep in handy the list of unit number(s) purchased by you.' + '\n\n'
                        + 'Regards,' + '\n'
                        + 'At your service team'  ;
                        //messageBody = emailTemplate.Body;
                    }
                    // } 
                    // else{
                    //     message = 'callinglist Id is missing';
                    // }

                    VirtualHandover_MSTeamsIntegration.createDLDTeamsMeeting(startTime, endTime, emailList, subject, messageBody,  callinglist);
                    
                    listAvailableSlots[0].Slot_Booked__c = true;
                    update listAvailableSlots;
                    callingList.CRE_Remarks__c = listAvailableSlots[0].Trustee_Email__c;
                    update callingList;
                    String strReturnUrl = '/'+recordId;
                    PageReference pg = new PageReference(strReturnUrl);
                    pg.setRedirect(true);
                    return pg;       
                }
                else {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 
                    'Sorry! ' + callinglist.Meeting_Slots__c + ' slot is not available currently. Please select next slot to send meeting invite !!'));
                    return null;
                }
            }
        }
        
        // VirtualHandover_MSTeamsIntegration.createDLDTeamsMeeting(callinglist.MeetingDateTime__c, callinglist.MeetingDateTime__c.addMinutes(60),
        //                                                              emailList, subject, messageBody,  callinglist);
        
        String strReturnUrl = '/'+recordId;
        PageReference pg = new PageReference(strReturnUrl);
        pg.setRedirect(true);
        return pg;                                                                       
    }

    public List<DLD_Trustee_Appointment_Slots__c> getAvailableTrustee( String selectedSlot ) {

        return [SELECT 
                    Appointment_Slot__c
                    , Slot_Booked__c
                    , Trustee_Email__c
                    , Trustee_Order__c
                FROM
                    DLD_Trustee_Appointment_Slots__c
                WHERE
                    Appointment_Slot__c =: selectedSlot
                AND
                    Slot_Booked__c = false
                ORDER BY
                    Trustee_Order__c ASC
                LIMIT 1 
                ];
    }
}