/*
 * Description: Class used to create Archival Task for Case
 */
public with sharing class ParkingArchivalTaskUtility {

    /**
     * Method used to create Archival Task and create a task on IPMS through process builder
     */
    @InvocableMethod
    public static void createArchivalTask(List<Case> lstCases) {
        Task objArchivalTask = new Task();
        objArchivalTask.ActivityDate = System.today() +1;
        objArchivalTask.Assigned_User__c = 'CDC';
        objArchivalTask.CurrencyIsoCode = 'AED';
        objArchivalTask.OwnerId = lstCases[0].OwnerId;
        objArchivalTask.Priority = 'High';
        objArchivalTask.Process_Name__c = 'Parking';
        objArchivalTask.Status = 'Not Started';
        objArchivalTask.Subject = 'Archival of Documents';
        objArchivalTask.WhatId = lstCases[0].Id;
        try {
            insert objArchivalTask;
            AdditionalParkingTaskHandler.createTaskForArchival(objArchivalTask);
        } catch(Exception exp) {
            DocumentExtension.errorLoggerNew('error for task parking '+exp.getMessage(),lstCases[0].Id,'');
        }
    }
}