/**
 * @File Name          : FMCaseEscalationViolationNoticeBatchTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/20/2019, 4:37:47 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/20/2019, 3:58:14 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
private class FMCaseEscalationViolationNoticeBatchTest {

    @isTest static void isShould() {
        Account objAcc = new Account();
        objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        Location__c objLoc = new Location__c();
        objLoc = TestDataFactoryFM.createLocation();
        insert objLoc;
        Violation_Rule__c objVR = new Violation_Rule__c();
        objVR.Building__c = objLoc.Id;
        objVR.Category__c  ='General Violations';
        objVR.Payble_Fine__c = 100;
        objVR.Remedial_Period__c = '7 days';
        objVR.Violation_Of_Rule__c = 'Abusing personnel working in the community';
        insert objVR;
        FM_User__c objFMUser = new FM_User__c(FM_User__c=UserInfo.getUserId(),
                    FM_Role__c = 'Property Manager',
                    Building__c = objLoc.Id);
        insert objFMUser;
        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAcc);
        insert objSR;
        Booking__c objBooking = TestDataFactoryFM.createBooking(objAcc, objSR);
        insert objBooking;
        Booking_unit__c objBU = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
        insert objBU;

        List<Violation_Notice__c> lstVRs = new List<Violation_Notice__c>();
        lstVRs.add(new Violation_Notice__c(
        Name = 'Rule 1',
        Active__c = true,
        Category__c  ='General Violations',
        Payble_Fine__c = 100,
        Remedial_Period__c = '7 days',
        Violation_Of_Rule__c = 'Abusing personnel working in the community'
        ));
        insert lstVRs;

        FM_Case__c fmObj = new FM_Case__c();
        Id RecordTypeIdFMCase = DamacUtility.getRecordTypeId(
            'FM_Case__c', 'Violation Notice'
        );
        fmObj.recordtypeid = RecordTypeIdFMCase;
        fmObj.Property_Director_Email__c = 'a@a.com';
        fmObj.Select_Notice_Type_Task_Created_Date__c = date.today().adddays(-5); 
        fmObj.Is_Violation_Case_Escalation_Sent__c = false;
        insert fmObj;

        FM_Case__c fmObj1 = new FM_Case__c();
        fmObj1.recordtypeid = RecordTypeIdFMCase;
        fmObj1.Property_Director_Email__c = 'a@a.com';
        fmObj1.Select_Notice_Type_Task_Created_Date__c = date.today().adddays(-5);
        fmObj1.Is_Violation_Case_Escalation_Sent__c = false;

        FM_Case__c fmObj2 = new FM_Case__c();
        fmObj2.recordtypeid = RecordTypeIdFMCase;
        fmObj2.Property_Director_Email__c = 'ab@a.com';
        fmObj2.Select_Notice_Type_Task_Created_Date__c = date.today().adddays(-5);
        fmObj2.Is_Violation_Case_Escalation_Sent__c = false;

        FM_Case__c fmObj3 = new FM_Case__c();
        fmObj3.recordtypeid = RecordTypeIdFMCase;
        fmObj3.Property_Director_Email__c = 'aba@a.com';
        fmObj3.Select_Notice_Type_Task_Created_Date__c = date.today().adddays(-5);
        fmObj3.Is_Violation_Case_Escalation_Sent__c = false;
        insert fmObj3;

        FM_Case__c fmObj4 = new FM_Case__c();
        fmObj4.recordtypeid = RecordTypeIdFMCase;
        fmObj4.Property_Director_Email__c = 'ab@a.com';
        fmObj4.Select_Notice_Type_Task_Created_Date__c = date.today().adddays(-5);
        fmObj4.Is_Violation_Case_Escalation_Sent__c = false;
        insert fmObj4;

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testdmacorg.com');

        System.runAs(u) {   
            insert fmObj1;         
            insert fmObj2;
        }

        FMCaseEscalationViolationNoticeBatch objClass = new FMCaseEscalationViolationNoticeBatch();
        Database.Executebatch(objClass);
    }
}