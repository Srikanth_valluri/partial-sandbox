@isTest
public class DAMAC_AssignPcs_test {
    public static testMethod void method1() {
        Unit_Allocation__c unitAlloc = New Unit_Allocation__c();
            unitAlloc.Start_Date__c = system.today();
            unitAlloc.End_Date__c = system.today();
        insert unitAlloc;
        Inventory__c inv = New Inventory__c ();
            inv.Unit_Allocation__c = unitAlloc.id;
        insert inv;
        DAMAC_AssignPcs obj = New DAMAC_AssignPcs(new ApexPages.StandardController(unitAlloc));
        obj.hiddenVal = 'notNull';
        obj.pcName = 'test';
        obj.hosName = 'test';
        obj.dosName ='test';
        obj.searchSectionAgencyName = 'test';
        obj.searchSectionCity = 'test';
        obj.searchSectionCountry = 'test';
        obj.sortColVal = 'country';
        obj.searchSectionTypeofAccount = 'Individual';
        ApexPages.currentPage().getParameters().put('limitVal ','10');
        ApexPages.currentPage().getParameters().put('hiddenVal','notNull');
        ApexPages.currentPage().getParameters().put('searchText','test');
        obj.limitVal = 10;
        obj.getRecCount('pageLoadCall');
        obj.filterData();
        obj.dataPagination();
        obj.nextbtn();
        obj.prvbtn();
        obj.refreshTableData();
      //obj.changeRecCountFunMethod();
        obj.searchRecs();
        obj.changeSortOrder();
        obj.getprv();
        obj.getnxt();
        DAMAC_AssignPcs.getAccounts('test');
        DAMAC_AssignPcs.saveInventoriesMethod(unitAlloc.id,userInfo.getUserId());
    }
}