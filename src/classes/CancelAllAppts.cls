public class CancelAllAppts{//AKISHOR

public string AptId {get;set;}
    public CancelAllAppts(ApexPages.StandardController controller) { AptId = apexpages.currentpage().getparameters().get('id');}
      
    public pagereference UpdateAppointment(){        
     
        List<Calling_List__c> CList= [SELECT ID,Appointment_Status__c FROM Calling_List__c WHERE ID =:AptId];
        system.debug('\n--lstCase--'+CList);
        if(!CList.isEmpty()){           
                 if(CList[0].Appointment_Status__c=='Confirmed'||CList[0].Appointment_Status__c=='Requested')
                 {
                     CList[0].Appointment_Status__c= 'Cancelled';               
                     system.debug('\n--lstCase--'+CList);
                      update CList;
                      pagereference page = new Pagereference('/'+AptId);
                      page.setRedirect(true);
                      return page;                 
                 }                        
        }
        return null;          
    }
}