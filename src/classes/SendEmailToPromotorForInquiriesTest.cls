@istest
public Class SendEmailToPromotorForInquiriesTest{

    @isTest 
     static void test_method_one() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];  
        User u = new User(Alias = 'standt', Email='sendemailtopromo@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='sendemailtoprnmolastname', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',ProfileId = p.Id,  
            TimeZoneSidKey='America/Los_Angeles', UserName='sendemailtopromo@testorg.com');
        insert u;    
             
        Inquiry__c inquiryInst = new Inquiry__c(
            First_Name__c = 'Test',
            Last_Name__c = 'Last Test',
            Inquiry_Source__c = 'Call Center',
            Inquiry_Status__c = 'Active',
            Email__c = 'test@gmail.com',
            OwnerId = UserInfo.getuserid()
        );
        insert inquiryInst;
        
        Inquiry__c inquiryInst2 = new Inquiry__c(
            First_Name__c = 'Test',
            Last_Name__c = 'Last Test',
            Inquiry_Source__c = 'Call Center',
            Inquiry_Status__c = 'Active',
            Email__c = 'test@gmail.com',
            OwnerId = UserInfo.getuserid()
        );
        insert inquiryInst2;    
        
        Promoter_Inquiry__c promoinq = new Promoter_Inquiry__c();
        promoinq.Inquiry__c = inquiryInst.id;
        promoinq.Created_By_Manager__c = u.id;
        insert promoinq;
        
        Promoter_Inquiry__c promoinq2 = new Promoter_Inquiry__c();
        promoinq.Inquiry__c = inquiryInst2.id;
        promoinq.Created_By_Manager__c = u.id;
        insert promoinq2;
        
        test.starttest();
            SendEmailToPromotorForInquiries  schduleobj = new SendEmailToPromotorForInquiries(System.now());
            Database.executeBatch(schduleobj);  
             SendEmailToPromoterManagerScheduler sendEmailToManagerObj = new SendEmailToPromoterManagerScheduler();
             String sch = ' 0 0 0/3 ? * * *';
             String jobID = System.schedule('Send Email To Managers', sch, sendEmailToManagerObj);
        test.stoptest();
    
    }
  
    /*  @isTest 
     static void test_method_two() {
       UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'test2sendpromoteremail@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        isActive = true,
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        
        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        
        System.runAs ( portalAccountOwner1 ) {
        //Create account
        Account portalAccount1 = new Account(
        Name = 'TestAccount',
        OwnerId = portalAccountOwner1.Id
        );
        Database.insert(portalAccount1);
        
        //Create contact
        Contact contact1 = new Contact(
        FirstName = 'Test',
        Lastname = 'McTesty',
        AccountId = portalAccount1.Id,
        Email = System.now().millisecond() + 'test@test.com'
        );
        Database.insert(contact1);
        Contact contact2 = new Contact(
        FirstName = 'Test',
        Lastname = 'McTesty',
        AccountId = portalAccount1.Id,
        Email = System.now().millisecond() + 'test@test.com'
        );
        Database.insert(contact2);
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name='Promoter Community Profile'];
        User userNew = new User(
        Username = System.now().millisecond() + 'test132345senepromomail@test.com',
        ContactId = contact2.Id,
        ProfileId = portalProfile.Id,
        Alias = 'test1233',
        Email = 'test123435@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        isActive = true,
        CommunityNickname = 'test123435',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US'
        );
        Database.insert(userNew);
        User user2 = new User(
        Username = System.now().millisecond() + 'test12345senepromomail@test.com',
        ContactId = contact1.Id,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'test12345@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        isActive = true,
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',managerid =userNew.id,
        LanguageLocaleKey = 'en_US'
        );
        Database.insert(user2);
            system.runAs(user2) {
                Promoter_Inquiry__c promoinq = new Promoter_Inquiry__c();
               // promoinq.Inquiry__c = inquiryInst.id;
                promoinq.Created_By_Manager__c = user2.Id;
                insert promoinq;
            }    
        }
        test.starttest();
             SendEmailToPromoterManagerScheduler sendEmailToManagerObj = new SendEmailToPromoterManagerScheduler();
             String sch = ' 0 0 0/3 ? * * *';
             String jobID = System.schedule('Send Email To Managers', sch, sendEmailToManagerObj);
        test.stoptest();
     }      */      
}