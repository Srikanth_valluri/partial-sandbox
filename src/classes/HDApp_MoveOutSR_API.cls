/**********************************************************************************************************************
Description: This API is used for creating Move-Out FM Case raised from Damac Living App
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   15-10-2020      | Shubham Suryawanshi | Created initial draft
1.1     |   05-11-2020      | Shubham Suryawanshi | Text change for SR Name
***********************************************************************************************************************/
@RestResource(urlMapping='/InsertMoveOutCase/*')
global class HDApp_MoveOutSR_API {

    public static String errorMsg;
    public static Integer statusCode;
    public static String unitRegId;
    public static String partyId;

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    @HttpPost
    global static FinalReturnWrapper apiMainMethod() {

        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString'+jsonString);
        System.debug('Request params:' + req.params);

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        FmCaseWrapper objFmWrap = new FmCaseWrapper(); 
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();

        if(String.isBlank(jsonString)) {
            objMeta = ReturnMetaResponse('No JSON body found in request', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        System.debug('jsonString:: ' + jsonString);
        objFmWrap = (FmCaseWrapper)JSON.deserialize(jsonString, FmCaseWrapper.class);
        System.debug('objFmWrap: ' + objFmWrap);

        if(objFmWrap.action == null || (!objFmWrap.action.equalsIgnoreCase('draft') && !objFmWrap.action.equalsIgnoreCase('submit') )) {
            objMeta = ReturnMetaResponse('Invalid parameter value in : action', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;

        }

        Boolean isTenant = false;
        if(String.isNotBlank(objFmWrap.account_id)) {
            List<User> usr = [SELECT id
                               , Name 
                               , Username
                               , Profile.Name 
                               , Contact.AccountId 
                         FROM User
                         WHERE Contact.AccountId =: objFmWrap.account_id];
            System.debug('usr:: ' + usr);
            isTenant = usr[0].profile.Name.contains('Tenant Community') ? true : false;
        }
        else {
            objMeta = ReturnMetaResponse('No account_id passed in request body', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        System.debug('isTenant: ' + isTenant);

        if(objFmWrap != null) {
            System.debug('action in InsertTenantRegCase: ' + objFmWrap.action);
            if(objFmWrap.action.equalsIgnoreCase('submit') && String.isBlank(objFmWrap.id) && objFmWrap.move_out_type.containsIgnoreCase('company')) {
                objMeta = ReturnMetaResponse('No FM Case id passed in the request', 3);
                returnResponse.meta_data = objMeta;
                return returnResponse;
            }

            if(objFmWrap.action.equalsIgnoreCase('submit') && String.isNotBlank(objFmWrap.id) && objFmWrap.move_out_type.containsIgnoreCase('company')) {
                //To verify if mandator Move-Out docs in case of Move-Out using company is done
                if(!HDAppInsertMoveInCase.VerifyMandatoryDocs(objFmWrap.id)) {
                    objMeta = ReturnMetaResponse('Mandatory Documents upload pending', 2);
                    returnResponse.meta_data = objMeta;
                    return returnResponse;
                }
            }

            if(objFmWrap.action.equalsIgnoreCase('submit') && String.isNotBlank(objFmWrap.id) && isTenant && !verifyIsNocDocUploaded(objFmWrap.id)) {
                objMeta = ReturnMetaResponse('Mandatory Documents upload pending', 2);
                returnResponse.meta_data = objMeta;
                return returnResponse;
            }

            FM_Case__c objFM = ProcessFMCase(objFmWrap, objFmWrap.action);
            System.debug('objFM: ' + objFm);

            System.debug('errorMsg:' + errorMsg);
            System.debug('statusCode:' + statusCode);
            objMeta = ReturnMetaResponse(errorMsg, statusCode);

            List<FM_Case__c> moveOutCaseDetails = objFM != null ? GetTenantRegistrationCase_API.getFMCase(objFM.id) : null;
            System.debug('moveOutCaseDetails:: ' + moveOutCaseDetails);
            if(moveOutCaseDetails != null) {
                objData.booking_unit_id = moveOutCaseDetails[0].Booking_Unit__c;
                objData.booking_unit_name = moveOutCaseDetails[0].Booking_Unit__r.Unit_Name__c;
                objData.sr_id = moveOutCaseDetails[0].id;
                objData.sr_number = moveOutCaseDetails[0].Name;
                objData.sr_type = 'Move out';
                objData.sr_status = moveOutCaseDetails[0].Status__c;
                //objData.unit_reg_id; = 

                objData.sr_submission_date = moveOutCaseDetails[0].Submission_Date__c != null ? Datetime.newInstance(moveOutCaseDetails[0].Submission_Date__c.year(), moveOutCaseDetails[0].Submission_Date__c.month(), moveOutCaseDetails[0].Submission_Date__c.day()).format('yyyy-MM-dd') : '';
                objData.move_out_date = moveOutCaseDetails[0].Expected_move_out_date__c != null ? Datetime.newInstance(moveOutCaseDetails[0].Expected_move_out_date__c.year(), moveOutCaseDetails[0].Expected_move_out_date__c.month(), moveOutCaseDetails[0].Expected_move_out_date__c.day()).format('yyyy-MM-dd') : ''; 
                objData.fm_case_details = getMoveOutCaseWrapper(moveOutCaseDetails[0]);

                //System.debug('errorMsg:' + errorMsg);
                //System.debug('statusCode:' + statusCode);
                //objMeta = ReturnMetaResponse(errorMsg, statusCode);
                
            }
            else {
                objMeta = String.isNotBlank(errorMsg) && statusCode != 1 ? ReturnMetaResponse(errorMsg, statusCode) : ReturnMetaResponse('No FM Case found/Created', 2);
            }
        }

        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;
        System.debug('returnResponse:: ' + returnResponse);

        return returnResponse;   
    }

    public Static FM_Case__c ProcessFMCase( FmCaseWrapper objFmCaseWrap, String action ) {

        List<Booking_Unit__c> unit = new List<Booking_Unit__c>();
        if(String.isNotBlank(objFmCaseWrap.booking_unit_id)) {
            unit = [ SELECT Id
                          , Unit_Name__c
                          , Booking__r.Account__c
                          , Booking__r.Account__r.Party_Id__c
                          , Registration_Id__c
                          , Property_City__c
                          , Resident__c
                          , Tenant__c
                     FROM Booking_Unit__c
                     WHERE Id = :objFmCaseWrap.booking_unit_id
                     LIMIT 1 ];

        }
        else {
            errorMsg = 'Missing parameter value : booking_unit_id in JSON request body';
            statusCode = 3;
            return null;
        }

        if(unit.isEmpty() && unit.size() < 1) {

            errorMsg = 'No booking unit found for given booking_unit_id';
            statusCode = 3;
            return null;
        }

        if(String.isBlank(objFmCaseWrap.account_id)) {
            errorMsg = 'No account_id found in request';
            statusCode = 3;
            return null;
        }

        Account objAcc = getCurrentAccountDetails(objFmCaseWrap.account_id);
        System.debug('objAcc:: ' + objAcc);

        unitRegId = unit[0].Registration_Id__c;
        partyId = unit[0].Booking__r.Account__r.Party_Id__c;

        Location__c objLoc = new Location__c();
        list<Location__c> lstLocs = new list<Location__c>();
        lstLocs = [ SELECT Community_Name__c
                        , Move_In_Move_Out_security_cheque_amount__c
                        , Name
                        , ( SELECT FM_User__c
                                   , FM_User__r.Email
                                   , FM_Role__c
                                   , FM_User__r.Name
                            FROM FM_Users__r )
                    FROM Location__c
                    WHERE Name =: unit[0].Unit_Name__c.substringBefore('/')
                    LIMIT 1 ];
        if(lstLocs.size() > 0) {
            objLoc = lstLocs[0];
        }

        System.debug('objFmCaseWrap:: ' + objFmCaseWrap);
        FM_Case__c objCase = new FM_Case__c();

        if(String.isNotBlank(objFmCaseWrap.id)) {       //When case is already created
            objCase.id = objFmCaseWrap.id;
        }

        //Populating common entities
        objCase.isHelloDamacAppCase__c = true;
        objCase.Raised_Date__c = System.now();
        objCase.Origin__c='Portal';
        objCase.Status__c='New';
        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get(
                                'Move Out').getRecordTypeId();
        objCase.RecordTypeId = devRecordTypeId;//Set record type
        Boolean isTenant = unit[0].Booking__r.Account__c == unit[0].Resident__c ? false : true;
        objCase.Initiated_by_tenant_owner__c = isTenant ? 'tenant' : 'owner';
        objCase.Request_Type_DeveloperName__c = isTenant ? 'Move_out_Request_T' : 'Move_out_Request';
        objCase.Building_Name__c = objLoc != null ? objLoc.Name : null;
        objCase.Request_Type__c = 'Move Out';
        objCase.Booking_Unit__c  = unit[0].id;
        objCase.Account__c = unit[0].Booking__r.Account__c;
        objCase.Tenant__c = objFmCaseWrap.account_id;


        if(action.equalsIgnoreCase('draft')) {
            objCase.Status__c = 'Draft Request';
        }

        //Submit
        if(action.equalsIgnoreCase('submit')) {

            if(objLoc != NULL && objLoc.FM_Users__r.size() > 0) {
                for(FM_User__c obj: objLoc.FM_Users__r) {
                    if(obj.FM_Role__c == 'FM Admin') {
                        objCase.Admin__c = obj.FM_User__c;
                        // break;
                    }
                    else if(obj.FM_Role__c == 'Property Manager') {
                        objCase.Property_Manager_Email__c = obj.FM_User__r.Email;
                        objCase.Property_Manager_Name__c = obj.FM_User__r.Name;
                        // break;
                    }
                }
            }

            list<FM_Approver__mdt> lstApprovers = FM_Utility.fetchApprovers(
                            objCase.Request_Type_DeveloperName__c,unit[0].Property_City__c);
            System.debug('==lstApprovers==' + lstApprovers);
            String approvingRoles = '';
            for(FM_Approver__mdt mdt : lstApprovers) {
                approvingRoles += mdt.Role__c + ',';
            }
            approvingRoles = approvingRoles.removeEnd(',');
            objCase.Approving_Authorities__c = approvingRoles;

            objCase.Account__c = unit[0].Booking__r.Account__c;
            objCase.Status__c = 'Submitted';
            objCase.Approval_Status__c='Pending';
            objCase.Tenant_Email__c = objAcc != null ? objAcc.Email__pc : null;
            objCase.Submit_for_Approval__c = true;   
            objCase.Submitted__c = true;
            
        }

        //Populating move-ot specific fields
        objCase.Expected_move_out_date__c = String.isNotBlank(objFmCaseWrap.expected_move_out_date) ? Date.valueOf(objFmCaseWrap.expected_move_out_date) : null;
        objCase.Move_in_move_out_type__c = String.isNotBlank(objFmCaseWrap.move_out_type) ? objFmCaseWrap.move_out_type : null;
        objCase.Company__c = String.isNotBlank(objFmCaseWrap.moving_company_name) ? objFmCaseWrap.moving_company_name : null;
        objCase.Contractor__c = String.isNotBlank(objFmCaseWrap.moving_contractor_name) ?  objFmCaseWrap.moving_contractor_name : null;
        objCase.Mobile_no_contractor__c = String.isNotBlank(objFmCaseWrap.moving_contractor_phone) ? objFmCaseWrap.moving_contractor_phone : null;
        objCase.Reason_for_Move_Out__c = String.isNotBlank(objFmCaseWrap.reason_to_move_out) ? objFmCaseWrap.reason_to_move_out : null;


        try {
            System.debug('objCase before insert: ' + objCase);
            upsert objCase;

            errorMsg = 'Successful';
            statusCode = 1;
            System.debug('objCase after insert: ' + objCase);
        }
        catch(Exception e) {
            System.debug('Exception: ' + e.getMessage());
            errorMsg = e.getMessage();
            statusCode = 5;
        }

        return objCase;
    }

    public static Account getCurrentAccountDetails(String accId) {

        if(String.isBlank(accId)) {
            return null;
        }

        list<Account> acc = [SELECT id 
                                  , Name 
                                  , IsPersonAccount 
                                  , Email__pc
                                  , Email__c 
                                  , Party_Id__c
                             FROM Account 
                             WHERE id =: accId];
        return acc.size() > 0 ? acc[0] : null;
    }

    public static FmCaseReturnWrapper getMoveOutCaseWrapper(FM_Case__c objFmCase) {

        FmCaseReturnWrapper objFmCaseRetWrap = new FmCaseReturnWrapper();

        objFmCaseRetWrap.fm_case_id = objFmCase.id;
        objFmCaseRetWrap.fm_case_status =  objFmCase.Status__c;
        objFmCaseRetWrap.fm_case_number = objFmCase.Name;
        objFmCaseRetWrap.booking_unit_id = objFmCase.Booking_Unit__c;

        objFmCaseRetWrap.move_out_date = objFmCase.Expected_move_out_date__c != null ? Datetime.newInstance(objFmCase.Expected_move_out_date__c.year(), objFmCase.Expected_move_out_date__c.month(), objFmCase.Expected_move_out_date__c.day()).format('yyyy-MM-dd') : '';
        objFmCaseRetWrap.move_out_type = objFmCase.Move_in_move_out_type__c;    
        objFmCaseRetWrap.moving_company_name = objFmCase.Company__c;
        objFmCaseRetWrap.moving_contractor_name = objFmCase.Contractor__c;
        objFmCaseRetWrap.moving_contractor_phone = objFmCase.Mobile_no_contractor__c;
        objFmCaseRetWrap.reason_to_move_out = objFmCase.Reason_for_Move_Out__c; 

        list<cls_attachment> lstAttachments = new list<cls_attachment>();

        if(objFmCase.Documents__r.size() > 0) {
            System.debug('objFmCase.Documents__r'+objFmCase.Documents__r);
            for(SR_Attachments__c obj : objFmCase.Documents__r) {
                cls_attachment objAttach = new cls_attachment();
                objAttach.id = obj.id;
                objAttach.name = obj.Name;
                objAttach.file_extension = obj.Type__c;
                objAttach.attachment_url = obj.Attachment_URL__c;

                lstAttachments.add(objAttach);
            }
        }

        objFmCaseRetWrap.attachments = lstAttachments;
        return objFmCaseRetWrap;

    }

    public static boolean verifyIsNocDocUploaded(String fmCaseId ) {

        System.debug('Inside verifyIsNocDocUploaded');
        System.debug('fmCaseId: ' + fmCaseId);

        Boolean isDocsValid = false;
        List<String> onlyMandatoryDocs = new List<String>();
        
        onlyMandatoryDocs.add('noc from landlord');
        

        List<SR_Attachments__c> insertedAttachments = [SELECT Name
                                                       FROM SR_Attachments__c
                                                       WHERE FM_Case__c =: fmCaseId];

        Set<String> uploadedDocs = new Set<String>();
            
        for(SR_Attachments__c objA : insertedAttachments) {
            uploadedDocs.add(objA.Name.toLowerCase());
        }

        System.debug('uploadedDocs:; ' + uploadedDocs);
        System.debug('onlyMandatoryDocs: ' + onlyMandatoryDocs);

        for(String objReqDoc : onlyMandatoryDocs) {
            isDocsValid = uploadedDocs.contains(objReqDoc);
            if(!isDocsValid) {
                break;
            } 
        }
        System.debug('isDocsValid for Noc for landlord: ' + isDocsValid);

        if(Test.isRunningTest()) {
            return true;
        }
        return isDocsValid;
    }

    public static cls_meta_data ReturnMetaResponse(String message, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data();
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = null;
        return retMeta;
    }

    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }

    public class cls_data {
        public String booking_unit_id;
        public String booking_unit_name;
        public String sr_id;
        public String sr_number;
        public String sr_type;
        public String sr_status;
        //public String unit_reg_id; 
        public String sr_submission_date;
        public String move_out_date;
        public FmCaseReturnWrapper fm_case_details;
    }

    public class FmCaseWrapper {
        public String action;
        public String id;
        public String booking_unit_id;
        public String account_id;
        public String expected_move_out_date;
        public String move_out_type;
        public String moving_company_name;
        public String moving_contractor_name;
        public String moving_contractor_phone;
        public String reason_to_move_out;
        
    }

    public class FmCaseReturnWrapper {
        public String fm_case_id;   
        public String fm_case_status;   
        public String fm_case_number;
        public String booking_unit_id;

        public String move_out_date;
        public String move_out_type;    //Self move out / Moving company
        public String moving_company_name;
        public String moving_contractor_name;
        public String moving_contractor_phone;
        public String reason_to_move_out;
        public cls_attachment[] attachments;
    }

    public class cls_attachment {
        public String id;
        public String name;
        public String attachment_url;
        public String file_extension;
    }

}
/* INPUT JSON Body
{
    "action": "draft",
    "id": "",
    "booking_unit_id": "a0x0Y000002UIDF",
    "account_id" : "",
    "expected_move_out_date": "2020-07-29",
    "move_out_type": "Using moving company",   
    "moving_company_name" : "ABC",
    "moving_contractor_name" : "ABC contractor",
    "moving_contractor_phone" : "021321",
    "reason_to_move_out" : "test"
}
*/