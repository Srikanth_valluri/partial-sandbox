@RestResource(urlMapping='/api_inquiry/*')
global with sharing class API_Inquiry {
    
    
    @HttpGet
    global static void doGet() {
        RestRequest request = RestContext.request;
        
        String recordType = request.requestURI.substring(request.requestURI.lastIndexOf('/' ) + 1);
        String queryOffsetString = RestContext.request.params.get('offset');
        String queryLimitString = RestContext.request.params.get('limit');
        String sortBy = RestContext.request.params.get('sortBy');
        String sortType = RestContext.request.params.get('sortType');
        
        String whereClause = RestContext.request.params.get('whereClause');
        system.debug(recordType);
        Integer queryOffset = 0;
        Integer queryLimit = 15;
        
        if (!String.isBlank(queryOffsetString)) {
            queryOffset = Integer.valueOf(queryOffsetString);
        }
        
        if (!String.isBlank(queryLimitString)) {
            queryLimit = Integer.valueOf(queryLimitString);
        }
        List<Inquiry__c> inquiryList = getInquiryList(queryOffset, queryLimit, sortBy, sortType, whereClause, recordType);
        
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(inquiryList));        
    }
    
    global static List<Inquiry__c> getInquiryList(Integer queryOffset, Integer queryLimit, String sortBy, String sortType, String whereClause, String recordType) {
        if (String.isNotBlank(whereClause)) {
            whereClause = ' AND ' + whereClause;
        } else {
            whereClause = '';
        }
        if (String.isBlank(recordType) || recordType =='api_inquiry') {
            recordType = ''; 
        } else {
            recordType = ' AND RecordType.DeveloperName = \'' + recordType + '\'';  
        }
        system.debug(recordType);
        if (String.isBlank(sortBy)) {
            sortBy = 'Name';
        }
        
        if (String.isBlank(sortType)) {
            sortType = 'ASC';
        }
        String inquiryQuery = SOQLHelper.getObjectQuery('Inquiry__c');
        inquiryQuery += ' WHERE Id != null ';
        inquiryQuery += recordType;
        inquiryQuery += whereClause;
        inquiryQuery += ' ORDER BY ' + sortBy + ' ' + sortType;
        inquiryQuery += ' LIMIT :queryLimit ';
        inquiryQuery += ' OFFSET :queryOffset';
        system.debug(inquiryQuery);
        List<Inquiry__c> inquiryList = Database.query(inquiryQuery);
        return inquiryList;
    }
    
    @HttpPost
    global static void doPost(String recordTypeDeveloperName, Inquiry__c newInquiry) {        
        Integer statusCode = 200;
        
        if (Schema.SObjectType.Inquiry__c.getRecordTypeInfosByDeveloperName().get(recordTypeDeveloperName) != null) {
            newInquiry.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByDeveloperName().get(recordTypeDeveloperName).getRecordTypeId();
        }        
        
        Database.SaveResult saveResult = Database.insert(newInquiry);
        
        NewInquiryResponseDTO responseDTO = new NewInquiryResponseDTO();
        
        if (saveResult.isSuccess()) {
            statusCode = 200;
            
            responseDTO.success = true;
            responseDTO.inquiryId = saveResult.getId();
            try {
                responseDTO.IQNumber = [SELECT Name FROM Inquiry__c WHERE ID =: saveResult.getId() LIMIT 1].Name;
            } catch (Exception e) {}
            System.debug('Successfully inserted cil. cil ID: ' + saveResult.getId());
        } else {
            statusCode = 500;
            
            for(Database.Error err : saveResult.getErrors()) {
                System.debug('The following error has occurred.');                    
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug('cil fields that affected this error: ' + err.getFields());
                
                responseDTO.success = false;
                responseDTO.errors.add(err.getMessage());
                
            }
        }
        
        RestResponse response = RestContext.response;
        response.statusCode = statusCode;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(responseDTO));
    }
    
    global class NewInquiryResponseDTO {
        public String inquiryId {get; set;}
        public String IQNumber {get; set;}
        public boolean success {get; set;}
        public List<String> errors {get; set;}
        
        global NewInquiryResponseDTO() {
            inquiryId = '';
            success = false;
            errors = new List<String>();
        }
    }
}