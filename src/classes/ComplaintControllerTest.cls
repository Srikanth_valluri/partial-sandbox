/***********************************************************************************
* Description - Test class developed for ComplaintController
*
* Version            Date            Author            Description
* 1.0                12/12/17        Naresh (accely)           Initial Draft
**************************************************************************************/
@isTest(SeeAllData=false)
public class ComplaintControllerTest{

    @testsetup
    public static void TestData(){

        Id RecordTypeIdCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        Account  AccData=TestDataFactory_CRM.createPersonAccount();
        Insert AccData;
        System.assert(AccData!=null);


        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);

        /*Account Acc = TestDataFactory_CRM.createPersonAccount();
        insert Acc;
        System.assert(Acc != null);*/

        List<Booking__c> Booking =  TestDataFactory_CRM.createBookingForAccount(AccData.Id,Sr.Id,2);
        insert Booking;
        System.assert(Booking != null);

        List<Booking_Unit__c> BU =  TestDataFactory_CRM.createBookingUnits(Booking,2);
        insert BU;
        System.assert(BU != null);

        Case CaseData=TestDataFactory_CRM.createCase(AccData.Id,RecordTypeIdCase);
        CaseData.Booking_Unit__c  = BU[0].Id;
        Insert CaseData;
        System.assert(CaseData!=null);

        Attachment AttachData = new Attachment();
        AttachData.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        AttachData.body=bodyBlob;
        AttachData.parentId=CaseData.Id;
        AttachData.ContentType='application/msword';
        AttachData.IsPrivate=false;
        AttachData.Description='Test';
        Insert AttachData;
        System.assert(AttachData!=null);
    }

    public static testmethod void Test_AllContructors(){
        test.StartTest();
        ComplaintController instance = new ComplaintController();
        instance.bookingUnitList = null;
        instance.bookingUnitDetails = null;
        instance.bookingUnitDetailsFromIPMS = null;
        instance.accountsList = null;
        instance.selectedAcc =  'selectedAcc';
        instance.selectedBookingUnitId =  'Test';
        instance.strSelectedCategory =  'Test';
        instance.complaintFormURL =  'Test';
        instance.complaintFormURL =  'Test';
        instance.lstCategories = null ;
        instance.lstExistingCase = null ;
        instance.complaintCaseObj = null;
        instance.complaintFormURL = 'null';
        instance.crfAttachmentBody = 'Test';
        instance.crfAttachmentName = 'Test.pdf';
        instance.crfAttachmentUploaded = false;
        instance.attachment1Body = 'Test';
        instance.attachment1Name = 'Test.pdf';
        instance.attachment1Uploaded = false;
        instance.attachment2Body = 'Test';
        instance.attachment2Name = 'Test.pdf';
        instance.attachment2Uploaded = false;
        instance.caseNumber = '12456';

        test.StopTest();
    }

    public static testmethod void getTest(){
      test.startTest();
      ComplaintController obj = new ComplaintController();
      Booking_Unit__c bu = getBU();
      system.assert(bu != null);
      ApexPages.currentPage().getParameters().put('bookingUnitId', bu.id);
      obj.getUpdatedUnitDetails();
      test.StopTest();
    }

    public static testmethod void getUnitListMethod(){
        test.startTest();
        ComplaintController obj = new ComplaintController();
        Account acc = getAccount();
        obj.selectedAcc = acc.Id;
        Case objCase = getCase();
        ApexPages.currentPage().getParameters().put('CaseId',objCase.id);
        obj.getUnitListMethod();
        test.StopTest();
    }

    public static testmethod void getUnitListMethod1(){
        test.startTest();
        ComplaintController obj = new ComplaintController();
        obj.getUnitListMethod();
        test.StopTest();
    }

    public static testmethod void getUnitListMethod2(){
        test.startTest();
        ComplaintController obj = new ComplaintController();
        obj.isError = false;
        obj.getUnitListMethod();
        test.StopTest();
    }

    public static testmethod void complaintSubmitCase(){
        test.startTest();
        ComplaintController obj = new ComplaintController();
        //obj.init();
        Account acc = getAccount();
        obj.selectedAcc = acc.Id;
        obj.strSRType = 'Complaint';
        obj.getName();
        PageReference page = obj.complaintSubmitCase();
        test.StopTest();
    }

    public static testmethod void getUnits(){
        test.startTest();
        ComplaintController obj = new ComplaintController();
        Account acc = getAccount();
        obj.selectedAcc = acc.Id;
        obj.getUnits();
        test.StopTest();
    }
    public static testmethod void getCustomerFlagsFromIPMS1(){
        test.startTest();
        ComplaintController obj = new ComplaintController();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );        
        Account acc = getAccount();
        obj.selectedAcc = acc.Id;
        obj.getCustomerFlagsFromIPMS();
        test.StopTest();
    }

    public static testmethod void getCustomerFlagsFromIPMS2(){
        test.startTest();
        ComplaintController obj = new ComplaintController();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(2) );        
        Account acc = getAccount();
        obj.selectedAcc = acc.Id;
        obj.getCustomerFlagsFromIPMS();
        test.StopTest();
    }

    public static testmethod void getCustomerFlagsFromIPMS3(){
        test.startTest();
        ComplaintController obj = new ComplaintController();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(3) );        
        Account acc = getAccount();
        obj.selectedAcc = acc.Id;
        obj.getCustomerFlagsFromIPMS();
        test.StopTest();
    }

    public static testmethod void getComplaintSOAURL1(){
        test.startTest();
        ComplaintController obj = new ComplaintController();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );
        Booking_Unit__c bu = getBU();
        obj.bookingUnitDetails = bu;
        obj.complaintCaseObj = getCase();
        obj.crfAttachmentBody='crfAttachmentBody';
        obj.crfAttachmentName='crfAttachmentName.pdf';
        obj.attachment1Body='crfAttachmentBody';
        obj.attachment1Name='crfAttachmentName.pdf';
        obj.getComplaintSOAURL();
        test.StopTest();
    }

    public static testmethod void getComplaintSOAURL2(){
        test.startTest();
        ComplaintController obj = new ComplaintController();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(2) );
        Booking_Unit__c bu = getBU();
        obj.bookingUnitDetails = bu;
        obj.complaintCaseObj = getCase();
        obj.crfAttachmentBody='crfAttachmentBody';
        obj.crfAttachmentName='crfAttachmentName.pdf';
        obj.attachment1Body='crfAttachmentBody';
        obj.attachment1Name='crfAttachmentName.pdf';
        obj.getComplaintSOAURL();
        test.StopTest();
    }

    public static testmethod void getComplaintSOAURL3(){
        test.startTest();
        ComplaintController obj = new ComplaintController();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(3) );
        Booking_Unit__c bu = getBU();
        obj.bookingUnitDetails = bu;
        obj.complaintCaseObj = getCase();
        obj.crfAttachmentBody='crfAttachmentBody';
        obj.crfAttachmentName='crfAttachmentName.pdf';
        obj.attachment1Body='crfAttachmentBody';
        obj.attachment1Name='crfAttachmentName.pdf';
        obj.getComplaintSOAURL();
        test.StopTest();
    }

    public static testmethod void saveComplaintCase(){
        test.startTest();
        ComplaintController obj = new ComplaintController();
        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(3) );

        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
        MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response_x = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
        response_x.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-12345--Test1.pdf Processed] PK Value#:2-2-005058 Additionalfile","PARAM_ID":"IPMS-12345-Test1.pdf","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-12345-Test1.pdf"}],"message":"Process Completed Returning 2 Response Message(s)...","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());

        Blob blob1=Blob.valueOf('Test Data');
        obj.crfAttachmentBody=EncodingUtil.base64Encode(blob1);
        obj.crfAttachmentName='crfAttachmentName.pdf';

        Booking_Unit__c bu = getBU();
        obj.bookingUnitDetails = bu;
        obj.complaintCaseObj = getCase();

        obj.attachment1Body=EncodingUtil.base64Encode(blob1);
        obj.attachment1Name='crfAttachmentName.pdf';

        obj.attachment2Body=EncodingUtil.base64Encode(blob1);
        obj.attachment2Name='crfAttachmentName.pdf';

        obj.saveComplaintCase();
        test.StopTest();
    }


    public static testmethod void getComplaintCRF(){
        test.startTest();
        ComplaintController obj = new ComplaintController();

        SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateCRFService.GetCustomerRequestFormResponse_element>();
        GenerateCRFService.GetCustomerRequestFormResponse_element response1 = new GenerateCRFService.GetCustomerRequestFormResponse_element();
        response1.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=403501 and Request Id :40815712 ...","ATTRIBUTE3":"403501","ATTRIBUTE2":"40815712","ATTRIBUTE1":"https://sftest.deeprootsurface.com/docs/e/40815712_74428_CRF.pdf","PARAM_ID":"74428"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );

        Booking_Unit__c bu = getBU();
        obj.bookingUnitDetails = bu;
        obj.complaintCaseObj = getCase();

        obj.getComplaintCRF();
        test.StopTest();
    }

    public static testmethod void getComplaintCRF2(){
        test.startTest();
        ComplaintController obj = new ComplaintController();

        SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateCRFService.GetCustomerRequestFormResponse_element>();
        GenerateCRFService.GetCustomerRequestFormResponse_element response1 = new GenerateCRFService.GetCustomerRequestFormResponse_element();
        response1.return_x = '{"data":[{"PROC_STATUS":"E","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=403501 and Request Id :40815712 ...","ATTRIBUTE3":"403501","ATTRIBUTE2":"40815712","ATTRIBUTE1":"https://sftest.deeprootsurface.com/docs/e/40815712_74428_CRF.pdf","PARAM_ID":"74428"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );

        Booking_Unit__c bu = getBU();
        obj.bookingUnitDetails = bu;
        obj.complaintCaseObj = getCase();

        obj.getComplaintCRF();
        test.StopTest();
    }


    // To get Account Records
    public static Account getAccount(){
        return [SELECT id,Name,Name_Arabic__c,Country__c,Country_Arabic__c,Party_ID__c,Address_Line_1_Arabic__c,Address_Line_2__c,
        Address_Line_2_Arabic__c,Address_Line_3__c,Address_Line_3_Arabic__c,Address_Line_4__c,Address_Line_4_Arabic__c,City__c,
        City_Arabic__c,State__c,State_Arabic__c,Postal_Code_Arabic__c,Email__c,Mobile_Country_Code__c,Mobile_Phone_Encrypt__c,
        Home_Phone_Country_Code__c,Home_Phone_Encrypt__c,Work_Phone_Country_Code__c,Other_Phone_Encrypt__c,Middle_Name__c,
        Middle_Name_Arabic__c,Last_Name__c,Last_Name_Arabic__c,Country__pc,Country_Arabic__pc,Address_Line_1__pc,Address_Line_2__pc,
        Address_Line_3__pc,Address_Line_4__pc,CR_Registration_Expiry_Date__c,Address_Line_1_Arabic__pc,Address_Line_2_Arabic__pc,
        Address_Line_3_Arabic__pc,Address_Line_4_Arabic__pc,City__pc,City_Arabic__pc,BillingPostalCode,Email__pc,Mobile_Country_Code__pc,
        Mobile_Phone_Encrypt__pc,Passport_Number__pc,Passport_Expiry_Date__pc,Passport_Issue_Place__pc,Passport_Issue_Place_Arabic__pc,
        Nationality__pc,Nationality_Arabic__pc,Nationality__c,Nationality_Arabic__c,Title_Arabic__pc,FirstName,First_Name_Arabic__pc,
        MiddleName,Middle_Name_Arabic__pc,LastName,Last_Name_Arabic__pc,IsPersonAccount FROM Account WHERE FirstName='Test FirstName'];
    }

    // To get Case Records    
    public static Case getCase(){
        return [SELECT id,Customer_First_Name__c,Booking_Unit__c,AccountId,RecordType.Name,Customer_First_Name_Arabic__c,Country__c,Country_Arabic__c,
        Address__c,Address_Arabic_1__c,Address_2__c,Address_Arabic_2__c,Address_3__c,Address_Arabic_3__c,Address_4__c,Address_Arabic_4__c,City__c,
        City_Arabic__c,State__c,State_Arabic__c,Postal_Code_Arabic__c,Contact_Email__c,Mobile_Country_Code__c,Contact_Mobile__c,
        Home_Phone_Country_Code__c,Home_Phone__c,Work_Phone_Country_Code__c,Work_Phone__c,Customer_Middle_Name__c,Customer_Middle_Name_Arabic__c,
        Customer_Last_Name__c,Customer_Last_Name_Arabic__c,CRF_File_URL__c,Passport_Issue_Date__c,Postal_Code__c,New_CR__c,Passport_Issue_Place__c,
        Passport_Issue_Place_Arabic__c,Changed_Nationality__c,POA_File_URL__c,Changed_Nationality_Arabic__c,
        Title__c,Title_Arabic__c,Additional_Doc_File_URL__c,OD_File_URL__c FROM Case WHERE Credit_Note_Amount__c=5000];
    }


    public static Booking_Unit__c getBU(){
        return [select id,Registration_ID__c from Booking_Unit__c where Unit_Selling_Price_AED__c = 100 LIMIT 1];
    }
}