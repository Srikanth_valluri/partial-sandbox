/****************************************************************************************************
* Name          : AppointmentHandlerTest                                                            *
* Description   : Developed test class for class AppointmentHandler                                 *
* Created Date  : 05-02-2019                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE        COMMENTS                                                    *
* 1.0                       06/02/2018  Initial Draft.                                              *
****************************************************************************************************/
@isTest

public class AppointmentHandlerTest {
    @testSetup()
    private static  void testData(){
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
    }
    
    @isTest
    public static void test1() {
        //Create account
        Account objAccount = createAccount();
        System.debug('test account = ' + objAccount);
        
        //Create Service Request
        NSIBPM__Service_Request__c objDealSr = createDealSR();

        //Create Booking
        Booking__c objBooking = createBooking(objAccount.Id, objDealSr.Id);

        //Create Booking Unit
        Booking_Unit__c objBU = createBookingUnit(objBooking.Id,'test Hod');

        //Create User
        User user1 = createUser('standardusertesttest1@testorg.com');
        User user2 = createUser('standardusertesttest2@testorg.com');
        createAppointment(user2.id);
        //create calling list
        Calling_List__c objCL = createCallingList(objAccount.Id, objBU.Id);
        objcl.Assigned_CRE__c = user1.Id;
        insert objcl;
        system.debug('insert objcl == ' + objcl);
        objcl.Assigned_CRE__c = user2.Id;
        Test.startTest();
        update objcl;
        Test.stopTest();
        system.debug('update objcl == ' + objcl);
    }
    
    static Account createAccount() {
         Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        //Create Customer record
        Account objAcc = new Account(FirstName='Test FirstName'
                                    , LastName='Test LastName'
                                    , Email__pc = 'test123@tmail.com'
                                    , recordTypeId = personAccRTId);
        insert objAcc;
        return objAcc;
    }

    Static NSIBPM__Service_Request__c createDealSR() {
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
        return objSR;
    }

    Static Booking__c createBooking(Id accId, Id SRId) {
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = accId;
        objBooking.Deal_SR__c = SRId;
        insert objBooking;
        return objBooking;

    }

    static Booking_Unit__c createBookingUnit(Id bookingId, String HodName) {
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/001';
        objBookingUnit.Booking__c  = bookingId;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Hod_name__c = HodName;
        insert objBookingUnit;
        System.debug('test objBookingUnit = ' + objBookingUnit);
        return objBookingUnit;
    }
    
    static user createUser(String userName) {
      Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standardusertest@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName=userName); 
        insert u;
        return u;
    }
    
    static Calling_List__c createCallingList(Id accId, Id BUId) {

        Id appointmentCallingRecTypeId =
            Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get(
            'Appointment Scheduling').RecordTypeId;
        System.debug('test FMCollectionRecordTypeId = ' + appointmentCallingRecTypeId);
        Date aptDate = Date.newInstance(2018, 2, 10);
        Datetime startT = Datetime.newInstance(2018, 2, 10, 9, 00, 00);
        Datetime endT = Datetime.newInstance(2018, 2, 10, 9, 30, 00);
        Calling_List__c callListObj = new Calling_List__c(Account__c = accId
                                                        , Booking_Unit__c = BUId
                                                        , recordTypeId = appointmentCallingRecTypeId
                                                        , Calling_List_Type__c = 'DP Calling'
                                                        , Calling_List_Status__c = 'New'
                                                        , Mobile_Phone__c = '8877665542'
                                                        , Email__c = '1test1@gmail.com'
                                                        , Customer_Name__c = 'test customer'
                                                        , Unit_Name__c = 'JNU/ABC/001'
                                                        , Service_Type__c = 'Handover'
                                                        , Sub_Purpose__c = 'Documentation'
                                                        , Appointment_Date__c = aptDate
                                                        , Appointment_Start_DateTime__c = startT
                                                        , Appointment_End_DateTime__c = endT);
        //insert callListObj;
        return callListObj;
    }
    
    static void createAppointment(Id assignedCRE) {
        Date aptDate = Date.newInstance(2018, 2, 09);
        Date aptDateEnd = Date.newInstance(2018, 2, 12);
        Datetime endT = Datetime.newInstance(2018, 2, 10, 9, 30, 00);
        Appointment__c appointment = new Appointment__c(Assigned_CRE__c = assignedCRE
                                                        , Processes__c = 'Handover'
                                                        , Sub_Processes__c = 'Documentation'
                                                        , Appointment_Date__c = aptDate
                                                        , Appointment_End_Date__c = aptDateEnd
                                                        , Slots__c = '09:00 - 09:30'
        );
        
        insert appointment;
        system.debug('appointment == ' + [SELECT Assigned_CRE__c, Processes__c 
                                                        , Sub_Processes__c 
                                                        , Appointment_Date__c
                                                        , Appointment_End_Date__c 
                                                        , Slots__c
                                                    FROM Appointment__c]);
    }
}