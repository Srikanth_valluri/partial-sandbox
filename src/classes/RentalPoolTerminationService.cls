/*
* Description - Class used as a service class for main RentalPoolTerminationController class.
*
* Version            Date            Author            Description
* 1.0              			         Ashish           Initial Draft
*/

public without sharing class RentalPoolTerminationService {
	
	private Id caseId ;
	
	public RentalPoolTerminationService( ApexPages.StandardController sc ) {
		caseId = sc.getId();
	}
    
    public PageReference generateLetterOfTermination() {
    	if( caseId != null ) {
	    	Case objCase = RentalPoolTerminationUtility.getCaseDetails( caseId );
	    	if( objCase != null ) {
	    		if( objCase.RecordTypeId != null && 
	    			objCase.RecordType.DeveloperName.equalsIgnoreCase('Rental_Pool_Termination') && 
	    			String.isNotBlank( objCase.Status ) && 
	    			objCase.Status.equalsIgnoreCase('HE Approved') &&
	    			String.isNotBlank( objCase.Rental_Pool_Termination_Status__c ) && 
	    			objCase.Rental_Pool_Termination_Status__c.equalsIgnoreCase('Approved from Hospitality Executive') &&
	    			objCase.Final_Settlement_Amount__c != null && 
	    			objCase.Final_Termination_Date__c != null ) {
	    			
	    			//Generate LOT
	    			String strDocumentURL = fetchLetterOfTermination( objCase );
	    			if( String.isNotBlank( strDocumentURL ) ) {
		    			//Insert in Custom Attachment
		    			SR_Attachments__c objCustAttach = new SR_Attachments__c();
			            objCustAttach.Account__c = objCase.AccountId ;
			            objCustAttach.Attachment_URL__c = strDocumentURL ;
			            objCustAttach.Booking_Unit__c = objCase.Booking_Unit__c ;
			            objCustAttach.Case__c = objCase.Id ;
		                objCustAttach.Type__c = 'Letter Of Termination' ;
		                objCustAttach.Name = 'Letter Of Termination' ;
		                objCustAttach.isValid__c = true ;
			            try {
			            	insert objCustAttach ;
			            	
			            	//Update Case Status.
			            	objCase.Status = 'Letter of Termination Issued';
			            	objCase.Rental_Pool_Termination_Status__c = 'Letter of Termination Issued';
			            	update objCase ;
			            	
			            	//Close the corresponding task of generating the LOT.
			            	Task objTask = RentalPoolTerminationUtility.getTerminationTaskOfCRE( objCase.Id );
			            	if( objTask != null ) {
			            		objTask.Status = 'Completed';
			            		update objTask;
			            	}
			            	
			            	//Add a new task for dispatching the LOT.
			            	Task objNewTask = RentalPoolTerminationHelper.createTaskObject( 'CRE', 'Normal', 'Rental Pool Termination', 'Pending',
    								'Displatch Letter of Termination', objCase, '' );
    						insert objNewTask ;
			            	
			            	Pagereference pg = new Pagereference( strDocumentURL );
			            	pg.setRedirect(true);
                			return pg;
			            }
			            catch( Exception e ) {
			            	ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, e.getMessage() ) );
			            }
	    			}
	    			else {
	    				ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR,'No response from server.') );
	    			}
	    		}
	    		else if( String.isNotBlank( objCase.Status ) &&
	    				 objCase.Status.equalsIgnoreCase( 'Letter of Termination Issued' ) ) {
	    			ApexPages.addmessage( new ApexPages.message( ApexPages.severity.INFO,'Letter of Termination already generated. Please find it in attached documents.') );
	    		}
	    		else {
		    		ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR,'Case not eligible for Letter of Termination.') );
		    		ApexPages.addmessage( new ApexPages.message( ApexPages.severity.INFO,'Eligiblity Citeria : \n 1) SR must be of Rental Pool Termination. \n 2) Status must be "HE Approved". \n 3) Rental Pool Termination Status must be "HE Approved". \n 4) Final Settlement Amount must be there. \n 5) Final Termination Date must be there.') );
		    	}
	    	}
	    	else {
	    		ApexPages.addmessage( new ApexPages.message( ApexPages.severity.INFO,'Case not found.') );
	    	}
    	}
    	return null;
    }
     
    public static String fetchFMCharges( Booking_Unit__c objUnit ) {
    	if( objUnit != null ) {
    		String strDueResponse = assignmentEndpoints.fetchAssignmentDues( objUnit );
    		if( String.isNotBlank( strDueResponse ) ) {
    			map<String,Object> mapDeserializeDue = (map<String,Object>)JSON.deserializeUntyped( strDueResponse );
    			if( mapDeserializeDue.containsKey( Label.FM_Charges ) ) {
    				return String.valueof( mapDeserializeDue.get( Label.FM_Charges ) );
    			}
    		}
    	}
    	return '';
    }
    
    public static String generateCustomerRequestForm( Booking_Unit__c objUnit, UnitDetailsService.BookinUnitDetailsWrapper objUnitdetails ) {
    	AOPTDocumentGenerationXsd.DocGenDTO objAttributes = RentalPoolTerminationHelper.createAttributesWrapper( objUnit, objUnitdetails );
    	AOPTDocumentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint objGenerateLetter = new AOPTDocumentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint();
        objGenerateLetter.timeout_x = 120000;
        String strResponse = objGenerateLetter.DocGeneration( 'CRF_RPTermination_New' , objAttributes );
        return strResponse;
    }
    
    public static String fetchLetterOfTermination( Case objCase ) {
    	AOPTDocumentGenerationXsd.DocGenDTO objAttributes = RentalPoolTerminationHelper.createWrapperObject( objCase, '' );
    	AOPTDocumentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint objGenerateLetter = new AOPTDocumentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint();
        objGenerateLetter.timeout_x = 120000;
        String strResponse = objGenerateLetter.DocGeneration( 'Draft_termination_letter' , objAttributes );
        return strResponse;
    }
    
}