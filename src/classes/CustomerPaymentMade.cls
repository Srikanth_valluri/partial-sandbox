public with sharing class CustomerPaymentMade {
    
    public static String getCustomerPaymentMade(String strRegId) {
    	CustomerPaymentMade.PaymentWrapper wrapperObj = new CustomerPaymentMade.PaymentWrapper();
    	String strResponse = null;
    	if( String.isNotBlank( strRegId ) ) {
    		PaymentMade1.CustomerPaymentHttpSoap11Endpoint objPayment = new PaymentMade1.CustomerPaymentHttpSoap11Endpoint();
    		objPayment.timeout_x = 120000;

            PaymentMade2.APPSXXDC_PROCESS_SERX1794747X2X4 regTerms = new PaymentMade2.APPSXXDC_PROCESS_SERX1794747X2X4();
            regTerms.ATTRIBUTE1= '';
			regTerms.ATTRIBUTE10= '';
			regTerms.ATTRIBUTE11= '';
			regTerms.ATTRIBUTE12= '';
			regTerms.ATTRIBUTE13= '';
			regTerms.ATTRIBUTE14= '';
			regTerms.ATTRIBUTE15= '';
			regTerms.ATTRIBUTE16= '';
			regTerms.ATTRIBUTE17= '';
			regTerms.ATTRIBUTE18= '';
			regTerms.ATTRIBUTE19= '';
			regTerms.ATTRIBUTE2= '';
			regTerms.ATTRIBUTE20= '';
			regTerms.ATTRIBUTE21= '';
			regTerms.ATTRIBUTE22= '';
			regTerms.ATTRIBUTE23= '';
			regTerms.ATTRIBUTE24= '';
			regTerms.ATTRIBUTE25= '';
			regTerms.ATTRIBUTE26= '';
			regTerms.ATTRIBUTE27= '';
			regTerms.ATTRIBUTE28= '';
			regTerms.ATTRIBUTE29= '';
			regTerms.ATTRIBUTE3= '';
			regTerms.ATTRIBUTE30= '';
			regTerms.ATTRIBUTE31= '';
			regTerms.ATTRIBUTE32= '';
			regTerms.ATTRIBUTE33= '';
			regTerms.ATTRIBUTE34= '';
			regTerms.ATTRIBUTE35= '';
			regTerms.ATTRIBUTE36= '';
			regTerms.ATTRIBUTE37= '';
			regTerms.ATTRIBUTE38= '';
			regTerms.ATTRIBUTE39= '';
			regTerms.ATTRIBUTE4= '';
			regTerms.ATTRIBUTE41= '';
			regTerms.ATTRIBUTE42= '';
			regTerms.ATTRIBUTE43= '';
			regTerms.ATTRIBUTE44= '';
			regTerms.ATTRIBUTE45= '';
			regTerms.ATTRIBUTE46= '';
			regTerms.ATTRIBUTE47= '';
			regTerms.ATTRIBUTE48= '';
			regTerms.ATTRIBUTE49= '';
			regTerms.ATTRIBUTE5= '';
			regTerms.ATTRIBUTE50= '';
			regTerms.ATTRIBUTE6= '';
			regTerms.ATTRIBUTE7= '';
			regTerms.ATTRIBUTE8= '';
			regTerms.ATTRIBUTE9= '';
			regTerms.PARAM_ID= strRegId;

            try {
                strResponse = objPayment.customerPaymentMade('2-'+String.valueOf( Datetime.now().getTime()),'GET_CUSTOMER_PAYMENT_MADE','SFDC', regTerms);
                system.debug('== Payment strResponse ==  '+strResponse  );
            }
            catch( Exception e ) {
                system.debug(' Exception occured while fetching unit details ==  '+e.getMessage() );
                wrapperObj = null ;
                return strResponse ;
            }
    	}

        return strResponse;
    }

    public class PaymentWrapper{
        public String ATTRIBUTE1;
        public String ATTRIBUTE2;
        public String ATTRIBUTE3;
        public String ATTRIBUTE4;
        public String ATTRIBUTE5;
        public String ATTRIBUTE6;
        public String ATTRIBUTE7;
        public String ATTRIBUTE8;
        public String ATTRIBUTE9;
        public String ATTRIBUTE10;
    }
}