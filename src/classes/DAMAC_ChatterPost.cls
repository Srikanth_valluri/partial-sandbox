@RestResource(urlMapping='/ziwoChatterPOST/*')
global class DAMAC_ChatterPost {

    @HttpPOST
    global static void chatterPOST () {
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        String requestBody = request.requestbody.tostring();
        
        Map <String, Object> requestJSON = (Map <String, Object>) JSON.deserializeUntyped(requestBody);
        DAMAC_ziwo_listener_inbound.doChatterPost(String.valueOf (requestJSON.get ('userId')), String.valueOf (requestJSON.get ('agentMobilePhoneforSMS')),
                                        String.valueOf (requestJSON.get ('taskId')), String.valueOf (requestJSON.get ('direction')),
                                        String.valueOf (requestJSON.get ('type')));
    }
}