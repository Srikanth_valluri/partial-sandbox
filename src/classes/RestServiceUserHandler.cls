@RestResource(urlMapping='/DAMACUserFactory/*')
global class RestServiceUserHandler {
    @HttpPost
    global static void doPost(){
        
        RestContextHandler handler = new RestContextHandler(true);
        RestRequest req = handler.restRequest;
        System.debug('>>>>>requestURI>>>>>>>'+req.requestURI);
        String action = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        System.debug('>>>>>action>>>>>>>'+action);

        try{
            Map<String, string> params = handler.getRequestParams();
            System.debug('>>>>>params>>>>>>>'+params); 
            if(action.toLowerCase() == 'createuser')
                handler = createUser(params);
            
            handler.finalize();
        }
        catch (Exception exc) {
            system.debug('>>>>>>>>>>ERROR>>>>>>>>>>>' +exc.getmessage()+'>>>>>>LineNO>>>>>'+exc.getlinenumber()+'>>>>>>Action>>>>>>>'+action);
            handler.response.success = false;
            handler.response.statusCode = WebServiceResponse.STATUS_CODE_SYSTEM_ERROR;
            handler.response.message = exc.getMessage()+'-'+exc.getLineNumber();
            handler.finalize(exc);
        }
    }

    public static RestContextHandler createUser(map<string, string> params){
        RestContextHandler handler = new RestContextHandler(true);
        string userData = params.get('user');
        User userRec = (User) JSON.deserialize(userData, User.class);

        if(userRec.ManagerId != null){
            user manager = [Select id from User where  IPMS_Employee_ID__c =: userRec.ManagerId];
            userRec.ManagerId = manager.Id;
        }

        //userRec.Alias = (userRec.email).split('@')[0];
        userRec.UserRoleId = '00E1n000000TXoX';
        userRec.ProfileId = '00e0Y000000Qlkd'; 
        userRec.emailencodingkey = 'UTF-8';
        userRec.languagelocalekey = 'en_US';
        userRec.localesidkey = 'en_GB'; 
        userRec.timezonesidkey = 'Asia/Dubai';

        insert userRec;
        handler.response.success = true;
        handler.response.message = 'User created successfully';                 
        handler.response.data = userRec.Id;
        return handler;
    }
}