/************************************************************************************************
 * @Name              : DAMAC_Inventory_External_SearchAPI_Test
 * @Description       : Test Class for DAMAC_Inventory_External_SearchAPI
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         26/07/2020       Created
***********************************************************************************************/

@isTest
public class DAMAC_Inventory_External_SearchAPI_Test{

@isTest
    static void testAPI(){
        
        Marketing_Documents__c mark = new Marketing_Documents__c();
        mark.Name = 'ZADI';
        mark.Marketing_Plan__c = '';
        insert mark;
        Location__c loc = new Location__c();
        loc.Name = 'ZAD';
        loc.Location_ID__c = 'ZAD';
        insert loc;
        Inventory__c inv = new Inventory__c();
        inv.Marketing_Name_Doc__c = mark.Id;
        inv.Property_City__c = 'Dubai'; 
        inv.District__c = 'Dubai';
        inv.Unit_Location__c = loc.Id;
        insert inv;
        
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.params.put ('searchTerm', 'ZAD');
        request.requestUri ='/queryInventorySearchAPIPagination';
        request.httpMethod = 'GET';       
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Inventory_External_SearchAPI.getResults();
        
        request.params.put ('searchTerm', 'Avenue');
        request.requestUri ='/queryInventorySearchAPIPagination';
        request.httpMethod = 'GET';       
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Inventory_External_SearchAPI.getResults();
        Test.stopTest();
        
    }
    
    
    static void testAPI2(){
        
        Marketing_Documents__c mark = new Marketing_Documents__c();
        mark.Name = 'ZADI';
        mark.Marketing_Plan__c = '';
        insert mark;
        Location__c loc = new Location__c();
        loc.Name = 'ZAD';
        loc.Location_ID__c = 'ZAD';
        insert loc;
        Inventory__c inv = new Inventory__c();
        inv.Marketing_Name_Doc__c = mark.Id;
        inv.Property_City__c = ''; 
        inv.District__c = '';
        inv.Unit_Location__c = loc.Id;
        insert inv;
        
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.params.put ('searchTerm', 'ZAD');
        request.requestUri ='/queryInventorySearchAPIPagination';
        request.httpMethod = 'GET';       
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Inventory_External_SearchAPI.getResults();
        
        request.params.put ('searchTerm', 'Avenue');
        request.requestUri ='/queryInventorySearchAPIPagination';
        request.httpMethod = 'GET';       
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Inventory_External_SearchAPI.getResults();
        Test.stopTest();
        
    }

}