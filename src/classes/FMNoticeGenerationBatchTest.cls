@isTest
private class FMNoticeGenerationBatchTest{
    @isTest
    static void testMethod1(){
        Report_Types__c reportCS = new Report_Types__c();
        reportCS.Name = 'FM Quarterly Notice';
        reportCS.Services_Required__c = 'FM_SOA,FM_Invoice';
        insert reportCS;

        Additional_Document_URLs__c AddDocCS = new Additional_Document_URLs__c();
        AddDocCS.Name = 'Add Doc';
        AddDocCS.Document_URL__c = 'www.google.com';
        insert AddDocCS;
        
        Quarters__c quarterCS = new Quarters__c();
        quarterCS.Name = 'Quarter 1';
        quarterCS.Start_Date__c = system.today();
        quarterCS.End_Date__c = system.today().addDays(30);
        insert quarterCS;

        Email_Template__c etCS = new Email_Template__c();
        etCS.Name = 'EHO';
        insert etCS;

        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'LSB';
        loc.Additional_Document_Name__c = 'Add Doc';
        insert loc;

        Inventory__c objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        objInv.Building_Location__c = loc.Id;
        insert objInv;

        Account a = new Account();
        a.Name = 'Test Account';  
        a.party_ID__c = '1039032';
        a.Email__c = 'test@test.com';
        a.Primary_Language__c = 'English';
        a.Nationality__c = 'Omani';
        insert a;
        
        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = a.id;
        sr.RecordTypeId = RecType;
        insert sr;

        Booking__c  bk = new  Booking__c();
        bk.Account__c = a.id;
        bk.Deal_SR__c = sr.id;
        insert bk;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = bk.id;
        bu.Unit_Name__c = 'LSB/10/B1001'; 
        bu.Registration_Status__c = 'Agreement Cancellation Verified';
        bu.Registration_DateTime__c = System.now();
        bu.Registration_ID__c = '74655';
        bu.Inventory__c = objInv.Id;
        insert bu;

        string buID = String.valueOf(bu.id);
        Booking_Unit_Active_Status__c cs = new Booking_Unit_Active_Status__c();
        cs.Name = 'Agreement Cancellation Verified';
        cs.Status_Value__c = 'Agreement Cancellation Verified';
        insert cs;

        Map<String,String> dataParams = new Map<String,String>();
        dataParams.put('regID_BUID','64547-'+bu.id);
        dataParams.put('__service','FM_SOA');

        Service_Information__c siCs = new Service_Information__c();
        siCs.Name = 'FM_SOA';
        siCs.ClassName__c = 'FMSOA_WebServiceAdapter';
        insert siCs;
        
        Service_Information__c siC1 = new Service_Information__c();
        siC1.Name = 'FM_Invoice';
        siC1.ClassName__c = 'FMInvoice_WebServiceAdapter';
        insert siC1;

        List<String> BUIds = new List<String>();
        BUIds.add(buID);
        
        
        String TEST_URL = 'https://test.damacgroup.com/test.pdf';
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));
        
        
        FMNoticeGenerationBatch objClass = new FMNoticeGenerationBatch(BUIds, 'Quarter 1');
        Database.Executebatch(objClass);
        test.stopTest();
    }
}