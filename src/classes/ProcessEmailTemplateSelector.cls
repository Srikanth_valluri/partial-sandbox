/**
 * @File Name          : ProcessEmailTemplateSelector.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 12/18/2019, 12:29:30 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    7/10/2019, 2:26:58 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class ProcessEmailTemplateSelector {

    /*********************************************************************************
    * Method Name : getEmailTemplateName
    * Description : This method is used to get the name of the template base on Process name ,Nationality and Language
    * Return Type : String
    * Parameter(s): processName,nationality
    **********************************************************************************/ 
    public static String getEmailTemplateName(String processName, String nationality) {
        Process_Email_Template__mdt emt = [SELECT 
                                                Process_Name__c, 
                                                //Language__c, 
                                                Email_Template__c,
                                                Nationality__c
                                           FROM Process_Email_Template__mdt
                                           WHERE Process_Name__c =: processName
                                           AND  Nationality__c =: nationality
                                          // AND Language__c =:language
                                          limit 1
                                         ];

        System.debug('Email template Name is: hello' + emt.Email_Template__c);
        return emt.Email_Template__c;
    }

    /*********************************************************************************
    * Method Name : getEmailTemplateName
    * Description : This method is used to get the name of the template base on Process name ,Nationality and Language
    * Return Type : String
    * Parameter(s): processName,nationality
    **********************************************************************************/ 
    public static String getEmailTemplateNameForDpInvoice(String processName, String nationality, String milestoneType) {
        Process_Email_Template__mdt emt = [SELECT 
                                                Process_Name__c, 
                                                //Language__c, 
                                                Email_Template__c,
                                                Nationality__c,
                                                Milestone_Type__c
                                           FROM Process_Email_Template__mdt
                                           WHERE Process_Name__c =: processName
                                           AND  Nationality__c =: nationality
                                           AND Milestone_Type__c = :milestoneType
                                          // AND Language__c =:language
                                         ];

        System.debug('Email template Name is: ' + emt.Email_Template__c);
        return emt.Email_Template__c;
    }
    /*********************************************************************************
    * Method Name : fetchEmailTemplateRecord
    * Description : This method is used ot fetch the email template record based on the name of the template
    * Return Type : EmailTemplate
    * Parameter(s): emailTemplateName
    **********************************************************************************/ 
    public static EmailTemplate fetchEmailTemplateRecord(String emailTemplateName){
        List<EmailTemplate> emailTemplateObj = [SELECT 
                                                ID, 
                                                Subject, 
                                                Body, 
                                                name,
                                                HtmlValue, 
                                                TemplateType,
                                                BrandTemplateId
                                            FROM  EmailTemplate 
                                            WHERE name =: emailTemplateName limit 1];
        if(emailTemplateObj != NULL){
            return emailTemplateObj[0];
        }
        return NULL;
    }

}