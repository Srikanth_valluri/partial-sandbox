/**************************************************************************************************
pRATIKSHA NARVEKAR                                                             
**************************************************************************************************/
public with sharing class AgentPortalProfileController {
    /**************************************************************************************************
    Variables used in the class
    **************************************************************************************************/
     
    public List<Contact> amendments{set;get;}
    public List<Contact> amendments2{set;get;}
    public Account accountDetail{set;get;}
    public String regNumber {get; set;}
    public Date  certiDate {get; set;}
    public string registrationUpdatePageID{set;get;}
    public boolean isSRPending{set;get;}
    public Boolean showdetails{get;set;}
    public Boolean showdetailsNone{get;set;}
    public String serviceRequestID{set;get;}
    private static NSIBPM__Service_Request__c serviceRequest;
    public static boolean isError {get;set;}
    
    /**************************************************************************************************
    Method:         DamacCompanyProfileController
    Description:    Constructor executing model of the class 
    **************************************************************************************************/
    
    public AgentPortalProfileController () {
        try{
            isError = false;
            showdetails = false;
            showdetailsNone =false;
            Id accountId = UtilityQueryManager.getAccountId();
            /*if(null == accountId){
                accountId = apexpages.currentpage().getParameters().get('Accid');
            }*/
            List<User> userList = new List<User>();
            ID userID = userinfo.getUserId();
            ID ContactID;
            userList =[select id,ContactID from User where id=: userID Limit 1];
            ContactID = userList[0].ContactID;
            amendments = new List<Contact>();
            amendments2 = new List<Contact>();
            amendments2 = UtilityQueryManager.getAllContacts2(ContactID);
             System.debug('...amendments2...'+amendments2);
            for (Contact con : UtilityQueryManager.getAllContacts(accountId)) {
                 System.debug('...con.Status__c...'+con.Status__c);
                if (con.Status__c != 'Cancelled') {
                    amendments.add(con);
                }
            }
            //amendments = UtilityQueryManager.getAllContacts(accountId);
             List<Contact> listCon= [SELECT Id,
                                            Status__c,
                                            Owner__c,
                                            Authorised_Signatory__c,
                                            Portal_Administrator__c,
                                            Agent_Representative__c
                                       FROM Contact
                                      WHERE ID =: ContactID];
            for(Contact objCon :listCon) {
                if(objCon.Owner__c == true 
                    || objCon.Portal_Administrator__c == true
                    || objCon.Authorised_Signatory__c == true 
                ){

                    showdetails = true;
                    showdetailsNone =false;
                } else {
                    System.debug('...inside else....');
                    showdetails = false;
                    showdetailsNone = true;
                }
            }
            accountDetail = new Account();
            accountDetail = UtilityQueryManager.getCompanyProfileDetail(accountId);
            registrationUpdatePageID = UtilityQueryManager.getProcessFlowId(LABEL.Agent_Portal_Registration_Update_Page_Flow_Name);
            serviceRequest = UtilityQueryManager.checkPendingSR(accountId);
            
            if(null != serviceRequest){
                isSRPending = true;
                serviceRequestID = serviceRequest.Id;
            }
            Agent_Site__c[] AgSite = [SELECT Tax_Registration_Number__c,Registration_Certificate_Date__c 
                                        FROM Agent_Site__c 
                                       WHERE Agency__c=:accountId 
                                         AND Name = 'UAE'];
            System.debug('accountId----  '+accountId);
            System.debug('AgSite----  '+AgSite);
            if (AgSite.Size() > 0) {
                regNumber = AgSite[0].Tax_Registration_Number__c;
                certiDate = AgSite[0].Registration_Certificate_Date__c;
            }
        } catch(exception ex){
            isError = true;
            System.debug('Error Message In AgentPortalProfileController----  '+ex.getMessage());
            System.debug('Error @line In AgentPortalProfileController----  '+ex.getLineNumber());
        }
    }
    
    public static pagereference init(){
        Pagereference pg = null;
        if(isError){  
            System.debug('**INSIDE INIT**');
            Id accountId = apexpages.currentpage().getParameters().get('Accid');
            serviceRequest = UtilityQueryManager.checkPendingSR(accountId);
            if(null != serviceRequest)
              pg = new Pagereference('/'+serviceRequest.id);
        }
        return pg;
    }
}