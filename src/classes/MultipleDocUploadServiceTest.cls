/*
* Description - Test class developed for 'MultipleDocUploadService'
*
* Version            Date            Author            Description
* 1.0                20/11/17        Sid               Initial Draft
  1.1                21/11/17        Naresh(Accely)            Increased code Coverage from 10% 
*/

@isTest 
public class MultipleDocUploadServiceTest {
    
    static MultipleDocUploadService.AOPTHttpSoap11Endpoint   obj =  new MultipleDocUploadService.AOPTHttpSoap11Endpoint();
    static String resp ;
    /*
    public static testMethod void testGenerateWSDL(){
        
        MultipleDocUploadService.RegistrationDetails_element objRegDetails = 
            new MultipleDocUploadService.RegistrationDetails_element();
    
        MultipleDocUploadService.RegistrationDetailsResponse_element objResponseDetails = 
            new MultipleDocUploadService.RegistrationDetailsResponse_element();
    
        MultipleDocUploadService.DocumentAttachmentMultipleResponse_element objDocAttachMultiResponse = 
            new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
    
        MultipleDocUploadService.DocumentAttachment_element objDocAttachment = 
            new MultipleDocUploadService.DocumentAttachment_element();
    
        MultipleDocUploadService.PaymentPlanCreationResponse_element objPaymentPlanCreationResponse = 
            new MultipleDocUploadService.PaymentPlanCreationResponse_element();
    
        MultipleDocUploadService.getMilestonePaymentDetails_element objGetMilestonePaymentDetails = 
            new MultipleDocUploadService.getMilestonePaymentDetails_element();
    
        MultipleDocUploadService.PaymentPlanCreation_element objPaymentPlanCreation = 
            new MultipleDocUploadService.PaymentPlanCreation_element();
        
        MultipleDocUploadService.DocumentAttachmentMultiple_element objDocumentAttachmentMultiple_element = 
            new MultipleDocUploadService.DocumentAttachmentMultiple_element();
        
        MultipleDocUploadService.PaymentPlanHistory_element objPaymentPlanHistory_element = 
            new MultipleDocUploadService.PaymentPlanHistory_element();
        
        MultipleDocUploadService.PaymentPlanReversalResponse_element objPaymentPlanReversalResponse_element = 
            new MultipleDocUploadService.PaymentPlanReversalResponse_element();
        
        MultipleDocUploadService.EarlyHandoverPaymentPlanCreation_element objEarlyHandoverPaymentPlanCreation_element = 
            new MultipleDocUploadService.EarlyHandoverPaymentPlanCreation_element();
        
        MultipleDocUploadService.DocumentAttachmentResponse_element objDocumentAttachmentResponse_element = 
            new MultipleDocUploadService.DocumentAttachmentResponse_element();
        
        MultipleDocUploadService.PaymentPlanHistoryResponse_element objPaymentPlanHistoryResponse_element = 
            new MultipleDocUploadService.PaymentPlanHistoryResponse_element();
        
        MultipleDocUploadService.getMasterMilestone_element objGetMasterMilestone_element = 
            new MultipleDocUploadService.getMasterMilestone_element();

        MultipleDocUploadService.getMasterMilestoneResponse_element objGetMasterMilestoneResponse_element = 
            new MultipleDocUploadService.getMasterMilestoneResponse_element();

        MultipleDocUploadService.PaymentPlanReversalCurrent_element objPaymentPlanReversalCurrent_element = 
            new MultipleDocUploadService.PaymentPlanReversalCurrent_element();
        
        MultipleDocUploadService.PaymentPlanReversal_element objPaymentPlanReversal_element = 
            new MultipleDocUploadService.PaymentPlanReversal_element();        
        
        MultipleDocUploadService.getMilestonePaymentDetailsResponse_element objGetMilestonePaymentDetailsResponse_element = 
            new MultipleDocUploadService.getMilestonePaymentDetailsResponse_element();
        
        MultipleDocUploadService.EarlyHandoverPaymentPlanCreationResponse_element objEarlyHandoverPaymentPlanCreationResponse_element = 
            new MultipleDocUploadService.EarlyHandoverPaymentPlanCreationResponse_element();
        
        MultipleDocUploadService.PaymentPlanReversalCurrentResponse_element objPaymentPlanReversalCurrentResponse_element = 
            new MultipleDocUploadService.PaymentPlanReversalCurrentResponse_element();               
    }
    */
    
    // To Cover PaymentPlanCreation()
    public static testmethod void PaymentPlanCreationTest(){
        
        Test.startTest();
        List<paymentPlanCreationXxdcAoptPkgWsPMult.APPSXXDC_AOPT_PKG_WSX1843128X6X5> regTerms =  new List<paymentPlanCreationXxdcAoptPkgWsPMult.APPSXXDC_AOPT_PKG_WSX1843128X6X5>();
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.PaymentPlanCreationResponse_element>();
        MultipleDocUploadService.PaymentPlanCreationResponse_element response_x = new MultipleDocUploadService.PaymentPlanCreationResponse_element();
        response_x.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        obj.PaymentPlanCreation('P_REGISTRATION_ID','P_SR_NUMBER','P_SR_TYPE',regTerms);
        Test.stopTest();
        
    }
    
        // To Cover EarlyHandoverPaymentPlanCreation()
        public static testmethod void EarlyHandoverPaymentPlanCreationTest(){
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.EarlyHandoverPaymentPlanCreationResponse_element >();
        MultipleDocUploadService.EarlyHandoverPaymentPlanCreationResponse_element  response_x = new MultipleDocUploadService.EarlyHandoverPaymentPlanCreationResponse_element ();
        response_x.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.EarlyHandoverPaymentPlanCreation('P_REGISTRATION_ID','P_SR_NUMBER','P_SR_TYPE','REGISTRATION_ID','INSTALLMENT',
                                               'DESCRIPTION','PAYMENT_DATE','EXPECTED_DATE','MILESTONE_EVENT','PERCENT_VALUE',
                                               'TRANSFER_AR_INTER_FLAG','PAYMENT_AMOUNT');
                                               
        System.assertEquals(resp, 'S');                                       
        Test.stopTest();
        
    }
    
    
    // To Cover DocumentAttachmentMultiple()
        public static testmethod void DocumentAttachmentMultipleTest(){
        
        Test.startTest();
        List<beanComXsdMultipleDocUpload.DocUploadDTO> regTerms = new List<beanComXsdMultipleDocUpload.DocUploadDTO>();
        
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentMultipleResponse_element  >();
        MultipleDocUploadService.DocumentAttachmentMultipleResponse_element   response_x = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element  ();
        response_x.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.DocumentAttachmentMultiple('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM',regTerms);
        System.assertEquals(resp, 'S');
        Test.stopTest();
        
    }
    
    
    
        // To Cover PaymentPlanReversal()
        public static testmethod void PaymentPlanReversalTest(){
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.PaymentPlanReversalResponse_element   >();
        MultipleDocUploadService.PaymentPlanReversalResponse_element response_x = new MultipleDocUploadService.PaymentPlanReversalResponse_element   ();
        response_x.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.PaymentPlanReversal('P_REGISTRATION_ID','P_SR_NUMBER');
        System.assertEquals(resp, 'S');
        Test.stopTest();
        
    }
    
    
    
      // To Cover DocumentAttachment()
        public static testmethod void DocumentAttachmentTest(){
        
        Test.startTest();
        
        soapencodingTypesDatabindingAxis2ApaMult.Base64Binary b = new soapencodingTypesDatabindingAxis2ApaMult.Base64Binary();
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentResponse_element    >();
        MultipleDocUploadService.DocumentAttachmentResponse_element  response_x = new MultipleDocUploadService.DocumentAttachmentResponse_element    ();
        response_x.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.DocumentAttachment('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','SourceId',
                                'RegistrationId','EntityName','Category','FileId','FileName','FileDescription','SourceFileName', b);
                                
        System.assertEquals(resp, 'S');                        
        Test.stopTest();
        
    }
    
    
     // To Cover getMasterMilestone()
        public static testmethod void getMasterMilestoneTest(){
        
        Test.startTest();
        soapencodingTypesDatabindingAxis2ApaMult.Base64Binary b = new soapencodingTypesDatabindingAxis2ApaMult.Base64Binary();
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.getMasterMilestoneResponse_element     >();
        MultipleDocUploadService.getMasterMilestoneResponse_element   response_x = new MultipleDocUploadService.getMasterMilestoneResponse_element();
        response_x.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.getMasterMilestone('REGISTRATION_ID');
        System.assertEquals(resp, 'S');
        Test.stopTest();
        
    }
    
    // To Cover PaymentPlanReversalCurrent()
        public static testmethod void PaymentPlanReversalCurrentTest(){
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.PaymentPlanReversalCurrentResponse_element      >();
        MultipleDocUploadService.PaymentPlanReversalCurrentResponse_element    response_x = new MultipleDocUploadService.PaymentPlanReversalCurrentResponse_element ();
        response_x.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.PaymentPlanReversalCurrent('P_REGISTRATION_ID' , 'P_SR_NUMBER');
        System.assertEquals(resp, 'S');
        Test.stopTest();
        
    }
    
    
    // To Cover getMilestonePaymentDetails()
        public static testmethod void getMilestonePaymentDetailsTest(){
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.getMilestonePaymentDetailsResponse_element>();
        MultipleDocUploadService.getMilestonePaymentDetailsResponse_element     response_x = new MultipleDocUploadService.getMilestonePaymentDetailsResponse_element();
        response_x.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.getMilestonePaymentDetails('REGISTRATION_ID');
        System.assertEquals(resp, 'S');
        Test.stopTest();
        
    }
    
    
        
    // To Cover RegistrationDetails()
        public static testmethod void RegistrationDetailsTest(){
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.RegistrationDetailsResponse_element >();
        MultipleDocUploadService.RegistrationDetailsResponse_element  response_x = new MultipleDocUploadService.RegistrationDetailsResponse_element ();
        response_x.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.RegistrationDetails('P_REGISTRATION_ID','P_SR_NUMBER');
        System.assertEquals(resp, 'S');
        Test.stopTest();
        
    }
    
    
      // To Cover PaymentPlanHistory()
        public static testmethod void PaymentPlanHistoryTest(){
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.PaymentPlanHistoryResponse_element  >();
        MultipleDocUploadService.PaymentPlanHistoryResponse_element   response_x = new MultipleDocUploadService.PaymentPlanHistoryResponse_element  ();
        response_x.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        resp= obj.PaymentPlanHistory('P_REGISTRATION_ID','P_SR_NUMBER');
        System.assertEquals(resp, 'S');
        Test.stopTest();
        
    }
    
}