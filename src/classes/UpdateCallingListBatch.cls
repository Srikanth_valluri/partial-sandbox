public class UpdateCallingListBatch implements Database.Batchable<sObject>{

 List<Calling_List__c> callingListToBeUpdatedByBatch = new List<Calling_List__c>();

   public UpdateCallingListBatch(List<Calling_List__c> callingListToBeUpdated){
   callingListToBeUpdatedByBatch = callingListToBeUpdated;          
   }

   public List<SObject> start(Database.BatchableContext BC){
      return callingListToBeUpdatedByBatch;
   }

   public void execute(Database.BatchableContext BC,List<Calling_List__c> scope){
	System.debug('scope----->'+scope);
      List<Calling_List__c> callingList = new List<Calling_List__c>();
      for(Calling_List__c sObj : scope) {
	   System.debug('sObj-->'+sObj);
       callingList.add(sObj);
	  }
      System.debug('callingList--->'+callingList);
      update callingList; 
   }

   public void finish(Database.BatchableContext BC){

   }

}