/**
 * @File Name          : CustomerFeedbackForCallingListCntrlTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 9/5/2019, 2:44:52 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    9/5/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest 
public class CustomerFeedbackForCallingListCntrlTest { 
    static List<User>userList;
    static List<Calling_List__c>insertCallingLst;
    static Id walkInCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Walk In Calling List').RecordTypeId;
    
     @isTest 
      private static  void testHappyFeedback(){
            Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
            Profile someProfile = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
            Profile adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
            Profile stdProfile = [SELECT Id FROM Profile WHERE Name = 'Standard Platform User' LIMIT 1];
            Account objAcc =  new Account(RecordTypeId = personAccRTId, 
                        FirstName='Test FirstName1', 
                        LastName='Test LastName2', 
                        Type='Person'
                        );
            insert objAcc ;
            insertCallingLst = new List<Calling_List__c>();
            userList = new List<User>();
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                            OnOffCheck__c = true);
            settingLst2.add(newSetting1);
            insert settingLst2;
            Calling_List__c walkinObj = new Calling_List__c(RecordTypeId = walkInCallingRecordTypeId,
                                                        Account__c = objAcc.Id,
                                                        Customer_Flag__c = true,
                                                        Purpose_of_Walk_in__c = 'Collections',
                                                        Calling_List_Status__c = 'New',
                                                        Notification_Time_for_outcome__c = DateTime.newInstance(2017,12,12,8,30,0),
                                                        Service_start__c = DateTime.newInstance(2017,12,12,2,37,0));
        
            insert walkinObj;
            PageReference currentPage = Page.CustomerFeedbackPageForCallingList;
            currentPage.getParameters().put('Id', walkinObj.id);
            Test.setCurrentPage(currentPage);

            Test.startTest(); 
                ApexPages.StandardController sc = new ApexPages.StandardController(walkinObj);
                CustomerFeedbackForCallingListCntrl controller = new CustomerFeedbackForCallingListCntrl(sc);
                controller.isHappy = true;
                controller.customerComments = 'I am happy';
                controller.submitFeedback();
            Test.stopTest(); 
      }
      @isTest 
      private static  void testUnsatisfiedFeedback(){
            Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
            Profile someProfile = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
            Profile adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
            Profile stdProfile = [SELECT Id FROM Profile WHERE Name = 'Standard Platform User' LIMIT 1];
            Account objAcc =  new Account(RecordTypeId = personAccRTId, 
                        FirstName='Test FirstName1', 
                        LastName='Test LastName2', 
                        Type='Person'
                        );
            insert objAcc ;
            insertCallingLst = new List<Calling_List__c>();
            userList = new List<User>();
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                            OnOffCheck__c = true);
            settingLst2.add(newSetting1);
            insert settingLst2;
            Calling_List__c walkinObj = new Calling_List__c(RecordTypeId = walkInCallingRecordTypeId,
                                                        Account__c = objAcc.Id,
                                                        Customer_Flag__c = true,
                                                        Purpose_of_Walk_in__c = 'Collections',
                                                        Calling_List_Status__c = 'New',
                                                        Notification_Time_for_outcome__c = DateTime.newInstance(2017,12,12,8,30,0),
                                                        Service_start__c = DateTime.newInstance(2017,12,12,2,37,0));
        
            insert walkinObj;
            PageReference currentPage = Page.CustomerFeedbackPageForCallingList;
            currentPage.getParameters().put('Id', walkinObj.id);
            Test.setCurrentPage(currentPage);

            Test.startTest(); 
                ApexPages.StandardController sc = new ApexPages.StandardController(walkinObj);
                CustomerFeedbackForCallingListCntrl controller = new CustomerFeedbackForCallingListCntrl(sc);
                controller.isNotSatisfied = true;
                controller.customerComments = 'I am Not Satisfied';
                controller.submitFeedback();
            Test.stopTest(); 
      }
       @isTest 
      private static  void testSatisfiedFeedback(){
            Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
            Profile someProfile = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
            Profile adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
            Profile stdProfile = [SELECT Id FROM Profile WHERE Name = 'Standard Platform User' LIMIT 1];
            Account objAcc =  new Account(RecordTypeId = personAccRTId, 
                        FirstName='Test FirstName1', 
                        LastName='Test LastName2', 
                        Type='Person'
                        );
            insert objAcc ;
            insertCallingLst = new List<Calling_List__c>();
            userList = new List<User>();
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                            OnOffCheck__c = true);
            settingLst2.add(newSetting1);
            insert settingLst2;
            Calling_List__c walkinObj = new Calling_List__c(RecordTypeId = walkInCallingRecordTypeId,
                                                        Account__c = objAcc.Id,
                                                        Customer_Flag__c = true,
                                                        Purpose_of_Walk_in__c = 'Collections',
                                                        Calling_List_Status__c = 'New',
                                                        Notification_Time_for_outcome__c = DateTime.newInstance(2017,12,12,8,30,0),
                                                        Service_start__c = DateTime.newInstance(2017,12,12,2,37,0));
        
            insert walkinObj;
            PageReference currentPage = Page.CustomerFeedbackPageForCallingList;
            currentPage.getParameters().put('Id', walkinObj.id);
            Test.setCurrentPage(currentPage);

            Test.startTest(); 
                ApexPages.StandardController sc = new ApexPages.StandardController(walkinObj);
                CustomerFeedbackForCallingListCntrl controller = new CustomerFeedbackForCallingListCntrl(sc);
                controller.isSatisfied = true;
                controller.customerComments = 'I am  Satisfied';
                controller.submitFeedback();
            Test.stopTest(); 
      }
       @isTest 
      private static  void testNegative(){
            Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
            Profile someProfile = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
            Profile adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
            Profile stdProfile = [SELECT Id FROM Profile WHERE Name = 'Standard Platform User' LIMIT 1];
            Account objAcc =  new Account(RecordTypeId = personAccRTId, 
                        FirstName='Test FirstName1', 
                        LastName='Test LastName2', 
                        Type='Person'
                        );
            insert objAcc ;
            insertCallingLst = new List<Calling_List__c>();
            userList = new List<User>();
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                            OnOffCheck__c = true);
            settingLst2.add(newSetting1);
            insert settingLst2;
            Calling_List__c walkinObj = new Calling_List__c(RecordTypeId = walkInCallingRecordTypeId,
                                                        Account__c = objAcc.Id,
                                                        Customer_Flag__c = true,
                                                        Purpose_of_Walk_in__c = 'Collections',
                                                        Calling_List_Status__c = 'New',
                                                        Notification_Time_for_outcome__c = DateTime.newInstance(2017,12,12,8,30,0),
                                                        Service_start__c = DateTime.newInstance(2017,12,12,2,37,0));
        
            insert walkinObj;
            PageReference currentPage = Page.CustomerFeedbackPageForCallingList;
            currentPage.getParameters().put('Id', walkinObj.id);
            Test.setCurrentPage(currentPage);

            Test.startTest(); 
                ApexPages.StandardController sc = new ApexPages.StandardController(walkinObj);
                CustomerFeedbackForCallingListCntrl controller = new CustomerFeedbackForCallingListCntrl(sc);
                controller.isHappy = false;
                controller.isSatisfied = false;
                controller.isNotSatisfied = false;
                controller.customerComments = 'I am  Satisfied';
                controller.submitFeedback();
            Test.stopTest(); 
      }
}