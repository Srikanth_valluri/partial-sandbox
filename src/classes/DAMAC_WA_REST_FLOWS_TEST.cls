@isTest

public class DAMAC_WA_REST_FLOWS_TEST{


    static testmethod void testFlow_OK(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        // pass the req and resp objects to the method     
        req.requestURI = 'https://fullcopy-damacproperties.cs105.force.com/damac/services/apexrest/waflows';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        req.params.put('event','message'); 
        req.params.put('muid',''); 
        req.params.put('contact[uid]','919652300133'); 
        req.params.put('ack',''); 
        req.params.put('token',''); 
        req.params.put('message[dir]','i'); 
        req.params.put('message[body][text]','ok'); 
        try{
            DAMAC_WA_REST_FLOWS.doPost();
        }catch(exception e){
        
        }
    }
    
    
    static testmethod void testFlow_random_message(){
        id parentflowRtypeId = Schema.SObjectType.Whats_App_Flows__c.getRecordTypeInfosByName().get('Parent Flow').getRecordTypeId();
        id childFlowRtypeId = Schema.SObjectType.Whats_App_Flows__c.getRecordTypeInfosByName().get('Child Flow').getRecordTypeId();
        id userflowRtypeId =  Schema.SObjectType.Whats_App_Flows__c.getRecordTypeInfosByName().get('User Flow').getRecordTypeId();
        // parent Flow
        Whats_App_Flows__c parent = new Whats_App_Flows__c();
        parent.name = 'Feedback';
        parent.Menu__c = '1';
        //parent.keyword__c = 'hi';
        parent.start_this_on_confusion__c = true;
        parent.confusion_menu__c = '1';
        parent.recordtypeid= parentflowRtypeId;
        parent.end_message__c= 'asdsaas';
        insert parent;
        
         Whats_App_Flows__c child = new Whats_App_Flows__c();
        child.Name = 'END';
        child.parent_flow__c =parent.id;
        child.keyword__c = '1';
        child.recordtypeid= childFlowRtypeId ;
        //child.Salesforce_Record_Id__c = officeMeeting.id;
        insert child;
        
        //User Flow
        Whats_App_Flows__c userFlow = new Whats_App_Flows__c();
        userflow.name = '919652300133';
        userflow.active_flow__c = true;
        userflow.Waiting_For_Input__c = true;
        userflow.user_selected_flow__c = parent.id;
        userflow.recordtypeid = userflowRtypeId;
        //userflow.Salesforce_Record_Id__c = officeMeeting.id;
        insert userflow;
    
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        // pass the req and resp objects to the method     
        req.requestURI = 'https://fullcopy-damacproperties.cs105.force.com/damac/services/apexrest/waflows';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        req.params.put('event','message'); 
        req.params.put('muid',''); 
        req.params.put('contact[uid]','919652300133'); 
        req.params.put('ack',''); 
        req.params.put('token',''); 
        req.params.put('message[dir]','i'); 
        req.params.put('message[body][text]','NOthing'); 
        DAMAC_WA_REST_FLOWS.doPost();
    }
    
    static testmethod void testFlow_KEYWORD(){
        
         User PCuser = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
         LastName = 'last',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US'
        );
        insert PCuser;
        
         List<String> inqSalesOffice = new List<String>();

        Schema.DescribeFieldResult fieldResult = Inquiry__c.Sales_Office__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple){
            inqSalesOffice.add(f.getValue());
        }
        system.debug('====Test class===inqSalesOffice=======' +inqSalesOffice); 

        
        List<QueueSObject> QueueList = [SELECT Queue.Id, 
                                               Queue.Name, 
                                               Queue.Type 
                                          FROM QueueSObject 
                                         WHERE Queue.Type ='Queue' 
                                           AND Queue.Name IN :inqSalesOffice];
                                           
       

        
        Id devRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        Inquiry__c inq = new Inquiry__c();
        inq.Mobile_Phone_Encrypt__c = '05789088';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Preferred_Language__c = 'English';
        inq.Inquiry_Status__c = 'New' ;
        inq.First_Name__c = 'Test First name';
        inq.Last_Name__c = 'Test Last name';
        inq.Email__c = 'test@gmail.com';
        inq.Inquiry_Source__c = 'Customer Referral';
        inq.Mobile_Phone__c = '456789';
        inq.RecordTypeId = devRecordTypeId;
        inq.Assigned_PC__c = PCuser.Id;
        inq.Inquiry_Status__c = 'Meeting Scheduled';
        inq.Sales_Office__c = 'AKOYA';
        inq.Meeting_Due_Date__c = system.now();
        insert inq;


        
        Office_Meeting__c  officeMeeting = new Office_Meeting__c();
        officeMeeting.Inquiry__c = inq.id;
        officeMeeting.Outcome__c = 'Show';
        officeMeeting.Status__c = 'Not Started';
        for(QueueSObject queue : QueueList){
            // system.debug('*****queueName*****' + queue);
            String queueName = queue.Queue.Name;
            if(queueName.equalsIgnoreCase(inq.Sales_Office__c)) {
                 officeMeeting.OwnerId = queue.Queue.Id;
            }
        }
        insert officeMeeting;

    
    
        id parentflowRtypeId = Schema.SObjectType.Whats_App_Flows__c.getRecordTypeInfosByName().get('Parent Flow').getRecordTypeId();
        id childFlowRtypeId = Schema.SObjectType.Whats_App_Flows__c.getRecordTypeInfosByName().get('Child Flow').getRecordTypeId();
        id userflowRtypeId =  Schema.SObjectType.Whats_App_Flows__c.getRecordTypeInfosByName().get('User Flow').getRecordTypeId();
        // parent Flow
        Whats_App_Flows__c parent = new Whats_App_Flows__c();
        parent.name = 'Feedback';
        parent.Menu__c = '1';
        parent.keyword__c = 'hi';
        parent.start_this_on_confusion__c = true;
        parent.confusion_menu__c = '1';
        parent.recordtypeid= parentflowRtypeId;
        parent.Restart_Cancel_Keyword__c = 'hi';
        insert parent;
        
        //Child flow
        Whats_App_Flows__c child = new Whats_App_Flows__c();
        child.Name = 'END';
        child.parent_flow__c =parent.id;
        child.keyword__c = '1';
        child.recordtypeid= childFlowRtypeId ;
        //child.Salesforce_Record_Id__c = officeMeeting.id;
        insert child;
        
        
        //end CHILD
        Whats_App_Flows__c childEND = new Whats_App_Flows__c();
        childEND.Name = 'END';
        childEND.parent_flow__c = child.id;        
        childEND.recordtypeid= childFlowRtypeId ;
        //childEND.Salesforce_Record_Id__c = officeMeeting.id;
        insert childEND;
        
        //User Flow
        Whats_App_Flows__c userFlow = new Whats_App_Flows__c();
        userflow.name = '919652300133';
        userflow.active_flow__c = true;
        userflow.Waiting_For_Input__c = true;
        userflow.user_selected_flow__c = parent.id;
        userflow.recordtypeid = userflowRtypeId;
        userflow.Salesforce_Record_Id__c = officeMeeting.id;
        insert userflow;
    
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        // pass the req and resp objects to the method     
        req.requestURI = 'https://fullcopy-damacproperties.cs105.force.com/damac/services/apexrest/waflows';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        req.params.put('event','message'); 
        req.params.put('muid',''); 
        req.params.put('cuid','919652300133'); 
        req.params.put('contact[uid]','919652300133'); 
        req.params.put('ack',''); 
        req.params.put('token',''); 
        req.params.put('message[dir]','i'); 
        req.params.put('message[body][text]','Hi'); 
        req.params.put('message[uid]','123abc'); 
        
        DAMAC_WA_REST_FLOWS.doPost();
        DAMAC_WA_REST_FLOWS.updateFlow(userflow.id);
    }
    
    static testmethod void testFlow_flow(){
        id parentflowRtypeId = Schema.SObjectType.Whats_App_Flows__c.getRecordTypeInfosByName().get('Parent Flow').getRecordTypeId();
        id childFlowRtypeId = Schema.SObjectType.Whats_App_Flows__c.getRecordTypeInfosByName().get('Child Flow').getRecordTypeId();
        id userflowRtypeId =  Schema.SObjectType.Whats_App_Flows__c.getRecordTypeInfosByName().get('User Flow').getRecordTypeId();
        // parent Flow
        Whats_App_Flows__c parent = new Whats_App_Flows__c();
        parent.name = 'Feedback';
        parent.Menu__c = '1';
        parent.keyword__c = 'Hi';
        parent.start_this_on_confusion__c = true;
        parent.confusion_menu__c = '1';
        parent.recordtypeid= parentflowRtypeId;
        insert parent;
        
        //Child flow
        Whats_App_Flows__c child = new Whats_App_Flows__c();
        child.Name = 'Thanks';
        child.parent_flow__c =parent.id;
        child.keyword__c = '1';
        child.recordtypeid= childFlowRtypeId ;
        insert child;
        
        Whats_App_Flows__c child1 = new Whats_App_Flows__c();
        child1.Name = 'END';
        child1.parent_flow__c =CHILD.id;        
        child1.recordtypeid= childFlowRtypeId ;
        insert child1;
        
        //User Flow
        Whats_App_Flows__c userFlow = new Whats_App_Flows__c();
        userflow.name = '919652300133';
        userflow.active_flow__c = true;
        userflow.Waiting_For_Input__c = false;
        userflow.user_selected_flow__c = parent.id;
        userflow.recordtypeid = userflowRtypeId;
        insert userflow;
        
        //User Flow
        Whats_App_Flows__c userFlow1 = new Whats_App_Flows__c();
        userflow1.name = '919652300133';
        userflow1.active_flow__c = false;
        userflow1.Waiting_For_Input__c = true;
        userflow1.user_selected_flow__c = userflow.id;
        userflow1.recordtypeid = userflowRtypeId;
        insert userflow1;
    
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        // pass the req and resp objects to the method     
        req.requestURI = 'https://fullcopy-damacproperties.cs105.force.com/damac/services/apexrest/waflows';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        req.params.put('event','message'); 
        req.params.put('muid',''); 
        req.params.put('cuid','919652300133'); 
        req.params.put('contact[uid]','919652300133'); 
        req.params.put('ack',''); 
        req.params.put('token',''); 
        req.params.put('message[dir]','i'); 
        req.params.put('message[body][text]','1'); 
        req.params.put('message[uid]','123ABC'); 
        
        DAMAC_WA_REST_FLOWS.doPost();
        
    }



}