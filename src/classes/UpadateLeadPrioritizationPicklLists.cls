public class UpadateLeadPrioritizationPicklLists implements Schedulable {
    String sessionId;

    public UpadateLeadPrioritizationPicklLists(String sessionId) {
        this.sessionId = sessionId;
    }
	public void execute(SchedulableContext sc) {
        updateCampaignType(sessionId);
        updateCampaignCategory(sessionId);
        updateCampaignSubCategory(sessionId);
	}

    @future(callout=true)
    public static void updateCampaignType(String userSessionId) {
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Campaign__c.Campaign_Type_New__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry entry : ple){
            options.add(entry.getValue());
        }
        //MetadataService.MetadataPort service = createService();
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = userSessionId;
        MetadataService.CustomField customField = new MetadataService.CustomField();
        List<MetadataService.PicklistValue> ptv = new list<MetadataService.PicklistValue>();
        customField.fullName = 'Lead_Prioritization_Configuration__c.Campaign_Type__c';// your object. fields name
        customField.label = 'Campaign Type';// label of field.
        customField.type_x = 'Picklist';// type
        metadataservice.Picklist pt = new metadataservice.Picklist();
        pt.sorted= false;
        for(String str : options) {
        metadataservice.PicklistValue toadd = new metadataservice.PicklistValue();
          toadd.fullName = str;
          toadd.default_x = false ;
          ptv.add(toadd);
        }
        pt.picklistValues = ptv;
        customField.picklist = pt;
        List<MetadataService.SaveResult> results = service.updateMetadata(new MetadataService.Metadata[] {customField});
    }

     @future(callout=true)
    public static void updateCampaignCategory(String userSessionId) {
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Campaign__c.Campaign_Category_New__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry entry : ple){
            options.add(entry.getValue());
        }
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = userSessionId;
        MetadataService.CustomField customField = new MetadataService.CustomField();
        List<MetadataService.PicklistValue> ptv = new list<MetadataService.PicklistValue>();
        customField.fullName = 'Lead_Prioritization_Configuration__c.Campaign_Category__c';// your object. fields name
        customField.label = 'Campaign Category';// label of field.
        customField.type_x = 'Picklist';// type
        metadataservice.Picklist pt = new metadataservice.Picklist();
        pt.sorted= false;
        for(String str : options) {
        metadataservice.PicklistValue toadd = new metadataservice.PicklistValue();
          toadd.fullName = str;
          toadd.default_x = false ;
          ptv.add(toadd);
        }
        pt.picklistValues = ptv;
        customField.picklist = pt;
        List<MetadataService.SaveResult> results = service.updateMetadata(new MetadataService.Metadata[] {customField});
    }

     @future(callout=true)
    public static void updateCampaignSubCategory(String userSessionId) {
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Campaign__c.Campaign_Sub_Category_1__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry entry : ple){
            options.add(entry.getValue());
        }
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = userSessionId;
        MetadataService.CustomField customField = new MetadataService.CustomField();
        List<MetadataService.PicklistValue> ptv = new list<MetadataService.PicklistValue>();
        customField.fullName = 'Lead_Prioritization_Configuration__c.Campaign_Sub_Category_1__c';// your object. fields name
        customField.label = 'Campaign Sub Category 1';// label of field.
        customField.type_x = 'Picklist';// type
        metadataservice.Picklist pt = new metadataservice.Picklist();
        pt.sorted= false;
        for(String str : options) {
        metadataservice.PicklistValue toadd = new metadataservice.PicklistValue();
          toadd.fullName = str;
          toadd.default_x = false ;
          ptv.add(toadd);
        }
        pt.picklistValues = ptv;
        customField.picklist = pt;
        List<MetadataService.SaveResult> results = service.updateMetadata(new MetadataService.Metadata[] {customField});
    }
}