public without sharing class CreateDebitMemoInIPMS{
    @future(callout=true)
    public static void postFinancialDetails(set<Id> setCaseIds){
        system.debug('CreateDebitMemoInIPMS***** setCaseIds***** '+setCaseIds);
        list<Error_Log__c> lstErrors = new list<Error_Log__c>();
        map<Id,Booking_Unit__c> mapId_Unit = new map<Id,Booking_Unit__c>();
        for(Case objCase : [Select Id
                                 , Booking_Unit__c
                                 , Assignment_Fee__c
                                 , Registration_ID__c
                                 , Seller__c 
                            From Case
                            Where Id IN : setCaseIds]){
            actionCom.COCDHttpSoap11Endpoint memoCalloutObj =
            new actionCom.COCDHttpSoap11Endpoint();
            memoCalloutObj.timeout_x = 120000;
            String response =
            memoCalloutObj.CreateDebitCreditMemo(String.valueOf(Datetime.now().getTime()),
                                                 'CREATE_DM_CM',
                                                 'SFDC',
                                                 objCase.Registration_ID__c ,
                                                 String.valueOf(objCase.Assignment_Fee__c),
                                                 'Assignment Fees',
                                                 'Assignment Fees',
                                                 String.valueOf( Datetime.now().getTime()),
                                                 'Assignment Fees');
            system.debug('CreateDebitMemoInIPMS response***********'+response);
            if(response != null
            && response.contains('E~')){
                Error_Log__c objErr = new Error_Log__c();
                objErr.Account__c = objCase.Seller__c;
                objErr.Booking_Unit__c = objCase.Booking_Unit__c;
                objErr.Case__c = objCase.Id;
                objErr.Process_Name__c = 'Assignment';
                objErr.Error_Details__c = 'Error : Re-posting Assignment Fee to IPMS failed. Please try again later.';
                lstErrors.add(objErr);
            }else if(response != null
            && !response.contains('E~')){
                Booking_Unit__c objBU = new Booking_Unit__c();
                objBU.Id = objCase.Booking_Unit__c;
                objBU.NOC_Re_Issued_Date__c = date.today();
                mapId_Unit.put(objCase.Id, objBU);
            }
        }
        
        system.debug('mapId_Unit.values*****'+mapId_Unit.values());
        if(!mapId_Unit.isEmpty()){
            update mapId_Unit.values();
        }
        system.debug('lstErrors*****'+lstErrors);
        if(!lstErrors.isEmpty()){
            insert lstErrors;
        }
    }
}