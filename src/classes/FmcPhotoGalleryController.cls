public with sharing class FmcPhotoGalleryController {

    @RemoteAction
    public static Map<String, List<SR_Attachments__c>> fetchPhotoGallery() {
        Set<String> documentTypes = new Set<String> { 'Photo', 'PhotoGallery' };
        return FmcDocumentsController.fetchDocumentsOfType(documentTypes);
    }

}