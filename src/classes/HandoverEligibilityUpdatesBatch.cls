/****************************************************************************************************
* Name          : HandoverEligibilityUpdatesBatch                                                   *
* Description   : Used for sending daily emails of "Handover Eligibility status of Unit xyz 	    *
*				  has changed.Please check and send Handover/EHO notice as required." 				*
*				  tasks which are pending from one day to team.       								* 
* Created Date  : 13-02-2019                                                                        *
* Created By    : ESPL																				*
* Cron Expression : 0 0 7 * * ?                                                                     *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE        COMMENTS                                                    *
* 1.0                                   Initial Draft.                                              *
****************************************************************************************************/

public with sharing class HandoverEligibilityUpdatesBatch implements Database.Batchable<SObject>, 
																	 Database.Stateful,
																	 Schedulable {
	
	private list<Booking_Unit__c> lstUnits = new list<Booking_Unit__c>();
	private map<Id,DateTime> mapUnitIdCreatedDate = new map<Id,DateTime>();
	
	public Database.QueryLocator start( Database.BatchableContext BC ) {
		String strQuery = 'SELECT Id ' +
						  	   ', Subject ' +
						  	   ', WHatId ' +
						  	   ', Status '+
						  	   ', CreatedDate '+
						  	'FROM Task '+
						   'WHERE Status NOT IN (\'Completed\') '+
						     'AND Subject LIKE \'%Handover Eligibility status%\' ' ;
		if( !Test.isRunningTest() ) {				     
	    	strQuery += 'AND CreatedDate < YESTERDAY ' ;
		}
		system.debug('==strQuery=='+strQuery);
		return Database.getQueryLocator( strQuery );
	}
	
	public void execute( Database.BatchableContext BC, list<Task> lstTasks ) {
		String strKeyPrefix = Schema.getGlobalDescribe().get( 'Booking_Unit__c' ).getDescribe().getKeyPrefix();
		set<Id> setUnitIds = new set<Id>();
		for( Task objTask : lstTasks ) {
			if( objTask.WhatId != NULL && 
				String.valueOf( objTask.WhatId ).startsWith( strKeyPrefix ) ) {
				setUnitIds.add( objTask.WhatId );
				mapUnitIdCreatedDate.put( objTask.WhatId, objTask.CreatedDate );
			}
		}
		if( !setUnitIds.isEmpty() ) {
			lstUnits.addAll( getUnitDetails( setUnitIds ) );
		}
	}
	
	public void finish( Database.BatchableContext BC ) {
        if( !lstUnits.isEmpty() ) {
	        Messaging.SingleEmailMessage objEmailMessage = new Messaging.SingleEmailMessage();
	        objEmailMessage.toAddresses = Label.Handover_Eligibility_Updates_Notification.split(',');
	        objEmailMessage.subject = 'Handover Notice Sending : Pending Tasks';
	        objEmailMessage.HtmlBody = getHTMLEmailBody();
	        Messaging.SendEmailResult[] results = Messaging.sendEmail( new list<Messaging.SingleEmailMessage>{ objEmailMessage } );
        }
	}
	
	public void execute( SchedulableContext sc ) {
        Database.executebatch( new HandoverEligibilityUpdatesBatch() );
    }
	
	
	private list<Booking_Unit__c> getUnitDetails( set<Id> setUnitIds ) {
		return [ SELECT Id
					  , Property_Name__c
					  , Unit_Name__c
					  , Registration_ID__c
					  , Registration_DateTime__c
					  , Registration_Status__c
					  , Approval_Status__c
					  , Reason_for_Hold__c
					  , DP_OK__c
					  , Doc_OK__c
					  , Handover_Notice_Sent__c
					  , Handover_Notice_Sent_Date__c
					  , EHO_Notice_Sent__c
					  , EHO_Notice_Sent_Date__c
					  , JOPD_Area__c
					  , Dispute_Flag__c
					  , Recovery__c
					  , Bedroom_Type__c
				   FROM Booking_Unit__c
				  WHERE Id IN :setUnitIds ]; 
	}
	
	private String getHTMLEmailBody() {
		String strBody = '';
		strBody += 'Dear All,<br/><br/>Please find below the open tasks report.';
		strBody += '<br/><br/><html><body>' 
                + '<table style="border:1px solid black; text-align:left; border-collapse:collapse;">'+
                +'<tr><th style="border:1px solid black;border-collapse:collapse; text-align:center;">Property Name</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Unit Name</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Registration ID</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Registration Date</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Registration Status</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Approval Status</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Reason for Hold</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">DP OK</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">DOC OK</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Bedroom Type</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Handover Notice Sent</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Handover Notice Sent Date</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">EHO Notice Sent</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">EHO Notice Sent Date</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">JOPD Area</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Dispute</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Recovery</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Task Created Date</th>';
        for( Booking_Unit__c objUnit : lstUnits ) {
        	
        	String strRegistrationDateTime = String.isNotBlank( String.valueOf( objUnit.Registration_DateTime__c ) ) ?
        									 String.valueOf( objUnit.Registration_DateTime__c.format('dd/MM/YYYY') ) :
        									 '';
        	String strReasonForHold = String.isNotBlank( objUnit.Reason_for_Hold__c ) ? objUnit.Reason_for_Hold__c : '' ;
        	String strDPOK = objUnit.DP_OK__c ? 'Yes' : 'No' ;
        	String strDocOK = objUnit.Doc_OK__c ? 'Yes' : 'No' ;
        	String strHandoverNotice = objUnit.Handover_Notice_Sent__c ? 'Yes' : 'No' ;
        	String strEHONotice = objUnit.EHO_Notice_Sent__c ? 'Yes' : 'No' ;
        	String strHandoverDate = String.isNotBlank( String.valueOf( objUnit.Handover_Notice_Sent_Date__c ) ) ? 
        							 String.valueOf( objUnit.Handover_Notice_Sent_Date__c ) : '' ;
      		String strEHODate = String.isNotBlank( String.valueOf( objUnit.EHO_Notice_Sent_Date__c ) ) ? 
        							 String.valueOf( objUnit.EHO_Notice_Sent_Date__c ) : '' ;
        	String strDispute = String.isNotBlank( objUnit.Dispute_Flag__c ) ? objUnit.Dispute_Flag__c : '' ;
        	String strRecovery = String.isNotBlank( objUnit.Recovery__c ) ? objUnit.Recovery__c : '' ;
        	
        	strBody += '<tr>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + objUnit.Property_Name__c + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + objUnit.Unit_Name__c + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + objUnit.Registration_ID__c + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + strRegistrationDateTime + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + objUnit.Registration_Status__c + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + objUnit.Approval_Status__c + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + strReasonForHold + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + strDPOK + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + strDocOK + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + objUnit.Bedroom_Type__c + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + strHandoverNotice + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + strHandoverDate + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + strEHONotice + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + strEHODate + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + objUnit.JOPD_Area__c + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + strDispute + '</td>';
            strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + strRecovery + '</td>';
            if( mapUnitIdCreatedDate.containsKey( objUnit.Id ) ) {
            	strBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' + String.valueOf( mapUnitIdCreatedDate.get( objUnit.Id ).format('dd/MM/YYYY')) + '</td>';
            }
            strBody += '</tr>';
        }
        strBody += '</table></body></html><br/>';
		return strBody ;
	}
	
}