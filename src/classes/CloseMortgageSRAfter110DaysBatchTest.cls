@isTest
public class CloseMortgageSRAfter110DaysBatchTest{
     private static testmethod void batchTest(){

        

        User u1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Collection - CRE'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            UserName = 'puser000@amama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias1',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
           
        );
        insert u1;
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Collection - CRE'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Manager = u1,
            UserName = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            
        );
        insert u;


        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
         
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;

        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id; 
        BU.Registration_ID__c = '123135'; 
        insert BU; 

        Buyer__c buyer =  new Buyer__c();
        buyer.Account__c  = Acc.Id;
        buyer.Booking__c   = Booking.Id;
        buyer.Primary_Buyer__c   = false;
        buyer.First_Name__c  = 'Test';      
        buyer.Last_Name__c  = 'Test Last';
        buyer.IPMS_Registration_ID__c ='123456';
        insert buyer;
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Mortgage').getRecordTypeId();
        
        
        Case caseObj = TestDataFactory_CRM.createCase(Acc.Id, recTypeId);
        caseObj.Status = 'Submitted';
        caseObj.Booking_Unit__c = BU.Id;
        caseObj.OwnerId  = u.Id;
        caseObj.Mortgage_Flag__c = 'I';
        caseObj.Type = 'Mortgage';
        caseObj.Extended__c = true;
        insert caseObj;


        Task t = new Task(Subject='Donni',Status='New',Priority='Normal',WhatId = caseObj.Id );
        insert t ;
        
        Datetime yesterday = Datetime.now().addDays(-110);
        
        Test.setCreatedDate(caseObj.Id, yesterday);

        Test.startTest();
            Database.executeBatch(new CloseMortgageSRAfter110DaysBatch(),1);   
        Test.stopTest();



     }
        

}