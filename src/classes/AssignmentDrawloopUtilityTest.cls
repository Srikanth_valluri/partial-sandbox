/**
 * Description: Test class for AssignmentDrawloopUtility
 */
@isTest
private class AssignmentDrawloopUtilityTest {

    /*
     * Description : Test method to check whether the DDI and DDI Child records are deleted or not.
     */
    @isTest static void testDeleteDDIRecords() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id assignmentRecordTypeID = getRecordTypeIdForAssignment();

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , assignmentRecordTypeID);      

        // Creation of DDI
        DDI__c objDDI = new DDI__c();
        insert objDDI;
        objCase.DDI__c = objDDI.Id;
        insert objCase;

        // Creation of DDI Child
        DDI_Child_1__c objDDIChild = new DDI_Child_1__c();
        objDDIChild.Case__c = objCase.Id;
        insert objDDIChild;

        Test.startTest();
            AssignmentDrawloopUtility.deleteDDIRecords(new List<Case>{objCase});
        Test.stopTest();

        // Check whether error message is generated or not
        objCase = [Select DDI__c FROM Case WHERE Id =:objCase.Id];
        System.assert(objCase != null);
        System.assertEquals(null, objCase.DDI__c);
        
        List<DDI_Child_1__c> lstDDIChild = [Select Id From DDI_Child_1__c Where Case__c =: objCase.Id];
        System.assert(lstDDIChild.size() == 0);
    }

    /**
     * Method to get the "Assignment" record type
     */
    private static Id getRecordTypeIdForAssignment() {
      Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id assignmentRecordTypeID = caseRecordTypes.get('Assignment').getRecordTypeId();
      return assignmentRecordTypeID;
    }
}