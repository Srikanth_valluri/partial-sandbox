public class GetFileIdOffice365AndSendEmailToCustomer {
    public static List<Attachment> getFileAndReturnAttachment (String fileName,String bookingUnitId){
        List<Attachment> lstAttach = new List<Attachment>();
        String fileId;
            if(String.isNotBlank(fileName)) {
             if(!Test.isRunningTest()){
               Office365RestService.isForHandoverStatment = true; 
             }
               fileId = Office365RestService.downloadFromOffice365ByName(fileName, '',bookingUnitId, '', '');
                System.debug('fileId>>>>>'+fileId);           
                Office365RestService.ResponseWrapper responseWrap = new Office365RestService.ResponseWrapper();
                //responseWrap  = Office365RestService.downloadFromOffice365(fileId,fileName,'',bookingUnitId,'','');
                // responseWrap = Office365RestService.downloadFromOffice365ByIdWhatsapp(fileId,'','','','','');
                if(Test.isRunningTest()) {
                     responseWrap = Office365RestService.downloadFromOffice365ById(fileId,'','','','','');
                 }
                 else {
                     responseWrap = Office365RestService.downloadFromOffice365ByIdWhatsapp(fileId,'','','','','');
                 }
                System.debug('responseWrap.downloadUrl' + responseWrap);
                if(responseWrap != Null && responseWrap.downloadUrl != Null) {
                    System.debug('responseWrap.downloadUrl' + responseWrap.downloadUrl);
                    HttpRequest req = new HttpRequest();
                    HttpResponse response;
                    req.setEndpoint(responseWrap.downloadUrl);
                    req.setHeader('Accept', 'application/json');
                    req.setMethod('GET');
                    req.setTimeout(120000);
                    if(!Test.isRunningTest()){
                        response = new Http().send(req);
                    }
                    else {
                        response = new HttpResponse();
                    }
                    System.debug('response.getBody()>>>'+response.getBody());
                    System.debug('response.getBodyAsBlob()>>>'+response.getBodyAsBlob());
                      if(response.getStatusCode() == 200) { 
                        Attachment attach = new Attachment();
                        attach.contentType = 'application/pdf';
                        attach.name = fileName;
                        attach.body = response.getBodyAsBlob();
                        lstAttach.add(attach);  
                      }
                }
                
            }
            
            return lstAttach;   
    }
    
    public static List<EmailMessage> sendEmailToCustomer(Booking_Unit__c objBookingUnit, List<Attachment> lstAttchement, String templateName, String currentMonth, String currentQuarter,String currentYear) {
        Map<String, String> mapOfMonthNumToMonth = new Map<String, String>{'M1' => 'Jan','M2' => 'Feb','M3' => 'Mar','M4' => 'Apr','M5' => 'May',
        'M6' => 'Jun','M7' => 'July','M8' => 'Aug','M9' => 'Sep','M10' => 'Oct','M11' => 'Nov','M12' => 'Dec'};
        Map<String, String> mapOfQuarterToMOnth = new Map<String, String>{'Q1' => 'Jan to Mar', 'Q2' => 'Apr to Jun','Q3' => 'Jul to Sep','Q4' => 'Oct to Dec'};
        EmailTemplate hospitalityTemplate;
        List<EmailMessage> lstEmails = new List<EmailMessage>();
        String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';
        String subject, strAccountId, strDPIId, bccAddress ='', strCCAddress ='';
        hospitalityTemplate = [SELECT ID, Subject, Body, HtmlValue FROM EmailTemplate 
                                        WHERE DeveloperName =: templateName limit 1];
         strAccountId = objBookingUnit.Booking__r.Account__c;
         toAddress = objBookingUnit.Booking__r.Account__r.Email__pc;
         bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
         fromAddress = Label.CRM_Hospitality_Address;
         //fromAddress = 'CRM.hospitality@damacgroup.com';
    
         subject =  hospitalityTemplate.Subject.replace('{!Case.Unit_Name__c}', objBookingUnit.Unit_Name__c);
         contentType = 'text/html';
         contentValue = contentValue = hospitalityTemplate.htmlValue;
          System.debug('currentMonth>>>'+currentMonth);
          System.debug('currentQuarter>>>'+currentQuarter);
         if(hospitalityTemplate.body != NULL) {
           contentBody =  hospitalityTemplate.body;
           contentBody = contentBody.replace('{!Case.Account}', objBookingUnit.Booking__r.Account__r.Name);
           contentBody = contentBody.replace('{!Case.Unit_Name__c}', objBookingUnit.Unit_Name__c);
           if(currentMonth != Null && currentMonth != '') {
               System.debug('Inside first if');
               contentBody = contentBody.replace('{!monthlyOrQuarterly}', mapOfMonthNumToMonth.get(currentMonth)+'-'+currentYear);
               contentBody = contentBody.replace('{!monthOrYear}', 'month');
           }
           else if(currentQuarter != Null && currentQuarter != '') {
               System.debug('Inside else if');
               contentBody = contentBody.replace('{!monthlyOrQuarterly}', mapOfQuarterToMOnth.get(currentQuarter)+'-'+currentYear);
               contentBody = contentBody.replace('{!monthOrYear}','month');
               System.debug('contentBody>>>>'+contentBody);
           } 
             else {
                System.debug('Inside else');
               contentBody = contentBody.replace('{!monthlyOrQuarterly}', currentYear);
               contentBody = contentBody.replace('{!monthOrYear}', 'year');
               contentBody = contentBody.replace('{!Case.Account}', objBookingUnit.Booking__r.Account__r.Name);
           }          
         }
         if(string.isblank(contentValue)){ contentValue='No HTML Body'; }
       if(hospitalityTemplate.htmlValue != NULL) {
         contentValue = contentValue.replace('{!Case.Account}', objBookingUnit.Booking__r.Account__r.Name);
         contentValue = contentValue.replace('{!Case.Unit_Name__c}', objBookingUnit.Unit_Name__c);
          if(currentMonth != Null && currentMonth != '') {
               contentValue = contentValue.replace('{!monthlyOrQuarterly}', mapOfMonthNumToMonth.get(currentMonth)+'-'+currentYear);
               contentValue = contentValue.replace('{!monthOrYear}', 'month');
           }
           else if(currentQuarter != Null && currentQuarter != '') {
               contentValue = contentValue.replace('{!monthlyOrQuarterly}', mapOfQuarterToMOnth.get(currentQuarter)+'-'+currentYear);
               contentValue = contentValue.replace('{!monthOrYear}', 'month');
                System.debug('contentBody>>>>'+contentBody);
           }
           else {
               contentValue = contentValue.replace('{!monthlyOrQuarterly}', currentYear);
               contentValue = contentValue.replace('{!monthOrYear}', 'year');
               contentValue = contentValue.replace('{!Case.Account}', objBookingUnit.Booking__r.Account__r.Name);
           }
         }
          
            GenericUtility.SendGridWrapper sgWrap = new GenericUtility.SendGridWrapper();
            sgWrap.toAddress =  String.isNotBlank( toAddress ) ? toAddress + ',' + sgWrap.toAddress : sgWrap.toAddress;
            sgWrap.contentType = 'text/html';
            sgWrap.fromAddress = fromAddress;
            sgWrap.contentValue = contentValue;
            sgWrap.contentBody = contentBody;
            sgWrap.subject = subject;           
            sgWrap.listAttachment = lstAttchement;
            sgWrap.relatedToId = strAccountId;
            sgWrap.AccountId = strAccountId;
            System.debug('sgWrap.toAddress>>>>'+sgWrap.toAddress);
            System.debug('sgWrap.toAddress>>>>'+sgWrap.contentValue);
            System.debug('sgWrap.toAddress>>>>'+sgWrap.contentBody);
            System.debug('sgWrap.toAddress>>>>'+sgWrap.subject);
            System.debug('sgWrap.toAddress>>>>'+sgWrap.listAttachment);
            System.debug('sgWrap.toAddress>>>>'+sgWrap.relatedToId);
            System.debug('sgWrap.toAddress>>>>'+sgWrap.AccountId);

            
            if( String.isNotBlank( sgWrap.toAddress ) ) {
                lstEmails = GenericUtility.sendEmailsBySendGrid( sgWrap );
            }
            return lstEmails;
    }
    
}