@isTest
public class POPPaymentModesAPITest {
    @isTest
    static void testPaymentModeInstallment() {
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popPaymentModes';
        req.addParameter('paymentType','installment');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        POPPaymentModesAPI.getPaymentModes();
        
        Test.stopTest();
    }
    
    @isTest
    static void testPaymentModeServiceCharge() {
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popPaymentModes';
        req.addParameter('paymentType','service_charge');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        POPPaymentModesAPI.getPaymentModes();
        
        Test.stopTest();
    }
    
    @isTest
    static void testPaymentTypeIdNull() {
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popPaymentModes';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        POPPaymentModesAPI.getPaymentModes();
        
        Test.stopTest();
    }
    
    @isTest
    static void testPaymentTypeIdBlank() {
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popPaymentModes';
        req.addParameter('paymentType','');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        POPPaymentModesAPI.getPaymentModes();
        
        Test.stopTest();
    }
}