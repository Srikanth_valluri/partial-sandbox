/***********************************************************************************
* Description - Test class developed for HandoverMQServices
*
* Version            Date            Author                    Description
* 1.0				 14/12/2017		 Ashish Agarwal				Initial Draft
*************************************************************************************/
@isTest
private class HandoverMQServicesTest {

    static testMethod void testUpdatePDCInformation() {
        // TO DO: implement unit test
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, HandoverMQServices.UpdatePDCInformationResponse_element>();
            HandoverMQServices.UpdatePDCInformationResponse_element response1 = new HandoverMQServices.UpdatePDCInformationResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	HandoverMQServices.HandoverHttpSoap11Endpoint objCall = new HandoverMQServices.HandoverHttpSoap11Endpoint();
        	objCall.UpdatePDCInformation('Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test');
        test.stopTest();
    }
    
    static testMethod void testGeneratePCC() {
        // TO DO: implement unit test
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, HandoverMQServices.generatePCCResponse_element>();
            HandoverMQServices.generatePCCResponse_element response1 = new HandoverMQServices.generatePCCResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	HandoverMQServices.HandoverHttpSoap11Endpoint objCall = new HandoverMQServices.HandoverHttpSoap11Endpoint();
        	objCall.GeneratePCC('Test', 'Test', 'Test', new list<HandoverMQServicesWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5>() );
        test.stopTest();
    }
    
    static testMethod void testEarlyHandoverPaymentPlanCreation() {
        // TO DO: implement unit test
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, HandoverMQServices.EarlyHandoverPaymentPlanCreationResponse_element>();
            HandoverMQServices.EarlyHandoverPaymentPlanCreationResponse_element response1 = new HandoverMQServices.EarlyHandoverPaymentPlanCreationResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	HandoverMQServices.HandoverHttpSoap11Endpoint objCall = new HandoverMQServices.HandoverHttpSoap11Endpoint();
        	objCall.EarlyHandoverPaymentPlanCreation('Test', 'Test', 'Test', new list<HandoverMQServicesXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5>() );
        test.stopTest();
    }
    
    static testMethod void testGET_EARLY_HO_DETAILS() {
        // TO DO: implement unit test
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, HandoverMQServices.GET_EARLY_HO_DETAILSResponse_element>();
            HandoverMQServices.GET_EARLY_HO_DETAILSResponse_element response1 = new HandoverMQServices.GET_EARLY_HO_DETAILSResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	HandoverMQServices.HandoverHttpSoap11Endpoint objCall = new HandoverMQServices.HandoverHttpSoap11Endpoint();
        	objCall.GET_EARLY_HO_DETAILS('Test', 'Test', 'Test', new list<HandoverMQServicesWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5>() );
        test.stopTest();
    }
    
    static testMethod void testGetHandoverDetails() {
        // TO DO: implement unit test
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, HandoverMQServices.GetHandoverDetailsResponse_element>();
            HandoverMQServices.GetHandoverDetailsResponse_element response1 = new HandoverMQServices.GetHandoverDetailsResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	HandoverMQServices.HandoverHttpSoap11Endpoint objCall = new HandoverMQServices.HandoverHttpSoap11Endpoint();
        	objCall.GetHandoverDetails('Test', 'Test', 'Test', 'Test' );
        test.stopTest();
    }
    
    static testMethod void testEHOCRFRequestGeneration() {
        // TO DO: implement unit test
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, HandoverMQServices.EHOCRFRequestGenerationResponse_element>();
            HandoverMQServices.EHOCRFRequestGenerationResponse_element response1 = new HandoverMQServices.EHOCRFRequestGenerationResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	HandoverMQServices.HandoverHttpSoap11Endpoint objCall = new HandoverMQServices.HandoverHttpSoap11Endpoint();
        	objCall.EHOCRFRequestGeneration('Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test');
        test.stopTest();
    }
    
    static testMethod void testLetterofDischarge() {
        // TO DO: implement unit test
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, HandoverMQServices.LetterofDischargeResponse_element>();
            HandoverMQServices.LetterofDischargeResponse_element response1 = new HandoverMQServices.LetterofDischargeResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	HandoverMQServices.HandoverHttpSoap11Endpoint objCall = new HandoverMQServices.HandoverHttpSoap11Endpoint();
        	objCall.LetterofDischarge('Test', 'Test', 'Test', new list<HandoverMQServicesWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5>() );
        test.stopTest();
    }
    
    static testMethod void testGetFinalInvoice() {
        // TO DO: implement unit test
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, HandoverMQServices.GetFinalInvoiceResponse_element>();
            HandoverMQServices.GetFinalInvoiceResponse_element response1 = new HandoverMQServices.GetFinalInvoiceResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	HandoverMQServices.HandoverHttpSoap11Endpoint objCall = new HandoverMQServices.HandoverHttpSoap11Endpoint();
        	objCall.GetFinalInvoice('Test', 'Test', 'Test', 'Test' ); 
        test.stopTest();
    }
    
    static testMethod void testUPDATE_EARLY_HO_FLAG() {
        // TO DO: implement unit test
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, HandoverMQServices.UPDATE_EARLY_HO_FLAGResponse_element>();
            HandoverMQServices.UPDATE_EARLY_HO_FLAGResponse_element response1 = new HandoverMQServices.UPDATE_EARLY_HO_FLAGResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	HandoverMQServices.HandoverHttpSoap11Endpoint objCall = new HandoverMQServices.HandoverHttpSoap11Endpoint();
        	objCall.UPDATE_EARLY_HO_FLAG('Test', 'Test', 'Test', new list<HandoverMQServicesWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5>() );
        test.stopTest();
    }
    
    static testMethod void testUpdateBookingUnit() {
        // TO DO: implement unit test
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, HandoverMQServices.UpdateBookingUnitResponse_element>();
            HandoverMQServices.UpdateBookingUnitResponse_element response1 = new HandoverMQServices.UpdateBookingUnitResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	HandoverMQServices.HandoverHttpSoap11Endpoint objCall = new HandoverMQServices.HandoverHttpSoap11Endpoint();
        	objCall.UpdateBookingUnit('Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test' );
        test.stopTest();
    }
    
    static testMethod void testGetBuildingRERAPercentage() {
        // TO DO: implement unit test
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, HandoverMQServices.getBuildingRERAPercentageResponse_element>();
            HandoverMQServices.getBuildingRERAPercentageResponse_element response1 = new HandoverMQServices.getBuildingRERAPercentageResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	HandoverMQServices.HandoverHttpSoap11Endpoint objCall = new HandoverMQServices.HandoverHttpSoap11Endpoint();
        	objCall.getBuildingRERAPercentage('Test', 'Test', 'Test', 'Test' );
        test.stopTest();
    }
    
    static testMethod void testGenerateKeyReleaseForm() {
        // TO DO: implement unit test
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, HandoverMQServices.GenerateKeyReleaseFormResponse_element>();
            HandoverMQServices.GenerateKeyReleaseFormResponse_element response1 = new HandoverMQServices.GenerateKeyReleaseFormResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	HandoverMQServices.HandoverHttpSoap11Endpoint objCall = new HandoverMQServices.HandoverHttpSoap11Endpoint();
        	objCall.GenerateKeyReleaseForm('Test', 'Test', 'Test', new list<HandoverMQServicesWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5>() );
        test.stopTest();
    }
}