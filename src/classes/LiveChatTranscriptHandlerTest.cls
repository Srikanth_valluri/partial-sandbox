@isTest
private class LiveChatTranscriptHandlerTest{

    @isTest static void checkPersonAccountEmailSentOrNot() {

        // Insert Account        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__pc = 'test@test.com';
        insert objAcc;
        
        LiveChatVisitor objVisitor = new LiveChatVisitor();
        insert objVisitor;

        Test.startTest();
            LiveChatTranscript objTranscript = new LiveChatTranscript(AccountId = objAcc.Id,LiveChatVisitorId = objVisitor.Id, Body = 'Test');
            insert objTranscript;
        Test.stopTest();
    }

    @isTest static void checkBusinessAccountEmailSentOrNot() {

        // Insert Account        
        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        objAcc.Email__c = 'test@test.com';
        insert objAcc;

        LiveChatVisitor objVisitor = new LiveChatVisitor();
        insert objVisitor;
        Test.startTest();
            LiveChatTranscript objTranscript = new LiveChatTranscript(AccountId = objAcc.Id, LiveChatVisitorId = objVisitor.Id, Body = 'Test');
            insert objTranscript;
        Test.stopTest();
    }
}