@isTest
public Class ProcessEmailTemplateSelectorTest {
    @isTest
    public static void test_1() {
        Test.startTest();
        String templateName = ProcessEmailTemplateSelector.getEmailTemplateName('Invoice Process', 'Indian');
        ProcessEmailTemplateSelector.fetchEmailTemplateRecord(templateName);
        Test.stopTest();
    }
     @isTest
    public static void test_2() {
        Test.startTest();
        String templateName = ProcessEmailTemplateSelector.getEmailTemplateNameForDpInvoice('Invoice Process', 'Indian','Construction');
        ProcessEmailTemplateSelector.fetchEmailTemplateRecord(templateName);
        Test.stopTest();
    }
}