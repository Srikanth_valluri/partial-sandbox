/*
* Test Class for SendEmailtoPB,BookingUnitTriggerHandler
*/
@isTest
private class SendEmailtoPB_Test {
     private static final Id DEAL_SR_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('NSIBPM__Service_Request__c', 'Deal');
    @testSetup static void setupData() {
        
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        insert sr;
        
        NSIBPM__Step_Template__c stpTemplate =  InitializeSRDataTest.createStepTemplate('Deal','MANAGER_APPROVAL');
        insert stptemplate;
        
        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
            Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);
        
        
        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id,stepStatuses.values()[0].id,stptemplate.id);
        insert stp;
        
        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        insert lstbk;
        
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        insert loc;       
        
        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;
        
        Promotion__c newPromo = new Promotion__c();
        newPromo.Promotion_Title__c = 'New Promo';
        newPromo.Start_Date__c = system.now().date();
        newPromo.End_Date__c = system.now().addDays(10).date();
        insert newPromo;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        bu.Related_Promotion__c = newPromo.Id;
        insert bu;
        
        //
        bu.Registration_Status_Code__c = 'abc';
        bu.tBits_Ready__c = true;
        bu.Receipt_ID__c = '67878';
        update bu;
        
        NSIBPM__Document_Master__c newDoc = new NSIBPM__Document_Master__c();
        newDoc.NSIBPM__Doc_ID__c = 'abca12';
        newDoc.Promotion_Letter__c = true;
        insert newDoc;
        
        NSIBPM__SR_Doc__c srdoc = new NSIBPM__SR_Doc__c();
        srdoc.name= 'ppt';
        srdoc.Booking_Unit__c  = bu.id;
        srdoc.NSIBPM__Service_Request__c = sr.id;
        srdoc.NSIBPM__Document_Master__c = newDoc.id;
        insert srdoc;
    }
    
    @isTest static void test_method_1() {
        SendEmailtoPB obj = new SendEmailtoPB();
        NSIBPM__Service_Request__c sr = [select id,name from NSIBPM__Service_Request__c limit 1];
        NSIBPM__Step__c stp = [select id,name,NSIBPM__SR__c from NSIBPM__Step__c where NSIBPM__SR__c =: sr.id];
        string str = obj.EvaluateCustomCode(sr,stp);
    }
    @isTest static void test_method_2() {
        SendEmailtoPB obj = new SendEmailtoPB();
        string str = obj.EvaluateCustomCode(null,null);
    }
    
}