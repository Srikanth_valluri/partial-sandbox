/**************************************************************************************************
* Name               : CC_RegisterAgents                                                          *
* Description        : Custom code invoked on manager review step on agent related SR's.          *
* Created Date       :                                                                            *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION   AUTHOR          DATE            COMMENTS                                              *
* 1.0                                       Initial Draft.                                        *
* 2.0       Rahul Godara    09/07/2017      Now account update on behalf of account Id,           *
*                                           instead of external Id.                               *
* 3.0       Craig Lobo      04/02/2018      Moved the static text of Agency Owner names to Labels *
*                                           (Agency_Owner_Non_UAE, Agency_Owner_UAE)              *
* 4.0       Craig Lobo      25-02-2018      Restrict the Update of Account Recordtype             *
*                                           if Account exist (#232)                               *
* 5.0       Craig Lobo      03-06-2018      Account Owners according to Country and Corporate Type* 
* 6.0       Twinkle P       03/07/2018      Added Null Check For Owner Id                         *
* 7.0       QBurst          20/04/2020      Agent Owner change only for Corporates                *
**************************************************************************************************/
global without sharing class CC_RegisterAgents implements NSIBPM.CustomCodeExecutable, Queueable {
    
    public Id currentSrId;
    public static List<Amendment__c> amd_List = new List<Amendment__c>();
    public static Map<String, Amendment__c> amdMap = new Map<String, Amendment__c>();
    public static Map<String, Contact> conMap = new Map<String, Contact>();
    public RecordType RecordTypePA = [Select Id from RecordType Where developerName = 'Individual_Agency' and SObjectType = 'Account'];
    public String strCurrSR;
    
    // v3.0 Craig Lobo 04/02/2018 
    // Moved the static text of Agency Owner names to Labels
    //public final String agencyOwnerNameUAE = Label.Agency_Owner_UAE;
    //public final String agencyOwnerNameNonUAE = Label.Agency_Owner_Non_UAE;
    
    public CC_RegisterAgents(){
        
    }
    public CC_RegisterAgents(Id currentSrId){
        this.currentSrId = currentSrId;
    }
    
    public String EvaluateCustomCode(NSIBPM__Service_Request__c SR, NSIBPM__Step__c step) {
        String retStr = 'Success';
        strCurrSR = '';
        Account newAcc;
        Account exacc;
        List<id> accids = new List<id>();
        try {
            Boolean bankchange = false;
            NSIBPM__Service_Request__c currSR = getSRDetails(step.NSIBPM__SR__c);
            if(currSR.NSIBPM__Customer__c !=null){
                
                exacc =[Select Id, Name,Vendor_ID__c,Bank_Name__c,Bank_Country__c,
                        Bank_Branch__c,Bank_City__c,Bank_Account_Currency__c,
                        Bank_Account_Number__c,IBAN_Number__c,Swift_Code__c,
                        Sort_Code__c,IFSC_Code__c 
                        FROM Account 
                        WHERE id =:currSR.NSIBPM__Customer__c];
                system.debug('>>>>exacc.Bank_Name__c'+exacc.Bank_Name__c);
                system.debug('>>>>currSR.Bank_Name__c'+currSR.Bank_Name__c);
                
                if(exacc.Bank_Name__c!= currSR.Bank_Name__c)
                    bankchange=true;
                else if(exacc.Bank_Country__c!= currSR.Account_Details_Country__c)
                    bankchange=true;
                else if(exacc.Bank_Branch__c!= currSR.Bank_Branch__c)
                    bankchange=true;
                else if(exacc.Bank_City__c!= currSR.Bank_City__c)
                    bankchange=true;
                else if(exacc.Bank_Account_Currency__c!= currSR.Bank_Account_Currency__c)
                    bankchange=true;
                else if(exacc.Bank_Account_Number__c!= currSR.Account_Number__c)
                    bankchange=true;
                else if(exacc.IBAN_Number__c!= currSR.IBAN_Number__c)
                    bankchange=true;
                else if(exacc.Swift_Code__c!= currSR.Swift_Code__c)
                    bankchange=true;
                else if(exacc.Sort_Code__c!= currSR.Sort_Code__c)
                    bankchange=true;
                else if(exacc.IFSC_Code__c!= currSR.IFSC_Code__c)
                    bankchange=true;
                
            }
            amd_List =  getAllAmendments(currSR.Id);
            currentSrId = step.NSIBPM__SR__c;
            //AmendmentID -->Amendment
            for (Amendment__c amd : amd_List) {
                amdMap.put(amd.Id, amd);
            }
            
            //Cretae portal user/contacts for Corporate Agency SRs
            if (currSR.Agency_type__c == 'Corporate') {
                System.debug('==>Corporate Agency');
                //newAcc = createAccount(currSR);
                id corporrateRecordTypeId=  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
                newAcc=UpsertAccount(currSR,corporrateRecordTypeId,false);
                System.debug('==>newAcc'+ newAcc);
                //Update SR to lookup on account
                if(newAcc != null){
                    currSR.NSIBPM__Customer__c = newAcc.Id;
                    update currSR;
                    // Stamp the Account on the Inquiry related to the SR 
                    if (currSR.Inquiry__c != null) {
                        Inquiry__c relatedInquiry = new Inquiry__c(
                            Id = currSR.Inquiry__c, 
                            Agency_Name__c = newAcc.Id,
                            Bypass_Inquiry_Status__c = true,
                            Inquiry_Status__c = 'Agent Converted'
                        );
                        update relatedInquiry;
                        
                    }
                    if(currSR.NSIBPM__Record_Type_Name__c == Label.ManageAgencySiteInsert)
                        SRUtility.CreateChatterGroup(new map<id,account>{newAcc.id => newAcc});
                    //Create Contacts
                    List<Contact> newCon_List = createContacts(currSR, newAcc);
                    for (Contact con :  newCon_List) {
                        conMap.put(con.AmendmentID__c, Con);
                    }
                    //Update amendment Acc/Contact lookups
                    amd_List = updateAmendmentLookups(newAcc);
                    System.debug('amd_List==>>' + amd_List);
                    System.enqueueJob(new CC_RegisterAgents(currSR.Id));
                }
            } else {
                System.debug('==>Individual Agency');
                String keyVal = currSR.First_Name__c + currSR.Last_Name__c + currSR.Agency_Mobile__c;
                System.debug('keyVal ==>'+keyVal);
                if(keyVal != NULL && keyVal != '')
                    if (!checkForDuplicatePersonAccounts(keyVal,currSR.NSIBPM__Customer__c)) {
                        System.debug('==>No Duplicate');
                        //newAcc =  createPersonAccount(currSR);
                        newAcc=UpsertAccount(currSR,RecordTypePA.Id,true);
                        if(newAcc != null){
                            currSR.NSIBPM__Customer__c = newAcc.Id;
                            for(user u: [select id,name,profile.name,Profile.UserLicense.Name,contactid from user where id = : currSR.createdbyid]){
                                if(u.profile.name == 'Property Consultant' && currSR.NSIBPM__Record_Type_Name__c == 'Agent_Registration'){
                                       addpctoAccount(currSR.NSIBPM__Customer__c,u.id,u.name);
                                   }
                            }
                            update currSR;
                            // Stamp the Account on the Inquiry related to the SR 
                            if (currSR.Inquiry__c != null) {
                                Inquiry__c relatedInquiry = new Inquiry__c(
                                    Id = currSR.Inquiry__c, 
                                    Agency_Name__c = newAcc.Id,
                                    Bypass_Inquiry_Status__c = true,
                                    Inquiry_Status__c = 'Agent Converted'
                                );
                                update relatedInquiry;
                            }
                            System.debug('==>' + newAcc);
                        }
                    } else {
                        retStr = 'Duplicate Person Account.';
                        return retstr;
                    }
            }
            
            if(newAcc.id!=null){
                accids.add(newAcc.id);
            }
            if(accids.size()>0){
                IF(currSR.NSIBPM__Record_Type_Name__c == Label.ManageAgencySiteInsert)
                    system.enqueueJob(new AsyncAgentWebservice (accids,'Agent Creation'));
                if(bankchange)
                    system.enqueueJob(new AsyncAgentWebservice (accids,'Bank Updation'));
                
            }
            
            if(currSR.NSIBPM__Customer__c != null){
                if(currSR.NSIBPM__Record_Type_Name__c == Label.ManageAgencySiteUpdate && currSR.Country_of_Sale__c != null){
                    ManageAgencySites.UpdateAgencySites(currSR.Country_of_Sale__c,currSR.NSIBPM__Customer__c);
                }
            }
        } catch (Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
        }
        return retStr;
    }
    
    public void execute(QueueableContext qc) {
        system.debug('createPortalUser>>>>  = '+currentSrId);
        createPortalUser(currentSrId);
    }
    
    public List<Amendment__c> updateAmendmentLookups(Account acc) {
        List<Amendment__c> amdUpdateList = new List<Amendment__c>();
        for (Amendment__c amd :  amd_List) {
            amd.Account__c = acc.Id;
            amd.Contact__c = conMap.get(amd.Id).Id;
            amdUpdateList.add(amd);
        }
        update amdUpdateList;
        return amdUpdateList;
    }
    
    public Account UpsertAccount(NSIBPM__Service_Request__c currSR,id recordTypeId,boolean isPersonAccount) {
        try{
            Account personAcc  = new Account(id=currSR.NSIBPM__Customer__c);
            
            // v4.0 Craig 25/02/2018  #232
            // If Account exist assigned previous Record Type of Account while upserting
            if (currSR.NSIBPM__Customer__c != null) {
                List<Account> accList = [ SELECT Id, Name, RecordTypeId 
                                         FROM Account 
                                         WHERE Id = :currSR.NSIBPM__Customer__c
                                         LIMIT 1
                                        ];
                if(!accList.isEmpty()) {
                    personAcc.RecordTypeId = accList[0].RecordTypeId;
                }
            } else {
                personAcc.RecordTypeId = recordTypeId;
            }
            
            system.debug('>>>>personAcc>>>>'+personAcc);
            system.debug('>>>>personAcc.RecorrdTypeId>>>>'+personAcc.RecordTypeId);
            // v4.0 Craig 25/02/2018 
            
            // Map SR fields to Account Record
            List<Account_SR_Field_Mapping__c> CS = Account_SR_Field_Mapping__c.getAll().values();
            for (Account_SR_Field_Mapping__c mapping : CS) {
                if((isPersonAccount && (mapping.Is_Person_Account__c || mapping.Is_Common_to_All_RT__c)) ||
                   (!isPersonAccount && mapping.Is_Common_to_All_RT__c) ||
                   (!isPersonAccount && !mapping.Is_Person_Account__c && !mapping.Is_Common_to_All_RT__c)){
                       //if(currSR.get(mapping.SR_Field__c) != null){ //ALOK 25/Oct/2017 Commented For Bug #131
                       system.debug('>>>>>>>>'+mapping.Account_Field__c);
                       system.debug('>>>>>>>>'+currSR.get(mapping.SR_Field__c));
                       personAcc.put(mapping.Account_Field__c, currSR.get(mapping.SR_Field__c));
                       //}
                   }
            }
            
            system.debug('personAcc>>>>  = ' + personAcc.OwnerId);
            system.debug('personAcc>>>>  = ' + personAcc.Agency_Type__c);
            system.debug('isPersonAccount>>>>  = '+isPersonAccount);
            String countryName = '';
            if(!isPersonAccount){
                system.debug('Inside>>>>  = ');
                personAcc.put('Name', currSR.get('Agency_Name__c'));
                countryName = currSR.Country_Of_Incorporation_New__c;
            } else {
                countryName = currSR.Country__c;
            }
            if (personAcc.Agency_Type__c == 'Corporate' && personAcc.Id == null ) { // 7.0
                String recordOwnerId = getAgencyOwnerId(currSR.Country__c, currSR.Agency_Corporate_Type__c, currSR.Country_Of_Incorporation_New__c);
                if (String.isNotBlank(recordOwnerId)) {
                    personAcc.put('OwnerId', recordOwnerId);
                }
            }
            system.debug('personAcc CCRegister >>> '+personAcc.ownerId);
            upsert personAcc ;
            return personAcc;
        } catch(exception ex){
            system.debug('Exception at line number = '+ex.getLineNumber()+', Exception message = '+ex.getMessage());
            return null;
        }
    }
    
    public boolean checkForDuplicatePersonAccounts(String keyVal,id accID) {
        Boolean flag ;
        strCurrSR = '';
        List<Account> acc_List = new List<Account>();
        if(accID != null){
            for(Account a : [SELECT Id, RecordType.developerName FROM Account
                             WHERE RecordType.developerName = 'Individual_Agency' AND
                             PersonAccountKey__c =: keyVal AND
                             PersonAccountKey__c != NULL AND
                             Id != : accID LIMIT 1] ){
                                 acc_List.add(a);
                             }
        }else{
            for(Account a : [SELECT Id, RecordType.developerName FROM Account
                             WHERE RecordType.developerName = 'Individual_Agency' AND
                             PersonAccountKey__c =: keyVal AND
                             PersonAccountKey__c != NULL LIMIT 1] ){
                                 acc_List.add(a);
                             }
        }
        flag = (acc_List.Size() > 0) ? true : false;
        return flag;
    }
    
    public String retPrefix() {
        Integer y = system.now().year();
        Integer m = system.now().month();
        Integer d = system.now().day();
        Integer hr = system.now().hour();
        Integer min = system.now().minute();
        Integer sec = system.now().second();
        String t = string.valueOf(y) + string.valueOf(m) + string.valueOf(d) + string.valueOf(hr) + string.valueOf(min) + string.valueOf(sec);
        return t;
    }
    
    public static String randomWithLimit(Integer upperLimit) {
        Integer rand = Math.round(Math.random() * 1000);
        return String.valueOf(Math.mod(rand, 9999)).leftpad(4, '0');
    }
    
    
    public static string getUserName(Contact c) {
        String username = '';
        if (c.FirstName != NULL) {
            username += c.FirstName.replaceAll( '\\s+', '');
        }
        if (c.LastName != NULL) {
            username += c.LastName.replaceAll( '\\s+', '');
        }
        username += randomWithLimit(9999);
        username += '@damacagents.com';
        return username;
    }
    
    public static String getUserProfile(Amendment__c amd) {
        //added by Cloudzlab
        String profileName = 'Customer Community - Agent';
        if (amd.Portal_Administrator__c && !amd.Authorised_Signatory__c && !amd.Agent_Representative__c && !amd.Owner__c) {
            profileName = 'Customer Community - Admin';
        }
        if (amd.Agent_Representative__c && !amd.Authorised_Signatory__c && !amd.Owner__c && !amd.Portal_Administrator__c) {
            profileName = 'Customer Community - Agent';
        }
        if (amd.Agent_Representative__c && amd.Portal_Administrator__c && !amd.Owner__c && !amd.Authorised_Signatory__c) {
            profileName = 'Customer Community - Agent + Admin';
        }
        if(amd.Agent_Representative__c && amd.Owner__c && !amd.Portal_Administrator__c && !amd.Authorised_Signatory__c){
            profileName = 'Customer Community - Super User';
        }
        if(amd.Agent_Representative__c && amd.Authorised_Signatory__c && !amd.Owner__c && !amd.Portal_Administrator__c){
            profileName = 'Customer Community - Super User';
        }
        if(amd.Agent_Representative__c && amd.Owner__c && amd.Authorised_Signatory__c && amd.Portal_Administrator__c){
            profileName = 'Customer Community - Super User';
        }
        if(amd.Agent_Representative__c && amd.Owner__c && amd.Authorised_Signatory__c && !amd.Portal_Administrator__c){
            profileName = 'Customer Community - Super User';
        }
        if(amd.Agent_Representative__c && amd.Authorised_Signatory__c && amd.Portal_Administrator__c && !amd.Owner__c){
            profileName = 'Customer Community - Super User';
        }
        if(amd.Agent_Representative__c && amd.Owner__c && amd.Portal_Administrator__c && !amd.Authorised_Signatory__c){
            profileName = 'Customer Community - Super User';
        }
        if(amd.Portal_Administrator__c && amd.Owner__c && amd.Authorised_Signatory__c && !amd.Agent_Representative__c){
            profileName = 'Customer Community - Super User';
        }
        if(amd.Owner__c && amd.Authorised_Signatory__c && !amd.Portal_Administrator__c && !amd.Agent_Representative__c){
            profileName = 'Customer Community - Super User';
        }
        if(amd.Authorised_Signatory__c && !amd.Owner__c && !amd.Agent_Representative__c && !amd.Portal_Administrator__c){
            profileName = 'Customer Community - Super User';
        }
        if(amd.Portal_Administrator__c && amd.Owner__c && !amd.Authorised_Signatory__c && !amd.Agent_Representative__c){
            profileName = 'Customer Community - Super User';
        }
        if(amd.Portal_Administrator__c && amd.Authorised_Signatory__c && !amd.Agent_Representative__c && !amd.Owner__c){
            profileName = 'Customer Community - Super User';
        }
        return profileName;
    }
    
    public static void addpctoAccount(id accid,id userid,string username){
        Agency_PC__c agp = new Agency_PC__c(Agency__c = accid,User__c = userid,Name = username);
        //insert agp;
    }
    
    public static void createPortalUser( Id SrId) {
        NSIBPM__Service_Request__c currSR = getSRDetails(SrId);
        System.debug('#### createPortalUser = ' + currSR);
        //skip the creation of user if a SR is created from users who belongs to profiles mentioned in the label.
        for(user u : [SELECT Id, Name, Profile.Name, Profile.UserLicense.Name, ContactId 
                      FROM User where Id = : currSR.CreatedbyId]) {
            if(u.profile.name == 'Property Consultant' && currSR.NSIBPM__Record_Type_Name__c == 'Agent_Registration'){
                addpctoAccount(currSR.NSIBPM__Customer__c, u.id,u.name);
            }
        }
        
        List<String> portalProfileName = new List<String> {
            'Customer Community - Admin', 
                'Customer Community - Agent', 
                'Customer Community - Agent + Admin', 
                'Customer Community - Agent + Admin + Auth', 
                'Customer Community - Auth + Admin',
                'Customer Community - Auth + Agent', 
                'Customer Community - Owner',
                'Customer Community - Super User',    
                'Customer Community - Auth Officer' //ALOK 27/Dec/2017 Added for Portal User Creation Bug
                };
                    Map<String, Id> profileMap = new Map<String, Id>();
        for (Profile p : [select id, Name from profile where Name IN :portalProfileName]) {
            profileMap.put(p.Name, p.Id);
        }
        System.debug('#### profileMap = ' + profileMap);
        List<User> portalUserList = new List<User>();
        User u ;
        Database.DMLOptions dlo = new Database.DMLOptions();
        dlo.EmailHeader.triggerUserEmail = true;
        dlo.EmailHeader.triggerAutoResponseEmail = true;
        system.debug('#### currentSrId = '+SrId);
        
        System.debug('***'+currSR.NSIBPM__Customer__c);
        Id accId = currSR.NSIBPM__Customer__c;
        
        //AmendmentID -->Amendment
        for (Amendment__c amd : getAllAmendments(SrId)) {
            amdMap.put(amd.Id, amd);
        }
        system.debug('#### amdMap = ' + amdMap);
        String conQuery = ' SELECT FirstName, Passport_Number__c, LastName, Designation__c, ' 
            + ' Nationality__c, Broker_Card_Expiry_Date__c, AmendmentID__c, ' 
            + ' Broker_Card_Number__c, MobilePhone, AccountId, ID_Type__c , Email, ' 
            + ' Id, Shareholding__c, Is_Eligible_To_Sell__c, Agent_Representative__c, ' 
            + ' Owner__c, Authorised_Signatory__c, Portal_Administrator__c '
            + ' FROM Contact ';
        conQuery += ' WHERE AccountId = :accId ';
        for (Contact con :  Database.Query(conQuery)) {
            conMap.put(con.AmendmentID__c, Con);
        }
        system.debug('#### conMap = ' + conMap);
        Map<Id,user> conIdUserMap = getUserContactMap(currSR.NSIBPM__Customer__c);
        
        System.debug('amdMap.Values()=>' + amdMap.Values());
        if (currSR.Agency_type__c == 'Corporate') {
            for (Amendment__c amd : amdMap.Values()) {
                Contact con = conMap.get(amd.Id);
                //Check if Contact Already has an user associated to update it
                ID userId = (conIdUserMap.get(con.id) != NULL) ? conIdUserMap.get(con.id).Id : NULL;
                System.debug('User Id : '+userId);
                u =  new User( Id= userId);
                
                String UserName = '';
                String aliasVal = String.valueOf(con.Id);
                u.alias = aliasVal.substring(0, 5);
                u.email = con.Email;
                u.Is_Eligible_To_Sell__c = amd.Is_Eligible_To_Sell__c;
                u.emailencodingkey = 'UTF-8';
                u.timezonesidkey = 'Asia/Dubai';
                u.FirstName = con.FirstName;
                u.LastName = con.LastName;
                //Skip assigning of contact for update operation
                if(userId == NULL){
                    u.ContactId = con.Id;
                }
                u.ProfileId = profileMap.get(getUserProfile(amd));
                u.languagelocalekey = 'en_US';
                u.localesidkey = 'en_GB';
                if(userId == null)
                    u.username = getUserName(con);
                u.setOptions(dlo);
                portalUserList.add(u);
            }
        }
        System.debug('portalUserList==>' + portalUserList);
        
        //Added by CloudzLab, reference: DAM-164
        if (!Test.isRunningTest() && !portalUserList.isEmpty() ) {
            /*Skip_Validation_Rules__c SkipUserValidation = Skip_Validation_Rules__c.getInstance();
            SkipUserValidation.Skip__c = true;
            upsert SkipUserValidation;
            */
            Database.Upsert(portalUserList,User.Fields.Id,true);
            /*
            SkipUserValidation.Skip__c = false;
            upsert SkipUserValidation;*/
        }
    }
    
    public static List<Amendment__c> getAllAmendments(Id SrId) {
        NSIBPM__Service_Request__c currentSR = [SELECT Id, OwnerId
                                                FROM NSIBPM__Service_Request__c
                                                WHERE Id =  :SrId];
        String amendmentQuery = UtilityQueryManager.getAllFields(Amendment__c.getsObjectType().getDescribe());
        amendmentQuery = amendmentQuery.replaceFirst(' SELECT ', ' SELECT Contact__r.OwnerId, ');
        amendmentQuery += ' WHERE Service_Request__c =: SRId AND OwnerId = \''
            + currentSR.OwnerId + '\' ';
        return (Database.query(amendmentQuery));
    }
    
    public List<Contact> createContacts(NSIBPM__Service_Request__c SR, Account newAcc) {
        strCurrSR = '';
        Id agencyContactRTId =
            Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Agency Contact').getRecordTypeId();
        Id SRId = SR.Id;
        Id srOwnerId = SR.OwnerId;
        System.debug('srOwnerId==>' + srOwnerId);
        List<Contact> conList = new List<Contact>();
        Contact newCon;
        System.debug('SR==>' + SRId);
        List<User> srOwnerList = [SELECT Id, Name, IsPortalEnabled, User_License_type__c 
                                  FROM User 
                                  WHERE Id = :srOwnerId LIMIT 1];
        
        for (Amendment__c amd : amdMap.values()) {
            newCon = new Contact(id = amd.Contact__c);
            system.debug (amd.contact__c);
            List<User> userList = new List<User>();
            //Added By Twinkle to Avoid Owner Update If the Owner is a Queue
            if(srOwnerList != null && !srOwnerList.isEmpty()) {
                System.debug('srOwnerList==> ' + srOwnerList);
                System.debug('srOwnerList[0].IsPortalEnabled ==> ' + srOwnerList[0].User_License_type__c );
                System.debug('srOwnerList[0].IsPortalEnabled ==> ' + srOwnerList[0].IsPortalEnabled );
                if (srOwnerList[0].IsPortalEnabled == true 
                    || srOwnerList[0].User_License_type__c == 'Guest User License') {
                        if (amd.Contact__c == null) {
                            // Craig 03-06-2018  > Map Account Owners
                            system.debug('Country_Of_Incorporation_New__c>>>>  = ' + SR.Country_Of_Incorporation_New__c); 
                            String recordOwnerId = getAgencyOwnerId(
                                SR.Country__c, 
                                SR.Agency_Corporate_Type__c,
                                SR.Country_Of_Incorporation_New__c
                            );
                            System.debug('recordOwnerId ==> ' + recordOwnerId );
                            if (String.isNotBlank(recordOwnerId)) {
                                newCon.OwnerId = recordOwnerId;
                            }
                        }
                    } else if (srOwnerList[0].IsPortalEnabled == false) {
                        if (amd.Contact__c == null) {
                            newCon.OwnerId = srOwnerId;
                        }
                    }
            }
            newCon.FirstName =  amd.First_Name__c;
            newCon.LastName = amd.Last_Name__c;
            newCon.AccountId = newAcc.Id;
            newCon.Email = amd.Email__c;
            newCon.ID_Issue_Date__c = amd.ID_Issue_Date__c;
            newCon.MobilePhone = amd.Mobile__c;
            newCon.Shareholding__c = amd.Shareholding__c;
            newCon.Designation__c = amd.Designation__c;
            newCon.Country_Of_Origin__c = amd.Nationality__c;
            newCon.Salutation = amd.Title__c;
            newCon.ID_Number__c = amd.ID_Number__c;
            newCon.AmendmentID__c = amd.Id;
            newCon.ID_Type__c = amd.ID_Type__c;
            newCon.Date_of_Birth__c = amd.BirthDate__c;
            newCon.Birthdate = amd.BirthDate__c;
            newCon.Broker_Card_Expiry_Date__c = amd.Broker_Card_Expiry_Date__c;
            newCon.Broker_Card_Number__c = amd.Broker_Card_Number__c;
            newCon.Is_Eligible_To_Sell__c = amd.Is_Eligible_To_Sell__c;
            newCon.Agent_Representative__c = amd.Agent_Representative__c;
            newCon.Owner__c = amd.Owner__c;
            newCon.Mobile_Country_Code__c = amd.Mobile_Country_Code__c;
            newCon.Id_Expiry_date__c = amd.ID_Expiry_Date__c;
            newCon.Authorised_Signatory__c = amd.Authorised_Signatory__c;
            newCon.Portal_Administrator__c = amd.Portal_Administrator__c;
            if(amd.Mark_for_Deletion__c)
                newCon.Status__c = 'Cancelled';
            newCon.RecordTypeId  = agencyContactRTId;
            conList.add(newCon);
        }
        System.debug('conList==>' + conList);
        upsert conList;
        return conList;
    }
    
    public static NSIBPM__Service_Request__c getSRDetails(Id SRId) {
        NSIBPM__Service_Request__c currSR = new NSIBPM__Service_Request__c();
        DescribeSObjectResult describeResult = NSIBPM__Service_Request__c.getSObjectType().getDescribe();
        List<String> fieldNames = new List<String> ( describeResult.fields.getMap().keySet() );
        String query = ' SELECT ' + String.join( fieldNames, ',' ) + ' FROM ' + describeResult.getName();
        query += ' WHERE Id =: SRId LIMIT 1';
        System.debug('==>' + query);
        currSR = Database.query(query);
        return currSR;
    }
    
    /**
    * Map the Account Owners according to Country and Corporate Type
    */
    public String getAgencyOwnerId(String pAgencyCountry, String pAgencyCorporateType, String pCountryOfIncorp) {
        String agencyOwnerId;
        String agencyOwnerName;
        Agency_Owner__c agencyOwnerRec;
        System.debug('pCountryOfIncorp==>' + pCountryOfIncorp);
        System.debug('pAgencyCountry==>' + pAgencyCountry);
        if (String.isNotBlank(pCountryOfIncorp)) {
            agencyOwnerRec = Agency_Owner__c.getInstance(pCountryOfIncorp);
        } else if(String.isNotBlank(pAgencyCountry)){
            agencyOwnerRec = Agency_Owner__c.getInstance(pAgencyCountry);
        } 
       
        /* ============================================ 03-01-2019 ========================================== */
        if (agencyOwnerRec == null) { 
            agencyOwnerRec = Agency_Owner__c.getInstance('All Except Above');
        }
        
        /* ================================================================================================== */
        system.debug('agencyOwnerRec>>>>  = '+agencyOwnerRec);
        system.debug('agencyOwnerName>>>>  = '+agencyOwnerName);
        if (agencyOwnerRec != null && String.isNotBlank(agencyOwnerRec.User_Name__c)) {
            List<User> userList = [ SELECT Id, Name 
                                  FROM User 
                                  WHERE Name = :agencyOwnerRec.User_Name__c 
                                  LIMIT 1];
            system.debug('agencyOwnerName>>>>  = ' + userList);
            if (userList != null && !userList.isEmpty()) {
               agencyOwnerId = userList[0].Id;
               system.debug('agencyOwnerId>>>>  = '+agencyOwnerId);
            }
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select the valid owner'));
        }
        return agencyOwnerId;
    }
    
    
    
    public static Map<Id,user> getUserContactMap(Id AccId){
        Map<Id,user>conIdUsermap = new Map<Id,User>();
        for(User u :  [Select Id,FirstName,LastName,ContactId from user Where ContactId IN (Select Id From Contact where AccountId =:AccId)]){
            conIdUsermap.put(u.ContactId,u);
            
        }
        return conIdUserMap;
    }
}// End of class.