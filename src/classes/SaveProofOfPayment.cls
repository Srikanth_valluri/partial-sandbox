@RestResource(urlMapping='/SaveProofOfPayment/*')
 Global class SaveProofOfPayment
 {
     @HtTPPost
    Global static Case ProofOfPayment(ProofOfPaymentWrapper proofOfPaymentWrapper)
    {
      
                    case objcase = new case();
                    objcase.RecordtypeID =Schema.SObjectType.case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
                     Group g=[select Id,name from Group where Type = 'Queue' and name='Collection Queue'];
                   
                    if(proofOfPaymentWrapper.Totalamount>0)
                    objcase.Total_Amount__c =  proofOfPaymentWrapper.Totalamount;
                    objcase.Payment_Allocation_Details__c = proofOfPaymentWrapper.PARemark;
                    objcase.Payment_Mode__c = proofOfPaymentWrapper.PaymentMode;
                    objcase.Payment_Date__c =proofOfPaymentWrapper.PaymentDate;
                    objcase.Payment_Currency__c = proofOfPaymentWrapper.PaymentCurrency;
                    objcase.Sender_Name__c = proofOfPaymentWrapper.SenderName;
                    objcase.Cheque_Bank_Name__c = proofOfPaymentWrapper.BankName;
                    objcase.Swift_Code__c = proofOfPaymentWrapper.SwiftCode;
                    objcase.Status = proofOfPaymentWrapper.status;
                    objcase.Origin = proofOfPaymentWrapper.origin;
                     objcase.Type = 'Proof of Payment SR';
                     objcase.SR_Type__c = 'Proof of Payment SR';
                     list<Contact>c=[select id from Contact where AccountID=:proofOfPaymentWrapper.AccountId];
                    list<user>u=[select id from user where ContactID=:c[0].id];
                     objcase.OwnerId = g.id;
                        objcase.id=proofOfPaymentWrapper.salesforceId;
                  //  objcase = setCaseOwner(objcase);
                   try{
                       upsert objCase;
                       }catch(DMLException e)
                       {
                          objcase.Payment_Terms_Error__c=e.getMessage();
                       return objcase; 
                       }
                       
                   //   List<SR_Booking_Unit__c> lstSRbooking = new List<SR_Booking_Unit__c>();
                   // for(Booking_Unit__c objBooking : selectedBookingUnit){
                        SR_Booking_Unit__c objSRBooking = new SR_Booking_Unit__c();
                        objSRBooking.Booking_Unit__c = proofOfPaymentWrapper.bookingUnitId;
                        objSRBooking.Case__c = objCase.id;
                        insert objSRBooking;
                       // lstSRbooking.add(objSRBooking);
                    //}
                   // if (lstSRbooking.size() > 0) {
                       // insert lstSRbooking;
                   // }


                    
                     
                    SR_Attachments__c objSRatt = new SR_Attachments__c ();
                    objSRatt.case__c = objCase.id;
                    objSRatt.Type__C = 'POP';
                    objSRatt.Name = ProofOfPaymentWrapper.fileName;
                    if(String.isNotBlank(proofOfPaymentWrapper.AttachmentURL)){
                         objSRatt.Attachment_URL__c = proofOfPaymentWrapper.AttachmentURL;
                        }
                       
                    insert objSRatt;    
                      Task objTask = new Task();
                    objTask.WhatId = objcase.id;
                    objTask.Subject = 'Verify Proof of Payment Details in IPMS';
                    objTask.ActivityDate = system.today().addDays(1);
                    objTask.Assigned_User__c = 'Finance';
                   /* objTask = TaskUtility.getTask( (SObject)objCase, 'Verify Case Details', 'CRE', 
                                   'POP', system.today().addDays(1) );
                    objTask.OwnerId = Label.DefaultCaseOwnerId;*/
                    objTask.Document_URL__c = objSRatt.Attachment_URL__c;
                    objTask.Process_Name__c ='POP';
                    objTask.Priority = 'High';
                    objTask.Status = 'Not Started';
                      if(objcase.Status=='Submitted' && !test.isrunningtest())
                    { 
                     
                 
                   
                   
                    insert objTask;
                    
                    }
                    return objcase;
    
    }
    
     /*  private static Case setCaseOwner(Case srPop) {
        List<QueueSobject> lstQueue = [
            SELECT  Id, Queue.Name, QueueId FROM QueueSobject
            WHERE   SobjectType = 'Case' AND Queue.Name = 'Collection Queue'
            LIMIT   1
        ];
        if (!lstQueue.isEmpty()) {
            srPop.OwnerId = lstQueue[0].QueueId;
        }
        return srPop;
    }*/
      Global class ProofOfPaymentWrapper{
        public String fileName;
        public String AttachmentURL;
        public Decimal Totalamount;
        public String PARemark;
        public String PaymentMode;  
        public Date PaymentDate;  
        public String PaymentCurrency;
        public String SenderName;
        public String BankName;
        public String AccountId;
        public String SwiftCode;
        public String RecordType;
         public String status;
          public String origin;
          public String salesforceId;
           public String bookingUnitId;
      
    }
 }