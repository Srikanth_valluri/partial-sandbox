/*-------------------------------------------------------------------------------------------------
Description: Helper class for Calling List handler

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 06-12-2017       | Lochana Rajput    | 1. Send email with PDF as attachment containing to Instructors containing
   =============================================================================================================================
*/
public without sharing class CallingListBouncedChequeHelper {

    //Method called on 'Before Update' event
    public static void OpenCasesForBouncedChequeCallingList(List<Calling_List__c> lstCallingList_old, List<Calling_List__c> lstCallingList_new) {
        Set<Id> setCaseIds = new Set<Id>();
        Set<Id> setBUIds = new Set<Id>();
        Set<Id> setCaseRelatedBUIds = new Set<Id>();
        Map<Id, List<Calling_List__c>> mapCaseId_lstCallingList = new Map<Id, List<Calling_List__c>>();
        Map<Id, Id> mapBUId_CaseID = new Map<Id, Id>();
        Id bouncedChequeCLRTId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Bounced Cheque').getRecordTypeId();
        Id AOPTCaseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('AOPT').getRecordTypeId();
        for(Integer i=0; i< lstCallingList_old.size(); i++) {
            if( /*String.isNotBlank(lstCallingList_old[i].Call_Outcome__c)
                &&*/ String.isNotBlank(lstCallingList_new[i].Call_Outcome__c)
                && lstCallingList_old[i].Call_Outcome__c != lstCallingList_new[i].Call_Outcome__c
                && lstCallingList_new[i].Call_Outcome__c.equalsIgnoreCase('AOPT Under process')
                && lstCallingList_new[i].RecordTypeId == bouncedChequeCLRTId) {

                setCaseIds.add(lstCallingList_new[i].Case__c);
                if(mapCaseId_lstCallingList.containsKey(lstCallingList_new[i].Case__c)) {
                    mapCaseId_lstCallingList.get(lstCallingList_new[i].Case__c).add(lstCallingList_new[i]);
                }
                else {
                    mapCaseId_lstCallingList.put(lstCallingList_new[i].Case__c, new Calling_List__c[] {lstCallingList_new[i]});
                }
            }
        }//for
        System.debug('==setCaseIds===' + setCaseIds);
        System.debug('==mapCaseId_lstCallingList===' + mapCaseId_lstCallingList);
        //Get Booking Units ids
        for(Case objCase : [SELECT ID,
                                (SELECT Booking_Unit__c, Case__c
                                FROM SR_Booking_Units__r
                                WHERE Booking_Unit__r.Registration_ID__c != NULL)
                                FROM Case
                                WHERE ID IN : setCaseIds
                                AND ID IN (SELECT Case__c FROM SR_Booking_Unit__C
                                WHERE Booking_Unit__c != NULL)]) {
            for(SR_Booking_Unit__c objSRBU : objCase.SR_Booking_Units__r) {
                setBUIds.add(objSRBU.Booking_Unit__c);
                mapBUId_CaseID.put(objSRBU.Booking_Unit__c, objCase.Id);
            }
        }//outer for
        System.debug('==setBUIds===' + setBUIds);
        System.debug('==mapBUId_CaseID===' + mapBUId_CaseID);
        //Get AOPT Open Cases for booking units
        for(Case objCase : [SELECT Id, Booking_Unit__c
                            FROM Case
                            WHERE Booking_Unit__c IN :setBUIds
                            AND (Status = 'Closed'
                            OR Status = 'Rejected'
                            OR Status = 'Cancelled')
                            AND RecordTypeId =: AOPTCaseRTId]) {
            setCaseRelatedBUIds.add(objCase.Booking_Unit__c);
        }
        System.debug('==setCaseRelatedBUIds===' + setCaseRelatedBUIds);
        if(setCaseRelatedBUIds.size() > 0) {
            for(Id BUId : setCaseRelatedBUIds) {
                System.debug('==BUId===' + BUId);
                if(mapBUId_CaseID.containsKey(BUId)) {
                    if(mapCaseId_lstCallingList.containsKey((mapBUId_CaseID.get(BUId)))) {
                        for(Calling_List__c objCallingList : mapCaseId_lstCallingList.get((mapBUId_CaseID.get(BUId)))) {
                            objCallingList.addError('Call outcome cannot be updated to AOPT Under Process since there are no open AOPT cases');
                        }
                    }
                }
            }//for
        }
        else {
            for(Calling_List__c objCallingList : lstCallingList_new) {
                if(objCallingList.RecordTypeId == bouncedChequeCLRTId
                    && String.isNotBlank(objCallingList.Call_Outcome__c)
                    && objCallingList.Call_Outcome__c.equalsIgnoreCase('AOPT Under process')) {
                    objCallingList.addError('Call outcome cannot be updated to AOPT Under Process since there are no open AOPT cases');
                }

            }//for
        }

    }
}