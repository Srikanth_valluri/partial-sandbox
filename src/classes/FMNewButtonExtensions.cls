/*-------------------------------------------------------------------------------------------------
Description: Extensions for FMNewButtonPage page
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 11-10-2018       | Lochana Rajput   | 1. Display message to create SR from Account detail page
=============================================================================================================================
*/

public with sharing class FMNewButtonExtensions {
	public String strMessage {get;set;}
	public String retURL {get;set;}
    public FMNewButtonExtensions(ApexPages.StandardController stdController) {
        strMessage = 'Please initiate request from Account using \'Create FM SR\' button';
		retURL = ApexPages.currentPage().getParameters().get('retURL');
    }
}