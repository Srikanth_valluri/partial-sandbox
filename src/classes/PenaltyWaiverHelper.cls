public with sharing class PenaltyWaiverHelper {
    
    //public static final set<String> setRecordTypesName = new set<String>{'Bounced_Cheque_SR', 'Fund_Transfer', 'Token_Refund', 'Customer_Refund' };
     
    public static CaseListWrapper getCaseAndUnitDetailsWrapper( Booking_Unit__c objUnit, set<String> setRecordTypesName ){//Id bookingUnitId ) {
        CaseListWrapper objCaseListWrapper = new CaseListWrapper();
        if(objUnit != null){
            set<Id> setCaseId = populateCaseInfoWrapper( PenaltyWaiverUtility.getAllRelatedCases( objUnit.Id ), objCaseListWrapper, true, objUnit, setRecordTypesName );
            
            set<Id> setCaseIdFromJunc = new set<Id>();
            for( SR_Booking_Unit__c objJunc : PenaltyWaiverUtility.getAllRelatedJunctions( objUnit.Id ) ) {
                if( !setCaseId.contains( objJunc.Case__c ) ) {
                    setCaseIdFromJunc.add( objJunc.Case__c );
                }
            }
            
            if( !setCaseIdFromJunc.isEmpty() ) {
                populateCaseInfoWrapper( PenaltyWaiverUtility.getCaseMap( setCaseIdFromJunc ).values(), objCaseListWrapper, false, objUnit, setRecordTypesName ) ;
            }
        }
        return objCaseListWrapper;
    }
    
    @testVisible
    private static set<Id> populateCaseInfoWrapper( list<Case> lstCases, CaseListWrapper objCaseListWrapper, 
                                                    boolean extractCaseIds, Booking_Unit__c objUnit, set<String> setRecordTypesName ) {
        set<Id> setCaseId = new set<Id>();
        for( Case objCase : lstCases ) {
            if( extractCaseIds ) {
                setCaseId.add( objCase.Id );
            }
            if( String.isNotBlank( objCase.Status ) ) {
                if( objCase.Status.equalsIgnoreCase( 'Closed' ) || objCase.Status.equalsIgnoreCase( 'Rejected' ) || 
                    objCase.Status.equalsIgnoreCase( 'Cancelled' ) ) {
                    objCaseListWrapper.lstCloseCase.add( objCase );
                }
                else {
                    objCaseListWrapper.lstOpenCase.add( objCase );
                    if( objCase.RecordTypeId != null && ( setRecordTypesName.contains( objCase.RecordType.DeveloperName ) || 
                          ( objUnit.Early_Handover__c && objUnit.Rental_Pool__c ) ) ) {
                        objCaseListWrapper.isAllowedToOpenPenaltyCase = false ;
                        objCaseListWrapper.strOpenSrType = objCase.RecordType.Name ;
                    }
                }
            }
            
            System.debug('RecordTypeId ===='+objCase.RecordTypeId );
            System.debug('DeveloperName===='+objCase.RecordType.DeveloperName);
            System.debug('Approval_Status__c ===='+objCase.Approval_Status__c );
            if( objCase.RecordTypeId != null && objCase.RecordType.DeveloperName.equalsIgnoreCase( 'Penalty_Waiver' ) && 
                String.isNotBlank( objCase.Approval_Status__c ) && objCase.Approval_Status__c.equalsIgnoreCase( 'Approved' ) ) {
                if( objCase.Approved_Amount__c != null ) {
                    objCaseListWrapper.totalAmount += objCase.Approved_Amount__c ;
                }
            }
            
        }
        return setCaseId ;
    }
    
    public static Boolean isPenaltyChargedLessEqual( String regid, Decimal value ){
        Boolean response = false;
        UnitDetailsService.BookinUnitDetailsWrapper bookingUnitDetailsWrapperobj;
        try{
            bookingUnitDetailsWrapperobj =  UnitDetailsService.getBookingUnitDetails( regid );
        }
        catch(Exception e) {
            system.debug('unit details exception == ' + e.getMessage());
        }
        if( bookingUnitDetailsWrapperobj != null ){
            system.debug(' == Penalty Amount before updating == '+bookingUnitDetailsWrapperobj.strPenaltyRemaining );
            /*if( String.isBlank( bookingUnitDetailsWrapperobj.strPenaltyCharged ) ) { 
                bookingUnitDetailsWrapperobj.strPenaltyCharged = '100';
            }*/
            if( String.isNotBlank( bookingUnitDetailsWrapperobj.strPenaltyRemaining ) && Decimal.valueOf( bookingUnitDetailsWrapperobj.strPenaltyRemaining ) >= value){
                response = true;
            }
        }
        return response;
    }
    
    /*public static Decimal getWaivedAmountAccountLevel( Id accountId ) { 
        Decimal decAmountWaived = 0;
        if( accountId != null  ) {
            for( Case objCase : PenaltyWaiverUtility.getAllPWCasesFromAccount( accountId ) ) {
                decAmountWaived += objCase.Amount_Approved__c ;
            }
        }
        return decAmountWaived;
    }*/
    
    public static Decimal getCumulativePercent( list<Booking_Unit__c> lstBookingUnits, Id accountId, Decimal intAmountRequested ) {
        Decimal intPercent = 0 ;
        
        Decimal decPrevWaivers = 0;//getWaivedAmountAccountLevel( accountId );   
        Decimal decPendingDues = 0 ;
        
        for( Booking_Unit__c bookingunitobj : lstBookingUnits ){
            /*String strDueResponse = assignmentEndpoints.fetchAssignmentDues( bookingunitobj );
            system.debug('== Pendinf dues on '+bookingunitobj.Registration_Id__c + ' ==' +strDueResponse);
            if( String.isNotBlank( strDueResponse ) ){
                map<String, Object> resp = ( map<String, Object> )JSON.deserializeUntyped( strDueResponse ); 
                decPendingDues += Decimal.valueOf( String.valueOf( resp.get('Penalties') ) );
            }*/
            UnitDetailsService.BookinUnitDetailsWrapper objWrap = UnitDetailsService.getBookingUnitDetails( bookingunitobj.Registration_ID__c );
            if( objWrap != null && String.isNotBlank( objWrap.strPenaltyRemaining ) ) {
                decPendingDues += Decimal.valueOf( objWrap.strPenaltyRemaining );
                decPrevWaivers += Decimal.valueOf( objWrap.strPenaltyWaived );
            }
        }
        system.debug(' == PrevWaivers =='+decPrevWaivers);
        system.debug(' == AmountRequested =='+intAmountRequested);
        
        Decimal numerator = decPrevWaivers + intAmountRequested ;
        Decimal denominator = decPrevWaivers + intAmountRequested + ( decPendingDues - intAmountRequested );
        
        system.debug(' == numerator =='+numerator);
        system.debug(' == denominator =='+denominator);
        
        if( denominator != null && denominator > 0 ) {
            intPercent = ( numerator / denominator )*100 ;
            intPercent = intPercent.setScale(2);
        }
        
        return intPercent ;
    }
    
    public static UploadMultipleDocController.MultipleDocRequest makeWrapperObject( String strBody ,    
                                                                                    String strType , 
                                                                                    String strName , 
                                                                                    String strRegId , 
                                                                                    String strCaseNumber ,
                                                                                    String strCounter ) {
        String strFileExtension = '';   
        if( String.isNotBlank( strName ) && strName.lastIndexOf('.') != -1 ) {
            strFileExtension = strName.substring( strName.lastIndexOf('.')+1 , strName.length() );
        }
                                                                                        
        UploadMultipleDocController.MultipleDocRequest objReqWrap = new UploadMultipleDocController.MultipleDocRequest();
        objReqWrap.Category = 'Document';
        objReqWrap.entityName = 'Damac Service Requests';
        objReqWrap.base64Binary = strBody ;
        objReqWrap.fileDescription = strType ;
        objReqWrap.fileId = strCaseNumber + '-' + System.currentTimeMillis() + '-' + strCounter + '-' + strFileExtension ; 
        objReqWrap.fileName = strCaseNumber + '-' + System.currentTimeMillis() + '-' + + strCounter + '.' + strFileExtension ;
        objReqWrap.registrationId = '2-'+strRegId;
        //objReqWrap.registrationId = strRegId ;
        objReqWrap.sourceFileName = strName; 
        objReqWrap.sourceId = strName;
        system.debug('== objReqWrap =='+objReqWrap);
        return objReqWrap ;
    }
    
   // @testVisible
    public class CaseListWrapper {
        public List<Case> lstOpenCase;
        public List<Case> lstCloseCase;
        public Case caseobj;
        public Decimal totalAmount;
        public boolean isAllowedToOpenPenaltyCase ;
        public String strOpenSrType ;
        
        public CaseListWrapper() {
            lstOpenCase = new list<Case>();
            lstCloseCase = new list<Case>();
            caseobj = new case();
            totalAmount = 0 ;
            isAllowedToOpenPenaltyCase = true ;
        }
    }
   
}