@isTest
public class LeadUpdatesController_Test {
    public static testmethod void m1(){
        Inquiry__c inq = InitialiseTestData.getInquiryDetails('Inquiry',1234);
        insert inq;
        
        LeadUpdatesController obj = new LeadUpdatesController(new ApexPages.StandardController(inq));
        obj.updateInquiry();

        inq.Email__c = '12342345345';
        obj.updateInquiry();
    }
}