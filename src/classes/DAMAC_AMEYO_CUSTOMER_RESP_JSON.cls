public class DAMAC_AMEYO_CUSTOMER_RESP_JSON {
    public cls_beanResponse[] beanResponse;
    public class cls_beanResponse {
        public cls_inputCustomerRecord inputCustomerRecord;
        public boolean inserted;
        public Integer customerId;  //4148793
        public String resultTypeString; //UPDATED
        public boolean crmIntegrated;
        public boolean crmSuccess;
    }
    public class cls_inputCustomerRecord {
        public String name; //Amit4
        public String id;   //123
        public String phone1;   //7838735378
    }
    public static DAMAC_AMEYO_CUSTOMER_RESP_JSON  parse(String json){
        return (DAMAC_AMEYO_CUSTOMER_RESP_JSON ) System.JSON.deserialize(json, DAMAC_AMEYO_CUSTOMER_RESP_JSON .class);
    }

   
}