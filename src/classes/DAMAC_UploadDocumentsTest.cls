@isTest
private class DAMAC_UploadDocumentsTest {

    @testSetup static void setupData() {
        NSIBPM__Document_Master__c LOI_DM = new NSIBPM__Document_Master__c(
            NSIBPM__Code__c = 'LOI_AGREEMENT',
            NSIBPM__Available_to_client__c = true
        );
        insert LOI_DM;
        
        Page_Flow__c pf = new Page_Flow__c();
        pf.Name = 'Pre-Approval';
        pf.Flow_Description__c   = 'Pre-Approval - Company Formation';
        pf.Master_Object__c  = 'NSIBPM__Service_Request__c';
        pf.Record_Type_API_Name__c  = 'RAKFTZ_Pre_Approval';
        pf.Requirements__c = 'Test';
        insert pf;

        Page__c page = new Page__c();
        page.No_Quick_navigation__c = true;
        page.Is_Custom_Component__c = true; 
        page.Migration_Rec_Id__c  = '';
        page.Page_Description__c = 'PRE-APPROVAL FOR VISA SERVICES';
        page.Page_Flow__c = pf.id;
        page.Page_Order__c = 1;
        page.Page_Title__c = 'Package';
        
        page.Render_By_Default__c = true;
        page.VF_Page_API_Name__c = 'Process_Flow';
        page.What_Id__c = '';
        insert page;

        Section__c  sec = new Section__c();
        sec.Name = 'Section name';
        sec.Order__c = 1;
        sec.Section_Title__c = 'Sectoin title';
        sec.Section_Type__c = 'PageBlockSection';
        sec.Layout__c  = '2';
        sec.Page__c = page.Id;
        insert sec;
        
        Section_Detail__c sd = new Section_Detail__c();
        sd.Order__c = 1;
        sd.Name = 'Save';
        sd.Navigation_Directions__c = 'Forward';
        sd.Component_Label__c   = 'Save';
        sd.Component_Type__c = 'Command Button';
        sd.Section__c = sec.Id;
        insert sd;



         
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;


        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.Agency_Name__c = 'Test Account';
        SR.NSIBPM__Customer__c  = a.Id;
        SR.NSIBPM__Email__c = 'test@nsigulf.com';
        SR.NSIBPM__Send_SMS_to_Mobile__c= '12123';
        SR.COUNTRY_OF_SALE__C = 'UAE;Lebanon;Jordan';
        insert SR;


        Amendment__c amd = new Amendment__c();
        amd.Service_Request__c = SR.Id;
        amd.Last_Name__c = 'Test';
        
        insert amd;

        Amendment__c amd1 = new Amendment__c();
        amd1.Service_Request__c = SR.Id;
        amd.Last_Name__c = 'Testing';
        insert amd1;
        
        NSIBPM__Document_Master__c Dm = new NSIBPM__Document_Master__c();
        Dm.Name = 'Test DM';
        Dm.NSIBPM__Code__c = 'Bank Statement';
        Dm.NSIBPM__Document_Type_Code__c = 'Doc Type';
        insert Dm;

        List<NSIBPM__SR_Doc__c> SRDoc_List = createSRDocs(SR.Id, Dm);

       


     }
     

     public static List<NSIBPM__SR_Doc__c> createSRDocs(Id SRId,NSIBPM__Document_Master__c Dm)
     {
        List<NSIBPM__SR_Doc__c> SRDoc_List = new List<NSIBPM__SR_Doc__c>();
        NSIBPM__SR_Doc__c SRDoc;
        for(Integer i=0;i<10;i++)
        {
           SRDoc = new NSIBPM__SR_Doc__c();
           SRDoc.Name = 'Test' + i;
           SRDoc.NSIBPM__Document_Master__c = Dm.Id;
           SRDoc.NSIBPM__Service_Request__c = SRId;
           SRDoc.Agreement_Name__c = 'test';
           SRDoc_List.add(SRDoc); 
        }
        insert SRDoc_List;
        return SRDoc_List;
     }

    @isTest static void test_method_one() {
        // Implement test code
        // 

        SR_Process__c objSRProcess = new SR_Process__c();
        objSRProcess.New_Document_Process__c = true;
        objSRProcess.Name = 'System Administrator';
        insert objSRProcess;
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US', ProfileId = p1.Id,
                        TimeZoneSidKey='America/Los_Angeles',
                        UserName=uniqueUserName);
        insert u;
        System.runAs(u) {
            NSIBPM__Service_Request__c  SR = [Select Id from NSIBPM__Service_Request__c];
            Page_Flow__c pf = [Select id From Page_Flow__c LIMIT 1 ];
            Page__c p =  [Select Id from Page__c LIMIT 1];
            Section__c sec = [Select id from Section__c LIMIT 1];
            Section_Detail__c sd =  [Select id from Section_Detail__c LIMIT 1];
            List<Amendment__c> amdList = [Select Id from Amendment__c];
            List<NSIBPM__SR_Doc__c> SRDoc_List = [Select Id,Agreement_Name__c from NSIBPM__SR_Doc__c];
            PageReference Process_Flow = Page.Process_Flow;
            Test.setCurrentPageReference(Process_Flow);

            Unit_Documents__c objUnitDocument = new Unit_Documents__c();
            objUnitDocument.Document_Name__c = 'test';
            objUnitDocument.Document_Description__c = 'test wedcf d';
            objUnitDocument.Document_type__c = 'Registration';
            objUnitDocument.Optional__c = false;
            objUnitDocument.Status__c = 'Pending Upload';
            objUnitDocument.User_Doc__c = false;
            objUnitDocument.Sys_Doc_ID__c = null;
            objUnitDocument.Service_Request__c = SR.Id; 
            insert objUnitDocument;
            
            
            ContentVersion objContentVersion;
            objContentVersion = new ContentVersion( Title = 'test',
                                                PathOnClient = 'test',
                                                VersionData = Blob.valueOf('test'));
            
            insert objContentVersion;
            
            Process_Flow.getParameters().put('FlowId',pf.Id);
            Process_Flow.getParameters().put('Type','RAKFTZ_Pre_Approval');
            Process_Flow.getParameters().put('id',SR.Id);
            Process_Flow.getParameters().put('PageId',p.Id);
            Process_Flow.getParameters().put('UnitId',objUnitDocument.Id);
            Test.startTest();
            DAMAC_UploadDocuments cls = new DAMAC_UploadDocuments();
            cls.isProcessFlow=false;
            cls.strObjectId = SR.Id;
            system.debug('--12>>>>>>  ' + cls.strObjectId);
            system.debug('--12->>>>>>  ' + SR.Id);
            //cls.generateLOIAgreements();
            cls.strDocumentName = 'test';
            cls.FileName = 'Test';
            cls.AgrDocId  = 'testId';
            cls.strSRDocId = SRDoc_List[0].Id;
            //cls.objSRDocEdit = SRDoc_List[0];
            cls.strError = 'Error';
            cls.strAttachmentName = 'strAttachmentName';
            cls.setstrObjectName('test');
            String str = cls.getstrObjectName();
            SRDoc_List[0].NSIBPM__Doc_ID__c = NULL;
            cls.PreviousPage();
            cls.NextPage();
            cls.Cancel();
            cls.docBody = 'TestBody';
            cls.document.name = 'test';
            cls.document.body = Blob.valueOf('test');
            cls.generateLOIAgreementsNew(); 
            
            cls.versionId = objContentVersion.Id;
            cls.generateContentDocument('test', objUnitDocument.Id);
            DAMAC_UploadDocuments.generateFiles(new List<ContentVersion>{objContentVersion},SRDoc_List);
            cls.SaveDoc();
            cls.EditDoc();
            cls.strSRDocId = null;
            cls.Upload();
            Process_Flow.getParameters().put('srId',SRDoc_List[0].id);
            cls.isUnitDoc = false;
            cls.strSRDocId = SRDoc_List[0].id;
            Process_Flow.getParameters().put('UnitId','');
            cls.Upload();
            Unit_Documents__c objUnitDocument1 = new Unit_Documents__c();
            objUnitDocument1.Document_Name__c = 'test';
            objUnitDocument1.Document_Description__c = 'test wedcf d';
            objUnitDocument1.Document_type__c = 'Registration';
            objUnitDocument1.Optional__c = false;
            objUnitDocument1.Status__c = 'Uploaded';
            objUnitDocument1.Service_Request__c = SR.ID;
            objUnitDocument1.User_Doc__c = false;
            objUnitDocument1.Sys_Doc_ID__c =  '122132323232';
            insert objUnitDocument1;
            cls.getlstDoc();
            objUnitDocument1.Sys_Doc_ID__c = objContentVersion.ContentDocumentId ;
            update objUnitDocument1;
            cls.getlstDoc();
            Test.stopTest();
        }

    }
    @isTest static void test_goTopage() {
        // Implement test code
        // 
        NSIBPM__Service_Request__c  SR = [Select Id from NSIBPM__Service_Request__c];
        Page_Flow__c pf = [Select id From Page_Flow__c LIMIT 1 ];
        Page__c p =  [Select Id from Page__c LIMIT 1];
        Section__c sec = [Select id from Section__c LIMIT 1];
        Section_Detail__c sd =  [Select id from Section_Detail__c LIMIT 1];
        List<Amendment__c> amdList = [Select Id from Amendment__c];
        List<NSIBPM__SR_Doc__c> SRDoc_List = [Select Id from NSIBPM__SR_Doc__c];
        PageReference Process_Flow = Page.Process_Flow;
        Test.setCurrentPageReference(Process_Flow);
        
        Process_Flow.getParameters().put('FlowId',pf.Id);
        Process_Flow.getParameters().put('Type','RAKFTZ_Pre_Approval');
        Process_Flow.getParameters().put('id',p.Id);
        Process_Flow.getParameters().put('PageId',p.Id);
        Test.startTest();
        DAMAC_UploadDocuments cls = new DAMAC_UploadDocuments();
        cls.FlowId  = pf.Id;
        cls.goTopage();
        Test.stopTest();

    }
    @isTest static void test_DynamicButtonAction() {
        // Implement test code
        // 
        NSIBPM__SR_Template__c srt = new NSIBPM__SR_Template__c();
        srt.Name = 'Agent Registration';
        srt.NSIBPM__SR_RecordType_API_Name__c = 'Agent_Update';
        insert srt;
        NSIBPM__Service_Request__c  SR = [Select Id from NSIBPM__Service_Request__c];
        SR.NSIBPM__SR_Template__c = srt.Id;
        SR.Country_of_Sale__c = 'UAE';
        update SR;
        Page_Flow__c pf = [Select id From Page_Flow__c LIMIT 1 ];
        Page__c p =  [Select Id from Page__c LIMIT 1];
        Section__c sec = [Select id from Section__c LIMIT 1];
        Section_Detail__c sd =  [Select id from Section_Detail__c LIMIT 1];
        List<Amendment__c> amdList = [Select Id from Amendment__c];
        List<NSIBPM__SR_Doc__c> SRDoc_List = [Select Id from NSIBPM__SR_Doc__c];
        PageReference Process_Flow = Page.Process_Flow;
        Test.setCurrentPageReference(Process_Flow);
        
        Process_Flow.getParameters().put('FlowId',pf.Id);
        Process_Flow.getParameters().put('Type','RAKFTZ_Pre_Approval');
        Process_Flow.getParameters().put('id',p.Id);
        Process_Flow.getParameters().put('PageId',p.Id);
        Test.startTest();
        DAMAC_UploadDocuments cls = new DAMAC_UploadDocuments();
        cls.objSRFlow = SR;
        cls.strObjectId = SR.Id;
        cls.strActionId  = sd.Id;
        SRDoc_List[0].NSIBPM__Is_Not_Required__c = false;
        SRDoc_List[0].NSIBPM__Doc_ID__c  = NULL;
        update SRDoc_List[0];
        cls.DynamicButtonAction();
        cls.EditCompanySR = true;
        NSIBPM__SR_Status__c srSt = new NSIBPM__SR_Status__c ();
        srSt.NSIBPM__Code__c = 'Closed';
        srSt.Name = 'Submitted';
        insert srSt;
        cls.userProfileName = 'System Administrator';
        cls.DynamicButtonAction();
        cls.strNavigatePageId = SR.Id;
        cls.goTopage();
        cls.pageMessage  = '';
        cls.strPageId = '';
        cls.thumbBase64 = '';
        Test.stopTest();

    }
    @isTest static void test_DyncPgMainPB() {
        // Implement test code
        // 
        NSIBPM__Service_Request__c  SR = [Select Id from NSIBPM__Service_Request__c];
        Page_Flow__c pf = [Select id From Page_Flow__c LIMIT 1 ];
        Page__c p =  [Select Id from Page__c LIMIT 1];
        Section__c sec = [Select id from Section__c LIMIT 1];
        Section_Detail__c sd =  [Select id from Section_Detail__c LIMIT 1];
        List<Amendment__c> amdList = [Select Id from Amendment__c];
        List<NSIBPM__SR_Doc__c> SRDoc_List = [Select Id from NSIBPM__SR_Doc__c];
        PageReference Process_Flow = Page.Process_Flow;
        Test.setCurrentPageReference(Process_Flow);
        
        Process_Flow.getParameters().put('FlowId',pf.Id);
        Process_Flow.getParameters().put('Type','RAKFTZ_Pre_Approval');
        Process_Flow.getParameters().put('id',p.Id);
        Process_Flow.getParameters().put('PageId',p.Id);
        Test.startTest();
        DAMAC_UploadDocuments cls = new DAMAC_UploadDocuments();
        
       // cls.getDyncPgMainPB();
        cls.getDyncPgMainPBBottom();
        cls.Back_To_SR();
        cls.AgrDocId = SRDoc_List[0].Id;
        cls.EditDoc();
        cls.SaveDoc();

        Test.stopTest();

    }

    @isTest static void test_method_second() {

        NSIBPM__Service_Request__c  SR = [Select Id from NSIBPM__Service_Request__c];
        SR.filled_page_ids__c = 'Docs-05';
        update SR;
        Page_Flow__c pf = [Select id From Page_Flow__c LIMIT 1 ];
        Page__c p =  [Select Id from Page__c LIMIT 1];
        Section__c sec = [Select id from Section__c LIMIT 1];
        Section_Detail__c sd =  [Select id from Section_Detail__c LIMIT 1];
        List<Amendment__c> amdList = [Select Id from Amendment__c];
        List<NSIBPM__SR_Doc__c> SRDoc_List = [Select Id,Agreement_Name__c from NSIBPM__SR_Doc__c];
        PageReference Process_Flow = Page.Process_Flow;
        Test.setCurrentPageReference(Process_Flow);
        
        Integer i=0;
        for(Amendment__c am : amdList) {
            am.Last_Name__c = 'Testingg90982uu'+i;
            am.Email__c = 'Testingg90982uu'+i+'@gmail.com';
            i++;
        } 
        update amdList;
        Unit_Documents__c objUnitDocument = new Unit_Documents__c();
        objUnitDocument.Document_Name__c = 'test';
        objUnitDocument.Document_Description__c = 'test wedcf d';
        objUnitDocument.Document_type__c = 'Registration';
        objUnitDocument.Optional__c = false;
        objUnitDocument.Status__c = 'Pending Upload';
        objUnitDocument.User_Doc__c = false;
        insert objUnitDocument;
        ContentVersion objContentVersion;
        objContentVersion = new ContentVersion( Title = 'test',
                                            PathOnClient = 'test',
                                            VersionData = Blob.valueOf('test'));
        
        insert objContentVersion;
        
        Process_Flow.getParameters().put('FlowId',pf.Id);
        Process_Flow.getParameters().put('Type','RAKFTZ_Pre_Approval');
        Process_Flow.getParameters().put('id',SR.Id);
        Process_Flow.getParameters().put('PageId',p.Id);
        Process_Flow.getParameters().put('UnitId',objUnitDocument.Id);
        Test.startTest();
        DAMAC_UploadDocuments cls = new DAMAC_UploadDocuments();
        cls.strObjectId = SR.Id;
        system.debug('--12>>>>>>  ' + cls.strObjectId);
        system.debug('--12->>>>>>  ' + SR.Id);
        //cls.generateLOIAgreements();
        cls.strDocumentName = 'test';
        cls.FileName = 'Test';
        cls.AgrDocId  = 'testId';
        cls.strSRDocId = SRDoc_List[0].Id;
        //cls.objSRDocEdit = SRDoc_List[0];
        cls.strError = 'Error';
        cls.strAttachmentName = 'strAttachmentName';
        cls.setstrObjectName('test');
        String str = cls.getstrObjectName();
        SRDoc_List[0].NSIBPM__Doc_ID__c = NULL;
        cls.PreviousPage();
        cls.NextPage();
        cls.Cancel();
        cls.generateLOIAgreementsNew();
        NSIBPM__SR_Status__c srSt = new NSIBPM__SR_Status__c ();
        srSt.NSIBPM__Code__c = 'Closed';
        insert srSt;
        cls.updateAddAgentSrRecord();
        
        DAMAC_UploadDocuments.uploadFile (objContentVersion.Id, objUnitDocument.Id);
        Test.stopTest();

    }
    
}