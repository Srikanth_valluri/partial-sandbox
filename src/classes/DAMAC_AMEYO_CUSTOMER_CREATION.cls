/*

    Name:DAMAC_AMEYO_CUSTOMER_CREATION 

    Desc: CM API push to Dialer , 

    Mapping the Dialer Fields and Pushing via Call out , Triggered by ICS Push

    

    Change Log:

    May 28 2020 - attribute30 mapped from lastModified to Ameyo_Date_Time_Now__c

*/

 

public class DAMAC_AMEYO_CUSTOMER_CREATION {

   

    // Async transaction to get the customer id and sending the callback info

    @Future(Callout = TRUE)

    Public static void setCustomerManagerAsync (list<id> inquiryIds){

           

        Ameyo_Credentials__c credentials = Ameyo_Credentials__c.getInstance(UserInfo.getUserID());

       

        // For Customer Manager API 

        DAMAC_AMEYO_CUSTOMER_JSON obj = new DAMAC_AMEYO_CUSTOMER_JSON();

        

        obj.sessionId = DAMAC_AMEYO_CUSTOMER.checkSession (); // To get the active session from Ameyo

        obj.numAttempts = String.valueOf (inquiryIds.size ());

        obj.Status = 'NOT_TRIED';

        

        DAMAC_AMEYO_CUSTOMER_JSON.cls_properties prop = new DAMAC_AMEYO_CUSTOMER_JSON.cls_properties ();

        prop.update_customer = true;

        prop.migrate_customer = true;        

        obj.properties = prop;

        

        List <DAMAC_AMEYO_CUSTOMER_JSON.cls_customerRecords> records = new List <DAMAC_AMEYO_CUSTOMER_JSON.cls_customerRecords > ();

        Set <ID> ownerIds = new Set <ID> ();
        for (Inquiry__c inq :[SELECT OwnerId FROM Inquiry__c WHERE id in :inquiryIds]){
            ownerIds.add (inq.OwnerId);
        }

        Map <ID, User> ownerDetails = new Map <ID, User>([SELECT FederationIdentifier FROM User WHERE ID IN: ownerIds]);

        

        for (Inquiry__c inq:[SELECT Owner_Queue_Name__c, Campaign_Name_Text__c, Full_Name__c,  Ameyo_Campaign_Id_for_CM_API__c,
                            Owner.Name, Is_Owner_Queue__c, Ameyo_Callback_Id__c, Inquiry_Status__c,Ameyo_Date_Time_Now__c,
                            Ameyo_Customer_Id__c, Call_back_Date_time__c, Inquiry_Source__c, Mobile_CountryCode__c,
                            Short_Code_Mobile_Country__c, Ameyo_Name__c, OwnerId, Ameyo_Dialing_Priority_Attribute__c, LastModifiedDate,
                            Inquiry_Created_Date__c, Nationality__c, Inquiry_Number__c, City__c, Country_of_Residence__c,
                            Agency_Name__r.Name, class__c, All_Activities_Counter__c, Last_Activity_Date__c, Campaign__r.Name,
                            Ameyo_Campaign_Id__c, Encrypted_Mobile__c,Inquiry_18_Digit__c FROM Inquiry__c WHERE id in :inquiryIds]){

            

            // Customer Manager API

            obj.campaignId = inq.Ameyo_Campaign_Id_for_CM_API__c;

            DAMAC_AMEYO_CUSTOMER_JSON.cls_customerRecords rec = new DAMAC_AMEYO_CUSTOMER_JSON.cls_customerRecords ();

            rec.id = inq.Inquiry_18_Digit__c;

            rec.phone1 = inq.Encrypted_Mobile__c;

            rec.name = inq.Ameyo_Name__c;

            

            rec.campaign_name_text = inq.Campaign_Name_Text__c;

            rec.ownerid = inq.OwnerId;

            rec.ownername = inq.Owner_Queue_Name__c;

            rec.inquiry_source = inq.Inquiry_Source__c;

            rec.mobilecountrycode2 = inq.Short_Code_Mobile_Country__c;

            rec.mobilecountrycode1 = inq.Mobile_CountryCode__c;

            rec.attribute1 = inq.Inquiry_Status__c;

            rec.attribute3 = inq.Ameyo_Campaign_Id__c;

            rec.attribute2 = inq.Ameyo_Dialing_Priority_Attribute__c;

            rec.wrapupurl = inq.Inquiry_18_Digit__c;

            rec.inquiry_created_date = ''+inq.Inquiry_Created_Date__c;

            //rec.attribute30 = ''+inq.LastModifiedDate; 

            rec.attribute30 = ''+inq.Ameyo_Date_Time_Now__c; 

            rec.inquiry_number = ''+inq.Inquiry_Number__c;

            rec.attribute4 = inq.class__c == null ? '""' : inq.class__c;                    
            rec.attribute5 = inq.All_Activities_Counter__c != null ? string.valueOf(inq.All_Activities_Counter__c) : '0';
            rec.attribute6 = inq.Last_Activity_Date__c != null ? string.valueOf(inq.Last_Activity_Date__c) : '""';
            rec.attribute7 = inq.Campaign__r.Name == null ? '""' : inq.Campaign__r.Name;
            rec.attribute8 = inq.Agency_Name__r.Name == null ? '""' : inq.Agency_Name__r.Name;
            
            rec.DAMAC_JSON_class = inq.Class__c==null?'':inq.Class__c;
            rec.nationality = inq.Nationality__c==null?'':inq.Nationality__c;
            rec.city = inq.City__c==null?'':inq.City__c;
            rec.country_of_residence = inq.Country_of_Residence__c==null?'':inq.Country_of_Residence__c;
            /*

            rec.last_name = '';

            rec.timezone = '';

            rec.phone2 = '';

            rec.phone3 = '';

            rec.phone4 = '';

            rec.phone5 = '';

            rec.timezone2 = '';

            rec.phone = '';

            rec.first_name = '';

            rec.email = '';

            rec.attribute4 = '';

            rec.attribute5 = '';

            rec.attribute6 = '';

            rec.attribute7 = '';

            rec.attribute8 = '';

            rec.attribute9 = '';

            rec.attribute10 = '';

            rec.attribute11 = '';

            rec.attribute12 = '';

            rec.attribute13 = '';

            rec.attribute14 = '';

            rec.attribute15 = '';

            rec.attribute16 = '';

            rec.attribute17 = '';

            rec.attribute18 = '';

            rec.attribute19 = '';

            rec.attribute20 = '';

            rec.attribute21 = '';

            rec.attribute22 = '';

            rec.attribute23 = '';

            rec.attribute24 = '';

            rec.attribute25 = '';

            rec.attribute26 = '';

            rec.attribute27 = '';

            rec.attribute28 = '';

            rec.attribute29 = '';

            rec.attribute30 = '';            

            obj.leadId = NULL;

            */

            

            if (inq.Is_Owner_Queue__c && inq.Owner.Name == 'Ameyo Dialer Queue') {

                if (inq.Ameyo_Campaign_Id__c != NULL)

                    obj.LeadId = Integer.valueOf(inq.Ameyo_Campaign_Id__c);

            } 

            

            records.add (rec);

            

        }

        

        obj.customerRecords = records;

        

        String reqBody = JSON.serialize (obj);

        reqBody = reqBody.replace ('update_customer', 'update.customer');

        reqBody = reqBody.replace ('migrate_customer', 'migrate.customer');

        reqBody = reqBody.replace ('DAMAC_JSON_', '');

        reqBody = reqBody.replace ('"leadId": null,', '');

        

        System.Debug ('::: Customer Insert/Update Body to Send :::'+reqBody);    

        

        // Http Request to get the Customer id based on the inquiry information

        HTTPRequest req = new HTTPRequest ();

        req.setEndpoint (credentials.Endpoint__c+'?command=uploadContacts');

        req.setHeader ('Content-Type', 'application/x-www-form-urlencoded');

        req.setHeader ('hash-key', credentials.hash_key__c);

        req.setHeader ('Host', credentials.host__c);

        req.setHeader ('policy-name', credentials.policy_name__c);

        req.setHeader ('requesting-host', credentials.requesting_host__c);

        req.setTimeout (120000);

        req.setMethod ('POST');

        req.setBody ('data='+EncodingUtil.urlEncode(reqBody, 'UTF-8'));

                

        HTTP http = new HTTP ();

        HTTPResponse res = new HTTPResponse ();

        if (!Test.isRunningTest ()){

            res = http.send (req);

        }

        

        System.Debug ('Customer Insert/update Response=====>'+res.getStatusCode ()+' :::: '+res.getBody());

        

        String responseBody = '';

        

        if (!Test.isRunningTest ()) {

            responseBody = res.getBody ();

        } else {

            responseBody = '{"beanResponse":[{"inputCustomerRecord":'

                +'{"name":"Amit4","id":"123","phone1":"7838735378"},"inserted":true,'

                +'"customerId":4148793,"resultTypeString":"UPDATED","crmIntegrated":false,'

                +'"crmSuccess":false},{"inputCustomerRecord":{"name":"Ashu2","id":"123",'

                +'"phone1":"9773925451"},"inserted":false,"exception":"Duplicate key 123",'

                +'"resultTypeString":"DUPLICATE_NOT_ADDED","crmIntegrated":false,"crmSuccess":false}]}';

            res.setStatusCode (200);

        }

        

        if (responseBody != '' && res.getStatusCode () == 200) {

        

            Map <String, String> customerIds = new Map <String, String> ();

            DAMAC_AMEYO_CUSTOMER_RESP_JSON respObj = DAMAC_AMEYO_CUSTOMER_RESP_JSON.parse (responseBody);

            

            for (DAMAC_AMEYO_CUSTOMER_RESP_JSON.cls_beanResponse resp : respObj.beanResponse) {

                if (resp.customerId != NULL) {

                    String customerId = String.valueOf (resp.customerId);

                    String inqId = resp.inputCustomerRecord.id;

                    customerIds.put (inqId, customerId);

                }

            }

            

            if (customerIds.keySet ().size () > 0) {

                List <Inquiry__c> inqToUpdate = new List <Inquiry__c> ();

                for (Inquiry__c inq :[SELECT Ameyo_Customer_Id__c FROM Inquiry__c WHERE ID IN :customerIds.keySet ()]) {

                    inq.Ameyo_Customer_Id__c = customerIds.get (inq.id);

                    inqToUpdate.add (inq);

                    

                }

                if (InqToUpdate.size () > 0) {

                    DAMAC_Constants.skip_InquiryTrigger = true;

                    Update inqToUpdate;

                }

            }

        }

        

        if (obj.sessionId != '' && credentials.Session_Id__c != obj.sessionId) {

            Ameyo_Credentials__c cred = [SELECT Session_Id__c FROM Ameyo_Credentials__c LIMIT 1];

            cred.Session_Id__c = obj.sessionId;

            update cred;

        

        }

    }
}