@isTest
global class ProofOfPaymentLWCControllerMockGenerator implements HttpCalloutMock{
    
    public static final string SAMPLE_RESPONSE=  '{ "OutputParameters" : { "@xmlns" : "http://xmlns.oracle.com/apps/ont/rest/XXDC_POP_RECEIPTS/apply_unidentified_receipts/", "@xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance", "P_RECEPT_OUT_REC" : { "P_RECEPT_OUT_REC_ITEM" : [ { "X_CASH_RECEPT_ID" : "3308956", "X_URL" : "https://sftest.deeprootsurface.com/docs/e/65561599_3308956_RECEIPT.pdf", "X_STATUS" : "S", "X_ERROR_DESC" : "Receipt is applied successfully ..." } ] }, "X_RETURN_STATUS" : "S", "X_RETURN_MESSAGE" : "Applicatin is completed Successfully " } }';
        
    
    global HTTPResponse respond(HTTPRequest req) {
       
        System.assertEquals('http://83.111.194.181:8033/webservices/rest/XXDC_POP_RECEIPTS/apply_unidentified_receipts/', req.getEndpoint());
        
        req.setMethod('POST');
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(SAMPLE_RESPONSE);
        res.setStatusCode(200);
        return res;
    }
}