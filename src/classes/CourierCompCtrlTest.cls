@isTest
public without sharing Class CourierCompCtrlTest {
    public class FirstFlightServiceMock implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req) {
            req.setMethod('POST');
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String Json = '{ "AirwayBillTrackList": [{ "AirWayBillNo": "12345", "Destination": "ABU DHABI-UNITED ARAB EMIRATES", "ForwardingNumber": "", "Origin": "DUBAI-UNITED ARAB EMIRATES", "ShipmentProgress": 3, "ShipperReference": "AE5248222", "TrackingLogDetails": [{ "ActivityDate": "Saturday 02 November 2019", "ActivityTime": "12:21", "DeliveredTo": "", "Location": "DUBAI", "Remarks": "Location Information Received", "Status": "LC" }, { "ActivityDate": "Saturday 02 November 2019", "ActivityTime": "11:43", "DeliveredTo": "", "Location": "DUBAI", "Remarks": "Whatsapp To Customer", "Status": "SM" } ], "Weight": "1.000" }], "Code": 1, "Description": "Success" }';
            res.setBody(Json);
            res.setStatusCode(200);
            return res;
        }
    }
    
    public class FirstFlightAirwayBillPDFMock implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req) {
            req.setMethod('POST');
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String Json = '{ "Code": 1, "Description": "Success", "ReportDoc": "JVBERi0xLjIgCiXi48/TIAoxIDAgb2JqIAo8PCAKL1R5cGUgL0NhdGFsb2cgCi9QYWdlcyAyIDAgUiAKL1BhZ2VNb2RlIC9Vc2VOb25lIAovVmlld2VyUHJlZmVyZW5jZXMgPDwgCi9GaXRXaW5kb3cgdHJ1ZSAKL1BhZ2VMYXlvdXQgL1NpbmdsZVBhZ2UgCi9Ob25GdWxsUzIAo1MDAgCjI3OCAKNTU2IAo1MDAgCjcyMiAKMCAKNTAwIApdIAplbmRvYmogCjE1IDAgb2JqIAovQUFBQUFBK0FyaWFsIAplbmRvYmogCjE3IDAgb2JqIAo8PCAKL1R5cGUgL0ZvbnREZXNjcmlwdG9yIAovQXNjZW50IDkwNSAKL0NhcEhlaWdodCA1MDAgCi9EZXNjZW50IC0yMTIgCi9GbGFnDAwMDAwIG4gCjAwMDAwNDUyOTggMDAwMDAgbiAKdHJhaWxlciAKPDwgCi9TaXplIDI5IAovUm9vdCAxIDAgUiAKL0luZm8gMjggMCBSIAo+PiAKc3RhcnR4cmVmIAo0NTM4NiAKJSVFT0YgCg==" }';
            res.setBody(Json);
            res.setStatusCode(200);
            return res;
        }
    }
    @isTest
    public static void submitDetailsTest() {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.FirstName = 'Test';
        objAcc.LastName = 'Account';
        //objAcc.Master_Address__c = 'Test Address';
        objAcc.Zip_Postal_Code__c = '123456';
        objAcc.City__c = 'Dubai';
        objAcc.Country__c = 'Oman';
        objAcc.Phone_1__c = '123456';
        objAcc.Phone_2__c = '123456';
        objAcc.Fax__c = '123';
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        insert objCase;
        
        
        // courierCompInst.accId = objAcc.Id;
        // courierCompInst.caseId = objCase.Id;
        Credentials_Details__c objCred = new Credentials_Details__c();
        Test.setMock(HttpCalloutMock.class, new FirstFlightServiceMock ());
        objCred.Name = 'First Flight Create AirwayBill';
        objCred.Endpoint__c = 'https://ontrack.firstflightme.com/FFCService.svc/CreateAirwayBill';
        objCred.User_Name__c = '3000';
        objCred.Password__c = 'fftes';
        insert objCred;
        
        Credentials_Details__c objCred1 = new Credentials_Details__c();
        objCred1.Name = 'First Flight AWB PDF';
        objCred1.Endpoint__c = 'https://ontrack.firstflightme.com/FFCService.svc/Tracking';
        objCred1.User_Name__c = '3000';
        objCred1.Password__c = 'fftes';
        objCred1.Resource__c = '3000';
        insert objCred1;
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;
        
        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;
        
        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAcc.Id,objDealSR.Id,1);
        insert bookingList;
        
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,3);
        insert bookingUnitList;
        
        
        PageReference courierPage = Page.CourierPage_BU;
        Test.setCurrentPage(courierPage);       
        ApexPages.currentPage().getParameters().put('id', bookingUnitList[0].Id);
        CourierCompCtrl courierCompInst = new CourierCompCtrl();
    
        //Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        UploadMultipleDocController.strLabelValue ='N';
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());
        SOAPCalloutServiceMock.returnToMe = new Map<String, unitDetailsController.getUnitDetailValuesResponse_element>();
        unitDetailsController.getUnitDetailValuesResponse_element response = new unitDetailsController.getUnitDetailValuesResponse_element();
        response.return_x = '["{\"ATTRIBUTE1\":\"78152\",\"ATTRIBUTE2\":\"\",\"ATTRIBUTE3\":\"LE\",\"ATTRIBUTE4\":\"N\",\"ATTRIBUTE5\":\"0\",\"ATTRIBUTE6\":\"\",\"ATTRIBUTE7\":\"\",\"ATTRIBUTE8\":\"253760\",\"ATTRIBUTE9\":\"253760\",\"ATTRIBUTE10\":\"0\",\"ATTRIBUTE11\":\"2525\",\"ATTRIBUTE12\":\"N\",\"ATTRIBUTE13\":\"Y\",\"ATTRIBUTE14\":\"N\",\"ATTRIBUTE15\":\"\",\"ATTRIBUTE16\":\"N\",\"ATTRIBUTE17\":\"N\",\"ATTRIBUTE18\":\"123745600\",\"ATTRIBUTE19\":\"0\",\"ATTRIBUTE20\":\"6.81\",\"ATTRIBUTE21\":\"1\",\"ATTRIBUTE22\":\"Y\",\"ATTRIBUTE23\":\"1523\",\"ATTRIBUTE24\":\"0\",\"ATTRIBUTE25\":\"1523\",\"ATTRIBUTE26\":\"PLATINUM\",\"ATTRIBUTE27\":\"N\",\"ATTRIBUTE28\":\"N\",\"ATTRIBUTE29\":\"OFF-PLAN\",\"ATTRIBUTE30\":\"N\",\"ATTRIBUTE31\":\"N\",\"ATTRIBUTE32\":\"\",\"ATTRIBUTE33\":\"440\",\"ATTRIBUTE34\":\"N\",\"ATTRIBUTE35\":\"51794\",\"ATTRIBUTE36\":\"\",\"ATTRIBUTE37\":\"\",\"ATTRIBUTE38\":\"\",\"ATTRIBUTE39\":\"27-MAR-2016\",\"ATTRIBUTE40\":\"AYKCB/40/4011\",\"ATTRIBUTE41\":\"253760\",\"ATTRIBUTE42\":\"0\",\"ATTRIBUTE43\":\"2525\",\"ATTRIBUTE44\":\"0\",\"ATTRIBUTE45\":\"N\",\"ATTRIBUTE46\":\"N\",\"ATTRIBUTE47\":\"N\",\"ATTRIBUTE48\":\"N\",\"ATTRIBUTE49\":\"Y\",\"ATTRIBUTE50\":\"N\",\"ATTRIBUTE51\":\"Agreement executed by DAMAC\",\"ATTRIBUTE52\":\"00\",\"ATTRIBUTE53\":\"6.81\",\"ATTRIBUTE54\":\"17-AUG-2020\",\"ATTRIBUTE55\":\"31-OCT-2021\",\"ATTRIBUTE56\":\"0\",\"ATTRIBUTE57\":\"0\",\"ATTRIBUTE58\":\"51794\",\"ATTRIBUTE59\":\"20\",\"ATTRIBUTE60\":\".2\",\"ATTRIBUTE61\":\"0\",\"ATTRIBUTE62\":\"0\",\"ATTRIBUTE63\":\"\",\"ATTRIBUTE64\":\"\",\"ATTRIBUTE65\":\"N\",\"ATTRIBUTE66\":\"N\",\"ATTRIBUTE67\":\"N\",\"ATTRIBUTE68\":\"0\",\"ATTRIBUTE69\":\"N\",\"ATTRIBUTE70\":\"\",\"ATTRIBUTE71\":\"\",\"ATTRIBUTE72\":\"1\",\"ATTRIBUTE73\":\"PARK_BAY\",\"ATTRIBUTE74\":\"\",\"ATTRIBUTE75\":\"0\",\"ATTRIBUTE76\":\"0\",\"ATTRIBUTE77\":\"\",\"ATTRIBUTE78\":\"1015040\",\"ATTRIBUTE79\":\"\",\"ATTRIBUTE80\":\"\",\"ATTRIBUTE81\":\"\",\"ATTRIBUTE82\":\"SA \u2013 Execution Pending awaiting Deposit\",\"ATTRIBUTE83\":\"Damac Canal One Property Development LLC\",\"ATTRIBUTE84\":\"N\",\"ATTRIBUTE85\":\"1600\",\"ATTRIBUTE86\":\"0\",\"ATTRIBUTE87\":\"20\",\"ATTRIBUTE88\":\"253760\",\"ATTRIBUTE89\":\"1444\",\"ATTRIBUTE90\":\"AYKCB\",\"ATTRIBUTE91\":\"AYKON CITY TOWER - B\",\"ATTRIBUTE92\":\"\",\"ATTRIBUTE93\":\"\",\"ATTRIBUTE94\":\"1074105\",\"ATTRIBUTE95\":\"N...\",\"ATTRIBUTE96\":\"793\",\"ATTRIBUTE97\":\"Normal\",\"ATTRIBUTE98\":\"27-MAR-2016\",\"ATTRIBUTE99\":\"APARTMENT\",\"ATTRIBUTE100\":\"\",\"ATTRIBUTE101\":\"Y\",\"ATTRIBUTE102\":\"1268800\",\"ATTRIBUTE103\":\"Y\",\"ATTRIBUTE104\":\"Y\",\"ATTRIBUTE105\":\"0\",\"ATTRIBUTE106\":\"27-MAR-2016\",\"ATTRIBUTE107\":\"N\",\"ATTRIBUTE108\":\"AYKON CITY\",\"ATTRIBUTE109\":\"DUBAI\",\"ATTRIBUTE110\":\"One Bedroom\",\"ATTRIBUTE111\":\"HOTEL APARTMENTS\",\"ATTRIBUTE112\":\"\",\"ATTRIBUTE113\":\"\",\"ATTRIBUTE114\":\"\",\"ATTRIBUTE115\":\"\",\"ATTRIBUTE116\":\"\",\"ATTRIBUTE117\":\"\",\"ATTRIBUTE118\":\"\",\"ATTRIBUTE119\":\"\",\"ATTRIBUTE120\":\"\"}"]';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response);
        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        Test.setMock(HttpCalloutMock.class, new CourierServiceMock());
        Test.setMock(HttpCalloutMock.class, new FirstFlightAirwayBillPDFMock ());
           Test.startTest();
            courierCompInst.setcaseId(objCase.Id);
            courierCompInst.setaccId(objAcc.Id);
            courierCompInst.setbuId(bookingUnitList[0].Id);
            Id caseId = courierCompInst.getcaseId();
            Id buId = courierCompInst.getbuId();
            Id accId = courierCompInst.getaccId();
            courierCompInst.courierInst.Courier_Service__c = 'First Flight';
            courierCompInst.submitDetails();
            courierCompInst.courierInst.Courier_Service__c = 'Aramex';
            courierCompInst.submitDetails();
        Test.stopTest();
    }
}