/**
=====================================================================================
*Class Name      : SendRejectionEmail
*Test Class Name : SendRejectionEmailTest
*Description     : Class called from SendRejectionEmailPage Visualforce Page which is called from button "Send Rejection Email" on Case Object.
*Developer Name  : 
*Created Date    : 07-12-2020
=====================================================================
************************************************************************************* */
public class SendRejectionEmail
{
    
    private Case caseobj;
    private List<Task> TaskList = new List<Task>();
    public SendRejectionEmail(ApexPages.StandardController controller) {
        this.caseobj= (Case)controller.getRecord();
        caseobj= [SELECT id ,SR_Type__c,
                  Status
                  ,CaseNumber
                  ,Payment_Mode__c
                  ,Payment_Date__c
                  ,Payment_Allocation_Details__c
                  ,Total_Amount__c
                  ,Account.PersonEmail
                  ,Account.Person_Business_Email__c
                  ,Account.IsPersonAccount
                  ,Account.Email__pc
                  ,Account.Email__c
                  ,Account.Name
                  ,CreatedById
                  ,Account.FirstName,AccountId
                  ,Account.owner.Name FROM Case WHERE id = :ApexPages.CurrentPage().getParameters().get('id')];
                  TaskList = [Select Id, Notes__c From Task where WhatId = :caseobj.Id and status='Reject'];
        system.debug('TaskList' +TaskList);
    }
    
    public Case getCase(){
        if(caseobj<>null){
            return caseobj;
        }
        return null;
    }
    
   //Reject Email Should be triggered when case status is Rejected.
    public pageReference send(){
        if(caseobj<>null && caseobj.Status=='Rejected'){
             List<EmailMessage> lstEmails = new List<EmailMessage>();
             List<Attachment> lstAttach = new List<Attachment>();
            String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';
        String subject, strAccountId, strDPIId, bccAddress ='', strCCAddress,body;  
            strAccountId= caseobj.AccountId;
        toAddress=caseobj.Account.Person_Business_Email__c;
         //Attachment[] attachmentLst = [ SELECT Id,ContentType,Name, Body 
                     //From Attachment where parentId = : caseobj.Id];
        //lstAttach.addAll(  attachmentLst );
		
	 
        fromAddress = Label.DefaultOrgWideEmailAddress; //'noreply@damacgroup.com';
        bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
        contentType = 'text/html';
        strCCAddress = '';
            
        if( toAddress != '' ) {
		  
		     body='Dear Valued Customer,<br/><br/>';
             body+='Please note that your proof of payment request has been rejected.<br/><br/>';
             body+='Following are the rejection comments: <br/>';
            if(!TaskList.isEmpty()){
              body+= TaskList[0].Notes__c + '<br/><br/>';}
             body+='Our At Your Service Team will contact you shortly in this regard.<br/>';
             body+='In the mean time, should you require further assistance,';
             body+='please contact us on: +97142375000 or email us on: Atyourservice@damacproperties.com';
             body+= '<br/><br/> Thank you,<br/> DAMAC Properties';
		     
		
            contentBody = body;
			contentValue=body;
           
        }
        subject='Proof Of Payment – SR# -'+caseobj.CaseNumber;
        
            // Callout to sendgrid to send an email
            SendGridEmailService.SendGridResponse objSendGridResponse =
                SendGridEmailService.sendEmailService(
                    toAddress
                    , ''
                    , strCCAddress
                    , ''
                    , bccAddress
                    , ''
                    , subject
                    , ''
                    , fromAddress
                    , ''
                    , replyToAddress
                    , ''
                    , contentType
                    , contentValue
                    , ''
                    , lstAttach
                   
                );

            System.debug('objSendGridResponse == ' + objSendGridResponse);
            //Create Email Activity
            String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
            if (responseStatus == 'Accepted') {
                EmailMessage mail = new EmailMessage();
                mail.Subject = subject;
                mail.MessageDate = System.Today();
                mail.Status = '3';//'Sent';
                mail.RelatedToId = strAccountId;
                mail.Account__c  = strAccountId;
                mail.Type__c = 'CSR Email';
                mail.ToAddress = toAddress;
                mail.FromAddress = fromAddress;
                mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                mail.Sent_By_Sendgrid__c = true;
                mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                mail.CcAddress = strCCAddress;
                mail.BccAddress = bccAddress;
                lstEmails.add(mail);
                system.debug('Mail obj == ' + mail);


            }//End Email Activity Creation
            if(lstEmails != null && lstEmails.size()>0 ){
                insert lstEmails;
                system.debug('lstEmails Is'+lstEmails);
                //update objCase;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Email Sent Successfully' );
                ApexPages.addMessage(msg);
            }
           
         
            
        }
        //If case status is not rejected show message
        else{
            
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Case Status is not rejected' );
            ApexPages.addMessage(msg);
        }
        return null; 
    }
    
}