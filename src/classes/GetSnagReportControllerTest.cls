/********************************************************************************
* Description - Controller of  GetSnagReport to show snag report of Case        *
*                                                                               *
* Version            Date            Author                    Description      *
* 1.0                17/01/2019      Arjun Khatri              Initial Draft    *
********************************************************************************/
@isTest
public class GetSnagReportControllerTest {

    

    @isTest
    public static void test1() {

        
        //Create Customer record
        Account objAcc = new Account(Name = 'Test Account');
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs [0].Inventory__c = objInventory.Id;
        insert listBUs;
        
        //Create CS
        SNAG_URLs__c objSnag = new SNAG_URLs__c(Name = listBUs [0].Unit_name__c.substringbefore('/'),
                                               URL__c = 'test');
        insert objSnag;        
            
        //EHO Case record type Id
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        
        //Create Case
        Case  objCase = TestDataFactory_CRM.createCase(objAcc.Id, caseRecTypeId);
        objCase.Booking_Unit__c = listBUs[0].Id;
        insert objCase;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        PageReference pageRef = Page.GetSnagReport;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id));
        Test.setCurrentPage(pageRef);
        //GetSnagReportController objCtrl = new GetSnagReportController();
        
        Test.startTest();
        GetSnagReportController objCtrl = new GetSnagReportController(sc);
        Test.setMock(HttpCalloutMock.class, new MockHttpCalloutSnagClass(1));
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        
        objCtrl.init();
        Test.stopTest();
        
    }
    
    @isTest
    public static void testError() {
        
        //Create Customer record
        Account objAcc = new Account(Name = 'Test Account');
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs [0].Inventory__c = objInventory.Id;
        insert listBUs;
        
        //Create CS
        SNAG_URLs__c objSnag = new SNAG_URLs__c(Name = listBUs [0].Unit_name__c.substringbefore('/'),
                                               URL__c = 'test');
        insert objSnag;  
        
        //EHO Case record type Id
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        
        //Create Case
        Case  objCase = TestDataFactory_CRM.createCase(objAcc.Id, caseRecTypeId);
        objCase.Booking_Unit__c = listBUs[0].Id;
        insert objCase;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        PageReference pageRef = Page.GetSnagReport;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id));
        Test.setCurrentPage(pageRef);
        //GetSnagReportController objCtrl = new GetSnagReportController();
        
        Test.startTest();
        GetSnagReportController objCtrl = new GetSnagReportController(sc);
        Test.setMock(HttpCalloutMock.class, new MockHttpCalloutSnagClass(2));
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        
        objCtrl.init();
        Test.stopTest();
        
    }
}