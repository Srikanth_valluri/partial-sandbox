@isTest
public class GenerateHCFromIPMSCtrlTest {
    @isTest
    static void testGenerateHC(){
        Case objCase = new Case();
        PageReference pageRef = Page.GenerateHCFromIPMS;
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateHCFromIPMSCtrl objGenerateHC = new GenerateHCFromIPMSCtrl(sc);
        objGenerateHC.callGenerateHC();
        Test.stopTest();
    }
}