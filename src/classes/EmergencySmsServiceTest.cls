@isTest
public Class EmergencySmsServiceTest{
    
    @isTest
    public static void test1() {
    
        String myJSON = '{"strMessageInput": {"objBuildingName":[{"buildName":"DRZ"}],"objVisitorMibiles":[{"visNumber":"463346"}]}}';
        
        /*RestRequest request = new RestRequest();
        request.requestUri ='services/apexrest/upsertaccount';
        request.httpMethod = 'POST';
        request.requestBody = myJSON;
        RestContext.request = request;*/
        EmergencySmsService.BuildingName objBuild = new EmergencySmsService.BuildingName();
        objBuild.buildName = 'DRZ';
        
        EmergencySmsService.VisitorMobiles objVisitor = new EmergencySmsService.VisitorMobiles();
        objVisitor.visNumber = '34234';
        EmergencySmsService.MessageInput objInput = new EmergencySmsService.MessageInput();
        objInput.objBuildingName = new List<EmergencySmsService.BuildingName>{objBuild};
        objInput.objVisitorMibiles = new List<EmergencySmsService.VisitorMobiles >{objVisitor};
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpAgentOTPController());
        String str = EmergencySmsService.EmergencySMS(objInput);
        Test.stopTest();
    }
}