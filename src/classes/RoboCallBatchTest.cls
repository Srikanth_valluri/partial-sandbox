@isTest
public class RoboCallBatchTest{

    @isTest
    static void Test1(){

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account(LastName= 'Test Account',
                                      Phone = '1234567890',
                                      RecordtypeId = rtId,
                                      Phone_Country_Code__c = ' India: 0091',
                                      Mobile_Country_Code__c = 'India: 0091',
                                      Mobile_Phone_Encrypt__pc = '8149071353',
                                      Mobile_Phone_Encrypt__c = '1324567890',
                                      Phone_Encrypt__c = 'adswsfds',
                                      Mobile__c = '1234567890'
                                      );
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        Booking_unit__c objBU = new Booking_Unit__c (Booking__c = booking.Id,
                                                     Owner__c = account.Id,
                                                     FM_Outstanding_Amount__c = '10000',
                                                     FM_Outstanding_Date__c = System.today(),
                                                     Unit_Name__c = 'Abc/10/1010',
                                                     Property_Name__c = 'xyz',
                                                     SC_From_RentalIncome__c = false,
                                                     Registration_Status_Code__c = 'ABC',
                                                     CM_Units__c = NULL
                                                     );
        insert objBU;

        Robocall_Campaign__c roboCampaign = new Robocall_Campaign__c(Campaign_Id__c = '5678',
                                                                     Priority__c = '7'
                                                                     );
        insert roboCampaign;

        FmHttpCalloutMock.Response roboCallAPIsReponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
        '     "dnd_count": "1",  '  +
        '     "message": "call added successfully",  '  +
        '     "sivr_call_ids": [  '  +
        '       {  '  +
        '         "call_id": "a3341e69-b806-4e46-bed3-cc695d7fd90f"  '  +
        '       },  '  +
        '       {  '  +
        '         "call_id": "1ea7a246-3a76-4b71-af7d-5dbe62aff5a9"  '  +
        '       }  '  +
        '     ]  '  +
        '  }  ' );

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response>{
            Label.RC_Add_Calls => roboCallAPIsReponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.startTest();
        RoboCallBatch be = new RoboCallBatch('1234', roboCampaign.Id);
        database.executebatch(be, 100);
        Test.stopTest();
    }
}