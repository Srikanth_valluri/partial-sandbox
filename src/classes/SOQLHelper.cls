public class SOQLHelper  {
    public static String getObjectQuery(String ObjectName) { 
        String query = 'SELECT ';
        
        Map<String, Schema.SObjectField> SObjectFieldMap = Schema.getGlobalDescribe().get(ObjectName).getDescribe().fields.getMap();
        
        for (Schema.SObjectField f : SObjectFieldMap.values()) { 
            Schema.DescribeFieldResult d = f.getDescribe();
            
            query += d.getName() + ',';
        } 
        
        query = query.left(query.length() - 1);
        query += ' FROM ' + ObjectName;
        System.debug(query);
        return query;   
    }

    public static String getObjectQueryWithParent(String ObjectName, Map<String, String> parentObjectNameMap) { 
        Map<String, String> childObjectRelationNameMap = new Map<String, String>();
        
        return getObjectQueryWithParent(ObjectName, parentObjectNameMap, childObjectRelationNameMap);
    }

    public static String getObjectQueryWithParent(String ObjectName, Map<String, String> parentObjectNameMap, Map<String, String> childObjectRelationNameMap) {
        Map<String, String> childObjectOrderMap = new Map<String, String>();
        
        return getObjectQueryWithParent(ObjectName, parentObjectNameMap, childObjectRelationNameMap, childObjectOrderMap);
    }
    
    public static String getObjectQueryWithParent(String ObjectName, Map<String, String> parentObjectNameMap, Map<String, String> childObjectRelationNameMap, Map<String, String> childObjectOrderMap) { 
        String query = 'SELECT ';
        
        query += getObjectFields(ObjectName, '');

        for (String fieldName : parentObjectNameMap.keySet()) {
            String parentObjectName = parentObjectNameMap.get(fieldName);

            query += ',' + getObjectFields(parentObjectName, fieldName);

        }
        
        for (String relationName : childObjectRelationNameMap.keySet()) {
            String parentObjectName = childObjectRelationNameMap.get(relationName);
            String orderByField = childObjectOrderMap.get(relationName);
            
            if (orderByField != null && !String.isBlank(orderByField)) {
                orderByField = ' ORDER BY ' + orderByField;
            } else {
                orderByField = '';
            }

            query += ', ( SELECT ' + getObjectFields(parentObjectName, '') + ' FROM ' + relationName + orderByField + ')';

        }
        
        //query = query.left(query.length() - 1);
        query += ' FROM ' + ObjectName;
        System.debug(query);
        return query;   
    }
    
    public static String getObjectFields(String objectName, String prefix) {
        String query = '';
        
        Map<String, Schema.SObjectField> SObjectFieldMap = Schema.getGlobalDescribe().get(ObjectName).getDescribe().fields.getMap();
        
        for (Schema.SObjectField f : SObjectFieldMap.values()) { 
            
            Schema.DescribeFieldResult d = f.getDescribe();
            
            if (!d.isAccessible()) {
                continue;
            }
            
            String fieldPrefix = '';
            if (!String.isBlank(prefix)) {
                fieldPrefix = prefix.replace('__c', '__r') + '.';
            }
            query += fieldPrefix + d.getName() + ',';
        } 
        
        query = query.left(query.length() - 1);
        System.debug(query);
        return query;   
    }
}