public class MollakInvoiceSearchLWCController {
    @AuraEnabled(cacheable=true)
    public static list<EmailMessage> searchMollakInvoice(String recordId) {
        System.debug('recordId>>>>'+recordId);
        Set<String> setOfEmails;
        List<EmailMessage> emailMessageList = new List<EmailMessage> ();
        if(String.isNotBlank(recordId)) {
             if(recordId.startsWithIgnoreCase('001')) {
                setOfEmails = getAccountEmailDetailsSet(recordId);
             }
             else if(recordId.startsWithIgnoreCase('a0x')) {
                Booking_Unit__c objBookingUnit = [Select Id,
                                                         Account_Id__c
                                                  FROM Booking_Unit__c
                                                  WHERE Id =:recordId];
                if(String.isNotBlank(objBookingUnit.Account_Id__c)) {
                    setOfEmails = getAccountEmailDetailsSet(objBookingUnit.Account_Id__c);
                }
                
             }
        }
        if(setOfEmails != null && setOfEmails.size() > 0) {
            System.debug('emailMessageList>>>>>'+setOfEmails);
            List<EmailMessage> emailMessagesList = [SELECT Id,
                                            HtmlBody,
                                            Parent.CaseNumber,
                                            Parent.Recordtype.Name,
                                            Parent.Subject,
                                            Parent.SuppliedEmail,
                                            ParentId,
                                            Subject,
                                            ToAddress
                                    FROM EmailMessage
                                    WHERE  Parent.Recordtype.Name = 'FM Collection Email'
                                    AND    Parent.Subject LIKE '%SERVICE CHARGE INVOICE%'
                                    AND    Parent.SuppliedEmail = 'noreply@dubailandmail.gov.ae'
                                    AND    To_Add__c LIKE :setOfEmails
                                    Order by CreatedDate DESC
                                    ];
                                    
            for(EmailMessage objEmail : emailMessagesList) {
                System.debug('objEmail>>>>>'+objEmail);
                emailMessageList.add(objEmail);
            }
            System.debug('emailMessageList>>>>>'+emailMessageList);
            System.debug('emailMessageListSize>>>>>'+emailMessageList.size());

        }

        return emailMessageList;
    }
    
    public static set<String> getAccountEmailDetailsSet(String recordId) {
        Account account = new Account();
        Set<String> setOfEmails = new Set<String>();
        GetPhoneAndEmailFieldAccount__mdt getPhoneAndEmailFieldAccountList = [SELECT Id,
                                                                                    Email_Field_Name__c,
                                                                                    Phone_Field_Name__c,
                                                                                    Account_Record_Types__c
                                                                            FROM GetPhoneAndEmailFieldAccount__mdt
                                                                            WHERE DeveloperName = 'Fields' ];
        if(getPhoneAndEmailFieldAccountList != null) {
            String query='SELECT Id,'
                    +getPhoneAndEmailFieldAccountList.Email_Field_Name__c+
                    ' FROM Account';
                    query+=' WHERE'+' Id =: recordId';
                    
            account = Database.query(query);        
        }
        System.debug('account>>>>'+account);
        System.debug('accountEmail>>>>'+getPhoneAndEmailFieldAccountList.Email_Field_Name__c);
        if(account != Null) {
            for(String str : getPhoneAndEmailFieldAccountList.Email_Field_Name__c.split(',') ) {
                if(account.get(str) != null) {
                    String strWithMod = '%'+account.get(str)+'%';
                    setOfEmails.add(strWithMod);
                }
    
            }
        System.debug('setOfEmails>>>>>'+setOfEmails);
        }
        
        return setOfEmails;
    }
    
        @AuraEnabled(cacheable=true)
    public static string reSendEmail(String emailId) {
        String message;
        if(String.isNotBlank(emailId)) {
        EmailMessage emailMessageObj = [SELECT Id,
                                    HtmlBody,
                                    Parent.CaseNumber,
                                    Parent.Recordtype.Name,
                                    Parent.Subject,
                                    Parent.Email__c,
                                    ParentId,
                                    Subject,
                                    ToAddress
                            FROM EmailMessage 
                            WHERE Id =: emailId];
        
        String subject = String.isNotBlank(emailMessageObj.Subject) ? emailMessageObj.Subject : '';
        String toAddress = String.isNotBlank(emailMessageObj.ToAddress) ? emailMessageObj.ToAddress.replace(';',',') : '';
        String contentType = 'text/html';
        String bccAddress = '';
        String replyToAddress ='';
        String strCCAddress = '';
        String contentValue = String.isNotBlank(emailMessageObj.HtmlBody) ? emailMessageObj.HtmlBody : '';
        String fromAddress = Label.MollakEmailFromAddress;
        system.debug('toAddress'+toAddress);
        system.debug('subject'+subject);
        system.debug('contentValue'+contentValue);
          if( String.isNotBlank( toAddress ) && String.isNotBlank( subject ) 
               && String.isNotBlank( contentValue ) ) {
                     try {
                       system.debug('in send email');
                       
                       //Sending emails through Sendgrid
                       SendGridEmailService.SendGridResponse objSendGridResponse = 
                       SendGridEmailService.sendEmailService( toAddress,''
                                                             , strCCAddress, ''
                                                             , bccAddress, ''
                                                             , subject, ''
                                                             , fromAddress, ''
                                                             , replyToAddress, ''
                                                             , contentType
                                                             , contentValue, '',
                                                             new List<Attachment>{});
                       String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                       system.debug('responseStatus------>'+responseStatus);
                       message = 'Email resent successfully';
                    }
                     catch( Exception e ) {
                        system.debug('error while sending email'+e);
                        message =e.getMessage();
                    }
               
        
            }
        }
       return message;
    }
}