@RestResource(urlMapping='/picklistInfo/*')
global class Damac_PickListInfo{
    @HttpGet 
    global static void getNewFundraiser(){
        RestResponse res = RestContext.response;
        RestRequest req = RestContext.request;
        
        Map<String, List <String>> results = new Map<String, List <String>>();
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get ('Inquiry__c').getDescribe ().fields.getMap ();
        
        Schema.DescribeFieldResult theFieldResult = fieldMap.get('Interested_Develop__c').getDescribe();
        Schema.sObjectField theField = theFieldResult.getSObjectField();
        
        Schema.DescribeFieldResult ctrlFieldResult = fieldMap.get('Interested_Loc__c').getDescribe();
        Schema.sObjectField ctrlField = ctrlFieldResult.getSObjectField();
        
        results = DependentPickListFields.getDependentOptionsImpl(theField, ctrlField);
        
        String responseBody = '['+JSON.serialize(results);
        
        responseBody += ',{"Products": '+JSON.serialize (getPRoducts())+'}]';
        
        res.responseBody = blob.valueof(responseBody);
        res.statusCode = 200;
                
    }
    
    public static Map <String, ID> getProducts() {
        list<inventory__c> invS = new list<inventory__c>();
        invS  = [select Marketing_Name_Doc__r.name, Marketing_Name_Doc__c from inventory__c where status__c='Released' ORDER BY ACD_Date__c asc LIMIT 10000 ];

        Map<String, Id> productname_idmap = new Map<String, Id>();
        
        if(invS.size() > 0){
            for(inventory__c inv:invs){
                if(inv.Marketing_Name_Doc__c != null){
                    productname_idmap.put(inv.Marketing_Name_Doc__r.name,inv.Marketing_Name_Doc__c);  
                }
            }
        }
        return productname_idmap;
    }
    
}