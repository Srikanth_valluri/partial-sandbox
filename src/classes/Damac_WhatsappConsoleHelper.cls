global class Damac_WhatsappConsoleHelper {
    
    public List <Nexmo_Whats_App_Request__c> newConversations { get; set; }
    public List <Inquiry__c> newRequestsToStart { get; set; }
    public Map <ID, Nexmo_Whats_App_Message__c> latestMessage { get; set; }
    public String waId { get; set; }
    public String inqId { get; set; }
    public Damac_WhatsappConsoleHelper () {
        inqId = '';
        init ();
    }
    
    @RemoteAction
    global static List < Inquiry__c > searchInquiryName(String pSearchKey) {
        String searchString = '%' +pSearchKey +'%';
        ID userId = UserInfo.getUserID();
        User userCheck = new User();
        userCheck = [SELECT FederationIdentifier FROM User WHERE ID =: UserInfo.getUserId()];
        if (userCheck.FederationIdentifier == 'venkatasubhash.k') {
            return [SELECT Full_Name__c, Name, Pre_Inquiry_Number__c FROM Inquiry__c
                    WHERE NAME != NULL
                    AND (First_Name__c LIKE: searchString OR Last_Name__c LIKE: searchString OR Name LIKE: searchString )
                    ORDER By Name DESC
                    LIMIT 10
                   ];
        } else {
            return [SELECT Full_Name__c, Name, Pre_Inquiry_Number__c FROM Inquiry__c
                    WHERE NAME != NULL
                    AND ownerId =:userId
                    AND (First_Name__c LIKE: searchString OR Last_Name__c LIKE: searchString OR Name LIKE: searchString )
                    ORDER By Name DESC
                    LIMIT 10
                   ];
        }
    }
    
    
    
    public void init () {        
        newConversations = new List <Nexmo_Whats_App_Request__c> ();
        newRequestsToStart = new List <Inquiry__c> ();
        latestMessage = new Map <Id, Nexmo_Whats_App_Message__c> ();
        waId = '';
        newConversations = [SELECT Name, Last_Message_Sent__c, Read_At__c, Customer_Name__c, Last_Message__c, Latest_Inbound_Message_Received_on__c,
                            Inquiry__c, Inquiry__r.Name, Inquiry__r.Full_Name__c FROM Nexmo_Whats_App_Request__c
                            WHERE Inquiry__c != NULL 
                            AND Inquiry__r.ownerId=:UserInfo.getUserID()
                            AND Latest_Inbound_Message_Received_on__c !=null Order By Latest_Inbound_Message_Received_on__c DESC LIMIT 100]; 
        System.debug('+++inqId:::'+inqId);
        if (inqId != '' && inqId != NULL) {
            
            for (Nexmo_Whats_App_Request__c req :[SELECT Last_Message_Sent__c, Read_At__c, 
                                                  Customer_Name__c, Last_Message__c, Latest_Inbound_Message_Received_on__c,
                                                  Inquiry__c, Inquiry__r.Name, Inquiry__r.Full_Name__c FROM Nexmo_Whats_App_Request__c
                                                  WHERE Inquiry__c =: inqId
                                                  AND Inquiry__r.ownerId=:UserInfo.getUserID()
                                                  AND Latest_Inbound_Message_Received_on__c !=null
                                                  Order By Latest_Inbound_Message_Received_on__c DESC LIMIT 100])
            {
                newConversations.add (req);
                waId = req.id;                
            }
            
        }
        Set <ID> msgIds = new Set <ID> ();
        for (Nexmo_Whats_App_Request__c req :newConversations) {
            msgIds.add (req.Id);
        }          
        for (Nexmo_Whats_App_Message__c msg :[SELECT Nexmo_Whats_App_Request__r.Inquiry__r.Name, CreatedDate, Nexmo_Whats_App_Request__c, Nexmo_Message_Body__c FROM Nexmo_Whats_App_Message__c
                                              WHERE Nexmo_Whats_App_Request__c IN :msgIds order by CreatedDate DESC]) 
        {
            msg.message_body_url__c = msg.CreatedDate.format();
            latestMessage.put (msg.Nexmo_Whats_App_Request__c, msg);
        }
        System.debug(waId);
        if (waId == '' && inqId != '' && inqId != NULL) {
            Inquiry__c inq = [SELECT Name, Full_Name__c FROM Inquiry__c WHERE Id =: inqId];
            newRequestsToStart.add (inq);
            
        }
    }
    
    @RemoteAction
    global static void updateReadMsg (ID waReqId) {
        Nexmo_Whats_App_Request__c req = new Nexmo_Whats_App_Request__c();
        req.id = waReqId;
        req.Last_Read_by_Agent_At__c = System.Now();
        update req;
    }
    
}