@RestResource(urlMapping='/ShareSRWithDealTeam/*')
global without sharing class ShareSRWithDealTeam{   
    @httpPOST
    global static void doPOST () {
        RestRequest  req     = RestContext.request;   
        String jsonInput  = req.requestBody.toString();
        System.debug('::::::::::Response Body::::::::::::'+jsonInput );
        if (jsonInput != NULL && jsonInput != '') {
            Set <ID> inqIds = new Set <ID> ();
            Map <String, Object> response = (Map <String, Object>) JSON.deserializeuntyped(jsonInput );
            String srID = String.valueOf (response.get ('srId'));
            
            updateDealTeam(srId);
            
        }
  }
  
  public static void updateDealTeam(id SrRecordId){
        
        Map<ID, Deal_Team__c> dealTeamMap = new Map<ID, Deal_Team__c> ([SELECT Associated_PC__c, Associated_Deal__c, 
                        Associated_DOS__c, Associated_HOS__c, Associated_HOD__c FROM Deal_Team__c
                        WHERE Associated_Deal__c =: SrRecordId]);
                        
        DealTeamTrgHandler obj = new DealTeamTrgHandler();
        obj.sharingSRonDealToPC(dealTeamMap );
    }
}