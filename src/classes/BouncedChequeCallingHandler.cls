/**/
public without sharing class BouncedChequeCallingHandler {
    public static Id bouncedChequeCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Bounced Cheque').RecordTypeId;
    public static List<Calling_List__c>updateNewCallingLst;
    public static  List<Calling_List__c>cloneLstForQueue ;
    public static  List<Calling_List__c>clonedCallingLst;
    public static Set<Id>updateDoNotCalllingLst;
    public static Map<Id,Calling_List__c>accountIdsToCallingForCREMap;
    
    /*this method is used to perform before update actions */
    public static void onBeforeUpdate(Map<Id,Calling_List__c> mapOldCallList, List<Calling_List__c>  newCallingLst ){
        updateNewCallingLst = new List<Calling_List__c>();
        for(Calling_List__c callingInst : newCallingLst ){
            if( callingInst.RecordTypeId == bouncedChequeCallingRecordTypeId){
                    updateNewCallingLst.add(callingInst);
            }
        }
        if(updateNewCallingLst != null && !updateNewCallingLst.isEmpty() ){
             for(Calling_List__c callInst : updateNewCallingLst){
                 if(callInst.Call_Outcome__c != null && 
                    (mapOldCallList.get(callInst.Id).Call_Outcome__c != callInst.Call_Outcome__c)){
                          if(callInst.Call_Outcome__c == 'Call Back'){
                                if(callInst.Call_Back_Date__c != null){
                                    DateTime dt  = DateTime.valueOf(String.valueOf(Date.valueOf(callInst.Call_Back_Date__c)) +' '+'08:30:00');
                                    callInst.Notification_Time_for_outcome__c = DateTime.valueOf(dt.format('yyyy-MM-dd HH:mm:ss'));
                                    system.debug('callInst.Notification_Time_for_outcome__c:::##'+callInst.Notification_Time_for_outcome__c);
                                }
                          }
                   }
             }
        }
    }
    public static void onAfterUpdate(Map<Id,Calling_List__c> mapOldCallList, List<Calling_List__c>  newCallingLst ) { 
        Set<Id>AccountIdSet = new Set<Id>();
        updateNewCallingLst = new List<Calling_List__c>();
        accountIdsToCallingForCREMap = new Map<Id,Calling_List__c>();
        System.debug('newCallingLst::size:'+newCallingLst.size());
        for(Calling_List__c callInst : newCallingLst){
            if(callInst.recordTypeId == bouncedChequeCallingRecordTypeId){
                updateNewCallingLst.add(callInst);
            }
        }
        if(updateNewCallingLst != null && !updateNewCallingLst.isEmpty() ){
            for(Calling_List__c callInst : updateNewCallingLst){
                 if(callInst.Call_Outcome__c != null && 
                   (mapOldCallList.get(callInst.Id).Call_Outcome__c != callInst.Call_Outcome__c)){
                        if(callInst.Call_Outcome__c == 'Incorrect Contact Details'||
                            callInst.Call_Outcome__c == 'Not Reachable - Email sent for call back'){
                            accountIdsToCallingForCREMap.put(callInst.Account__c,callInst);
                        }
                        else if(callInst.Call_Outcome__c == 'Legal/Dispute cases/DLD/RERA Related'){
                            AccountIdSet.add(callInst.Account__c);
                        }
                 }  
            }
        }
        /**/
        if(accountIdsToCallingForCREMap != null && !accountIdsToCallingForCREMap.isEmpty()){
            fetchPrimaryAndSecondaryCRE(accountIdsToCallingForCREMap,false);
        }
        /*for checking do not call on accounts */
        if(AccountIdSet != null && !AccountIdSet.isEmpty()){
            checkDoNotCallforAccount(AccountIdSet);
        }
    }
       /*fetch Prmary and secondary CRE for the account to send notification*/
    public static  void fetchPrimaryAndSecondaryCRE( Map<Id,Calling_List__c> accoundIdToCallingMap,Boolean incorrectCOD){
         Map<Id,List<String>>accIdToUserEmailsLstMap = new Map<Id,List<String>>();
         cloneLstForQueue = new List<Calling_List__c>(); 
         List<Account> PrimarySecondaryCREAccLst = [ SELECT Id,
                                                          Primary_CRE__c,
                                                          Tertiary_CRE__c,
                                                          Secondary_CRE__c,
                                                          Primary_CRE__r.Email,
                                                          Secondary_CRE__r.Email
                                                     FROM Account 
                                                    WHERE Id 
                                                     IN : accoundIdToCallingMap.keySet()];
        Map<Account,Calling_List__c>accountToCallingMap = new Map<Account,Calling_List__c>();
        if(incorrectCOD == false){
            for(Account accObj : PrimarySecondaryCREAccLst){
                if(!accountToCallingMap.containsKey(accObj)){
                    accountToCallingMap.put(accObj,accoundIdToCallingMap.get(accObj.Id));
                }
            }
            for(Account accInst : accountToCallingMap.keySet()){
                if(accInst.Primary_CRE__c == null &&
                    accInst.Secondary_CRE__c == null && 
                    accInst.Tertiary_CRE__c == null){
                        cloneLstForQueue.add(accountToCallingMap.get(accInst));
                }
            }
            
        }
        
        /*cloneLstForQueue clone this calling list and assigned 'Contact Center Queue'
         queueOwner to cloned record */
         boolean isQueueOwner = false;
        if(cloneLstForQueue != null && !cloneLstForQueue.isEmpty()){
            isQueueOwner = true;
            List<Calling_List__c>clonedLst = createClone(cloneLstForQueue,isQueueOwner);
        }
        
    } 
    /*method for cloning the calling list record */
    public static List<Calling_List__c>  createClone(List<Calling_List__c> toBeCloneCallingLst,Boolean createClone){
            Id recoveryCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Recovery Calling List').RecordTypeId;
            clonedCallingLst = new list<Calling_List__c>() ;
            if(toBeCloneCallingLst != null && !toBeCloneCallingLst.isEmpty()){
                for(Calling_List__c toBecloneInst : toBeCloneCallingLst){
                     Calling_List__c clonedObj = toBecloneInst.clone(false,false,false,false);
                     clonedObj.Calling_List__c = toBecloneInst.Id;
                     if(createClone == false){
                        clonedObj.RecordTypeId = recoveryCallingRecordTypeId;
                        clonedCallingLst.add(clonedObj);
                     }
                     else{
                        /*fetch queue record and update that queue id with cloned ownerId*/
                         List<Group> groupLst = [SELECT Id 
                                     FROM Group 
                                     WHERE Type = 'Queue' 
                                     AND NAME = 'Contact Center Queue'
                                     limit 1];
                          clonedObj.OwnerId = groupLst[0].Id;
                         clonedCallingLst.add(clonedObj);
                     }
                }
            }
        insert clonedCallingLst;
        return clonedCallingLst;
    }
     /*method for sending an email*/
    /*public static void sendNotification(List<String>toAddressLst,List<String>ccAddressLst,String subject,String body){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSubject(subject);
        mail.setHtmlBody(body);
        mail.setToAddresses(toAddressLst);
        if(ccAddressLst != null && !ccAddressLst.isEmpty()){
            mail.setCcAddresses(ccAddressLst);
        }
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }*/
    /*Method for updating accounts with do not call*/
    public static void checkDoNotCallforAccount(Set<Id> accountIdSet){
        List<Calling_List__c>hideCallingList = new List<Calling_List__c>();
        List<Calling_List__c>updateHideCallingList = new List<Calling_List__c>();
        List<Account>doNotCallAccLst = new List<Account>();
        List<Account> accLst = [SELECT Id,
                                       PersonDoNotCall 
                                  FROM Account 
                                  WHERE Id IN :accountIdSet];
        for(Account acc : accLst){
            if(acc.PersonDoNotCall != true){
                acc.PersonDoNotCall = true;
                doNotCallAccLst.add(acc);
            }
        }
        update doNotCallAccLst; 
        /*check IsDoNotCallAccount field for all calling list records which are associated with above accounts */
        updateDoNotCalllingLst = new Set<Id>();
        for(Calling_List__c callObj : updateNewCallingLst){
            updateDoNotCalllingLst.add(callObj.Id);
        }
        hideCallingList = [SELECT Id ,
                                  Account__c,
                                  IsDoNotCallAccount__c
                             FROM Calling_List__c 
                             WHERE Account__r.Id IN : accountIdSet
                             AND IsDoNotCallAccount__c = false 
                             AND Id IN : updateDoNotCalllingLst
                            ];
        for(Calling_List__c callInst : hideCallingList){
            if(callInst.IsDoNotCallAccount__c != true){
                    callInst.IsDoNotCallAccount__c = true;
                    updateHideCallingList.add(callInst);
            }
        }
        update updateHideCallingList;
    }
}