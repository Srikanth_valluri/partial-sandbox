public without sharing class ThankYouPageSiteRedirectController {
    public String savedOrSubmitted{get;set;}
    public ThankYouPageSiteRedirectController(){
        savedOrSubmitted=ApexPages.currentPage().getParameters().get('savedOrSubmitted');
        addPageMessage();
    }
    
    public void addPageMessage(){
       try{
            ApexPages.Message myMsg;
            if(savedOrSubmitted=='saved'){
                myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Succesfully Saved');
            }else{
                myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Succesfully Submitted');
            }
            ApexPages.addMessage(myMsg);
       }
       catch(Exception e){
           ApexPages.addMessages(e);
       }
    }
}