/*
*Change History:
* CH01 # 337 # 14-03-2017 # Sivasankar # Encrypting the Mobile number
* CH02 # 14-03-2018 #Tejashre Chavan # Added Number and email validation logic
* CH03 # 14-02-2019 #Craig Lobo # Send Booking Units to IMPS for Update
* CH04 # 10-01-2020 #QBurst # Send Booking Units to IMPS for Update
* CH05 # 06-11-2020 #QBurst # Added call to util methods in 'DL_ProfileCompletionNotificationUtil' to send promotion email for mobile app user on profile data completion.
*/
public class AccountTrgHandler implements TriggerFactoryInterface  {

    //------ Variables
    public List<User> userListToActivate = new List<User>();
    public List<User> userListToDeActivate = new List<User>();


    public static void updateAgencySites(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        set<ID> accids = new set<Id>();
        for(Id thisKey : mapNewRecords.keySet()){
                Account newAccount = (Account)mapNewRecords.get(thisKey);
                Account oldAccount = (Account)mapOldRecords.get(thisKey);
            if(newAccount.Blacklisted__c != oldAccount.Blacklisted__c || newAccount.Terminated__c != oldAccount.Terminated__c){
                accids.add(thisKey);
            }
        }
        //and (Agency__r.Blacklisted__c = true or Agency__r.Terminated__c = true)
        List<Agent_Site__c> lstAgs = new List<Agent_Site__c>();
        for(Agent_Site__c ags : [select id,name,Start_Date__c,Agency__r.Blacklisted__c,IsAccountBlackListedTerminated__c,Agency__r.Terminated__c,Active__c,Agency__c,End_Date__c,Reinstated_Date__c from Agent_Site__c where Agency__c in : accids]){
            if((ags.Agency__r.Blacklisted__c || ags.Agency__r.Terminated__c) && ags.End_Date__c == null){
                ags.End_Date__c = system.today();
                ags.Active__c = false;
                ags.IsAccountBlackListedTerminated__c = true;
            }
            else if(!ags.Agency__r.Blacklisted__c && !ags.Agency__r.Terminated__c && ags.IsAccountBlackListedTerminated__c){
                ags.End_Date__c = null;
                ags.Active__c = true;
                ags.IsAccountBlackListedTerminated__c = false;
            }
            lstAgs.add(ags);
        }
        if(lstAgs != null && !lstAgs.isempty()){
            update lstags;
        }

    }
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed before insert.                       *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeBeforeInsertTrigger(List<sObject> newAccountsList){
        List<Account> updateAccountList = new List<Account>();
        try{
            encryptMobileNumbers((List<Account>)newAccountsList);//CH01
            for(sObject thisSObject : newAccountsList){
                Account thisAccount = (Account) thisSObject;
                if(thisAccount.IsPersonAccount){
                    updateAccountList.add(thisAccount);
                }
            }
            // Calling method to update keys.
            if(!updateAccountList.isEmpty()){
                updateKeys(updateAccountList);
            }


        }catch(exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
    }

    /*********************************************************************************************
    * @Description : Method to contain logic to be executed before update.                       *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeBeforeUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        List<Account> updateAccountList = new List<Account>();
        //ShareAccount.ShareAccountRecord(mapNewRecords,mapOldRecords);
        try{
            encryptMobileNumbers((List<Account>)mapNewRecords.values());//CH01
            for(Id thisKey : mapNewRecords.keySet()){
                Account newAccount = (Account)mapNewRecords.get(thisKey);
                Account oldAccount = (Account)mapOldRecords.get(thisKey);
                if(newAccount.IsPersonAccount){
                    if(newAccount.FirstName != oldAccount.FirstName ||
                       newAccount.LastName != oldAccount.LastName ||
                       newAccount.Phone != oldAccount.Phone ||
                       newAccount.Email__c != oldAccount.Email__c){
                        updateAccountList.add(newAccount);
                    }
                }
            }
            // Calling method to update keys.
            if(!updateAccountList.isEmpty()){
                updateKeys(updateAccountList);
            }
        }catch(exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
    }

    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after insert.                        *
    * @Params      : Map<Id, sObject>                                                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterInsertTrigger(Map<Id, sObject> mapNewRecords) {
        // CH02 #Tejashree Chavan # Validation of number and email.
        Set<Id> newAccountId = new Set<Id>();
        for(Account acc : (List<Account>) mapNewRecords.values()) {
                newAccountId.add(acc.Id);
        }
        //Verify phone Number and Email
        if(!newAccountId.isEmpty()) {
            system.debug('------validate mobile and emial----'+newAccountId);
            //ValidateAccountEmailAndNumbers.verifyAccEmailAndNumbersFromWebService(newAccountId);
        }
    }

    /*
    * By Pavithra , Is invoked by approval process result when Blacklisted/Terminated/Unblacklisted/Unterminated
    */
    public void executeAfterUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        List<Id> accountListForDeactivateUser = new List<Id>();
        List<Id> accountListToActivateUser = new List<Id>();
        List<String> accountIdList = new List<String>();
        Set<Id> accountIdSet = new Set<Id>();
        List<Agency_PC__c> agencyPCList = new List<Agency_PC__c>();

        for(Sobject sobj: mapNewRecords.values()){
            Account updatedAccount = (Account) sobj;
            Account previousValues = (Account) mapOldRecords.get(updatedAccount.Id);
            if((updatedAccount.Blacklisted__c &&  previousValues.Blacklisted__c==false)
                || (updatedAccount.Terminated__c && previousValues.Terminated__c==false)) {
                accountListForDeactivateUser.add(updatedAccount.Id);
            } else if(updatedAccount.Blacklisted__c != previousValues.Blacklisted__c ||
                updatedAccount.Terminated__c != previousValues.Terminated__c
                && (updatedAccount.Terminated__c == false && updatedAccount.Blacklisted__c == false)) {
                //else if(() ((updatedAccount.Blacklisted__c==false &&  previousValues.Blacklisted__c==true) || (updatedAccount.Terminated__c==false && previousValues.Terminated__c==true))){
                accountListToActivateUser.add(updatedAccount.Id);
            }
            // Get all updated account owner to update Agency PC
            if (updatedAccount.OwnerId != previousValues.OwnerId &&
                updatedAccount.Agency_Type__c != null &&
                updatedAccount.Agency_Type__c.Equals('Individual')) {
                accountIdSet.add(updatedAccount.Id);
            }

            if (String.isNotBlank(updatedAccount.Vendor_ID__c) 
                        && updatedAccount.Vendor_ID__c != previousValues.Vendor_ID__c) { // #CH04
                accountIdList.add(updatedAccount.Id);
            }
        }
        
        /* Section below Added on 06-11-2020 : Subin Antony (QBurst) : Azure task 2729 ................ BEGIN */
        Map<ID, Account> newAccMap = (Map<ID, Account>)mapNewRecords;
        Map<ID, Account> oldAccMap = (Map<ID, Account>)mapOldRecords;
        Set<Account> completedProfileOwners = DL_ProfileCompletionNotificationUtil.filterCompletedProfileOwners(newAccMap, oldAccMap);
        DL_ProfileCompletionNotificationUtil.sendPromoCouponNotification(completedProfileOwners);
        /* Section Added on 06-11-2020 : Subin Antony (QBurst) : Azure task 2729 ........................ END */
        
        if(!accountIdSet.isEmpty()) {
            createAgencyPC(mapNewRecords,accountIdSet);
        }
        //---------- Blacklisted or Termination is unchecked
        if(!accountListForDeactivateUser.isEmpty()){
            deactivateAgencyUsers(accountListForDeactivateUser);
            system.enqueueJob(new AsyncAgentWebservice (accountListForDeactivateUser,'Agent Updation'));
        }
        //---------- Blacklisted or Termination is checked
        if(!accountListToActivateUser.isEmpty()){
            activateAgencyUsers(accountListToActivateUser);
            system.enqueueJob(new AsyncAgentWebservice (accountListToActivateUser,'Agent Updation'));
        }
        updateAgencySites(mapNewRecords,mapOldRecords);
        ShareAccount.ShareAccountRecord(mapNewRecords,mapOldRecords);
        updateBUToIPMS(accountIdList);
        ///if (mapNewRecords mapOldRecords)

    }

    public void createAgencyPC(Map<Id, sObject> mapNewRecords,set<Id> accountIdSet) {
        List<Agency_PC__c> newAgencyPCs = new List<Agency_PC__c>();
        Map<String,Map<String,Agency_PC__c>> existingAgencyPC = getExistingAgencyPC(accountIdSet);
        for(Id accId : accountIdSet) {
            Account updatedAccount = (Account) mapNewRecords.get(accId);
            if(existingAgencyPC != null && !existingAgencyPC.isEmpty() &&
               existingAgencyPC.containsKey(accId)) {
                if(existingAgencyPC.get(accId) != null && !existingAgencyPC.get(accId).isEmpty() &&
                   !existingAgencyPC.get(accId).containsKey(updatedAccount.OwnerId) &&
                   updatedAccount.Owner_Profile__c.Equals('Property Consultant')) {
                    Agency_PC__c pcObj = new Agency_PC__c();
                    pcObj.Name = updatedAccount.Account_Owner__c;
                    pcObj.Agency__c = accId;
                    pcObj.Contact__c = updatedAccount.PersonContactId;
                    pcObj.User__c = updatedAccount.OwnerId;
                    newAgencyPCs.add(pcObj);
                }
            }
            else {
                if(updatedAccount.Owner_Profile__c.Equals('Property Consultant')) {
                    Agency_PC__c pcObj = new Agency_PC__c();
                    pcObj.Name = updatedAccount.Account_Owner__c;
                    pcObj.Agency__c = accId;
                    pcObj.Contact__c = updatedAccount.PersonContactId;
                    pcObj.User__c = updatedAccount.OwnerId;
                    newAgencyPCs.add(pcObj);
                }
            }
        }
        if(!newAgencyPCs.isEmpty()) {
            insert newAgencyPCs;
        }
    }

    public Map<String,Map<String,Agency_PC__c>> getExistingAgencyPC(Set<Id> acountIds) {
        Map<String,Map<String,Agency_PC__c>> existingAgencyPC = new Map<String,Map<String,Agency_PC__c>>();
        for(Agency_PC__c pcs : [SELECT
                                    Id,
                                    Name,
                                    Contact__c,
                                    Agency__c,
                                    User__c
                                FROM Agency_PC__c
                                WHERE Agency__c IN :acountIds]) {
            if(!existingAgencyPC.containsKey(pcs.Agency__c)) {
                existingAgencyPC.put(pcs.Agency__c,new Map<String,Agency_PC__c>{});
            }
            if(existingAgencyPC.get(pcs.Agency__c) != null &&
               !existingAgencyPC.get(pcs.Agency__c).containsKey(pcs.User__c)) {
                existingAgencyPC.get(pcs.Agency__c).put(pcs.User__c,pcs);
            }
        }
        return existingAgencyPC;
    }

    /*
    * Activate Account Users
    */
    public void activateAgencyUsers(List<Id> listToActivateUser){
        for(User activateUser : UtilityQueryManager.getAllUsersOfAccounts(listToActivateUser)){
            activateUser.isActive = true ;
            userListToActivate.add(activateUser);
        }

        if(!userListToActivate.isEmpty())
           System.enqueueJob(new UpdateIsActiveOnUser(userListToActivate,userListToDeActivate));

    }

    /*
    * DeActivate Account Users
    */
    public void deactivateAgencyUsers(List<Id> listToDeActivateUser){
        for(User deactivateUser : UtilityQueryManager.getAllUsersOfAccounts(listToDeActivateUser)){
            deactivateUser.isActive = false ;
            userListToDeActivate.add(deactivateUser);
        }
        if(!userListToDeActivate.isEmpty())
            System.enqueueJob(new UpdateIsActiveOnUser(userListToActivate,userListToDeActivate));
    }

    /*********************************************************************************************
    * @Description : Method to update the keys on account record.                                *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    private void updateKeys(List<Account> newAccountRecordsList){
        for(Account thisAccount : newAccountRecordsList){
            if(thisAccount.IsPersonAccount){
                if(String.isNotBlank(thisAccount.FirstName) &&
                   String.isNotBlank(thisAccount.LastName)){
                    if(String.isNotBlank(thisAccount.Email__c)){
                        thisAccount.Email_Key__c = thisAccount.FirstName+'__'+ thisAccount.LastName +'__'+ thisAccount.Email__c;
                    }
                    if(String.isNotBlank(thisAccount.Phone)){
                        thisAccount.Phone_Key__c = thisAccount.FirstName+'__'+ thisAccount.LastName +'__'+ thisAccount.Phone;
                    }
                }
            }
        }
    }

    // TOBE Implemented
    public void executeBeforeInsertUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){ }
    public void executeBeforeDeleteTrigger(Map<Id,sObject> oldRecordsMap){ }
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){

    }
    public void executeAfterDeleteTrigger(Map<Id,sObject> oldRecordsMap){ }

    //CH01.Start
    /*********************************************************************************************
    * @Description : Method to mask the mobile number and encrypt them accordingly               *
    * @Params      : List<Account__c>                                                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    @Testvisible private void encryptMobileNumbers(List<Account> newListAccounts){
        //iterate the Inquiries for masking
        for(Account thisAccount: newListAccounts){
            system.debug('>>>>thisAccount.Record_Type_Name__c>>>>'+thisAccount.Record_Type_Name__c);
            if (thisAccount.Record_Type_Name__c.containsIgnoreCase('Business') || thisAccount.Record_Type_Name__c.containsIgnoreCase('Person')){
                system.debug('>>>>thisAccount.Record_Type_Name__c>>>>'+thisAccount.Record_Type_Name__c);
                if(!validateMobileNumbers(thisAccount)){
                    if(thisAccount.Record_Type_Name__c.containsIgnoreCase('Person')){
                        thisAccount.PersonAssistantPhone = ( String.isBlank(thisAccount.Asst_Phone_Encrypt__c) && String.isBlank(thisAccount.Asst_Phone_Country_Code__c) ? '' :  ((String.isNotBlank(thisAccount.Asst_Phone_Encrypt__c) && (Trigger.isInsert || thisAccount.PersonAssistantPhone == null)) ? (thisAccount.Asst_Phone_Country_Code__c.subStringAfter(':')).trim()+''+UtilityHelperCls.removeFirstZero(thisAccount.Asst_Phone_Encrypt__c) : thisAccount.PersonAssistantPhone));
                        thisAccount.PersonHomePhone = ( String.isBlank(thisAccount.Home_Phone_Encrypt__c) && String.isBlank(thisAccount.Home_Phone_Country_Code__c) ? '' :  ((String.isNotBlank(thisAccount.Home_Phone_Encrypt__c) && (Trigger.isInsert || thisAccount.PersonHomePhone == null)) ? (thisAccount.Home_Phone_Country_Code__c.subStringAfter(':')).trim()+''+UtilityHelperCls.removeFirstZero(thisAccount.Home_Phone_Encrypt__c) : thisAccount.PersonHomePhone));
                        thisAccount.PersonMobilePhone = ( String.isBlank(thisAccount.Mobile_Phone_Encrypt__c) && String.isBlank(thisAccount.Mobile_Country_Code__c) ? '' :  ((String.isNotBlank(thisAccount.Mobile_Phone_Encrypt__c) && (Trigger.isInsert || thisAccount.PersonMobilePhone == null)) ? (thisAccount.Mobile_Country_Code__c.subStringAfter(':')).trim()+''+UtilityHelperCls.removeFirstZero(thisAccount.Mobile_Phone_Encrypt__c) : thisAccount.PersonMobilePhone));
                        thisAccount.PersonOtherPhone = ( String.isBlank(thisAccount.Other_Phone_Encrypt__c) && String.isBlank(thisAccount.Other_Phone_Country_Code__c) ? '' :  ((String.isNotBlank(thisAccount.Other_Phone_Encrypt__c) && (Trigger.isInsert || thisAccount.PersonOtherPhone == null)) ? (thisAccount.Other_Phone_Country_Code__c.subStringAfter(':')).trim()+''+UtilityHelperCls.removeFirstZero(thisAccount.Other_Phone_Encrypt__c) : thisAccount.PersonOtherPhone));

                        //Encrypting Mobile and storing it on the record.
                        thisAccount.Asst_Phone_Encrypt__c = UtilityHelperCls.encryptMobile(UtilityHelperCls.removeFirstZero(thisAccount.PersonAssistantPhone));
                        thisAccount.Home_Phone_Encrypt__c = UtilityHelperCls.encryptMobile(UtilityHelperCls.removeFirstZero(thisAccount.PersonHomePhone));
                        thisAccount.Mobile_Phone_Encrypt__c = UtilityHelperCls.encryptMobile(UtilityHelperCls.removeFirstZero(thisAccount.PersonMobilePhone));
                        thisAccount.Other_Phone_Encrypt__c = UtilityHelperCls.encryptMobile(UtilityHelperCls.removeFirstZero(thisAccount.PersonOtherPhone));
                    }
                    thisAccount.Phone = ( String.isBlank(thisAccount.Phone_Encrypt__c) && String.isBlank(thisAccount.Phone_Country_Code__c) ? '' :  ((String.isNotBlank(thisAccount.Phone_Encrypt__c) && (Trigger.isInsert || thisAccount.Phone == null)) ? (thisAccount.Phone_Country_Code__c.subStringAfter(':')).trim()+''+UtilityHelperCls.removeFirstZero(thisAccount.Phone_Encrypt__c) : thisAccount.Phone));
                    thisAccount.Phone_Encrypt__c = UtilityHelperCls.encryptMobile(UtilityHelperCls.removeFirstZero(thisAccount.Phone));
                }
            }
        }
    }



    /*********************************************************************************************
    * @Description : Method to validate the mobile number and country code values                *
    * @Params      : Account                                                                     *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    @TestVisible private Boolean validateMobileNumbers(Account thisAccount){
        Boolean isError = false;
        system.debug('>>>Mobile_Country_Code__c>>>'+thisAccount.Mobile_Country_Code__c);
        system.debug('>>>Mobile_Phone_Encrypt__c>>>'+thisAccount.Mobile_Phone_Encrypt__c);
        if(String.isNotBlank(thisAccount.Asst_Phone_Country_Code__c) && String.isBlank(thisAccount.Asst_Phone_Encrypt__c)){
             thisAccount.Asst_Phone_Encrypt__c.addError('Asst. Phone can\'t be balnk if country code is selected.');
             isError = true;
        }
        if (String.isBlank(thisAccount.Asst_Phone_Country_Code__c) && String.isNotBlank(thisAccount.Asst_Phone_Encrypt__c)){
            thisAccount.Asst_Phone_Country_Code__c.addError('Asst. Phone Country Code can\'t be balnk if Asst. Phone is Populated.');
            isError = true;
        }
        if(String.isNotBlank(thisAccount.Home_Phone_Country_Code__c) && String.isBlank(thisAccount.Home_Phone_Encrypt__c)){
             thisAccount.Home_Phone_Encrypt__c.addError('Home Phone can\'t be balnk if Home Phone country code is selected.');
             isError = true;
        }
        if (String.isBlank(thisAccount.Home_Phone_Country_Code__c) && String.isNotBlank(thisAccount.Home_Phone_Encrypt__c)){
            thisAccount.Home_Phone_Country_Code__c.addError('Home Phone Country Code can\'t be balnk if Home Phone is Populated.');
            isError = true;
        }
        if(String.isNotBlank(thisAccount.Mobile_Country_Code__c) && String.isBlank(thisAccount.Mobile_Phone_Encrypt__c)){
             thisAccount.Mobile_Phone_Encrypt__c.addError('Mobile can\'t be balnk if Mobile country code is selected.');
             isError = true;
        }
        if (String.isBlank(thisAccount.Mobile_Country_Code__c) && String.isNotBlank(thisAccount.Mobile_Phone_Encrypt__c)){
            thisAccount.Mobile_Country_Code__c.addError('Mobile Country Code can\'t be balnk if Mobile is Populated.');
            isError = true;
        }
        if(String.isNotBlank(thisAccount.Other_Phone_Country_Code__c) && String.isBlank(thisAccount.Other_Phone_Encrypt__c)){
             thisAccount.Other_Phone_Encrypt__c.addError('Other Phone can\'t be balnk if Other Phone country code is selected.');
             isError = true;
        }
        if (String.isBlank(thisAccount.Other_Phone_Country_Code__c) && String.isNotBlank(thisAccount.Other_Phone_Encrypt__c)){
            thisAccount.Other_Phone_Country_Code__c.addError('Other Phone Country Code can\'t be balnk if Other Phone is Populated.');
            isError = true;
        }
        if(String.isNotBlank(thisAccount.Phone_Country_Code__c) && String.isBlank(thisAccount.Phone_Encrypt__c)){
             thisAccount.Phone_Encrypt__c.addError('Phone can\'t be balnk if Phone country code is selected.');
             isError = true;
        }
        if (String.isBlank(thisAccount.Phone_Country_Code__c) && String.isNotBlank(thisAccount.Phone_Encrypt__c)){
            thisAccount.Phone_Country_Code__c.addError('Phone Country Code can\'t be balnk if Phone is Populated.');
            isError = true;
        }
        return isError;
    }
    //CH01.End

    //CH03 Start
    public void updateBUToIPMS(List<String> pAccountIdList) {
        System.debug('>>>>>>updateBUToIPMS>>>>>>>  ' + pAccountIdList);
        if(pAccountIdList != null && !pAccountIdList.isEmpty()){
            /*
            Map<Id, Booking_Unit__c> bookingUnitMap = new Map<Id, Booking_Unit__c>(
                [SELECT Id
                   FROM Booking_Unit__c
                  WHERE Booking__r.Deal_SR__r.Agency__c IN :pAccountIdList
            ]);

            List<String> bookingUnitIdList = 
                (List<String>) JSON.deserialize(JSON.serialize(bookingUnitMap.keySet()), List<String>.class);

            if (bookingUnitIdList != null && !bookingUnitIdList.isEmpty()) {
                System.enqueueJob(new AsyncReceiptWebservice (bookingUnitIdList, 'Agent Update'));
                System.debug('>>>>>>AsyncReceiptWebservice>>>>>>>  ' + bookingUnitIdList);
            }
            */
             // CH04 Start
             Map<Id, Booking__c> bookingMap = new Map<Id, Booking__c>(
                [SELECT Id
                   FROM Booking__c
                  WHERE Deal_SR__r.Agency__c IN :pAccountIdList
            ]);

            List<String> bookingIdList = 
                (List<String>) JSON.deserialize(JSON.serialize(bookingMap.keySet()), List<String>.class);

            if (bookingIdList != null && !bookingIdList.isEmpty()) {
                System.enqueueJob(new AsyncReceiptWebservice (bookingIdList, 'Agent Update'));
                System.debug('>>>>>>AsyncReceiptWebservice>>>>>>>  ' + bookingIdList);
            }
            // CH04 End
            
        }
    }
    //CH03 END

}// End of class.