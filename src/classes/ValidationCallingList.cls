public without sharing class ValidationCallingList {
    
    public static final map<String, Id> mapRecordTypeId ;
    
    static {
        mapRecordTypeId = new map<String, Id>();
        for(RecordType recTypeInst : [select Id, DeveloperName from RecordType
                                        where SobjectType = 'Calling_List__c']) {
            mapRecordTypeId.put(recTypeInst.DeveloperName, recTypeInst.Id);
        }
        system.debug('mapRecordTypeId==='+mapRecordTypeId);
        
    }
    
    public static void Validate(List<Calling_List__c> lstCallingList) {
        
        
        //Added validation For Mortgage_Calling_List Class
        system.debug('mapRecordTypeId==='+mapRecordTypeId.get('Mortgage_Calling_List'));
        Id MortgageRecordTypeId =  mapRecordTypeId.get('Mortgage_Calling_List');
        
        for(Calling_List__c objCL:lstCallingList){
            
            //Added validation For Mortgage_Calling_List Class
            if(objCL.RecordTypeId == MortgageRecordTypeId && objCL.Booking_Unit__c != NULL){
                Booking_Unit__c objBU = [Select Id,(Select id From Calling_List__r Where RecordTypeId =: MortgageRecordTypeId ) 
                                        FROM Booking_Unit__c WHERE Id =: objCL.Booking_Unit__c LIMIT 1];
                system.debug('objBU ==='+ objBU );
                system.debug('objBU ==='+ objBU.Calling_List__r.size() );
                if(objBU.Calling_List__r.size() > 1 )
                objCL.addError('Calling List already there for the selected Unit');
            }
            
        }
        
    }
    
    public static void ValidateCBR(List<Calling_List__c> lstCallingList) {
        
        Id CBRRecordTypeId =  mapRecordTypeId.get('Call_Back_Request');
        
        for(Calling_List__c objCL:lstCallingList){
        //Added validation For Call_Back_Request  Class
            if(objCL.RecordTypeId == CBRRecordTypeId && objCL.Account__c != NULL &&
            objCL.Call_Outcome__c == 'Customer want to re-negotiate' && objCL.Case__c != NULL){
                Map<Id,Case> idCasemap = new Map<Id,Case>();
                Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
                
                List<Case> lstCSRcase = [Select id,Created_DateTime__c From Case 
                                            Where RecordTypeId =: caseRecTypeId
                                            AND AccountId =: objCL.Account__c
                                        ];
                boolean showError = true;
                
                if(lstCSRcase.size() < 2){
                    showError = true;
                    
                }else if(lstCSRcase.size() > 1){
                    for(Case objCase :lstCSRcase){
                        idCasemap.put(objCase.id,objCase);
                    }
                    for(Case objCase :lstCSRcase){
                        if(objCase.Created_DateTime__c > idCasemap.get(objCL.Case__c).Created_DateTime__c )
                        showError = false;
                    }
                }
                
                if(showError)
                objCL.addError('Please create the case summary case for Cx again ');
            }
        }
    }
}