@isTest
public class DeleteSignNowTemplateSettingsBatchTest {
	@isTest
    public static void testBatch(){
        String strRolejson = '[{"order":"1", "name":"Primary Buyer", "email":"test@gmail.com","allow_forwarding":"yes"}]';
        cuda_signnow__CN_TemplateSetting__c objTemplateSetting = new cuda_signnow__CN_TemplateSetting__c();
        objTemplateSetting.Name = 'HO Checklist';
        objTemplateSetting.cuda_signnow__Roles__c = strRolejson;
        objTemplateSetting.cuda_signnow__subject__c = 'Handover checklist for Signature';
        objTemplateSetting.cuda_signnow__Body__c = 'Test Body';
        objTemplateSetting.cuda_signnow__Automatically_Send__c = true;
        objTemplateSetting.cuda_signnow__Skip_to_End__c = true;
        objTemplateSetting.cuda_signnow__Hide_Adv_Set__c = true;
        
        System.debug('###--->strRolejson'+strRolejson);  
        Database.SaveResult[] srList = Database.insert(new List<cuda_signnow__CN_TemplateSetting__c>{objTemplateSetting}, false);
		System.assertEquals(1, [Select count() from cuda_signnow__CN_TemplateSetting__c]);
        
        Datetime yesterday = Datetime.now().addDays(-1);
        Test.setCreatedDate(objTemplateSetting.Id, yesterday);
        
        System.Test.startTest();
        Database.executeBatch(new DeleteSignNowTemplateSettingsBatch());
        System.Test.stopTest();
        System.assertEquals(0, [Select count() from cuda_signnow__CN_TemplateSetting__c]);
    }
}