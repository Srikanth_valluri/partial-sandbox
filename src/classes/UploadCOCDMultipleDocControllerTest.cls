/*
* Description - Test class developed for 'UploadCOCDMultipleDocController'
*
* Version            Date            Author            Description
* 1.0                17/11/17        Snehil            Initial Draft

*/

@isTest 
private class UploadCOCDMultipleDocControllerTest {  
	//UploadCOCDMultipleDocController objUploadCOCDMultipleDocController = new UploadCOCDMultipleDocController();
	//List<UploadCOCDMultipleDocController.MultipleDocRequest> lstWrap = new List<UploadCOCDMultipleDocController.MultipleDocRequest>();
	
    static testMethod void testMethod1() {
    	UploadCOCDMultipleDocControllerTest objcls = new UploadCOCDMultipleDocControllerTest();
    	// Insert Accont
		Account objAcc = TestDataFactory_CRM.createPersonAccount();
		insert objAcc ;
		 
		//Insert Service Request
		NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
		insert objSR ;

		Id dealId = objSR.Deal_ID__c;
		//Insert Bookings
		List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
		insert lstBookings ; 
		  
		//Insert Booking Units
		List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
		insert lstBookingUnits;
		
		//Insert Cases for the units
		list<Case> lstCases = new list<Case>();
		Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
		for( Booking_Unit__c objUnit : lstBookingUnits ) {
			Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
			objCase.Booking_Unit__c = objUnit.Id;
			lstCases.add( objCase );
		}
		insert lstCases ; 
		
		list<SR_Attachments__c> lstAttachment = new list<SR_Attachments__c>();
		for( Case objCase : lstCases ) {
			lstAttachment.add( TestDataFactory_CRM.createCaseDocument( objCase.Id, PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF ) );
		}
		insert lstAttachment ;
		
		UploadCOCDMultipleDocController objUploadCOCDMultipleDocController = new UploadCOCDMultipleDocController();
		List<UploadCOCDMultipleDocController.MultipleDocRequest> lstMultipleDocReq = new List<UploadCOCDMultipleDocController.MultipleDocRequest>();
		UploadCOCDMultipleDocController.MultipleDocRequest objMultipleDoc = new UploadCOCDMultipleDocController.MultipleDocRequest();
		UploadCOCDMultipleDocController.MultipleDocResponse objMultipleDocResponse = new UploadCOCDMultipleDocController.MultipleDocResponse();
		objMultipleDocResponse.PROC_STATUS='STATUS';
        objMultipleDocResponse.PROC_MESSAGE='Test MESSAGE';
        objMultipleDocResponse.PARAM_ID ='12345';
        objMultipleDocResponse.url='http://test.com';
        objMultipleDocResponse.FTP_Response ='S~';

        String crfAttachmentName = 'test1.txt';
        String str1 = 'TestStringToBlob';
		Blob objBlob = Blob.valueof(str1);
        objMultipleDoc.base64Binary =  EncodingUtil.base64Encode(objBlob);
        objMultipleDoc.category = 'Document';
        objMultipleDoc.entityName = 'Damac Service Requests';
        objMultipleDoc.fileDescription = 'CRF Form';
        objMultipleDoc.fileId = 'IPMS-'+lstCases[0].Booking_Unit__r.Registration_ID__c+'-'+objcls.extractName(crfAttachmentName);
        objMultipleDoc.fileName = 'IPMS-'+lstCases[0].Booking_Unit__r.Registration_ID__c+'-'+objcls.extractName(crfAttachmentName);
        objMultipleDoc.partyId = '2-'+lstCases[0].CaseNumber;
        objMultipleDoc.sourceFileName = 'IPMS-'+lstCases[0].Booking_Unit__r.Registration_ID__c+'-'+objcls.extractName(crfAttachmentName);
        objMultipleDoc.sourceId = 'IPMS-'+lstCases[0].Booking_Unit__r.Registration_ID__c+'-'+objcls.extractName(crfAttachmentName);
        //fileBody = NULL;//if user deletes, and saves the draft 
        lstMultipleDocReq.add(objMultipleDoc);

        test.startTest();
        UploadCOCDMultipleDocController.strLabelValue='N';
        UploadCOCDMultipleDocController.data objData = UploadCOCDMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);
        objData.data = new List<UploadCOCDMultipleDocController.MultipleDocResponse>();
        test.stopTest();

    }

    @isTest static void test_DocumrntUploadOfficeSuccess() {
		UploadCOCDMultipleDocControllerTest objcls = new UploadCOCDMultipleDocControllerTest();
    	// Insert Accont
		Account objAcc = TestDataFactory_CRM.createPersonAccount();
		insert objAcc ;
		 
		//Insert Service Request
		NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
		insert objSR ;

		Id dealId = objSR.Deal_ID__c;
		//Insert Bookings
		List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
		insert lstBookings ; 
		  
		//Insert Booking Units
		List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
		insert lstBookingUnits;
		
		//Insert Cases for the units
		list<Case> lstCases = new list<Case>();
		Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
		for( Booking_Unit__c objUnit : lstBookingUnits ) {
			Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
			objCase.Booking_Unit__c = objUnit.Id;
			lstCases.add( objCase );
		}
		insert lstCases ; 
		
		list<SR_Attachments__c> lstAttachment = new list<SR_Attachments__c>();
		for( Case objCase : lstCases ) {
			lstAttachment.add( TestDataFactory_CRM.createCaseDocument( objCase.Id, PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF ) );
		}
		insert lstAttachment ;



		List<UploadCOCDMultipleDocController.MultipleDocRequest> lstMultipleDocReq = new List<UploadCOCDMultipleDocController.MultipleDocRequest>();
		UploadCOCDMultipleDocController.MultipleDocRequest objMultipleDoc = new UploadCOCDMultipleDocController.MultipleDocRequest();
        
        
        String crfAttachmentName = 'test1.txt';
        String str1 = 'TestStringToBlob';
		Blob objBlob = Blob.valueof(str1);
        objMultipleDoc.base64Binary =  EncodingUtil.base64Encode(objBlob);
        objMultipleDoc.category = 'Document';
        objMultipleDoc.entityName = 'Damac Service Requests';
        objMultipleDoc.fileDescription = 'CRF Form';
        objMultipleDoc.fileId = 'IPMS-'+lstCases[0].Booking_Unit__r.Registration_ID__c+'-'+objcls.extractName(crfAttachmentName);
        objMultipleDoc.fileName = 'IPMS-'+lstCases[0].Booking_Unit__r.Registration_ID__c+'-'+objcls.extractName(crfAttachmentName);
        objMultipleDoc.partyId = '2-'+lstCases[0].CaseNumber;
        objMultipleDoc.sourceFileName = 'IPMS-'+lstCases[0].Booking_Unit__r.Registration_ID__c+'-'+objcls.extractName(crfAttachmentName);
        objMultipleDoc.sourceId = 'IPMS-'+lstCases[0].Booking_Unit__r.Registration_ID__c+'-'+objcls.extractName(crfAttachmentName);
        //fileBody = NULL;//if user deletes, and saves the draft 
        lstMultipleDocReq.add(objMultipleDoc);




		Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
		//Set mock response
		Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
		test.startTest();
        UploadCOCDMultipleDocController.strLabelValue='Y';
        UploadCOCDMultipleDocController.data objData = UploadCOCDMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);
        objData.data = new List<UploadCOCDMultipleDocController.MultipleDocResponse>();
        test.stopTest();
	}


    public String extractName( String strName ) {
      return strName.substring( strName.lastIndexOf('\\')+1 ) ;
    }
}