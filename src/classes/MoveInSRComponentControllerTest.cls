@istest
public without sharing class MoveInSRComponentControllerTest {
  static Account a = new Account();
  static Account a1 = new Account();
  static Booking__c  bk = new  Booking__c();
  static NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
  static Booking_Unit__c bu = new Booking_Unit__c();
  static Booking_Unit__c bu1 = new Booking_Unit__c();
  static Location__c locObj = new Location__c();
  static FM_Case__c fmObj = new FM_Case__c();
  static FM_Case__c fmObj1 = new FM_Case__c();
  static FM_Case__c fmObjRes = new FM_Case__c();
  static FM_Case__c fmObjVeh = new FM_Case__c();
  static FM_Case__c fmObjHousehold = new FM_Case__c();
  static FM_User__c userObj = new FM_User__c();
  static FM_Additional_Detail__c addDetails = new FM_Additional_Detail__c();
  static FM_Additional_Detail__c addDetailsRes = new FM_Additional_Detail__c();
  static FM_Additional_Detail__c addDetailsVeh = new FM_Additional_Detail__c();
  static FM_Additional_Detail__c addDetailsHouseHold = new FM_Additional_Detail__c();
  static SR_Attachments__c srObj = new SR_Attachments__c();
  static Task taskObj = new Task();

  static void SetUp(){

    a.Name = 'Test Account';
    a.party_ID__C = '1039032';
    insert a;

    Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
    sr.NSIBPM__Customer__c = a.id;
    sr.RecordTypeId = RecType;
    insert sr;

    bk.Account__c = a.id;
    bk.Deal_SR__c = sr.id;
    insert bk;

    bu.Booking__c = bk.id;
    bu.Unit_Name__c = 'LSB/10/B1001';
    bu.Owner__c = a.id;
    bu.Property_City__c = 'Dubai';
    insert bu;
    bu1.Booking__c = bk.id;
    bu1.Unit_Name__c = 'LSB/10/B1001';
    bu1.Owner__c = a1.id;
    bu1.Property_City__c = 'Dubai';
    insert bu1;

    locObj.Name  = 'LSB';
    locObj.Location_ID__c = '83488';
    insert locObj;

        userObj.Building__c = locObj.id;
        userObj.FM_Role__c = 'FM Admin';
        insert userObj;

    Id RecordTypeIdFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move In').getRecordTypeId();
    fmObj.Booking_Unit__c = bu.id;
    fmObj.Expected_move_out_date__c = Date.Today();
    fmObj.Move_in_date__c = Date.Today();
    fmObj.Tenant__c = a.id;
    fmObj.Account__c = a.id;
    fmObj.Access_card_required__c = 'Yes';
    fmObj.Actual_move_in_date__c = system.today();
    fmObj.recordtypeid = RecordTypeIdFMCase;
    insert fmObj;

    fmObj1.Booking_Unit__c = bu.id;
    fmObj1.Expected_move_out_date__c = Date.Today();
    fmObj1.Move_in_date__c = Date.Today();
    fmObj1.Tenant__c = a.id;
    fmObj1.Account__c = a.id;
    fmObj1.Access_card_required__c = 'Yes';
    fmObj1.Actual_move_in_date__c = system.today();
    fmObj1.recordtypeid = RecordTypeIdFMCase;
    fmObj1.status__c = 'new';
    insert fmObj1;

    fmObjRes.Booking_Unit__c = bu.id;
    fmObjRes.Expected_move_out_date__c = Date.Today();
    fmObjRes.Move_in_date__c = Date.Today();
    fmObjRes.Tenant__c = a.id;
    fmObjRes.Account__c = a.id;
    fmObjRes.Access_card_required__c = 'Yes';
    fmObjRes.Actual_move_in_date__c = system.today();
    fmObjRes.recordtypeid = RecordTypeIdFMCase;
    insert fmObjRes;

    fmObjVeh.Booking_Unit__c = bu.id;
    fmObjVeh.Expected_move_out_date__c = Date.Today();
    fmObjVeh.Move_in_date__c = Date.Today();
    fmObjVeh.Tenant__c = a.id;
    fmObjVeh.Account__c = a.id;
    fmObjVeh.Access_card_required__c = 'Yes';
    fmObjVeh.Actual_move_in_date__c = system.today();
    fmObjVeh.recordtypeid = RecordTypeIdFMCase;
    insert fmObjVeh;

    fmObjHouseHold.Booking_Unit__c = bu.id;
    fmObjHouseHold.Expected_move_out_date__c = Date.Today();
    fmObjHouseHold.Move_in_date__c = Date.Today();
    fmObjHouseHold.Tenant__c = a.id;
    fmObjHouseHold.Account__c = a.id;
    fmObjHouseHold.Access_card_required__c = 'Yes';
    fmObjHouseHold.Actual_move_in_date__c = system.today();
    fmObjHouseHold.recordtypeid = RecordTypeIdFMCase;
    insert fmObjHouseHold;



        srObj.Name = 'Test';
    srObj.FM_Case__c = fmObj.id;
        insert srObj;

    addDetails.Resident_Account__c = a.id;
    addDetails.Household_Account__c = a.id;
    addDetails.Pet_Account__c = a.id;
    addDetails.Vehicle_Account__c = a.id;
    addDetails.Emergency_Contact_Account__c = a.id;
    addDetails.Emergency_Contact_Case__c = fmObj.id;


    addDetails.Resident_Case__c = fmObjRes.id;
    insert addDetails;

    Id RecordTypeIdaddDetailsRes = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Resident').getRecordTypeId();
    addDetailsRes.RecordTypeId  =  RecordTypeIdaddDetailsRes;
    addDetailsRes.Resident_Account__c = a.id;
    addDetailsRes.Household_Account__c = a.id;
    addDetailsRes.Pet_Account__c = a.id;
    addDetailsRes.Vehicle_Account__c = a.id;
    addDetailsRes.Resident_Case__c = fmObjRes.id;
    insert addDetailsRes;
    Id RecordTypeIdaddDetailsVeh = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Vehicle').getRecordTypeId();
    addDetailsVeh.RecordTypeId  =  RecordTypeIdaddDetailsVeh;
    addDetailsVeh.Resident_Account__c = a.id;
    addDetailsVeh.Household_Account__c = a.id;
    addDetailsVeh.Pet_Account__c = a.id;
    addDetailsVeh.Vehicle_Case__c = fmObjVeh.id;
    addDetailsVeh.Vehicle_Account__c = a.id;
    insert addDetailsVeh;
    Id RecordTypeIdaddDetailsHH = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Household').getRecordTypeId();
    addDetailsHouseHold.RecordTypeId  =  RecordTypeIdaddDetailsHH;
    addDetailsHouseHold.Resident_Account__c = a.id;
    addDetailsHouseHold.Household_Account__c = a.id;
    addDetailsHouseHold.Pet_Account__c = a.id;
    addDetailsHouseHold.Vehicle_Account__c = a.id;
    addDetailsHouseHold.Household_Case__c = fmObjHousehold.id;
    insert addDetailsHouseHold;

    taskObj.Description = 'Test'; //string
    taskObj.Status ='Completed';
    taskObj.Subject = Label.Move_in_date_task;
    taskObj.WhatId = fmObj.id; //record id
    insert taskObj;

  }
  static testMethod void testMethod1() {
        SetUp();
        PageReference myVfPage = Page.MoveInRequestPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',a.id);
        ApexPages.currentPage().getParameters().put('SRType','Move_in_Request');
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        ApexPages.currentPage().getParameters().put('UnitId',String.valueOf(bu.id));
        MoveInSRComponentController moveInObj = new MoveInSRComponentController();
        moveInObj.strDate = String.valueOf('12/12/2018');
        Test.startTest();
            moveInObj.submit();
        Test.stopTest();
    }

    static testMethod void testMethodExistingCases() {
        
        account acc = new account();
        acc.Name = 'Test Account1';
        acc.party_ID__C = '103903';
        insert acc;

        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr1 = new NSIBPM__Service_Request__c();
        sr1.NSIBPM__Customer__c = a.id;
        sr1.RecordTypeId = RecType;
        insert sr1;

        booking__c bk1 = new booking__c();
        bk1.Account__c = acc.id;
        bk1.Deal_SR__c = sr1.id;
        insert bk1;

        booking_unit__c bu1 = new booking_unit__c();
        bu1.Booking__c = bk1.id;
        bu1.Unit_Name__c = 'LSB/10/B1001';
        bu1.Owner__c = acc.id;
        bu1.Property_City__c = 'Dubai';
        insert bu1;

        Id RecordTypeIdFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move In').getRecordTypeId();
        
        fm_case__c fmObj1 = new fm_case__c();
        fmObj1.Booking_Unit__c = bu1.id;
        fmObj1.Expected_move_out_date__c = Date.Today();
        fmObj1.Move_in_date__c = Date.Today();
        fmObj1.Tenant__c = acc.id;
        fmObj1.Account__c = acc.id;
        fmObj1.Access_card_required__c = 'Yes';
        fmObj1.Actual_move_in_date__c = system.today();
        fmObj1.recordtypeid = RecordTypeIdFMCase;
        fmObj1.status__c='New';
        fmObj1.Request_Type__c = 'Move In';
        insert fmObj1;

        fm_case__c fmObj = new fm_case__c();
        fmObj.Booking_Unit__c = bu1.id;
        fmObj.Expected_move_out_date__c = Date.Today();
        fmObj.Move_in_date__c = Date.Today();
        fmObj.Tenant__c = acc.id;
        fmObj.Account__c = acc.id;
        fmObj.Access_card_required__c = 'Yes';
        fmObj.Actual_move_in_date__c = system.today();
        fmObj.recordtypeid = RecordTypeIdFMCase;
        fmObj.Request_Type__c = 'Move In';
        insert fmObj;
        PageReference myVfPage = Page.MoveInRequestPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acc.id);
        ApexPages.currentPage().getParameters().put('SRType','Move_in_Request');
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        ApexPages.currentPage().getParameters().put('UnitId',String.valueOf(bu1.id));
        MoveInSRComponentController moveInObj = new MoveInSRComponentController();
        moveInObj.strDate = String.valueOf('12/12/2018');
        Test.startTest();
            moveInObj.submit();
        Test.stopTest();
    }

    static testMethod void testMethodExistingCases1() {
        
        account acc = new account();
        acc.Name = 'Test Account1';
        acc.party_ID__C = '103903';
        insert acc;

        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr1 = new NSIBPM__Service_Request__c();
        sr1.NSIBPM__Customer__c = a.id;
        sr1.RecordTypeId = RecType;
        insert sr1;

        booking__c bk1 = new booking__c();
        bk1.Account__c = acc.id;
        bk1.Deal_SR__c = sr1.id;
        insert bk1;

        booking_unit__c bu1 = new booking_unit__c();
        bu1.Booking__c = bk1.id;
        bu1.Unit_Name__c = 'LSB/10/B1001';
        bu1.Owner__c = acc.id;
        bu1.Property_City__c = 'Dubai';
        insert bu1;

        Id RecordTypeIdFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move In').getRecordTypeId();
        
        fm_case__c fmObj1 = new fm_case__c();
        fmObj1.Booking_Unit__c = bu1.id;
        fmObj1.Expected_move_out_date__c = Date.Today();
        fmObj1.Move_in_date__c = Date.Today();
        fmObj1.Tenant__c = acc.id;
        fmObj1.Account__c = acc.id;
        fmObj1.Access_card_required__c = 'Yes';
        fmObj1.Actual_move_in_date__c = system.today();
        fmObj1.recordtypeid = RecordTypeIdFMCase;
        fmObj1.status__c='New';
        fmObj1.Request_Type__c = 'Move In';
        insert fmObj1;

        fm_case__c fmObj = new fm_case__c();
        fmObj.Booking_Unit__c = bu1.id;
        fmObj.Expected_move_out_date__c = Date.Today();
        fmObj.Move_in_date__c = Date.Today();
        fmObj.Tenant__c = acc.id;
        fmObj.Account__c = acc.id;
        fmObj.Access_card_required__c = 'Yes';
        fmObj.Actual_move_in_date__c = system.today();
        fmObj.recordtypeid = RecordTypeIdFMCase;
        fmObj.Request_Type__c = 'Move In';
        insert fmObj;
        PageReference myVfPage = Page.MoveInRequestPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acc.id);
        ApexPages.currentPage().getParameters().put('SRType','Move_in_Request');
        //ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        ApexPages.currentPage().getParameters().put('UnitId',String.valueOf(bu1.id));
        MoveInSRComponentController moveInObj = new MoveInSRComponentController();
        moveInObj.strDate = String.valueOf('12/12/2018');
        Test.startTest();
            moveInObj.submit();
        Test.stopTest();
    }


  static testMethod void testMethodSaveAsDraft() {
        SetUp();
        PageReference myVfPage = Page.MoveInRequestPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        MoveInSRComponentController moveInObj = new MoveInSRComponentController();
        Id RecordTypeIdaddDetails = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
        addDetails.RecordTypeId  =  RecordTypeIdaddDetails;
        Test.startTest();
            moveInObj.insertCase();
            moveInObj.saveAsDraft();
        Test.stopTest();
    }
    static testMethod void testMethodSaveAsDraftRes() {
        SetUp();
        PageReference myVfPage = Page.MoveInRequestPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObjRes.id));
        MoveInSRComponentController moveInObj = new MoveInSRComponentController();
        Test.startTest();
            moveInObj.insertCase();
            moveInObj.saveAsDraft();
        Test.stopTest();
    }
    static testMethod void testMethodSaveAsDraftVeh() {
        SetUp();
        PageReference myVfPage = Page.MoveInRequestPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObjVeh.id));
        MoveInSRComponentController moveInObj = new MoveInSRComponentController();
        Test.startTest();
            moveInObj.insertCase();
            moveInObj.saveAsDraft();
        Test.stopTest();
    }
    static testMethod void testMethodSaveAsDraftHouseHold() {
        SetUp();
        PageReference myVfPage = Page.MoveInRequestPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObjHouseHold.id));
        MoveInSRComponentController moveInObj = new MoveInSRComponentController();
        Test.startTest();
            moveInObj.insertCase();
            moveInObj.saveAsDraft();
        Test.stopTest();
    }
  static testMethod void testMethodaddDetailsRes() {
        SetUp();
        PageReference myVfPage = Page.MoveInRequestPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        MoveInSRComponentController moveInObj = new MoveInSRComponentController();
        moveInObj.detailType = 'Resident';
        Test.startTest();
            moveInObj.addDetails();
            moveInObj.indexVar = 0;
            moveInObj.removeDetails();
        Test.stopTest();
    }
  static testMethod void testMethodaddDetailshouseHold() {
        SetUp();
        PageReference myVfPage = Page.MoveInRequestPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        MoveInSRComponentController moveInObj = new MoveInSRComponentController();
        moveInObj.detailType = 'Household';
        Test.startTest();
            moveInObj.addDetails();
            moveInObj.indexVar = 0;
            moveInObj.removeDetails();
        Test.stopTest();
    }
  static testMethod void testMethodaddDetailsVehicle() {
        SetUp();
        PageReference myVfPage = Page.MoveInRequestPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        MoveInSRComponentController moveInObj = new MoveInSRComponentController();
        moveInObj.detailType = 'Vehicle';
        Test.startTest();
            moveInObj.addDetails();
            moveInObj.indexVar = 0;
            moveInObj.removeDetails();
            moveInObj.addVehicleDetails();
            moveInObj.addResidentialDetails();
        Test.stopTest();
    }
  static testMethod void testMethodaddDetailsEC() {
        SetUp();
        PageReference myVfPage = Page.MoveInRequestPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        MoveInSRComponentController moveInObj = new MoveInSRComponentController();
        moveInObj.detailType = 'Emergency Contacts';
        Test.startTest();
            moveInObj.addDetails();
            moveInObj.indexVar = 0;
            moveInObj.removeDetails();
        Test.stopTest();
    }

  static testMethod void testMethoddeleteDoc() {
        SetUp();
        PageReference myVfPage = Page.MoveInRequestPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        MoveInSRComponentController moveInObj = new MoveInSRComponentController();
        Test.startTest();
            moveInObj.docIdToDelete = srObj.id;
            moveInObj.deleteDocument();
        Test.stopTest();
    }
  static testMethod void testMethodUploadAndDeleteDocument(){
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
         User u = [SELECT Id FROM User WHERE Profile.name = 'System Administrator' AND isActive = True limit 1];
          System.runAs(u){
            SetUp();
            String fileName = 'VfRemotingJs,TestType';
            PageReference myVfPage = Page.MoveInRequestPage;
            Test.setCurrentPage(myVfPage);
            ApexPages.currentPage().getParameters().put('AccountId',a.id);
            ApexPages.currentPage().getParameters().put('SRType','Move_in_Request');
            ApexPages.currentPage().getParameters().put('UnitId',String.valueOf(bu.id));
            ApexPages.currentPage().getParameters().put('fileName',fileName);
          }
        List<Task> lstTasks = new List<Task>();
        lstTasks.add(taskObj);
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        MoveInSRComponentController moveInObj = new MoveInSRComponentController();
        moveInObj.strDocBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        moveInObj.strDocName = 'VfRemotingJs';
        Test.startTest();
          moveInObj.uploadDocument();
          MoveInSRComponentController.closeMoveInFMCase(lstTasks);
        Test.stopTest();
  }
}