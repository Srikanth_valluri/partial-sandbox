@isTest
public class BookingUnitTriggerHandlerHOTest{
    static List<Booking_Unit_Active_Status__c> lstActives;
    static list<RuleEngineDocuments__c> lstRuleDocs;
    static Account objAcc;
    static NSIBPM__Service_Request__c objSR;
    static List<Booking__c> lstBookings;
    static Property__c objProp;
    static Inventory__c objInv;
    static List<Booking_Unit__c> lstBookingUnits;
    static List<Option__c> lstOptions;
    static list<Buyer__c> lstJB;
    static Payment_Plan__c objPP;
    
    static void init() {
        lstActives = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActives;
        
        lstRuleDocs = TestDataFactory_CRM.createRuleDocs();
        insert lstRuleDocs;
        
        // Insert Account        
        objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        system.debug('******Acc Id*************'+objAcc.Id);
        
        //Insert Service Request
        objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        system.debug('******SR Id*************'+objSR.Id);
        
        //Insert Bookings
        lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings ;
        system.debug('******bookings Id*************'+lstBookings);
        
        objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        insert lstBookingUnits;
        system.debug('******Unit Id*************'+lstBookingUnits);
    }
    
    private static testMethod void test1(){
        init();
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].Handover_Date__c = date.today();
        lstBookingUnits[0].Handover_Notice_Sent_Date__c = date.today();
        update lstBookingUnits;
        
    }
    
    private static testMethod void test2(){
        init();
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].Handover_Date__c = date.today();
        lstBookingUnits[0].Early_Handover__c = true;
        lstBookingUnits[0].Lease_Commencement_Date__c = '19/11/2015';
        update lstBookingUnits;
        
    }
    
    private static testMethod void test3(){
        init();
        Milestones_For_Handover__c objMS = new Milestones_For_Handover__c();
        objMS.Name = 'Latest';
        insert objMS;
        
        objPP = new Payment_Plan__c();
        objPP.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objPP;
        
        Payment_Terms__c objPT = new Payment_Terms__c();
        objPT.Installment__c = '1';
        objPT.Payment_Plan__c = objPP.Id;
        objPT.Payment_Date__c = date.today();
        objPT.Milestone_Event__c = 'New';
        insert objPT;
        
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].Handover_Date__c = date.today();
        lstBookingUnits[0].Early_Handover__c = true;
        lstBookingUnits[0].Early_Handover_Date__c = date.today();
        lstBookingUnits[0].Lease_Commencement_Date__c = null;
        update lstBookingUnits;
    }
    private static testMethod void test4(){
        init();
        lstBookingUnits[0].Handover_Flag__c = 'N';
        lstBookingUnits[0].Handover_Date__c = NULL;
        lstBookingUnits[0].Early_Handover__c = false;
        lstBookingUnits[0].Early_Handover_Date__c = NULL;
        lstBookingUnits[0].Lease_Handover__c = true;
        lstBookingUnits[0].Lease_Handover_Date__c = date.today();
        
        update lstBookingUnits;
        
    }
    
    private static testMethod void test5(){
        init();
        lstBookingUnits[0].Handover_Flag__c = 'N';
        lstBookingUnits[0].Handover_Date__c = NULL;
        lstBookingUnits[0].Early_Handover__c = true;
        lstBookingUnits[0].Early_Handover_Date__c = system.today();
        lstBookingUnits[0].Lease_Commencement_Date__c = '19/11/2015';
        lstBookingUnits[0].Lease_Handover__c = false;
        lstBookingUnits[0].Lease_Handover_Date__c = NULL;
        
        update lstBookingUnits;
        
    }
    
    private static testMethod void test6(){
        init();
        lstBookingUnits[0].Handover_Flag__c = 'N';
        lstBookingUnits[0].Handover_Date__c = NULL;
        lstBookingUnits[0].Early_Handover__c = true;
        lstBookingUnits[0].Early_Handover_Date__c = system.today();
        lstBookingUnits[0].Lease_Handover__c = true;
        lstBookingUnits[0].Lease_Handover_Date__c = date.today();
        
        update lstBookingUnits;
        
    }
}