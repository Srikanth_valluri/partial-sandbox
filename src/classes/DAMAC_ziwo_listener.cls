/*
Name:DAMAC_ziwo_listener
Desc: To listen the Ziwo call details for the both inbound and outbound
*/

global with sharing class DAMAC_ziwo_listener{
    global DAMAC_ziwo_listener(){
       
    }
    global User getUserDetails () {
        return [SELECT Ziwo_DID__c, Ziwo_Extn__c FROM User WHERE ID =: UserInfo.getUserId() LIMIT 1];
    }
    
    global Map <String, Ziwo_Object_Settings__mdt> getMappings () {
        Map <String, Ziwo_Object_Settings__mdt> mappings = new Map<String, Ziwo_Object_Settings__mdt> ();
        for (Ziwo_Object_Settings__mdt obj : [SELECT Object_API__c,  Label FROM Ziwo_Object_Settings__mdt WHERE Object_API__c != NULL])
        {
        
            mappings.put (obj.Object_API__c, obj);
        }
        return mappings;
    }
    
    
    // To display the ziwo configured objects on the home page component
    global List <SelectOption> getObjects  () {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        for (Ziwo_Object_Settings__mdt obj : [SELECT Label, Object_API__c FROM Ziwo_Object_Settings__mdt WHERE Phone_Field_API__c != null]) {
            
            options.add(new SelectOption(obj.Object_API__c, obj.Label));        
        }
        return options;
    }
    
    // To display the selected object related configured phone fields on the home page component
    @remoteAction
    global Static Map <String, String> getFieldApis (String objectName) {
        System.debug (objectName);
        Map <String, String> resultMap = new Map <String, String> ();
        for (Ziwo_Object_Settings__mdt obj : [SELECT Label, Default_Phone_Field_API__c, Phone_Field_API__c 
                                                    FROM Ziwo_Object_Settings__mdt 
                                                    WHERE Object_API__c =: objectName AND Phone_Field_API__c != NULL]) {
            String objAPI = obj.Phone_Field_API__c;
            List <String> fieldAPIs = new List <String> ();
            if (objAPI != null) {
                
                if (objAPI.contains(',')) {
                    for (String key : objAPI.split (',')) {
                        fieldAPIS.add (key);
                    }
                }
                else {
                    fieldApis.add (objAPI);
                }
            }
            
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType objSchema = schemaMap.get(objectName);
            Map<String, Schema.SObjectField> fieldMap = objSchema.getDescribe().fields.getMap();
            for (String fieldName: fieldApis) {
                resultMap.put (fieldName, fieldMap.get(fieldName).getDescribe().getLabel());
            }
        }
        return resultMap;
    }
    
    
    // To display the search results to to initiate the call
    @RemoteAction 
    global static Map <ID, String> searchName (String searchKey, String sObjName) {
        Ziwo_Object_Settings__mdt objectMapping = new Ziwo_Object_Settings__mdt ();
        objectMapping = [SELECT Name_Field_API__c, Phone_Field_API__c FROM Ziwo_Object_Settings__mdt WHERE Object_API__c =: sObjName LIMIT 1];
        
        String key = '%'+searchKey+'%';
        System.debug (key);
        Map <ID, String> resultMap = new Map <ID, String> ();
        for (Sobject obj : Database.query ('SELECT '+objectMapping.Name_Field_API__c
                                +' from '+sObjName+' WHERE '+objectMapping.Name_Field_API__c+' LIKE : key   LIMIT 10')) 
        {
            resultMap.put (obj.Id, ''+obj.get (objectMapping.Name_Field_API__c));
        }
        return resultMap;
    }
    
    
    // To call the customer from home page component
    @remoteAction
    webservice static String dialCustomer (ID objId, String phoneAPI) {
        return DAMAC_Ziwo_Click_to_Call.callZiwo (objId, phoneAPI);
    }
    
    // To get the task id based on callid parameter to show the wrapup screen on home page
    @remoteAction
    global static String getTaskId (string callId, String did) {
        try {
            Task t = new Task ();
            t = [SELECT ID FROM Task WHERE CrtObjectId__c =: callId];
            
            User u = [SELECT Ziwo_DID__c FROM User WHERE ID =: UserInfo.getUserId() ];
            if (u.Ziwo_DID__c == did) {
                return t.Id;
            }
            else
                return 'Error';
        } catch (Exception e) {
            System.debug (e.getMessage ());
            return '';
        }
    }

    

    

}