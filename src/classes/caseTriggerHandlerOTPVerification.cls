/**
 * v1.1     30-09-2020      Aishwarya Todkar    Added On/Off Settings
 */
public without sharing class caseTriggerHandlerOTPVerification {
    public static void afterUpdate(map<Id,Case> newMap, map<Id,Case> oldMap){
        Apex_Class_ON_OFF_Setting__c onOffCs = Apex_Class_ON_OFF_Setting__c.getInstance( 'SendOtpFromTrigger');
        if( onOffCs == null || ( onOffCs != null && onOffCs.ON_OFF__c ) ) {
            set<Id> setBuyerIds = new set<Id>();
            for(Case objC : newMap.values()){
                system.debug('objC.Buyer__c**********'+objC.Buyer__c);
                system.debug('objC.Status**********'+objC.Status);
                system.debug('oldMap.get(objC.Id).Status**********'+oldMap.get(objC.Id).Status);
                if(objC.Buyer__c != null
                && objC.Status.equalsIgnoreCase('Buyer OTP Verification')
                && oldMap.get(objC.Id).Status != objC.Status){
                    setBuyerIds.add(objC.Buyer__c);
                }
            }
            system.debug('setBuyerIds********'+setBuyerIds);
            if(!setBuyerIds.isEmpty()){
                map<Id,Buyer__c> mapId_Buyer = new map<Id,Buyer__c>([Select b.Phone__c
                                                                        , b.Phone_Country_Code__c
                                                                        , b.OTP__c
                                                                        , b.Buyer_ID__c
                                                                        , b.Id
                                                                        , b.Phone_with_Country_Code__c
                                                                        From Buyer__c b
                                                                        where b.Id IN : setBuyerIds]);
                if(!mapId_Buyer.isEmpty()){
                    Integer intCount = 1;
                    list<String> setPhoneNos = new list<String>();
                    for(Case objC : newMap.values()){
                        if(objC.Buyer__c != null
                        && mapId_Buyer.containsKey(objC.Buyer__c)
                        && mapId_Buyer.get(objC.Buyer__c) != null
                        && mapId_Buyer.get(objC.Buyer__c).Phone_with_Country_Code__c != null
                        && !String.isBlank(mapId_Buyer.get(objC.Buyer__c).Phone_with_Country_Code__c)){
                            Configuration_Control__c mc = Configuration_Control__c.getOrgDefaults();
                            String conCatValue = objC.Id+':';
                            system.debug('*****mc*****'+mc);
                            if(mc.OTP_Test_Service_Enabled__c && !String.isBlank(mc.OTP_Test_Service_Number__c)){
                                conCatValue = conCatValue + mc.OTP_Test_Service_Number__c+','+mapId_Buyer.get(objC.Buyer__c).Phone_with_Country_Code__c;
                            }else{
                                conCatValue = conCatValue + mapId_Buyer.get(objC.Buyer__c).Phone_with_Country_Code__c;
                            }
                                
                            if(objC.Buyer_POA_Country_Code__c != null
                            && objC.Buyer_POA_Phone__c != null){
                                String st = objC.Buyer_POA_Country_Code__c.substringAfter(':').trim();
                                st = st.substringAfter('00');
                                st = st + objC.Buyer_POA_Phone__c;
                                conCatValue = conCatValue +','+st;
                            }
                            if(objC.Seller_POA_Country_Code__c != null
                            && objC.Seller_POA_Phone__c != null){
                                String st = objC.Seller_POA_Country_Code__c.substringAfter(':').trim();
                                st = st.substringAfter('00');
                                st = st + objC.Seller_POA_Phone__c;
                                conCatValue = conCatValue +','+st;
                            }
                            system.debug('conCatValue*****'+conCatValue);
                            OTPService.Sendtextmessage(conCatValue);
                                //setPhoneNos.add(conCatValue);
                        }
                    }
                }
            }
        }
    } // end of afterUpdate
} // end of class