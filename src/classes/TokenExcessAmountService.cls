/*
 * Description - Class used for fetching excess and token amount for active and rejected units
 *
 * Version        Date            Author            Description
 * 1.0            08/04/18        Vivek Shinde      Initial Draft
 */
public with sharing class TokenExcessAmountService {
    
    //Method to perform callout to fetch token amount
    public static RefundResponse getTokenRefund(String strRegId) {
        Refunds.CustomerFundAndTokenHttpSoap11Endpoint objReq = new Refunds.CustomerFundAndTokenHttpSoap11Endpoint();
        objReq.timeout_x = 120000;
        
        try {
            System.debug('--strRegId---'+strRegId);
            List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
            String strResponse = objReq.getTokenRefunds('2-'+strRegId, 'TOKEN_REF_AMOUNTS', 'SFDC', strRegId);
            System.debug('--strResponse ---'+strResponse );
            Map<String, Object> mapResponse = (Map<String, Object>)JSON.deserializeUntyped(strResponse);
            if(String.isNotBlank(strResponse)) {
                
                if(mapResponse.get('Status') != null) {
                    String strStatus = (String)mapResponse.get('Status');
                    if(String.isNotBlank(strStatus) && strStatus.equalsIgnoreCase('S')) { //&&
                       /*((mapResponse.get('Amount_Paid') != null && mapResponse.get('Amount_Paid') != 'null') ||
                       (mapResponse.get('Amount_COCA') != null && mapResponse.get('Amount_COCA') != 'null'))) {*/
                        System.debug('--mapResponse Amount_Paid--'+mapResponse.get('Amount_Paid'));
                        System.debug('--mapResponse Amount_COCA--'+mapResponse.get('Amount_COCA'));
                        //Decimal.valueOf(String.valueOf(mapResponse.get('Excess_Amount')));
                        Decimal decCOCAAmount = 0; 
                        Decimal decPaidAmount = 0;
                        decCOCAAmount = mapResponse.get('Amount_COCA') != null ? Decimal.valueOf((String)mapResponse.get('Amount_COCA')) : 0;
                        decPaidAmount = mapResponse.get('Amount_Paid') != null ? Decimal.valueOf((String)mapResponse.get('Amount_Paid')) : 0;
                        System.debug('--decCOCAAmount --'+decCOCAAmount );
                        System.debug('--decPaidAmount --'+decPaidAmount );
                        return new RefundResponse('Success', decCOCAAmount, decPaidAmount, null);
                    }
                    else {
                        String strMessage = mapResponse.get('Message') != null ? (String)mapResponse.get('Message') : null;
                        /*if(mapResponse.get('Message') != null) {
                            lstErrorLog.add(new Error_Log__c(Error_Details__c = (String)mapResponse.get('Message')));
                            insert lstErrorLog;
                        }*/
                        return new RefundResponse('Error', null, null, strMessage);
                    }
                }
            }
            else {
                return new RefundResponse('Error', null, null, null);
            }
        }
        catch(Exception excGen) {
            return new RefundResponse('Error', null, null, excGen.getStackTraceString());
        }
        return null;
    }
    
    public Class RefundResponse {
        public String strStatus;
        public Decimal decCOCAAmount;
        public Decimal decPaidAmount;
        public String strMessage;
        
        public RefundResponse(String strStatus, Decimal decCOCAAmount, Decimal decPaidAmount, String strMessage) {
            this.strStatus = strStatus;
            this.decCOCAAmount = decCOCAAmount;
            this.decPaidAmount = decPaidAmount;
            this.strMessage = strMessage;
        }
    }
}