public class AramexService {
    public Static Blob getAirWayBillBody(String resUrl){
        Blob pdfBlob;
        List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
        
        if (resUrl != '' && resUrl != NULL ){
            HttpRequest req = new HttpRequest();
            req.setEndpoint(resUrl);
            req.setMethod('GET');
            req.setTimeout(120000);
            HttpResponse response = new Http().send(req);
            if(response.getbody()!= null && response.getStatusCode() == 200){
                pdfBlob = response.getBodyAsBlob();
            }else{
                //Error_Log__c objError = GenericUtility.createErrorLog(response.getBody(), objCourier.Account__c, objCourier.UnitName__c, '', objCourier.Case__c);
                //objError.Service_Request_Body__c = rtnBody;
                //objError.Service_Response_Body__c = resp.getBody();
                //lstErrorLog.add(objError);
             }
            
            System.debug('response'+response);
            System.debug('pdfBlob'+pdfBlob);
        }
        if(!lstErrorLog.isEmpty()){
            Insert lstErrorLog;
        }
        return pdfBlob;
    }
    
    public static List<Courier_Delivery__c> createShipping(String courierService,List<Courier_Delivery__c> lstCourier){
       System.debug('courierService>>>>>'+courierService);
       Credentials_Details__c Cre;
       if(courierService == 'Aramex') {
          Cre = Credentials_Details__c.getValues('Aramex Shipping');
       }
       else if (courierService == 'Aramex CDS') {
           Cre = Credentials_Details__c.getValues('Aramex Shipping CDS');
       }
        
        String strEndpoint = Cre.Endpoint__c;
        List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
        List<Id> caseIds = new List<Id>();
        List<Id> lstAccIds = new List<Id>();
        List<Case> cases = new List<Case>();
        List<Id> buIds = new List<Id>();
        List<Booking_Unit__c> BUs = new List<Booking_Unit__c>();
        for(Courier_Delivery__c objCourier: lstCourier){
            caseIds.add(objCourier.Case__c);
            buIds.add(objCourier.UnitName__c);
            lstAccIds.add(objCourier.Account__c);
        }
        
        Map<Id, Case> mapCase = new Map<Id, Case>([SELECT Id, Courier_Request_Already_Created__c FROM Case WHERE Id IN: caseIds]);
        Map<Id, Booking_Unit__c> mapBookingUnit = new Map<Id, Booking_Unit__c>([SELECT Id,Courier_Request_Already_Created__c,Unit_Name__c FROM Booking_Unit__c WHERE Id IN: buIds]);
        Map<Id, Account> mapAccount = new Map<Id, Account>([SELECT Id, Name, P_O_Box_Zip_Postal_Code__c, Mobile_Person_Business__c, Person_Business_Email__c, Party_ID__c FROM Account WHERE Id IN: lstAccIds]);
        
        for(Courier_Delivery__c objCourier: lstCourier){
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            AramexJasonResponce  objRes;
            req.setEndpoint(strEndpoint);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Accept', 'application/json');
            String rtnBody = GenerateBody(objCourier, mapAccount,mapBookingUnit, Cre);
            system.debug('rtnBody>>>>>'+rtnBody);
            req.setBody(rtnBody);
            req.setTimeout(120000);
            HttpResponse res = h.send(req);
            system.debug('bofy>>>>>'+res.getStatusCode());
            if(res.getbody()!= null && res.getStatusCode() == 200){
                objRes = AramexJasonResponce.parse(res.getbody());
                system.debug('res.getbody()>>>>>'+res.getbody());
                system.debug('objRes>>>>>'+objRes);
                system.debug('ID>>>>>'+objRes.Shipments);
                
                    if(objRes.HasErrors){
                       objCourier.Courier_Status__c = 'Error';
                       objCourier.Error_Description__c = res.getbody();
                    }else{
                        objCourier.Request_Number__c = objRes.Shipments[0].ID;
                        objCourier.Airway_Bill__c = objRes.Shipments[0].ID;
                        objCourier.Airway_PDF__c = objRes.Shipments[0].ShipmentLabel.LabelURL;
                        objCourier.Courier_Status__c = 'Success'; 
                        if(objCourier.Case__c != null && mapCase.containsKey(objCourier.Case__c)){
                            Case caseInst = mapCase.get(objCourier.Case__c);
                            caseInst.Courier_Request_Already_Created__c = true;
                            //caseInst.Airway_Bill_Number__c = objCourier.Airway_Bill__c;
                            cases.add(caseInst);
                        }
                        if(objCourier.UnitName__c != null && mapBookingUnit.containsKey(objCourier.UnitName__c)){
                            Booking_Unit__c buInst = mapBookingUnit.get(objCourier.UnitName__c);
                            buInst.Courier_Request_Already_Created__c = true;
                            BUs.add(buInst);
                        }
                    }
            }else{
                    Error_Log__c objError = GenericUtility.createErrorLog(res.getBody(), objCourier.Account__c, objCourier.UnitName__c, '', objCourier.Case__c);
                    objError.Service_Request_Body__c = rtnBody;
                    objError.Service_Response_Body__c = res.getBody();
                    lstErrorLog.add(objError);
                }
        }
        
        if(!lstErrorLog.isEmpty()){
            Insert lstErrorLog;
        }
        if(!cases.isEmpty()){
            Update cases;
        }
        if(!BUs.isEmpty()){
            Update BUs;
        }
        return lstCourier;
    }
    
     public static string  GenerateBody(Courier_Delivery__c couriList, Map<Id, Account> mapAccount, Map<Id, Booking_Unit__c> mapBookingUnit, Credentials_Details__c Cre){
         List<Courier_Sender_Details__mdt> lstSenderDetails = [Select Address__c
                                                                    , Email_Address__c
                                                                    , Person_Name__c
                                                                    , Phone__c
                                                                    , PostCode__c
                                                                    , City__c
                                                                 From Courier_Sender_Details__mdt
                                                                Where MasterLabel = 'Sender Details'];
         Map<String, Country_City_Code_Mapping__mdt> mapCountryCode = getCountryCodeMap();
         String AccountNumber = Cre.Resource__c;   //'45796';
         String AccountPin = Cre.grant_type__c;   //'116216';
         String Username = Cre.User_Name__c;     //'testingapi@aramex.com';
         String Password = Cre.Password__c;     //'R123456789$r';
         
         String strSenderName = lstSenderDetails != null && lstSenderDetails.size() > 0 ? lstSenderDetails[0].Person_Name__c : '';
         String strPhone = lstSenderDetails != null && lstSenderDetails.size() > 0 ? lstSenderDetails[0].Phone__c : '';
         String strAddress = lstSenderDetails != null && lstSenderDetails.size() > 0 ? lstSenderDetails[0].Address__c : '';
         String strEmail = lstSenderDetails != null && lstSenderDetails.size() > 0 ? lstSenderDetails[0].Email_Address__c : '';
         String strPostCode = lstSenderDetails != null && lstSenderDetails.size() > 0 ? lstSenderDetails[0].PostCode__c : '';
         String strCity = lstSenderDetails != null && lstSenderDetails.size() > 0 ? lstSenderDetails[0].City__c : '';
         String strCountryCode= 'AE';
         Datetime shippingDate = System.now();
         Long dtShipping = shippingDate.getTime();
         
         Datetime dueDate1 = System.now();
         Long dtDue = dueDate1.getTime();
         
         //string Reference1=(mapBookingUnit != null && mapBookingUnit.containsKey(couriList.UnitName__c) != null)?mapBookingUnit.get(couriList.UnitName__c).Unit_Name__c:'';
         //string Reference1= String.isNotBlank(couriList.Unit_Names__c)?String.valueOf(couriList.Unit_Names__c):'';
         string Reference1= String.isNotBlank(couriList.Type_of_Document__c)?String.valueOf(couriList.Type_of_Document__c):'';
         //string Reference1=null;
         string Reference2=null;
         string Reference3=null;
         
         string Line1 = couriList.Address_1__c != null ? couriList.Address_1__c : '';
         string Line2 = couriList.Address_2__c != null ? couriList.Address_2__c : '';
         string Line3 = couriList.Address_3__c != null ? couriList.Address_3__c : '';
         string City = couriList.City__c != null ? couriList.City__c : '';
         string StateOrProvinceCode = couriList.City__c != null ? couriList.City__c : '';
         string PostCode = couriList.Postal_Code__c != null
                         ? couriList.Postal_Code__c 
                         : mapAccount.get(couriList.Account__c).P_O_Box_Zip_Postal_Code__c != null 
                         ? mapAccount.get(couriList.Account__c).P_O_Box_Zip_Postal_Code__c : '';
         //System.debug('couriList.Country__c>>>>>'+couriList.Country__c);
         //System.debug('mapCountryCode>>>>'+mapCountryCode);
         //System.debug('mapCountryCode.containsKey(couriList.Country__c)'+mapCountryCode.containsKey(couriList.Country__c.toLowerCase()));
         //System.debug('mapCountryCodeCityCode>>>>'+mapCountryCode.get(couriList.Country__c.toLowerCase()).City_Code__c);

         string CountryCode = String.isNotEmpty(couriList.Country__c) && mapCountryCode.containsKey(couriList.Country__c.toLowerCase())
                            ? mapCountryCode.get(couriList.Country__c.toLowerCase()).City_Code__c : '';
         String personName = couriList.Consignee_Name__c != null
                            ? couriList.Consignee_Name__c : '';
         String companyName = couriList.Company_Name__c != null
                            ? couriList.Company_Name__c : '';
         String phoneNumber1 = couriList.Phone_1__c != null
                             ? couriList.Phone_1__c : '';
         String phoneNumber2 = couriList.Phone_2__c != null
                             ? couriList.Phone_2__c : '';
         String emailAddress = mapAccount.get(couriList.Account__c).Person_Business_Email__c != null
                             ? mapAccount.get(couriList.Account__c).Person_Business_Email__c : '';
         integer Longitude=0;
         integer Latitude=0;
         integer BuildingNumber=null;
         string BuildingName=null;
         string Apartment=null;
         string Floor=null;
         //string Description=String.isNotBlank(couriList.Unit_Names__c)&& String.isNotBlank(couriList.Type_of_Document__c)?couriList.Unit_Names__c+'-'+couriList.Type_of_Document__c:'';
         //string Description=(mapBookingUnit != null && mapBookingUnit.containsKey(couriList.UnitName__c) != null && String.isNotBlank(couriList.Type_of_Document__c))?mapBookingUnit.get(couriList.UnitName__c).Unit_Name__c+'-'+couriList.Type_of_Document__c:'';
         string Description=null;
         string Title =null;
         string Department ='';
         string FaxNumber =null;
         string Type =null;
         string ThirdParty=null ;
         string ShippingDateTime = '/Date('+String.valueOf(dtShipping)+')/';               //EX:'/Date(1579758034000)/'; 
         string DueDate= '/Date('+String.valueOf(dtDue)+')/';                             //EX: '/Date(1579758034000)/' ; 
         string Comments = couriList.Remarks__c != null ?  couriList.Remarks__c : 'Confidencial Documents';
         string PickupLocation =null;
         string OperationsInstructions=null;
         string AccountingInstrcutions=null;
         string Dimensions=null ;
         string Unit ='KG';
         Decimal  Value = 0.5;
         string ChargeableWeight= null ;
         string DescriptionOfGoods= String.isNotBlank(couriList.Unit_Names__c)?couriList.Unit_Names__c:'';
         string GoodsOriginCountry='AE';
         string PaymentType='P' ;
         string PaymentOptions= null;
         string CustomsValueAmount= null;
         string CashOnDeliveryAmount=null ;
         string InsuranceAmount=null ;
         string CashAdditionalAmount=null ;
         string CashAdditionalAmountDescription=null ;
         string CollectAmount=null ;
         string Services=null ;
         string Items=null ;
         string ProductGroup = CountryCode != '' && CountryCode != 'AE'
                             ? 'EXP' : 'DOM';
         string ProductType = CountryCode != '' && CountryCode != 'AE'
                            ? 'PDX' : 'CDS';
         string PackageType ='Box';
         integer  NumberOfPieces = couriList.Number_of_Pieces__c != null
                                 ? Integer.valueOf(couriList.Number_of_Pieces__c) : 0 ;
         string DeliveryInstructions=null ;
         string Attachments=null ;
         string ForeignHAWB=String.valueOf(System.now().getTime());
         //string ForeignHAWB=null;
         integer TransportType=0;
         string PickupGUID=null ;
         string Numbers=null;
         string ScheduledDelivery=null ;
         integer ReportID =9729;
         string ReportType ='URL';
         integer  Source =24;
         string AccountEntity='DXB';
         string AccountCountryCode='AE';
         string Version='V1';
         string requestBody;
         string PhoneNumber1Ext=null;
         
            if(couriList!=null){
            requestBody='{'+
                '"Shipments": ['+
                    '{'+
                        '"Reference1":"'+Reference1+'",'+
                        '"Reference2":'+ Reference2+','+
                        '"Reference3":'+ Reference3+','+
                        '"Shipper": {'+
                            '"Reference1":"'+string.valueof(couriList.Account__c)+'",'+
                            '"Reference2": "'+string.valueof(couriList.Account__c)+'",'+
                            '"AccountNumber": "'+AccountNumber+'",'+
                            '"PartyAddress": {'+
                                '"Line1": "'+strAddress+'",'+
                                '"Line2": "",'+
                                '"Line3": "",'+
                                '"City": "'+strCity+'",'+
                                '"StateOrProvinceCode": "'+strCity+'",'+
                                '"PostCode": "'+strPostCode+'",'+
                                '"CountryCode": "'+strCountryCode+'",'+
                                '"Longitude": '+Longitude+','+
                                '"Latitude": '+Latitude+','+
                                '"BuildingNumber": '+BuildingNumber+','+
                                '"BuildingName": '+BuildingName+','+
                                '"Floor": '+Floor+','+
                                '"Apartment":'+Apartment+','+
                                '"POBox": '+couriList.PO_Box__c+','+
                                '"Description": '+Description+
                            '},'+
                            '"Contact": {'+
                                '"Department": "'+Department+'",'+
                                '"PersonName": "'+strSenderName+'",'+
                                '"Title":'+Title+','+
                                '"CompanyName": "'+'DAMAC'+'",'+
                                '"PhoneNumber1": "'+strPhone+'",'+
                                '"PhoneNumber1Ext": '+PhoneNumber1Ext+','+
                                '"PhoneNumber2": '+PhoneNumber1Ext+','+
                                '"PhoneNumber2Ext":'+PhoneNumber1Ext+','+
                                '"FaxNumber": '+FaxNumber+','+
                                '"CellPhone": "'+strPhone+'",'+
                                '"EmailAddress": "'+strEmail+'",'+
                                '"Type":'+Type+
                            '}'+
                        '},'+
                        '"Consignee": {'+
                            '"Reference1":"' +couriList.Account__c+'",'+
                            '"Reference2":"'+couriList.Account__c+system.now().getTime()+'",'+
                            '"AccountNumber": "'+AccountNumber+'",'+
                            '"PartyAddress": {'+
                                '"Line1": "'+Line1+'",'+
                                '"Line2": "'+Line2+'",'+
                                '"Line3": "'+Line3+'",'+
                                '"City": "'+City+'",'+
                                '"StateOrProvinceCode": "'+StateOrProvinceCode+'",'+
                                '"PostCode": "'+PostCode+'",'+
                                '"CountryCode": "'+CountryCode+'",'+
                                '"Longitude": '+Longitude+','+
                                '"Latitude":' +Latitude+','+
                                '"BuildingNumber":'+BuildingNumber+','+
                                '"BuildingName": '+BuildingName+','+
                                '"Floor": '+Floor+','+
                                '"Apartment":'+Apartment+','+
                                '"POBox":"'+PostCode+'",'+
                                '"Description":'+Description+
                            '},'+
                            '"Contact": {'+
                                '"Department": "'+Department+'",'+
                                '"PersonName": "'+personName+'",'+
                                '"Title":'+Title+','+
                                '"CompanyName": "'+companyName+'",'+
                                '"PhoneNumber1": "'+phoneNumber1+'",'+ //mapAccount.get(couriList.Account__c).Mobile_Person_Business__c
                                '"PhoneNumber1Ext": '+PhoneNumber1Ext+','+
                                '"PhoneNumber2": "'+phoneNumber2+'",'+
                                '"PhoneNumber2Ext": '+PhoneNumber1Ext+','+
                                '"FaxNumber": '+FaxNumber+','+
                                '"CellPhone": "'+phoneNumber1+'",'+
                                '"EmailAddress": "'+emailAddress+'",'+
                                '"Type":'+Type+
                            '}'+
                        '},'+
                        '"ThirdParty":'+ThirdParty+','+
                        '"ShippingDateTime": "'+ShippingDateTime+'",'+
                        '"DueDate": "'+DueDate+'",'+
                        '"Comments":"'+Comments+'",'+
                        '"PickupLocation": '+PickupLocation+','+
                        '"OperationsInstructions": '+OperationsInstructions+','+
                        '"AccountingInstrcutions":'+AccountingInstrcutions+','+
                        '"Details": {'+
                            '"Dimensions": '+Dimensions+','+
                            '"ActualWeight": {'+
                                '"Unit": "'+Unit+'",'+
                                '"Value": '+Value+
                            '},'+
                            '"ChargeableWeight": '+ChargeableWeight+','+
                            '"DescriptionOfGoods": "'+DescriptionOfGoods+'",'+
                            '"GoodsOriginCountry": "'+GoodsOriginCountry+'",'+
                            '"NumberOfPieces": '+NumberOfPieces+','+
                            '"ProductGroup": "'+ProductGroup+'",'+
                            '"ProductType": "'+ProductType+'",'+
                            '"PaymentType": "'+PaymentType+'",'+
                            '"PaymentOptions": '+PaymentOptions+','+
                            '"CustomsValueAmount": '+CustomsValueAmount+','+
                            '"CashOnDeliveryAmount":'+CashOnDeliveryAmount+','+
                            '"InsuranceAmount": '+InsuranceAmount+','+
                            '"CashAdditionalAmount": '+CashAdditionalAmount+','+
                            '"CashAdditionalAmountDescription":'+CashAdditionalAmountDescription+','+
                            '"CollectAmount": '+CollectAmount+','+
                            '"Services":'+Services+','+
                            '"Items": '+Items+','+
                            '"DeliveryInstructions":'+DeliveryInstructions+
                       ' },'+
                        '"Attachments":'+Attachments+','+
                        '"ForeignHAWB": '+ForeignHAWB+','+
                        '"TransportType":'+TransportType+','+
                        '"PickupGUID": '+PickupGUID+','+
                        '"Number": '+Numbers+','+
                        '"ScheduledDelivery": '+ScheduledDelivery+
                    '}'+
                '],'+
                '"LabelInfo": {'+
                    '"ReportID": '+ReportID+','+
                    '"ReportType": "'+ReportType+'"'+
                '},'+
                '"ClientInfo": {'+
                    '"Source": '+Source+','+
                    '"AccountCountryCode": "'+AccountCountryCode+'",'+
                    '"AccountEntity": "'+AccountEntity+'",'+
                    '"AccountPin": "'+AccountPin+'",'+
                    '"AccountNumber": "'+AccountNumber+'",'+
                    '"UserName": "'+UserName+'",'+
                    '"Password": "'+Password+'",'+
                    '"Version": "'+Version+'"'+
                '},'+
                '"Transaction": {'+
                    '"Reference1": "",'+
                    '"Reference2": "",'+
                    '"Reference3": "",'+
                    '"Reference4": "",'+
                    '"Reference5": ""'+
                '}'+
            '}';
        }
        system.debug('requestBody>>>>'+requestBody);
        return requestBody;
   }
    
    public static AramaxTrackingResponseJSON2Apex getAramexTracking(String shipmentNo, String accountCountryCode, String accountEntity, String accountNumber){
        Map<String, Country_City_Code_Mapping__mdt> mapCountryCode = getCountryCodeMap();
        string CountryCode = String.isNotEmpty(accountCountryCode) && mapCountryCode.containsKey(accountCountryCode.toLowerCase())
                            ? mapCountryCode.get(accountCountryCode.toLowerCase()).City_Code__c : '';
        Credentials_Details__c creds = Credentials_Details__c.getInstance( 'Aramax Tracking' );
        String body = '{ "Shipments": [ "'+shipmentNo+'" ], "GetLastTrackingUpdateOnly": false, "ClientInfo": { "Source": 24, "AccountCountryCode": "'+CountryCode+'", "AccountEntity": "'+accountEntity+'", "AccountPin": "'+creds.grant_type__c+'", "AccountNumber": "'+creds.Resource__c+'", "UserName":  "'+creds.User_Name__c+'", "Password":  "'+creds.Password__c+'", "Version": "v1" }, "Transaction": { "Reference1": null, "Reference2": null, "Reference3": null, "Reference4": null, "Reference5": null } }';
        system.debug('body---->'+body);
        if(body != Null){
            HTTPResponse resp = FirstFlightService.returnResponseFromService(creds.Endpoint__c, 'POST', creds.User_Name__c, creds.Password__c, body); //h.send(request);
            AramaxTrackingResponseJSON2Apex responseObj = (AramaxTrackingResponseJSON2Apex)System.JSON.deserialize(resp.getBody(), AramaxTrackingResponseJSON2Apex.class);
            system.debug('responseObj ---->'+responseObj);
            if(resp.getStatusCode() != 200){
                Error_Log__c objError = new Error_Log__c(Service_Request_Body__c = body, Service_Response_Body__c = resp.getBody());
                Insert objError;
            }
            return responseObj;
        }
        return null;
    }
    
    public static Map<String, Country_City_Code_Mapping__mdt> getCountryCodeMap(){
        Map<String, Country_City_Code_Mapping__mdt> mapCountryCode = new Map<String, Country_City_Code_Mapping__mdt>();
        List<Country_City_Code_Mapping__mdt> lstMetadata = [Select DeveloperName
                                                                 , MasterLabel
                                                                 , City_Code__c 
                                                             From Country_City_Code_Mapping__mdt];
        for(Country_City_Code_Mapping__mdt objMetadata : lstMetadata){
            String strCountryName = objMetadata.MasterLabel.toLowerCase();
            mapCountryCode.put(strCountryName, objMetadata);   
        }
        return mapCountryCode;
    }
}