/*
* Description - Test class developed for Resubmitcontroller
*
* Version            Date            Author            Description
* 1.0              21/11/2017      Ashish Agarwal     Initial Draft
*/

@isTest
public class ResubmitcontrollerTest {
    
    static testMethod void callUpdatedetails() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase ;
        
        test.startTest();
        
        PageReference pageRef = Page.ResubmitCasePage;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(objCase);
        Resubmitcontroller objController = new Resubmitcontroller(sc);
        PageReference pageRefNew = objController.callUpdatedetails();
        test.stopTest();
    } 
    
    static testMethod void callUpdatedetailsCase2() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Approving_Percent__c  = 1000;
        objCase.Amount_to_be_waived__c  = 10;
        objCase.Approval_Status__c = 'Approved';
        insert objCase ;
        
        test.startTest();
            PageReference pageRef = Page.ResubmitCasePage;
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.standardController(objCase);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
            Resubmitcontroller objController = new Resubmitcontroller(sc);
            PageReference pageRefNew = objController.callUpdatedetails();
        test.stopTest();
    } 
    
    static testMethod void callUpdatedetailsCase3() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Approving_Percent__c  = 100;
        objCase.Amount_to_be_waived__c  = 50000;
        objCase.Approval_Status__c = 'Approved';
        insert objCase ;
        
        test.startTest();
            PageReference pageRef = Page.ResubmitCasePage;
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.standardController(objCase);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
            Resubmitcontroller objController = new Resubmitcontroller(sc);
            PageReference pageRefNew = objController.callUpdatedetails();
        test.stopTest();
    }
}