/****************************************************************************************************
* Description - The apex class developed for Scheduling InquiryShareBatch                           *
*---------------------------------------------------------------------------------------------------*
* Ver   Date        Author              Description                                                 *
* 1.0   25/02/2018  Monali              Initial Draft                                               *
* 1.1   07-06-2018  Craig Lobo          Updated the code to fetch records on the time from the CS   *
***************************************************************************/

// SHARING DONE FROM INFORMATICA

global without sharing class InquiryShareSchedular implements Schedulable {
    
    global void  execute(SchedulableContext sc) {
        /*System.debug('InquiryShareSchedular started..........');
        Generic_Switch_Setting__c csRecord = Generic_Switch_Setting__c.getValues('Promoter Sharing');
        if (csRecord != null && csRecord.Active__c && csRecord.Cron_Expression__c != null) {
            String cronExpr = csRecord.Cron_Expression__c;
            System.debug('cronExpr started...............' + cronExpr);
            System.schedule('InquiryShareBatch ' + system.now(), cronExpr, new InquiryShareSchedular());
            Database.executebatch(new InquiryShareBatch());
            System.debug('InquiryShareSchedular done............');
        }*/
    } // End of method 
} // End of class