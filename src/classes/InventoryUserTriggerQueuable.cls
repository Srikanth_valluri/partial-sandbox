public class InventoryUserTriggerQueuable implements Queueable {
    public List<Assigned_PC__c> newRecordsList;
    public static final String READ_PERMISSION = 'Read';
    public InventoryUserTriggerQueuable(List<Assigned_PC__c> newRecordsList){
        this.newRecordsList = newRecordsList;
    }
    
    public void execute(QueueableContext context) {
        List<Inventory_User__c> upsertInventoryUser = new List<Inventory_User__c>();        
        Map<ID,List<Assigned_PC__c>> mapCampainAssignedPc = new Map<ID,List<Assigned_PC__c>>();
        List<Campaign_Inventory_History__c> campaignHistoryList = new List<Campaign_Inventory_History__c>();
        List<SObject> shareRecordsList = new List<SObject>();
        
        for(Assigned_PC__c thisCamp_PC : newRecordsList ){
            if(mapCampainAssignedPc.containsKey(thisCamp_PC.Campaign__c))
                mapCampainAssignedPc.get(thisCamp_PC.Campaign__c).add(thisCamp_PC);
            else
                mapCampainAssignedPc.put(thisCamp_PC.Campaign__c,new List<Assigned_PC__c>{thisCamp_PC});
            
            Campaign_Inventory_History__c campaignHistoryObj = new Campaign_Inventory_History__c();
            campaignHistoryObj.Marketing_Campaign__c = thisCamp_PC.Campaign__c;
            campaignHistoryObj.Type__c = 'Assigned PC History';
            campaignHistoryObj.Assign_PC__c = thisCamp_PC.User__c;
            campaignHistoryList.add(campaignHistoryObj);
            
            if(thisCamp_PC.User__c != null){ 
                /* Calling sharing utility class to grant access to user hierarchy. */
                sObject shareRecord = createShareRecord( new UtilityWrapperManager.SharingWrapper(
                                                                thisCamp_PC.Campaign__c.getSobjectType().getDescribe().getName(), 
                                                                thisCamp_PC.Campaign__c, thisCamp_PC.User__c, READ_PERMISSION, 
                                                                'Property_Consultant_Access__c'));
                if(shareRecord != null){
                    shareRecordsList.add(shareRecord);     
                } 
            }
        }
        insert campaignHistoryList;
        if(!shareRecordsList.isEmpty()){
            /* Calling queable method to insert sharing records in the sharing table. */
            system.enqueueJob(new UtilitySharingManager(shareRecordsList, new List<sObject>()));
        } 
        
        for(Campaign_Inventory__c inv:[select id,Inventory__c,Campaign__c,Campaign__r.Start_Date__c,Campaign__r.End_Date__c 
                                            from Campaign_Inventory__c WHERE Campaign__c IN : mapCampainAssignedPc.keySet()]){
                for(Assigned_PC__c pc : mapCampainAssignedPc.get(inv.Campaign__c)){
                    upsertInventoryUser.add(new Inventory_User__c(Inventory__c = inv.Inventory__c,
                                                              User__c = pc.user__c,
                                                              Campaign__c = inv.Campaign__c,
                                                              Campaign_ID__c = inv.Campaign__c,
                                                              Unique_Key__c = pc.User__c+'###'+inv.Inventory__c+'###'+inv.Campaign__c,
                                                              Start_Date__c = inv.Campaign__r.Start_Date__c,
                                                              End_Date__c = inv.Campaign__r.End_Date__c
                                                            ));
             }   
        }
        if(!upsertInventoryUser.isEmpty() && upsertInventoryUser.size() > 0){
            Database.UpsertResult[] upsertResults = Database.upsert(upsertInventoryUser,Inventory_User__c.unique_key__c.getDescribe().getSObjectField(),false);
       for (Database.UpsertResult sr : upsertResults) {
                if (sr.isSuccess()) {
                    System.debug('#### Successfully11 inserted record. ' + sr.getId());
                }else {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('#### The following error has occurred.');                   
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('#### Fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }  
    }
    public sObject createShareRecord(UtilityWrapperManager.SharingWrapper uwm_swObject){
      String sharingObjectName;
      sObject shareRecord;
      if(String.isNotBlank(uwm_swObject.objectName)){
        if(uwm_swObject.objectName.endsWithIgnoreCase('__c')){
          sharingObjectName = uwm_swObject.objectName.substringBefore('__c')+'__share';    
        }else{
          sharingObjectName = uwm_swObject.objectName+'share';  
        }
          Schema.SObjectType typeOfShareObject = Schema.getGlobalDescribe().get(sharingObjectName);
          shareRecord = typeOfShareObject.newSObject();
          if(shareRecord != null){
            if(String.isNotBlank(uwm_swObject.objectName) && 
               uwm_swObject.objectName.equalsIgnoreCase('User')){
             shareRecord.put('UserId', uwm_swObject.parentId);  
             shareRecord.put('UserAccessLevel', uwm_swObject.AccessLevel); 
            }else{
              shareRecord.put('ParentId', uwm_swObject.parentId); 
              shareRecord.put('AccessLevel', uwm_swObject.AccessLevel);    
            }
        shareRecord.put('UserOrGroupId', uwm_swObject.UserOrGroupId);  
        shareRecord.put('RowCause', uwm_swObject.RowCause);  
          }
      }
    return shareRecord;
    }
}