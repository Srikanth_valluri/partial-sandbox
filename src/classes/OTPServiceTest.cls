/****************************************************************************************
* Description - Test class developed for AssignmentCancellationController
*
* Version            Date            Author                    Description
* 1.0                22/11/17        Naresh Kaneriya (Accely)  Initial Draft
****************************************************************************************/

@isTest
public class OTPServiceTest{

public static testMethod void Test_1(){

    Test.startTest();
    /*
    list<String> lstPhoneNumbers =  new list<String>();
    lstPhoneNumbers.add('9179876547');
    lstPhoneNumbers.add('5004E0000047ynz:9879076547');    
    lstPhoneNumbers.add('9579876847');
    */
    
    Test.setMock(HttpCalloutMock.class, new MockHttpAgentOTPController());
    String stNumber = '9179876547';
    OTPService.Sendtextmessage(stNumber);
    stNumber = '5004E0000047ynz:9879076547';
    OTPService.Sendtextmessage(stNumber);
    Test.StopTest();

}

}