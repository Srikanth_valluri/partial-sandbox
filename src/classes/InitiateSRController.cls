public class InitiateSRController {
        Calling_List__c objCalling_List;
        public InitiateSRController(ApexPages.StandardController controller) {
            objCalling_List= (Calling_List__c)controller.getrecord();
            objCalling_List = [ Select Account__c,Booking_Unit__c,RecordType.DeveloperName from Calling_List__c where id= :objCalling_List.id limit 1 ];
        }

        public Pagereference initiateAOPTSR() {
            system.debug('objCalling_List :'+objCalling_List );
            system.debug('objCalling_List.RecordType.DeveloperName :'+objCalling_List.RecordType.DeveloperName);
            PageReference pg = null;
            
            if( objCalling_List != null ) {

                if( objCalling_List.RecordType.DeveloperName != null ) {
                // objCalling_List.RecordType.DeveloperName == 'Collections_Calling_List' &&
                    if( objCalling_List.Account__c != null ) {
                        pg = callCREPortalHomeForAOPT( objCalling_List.Account__c );              
                    }
                }
            }
            else {
                system.debug('caseobj is null');
            }
            return pg;
        }

        public Pagereference initiatePOPSR() {
            system.debug('objCalling_List :'+objCalling_List );
            system.debug('objCalling_List.RecordType.DeveloperName :'+objCalling_List.RecordType.DeveloperName);
            PageReference pg = null;
            
            if( objCalling_List != null ) {

                if( objCalling_List.RecordType.DeveloperName != null ) {
                //objCalling_List.RecordType.DeveloperName == 'Collections_Calling_List' &&
                    if(  objCalling_List.Account__c != null ) {
                        pg = callCREPortalPOP( objCalling_List.Account__c );              
                    }
                }
            }
            else {
                system.debug('caseobj is null');
            }
            return pg;
        }

        public Pagereference callCREPortalHomeForAOPT( Id accountID ) {
            PageReference pg = new PageReference('/apex/DummyTitleDeedCREPortal?AccountId='+accountID + '&SRType=AOPT');
            pg.setredirect(true);
            system.debug('in aopt redirect method');
            return pg;
        }
        
        public Pagereference callCREPortalPOP( Id accountID ) {
            PageReference pg = new PageReference('/apex/DummyTitleDeedCREPortal?AccountId='+accountID + '&SRType=ProofOfPayment');
            pg.setredirect(true);
            system.debug('in callCREPortalPOPredirect method');
            return pg;
        }
        
        public Pagereference initiateFMPOPSR() {
            system.debug('objCalling_List :'+objCalling_List );
            system.debug('objCalling_List.RecordType.DeveloperName :'+objCalling_List.RecordType.DeveloperName);
            PageReference pg = null;
            
            if( objCalling_List != null ) {
    
                if( objCalling_List.RecordType.DeveloperName != null ) {
                //objCalling_List.RecordType.DeveloperName == 'Collections_Calling_List' &&
                    if(  objCalling_List.Account__c != null ) {
                        pg = callFMPortalPOP( objCalling_List.Account__c , objCalling_List.Booking_Unit__c );              
                    }
                }
            }
            else {
                system.debug('caseobj is null');
            }
            return pg;
        }
        
        public Pagereference callFMPortalPOP( Id accountID , Id BuId ) {
            PageReference pg = new PageReference('/apex/ProofOfPaymentProcessPage?AccountId='+accountID + '&SRType=Proof_of_Payment&UnitId='+BuId );
            pg.setredirect(true);
            system.debug('in callCREPortalPOPredirect method');
            return pg;
        }        
        
    }