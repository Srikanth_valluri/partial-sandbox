@isTest
public class CreateTaskFromPBTest {
    public static testMethod void unitTest() {
        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id aoptRecordTypeID = caseRecordTypes.get('EOI Refund').getRecordTypeId();
        Account objA = TestDataFactoryFM.createAccount();
        insert objA;
        
        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objA);
        insert objSR;
        
        Booking__c objBooking = TestDataFactoryFM.createBooking(objA,objSR);
        insert objBooking;

        Location__c locObj=TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c objBU = TestDataFactoryFM.createBookingUnit(objA,objBooking);
        objBU.Inventory__c=invObj.id;
        insert objBU;
        
        list<FM_Case__c> lstC = TestDataFactoryFM.createFMCase(2);
        Case objCase = TestDataFactory_CRM.createCase(objA.Id , aoptRecordTypeID);
        objCase.Status = 'Submitted';
        insert objCase;
        
        Case objCase1 = TestDataFactory_CRM.createCase(objA.Id , aoptRecordTypeID);
        objCase1.Status = 'Submitted';
        insert objCase1;

        list<Id> lstId = new list<Id>();
        lstId.add(objCase1.Id);
        lstId.add(objCase.Id);
        CreateTaskFromPB.updateCreateTask(lstId);

    }
}