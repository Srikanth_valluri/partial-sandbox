public without sharing class FinancePagePicklistCtrl{
    
    Public list<SelectOption> statusOptionsResult {get;set;}
    public List<POP_Bank_Details__c> popBankDetails;
    public String caseId;
    Public String selectedOption {get;set;}
     public FinancePagePicklistCtrl(ApexPages.StandardSetController controller) {
        statusOptionsResult = new list<SelectOption>();
          if (ApexPages.currentPage().getParameters().containsKey('id')) {
               caseId = Id.valueOf(ApexPages.currentPage().getParameters().get('id'));
               System.debug('caseId>>>>>'+caseId);
              
              
          }
        //popBankDetails = controller.getRecords();
        System.debug('popBankDetails'+popBankDetails);
         List<POP_Bank_Details__c>  popBankDetailsLst  = [
                                                                SELECT Id
                                                                , Account_Number__c
                                                                , Bank_Reference_number__c
                                                                , Bank_Account_number__c
                                                                , Beneficiary_Name__c
                                                                , Beneficiary_Bank__c
                                                                , Deposit_Amount__c 
                                                                , Deposit_Date__c
                                                                , ForFinance__c
                                                             FROM POP_Bank_Details__c
                                                            WHERE ForFinance__c = true
                                                            ];  


          System.debug('popBankDetailsLst>>>>'+popBankDetailsLst);
          if(popBankDetailsLst != Null && popBankDetailsLst.size() > 0) {
              for (POP_Bank_Details__c objPop : popBankDetailsLst) {                
                        statusOptionsResult.add(new SelectOption(objPop.Bank_Account_number__c+' / '+objPop.Beneficiary_Name__c+' / '+objPop.Beneficiary_Bank__c+' / '+objPop.Account_number__c,objPop.Bank_Account_number__c+' / '+objPop.Beneficiary_Name__c+' / '+objPop.Beneficiary_Bank__c+' / '+objPop.Account_number__c));                                       
             }   
          }                                                                                                           
     }
     
     public PageReference saveDetails () {
      if(caseId != Null) {
        System.debug('selectedOption>>>>'+selectedOption);
        List<POP_Bank_Details__c> popBankDetails =      [SELECT Id
                                                        , Account_Number__c
                                                        , Bank_Reference_number__c
                                                        , Bank_Account_number__c
                                                        , Beneficiary_Name__c
                                                        , Beneficiary_Bank__c
                                                        , Deposit_Amount__c 
                                                        , Deposit_Date__c
                                                        , ForFinance__c
                                                     FROM POP_Bank_Details__c
                                                    WHERE Case__c =: caseId
                                                    ];  
                 if(popBankDetails != null && popBankDetails.size() >0) {
                     System.debug('popBankDetails>>>'+popBankDetails);
                     if(String.isNotBlank(selectedOption)) {
                         popBankDetails[0].Bank_Account_number__c = Decimal.valueOf(selectedOption.split(' / ')[0].trim());
                         
                         popBankDetails[0].Beneficiary_Name__c = selectedOption.split(' / ')[1].trim();   
                         popBankDetails[0].Beneficiary_Bank__c = selectedOption.split(' / ')[2].trim(); 
                         popBankDetails[0].Account_Number__c = selectedOption.split(' / ')[3].trim();                       
                         
                         
                         update popBankDetails[0];
                     }
                 }
            PageReference pageRef = new PageReference('/'+caseId);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null; 
         
     }
}