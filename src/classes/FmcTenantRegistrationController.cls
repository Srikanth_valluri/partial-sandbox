/**
 * @File Name          : FmcTenantRegistrationController.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 1/20/2020, 3:12:14 PM
 * @Modification Log   : 
 * Ver       Date            Author                                 Modification
 * 1.0   |   1/20/2020   |   ChangeMeIn@UserSettingsUnder.SFDoc   | Initial Version
 * 2.0   |   29/09/2020  |   Komal Shitole                        | 1. Changes made in class for assigning owner to FM Building Admin on Submit and Save as Draft.
                                                                  | 2. Changes made in class for Verify codes logic.
                                                                  | 3. Changes made for not to show I agree checkbox when access codes are not verified.

**/
public without sharing class FmcTenantRegistrationController extends FmTenantRegistrationController {

    public  List<SelectOption>  lstUnit             {get; set;}
    public  Boolean             continueInit        {get; set;}
    public  String              enteredEmailCode    {get; set;}
    public  String              enteredMobileCode   {get; set;}
    public  Boolean             verificationSent    {get; set;}
    public  Boolean             contactsValidated   {get; set;}
    public  Boolean             showIAgreeCheckbox   {get;set;}
    public  String              termsLink           {get; set;}
    @testVisible
    private String              actualCodes         {get; set;}

    private static final String CASE_ALREADY_SUBMITTED_MESSAGE = 'Tenant Registration Request {0} is  submitted'
                                            + ' for this unit. Please contact Building Admin for more details.';

    public FmcTenantRegistrationController() {
        super(false);
        if (FmcUtils.isCurrentView('TenantRegistration')) {
            strSRType = 'Tenant_Registration';
            strAccountId = CustomerCommunityUtils.customerAccountId;
            continueInit = true;
            verificationSent = false;
            contactsValidated = false;
            showIAgreeCheckbox = false;
            enteredEmailCode = '';
            enteredMobileCode = '';
            termsLink = '/apex/safetyregulationspage?CaseId=';
            if ('Guest'.equalsIgnoreCase(UserInfo.getUserType())) {
                //Guest TR
                String caseId = ApexPages.currentPage().getParameters().get('id');
                System.debug('--- caseId --- : '+caseId);
                List<FM_Case__c> lstCase;
                if (String.isBlank(caseId)) {
                    //Case Id is not in URL
                    strSelectedUnit = ApexPages.currentPage().getParameters().get('unitId');
                    if (String.isNotBlank(strSelectedUnit)) {
                        // UnitId selected/present in URL
                        List<Booking_Unit__c> unit = [ SELECT Id
                                                              , Unit_Name__c
                                                              , Booking__r.Account__c
                                                        FROM Booking_Unit__c
                                                        WHERE Id = :strSelectedUnit
                                                        LIMIT 1 ];
                        if (!unit.isEmpty()) {
                            //Selected unit exists
                            lstUnit = new List<SelectOption>{
                                new SelectOption(unit[0].Id, unit[0].Unit_Name__c, true)
                            };
                            objUnit = FM_Utility.getUnitDetails(strSelectedUnit);
                            strAccountId = unit[0].Booking__r.Account__c;
                            List<FM_Case__c> lstSubmittedCase = [ SELECT Id
                                                                        , Name
                                                                        , Booking_Unit__c
                                                                        , Booking_Unit__r.Unit_Name__c
                                                                   FROM FM_Case__c
                                                                   WHERE CreatedById = :UserInfo.getUserId()
                                                                   AND Booking_Unit__c = :unit[0].Id
                                                                   AND Origin__c = 'Portal - Guest'
                                                                   AND Request_Type__c = 'Tenant Registration'
                                                                   AND Status__c = 'Submitted'
                                                                   LIMIT 1 ];
                            if (!lstSubmittedCase.isEmpty()) {
                                ApexPages.addMessage(new ApexPages.message(
                                    ApexPages.Severity.WARNING,
                                    String.format(CASE_ALREADY_SUBMITTED_MESSAGE, new List<String> {lstSubmittedCase[0].Name})
                                ));
                                continueInit = false;
                                return;
                            }
                        }
                    }
                } else {
                    // Case Id present in URL
                    lstCase = [ SELECT Id
                                       , Booking_Unit__c
                                       , Booking_Unit__r.Unit_Name__c
                                FROM FM_Case__c
                                WHERE Origin__c = 'Portal - Guest'
                                AND Request_Type__c = 'Tenant Registration'
                                AND Id = :caseId
                                AND Status__c IN ('Draft Request' , 'In Progress' , 'New' , 'Awaiting Correction')
                                LIMIT 1 ];
                    System.debug('lstCase = ' + lstCase);
                    if (!lstCase.isEmpty()) {
                        // Case Awaiting correction present
                        strCaseId = lstCase[0].Id;
                    } else {
                        List<FM_Case__c> lstSubmittedCase = [ SELECT Id
                                                                    , Name
                                                                    , Booking_Unit__c
                                                                    , Booking_Unit__r.Unit_Name__c
                                                              FROM FM_Case__c
                                                              WHERE Account__c = :strAccountId
                                                              AND Origin__c = 'Portal - Guest'
                                                              AND Request_Type__c = 'Tenant Registration'
                                                              AND Id = :caseId
                                                              AND Status__c = 'Submitted'
                                                              LIMIT 1 ];
                        if (!lstSubmittedCase.isEmpty()) {
                            ApexPages.addMessage(new ApexPages.message(
                                ApexPages.Severity.WARNING, String.format(
                                    CASE_ALREADY_SUBMITTED_MESSAGE, new List<String> {lstSubmittedCase[0].Name}
                                )
                            ));
                            continueInit = false;
                            return;
                        }
                    }
                    System.debug('strCaseId = ' + strCaseId);
                    //strCaseId = ApexPages.currentPage().getParameters().get('Id');
                    if( String.isBlank( strCaseId ) ) {
                        strSelectedUnit = '';
                    } else {
                        objFMCase = FM_Utility.getCaseDetails( strCaseId );
                        System.debug('objFMCase = ' + objFMCase);
                        System.debug('objFMCase.Booking_Unit__c = ' + objFMCase.Booking_Unit__c);
                        strSelectedUnit = objFMCase.Booking_Unit__c ;
                        System.debug('strSelectedUnit = ' + strSelectedUnit);
                        if (String.isNotBlank(strSelectedUnit)) {
                            lstUnit = new List<SelectOption>{
                                new SelectOption(lstCase[0].Booking_Unit__c, lstCase[0].Booking_Unit__r.Unit_Name__c, true)
                            };
                            objUnit = FM_Utility.getUnitDetails(strSelectedUnit);
                        }
                    }
                }

            } else {
                // Authenticated Portal User
                lstUnit = getUnitOptions();

                String caseId = ApexPages.currentPage().getParameters().get('id');
                List<FM_Case__c> lstCase;
                if (String.isBlank(caseId)) {
                    lstCase = [ SELECT Id
                                       , Status__c
                                FROM FM_Case__c
                                WHERE Account__c = :strAccountId
                                AND CreatedById = :UserInfo.getUserId()
                                AND Origin__c = 'Portal'
                                AND Request_Type__c = 'Tenant Registration'
                                AND Status__c IN ('New', 'Draft Request', 'In Progress')
                                ORDER BY Status__c
                                LIMIT 1 ];
                } else {
                    lstCase = [ SELECT Id
                                FROM FM_Case__c
                                WHERE Account__c = :strAccountId
                                AND Origin__c = 'Portal'
                                AND Request_Type__c = 'Tenant Registration'
                                AND (Id = :caseId)
                                ORDER BY Status__c
                                LIMIT 1 ];
                }
                System.debug('lstCase = ' + lstCase);
                if (!lstCase.isEmpty()) {
                    strCaseId = lstCase[0].Id;
                } else {
                    List<FM_Case__c> lstSubmittedCase = [ SELECT Id
                                                                , Name
                                                                , Status__c
                                                          FROM FM_Case__c
                                                          WHERE Account__c = :strAccountId
                                                          AND CreatedById = :UserInfo.getUserId()
                                                          AND Origin__c = 'Portal'
                                                          AND Request_Type__c = 'Tenant Registration'
                                                          AND Status__c = 'Submitted'
                                                          ORDER BY Status__c
                                                          LIMIT 1 ];
                    if (!lstSubmittedCase.isEmpty()) {
                        ApexPages.addMessage(new ApexPages.message(
                            ApexPages.Severity.WARNING,
                                String.format(CASE_ALREADY_SUBMITTED_MESSAGE, new List<String> { lstSubmittedCase[0].Name})
                        ));
                        continueInit = false;
                        return;
                    }
                }

                //strCaseId = ApexPages.currentPage().getParameters().get('Id');
                if( String.isBlank( strCaseId ) ) {
                    strSelectedUnit = '';
                } else {
                    objFMCase = FM_Utility.getCaseDetails(strCaseId);
                    strSelectedUnit = objFMCase.Booking_Unit__c;
                    objUnit = FM_Utility.getUnitDetails(strSelectedUnit);
                    if (new Set<String>{'New', 'Draft Request', 'In Progress'}.contains(objFMCase.Status__c)
                        || String.isBlank(strSelectedUnit)
                    ) {
                        lstUnit = getUnitOptions();
                    } else {
                        lstUnit = new List<SelectOption>{
                            new SelectOption(objUnit.Id, objUnit.Unit_Name__c, true)
                        };
                    }
                }
                System.debug('strSelectedUnit = ' + strSelectedUnit);
            }

            System.debug('== strAccountId =='+strAccountId );

            System.debug('strCaseId = ' + strCaseId);
            System.debug('objFMCase = ' + objFMCase);
            System.debug('strSelectedUnit = ' + strSelectedUnit);

            if(objFMCase != NULL && objFMCase.Origin__c != NULL) {
                strOrigin = objFMCase.Origin__c;
            }
            System.debug('--- strOrigin --- : '+strOrigin);

            if( String.isNotBlank( strSelectedUnit ) ) {
                //init();
            }
        }
    }

    public override String checkExistingSubmittedCases() {
        return NULL;
    }

    protected override void initializeFMCase() {
        if(strCaseId == null){
            objFMCase = new FM_Case__c();
            objFMCase.Account__c = strAccountId ;
            objFMCase.Booking_Unit__c = strSelectedUnit ;
            objFMCase.Request_Type_DeveloperName__c = strSRType;
            objFMCase.Status__c = 'New';
            objFMCase.Origin__c = 'Guest'.equalsIgnoreCase(UserInfo.getUserType()) ? 'Portal - Guest' : 'Portal';
            objFMCase.Request_Type__c = 'Tenant Registration';
            objFMCase.Request_Type_DeveloperName__c = 'Tenant_Registration';
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId();
            strOrigin = objFMCase.Origin__c;
            System.debug('--- strOrigin --- : '+strOrigin);
        }
    }

    protected override void populateTenantRecord() {
        Id tenantId = searchTenant( objFMCase.Nationality__c, objFMCase.Passport_Number__c );

        System.debug('-- objFMCase.Mobile_Country_Code__c -- : '+objFMCase.Mobile_Country_Code__c);
        String strCountryCode = String.isNotBlank(objFMCase.Mobile_Country_Code__c)
                                && objFMCase.Mobile_Country_Code__c.split(':').size() >=1 ?
                                    objFMCase.Mobile_Country_Code__c.split(':')[1] :
                                    String.isNotBlank(objFMCase.Mobile_Country_Code__c) ?
                                    objFMCase.Mobile_Country_Code__c : '';
        strCountryCode = strCountryCode.trim();
        System.debug('-- strCountryCode -- : '+strCountryCode);
        System.debug('-- objFMCase.Mobile_no__c -- : '+objFMCase.Mobile_no__c);

        if (String.isBlank(tenantId)) {
            if ('Guest'.equalsIgnoreCase(UserInfo.getUserType())) {
                System.debug('Mobile No. w/o CountryCode ::: '+Long.valueOf(objFMCase.Mobile_no__c.remove('-').remove(' ').remove('+').removeStart('0').removeStart('0')));
                Account tenantAccount = new Account(
                    Id = tenantId,
                    RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId(),
                    FirstName = objFMCase.First_Name__c,
                    LastName = objFMCase.Last_Name__c,
                    PersonMobilePhone = strCountryCode + Long.valueOf(objFMCase.Mobile_no__c.remove('-').remove(' ').remove('+') ),
                    PersonEmail = objFMCase.Tenant_Email__c,
                    Email__pc = objFMCase.Tenant_Email__c,
                    Gender__pc = objFMCase.Gender__c,
                    Nationality__pc = objFMCase.Nationality__c,
                    Passport_Number__pc = objFMCase.Passport_Number__c,
                    Tenant__c = 'Yes',
                    OwnerId = Label.FMDefaultCaseOwnerId,

                    Mobile_Country_Code__pc = objFMCase.Mobile_Country_Code__c,
                    Mobile_Country_Code__c = objFMCase.Mobile_Country_Code__c,

                    Mobile_Country_Code_2__c = objFMCase.Mobile_Country_Code_2__c,
                    Mobile_Country_Code_2__pc = objFMCase.Mobile_Country_Code_2__c,

                    Mobile_Country_Code_3__c = objFMCase.Mobile_Country_Code_3__c,
                    Mobile_Country_Code_3__pc = objFMCase.Mobile_Country_Code_3__c,

                    Mobile_Country_Code_4__c = objFMCase.Mobile_Country_Code_4__c,
                    Mobile_Country_Code_4__pc = objFMCase.Mobile_Country_Code_4__c,

                    Mobile_Country_Code_5__c = objFMCase.Mobile_Country_Code_5__c,
                    Mobile_Country_Code_5__pc = objFMCase.Mobile_Country_Code_5__c,

                    Mobile_Phone_Encrypt__c = objFMCase.Mobile_no__c,

                    Mobile_Phone_Encrypt__pc = strCountryCode + Long.valueOf(objFMCase.Mobile_no__c.remove('-').remove(' ').remove('+').removeStart('0').removeStart('0') ),

                    Mobile_Phone__pc = objFMCase.Mobile_no__c,
                    Mobile_Phone_2__c = objFMCase.Mobile_Phone_2__c,
                    Mobile_Phone_2__pc = objFMCase.Mobile_Phone_2__c,

                    Mobile_Phone_3__c = objFMCase.Mobile_Phone_3__c,
                    Mobile_Phone_3__pc = objFMCase.Mobile_Phone_3__c,

                    Mobile_Phone_4__c = objFMCase.Mobile_Phone_4__c,
                    Mobile_Phone_4__pc = objFMCase.Mobile_Phone_4__c,

                    Mobile_Phone_5__c = objFMCase.Mobile_Phone_5__c,
                    Mobile_Phone_5__pc = objFMCase.Mobile_Phone_5__c
                );
                insert tenantAccount;
                objFMCase.Tenant__c = tenantAccount.Id;
            } else {
                objFMCase.Create_Tenant__c = true;
            }
        } else {
            objFMCase.Tenant__c = tenantId;
            if ('Guest'.equalsIgnoreCase(UserInfo.getUserType())) {
                objFMCase.Update_Tenant__c = true;
            } else {
                update new Account(
                    Id = tenantId,
                    RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId(),
                    FirstName = objFMCase.First_Name__c,
                    LastName = objFMCase.Last_Name__c,
                    PersonMobilePhone = strCountryCode + Long.valueOf(objFMCase.Mobile_no__c.remove('-').remove(' ').remove('+') ),
                    PersonEmail = objFMCase.Tenant_Email__c,
                    Email__pc = objFMCase.Tenant_Email__c,
                    Gender__pc = objFMCase.Gender__c,
                    Nationality__pc = objFMCase.Nationality__c,
                    Passport_Number__pc = objFMCase.Passport_Number__c,
                    Tenant__c = 'Yes',

                    Mobile_Country_Code__pc = objFMCase.Mobile_Country_Code__c,
                    Mobile_Country_Code__c = objFMCase.Mobile_Country_Code__c,

                    Mobile_Country_Code_2__c = objFMCase.Mobile_Country_Code_2__c,
                    Mobile_Country_Code_2__pc = objFMCase.Mobile_Country_Code_2__c,

                    Mobile_Country_Code_3__c = objFMCase.Mobile_Country_Code_3__c,
                    Mobile_Country_Code_3__pc = objFMCase.Mobile_Country_Code_3__c,

                    Mobile_Country_Code_4__c = objFMCase.Mobile_Country_Code_4__c,
                    Mobile_Country_Code_4__pc = objFMCase.Mobile_Country_Code_4__c,

                    Mobile_Country_Code_5__c = objFMCase.Mobile_Country_Code_5__c,
                    Mobile_Country_Code_5__pc = objFMCase.Mobile_Country_Code_5__c,

                    Mobile_Phone_Encrypt__c = objFMCase.Mobile_no__c,
                    Mobile_Phone_Encrypt__pc = strCountryCode + Long.valueOf(objFMCase.Mobile_no__c.remove('-').remove(' ').remove('+').removeStart('0').removeStart('0') ),

                    Mobile_Phone__pc = objFMCase.Mobile_no__c,
                    Mobile_Phone_2__c = objFMCase.Mobile_Phone_2__c,
                    Mobile_Phone_2__pc = objFMCase.Mobile_Phone_2__c,

                    Mobile_Phone_3__c = objFMCase.Mobile_Phone_3__c,
                    Mobile_Phone_3__pc = objFMCase.Mobile_Phone_3__c,

                    Mobile_Phone_4__c = objFMCase.Mobile_Phone_4__c,
                    Mobile_Phone_4__pc = objFMCase.Mobile_Phone_4__c,

                    Mobile_Phone_5__c = objFMCase.Mobile_Phone_5__c,
                    Mobile_Phone_5__pc = objFMCase.Mobile_Phone_5__c

                );
            }
        }
    }

    protected override void setApprovalFields() {
        objFMCase.Approval_Status__c = 'Pending';
        return;
        //if (String.isNotBlank(objFMCase.Tenant__c)) {
        //    objFMCase.Submit_for_Approval__c = true ;
        //}
    }

    protected override Boolean getDocumentValidity() {
        return false;
    }

    public PageReference saveDraft() {
        createFmCase();
        String nextSteps = 'You can click on "Submit Request" button to submit your request.';
        if(objFMCase.Tenant_Email__c == '' || objFMCase.Tenant_Email__c == NULL) {
            ApexPages.addMessage(new ApexPages.message(
                    ApexPages.Severity.ERROR, 'Kindly enter your email id to receive the Access Code to the draft request Or Else please take note of the Access Code provided below.'
            ));
        }
        if (objFMCase.VerifiedEmail__c != objFMCase.Tenant_Email__c
            || objFMCase.VerifiedMobile__c
                !=  ((String.isBlank(objFMCase.Mobile_Country_Code__c) ? '' : objFMCase.Mobile_Country_Code__c)
                    +
                    (String.isBlank(objFMCase.Mobile_no__c) ? '' : objFMCase.Mobile_no__c))
        ) {
            nextSteps = 'You can click on "Continue" button to verify your contact details.';
        }
        ApexPages.addMessage(new ApexPages.message(
            ApexPages.Severity.INFO, 'Your request has been saved successfully.' + ' ' + nextSteps
                    + ' ' + ' Your Access Code for the draft request is : ' + objFMCase.id
        ));
        return NULL;
    }

    public void sendVerificationCodes() {
        String tenantName = '';
        if(objFMCase.First_Name__c != NULL && objFMCase.Last_Name__c != NULL) {
            tenantName = objFMCase.First_Name__c + ' ' +objFMCase.Last_Name__c;
        }
        System.debug('--- tenantName --- : '+tenantName);

        //createFmCase();

        String emailCode    = String.valueOf((Math.random() * 10000).intValue()).leftPad(4, '0');
        String smsCode      = String.valueOf((Math.random() * 10000).intValue()).leftPad(4, '0');
        actualCodes         = emailCode + smsCode;
        //System.debug('actualCodes = ' + actualCodes);
        try {
            emailVerificationCode(emailCode);
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,
                        'There was an error while verifying your email address. Please try again later'));
            insert new Error_Log__c (
                Account__c = strAccountId,
                FM_Case__c = objFMCase.Id,
                Error_Details__c = 'TR Email OTP Verification request failed: ' + e.getMessage(),
                Process_Name__c = 'Generic Email'
            );
        }

        createFmCase();

        smsVerificationCode(smsCode);
        verificationSent = true;


        if (objFMCase != NULL && objFMCase.id != NULL) {
            System.debug('---objFMCase Id---: '+objFMCase.id);
            termsLink = '/apex/safetyregulationspage?CaseId='+objFMCase.id+'&tenantName='+tenantName;
        }
        System.debug('>>>---termsLink--- : '+termsLink);

        //ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'We have sent two separate verification codes to both your email address ' + objFMCase.Tenant_Email__c + ' and on the provided contact number '
        //    + objFMCase.Mobile_Country_Code__c + objFMCase.Mobile_no__c));
    }

    @testVisible
    private void emailVerificationCode(String verificationCode) {

        String tenantName = '';
        if(objFMCase.First_Name__c != NULL && objFMCase.Last_Name__c != NULL) {
            tenantName = objFMCase.First_Name__c + ' ' + objFMCase.Last_Name__c;
        }

        List<OrgWideEmailAddress> orgWideAddressHelloDamac = [
            SELECT  Id
                    , Address
                    , DisplayName
            FROM    OrgWideEmailAddress
            WHERE   Id = :Label.CommunityPortalOrgWideAddressId
        ];

        String emailSubject = 'Community Portal New Tenant Registration Verification Code';

        String emailBody = 'Hello ' + tenantName + ',';
        emailBody += '\n\n' + verificationCode + ' is the code for verifying your email id.';
        emailBody += '\nPlease enter this code along with the code received through SMS (on your'
                        +' registered mobile number) on the My Community portal to Submit the'
                        +' Tenant Registration Request.';
        //emailBody += '\n\nEmail Code: 1234\n';
        //emailBody += 'SMS Code: 5678\n';
        //emailBody += 'Verification Code to be entered on Portal: 12345678\n';
        emailBody += '\n\nRegards,\nMy Community - LOAMS';

        SendGridEmailService.SendGridResponse response = SendGridEmailService.sendEmailService(
            objFMCase.Tenant_Email__c, tenantName, '', '', '', '', emailSubject, '',
            orgWideAddressHelloDamac[0].Address, orgWideAddressHelloDamac[0].DisplayName, '', '', 'text/plain',
            emailBody, '', new List<Attachment>()
        );
        if(response.ResponseStatus == 'Accepted') {
            EmailMessage mail = new EmailMessage();
            mail.Subject = emailSubject;
            mail.MessageDate = System.Today();
            mail.Status = '3';
            mail.RelatedToId = objFMCase.Id;//Put FM case id
            mail.ToAddress = objFMCase.Tenant_Email__c;
            mail.FromAddress = orgWideAddressHelloDamac[0].Address;
            mail.TextBody = emailBody;
            mail.CcAddress = '';
            mail.BccAddress = '';
            mail.Sent_By_Sendgrid__c = true;
            mail.SentGrid_MessageId__c = response.messageId;
            mail.Booking_Unit__c = objFMCase.Booking_Unit__c;
            mail.Account__c = objFMCase.Tenant__c;
            insert mail;
        }
        else {
            Error_Log__c objError = new Error_Log__c();
            objError.Account__c = strAccountId;
            objError.FM_Case__c = objFMCase.Id;
            objError.Error_Details__c = 'TR Email OTP Verification request failed from SendGrid';
            objError.Process_Name__c = 'Generic Email';
            insert objError;
        }
    }

    private void smsVerificationCode(String verificationCode) {
        SMS_History__c verificationSms = new SMS_History__c();
        verificationSms.FM_Case__c = objFMCase.Id;
        verificationSms.Message__c = verificationCode + ' is the code for verifying your mobile number. '
                + 'Please enter this code and the email verification code to submit the Tenant Registration Request '
                + 'on My Community Portal';
        verificationSms.Phone_Number__c =
                (objFMCase.Mobile_Country_Code__c == NULL
                    ? ''
                    : objFMCase.Mobile_Country_Code__c.substringAfterLast(':').trim())
                + objFMCase.Mobile_no__c;

        verificationSms.Name = 'Portal Tenant Registration Verification for '
                + [SELECT Id, Name FROM FM_Case__c WHERE Id = :objFMCase.Id].Name;
        if(Test.isRunningTest()) {

        }
        else {
            //System.debug('verificationSms :'+verificationSms);
            insert verificationSms;
        }
    }

    public void verifyCodes() {
        contactsValidated = actualCodes.equalsIgnoreCase(enteredEmailCode + enteredMobileCode) ? true:false;
        if (contactsValidated) {
            objFMCase.VerifiedEmail__c = objFMCase.Tenant_Email__c;
            objFMCase.VerifiedMobile__c = objFMCase.Mobile_Country_Code__c + objFMCase.Mobile_no__c;
            showIAgreeCheckbox = true;
        }
        else {
            showIAgreeCheckbox = false;
        }
        if(objFMCase.Id != NULL){
            update objFMCase;
        }
    }

    public PageReference submitSr() {
        //objFMCase.OwnerId = UserInfo.getUserId();
        //Code Added on 23/09/2020 to fix the issue of Guest User Cannot be record owner
        /*if ('Guest'.equalsIgnoreCase(UserInfo.getUserType())) {
            objFMCase.OwnerId = Label.Owner_Of_Guest_User_Records;
        }
        else {
            objFMCase.OwnerId = UserInfo.getUserId();
        }*/
        //Code Added on 23/09/2020 End here
        /*Booking_Unit__c objBookingUnit = [Select Id,Unit_Name__c From Booking_Unit__c where Id =:strSelectedUnit];
        Location__c ObjLoc = [SELECT Community_Name__c
                                            , ( SELECT FM_User__c
                                                       , FM_User__r.Email
                                                       , FM_Role__c
                                                       , FM_User__r.Name
                                                FROM FM_Users__r 
                                                WHERE FM_Role__c = 'FM Admin')
                                      FROM Location__c
                                      WHERE Name =: objBookingUnit.Unit_Name__c.substringBefore('/')
                                      AND Recordtype.Name = 'Building'
                                      LIMIT 1 ];
                                      
        if(ObjLoc != Null && objLoc.FM_Users__r.size() > 0) {
            objFMCase.OwnerId = objLoc.FM_Users__r[0].FM_User__c;
        }
        else {
            objFMCase.OwnerId = Label.Owner_Of_Guest_User_Records;
        }*/
        if(String.isNotBlank(strSelectedUnit)) {
           assignFMAdminAsOwner(strSelectedUnit);
        }            
        objFMCase.Submit_for_Approval__c = true; // Added on 17/05/2020

        System.debug('objFmCase before upserting : ' + objFMCase);
        
        PageReference nextPage = submitFmCase();
        if (nextPage != NULL) {
            String unitId = nextPage.getUrl().substringAfterLast('/');
            nextPage = new PageReference(ApexPages.currentPage().getUrl());
            nextPage.getParameters().clear();
            nextPage.getParameters().put('view', 'CaseDetails');
            nextPage.getParameters().put('id', unitId);
            nextPage.setRedirect(true);
        }
        System.debug('nextPage = ' + nextPage);
        return nextPage;
    }

    public void selectUnit() {
        System.debug('strSelectedUnit = ' + strSelectedUnit);
        if( String.isNotBlank( strSelectedUnit ) && continueInit) {
            init();
        }
          System.debug('---objFMCase Id---: ');
    }

    private static List<SelectOption> getUnitOptions() {
        List<SelectOption> lstOptions = new List<SelectOption>{new SelectOption('', 'Select', true)};
        System.debug('CustomerCommunityUtils.customerAccountId:: '+CustomerCommunityUtils.customerAccountId);
        for (Booking_Unit__c unit : FmcUtils.queryOwnedUnitsForAccount(
            CustomerCommunityUtils.customerAccountId, 'Id, Name, Unit_Name__c'
        )) {
            lstOptions.add(new SelectOption(unit.Id, unit.Unit_Name__c));
        }
        return lstOptions;
    }
}