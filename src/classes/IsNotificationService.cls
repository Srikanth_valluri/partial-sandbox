public interface IsNotificationService {
    List<NotificationService.Notification> getNotifications();
    List<NotificationService.Notification> getNotifications(NotificationService.Notification request);
    list<NotificationService.Notification> markRead(list<NotificationService.Notification> notificationList);
    NotificationService.Notification markUnRead(NotificationService.Notification notification);
}