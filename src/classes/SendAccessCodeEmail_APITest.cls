@isTest
public class SendAccessCodeEmail_APITest {
    
     @isTest
    static void testProcessCaseIdBlank(){
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;
        
        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Tenant_Registration',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId(),
                                           Request_Type__c = 'Tenant Registration',
                                           Booking_Unit__c = bookingUnit.Id,
                                           Origin__c = 'Portal - Guest',
                                           Status__c = 'Draft Request',
                                           Tenant_Email__c = 'test@test.com');
        insert fmCase;

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/sendAccessCodeEmail';  
        req.addParameter('bookingUnitId', bookingUnit.id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        SendAccessCodeEmail_API.ProcessCase();
        SendAccessCodeEmail_API.SendTenantEmailAddress();
        Test.stopTest();
    }
    
    @isTest
    static void testProcessCaseIdNotBlank(){
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;
        
        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Tenant_Registration',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId(),
                                           Request_Type__c = 'Tenant Registration',
                                           Booking_Unit__c = bookingUnit.Id,
                                           Origin__c = 'Portal - Guest',
                                           Status__c = 'Draft Request',
                                           Tenant_Email__c = 'test@test.com');
        insert fmCase;
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/sendAccessCodeEmail';  
        //req.addParameter('bookingUnitId', '');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        SendAccessCodeEmail_API.ProcessCase();
        SendAccessCodeEmail_API.SendTenantEmailAddress();
        Test.stopTest();

    }

        @isTest
        static void testProcessCaseIdNotBlank2(){
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;
        
        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Tenant_Registration',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId(),
                                           Request_Type__c = 'Tenant Registration',
                                           Booking_Unit__c = bookingUnit.Id,
                                           Origin__c = 'Portal - Guest',
                                           Status__c = 'Draft Request',
                                           Tenant_Email__c = 'test@test.com');
        insert fmCase;
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/sendAccessCodeEmail';  
        req.addParameter('bookingUnitId', '');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        SendAccessCodeEmail_API.ProcessCase();
        SendAccessCodeEmail_API.SendTenantEmailAddress();
        Test.stopTest();
    }
    
   
}