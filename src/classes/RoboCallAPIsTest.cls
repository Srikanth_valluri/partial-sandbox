@isTest
public class RoboCallAPIsTest {

    @isTest
    static void addCallsTest() {
        FmHttpCalloutMock.Response addCallsResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
        '     "dnd_count": "1",  '  +
        '     "message": "call added successfully",  '  +
        '     "sivr_call_ids": [  '  +
        '       {  '  +
        '         "call_id": "a3341e69-b806-4e46-bed3-cc695d7fd90f"  '  +
        '       },  '  +
        '       {  '  +
        '         "call_id": "1ea7a246-3a76-4b71-af7d-5dbe62aff5a9"  '  +
        '       }  '  +
        '     ]  '  +
        '  }  ' );

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response>{
            Label.RC_Add_Calls => addCallsResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.startTest();
        RoboCallAPIs.addCalls('1324','abcd');
        Test.stopTest();
    }

    @isTest
    static void CreateCampaignTest() {
        RoboCallAPIs.campaignWrapper objWrapper = new RoboCallAPIs.campaignWrapper();
        objWrapper.ivr_id = '1234';
        objWrapper.timezone = 'Africa/Bujumbura';
        objWrapper.priority = '6';

        FmHttpCalloutMock.Response createCampaignReponse = new FmHttpCalloutMock.Response(200, 'Success',  '   {  '  +
        '      "order_id" : "1324",  '  +
        '      "result" : "Success",  '  +
        '      "status_code" : "Done"  '  +
        '   }  '  );

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response>{
            Label.RC_Campaign => createCampaignReponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.startTest();
        RoboCallAPIs.CreateCampaign( objWrapper);
        Test.stopTest();

    }

}