public virtual without sharing class FM_ProofOfPaymentController {

    public String strHelpPageId { get; set; }
    public String strAccountId { get; set; }
    public String strCaseId { get; set; }
    public String strSRType { get; set; }
    public boolean disableIt { get; set;}
    public String docNameMand {get; set;}
    public transient String strDocumentBody { get; set; }
    public transient String strDocumentName { get; set; }
    public transient String deleteAttRecId { get; set; }
    //public transient UnitDetailsService.BookinUnitDetailsWrapper objUnitInfoWrap { get; set; }

    public FM_Case__c objFMCase { get; set; }

    public Booking_Unit__c objUnit { get; set; }

    public list<SR_Attachments__c> lstRecentAttachment { get; set; }
    public list<SR_Attachments__c> lstUploadedDocs { get; set; }
    public list<SelectOption> lstCurrency { get; set; }
    public list<BookingUnitWrapper> lstUnitsWrap { get; set; }

    public list<FM_Documents__mdt> lstDocuments { get; set; }

    @testVisible protected map<String, FM_Documents__mdt> mapProcessDocuments ;
    @testVisible protected map<String, SR_Attachments__c> mapUploadedDocs ;
    @testVisible protected map<Id, SR_Booking_Unit__c> mapUnitJunction ;

    public FM_ProofOfPaymentController() {
        initializeVariables();
        init();
    }

    protected FM_ProofOfPaymentController(Boolean shouldCall) {}

    protected virtual void initializeVariables() {
        strCaseId = ApexPages.currentPage().getParameters().get('Id');
        if( String.isBlank( strCaseId ) ) {
            strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
            strSRType = ApexPages.currentPage().getParameters().get('SRType');
        }
        else {
            objFMCase = FM_Utility.getCaseDetails( strCaseId );
            strAccountId = objFMCase.Account__c ;
            strSRType = objFMCase.Request_Type_DeveloperName__c ;
        }
    }

    public void init() {
        //disableSubmit();
        if( String.isNotBlank( strAccountId ) ) {
            processUnitsWrapper();
        }

        if( objFMCase == NULL || objFMCase.Id == NULL ) {
            initializeFMCase();
        }

        lstRecentAttachment = fetchRecentPops();

        if( String.isNotBlank( strSRType ) && lstUnitsWrap != NULL ) {
            processDocuments();
        }
        /*if( objUnit != NULL && String.isNotBlank( objUnit.Registration_Id__c ) ) {
            //objUnitInfoWrap = UnitDetailsService.getBookingUnitDetails( objUnit.Registration_Id__c );
        }*/
        fecthConfiguredCurrency();
    }

    public PageReference savePopCase() {
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            try {

                objFMCase.Status__c = 'In Progress';
                upsert objFMCase ;

                list<SR_Booking_Unit__c> lstToBeDeleted = new list<SR_Booking_Unit__c>();
                list<SR_Booking_Unit__c> lstToBeInserted = new list<SR_Booking_Unit__c>();
                system.debug('>>>lstUnitsWrap>>>'+lstUnitsWrap);
                for( BookingUnitWrapper objUnitWrap : lstUnitsWrap ) {
                    if( objUnitWrap.isChecked ) {
                        system.debug('>>>mapUnitJunction>>>'+mapUnitJunction);
                        if( mapUnitJunction != NULL && !mapUnitJunction.containsKey( objUnitWrap.objUnit.Id ) ) {
                            lstToBeInserted.add( new SR_Booking_Unit__c( Booking_Unit__c = objUnitWrap.objUnit.Id,
                                                                         FM_Case__c = objFMCase.Id ) );
                        }
                    }
                    else if( mapUnitJunction != NULL && mapUnitJunction.containsKey( objUnitWrap.objUnit.Id ) ){
                        lstToBeDeleted.add( mapUnitJunction.get( objUnitWrap.objUnit.Id ) );
                    }
                }

                insert lstToBeInserted ;
                delete lstToBeDeleted ;

                return new pageReference('/' + objFMCase.Id );
            }
            catch( Exception e ) {
                Error_Log__c objError = FM_Utility.createErrorLog( strAccountId,
                                                                   NULL,
                                                                   objFMCase.Id,
                                                                   'Proof Of Payment',
                                                                   e.getMessage() );
                insert objError ;
            }
        }
        else {
            Error_Log__c objError = FM_Utility.createErrorLog( strAccountId,
                                                               NULL,
                                                               NULL,
                                                               'Proof Of Payment',
                                                               'Case object invalid.' );
            insert objError ;
        }
        return NULL;
    }

    public PageReference submitPopCase() {
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            try {
                objFMCase.Status__c = 'Submitted';
                upsert objFMCase ;

                list<SR_Booking_Unit__c> lstToBeDeleted = new list<SR_Booking_Unit__c>();
                list<SR_Booking_Unit__c> lstToBeInserted = new list<SR_Booking_Unit__c>();
                for( BookingUnitWrapper objUnitWrap : lstUnitsWrap ) {
                    if( objUnitWrap.isChecked ) {
                        if( mapUnitJunction != NULL && !mapUnitJunction.containsKey( objUnitWrap.objUnit.Id ) ) {
                            lstToBeInserted.add( new SR_Booking_Unit__c( Booking_Unit__c = objUnitWrap.objUnit.Id,
                                                                         FM_Case__c = objFMCase.Id ) );
                        }
                    }
                    else if( mapUnitJunction != NULL && mapUnitJunction.containsKey( objUnitWrap.objUnit.Id ) ){
                        lstToBeDeleted.add( mapUnitJunction.get( objUnitWrap.objUnit.Id ) );
                    }
                }

                insert lstToBeInserted ;
                delete lstToBeDeleted ;

                return new pageReference('/' + objFMCase.Id );
            }
            catch( Exception e ) {
                Error_Log__c objError = FM_Utility.createErrorLog( strAccountId,
                                                                   NULL,
                                                                   objFMCase.Id,
                                                                   'Proof Of Payment',
                                                                   e.getMessage() );
                insert objError ;
            }
        }
        else {
            Error_Log__c objError = FM_Utility.createErrorLog( strAccountId,
                                                               NULL,
                                                               NULL,
                                                               'Proof Of Payment',
                                                               'Case object invalid.' );
            insert objError ;
        }
        return NULL;
    }

    public void uploadDocument() {
        system.debug('== strDocumentBody =='+strDocumentBody);
        system.debug('== strDocumentName =='+strDocumentName);

        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            system.debug('*****objFMCase*****'+objFMCase);
            system.debug('*****objFMCase.Name*****'+objFMCase.Name);
            UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
            list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
            if( String.isNotBlank( strDocumentName ) && String.isNotBlank( strDocumentBody ) ) {
                lstWrapper.add( FM_Utility.makeWrapperObject( EncodingUtil.Base64Encode( RentalPoolTerminationHelper.extractBody( strDocumentBody ) ) ,
                                                                       strDocumentName ,
                                                                       strDocumentName ,
                                                                       objFMCase.Name, '1' ) );

            }
            if( !lstWrapper.isEmpty() ) {
                objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo(lstWrapper, 'FM');
                system.debug('== objResponse document upload =='+objResponse);
                if( objResponse != NULL && objResponse.data != NULL ) {
                    insertCustomAttachment( objFMCase.Id, objResponse );
                }
            }
        }
    }

    public void insertCase() {
        try {
            if( objFMCase.Id == NULL ) {
                insert objFMCase ;
                objFMCase = FM_Utility.getCaseDetails( objFMCase.Id );
            }

        }
        catch( Exception e ) {
            System.debug('Exception = ' + e);
            Error_Log__c objError = FM_Utility.createErrorLog( strAccountId,
                                                               NULL,
                                                               objFMCase.Id,
                                                               'NOC for Fit out/Alterations',
                                                               e.getMessage() );
            insert objError ;
        }

    }

    public void deleteAttachment() {
        if( String.isNotBlank( deleteAttRecId ) ) {
           delete new SR_Attachments__c( Id = deleteAttRecId );
           processDocuments();
        }
    }

    @testVisible
    protected void insertCustomAttachment( Id fmCaseId, UploadMultipleDocController.data objResponse ) {
        list<SR_Attachments__c> lstCustomAttachments = new list<SR_Attachments__c>();

        strDocumentName = strDocumentName.substring( 0, strDocumentName.lastIndexOf('.') );

        FM_Documents__mdt objDocMeta = mapProcessDocuments != null && mapProcessDocuments.containsKey( strDocumentName ) ?
                                       mapProcessDocuments.get( strDocumentName ) :
                                       NULL ;

        for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            SR_Attachments__c objCustAttach = new SR_Attachments__c();
            objCustAttach.Account__c = strAccountId ;
            objCustAttach.Attachment_URL__c = objFile.url;
            //objCustAttach.Booking_Unit__c = strSelectedUnit ;
            objCustAttach.FM_Case__c = fmCaseId ;
            if( objDocMeta != NULL && objDocMeta.MasterLabel.equalsIgnoreCase('POP Document')){
                objFMCase.Additional_Doc_File_URL__c = objFile.url;
            }
            objCustAttach.Name = objDocMeta != NULL ? objDocMeta.MasterLabel : '' ;
            objCustAttach.Type__c = objDocMeta != NULL ? objDocMeta.Type__c : '' ;
            objCustAttach.isValid__c = true ;
            lstCustomAttachments.add( objCustAttach );
            strDocumentName = '';
            strDocumentBody = '';
        }
        if( !lstCustomAttachments.isEmpty() ) {
            insert lstCustomAttachments ;
            system.debug('==lstCustomAttachments=='+lstCustomAttachments);
            processDocuments() ;
        }
    }

    @testVisible
    protected virtual void initializeFMCase() {
        objFMCase = new FM_Case__c();
        objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Proof Of Payment').getRecordTypeId();
        objFMCase.Account__c = strAccountId ;
        objFMCase.Status__c = 'New';
        objFMCase.Request_Type__c = 'Proof Of Payment';
        objFMCase.Origin__c = 'Walk-In';
        objFMCase.Request_Type_DeveloperName__c = strSRType ;
        //objFMCase.Email__c =
    }

    @testVisible
    protected void fecthConfiguredCurrency() {
        lstCurrency =  new list<SelectOption>();
        lstCurrency.add( new selectOption( '', '--- None ---') );
        Schema.DescribeFieldResult fieldResult = FM_Case__c.CurrencyIsoCode.getDescribe();
        list<Schema.picklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.picklistEntry f : ple )         {
            lstCurrency.add(new selectOption(f.getLabel(),f.getValue()));
        }
    }

    @testVisible
    protected void processDocuments() {
        lstUploadedDocs = new list<SR_Attachments__c>();
        lstDocuments = new list<FM_Documents__mdt>();
        mapProcessDocuments = new map< String, FM_Documents__mdt >();
        mapUploadedDocs = new map<String, SR_Attachments__c>();

        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            for( SR_Attachments__c objAttach : fetchUploadedDocs() ) {
                mapUploadedDocs.put( objAttach.Name, objAttach );
                lstUploadedDocs.add( objAttach );
            }
        }

        String strCities = '';
        for( BookingUnitWrapper objUnitWrap : lstUnitsWrap ) {
            if( objUnitWrap.objUnit != NULL && String.isNotBlank( objUnitWrap.objUnit.Property_City__c ) ) {
                strCities +=  objUnitWrap.objUnit.Property_City__c + ',' ;
            }
        }
        strCities = strCities.removeEnd(',');

        for( FM_Documents__mdt objDocMeta : FM_Utility.getDocumentsList( strSRType, strCities ) ) {
            mapProcessDocuments.put( objDocMeta.DeveloperName, objDocMeta );
            if( !mapUploadedDocs.containsKey( objDocMeta.MasterLabel ) ) {
                lstDocuments.add( objDocMeta );
            }
            if(objDocMeta.Mandatory__c == true && mapUploadedDocs.containsKey( objDocMeta.MasterLabel )){
                disableIt = true;
            }
        }
    }
    /*public void disableSubmit(){
        disableIt = true;
        List<FM_Documents__mdt > docsList = new List<FM_Documents__mdt >();
        docsList = [ SELECT
                        Document_Name__c
                        ,Mandatory__c
                        ,Process__c
                     FROM
                        FM_Documents__mdt
                    WHERE
                        Process__r.DeveloperName =: strSRType];
        for(FM_Documents__mdt docObj :docsList){
            if(docObj.Mandatory__c == true){
                docNameMand = docObj.Document_Name__c;
                system.debug('>>>>docNameMand>>>>'+docNameMand);
                disableIt = false;
                break;
            }

        }
    }*/
    @testVisible
    protected void processUnitsWrapper() {
        lstUnitsWrap = new list<BookingUnitWrapper>();
        mapUnitJunction = new map<Id, SR_Booking_Unit__c>();
        system.debug('>>>strCaseId>>>'+strCaseId);
        if( String.isNotBlank( strCaseId ) ) {
            for( SR_Booking_Unit__c objUnitJunc : [ SELECT Id
                                                         , Booking_Unit__c
                                                      FROM SR_Booking_Unit__c
                                                     WHERE FM_Case__c = :strCaseId ] ) {
                system.debug('>>>objUnitJunc>>>'+objUnitJunc);
                mapUnitJunction.put( objUnitJunc.Booking_Unit__c, objUnitJunc );
            }
        }
        system.debug('>>>mapUnitJunction>>>'+mapUnitJunction);
        if (String.isNotBlank(strAccountId)) {
            for( Booking_Unit__c objUnit :
                        FmcUtils.queryUnitsForAccount(strAccountId, 'Id, Unit_Name__c, Property_City__c')
            ) {
                lstUnitsWrap.add( new BookingUnitWrapper( mapUnitJunction.containsKey( objUnit.Id ), objUnit ) );
            }
        }
    }

    public list<SR_Attachments__c> fetchRecentPops() {
        if( String.isNotBlank( strAccountId ) ) {
            return [ SELECT Id
                          , FM_Case__r.Name
                          , FM_Case__r.Payment_Date__c
                          , FM_Case__r.Payment_Mode__c
                          , FM_Case__r.Payment_Allocation_Details__c
                          , FM_Case__r.Total_Amount__c
                          , FM_Case__r.Currency__c
                          , FM_Case__r.Sender_Name__c
                          , FM_Case__r.Bank_Name__c
                          , FM_Case__r.Swift_Code__c
                          , FM_Case__r.CreatedDate
                          , Type__c
                          , Attachment_URL__c
                       FROM SR_Attachments__c
                      WHERE FM_Case__r.Account__c =: strAccountId
                        AND Type__c ='POP'
                        AND FM_Case__r.Status__c = 'Submitted'
                   ORDER BY FM_Case__r.CreatedDate DESC
                      Limit 5 ];
        }
        return new list<SR_Attachments__c>();
    }

    public list<SR_Attachments__c> fetchUploadedDocs() {
        if( String.isNotBlank( objFMCase.Id ) ) {
            return [ SELECT Id
                          , Type__c
                          , isValid__c
                          , Attachment_URL__c
                          , Name
                       FROM SR_Attachments__c
                      WHERE FM_Case__c =: objFMCase.Id ];
        }
        return new list<SR_Attachments__c>();
    }

    public class BookingUnitWrapper {

        public Boolean isChecked { get; set; }
        public Booking_Unit__c objUnit { get; set; }
        //public UnitDetailsService.BookinUnitDetailsWrapper objUnitInfoWrap { get; set; }

        public BookingUnitWrapper( Boolean isCheckedTemp, Booking_Unit__c objUnitTemp ) {
            isChecked = isCheckedTemp ;
            objUnit = objUnitTemp ;
            //objUnitInfoWrap = NULL ;
        }
    }

}