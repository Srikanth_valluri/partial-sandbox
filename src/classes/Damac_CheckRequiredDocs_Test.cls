@isTest
public with sharing class Damac_CheckRequiredDocs_Test {

    static testmethod void testUnitDocs () {
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.Agency_Name__c = 'Test Account';
        SR.NSIBPM__Email__c = 'test@nsigulf.com';
        SR.NSIBPM__Send_SMS_to_Mobile__c= '12123';
        SR.COUNTRY_OF_SALE__C = 'UAE;Lebanon;Jordan';
        insert SR;        
        
        Id devRecordTypeId = Schema.SObjectType.Unit_Documents__c.getRecordTypeInfosByName().get('Registration').getRecordTypeId();
        Unit_Documents__c objUnitDocument = new Unit_Documents__c();
        objUnitDocument.Document_Name__c = 'test';
        objUnitDocument.Document_Description__c = 'test wedcf d';
        objUnitDocument.Document_type__c = 'Registration';
        objUnitDocument.Optional__c = false;
        objUnitDocument.Status__c = 'Pending Upload';
        objUnitDocument.User_Doc__c = false;
        objUnitDocument.Service_Request__c = sr.id;
        objUnitDocument.recordtypeId = devRecordTypeId;
        insert objUnitDocument;
    }
}