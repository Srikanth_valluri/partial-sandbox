/****************************************************************************************************
* Name               : ServiceRequestLOITrgHandler                                                  *
* Description        : This is a handler class for service request records(LOI).                    *
* Created Date       : 04/01/2017                                                                   *
* Created By         : NSI                                                                          *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE        DESCRIPTION                                                 *
* 1.0   tejas naik          26/06/2018  Initial Draft                                               *
* 1.1   Craig               10-09-2018  Update the Registartion SR with the Inventory Country       *
* 1.2   Craig               19-09-2018  Added code to Create LOI Agreements                         *
* 1.3   Craig               27-09-2018  Create Step Under the Reg SR to generate Agency Agreements  *
****************************************************************************************************/


global without Sharing class ServiceRequestLOITrgHandler {

    public static Map<String, Attachment> LOI_SRDocAttachmentMap = new Map<String, Attachment>();
    public static List<NSIBPM__SR_Doc__c> LOI_SRDocList = new List<NSIBPM__SR_Doc__c>();
    public static String AGREEMENT_SR_STEP = Label.Agreement_Generation_SR_Step;
    public static String AGREEMENT_STEP_TEMPLATE = Label.Agreement_Generated_Step_Template;
    public static String CLOSED_STEP_STATUS_ID = Label.Closed_Step_Status;
    public static String EXECUTIVE_QUEUE_ID = Label.Executve_Queue;
    public static String DRAFT_SR_STATUS_ID;



    /**
     * executeAfterUpdateTrigger
     */
    public static void updateServiceRequestLOI(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){
        System.debug('>>>>>> ');
        NSIBPM__Service_Request__c newSR = new NSIBPM__Service_Request__c();
        NSIBPM__Service_Request__c oldSR = new NSIBPM__Service_Request__c();
        Set<Id> accountIds = new Set<Id> ();
        
        //List<NSIBPM__Service_Request__c> dealSRList = new List<NSIBPM__Service_Request__c>();
        List<Id> dealSRIdList = new List<Id>();

        for (sObject objSR : newRecordsMap.values()) {
            newSR = (NSIBPM__Service_Request__c)objSR; 
            oldSR = (NSIBPM__Service_Request__c)oldRecordsMap.get(newSR.Id);
            System.debug('===newSR>>>>> ' + newSR.NSIBPM__External_Status_Name__c);
            System.debug('===oldSR>>>>> ' + oldSR.NSIBPM__External_Status_Name__c);
            if (newSR.NSIBPM__Record_Type_Name__c == 'Deal'
                && oldSR.NSIBPM__External_Status_Name__c == 'Submitted'
                && newSR.NSIBPM__External_Status_Name__c == 'Token Deposit Paid'
                
            ) {
                if (newSR.Agency__c != null  ) {
                    accountIds.add(newSR.Agency__c); // Get all the account ids for the Deal SRs.
                }
                dealSRIdList.add(newSR.Id);
            }

            // v1.1 10-09-2018 Craig - Update the Registartion SR with the Inventory Country  
            //if (newSR.NSIBPM__Record_Type_Name__c == 'Deal'
            //    && oldSR.NSIBPM__External_Status_Name__c == 'Draft'
            //    && newSR.NSIBPM__External_Status_Name__c == 'Submitted'
            //) {
                //dealSRList.add(newSR);
                //dealSRIdList.add(newSR.Id);
            //}
            // v1.1 10-09-2018  

        }//End of for

        // v1.1 10-09-2018 Craig - Update the Registartion SR with the Inventory Country 
        if (dealSRIdList != null && !dealSRIdList.isEmpty()) {
            populateRegistrationSR(dealSRIdList);
        }
        // v1.1 END

        System.debug('--Account ids ---- '+ accountIds);
        if (!accountIds.isEmpty()){
            List<NSIBPM__Service_Request__c> regSRList = new List<NSIBPM__Service_Request__c>();
            List<NSIBPM__Service_Request__c> agreementSRList = new List<NSIBPM__Service_Request__c>();
            regSRList = [SELECT NSIBPM__Internal_SR_Status__c,
                                NSIBPM__External_SR_Status__c,
                                NSIBPM__Internal_Status_Name__c,
                                RecordType.Name, LOI_Process__c,
                                UAE_Sale__c, UAE_Generated__c,
                                Jordan_Sale__c, Jordan_Generated__c,
                                Lebanon_Sale__c, Lebanon_Generated__c,
                                Agent_Registration_Type__c
                           FROM NSIBPM__Service_Request__c 
                          WHERE NSIBPM__Customer__c IN :accountIds
                            AND RecordType.Name = 'Agent Registration'
                            AND LOI_Process__c = true
                            //AND Agent_Registration_Type__c = 'LOI'
                            //AND NSIBPM__Internal_Status_Name__c = 'LOI Submitted'
            ];
            System.debug('--Service requests list ---- '+ regSRList);
            if (!regSRList.isEmpty()) {
                for (NSIBPM__SR_Status__c srStatus : [SELECT Id, NSIBPM__Code__c 
                                                        FROM NSIBPM__SR_Status__c 
                                                       WHERE NSIBPM__Code__c = 'DRAFT'
                ]) {
                    if (srStatus.NSIBPM__Code__c == 'DRAFT') {
                        DRAFT_SR_STATUS_ID = srStatus.Id;
                    }
                }
                for(NSIBPM__Service_Request__c srLoopObj : regSRList) {
                    if (srLoopObj.LOI_Process__c) {
                        if (srLoopObj.NSIBPM__Internal_Status_Name__c == 'LOI Submitted'
                            && srLoopObj.Agent_Registration_Type__c == 'LOI' 
                        ) {
                            System.debug('Internal_Status_Name__c>>>>' + srLoopObj.NSIBPM__Internal_Status_Name__c);
                            srLoopObj.NSIBPM__Internal_SR_Status__c = DRAFT_SR_STATUS_ID;
                            srLoopObj.NSIBPM__External_SR_Status__c = DRAFT_SR_STATUS_ID;
                            srLoopObj.NSIBPM__finalizeAmendmentFlg__c = false; 
                            srLoopObj.NSIBPM__Submitted_Date__c = null; 
                            srLoopObj.NSIBPM__Submitted_DateTime__c = null; 
                            srLoopObj.Agent_Registration_Type__c = 'Basic';
                        }
                        if (srLoopObj.NSIBPM__Internal_Status_Name__c != 'LOI Submitted'
                            && srLoopObj.NSIBPM__Internal_Status_Name__c != 'Submitted'
                            && srLoopObj.NSIBPM__Internal_Status_Name__c != 'Draft'
                        ) {
                            if ((srLoopObj.UAE_Sale__c && !srLoopObj.UAE_Generated__c) 
                                || (srLoopObj.Jordan_Sale__c && !srLoopObj.Jordan_Generated__c)
                                || (srLoopObj.Lebanon_Sale__c && !srLoopObj.Lebanon_Generated__c)
                            ){
                                agreementSRList.add(srLoopObj);
                            }
                        }
                    }
                }
                if (!regSRList.isEmpty()) {
                    update regSRList;
                }
                if (!agreementSRList.isEmpty()) {
                    createAgreementGeneratedStep(agreementSRList);
                }
            }
        }
    }

    /**
     * v1.1  10-09-2018  Craig
     * Update the Registartion SR with the Inventory Country 
     */
    public static void populateRegistrationSR(List<Id> pDealSRIdList) {
        if (pDealSRIdList != null) {
            System.debug('populateRegistrationSR>>>> ' + pDealSRIdList);
            List<Selected_Units__c> selectedUnitsList = new List<Selected_Units__c>();
            selectedUnitsList = [ SELECT Id, Inventory__c, Inventory__r.Property_Country__c, 
                                         Service_Request__r.Agency__c, 
                                         Service_Request__r.Agency__r.Reg_SR_Id__c
                                    FROM Selected_Units__c
                                   WHERE Inventory__c != null
                                     AND Service_Request__c IN :pDealSRIdList 
                                     AND Service_Request__r.Agency__c != null
                                     AND Service_Request__r.Agency__r.Reg_SR_Id__c != ''
                                     AND Inventory__r.Property_Country__c != ''
            ];
            System.debug('selectedUnitsList>>>> ' + selectedUnitsList);
            Map<Id, NSIBPM__Service_Request__c> regSRUpdateMap = new Map<Id, NSIBPM__Service_Request__c>();
            for (Selected_Units__c selectedUnit : selectedUnitsList) {
                System.debug('selectedUnit>>>> ' + selectedUnit);
                String srId = selectedUnit.Service_Request__r.Agency__r.Reg_SR_Id__c;
                String invCountry = selectedUnit.Inventory__r.Property_Country__c;
                System.debug('invCountry>>>> ' + invCountry);
                if (regSRUpdateMap.containsKey(srId) 
                    && String.isNotBlank(invCountry) 
                ) {
                    if (invCountry.equalsIgnoreCase('Jordan')) {
                        regSRUpdateMap.get(srId).Jordan_Sale__c = true;
                    } else if (invCountry.equalsIgnoreCase('Lebanon')) {
                        regSRUpdateMap.get(srId).Lebanon_Sale__c = true;
                    } else if (invCountry.equalsIgnoreCase('United Arab Emirates')) {
                         regSRUpdateMap.get(srId).UAE_Sale__c = true;
                    }
                } else {
                    NSIBPM__Service_Request__c regSR = new NSIBPM__Service_Request__c();
                    regSR.Id = srId;
                    if (invCountry.equalsIgnoreCase('Jordan')) {
                        regSR.Jordan_Sale__c = true;
                    } else if (invCountry.equalsIgnoreCase('Lebanon')) {
                        regSR.Lebanon_Sale__c = true;
                    } else if (invCountry.equalsIgnoreCase('United Arab Emirates')) {
                        regSR.UAE_Sale__c = true;
                    }
                    regSRUpdateMap.put(srId, regSR);
                    System.debug('regSR>>>> ' + regSR);
                }
            }
            System.debug('regSRUpdateMap>>>> ' + regSRUpdateMap);
            update regSRUpdateMap.values();
        }
    }

    /**
     * v1.3  27-09-2018  Craig
     * Create Step Under the Reg SR to generate Agency Agreements
     */
    public static void createAgreementGeneratedStep(List<NSIBPM__Service_Request__c> pSRListForStepCreation) {
        if (pSRListForStepCreation != null && !pSRListForStepCreation.isEmpty()) {
            List<NSIBPM__Step__c> agreementStepList = new List<NSIBPM__Step__c>();
            for (NSIBPM__Service_Request__c srLoopObj : pSRListForStepCreation) {
                NSIBPM__Step__c agreementStep = new NSIBPM__Step__c();
                agreementStep.NSIBPM__SR__c = srLoopObj.Id;
                agreementStep.NSIBPM__SR_Step__c = AGREEMENT_SR_STEP;
                agreementStep.NSIBPM__Status__c = CLOSED_STEP_STATUS_ID;
                agreementStep.NSIBPM__Step_Template__c = AGREEMENT_STEP_TEMPLATE;
                agreementStep.OwnerId = EXECUTIVE_QUEUE_ID;
                agreementStep.NSIBPM__Summary__c = 'Site Agreements Generated';
                agreementStep.NSIBPM__Start_Date__c = System.today(); 
                agreementStep.NSIBPM__Closed_Date__c = System.today(); 
                agreementStep.NSIBPM__Closed_Date_Time__c = System.now(); 
                
                agreementStepList.add(agreementStep);
            }
            insert agreementStepList;
        }
    }

    // executeBeforeInsertTrigger
    public static void updateAgentregistrationtype(List<NSIBPM__Service_Request__c>  newserviceRequestlst){ 
        for(NSIBPM__Service_Request__c srobj: newserviceRequestlst){
            if(srobj.Agent_Registration_Type__c =='LOI'){
                srobj.LOI_SR__c = true;
            }
        }
    }

    // executeBeforeUpdateTrigger
    public static void checkIsLoi(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){
        List<NSIBPM__Service_Request__c> registartionSRList = new List<NSIBPM__Service_Request__c>();
        for (sObject objSR : newRecordsMap.values()) {
            NSIBPM__Service_Request__c tempSR = (NSIBPM__Service_Request__c) objSR;
            if(tempSR.NSIBPM__Record_Type_Name__c == 'Agent_Registration'
                && tempSR.NSIBPM__Customer__c == null) {
                registartionSRList.add(tempSR);
            }
        }

        if (!registartionSRList.isEmpty()) {
            NSIBPM__Service_Request__c oldSR = new NSIBPM__Service_Request__c();
            List<String> LOI_ProfilesList = Label.LOI_Profiles.split(',');
            User objUsr = [SELECT Id, Name, Profile.Name
                             FROM User
                            WHERE Id = :UserInfo.getUserId() 
                            LIMIT 1
            ];
            System.debug('objUsr>>>> ' + objUsr);
            System.debug('LOI_ProfilesList>>>> '  + LOI_ProfilesList);
            for (NSIBPM__Service_Request__c newRegSR : registartionSRList) {
                oldSR = (NSIBPM__Service_Request__c) oldRecordsMap.get(newRegSR.Id);
                if(LOI_ProfilesList.contains(objUsr.Profile.Name)) {
                    if (newRegSR.Agent_Registration_Type__c == 'LOI') {
                        newRegSR.LOI_SR__c = true; 
                    } else {
                        newRegSR.LOI_SR__c = false;
                    }
                }
            }
        }
    }


/** The Below Code is Called from Generate LOI Agreements Button on Service Request Object **/

    /**
     * 1.2 CRAIG 18/09/2018 - Create LOI Agreements
     */
    webservice static void generateLOIAgreements(String srId) { 
         System.debug('--generateLOIAgreements ids ---- '+ srId);
        List<NSIBPM__Service_Request__c> srList = [ SELECT Id, Country_of_Sale__c, Zip_Postal_Code__c, 
                                                           Agency_Corporate_Type__c, P_O_Box_Zip_Postal_Code__c,
                                                           CreatedDate,First_Name__c, Last_Name__c, City__c,
                                                           Agency_Type__c, Country__c, Street2__c, Street__c,
                                                           Agency_Name__c, P_O_Box_No__c, Emirate_State__c,
                                                           Trade_License_Number__c, ORN_Number__c, Primary_Language__c,
                                                           Contains_Lebanon__c, Contains_Jordan__c, Contains_UAE__c
                                                      FROM NSIBPM__Service_Request__c 
                                                     WHERE Id = :srId];
        List<NSIBPM__Document_Master__c> masterList = [ SELECT Id, Name, NSIBPM__Code__c,
                                                               NSIBPM__Available_to_client__c
                                                          FROM NSIBPM__Document_Master__c 
                                                         WHERE NSIBPM__Code__c = 'LOI_AGREEMENT'
                                                           AND NSIBPM__Available_to_client__c = true
        ];
        if (srList != null && masterList != null) {
            NSIBPM__Service_Request__c srObject = srList[0];
            String masterDocId = masterList[0].Id;
            LOI_SRDocAttachmentMap = new Map<String, Attachment>();
            LOI_SRDocList = new List<NSIBPM__SR_Doc__c>();
            if (srObject.Contains_Lebanon__c == 'true' 
                || srObject.Contains_Jordan__c == 'true'
                || srObject.Contains_UAE__c == 'true'
            ) {
                List<String> countryOfSaleList = srObject.Country_of_Sale__c.split(';');
                if (countryOfSaleList != null && !countryOfSaleList.isEmpty()) {
                    system.debug('--12-objSR>>>>>>  ' + srObject);
                    delete [SELECT Id, NSIBPM__Document_Description_External__c
                             FROM NSIBPM__SR_Doc__c 
                            WHERE NSIBPM__Document_Description_External__c LIKE 'Download LOI Agreement%'
                              AND NSIBPM__Service_Request__c = :srObject.Id ];
                    system.debug('---objSR>>>>>>  ' + LOI_SRDocList);
                    for (String countryName : countryOfSaleList) {
                        generateLOIDocumentHelper(countryName, srObject, masterDocId);
                    }
                    system.debug('-12--LOI_SRDocList>>>>>>  ' + LOI_SRDocList);
                    insert LOI_SRDocList;
                    system.debug('12---objSR>>>>>>  ');
                    for (NSIBPM__SR_Doc__c srDocObj : LOI_SRDocList) {
                        LOI_SRDocAttachmentMap.get(srDocObj.NSIBPM__Document_Description_External__c).ParentId = srDocObj.Id;
                    }
                    system.debug('---objSR>>>>>>  ' + srObject);
                    insert LOI_SRDocAttachmentMap.values();
                    system.debug('--12-objSR>>>>>>  ' + LOI_SRDocAttachmentMap.values());
                    
                }
            }
        }
    }

    /**
     * 1.2 CRAIG 18/09/2018 - Create LOI Agreements 
     */
    webservice static void generateLOIDocumentHelper(String pDocName, NSIBPM__Service_Request__c pSRObject, String pMasterDocId) {
        PageReference LOI_DocPage;
        String docVersion = '';
        String todaysDate = String.valueOf(System.Today()); 
        todaysDate = todaysDate.substring(0, 10); 
        System.debug(' Primary_Language__c==>'+pSRObject.Primary_Language__c);
        if (pDocName == 'UAE') {
            if (pSRObject.Agency_Corporate_Type__c != 'Real Estate') {
                if (pSRObject.Primary_Language__c == 'Chinese') {
                    LOI_DocPage = Page.Agent_LOI_UAE_Non_RERA_China;
                    docVersion = 'FindersFee Corp China LOI-';
                } else {
                    LOI_DocPage = Page.Agent_LOI_UAE_Non_RERA;
                    docVersion = 'FindersFee LOI-';
                }
            } else {
                LOI_DocPage = Page.Agent_LOI_UAE_RERA;
                docVersion = 'Agents UAE-RERA-LOI-';
            }
        } else if(pDocName == 'Lebanon') {
            LOI_DocPage = Page.Agent_LOI_Lebanon;
            docVersion = 'Agents LOI-Lebanon-';
        } else if (pDocName == 'Jordan') {
            LOI_DocPage = Page.Agent_LOI_Jordan;
            docVersion = 'Agents LOI-Jordan-';
        }

        if(LOI_DocPage != null ) {
            NSIBPM__SR_Doc__c LOI_SRDoc = new NSIBPM__SR_Doc__c();
            if (pSRObject.Agency_Type__c == 'Individual' ) {
                LOI_SRDoc.Name = pSRObject.First_Name__c 
                                + ' ' 
                                + pSRObject.Last_Name__c
                                + ' ' 
                                +  docVersion + todaysDate;
            } else {
                if (pSRObject.Agency_Name__c.length() > 35) {
                    pSRObject.Agency_Name__c  = pSRObject.Agency_Name__c.substring(0,35);
                }
                LOI_SRDoc.Name = pSRObject.Agency_Name__c 
                                + ' '
                                + docVersion + todaysDate;
            }
            LOI_SRDoc.NSIBPM__Service_Request__c = pSRObject.Id;
            LOI_SRDoc.NSIBPM__Status__c = 'Generated';
            LOI_SRDoc.NSIBPM__Document_Master__c = pMasterDocId;
            LOI_SRDoc.NSIBPM__Sys_IsGenerated_Doc__c = true;
            LOI_SRDoc.NSIBPM__Document_Description_External__c = 'Download LOI Agreement for ' + pDocName;
            LOI_SRDocList.add(LOI_SRDoc);
            System.debug(' create LOI_SRDoc ==>'+LOI_SRDoc);

            NSIBPM__Service_Request__c LOI_SR = new NSIBPM__Service_Request__c();
            LOI_SR.Agency_Corporate_Type__c = pSRObject.Agency_Corporate_Type__c;
            LOI_SR.P_O_Box_Zip_Postal_Code__c = pSRObject.P_O_Box_Zip_Postal_Code__c;
            LOI_SR.Country_of_Sale__c = pSRObject.Country_of_Sale__c;
            LOI_SR.Zip_Postal_Code__c = pSRObject.Zip_Postal_Code__c;
            LOI_SR.CreatedDate = pSRObject.CreatedDate;
            LOI_SR.First_Name__c = pSRObject.First_Name__c;
            LOI_SR.Last_Name__c = pSRObject.Last_Name__c;
            LOI_SR.Agency_Type__c = pSRObject.Agency_Type__c;
            LOI_SR.Country__c = pSRObject.Country__c;
            LOI_SR.City__c = pSRObject.City__c;
            LOI_SR.Street__c = pSRObject.Street__c;
            LOI_SR.Street2__c = pSRObject.Street2__c;
            LOI_SR.Agency_Name__c = pSRObject.Agency_Name__c;
            LOI_SR.P_O_Box_No__c = pSRObject.P_O_Box_No__c;
            LOI_SR.Emirate_State__c = pSRObject.Emirate_State__c; 
            LOI_SR.Trade_License_Number__c = pSRObject.Trade_License_Number__c;
            LOI_SR.ORN_Number__c = pSRObject.ORN_Number__c;

            String JSONString = JSON.serialize(LOI_SR);
            LOI_DocPage.getparameters().put('srJSON', JSONString);

            Blob LOI_DocBody;
            try {
                LOI_DocBody = LOI_DocPage.getContentAsPDF();
                System.debug('body should be fine');
            } catch (Exception e) {
                System.debug('in the catch block');
                LOI_DocBody = Blob.valueOf('Some Text');
            }

            Attachment LOI_Attachment = new Attachment();
            LOI_Attachment.Body = LOI_DocBody;
            LOI_Attachment.Name = pDocName + 'Agreement' + '.pdf';
            LOI_Attachment.IsPrivate = false;
            LOI_Attachment.ContentType='application/pdf';
            //LOI_Attachment.ParentId = dealSRId;
            //LOI_SRDoc_Attachment_List(LOI_Attachment);
            String mapKey  = LOI_SRDoc.NSIBPM__Document_Description_External__c;
            LOI_SRDocAttachmentMap.put(mapKey, LOI_Attachment);
        }
    }


}