public class GetStatementController {
    public Id recordId{get;set;}
    public string url2{get;set;}
    public String selectedYear{get;set;}
    //public List<SelectOption> yearLstOptions {get;set;}
    public string quarterSelected{get;set;}
    public string periodSelected {get;set;}
    public string monthSelected {get;set;}
    public Booking_Unit__c objBookingUnit;
    public GetStatementController(ApexPages.StandardController controller) {
        recordId = ApexPages.currentPage().getParameters().get('id');
        init();
    }
    
    public void init() {
         if(recordId != Null) {
                    objBookingUnit = [SELECT Id,
                                              Unit_Name__c,
                                              Booking__r.Account__c,
                                              Booking__r.Account__r.Email__pc,
                                              Booking__r.Account__r.Name
                                      FROM Booking_Unit__c
                                      WHERE Id =: recordId];
         }
    }
    
    public PageReference getDocuments() {
       if(objBookingUnit != Null) {
        String unitName;
        String fileName;
        unitName = (objBookingUnit.Unit_Name__c).replace('/','_');
        System.debug('selectedYear>>>'+selectedYear);
        /*if(Test.isRunningTest()){
                 fileName = 'test.png';
        }*/
        //else {
            if(periodSelected == 'Quarterly') {
                fileName = unitName+'_'+quarterSelected+'_'+selectedYear+'.pdf';
                monthSelected = '';
            }
            else if(periodSelected == 'Monthly') {
                fileName = unitName+'_'+monthSelected+'_'+selectedYear+'.pdf';
                quarterSelected = '';
            }
            else if(periodSelected == 'Yearly') {
                fileName = unitName+'_'+'Y'+'_'+selectedYear+'.pdf';
                quarterSelected = '';
                monthSelected = '';
            }
        //}
        system.debug('fileName>>>>>'+fileName);
           List<Attachment> attchamentList =  GetFileIdOffice365AndSendEmailToCustomer.getFileAndReturnAttachment(fileName,String.valueOf(recordId));
           if(attchamentList != Null && attchamentList.size() > 0) {
              List<EmailMessage> emailMessageList =  GetFileIdOffice365AndSendEmailToCustomer.sendEmailToCustomer(objBookingUnit,attchamentList,'Hospitality_Template', monthSelected, quarterSelected, selectedYear);
              if(emailMessageList != Null && emailMessageList.size() > 0) {
                   ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Email Sent Successfully'));
                  insert emailMessageList;                  
              }
              else {
                  System.debug('Some error occured while sending email');
                  ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Some error occured while sending email'));
              }
           }
        else {
           System.debug('Could not find file');
           ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Could not find file')); 
        }
      }
      
      return null;
    }
    
    public void viewDocument() {
        System.debug('viewDocument>>>');
        String unitName;
        String fileName;
        if(objBookingUnit != Null) {
            unitName = (objBookingUnit.Unit_Name__c).replace('/','_');       
            //String fileName = unitName+'_'+quarterSelected+'_'+selectedYear+'.pdf';
            if(Test.isRunningTest()){
                 fileName = 'test.png';
            }
            else {
                if(periodSelected == 'Quarterly') {
                    fileName = unitName+'_'+quarterSelected+'_'+selectedYear+'.pdf';
                }
                else if(periodSelected == 'Monthly') {
                    fileName = unitName+'_'+monthSelected+'_'+selectedYear+'.pdf';
                }
                else if(periodSelected == 'Yearly') {
                    fileName = unitName+'_'+'Y'+'_'+selectedYear+'.pdf';
                }
            }
            system.debug('fileName>>>>>'+fileName);
            Office365RestService.isForHandoverStatment = true; 
            String fileId = Office365RestService.downloadFromOffice365ByName(fileName, '',objBookingUnit.Id, '', '');          
            Office365RestService.ResponseWrapper responseWrap = new Office365RestService.ResponseWrapper();
            responseWrap = Office365RestService.downloadFromOffice365ById(fileId,'','','','','');
            if(responseWrap != Null && responseWrap.downloadUrl != Null) {
                 url2 = String.isNotBlank(responseWrap.downloadUrl)?responseWrap.downloadUrl:'ERROR' ;
            }
            else {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Document not found')); 
            }
        }
        
        System.debug('url2>>'+url2);
   }
    
}