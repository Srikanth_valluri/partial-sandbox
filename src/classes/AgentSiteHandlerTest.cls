@isTest(seeAllData=false)
public class AgentSiteHandlerTest {
    @isTest static void AgentSiteUpdationOnAccountTest()
    {
        Test.startTest();
        Account accObj= new Account(name='ABC');
        insert accObj; 
        
        Agent_Site__c lstagsites = new Agent_Site__c();
        lstagsites.NAme='UAE';
        lstagsites.Agency__c = accObj.id;
        lstagsites.Active__c = true;
        insert lstagsites;
        
        lstagsites.Start_Date__c=system.today()+1;
        lstagsites.End_Date__c = system.today()+1;
        lstagsites.Active__c = false;
        lstagsites.Tax_Registration_Number__c = '214243';
        lstagsites.Registration_Certificate_Date__c = system.today();
        update lstagsites;
        
        Agent_Site__c lstagsites1 = new Agent_Site__c();
        lstagsites1.NAme='KSA';
        lstagsites1.Agency__c = accObj.id;
        lstagsites1.Active__c = true;
        insert lstagsites1;
        
        lstagsites1.Start_Date__c=system.today()+1;
        lstagsites1.End_Date__c = system.today()+1;
        lstagsites1.Active__c = false;
        lstagsites1.Tax_Registration_Number__c = '21424311';
        lstagsites1.Registration_Certificate_Date__c = system.today();
        update lstagsites1;
        
        Agent_Site__c lstagsites3 = new Agent_Site__c();
        lstagsites3.NAme = 'UAE';
        lstagsites3.Agency__c = accObj.id;
        lstagsites3.Active__c = false;
        lstagsites3.Start_Date__c = system.today();
        lstagsites3.End_Date__c = system.today();
        insert lstagsites3;
        
        lstagsites3.Start_Date__c = null;
        lstagsites3.End_Date__c = null;
        lstagsites3.Active__c = false;
        lstagsites3.Tax_Registration_Number__c = '214243';
        lstagsites3.Registration_Certificate_Date__c = system.today();
        update lstagsites3;
        
        // Inquiry__c inqTestObj=inqTriggerObject.updateExistingMatchingInquiry(inqObj,inqObjNew);
        Test.stopTest();
    }
    @isTest static void AgentSiteUpdationOnAccountTest2()
    {
        Test.startTest();
        Account accObj= new Account(name='ABC');
        insert accObj; 
        Agent_Site__c lstagsites = new Agent_Site__c();
        lstagsites.NAme='UAE';
        lstagsites.Start_Date__c=system.today();
        lstagsites.End_Date__c = system.today();
        lstagsites.Agency__c = accObj.id;
        lstagsites.Active__c = true;
        insert lstagsites;
        lstagsites.Active__c = true;
        lstagsites.Tax_Registration_Number__c = '214243';
        lstagsites.Registration_Certificate_Date__c = system.today();
        update lstagsites;
        // Inquiry__c inqTestObj=inqTriggerObject.updateExistingMatchingInquiry(inqObj,inqObjNew);
        Test.stopTest();
    }
    @isTest static void AgentSiteUpdationOnAccountTest3()
    {
        Test.startTest();
        Account accObj= new Account(name='ABC');
        insert accObj; 
        Agent_Site__c lstagsites = new Agent_Site__c();
        lstagsites.NAme='UK';
        lstagsites.Start_Date__c=system.today();
        lstagsites.End_Date__c = system.today();
        lstagsites.Agency__c = accObj.id;
        lstagsites.Active__c = true;
        insert lstagsites;
        lstagsites.End_Date__c = null;
        lstagsites.Active__c = false;
        lstagsites.Tax_Registration_Number__c = '214243';
        lstagsites.Registration_Certificate_Date__c = system.today();
        update lstagsites;
        // Inquiry__c inqTestObj=inqTriggerObject.updateExistingMatchingInquiry(inqObj,inqObjNew);
        Test.stopTest();
    }
    @isTest static void AgentSiteUpdationOnAccountTest4()
    {
        Test.startTest();
        Account accObj= new Account(name='ABC');
        insert accObj; 
        Agent_Site__c lstagsites = new Agent_Site__c();
        lstagsites.NAme='UK';
        lstagsites.Agency__c = accObj.id;
        lstagsites.Active__c = false;
        insert lstagsites;
        lstagsites.Start_Date__c=system.today()+1;
        lstagsites.End_Date__c = system.today()+1;
        lstagsites.Active__c = true;
        lstagsites.Tax_Registration_Number__c = '214243';
        lstagsites.Registration_Certificate_Date__c = system.today();
        update lstagsites;
        // Inquiry__c inqTestObj=inqTriggerObject.updateExistingMatchingInquiry(inqObj,inqObjNew);
        Test.stopTest();
    }
}