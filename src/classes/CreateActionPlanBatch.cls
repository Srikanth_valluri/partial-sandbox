/****************************************************************************************************
* Name          : CreateActionPlanBatch                                                             *
* Description   : Batch class to create User Action Plan records on every 1st of the month          *
* Created Date  : 27/03/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE            COMMENTS                                                *
* 1.0   Craig Lobo          27/03/2018      Initial Draft.                                          *
* 1.1   Craig Lobo          08/04/2018      a. Populated the Sales Amount for last 6 Months and     *
                                            last 1 year on the the Zero Sales User record.          *
                                            b. Populated a new field Notes on the Zer Sales User    *
                                            Record with a predefiled value                          *
                                            c. Exclude the creation of Zero Sales user record       *
                                            if Target record exist and value is zero for a User     *
* 1.2   Twinkle P           18/06/2018      a. Allow creation of only one User Action Plan record   *
                                             with Action Plan Status as 1 - 25th                    *
                                            b. As batch will be executed every month, only one User *
                                            Action Plan record will be created                      *
                                            c. The User will report to DOS (Manager) instead of HOS *
* 1.3   Twinkle P           20/06/2018      Update the Target & Sales for Previous, Pervious to     *
                                            Previous & Previous to Previous to Previous Month       *
*****************************************************************************************************/

global class CreateActionPlanBatch implements Database.Batchable<sObject> {

    public String BU_QUERY  = ' SELECT Id, Registration_Status__c, Registration_DateTime__c, '
                            + ' Booking__r.Deal_SR__r.OwnerId, Requested_Price__c '
                            + ' FROM Booking_Unit__c '
                            + ' WHERE Booking__r.Deal_SR__r.OwnerId IN :userIdList '
                            + ' AND Registration_Status__c != \'Agreement Transfer\' '
                            + ' AND Registration_Status__c != \'Terminated & Released to Sales Admin\' '
                            + ' AND Registration_Status__c != \'Agreement Rejected By Sales Admin\' '
                            + ' AND Registration_Status__c != \'Auto terminated and Released\' '
                            + ' AND Registration_Status__c != \'Direct Released\' '
                            + ' AND Registration_Status__c != \'Rejected due to Time Out\' '
                            + ' AND Registration_Status__c != \'Rejected During Assignment\' '
                            + ' AND Registration_Status__c != \'Reserved\' '
                            + ' AND Registration_Status__c != \'\' '
                            + ' AND Requested_Price__c != null ';
    public String NOTES     = ' <html> <body> <div style="color: red;"> <b> '
                            + ' Split Deals and UK Sales are not included '
                            + ' </b> </div> </body> </html> ';
    String userQuery        = '';

    /**
     * Start Block
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Id pcProfielId = [ SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id;
        //v1.2 Twinkle P: Change  in query as per : The User will report to DOS (Manager) instead of HOS
        userQuery = ' SELECT Id, Name, ManagerId, Manager.Profile.Name,Manager.Email,Manager.ManagerId,Manager.Manager.Email'
                  + ' FROM User '
                  + ' WHERE ProfileId = \''
                  + pcProfielId
                  + '\' AND isActive = true '
                  + ( Test.isRunningTest()
                      ? ' AND LastName = \'CreateActionPlanBatchTest\' LIMIT 5 '
                      : ''
                  );
        return Database.getQueryLocator(userQuery);
    }

    /**
     * Execute Block
     */
    global void execute(Database.BatchableContext BC, List<sObject> scope) {

        List<String> userIdList                         = new List<String>();
        List<Zero_Sales_User__c> zeroSalesUsersList     = new List<Zero_Sales_User__c>();
        List<User_Action_Plan__c> actionPlanList        = new List<User_Action_Plan__c>();
        Map<String, User> userIdMap                     = new Map<String, User>();
        Map<String, String> userMonthTargetMap          = new Map<String, String>();
        Map<String, Double> userMonthSalesMap           = new Map<String, Double>();
        Map<String, Double> userIdSalesAmountMap        = new Map<String, Double>();
        Map<String, Double> userIdLastYearSales         = new Map<String, Double>();
        Map<String, Double> userIdLastSixMonthsSalesMap = new Map<String, Double>();

        Decimal salesAmount;
        String yearStr = '';

        // Get the list of User Ids to fetch their sales for the last two months
        for (User userObj : (List<User>) scope) {
            userIdMap.put(userObj.Id, userObj);
            userIdSalesAmountMap.put(userObj.Id, 0);
            userIdList.add(userObj.Id);
        }
        System.debug('userIdList >>>> '  + userIdList);

        //Get Last Day of previous month
        Date todaysDate = System.today();
    System.debug('todaysDate >>>> '  + todaysDate);
        Date lastDayOfLastMonth = todaysDate.addDays(-1);
        System.debug('lastDayOfLastMonth >>>> '  + lastDayOfLastMonth);

        DateTime todaysDateTime = datetime.now();
        System.debug('todaysDateTime >>>> '  + todaysDateTime);

        String currentMonth = todaysDateTime.format('MMMMM');
        String lastMonthName = todaysDateTime.addmonths(-1).format('MMMMM');
        String secondLastMonthName = todaysDateTime.addmonths(-2).format('MMMMM');
        String thirdLastMonthName = todaysDateTime.addmonths(-3).format('MMMMM');
      System.debug('currentMonth >>>> '  + currentMonth);
        System.debug('lastMonthName >>>> '  + lastMonthName);
        System.debug('secondLastMonthName >>>> '  + secondLastMonthName);
        System.debug('thirdLastMonthName >>>> '  + thirdLastMonthName);
        
        Date lastyearDate = todaysDate.addYears(-1);
        System.debug('lastyearDate >>>> '  + lastyearDate);

        Date lastSixMonthDate = todaysDate.addMonths(-6);
        System.debug('lastSixMonthDate >>>> '  + lastSixMonthDate);

        // Year
        Integer monthNumber = todaysDate.Month();
        if (monthNumber != 1) {
            yearStr = String.valueOf(todaysDate.year());
        } else {
            yearStr = String.valueOf(todaysDate.addYears(-1).year());
        }
        System.debug('yearStr >>>> '  + yearStr);

        // User's targets for the Last 3 month and the current month
        for(Target__c targetObj : [ SELECT Id, Month__c, User__c, Target__c, Year__c
                                    FROM Target__c
                                    WHERE User__c IN :userIdList
                                    AND Target__c != null
                                    AND Target__c > 0
                                    AND CreatedDate = LAST_N_MONTHS:3
        ]) {
            // v1.1 Craig  -  Added Check for Target Value = 0
            userMonthTargetMap.put(
                targetObj.User__c + '-' + targetObj.Month__c,
                targetObj.Month__c  + ' - AED ' + targetObj.Target__c
            );

        }
        System.debug('userMonthTargetMap >>>> '  + userMonthTargetMap);

        // Calculation of Sales for the last 1 months
        /*String bookingUnitQuery = BU_QUERY
                                + ' AND Registration_DateTime__c = :firstDayOfSecondLastMonth '
                                + ' AND Registration_DateTime__c <= :lastDayOfLastMonth ';

        for (Booking_Unit__c bookingUnitObj : Database.query(bookingUnitQuery)) {
            salesAmount = 0;
            if (userIdSalesAmountMap.containskey(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId)) {
                salesAmount = userIdSalesAmountMap.get(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId);
                salesAmount += bookingUnitObj.Requested_Price__c;
                userIdSalesAmountMap.put(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId, salesAmount);
            } else {
                salesAmount += bookingUnitObj.Requested_Price__c;
                userIdSalesAmountMap.put(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId, salesAmount);
            }
        }*/
    
        // Calculation of Sales from the past 1 year
        String bookingUnitQuery = '';
        bookingUnitQuery = BU_QUERY
                         + ' AND Registration_DateTime__c >= :lastyearDate '
                         + ' AND Registration_DateTime__c <= :todaysDate ';
        System.debug('bookingUnitQuery >>>> '  + bookingUnitQuery);
    System.debug('Database.query(bookingUnitQuery) >>>> '  + Database.query(bookingUnitQuery));
        for (Booking_Unit__c bookingUnitObj : Database.query(bookingUnitQuery)) {
            salesAmount = 0;
            // 1 year Sales
            if (userIdLastYearSales.containskey(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId)) {
                salesAmount = userIdLastYearSales.get(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId);
                salesAmount += bookingUnitObj.Requested_Price__c;
                userIdLastYearSales.put(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId, salesAmount);
            } else {
                salesAmount += bookingUnitObj.Requested_Price__c;
                userIdLastYearSales.put(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId, salesAmount);
            }

            // 6 Months Sales
            if (bookingUnitObj.Registration_DateTime__c >= lastSixMonthDate) {
                salesAmount = 0;
                if (userIdLastSixMonthsSalesMap.containskey(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId)) {
                    salesAmount = userIdLastSixMonthsSalesMap.get(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId);
                    salesAmount += bookingUnitObj.Requested_Price__c;
                    userIdLastSixMonthsSalesMap.put(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId, salesAmount);
                } else {
                    salesAmount += bookingUnitObj.Requested_Price__c;
                    userIdLastSixMonthsSalesMap.put(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId, salesAmount);
                }
            }
        }
        System.debug('userIdLastSixMonthsSalesMap >>>> '  + userIdLastSixMonthsSalesMap);
        System.debug('userIdLastYearSales >>>> '  + userIdLastYearSales);
        
        //V1.3 Twinkle P :  Calculation of Sales in past 3 months and add the records Sales Amount for previous month
        bookingUnitQuery = '';
        Integer numberOfMonths = 3;
        bookingUnitQuery = BU_QUERY
                         + ' AND Registration_DateTime__c = LAST_N_MONTHS :' +numberOfMonths ;
    
        System.debug('bookingUnitQuery >>>> '  + bookingUnitQuery);
        for (Booking_Unit__c bookingUnitObj : Database.query(bookingUnitQuery)) {
            Decimal salesAmountForMonth = 0.0;
            String monthNumberVal = bookingUnitObj.Registration_DateTime__c.format('MMMMM');
            // 3 months  Sales
            String keyValue = bookingUnitObj.Booking__r.Deal_SR__r.OwnerId + '-' +monthNumberVal ;
            System.debug('keyValue >>>> '  + keyValue);
            if (userMonthSalesMap.containskey(keyValue)) {
                salesAmountForMonth = userMonthSalesMap.get(keyValue);
                salesAmountForMonth += bookingUnitObj.Requested_Price__c;
                userMonthSalesMap.put(keyValue, salesAmountForMonth);
            } else {
                salesAmountForMonth += bookingUnitObj.Requested_Price__c;
                userMonthSalesMap.put(keyValue, salesAmountForMonth);
            }
            if(monthNumberVal.equalsIgnoreCase(lastMonthName)) {
              salesAmount = 0;
              if (userIdSalesAmountMap.containskey(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId)) {
                  salesAmount = userIdSalesAmountMap.get(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId);
                  salesAmount += bookingUnitObj.Requested_Price__c;
                  userIdSalesAmountMap.put(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId, salesAmount);
              } else {
                  salesAmount += bookingUnitObj.Requested_Price__c;
                  userIdSalesAmountMap.put(bookingUnitObj.Booking__r.Deal_SR__r.OwnerId, salesAmount);
              }
            }
        }
        System.debug('userIdSalesAmountMap >>>> '  + userIdSalesAmountMap);
        
        // Create Zero Sales User records for the PC who have Zero Sales for the last months
        for (String userId : userIdSalesAmountMap.keySet()) {
            System.debug('userId >>>> '  + userId);
            if (userIdSalesAmountMap.get(userId) == 0.0) {
                String userMonthStr1 = userId
                                     + '-'
                                     + lastMonthName;
                String userMonthStr2 = userId
                                     + '-'
                                     + secondLastMonthName;
                String userMonthStr3  = userId
                                      + '-'
                                      + thirdLastMonthName;
                String userMonthStrCurr = userId
                                        + '-'
                                        + currentMonth;
                System.debug('userMonthStr3 >>>> '  + userMonthStr3);
                System.debug('userMonthStr2 >>>> '  + userMonthStr2);
                System.debug('userMonthStr1 >>>> '  + userMonthStr1);
                System.debug('userMonthStrCurr >>>> '  + userMonthStrCurr);
                System.debug('userMonthTargetMap.containsKey(userMonthStr1) >>>> '  + userMonthTargetMap.containsKey(userMonthStr1));
                //if (userMonthTargetMap.containsKey(userMonthStr1)) {
                    System.debug('CONTAINS KEY >>>> ' );
                    Zero_Sales_User__c zsUser = new Zero_Sales_User__c();
                    zsUser.Name = userIdMap.get(userId).Name
                                + '\'s Action Plan for '
                                + todaysDate.month()
                                + '/'
                                + todaysDate.year();
                    zsUser.Employee__c = userId;
                    zsUser.HOS_Descision__c = 'Pending';
                    zsUser.Target_Month_1__c = userMonthTargetMap.containskey(userMonthStr1) ? userMonthTargetMap.get(userMonthStr1) : '';
                    zsUser.Target_Month_2__c = userMonthTargetMap.containskey(userMonthStr2) ? userMonthTargetMap.get(userMonthStr2) : '';
                    zsUser.Target_Month_3__c = userMonthTargetMap.containskey(userMonthStr3) ? userMonthTargetMap.get(userMonthStr3) : '';
                    zsUser.Sales_Month_1__c  = lastMonthName  + ' - AED ' + (userMonthSalesMap.get(userMonthStr1) == null ? 0 : userMonthSalesMap.get(userMonthStr1)) ;
                    zsUser.Sales_Month_2__c   = secondLastMonthName  + ' - AED ' +  (userMonthSalesMap.get(userMonthStr2) == null ? 0 : userMonthSalesMap.get(userMonthStr2));
                    zsUser.Sales_Month_3__c  = thirdLastMonthName  + ' - AED ' +  (userMonthSalesMap.get(userMonthStr3) == null ? 0 : userMonthSalesMap.get(userMonthStr3));
                    zsUser.Current_Month_Target__c = userMonthTargetMap.get(userMonthStrCurr);
                    zsUser.Last_1_Year_Sales__c = userIdLastYearSales.get(userId);
                    // v1.1 Craig  - Added new field for Note
                    zsUser.Note__c = NOTES;


                    zsUser.Last_6_Months_Sales__c = userIdLastSixMonthsSalesMap.get(userId);
                    //v1.2 Twinkle P: The User will report to DOS (Manager) instead of HOS
                    if (userIdMap.get(userId).ManagerId != null &&
                        ( userIdMap.get(userId).Manager.Profile.Name.equalsIgnoreCase ('Director of Sales')
                            ||
                          userIdMap.get(userId).Manager.Profile.Name.equalsIgnoreCase ('Head of Sales')
                        )
                    ) {
                        zsUser.OwnerId = userIdMap.get(userId).ManagerId;
                        zsUser.DOS_Email_Id__c =   userIdMap.get(userId).Manager.Email;
                        zsUser.Send_Email_For_DOS__c = true;
                        if(userIdMap.get(userId).Manager.ManagerId != null) {
                              zsUser.HOS_Email_Id__c =  userIdMap.get(userId).Manager.Manager.Email;
                              zsUser.Send_Email__c = true;
                        }

                    }
                    zeroSalesUsersList.add(zsUser);
                    System.debug('zeroSalesUsersList ADD >>>> ' + zeroSalesUsersList);
                //}
            }
        }
     System.debug('zeroSalesUsersList >>>> '  + zeroSalesUsersList);
        // DML
        if(!zeroSalesUsersList.isEmpty()) {
            System.debug('zeroSalesUsersList ADD >>>> ' + zeroSalesUsersList);
            insert zeroSalesUsersList;
        }

        // Create Action Plan for the Zero Sales Users
        for (Zero_Sales_User__c zsUserRec : zeroSalesUsersList) {
            //v1.2 Twinkle P: Allow creation of only one User Action Plan record with Action Plan Status as 1 - 25th
            User_Action_Plan__c actionPlan = new User_Action_Plan__c();
            actionPlan.Name = userIdMap.get(zsUserRec.Employee__c).Name
                            + '\'s Action Plan ';
            actionPlan.Zero_Sales_User__c = zsUserRec.Id;
            actionPlan.PC__c = zsUserRec.Employee__c;
            actionPlan.Action_Plan__c = '1st to 25th';
            actionPlanList.add(actionPlan);
        }
     System.debug('actionPlanList >>>> '  + actionPlanList);
        // DML
        if(!actionPlanList.isEmpty()) {
            insert actionPlanList;
        }
    }

    /**
     * Finish Block
     */
    global void finish(Database.BatchableContext BC) {}

}