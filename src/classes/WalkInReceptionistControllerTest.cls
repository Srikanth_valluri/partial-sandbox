@isTest
private class WalkInReceptionistControllerTest {
    
    static testMethod void getTestResults() {
                
        User PCuser = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
         LastName = 'last',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US'
        );
        insert PCuser;
        
        
        Id recordTypeId = Schema.sObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        
        Id devRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        Inquiry__c inq = new Inquiry__c();
        inq.Mobile_Phone_Encrypt__c = '05789088';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Preferred_Language__c = 'English';
        inq.Inquiry_Status__c = 'New' ;
        inq.First_Name__c = 'Test First name';
        inq.Last_Name__c = 'Test Last name';
        inq.Email__c = 'test@gmail.com';
        inq.Inquiry_Source__c = 'Customer Referral';
        inq.Mobile_Phone__c = '456789';
        inq.RecordTypeId = devRecordTypeId;
        inq.Inquiry_Status__c = 'Meeting Scheduled';
        inq.Sales_Office__c = 'AKOYA';
        inq.Meeting_Due_Date__c = system.now();
        inq.Assigned_PC__c = PCuser.Id;
        insert inq;
        
        Office_Meeting__c  officeMeeting = new Office_Meeting__c();
        officeMeeting.Inquiry__c = inq.id;
        officeMeeting.Outcome__c = 'Show';
        officeMeeting.Status__c = 'Not Started';
        insert officeMeeting;

        WalkInReceptionistController.searchInquiryName('test');
        WalkInReceptionistController cls = new WalkInReceptionistController();
        cls.inqNumber = inq.Id;
        cls.showInqDetails = true;
        
        cls.searchInquiry();
        cls.showCheckIn = true;
        officeMeeting.outcome__c = 'Show';
        cls.updateCheckIn();
        
        cls.updateInqDetails();
        cls.showCheckOut = true;
        cls.UpdateCheckOut();
        cls.updateSurvey();
    }
}