//Generated by wsdl2apex

public class AOPTDocumentGenerationUtilComXsd {
    public class AttributeArrForAll {
        public String attribute1;
        public String attribute10;
        public String attribute11;
        public String attribute12;
        public String attribute13;
        public String attribute14;
        public String attribute15;
        public String attribute16;
        public String attribute17;
        public String attribute18;
        public String attribute19;
        public String attribute2;
        public String attribute20;
        public String attribute3;
        public String attribute4;
        public String attribute5;
        public String attribute6;
        public String attribute7;
        public String attribute8;
        public String attribute9;
        private String[] attribute1_type_info = new String[]{'attribute1','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute10_type_info = new String[]{'attribute10','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute11_type_info = new String[]{'attribute11','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute12_type_info = new String[]{'attribute12','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute13_type_info = new String[]{'attribute13','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute14_type_info = new String[]{'attribute14','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute15_type_info = new String[]{'attribute15','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute16_type_info = new String[]{'attribute16','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute17_type_info = new String[]{'attribute17','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute18_type_info = new String[]{'attribute18','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute19_type_info = new String[]{'attribute19','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute2_type_info = new String[]{'attribute2','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute20_type_info = new String[]{'attribute20','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute3_type_info = new String[]{'attribute3','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute4_type_info = new String[]{'attribute4','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute5_type_info = new String[]{'attribute5','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute6_type_info = new String[]{'attribute6','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute7_type_info = new String[]{'attribute7','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute8_type_info = new String[]{'attribute8','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute9_type_info = new String[]{'attribute9','http://util.com/xsd',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://util.com/xsd','true','true'};
        private String[] field_order_type_info = new String[]{'attribute1','attribute10','attribute11','attribute12','attribute13','attribute14','attribute15','attribute16','attribute17','attribute18','attribute19','attribute2','attribute20','attribute3','attribute4','attribute5','attribute6','attribute7','attribute8','attribute9'};
    }
    public class AttributeArr1 {
        public String attribute1;
        public AOPTDocumentGenerationUtilComXsd.AttributeArrForAll[] attribute2;
        private String[] attribute1_type_info = new String[]{'attribute1','http://util.com/xsd',null,'0','1','true'};
        private String[] attribute2_type_info = new String[]{'attribute2','http://util.com/xsd',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://util.com/xsd','true','true'};
        private String[] field_order_type_info = new String[]{'attribute1','attribute2'};
    }
}