/****************************************************************************************************************
* Name                  : CampaignInventoryTriggerHandler                                                       *
* Trigger               : CampaignInventoryTrigger                                                              *
* Test Class            : CampaignInventoryTriggerHandlerTest                                                   *
* Description           : This trigger will create the Team users or Agency users in Inventory Users            *
* Created By            : NSI                                                                                   *               
* Created Date          : 07/Feb/2017                                                                           *   
* ------------------------------------------------------------------------------------------------------------  *
* VERSION     AUTHOR                DATE            Description                                                 *
* 1.0         Sivasankar K        07/02/2017        Initial development                                         *
* 2.0         Rahul-Godara        04/07/2017        Inventory shared with campaign PC                           *
* 3.0         Vineet              10/07/2017        Added code to update the associated inventory in case the,  * 
*                                                   campaign inventory is deleted.                              *
****************************************************************************************************************/
public class CampaignInventoryTriggerHandler implements TriggerFactoryInterface{
    
    /*********************************************************************************************
    * @Description : Method to execute after insert.                                             *
    * @Params      : Map<Id, sObject>                                                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterInsertTrigger(Map<Id, sObject> newRecordsMap){
        Map<Id,List<Campaign_Inventory__c>> mapCampainInventories = new Map<Id,List<Campaign_Inventory__c>>();
        List<Campaign_Inventory_History__c> campaignHistoryList= new List<Campaign_Inventory_History__c>();
        for(Campaign_Inventory__c campInvInstance : (List<Campaign_Inventory__c>)newRecordsMap.values()) {
            Campaign_Inventory_History__c campaignHistoryObj = new Campaign_Inventory_History__c();
            campaignHistoryObj.Campaign_Inventory__c = campInvInstance.Id;
            campaignHistoryObj.Inventory__c = campInvInstance.Inventory__c;
            campaignHistoryObj.Marketing_Campaign__c = campInvInstance.Campaign__c;
            campaignHistoryObj.Type__c = 'Campaign Inventory History';
            campaignHistoryList.add(campaignHistoryObj);
        }
        insert campaignHistoryList;

        try{
            for(Campaign_Inventory__c thisCamp_Inventory : (List<Campaign_Inventory__c>)newRecordsMap.values()){
                if(mapCampainInventories.containsKey(thisCamp_Inventory.Campaign__c)){
                    mapCampainInventories.get(thisCamp_Inventory.Campaign__c).add(thisCamp_Inventory);
                }else{
                    mapCampainInventories.put(thisCamp_Inventory.Campaign__c,new List<Campaign_Inventory__c>{thisCamp_Inventory});
                }
            }
            if(!mapCampainInventories.isEmpty()){
                /* Calling method to create inventory users. */
                createInventoryUsers(mapCampainInventories);
            }
        }catch(Exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to execute after delete.                                             *
    * @Params      : List<Campaign_Inventory__c>                                                 *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterDeleteTrigger(Map<Id,sObject> oldRecordsMap){
          
        try{
            /* Calling method to delete the inventory user record on campaign inventory record deletion. */
            deleteInventoryUsers((List<Campaign_Inventory__c>)oldRecordsMap.values());
            /* Calling method to update associated inventory. */
            updateInventory((List<Campaign_Inventory__c>)oldRecordsMap.values());
        }catch(Exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
    }
    
    /*********************************************************************************************
    * @Description : Delete the inventory user records on deletion of campaign inventory.        *
    * @Params      : List<Campaign_Inventory__c>                                                 *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    @TestVisible private void deleteInventoryUsers(List<Campaign_Inventory__c> oldCampaignInventories){
        Set<Id> campaignIds = new Set<Id>();
        Set<Id> inventoryIds = new Set<Id>();
        for(Campaign_Inventory__c thisCamp_Inventory : oldCampaignInventories){
            if(thisCamp_Inventory.Campaign__c != null){
                campaignIds.add(thisCamp_Inventory.Campaign__c);
            }
            if(thisCamp_Inventory.Inventory__c != null){
                inventoryIds.add(thisCamp_Inventory.Inventory__c);
            }
        }
        delete [SELECT Id FROM Inventory_User__c WHERE Campaign_Id__c IN: campaignIds AND Inventory__c IN:inventoryIds];
    }
    
    /*********************************************************************************************
    * @Description : Method to update the associated inventory to the campaign.                  *
    * @Params      : List<Campaign_Inventory__c>                                                 *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    @TestVisible private void updateInventory(List<Campaign_Inventory__c> campaignInventoryList){
        List<Inventory__c> updateInventoryList = new List<Inventory__c>();
        for(Campaign_Inventory__c thisRecord : campaignInventoryList){
            Inventory__c updateInventory = new Inventory__c(Id = thisRecord.Inventory__c);
            updateInventory.Is_Assigned__c = false;
            updateInventoryList.add(updateInventory);   
        }   
        if(!updateInventoryList.isEmpty()){
            update updateInventoryList; 
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to create inventory user records when an inventory is allocated to,  *  
    *                the campaign and the PC's and Agents are already added to the campaign.     *
    * @Params      : Map<Id,List<Campaign_Inventory__c>>                                         *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    @TestVisible private void createInventoryUsers(Map<Id, List<Campaign_Inventory__c>> mapOfCampInventories){
        List<Inventory_User__c> upsertInventoryUser = new List<Inventory_User__c>();
        for(Assigned_Agent__c assAgent : [SELECT Id, Campaign__c, Start_Date__c, End_Date__c, User__c 
                                          FROM Assigned_Agent__c 
                                          WHERE Campaign__c IN: mapOfCampInventories.keySet()]){
            for(Campaign_Inventory__c campInve : mapOfCampInventories.get(assAgent.Campaign__c)){
                upsertInventoryUser.add(
                    new Inventory_User__c(Inventory__c = campInve.Inventory__c,
                                          User__c = assAgent.User__c,
                                          Campaign__c = campInve.Campaign__c,
                                          Campaign_Id__c = campInve.Campaign__c,
                                          Unique_Key__c = assAgent.User__c+'###'+campInve.Inventory__c+'###'+campInve.Campaign__c,
                                          Start_Date__c = assAgent.Start_Date__c,
                                          End_Date__c = assAgent.End_Date__c));
            }
        }
        for(Assigned_PC__c thisPc : [SELECT Id, Campaign__c, Start_Date__c, End_Date__c, User__c 
                                     FROM Assigned_PC__c 
                                     WHERE Campaign__c IN: mapOfCampInventories.keySet()]){
            for(Campaign_Inventory__c campInve : mapOfCampInventories.get(thisPc.Campaign__c)){
                upsertInventoryUser.add(
                    new Inventory_User__c(Inventory__c = campInve.Inventory__c,
                                          User__c = thisPc.User__c,
                                          Campaign__c = campInve.Campaign__c,
                                          Campaign_Id__c = campInve.Campaign__c,
                                          Unique_Key__c = thisPc.User__c+'###'+campInve.Inventory__c+'###'+campInve.Campaign__c,
                                          Start_Date__c = thisPc.Start_Date__c,
                                          End_Date__c = thisPc.End_Date__c));
            }
        }
        if(!upsertInventoryUser.isEmpty() && upsertInventoryUser.size() > 0){
            Database.UpsertResult[] upsertResults = Database.upsert(upsertInventoryUser, Inventory_User__c.unique_key__c.getDescribe().getSObjectField(), false);
        }
    }

    // TO BE Implemented
    public void executeBeforeInsertTrigger(List<sObject> newRecordsList){}
    public void executeBeforeUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){}
    public void executeBeforeInsertUpdateTrigger(List<sObject> newRecordsList, Map<Id,sObject> oldRecordsMap){}

    public void executeBeforeDeleteTrigger(Map<Id,sObject> oldRecordsMap) {
       List<Campaign_Inventory_History__c> campaignPCHistoryList = new List<Campaign_Inventory_History__c>();
       for(Campaign_Inventory_History__c campHisObj : [SELECT
                                                            Inventory__c
                                                          FROM
                                                            Campaign_Inventory_History__c
                                                          WHERE
                                                            Campaign_Inventory__c IN :oldRecordsMap.keySet()])
       {
           campHisObj.Deleted_On__c = System.now();
           campHisObj.Deleted_By__c = UserInfo.getName();
           campaignPCHistoryList.add(campHisObj);
        }
        update campaignPCHistoryList;
    }
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){}
    public void executeAfterUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){}
}// End of class.