public without sharing class CheckDuesStatusController {
    Id callingListId;
    public CheckDuesStatusController(ApexPages.StandardController sc) {
        callingListId = ApexPages.currentPage().getParameters().get('id');
    }

    public PageReference updateCallingList() {
        List<Calling_List__c> lstCallingList = new List<Calling_List__c>();
        Id bouncedChequeCLRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Bounced Cheque').getRecordTypeId();
        lstCallingList = [SELECT Case__C, Id
                            FROM Calling_List__c
                            WHERE RecordtypeId =: bouncedChequeCLRecordTypeId
                            AND Calling_List_Status__c != 'Closed'
                            AND ID =: callingListId];
        if( ! lstCallingList.isEmpty()) {
            BouncedChequeUtility.updateCallingList(lstCallingList);
        }
        return new PageReference('/' + callingListId);
    }
}