/****************************************************************************************************************
Description :
-----------------------------------------------------------------------------------------------------------------
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
-----------------------------------------------------------------------------------------------------------------
1.1     | 22-09-2020        |Aishwarya Todkar   | Added condition of CrtObjectId__c
*****************************************************************************************************************/
global with sharing class CallLogTranscriptHelper{
    
    global static void sendToPhpUtility(List<Call_Log__c> listCallLog) {
        Set<Id> setCallLogId = new Set<Id>();
        for (Call_Log__c objCallLog : listCallLog) {
            if( objCallLog.Call_Type__c != 'Outbound Attempted' && String.isNotBlank( objCallLog.CrtObjectId__c ) ) {
                setCallLogId.add(objCallLog.Id);
            }
        }
        
        if (!setCallLogId.isEmpty() && !Test.isRunningTest()) {
            sendData(setCallLogId);
        }
    }
    
    @future(callout=true)
    public static void sendData(Set<Id> setCallId) {
        CallLogTranscriptUtility.sendCallOutData(setCallId);
    }
}