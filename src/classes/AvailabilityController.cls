/**************************************************************************************************
* Name               : AvailabilityController                                               
* Description        : An apex page controller for                                              
* Created Date       : NSI - Diana                                                                        
* Created By         : 24/Jan/2017                                                                 
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         NSI - Diana          24/Jan/2017                                                               
**************************************************************************************************/
public class AvailabilityController {
    
    /**************************************************************************************************
            Variables used in the class
    **************************************************************************************************/
    private String otherInvUserQuery = ' SELECT Id, Inventory__c, User__c FROM Inventory_User__c '
                                     + ' WHERE User__c != \''
                                     + UserInfo.getUserId()
                                     + '\'';

    private String currentInvUserQuery  = ' SELECT Id, User__c, Inventory__c FROM Inventory_User__c '
                                        + ' WHERE User__c = \''
                                        + Userinfo.getUserId()
                                        + '\' AND Inventory__r.Status__c = \'Released\' ';

    public Set<Id> usersInvIdSet = new Set<Id>();
    public Set<Id> otherInvIdSet;

    public Map<string, set<String>> mpProperties{get; set;}
    public Map<string, List<Inventory__c>> mpAvailable{get; set;}
    public Map<string, Map<string, List<Inventory__c>>> mpFinal{get; set;}
    public Map<string, Location__c> mpBuildings {get; set;}
    public string widthPx{get; set;}
    public string ptWidthPx{get; set;}
    public Map<String, List<Payment_Plan__c>> mpPaymentPlans{get; set;}
    public Map<Id, List<Payment_Terms__c>> mpPaymentTerms{get; set;}
    public string planKeyStr{get; set;}
    public string pTermKeyStr{get; set;}
    public String propId{set;get;}
    public List<String> marketingDocList = new List<String>();
    public Double offset{
        get{
            TimeZone timeZ = UserInfo.getTimeZone();
            return timeZ.getOffset(Datetime.now())/(1000*3600*24.0);
        }
    }
    
    /**************************************************************************************************
    Method:         AvailabilityController
    Description:    Constructor executing model of the class 
    **************************************************************************************************/
    public AvailabilityController(){
        
        if(ApexPages.currentPage().getParameters().containsKey('Page') 
            && ApexPages.currentPage().getParameters().get('Page') == 'invertorySearch'
        ){
            preparePDF();
        } else {
            prepareData();
        }
    }

    /**************************************************************************************************
    Method:         prepareData
    Description:    retrieve inventory information to display in pdf 
    **************************************************************************************************/
    public void prepareData(){
        
        id agentId = ApexPages.currentPage().getParameters().get('agentId');
        propId = ApexPages.currentPage().getParameters().get('propId');
        System.debug('>>>>>>>>>>>>>>>>'+agentId);
        System.debug('>>>>>>>>>PropId'+propId);

        string query = DamacUtility.getCreatableFieldsSOQL('Inventory__c');
        query+= ' where Unit_location__c != null and Status__c = \'Released\''; 
        if(propid != null)
            query+= ' and Property__r.Id =: propId';
        
        if(agentId != null){
            System.debug('>>>>>>>>>>'+agentId);
            string inventoryIdQuery ='SELECT Inventory__c from Inventory_User__c where Inventory__r.Status__c = \'RELEASED\' AND Inventory__r.Unit_Location__c != null'+ 
                                         ' AND Inventory__r.Address__c != null AND End_Date__c >= TODAY AND User__c=\''+agentId+'\'';

            String generalInventoryQuery = 'SELECT Id from Inventory__c where Status__c=\'RELEASED\' AND Unit_Location__c != null'+                                
            ' AND Address__c != null AND Is_Assigned__c = false';  


            //Location Name Selected is blank or All then dont include it in query
            
            if(ApexPages.currentPage().getParameters().containsKey('Bedrooms') && ApexPages.currentPage().getParameters().get('Bedrooms') != ''
                && ApexPages.currentPage().getParameters().get('Bedrooms') != 'All'){
                String bedroomsSelectedFromURL = ApexPages.currentPage().getParameters().get('Bedrooms');
                
                if(null != bedroomsSelectedFromURL){
                    List<String> bedroomsSelectedLists = bedroomsSelectedFromURL.split(',');
                    
                    if(null != bedroomsSelectedLists){
                        inventoryIdQuery += ' AND Inventory__r.IPMS_Bedrooms__c IN (\''+String.join(bedroomsSelectedLists,'\',\'')+'\')';
                        generalInventoryQuery += ' AND IPMS_Bedrooms__c IN (\''+String.join(bedroomsSelectedLists,'\',\'')+'\')';
                        system.debug('**location query '+inventoryIdQuery);
                    }
                }
                 
               
             }  
             
             if(ApexPages.currentPage().getParameters().containsKey('Type') && ApexPages.currentPage().getParameters().get('Type') != ''
                && ApexPages.currentPage().getParameters().get('Type') != 'All'){
               String typeSelectedFromURL= ApexPages.currentPage().getParameters().get('Type');
                
                if(null != typeSelectedFromURL){
                    List<String> typeSelectedLists= typeSelectedFromURL.split(',');
                    
                    if(null != typeSelectedLists){
                        inventoryIdQuery += ' AND Inventory__r.Unit_Type__c IN (\''+String.join(typeSelectedLists,'\',\'')+'\')';
                        generalInventoryQuery += ' AND Unit_Type__c IN (\''+String.join(typeSelectedLists,'\',\'')+'\')';
                        system.debug('**location query '+inventoryIdQuery);
                    }
                }
                       
             }

            if(ApexPages.currentPage().getParameters().containsKey('Location') && ApexPages.currentPage().getParameters().get('Location') != ''
                && ApexPages.currentPage().getParameters().get('Location') != 'All'){
                String locationSelectedFromURL = ApexPages.currentPage().getParameters().get('Location');
                
                if(null != locationSelectedFromURL){
                    List<String> locationSelectedLists = locationSelectedFromURL.split(',');
                    
                    if(null != locationSelectedLists){
                        inventoryIdQuery += ' AND Inventory__r.Address__r.City__c IN (\''+String.join(locationSelectedLists,'\',\'')+'\')';
                        generalInventoryQuery += ' AND Address__r.City__c IN (\''+String.join(locationSelectedLists,'\',\'')+'\')';
                        system.debug('**location query '+inventoryIdQuery);
                    }
                }
            }

            if(ApexPages.currentPage().getParameters().containsKey('MinPrice') 
                && ApexPages.currentPage().getParameters().get('MinPrice') != ''
            ){
                String priceMin = ApexPages.currentPage().getParameters().get('MinPrice');
                inventoryIdQuery += ' AND ( Inventory__r.Selling_Price__c >=' + priceMin +' OR Inventory__r.Special_Price_calc__c >= ' +priceMin + ') ';
                generalInventoryQuery += ' AND ( Selling_Price__c >=' + priceMin +' OR Special_Price_calc__c >= ' +priceMin + ') ';
            }
            
            if(ApexPages.currentPage().getParameters().containsKey('MaxPrice') 
                && ApexPages.currentPage().getParameters().get('MaxPrice') != ''
            ){
                String priceMax = ApexPages.currentPage().getParameters().get('MaxPrice');
                inventoryIdQuery += ' AND ( Inventory__r.Selling_Price__c <=' + priceMax +' OR Inventory__r.Special_Price_calc__c <= ' +priceMax + ') ';
                generalInventoryQuery += ' AND ( Selling_Price__c <=' + priceMax +' OR Special_Price_calc__c <= ' +priceMax + ') ';
            }

            if(ApexPages.currentPage().getParameters().containsKey('villaType') && ApexPages.currentPage().getParameters().get('villaType') != ''
                && ApexPages.currentPage().getParameters().get('villaType') != 'All'){
                String villaTypeSelectedFromURL = ApexPages.currentPage().getParameters().get('villaType');
                
                if(null != villaTypeSelectedFromURL){
                    List<String> villaTypeSelectedLists =villaTypeSelectedFromURL.split(',');
                    
                    if(null != villaTypeSelectedLists){
                        inventoryIdQuery += ' AND Inventory__r.Project_Category__c IN (\''+String.join(villaTypeSelectedLists,'\',\'')+'\')';
                        generalInventoryQuery += ' AND Project_Category__c IN (\''+String.join(villaTypeSelectedLists,'\',\'')+'\')';
                        
                    }
                }
            }

             if(ApexPages.currentPage().getParameters().containsKey('propertyStatus') && ApexPages.currentPage().getParameters().get('propertyStatus') != ''
                && ApexPages.currentPage().getParameters().get('propertyStatus') != 'All'){
                String propertyStatusSelectedFromURL = ApexPages.currentPage().getParameters().get('propertyStatus');
                if(null != propertyStatusSelectedFromURL){
                    List<String> propertyStatusSelectedLists =propertyStatusSelectedFromURL.split(',');
                    
                    if(null != propertyStatusSelectedLists){
                        inventoryIdQuery += ' AND Inventory__r.Property_Status__c IN (\''+String.join(propertyStatusSelectedLists,'\',\'')+'\')';
                        generalInventoryQuery += ' AND Property_Status__c IN (\''+String.join(propertyStatusSelectedLists,'\',\'')+'\')';
                        
                    }
                }
            }

            if(ApexPages.currentPage().getParameters().containsKey('floorPkgType') && ApexPages.currentPage().getParameters().get('floorPkgType') != ''
                && ApexPages.currentPage().getParameters().get('floorPkgType') != 'All'){
                String floorPkgTypeSelectedFromURL = ApexPages.currentPage().getParameters().get('floorPkgType');
                
                if(null != floorPkgTypeSelectedFromURL){
                    List<String> floorPkgTypeSelectedLists =floorPkgTypeSelectedFromURL.split(',');
                    
                    if(null != floorPkgTypeSelectedLists){
                        inventoryIdQuery += ' AND Inventory__r.Floor_Package_Type__c IN (\''+String.join(floorPkgTypeSelectedLists,'\',\'')+'\')';
                        generalInventoryQuery += ' AND Floor_Package_Type__c IN (\''+String.join(floorPkgTypeSelectedLists,'\',\'')+'\')';
                        
                    }
                }
            }

            if(ApexPages.currentPage().getParameters().containsKey('floorPkgName') && 
                ApexPages.currentPage().getParameters().get('floorPkgName') != ''){
                String floorPkgName = ApexPages.currentPage().getParameters().get('floorPkgName');
                inventoryIdQuery += '  AND Inventory__r.Floor_Package_Name__c =\''+floorPkgName+'\'';
                generalInventoryQuery += '  AND Floor_Package_Name__c =\''+floorPkgName+'\'';
            }

             String masterDeveloperSelectedFromURL = null;
            if(ApexPages.currentPage().getParameters().containsKey('MasterDeveloper') &&
             ApexPages.currentPage().getParameters().get('MasterDeveloper') != '' && 
             null != ApexPages.currentPage().getParameters().get('MasterDeveloper')
                ){
                masterDeveloperSelectedFromURL = ApexPages.currentPage().getParameters().get('MasterDeveloper');
            }else if(ApexPages.currentPage().getParameters().containsKey('MasterDeveloperNonFilter') &&
             ApexPages.currentPage().getParameters().get('MasterDeveloperNonFilter') != '' && 
             null != ApexPages.currentPage().getParameters().get('MasterDeveloperNonFilter')){
                 masterDeveloperSelectedFromURL = ApexPages.currentPage().getParameters().get('MasterDeveloperNonFilter');
             }
                

            if(null != masterDeveloperSelectedFromURL){
               
                inventoryIdQuery += '  AND Inventory__r.Master_Developer_EN__c =\''+masterDeveloperSelectedFromURL+'\'';
                generalInventoryQuery += '  AND Master_Developer_EN__c =\''+masterDeveloperSelectedFromURL+'\'';
            }

             if(ApexPages.currentPage().getParameters().containsKey('ACDDate') && ApexPages.currentPage().getParameters().get('ACDDate') != ''){
                String acdDateFromURL = ApexPages.currentPage().getParameters().get('ACDDate');
                
                if(null != acdDateFromURL){
                    List<String> acdDateLists =acdDateFromURL.split(',');
                    
                    if(null != acdDateLists){
                        inventoryIdQuery += ' AND Inventory__r.Anticipated_Completion_Date__c IN (\''+String.join(acdDateLists,'\',\'')+'\')';
                        generalInventoryQuery += ' AND Anticipated_Completion_Date__c IN (\''+String.join(acdDateLists,'\',\'')+'\')';
                        
                    }
                }
            }

             if(ApexPages.currentPage().getParameters().containsKey('BedroomType') && ApexPages.currentPage().getParameters().get('BedroomType') != ''){
                String bedroomTypeFromURL = ApexPages.currentPage().getParameters().get('BedroomType');
                
                if(null != bedroomTypeFromURL){
                    List<String> bedroomTypeLists =bedroomTypeFromURL.split(',');
                    if(null != bedroomTypeLists){
                        inventoryIdQuery += ' AND Inventory__r.Bedroom_Type__c IN (\''+String.join(bedroomTypeLists,'\',\'')+'\')';
                        generalInventoryQuery += ' AND Bedroom_Type__c IN (\''+String.join(bedroomTypeLists,'\',\'')+'\')';
                        system.debug('**generalInventoryQuery query '+generalInventoryQuery);
                    }
                }
            }

            if(ApexPages.currentPage().getParameters().containsKey('MarketingName') &&
             ApexPages.currentPage().getParameters().get('MarketingName') != '' && 
             null != ApexPages.currentPage().getParameters().get('MarketingName')
                ){
                 String MarketingNameSelectedFromURL = ApexPages.currentPage().getParameters().get('MarketingName');

                if(null != MarketingNameSelectedFromURL){

                    inventoryIdQuery += '  AND Inventory__r.Marketing_Name__c =\''+MarketingNameSelectedFromURL+'\'';
                    generalInventoryQuery += '  AND Marketing_Name__c =\''+MarketingNameSelectedFromURL+'\'';
                   
                }
            }

             if(ApexPages.currentPage().getParameters().containsKey('District') &&
             ApexPages.currentPage().getParameters().get('District') != '' && 
             null != ApexPages.currentPage().getParameters().get('District')
                ){
                 String DistrictSelectedFromURL = ApexPages.currentPage().getParameters().get('District');

                if(null != DistrictSelectedFromURL){
                    inventoryIdQuery += '  AND Inventory__r.Property__r.District__c =\''+DistrictSelectedFromURL+'\'';
                    generalInventoryQuery += '  AND Property__r.District__c =\''+DistrictSelectedFromURL+'\'';
                }
            }

            system.debug('**marketingDocList query '+marketingDocList);
            if(!marketingDocList.isEmpty()){
                inventoryIdQuery += ' AND Inventory__r.Marketing_Name_Doc__r.Id IN (\''+String.join(marketingDocList,'\',\'')+'\')';
                generalInventoryQuery += ' AND Marketing_Name_Doc__r.Id IN (\''+String.join(marketingDocList,'\',\'')+'\')';
                system.debug('**location query '+inventoryIdQuery);
                
            }


            system.debug('inventoryIdQuery>>>' + inventoryIdQuery);
            system.debug('generalInventoryQuery>>>' + generalInventoryQuery);

            Set<Id> InventoryIDs = UtilityQueryManager.getInventoryIDs(inventoryIdQuery);
            Set<Id> generalInventoryIds= UtilityQueryManager.getAllGeneralInventories(generalInventoryQuery);        
            
            system.debug('**** generalInventoryId '+generalInventoryIds );        
            
            InventoryIDs.addAll(generalInventoryIds);
            
            query+= ' and Id in : InventoryIDs';
            
        }

        mpProperties = new Map<String,set<String>>();
        mpAvailable = new Map<String, list<Inventory__c>>();
        mpFinal = new Map<string, Map<string, List<Inventory__c>>>();
        mpBuildings = new Map<String,Location__c>();
        mpPaymentTerms = new Map<Id, List<Payment_Terms__c>>();
        mpPaymentPlans = new Map<String, List<Payment_Plan__c>>();
        planKeyStr = '';
        
        List<Schema.FieldsetMember> lstMembers = SObjectType.Inventory__c.FieldSets.Project_Unit.getFields();
        integer width= Math.round(100/(lstMembers.size()-1));
        widthpx = width+'%';
        
        lstMembers = SObjectType.Payment_Terms__c.FieldSets.PaymentTerm.getFields();
        width= Math.round(100/lstMembers.size());
        ptWidthPx = width+'%';
        
        system.debug('**** query '+query );   
        for(Inventory__c inv: database.query(query)){
             if(!mpAvailable.containsKey(inv.Building_ID__c)){
                mpAvailable.put(inv.Building_ID__c, new List<Inventory__c>{inv});
             }
             else{
                List<Inventory__c> existing = mpAvailable.get(inv.Building_ID__c);
                existing.add(inv);
                mpAvailable.put(inv.Building_ID__c,existing);
             }
        }
        
        for(Location__c loc: [Select id, location_Id__c, Construction_status__c, status__c, Building_name__c from Location__c where Location_Id__c in: mpAvailable.keyset()]){
            mpBuildings.put(loc.location_Id__c, loc);
        }
        
        set<string> buildingIds = new set<string>();
        set<Id> PaymentPlanIds = new set<Id>();
        date currDate = system.today();
        for(Payment_Plan__c pp: [Select id,Building_ID__c from Payment_Plan__c where Building_ID__c in: mpBuildings.keyset() and Effective_From__c<=:currDate and Effective_To_calculated__c>=:currDate]){
            PaymentPlanIds.add(pp.Id);
            buildingIds.add(pp.Building_ID__c);
            if(!mpPaymentPlans.containsKey(pp.Building_ID__c))
                mpPaymentPlans.put(pp.Building_ID__c, new List<Payment_Plan__c>{pp});
            else{
                List<Payment_Plan__c> existing = mpPaymentPlans.get(pp.Building_ID__c);
                existing.add(pp);
                mpPaymentPlans.put(pp.Building_ID__c, existing); 
            }
        }
        
        for(string bId : buildingIds){
            planKeyStr+=bId+';';
        }
        
        set<String> paymentIds = new set<String>();
        string ppQuery = DamacUtility.getCreatableFieldsSOQL('Payment_Terms__c');
        ppQuery+=' where Payment_Plan__c in: PaymentPlanIds order by line_ID__c';
        for(Payment_Terms__c pt: database.query(ppQuery)){
            paymentIds.add(pt.Payment_Plan__c);
            if(!mpPaymentTerms.containsKey(pt.Payment_Plan__c)){
                mpPaymentTerms.put(pt.Payment_Plan__c, new list<Payment_Terms__c>{pt});
            }
            else{
                List<Payment_Terms__c> existing = mpPaymentTerms.get(pt.Payment_Plan__c);
                existing.add(pt);
                mpPaymentTerms.put(pt.Payment_Plan__c, existing);
            }
            
        }
        system.debug(mpProperties);
        system.debug(mpAvailable);
        system.debug(mpFinal);
        system.debug(mpBuildings);
        system.debug(mpPaymentTerms);
        system.debug(mpPaymentPlans);

        for(string pId : paymentIds){
            pTermKeyStr+=pId+';';
        }
    }

    /*ESPL*/
    public void preparePDF(){
        System.debug('>>>>>>>>>preparePDF');
        id agentId = ApexPages.currentPage().getParameters().get('agentId');
        propId = ApexPages.currentPage().getParameters().get('propId');
        System.debug('>>>>>>>>>>>>>>>>'+agentId);
        System.debug('>>>>>>>>>PropId'+propId);
        
        otherInvIdSet = new Set<Id>();
        for(Inventory_User__c currInvUserRec :  Database.query(currentInvUserQuery) ){
            usersInvIdSet.add(currInvUserRec.Inventory__c);
        }
        System.debug('usersInvIdSet>>>' + usersInvIdSet);

        for(Inventory_user__c otherInvUserRec : Database.query(otherInvUserQuery) ){
            if(usersInvIdSet.size() > 0){
                if(!usersInvIdSet.contains(otherInvUserRec.Inventory__c)){
                    otherInvIdSet.add(otherInvUserRec.Inventory__c);
                }
            } else{
                otherInvIdSet.add(otherInvUserRec.inventory__c);
            }
        }
        System.debug('otherInvIdSet>>>' + otherInvIdSet);




        String inventoryIdQuery = ' SELECT Inventory__c from Inventory_User__c ' 
                                + ' WHERE Inventory__r.Status__c = \'RELEASED\' '
                                + ' AND Inventory__r.Address__c != null '
                                + ' AND End_Date__c >= TODAY '
                                + ' AND User__c = \'' + agentId + '\'';

        String generalInventoryQuery    = ' SELECT Id from Inventory__c '
                                        + ' WHERE Status__c = \'RELEASED\' '
                                        + ' AND Address__c != null '
                                        + ' AND Is_Assigned__c = false '
                                        + ' AND ID NOT IN :otherInvIdSet ';  





        String query = DamacUtility.getCreatableFieldsSOQL('Inventory__c');

        if(agentId != null){ 

            query += ' WHERE Status__c = \'Released\''; 
            if(String.isNotBlank(propid)) {
                query += ' AND Property__r.Id =: propId';
            }

            if(ApexPages.currentPage().getParameters().containsKey('MarketingDoc') 
                && ApexPages.currentPage().getParameters().get('MarketingDoc') != ''
            ){
                String markDoc = ApexPages.currentPage().getParameters().get('MarketingDoc');
                List<String> markDocNameList = markDoc.split(',');
                for (Marketing_Documents__c markObj : [ SELECT ID FROM Marketing_Documents__c 
                                                        WHERE Name IN :markDocNameList]
                ) {
                    marketingDocList.add(markObj.Id);
                }
                if (!marketingDocList.isEmpty()) {
                    query+= ' AND Marketing_Name_Doc__c IN (\''+String.join(marketingDocList,'\',\'')+'\')';

                    inventoryIdQuery += ' AND Inventory__r.Marketing_Name_Doc__c IN (\''
                                        +  String.join(marketingDocList,'\',\'')
                                        + '\')';
                    generalInventoryQuery += ' AND Marketing_Name_Doc__c IN (\''
                                            + String.join(marketingDocList,'\',\'')
                                            + '\')';
                }
            }

            // BEDROOMS
            if(ApexPages.currentPage().getParameters().containsKey('Bedrooms') 
                && ApexPages.currentPage().getParameters().get('Bedrooms') != ''
                && ApexPages.currentPage().getParameters().get('Bedrooms') != 'All'
            ){
                String bedroomsSelectedFromURL = ApexPages.currentPage().getParameters().get('Bedrooms');
                if(null != bedroomsSelectedFromURL){
                    List<String> bedroomsSelectedLists = bedroomsSelectedFromURL.split(',');
                    
                    if(null != bedroomsSelectedLists){
                        inventoryIdQuery += ' AND Inventory__r.IPMS_Bedrooms__c IN (\''
                                          +  String.join(bedroomsSelectedLists,'\',\'')
                                          + '\')';
                        generalInventoryQuery += ' AND IPMS_Bedrooms__c IN (\''
                                               + String.join(bedroomsSelectedLists,'\',\'')
                                               + '\')';
                    }
                }
            }

            if(ApexPages.currentPage().getParameters().containsKey('Location') 
                && ApexPages.currentPage().getParameters().get('Location') != ''
                && ApexPages.currentPage().getParameters().get('Location') != 'All'
            ){
                String locationSelectedFromURL = ApexPages.currentPage().getParameters().get('Location');
                if(null != locationSelectedFromURL){
                    List<String> locationSelectedLists = locationSelectedFromURL.split(',');
                    if(null != locationSelectedLists){
                        inventoryIdQuery += ' AND Inventory__r.Property_city__c IN (\''
                                          + String.join(locationSelectedLists,'\',\'')
                                          + '\')';
                        generalInventoryQuery += ' AND Property_city__c IN (\''
                                               + String.join(locationSelectedLists,'\',\'')
                                               + '\')';
                    }
                }
            }

            if(ApexPages.currentPage().getParameters().containsKey('Type') 
                && ApexPages.currentPage().getParameters().get('Type') != ''
                && ApexPages.currentPage().getParameters().get('Type') != 'All'
            ){
                String typeSelectedFromURL= ApexPages.currentPage().getParameters().get('Type');
                if(null != typeSelectedFromURL){
                    List<String> typeSelectedLists= typeSelectedFromURL.split(',');
                    if(null != typeSelectedLists){
                        inventoryIdQuery += ' AND Inventory__r.Unit_Type__c IN (\''
                                          + String.join(typeSelectedLists,'\',\'')
                                          + '\')';
                        generalInventoryQuery += ' AND Unit_Type__c IN (\''
                                               + String.join(typeSelectedLists,'\',\'')
                                               +'\')';
                    }
                }
            }

            if(ApexPages.currentPage().getParameters().containsKey('MinPrice') 
                && ApexPages.currentPage().getParameters().get('MinPrice') != ''
                && ApexPages.currentPage().getParameters().get('MinPrice') != 'undefined'
            ){
                String priceMin = ApexPages.currentPage().getParameters().get('MinPrice');
                inventoryIdQuery += ' AND ( Inventory__r.Selling_Price__c >=' 
                                  + priceMin 
                                  + ' OR Inventory__r.Special_Price_calc__c >= ' 
                                  + priceMin 
                                  + ') ';
                generalInventoryQuery += ' AND ( Selling_Price__c >=' 
                                       + priceMin 
                                       + ' OR Special_Price_calc__c >= ' 
                                       + priceMin 
                                       + ') ';
            }

            if(ApexPages.currentPage().getParameters().containsKey('MaxPrice') 
                && ApexPages.currentPage().getParameters().get('MaxPrice') != ''
                && ApexPages.currentPage().getParameters().get('MinPrice') != 'undefined'
            ){
                String priceMax = ApexPages.currentPage().getParameters().get('MaxPrice');
                inventoryIdQuery += ' AND ( Inventory__r.Selling_Price__c <=' 
                                  + priceMax 
                                  + ' OR Inventory__r.Special_Price_calc__c <= ' 
                                  + priceMax 
                                  + ') ';
                generalInventoryQuery += ' AND ( Selling_Price__c <=' 
                                       + priceMax 
                                       + ' OR Special_Price_calc__c <= ' 
                                       + priceMax 
                                       + ') ';
            }

             if(ApexPages.currentPage().getParameters().containsKey('PropertyStatus') 
                && ApexPages.currentPage().getParameters().get('PropertyStatus') != ''
                && ApexPages.currentPage().getParameters().get('PropertyStatus') != 'All'
            ){
                String propertyStatusSelectedFromURL = ApexPages.currentPage().getParameters().get('PropertyStatus');
                if(null != propertyStatusSelectedFromURL){
                    List<String> propertyStatusSelectedLists =propertyStatusSelectedFromURL.split(',');
                    if(null != propertyStatusSelectedLists){
                        inventoryIdQuery += ' AND Inventory__r.Property_Status__c IN (\''
                                          + String.join(propertyStatusSelectedLists,'\',\'')
                                          + '\')';
                        generalInventoryQuery += ' AND Property_Status__c IN (\''
                                               + String.join(propertyStatusSelectedLists,'\',\'')
                                               + '\')';
                    }
                }
            }


            if(ApexPages.currentPage().getParameters().containsKey('ACDDate') 
                && ApexPages.currentPage().getParameters().get('ACDDate') != ''
            ){
                String acdDateFromURL = ApexPages.currentPage().getParameters().get('ACDDate');
                if(null != acdDateFromURL){
                    List<String> acdDateLists =acdDateFromURL.split(',');
                    if(null != acdDateLists){
                        inventoryIdQuery += ' AND Inventory__r.Anticipated_Completion_Date__c IN (\''
                                          + String.join(acdDateLists,'\',\'')
                                          + '\')';
                        generalInventoryQuery += ' AND Anticipated_Completion_Date__c IN (\''
                                               + String.join(acdDateLists,'\',\'')
                                               + '\')';
                    }
                }
            }

            if(ApexPages.currentPage().getParameters().containsKey('BedroomType') 
                && ApexPages.currentPage().getParameters().get('BedroomType') != ''
            ){ 
                String bedroomTypeFromURL = ApexPages.currentPage().getParameters().get('BedroomType');
                if(null != bedroomTypeFromURL){
                    List<String> bedroomTypeLists =bedroomTypeFromURL.split(',');
                    if(null != bedroomTypeLists){
                        inventoryIdQuery += ' AND Inventory__r.Bedroom_Type__c IN (\''
                                          + String.join(bedroomTypeLists,'\',\'')
                                          + '\')';
                        generalInventoryQuery += ' AND Bedroom_Type__c IN (\''
                                               + String.join(bedroomTypeLists,'\',\'')
                                               + '\')';
                        
                    }
                }
            }

            if(ApexPages.currentPage().getParameters().containsKey('PropertyName') 
                && ApexPages.currentPage().getParameters().get('PropertyName') != ''
            ){
                String floorPkgName = ApexPages.currentPage().getParameters().get('PropertyName');
                if(null != floorPkgName){
                    List<String> floorPkgNameLists = floorPkgName.split(',');
                    if(null != floorPkgNameLists){
                        inventoryIdQuery += ' AND Inventory__r.Property_Name_2__c IN (\''
                                          + String.join(floorPkgNameLists,'\',\'')
                                          + '\')';
                        generalInventoryQuery += '  AND Property_Name_2__c IN (\''
                                               + String.join(floorPkgNameLists,'\',\'')
                                               + '\')';
                    }
                }
            }

            if(ApexPages.currentPage().getParameters().containsKey('MinSF') 
                && ApexPages.currentPage().getParameters().get('MinSF') != ''
                && ApexPages.currentPage().getParameters().get('MinSF') != 'undefined'
            ){
                String minSF = ApexPages.currentPage().getParameters().get('MinSF');
                inventoryIdQuery += ' AND Inventory__r.Area_Sqft_2__c >= '
                                  + minSF;
                generalInventoryQuery += '  AND Area_Sqft_2__c >= '
                                       + minSF;
            }

            if(ApexPages.currentPage().getParameters().containsKey('MaxSF') 
                && ApexPages.currentPage().getParameters().get('MaxSF') != ''
                && ApexPages.currentPage().getParameters().get('MaxSF') != 'undefined'
            ){
                String maxSF = ApexPages.currentPage().getParameters().get('MaxSF');
                inventoryIdQuery += ' AND Inventory__r.Area_Sqft_2__c <= '
                                  + maxSF;
                generalInventoryQuery += '  AND Area_Sqft_2__c <= '
                                       + maxSF;
            }

            system.debug('inventoryIdQuery>>>' + inventoryIdQuery);
            system.debug('AVAILABILITYgeneralInventoryQuery>>>' + generalInventoryQuery);

            Set<Id> InventoryIDs = UtilityQueryManager.getInventoryIDs(inventoryIdQuery);
            
            Set<Id> generalInventoryIds = (new Map<Id, SObject>(Database.query(generalInventoryQuery)).keySet());
            //Set<Id> generalInventoryIds= UtilityQueryManager.getAllGeneralInventories(generalInventoryQuery);        
            
            system.debug('**** generalInventoryId '+generalInventoryIds );        
            
            InventoryIDs.addAll(generalInventoryIds);
            system.debug('**** InventoryIDs '+InventoryIDs );  
            query+= ' and Id in : InventoryIDs';
        }

        mpProperties = new Map<String,set<String>>();
        mpAvailable = new Map<String, list<Inventory__c>>();
        mpFinal = new Map<string, Map<string, List<Inventory__c>>>();
        mpBuildings = new Map<String,Location__c>();
        mpPaymentTerms = new Map<Id, List<Payment_Terms__c>>();
        mpPaymentPlans = new Map<String, List<Payment_Plan__c>>();
        planKeyStr = '';
        
        List<Schema.FieldsetMember> lstMembers = SObjectType.Inventory__c.FieldSets.Project_Unit.getFields();
        integer width= Math.round(100/(lstMembers.size()-1));
        widthpx = width+'%';
        
        lstMembers = SObjectType.Payment_Terms__c.FieldSets.PaymentTerm.getFields();
        width= Math.round(100/lstMembers.size());
        ptWidthPx = width+'%';
        
        system.debug('**** query '+ query );   
        for(Inventory__c inv: database.query(query)){
             if(!mpAvailable.containsKey(inv.Building_ID__c)){
                mpAvailable.put(inv.Building_ID__c, new List<Inventory__c>{inv});
             }
             else{
                List<Inventory__c> existing = mpAvailable.get(inv.Building_ID__c);
                existing.add(inv);
                mpAvailable.put(inv.Building_ID__c,existing);
             }
        }
        
        for(Location__c loc: [Select id, location_Id__c, Construction_status__c, status__c, Building_name__c from Location__c where Location_Id__c in: mpAvailable.keyset()]){
            mpBuildings.put(loc.location_Id__c, loc);
        }
        
        set<string> buildingIds = new set<string>();
        set<Id> PaymentPlanIds = new set<Id>();
        date currDate = system.today();
        for(Payment_Plan__c pp: [Select id,Building_ID__c from Payment_Plan__c where Building_ID__c in: mpBuildings.keyset() and Effective_From__c<=:currDate and Effective_To_calculated__c>=:currDate]){
            PaymentPlanIds.add(pp.Id);
            buildingIds.add(pp.Building_ID__c);
            if(!mpPaymentPlans.containsKey(pp.Building_ID__c))
                mpPaymentPlans.put(pp.Building_ID__c, new List<Payment_Plan__c>{pp});
            else{
                List<Payment_Plan__c> existing = mpPaymentPlans.get(pp.Building_ID__c);
                existing.add(pp);
                mpPaymentPlans.put(pp.Building_ID__c, existing); 
            }
        }
        
        for(string bId : buildingIds){
            planKeyStr+=bId+';';
        }
        
        set<String> paymentIds = new set<String>();
        string ppQuery = DamacUtility.getCreatableFieldsSOQL('Payment_Terms__c');
        ppQuery+=' where Payment_Plan__c in: PaymentPlanIds order by line_ID__c';
        for(Payment_Terms__c pt: database.query(ppQuery)){
            paymentIds.add(pt.Payment_Plan__c);
            if(!mpPaymentTerms.containsKey(pt.Payment_Plan__c)){
                mpPaymentTerms.put(pt.Payment_Plan__c, new list<Payment_Terms__c>{pt});
            }
            else{
                List<Payment_Terms__c> existing = mpPaymentTerms.get(pt.Payment_Plan__c);
                existing.add(pt);
                mpPaymentTerms.put(pt.Payment_Plan__c, existing);
            }
            
        }
        system.debug(mpProperties);
        system.debug(mpAvailable);
        system.debug(mpFinal);
        system.debug(mpBuildings);
        system.debug(mpPaymentTerms);
        system.debug(mpPaymentPlans);

        for(string pId : paymentIds){
            pTermKeyStr+=pId+';';
        }
    }
    
}