public without sharing class MortgageFlagUpdateInIPMS {
     // public static Id AddressCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Welcome Calling List').RecordTypeId;
    public static Id mortgageCaseRecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Mortgage').RecordTypeId;

    @InvocableMethod
    public static void updateMortgageFlag( List<Case> lstCase ) {
        system.debug('in  updateMortgageFlag: '+lstCase);
        Set<String> addCasesIds = new Set<String>();
       
        
        for( case objCaseList : lstCase ) {
            //system.debug(objCaseList.RecordTypeId + ' mortgageCaseRecordTypeId : '+AddressCallingRecordTypeId);
            if( objCaseList.RecordTypeId == mortgageCaseRecordTypeId ) {
                 system.debug('In Address If');
                addCasesIds.add( objCaseList.Id );
            }
        }
        if( !addCasesIds.isEmpty() ) {
            getmortgageFlagUpdate(addCasesIds);
        }
        
    }
  @Future(callout=true)   
 public static void getmortgageFlagUpdate( Set<String> caseIds ) {
        system.debug('caseLst : '+caseIds);
        
        List<Case> caseLst = [SELECT Id, Booking_Unit__c,
                                     Booking_Unit__r.Mortgage_Applied__c,Booking_Unit__r.Legal_Remarks__c,
                                     Mortgage_Bank_Name__c,Mortgage_Amount__c,Mortgage_Status__c,Booking_Unit__r.General_Remarks__c,
                                     Booking_Unit__r.Id,Booking_Unit__r.Property_Name__c,Booking_Unit__r.Registration_ID__c,
                                     Account.Id,Owner.Name,
                                     Booking_Unit__r.Mortgage__c,CreatedDate
                                FROM Case WHERE Id IN :caseIds ];
        
        MortgageFlagactionCom.ProcessHttpSoap11Endpoint calloutObj = new MortgageFlagactionCom.ProcessHttpSoap11Endpoint();
        calloutObj.timeout_x = 120000;
        Data resObj = new Data();

        try{
            system.debug('in try');
            List<MortgageFlagService.APPSXXDC_PROCESS_SERX1794747X1X5> regTerms = new List<MortgageFlagService.APPSXXDC_PROCESS_SERX1794747X1X5>();
            MortgageFlagService.APPSXXDC_PROCESS_SERX1794747X1X5 objTerms = new MortgageFlagService.APPSXXDC_PROCESS_SERX1794747X1X5();
            //if( callingList[0].Customer_Eligible_For_Mortgage__c == true ) {
                objTerms.ATTRIBUTE1     = '';
          /*  }else{
                objTerms.ATTRIBUTE1     = 'N';
            }*/
            
            
            objTerms.ATTRIBUTE10 = '';
            
            Datetime dt = caseLst[0].CreatedDate;
            objTerms.ATTRIBUTE11 = String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase());

            
            objTerms.ATTRIBUTE12 = '';
            objTerms.ATTRIBUTE13 = '';
            objTerms.ATTRIBUTE14 = caseLst[0].Mortgage_Status__c;
            objTerms.ATTRIBUTE15 = '';
            objTerms.ATTRIBUTE16 = '';
            objTerms.ATTRIBUTE17 = '';
            objTerms.ATTRIBUTE18 = caseLst[0].Booking_Unit__r.Legal_Remarks__c;
            //objTerms.ATTRIBUTE19 = caseLst[0].OwnerId.Manager.Name;
            //Booking_Unit__r.Property_Name__c;
            objTerms.ATTRIBUTE2     = '';
            objTerms.ATTRIBUTE20 = '';
            objTerms.ATTRIBUTE21 = '';
            objTerms.ATTRIBUTE22 = '';
            objTerms.ATTRIBUTE23 = '';
            objTerms.ATTRIBUTE24 = '';
            objTerms.ATTRIBUTE25 = '';
            objTerms.ATTRIBUTE26 = '';
            objTerms.ATTRIBUTE27 = '';
            objTerms.ATTRIBUTE28 = '';
            objTerms.ATTRIBUTE29 = '';
            objTerms.ATTRIBUTE3     = '';
            objTerms.ATTRIBUTE30 = '';
            objTerms.ATTRIBUTE31 = '';
            objTerms.ATTRIBUTE32 = '';
            objTerms.ATTRIBUTE33 = '';
            objTerms.ATTRIBUTE34 = '';
            objTerms.ATTRIBUTE35 = '';
            objTerms.ATTRIBUTE36 = '';
            objTerms.ATTRIBUTE37 = '';
            objTerms.ATTRIBUTE38 = '';
            objTerms.ATTRIBUTE39 = '';
            objTerms.ATTRIBUTE4     = '';
            objTerms.ATTRIBUTE41 = '';
            objTerms.ATTRIBUTE42 = '';
            objTerms.ATTRIBUTE43 = '';
            objTerms.ATTRIBUTE44 = '';
            objTerms.ATTRIBUTE45 = '';
            objTerms.ATTRIBUTE46 = '';
            objTerms.ATTRIBUTE47 = '';
            objTerms.ATTRIBUTE48 = '';
            objTerms.ATTRIBUTE49 = '';
            objTerms.ATTRIBUTE5     = caseLst[0].Mortgage_Bank_Name__c;
            objTerms.ATTRIBUTE50 = '';
            objTerms.ATTRIBUTE6     = '';
            objTerms.ATTRIBUTE7     = String.valueOf( caseLst[0].Mortgage_Amount__c);
            objTerms.ATTRIBUTE8     = '';
            objTerms.ATTRIBUTE9     = '';
            objTerms.PARAM_ID   = caseLst[0].Booking_Unit__r.Registration_ID__c;
            regTerms.add(objTerms);
            system.debug( 'regTerms' + regTerms);
            
            String response = calloutObj.updateMortgage( '2-'+String.valueOf( Datetime.now().getTime())
                                                                  , 'UPDATE_MORTGAGE' ,'SFDC' ,regTerms);
            system.debug('== response  =='+response );
            resObj = (Data)JSON.deserialize(response, MortgageFlagUpdateInIPMS.Data.class);
            system.debug('resObj response === '+ resObj);
            if( resObj.data[0].PROC_STATUS.equalsIgnoreCase('S')) {
            //if( resObj.status.equalsIgnoreCase('S')) {
                //caseLst[0].Booking_Unit__r.Mortgage__c = true;
                caseLst[0].Mortgage_Status__c = 'Mortgage Original lien Letter';
                
                Booking_Unit__c objBu = new Booking_Unit__c();
                objBu.Id = caseLst[0].Booking_Unit__c;
                objBu.Mortgage__c = true;
                update objBu;
                
            }else {
                caseLst[0].Booking_Unit__r.Mortgage__c = false;
                Error_Log__c errorInst = new Error_Log__c();
                errorInst.Error_Details__c = resObj.message;
                errorInst.Booking_Unit__c = caseLst[0].Booking_Unit__r.Id;
                if(caseLst[0].Account != null) {
                    errorInst.Account__c = caseLst[0].Account.Id;
                }
                if(caseLst[0].Id != null) {
                    errorInst.Case__c = caseLst[0].Id;
                }
                errorInst.Process_Name__c = 'Mortgage';
                insert errorInst;               
            }
            update caseLst;
                
        } catch ( Exception e ){
            Error_Log__c errorInst = new Error_Log__c();
            errorInst.Error_Details__c = 'No Data Found';
            errorInst.Booking_Unit__c = caseLst[0].Booking_Unit__r.Id;
            if(caseLst[0].Account != null){
                errorInst.Account__c = caseLst[0].Account.Id;
            }
            if(caseLst[0].Id != null){
                errorInst.Case__c = caseLst[0].Id;
            }
            errorInst.Process_Name__c = 'Mortgage';
            insert errorInst;
            
            
            resObj.status = 'Exception';
            resObj.message = String.valueOf( e ) ;
        }
        
    }
      public class Data {
        public List<CaseFlagUpdateResponse> data                {get;set;}
        public String message                                   {get;set;}
        public String status                                    {get;set;}
    }
    public class CaseFlagUpdateResponse {
        public String PROC_STATUS                               {get;set;}
        public String PROC_MESSAGE                              {get;set;}
        public String PARAM_ID                                  {get;set;}
        public String Message_ID                                {get;set;}
    }

}