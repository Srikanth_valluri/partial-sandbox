public class AppointmentSchedulingBtnController{
    public Calling_List__c objCalling {get;set;}
    
    public static Id appointmentRecTypeID = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Appointment Scheduling').RecordTypeId;
    
    public AppointmentSchedulingBtnController(ApexPages.StandardController stdController) {
        objCalling = new Calling_List__c();
        this.objCalling = (Calling_List__c)stdController.getRecord();
    }
    
    public AppointmentSchedulingBtnController(){}
    
    public pageReference updateCallingList(){
        try{
            //List<Group> lstGroup = new List<Group>();
            //lstGroup = [Select Id,name,DeveloperName from Group where DeveloperName = 'Appointment_Queue' and Type = 'Queue'];
            list<Calling_List__c> lstCallingList = new list<Calling_List__c>();     
            objCalling = [Select id,Name,OwnerID, Account__c,Appointment_Date__c,
                         Account__r.Name,Account__r.Email__c,Account__r.PersonEmail,
                         Appointment_Slot__c,Account_Name_for_Walk_In__c, RecordTypeId,
                         RecordType.DeveloperName, Assigned_CRE__c,Service_Type__c, 
                         Assigned_CRE__r.id, CRE_Email__c, Account_Email__c, Customer_Name__c,
                         Handover_Team_Email__c, HO_Calling_list_Owner__c,
                         Appointment_Status__c, Sub_Purpose__c,Booking_Unit_Name__c,Booking_Unit__r.Property_Name__c,
                         Account__r.IsPersonAccount,Account__r.PersonMobilePhone,Account__r.Mobile_Phone_Encrypt__pc,
                         Account__r.Mobile__c,Virtual_Viewing__c, Virtual_Meeting_Link__c,
                         (Select id,StartDateTime,EndDateTime,Subject,status__c,description,OwnerId
                         from Events where status__c = 'Requested')
                         from Calling_List__c where id=: objCalling.id ];
            System.debug('objCalling-->' +objCalling);
            System.debug('objCalling Service_Type__c -->' +objCalling.Service_Type__c );
            System.debug('objCalling Sub_Purpose__c-->' +objCalling.Sub_Purpose__c);
            System.debug('objCalling Virtual_Viewing__c-->' +objCalling.Virtual_Viewing__c);
            System.debug('objCalling Virtual_Meeting_Link__c-->' +objCalling.Virtual_Meeting_Link__c);
            if(objCalling.Service_Type__c  == 'Handover' && objCalling.Sub_Purpose__c == 'Unit Viewing' 
                && objCalling.Virtual_Viewing__c == true && String.isBlank(objCalling.Virtual_Meeting_Link__c)){
                System.debug('Virtual Handover');
                ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.Warning,'Virtual Teams Meeting not created. Please create one to proceed ahead.'));     
                return null;
            }else{
                if(objCalling.Events.Size()>0){
                    if (string.isBlank(objCalling.CRE_Email__c)) {
                        /*objCalling.Events[0].status__c = 'Accepted';
                        
                        objCalling.Appointment_Status__c = 'Accepted';
                        update objCalling.Events[0];
                        update objCalling;*/
                        ApexPages.addmessage(new ApexPages.message(
                        ApexPages.severity.Warning,'CRE is not assigned for Appointment, request you to assign CRE.'));     
                        return null;
                    } else {
                        objCalling.Events[0].status__c = 'Confirmed';
                        objCalling.Events[0].OwnerId = objCalling.Assigned_CRE__c;
                        
                        objCalling.Appointment_Status__c = 'Confirmed';
                        try {
                            update objCalling.Events[0];
                            update objCalling; 
                        } catch (Exception e) {
                            system.debug('!!!!except'+e);
                        }
                        lstCallingList.add(objCalling);
                        if( objCalling.RecordTypeId == appointmentRecTypeID ) {
                            /*if (objCalling.Sub_Purpose__c == 'Key Handover' || objCalling.Sub_Purpose__c == 'Unit Viewing' ||
                || objCalling.Sub_Purpose__c == 'Unit Viewing or Key Handover'  || objCalling.Sub_Purpose__c == 'Documentation'  ) {*/
                                sendEmail(lstCallingList);
                            //}
                        }
                        
                    }      
                    
                    
                     pageReference pgr = new pageReference('/'+objCalling.id);
                     pgr.setRedirect(true);
                     return pgr;
                }
                else{
                     ApexPages.addmessage(new ApexPages.message(
                     ApexPages.severity.Warning,'This calling list does not have requested event to accept')); 
                }
                
                /*if(lstGroup.Size()>0){
                    if(objCalling.Assigned_CRE__c == null 
                      && lstGroup[0].id == objCalling.OwnerID 
                      && objCalling.RecordType.DeveloperName == 'Appointment_Scheduling' ){
                     
                     //objCalling.Assigned_CRE__c = UserInfo.getUserId();
                     //update objCalling;
                    
                    String startTime = objCalling.Appointment_Slot__c.substring(0,2);
                    
                    Datetime StartDT = datetime.newInstance(objCalling.Appointment_Date__c.year(), objCalling.Appointment_Date__c.month(),objCalling.Appointment_Date__c.day(),
                                                    integer.Valueof(startTime)  , 00, 00);
                    Datetime EndDT = datetime.newInstance(objCalling.Appointment_Date__c.year(), objCalling.Appointment_Date__c.month(),objCalling.Appointment_Date__c.day(),
                                                    (integer.Valueof(startTime) + 1)  , 00, 00);                                
                    
                    System.debug('----StartDT -------'+StartDT );
                    System.debug('----EndDT -------'+EndDT );
                     
                    Event objEvent = new Event();
                    objEvent.Type = 'Other';               
                    objEvent.OwnerId = UserInfo.getUserId();
                    objEvent.WhatId = objCalling.id;
                    objEvent.StartDateTime = StartDT ;
                    objEvent.EndDateTime =  EndDT ;
                    objEvent.Subject = 'Appointment Scheduled with Customer:'+objCalling.Account__r.Name;
                    objEvent.Description= 'Appointment Scheduled with Customer:'+objCalling.Account__r.Name;
                    insert objEvent ;
                    
                    List<Messaging.SingleEmailMessage> lstEmails = new List<Messaging.SingleEmailMessage>();
                   
                    List<String> lstToaddress = new List<String>();
                    lstToaddress.add(objCalling.Account__r.Email__c);
                    
                    if(lstToaddress.Size()>0){                   
                        
                        String body = 'Hello '+objCalling.Account_Name_for_Walk_In__c+',';
                               body += ' Your appointment for process '+objCalling.Service_Type__c+' on '+objCalling.Appointment_Date__c.format()+' has been confirmed';
                               body += ' at '+objCalling.Appointment_Slot__c+' (24hr format)';
                               body += ' Thanks,';
                               body += ' Damac Group';                      
                                              
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();                    
                        mail.setSubject('Appointment Confirmation for '+objCalling.Service_Type__c+' Process' );
                        mail.setHtmlBody(body);
                        mail.setToAddresses(lstToaddress);
                        mail.setSenderDisplayName('Damac Group');                                       
                        lstEmails.add(mail);
                        Messaging.sendEmail(lstEmails);
                    } 
                     pageReference pgr = new pageReference('/'+objCalling.id);
                     pgr.setRedirect(true);
                     return pgr;
                    }
                    else{
                        ApexPages.addmessage(new ApexPages.message(
                        ApexPages.severity.Warning,'This record already assigned to UserID: '+objCalling.Assigned_CRE__r.id)); 
                    }
                }
                else{
                    ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Warning,'Appointment_Queue not Found')); 
                }
                */
                
                return null;
        
            }
            }
            
        catch(exception ex){
            ApexPages.addmessage(new ApexPages.message(
            ApexPages.severity.Error,ex.getMessage()+'----'+ex.getLineNumber()));
            return null;
        } 

    }
    
    public pageReference back(){
        pageReference pgr = new pageReference('/'+objCalling.id);
        pgr.setRedirect(true);
        return pgr;
    }
    
     public void sendEmail(List<Calling_List__c> lstAppointment) {
        List<Messaging.SingleEmailMessage> messages =   new List<Messaging.SingleEmailMessage>();
        EmailTemplate reqEmailTemplate =  [SELECT ID, Subject, Body, BrandTemplateId, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Handover_Appointment_Confirmation' limit 1];
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
        for (Calling_List__c objCL : lstAppointment) {
            if (objCL != null && objCL.Service_Type__c == 'Handover'  &&  objCL.Sub_Purpose__c == 'Unit Viewing' && objCL.Appointment_Status__c == 'Confirmed') {
                GenericEmailUtility objGenericEmailUtility = new GenericEmailUtility();
                String strMessageBody='Your appointment for <Purpose> at <Project Name>, <Unit Number>, on <dd/mm/yyyy> at <hh:mm> (UAE time) is Confirmed. A confirmation email has been sent to your registered e-mail address. For assistance call +971 4 2375000 or write to atyourservice@damacproperties.com';
                objGenericEmailUtility.sendEmail(new List<Calling_List__c> {objCL}, 'Unit_Viewing_Appointments_Confirmed');
                objGenericEmailUtility.sendMessage(new List<Calling_List__c> {objCL}, strMessageBody);
            } else if (objCL != null && objCL.Service_Type__c == 'Handover'  &&  objCL.Sub_Purpose__c == 'Key Handover' && objCL.Appointment_Status__c == 'Confirmed') {
                GenericEmailUtility objGenericEmailUtility = new GenericEmailUtility();
                String strMessageBody='Your appointment for <Purpose> at <Project Name>, <Unit Number>, on <dd/mm/yyyy> at <hh:mm> (UAE time) is Confirmed. A confirmation email has been sent to your registered e-mail address. For assistance call +971 4 2375000 or write to atyourservice@damacproperties.com';
                objGenericEmailUtility.sendEmail(new List<Calling_List__c> {objCL}, 'Key_Handover_Appointments_Confirmed');
                objGenericEmailUtility.sendMessage(new List<Calling_List__c> {objCL}, strMessageBody);
            } else {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setWhatId(objCL.Id);
                mail.setSaveAsActivity(true);
                //mail.setTemplateId(reqEmailTemplate.BrandTemplateId);
                list<String> bccAddress = new list<String>();
                List<String> sendTo = new List<String>();
                String body, sub;
                if (reqEmailTemplate != null) {
                    body = reqEmailTemplate.HtmlValue;
                    sub = reqEmailTemplate.Subject;
                    if (objCL.Customer_Name__c!= null) {
                        body = body.replace('{!Calling_List__c.Customer_Name__c}', objCL.Customer_Name__c);
                    } else {
                        body = body.replace('{!Calling_List__c.Customer_Name__c}', '');
                    }
                    if (objCL.Sub_Purpose__c!= null ) {
                        if( objCL.Sub_Purpose__c == 'Unit Viewing' || objCL.Sub_Purpose__c == 'Key Handover' || objCL.Sub_Purpose__c == 'Unit Viewing or Key Handover') {
                            String strBody = objCL.Sub_Purpose__c + ' at '+objCL.Booking_Unit__r.Property_Name__c+'-Handover Office';
                            body = body.replace('{!Calling_List__c.Sub_Purpose__c}', strBody);
                            //body = body + 'at {'+objCL.Booking_Unit__r.Property_Name__c+'-Handover Office}';
                        } else if( objCL.Sub_Purpose__c == 'Documentation'  ) {
                            String strBody = objCL.Sub_Purpose__c + ' at Executive Heights – Barsha Heights';
                            body = body.replace('{!Calling_List__c.Sub_Purpose__c}', strBody);
                            //body = body + 'at {Executive Heights – Barsha Heights}';
                        }
                    } else {
                        body = body.replace('{!Calling_List__c.Sub_Purpose__c}', '');
                    }
                    
                    if (objCL.Booking_Unit_Name__c != null) {
                        body = body.replace('{!Calling_List__c.Booking_Unit_Name__c}', objCL.Booking_Unit_Name__c);
                    } else {
                        body = body.replace('{!Calling_List__c.Booking_Unit_Name__c}', '');
                    }
                    if (objCL.Appointment_Date__c != null) {
                        body = body.replace('{!Calling_List__c.Appointment_Date__c}', String.valueOf(objCL.Appointment_Date__c));
                    } else {
                        body = body.replace('{!Calling_List__c.Appointment_Date__c}', '');
                    }
                    if (objCL.Appointment_Slot__c != null) {
                        body = body.replace('{!Calling_List__c.Appointment_Slot__c}', objCL.Appointment_Slot__c);
                    } else {
                        body = body.replace('{!Calling_List__c.Appointment_Slot__c}', '');
                    }
                    
                    if (objCL.Name != null) {
                        body = body.replace('{!Calling_List__c.Name}', '<b><u>'+objCL.Name+'</u></b>');
                    } else {
                        body = body.replace('{!Calling_List__c.Name}', '');
                    }
                    /*
                    if (objCL.Appointment_Status__c != null) {
                        body = body.replace('{!Calling_List__c.Appointment_Status__c}', objCL.Appointment_Status__c);
                    } else {
                        body = body.replace('{!Calling_List__c.Appointment_Status__c}', '');
                    }
                    if (objCL.Service_Type__c != null) {
                        body = body.replace('{!Calling_List__c.Service_Type__c}', objCL.Service_Type__c);
                    } else {
                        body = body.replace('{!Calling_List__c.Service_Type__c}', '');
                    }*/


                    body = body.replace(']]>', '');                
                    if (objCL.Service_Type__c != null) {
                        sub = sub.replace('{!Calling_List__c.Service_Type__c}', objCL.Service_Type__c);
                    } else {
                        sub = sub.replace('{!Calling_List__c.Service_Type__c}', '');
                    }
                    if (objCL.Appointment_Status__c != null) {
                        sub = sub.replace('{!Calling_List__c.Appointment_Status__c}', objCL.Appointment_Status__c);
                    } else {
                        sub = sub.replace('{!Calling_List__c.Appointment_Status__c}', '');
                    }
                    mail.setHtmlBody(body);
                    mail.setSubject(sub);
                }
                if ((objCL.Sub_Purpose__c == 'Key Handover' || objCL.Sub_Purpose__c == 'Unit Viewing' || objCL.Sub_Purpose__c == 'Unit Viewing or Key Handover') &&
                    objCL.Handover_Team_Email__c != null) {
                    bccAddress.add(objCL.Handover_Team_Email__c);
                }
                if (objCL.HO_Calling_list_Owner__c != null) {
                    bccAddress.add(objCL.HO_Calling_list_Owner__c);
                }
                if (objCL.CRE_Email__c != null) {
                    bccAddress.add(objCL.CRE_Email__c);
                }
                if (bccAddress != null) {
                    mail.setBccAddresses(bccAddress);
                }
                sendTo.add(objCL.Account_Email__c);
                mail.setToAddresses(sendTo);            
                messages.add(mail);
                
                if ( owea.size() > 0 ) {
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            }
        }
    }
    
}