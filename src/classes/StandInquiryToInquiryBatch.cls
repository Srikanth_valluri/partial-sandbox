/****************************************************************************************************
* Class Name: StandInquiryToInquiryBatch
* Descritpion: Batch class to create Inquiry records from Stand Inquiry records based on Meeting Type
* Test Class: StandInquiryToInquiryBatch_Test
*
* 1.0    08-Jan-2020    QBurst    Created the class
******************************************************************************************************/
global class StandInquiryToInquiryBatch implements Database.Batchable <sObject>, Database.Stateful { 
    global Map<String, String> inquiryFieldMappings;
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        inquiryFieldMappings = new Map<String, String>();
        Set<string> standInquiryFields = new Set<string>();
        for(Stand_Inquiry_to_Inquiry_Mapping__mdt mapping: [SELECT Inquiry_Field_API_Name__c, 
                                                                    Stand_Inquiry_API_Name__c 
                                                            FROM Stand_Inquiry_to_Inquiry_Mapping__mdt]){
            inquiryFieldMappings.put(mapping.Inquiry_Field_API_Name__c, mapping.Stand_Inquiry_API_Name__c);
            standInquiryFields.add(mapping.Stand_Inquiry_API_Name__c);
        }
        String query = 'SELECT ';
        
          for(String field: standInquiryFields){
              query += field + ', ';
          }     
          query += 'LastModifiedDate FROM Stand_Inquiry__c '
                + 'WHERE  (Meeting_Type__c = \'Direct Tour\') '
              //  + 'AND First_Name__c = \'Test on Jan07-\''
                + 'AND  Id NOT IN (SELECT Stand_Inquiry__c from Inquiry__c WHERE Stand_Inquiry__c != NULL)'
                + 'AND createddate > 2020-01-07T12:29:32.000Z '
                + 'LIMIT 100';
        System.debug('Query----'+query); 
        system.debug('inquiryFieldMappings1: ' + inquiryFieldMappings);
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Stand_Inquiry__c> scope) {
      //  try{
            List<Inquiry__c> inquiriesToInsert = new List<Inquiry__c>();
            system.debug('inquiryFieldMappings2: ' + inquiryFieldMappings);
            system.debug('standInquiries: ' + scope);
            for(Stand_Inquiry__c standInq: scope){
                Inquiry__c inquiry = new Inquiry__c();
                inquiry.Inquiry_Source__c = 'Stands';   
                for (String field : inquiryFieldMappings.keyset()) {
                    if(inquiryFieldMappings.get(field) != null 
                                    && standInq.get(inquiryFieldMappings.get(field)) != null){
                        inquiry.put(field, standInq.get(inquiryFieldMappings.get(field)));
                    }
                }
                system.debug('new inquiry: ' + inquiry);
                inquiriesToInsert.add(inquiry);
            }
            if(inquiriesToInsert.size() > 0){
                insert inquiriesToInsert;
            }
    
      /*  }
        catch(exception ex){
            system.debug('Exception e: ' + ex);
        } */
    }
   
   global void finish(Database.BatchableContext BC) { 
       //Below code will fetch the job Id 
       AsyncApexJob a = [Select a.TotalJobItems, a.Status, a.NumberOfErrors, a.JobType, 
                               a.JobItemsProcessed, a.ExtendedStatus, a.CreatedById, a.CompletedDate 
                         From AsyncApexJob a WHERE id = :BC.getJobId()];//get the job Id 
        if(a.Status == 'Completed') { 
            List<CronTrigger> scheduledJobs = [Select id from cronTrigger where CronJobDetail.name = 'StandInquiryToInquiryBatch Scheduler']; 
            if(scheduledJobs.isEmpty()) { 
                System.scheduleBatch(new StandInquiryToInquiryBatch(),'StandInquiryToInquiryBatch Scheduler', 2, 5); 
            } 
        } 
    }
}