@isTest
public class DAMAC_SMS_Job_Status_RespTest {
    public static testMethod void method1() {

        String json=        '{'+
        '    "Status": "OK",'+
        '    "Data": {'+
        '        "BulkId": 3032707,'+
        '        "BulkStatus": "Finished",'+
        '        "DateAdded": "2017-11-06T10:30:14.36",'+
        '        "DateToStart": "2017-11-06T10:31:00",'+
        '        "IndividualResponses": ['+
        '            {'+
        '                "countryId": 203,'+
        '                "creditsUsed": "0.055000",'+
        '                "dlrStatus": "Delivered",'+
        '                "messageLength": 1,'+
        '                "mobileNo": "971000000000",'+
        '                "msgId": 527743259'+
        '            }'+
        '        ],'+
        '        "Message": "Hi This is a Test SMS 00",'+
        '        "ReferenceName": "DAMAC",'+
        '        "SenderName": "DAMAC",'+
        '        "TotalNumbers": 1,'+
        '        "TotalProcessed": 1,'+
        '        "TotalValid": 1'+
        '    }'+
        '}';
        DAMAC_SMS_Job_Status_Resp obj = DAMAC_SMS_Job_Status_Resp.parse(json);
        System.assert(obj != null);
    }
}