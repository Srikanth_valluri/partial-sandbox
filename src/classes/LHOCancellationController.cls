public without sharing class LHOCancellationController {

    String caseId;
    public LHOCancellationController(ApexPages.StandardController sc){
        caseId = sc.getId();
    }
    
    public pagereference cancelLHO() {
        system.debug('cancel case called');
        Savepoint sp;
         try {
             if(String.isNotBlank(caseId)) {
                 Case objCase = [ SELECT
                         Id,Status,Approval_Status__c
                        ,Offer_Acceptance_Letter_Generated__c
                        ,O_A_Signed_Copy_Uploaded__c
                        ,Is_New_Payment_Terms_Applied__c
                        , Booking_Unit__c 
                        FROM Case WHERE Id =:caseId
                       ];

                if( String.isNotBlank(objCase.Status) && objCase.Status.equalsIgnoreCase('submitted'))
                {
                    sp = Database.setSavepoint();
                    
                     List<Task> objTaskList = [ SELECT 
                                           Id,Subject,Status, WhatId
                                           FROM Task
                                           WHERE Status != 'Completed' and WhatId =:objCase.Id
                                         ];
                    if(objTaskList == null || objTaskList.size() <= 0)
                    {
                      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error :No Open Task Found with status Update LHO Details in IPMS.'));
                      return null;
                    } else  if(objTaskList != null && objTaskList.size() > 0) {
                        Task objTask = new Task(Id = objTaskList[0].Id);
                        objTask.Status = 'Closed';
                        objTask.Pushed_to_IPMS__c = false;
                        objTask.Description = 'Cancellation initiated by CRE';
                        update objTask;
                    }
                } else
                {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error :You cannot cancel this LHO SR as it is not Submitted.'));
                    return null;
                }
                objCase.Status = 'Cancelled';
                update objCase;
    
                PageReference  pg = new PageReference ('/'+caseId);
                pg.setRedirect(true);
                return pg;
             } 
             else
             {
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error :No case selected, Select a Case first.'));
                  return null;
             }            
        }
        catch(Exception exp)
        { 
          Database.rollback(sp);
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please contact your system adminstrator '+exp.getMessage()));
          return null; 
        }
        //return null;
    }
}