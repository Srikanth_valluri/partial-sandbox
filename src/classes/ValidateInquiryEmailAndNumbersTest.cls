@isTest(SeeAlldata=false) 
public class ValidateInquiryEmailAndNumbersTest {
    
    public static testmethod void ValidateInquiryEmailAndNumbersTest1() {
        
        
        Verify_Inquiry_Contact_Details__c csRecord1 = new Verify_Inquiry_Contact_Details__c(
            Name = 'InquiryInfo',
            Email_API__c = 'Email__c',
            Mobile_Country__c = 'Mobile_Country__c',
            Mobile_Phone_API__c = 'Mobile_Phone__c',
            Phone_Number_Type__c = 'Phone_Number_Type__c',
            Ported__c = 'Ported__c',
            Reachable__c = 'Reachable__c',
            Roaming__c = 'Roaming__c',
            Status__c = 'BVEmailStatus__c',
            Sub_Status__c = 'BVSecondaryStatus__c',
            Validity__c = 'Validity__c',
            Disposable__c = 'Disposable__c',
            Toxic__c = 'Toxic__c' 
        );
        insert csRecord1;
        Test.setMock(HttpCalloutMock.class, new ValidateEmailMockTest());       
        Inquiry__c inqRec1 = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT, 1); 
        inqRec1.Email__c = 'craig.lobo@eternussolutions.com';
        inqRec1.BVEmailStatus__c = '';
        System.debug('>>>>' + inqRec1);
        insert inqRec1;
        System.Test.StartTest();
        Set<Id> inqIdSet = new Set<Id>();
        inqIdSet.add(inqRec1.Id);
        ValidateInquiryEmailAndNumbers.verifyInqEmailAndNumbersFromWebService(inqIdSet);
        System.Test.StopTest();

    }

    public static testmethod void ValidateInquiryEmailAndNumbersTest2() {
        

        Verify_Inquiry_Contact_Details__c csRecord2 = new Verify_Inquiry_Contact_Details__c(
            Name = 'InquiryInfo',
            Email_API__c = 'Email__c',
            Mobile_Country__c = 'Mobile_Country__c',
            Mobile_Phone_API__c = 'Mobile_Phone__c',
            Phone_Number_Type__c = 'Phone_Number_Type__c',
            Ported__c = 'Ported__c',
            Reachable__c = 'Reachable__c',
            Roaming__c = 'Roaming__c',
            Status__c = 'BVEmailStatus__c',
            Sub_Status__c = 'BVSecondaryStatus__c',
            Validity__c = 'Validity__c',
            Disposable__c = 'Disposable__c',
            Toxic__c = 'Toxic__c' 
        ); 
        insert csRecord2;
        Test.setMock(HttpCalloutMock.class, new ValidateNumberMockTest());        
        Inquiry__c inqRec2 = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT, 1);  
        inqRec2.Mobile_Phone_Encrypt__c = '70157 74814';
        inqRec2.Mobile_CountryCode__c = 'India: 0091';
        inqRec2.Validity__c = '';   
        insert inqRec2;
        System.Test.StartTest();
        Set<Id> inqIdSet = new Set<Id>();
        inqIdSet.add(inqRec2.Id);
        ValidateInquiryEmailAndNumbers.verifyInqEmailAndNumbersFromWebService(inqIdSet);

        System.Test.StopTest();

    }

    public static testmethod void ValidateInquiryEmailAndNumbersTest3() {

        Inquiry__c inqRec2 = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT, 1);
        inqRec2.Mobile_Phone_Encrypt__c = '70157 74814';
        inqRec2.Mobile_CountryCode__c = 'India: 0091';
        inqRec2.Validity__c = '';   
        insert inqRec2;
        System.Test.StartTest();
        Database.executebatch(new ValidateInquiryEmailAndNumbersBatch(null));
        System.Test.StopTest();

    }




}