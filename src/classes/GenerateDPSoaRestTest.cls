/*********************************************************************************
* Description - Test class for class GenerateDPSoaRest.                          *
*--------------------------------------------------------------------------------*
* Version            Date            Author         Description                  *
* 1.0                14/02/2019      Arjun Khatri   Initial Draft                *
**********************************************************************************/
@isTest
Public class GenerateDPSoaRestTest {
    @isTest
    public static void testGenerateDpSoa() {
        //Create account
        Account objAccount = createAccount();
        System.debug('test account = ' + objAccount);
        
        //Create Service Request
        NSIBPM__Service_Request__c objDealSr = createDealSR();

        //Create Booking
        Booking__c objBooking = createBooking(objAccount.Id, objDealSr.Id);

        //Create Booking Unit
        Booking_Unit__c objBU = createBookingUnit(objBooking.Id,'test Hod');

        //create calling list
        Calling_List__c objCL = createCallingList(objAccount.Id, objBU.Id);
        
        String jsonStr =   '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ' ;

        
        String responseBody;
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new DPSoaHttpMock(1));
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));
            GenerateDPSoaRest.generateDPSoa('12345'
                                            , String.valueOf(objAccount.Id)
                                            , String.valueOf(objBU.Id)
                                            , String.valueOf(objCL.id));
            GenerateDPSoaRest.createErrorLog( 'test'
                                            , String.valueOf(objAccount.Id)
                                            , String.valueOf(objBU.Id)
                                            , String.valueOf(objCL.id) );
        Test.stopTest();
    }
    
    @isTest
    public static void testEmptyActionsError() {
        //Create account
        Account objAccount = createAccount();
        System.debug('test account = ' + objAccount);
        
        //Create Service Request
        NSIBPM__Service_Request__c objDealSr = createDealSR();

        //Create Booking
        Booking__c objBooking = createBooking(objAccount.Id, objDealSr.Id);

        //Create Booking Unit
        Booking_Unit__c objBU = createBookingUnit(objBooking.Id,'test Hod');

        //create calling list
        Calling_List__c objCL = createCallingList(objAccount.Id, objBU.Id);
        //Test.setMock(HttpCalloutMock.class, new DPSoaHttpMock(3));
        FmHttpCalloutMock.Response loginResponse = new FmHttpCalloutMock.Response( 200 , 'Success' ,
            '   {  '  +
            '     '  +
            '       "token": "eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1NTS2MLE1NjUx2l1IoCqIClhSVIoLQ4tSgvMTcVqM8i2NMt3NxCN8ggKCJM18MwMMpNNyjYM8gv1EypFgCrDSnJXQAAAA.msD8bcPXX_LvL7aBvkry5gUkHdbt_jDCjFLhWh_gHVw",  '  +
            '     '  +
            '       "tokenType": "Bearer",  '  +
            '     '  +
            '       "validTill": "01-Jun-2019 at 18:22:15"  '  +
            '     '  +
            '  }  '
        );

        FmHttpCalloutMock.Response unitSoaMockResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
        '   "timestamp": "2019-06-19T12:05:33.818+0000",  '  +
        '   "status": 401,  '  +
        '   "error": "Unauthorized",  '  +
        '   "message": "Unauthorized",  '  +
        '   "path": "/COFFEE/customer/DPSOA"  '  +
        '  }  ');

        String UNIT_SOA_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.UNIT_SOA, new List<String> {'3901'});
        String APEX_LOGIN_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.APEX_LOGIN, new List<String> {'8MABLQM-KJ8I8-1XA58-WWCM1S1'} );

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            UNIT_SOA_str => unitSoaMockResponse,
            APEX_LOGIN_str => loginResponse
        };

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            GenerateDPSoaRest.generateDPSoa('12345'
                                            , String.valueOf(objAccount.Id)
                                            , String.valueOf(objBU.Id)
                                            , String.valueOf(objCL.Id));
        Test.stopTest();
    }
    
    @isTest
    public static void testStatusCodeError() {
        //Create account
        Account objAccount = createAccount();
        System.debug('test account = ' + objAccount);
        
        //Create Service Request
        NSIBPM__Service_Request__c objDealSr = createDealSR();

        //Create Booking
        Booking__c objBooking = createBooking(objAccount.Id, objDealSr.Id);

        //Create Booking Unit
        Booking_Unit__c objBU = createBookingUnit(objBooking.Id,'test Hod');

        //create calling list
        Calling_List__c objCL = createCallingList(objAccount.Id, objBU.Id);
        Test.setMock(HttpCalloutMock.class, new DPSoaHttpMock(2));
        Test.startTest();
            GenerateDPSoaRest.generateDPSoa('12345'
                                            , String.valueOf(objAccount.Id)
                                            , String.valueOf(objBU.Id)
                                            , String.valueOf(objCL.Id));
        Test.stopTest();
    }
    
    static Account createAccount() {
        //Create Customer record
        Account objAcc = new Account(Name='Test Name');
        insert objAcc;
        return objAcc;
    }

    Static NSIBPM__Service_Request__c createDealSR() {
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
        return objSR;
    }

    Static Booking__c createBooking(Id accId, Id SRId) {
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = accId;
        objBooking.Deal_SR__c = SRId;
        insert objBooking;
        return objBooking;

    }

    static Booking_Unit__c createBookingUnit(Id bookingId, String HodName) {
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/001';
        objBookingUnit.Booking__c  = bookingId;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Hod_name__c = HodName;
        insert objBookingUnit;
        System.debug('test objBookingUnit = ' + objBookingUnit);
        return objBookingUnit;
    }

    static Calling_List__c createCallingList(Id accId, Id BUId) {

        Id collectioncallingRecordTypeId =
            Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get(
            'Collections Calling List').RecordTypeId;
        System.debug('test FMCollectionRecordTypeId = ' + collectioncallingRecordTypeId);
        Calling_List__c callListObj = new Calling_List__c(Account__c = accId,
                                                        Booking_Unit__c = BUId,
                                                        recordTypeId = collectioncallingRecordTypeId,
                                                        Calling_List_Type__c = 'DP Calling',
                                                        Calling_List_Status__c = 'New',
                                                        Mobile_Phone__c = '8877665542',
                                                        Email__c = '1test1@gmail.com',
                                                        Customer_Name__c = 'test customer',
                                                        Unit_Name__c = 'JNU/ABC/001');
        //insert callListObj;
        return callListObj;
    }
}