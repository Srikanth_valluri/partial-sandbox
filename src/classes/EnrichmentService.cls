/****************************************************************************************
Description: Class to consume Enrichment Adfolks service.
----------------------------------------------------------------------------------------
Version     Date           Author               Description                                 
1.0       11/8/2020    Ruchika Choudhary       Initial Draft
*****************************************************************************************/
global class EnrichmentService{
    
     /* Method Description : Method to call service method of Enrichment 
     * Input Parameters : accId - Account Id
     *                    emailId - Email id of Account
     * Return Type : void
     */    
    global static void sendHttpRequest(String accId, String emailId){

            Http http = new Http();
            HttpResponse response = new HttpResponse();
            HttpRequest request = new HttpRequest();
            Credentials_Details__c creds = getCredentials();
            if (creds.Endpoint__c != null) {
                request.setEndpoint(creds.Endpoint__c);
            }
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json');
            request.setBody('{"ExternalId": "'+accId+'","email": "'+emailId+'"}');
            request.setHeader('Authorization', System.Label.Enrichment_Token);  //System.Label.Enrichment_Token
         try {
         System.debug('request --- '+request + '<<<<<<< request body :' + request.getBody());
            response = http.send(request);
            System.debug('Response --- '+ response.getBody());
        } catch (Exception e) {
            System.debug('Exception --- ' + e);
        }
    }
    
    
    /* Method Description : Method to fetch Adfolks creds and endpoint from
    *                      custom setting 'Credentials Details'
    * Return Type : void
    */
    public static Credentials_Details__c getCredentials() {
        List<Credentials_Details__c> lstCreds = [ SELECT
                                                     Id
                                                     , Name
                                                     , User_Name__c
                                                     , Password__c
                                                     , Endpoint__c
                                                 FROM
                                                     Credentials_Details__c
                                                 WHERE
                                                     Name = :System.Label.Enrichment_label ];
        if( lstCreds != null && !lstCreds.isEmpty()) {
            return lstCreds[0];
        } else {
            return null;
        }
    }
  
 }