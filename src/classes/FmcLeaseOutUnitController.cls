public with sharing class FmcLeaseOutUnitController {

    public  List<SelectOption>          units                   {get; set;}
    public  FM_Request__c               req                     {get; set;}

    private static final String SR_SUBMIT_SUCCESS = 'Thank you for submitting the details. You will be contacted shortly by us!';
    private static final String GENERIC_SAVE_ERROR = 'There was an error while processing your request. Please try again later.';

    private Id leaseOutUnitRecordTypeId = Schema.SObjectType.FM_Request__c.getRecordTypeInfosByName()
                                        .get('Lease Out Unit').getRecordTypeId();
    private Map<Id, Booking_Unit__c>    mapUnits                {get; set;}
    private Id                          customerAccountId   =   CustomerCommunityUtils.customerAccountId;
    private Account customerAccount;

    public FmcLeaseOutUnitController() {
        if (FmcUtils.isCurrentView('LeaseOutUnit')) {
            initializeVariables();
        }
    }

    public void initializeVariables() {
        units = new List<SelectOption>();
        mapUnits = new Map<Id, Booking_Unit__c>();
        customerAccountId = CustomerCommunityUtils.customerAccountId;
        customerAccount = [
            SELECT  Id, Name, Party_Type__c, IsPersonAccount, Email__pc, Email__c
                    , Mobile_Phone_Encrypt__pc, Mobile__c, Mobile_Country_Code__c, Mobile_Country_Code__pc
            FROM    Account
            WHERE   Id = :customerAccountId];
        for (Booking_Unit__c unit : FmcUtils.queryUnitsForAccount(
            customerAccountId,
            'Id, Name, Unit_Name__c, Booking__r.Account__c, Inventory__c, Inventory__r.Building_Location__c, '
                + 'Inventory__r.Building_Location__r.Id, Inventory__r.Building_Location__r.Name, '
                + 'Inventory__r.Building_Location__r.Building_Name__c'
        )) {
            units.add(new SelectOption(unit.Id, unit.Unit_Name__c));
            mapUnits.put(unit.Id, unit);
        }

        req = new FM_Request__c(
            Source__c = 'LOAMS Portal',
            RecordTypeId = leaseOutUnitRecordTypeId,
            Request_Type__c = 'Lease Out Unit',
            Account__c = customerAccountId,
            Email__c = customerAccount.IsPersonAccount ? customerAccount.Email__pc : customerAccount.Email__c,
            Mobile_Country_Code__c = customerAccount.IsPersonAccount ? customerAccount.Mobile_Country_Code__pc : customerAccount.Mobile_Country_Code__c,
            Mobile_no__c = customerAccount.IsPersonAccount ? customerAccount.Mobile_Phone_Encrypt__pc : customerAccount.Mobile__c,
            Assigned_User_Email__c = Label.FmUnitLeasingEmail
        );
    }

    public void submitRequest() {

        Booking_Unit__c unit = mapUnits.get(req.Booking_Unit__c);

        req.Status__c = 'Submitted';
        req.Source__c = 'LOAMS Portal';
        req.RecordTypeId = leaseOutUnitRecordTypeId;
        req.Request_Type__c = 'Lease Out Unit';
        req.Account__c = customerAccountId;
        req.Email__c = customerAccount.IsPersonAccount ? customerAccount.Email__pc : customerAccount.Email__c;
        req.Mobile_Country_Code__c = customerAccount.IsPersonAccount ? customerAccount.Mobile_Country_Code__pc : customerAccount.Mobile_Country_Code__c;
        req.Mobile_no__c = customerAccount.IsPersonAccount ? customerAccount.Mobile_Phone_Encrypt__pc : customerAccount.Mobile__c;
        req.Assigned_User_Email__c = Label.FmUnitLeasingEmail;

        try {
            upsert req;
        } catch(Exception excp) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, GENERIC_SAVE_ERROR));
            return;
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, SR_SUBMIT_SUCCESS));
    }
}