@isTest
global class MockHttpResponseSMSService implements HttpCalloutMock {
    
    public integer intResponseNumber ;
    
    public MockHttpResponseSMSService( integer intNum ) {
        intResponseNumber = intNum ;
    }

    public MockHttpResponseSMSService() 
    {
    }

    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('http://api.smscountry.com/SMSCwebservice_bulk.aspx', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('SMS message(s) sent');
        if(intResponseNumber == 1)
        {
          res.setBody(null);
        }
        if(intResponseNumber == 2)
        {
          res.setBody('Error while sending SMS');
        }
        res.setStatusCode(200);
        return res;
    }
}