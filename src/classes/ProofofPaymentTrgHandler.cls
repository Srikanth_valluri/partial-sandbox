/******************************************************************************************
 *  Author   : Venkata Subhash K
 *  Company  : PwC ME
 *  Date     : June 2017                        
 ------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By          Description
 ----------------------------------------------------------------------------------------              
   
*******************************************************************************************/
public class ProofofPaymentTrgHandler  implements TriggerFactoryInterface{
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed After Update.                        *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/

    // After Update Implementation
    public void executeAfterUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){ 
       // Call POP SR Job 
       queuePopSRJobs(mapNewRecords.values());               
    }
    
    public void queuePopSRJobs(List<proof_of_payment__c > lstpops){
        list<proof_of_payment__c> newPopsToSendAttachments = new list<proof_of_payment__c>();
        newPopsToSendAttachments  = [select id,(select id from attachments) from proof_of_payment__c where id in:lstpops and status__c = 'SR Submitted to Finance'];
        list<id> attchmentIds = new list<id>();
        if(newPopsToSendAttachments.size()>0){
            for(proof_of_payment__c pop:newPopsToSendAttachments){
                for(attachment a:pop.attachments){
                    attchmentIds.add(a.id);
                }    
            }
            if(attchmentIds.size()>0){
                system.enqueueJob(new IPMS_Requests(attchmentIds,'SEND_POP_DOC')); 
            }
        } 
    }
    
    
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
    public void executeBeforeInsertUpdateTrigger(List<sObject> newRecordsList, Map<Id,sObject> oldRecordsMap){}
    public void executeBeforeDeleteTrigger(Map<Id,sObject> oldRecordsMap){}
    public void executeAfterDeleteTrigger(Map<Id,sObject> oldRecordsMap){}
    public static void executeAfterInsertTrigger(Map<Id, sObject> mapNewRecords){}
    public void executeBeforeUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
    public void executeBeforeInsertTrigger(List<sObject> newRecordsList){}
    
}