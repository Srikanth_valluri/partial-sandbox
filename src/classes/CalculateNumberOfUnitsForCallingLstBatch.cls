/*This Batch is used to Update Number of units on Calling List
 */
        
global class CalculateNumberOfUnitsForCallingLstBatch implements Database.Batchable<sObject> {
     String query;
     Id collectioncallingRecordTypeId;
     global Database.QueryLocator start(Database.BatchableContext BC) {
        collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
        
        query = 'SELECT Id,Account__c,RecordTypeId FROM Calling_List__c WHERE RecordtypeId =: collectioncallingRecordTypeId AND IsHideFromUI__c = false AND Account__c != null';
          
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Calling_List__c> lstOfCallingList) {
        Map<String,TriggerOnOffCustomSetting__c> triggerSettingMap = TriggerOnOffCustomSetting__c.getAll();
        TriggerOnOffCustomSetting__c callingTriggerInst = triggerSettingMap.get('CallingListTrigger');
        List<TriggerOnOffCustomSetting__c>updateTriggerSetting = new List<TriggerOnOffCustomSetting__c>();
        Boolean isOn = callingTriggerInst.OnOffCheck__c;
        if(isOn){
            callingTriggerInst.OnOffCheck__c = false;
            updateTriggerSetting.add(callingTriggerInst);
        }
        /*calling list trigger is disable*/
        update updateTriggerSetting;
        if(!isOn){
            if(lstOfCallingList != null && !lstOfCallingList.isEmpty()){
                CalculateNumberOfUnitsForCallingList.calculateNumberOfUnits(lstOfCallingList);
            }
        }
    }
    global void finish(Database.BatchableContext BC) {
        /*calling list trigger is enable*/
        Map<String,TriggerOnOffCustomSetting__c> triggerSettingMap = TriggerOnOffCustomSetting__c.getAll();
        TriggerOnOffCustomSetting__c callingTriggerInst = triggerSettingMap.get('CallingListTrigger');
        List<TriggerOnOffCustomSetting__c>updateTriggerSetting = new List<TriggerOnOffCustomSetting__c>();
        Boolean isOn = callingTriggerInst.OnOffCheck__c;
        if(!isOn){
            callingTriggerInst.OnOffCheck__c = true;
            updateTriggerSetting.add(callingTriggerInst);
        }
        
        update updateTriggerSetting;
        //System.scheduleBatch(new CalculateNumberOfUnitsForCallingLstBatch(),'Update Number Of Units '+System.Now(),15);
    }
}