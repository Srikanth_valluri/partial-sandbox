global class NoAttachmentsInPopEmailBatchOnCase implements Database.Batchable<sObject>
,Database.AllowsCallouts  {

    public class blobWrapper{
        String receiptName{get;set;}
        blob receiptFile{get;set;}
        String languageCode{get;set;}

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('*** NoAttachmentsInPopEmailBatchOnCase start ****');
        String strQuery = 'SELECT id, CaseNumber, Payment_Mode__c, Payment_Date__c ,Payment_Allocation_Details__c' +
                           ',Total_Amount__c, Account.PersonEmail, Account.IsPersonAccount, Account.Email__pc' +
                           ',Account.Email__c, Account.Party_ID__c, Account.Name, AccountId, Account.owner.Name,' +
                           'No_Attachments_in_POP_Email__c,' +
                           ' (SELECT WhoId, WhatId, Type, Status, OwnerId, Id, Subject, CreatedDate, Description' +
                              ', Assigned_User__c, ActivityDate, Owner.Name, Document_URL__c, Cash_Receipt_Id__c' +
                              ' FROM Tasks) ' +
                           'FROM Case WHERE No_Attachments_in_POP_Email__c = True';

        System.debug('strQuery is : ' + strQuery);
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<Case> listCases){
        System.debug('***NoAttachmentsInPopEmailBatchOnCase execute****' + listCases);

        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];

        List<OrgWideEmailAddress> bccMailAddress = [
            SELECT Id,
                   Address,
                   DisplayName
            FROM   OrgWideEmailAddress
            WHERE  DisplayName = :Label.Sf_Copy_Bcc_Mail_Id
        ];

        list<String> listReceiptSoaUrl = new list<String>();
        list<blobWrapper> listblobWrapper = new list<blobWrapper>();
        Messaging.SingleEmailMessage mail=new  Messaging.SingleEmailMessage();
        list<Messaging.SingleEmailMessage> mails=new  list<Messaging.SingleEmailMessage>();
        List<String> lstReceiptURLs = new List<String>();
        List<Case> lstCaseToUpdate = new List<Case>();  //New
        List<EmailMessage> lstEmailMsgToInsert = new List<EmailMessage>();  //New

        try{
            FmIpmsRestCoffeeServices.updateCustomSetting = false;

            For(Case objCase : listCases) {
                List<String> lstBulkSoaURLs = FmIpmsRestCoffeeServices.getAllBulkSoaInLanguageUrl(objCase.Account.Party_ID__c);
                if(!lstBulkSoaURLs.isEmpty() && lstBulkSoaURLs != null ) {
                    listReceiptSoaUrl.addAll(lstBulkSoaURLs);
                }
                String accessToken = FmIpmsRestCoffeeServices.accessToken;
                for(Task objT : objCase.Tasks) {
                    SYstem.debug('Cash receipt id is: ' + objT.Cash_Receipt_Id__c);
                    lstReceiptURLs = FmIpmsRestCoffeeServices.getAllReceiptUrl(objT.Cash_Receipt_Id__c);
                    System.debug('lstReceiptURLs is: ' + lstReceiptURLs);
                    if(lstReceiptURLs != null && !lstReceiptURLs.isEmpty() ) {
                        System.debug('Inside lstReceiptURLs IF ');
                        listReceiptSoaUrl.addAll(lstReceiptURLs);
                    }
                }

                System.debug('listReceiptSoaUrl : ' + listReceiptSoaUrl);
                for(String objStr : listReceiptSoaUrl) {
                    System.debug('...Multiple urls....'+objStr);
                    blobWrapper objblobWrapper = new blobWrapper();

                    Http h = new Http();
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint(objStr);
                    req.setHeader('Accept', 'application/json');
                    req.setHeader('Authorization','Bearer' + accessToken);
                    req.setMethod('GET');
                    req.setHeader('Content-Type', 'application/pdf');
                    req.setCompressed(true);
                    req.setTimeout(60000);
                    HttpResponse res  = h.send(req);
                    blob retFile = res.getBodyAsBlob();
                    objblobWrapper.languageCode = FmIpmsRestCoffeeServices.urlLanguageCodeMap.get(objStr);
                    objblobWrapper.receiptFile = retFile;
                    objblobWrapper.receiptName = objStr;
                    objblobWrapper.receiptName  = objblobWrapper.receiptName.substring(objblobWrapper.receiptName.lastIndexOf('/')+1);
                    listblobWrapper.add(objblobWrapper);
                }
                String body;
                string name = CREPOPSendEmail.nameFormat(objCase.Account.Name); //*
                system.debug( ' name : '+ name );
                body='Dear '+name+', <br/><br/>';
                body+='Greeting from Damac Properties !! <br/><br/>Please find attached receipt of your payment.<br/><br/>';
                body+='Should you require further assistance, please do not hesitate to contact us on +971 4 2375000. You can also email us your query to atyourservice@damacproperties.com';
                body+= '<br/><br/> Thank you,<br/> DAMAC Properties';

                String languageName;
                List<Attachment> attachlst = new List<Attachment>();
                if(!listblobWrapper.isEmpty()){
                    System.debug('...listblobWrapper...'+listblobWrapper);

                    for(blobWrapper oblobWrapper: listblobWrapper) {
                        languageName = FmIpmsRestCoffeeServices.languageCodeMap.get(oblobWrapper.languageCode) ;
                        Attachment attach = new Attachment();
                        attach.contentType = 'application/pdf';
                        attach.name =oblobWrapper.receiptName +' '+languageName+ '.pdf' ;
                        attach.body = oblobWrapper.receiptFile;
                        attachlst.add(attach);
                    }
                }

                if ( owea.size() > 0 ) {
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);

                    //Sendgrid:
                    EmailMessage emailMsg = new EmailMessage();

                    String emailAddress = objCase.Account.IsPersonAccount
                                        ? objCase.Account.Email__pc
                                        : objCase.Account.Email__c;
                    System.debug('attachlst : ' + attachlst);
                    if(attachlst.size() > 0) {
                        SendGridEmailService.SendGridResponse sendGridResponse = SendGridEmailService.sendEmailService(
                        emailAddress, CREPOPSendEmail.nameFormat(objCase.Account.Name),
                        '','',
                        bccMailAddress[0].Address, '',
                        'Receipt for your payment', '',
                        Label.DefaultOrgWideEmailAddress, 'POP SR - Document',
                        'noreply@Damacgroup.com', '',
                        'text/html', body,
                        '', attachlst
                        );


                        if(sendGridResponse.ResponseStatus == 'Accepted') {
                            System.debug('sendGrid response status is Accepted');
                            emailMsg.ToAddress=emailAddress;    //*
                            emailMsg.FromAddress = Label.DefaultOrgWideEmailAddress;
                            emailMsg.FromName = 'POP SR - Document';
                            emailMsg.Subject='Receipt for your payment';
                            emailMsg.HtmlBody=body;
                            emailMsg.ParentId = objCase.id; //Attach with the case
                            emailMsg.MessageDate = system.now();
                            emailMsg.Status = '3';
                            emailMsg.SendGrid_Status__c = sendGridResponse.ResponseStatus;
                            emailMsg.SentGrid_MessageId__c = sendGridResponse.messageId;
                            emailMsg.Account__c = objCase.Account.Id;
                            emailMsg.BccAddress = bccMailAddress[0].Address;
                            emailMsg.Sent_By_Sendgrid__c = true;
                            //insert emailMsg;
                            lstEmailMsgToInsert.add(emailMsg);

                            objCase.No_Attachments_in_POP_Email__c = false;
                            lstCaseToUpdate.add(objCase);
                        }
                    }
                }   //End of If(owe.Size > 0)
            }//End of For Loop (Case objCase : CaseList)
            if(!lstEmailMsgToInsert.isEmpty()) {
                System.debug('EmailMessages inserted!!');
                insert lstEmailMsgToInsert;
            }
            if(!lstCaseToUpdate.isEmpty()) {
                System.debug('Updating Cases!!');
                update lstCaseToUpdate;
            }
            FmIpmsRestCoffeeServices.updateCustomSetting = true;
            FmIpmsRestCoffeeServices.updateBearerToken();//calling updateMethod of Custom Setting after all callouts

        }
        catch(Exception Ex) {
            System.debug('Error Message in AgentEmailSend---- '+Ex.getMessage());
            System.debug('Error Message @Line Number---- '+Ex.getLineNumber());
        }

    }

    global void finish(Database.BatchableContext BC){
        System.debug('***NoAttachmentsInPopEmailBatchOnCase finish****');
    }

}