/**************************************************************************************************
* Name               : AP_BookingProgressController
* Description        : An apex coponent controller for AP_BookingProgress
* Created Date       : 29/10/2018
* Created By         : Nikhil Pote
* Last Modified Date :
* Last Modified By   : 
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR                    DATE
* 1.0         Nikhil Pote               25/09/2018
**************************************************************************************************/
public class AP_BookingProgressController {
    public boolean awaitingTokenPayment {get;set;}
    public boolean buyersConfirmation {get;set;}
    public boolean documentsVerification {get;set;}
    public boolean SPASigned {get;set;}
    public string SRStatus {get;set;}
    public string statusName {get;set;}
    public static string statusName1;

    public AP_BookingProgressController() {
        statusName = statusName1;
    }

    public void SRStatusMapping() {
        system.debug('SRStatus--'+SRStatus);
        
        Map<String, BookingSRStatus__c> bookingStatusMap = BookingSRStatus__c.getAll();
        system.debug('bookingStatusMap--'+bookingStatusMap);
        system.debug('bookingStatusMap--2'+bookingStatusMap.values());
        for(BookingSRStatus__c objBS: bookingStatusMap.values()) {

            if(string.isnotblank(SRStatus) && objBS.status__c.contains(SRStatus)) {
                system.debug('ob name--'+objBS.name);
                statusName = objBS.name;
                system.debug('statusName--'+statusName);
            }
        }
    }
    public static String SRStatusMapping1(String statusCode) {
        system.debug('statusCode--'+statusCode);
        
        Map<String, BookingSRStatus__c> bookingStatusMap = BookingSRStatus__c.getAll();
        system.debug('bookingStatusMap--'+bookingStatusMap);
        system.debug('bookingStatusMap--2'+bookingStatusMap.values());
        for(BookingSRStatus__c objBS: bookingStatusMap.values()) {

            if(string.isnotblank(statusCode) && objBS.status__c.contains(statusCode)) {
                system.debug('in if');
                system.debug('ob name--'+objBS.name);
                statusName1 = objBS.name;
                system.debug('statusName--'+statusName1);
            } 
        }
        return statusName1;
    }
}