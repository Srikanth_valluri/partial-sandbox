public with sharing class AgencyActivitiesController {

    public List<String> taskHeaderList  {get; set;}
    public List<String> taskFieldList   {get; set;}
    public List<Task> taskList          {get; set;}
    public String accountId             {get; set;}

    public AgencyActivitiesController(ApexPages.StandardController controller){

        accountId = controller.getId();
        String loggedinUserId = UserInfo.getUserId();
        String loggedinUserRoleId = UserInfo.getUserRoleId();
        String loggedinUserProfileId = UserInfo.getProfileId();
        taskList = new List<Task>();
        taskHeaderList = new List<String>();
        taskFieldList = new List<String>();
        String queryFields = '';

        Map<String,List<Task>> taskOwnerMap = new Map<String,List<Task>>();
        Set<Id> agentsTeamProfileIdSet = new Set<Id>();
        for(Profile profileObj : [Select Id
                                    FROM Profile
                                    WHERE Name = :Label.Agent_Executive_Manager_Profile
                                      OR Name = :Label.Agent_Executive_Team_Profile
                                      OR Name = :Label.Agent_Admin_Team_Profile
                                      OR Name = :Label.Agent_Admin_Manager_Profile
        ]) {
            agentsTeamProfileIdSet.add(profileObj.Id);
        }

        Account acc = [SELECT Id, Partnership_Program_Owner__c, Partnership_Program_Owner__r.Id, OwnerId FROM Account WHERE Id = :accountId];

        for (Schema.FieldSetMember taskField : SObjectType.Task.FieldSets.AccountTaskList.getFields()
        ) {
            queryFields += taskField.getFieldPath() + ', ';
            taskHeaderList.add(taskField.getLabel());
            taskFieldList.add(taskField.getFieldPath());
        }

        String taskQuery = ' SELECT '
                         + queryFields
                         + ' WhatId, Owner.Name, WhoId, Id '
                         + ' FROM Task '
                         + ' WHERE WhatId = :accountId Order By CreatedDate Desc LIMIT 400';

        if (loggedinUserId == acc.OwnerId /* || agentsTeamProfileIdSet.contains(loggedinUserProfileId) */)
         {
            taskList = Database.query(taskQuery);
        }
        if(taskList.isEmpty()) {
            if(String.isNotBlank(accountId)) {
                for(Task taskObj : Database.query(taskQuery)) {
                    if(!taskOwnerMap.containsKey(taskObj.OwnerId)) {
                        taskOwnerMap.put(taskObj.OwnerId, new List<Task>{});
                    }
                    taskOwnerMap.get(taskObj.OwnerId).add(taskObj);
                }
            }
            getAllSubRoleIds(loggedinUserId, taskOwnerMap);
        }
    }

    public void getAllSubRoleIds(String pUserId, Map<String,List<Task>> pTaskOwnerMap) {

        if (String.isNotBlank(pUserId) && !pTaskOwnerMap.isEmpty()) {

            // get all of the roles underneath the passed roles
            for(User userObj : [ SELECT Id, Name, ManagerId
                                   FROM User
                                  WHERE Id = :pUserId]) {
                if(pTaskOwnerMap.containsKey(userObj.Id)) {
                    taskList.addAll(pTaskOwnerMap.get(userObj.Id));
                }
            }
            getChildUsers(new Set<Id>{pUserId}, pTaskOwnerMap);
        }
    }

    public void getChildUsers(Set<Id> pUserIdSet, Map<String,List<Task>> pTaskOwnerMap) {

        Set<Id> userIdSet = new Set<Id>();
        if (pUserIdSet.size() > 0) {
            for(User us : [SELECT Id, Name, ManagerId
                             FROM User
                            WHERE ManagerId IN :pUserIdSet]) {
                if(pTaskOwnerMap.containsKey(us.Id)) {
                    taskList.addAll(pTaskOwnerMap.get(us.Id));
                }
                userIdSet.add(us.Id);
            }
            if(!userIdSet.isEmpty()) {
                getChildUsers(userIdSet, pTaskOwnerMap);
            }
        }
    }

}