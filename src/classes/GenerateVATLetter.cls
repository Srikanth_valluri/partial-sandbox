public without sharing class GenerateVATLetter  {
    public String HCurl;
    Booking_Unit__c objCase;
    String strPageID;
    String drawloopFlag;
    public GenerateVATLetter (ApexPages.standardController controller){
       
    }//End constructor
     public GenerateVATLetter (){
       
    }
    public pagereference init(){
        strPageID = ApexPages.currentPage().getParameters().get('id');
        objCase = [select Id
                        , Account_Id__c
                        , Registration_ID__c
                        , Unit_Type__c
                        , Property_Name__c
                        , Property_Country__c
                        , Building_Name__c
                        , District__c
                        , Property_City__c
                        , Permitted_Use__c
                        , Inventory__c
                        ,  Area_Variation_Formula__c
                        , Inventory__r.Building_Location__c
                        , Inventory__r.Building_Location__r.New_Ho_Doc_Type__c
                From Booking_Unit__c 
                    Where id =:strPageID];
        //generating HO letters
        generateHOLetter( objCase );

       if(objCase != null){

            drawloopFlag = 'n';
            
            HandoverChecklistGeneration.HCResponse objHC;
            // for normal handover send  HOCHECKLIST instead of E-HOCHECKLIST
            String processName = '';
            processName = 'HOCHECKLIST';

            objHC = HandoverChecklistGeneration.GetHandoverChecklist(processName, objCase.Registration_ID__c);
            system.debug('objHC*****'+objHC);
            HCurl = objHC.url; 
            list<SR_Attachments__c> lstCaseAttachment = new list<SR_Attachments__c>();
            SR_Attachments__c objCaseAttachment1 = new SR_Attachments__c();
            if (objCase.Inventory__r.Building_Location__r.New_Ho_Doc_Type__c.equalsIgnoreCase( 'Villa' )) {
                 objCaseAttachment1.Name = objCase.Registration_ID__c+'  Handover Checklist Villa' +system.now();
            }else {
                if (objCase.Inventory__r.Building_Location__r.New_Ho_Doc_Type__c.equalsIgnoreCase( 'Unit' )) {
                    objCaseAttachment1.Name = objCase.Registration_ID__c+'  Handover Checklist Unit' +system.now();
                }else {
                    if (objCase.Inventory__r.Building_Location__r.New_Ho_Doc_Type__c.equalsIgnoreCase( 'Apartment' )) {
                        objCaseAttachment1.Name = objCase.Registration_ID__c+'  Handover Checklist Apartment' +system.now();
                    } else {
                        objCaseAttachment1.Name = objCase.Registration_ID__c+'  Handover Checklist ' +system.now();
                    }
                }
                
            }
            
           
            objCaseAttachment1.Booking_Unit__c = objCase.Id;
            lstCaseAttachment.add(objCaseAttachment1);
            if (lstCaseAttachment != null && lstCaseAttachment.size()>0){
                insert lstCaseAttachment;
            }
            
           PageReference pr = new PageReference('salesforce_url/console#%2F' + strPageID);
           // pagereference newpg = new Pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+strPageID);
            //newpg.setRedirect(false);
            return null ;
            
        }else{
            return null;
        }

        
    }

    public static void generateHOLetter( Booking_Unit__c  objCase ) {
        if( objCase != null 
        && objCase.Inventory__c != null 
        && objCase.Inventory__r.Building_Location__c != null 
        && String.isNotBlank( objCase.Inventory__r.Building_Location__r.New_Ho_Doc_Type__c ) 
        && ( objCase.Inventory__r.Building_Location__r.New_Ho_Doc_Type__c.equalsIgnoreCase( 'Villa' ) 
            || objCase.Inventory__r.Building_Location__r.New_Ho_Doc_Type__c.equalsIgnoreCase( 'Unit' ) 
            || objCase.Inventory__r.Building_Location__r.New_Ho_Doc_Type__c.equalsIgnoreCase( 'Apartment' ) ) 
        ) {
            
            String csName = '';
            csName = objCase.Inventory__r.Building_Location__r.New_Ho_Doc_Type__c.equalsIgnoreCase( 'Villa' ) ? 'HO Letter Villa' : 'HO Letter Unit';
            
            Riyadh_Rotana_Drawloop_Doc_Mapping__c cs = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( csName );
            System.debug('cs--'+cs);
            if(cs != null ) {
                try {
                    DrawloopDocGen.generateDoc( cs.Drawloop_Document_Package_Id__c
                                                , cs.Delivery_Option_Id__c
                                                , objCase.Id );
                    ApexPages.addmessage(
                        new ApexPages.message( ApexPages.severity.INFO, 
                        csName + ' generated!')
                    );                                
                } 
                catch (Exception ex ) {
                    ApexPages.addmessage(
                        new ApexPages.message( ApexPages.severity.ERROR, 
                        ex.getMessage() )
                    );
                }
                    
                
            }//End cs If
            else {
                ApexPages.addmessage(
                    new ApexPages.message( ApexPages.severity.ERROR, 
                    'Can not find Drawloop Document Package - ' + csName)
                );
            }
        }   //End CaseId if
        /*else {
            ApexPages.addmessage(
                new ApexPages.message( ApexPages.severity.ERROR, 
                'Case Id not found!')
            );
        }*/
    }
}