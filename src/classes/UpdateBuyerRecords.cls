global class UpdateBuyerRecords {
    
    webservice static string updateBuyers(string[] buyerRecIds) {
        System.debug('bbbbbbbbbbbbbb'+buyerRecIds);
        List<Buyer__c> buyerRecords = [select Title__c,Email__c,Phone_Country_Code__c,Phone_with_Country_Code__c,Account__c,
                                          First_Name__c,Last_Name__c,Passport_Number__c,Nationality__c,Address_Line_1__c,Address_Line_2__c,
                                          Address_Line_3__c,Address_Line_4__c,Country__c,city__c from Buyer__c where id in :buyerRecIds
                                          and Service_Request__r.Deal_Type__c = 'Recovery'];
        if ( buyerRecords.size() > 0 ) {
            set<Id> recIds = new Set<Id>();
            Map<Id,List<Buyer__c>> accBuyers = new Map<Id,List<Buyer__c>>();
            for (Buyer__c buyer :buyerRecords) {
                recIds.add(buyer.Account__c);
                if ( accBuyers.containskey(buyer.Account__c)) {
                    accBuyers.get(buyer.Account__c).add(buyer);
                } else {
                    accBuyers.put(buyer.Account__c,new List<Buyer__c>{buyer});
                }
            }
            List<Buyer__c> updateBuyerList = new List<Buyer__c>();
            string allFields = getAllFields('Account');
            string query = 'Select '+allFields+' From Account where id in : recIds';
            List<Account> listAccount = database.query(query);
            /* List<Account> listAccount = [SELECT Id, Name, IsPersonAccount, First_Name__c,MiddleName,FirstName,Mobile_Phone_Encrypt__pc,
                                                LastName, Last_Name__c,Middle_Name__c,Email__pc, Email__c, Mobile__c,
                                                Mobile_Country_Code__c, Passport_Number__pc, Passport_Number__c,
                                                Nationality__pc, Nationality__c, City__c, City__pc,
                                                Country__pc, Country__c, Address_Line_1__pc,
                                                Address_Line_2__pc, Address_Line_3__pc,
                                                Address_Line_4__pc, Address_Line_1__c,
                                                Address_Line_2__c, Address_Line_3__c,
                                                Address_Line_4__c, PersonTitle,
                                                P_O_Box_Zip_Postal_Code__c,
                                                Title_Arabic__c
                                           FROM Account
                                          WHERE Id IN :recIds
            ]; */
            System.debug('====UpdateBuyer=listAccount===='+listAccount);
            for(Account account : listAccount){
                System.debug('====account=='+account);
                for(Buyer__c buyer : accBuyers.get(account.Id)){
                    buyer.Title__c = account.PersonTitle != null ? account.PersonTitle : buyer.Title__c;
                    buyer.Email__c = account.Email__pc != null ? account.Email__pc : account.Email__c;
                    buyer.Zip_Code__c = account.P_O_Box_Zip_Postal_Code__c;
                    System.debug('sssssssssssssssss'+account.IsPersonAccount);
                    if(account.IsPersonAccount){
                        if ( buyer.Phone_Country_Code__c == null && buyer.Phone_with_Country_Code__c == null ) {
                            buyer.Phone__c = account.Mobile_Phone_Encrypt__pc;
                        }
                        buyer.First_Name__c = account.FirstName != null ? account.FirstName : buyer.First_Name__c;
                        buyer.Middle_Name__c = account.MiddleName != null ? account.MiddleName : buyer.Middle_Name__c;
                        buyer.Last_Name__c = account.LastName != null ? account.LastName : buyer.Last_Name__c;
                        buyer.Passport_Number__c = account.Passport_Number__pc != null ? account.Passport_Number__pc : buyer.Passport_Number__c;
                        buyer.Nationality__c = account.Nationality__pc != null ? account.Nationality__pc : buyer.Nationality__c;
                        buyer.Address_Line_1__c = account.Address_Line_1__pc != null ? account.Address_Line_1__pc : buyer.Address_Line_1__c;
                        buyer.Address_Line_2__c = account.Address_Line_2__pc != null ? account.Address_Line_2__pc : buyer.Address_Line_2__c;
                        buyer.Address_Line_3__c = account.Address_Line_3__pc != null ? account.Address_Line_3__pc : buyer.Address_Line_3__c;
                        buyer.Address_Line_4__c = account.Address_Line_4__pc != null ? account.Address_Line_4__pc : buyer.Address_Line_4__c;
                        buyer.Country__c = account.Country__pc != null ? account.Country__pc : buyer.Country__c;
                        if ( buyer.Country__c != null ) {
                            if (account.City__pc != null) {
                                List <String> cityList = getCityValues(buyer.Country__c);
                                if ( cityList.contains(account.City__pc) ) {
                                    buyer.City__c = account.City__pc ;                        
                                } else {
                                    buyer.City__c = null;
                                }
                                buyer.City_Text__c = account.City__pc;
                            } else if ( account.City__c != null ) {
                                List <String> cityList = getCityValues(buyer.Country__c);
                                if ( cityList.contains(account.City__c) ) {
                                    buyer.City__c = account.City__c ;                        
                                } else {
                                    buyer.City__c = null;
                                }
                                buyer.City_Text__c = account.City__c;
                            }
                        }
                        buyer.Title_Arabic__c = account.Title_Arabic__pc;
                        buyer.First_Name_Arabic__c = account.First_Name_Arabic__pc;
                        buyer.Middle_Name_Arabic__c = account.Middle_Name_Arabic__pc;
                        buyer.Last_Name_Arabic__c = account.Last_Name_Arabic__pc;
                        buyer.Place_of_Issue_Arabic__c = account.Passport_Issue_Place_Arabic__pc;
                        buyer.Country_Arabic__c = account.Country_Arabic__pc;
                        buyer.City_Arabic__c = account.City_Arabic__pc;
                        buyer.Address_Line_1_Arabic__c = account.Address_Line_1_Arabic__pc;
                        buyer.Address_Line_2_Arabic__c = account.Address_Line_2_Arabic__pc;
                        buyer.Address_Line_3_Arabic__c = account.Address_Line_3_Arabic__pc;
                        buyer.Address_Line_4_Arabic__c = account.Address_Line_4_Arabic__pc;
                        
                        
                    } else {
                        buyer.First_Name__c = account.First_Name__c != null ? account.First_Name__c : buyer.First_Name__c;
                        buyer.Middle_Name__c = account.Middle_Name__c != null ? account.Middle_Name__c : buyer.Middle_Name__c;
                        buyer.Last_Name__c = account.Last_Name__c != null ? account.Last_Name__c : buyer.Last_Name__c;
                        buyer.Passport_Number__c = account.Passport_Number__c != null ? account.Passport_Number__c : buyer.Passport_Number__c;
                        buyer.Nationality__c = account.Nationality__c != null ? account.Nationality__c : buyer.Nationality__c;
                        buyer.Address_Line_1__c = account.Address_Line_1__c != null ? account.Address_Line_1__c : buyer.Address_Line_1__c;
                        buyer.Address_Line_2__c = account.Address_Line_2__c != null ? account.Address_Line_2__c : buyer.Address_Line_2__c;
                        buyer.Address_Line_3__c = account.Address_Line_3__c != null ? account.Address_Line_3__c : buyer.Address_Line_3__c;
                        buyer.Address_Line_4__c = account.Address_Line_4__c != null ? account.Address_Line_4__c : buyer.Address_Line_4__c;
                        buyer.Country__c = account.Country__c != null ? account.Country__c : buyer.Country__c;
                        if (account.City__c != null) {
                            List <String> cityList = getCityValues(buyer.Country__c);
                            if ( cityList.contains(account.City__c) ) {
                                buyer.City__c = account.City__c ;                        
                            }
                        }
                        buyer.City_Text__c = account.City__c;
                        
                        buyer.Title_Arabic__c = account.Title_Arabic__c;
                        buyer.First_Name_Arabic__c = account.First_Name_Arabic__c;
                        buyer.Middle_Name_Arabic__c = account.Middle_Name_Arabic__c;
                        buyer.Last_Name_Arabic__c = account.Last_Name_Arabic__c;
                        buyer.Place_of_Issue_Arabic__c = account.Passport_Issue_Place_Arabic__c;
                        buyer.Country_Arabic__c = account.Country_Arabic__c;
                        buyer.City_Arabic__c = account.City_Arabic__c;
                        buyer.Address_Line_1_Arabic__c = account.Address_Line_1_Arabic__c;
                        buyer.Address_Line_2_Arabic__c = account.Address_Line_2_Arabic__c;
                        buyer.Address_Line_3_Arabic__c = account.Address_Line_3_Arabic__c;
                        buyer.Address_Line_4_Arabic__c = account.Address_Line_4_Arabic__c;
                        
                         
                    }
                    updateBuyerList.add(buyer);
                }
            }
            update updateBuyerList;
            return 'success';
            
        } else {
            return 'fail';
        }
    }
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        
        if (objectType == null)
            return fields;
        
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            
            fields += sfield.getDescribe().getName ()+ ', ';
            
        }
        return fields.removeEnd(', '); 
    }
    public static List <String> getCityValues  (String countryVal) {
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get ('Buyer__c').getDescribe ().fields.getMap ();
        
        Schema.DescribeFieldResult theFieldResult = fieldMap.get('City__c').getDescribe();
        Schema.sObjectField theField = theFieldResult.getSObjectField();
        
        Schema.DescribeFieldResult ctrlFieldResult = fieldMap.get('Country__c').getDescribe();
        Schema.sObjectField ctrlField = ctrlFieldResult.getSObjectField();
        
        
        Map<String, List<String>> results = DependentPickListFields.getDependentOptionsImpl(theField, ctrlField);
        if (results.containsKey (countryVal)) {
            return results.get (countryVal);
        }
        else
            return null;
    }
}