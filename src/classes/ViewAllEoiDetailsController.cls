public Class ViewAllEoiDetailsController {

    public List<EOI_Process__c> eoilst{get; set;}
    
    public ViewAllEoiDetailsController(){    
        eoilst = new List<EOI_Process__c>();
        initQuery();
    
    }
    
    public void initQuery(){
        User portaluser = new User();
        Contact portalcon = new Contact();
        
        portaluser = [Select accountid,contactid,id from User where id = :Userinfo.getuserid()];
        if(portaluser.contactid != null){
            portalcon = [ Select id,Accountid, Owner__c, Portal_Administrator__c from Contact where id = :portaluser.contactid];
        }
        system.debug('Portal user @@@ ' + portaluser );
        // Get the fields for the EOI_Process__c object field Set
        String eoiprocessquery = 'Select Property__r.name,';
        List<Schema.FieldSetMember> eoidetailMemberList 
            = SObjectType.EOI_Process__c.FieldSets.Eoidetailsfiedset.getFields();
        for (Schema.FieldSetMember fsMember : eoidetailMemberList) {
            eoiprocessquery += fsMember.getFieldPath() + ', ';
        }
        eoiprocessquery = eoiprocessquery.removeEnd(', ');
        eoiprocessquery += ' from EOI_Process__c where ownerid =\'' + portaluser.id +'\'';
        
        if(portalcon != null && portalcon.Owner__c == true){
           eoiprocessquery += ' OR Agency__c =\'' + portalcon.Accountid + '\''; 
        } else if(portalcon != null && portalcon.id != null){
           eoiprocessquery += ' OR Agency_Contact__c = \'' + portalcon.id + '\''; 
        }
        
        System.debug('Queryy @@@@ '+ eoiprocessquery);
        eoilst = Database.query(eoiprocessquery);
        System.debug('List eoi @@@  ' + eoilst);
        
    }//end of initQuery
}