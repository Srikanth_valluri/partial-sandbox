@isTest(seeAlldata=true)
private class BuildingPaymentPlanControllerTest {

@isTest static void Test1() {
    test.starttest();
    Payment_Terms__c pt = new Payment_Terms__c();
    pt = [select id,Payment_Plan__c from Payment_Terms__c where Payment_Plan__c !='' Limit 1];
    
    Payment_Plan__c pp = new Payment_Plan__c();
    pp = [select id,Building_ID__c,Effective_From__c from Payment_Plan__c where id=: pt.Payment_Plan__c Limit 1];
    
    Inventory__c inv = new Inventory__c();
    inv = [select id,Inventory_ID__c,Building_ID__c,Floor_ID__c,Address_Id__c,Property_ID__c,Status__c,Unit_ID__c 
           from Inventory__c 
           where Status__c='Released'
           Limit 1];
    
    
    
    /*ContentVersion cv= new ContentVersion();
    cv.title='test';
    cv.PathonClient='/d07/layouts/PRVB111104.pdf';
    cv.versionData=blob.valueof('124');
    insert cv; 
    ContentDistribution cd = new ContentDistribution();
    cd.name='test_P';
    cd.ContentversionId=cv.id;
    cd.RelatedRecordId=unit.id;
    insert cd;
    
    ContentVersion cv1= new ContentVersion();
    cv1.title='test1';
    cv1.PathonClient='/d07/layouts/PRVB111104.pdf';
    cv1.versionData=blob.valueof('124');
    insert cv1;
    ContentDistribution cd1 = new ContentDistribution();
    cd1.name='test1';
    cd1.ContentversionId=cv1.id;
    cd1.RelatedRecordId=flr.id;
    insert cd1;*/
    
    
    PageReference pageRef = Page.Building_Payment_Plan;
    Test.setCurrentPage(pageRef);
    
     ApexPages.StandardController sc = new ApexPages.StandardController(inv);
     ApexPages.currentPage().getParameters().put('Id',inv.id);
     BuildingPaymentPlanController cls = new BuildingPaymentPlanController (sc);
                
        
    //Test.startTest();
    
    BuildingPaymentPlanController.getCreatableFieldsSOQL('Inventory__c');
    BuildingPaymentPlanController.planwrapper p= new BuildingPaymentPlanController.planwrapper();
    p.FileName='test';
    p.FileURL='222';
    cls.prepareData();
    
    cls.getPlans();
    cls.getAllPlans();
    //Test.stopTest();
    test.stoptest();
    }

}