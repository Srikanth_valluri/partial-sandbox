/* Sending free appointment slots to Mobile App*/
@RestResource(urlMapping='/SendAvailableAppointmentsToMObileApp/*')
global with sharing class SendAvailableAppointmentsToMObileApp{
   Global static final String HANDOVER_EARLY = '02-Handover - Early';
    Global static final String HANDOVER_NORMAL = '01-Handover';
    Global static final String PROPERTY_RESALE = '02-Property Resale';
    Global static final String BILLING_MATTERS = '03-Statement/Billing Matters';
    Global static final String PROJECT_UPDATES = '04-Project Updates';
    Global static final String COCD = ' 05-Change of Details';
    Global static final String ADDITIONAL_PARKING = '06-Additional Parking';
    Global static final String REJECTED_UNITS = '07-Rejected Units';
    Global static final String RENTAL_POOL = '08-Rental Pool';
    Global static final String TITLE_DEED = '09-Title Deed';
    Global static final String FUND_TRANSFER = '10-Fund Transfer';
    Global static final String CHEQUE_COLLECTION = '12-Cheque Collection';
    Global static final String PAYMENTS = '13-Payments';
    Global static final String ENQUIRIES = 'Enquiries';
    Global static final String MORTGAGE = 'Mortgage';
    Global static final String VISA_NOC ='11-Visa NOC';
    @HTTPPost
    global static AppointmentWrapper  getAppointments(String strSelectedProcess, String strSelectedSubProcess, String strSelectedDate, String buildingId) {
        
       list<Booking_Unit__c> bu=[select id,Unit_Name__c,Inventory__r.Building_Location__c from Booking_Unit__c where Unit_Name__c=:buildingId and Inventory__c!=null limit 1];
      system.debug('***bu***'+bu);
       String Query ;
       String QueryStr;
   
       AppointmentWrapper apWrapper=new AppointmentWrapper();
        list<AppointmentWrapper>a=new list<AppointmentWrapper >();
       // apWrapper.appointmentLIst =new list<Appointment__c>();
        apWrapper.todaySlotsList =new list<String>();
        apWrapper.AvailableSlotsList=new list<String>();
        
        //apWrapper.apWrpMap =new Map<String,list<String>>();
        list<Appointment__c> formattedSlots = new list<Appointment__c>();
        system.debug('strSelectedProcess '+strSelectedProcess);
        system.debug('strSelectedSubProcess '+strSelectedSubProcess);        
        system.debug('strSelectedDate '+strSelectedDate);
        Date objDate = Date.valueOf( strSelectedDate );
        system.debug('>>>buildingId>>> '+buildingId);
        system.debug('objDate '+objDate);
        String now = (String.valueOf(System.Now().time())).substringBeforelast(':');
        system.debug('now '+now);
      
       // slotLst = new list<String>();
        list<Appointment__c>appointmntSlotLst = new list<Appointment__c>();
        set<String>appointmntSlotSet = new Set<String>();
        list<id> appLst = new list<id>();
        list<Appointment__c>appointmntObj = new list<Appointment__c>();        
        list<Calling_List__c> lstCall = [Select id, Appointment__c from Calling_List__c where Appointment__c != NULL ];
        for(Integer i = 0; i<lstCall.size();i++){
            appLst.add(lstCall[i].Appointment__c);
        }        
        system.debug('appLst '+appLst);
        if(objDate == System.Today()){
            
                       apWrapper.error= 'You can book appointments only after 24 hours from the present time';
        }
        Query = ' SELECT Id'
                        +', Name'
                        +', Appointment_Date__c'
                        +', Building__c'
                        +', Process_Name__c'
                        +', Slots__c'
                        +', Sub_Process_Name__c'
                        +', Time_Slot_Sub__c'
                        +' FROM Appointment__c ';
            if(strSelectedProcess == HANDOVER_NORMAL 
                    && (strSelectedSubProcess == 'Unit Viewing' || strSelectedSubProcess == 'Key Handover' || strSelectedSubProcess == 'Unit Viewing or Key Handover' || strSelectedSubProcess !='Documentation')
                    ){ 
                    if(bu.size()>0)    
                    {               
                if(bu[0].Inventory__r.Building_Location__c!= null){
                    if(objDate == System.Today() || objDate == System.Today().addDays(1)){
                        if(objDate == System.Today()){
                            objDate = (System.Today()).addDays(1);
                        }
                        QueryStr = Query + ' WHERE Appointment_Date__c = ' +
                                        (DateTime.newInstance(((System.Today()).addDays(2)).year(), ((System.Today()).addDays(2)).month(), ((System.Today()).addDays(2)).day())).format('YYYY-MM-dd')
                                +' AND Building__c = \''+bu[0].Inventory__r.Building_Location__c+'\''
                                +' AND Process_Name__c = \'Handover\''
                                +' AND id NOT IN ' + getIdListForQuery(appLst) ;                        
                    
                        Query +=    ' WHERE Appointment_Date__c >= ' +
                                        (DateTime.newInstance(((System.Today()).addDays(2)).year(), ((System.Today()).addDays(2)).month(), ((System.Today()).addDays(2)).day())).format('YYYY-MM-dd')
                                        +' AND Building__c = \''+bu[0].Inventory__r.Building_Location__c+'\''                                 
                                    +' AND Process_Name__c = \'Handover\''
                                    +' AND id NOT IN ' + getIdListForQuery(appLst) ;
                        formattedSlots = Database.Query(QueryStr);
                        for(Appointment__c appointment : formattedSlots){
                            system.debug('slotss'+(appointment.Slots__c).substringBefore('-'));
                            if((appointment.Slots__c).substringBefore('-') >= now){
                                appointmntObj.add(appointment);
                            }
                        }
                        system.debug('appointmntObj== '+appointmntObj);
                        appointmntSlotLst = Database.Query(Query);
                    }else{
                        QueryStr = Query + ' WHERE Building__c = \''+bu[0].Inventory__r.Building_Location__c+'\''
                                    +' AND Appointment_Date__c = ' +
                                        (DateTime.newInstance(objDate.year(),objDate.month(),objDate.day())).format('YYYY-MM-dd')
                                    +' AND Process_Name__c = \'Handover\''
                                    +' AND id NOT IN ' + getIdListForQuery(appLst) ;                        
                        
                        Query += ' WHERE Appointment_Date__c >= ' +
                                        (DateTime.newInstance(((System.Today()).addDays(2)).year(), ((System.Today()).addDays(2)).month(), ((System.Today()).addDays(2)).day())).format('YYYY-MM-dd')
                                        +' AND Building__c = \''+bu[0].Inventory__r.Building_Location__c+'\''                                 
                                    +' AND Process_Name__c = \'Handover\''
                                    +' AND id NOT IN ' + getIdListForQuery(appLst) ;   
                        /*' WHERE Building__c = \''+buildingId+'\''
                                    +' AND Appointment_Date__c >= ' +
                                        (DateTime.newInstance(((System.Today()).addDays(2)).year(), ((System.Today()).addDays(2)).month(), ((System.Today()).addDays(2)).day())).format('YYYY-MM-dd')
                                    +' AND Process_Name__c = \'Handover\''
                                    +' AND id NOT IN ' + getIdListForQuery(appLst) ;*/
                        
                        system.debug('>>>QueryString'+Query);
                        appointmntObj = Database.Query(QueryStr);
                        appointmntSlotLst = Database.Query(Query);  
                    }  
                }
                }
                else{
                    system.debug('in Else== No slots available');
                    apWrapper.error= 'No slots available since this appointment can be booked only after HO notice is issued for your unit.';
                    //return null;
                }      
            }                       
        else if(strSelectedProcess == CHEQUE_COLLECTION || strSelectedProcess == PAYMENTS) {
            if(objDate == System.Today() || objDate == System.Today().addDays(1)){
                if(objDate == System.Today()){
                    objDate = (System.Today()).addDays(1);
                }
                QueryStr = Query + ' WHERE Appointment_Date__c = ' +
                                (DateTime.newInstance(objDate.year(),objDate.month(),objDate.day())).format('YYYY-MM-dd')
                        +' AND Process_Name__c = \'Collections\''
                        +' AND id NOT IN ' + getIdListForQuery(appLst) ;                        
            
                Query +=    ' WHERE Appointment_Date__c >= ' +
                               (DateTime.newInstance(((System.Today()).addDays(2)).year(), ((System.Today()).addDays(2)).month(), ((System.Today()).addDays(2)).day())).format('YYYY-MM-dd')
                            +' AND Process_Name__c = \'Collections\''
                            +' AND id NOT IN ' + getIdListForQuery(appLst) ;
                formattedSlots = Database.Query(QueryStr);
                for(Appointment__c appointment : formattedSlots){
                    system.debug('slotss'+(appointment.Slots__c).substringBefore('-'));
                    if((appointment.Slots__c).substringBefore('-') >= now){
                        appointmntObj.add(appointment);
                    }
                }
                system.debug('appointmntObj== '+appointmntObj);
                appointmntSlotLst = Database.Query(Query);
            }else{
                QueryStr = Query + ' WHERE Appointment_Date__c = ' +
                                (DateTime.newInstance(objDate.year(),objDate.month(),objDate.day())).format('YYYY-MM-dd')
                            +' AND Process_Name__c = \'Collections\''
                            +' AND Sub_Process_Name__c = \'Head Office Appointment\''
                            +' AND id NOT IN ' + getIdListForQuery(appLst) 
                            +' ORDER BY Slots__c, Time_Slot_Sub__c';
                Query +=    ' WHERE Appointment_Date__c >= ' +
                                (DateTime.newInstance(((System.Today()).addDays(2)).year(), ((System.Today()).addDays(2)).month(), ((System.Today()).addDays(2)).day())).format('YYYY-MM-dd')
                            +' AND Process_Name__c = \'Collections\''
                            +' AND Sub_Process_Name__c = \'Head Office Appointment\''
                            +' AND id NOT IN ' + getIdListForQuery(appLst) 
                            +' ORDER BY Slots__c, Time_Slot_Sub__c,Appointment_Date__c';

                appointmntObj = Database.Query(QueryStr);
                appointmntSlotLst = Database.Query(Query);
            }
        }
        else if(strSelectedProcess == RENTAL_POOL) {
            if(objDate == System.Today() || objDate == System.Today().addDays(1)){
                if(objDate == System.Today()){
                    objDate = (System.Today()).addDays(1);
                }
                QueryStr = Query + ' WHERE Appointment_Date__c = ' +
                                (DateTime.newInstance(objDate.year(),objDate.month(),objDate.day())).format('YYYY-MM-dd')
                        +' AND Process_Name__c = \'Rental Pool\''
                        +' AND Sub_Process_Name__c = \'Head Office Appointment\''
                        +' AND id NOT IN ' + getIdListForQuery(appLst) ;                        
            
                Query +=    ' WHERE Appointment_Date__c >= ' +
                                (DateTime.newInstance(((System.Today()).addDays(2)).year(), ((System.Today()).addDays(2)).month(), ((System.Today()).addDays(2)).day())).format('YYYY-MM-dd')
                            +' AND Process_Name__c = \'Rental Pool\''
                            +' AND Sub_Process_Name__c = \'Head Office Appointment\''
                            +' AND id NOT IN ' + getIdListForQuery(appLst) ;
                formattedSlots = Database.Query(QueryStr);
                for(Appointment__c appointment : formattedSlots){
                    system.debug('slotss'+(appointment.Slots__c).substringBefore('-'));
                    if((appointment.Slots__c).substringBefore('-') >= now){
                        appointmntObj.add(appointment);
                    }
                }
                system.debug('appointmntObj== '+appointmntObj);
                appointmntSlotLst = Database.Query(Query);
            }else{
                QueryStr = Query + ' WHERE Appointment_Date__c = ' +
                                (DateTime.newInstance(objDate.year(),objDate.month(),objDate.day())).format('YYYY-MM-dd')
                            +' AND Process_Name__c = \'Rental Pool\''
                            +' AND Sub_Process_Name__c = \'Head Office Appointment\''
                            +' AND id NOT IN ' + getIdListForQuery(appLst) 
                            +' ORDER BY Slots__c, Time_Slot_Sub__c'; 
                Query +=    ' WHERE Appointment_Date__c >= ' +
                                (DateTime.newInstance(((System.Today()).addDays(2)).year(), ((System.Today()).addDays(2)).month(), ((System.Today()).addDays(2)).day())).format('YYYY-MM-dd')
                            +' AND Process_Name__c = \'Rental Pool\''
                            +' AND Sub_Process_Name__c = \'Head Office Appointment\''
                            +' AND id NOT IN ' + getIdListForQuery(appLst) 
                            +' ORDER BY Slots__c, Time_Slot_Sub__c,Appointment_Date__c' ;
                system.debug('==== QueryStr ===='+ QueryStr);
                appointmntObj = Database.Query(QueryStr);
                appointmntSlotLst = Database.Query(Query);
            }
        }
        else if(strSelectedProcess == MORTGAGE) {
            if(objDate == System.Today() || objDate == System.Today().addDays(1)){
                if(objDate == System.Today()){
                    objDate = (System.Today()).addDays(1);
                }
                QueryStr = Query + ' WHERE Appointment_Date__c = ' +
                                (DateTime.newInstance(objDate.year(),objDate.month(),objDate.day())).format('YYYY-MM-dd')
                        +' AND Process_Name__c = \'Mortgage\''
                        +' AND Sub_Process_Name__c = \'Head Office Appointment\''
                        +' AND id NOT IN ' + getIdListForQuery(appLst) ;                        
            
                Query +=    ' WHERE Appointment_Date__c >= ' +
                                (DateTime.newInstance(((System.Today()).addDays(2)).year(), ((System.Today()).addDays(2)).month(), ((System.Today()).addDays(2)).day())).format('YYYY-MM-dd')
                            +' AND Process_Name__c = \'Mortgage\''
                            +' AND Sub_Process_Name__c = \'Head Office Appointment\''
                            +' AND id NOT IN ' + getIdListForQuery(appLst) ;
                formattedSlots = Database.Query(QueryStr);
                for(Appointment__c appointment : formattedSlots){
                    system.debug('slotss'+(appointment.Slots__c).substringBefore('-'));
                    if((appointment.Slots__c).substringBefore('-') >= now){
                        appointmntObj.add(appointment);
                    }
                }
                system.debug('appointmntObj== '+appointmntObj);
                appointmntSlotLst = Database.Query(Query);
            }else{
                QueryStr = Query + ' WHERE Appointment_Date__c = ' +
                                (DateTime.newInstance(objDate.year(),objDate.month(),objDate.day())).format('YYYY-MM-dd')
                            +' AND Process_Name__c = \'Mortgage\''
                            +' AND Sub_Process_Name__c = \'Head Office Appointment\''
                            +' AND id NOT IN ' + getIdListForQuery(appLst) 
                            +' ORDER BY Slots__c, Time_Slot_Sub__c,Appointment_Date__c'; 
                Query +=    ' WHERE Appointment_Date__c >= ' +
                                (DateTime.newInstance(((System.Today()).addDays(2)).year(), ((System.Today()).addDays(2)).month(), ((System.Today()).addDays(2)).day())).format('YYYY-MM-dd')
                            +' AND Process_Name__c = \'Mortgage\''
                            +' AND Sub_Process_Name__c = \'Head Office Appointment\''
                            +' AND id NOT IN ' + getIdListForQuery(appLst) 
                            +' ORDER BY Slots__c, Time_Slot_Sub__c,Appointment_Date__c';

                appointmntObj = Database.Query(QueryStr);
                appointmntSlotLst = Database.Query(Query);
            }
        }
        else{
            if(objDate == System.Today() || objDate == System.Today().addDays(1)){
                if(objDate == System.Today()){
                    objDate = (System.Today()).addDays(1);
                }
                QueryStr = Query + ' WHERE Appointment_Date__c = ' +
                                (DateTime.newInstance(objDate.year(),objDate.month(),objDate.day())).format('YYYY-MM-dd')
                        +' AND Process_Name__c = \'Generic\''
                        +' AND Sub_Process_Name__c = \'Head Office Appointment\''
                        +' AND id NOT IN ' + getIdListForQuery(appLst) ;                        
            
                Query +=    ' WHERE Appointment_Date__c >= ' +
                                (DateTime.newInstance(((System.Today()).addDays(2)).year(), ((System.Today()).addDays(2)).month(), ((System.Today()).addDays(2)).day())).format('YYYY-MM-dd')
                            +' AND Process_Name__c = \'Generic\''
                            +' AND Sub_Process_Name__c = \'Head Office Appointment\''
                            +' AND id NOT IN ' + getIdListForQuery(appLst) ;
                formattedSlots = Database.Query(QueryStr);
                for(Appointment__c appointment : formattedSlots){
                    system.debug('slotss'+(appointment.Slots__c).substringBefore('-'));
                    if((appointment.Slots__c).substringBefore('-') >= now){
                        system.debug('slot'+(appointment.Slots__c));
                        appointmntObj.add(appointment);
                    }
                }
                system.debug('appointmntObj== '+appointmntObj);
                appointmntSlotLst = Database.Query(Query);
            }else{
                QueryStr = Query + ' WHERE Appointment_Date__c = ' +
                                (DateTime.newInstance(objDate.year(),objDate.month(),objDate.day())).format('YYYY-MM-dd')
                            +' AND Process_Name__c = \'Generic\''
                            +' AND Sub_Process_Name__c = \'Head Office Appointment\''
                            +' AND id NOT IN ' + getIdListForQuery(appLst) 
                            +' ORDER BY Slots__c, Time_Slot_Sub__c,Appointment_Date__c'; 
                Query +=    ' WHERE Appointment_Date__c >= ' +
                                (DateTime.newInstance(((System.Today()).addDays(2)).year(), ((System.Today()).addDays(2)).month(), ((System.Today()).addDays(2)).day())).format('YYYY-MM-dd')
                            +' AND Process_Name__c = \'Generic\''
                            +' AND Sub_Process_Name__c = \'Head Office Appointment\''
                            +' AND id NOT IN ' + getIdListForQuery(appLst) 
                            +' ORDER BY Slots__c, Time_Slot_Sub__c,Appointment_Date__c';            
                system.debug('==== QueryStr ===='+ QueryStr);
                 system.debug('==== Query===='+ Query);
                appointmntObj = Database.Query(QueryStr);
                appointmntSlotLst = Database.Query(Query);
            }
        }
        system.debug('==== appointmntObj ===='+ appointmntObj);
        system.debug('==== appointmntSlotLst ===='+ appointmntSlotLst);
                system.debug('==== appointmntSlotLst size ===='+ appointmntSlotLst.size());

         if(appointmntObj != null && !appointmntObj.isEmpty()){
            for(Appointment__c ObjApp: appointmntObj ){
                appointmntSlotSet.add(ObjApp.Slots__c);
            }
             system.debug('==== appointmntSlotSet===='+ appointmntSlotSet);
            // 
        }
        
        else if(strSelectedSubProcess != 'Unit Viewing' && strSelectedSubProcess != 'Key Handover'
             && strSelectedSubProcess != 'Unit Viewing or Key Handover' && strSelectedSubProcess !='Documentation'){
            system.debug('strSelectedSubProcess===='+strSelectedSubProcess);
            apWrapper.error='No Appointment slots available for '+(DateTime.newInstance(objDate.year(),objDate.month(),objDate.day())).format('YYYY-MM-dd') +' Date.Please try for some other day.';
        }
        if(appointmntSlotSet!= null && !appointmntSlotSet.isEmpty()){
            for(string setElement : appointmntSlotSet){
               
                //slotLst.add(setElement);
                apWrapper.todaySlotsList.add(setElement);
            }
           
        }
        
         if(appointmntSlotLst!= null && !appointmntSlotLst.isEmpty()){
          list<String>slotsList=new list<String>();
                list<String>slotsSet=new list<String>();
            for(Appointment__c ObjApp:  appointmntSlotLst){
               
               
                slotsSet.add(string.valueof(ObjApp.Appointment_Date__c)+'('+ObjApp.Slots__c+')');
               
              // apWrapper.appointmentLIst.add(ObjApp);
            }
             apWrapper.AvailableSlotsList.AddAll(slotsSet);
           
        }
        
           system.debug('==== QueryStr last===='+ QueryStr);
                 system.debug('==== Query last===='+ Query);
        a.add(apWrapper);
       // system.debug('==== slotLst===='+ slotLst);
        return apWrapper;
        //return Query;
    
    }

    global static string getIdListForQuery(List<Id> lstId) {
        String ids = '';
        for (Id recordId : lstId) {
            ids += '\'' + recordId + '\'' + ', ';
        }
        return '(' + ids.removeEnd(', ') +')';
    }
  
      Global class AppointmentWrapper{
     // public list<Appointment__c>appointmentLIst{get;set;}
      public list<String>todaySlotsList{get;set;}
       public list<String>AvailableSlotsList{get;set;}
      public String error{get;set;}
     // public map<String,list<String>>apWrpMap{get;set;}
      
  }
  
 }