/****************************************************************************************************
 Name          : AP_RegBasicInformation
 Description   : Controller to get basic info for registraion  
 Created Date  : 17-09-2018                                                                        
 Created By    : ESPL                                                                              
 --------------------------------------------------------------------------------------------------
 VER   AUTHOR             DATE        COMMENTS                                                    
 1.0   Nikhil Pote                    Initial Draft.                                     
****************************************************************************************************/

public class AP_RegBasicInformation {
    public String BASE_URL = Site.getBaseSecureUrl();
    public List<String> LOI_ProfilesList = Label.LOI_Profiles.split(',');
    public Id arRTId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get(
        'Agent Registration').getRecordTypeId();
    public Id auRTId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get(
        'Agent Update').getRecordTypeId();
    public NSIBPM__Service_Request__c objSR{get; set;}
    public User currentUser;
    public String issueDateVar          {get; set;}
    public String expDateVar            {get; set;}
    public String licExp                {get; set;}
    public String doincorp              {get; set;} 
    public String reraExpDate              {get; set;} 
    public String agencyType              {get; set;}
    public String primaryLang              {get; set;}
    public String saveProgressVal              {get; set;}
    public String redirectToPageName {get; set;}
    public String sdoi {get; set;}
    public String stld {get; set;}
    public String reraDate {get; set;}
    public String issueDate {get; set;}
    public String issueExpDate {get; set;}
    public String validate {get;set;}
    public String accountId {get;set;}
    public String recordType     {get;set;}
    public List<String> selectedCOS{get;set;}
    public List<String> pickList;
    public String strSRID{get;set;}
    private Set<String> countriesOfSale;
    


    public AP_RegBasicInformation() {
        selectedCOS = new List<String> ();
        countriesOfSale = new Set<String>();
        objSR = new NSIBPM__Service_Request__c(Agency_Type__c = 'Corporate', RecordTypeId = arRTId);
        recordType = 'AR';
        for (User objUsr : [ SELECT Id, Name, Profile.Name, Profile.UserLicense.Name, ContactId, 
                                    Contact.Email, Contact.AccountId, Contact.FirstName, Contact.LastName, 
                                    Contact.Account.Website, Contact.Account.Agency_Type__c
                               FROM User
                              WHERE Id = :UserInfo.getUserId()
        ]) {
            currentUser = objUsr;
        }
       

        for(NSIBPM__Service_Request__c varSR : [ SELECT Country_of_Sale__c 
                                                   FROM NSIBPM__Service_Request__c 
                                                  LIMIT 10000
        ]) {
            if(varSR.Country_of_Sale__c != null){
                countriesOfSale.add(varSR.Country_of_Sale__c );
            }
        }
        system.debug('countriesOfSale' + countriesOfSale);

        if(apexpages.currentPage().getParameters().get('Id') != null) {
            strSRID = apexpages.currentPage().getParameters().get('Id');
            String srQuery = UtilityQueryManager.getAllFields(NSIBPM__Service_Request__c.getsObjectType().getDescribe());
            srQuery += ' WHERE Id = \'' + strSRID + '\'';
            for (NSIBPM__Service_Request__c sr : Database.query(srQuery)) {
                objSR = sr;
                if (objSR.NSIBPM__Record_Type_Name__c == 'Agent_Registration') {
                    recordType = 'AR';
                } else {
                    recordType = 'AU';
                }
            }
        } else if(apexpages.currentPage().getParameters().get('accId') != null) {
            accountId =  apexpages.currentPage().getParameters().get('accId');
            objSR = prePopulateServiceRequest(objSR, accountId, 'Agent_Update');
            for (NSIBPM__SR_Template__c srTemplate : [ SELECT Id FROM NSIBPM__SR_Template__c 
                                                        WHERE NSIBPM__SR_RecordType_API_Name__c= 'Agent_Update'
                                                          AND Name = 'Agent Update' 
                                                        LIMIT 1
            ]) {
                objSR.NSIBPM__SR_Template__c = srTemplate.Id;
            }
            if (objSR.RERA_Expiry_Date__c == NULL)
            objSR.RERA_Expiry_Date__c = System.Now().Date().addYears (10);
            objSR.RecordTypeId = auRTId;
            objSR.NSIBPM__Customer__c = accountId;
            objSR.Filled_Page_Ids__c = 'Basic-01,Address-03,Company-05,Docs-06';
            if(objSR.Beneficiary_Name__c != null &&
                objSR.Bank_Name__c != null &&
                objSR.Bank_Account_Currency__c != null &&
                objSR.Bank_Address__c != null &&
                objSR.Bank_Branch__c != null &&
                objSR.Bank_City__c != null &&
                objSR.Account_Details_Country__c != null &&
                objSR.IBAN_IFSC_Swift_SORT__c != null){
                    objSR.Filled_Page_Ids__c += ',Bank-04';
            }
            
            recordType = 'AU';
        }

        

        system.debug('objSR' + objSR);
        system.debug('objSR' + objSR.NSIBPM__Customer__c);
        system.debug('objSR' + objSR.Agent_Registration_Type__c);
        if(objSR != null) {
            //if (objSR.SR_Owner_Profile__c ==  'Damac Agent Portal Profile') {
            if (objSR.SR_Owner_Profile__c ==  'Customer Community - Auth + Admin') {
                List<Group> agentAdminQueueList = [SELECT Id FROM Group 
                                                    WHERE Type = 'Queue' 
                                                      AND DeveloperName = 'Agent_Admin_Team']; 
                if (agentAdminQueueList != null && !agentAdminQueueList.isEmpty()) {
                    objSR.OwnerId = agentAdminQueueList[0].Id;
                }
            }
             system.debug('objSR.OwnerId >>> ' + objSR.OwnerId);
            if(String.isNotBlank(objSR.Country_of_Sale__c)){
                 system.debug('objSR.Country_of_Sale__c' + objSR.Country_of_Sale__c);
                List<String> tempList = (objSR.Country_of_Sale__c).split(';');
                system.debug('tempList' + tempList);
                selectedCOS = tempList;
                system.debug('selectedCOS' + selectedCOS);
            }
            String strCity = objSR.Country_of_Sale__c;
            if(objSR.Date_of_Incorporation__c != NULL){
                sdoi = getDate(objSR.Date_of_Incorporation__c);
            }
            if(objSR.Trade_License_Expiry_Date__c != NULL){
                stld = getDate(objSR.Trade_License_Expiry_Date__c);
            }
            if(objSR.RERA_Expiry_Date__c != NULL){
                reraDate = getDate(objSR.RERA_Expiry_Date__c);
            }
            if(objSR.ID_Issue_Date__c != NULL){
                issueDate = getDate(objSR.ID_Issue_Date__c);
            }
            if(objSR.ID_Expiry_Date__c != NULL){
                issueExpDate = getDate(objSR.ID_Expiry_Date__c);
            }
            /*if (objSR.NSIBPM__Customer__c != null && objSR.Agent_Registration_Type__c != 'LOI') {
                recordType = 'AU';
            } else {
                recordType = 'AR';
            }*/
        } else {
            system.debug('NSIBPM__Service_Request__c');
        }

    }

    public void moveAmendmentsToSR(Id pSRId, Id pInquiryId) {
        system.debug('---pInquiryId>>>>>>  ' + pInquiryId + 'pSRId>>>>' + pSRId);
        if (pSRId != null && pInquiryId != null) {
            List<Amendment__c> inquiryAmendmentList = new List<Amendment__c>();
            inquiryAmendmentList = [SELECT Id, Inquiry__c FROM Amendment__c WHERE Inquiry__c = :pInquiryId];
            if (!inquiryAmendmentList.isEmpty()) {
                for (Amendment__c inquiryAmd : inquiryAmendmentList) {
                    inquiryAmd.Service_Request__c = pSRId;
                }
                update inquiryAmendmentList;
            }
        }
    }

    /**
     * [prePopulateServiceRequest - Prepopulate SR fields  with Account details previously saved]
     * @param  SR        [Service Request]
     * @param  AccountId [Id of Account - Customer__c]
     * @param  RTName    [RecordType Name - to copy RT specific fields]
     * @return           [description]
     */
    public NSIBPM__Service_Request__c prePopulateServiceRequest(
        NSIBPM__Service_Request__c SR, 
        Id AccountId, 
        String RTName
    ){
        List<Account_SR_Field_Mapping__c> CS = Account_SR_Field_Mapping__c.getAll().values();
        List<Account> accList = new List<Account>();
        String UAETaxRegNum;
        Date UAETaxRegDate;
        accList  = getAccInfo(AccountId, RTName);
        if (accList.size() > 0 ) {
            Agent_Site__c[] AgSite = [SELECT Tax_Registration_Number__c,Registration_Certificate_Date__c 
                                        FROM Agent_Site__c 
                                       WHERE Agency__c=:AccountId 
                                         AND Name = 'UAE'
            ];
            if (AgSite.Size() > 0) {
                UAETaxRegNum = AgSite[0].Tax_Registration_Number__c;
                UAETaxRegDate = AgSite[0].Registration_Certificate_Date__c;
                system.debug('UAETaxRegNumber>>>>'+UAETaxRegNum);
                system.debug('UAETaxRegDate>>>>'+UAETaxRegDate);
                if(UAETaxRegNum != null && UAETaxRegDate != null){
                    if (SR.UAE_Tax_Registration_Number__c == null) {
                        SR.UAE_Tax_Registration_Number__c = UAETaxRegNum;
                    }
                    if(SR.VAT_Registration_Certificate_Date__c == null) {
                        SR.VAT_Registration_Certificate_Date__c = UAETaxRegDate;
                    }
                }
            }
            String agencyType = SR.Agency_type__c != null ? SR.Agency_type__c : (accList[0].Agency_Type__c != null ? accList[0].Agency_Type__c : '');
            system.debug('agencyType>>> = ' + agencyType);
            if(agencyType != null && agencyType != ''){
                for(Account_SR_Field_Mapping__c mapping : CS){
                    if(mapping.RecordType_Name__c.equals(RTName) ){
                        if(agencyType == 'Corporate'){
                            if(!mapping.Is_Person_Account__c){
                                system.debug('agencyType>>> =1 ' + SR.get(mapping.SR_Field__c) );
                                system.debug('agencyType>>> =2 ' + SR.get(mapping.SR_Field__c));
                                system.debug('agencyType>>> =3 ' + SR.Id);
                                SR.put(mapping.SR_Field__c, ((SR.get(mapping.SR_Field__c) != NULL && SR.get(mapping.SR_Field__c) !='')  || SR.Id != NULL ) ? SR.get(mapping.SR_Field__c) :  accList[0].get(mapping.Account_Field__c));
                            //SR.put('VAT_Registration_Certificate_Date__c','');
                            //SR.put('UAE_Tax_Registration_Number__c','');
                            }
                        } else{
                            if(mapping.Is_Person_Account__c || mapping.Is_Common_to_All_RT__c)
                                SR.put(mapping.SR_Field__c,
                                ((SR.get(mapping.SR_Field__c) != NULL && SR.get(mapping.SR_Field__c) !='')  || SR.Id != NULL ) ? SR.get(mapping.SR_Field__c) :  accList[0].get(mapping.Account_Field__c));
                        }
                    }
                }
            }
        }
        system.debug('### Service Request = '+SR);
        return SR;
    }


    public List<Account> getAccInfo(ID AccountId, String RTName){
        List<Account_SR_Field_Mapping__c> CS = Account_SR_Field_Mapping__c.getAll().values();
        List<Account> accList = new List<Account>();
        Map<string,integer> mpuniquesfields = new Map<string,integer>();
        String accQuery = 'SELECT Id,';
        for(Account_SR_Field_Mapping__c mapping : CS)
        {
            if(mapping.RecordType_Name__c.equals(RTName) && mapping.Account_Field__c.tolowercase()!='id'){
                mpuniquesfields.put(mapping.Account_Field__c,1);
            }
        }
        List<string> uniqFields = new List<string>();
        uniqFields.addall(mpuniquesfields.keyset());
        accQuery += string.join(uniqFields, ',');
        accQuery = accQuery.removeEnd(',');
        accQuery += ' FROM Account';
        accQuery += ' WHERE Id = :AccountId LIMIT 1';
        System.debug('**'+accQuery);
        accList = database.query(accQuery);
        return accList;
    }

    /*
    * @return: Get date in desired format
    */
    public string getDate(Date dt /*DateTime dt*/){
        system.debug('dt'+dt);
        Date myDateVar = date.newinstance(dt.year(), dt.month(), dt.day());
        system.debug('myDate1'+myDateVar);
        string strVar = string.valueof(myDateVar);
        system.debug('strVar'+strVar);
        strVar= strVar.substring(0,10);
        system.debug('strVar'+strVar);
        string str1 = strVar.substring(5,7);
        system.debug('str1'+str1);
        string str2 = strVar.substring(8,10);
        system.debug('str2'+str2);
        string str3 = strVar.substring(0,4);
        system.debug('str3'+str3);
        strVar=str1+'/'+str2+'/'+str3;
        system.debug('stld'+strVar);
        return strVar;
    }

    /**
     * Method to get the Country of sale
     * Param : NA
     * Return Type : Selectoptions
     */
    public List<SelectOption> getCOS() {
        List<SelectOption> options = new List<SelectOption>();
        //options.add(new selectOption('', '- None -'));
        Schema.DescribeSObjectResult objSchema = NSIBPM__Service_Request__c.sObjectType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objSchema.fields.getmap();
        List<Schema.Picklistentry>fld =fieldmap.get('Country_of_Sale__c').getDescribe().getpicklistValues();
        System.debug('fld'+fld);
        pickList = new List<String>();

        for(Schema.Picklistentry pl : fld)
        {
            pickList.add(pl.getValue());
        }
        System.debug('pickList'+pickList);
        System.debug('pickList'+pickList);
        for(string u:pickList){            
            options.add(new SelectOption(u,u));
        }
        system.debug('options'+options);
        return options;
    }

    /*
    * @return: Redirect to Next section
    */
    public PageReference submitDetails(){ 
        system.debug('issueDateVar------------'+issueDateVar);
        system.debug('redirectToPageName------------'+redirectToPageName);        
        system.debug('expDateVar------------'+expDateVar);
        system.debug('reraExpDate------------'+reraExpDate);
        system.debug('licExp------------'+licExp);
        system.debug('doincorp------------'+doincorp);
        
        system.debug('agencyType---------'+agencyType);
        system.debug('primaryLang---------'+primaryLang);
        system.debug('saveProgressVal---------'+saveProgressVal);        
        system.debug('strSRID--------------'+strSRID);
        system.debug('selectedCOS--------------'+selectedCOS);
        system.debug('String--------------'+String.join(selectedCOS,'; '));
        system.debug('objSR.recordType-------------'+recordType);
        
        if (objSR.RecordTypeId == arRTId) {
            for (NSIBPM__SR_Template__c srTemplate : [ SELECT Id FROM NSIBPM__SR_Template__c 
                                                WHERE NSIBPM__SR_RecordType_API_Name__c= 'Agent_Registration'
                                                    AND Name = 'Agent Registration' 
                                                LIMIT 1
            ]) {
                objSR.NSIBPM__SR_Template__c = srTemplate.Id;
            }
            recordType = 'AR';
        }
        
        system.debug(' objSR.NSIBPM__SR_Template__c-------------'+ objSR.NSIBPM__SR_Template__c);
        if(!selectedCOS.isEmpty()){
            objSR.Country_of_Sale__c = String.join(selectedCOS,'; ');            
        } else {
            validate = 'Please enter Country of Sale';
            return null;
        }
        if(String.isnotBlank(expDateVar)){
            String[] formatdate = expDateVar.split('/');
            Date formatissueDate = Date.newInstance(Integer.valueof(formatdate[2].trim()),Integer.valueof(formatdate[0].trim()),Integer.valueof(formatdate[1].trim()));
            objSR.ID_Expiry_Date__c = formatissueDate;
        }
        if(String.isnotBlank(issueDateVar)){
            String[] formatdate = issueDateVar.split('/');
            Date formatissueDate = Date.newInstance(Integer.valueof(formatdate[2].trim()),Integer.valueof(formatdate[0].trim()),Integer.valueof(formatdate[1].trim()));
            objSR.ID_Issue_Date__c = formatissueDate;
        }
        if(String.isnotBlank(licExp)){
            String[] formatdate = licExp.split('/');
            Date formatissueDate = Date.newInstance(Integer.valueof(formatdate[2].trim()),Integer.valueof(formatdate[0].trim()),Integer.valueof(formatdate[1].trim()));
            objSR.Trade_License_Expiry_Date__c = formatissueDate;
        }       
        if(String.isnotBlank(doincorp)){
            String[] formatdate = doincorp.split('/');
            Date formatissueDate = Date.newInstance(Integer.valueof(formatdate[2].trim()),Integer.valueof(formatdate[0].trim()),Integer.valueof(formatdate[1].trim()));
            objSR.Date_of_Incorporation__c = formatissueDate;
        }
        if(String.isnotBlank(reraExpDate) && objSR.City_Of_Incorporation_New__c=='Dubai' && objSR.Country_Of_Incorporation_New__c=='United Arab Emirates' && objSR.Agency_Corporate_Type__c=='Real Estate'){
            String[] formatdate = reraExpDate.split('/');
            Date formatissueDate = Date.newInstance(Integer.valueof(formatdate[2].trim()),Integer.valueof(formatdate[0].trim()),Integer.valueof(formatdate[1].trim()));
            objSR.RERA_Expiry_Date__c = formatissueDate;
        }
        if(String.isnotBlank(agencyType)){
            objSR.Corporate_Agency__c = agencyType;
        }
        if(String.isnotBlank(primaryLang)){
            objSR.Primary_Language__c = primaryLang;
        }
        String strFilledPageId;
        system.debug('pageId'+objSR.filled_page_ids__c);
        if(String.isnotBlank(objSR.filled_page_ids__c)){
            system.debug('in if');
            strFilledPageId = objSR.filled_page_ids__c;
            if(!strFilledPageId.contains('Basic-01') ) {
                objSR.filled_page_ids__c = objSR.filled_page_ids__c +',Basic-01';   
            } 
        } else {
            system.debug('in else');
            objSR.filled_page_ids__c = 'Basic-01';  
        }

        system.debug('objSR++----->'+objSR);
        system.debug('---LOI_ProfilesList>>  ' + LOI_ProfilesList);
        system.debug('---userProfileName>>  ' + currentUser.Profile.Name);
        system.debug('---objSRAgent_Registration_Type__c>>  ' + objSR.Agent_Registration_Type__c);  
        system.debug('---objSR.Country_of_Sale__c>>  ' + objSR.Country_of_Sale__c);  
        try {   
            PageReference pg;
            if(LOI_ProfilesList.contains(currentUser.Profile.Name)
                && objSR.RecordTypeId == arRTId
                && objSR.Agent_Registration_Type__c != 'Basic'
            ) {
                objSR.LOI_Profile__c = true;
                if ( !objSR.Country_of_Sale__c.contains('UK') 
                    && !objSR.Country_of_Sale__c.contains('KSA')
                ) {
                    objSR.Agent_Registration_Type__c = 'LOI';
                } else {
                    objSR.Agent_Registration_Type__c = '';
                }
            }
            // 1.3 CRAIG 04/09/2018
            system.debug('---objSR 1085 before>>>>>>  ' + objSR); 
            upsert objSR;
            system.debug('---objSR 1088 after>>>>>>  ' + objSR);


            system.debug('redirectToPageName--------------'+redirectToPageName);

            //if(String.isBlank(saveProgressVal)) {
                

            if(String.isBlank(redirectToPageName)) {
                if(objSR.Agency_Type__c == 'Corporate' 
                    && (objSR.City_Of_Incorporation_New__c == 'Dubai' ||
                        objSR.City_Of_Incorporation_New__c == 'Abu Dhabi')
                    && objSR.Country_of_Sale__c.contains('UAE')
                    && objSR.Agent_Registration_Type__c != 'LOI'
                ){
                    pg = Page.AP_VATConfirmation;
                } else {
                    pg = Page.AP_AddressInformation;

                }
            } else {
                pg = new PageReference(BASE_URL+'/'+redirectToPageName);
            }
            pg.getParameters().put('id',objSR.id);
        /*} else {
            validate = 'Your information is saved successfully';
            return null;
        }*/
            system.debug('pg----------------------'+pg);
            return pg; 
           
                
        } catch(Exception e) {
            system.debug('error'+e);
            validate = e.getdmlMessage(0);
            system.debug('validate >>> ' + validate);
            //ApexPages.add
            return null;
        }
    }
}