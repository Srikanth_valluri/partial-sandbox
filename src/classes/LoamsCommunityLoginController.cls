public without sharing class LoamsCommunityLoginController {

    public AccountSearchModel   model               {get; set;}
    public String               username            {get; set;}
    public String               password            {get; set;}
    public String               toastrError         {get; set;}
    public Boolean              showPartyId         {get; set;}
    public Boolean              showInfo            {get; set;}
    public String               partyId             {get; set;}
    public String               firstName           {get; set;}
    public String               lastName            {get; set;}
    public String               email               {get; set;}
    public String               phone               {get; set;}
    public String               confirmPassword     {get; set {
                                                        confirmPassword = value == NULL ? value : value.trim();
                                                    }}
    public String               communityNickname   {get; set {
                                                        communityNickname = value == NULL ? value : value.trim();
                                                    }}
    public List<Network>        lstNetwork          = new List<Network> ();
    public List<User>           lstUser             = new List<User>();

    public LoamsCommunityLoginController() {
        showPartyId = false;
        showInfo = true;
        toastrError = '';
        model =  new AccountSearchModel();
    }

    public PageReference authenticateUser() {
        PageReference pageRef = Apexpages.currentPage();
        List<Network> lstNetwork = [SELECT  Id
                                         , Name
                                     FROM  Network
                                    WHERE  Name =: Label.Network_Name];
        if (!lstNetwork.isEmpty()) {
            pageRef = new PageReference(Network.getLoginUrl(lstNetwork[0].Id));
        }

        Map<String, String> pageParams = Apexpages.currentPage().getParameters();
        System.debug('pageParams = ' + pageParams);
        username = pageParams.get('username');
        password = pageParams.get('password');
        String action = pageParams.get('action');
        if ('resetPassword'.equalsIgnoreCase(action)) {
            Boolean success = Site.forgotPassword(username);
            System.debug('reset password success = ' + success);
            //pageRef.getParameters().put('username', username);
            pageRef.getParameters().put('message', 'An email is sent to your registered email Id');
            return pageRef;
        }
        if (String.isNotBlank(username) && String.isNotBlank(password)) {
            password = Crypto.decryptWithManagedIV(
                'AES128',
                Crypto.generateDigest('MD5', Blob.valueOf(PaymentGateway__c.getInstance('LOAMS Portal').EncryptionKey__c)),
                EncodingUtil.convertFromHex(password)
            ).toString();
            System.debug('password = ' + password);
            PageReference fmHomePage = Site.login(username, password, ApexPages.currentPage().getParameters().get('startURL'));
            if (fmHomePage == NULL) {
                //username = username.left(2)+'xxx'+username.right(1);
                //pageRef.getParameters().put('username', username);
                pageRef.getParameters().put(
                    'error',
                    'Password is wrong! Please enter the correct password'
                );
            } else {
                return fmHomePage;
            }
        }
        return pageRef;
    }

    @testVisible
    private Boolean isValidPassword() {
        System.debug('model.password = ' + model.firstpassword);
        System.debug('model.confirmPassword = ' + model.confirmPassword);
        return model.firstpassword == model.confirmPassword;
    }

    /* Method to register a user if a account is available in salesforce with matching email and phone number */
    public PageReference registerUser() {
        toastrError = '';
        System.debug('registerUser');
        List<Contact> lstContact = new List<Contact>();
        Id accountId;
        Id contactId;

        /*if (!isValidPassword()) {
            toastrError = Label.Site.passwords_dont_match;
            return NULL;
        }*/
        if (model.partyId != NULL) {
            System.debug('model.partyId = ' + model.partyId);
            lstContact = [
                SELECT  Id
                        , Account.Party_ID__c
                        , AccountId
                FROM    Contact
                WHERE   Account.Party_ID__c = :model.partyId
                    AND (Is_Primary__c = true OR Account.IsPersonAccount = true)
            ];
            if (!lstContact.isEmpty() && lstContact.size() == 1) {
                accountId = lstContact[0].AccountId;
                partyId = lstContact[0].Account.Party_ID__c;
                contactId = lstContact[0].id;
            } else {
                toastrError = 'There is no account associated with this party Id. Please enter your registered Party Id. If you have any issues please contact DAMAC Customer Service , Email DAMAC Customer Service  atyourservice@damacproperties.com or Call +9714 2375000';
                System.debug('toastrError = ' + toastrError);
                return NULL;
            }
        } else {
            //System.debug('model.email = ' + model.email);
            //System.debug('model.phone = ' + model.phone);
            if (model.email != NULL && model.phone != NULL ) {
                lstContact = [
                    SELECT  Id
                            , Account.Party_ID__c
                            , AccountId
                    FROM    Contact
                   WHERE    (Account.Email__pc =: model.email
                             AND Account.Mobile_Phone_Encrypt__pc =: model.phone
                             AND Account.IsPersonAccount = true)
                        OR  (Account.Email__c =: model.email
                             AND Account.Mobile__c =: model.phone
                             AND Is_Primary__c = true)
                ];
            }
            if (lstContact.size() == 1 ) {
                accountId = lstContact[0].AccountId;
                partyId = lstContact[0].Account.Party_ID__c;
                contactId = lstContact[0].Id;
            }
            if (!lstContact.isEmpty() && lstContact.size() > 1 ) {
                lstContact  = [
                    SELECT  Id
                            , Account.Party_ID__c
                            , AccountId
                    FROM    Contact
                    WHERE   Firstname =:model.firstName
                        AND LastName =: model.lastName
                        AND ((Account.Email__pc =: model.email
                                AND Account.Mobile_Phone_Encrypt__pc =: model.phone
                                AND Account.IsPersonAccount = true)
                            OR (Account.Email__c =: model.email
                                AND Account.Mobile__c =: model.phone
                                AND Is_Primary__c = true))
                ];
                if (!lstContact.isEmpty() && lstContact.size() == 1) {
                    accountId = lstContact[0].AccountId;
                    partyId = lstContact[0].Account.Party_ID__c;
                    contactId = lstContact[0].Id;
                } else {
                    model.hasMulipleAccounts = true;
                    System.debug('enter party id');
                    toastrError = 'Please enter the Party id :';
                    showPartyId = true;
                    showInfo = false;
                    return NULL;
                }
            }
        }
        System.debug('model = ' + JSON.serializePretty(model));
        List<Profile> lstProfile;
        LoamsCommunitySettings__c loamsCommunitySettings = LoamsCommunitySettings__c.getInstance();
        System.debug('loamsCommunitySettings = ' + loamsCommunitySettings);
        System.debug('loamsCommunitySettings OwnerProfileName__c = ' + loamsCommunitySettings.OwnerProfileName__c);

        if (loamsCommunitySettings.OwnerProfileName__c != NULL) {
            lstProfile = [SELECT  Id, Name FROM Profile WHERE Name = :loamsCommunitySettings.OwnerProfileName__c];
        }
        System.debug('ContactId = ' + ContactId);
        User u = new User();
        u.Username = model.email;
        u.Email = model.email;
        u.FirstName = model.firstName;
        u.LastName = model.lastName;
        u.ContactId = ContactId;
        System.debug('u.ContactId = ' + u.ContactId);
        if (accountId == NULL) {
            toastrError = 'There is no account associated with the email address and Phone number. Please enter your registered email account and phone number . If you have any issues please contact DAMAC Customer Service , Email DAMAC Customer Service  atyourservice@damacproperties.com or Call +9714 2375000';
            return NULL;
        }
        u.CommunityNickname = partyId + 'name';
        System.debug('u.CommunityNickname = ' + u.CommunityNickname);

        if (lstProfile!= NULL && !lstProfile.isEmpty()) {
            u.ProfileId = lstProfile[0].id;
        } else {
            toastrError = 'Profile is not available Please contact CRE.';
            return NULL;
        }
        u.Phone = model.phone;
        u.MobilePhone = model.phone;
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_US';
        u.TimeZoneSidKey='America/Los_Angeles';
        u.Alias = partyId;
        u.EmailEncodingKey = 'UTF-8';

        String userId;
        try {
            userId = Site.createPortalUser(u, accountId, model.password, true);
            System.debug('userId = ' + userId);

            for(Apexpages.Message msg : ApexPages.getMessages()) {
                System.debug('msg.getDetail() = ' + msg.getDetail());
            }

            if (String.isNotBlank(userId)) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm,'An email has been sent to your registered Email Id. Please check your mail to login to Community.'));
                return NULL;
            }
        } catch(Site.ExternalUserCreateException ex) {
            System.debug('ExternalUserCreateException = ' + ex.getMessage());
            List<String> errors = ex.getDisplayMessages();
            for (String error : errors)  {
                toastrError = error;
            }
            return NULL;
        }
        catch (Exception excp) {
            toastrError = 'An error occurred while processing your request. Please try again on contact Site administrator';
            System.debug('Exception = ' + excp.getMessage());
            return NULL;
        }
        System.debug('userId = ' + userId);
        if (userId != NULL) {
            if (model.password != NULL && model.password.length() > 1) {
               // return Site.login(u.Username, model.password, ApexPages.currentPage().getParameters().get('startURL'));
            } else {
                PageReference page = System.Page.CommunitiesSelfRegConfirm;
                page.setRedirect(true);
               // return page;
            }
        }
        return NULL;
    }

    //Method to login into community through username or partyid
    public PageReference doLogin() {
        toastrError = '';
        String strUsername = '';
        System.debug('model.username = ' + model.username);
        if (model.username != NULL && model.username.contains('@')) {
            if (!checkUsername(model.username)) {
                toastrError = 'The entered username is wrong. Please enter a correct username/PartyId';
                return NULL;
            } else {
                strUsername = model.username;
            }
        } else {
            strUsername = fetchUsername(model.username);
            System.debug('strUsername = ' + strUsername);
            if (strUsername == NULL) {
                toastrError = 'Please enter a correct Party Id.';
                return NULL;
            }
        }
        Id networkId = Network.getNetworkId();
        List<Network> lstNetwork = [
            SELECT  Id
                    , Name
            FROM    Network
            WHERE   Name = 'Damac Customer Community Portal'
                AND Id = NULL
        ];
        if (!lstNetwork.isEmpty()) {
            networkId = lstNetwork[0].Id;
        }
        PageReference pageRef;
        if (strUsername != NULL && model.password != NULL) {
            //System.debug('site login');
            pageRef = Site.login(strUsername, model.password, ApexPages.currentPage().getParameters().get('startURL'));
            if (pageRef==NULL) {
                toastrError = 'Password is wrong, Please enter a correct password';
            }
        }
        //System.debug('pageRef'+pageRef);
        return pageRef;
    }

    // Method to reset the password
    public PageReference forgotPassword() {
        toastrError = '';
        Boolean success = false;
        String strUsername;
        System.debug('username = ' + model.username);
        if (model.username == NULL || String.isempty(model.username)) {
            toastrError = 'Please enter username';
            return NULL;
        }
        if (model.username != NULL) {
            if (model.username.contains('@')) {
                if (!checkUsername(model.username)) {
                    toastrError = 'The entered username is wrong. Please enter a correct username/PartyId';
                    return NULL;
                }
                username = model.username;
            } else {
                username = fetchUsername(model.username);
            }
            System.debug('username = ' + username);
            if (username!= NULL) {
                success = Site.forgotPassword(username);
                System.debug('success = ' + success);
                toastrError = 'An email is sent to your registered email Id';
            }
        }
        //PageReference pr = new PageReference(ApexPages.currentPage().getParameters().get('startURL'));
        //PageReference pr = new PageReference('https://devpro-servicecloudtrial-155c0807bf-1580afc5db1.cs86.force.com/Customer');
       // pr.setRedirect(true);
        if (success) {
            ApexPages.addMessage(new ApexPages.message(
                ApexPages.Severity.CONFIRM, 'An email is sent to your registered email id'
            ));
            //System.debug('pr = ' + pr);
        } else {
           toastrError = 'Please enter a valid username/PartyId.';
        }
        //System.debug('pr = ' + pr);
        return NULL;
    }

    //Method to fetch the username if ucer enters a party Id.
    public String fetchUsername(String strPartyId){
        List<User> lstUser = [
            SELECT  Id,
                    Username,
                    Email,
                    Contact.Account.Party_ID__c
            FROM    User
            WHERE   IsActive = TRUE
                AND Contact.Account.Party_ID__c = :strPartyId
        ];
        System.debug('lstUser = ' + lstUser);
        if (lstUser.isEmpty()) {
            return NULL;
        } else {
            return lstUser[0].Username;
        }
    }

    public Boolean checkUsername(String username) {
        System.debug('username = ' + username);
        List<User> lstUser = [SELECT Id,
                                      Username
                                 FROM User
                                WHERE Username=:username];
        if (lstUser.isEmpty()) {
            return false;
        } else {
            System.debug('model.username = ' + lstUser);
            return true;
        }
    }

    public PageReference gotoGuestPayment() {
        return Page.GuestMakePayment;
    }

    @RemoteAction
    public static String getOffersData() {
        return DamacDataCommunicator.getOffersData();
    }

    public class AccountSearchModel{
        public String firstName {set;get;}
        public String lastName {set;get;}
        public String email {set;get;}
        public String phone {set;get;}
        public String password {get; set {password = value == NULL ? value : value.trim(); } }
        public String confirmPassword {get; set { confirmPassword = value == NULL ? value : value.trim(); } }
        public String communityNickname {get; set { communityNickname = value == NULL ? value : value.trim(); } }
        public String partyId {set;get;}
        public Boolean hasMulipleAccounts  {get; set;}
        public Account chosenAccount {set;get;}
        public Boolean hasError = false;
        public String  errorMessage;
        public String username        {get;set;}
        public String password1 {get;set;}
        public String firstpassword {get;set;}
    }
}