/**
 *  Class Name:   HandoverController_CCTest
 *  Purpose:      Unit test class for Handover Controller
 *  Author:       Niranjani Mishra - ESPL
 *  Company:      ESPL
 */
 @isTest
 public with sharing class HandoverController_CCTest {
    public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';

    @testSetup
    private static void setupTestData() {
        insert new TriggerOnOffCustomSetting__c(Name = 'CallingListTrigger', OnOffCheck__c = true);
        insert new SNAG_URLs__c(Name = 'ABC', URL__c = 'avgttn');
    }

    private static void insertBookingUnitActiveStatus()
    {
        List<Booking_Unit_Active_Status__c> buActiveStatusList = new List<Booking_Unit_Active_Status__c>();

        Booking_Unit_Active_Status__c objBUActiveStatus = new Booking_Unit_Active_Status__c();
        //objBUActiveStatus.Name = strBookingUnitActiveStatus;
        objBUActiveStatus.Name = 'strBookingUnitActiveStatus';
        objBUActiveStatus.Status_Value__c = strBookingUnitActiveStatus;
        buActiveStatusList.add(objBUActiveStatus);

        insert buActiveStatusList;
    }
    @isTest(SeeAllData=false)
    static void init_test_scenario1(){
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();

        //insert Booking Unit Active Status custom setting record
        insertBookingUnitActiveStatus();

        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        List<User> LstUser = new List<User>();
        Id p = [select id from profile where name='Customer Community Login User(Use this)'].id;

        Contact con =[Select id, Name from Contact where Accountid = :objAccount.id];
        User u = new User();
        u.alias = 'test123';
        u.email='test123@noemail.com';
        u.emailencodingkey='UTF-8';
        u.lastname='Testing';
        u.languagelocalekey='en_US';
        u.localesidkey='en_US';
        u.profileid = p;
        u.country='United States';
        u.IsActive =true;
        u.ContactId = con.Id;
        u.timezonesidkey='America/Los_Angeles';
        u.username='tester@noemail.com';
        LstUser.add(u);
        insert LstUser;

        system.runAs(u){
            CustomerCommunityUtils.getcustomerAccountId();
        }
        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property data
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project1';
        objProperty.Property_Name__c = 'Test Property1';
        objProperty.Property_ID__c = 34311;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        Location__c objBuildingLocation = new Location__c();
        objBuildingLocation.Name = 'Test Building Name';
        objBuildingLocation.Location_Id__c = '123456';
        insert objBuildingLocation;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        objInventory.Building_Location__c = objBuildingLocation.Id;
        insert objInventory;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '747121';
          objBookingUnit.Re_Assigned__c = false;
          objBookingUnit.DEWA_Registration_Date__c = System.today();
          objBookingUnit.Empower_Registration_Date__c = System.today();
      }
      insert bookingUnitList;

      Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id handoverRecordTypeID = caseRecordTypes.get('Handover Parent').getRecordTypeId();

      Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , handoverRecordTypeID);
        //objCase.Name = 'Case Utility Registration';
        objCase.AccountId = CustomerCommunityUtils.customerAccountId;
        //objCase.ParentId = '';
        objCase.Pending_Amount__c = -30;
        objCase.Booking_Unit__c = bookingUnitList[0].Id;
        insert objCase;

        Case unitCase = objCase.clone(false, true);
        unitCase.RecordTypeId = caseRecordTypes.get('Handover').getRecordTypeId();
        unitCase.ParentId = objCase.Id;
        insert unitCase;

        system.debug('>>>>>>>>objCase.AccountId'+objCase.AccountId);
        List<SR_Attachments__c> objSRAttachments = new List<SR_Attachments__c> {
            new SR_Attachments__c(Name = 'Passport', Case__c = objCase.Id),
            new SR_Attachments__c(Name = 'DEWA or Utility Registration', Case__c = unitCase.Id),
            new SR_Attachments__c(Name = 'Empower or Utility Registration', Case__c = unitCase.Id)
        };
        insert objSRAttachments;



        Map <String,Schema.RecordTypeInfo> callingListRecordTypes = Calling_List__c.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id callingListRecordTypeID = callingListRecordTypes.get('Handover Calling List').getRecordTypeId();

        Calling_List__c objCL = new Calling_List__c();
        //objCL.Calling_List__c = '';
        objCL.Case__c = objCase.Id;
        objCL.RecordTypeId = callingListRecordTypeID;
        objCL.Account__c = objAccount.Id;
        objCL.Booking_Unit__c = bookingUnitList[0].Id;
        objCL.Documentation_Appointment_Date__c = System.today();
        objCL.Documentation_Appointment_Start_Time__c = '09:00';
        objCL.Documentation_Appointment_End_Time__c = '10:00 AM';
        objCL.Unit_Viewing_Date__c = System.today();
        objCL.Viewing_Start_Time__c = '09:00';
        objCL.Viewing_End_Time__c = '10:00:00 AM';
        objCL.Key_Handover_Appointment_Date__c = System.today();
        objCL.Key_Handover_Appointment_Start_Time__c = '09:00';
        objCL.Key_Handover_Appointment_Stop_Time__c = '10:00:00 AM';
        objCL.Appointment_Status__c = 'Completed';
        objCL.Party_ID__c = '123456';
        objCL.Call_Outcome__c = 'Recovery list';
        objCL.Registration_ID__c = '74712';
        insert objCL;
        List<Integer> ls1 = new List<Integer>();
        ls1.add(2017);
        ls1.add(10);
        ls1.add(10);


        List<EmailTemplate> templates = [
            SELECT Id, Subject FROM EmailTemplate WHERE Name = :HandoverController_CC.HO_TEMPLATE_NAME
        ];
        EmailTemplate template = templates[0];
        template.Subject = template.Subject == NULL
                            ? '' : template.Subject.replaceAll('\\{!(.*?)\\}', bookingUnitList[0].Unit_Name__c);

        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'atyourservice@damacgroup.com'];
        if ( owea.size() > 0 ) {
            message.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        message.subject = template.Subject;
        message.setHtmlBody('body');
        message.setSaveAsActivity(true);
        message.setWhatId(objAccount.Id);
        message.toAddresses = new String[] {'customer@damac.com'};
       // Messaging.SendEmailResult[] results = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {message});

        String data = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';

        ContentVersion cv = new ContentVersion();
        cv.ContentLocation = 'S';
        cv.PathOnClient = bookingUnitList[0].Unit_Name__c + ' Handover Pack.zip';
        cv.Title = bookingUnitList[0].Unit_Name__c + ' Handover Pack.zip';
        cv.VersionData = EncodingUtil.base64Decode(data);
        insert cv;
        cv = [select ContentDocumentId from ContentVersion where Id =: cv.Id];
        insert new ContentDocumentLink(
            LinkedEntityId = bookingUnitList[0].Id, ContentDocumentId = cv.ContentDocumentId, ShareType = 'V'
        );

        Test.startTest();
        Test.setCurrentPageReference(Page.Customer);
        ApexPages.currentPage().getParameters().put('view', 'handover');
        HandoverController_CC objClass = new HandoverController_CC();
        HandoverController_CC.fetchHandovers();
        HandoverController_CC.fetchHandoverEmail(objCase.id);
        HandoverController_CC.fetchCallingLists(objCase.id);
        HandoverController_CC.uploadDocument(objSRAttachments[1].id, 'Test.pdf', data, ls1, '123654');
        HandoverController_CC.fetchUnitDues(objCase.id);
        Test.stopTest();

    }

    @isTest(SeeAllData=false)
    static void init_test_scenario2(){
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();

        //insert Booking Unit Active Status custom setting record
        insertBookingUnitActiveStatus();

        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        List<User> LstUser = new List<User>();
        Id p = [select id from profile where name='Customer Community Login User(Use this)'].id;


        Contact con =[Select id, Name from Contact where Accountid = :objAccount.id];
        User u = new User();
        u.alias = 'test123';
        u.email='test123@noemail.com';
        u.emailencodingkey='UTF-8';
        u.lastname='Testing';
        u.languagelocalekey='en_US';
        u.localesidkey='en_US';
        u.profileid = p;
        u.country='United States';
        u.IsActive =true;
        u.ContactId = con.Id;
        u.timezonesidkey='America/Los_Angeles';
        u.username='tester@noemail.com';
        LstUser.add(u);
        insert LstUser;

        system.runAs(u){
            CustomerCommunityUtils.getcustomerAccountId();
        }
        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property data
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project1';
        objProperty.Property_Name__c = 'Test Property1';
        objProperty.Property_ID__c = 34311;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        Location__c objBuildingLocation = new Location__c();
        objBuildingLocation.Name = 'Test Building Name';
        objBuildingLocation.Location_Id__c = '123456';
        insert objBuildingLocation;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        objInventory.Building_Location__c = objBuildingLocation.Id;
        insert objInventory;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '747121';
          objBookingUnit.Re_Assigned__c = false;
          objBookingUnit.DEWA_Registration_Date__c = System.today();
          objBookingUnit.Empower_Registration_Date__c = System.today();
      }
      insert bookingUnitList;

      Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id handoverRecordTypeID = caseRecordTypes.get('Handover').getRecordTypeId();

      Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , handoverRecordTypeID);
        //objCase.Name = 'Case Utility Registration';
        objCase.AccountId = CustomerCommunityUtils.customerAccountId;
        //objCase.ParentId = '';
        objCase.Pending_Amount__c = -30;
        objCase.Booking_Unit__c = bookingUnitList[0].Id;
        insert objCase;
        system.debug('>>>>>>>>objCase.AccountId'+objCase.AccountId);
        SR_Attachments__c objSRAttachments = new SR_Attachments__c();
        objSRAttachments.Name = 'EMPOWER or Utility Registration';
        objSRAttachments.Case__c = objCase.Id;
        insert objSRAttachments;

        Map <String,Schema.RecordTypeInfo> callingListRecordTypes = Calling_List__c.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id callingListRecordTypeID = callingListRecordTypes.get('Handover Calling List').getRecordTypeId();

        Calling_List__c objCL = new Calling_List__c();
        //objCL.Calling_List__c = '';
        objCL.Case__c = objCase.Id;
        objCL.RecordTypeId = callingListRecordTypeID;
        objCL.Account__c = objAccount.Id;
        objCL.Booking_Unit__c = bookingUnitList[0].Id;
        objCL.Documentation_Appointment_Date__c = System.today();
        objCL.Documentation_Appointment_Start_Time__c = '09:00';
        objCL.Documentation_Appointment_End_Time__c = '10:00 AM';
        objCL.Unit_Viewing_Date__c = System.today();
        objCL.Viewing_Start_Time__c = '09:00';
        objCL.Viewing_End_Time__c = '10:00:00 AM';
        objCL.Key_Handover_Appointment_Date__c = System.today();
        objCL.Key_Handover_Appointment_Start_Time__c = '09:00';
        objCL.Key_Handover_Appointment_Stop_Time__c = '10:00:00 AM';
        objCL.Appointment_Status__c = 'Completed';
        objCL.Party_ID__c = '123456';
        objCL.Call_Outcome__c = 'Recovery list';
        objCL.Registration_ID__c = '74712';
        insert objCL;
        List<Integer> ls1 = new List<Integer>();
        ls1.add(2017);
        ls1.add(10);
        ls1.add(10);

        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        Test.startTest();
        Test.setCurrentPageReference(Page.Customer);
        ApexPages.currentPage().getParameters().put('view', 'handover');
        HandoverController_CC objClass = new HandoverController_CC();
        HandoverController_CC.fetchHandovers();
        HandoverController_CC.fetchHandoverEmail(NULL);
        HandoverController_CC.fetchHandoverEmail(objCase.id);
        HandoverController_CC.getHandoverPackForUnit(bookingUnitList[0].id);
        //HandoverController_CC.getUnitSnags(NULL);
        //HandoverController_CC.getUnitSnags('ABC/XYZ');
        HandoverController_CC.fetchCallingLists(objCase.id);
        //HandoverController_CC.fetchCallingLists(caseId);
        //HandoverController_CC.fetchUnitDues(objCase.id);
        HandoverController_CC.uploadDocument(objSRAttachments.id,'Test.pdf','ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9',ls1,'123654');
        Test.stopTest();

    }

    @isTest
    private static void niPaymentDataTest() {
        Test.startTest();
        System.assertEquals(NULL, HandoverController_CC.getNiPaymentData(NULL, NULL, NULL));
        Test.stopTest();
    }

    /*@isTest
    private static void fetchUnitDuesTest() {
        Test.startTest();
        System.assertEquals(0, HandoverController_CC.fetchUnitDues(0));
        Test.stopTest();
    }*/

    @isTest
    private static void getUnitSnagsTest() {

        Account customerAccount = TestDataFactoryFM.createAccount();
        insert customerAccount;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert sr;

        Booking__c  booking = TestDataFactoryFM.createBooking(customerAccount, sr);
        insert booking;

        Booking_Unit__c bookingUnit = TestDataFactoryFM.createBookingUnit(customerAccount, booking);
        bookingUnit.Unit_Name__c = 'ABC/XYZ';
        bookingUnit.Building_Name__c = 'ABC';
        insert bookingUnit;

        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id handoverRecordTypeID = caseRecordTypes.get('Handover').getRecordTypeId();

        Case objCase = TestDataFactory_CRM.createCase(customerAccount.Id , handoverRecordTypeID);
        objCase.Booking_Unit__c = bookingUnit.Id;
        insert objCase;

        SNAGs__c objSnag = new SNAGs__c();
        objSnag.SDCategory__c = 'test';
        objSnag.SDType__c = 'test';
        objSnag.SDType__c = 'test';
        objSnag.SD_Description__c = 'test';
        objSnag.User_Description__c = 'test';
        objSnag.User_Description__c = 'test';
        objSnag.CurrencyIsoCode = 'AED';
        objSnag.Booking_Unit__c = bookingUnit.id;
        objSnag.Case__c = objCase.id;
        insert objSnag;

        Test.setMock( HttpCalloutMock.class, new SnagHttpResponseGenerator(1));

        Test.startTest();
            HandoverController_CC.getUnitSnags(bookingUnit.Id, 'ABC/XYZ');
        Test.stopTest();
    }

    @isTest
    private static void getUnitSnagsTestNegative() {
        Account customerAccount = TestDataFactoryFM.createAccount();
        insert customerAccount;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert sr;

        Booking__c  booking = TestDataFactoryFM.createBooking(customerAccount, sr);
        insert booking;

        Booking_Unit__c bookingUnit = TestDataFactoryFM.createBookingUnit(customerAccount, booking);
        bookingUnit.Unit_Name__c = 'ABC/XYZ';
        bookingUnit.Building_Name__c = 'ABC';
        insert bookingUnit;

        Test.setMock( HttpCalloutMock.class, new SnagHttpResponseGenerator(1));

        Test.startTest();
            HandoverController_CC.getUnitSnags(NULL, 'ABC/XYZ');
            HandoverController_CC.getUnitSnags(bookingUnit.Id, 'ABC/XYZ');
        Test.stopTest();
    }

    @isTest
    private static void downloadUnitSnagsTest() {
        Account customerAccount = TestDataFactoryFM.createAccount();
        insert customerAccount;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert sr;

        Booking__c  booking = TestDataFactoryFM.createBooking(customerAccount, sr);
        insert booking;

        Booking_Unit__c bookingUnit = TestDataFactoryFM.createBookingUnit(customerAccount, booking);
        bookingUnit.Unit_Name__c = 'ABC/XYZ';
        bookingUnit.Building_Name__c = 'ABC';
        insert bookingUnit;

        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id handoverRecordTypeID = caseRecordTypes.get('Handover').getRecordTypeId();

        Case objCase = TestDataFactory_CRM.createCase(customerAccount.Id , handoverRecordTypeID);
        objCase.Booking_Unit__c = bookingUnit.Id;
        insert objCase;

        Test.startTest();
            HandoverController_CC.downloadUnitSnags(bookingUnit.Id,'ABC/XYZ');
        Test.stopTest();

    }

    @isTest
    private static void downloadUnitSnagsNullTest() {
        Account customerAccount = TestDataFactoryFM.createAccount();
        insert customerAccount;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert sr;

        Booking__c  booking = TestDataFactoryFM.createBooking(customerAccount, sr);
        insert booking;

        Booking_Unit__c bookingUnit = TestDataFactoryFM.createBookingUnit(customerAccount, booking);
        bookingUnit.Unit_Name__c = 'ABC/XYZ';
        bookingUnit.Building_Name__c = 'ABC';
        insert bookingUnit;

        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id handoverRecordTypeID = caseRecordTypes.get('Handover').getRecordTypeId();

        Case objCase = TestDataFactory_CRM.createCase(customerAccount.Id , handoverRecordTypeID);
        objCase.Booking_Unit__c = bookingUnit.Id;
        insert objCase;

        Test.startTest();
            HandoverController_CC.downloadUnitSnags(NULL,'ABC/XYZ');
        Test.stopTest();

    }

}