/*========================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
--------------------------------------------------------------------------------------------------------
1.0     |   01-10-2020      | Shubham Suryawanshi | Removed CM_Units, ByPassFMReminder__c, SC_From_RentalIncome__c check from service fee overdue check                                    
1.1     |   09-11-2020      | Anand Venkitakrishnan | Added a blocker for amenity booking if tenancy contract has expired
1.2     |   16-11-2020      | Anand Venkitakrishnan | Commented out amenity booking blocker temporarily to fix issue
1.3     |   17-12-2020      | Shubham Suryawanshi   | Added the condition-check for byPassing service charge overdue check for unit with Allow_Amenity_Booking__c flag marked as true
1.4     |   27-01-2021      | Shubham Suryawanshi   | Included paid amenities
--------------------------------------------------------------------------------------------------------
*******************************************************************************************************/   

@RestResource(urlMapping='/getAmenities/*')
global class GetAmenitiesByBookingUnit_API{

    public static final string NO_AMENITY_FOR_BUILDING = 'No amenities available for the selected building';
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong',
        7 => 'Tenancy contract expired'
    };
    
    @HttpPost
    global static FinalReturnWrapper GetAmenitiesForUnit() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_data objData = new cls_data();
        cls_meta_data objMeta = new cls_meta_data();
        List<cls_get_amenity_list> listAmenities = new List<cls_get_amenity_list>();

        if(!r.params.containsKey('bookingUnitId')) {

            objMeta.message = 'Missing parameter : bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;

        }
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) {
            objMeta.message = 'Missing parameter value: bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        if(String.isNotBlank(r.params.get('bookingUnitId')))  {

            System.debug('bookingUnitId:: ' + r.params.get('bookingUnitId'));

            List<Booking_Unit__c> lstBU = [SELECT id 
                                                , Unit_Name__c
                                                , Registration_Id__c 
                                                , Booking__r.Account__r.Party_ID__c
                                                , Property_Name__c
                                                , ByPassFMReminder__c
                                                , SC_From_RentalIncome__c
                                                , CM_Units__c
                                                , Lease_Expiry_Date__c
                                                , Allow_Amenity_Booking__c
                                           FROM Booking_Unit__c
                                           WHERE id =: r.params.get('bookingUnitId')];
            if(lstBU.size() > 0) {
                //Check tenancy contract expiry - 1.1,1.2
                //Boolean isContractExpired = lstBU[0].Lease_Expiry_Date__c >= System.today() ? false : true;
                //if(!isContractExpired) {
                    //Check service fee overdue
                    Boolean isDuePending = isServiceFeeDuePending(lstBU[0]);
                    if(!isDuePending) {
                        
                        //fetch all amenities for given BU
                        List<Resource__c> lstRes = GetAllAmenitiesForBU(lstBU[0], false, '');
                        
                        List<String> lstFootballRes = label.FootballAmenityForURLRedirect.split(',');
                        System.debug('lstFootballRes : ' + lstFootballRes);
                        
                        Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;
                        System.debug('IsSandbox is: '+isSandbox);
                        
                        for(Resource__c objRes : lstRes) {
                            cls_get_amenity_list objAmenity = new cls_get_amenity_list();
                            
                            System.debug('objRes.Name : ' + objRes.Name);
                            if(lstFootballRes.contains(objRes.Name)) {
                                objAmenity.is_booking_enabled = false;
                                objAmenity.booking_reason_not_allowed = 'Booking for selected Amenity is not handled by Damac';
                                if(isSandbox) {
                                    objAmenity.partner_site_url = 'https://google.com/';
                                }
                                else {
                                    objAmenity.partner_site_url = 'https://invictusacademy.skedda.com/';
                                    //https://invictusacademy.skedda.com/
                                }
                                
                            }
                            else {
                                objAmenity.is_booking_enabled = true; 
                            }
                            
                            objAmenity.id = objRes.id;
                            objAmenity.amenity_name = objRes.Name;
                            objAmenity.location_latitude = String.isNotBlank(objRes.Location_Latitude__c) ? Decimal.valueOf(objRes.Location_Latitude__c) : null;
                            objAmenity.location_longitude = String.isNotBlank(objRes.Location_Longitude__c) ? Decimal.valueOf(objRes.Location_Longitude__c) : null;
                            listAmenities.add(objAmenity);
                        }
                        
                        if(lstRes.size() > 0) {
                            objMeta.message = 'Successful';
                            objMeta.status_code = 1;
                        }
                        else {
                            //objMeta.message = 'No Amenities available for given bookingUnitId';
                            objMeta.message = NO_AMENITY_FOR_BUILDING;/*as per bug : 1978*/
                            objMeta.status_code = 2;
                        }
                        
                    }
                    else {
                        //Due pending
                        objMeta.message = 'Service fee overdue';
                        objMeta.status_code = 2;
                    }
                //}
                //else {
                //    //Tenancy contract expired
                //    objMeta.message = 'You won\'t be able to book the amenity as your tenancy contract expired. Please submit a tenancy renewal service request.';
                //    objMeta.status_code = 7;
                //}
            }
            else {
                objMeta.message = 'No Booking Unit found for the given bookingUnitId';
                objMeta.status_code = 3;
            }

        }

        objMeta.title = objMeta.message == NO_AMENITY_FOR_BUILDING ? 'Sorry!' : mapStatusCode.get(objMeta.status_code);/*As per Bug : 1978*/
        objMeta.developer_message = null;

        objData.amenities = listAmenities;

        returnResponse.meta_data = objMeta;
        returnResponse.data = objData;

        System.debug('returnResponse:: ' + returnResponse);
        
        return returnResponse;
    }

    public static boolean isServiceFeeDuePending(Booking_Unit__c objBU) {

        /*Below IF check is added for bypassing service charge overdue check for units with Allow_Amenity_Booking__c marked as TRUE - as discussed with Charith*/
        System.debug('objBU.Allow_Amenity_Booking__c : ' + objBU.Allow_Amenity_Booking__c);
        if(objBU.Allow_Amenity_Booking__c) {
            return false;
        }

        Boolean isDuePresent;
        FmIpmsRestServices.DueInvoicesResult dues = FmIpmsRestServices.getDueInvoices(objBU.Registration_Id__c, objBU.Booking__r.Account__r.Party_ID__c, objBU.Property_Name__c);
        System.debug('-->> Invoice dues.....'+ dues.dueAmountExceptCurrentQuarter);
        
        //Added the CM_Units, ByPassFMReminder__c, SC_From_RentalIncome__c check - Shubham 11/03/2020
        System.debug('due in label :'+label.ServiceChargeDueLimitInAmenityBooking);

        Decimal dueLimit = Decimal.valueOf(label.ServiceChargeDueLimitInAmenityBooking);
        System.debug('dueLimit: ' + dueLimit);

        //null == dues ||- The CM_Units, ByPassFMReminder__c, SC_From_RentalIncome__c check isremoved as per discussed with Shinu- Shubham 01/10/2020 
        if( dues.dueAmountExceptCurrentQuarter == null || dues.dueAmountExceptCurrentQuarter < dueLimit ) {
            isDuePresent = false;
        }
        else {
            isDuePresent = true;
        }

        System.debug('isDuePresent :'+isDuePresent);
        return isDuePresent;
    }

    public static List<Resource__c> GetAllAmenitiesForBU(Booking_Unit__c objBU, boolean fetchSignleAmenity, String resourceId) {
        
        System.debug('fetchSignleAmenity:: ' + fetchSignleAmenity);

        List<Resource__c> lstAmenities = new List<Resource__c>();
        if(objBU != null) {
            //Get building Id

            if(String.isNotBlank(objBU.Unit_Name__c)) {
                String buildingName = objBU.Unit_Name__c.split('/')[0];
                List<Location__c> lstBuildings = new List<Location__c>();
                lstBuildings = [SELECT  Id
                                        , Name
                                        , Property_Name__c
                                        , ( SELECT  FM_User__c
                                                    , FM_User__r.Email
                                                    , FM_Role__c
                                                    , FM_User__r.Name
                                            FROM FM_Users__r
                                            WHERE FM_Role__c = 'FM Admin'
                                            OR FM_Role__c = 'Master Community Admin'
                                            LIMIT 1)
                                FROM Location__c
                                WHERE Name =: buildingName
                                AND RecordType.DeveloperName = 'Building'
                                LIMIT 1];
                System.debug('===lstBuildings.size()==='+lstBuildings.size());
                if(lstBuildings.size() >0) {
                    lstAmenities =  getAvailableResources(lstBuildings[0].Property_Name__c, lstBuildings[0].Id, fetchSignleAmenity, resourceId);
                }
            }
        }

        return lstAmenities;
    }

    public Static List<Resource__c> getAvailableResources (String propertyId, String buildingId, boolean isSingleResource, String resourceId) {

        List<Resource__c> lstResources = new List<Resource__c>();
        
        //Prepare dynamic query
        System.debug('propertyId: '+propertyId);
        System.debug('buildingId: '+buildingId);
        System.debug('isSingleResource: '+isSingleResource);
        System.debug('resourceId: '+resourceId);

        String query = 'SELECT Name,ID,Contact_Person_Name__c,Contact_Person_Email__c,';
        query += 'Amenity_Admin__c,Amenity_Manager__c,Description__c,No_of_advance_closing_days__c,';
        query += 'Public_Amenity_Name__c, Chargeable_Fee_Slot_for_Guest__c,Deposit_for_Guest__c,';
        query += 'Contact_Person_Phone__c,No_of_advance_booking_days__c,Non_Operational_Days__c, Location_Latitude__c, Location_Longitude__c,';
        query += 'Chargeable_Fee_Slot__c,Deposit__c,Max_no_of_people_hosts_guests__c,No_of_deposit_due_days__c,';
        query += '(SELECT Non_operational_date__c FROM Non_operational_day__r)';
        query += ' FROM Resource__c ';
        query += ' WHERE Resource_Type__c = \'FM Amenity\'';
        //query += ' AND ((Chargeable_Fee_Slot__c = null OR Chargeable_Fee_Slot__c = 0.0 ) AND (Deposit__c = null OR Deposit__c = 0.0))';    //to filter out paid amenities (Ruled out on 27 Jan'21 to include paid amenities as well)
        //query += ' AND ID IN (SELECT Resource__c from Resource_Slot__c)';
        if(!isSingleResource) {
            query += ' AND  ID IN  (SELECT Resource__c FROM Resource_Sharing__c';
            query += ' WHERE (Property__c =: propertyId AND Property__c != NULL)';
            query += ' OR (Building__c =: buildingId AND Building__c != NULL))';
        }
        else {
            query += ' AND Id =: resourceId';
        }
        System.debug('===query===='+query);
        lstResources = Database.query(query);

        System.debug('lstResources: '+lstResources);

        return lstResources;
    }


    /*GET Method for getting only one Amenity with all its details*/
    @HttpGet 
    global static FinalReturnWrapperSingle GetSingleAmenityForUnit() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        FinalReturnWrapperSingle returnResponse = new FinalReturnWrapperSingle();
        cls_data_single objData = new cls_data_single();
        cls_meta_data objMeta = new cls_meta_data();
        List<cls_get_amenity_list> listAmenities = new List<cls_get_amenity_list>();
        cls_get_amenity_single objSingleAmenity= new cls_get_amenity_single();

        if(!r.params.containsKey('bookingUnitId')) {

            objMeta.message = 'Missing parameter : bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;

        }
        else if(!r.params.containsKey('amenityId')) {
            objMeta.message = 'Missing parameter : amenityId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) {
            objMeta.message = 'Missing parameter value: bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('amenityId') && String.isBlank(r.params.get('amenityId'))) {
            objMeta.message = 'Missing parameter value: amenityId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;   
        }

        if(String.isNotBlank(r.params.get('bookingUnitId')) && String.isNotBlank(r.params.get('amenityId')) )  {

            System.debug('bookingUnitId:: ' + r.params.get('bookingUnitId'));

            List<Booking_Unit__c> lstBU = [SELECT id 
                                                , Unit_Name__c
                                                , Registration_Id__c 
                                                , Booking__r.Account__r.Party_ID__c
                                                , Property_Name__c
                                                , ByPassFMReminder__c
                                                , SC_From_RentalIncome__c
                                                , CM_Units__c
                                           FROM Booking_Unit__c
                                           WHERE id =: r.params.get('bookingUnitId')];
            if(lstBU.size() > 0) {

                List<Resource__c> lstRes = GetAllAmenitiesForBU(lstBU[0], true, r.params.get('amenityId'));

                    for(Resource__c objRes : lstRes) {
                        cls_get_amenity_single objAmenity = new cls_get_amenity_single();
                        cls_amenity_charges_deposit objCharge = new cls_amenity_charges_deposit();
                        cls_amenity_contact_details objContact = new cls_amenity_contact_details();
                        List<String> lst_nonOp_days = new List<String>();
                        List<String> lst_nonOp_dates = new List<String>();

                        objAmenity.id = objRes.id;
                        objAmenity.amenity_name = objRes.Name;
                        objAmenity.description = objRes.Description__c;

                        /*Adding amenity charges info*/
                        objCharge.chargeable_fee_per_slot = objRes.Chargeable_Fee_Slot__c;
                        objCharge.security_deposit = objRes.Deposit__c;
                        /*END Adding amenity charges info*/

                        objAmenity.fee_and_deposit_details = objCharge;
                        objAmenity.is_paid_amenity = objCharge.chargeable_fee_per_slot != null && objCharge.security_deposit != null ? true : false;

                        /*Adding amenity contact info*/
                        objContact.contact_person_name = objRes.Contact_Person_Name__c;
                        objContact.contact_person_email = objRes.Contact_Person_Email__c;
                        objContact.contact_person_phone = objRes.Contact_Person_Phone__c;

                        objAmenity.amenity_contact_details = objContact;

                        //Lat long
                        objAmenity.location_latitude = String.isNotBlank(objRes.Location_Latitude__c) ? Decimal.valueOf(objRes.Location_Latitude__c) : null;
                        objAmenity.location_longitude = String.isNotBlank(objRes.Location_Longitude__c) ? Decimal.valueOf(objRes.Location_Longitude__c) : null;

                        /*End Adding amenity contact info*/

                        /*adding non-operational days*/
                        //if(String.isNotBlank(objRes.Non_Operational_Days__c)) {
                        //    lst_nonOp_days = objRes.Non_Operational_Days__c.split(';');
                        //    objAmenity.non_operational_days = lst_nonOp_days;
                        //}
                        /*END adding non-operational days*/

                        /*adding non-operational dates*/
                        //if(objRes.Non_operational_day__r.size() > 0) {

                        //    for(Non_operational_days__c objOp : objRes.Non_operational_day__r) {
                        //        lst_nonOp_dates.add(Datetime.newInstance(objOp.Non_operational_date__c.year(), objOp.Non_operational_date__c.month(), objOp.Non_operational_date__c.day()).format('yyyy-MM-dd'));
                        //    }
                        //    objAmenity.non_operational_dates = lst_nonOp_dates;
                        //}
                        /*END adding non-operational dates*/

                        objSingleAmenity = objAmenity;

                    }

                    if(lstRes.size() > 0) {
                        objMeta.message = 'Successful';
                        objMeta.status_code = 1;
                    }
                    else {
                        objMeta.message = 'No Amenity found for given amenityId';
                        objMeta.status_code = 2;
                    }

            }
            else {
                objMeta.message = 'No Booking Unit found for the given bookingUnitId';
                objMeta.status_code = 3;
            }
        }

        objMeta.title = mapStatusCode.get(objMeta.status_code);
        objMeta.developer_message = null;

        objData.amenity_details = objSingleAmenity;

        returnResponse.meta_data = objMeta;
        returnResponse.data = objData.amenity_details != null ? objData : null;

        System.debug('returnResponse:: ' + returnResponse);
        
        return returnResponse;

    }


    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }

    public class cls_data {
        public cls_get_amenity_list[] amenities;
    }

    public class cls_get_amenity_list {
        public String id;
        public String amenity_name;
        public boolean is_booking_enabled;
        public String booking_reason_not_allowed;
        public String partner_site_url;
        public Decimal location_latitude;
        public Decimal location_longitude;
    }


    /**/
    global class FinalReturnWrapperSingle {
        public cls_data_single data;
        public cls_meta_data meta_data;
    }

    public class cls_get_amenity_single {
        public String id;
        public String amenity_name;
        public String description;
        public Decimal location_latitude;
        public Decimal location_longitude;
        public Boolean is_paid_amenity;
        public cls_amenity_charges_deposit fee_and_deposit_details;
        public cls_amenity_contact_details amenity_contact_details;
        //public String[] non_operational_days;
        //public String[] non_operational_dates;
    }

    public class cls_amenity_charges_deposit {
        public Decimal chargeable_fee_per_slot;
        public Decimal security_deposit;
    }

    public class cls_amenity_contact_details {
        public String contact_person_name;
        public String contact_person_email;
        public String contact_person_phone; 
    }

    public class cls_data_single {
        public cls_get_amenity_single amenity_details;
    }

}