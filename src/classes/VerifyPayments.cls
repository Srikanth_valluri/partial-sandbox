public without sharing class VerifyPayments{
    @InvocableMethod
    public static void checkPendingAmountInIPMS(list<Id> lstIds){
        callIPMS(lstIds);
    } //end of checkPendingAmountInIPMS method
    
    @future(callout=true)
    public static void callIPMS(list<Id> lstIds){
        list<Case> listCaseToUpdate = new list<Case>();
        list<Error_Log__c> listErrors = new list<Error_Log__c>();
        for(Case objC : [select Id
                                , Seller__c
                                , Booking_Unit__r.Id
                                , Booking_Unit__r.Registration_ID__c
                                , Recordtype.DeveloperName
                                , Recordtype.Name
                        from Case 
                        where Booking_Unit__c != null 
                        and Id IN: lstIds
                        and Booking_Unit__r.Registration_ID__c != null]){
            Decimal decTotal = 0.0;
            String strDueResponse = assignmentEndpoints.fetchAssignmentDues(objC.Booking_Unit__r);
            system.debug('strDueResponse==='+strDueResponse);
            if(String.isNotBlank(strDueResponse)){
                map<String,Object> mapDeserializeDue = (map<String,Object>)JSON.deserializeUntyped(strDueResponse);
                if(mapDeserializeDue.get('status') == 'S'){
                    strDueResponse = strDueResponse.remove('{');
                    strDueResponse = strDueResponse.remove('}');
                    strDueResponse = strDueResponse.remove('"');
                    //lstPayments = new List<paymentInfo>();
                    system.debug('after all replacements******'+strDueResponse);
                    for(String st : strDueResponse.split(',')){
                        String strKey = st.substringBefore(':').trim();
                        system.debug('*****strKey*****'+strKey);
                        if(!strKey.equalsIgnoreCase('Status')) {
                            system.debug('st*****'+st);
                            Decimal calAmount = 0.0;
                            //paymentInfo objP = new paymentInfo();
                            //objP.strType = strKey;
                            if(Decimal.valueOf(st.subStringAfter(':').trim()) != null) {
                               if((objC.Recordtype.Developername == 'Assignment')
                               || (objC.Recordtype.Developername == 'Handover'
                               && strKey.contains('Balance as per SOA'))){
                                   system.debug('*****INSIDE*****');
                                   calAmount = Decimal.valueOf(st.subStringAfter(':').trim());
                               }
                            }
                            else {
                               calAmount = 0.0;
                            }
                            decTotal = decTotal + calAmount;
                            system.debug('decTotal*************'+decTotal);
                            //lstPayments.add(objP);
                        }
                    }
                    objC.Pending_Amount__c = decTotal;
                    if(objC.Recordtype.Developername == 'Handover'
                    && decTotal <= 0){
                        objC.Payment_Verified__c = true;
                    }
                    listCaseToUpdate.add(objC);

                }else if(mapDeserializeDue.get('status') == 'E'
                && mapDeserializeDue.containsKey('message')){
                    //errorMessage = 'Error : '+mapDeserializeDue.get('message');
                    Error_Log__c objErr = createErrorLogRecord(objC.Seller__c,
                                                               objC.Booking_Unit__r.Id, 
                                                               objC.Id,
                                                               objC.Recordtype.Name);
                    objErr.Error_Details__c = 'Error : '+mapDeserializeDue.get('message');
                    //insert objErr;
                    listErrors.add(objErr);
                }
            }
            else {
                //errorMessage = 'Error : No Response from IPMS for Payment Dues';
                Error_Log__c objErr = createErrorLogRecord(objC.Seller__c,
                                                           objC.Booking_Unit__r.Id, 
                                                           objC.Id,
                                                           objC.Recordtype.Name);
                objErr.Error_Details__c = 'Error : No Response from IPMS for Payment Dues';
                //insert objErr;
                listErrors.add(objErr);
            }
        } // end of for loop
        if(!listCaseToUpdate.isEmpty()){
            update listCaseToUpdate;
        }
        if(!listErrors.isEmpty()){
            insert listErrors;
        }
    }
    
    public static Error_Log__c createErrorLogRecord(Id accId, Id bookingUnitId, Id caseId, String processName){
        Error_Log__c objErr = new Error_Log__c();
        objErr.Account__c = accId;
        objErr.Booking_Unit__c = bookingUnitId;
        objErr.Case__c = caseId;
        objErr.Process_Name__c = processName;
        return objErr;
    }
} // end of class