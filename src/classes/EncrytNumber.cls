public class EncrytNumber {
  @InvocableMethod(label='EncrytNumber' description='update the Encryted fields on Account ' category='Account')
  public static void EncrytNumber(List<ID> ids) {
   
    List<Account> accounts = [SELECT Id,Mobile__c, Mobile_Phone_Encrypt__pc, Telephone__c,
                                        Mobile_c_CRM_Encrypt__c,
                                        Mobile_pc_CRM_Encrypt__c,
                                        POA_CRM_Encrypt__c,
                                        Telephone_CRM_Encrypt__c,
                                        POA_Phone__c 
                            FROM Account WHERE Id in :ids];
    for (Account account : accounts) {
      account.Mobile_c_CRM_Encrypt__c = String.isNotBlank(account.Mobile__c)?encryptNo(account.Mobile__c):'';
      account.Mobile_pc_CRM_Encrypt__c = String.isNotBlank(account.Mobile_Phone_Encrypt__pc)?encryptNo(account.Mobile_Phone_Encrypt__pc):'';
      account.Telephone_CRM_Encrypt__c = String.isNotBlank(account.Telephone__c)?encryptNo(account.Telephone__c):'';
      account.POA_CRM_Encrypt__c = String.isNotBlank(account.POA_Phone__c)?encryptNo(account.POA_Phone__c):'';
    }
    update accounts;
  }
  
  
      public static String encryptNo( String strMobile){
        

        //16 byte string. since characters used are ascii, each char is 1 byte.
        Blob cryptoKey = Blob.valueOf(Label.CRMEncrypyKey);
        Blob presetIV = Blob.valueOf(Label.CRMEncrypyKey);
        //encrypted blob
        
        //
        Blob cipherText = Crypto.encrypt('AES128', cryptoKey,presetIV, Blob.valueOf(strMobile));
        //encrypted string
        String encodedCipherText = EncodingUtil.convertToHex(cipherText);

        System.debug(encodedCipherText);

        return encodedCipherText;
      }
      
    public static String decryptNo( String ecryptMobile){
        
        //16 byte string. since characters used are ascii, each char is 1 byte.
            Blob cryptoKey = Blob.valueOf(Label.CRMEncrypyKey);
            Blob  presetIV = Blob.valueOf(Label.CRMEncrypyKey);
            
        //encrypted blob
        Blob encodedEncryptedBlob = EncodingUtil.convertFromHex(ecryptMobile);
        //decrypted blob
        System.debug('ecryptMobile'+ecryptMobile);
        Blob decryptedBlob = Crypto.decrypt('AES128', cryptoKey,presetIV, encodedEncryptedBlob);
        //decrypted string
        String decryptedText = decryptedBlob.toString();

        System.debug(decryptedText);
        return decryptedText;
    }
}