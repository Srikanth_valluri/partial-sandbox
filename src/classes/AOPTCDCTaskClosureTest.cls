/*
 * Description - Test class for AOPTCDCTaskClosure
 *
 * Version        Date            Author            Description
 * 1.0            22/05/18        Vivek Shinde      Initial Draft
 */
@isTest
private class AOPTCDCTaskClosureTest {
    
    static testMethod void CDCTaskClosureTest() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('AOPT').getRecordTypeId();
        Case objCase1 = new Case();        
        objCase1.Status = 'Submitted';        
        objCase1.RecordTypeID = devRecordTypeId;
        objCase1.Approving_Authorities__c  = 'VP';
        objCase1.AccountID = objAcc.id;
        objCase1.Offer_Acceptance_Letter_Generated__c = true;
        objCase1.O_A_Signed_Copy_Uploaded__c = true;
        objCase1.Approval_Status__c = 'approved';
        insert objCase1;
        
        List<Task> lstTask = new List<Task>();
        Task objTask = new Task();
        objTask.WhatId = objCase1.Id;
        objTask.Subject = 'Offer and Acceptance Letter Verification';
        objTask.Assigned_User__c = 'CDC';
        objTask.Process_Name__c = 'AOPT';
        objTask.Priority = 'High';
        objTask.Status = 'Not Started';
        lstTask.add(objTask);
        
        insert lstTask;
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        AOPTCDCTaskClosure.ValidateDocument(lstTask);
    } 
}