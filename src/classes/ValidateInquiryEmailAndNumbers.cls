/****************************************************************************************************
* Name          : ValidateInquiryEmailAndNumbers                                                    *
* Description   : This class is calles for the Inquiry trigger handler.                             *
*                 This class calls the sendNEXMOCallout() from the phoneValidation class            * 
                  to validate the Inquiry record Phone Numbers                                      *
* Created Date  : 17/01/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION   AUTHOR          DATE            COMMENTS                                                *
* 1.0       Craig Lobo      17/01/2018      Initial Draft.                                          *
****************************************************************************************************/
public without sharing class ValidateInquiryEmailAndNumbers{

    public ValidateInquiryEmailAndNumbers() {
        
    }

    /*********************************************************************************************
    * @Description : Method to contain logic to pass the Inquiry Phone Numbers to                *
    *                   the Web-Service validate them.                                           *
    * @Params      : Set<Id>                                                                     *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    //@future(callout = true)
    public static void verifyInqEmailAndNumbersFromWebService(Set<Id> pInqIdSet) {
        Inquiry__c inqRecord;
        Boolean isUpdate = false;

        List<Inquiry__c> inqUpdateList              = new List<Inquiry__c>();
        Map<String, Object> responseMap             = new Map<String, Object>();
        Map<String, Object> carrierMap              = new Map<String, Object>();
        Map<String, Object> roamingMap              = new Map<String, Object>();

        // Mobile Phone
        Map<String, String> inqNumberValidityMap    = new Map<String, String>();
        Map<String, String> inqNumberTypeMap        = new Map<String, String>();
        Map<String, String> inqNumberReachableMap   = new Map<String, String>();
        Map<String, String> inqNumberPortedMap      = new Map<String, String>();
        Map<String, String> inqNumberCountryMap     = new Map<String, String>();
        Map<String, String> inqNumberRoamingMap     = new Map<String, String>();

        // Email disposable toxic
        Map<String, String> inqEmailStatusMap       = new Map<String, String>();
        Map<String, String> inqEmailSubStatusMap    = new Map<String, String>();
        Map<String, String> inqEmailToxicMap       = new Map<String, String>();
        Map<String, String> inqEmailDisposableMap    = new Map<String, String>();

        for (Verify_Inquiry_Contact_Details__c csInstance : Verify_Inquiry_Contact_Details__c.getall().values()) {
            if (String.isNotBlank(csInstance.Mobile_Phone_API__c)) {
                inqNumberValidityMap.put(csInstance.Mobile_Phone_API__c, csInstance.Validity__c);
                inqNumberTypeMap.put(csInstance.Mobile_Phone_API__c, csInstance.Phone_Number_Type__c);
                inqNumberReachableMap.put(csInstance.Mobile_Phone_API__c, csInstance.Reachable__c);
                inqNumberPortedMap.put(csInstance.Mobile_Phone_API__c, csInstance.Ported__c);
                inqNumberCountryMap.put(csInstance.Mobile_Phone_API__c, csInstance.Mobile_Country__c);
                inqNumberRoamingMap.put(csInstance.Mobile_Phone_API__c, csInstance.Roaming__c);
            }
            if (String.isNotBlank(csInstance.Email_API__c)) {
                inqEmailStatusMap.put(csInstance.Email_API__c, csInstance.Status__c);
                inqEmailSubStatusMap.put(csInstance.Email_API__c, csInstance.Sub_Status__c);
                inqEmailToxicMap.put(csInstance.Email_API__c, csInstance.Toxic__c);
                inqEmailDisposableMap.put(csInstance.Email_API__c, csInstance.Disposable__c);
            }
        }

        List<String> inqFieldsToIterateList = new List<String>{
            'Email__c','Email_2__c','Email_3__c','Email_4__c','Email_5__c',
            'Mobile_Phone__c','Mobile_Phone_2__c','Mobile_Phone_3__c',
            'Mobile_Phone_4__c','Mobile_Phone_5__c'
        };

        for(Inquiry__c inq : [ SELECT Id
                                    , Name
                                    , Email__c
                                    , Email_2__c
                                    , Email_3__c
                                    , Email_4__c
                                    , Email_5__c
                                    , Mobile_Phone__c
                                    , Mobile_Phone_2__c
                                    , Mobile_Phone_3__c
                                    , Mobile_Phone_4__c
                                    , Mobile_Phone_5__c
                                 FROM Inquiry__c 
                                WHERE Id IN :pInqIdSet
        ]) {
            inqRecord = new Inquiry__c(Id = inq.Id);
            isUpdate = false;
            for(String inqFieldAPIName : inqFieldsToIterateList) {
                if (inq.get(inqFieldAPIName) != null) {
                    String checkString = (String) inq.get(inqFieldAPIName); 
                    System.debug('checkString>>>>>' + checkString);
                    System.debug('checkString.isNumeric()>>>>>' + checkString.isNumeric());

                    if (checkString.isNumeric() == true) {

                        // WEB-SERVICE Call for Phone Numbers
                        responseMap = phoneValidation.sendNEXMOCallout(checkString);
                        System.debug('NUMBER>>>>>' + checkString + ' > ' + responseMap);
                        System.debug('NUMBERinqFieldAPIName>>>>>' + inqFieldAPIName);
                        if (responseMap != null && !responseMap.isEmpty() ) {

                            // Network Type Check AND Mobile Country Check
                            if ((inqNumberTypeMap.containsKey(inqFieldAPIName) 
                                    && inqNumberTypeMap.get(inqFieldAPIName) != null
                                ) || (inqNumberCountryMap.containsKey(inqFieldAPIName) 
                                    && inqNumberCountryMap.get(inqFieldAPIName) != null
                                )
                            ) {
                                carrierMap = (Map<String, Object>)responseMap.get('original_carrier');

                                // Network Type Check
                                if ( carrierMap != null
                                    && carrierMap.containsKey('network_type') 
                                    && carrierMap.get('network_type') != null
                                ) {
                                    inqRecord.put(
                                        inqNumberTypeMap.get(inqFieldAPIName), 
                                        carrierMap.get('network_type')
                                    );
                                    isUpdate = true;
                                }

                                // Mobile Country Check
                                if ( carrierMap != null
                                    && carrierMap.containsKey('country') 
                                    && carrierMap.get('country') != null
                                ) {
                                    inqRecord.put(
                                        inqNumberCountryMap.get(inqFieldAPIName), 
                                        carrierMap.get('country')
                                    );
                                    isUpdate = true;
                                }
                            }

                            // Roaming Check
                            if (inqNumberRoamingMap.containsKey(inqFieldAPIName) 
                                && inqNumberRoamingMap.get(inqFieldAPIName) != null ) {
                                roamingMap = (Map<String, Object>) responseMap.get('roaming');
                                if (roamingMap != null) {
                                    inqRecord.put(
                                        inqNumberRoamingMap.get(inqFieldAPIName), 
                                        roamingMap.get('status')
                                    );
                                    isUpdate = true;
                                }
                            }

                            // Validity Check
                            if (inqNumberValidityMap.containsKey(inqFieldAPIName) 
                                && inqNumberValidityMap.get(inqFieldAPIName) != null ) {
                                inqRecord.put(
                                    inqNumberValidityMap.get(inqFieldAPIName),
                                    responseMap.get('valid_number')
                                );
                                isUpdate = true;
                            }

                            // Reachable Check
                            if (inqNumberReachableMap.containsKey(inqFieldAPIName) 
                                && inqNumberReachableMap.get(inqFieldAPIName) != null ) {
                                inqRecord.put(
                                    inqNumberReachableMap.get(inqFieldAPIName), 
                                    responseMap.get('reachable')
                                );
                                isUpdate = true;
                            }

                            // Ported Check
                            if (inqNumberPortedMap.containsKey(inqFieldAPIName) 
                                && inqNumberPortedMap.get(inqFieldAPIName) != null ) {
                                inqRecord.put(
                                    inqNumberPortedMap.get(inqFieldAPIName), 
                                    responseMap.get('ported')
                                );
                                isUpdate = true;
                            }

                        }
                    } else {

                        // WEB-SERVICE Call for Email Address
                        responseMap = emailValidation.sendZBCallout(checkString);
                        System.debug('EMAIL>>>>>' + checkString + ' > ' + responseMap);
                        System.debug('EMAILinqFieldAPIName>>>>>' + inqFieldAPIName);
                        if (responseMap != null && !responseMap.isEmpty()) {

                            // Status Check
                            if (inqEmailStatusMap.containsKey(inqFieldAPIName) 
                                && inqEmailStatusMap.get(inqFieldAPIName) != null ) {
                                inqRecord.put(
                                    inqEmailStatusMap.get(inqFieldAPIName),
                                    responseMap.get('status')
                                );
                                isUpdate = true;
                            }

                            // Sub Status Check
                            if (inqEmailSubStatusMap.containsKey(inqFieldAPIName) 
                                && inqEmailSubStatusMap.get(inqFieldAPIName) != null ) {
                                inqRecord.put(
                                    inqEmailSubStatusMap.get(inqFieldAPIName),
                                    responseMap.get('sub_status')
                                );
                                isUpdate = true;
                            }

                            // Disposable Check
                            if (inqEmailDisposableMap.containsKey(inqFieldAPIName) 
                                && inqEmailDisposableMap.get(inqFieldAPIName) != null ) {
                                inqRecord.put(
                                    inqEmailDisposableMap.get(inqFieldAPIName),
                                    String.valueOf(responseMap.get('disposable'))
                                );
                                isUpdate = true;
                            }

                            // Toxic Check
                            if (inqEmailToxicMap.containsKey(inqFieldAPIName) 
                                && inqEmailToxicMap.get(inqFieldAPIName) != null ) {
                                inqRecord.put(
                                    inqEmailToxicMap.get(inqFieldAPIName),
                                    String.valueOf(responseMap.get('toxic'))
                                );
                                isUpdate = true;
                            }

                        }
                    }
                }

            } // inqFieldAPI loop END
             System.debug('INQ_RECORD>>>>>' + inqRecord);
            System.debug('isUpdate>>>>>' + isUpdate);
            if (isUpdate == true && inqRecord.Id != null) {
                inqUpdateList.add(inqRecord);
            }
        } // Inqiry Query Loop END

        update inqUpdateList;

    } // verifyInqEmailAndNumbersFromWebService() END

} // Class END