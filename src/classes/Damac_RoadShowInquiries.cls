public withOut sharing class Damac_RoadShowInquiries {

    public List<Assigned_PC__c> assignedPcs { get; set; }
    public List<SelectOption> campaignNames { get; set; }
    public ID selectedCampaignId { get; set; }
    public Inquiry__c inq { get; set; }
    public Damac_RoadShowInquiries(ApexPages.standardController stdController) {
        inq = new Inquiry__c ();
        selectedCampaignId = NULL;
        campaignNames = new List<SelectOption>();
        Map <Id, Campaign__c> activeCampaigns = new Map <Id, Campaign__c> ([SELECT 
                            Campaign_Name__c, Name, Marketing_Start_Date__c, Marketing_End_Date__c
                             FROM Campaign__c 
                             WHERE Active__c = TRUE 
                             AND Marketing_Start_Date__c <= TODAY 
                             AND Marketing_End_Date__c >= TODAY
                             AND RecordType.Name = 'Roadshows']);

         if (activeCampaigns.size() > 0) {
             assignedPcs = new List<Assigned_PC__c>();
             ID userId = UserInfo.getUserID();
             assignedPcs = [SELECT Campaign__c, Name, User__c, User__r.Name FROM Assigned_PC__c WHERE User__c =:userId AND Campaign__c IN: activeCampaigns.keySet() order By Campaign__r.Name DESC];
             if (assignedPcs.size() > 0) {
                 for (Assigned_PC__c pc: assignedPcs) {
                     campaignNames.add(new SelectOption(pc.Campaign__c, activeCampaigns.get(pc.Campaign__c).Campaign_Name__c));
                 }
             }
         }
    }
    
    public pageReference save() {
        if (selectedCampaignId != NULL) {
            inq.Campaign__c = selectedCampaignId;
            insert inq;
            return new pageReference('/'+inq.Id);
        } else 
        { 
            
            return null; 
        }
    }
    
    public pageReference cancel() {
        
        return new pageReference('/');
    }
    
}