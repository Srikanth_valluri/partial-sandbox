/**************************************************************************************************
* Name               : InventoryRestrictionRemoveBatchTest
* Description        : Test class for InventoryRestrictionRemoveBatch class.
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst      06/10/2020      Initial Draft.
**************************************************************************************************/
@isTest
private class InventoryRestrictionRemoveBatchTest {

    static testMethod void testScheduler() {
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        //loc.recordTypeId = bildingLocRecTypeId;
        loc.Location_Name__c = 'test';
        loc.Address__c = 'test';
        loc.Contact__c = 'test';
        loc.Timings__c = 'test';
        loc.Map_URL__c = 'https://test';
        insert loc;
        Property__c objProperty = TestDataFactory_CRM.createProperty();
        insert objProperty;
        Inventory__c objInventory = TestDataFactory_CRM.createInventory(objProperty.id);
        objInventory.Area_Sqft__c = '1520';
        objInventory.Bedroom_Type__c = 'Test';
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit_Location__c=loc.id;
        objInventory.Floor_Package_Name__c='Floor1';
        objInventory.View_Type__c='Type1';
        objInventory.Property_City__c='Dubai';
        objInventory.Property_Status__c='Approve';
        objInventory.Space_Type_Lookup_Code__c='123';
        objInventory.Status__c = 'Restricted';
        insert objInventory;
        Inventory_Restriction_Request__c inventoryReq= new Inventory_Restriction_Request__c();
        inventoryReq.Status__c='Completed';
        inventoryReq.Function__c='Strategy';
        inventoryReq.Request_Processed_Datetime__c=DateTime.now();
        inventoryReq.Request_Date__c=DateTime.now();
        inventoryReq.Restrict_Reason__c='Leasing';
        inventoryReq.Request_Type__c='Inventory Restriction';
        inventoryReq.Restriction_End_Date__c=System.today();
        inventoryReq.Restriction_Extended__c=false;
        inventoryReq.Functional_User__c = userInfo.getUserId();
        insert inventoryReq;
        Inventory_Restriction_Unit__c inventoryUnit= new Inventory_Restriction_Unit__c();
        inventoryUnit.Inventory_Restriction_Request__c=inventoryReq.Id;
        inventoryUnit.Inventory_Initial_Status__c='Inventory';
        inventoryUnit.Inventory__c=objInventory.id;
        insert inventoryUnit; 
        Test.startTest();
            InventoryRestrictionRemoveBatchScheduler ascsObject = new InventoryRestrictionRemoveBatchScheduler();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test Scheduler', sch, ascsObject); 
        Test.stopTest();
    }
}