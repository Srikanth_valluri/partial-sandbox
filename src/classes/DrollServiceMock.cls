/*********************************************************************************
* Name               : DrollServiceMock
* Description        :Test class for wrpper class DrollServiceMock.
*----------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE          COMMENTS 
  1.0         Naresh 
  
**********************************************************************************/

global class DrollServiceMock  implements HTTPCalloutMock{
    global HTTPResponse respond(HTTPRequest req){
      
        String Body =        '{'+
        '    "data": ['+
        '        {'+
        '            "0": {'+
        '                "option2Price": -999999,'+
        '                "discountPSF_CN": 40,'+
        '                "subCategory": "null",'+
        '                "dateofBooking": 1499731200000,'+
        '                "waiverIdCN": null,'+
        '                "campId": "4",'+
        '                "waiverCN": "DLD 50%",'+
        '                "campName": "Delhi Roadshow",'+
        '                "option1Name": "Option 1 dummy",'+
        '                "price": 1590000,'+
        '                "templateIdOP5": "Zero Community Charge for 15 Yrs Promo",'+
        '                "templateIdOP4":  "Zero Community Charge for 15 Yrs Promo",'+
        '                "templateIdOP3":  "Zero Community Charge for 15 Yrs Promo",'+
        '                "templateIdOP2": "Zero Community Charge for 15 Yrs Promo",'+
        '                "templateIdOP1": "OXYGEN Serviced and Furnished promo",'+
        '                "giftCN": "Maserati Car",'+
        '                "discountFlatCN": 50000,'+
        '                "customerThresholdOld": 10000,'+
        '                "noofUnitsInput": 0,'+
        '                "noofunitsinputPN": 0,'+
        '                "option4Id": "4",'+
        '                "templateIdOP6":  "Zero Community Charge for 15 Yrs Promo",'+
        '                "option3Price":  -999999,'+
        '                "option3Name": "Option 1 dummy",'+
        '                "giftSN": "Audi A4",'+
        '                "buildingName": "DT",'+
        '                "noofUnits": 1,'+
        '                "templateIdPN": null,'+
        '                "discountPercentPN": 0,'+
        '                "promoIdPN": 0,'+
        '                "waiveridSN": null,'+
        '                "facing": null,'+
        '                "option5Name": "Option 1 dummy",'+
        '                "customerThresOutPN": null,'+
        '                "discountFlatSN": 10000,'+
        '                "inventoryThresholdOld": 10000,'+
        '                "customerId": null,'+
        '                "pcId": "POC User1",'+
        '                "inventoryThreshold": 0,'+
        '                "option5Id": "5",'+
        '                "totalDealValue": 1590000,'+
        '                "option1Id": "1",'+
        '                "priceChangePSF": 0,'+
        '                "discountPSF_OP1": 99999,'+
        '                "unitIdPN": null,'+
        '                "discountPSF_OP3": 90,'+
        '                "marketingProject": "DAMAC TOWER WITH INTERIORS BY VERSACE - LEBANON",'+
        '                "discountPSF_OP2": 99999,'+
        '                "unitIdPP": null,'+
        '                "discountPSF_OP5": 900,'+
        '                "inLeuPN": null,'+
        '                "discountPSF_OP4": 90,'+
        '                "inventoryId": null,'+
        '                "discountPSF_OP6": 890,'+
        '                "option1Price": -999999,'+
        '                "option4Price":-999999,'+
        '                "waiverSN": "DLD 4%",'+
        '                "promoIdOutputPN": null,'+
        '                "promoNamePN": null,'+
        '                "totalArea": 187,'+
        '                "agent": "null",'+
        '                "campaignNameInput": "null",'+
        '                "numberofBedrooms": "2",'+
        '                "discountPSF_PN": 0,'+
        '                "waiverIdPN": null,'+
        '                "schemeIdIn": "8",'+
        '                "giftPN": null,'+
        '                "residence": "Minsk",'+
        '                "discountPercentSN": 2,'+
        '                "option2Name": "Option 2 Dummy",'+
        '                "templateIdSN": "Ramadan Ready Home Promo",'+
        '                "area": 187,'+
        '                "waiverPN": null,'+
        '                "option6Id": "6",'+
        '                "option2Id": "2",'+
        '                "option6Price": -999999,'+
        '                "discountPercentCN": 2,'+
        '                "customerthresholdValue": 0,'+
        '                "templateIdCN": "Rebate of AED150,000 in lieu of Car Offer",'+
        '                "option4Name": "Option 1 dummy",'+
        '                "projectName": "Damac Tower",'+
        '                "region": "Belarus",'+
        '                "discountFlatPN": 0,'+
        '                "paymentAtMilestone": null,'+
        '                "totalDealValuePN": 0,'+
        '                "option5Price": -999999,'+
        '                "option6Name": "Option 1 dummy",'+
        '                "discountPercentOP4": 670,'+
        '                "discountPercentOP3": 670,'+
        '                "campaignIdInput": null,'+
        '                "discountPercentOP2": -999999,'+
        '                "discountPercentOP1": 9999,'+
        '                "cunstructionStatus": "Off Plan",'+
        '                "priceChangePN": 0,'+
        '                "schemeId": 8,'+
        '                "inLeuSN": "150,000 rebate",'+
        '                "promoNameSN": "Test Discount",'+
        '                "discountPercentOP6": 670,'+
        '                "bedroomType": "2 BR",'+
        '                "discountPercentOP5": 450,'+
        '                "floor": "DT/13A",'+
        '                "priceChangePer": 0,'+
        '                "productType": "USD",'+
        '                "views": "null",'+
        '                "inLeuCN": "100000 rebate",'+
        '                "totalAreaPN": 0,'+
        '                "unitIdCN": "DT/13A/1304",'+
        '                "discountFlatOP1": 9999,'+
        '                "discountFlatOP2": 9999,'+
        '                "option3Id": "3",'+
        '                "inventoryThreshOutPN": null,'+
        '                "discountFlatOP5": 70,'+
        '                "discountFlatOP6": 50,'+
        '                "discountFlatOP3": 40,'+
        '                "discountFlatOP4": 30,'+
        '                "discountPSF_SN": 100,'+
        '                "category": "Residential"'+
        '            }'+
        '        }'+
        '    ]'+
        '}';
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(Body);
        res.setStatusCode(200);
        return res;
    }
}