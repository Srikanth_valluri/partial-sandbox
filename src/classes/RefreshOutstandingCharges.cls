global class RefreshOutstandingCharges {
	@AuraEnabled
	webservice static void updateOutstandingCharges(String fmCaseId) {
		System.debug('fmCaseId : '+fmCaseId);

		FM_Case__c objFMCase = new FM_Case__c();
		FM_Case__c objFMCase2 = new FM_Case__c();

		objFmCase2 = [SELECT Id
							 , Booking_Unit__c
					  FROM FM_Case__c
					  WHERE Id = :fmCaseId ];

		Booking_Unit__c objBookingUnit = [ SELECT Id
												  , Registration_ID__c
										   FROM Booking_Unit__c
										   WHERE id = :objFmCase2.Booking_Unit__c ];

		if(objBookingUnit != NULL && String.isNotBlank(objBookingUnit.Registration_ID__c)
					&& String.isNotBlank(fmCaseId)) {
			objFMCase.id = fmCaseId;
			try {
                FmIpmsRestServices.DueInvoicesResult objResult =
                                    FmIpmsRestServices.getDueInvoices( objBookingUnit.Registration_ID__c, '','');
                if( objResult != NULL ) {
                    objFMCase.Outstanding_service_charges__c = String.isNotBlank( objResult.totalDueAmount )
								? objResult.totalDueAmount : '0';
                }
            }
            catch( Exception e ) {
                system.debug( '==Exception while fecthing oustanding balance=='+e.getMessage() );
            }
			System.debug('objFMCase : '+objFMCase);
			if(objFMCase != NULL) {
				update objFMCase;
			}
		}
	}
}