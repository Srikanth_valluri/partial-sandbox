global with sharing class DAMAC_SOA_Generation{
    // Call SOA generation.
    webservice static string callSOAGeneration(string srId){
        string status ='';        
            list<booking__c> bookings = [select id,(select id,booking__r.id from booking_units__r where status__c!='Removed' and registration_id__c != null) from booking__c where Deal_sr__c =:srId];
            for(booking__c book:bookings){
                if(book.booking_units__r.size() > 0){
                    //for(booking_unit__c booku:book.booking_units__r){
                        IPMS_REST_SOA_getProcessID.DataForRequest(book.id);
                    //}
                    status = 'Success';
                }else{
                    status = 'Error';
                }
            }
        return status;        
    }

}