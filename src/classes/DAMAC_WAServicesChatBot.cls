@RestResource(urlMapping='/WAService/*')
global class DAMAC_WAServicesChatBot{
    
    @HttpGET
    global static void getResults(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        if(req.requestURI == '/WAService/search') {
            String mobileNumber = req.params.get('mobile');
            if (mobileNumber.length() > 10) {
                mobileNumber = mobileNumber.right(10);                
            }
            mobileNumber = '%'+mobileNumber;
            String recordTypeName = 'Pre Inquiry';
            Inquiry__c inq = new Inquiry__c();
            try {
                inq = [SELECT Id, Name, First_Name__c, Last_Name__c FROM Inquiry__c 
                       WHERE Mobile_Phone__c LIKE : mobileNumber 
                       AND RecordType.Name =:recordTypeName
                       AND Owner.IsActive = TRUE
                       AND Is_Owner_Queue__c = FALSE
                       Order By LastModifiedDate Desc
                       LIMIT 1];
            } catch (Exception e) {}
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(inq));
        }
        if(req.requestURI == '/WAService/activeUsers') 
        {
            List <User> userList = new List<User>();
            Set <ID> userIds = new Set <ID> ();
            for (Inquiry_User_Assignment_Rules__c rule :[SELECT SetupOwnerId FROM Inquiry_User_Assignment_Rules__c WHERE Available_For_WA__c = TRUE]) {
                userIds.add(rule.setupOwnerId);
            }
            if (userIds.size() > 0)
                userList = [SELECT Name, IsActive FROM User WHERE id IN: userIds];
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(json.serialize(userList));
        }
    }
    
    
    @HttpPOST
    global static void createWAMessage(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        if(req.requestURI == '/WAService/message') {
            String reqBody = req.requestBody.toString().trim();
            
            createRequest(reqBody);
            
            RestContext.response.addHeader('Content-Type', 'application/json');
            
        }
        
    }
    
    public static void createRequest(String reqBody){    
        System.Debug(reqBody);
        Map<String, Object> reqJson = (Map<String, Object>) JSON.deserializeUntyped(reqBody);
        ID inqId = String.valueOf(reqJson.get('InqId'));
        String msgText = String.valueOf(reqJson.get('Message'));
        Inquiry__c inq = [select OwnerId FROM Inquiry__c WHERE Id =: inqId];
        
        // Insert Parent
        Nexmo_Whats_App_Request__c request = new Nexmo_Whats_App_Request__c();
        request.Record_Id__c = inqId;
        request.Record_Name__c = getRecordName(inqId, 'Mobile_Phone__c').substringbefore(';');
        request.To_number__c = decimal.valueof(getRecordName(inqId, 'Mobile_Phone__c').substringafter(';').removeStart('00'));
        request.Unique_Key__c =  getRecordName(inqId, 'Mobile_Phone__c').substringafter(';').removestart('00')+inq.ownerId;
        request.ownerid = inq.ownerId;
        if (reqJson.containsKey ('isNewNumber')) {
            if (reqJson.get('isNewNumber') != '' && reqJson.get('isNewNumber') != NULL)
                request.Is_incoming_from_New_Number__c = Boolean.valueOf(reqJson.get('isNewNumber'));
        }
        if (reqJson.containsKey ('ReceivedOn')) {
            if (reqJson.get('ReceivedOn') != '' && reqJson.get('ReceivedOn') != NULL)
                request.Latest_Inbound_Message_Received_on__c = DateTime.valueOf(String.valueOf(reqJson.get('ReceivedOn')).replace('T', ' '));
        }
        
        request.Last_Message_Sent__c = system.now();
        request.Last_Message__c = msgText;
        upsert request Unique_Key__c;
        System.Debug(request.unique_key__c);
        
        // Insert Child
        Nexmo_Whats_App_Message__c message = new Nexmo_Whats_App_Message__c();
        message.Nexmo_Whats_App_Request__c = request.id;
        message.direction__c = String.valueOf(reqJson.get('direction'));
        message.chatbot__c = true;
        try {
            for(string s: reqJson.keyset()){
                if(s == 'lattitude'){
                    message.message_body_lat__c = String.valueOf(reqJson.get(s));                     
                }
                if(s == 'longitude'){
                    message.message_body_lng__c = String.valueOf(reqJson.get(s));                     
                }
                if(s == 'mediaURL'){
                    message.message_body_url__c = String.valueOf(reqJson.get(s));                     
                }
                
            }
            
            
            Nexmo_Whats_App_Message__c mes = [select lastmodifiedbyid from Nexmo_Whats_App_Message__c where Nexmo_Whats_App_Request__c =: request.id limit 1];
            message.ownerId = mes.lastmodifiedbyid ;
        } catch (Exception e) {}
        message.Nexmo_Message_uuid__c = String.valueOf(reqJson.get('message_uuid'));
        message.Nexmo_Message_Body__c = msgText;
        message.Nexmo_Message_Request_Body__c = reqBody;
        insert message;
    }
    
    public static string getRecordName(Id recordId, String APIName){
        String sObjName = recordId.getSObjectType().getDescribe().getName();        
        Nexmo_WA_Fields__mdt waFldMapping = new Nexmo_WA_Fields__mdt();
        waFldMapping = [select id,API_Name__c,Object_Name__c from Nexmo_WA_Fields__mdt where Object_Name__c =:sObjName LIMIT 1];
        if(APIName == null){
            APIName = waFldMapping.API_Name__c;
        }
        String soqlQuery;
        soqlQuery = 'SELECT Id,Name,'+APIName+' from '+sObjName+ ' WHERE ID=:recordId';
        
        String valToreturn;
        Sobject sfRec = database.query(soqlQuery);
        
        if (sfRec.get(APIName) != null){
            valToreturn = string.valueof(sfRec.get('Name'))+';'+string.valueof(sfRec.get(APIName));
        } else {
            valToreturn = string.valueof(sfRec.get('Name'))+';'+string.valueof(0);
        }
        return valToreturn;
    }
}