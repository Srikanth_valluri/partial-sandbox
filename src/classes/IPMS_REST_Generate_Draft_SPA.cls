/************************************************************************************************
 * @Name              : IPMS_REST_Generate_Draft_SPA
 * @Test Class Name   : IPMS_REST_Generate_Draft_SPA_Test
 * @Description       : invokes the Queueable webservice class on Generate Draft SPA button click
 * Modification Log
 * 1.0    QBurst    29/01/2020        Created Class
 ************************************************************************************************/

global class IPMS_REST_Generate_Draft_SPA{

    public static string endPointURL;
    public static string sourceSystem;
    
    /**********************************************************************************************
    * @Description : Method to contain logic to be executed before insert.                        
    * @Params      : Id bookingUnitId - Id of Booking Unit for which Draft SPA is to be generated
    * @Return      : void                                                                         
    **********************************************************************************************/
    @future(callout=true)
    public static void DataForRequest(Id bookingUnitId){    
        system.debug('Inside DataForRequest()');
        Booking_Unit__c bookingUnit = [SELECT Id, Booking__c, Registration_id__c, Booking__r.Deal_SR__c
                                    FROM Booking_Unit__c 
                                    WHERE Id =: bookingUnitId LIMIT 1];
        IPMS_Integration_Settings__mdt settings = [SELECT Client_ID__c, Endpoint_URL__c, Username__c 
                                                   FROM IPMS_Integration_Settings__mdt 
                                                   WHERE DeveloperName = 'IPMS_Draft_SPA_Service' LIMIT 1];
        endPointURL = settings.Endpoint_URL__c + '/SPA?registration_id=' 
                                + bookingUnit.Registration_id__c + '&mode=DRAFT';
        system.enqueueJob(new Async_IPMS_Rest_Draft_SPA(endPointURL, bookingUnit));
    }
}