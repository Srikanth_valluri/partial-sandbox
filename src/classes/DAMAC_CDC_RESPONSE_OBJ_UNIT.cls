/*
Developed By: DAMAC IT Team
Usage:DAMAC_CDC_REST_UTILITY
*/

public class DAMAC_CDC_RESPONSE_OBJ_UNIT{
    
        public list<PaymentTerms> paymentTerms;
    
        public String X_RETURN_STATUS;  //S
        public String X_ERROR_MSG;  //Successfully fetched Data ..   
    
        public String REGISTRATION_ID;  //85957
        public String REGISTRATION_DATE;    //06-NOV-2006
        public String UNIT_NAME;    //NAP/2/204
        public String PROJECT_NAME; //THE PIAZZA
        public String STATUS;   //Agreement executed by DAMAC
        public String SELLER_NAME;  //Al Hikmah  International Enterprises WLL
        public String CUSTOMER_NAME;    //EIMAN SOLIMAN
        public String EMAIL_ADDRESS;    //XXn1ybm@GZMRZ
        public String ADDRESS1; //XXtn0gs QJD4N
        public String ADDRESS2;
        public String ADDRESS3;
        public String ADDRESS4;
        public String CITY; //Doha
        public String COUNTRY;  //Qatar
        public String POSTAL_CODE;
        public String PRIMARY_PHONE_NUM;    //974 33 45934068
        public String PHONE_NUMBER;
        public String FAX_NUMBER;
        public String NATIONALITY;  //CANADIAN
        public String PASSPORT_NO;
        public String DOCUMENTS_OK; //Y
        public String DEPOSIT_RECEIVED; //Y
        public String DISPUTE;
        public String FINANCE;  //HM
        public String SIZE_OF_APARTMENT;    //1278
        public String PERMITTED_USE;    //Residential
        public String APARTMENT_PRICE;  //1189000
        public String PARKING_ADDENDUM; //0
        public String CM_PRICE;
        public String PRESENT_CM_PRICE;
        public String EFF_DATE_OF_CURR_CM_PRICE;
        public string SFSRNUMBER;
        public string SFSRSTATUS;
        public string CDCSTATUS;
    
    
    
    public class PaymentTerms{
        public String INSTALLMENT;  //DP
        public String EVENT;    //Immediate
        public String EXPECTED_DATE;
        public String PERCENT;  //10
        public String PAYMENT_DATE; //02-JAN-2017
    }
    
    public static DAMAC_CDC_RESPONSE_OBJ_UNIT parse(String json){
        return (DAMAC_CDC_RESPONSE_OBJ_UNIT) System.JSON.deserialize(json, DAMAC_CDC_RESPONSE_OBJ_UNIT.class);
    }   
}