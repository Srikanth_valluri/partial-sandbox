/************************************************************************************************
 * @Name              : DAMAC_Agents_UnitDocuments
 * @Test Class Name   : DAMAC_Agents_UnitDocuments_Test
 * @Description       : RestResource Class for fetching Unit Documents for a given Contact 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         26/03/2020       Created
***********************************************************************************************/

@RestResource(urlMapping='/unitdocuments/*')
global without sharing class DAMAC_Agents_UnitDocuments{   

    @HttpGet
    global static void doGet(){
        RestRequest request = RestContext.request;
        String contactId = '';
        contactId = request.requestURI.substringAfter('unitdocuments/');
        System.Debug ('contactId ::::' + contactId);
        Map<String, String> docNameMap = new Map<String, String>();
        String jsonBody = '{';
        if(contactId != null && contactId != ''){
            // fetching Unit Documents
            for(Amendment__c amnd: [SELECT Id, Name, 
                                        (SELECT Id, Document_Name__c, Sys_Doc_ID__c, CV_Id__c, Preview_Download_Final__c, Document_Template__r.Name 
                                        FROM Unit_Documents__r WHERE Status__c = 'Uploaded' OR Status__c = 'Generated' OR Status__c = 'Approved') 
                                    FROM Amendment__c 
                                    WHERE Contact__c =: contactId
                                    AND Id IN (SELECT Amendment__c FROM Unit_Documents__c 
                                               WHERE Status__c = 'Uploaded' OR Status__c = 'Generated' OR Status__c = 'Approved') 
                                    ORDER BY CreatedDate DESC]){
                system.debug('amnd: ' + amnd);
                for(Unit_Documents__c unitDoc: amnd.Unit_Documents__r){
                    if(unitDoc.Document_Template__c != null && unitDoc.Document_Template__r.Name != ''
                                && !docNameMap.containsKey(unitDoc.Document_Template__r.Name)){
                        String body = '';
                        String url = URL.getOrgDomainUrl().toExternalForm();
                        if(unitDoc.Sys_Doc_ID__c != null && unitDoc.Sys_Doc_ID__c != '') {
                            if(UnitDoc.Sys_Doc_ID__c.startsWith('068')){
                                url += '/sfc/servlet.shepherd/version/download/' + unitDoc.CV_Id__c + '?asPdf=false&operationContext=CHATTER';
                            } else if(unitDoc.CV_Id__c != null && unitDoc.CV_Id__c != '') {
                                url += '/sfc/servlet.shepherd/version/download/' + unitDoc.CV_Id__c + '?asPdf=false&operationContext=CHATTER';
                            } else {
                                url +=  '/servlet/servlet.FileDownload?file=' + unitDoc.Sys_Doc_ID__c;
                            }
                        } else{
                            url = 'URL Missing';
                        }
                        system.debug('url: ' + url);
                        body += '"Name": "' + unitDoc.Document_Name__c + '", '
                                  + '"Template Name": "' + unitDoc.Document_Template__r.Name + '", '
                                  + '"Download URL": "' + url + '"}';
                        docNameMap.put(unitDoc.Document_Template__r.Name, body);
                    }
                }
            }
            // fetching SR Docs
            for(Amendment__c amnd: [SELECT Id, Name, 
                                        (SELECT Id, NSIBPM__Document_Name__c, NSIBPM__Doc_ID__c, Preview_Download_Document__c, 
                                                NSIBPM__SR_Template_Doc__r.NSIBPM__Document_Description__c 
                                          FROM SR_Docs__r 
                                          WHERE NSIBPM__Status__c = 'Uploaded' 
                                              OR NSIBPM__Status__c = 'Generated' 
                                              OR NSIBPM__Status__c = 'Approved')  
                                    FROM Amendment__c 
                                    WHERE Contact__c =: contactId
                                    AND Id IN (SELECT Amendment__c FROM NSIBPM__SR_Doc__c 
                                               WHERE NSIBPM__Status__c = 'Uploaded' 
                                                   OR NSIBPM__Status__c = 'Generated' 
                                                   OR NSIBPM__Status__c = 'Approved')
                                    ORDER BY CreatedDate DESC]){
                system.debug('amnd: ' + amnd);
                for(NSIBPM__SR_Doc__c srDoc: amnd.SR_Docs__r){
                        if(srDoc.NSIBPM__SR_Template_Doc__c != null && srDoc.NSIBPM__SR_Template_Doc__r.NSIBPM__Document_Description__c != ''
                                        && !docNameMap.containsKey(srDoc.NSIBPM__SR_Template_Doc__r.NSIBPM__Document_Description__c)){
                        String body = '';
                        String url = URL.getOrgDomainUrl().toExternalForm();
                        if(srDoc.NSIBPM__Doc_ID__c != null && srDoc.NSIBPM__Doc_ID__c != '') {
                            url +=  '/servlet/servlet.FileDownload?file=' + srDoc.NSIBPM__Doc_ID__c ;
                        } else{
                            url = 'URL Missing';
                        }
                        system.debug('url: ' + url);
                        body += '"Name": "' + srDoc.NSIBPM__Document_Name__c + '", '
                                  + '"Template Name": "' + srDoc.NSIBPM__SR_Template_Doc__r.NSIBPM__Document_Description__c + '", '
                                  + '"Download URL": "' + url + '"}';
                        docNameMap.put(srDoc.NSIBPM__SR_Template_Doc__r.NSIBPM__Document_Description__c, body);
                    }
                }
            }
        }
        // adding document details to JSON
        for(String body: docNameMap.values()){
            if(jsonBody != '{'){
                jsonBody += ', {' +  body;
            } else{
                jsonBody += '"data": [{'  +  body;
            }
        }
        if(jsonBody != '{'){
            jsonBody += ']';
        }
        jsonBody += '}';
        system.debug('jsonBody: ' + jsonBody);
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(jsonBody);  
    }
}