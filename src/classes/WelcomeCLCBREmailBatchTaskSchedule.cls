/**
 * @Description        : 
 * Ver       Date            Author              Modification
 * 1.0    9/26/2019         Arsh Dave               Initial Version
**/
global class WelcomeCLCBREmailBatchTaskSchedule implements Schedulable {
  global void execute(SchedulableContext sc) {
     
      database.executebatch(new WelcomeCLCBREmailBatch_Task(), 1);
  }
}