/*****************************************************************************************************
* Description - Test class developed for 'CheckAllTasksOfHOBuilding'
*
* Version            Date            Author               Description
* 1.0               04/02/2018                   		Initial Draft
 *******************************************************************************************************/
@isTest
private class CheckAllTasksOfHOBuildingTest {
      static testmethod void unitTestCheckAllTasksOfHOBuilding(){
      	Set<Id> setTaskId = new Set<Id>();
        List<Id> listTaskId = new List<Id>();
        List<Location__c>locLst = new List<Location__c>();
      	Location__c locObj1 = new Location__c();
      	locObj1.Location_ID__c = '56563';
      	locLst.add(locObj1);
      	
      	Location__c locObj2 = new Location__c();
      	locObj2.Location_ID__c = '56656';
      	locLst.add(locObj2);
      	
      	insert locLst;
      	
      	List<Task>taskList = new List<Task>();
     	Task taskObj1 = new Task();
        taskObj1.OwnerId = UserInfo.getUserId();
        taskObj1.Subject = 'Confirm Labour Delay applied Handover';
        taskObj1.Status = 'Completed';
        taskObj1.Priority = 'Normal';
        taskObj1.WhatId = locObj1.Id;
        taskList.add(taskObj1);
        
        Task taskObj2 = new Task();
        taskObj2.OwnerId = UserInfo.getUserId();
        taskObj2.Subject = 'Confirm penalties posted - Handover';
        taskObj2.Status = 'Completed';
        taskObj2.Priority = 'Normal';
        taskObj2.WhatId = locObj1.Id;
        taskList.add(taskObj2);
         
        Task taskObj3 = new Task();
        taskObj3.OwnerId = UserInfo.getUserId();
        taskObj3.Subject = 'Confirm Area Variation posted - Handover';
        taskObj3.Status = 'Completed';
        taskObj3.Priority = 'Normal';
        taskObj3.WhatId = locObj1.Id;
        taskList.add(taskObj3);
        
        Task taskObj4 = new Task();
        taskObj4.OwnerId = UserInfo.getUserId();
        taskObj4.Subject = 'Confirm Recharges Posted - Handover';
        taskObj4.Status = 'Completed';
        taskObj4.Priority = 'Normal';
        taskObj4.WhatId = locObj1.Id;
        taskList.add(taskObj4);
        
        Task taskObj5 = new Task();
        taskObj5.OwnerId = UserInfo.getUserId();
        taskObj5.Subject = 'Confirm final invoice generated - Handover';
        taskObj5.Status = 'Completed';
        taskObj5.Priority = 'Normal';
        taskObj5.WhatId = locObj1.Id;
        taskList.add(taskObj5);
        
        Task taskObj6 = new Task();
        taskObj6.OwnerId = UserInfo.getUserId();
        taskObj6.Subject = 'Confirm service charges posted - Handover';
        taskObj6.Status = 'Completed';
        taskObj6.Priority = 'Normal';
        taskObj6.WhatId = locObj2.Id;
        taskList.add(taskObj6);
        
        insert taskList;
         System.debug('taskList:size:'+taskList.size());
        System.debug('taskList::'+taskList);
        for(Task taskO : taskList){
        	setTaskId.add(taskO.Id);
        }
         Test.startTest();
         listTaskId.addAll(setTaskId);
         CheckAllTasksOfHOBuilding.updateTask(listTaskId);  
         Test.stopTest();
            
     }
     
}