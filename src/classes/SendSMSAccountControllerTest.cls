/*
* Description - Test class developed for 'SendSMSAccountController'
*
* Version            Date            Author            Description
* 1.0                26/11/17        Lochan           Initial Draft
*/
@isTest
public class SendSMSAccountControllerTest {
    
    @testSetup static void setup() {
        EmailTemplate et1 = new EmailTemplate (developerName = 'test_EmailTemplateName', IsActive=true, FolderId = UserInfo.getUserId(),TemplateType= 'text', Name = 'test'); // plus any other fields that you want to set
        insert et1;

    }
    
    static testMethod void testMethod1() {
    
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_2__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_3__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_4__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_5__pc = '123123123';
        objAcc.Telephone__c = '123123123';
        insert objAcc ;

        //Insert Service Request
        /*NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;*/

        ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
               
        PageReference pageRef = Page.SendSMSonAccount;
        pageRef.getParameters().put('Id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        SendSMSAccountController obj = new SendSMSAccountController(controller);
        obj.init();
    }

    static testMethod void testMethod2() {
        List<User> userList = [SELECT Id from User where Id != :UserInfo.getUserId() AND isActive = True LIMIT 1];
        System.debug('userList'+userList); 
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId,
                                     Name = 'Test Name' ,
                                     First_Name__c='Test FirstName',
                                      Last_Name__c='Test LastName',
                                       Type='Other',
                                       FAM_CAUTION__c = Label.FAMFieldValue,
                                       Primary_CRE__c = userList[0].Id
                                      );
        insert objAcc;
        
        /*Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.PersonEmail = 'a@a.com'; 
        objAcc.Mobile_Phone_Encrypt__c = '243213424'; 
        objAcc.Mobile_Phone_2__c = '242343'; 
        objAcc.Mobile_Phone_3__c = '231123'; 
        objAcc.Mobile_Phone_4__c = '24234'; 
        objAcc.Mobile_Phone_5__c = '2342345235'; 
        insert objAcc;*/

        Contact objCon = new Contact(Lastname='Test Lastname',AccountId = objAcc.Id);
        objCon.Mobile_Phone__c = '009123121323';
        objCon.Mobile_Phone_2__c = '1213322323';
        objCon.Mobile_Phone_3__c = '12132323';
        objCon.Mobile_Phone_4__c = '121324323';
        objCon.Mobile_Phone_5__c = '12123323';
         
        insert objCon;
        
        //Insert Service Request
        /*NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;

        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;*/

        ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
               
        PageReference pageRef = Page.SendSMSonAccount;
        pageRef.getParameters().put('Id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        SendSMSAccountController obj = new SendSMSAccountController(controller);
        List<string> lstPhnumber = new List<string>{'00971550524145'};
        
        /*obj.nsPhones = new set<String>{'00971550524145','00971550824145'};
        SelectOption firstOpt = new SelectOption('00971550524145','00971550524145');
        obj.soptions = new List<SelectOption>{firstOpt};
        obj.nsoptions = new List<SelectOption>{firstOpt};
        obj.selected = lstPhnumber;
        obj.sPhones = new set<String>{'00971550524145'};
        obj.removed = lstPhnumber;*/
        obj.init();
        obj.addElements();
        obj.removeElements();
        obj.callSMSService();
        List<SelectOption> SO = obj.getMyPersonalTemplateOptions();

        EmailTemplate et = [Select Id from EmailTemplate Where developerName = 'test_EmailTemplateName' Limit 1];
        obj.accountId = objAcc.Id; 
        obj.selectedTemplateId = et.Id;
        obj.showContent();
    }
    
}