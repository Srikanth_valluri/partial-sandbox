public  class AccountAttachmentController {
    public String selectedType                                         {get;set;}
    public Boolean selectedAwesomeness                                 {get;set;} 
    public String description                                          {get;set;}
    public Account objAccount                                          {get;set;}
    public String fileName                                             {get;set;}
    public Blob fileBody                                               {get;set;}
    public String nameDocument                                         {get;set;}
    public String selectedDocType                                      {get;set;}
    public List<selectOption> lstDocType                               {get;set;}
    public Account objAccount1;
    public SR_Attachments__c attachmentObj;
    //public Id Individual_AgencyId;
    //public Id Person_AccountId;
    public AccountAttachmentController(ApexPages.StandardController controller) {
        System.debug('===Constructor called==' + controller);
        this.objAccount = (Account) controller.getRecord();
        System.debug('===this.objAccount==' + this.objAccount);
        Map<String, Schema.RecordTypeInfo> schemaMap = Schema.SObjectType.Case.getRecordTypeInfosByName();
        System.debug('===Constructor schemaMap==' + schemaMap);
     //     Person_AccountId = schemaMap.get('Person Account').getRecordTypeId();
    }
    /* creates a new SR_Attachments__c record*/
    @TestVisible
    private Database.SaveResult saveCustomAttachment() {
        attachmentObj = new SR_Attachments__c();
        attachmentObj = getDocUrl(attachmentObj);
        system.debug('getDocUrl obj : '+attachmentObj);
        return Database.insert(attachmentObj);
    }
    public PageReference processUpload() {
        selectedDocType = objAccount.Document_Type__c;
       // if(objAccount.RecordTypeId == Person_AccountId ) {
            if(selectedDocType == 'none') {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select Document Type.'));
                return null;
            }
       // }
        try {
            Database.SaveResult customAttachmentResult = saveCustomAttachment();

            if (customAttachmentResult == null || !customAttachmentResult.isSuccess()) {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                    'Could not save attachment.'));
                return null;
            } else {
                //if(objAccount.RecordTypeId == Person_AccountId ) {
                    if(selectedDocType.equalsIgnoreCase('Additional Document')) {
                        objAccount1.Additional_Doc_File_URL__c = attachmentObj.Attachment_URL__c;
                    } 
                    else if(selectedDocType.equalsIgnoreCase('COD Document')) {
                        objAccount1.CRF_File_URL__c = attachmentObj.Attachment_URL__c;
                    } else if(selectedDocType.equalsIgnoreCase('Court Order')) {
                        objAccount1.CRF_File_URL__c = attachmentObj.Attachment_URL__c;
                    } else if(selectedDocType.equalsIgnoreCase('LOD Document')) {
                        objAccount1.OD_File_URL__c = attachmentObj.Attachment_URL__c;
                    } else if(selectedDocType.equalsIgnoreCase('Passport Document')) {
                        objAccount1.Passport_File_URL__c = attachmentObj.Attachment_URL__c;
                    } else if(selectedDocType.equalsIgnoreCase('POA document')) {
                        objAccount1.POA_File_URL__c = attachmentObj.Attachment_URL__c;
                    }
                    objAccount1.Document_Type__c = selectedDocType;
                    update objAccount1;
                    System.debug('===acc updated ==' + objAccount1);
                //}
            }
        } catch (Exception e) {
            ApexPages.AddMessages(e);
            return null;
        }
        // return null;
        return new PageReference('/' + objAccount.Id);
    }

    public PageReference back() {
        return new PageReference('/' + objAccount.Id);
    }
    
    
    public SR_Attachments__c getDocUrl(SR_Attachments__c obj) {
        objAccount1 = [Select Id,
                            AccountNumber
                            From Account
                      Where Id =: objAccount.Id
                    ];
        if (fileName != null && fileBody != null) {
            obj.Account__c = objAccount.Id;
            obj.Name = nameDocument;
            obj.description__c = description;
            obj.type__c = selectedType;
            obj.isValid__c = selectedAwesomeness;

            List < UploadMultipleDocController.MultipleDocRequest > lstMultipleDocReq = new List < UploadMultipleDocController.MultipleDocRequest > ();
            UploadMultipleDocController.data respObj = new UploadMultipleDocController.data();
            List < SR_Attachments__c > lstSRAttacg = new List < SR_Attachments__c > ();
            String errorMessage = '';
            List < Error_Log__c > errorLogList = new List < Error_Log__c > ();
            String stType = extractType(fileName);
            String crfAttachmentName = extractName(fileName);
            crfAttachmentName = crfAttachmentName.replaceAll(' ','_');
            crfAttachmentName = crfAttachmentName.replaceAll('/','_');
            system.debug('crfAttachmentName************'+crfAttachmentName);

            UploadMultipleDocController.MultipleDocRequest reqObjCRF = new UploadMultipleDocController.MultipleDocRequest();
            reqObjCRF.base64Binary = EncodingUtil.base64Encode(fileBody);
            reqObjCRF.category = 'Document';
            reqObjCRF.entityName = 'Damac Service Requests';
            reqObjCRF.fileDescription = fileName;
            //reqObjCRF.fileId = 'IPMS-' + objAccount1.CaseNumber + '-' + crfAttachmentName;
            //reqObjCRF.fileName = 'IPMS-' + objAccount1.CaseNumber + '-' + crfAttachmentName;
            reqObjCRF.fileId = objAccount1.AccountNumber + String.valueOf(System.currentTimeMillis())+'.'+stType;
            reqObjCRF.fileName = objAccount1.AccountNumber + String.valueOf(System.currentTimeMillis())+'.'+stType;
            
            reqObjCRF.registrationId = objAccount1.AccountNumber;
            reqObjCRF.sourceFileName = 'IPMS-' + objAccount1.AccountNumber + '-' + crfAttachmentName +'.'+stType;
            reqObjCRF.sourceId = 'IPMS-' + objAccount1.AccountNumber + '-' + crfAttachmentName;
            //fileBody = NULL; //if user deletes, and saves the draft
            lstMultipleDocReq.add(reqObjCRF);

            if (lstMultipleDocReq.size() > 0) {
                respObj = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);
                System.debug('===respObj==' + respObj);
            }

            if (respObj != NULL && lstMultipleDocReq.size() > 0) {
                if (respObj.status == 'Exception') {
                    errorMessage = respObj.message;
                    Error_Log__c objErr = new Error_Log__c();
                    errorMessage = respObj.message;
                    objErr.Error_Details__c = errorMessage;
                    objErr.Account__c = objAccount.Id;
                    errorLogList.add(objErr);
                }
                if (respObj.Data == null || respObj.Data.size() == 0) {
                    errorMessage = 'Problems while getting response from document upload';
                    Error_Log__c objErr = new Error_Log__c();
                    errorMessage = respObj.message;
                    objErr.Error_Details__c = errorMessage;
                    objErr.Account__c = objAccount.Id;
                    errorLogList.add(objErr);
                }

                for (UploadMultipleDocController.MultipleDocResponse objData: respObj.data) {
                    if (objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)) {
                        obj.Attachment_URL__c = objData.url;
                        system.debug('objData.url : '+objData.url);
                        system.debug('obj.Attachment_URL__c : '+obj.Attachment_URL__c);
                    } else {
                        errorMessage = 'Problems while getting response from document ' + objData.PARAM_ID;
                        Error_Log__c objErr = new Error_Log__c();
                        errorMessage = respObj.message;
                        objErr.Error_Details__c = errorMessage;
                        objErr.Account__c = objAccount.Id;
                        errorLogList.add(objErr);
                    }

                }

            }
            
            if (errorLogList != null && errorLogList.size() > 0) {
                insert errorLogList;
                System.debug('===errorLogList==' + errorLogList);
            }
        }
        return obj;
    }
    @TestVisible
    private String extractName(String strName) {
        return strName.substring(strName.lastIndexOf('\\') + 1);
    }
    @TestVisible
    private String extractType( String strName ) {
        strName = strName.substring( strName.lastIndexOf('\\')+1 );
        return strName.substring( strName.lastIndexOf('.')+1 ) ;
    }

}