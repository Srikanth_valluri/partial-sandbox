/***************************************************************************************************
 * @Description : Batch class to update DP Calling List                                            *
 *                                                                                                 *
 *                                                                                                 *
 *  Version     Author           Date         Description                                          *
 *  1.0                          26/12/2018   Initial development     

 
 
/*

We have below creteria in our batch :

Account__c not equals to null
AND Booking_Unit__c not equals to null
AND IsHideFromUI__c equals to false
AND Calling_List_Status__c not equals to 'Closed'
AND RecordTypeId equals to 'Collection Calling List'
AND Calling_List_Type__c equals to 'DP Calling'
AND owner.Name Not equals to Uncollectable_queue
AND owner.profile.Name Not equals to 'Collection - CRE', 'Collection - Director', 'Collection - Manager'
AND For_Welcome_Call__c = true
AND ( Calling_List_Month__c equals to 6 OR Calling_List_Month__c equals to 5)


if( callinglist --> Booking_Unit --> DP_OK == true ) {
simply hide that cl
}
else if( callinglist --> Booking_Unit --> DP_OK == false 
   AND callinglist --> Registration_Date  not equals to null 
     AND callinglist --> Account --> Country  not equals to null 
   AND callinglist --> Account --> Country = 'United Arab Emirates' 
   AND callinglist --> DP_Assignment_Date_CC   >= 11 ) {
    then hide current dp
    AND create new Collection CL and assign that to "Collection_New_Q"
}
else if( callinglist --> Booking_Unit --> DP_OK == false 
   AND callinglist --> Registration_Date  not equals to null 
     AND callinglist --> Account --> Country  not equals to null 
   AND callinglist --> Account --> Country = 'India','China','Nigeria','Ethiopia','Egypt',
                                                   'Democratic Republic of the Congo','South Africa',
                                                   'Tanzania','Kenya','Sudan','Algeria','Uganda',
                                                   'Morocco','Mozambique','Ghana','Angola','Ivory Coast',
                                                   'Madagascar','Cameroon','Niger','Burkina Faso','Mali',
                                                   'Malawi','Somalia','Chad','Zimbabwe','South Sudan',
                                                   'Rwanda','Tunisia','Guinea','Benin','Burundi','Eritrea',
                                                   'Togo','Sierra Leone','Libya','Republic of the Congo',
                                                   'Central African Republic','Liberia','Mauritania','Namibia',
                                                   'Botswana','Gambia','Equatorial Guinea','Lesotho','Gabon',
                                                   'Guinea-Bissau','Mauritius','Eswatini (Swaziland)','Djibouti',
                                                   'Réunion (France)','Cape Verde','Comoros','Western Sahara',
                                                   'Mayotte (France)','São Tomé and Príncipe','Seychelles' 
   AND callinglist --> DP_Assignment_Date_CC   >= 26 ) {
    then hide current dp
    AND create new Collection CL and assign that to "Collection_New_Q"
}          
else if( callinglist --> Booking_Unit --> DP_OK == false 
   AND callinglist --> Registration_Date  not equals to null 
     AND callinglist --> Account --> Country  not equals to null 
   AND callinglist --> Account --> Country not equals to 'India','China','Nigeria','Ethiopia','Egypt',
                                                   'Democratic Republic of the Congo','South Africa',
                                                   'Tanzania','Kenya','Sudan','Algeria','Uganda',
                                                   'Morocco','Mozambique','Ghana','Angola','Ivory Coast',
                                                   'Madagascar','Cameroon','Niger','Burkina Faso','Mali',
                                                   'Malawi','Somalia','Chad','Zimbabwe','South Sudan',
                                                   'Rwanda','Tunisia','Guinea','Benin','Burundi','Eritrea',
                                                   'Togo','Sierra Leone','Libya','Republic of the Congo',
                                                   'Central African Republic','Liberia','Mauritania','Namibia',
                                                   'Botswana','Gambia','Equatorial Guinea','Lesotho','Gabon',
                                                   'Guinea-Bissau','Mauritius','Eswatini (Swaziland)','Djibouti',
                                                   'Réunion (France)','Cape Verde','Comoros','Western Sahara',
                                                   'Mayotte (France)','São Tomé and Príncipe','Seychelles'
   AND callinglist --> Account --> Country not equals to 'United Arab Emirates'                           
   AND callinglist --> DP_Assignment_Date_CC   >= 16 ) {
    then hide current dp
    AND create new Collection CL and assign that to "Collection_New_Q"
}
else if( callinglist --> Booking_Unit --> DP_OK == false 
   AND callinglist --> Registration_Date  not equals to null 
     AND callinglist --> Account --> Country  equals to null 
   AND callinglist --> DP_Assignment_Date_CC   >= 11 ) {
    then hide current dp
    AND create new Collection CL and assign that to "Collection_New_Q"
}
logic ENDs here 
 
 
 
 
 
 
 
***************************************************************************************************/
global class UpdateDPCallingListBatch implements Database.batchable<sObject>, Database.Stateful {
  
    //Name of HOD
    Static String strHODName = 'Rami Tabbara';
  
    //Set of countries
    Static set<String> countries = new Set<String>{'India','China','Nigeria','Ethiopia','Egypt',
                                                   'Democratic Republic of the Congo','South Africa',
                                                   'Tanzania','Kenya','Sudan','Algeria','Uganda',
                                                   'Morocco','Mozambique','Ghana','Angola','Ivory Coast',
                                                   'Madagascar','Cameroon','Niger','Burkina Faso','Mali',
                                                   'Malawi','Somalia','Chad','Zimbabwe','South Sudan',
                                                   'Rwanda','Tunisia','Guinea','Benin','Burundi','Eritrea',
                                                   'Togo','Sierra Leone','Libya','Republic of the Congo',
                                                   'Central African Republic','Liberia','Mauritania','Namibia',
                                                   'Botswana','Gambia','Equatorial Guinea','Lesotho','Gabon',
                                                   'Guinea-Bissau','Mauritius','Eswatini (Swaziland)','Djibouti',
                                                   'Réunion (France)','Cape Verde','Comoros','Western Sahara',
                                                   'Mayotte (France)','São Tomé and Príncipe','Seychelles'//,'Saint Helena Ascension and Tristan da Cunha (UK)'
    };
    
    global Database.QueryLocator start(Database.BatchableContext info) {
        System.debug('=== DP Calling start ===');
        
        Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        System.debug('devRecordTypeId ===' + devRecordTypeId);
        
        Id  OwnerId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName('Collection_New_Q','Queue');
        
        Set<String> setCollectionProfiles = new set<String> {'Collection - CRE', 'Collection - Director', 'Collection - Manager'};
        
    String strCurrMonth = String.valueOf(Date.Today().Month());
    String strPreviousMonth = String.valueOf(Date.Today().addMonths(-1).Month());
    
        /*String query =  ' Select Id,Registration_ID__c,OwnerId,RecordTypeId,Account__r.Country__pc, nationality__c,Calling_List_Type__c, HOD_Name__c,Booking_Unit__r.DP_OK__c'+
                        ' ,DP_Call_Today__c,Booking_Unit__c,Registration_Date__c, DP_OK__c, DP_Assignment_Date_CC__c, owner.profile.Name, owner.Name FROM Calling_List__c '+
                        ' Where Account__c != null AND IsHideFromUI__c = false '+
                        ' AND Calling_List_Status__c != \'Closed\''+ 
                        ' AND RecordTypeId =: devRecordTypeId AND Calling_List_Type__c = \'DP Calling\' '+//AND id=\'a3A25000000BKH2\' ';
                        ' AND owner.Name !=: OwnerId '+
                        ' AND  owner.profile.Name NOT IN : setCollectionProfiles AND For_Welcome_Call__c = true AND ( Calling_List_Month__c =: strCurrMonth OR Calling_List_Month__c =: strPreviousMonth )';//AND id=\'a3A25000000BKH2EAO\' ';
            */

		String query =  ' Select Id,Registration_ID__c,OwnerId,RecordTypeId,Account__r.Country__pc, nationality__c,Calling_List_Type__c, HOD_Name__c,Booking_Unit__r.DP_OK__c'+
                        ' ,DP_Call_Today__c,Booking_Unit__c,Registration_Date__c, DP_OK__c, DP_Assignment_Date_CC__c, owner.profile.Name, owner.Name FROM Calling_List__c '+
                        ' Where Account__c != null AND IsHideFromUI__c = false '+
                        ' AND Calling_List_Status__c != \'Closed\''+ 
                        ' AND RecordTypeId =: devRecordTypeId AND Calling_List_Type__c = \'DP Calling\' '+
                        ' AND ownerId =: OwnerId AND Booking_Unit__r.Unit_Active__c = \'Active\' '+
                        ' AND Booking_Unit__r.Booking_Type__c = \'NW\' ';//AND id=\'a371n000000ENxe\' ';
             			
        system.debug( 'query  : '+ query );
        return Database.getQueryLocator(query);    
  }
  
  global void execute( Database.BatchableContext info, List<Calling_List__c> lstCallingLists ) {
        /*System.debug('====DP calling execute ====');
        System.debug('lstCallingLists ====' + lstCallingLists);
        
        //List of calling lists to be update
        List<Calling_List__c> lstCallingListsToUpdate = new List<Calling_List__c>();
  
        //Id of DP Calling List owner
        Id dpOwnerId = [SELECT 
            Id
          FROM 
            User
          WHERE 
            Name =: Label.DP_Calling_List_Owner 
            AND isActive = true LIMIT 1][0].Id;
        System.debug('dpOwnerId ===' + dpOwnerId);
        System.debug('countries ===' + countries.size());
        
        //Record type Id of Elite Calling List
        Id eliteRecTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get(
              'Elite Calling List').getRecordTypeId();
        //Id of Request for elite queue
        Id eliteOwnerId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName( 'Request_for_elite','Queue' );
            //[SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperNAME = 'Request_for_elite'][0].Id;
            //CallingListUtility.getGroupIdFromDeveloperName('Request_for_elite','Queue' );
    System.debug('eliteRecTypeId ===' + eliteRecTypeId);
        System.debug('eliteOwnerId ===' + eliteOwnerId);*/
    
    List<Calling_List__c> lstCLToInsertUpdate = new List<Calling_List__c>();
    List<Calling_List__c> lstToInsert = new List<Calling_List__c>();
    
    /*for(Calling_List__c objCl : lstCallingLists) {
        System.debug('objCl===' + objCl);
        if( objCl.Booking_Unit__c != null ) {
            if( objCl.Booking_Unit__r.DP_OK__c == true ) {
                system.debug('in if');
                objCL.IsHideFromUI__c = true;
                lstCLToInsertUpdate.add(objCL);
            }else if( objCl.Booking_Unit__r.DP_OK__c == false && objCl.Registration_Date__c != null 
                   && String.IsNotBlank( objCl.Account__r.Country__pc)) {
                if( objCl.Account__r.Country__pc.equalsIgnoreCase('United Arab Emirates') && objCl.DP_Assignment_Date_CC__c >= 11 ) {
                    system.debug('in else if 1');
                    objCL.IsHideFromUI__c = true;
                    lstCLToInsertUpdate.add(objCL);
                    lstToInsert = createCollectionCL(objCL);
                } else if( countries.contains(objCl.Account__r.Country__pc) && objCl.DP_Assignment_Date_CC__c >= 26 ) {
                    system.debug('in else if 2');
                    objCL.IsHideFromUI__c = true;
                    lstCLToInsertUpdate.add(objCL);
                    lstToInsert = createCollectionCL(objCL);
                } else if ( !countries.contains(objCl.Account__r.Country__pc) && !objCl.Account__r.Country__pc.equalsIgnoreCase('United Arab Of Emirates') 
                         && objCl.DP_Assignment_Date_CC__c >= 16  ) {
                    system.debug('in else if 3');
                    objCL.IsHideFromUI__c = true;
                    lstCLToInsertUpdate.add(objCL);
                    lstToInsert = createCollectionCL(objCL);
                }
            }else if(  objCl.Booking_Unit__r.DP_OK__c == false && objCl.Registration_Date__c != null 
                   && String.IsBlank( objCl.Account__r.Country__pc) && objCl.DP_Assignment_Date_CC__c >= 11 ) {
                    system.debug('in blaank country if else ');
                    objCL.IsHideFromUI__c = true;
                    lstCLToInsertUpdate.add(objCL);
                    lstToInsert = createCollectionCL(objCL);
      }
        }
    }*/


    Id ownerIdCollection_New_q1 = EmailMessageTriggerHandler.getGroupIdFromDeveloperName('Collection_New_q1','Queue');

    for(Calling_List__c objCl : lstCallingLists) {
        System.debug('objCl===' + objCl);
        if( objCl.Booking_Unit__c != null ) {
            if( objCl.Booking_Unit__r.DP_OK__c == true ) {
                system.debug('in if');
                objCL.IsHideFromUI__c = true;
                lstCLToInsertUpdate.add(objCL);
            }else if( objCl.Booking_Unit__r.DP_OK__c == false && objCl.Registration_Date__c != null 
                   && String.IsNotBlank( objCl.Account__r.Country__pc)) {
                if( objCl.Account__r.Country__pc.equalsIgnoreCase('United Arab Emirates') && objCl.DP_Assignment_Date_CC__c >= 11 ) {
                    system.debug('in else if 1');
                    //objCL.IsHideFromUI__c = true;
                    objCL.OwnerId = ownerIdCollection_New_q1;
                    lstCLToInsertUpdate.add(objCL);
                    //lstToInsert = createCollectionCL(objCL);
                } else if( countries.contains(objCl.Account__r.Country__pc) && objCl.DP_Assignment_Date_CC__c >= 26 ) {
                    system.debug('in else if 2');
                    //objCL.IsHideFromUI__c = true;
                    objCL.OwnerId = ownerIdCollection_New_q1;
                    lstCLToInsertUpdate.add(objCL);
                    //lstToInsert = createCollectionCL(objCL);
                } else if ( !countries.contains(objCl.Account__r.Country__pc) && !objCl.Account__r.Country__pc.equalsIgnoreCase('United Arab Of Emirates') 
                         && objCl.DP_Assignment_Date_CC__c >= 16  ) {
                    system.debug('in else if 3');
                    //objCL.IsHideFromUI__c = true;
                    objCL.OwnerId = ownerIdCollection_New_q1;
                    lstCLToInsertUpdate.add(objCL);
                    //lstToInsert = createCollectionCL(objCL);
                }
            }else if(  objCl.Booking_Unit__r.DP_OK__c == false && objCl.Registration_Date__c != null 
                   && String.IsBlank( objCl.Account__r.Country__pc) && objCl.DP_Assignment_Date_CC__c >= 11 ) {
                    system.debug('in blaank country if else ');
                    //objCL.IsHideFromUI__c = true;
                    objCL.OwnerId = ownerIdCollection_New_q1;
                    lstCLToInsertUpdate.add(objCL);
                    //lstToInsert = createCollectionCL(objCL);
      }
        }
    }    

        
        
      /*system.debug('owner ----> ' + objCl.Owner.Name + ' || ' + objCl.owner.profile.Name);
            if(String.IsNOtBlank(objCl.HOD_Name__c ) && objCl.HOD_Name__c != strHODName) {
        System.debug('---if HOD_Name__c!= StrHODName----');
                System.debug('country ====' + objCl.Account__r.Country__pc + ' || ' +objCl.nationality__c);
                    System.debug('strHODName ====' + strHODName);
                System.debug('objCl.DP_Assignment_Date_CC__c  ====' + objCl.DP_Assignment_Date_CC__c );
                if(!countries.IsEmpty() 
        && String.IsNotBlank( objCl.Account__r.Country__pc)
        && countries.contains(objCl.Account__r.Country__pc)
        && String.IsNotBlank(objCl.DP_Call_Today__c) 
                && objCl.DP_Call_Today__c == 'Local' 
        && objCl.DP_Assignment_Date_CC__c >= 25
                && objCl.nationality__c  != 'Chinese') {
          //change owner
          System.debug('---if local----');
          if(String.IsNotBlank(dpOwnerId)) {
            objCl.ownerId = dpOwnerId;
                        objCl.For_Welcome_Call__c = false;
            lstCallingListsToUpdate.add(objCl);
          }
        }
                else if(objCl.nationality__c  == 'Chinese'
                 && ((objCl.DP_Call_Today__c == 'Local' && objCl.DP_Assignment_Date_CC__c >= 12)
        || (objCl.DP_Call_Today__c == 'Intl' && objCl.DP_Assignment_Date_CC__c >= 16))) {
                  System.debug('---if Country__pc == Chinese ----');
                    if(eliteRecTypeId != Null)
                      objCl.RecordTypeId = eliteRecTypeId;
                    if(eliteOwnerId != Null)
                      objCl.ownerId = eliteOwnerId;
                    lstCallingListsToUpdate.add(objCl);
              }
        else if(!countries.IsEmpty() 
        && String.IsNotBlank( objCl.Account__r.Country__pc)
        && !countries.contains(objCl.Account__r.Country__pc)
        && String.IsNotBlank(objCl.DP_Call_Today__c)
        && ((objCl.DP_Call_Today__c == 'Local' && objCl.DP_Assignment_Date_CC__c >= 12)
        || (objCl.DP_Call_Today__c == 'Intl' && objCl.DP_Assignment_Date_CC__c >= 16))) {
          // assign to christopher
          System.debug('---if local or Intl----');
                    System.debug('---dpOwnerId----' +dpOwnerId);
          if(dpOwnerId != null) {
                        System.debug('---if dpOwnerId != null----'+ dpOwnerId );
            objCl.ownerId = dpOwnerId;
            objCl.For_Welcome_Call__c = false;
                        lstCallingListsToUpdate.add(objCl);
          }
        }
      }
      else if(String.IsNOtBlank(objCl.HOD_Name__c ) 
      && objCl.HOD_Name__c == strHODName 
      && objCl.DP_OK__c) {
                System.debug('---if HOD_Name__c == strHODName ----');
        objCl.Calling_List_Status__c = 'Closed';
        objCl.Call_Outcome__c = 'DP Paid';
        objCl.CE_Comments__c = 'Closed by system job, DP Paid';
        lstCallingListsToUpdate.add(objCl);
      }
            
            
    }
        System.debug('lstCallingListsToUpdate ====' + lstCallingListsToUpdate);
    if(!lstCallingListsToUpdate.ISEmpty() && lstCallingListsToUpdate.size() > 0) {
      update lstCallingListsToUpdate;
    }*/
    System.debug('lstCLToInsertUpdate==' + lstCLToInsertUpdate);
    System.debug('lstToInsert==' + lstToInsert);
    if( !lstCLToInsertUpdate.isEmpty() && lstCLToInsertUpdate.size() > 0  )  {
        update lstCLToInsertUpdate;
    }
    
    /*if( !lstToInsert.isEmpty() && lstToInsert.size() > 0 ) {
        insert lstToInsert;
    }*/
    
  }
  
  /*public static List<Calling_List__c> createCollectionCL( Calling_List__c objCL ) {
      system.debug('createCollectionCL==' + objCL);
        List<Calling_List__c> lstCL = new List<Calling_List__c>();
        Id collectionOwnerId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName( 'Collection_New_Q','Queue' );
        Calling_List__c objNewCLCall = objCL.clone(false,false,false,false);
        objNewCLCall.For_Welcome_Call__c = false;
        objNewCLCall.Welcome_Call_Outcome__c = '';
        objNewCLCall.Call_Outcome__c = '';
        objNewCLCall.Call_Outcome_Brkr__c = '';
        objNewCLCall.Call_Date__c = null;
        objNewCLCall.Result__c = '';
        objNewCLCall.Call_Back_Date__c = null;
        objNewCLCall.CE_Comments__c = '';
        objNewCLCall.MeetingDateTime__c = null;
        objNewCLCall.Promise_To_Pay_Date__c = null;
        objNewCLCall.PDC_Collected_Date__c = null;
        objNewCLCall.Welcome_Call_Remarks__c = '';
        objNewCLCall.Welcome_Call_Result__c = '';
        objNewCLCall.OwnerId = collectionOwnerId;
    objNewCLCall.Calling_List__c = objCL.Id;
    objNewCLCall.IsHideFromUI__c = false;
        lstCL.add(objNewCLCall);
        return lstCL;
  }*/
  
  
  global void finish(Database.BatchableContext info){     
    System.debug('====Dp calling END ====');
  } 
}