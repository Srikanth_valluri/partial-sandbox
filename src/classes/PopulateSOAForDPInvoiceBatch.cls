/**
 * @File Name          : PopulateSOAForDPInvoiceBatch.cls
 * @Description        : This Batch is chained from GenerateDPInvoiceDrawloopDocumentBatch to populate SOA 
 *                        for DPInvoice.
 * @Author             : Dipika Rajput
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/27/2019, 12:51:36 PM
 * @Modification Log   : 
 *==============================================================================================
 * Ver         Date                     Author                    Modification
 * 
 *==============================================================================================
 * 1.0    6/25/2019                   Dipika Rajput           Initial Version
**/
public without sharing class PopulateSOAForDPInvoiceBatch implements Database.Batchable<sObject>,
            Database.AllowsCallouts, Database.Stateful {

    private String query; 
    public map <Id,list<string>> mapDPInvoiceWithURLs;
    public List<DP_Invoices__c> soaDpInvoiceUpdateList;
    public Set<DP_Invoices__c> soaDpInvoiceUpdateset;
    public PopulateSOAForDPInvoiceBatch() {
      soaDpInvoiceUpdateList = new List<DP_Invoices__c>();
      soaDpInvoiceUpdateset = new Set<DP_Invoices__c>();
      mapDPInvoiceWithURLs = new map <Id,list<string>>();
    }
    public Database.Querylocator start( Database.BatchableContext bc ) {
        query = ' SELECT Id,'+ 
                ' Cover_Letter__c, '+
                ' SOA__c,'+
                ' TAX_Invoice__c,'+
                ' BookingUnits__r.Registration_ID__c,'+
                ' COCD_Letter__c '+
                ' FROM DP_Invoices__c '+
                ' Where '+
                ' SOA__c = NULL '+
                ' AND Other_Language_SOA__c = NULL';
        System.debug('Dp Invoice query ::: '+Database.getQueryLocator(query));
        return Database.getQueryLocator(query);
    }
    public void execute( Database.BatchableContext bc, list<DP_Invoices__c> dpInvoiceRecords ) {
        System.debug('dpInvoiceRecords ::: '+dpInvoiceRecords);
       
        // Iterate on Dp invoice records
        for(DP_Invoices__c dpInvoiceObj : dpInvoiceRecords){

                // Call SOA Language specific service which will give me all the list of SOA urls
                list<string> strSOAUrlResponseList = FmIpmsRestCoffeeServices.getAllUnitSoaInLanguageUrl(
                    dpInvoiceObj.BookingUnits__r.Registration_ID__c
                );
                System.debug('strSOAUrlResponseList ::: '+strSOAUrlResponseList);
                if(strSOAUrlResponseList != NULL && !strSOAUrlResponseList.isEmpty()){
                    
                    // Send SOAUrl Response list specific to dp Invoice record 
                    calloutToGetBlob(strSOAUrlResponseList, dpInvoiceObj);

                }
                System.debug('dpInvoiceObj ::: '+dpInvoiceObj);
        }
        System.debug('mapDPInvoiceWithURLs = ' + mapDPInvoiceWithURLs);
        
    }
    public void finish( Database.BatchableContext bc ) {
        System.debug('mapDPInvoiceWithURLs in finish= ' + mapDPInvoiceWithURLs);
        list<DP_Invoices__c> lstDPInvoicesToUpdate = new list<DP_Invoices__c>();
        
        for(DP_Invoices__c objDPInvoice : [Select id,Other_Language_SOA__c,SOA__c from DP_Invoices__c where id in: mapDPInvoiceWithURLs.keyset()]){
            list<string> lstURLs = new list<string>();
            lstURLs = mapDPInvoiceWithURLs.get(objDPInvoice.id);
            System.debug('lstURLs in finish= ' + lstURLs);
            if(!lstURLs.isEmpty()){
                System.debug('lstURLs in finish= ' + lstURLs.size());
                if(lstURLs.size() > 1){
                    objDPInvoice.SOA__c = lstURLs[0];
                    objDPInvoice.Other_Language_SOA__c = lstURLs[1];
                } else {
                    objDPInvoice.SOA__c = lstURLs[0];
                }   
            }   
            lstDPInvoicesToUpdate.add(objDPInvoice);                  
        }
        System.debug('lstDPInvoicesToUpdate in finish= ' + lstDPInvoicesToUpdate);
        if(!lstDPInvoicesToUpdate.isEmpty()){
            update lstDPInvoicesToUpdate;
        }
    }

    /* This Method is used to get the blob of the SOA urls for specific dpInoice record */
    public void calloutToGetBlob ( list<string> lstSoaResponse, DP_Invoices__c dpInvoiceRecord ){
        list<Blob> lstBlob = new list<blob>();
        String accessToken = FmIpmsRestCoffeeServices.accessToken;
        map<DP_Invoices__c,List<Blob>> dpInvoiceToSOAUrlBlobsMap = new map<DP_Invoices__c,List<Blob>>();
        System.debug('AccessToken is' + accessToken);
      
        if(!lstSoaResponse.isEmpty()){
            for(string objResponse: lstSoaResponse) {
                HttpRequest req = new HttpRequest();
                req.setEndpoint(objResponse);
                req.setHeader('Accept', 'application/json');
                req.setMethod('GET');
                req.setHeader('Authorization','Bearer' + accessToken);
                req.setTimeout(120000);
                HttpResponse response = new Http().send(req);
                System.debug('Status Code = ' + response.getStatusCode());
                System.debug('Pdf Body = ' + response.getBodyAsBlob());

                if(response.getStatusCode() == 200) {
                    blob resBlob =  response.getBodyAsBlob();
                    if(resBlob != NULL){
                        lstBlob.add(resBlob);
                    }                    
                }
            }            
        }
        System.debug('lstBlob = ' + lstBlob);
        if(!dpInvoiceToSOAUrlBlobsMap.containsKey(dpInvoiceRecord)){
            dpInvoiceToSOAUrlBlobsMap.put(
                dpInvoiceRecord,
                lstBlob
            );
        }
        System.debug('dpInvoiceToSOAUrlBlobsMap = ' + dpInvoiceToSOAUrlBlobsMap); 
        SendContentToCentralRepository.getPublicUrlsOfSOAForDpInvoice(dpInvoiceToSOAUrlBlobsMap);
        System.debug('SendContentToCentralRepository.mapAttachURLwithDPInvoice = ' + SendContentToCentralRepository.mapAttachURLwithDPInvoice); 
        for(Id DPInvoiceId: SendContentToCentralRepository.mapAttachURLwithDPInvoice.keyset()){
            mapDPInvoiceWithURLs.put(DPInvoiceId,SendContentToCentralRepository.mapAttachURLwithDPInvoice.get(DPInvoiceId));
        }
        System.debug('mapDPInvoiceWithURLs = ' + mapDPInvoiceWithURLs); 
    }

}