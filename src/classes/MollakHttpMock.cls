@isTest
global Class MollakHttpMock implements HttpCalloutMock{
    
    global HttpResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type','application/json');
        String strResponse = '{"timeStamp": "2019-07-10T10:35:56.1453363+04:00", "responseCode": 200,"errorMessage": "OWNER_CONTACT_DATA_UPLOADED_PARTIALLY","validationErrorsList": null,"response": {'
        + '"fileURL": null,"contacts": [{"mollakPropertyId": 1005,"name": "owner 1","nameAr":" اوزون","email": "email@domain.com","emirateId": "784124545614",'
        + '"passportNumber": "AA2651942","address": "Dubai ", "status": " Owner Mobile number is null","statudCode": 3024}]}';

        res.setBody(strResponse );
        res.setStatusCode(200);
        return res;
    }   
}