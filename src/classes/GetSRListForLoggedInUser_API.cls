/**********************************************************************************************************************
Description: This API is used for getting the list of SRs applicable for given accountId
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   05-11-2020      | Shubham Suryawanshi | Added text changes for SR names
1.1     |   19-11-2020      | Shubham Suryawanshi | Replaced 'Pending' text to - 'Submitted' in sr_approval_status param
1.2     |   11-11-2020      | Shubham Suryawanshi | Added comparable method to cls_data for sorting as per sr_created_dates
1.3     |   18-11-2020      | Shubham Suryawanshi | Added Noc for Fit Out/Alteration SR Option
1.4     |   21-01-2021      | Anand Venkitakrishnan | Added Work Permit SR Option
***********************************************************************************************************************/

@RestResource(urlMapping='/getMoveInSR/*')
global class GetSRListForLoggedInUser_API {

    public static final String NO_FMCASES = 'No FmCases found for given accountId';
    public static final String SUCCESS = 'successful';
    public static String responseMessage;

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        //200 => 'OK',
        //400 => 'Bad Request',
        //401 => 'Unauthorized',
        //500 => 'Internal Server Error' 
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    @HttpPost
    global static FinalReturnWrapper getSRList() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        List<cls_data> objData = new List<cls_data>();
        cls_meta_data objMeta = new cls_meta_data();
        //List<MoveInFMCaseWrapper> lstReturnMoveIn = new List<MoveInFMCaseWrapper>();
        //List<MoveInFMCaseWrapper_SRDisplay> srListDisplay = new List<MoveInFMCaseWrapper_SRDisplay>();
        List<cls_data> srListDisplay = new List<cls_data>();

        //if(!r.params.containsKey('recordType')) {
        //    objMeta.message = 'Missing parameter : recordType';
        //    objMeta.status_code = 3;
        //    objMeta.title = mapStatusCode.get(3);
        //    objMeta.developer_message = null;

        //    returnResponse.meta_data = objMeta;
        //    return returnResponse;
        //}
        if(!r.params.containsKey('accountId')){
            objMeta.message = 'Missing parameter : accountId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;

        }
        //else if(r.params.containsKey('recordType') && String.isBlank(r.params.get('recordType'))) {
        //    objMeta.message = 'Missing parameter value: recordType';
        //    objMeta.status_code = 3;
        //    objMeta.title = mapStatusCode.get(3);
        //    objMeta.developer_message = null;

        //    returnResponse.meta_data = objMeta;
        //    return returnResponse;
        //}
        else if(r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId'))) {
            objMeta.message = 'Missing parameter value: accountId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }   
        //else if(r.params.containsKey('recordType') && !r.params.get('recordType').containsIgnoreCase('Move In')) {
        //    objMeta.message = 'Invalid parameter value in: recordType';
        //    objMeta.status_code = 3;
        //    objMeta.title = mapStatusCode.get(3);
        //    objMeta.developer_message = null;

        //    returnResponse.meta_data = objMeta;
        //    return returnResponse;
        //}

        if(String.isNotBlank(r.params.get('accountId')) ) {

            System.debug('accountId:: ' + r.params.get('accountId'));
            String accountId = r.params.get('accountId');
            //List<Account> acc = [SELECT id, Name, Party_Id__c FROM Account WHERE Party_Id__c =:r.params.get('partyId')];
            //System.debug('acc:: ' + acc);

            //Query on user to get the Profile 
            List<User> usr = [SELECT id
                                   , Name 
                                   , Username
                                   , Profile.Name 
                                   , Contact.AccountId 
                                   , Contact.Account.Party_Id__c
                             FROM User
                             WHERE Contact.AccountId =: accountId];
            System.debug('usr:: ' + usr);

            if(usr == null || usr.isEmpty()) {
                objMeta = ReturnMetaResponse('No account present for given accountId', 3);
                returnResponse.meta_data = objMeta;
                return returnResponse;
            }

            Boolean isTenant = usr[0].profile.Name.contains('Tenant Community') ? true : false;
            System.debug('isTenant: ' + isTenant);

            if(usr.size() > 0) {

                //List<cls_data> lstCollectSRDetails = new List<cls_data>();

                if(!isTenant) {
                    List<FM_Case__c> lstMoveInCase = getSubmittedFMCases('Move In', usr[0].Contact.AccountId, isTenant);
                    if(lstMoveInCase.size() > 0) {
                    List<cls_data> moveInWrap = CreateMIReturnWrapperSRDisplay(lstMoveInCase);
                    srListDisplay.addAll(moveInWrap);
                    }
                }
                

                List<FM_Case__c> lstMoveOutCase = getSubmittedFMCases('Move Out', usr[0].Contact.AccountId, isTenant);
                if(lstMoveOutCase.size() > 0) {
                     List<cls_data> moveOutWrap = CreateMIReturnWrapperSRDisplay(lstMoveOutCase);
                     srListDisplay.addAll(moveOutWrap);
                }

                if(!isTenant) {
                    List<Case> lstCOCD = getCOCDCaseForOwner(usr[0].Contact.AccountId, 'Change of Contact Details');
                    if(lstCOCD.size() > 0) {
                        List<cls_data> cocdOwnerWrap = CreateCOCDReturnWrapperSRDisplay(lstCOCD, 'Change of Contact Details');
                        srListDisplay.addAll(cocdOwnerWrap);
                    }

                    List<Case> lstOwnerPass= getCOCDCaseForOwner(usr[0].Contact.AccountId, 'Passport Detail Update SR');
                    if(lstOwnerPass.size() > 0) {
                        List<cls_data> passportOwnerWrap = CreateCOCDReturnWrapperSRDisplay(lstOwnerPass, 'Passport Detail Update');
                        srListDisplay.addAll(passportOwnerWrap);
                    }
                }

                if(isTenant) {
                    List<FM_Case__c> lstPassport = getPassportCaseForTenant(usr[0].Contact.AccountId);
                    if(lstPassport.size() > 0) {
                        List<cls_data> passportTenantWrap  = CreatePassportReturnWrapperSRDisplay(lstPassport);
                        srListDisplay.addAll(passportTenantWrap);
                    }
                }

                //Following modules not to be uploaded on 5th Nov prod deployment
                ////SRType - Access Card
                List<FM_Case__c> lstAccessCardSR = getSubmittedFMCases('Request For Access Card', usr[0].Contact.AccountId, isTenant);
                System.debug('lstAccessCardSR:: ' + lstAccessCardSR);
                if(lstAccessCardSR.size() > 0) {
                     List<cls_data> accessCardWrap = CreateMIReturnWrapperSRDisplay(lstAccessCardSR);
                     srListDisplay.addAll(accessCardWrap);
                }
                ////SRType - Access Card END
                
                List<FM_Case__c> lstTenantRenewalCase = getSubmittedFMCases('Tenant Renewal', usr[0].Contact.AccountId, isTenant);
                if(lstTenantRenewalCase.size() > 0) {
                     List<cls_data> tenantRenewalWrap = CreateMIReturnWrapperSRDisplay(lstTenantRenewalCase);
                     srListDisplay.addAll(tenantRenewalWrap);
                }

                //For Fitout/Alterations SR
                List<FM_Case__c> lstFitOutCases = getSubmittedFMCases('NOC For FitOut', usr[0].Contact.AccountId, isTenant);
                if(lstFitOutCases.size() > 0) {
                     List<cls_data> fitOutWrap = CreateMIReturnWrapperSRDisplay(lstFitOutCases);
                     srListDisplay.addAll(fitOutWrap);
                }

                //Work Permit SR
                List<FM_Case__c> lstWorkPermitCase = getSubmittedFMCases('Work Permit', usr[0].Contact.AccountId, isTenant);
                if(lstWorkPermitCase.size() > 0) {
                     List<cls_data> workPermitWrap = CreateMIReturnWrapperSRDisplay(lstWorkPermitCase);
                     srListDisplay.addAll(workPermitWrap);
                }
                
                objMeta.message = 'successful';
                objMeta.status_code = 1;

            }
            else {
                objMeta.message = 'No account found for the given partyId';
                objMeta.status_code = 3;
            }
        }

        //objData.sr_list = lstReturnMoveIn;
        objData = srListDisplay;
        objMeta.title = mapStatusCode.get(objMeta.status_code);
        objMeta.developer_message = null;

        objData.sort();
        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;

        System.debug('returnResponse:: ' + returnResponse);

        return returnResponse;

    }

    public static List<FM_Case__c> getSubmittedFMCases(String recordType, String accountId, Boolean isTenant) {

        List<FM_Case__c> lstFmCase = new List<FM_Case__c>();

        String strQuery = 'SELECT id , Name , Status__c , Request_Type__c , Booking_Unit__c , Booking_Unit__r.Unit_Name__c'+
        ' ,Booking_Unit__r.Inventory__r.Building_Location__r.Property_Name__r.Name , CRE_Comments__c , Approval_Status__c , Submission_Date__c , CreatedDate , First_Name__c , Last_Name__c'+
        ' , Gender__c , Date_of_Birth__c , Nationality__c , Passport_Number__c , Expiry_Date__c , Emirates_Id__c , Mobile_Country_Code__c , Mobile_no__c , Tenant_Email__c , No_of_Adults__c'+ ', No_of_Children__c , Start_Date__c , End_Date__c'+
        ' , Ejari_Number__c , Move_in_date__c , Move_in_Type__c , Company__c , Contractor__c , Mobile_no_contractor__c , isHavingPets__c , isPersonWithSpecialNeeds__c, Type_of_Access_Card__c, New_Access_Card_Fee__c, Access_Card_Replacement_Fee__c, Access_Card_Permission__c, Noc_for_Fitout_Alterations_Fee__c'; 

        strQuery += ' FROM FM_Case__c WHERE Origin__c = \'Portal\'';
        if(isTenant && (recordType == 'Move Out' || recordType == 'Request For Access Card' || recordType == 'Tenant Renewal')) {
            strQuery += ' AND Tenant__c =:accountId';
        }
        else {
            strQuery += ' AND Account__c =:accountId';  
        }

        strQuery += ' AND Status__c != null AND Status__c != \'Draft Request\' AND Status__c != \'In Progress\' AND recordType.Name =: recordType AND (CreatedDate = Last_N_Months:6 OR CreatedDate = THIS_MONTH) ORDER BY CreatedDate DESC';

        System.debug('strQuery:: ' + strQuery);

        lstFmCase = Database.query(strQuery);


        //List<FM_Case__c>   lstFmCase = [SELECT id 
        //                                   , Name
        //                                   , Status__c
        //                                   , Request_Type__c
        //                                   , Booking_Unit__c
        //                                   , Booking_Unit__r.Unit_Name__c
        //                                   , Booking_Unit__r.Inventory__r.Building_Location__r.Property_Name__r.Name
        //                                   , CRE_Comments__c
        //                                   , Approval_Status__c
        //                                   , Submission_Date__c
        //                                   , CreatedDate
        //                                   , First_Name__c
        //                                   , Last_Name__c
        //                                   , Gender__c
        //                                   , Date_of_Birth__c
        //                                   , Nationality__c
        //                                   , Passport_Number__c
        //                                   , Expiry_Date__c
        //                                   , Emirates_Id__c
        //                                   , Mobile_Country_Code__c
        //                                   , Mobile_no__c
        //                                   , Tenant_Email__c
        //                                   , No_of_Adults__c
        //                                   , No_of_Children__c
        //                                   , Start_Date__c
        //                                   , End_Date__c
        //                                   , Ejari_Number__c
        //                                   , Move_in_date__c
        //                                   , Move_in_Type__c
        //                                   , Company__c
        //                                   , Contractor__c
        //                                   , Mobile_no_contractor__c
        //                                   , isHavingPets__c
        //                                   , isPersonWithSpecialNeeds__c
        //                             FROM FM_Case__c 
        //                             WHERE Origin__c = 'Portal'
        //                             AND Account__c =:accountId
        //                             AND Status__c != 'Draft Request'
        //                             AND recordType.Name =: recordType
        //                             AND CreatedDate = Last_N_Months:6 
        //                             ORDER BY CreatedDate DESC ];

        //System.debug('lstFmCase:: ' + lstFmCase);
        return lstFmCase;
    }

    public static List<Case> getCOCDCaseForOwner(String accountId, String srType) {

        List<Case> lstCOCDCase = [SELECT id 
                                       , CaseNumber
                                       , Status
                                       , SR_Type__c
                                       , Origin
                                       , isHelloDamacAppCase__c
                                       , Recordtype.Name
                                       , Approval_Status__c
                                       , CreatedDate
                                       , CRE_Comments__c
                                  FROM Case
                                  WHERE AccountId =: accountId
                                  AND SR_Type__c =: srType
                                  AND Origin = 'Portal'
                                  AND Status != 'Draft Request'
                                  AND (CreatedDate = Last_N_Months:6 
                                       OR CreatedDate = THIS_MONTH)
                                  ORDER BY CreatedDate DESC
                                  ];

        return lstCOCDCase;
    }

    public static List<FM_Case__c> getPassportCaseForTenant(String accountId) {

        List<FM_Case__c> lstPassportCase = [SELECT id 
                                                 , Name
                                                 , Status__c
                                                 , Request_Type__c
                                                 , Origin__c
                                                 , Approval_Status__c
                                                 , isHelloDamacAppCase__c
                                                 , Recordtype.Name
                                                 , CreatedDate
                                                 , CRE_Comments__c
                                            FROM FM_Case__c
                                            WHERE Account__c =: accountId
                                            AND Request_Type__c = 'Passport Detail Update'
                                            AND Origin__c = 'Portal'
                                            AND Status__c != 'Draft Request'
                                            AND (CreatedDate = Last_N_Months:6 
                                                 OR CreatedDate = THIS_MONTH)
                                            ORDER BY CreatedDate DESC
                                            ];

        return lstPassportCase;
    }

    public static List<cls_data> CreateMIReturnWrapperSRDisplay(List<FM_Case__c> lstFmCase) {

        List<cls_data> lstcls_move_in_sr_display = new List<cls_data>();

        for(FM_Case__c objFM : lstFmCase) {

            cls_data objSRInfo = new cls_data();
            objSRInfo.sr_id = objFM.id;
            objSRInfo.sr_number = objFM.Name;
            objSRInfo.category_id = 2;  //2 - for Residency category SRs 
            //objSRInfo.sr_status = objFM.Status__c;
            if(objFM.Request_Type__c == 'Move In') {
                objSRInfo.sr_type = 'Move in';
                objSRInfo.sr_type_id = 3;
            }
            if(objFM.Request_Type__c == 'Move Out') {
                objSRInfo.sr_type = 'Move out';
                objSRInfo.sr_type_id = 4;
            }
            if(objFM.Request_Type__c == 'Request for Access Card') {
                objSRInfo.sr_type = 'Access card';
                objSRInfo.sr_type_id = 7;
                if(String.isNotBlank(objFM.Type_of_Access_Card__c) && objFM.Type_of_Access_Card__c == 'New Request') {
                    objSRInfo.amount = objFM.New_Access_Card_Fee__c != null ? objFM.New_Access_Card_Fee__c : null;
                }
                else if(String.isNotBlank(objFM.Type_of_Access_Card__c) && objFM.Type_of_Access_Card__c == 'Replacement') {
                    objSRInfo.amount = objFM.Access_Card_Replacement_Fee__c != null ? objFM.Access_Card_Replacement_Fee__c : null;
                }

                objSRInfo.access_card_type = objFM.Access_Card_Permission__c != null ? objFM.Access_Card_Permission__c.replaceAll(';',', ') : null;
            }
            if(objFM.Request_Type__c == 'Tenant Renewal') {
                objSRInfo.sr_type = 'Tenancy renewal';
                objSRInfo.sr_type_id = 6;
            }
            if(objFM.Request_Type__c == 'NOC for Fit out/Alterations') {
                objSRInfo.sr_type = 'NOC for Fit Out/Alterations';
                objSRInfo.sr_type_id = 8;
                objSRInfo.amount = objFM.Noc_for_Fitout_Alterations_Fee__c;
                objSRInfo.category_id = 3;  //3 - for Modifications category SRs 

            }
            if(objFM.Request_Type__c == 'Work Permit') {
                objSRInfo.sr_type = 'Work Permit';
                objSRInfo.sr_type_id = 8;
                objSRInfo.category_id = 3;
            }
            
            objSRInfo.sr_approval_status = objFM.Status__c == 'Closed' ? 'Approved' : objFM.Status__c;  /*Changed as per Discussed with Charith*/
            //objSRInfo.sr_approval_status = objFM.Approval_Status__c == null || objFM.Approval_Status__c.containsIgnoreCase('Pending') ? 'Submitted' : objFM.Approval_Status__c;/*As per bug 2851*/
            objSRInfo.sr_creation_date = objFM.CreatedDate != null ? objFM.CreatedDate.format('yyyy-MM-dd') : '';
            objSRInfo.project_name = objFM.Booking_Unit__r.Inventory__r.Building_Location__r.Property_Name__r.Name;
            objSRInfo.unit_name = objFM.Booking_Unit__r.Unit_Name__c;
            objSRInfo.remark = String.isNotBlank(objFM.CRE_Comments__c) ? objFM.CRE_Comments__c : 'Your request is '+objFM.Status__c.tolowerCase();

            //moveInCaseWrap.sr_info = objSRInfo;
            lstcls_move_in_sr_display.add(objSRInfo);
        }
        return lstcls_move_in_sr_display;
    }

    //This method will be used for both Change of Contact Details & Passport details update - Owner
    public static List<cls_data> CreateCOCDReturnWrapperSRDisplay(List<Case>lstCOCDCase, String srType) {

        List<cls_data> lstcls_cocd_sr_display = new List<cls_data>();
        for(Case objCase : lstCOCDCase) {

            cls_data objSRInfo = new cls_data();
            objSRInfo.sr_id = objCase.id;
            objSRInfo.sr_number = objCase.CaseNumber;
            objSRInfo.category_id = 1;  //1 - for Personal category SRs 
            //objSRInfo.sr_status = objCase.Status__c;
            //objSRInfo.sr_type = srType;
            if(srType == 'Change of Contact Details') {
                objSRInfo.sr_type = 'Update primary contact details';
                objSRInfo.sr_type_id = 1;
            }
            if(srType == 'Passport Detail Update') {
                objSRInfo.sr_type = 'Update passport details';
                objSRInfo.sr_type_id = 2;
            }

            if( objCase.status == 'Rejected' || objCase.status == 'Cancelled') {
                objSRInfo.sr_approval_status = 'Rejected';
            }
            else if(objCase.status == 'Working' || objCase.status == 'Waiting') {
                objSRInfo.sr_approval_status = 'Submitted';/*As per bug 2851*/
            }
            else if(objCase.status == 'Approved' || objCase.status == 'Closed') {
                objSRInfo.sr_approval_status = 'Approved';
            }
            else {
                objSRInfo.sr_approval_status = 'Submitted';/*As per bug 2851*/
            }
            
            objSRInfo.sr_creation_date = objCase.CreatedDate != null ? objCase.CreatedDate.format('yyyy-MM-dd') : '';
            //objSRInfo.project_name = objCase.Booking_Unit__r.Inventory__r.Building_Location__r.Property_Name__r.Name;
            //objSRInfo.unit_name = objCase.Booking_Unit__r.Unit_Name__c;
            objSRInfo.remark = String.isNotBlank(objCase.CRE_Comments__c) ? objCase.CRE_Comments__c : 'Your request is '+objCase.status.tolowerCase();

            //moveInCaseWrap.sr_info = objSRInfo;
            lstcls_cocd_sr_display.add(objSRInfo);
        }

        return lstcls_cocd_sr_display;
    }


    //only for tenant
    public static List<cls_data> CreatePassportReturnWrapperSRDisplay(List<FM_Case__c>lstPassportCase) {

      List<cls_data> lstcls_passport_sr_display = new List<cls_data>();
      for(FM_Case__c objFMCase : lstPassportCase) {

            cls_data objSRInfo = new cls_data();
            objSRInfo.sr_id = objFMCase.id;
            objSRInfo.sr_number = objFMCase.Name;
            objSRInfo.category_id = 1;  //1 - for Personal category SRs 
            //objSRInfo.sr_status = objFMCase.Status__c;
            objSRInfo.sr_type = 'Update passport details';
            objSRInfo.sr_type_id = 2;

            if(objFMCase.Status__c == 'Closed') {
                objSRInfo.sr_approval_status = 'Approved';
            }
            else if(objFMCase.Status__c == 'Rejected' || objFMCase.Status__c == 'Cancelled') {
                objSRInfo.sr_approval_status = 'Rejected';
            }
            else {
                objSRInfo.sr_approval_status = 'Submitted';/*As per bug 2851*/
            }
                  
            objSRInfo.sr_creation_date = objFMCase.CreatedDate != null ? objFMCase.CreatedDate.format('yyyy-MM-dd') : '';
            //objSRInfo.project_name = objFMCase.Booking_Unit__r.Inventory__r.Building_Location__r.Property_Name__r.Name;
            //objSRInfo.unit_name = objFMCase.Booking_Unit__r.Unit_Name__c;
            objSRInfo.remark = String.isNotBlank(objFMCase.CRE_Comments__c) ? objFMCase.CRE_Comments__c : 'Your request is '+objFMCase.Status__c.tolowerCase();
            //moveInCaseWrap.sr_info = objSRInfo;
            lstcls_passport_sr_display.add(objSRInfo);
      }

      return lstcls_passport_sr_display;
    }




    @HttpGet 
    global static FinalReturnWrapperMICase getMoveInCaseFromCaseId() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        FinalReturnWrapperMICase returnResponse = new FinalReturnWrapperMICase();
        cls_data_single_case objData_single = new cls_data_single_case();
        cls_meta_data objMeta = new cls_meta_data();
        MoveInFMCaseWrapper returnMoveInCase = new MoveInFMCaseWrapper();
        List<MoveInFMCaseWrapper> lstReturnMoveIn = new List<MoveInFMCaseWrapper>();

        if(!r.params.containsKey('recordType')) {
            objMeta.message = 'Missing parameter : recordType';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(!r.params.containsKey('fmCaseId')){
            objMeta.message = 'Missing parameter : fmCaseId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;

        }
        else if(r.params.containsKey('recordType') && String.isBlank(r.params.get('recordType'))) {
            objMeta.message = 'Missing parameter value: recordType';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('fmCaseId') && String.isBlank(r.params.get('fmCaseId'))) {
            objMeta.message = 'Missing parameter value: fmCaseId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }   
        else if(r.params.containsKey('recordType') && !r.params.get('recordType').containsIgnoreCase('Move In')) {
            objMeta.message = 'Invalid parameter value in: recordType';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        if(String.isNotBlank(r.params.get('recordType')) && String.isNotBlank(r.params.get('fmCaseId')) ) {

            System.debug('recordType:: ' + r.params.get('recordType'));
            System.debug('fmCaseId:: ' + r.params.get('fmCaseId'));

            List<FM_Case__c> fetchedFMCase = GetTenantRegistrationCase_API.getFMCase(r.params.get('fmCaseId'));

            if(fetchedFMCase.size() > 0) {
                List<HDAppInsertMoveInCase.FmCaseReturnWrapper> moveInCaseLstWrap = HDAppInsertMoveInCase.CreateMIReturnWrapper(fetchedFMCase);
                lstReturnMoveIn = (List<MoveInFMCaseWrapper>)JSON.deserialize(JSON.serialize(moveInCaseLstWrap), List<MoveInFMCaseWrapper>.class);
                returnMoveInCase = lstReturnMoveIn[0];

                objMeta.message = 'successful';
                objMeta.status_code = 1;
            }
            else {
                objMeta.message = 'No Fm case found for the given fmCaseId';
                objMeta.status_code = 3;
            }

        } 

        objData_single.fm_case = returnMoveInCase;
        objMeta.title = mapStatusCode.get(objMeta.status_code);
        objMeta.developer_message = null;

        returnResponse.data = objData_single;
        returnResponse.meta_data = objMeta;

        System.debug('returnResponse:: ' + returnResponse);

        return returnResponse;

    } 

    public static cls_meta_data ReturnMetaResponse(String message, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data();
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = null;
        return retMeta;
    }


    global class FinalReturnWrapper {
        public cls_data[] data;
        public cls_meta_data meta_data;
    }

    global class FinalReturnWrapperMICase {
        public cls_data_single_case data;
        public cls_meta_data meta_data;
    } 

    public class cls_data implements Comparable {

        //To remove sr_list attribute
        public String sr_id;
        public String sr_number;
        public Integer category_id; //Newly added on 21-10-2020
        //public String sr_status;
        public String sr_type;
        public Integer sr_type_id;
        public Decimal amount;
        public String access_card_type;
        public String sr_approval_status;
        public String sr_creation_date;
        public String project_name;
        public String unit_name;
        public String remark;
        //To remove sr_list attribute

        //public List<MoveInFMCaseWrapper> fm_cases ;
        //public List<cls_data> sr_list ;
    

        public Integer compareTo(Object compareTo) {
            cls_data compareInstance = (cls_data)compareTo;
               
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if (Date.valueOf(sr_creation_date) > Date.ValueOf(compareInstance.sr_creation_date)) {
                // Set return value to a positive value.
                returnValue = -1;
            } else if (Date.valueOf(sr_creation_date) < Date.ValueOf(compareInstance.sr_creation_date)) {
                // Set return value to a negative value.
                returnValue = 1;
            }
               
            return returnValue;       
        }
    }

    public class cls_data_single_case {
        public MoveInFMCaseWrapper fm_case;
    }

    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }

    public class MoveInFMCaseWrapper_SRDisplay {

    public cls_data sr_info;
        //public String id; //new
        ////public String fm_case_status;   //new
        ////public String fm_case_number;
        //public String booking_unit_id;

        //public String move_in_date;
        //public String move_in_type;
        //public String moving_company_name;
        //public String moving_contractor_name;
        //public String moving_contractor_phone;

        //public String no_of_adults;
        //public String no_of_children;
        //public Boolean is_person_with_spl_needs;
        //public Boolean is_having_pets;

        //public cls_emergency_contact[] emergency_contact_details;
  //  public cls_vehicle_details[] vehicle_details;
        //public cls_additional_members[] additional_members;
        //public cls_attachment[] attachments;
        
    }

    //public class cls_data {

    //    public String sr_id;
    //    public String sr_number;
    //    //public String sr_status;
    //    public String sr_type;
    //    public String sr_approval_status;
    //    public String sr_creation_date;
    //    public String project_name;
    //    public String unit_name;
    //    public String remark;
    //}

    public class MoveInFMCaseWrapper {

        public String id;   //new
        public String fm_case_status;   //new
        public String fm_case_number;
        public String booking_unit_id;

        public String move_in_date;
        public String move_in_type;
        public String moving_company_name;
        public String moving_contractor_name;
        public String moving_contractor_phone;

        public String no_of_adults;
        public String no_of_children;
        public Boolean is_person_with_spl_needs;
        public Boolean is_having_pets;

        public cls_emergency_contact[] emergency_contact_details;
    public cls_vehicle_details[] vehicle_details;
        public cls_additional_members[] additional_members;
        public cls_attachment[] attachments;
        
    }

    public class cls_attachment {
    public String id;
    public String name;
    public String attachment_url;
    public String file_extension;
    }


    public class cls_additional_members {
        public String id;   //new
        public String member_first_name;
        public String member_last_name;
        public String member_gender;
        public String member_date_of_birth;
        public String member_nationality;
        public String member_passport_number;
        public String member_emirates_id;
        public String member_mobile_number;
        public String member_email_address;
    }

    public class cls_emergency_contact {

        public String id;
        public String emergency_full_name;
        public String emergency_relationship;
        public String emergency_email_address;
        public String emergency_mobile_number;

    }

    public class cls_vehicle_details {

        public String id;
        public String vehicle_number;
        public String vehicle_make;
        public String vehicle_access_card_number;
        public String vehicle_parking_slot_number;
    }

}