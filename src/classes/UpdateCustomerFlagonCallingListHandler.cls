public  class UpdateCustomerFlagonCallingListHandler {
    public static Map<Id,List<Calling_List__c>>accountIdToLstofPrimaryCallingMap;
    public static List<Calling_List__c>callingLst;
    public static Set<Calling_List__c>updateSubOrdinateCallingLst;
    public static Map<String,String>apiTocustomFieldsSettingMap;
    public static List<CallingWrapper>CallingWrapperLst;
    /*This method is called from UpdateCustomerFlagonCallingListBatch batch 
    to set customer flag true  on calling list */
    
    public static Id fmCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
    public static Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
    public static Id hoCallingList = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Handover Calling List').RecordTypeId;
    public static Id ehoCallingList = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Early Handover Calling List').RecordTypeId;
    public static Id eliteCallingList = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Elite Calling List').RecordTypeId;
    
    private static final Id INTEGRATION_CUSTOM_ADMIN_PROFILE_ID =  [select Name,ProfileId  from User  where Profile.Name = 'Integration (Custom Admin)' AND Name = 'Damac Integration' LIMIT 1].ProfileId;
    private static final Id CRM_SUPPORT_PROFILE_ID =  [select ProfileId  from User  where Profile.Name =: Label.CRM_Support_Profile_Name LIMIT 1].ProfileId;
	private static final Id CRM_COLLECTION_PROFILE_ID =  [select ProfileId  from User  where Profile.Name = 'Collection - CRE' LIMIT 1].ProfileId;
    
	public static String strRun_Subordinate_Logic = Label.Run_Subordinate_Logic;
    
    public static void updateFlagonCallingList( List<Calling_List__c> callingLst){
      CallingWrapperLst = new List<CallingWrapper>();
      List<Calling_List__c>updateCallingLst = new List<Calling_List__c>();
      Map<Id,List<Calling_List__c>>accIdToLstOfCallingMap = new Map<Id,List<Calling_List__c>>();
          if(callingLst != null && !callingLst.isEmpty()){
              for(Calling_List__c callInst : callingLst){
          String ownerId = callInst.ownerId != null ? String.valueOf( callInst.ownerId ) : '';
          if( String.isNotBlank(ownerId) && ownerId.startsWith('005') ) {          
            if(!accIdToLstOfCallingMap.containsKey(callInst.Account__c)){
              accIdToLstOfCallingMap.put(callInst.Account__c,new List<Calling_List__c>{callInst});
            }
            else{
              accIdToLstOfCallingMap.get(callInst.Account__c).add(callInst);
            }
          }
              }
          }
          if(accIdToLstOfCallingMap != null && !accIdToLstOfCallingMap.isEmpty()){
                for(Id accId : accIdToLstOfCallingMap.keySet()){
                    Map<Id,List<Calling_List__c>>recordTypeIdToCallingListMap = new Map<Id,List<Calling_List__c>>();
                    List<Calling_List__c> callingLstOfSpecificAccount = accIdToLstOfCallingMap.get(accId);
                    for(Calling_List__c callObj : callingLstOfSpecificAccount){
            String ownerId = callObj.ownerId != null ? String.valueOf( callObj.ownerId ) : '';
            if( String.isNotBlank(ownerId) && ownerId.startsWith('005') ) {  
                        if(!recordTypeIdToCallingListMap.containsKey(callObj.RecordTypeId)){
                            recordTypeIdToCallingListMap.put(callObj.RecordTypeId,new List<Calling_List__c>{callObj});
                        }
                        else{
                            recordTypeIdToCallingListMap.get(callObj.RecordTypeId).add(callObj);
                        }   
            }
                    }
                    CallingWrapperLst.add(new CallingWrapper(accId,recordTypeIdToCallingListMap) );
                }
          }
          if(CallingWrapperLst != null && !CallingWrapperLst.isEmpty()){
            for(CallingWrapper wrapperObj : CallingWrapperLst){
                  if(wrapperObj.RecordTypeIdToCallingListMap != null && !wrapperObj.RecordTypeIdToCallingListMap.isEmpty()){
                    for(Id recordTypeIdInsnt : wrapperObj.RecordTypeIdToCallingListMap.keySet()){
                        List<Calling_List__c>allCallingListofSpecificRecordType = new List<Calling_List__c>();
                        allCallingListofSpecificRecordType =  wrapperObj.RecordTypeIdToCallingListMap.get(recordTypeIdInsnt);
                        List<Calling_List__c>trueFlagCallingLst = new List<Calling_List__c>();
                        List<Calling_List__c>falseFlagCallingLst = new List<Calling_List__c>();
                        for(Calling_List__c callInstToUpdate : allCallingListofSpecificRecordType){
              String ownerId = callInstToUpdate.ownerId != null ? String.valueOf( callInstToUpdate.ownerId ) : '';
              if( String.isNotBlank(ownerId) && ownerId.startsWith('005') ) {                
                if( callInstToUpdate.Customer_Flag__c != true){
                   falseFlagCallingLst.add(callInstToUpdate);
                 }
                 else if(callInstToUpdate.Customer_Flag__c == true){
                  trueFlagCallingLst.add(callInstToUpdate);
                 }
              }
                        }
                        if(trueFlagCallingLst.size() == 0){
                            if(falseFlagCallingLst[0].Customer_Flag__c != true){
                                falseFlagCallingLst[0].Customer_Flag__c = true;
                                updateCallingLst.add(falseFlagCallingLst[0]);
                            }
                        }
                    }
                 }
            }
        }
        /*update the primary calling list */
      if(updateCallingLst != null && !updateCallingLst.isEmpty()){
        update updateCallingLst;
      }
       
    }
    
    
    
    
    
    
    
    /*This method is called from UpdateCallingListHandler class.
    this method is used to update subordinate calling list from primary calling list fields
    Parameter:::It requires list of calling list which has customer flag true*/
    public static void updateMetadataOnSubordinateCallings(List<Calling_List__c> primaryUpdatedCallingList){
		if( strRun_Subordinate_Logic.contains('Y') ) {
			If( Test.isRunningTest() || (UserInfo.getProfileId() == CRM_COLLECTION_PROFILE_ID)) {
				System.debug('@@@@@primaryUpdatedCallingList@@'+primaryUpdatedCallingList);
				accountIdToLstofPrimaryCallingMap = new Map<Id,List<Calling_List__c>>();
				
				 /*now will fetch custom setting records*/
				apiTocustomFieldsSettingMap = new Map<String,String>();
				List<Primary_To_child_CallingList_Setting__mdt>metadataSettingLst = [SELECT DeveloperName,
																							Custom_Field__c
																						FROM Primary_To_child_CallingList_Setting__mdt];
				for(Primary_To_child_CallingList_Setting__mdt settingInst : metadataSettingLst){
					apiTocustomFieldsSettingMap.put(settingInst.DeveloperName,settingInst.Custom_Field__c);
				}
				
				/*fill account id to primary map*/
				if(primaryUpdatedCallingList != null && !primaryUpdatedCallingList.isEmpty()){
					Id Collection_QueueId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName( 'Collection_Queue','Queue' ); 
					for(Calling_List__c primaryCallInst : primaryUpdatedCallingList){
						if( primaryCallInst.RecordTypeId == collectioncallingRecordTypeId  || primaryCallInst.RecordTypeId == fmCallingRecordTypeId 
							|| primaryCallInst.RecordTypeId == hoCallingList || primaryCallInst.RecordTypeId == ehoCallingList 
							|| primaryCallInst.RecordTypeId == eliteCallingList 
							) {
							if ( primaryCallInst.OwnerId != Collection_QueueId ) {
								if(!accountIdToLstofPrimaryCallingMap.containsKey(primaryCallInst.Account__c)){
									accountIdToLstofPrimaryCallingMap.put(primaryCallInst.Account__c,new List<Calling_List__c>{primaryCallInst});
								}
								else{
									accountIdToLstofPrimaryCallingMap.get(primaryCallInst.Account__c).add(primaryCallInst);
								}
							}else {
								primaryCallInst.addError('You can not update unit assigned to queue.');
							}
						}
					}
				}
				/*now fetch all calling lists related to that account (primary + sub ordinates)*/
				List<Account>AccountToAllCallingsLst = new List<Account>();
				if(accountIdToLstofPrimaryCallingMap != null && !accountIdToLstofPrimaryCallingMap.isEmpty()){
					Set<Id>accIdSet = new Set<Id>();
					accIdSet.addAll(accountIdToLstofPrimaryCallingMap.keySet());
					String dynamicQueryStr = 'SELECT Id,(SELECT Id,Name,Account__c,RecordTypeId ';
					for(String devNameItr: apiTocustomFieldsSettingMap.keySet()){
						dynamicQueryStr += ','+ apiTocustomFieldsSettingMap.get(devNameItr);
					}
				   // dynamicQueryStr += ', Customer_Flag__c FROM Calling_List__r WHERE RecordTypeId !='+walkInCallingRecordTypeId+' ) FROM Account WHERE Id IN: accIdSet';
					dynamicQueryStr += ', Customer_Flag__c FROM Calling_List__r Where IsHideFromUI__c = false AND ( RecordTypeId =: collectioncallingRecordTypeId OR RecordTypeId =: fmCallingRecordTypeId OR RecordTypeId =: hoCallingList OR RecordTypeId =: ehoCallingList OR RecordTypeId =: eliteCallingList) ) FROM Account WHERE Id IN: accIdSet';
					AccountToAllCallingsLst = Database.Query(dynamicQueryStr);
				}
				/*now create map of Account id + Record type id as a key and primary calling as a value
				and map of account id + record type id as a key and list of subordinates as a value*/
				Map<String,calling_List__c>accAndRecIdToPrimaryCallingMap = new Map<String,calling_List__c>();
				Map<String,List<calling_List__c>>accAndRecToSubOrdinateCallingMap = new Map<String,List<calling_List__c>>();
				
				if(AccountToAllCallingsLst != null && !AccountToAllCallingsLst.isEmpty()){
					for(Account accInst : AccountToAllCallingsLst){
						List<calling_List__c>allCallingLstOfSpecificAcc = accInst.Calling_List__r;
							if(allCallingLstOfSpecificAcc != null && !allCallingLstOfSpecificAcc.isEmpty()){
								for(calling_List__c callInst : allCallingLstOfSpecificAcc){
									if( callInst.RecordTypeId == collectioncallingRecordTypeId  || callInst.RecordTypeId == fmCallingRecordTypeId 
									|| callInst.RecordTypeId == hoCallingList || callInst.RecordTypeId == ehoCallingList  
									|| callInst.RecordTypeId == eliteCallingList ) {
										if(callInst.Customer_Flag__c == true){
											if(!accAndRecIdToPrimaryCallingMap.containsKey((String)accInst.Id+callInst.RecordTypeId)){
												accAndRecIdToPrimaryCallingMap.put((String)accInst.Id + callInst.RecordTypeId,callInst);
											}
										}
										else if(callInst.Customer_Flag__c == false){
											if(!accAndRecToSubOrdinateCallingMap.containsKey((String)accInst.Id+callInst.RecordTypeId)){
												accAndRecToSubOrdinateCallingMap.put((String)accInst.Id+callInst.RecordTypeId,new List<calling_List__c>{callInst});
											}
											else{
												accAndRecToSubOrdinateCallingMap.get((String)accInst.Id+callInst.RecordTypeId).add(callInst);
											}
										}
									}
								}
							}
						}
					}
					
					/*iterate on primary wala map and clone that primary wala map  */
					Map<Id,Calling_List__c>mapOfSubordinatesToUpdate = new Map<Id,Calling_List__c>();
					Map<Calling_List__c,List<Calling_List__c>>primaryToSubOrdinateCallingListMap = new Map<Calling_List__c,List<Calling_List__c>>(); 
					System.debug('accAndRecIdToPrimaryCallingMap:$$'+accAndRecIdToPrimaryCallingMap);   
					System.debug('accAndRecToSubOrdinateCallingMap:$$'+accAndRecToSubOrdinateCallingMap);   
					if(accAndRecIdToPrimaryCallingMap != null && !accAndRecIdToPrimaryCallingMap.isEmpty()){
						Calling_List__c primaryCalling;
						/*for each primary we have to clone and needs to create set of sub ordinate ids */
						//List<Calling_List__c>subOrdinateCallingsLstsRelatedToPrimary = new List<Calling_List__c>();
						for(String accRecTypeIdInst : accAndRecIdToPrimaryCallingMap.KeySet()){
							List<Calling_List__c> subOrdinateCallingsLstsRelatedToPrimary = new List<Calling_List__c> ();
							primaryCalling = new Calling_List__c() ;
							primaryCalling = accAndRecIdToPrimaryCallingMap.get(accRecTypeIdInst).clone(false,false,false,false);
							if(accAndRecToSubOrdinateCallingMap.get(accRecTypeIdInst) != null && !accAndRecToSubOrdinateCallingMap.get(accRecTypeIdInst).isEmpty()){
								
								subOrdinateCallingsLstsRelatedToPrimary.addAll(accAndRecToSubOrdinateCallingMap.get(accRecTypeIdInst));
							}
							if(subOrdinateCallingsLstsRelatedToPrimary != null && !subOrdinateCallingsLstsRelatedToPrimary.isEmpty()){
								for(Calling_List__c subInst : subOrdinateCallingsLstsRelatedToPrimary){
									if(apiTocustomFieldsSettingMap != null && !apiTocustomFieldsSettingMap.isEmpty()){
										for(String apinameInst : apiTocustomFieldsSettingMap.keySet()){
										if(primaryCalling.get(apiTocustomFieldsSettingMap.get(apinameInst)) != null){
												subInst.put((String)apiTocustomFieldsSettingMap.get(apinameInst),
															primaryCalling.get(apiTocustomFieldsSettingMap.get(apinameInst)));
															if(!mapOfSubordinatesToUpdate.containsKey(subInst.Id)){
																mapOfSubordinatesToUpdate.put(subInst.Id,subInst);
															}
										  }
										}
									}
								}
							}
						}
					}
					
					/**/
			list<Database.SaveResult> results  = new list<Database.SaveResult>();
					System.debug('mapOfSubordinatesToUpdate:::^^^^!'+mapOfSubordinatesToUpdate);
					System.debug('mapOfSubordinatesToUpdate:::^^size^^!'+mapOfSubordinatesToUpdate.size());
					if(mapOfSubordinatesToUpdate != null && !mapOfSubordinatesToUpdate.isEmpty()){
						//update mapOfSubordinatesToUpdate.values();
							results = Database.update(mapOfSubordinatesToUpdate.values(),false);
					}
			System.debug('results :   '+results);
			System.debug('results:::^^size^^ ! '+results.size());
			}
		}   
    }
    public class CallingWrapper{
        public Id accId {get;set;}
        public Map<Id,List<Calling_List__c>>RecordTypeIdToCallingListMap {get;set;}
        
        public CallingWrapper(Id accId, Map<Id,List<Calling_List__c>>RecordTypeIdToCallingListMap){
            this.accId = accId;
            this.RecordTypeIdToCallingListMap = RecordTypeIdToCallingListMap.clone();
        }
    }   
}