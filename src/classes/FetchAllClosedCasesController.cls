public with sharing class FetchAllClosedCasesController {
  public String callingListId {get;set;}
    public Id callingListRecordTypeId {get;set;}
    public Id caseRecordTypeId {get;set;}
    public List<Calling_List__c>callingList {get;set;}
    public List<Id> accList {get;set;}
    public List<Case>caseList {get;set;}
    public FetchAllClosedCasesController(ApexPages.StandardController controller) {
        fetchCases();
    }
     /*This Method is used to fetch all the cases of AOPT record type*/
    public void fetchCases(){
        callingList = new List<Calling_List__c>();
        Id bouncedChequeRTId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Bounced Cheque').RecordTypeId;
        accList = new List<Id>();
        caseList = new List<Case>();
        caseRecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('AOPT').RecordTypeId;
        callingListRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
        callingListId = ApexPages.currentPage().getParameters().get('id');
       // callingListId = String.valueOf(callingListId).substring(0, 15);
        System.debug('callingListId:::::::'+callingListId);
        callingList = [SELECT Id,
                            RecordTypeId,
                            Case__c,
                            Name,
                            Account__r.Name,
                            Account__c
                       FROM Calling_List__c
                       WHERE Id = :callingListId
                         ];//AND RecordTypeId = :callingListRecordTypeId
        //Method to display Cases for Bounced Cheque record type
        /*if( !callingList.isEmpty() && callingList[0].RecordTypeId == bouncedChequeRTId) {
            displayBouncedChequeCases(callingList[0].Case__c);
        }*/
       // else {
            system.debug('******callingList******'+callingList);
            for(Calling_List__c callingInst : callingList){
                if( callingInst.Account__c != null )
                accList.add(callingInst.Account__c);
            }
            System.debug('accList::::'+accList);
            if(accList != null && accList.isEmpty() == false ){
                System.debug('accList::inside if '+accList );
                caseList = [SELECT Id,
                                   caseNumber,
                                   Status,
                                   OwnerId,
                                   Owner.FirstName,
                                   CreatedDate,
                                   Approval_Status__c,
                                   RecordType.Name,
                                   AccountId
                              FROM Case
                             WHERE AccountId IN :accList
                             AND Status = 'Closed' 
                             AND Status = 'Rejected'
                             ORDER BY CreatedDate 
                             desc
                             limit 950];
                System.debug('caseList::************::'+caseList);
            }
            else{
                system.debug('inside else part');
            }
        //}


    }//fetchCases   
}