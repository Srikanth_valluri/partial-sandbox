@isTest
public class GetTenantDetailsAPITest {
  @isTest
  static void getTenantDetailsTest1() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
        
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
        
        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
    
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Villa';
        insert invObj;
    
        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'Y',
                                      Inventory__c = invObj.id,
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;
    
        List<FM_Case__c> lstFmCases = new List<FM_Case__c>();
        
        Id rtMoveInId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId();
        FM_Case__c objTenantRegn = new FM_Case__c();
        objTenantRegn.Origin__c = 'Portal';
        objTenantRegn.isHelloDamacAppCase__c = true;
        objTenantRegn.Tenant__c = account.Id;
        objTenantRegn.RecordtypeId = rtMoveInId;
        objTenantRegn.Status__c = 'Closed';
        objTenantRegn.Request_Type__c = 'Tenant Registration';
        objTenantRegn.Booking_Unit__c = bookingUnit.id;
        lstFmCases.add(objTenantRegn);
        
        Test.startTest();
    
        //insert lstFmCases;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getTenantDetails';  
        req.addParameter('bookingUnitId',bookingUnit.Id);
        req.addParameter('accountId',account.Id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        GetTenantDetailsAPI.getTenancyRenewalSRFromId();
        
        Test.stopTest();
  }
  
    @isTest
  static void getTenantDetailsTest2() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
        
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
        
        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
    
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Villa';
        insert invObj;
    
        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'Y',
                                      Inventory__c = invObj.id,
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;
        
        Id rtMoveInId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId();
        FM_Case__c objTenRenewal = new FM_Case__c();
        objTenRenewal.Origin__c = 'Portal';
        objTenRenewal.isHelloDamacAppCase__c = true;
        objTenRenewal.Tenant__c = account.Id;
        objTenRenewal.RecordtypeId = rtMoveInId;
        objTenRenewal.Status__c = 'In Progress';
        objTenRenewal.Request_Type__c = 'Tenant Renewal';
        objTenRenewal.Booking_Unit__c = bookingUnit.id;
        
        Test.startTest();
    
        insert objTenRenewal;
        
        FM_Additional_Detail__c objDetContact = new FM_Additional_Detail__c();
        objDetContact.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
        objDetContact.Emergency_Contact_Case__c = objTenRenewal.Id ;
        objDetContact.Emergency_Contact_Account__c = objTenRenewal.Tenant__c;
        objDetContact.Name__c = 'Test1 Add1';
        objDetContact.Relationship__c = 'Test Relation';
        objDetContact.Email__c = 'test1@test.com';
        objDetContact.Mobile__c = '1111111111';
        
        insert objDetContact;
        
        FM_Additional_Detail__c objResident = new FM_Additional_Detail__c();
        objResident.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Resident').getRecordTypeId();
        objResident.Resident_Case__c = objTenRenewal.Id;
        objResident.Name__c = 'Test2 Res2';
        objResident.Nationality__c = 'UAE';
        objResident.Gender__c = 'Male';
        objResident.Passport_Number__c = '222222222';
        objResident.Identification_Number__c = '333333333';
        objResident.Mobile__c = '44444444444';
        objResident.Email__c = 'test@test.com';
        
        insert objResident;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getTenantDetails';  
        req.addParameter('bookingUnitId',bookingUnit.Id);
        req.addParameter('accountId',account.Id);
        req.addParameter('fmCaseId',objTenRenewal.Id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        GetTenantDetailsAPI.getTenancyRenewalSRFromId();
        
        Test.stopTest();
  }
    
    @isTest
    static void getTenantDetailsTest3() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
        
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
        
        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
    
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Villa';
        insert invObj;
    
        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'Y',
                                      Inventory__c = invObj.id,
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;
    
        Test.startTest();
    
        RestResponse res = new RestResponse();
        
        RestRequest req1 = new RestRequest();
        req1.requestURI = '/getTenantDetails';
        req1.httpMethod = 'GET';
        RestContext.request = req1;
        RestContext.response = res;
        GetTenantDetailsAPI.getTenancyRenewalSRFromId();
        
        RestRequest req2 = new RestRequest();
        req2.requestURI = '/getTenantDetails';  
        req2.addParameter('bookingUnitId','');
        req2.httpMethod = 'GET';
        RestContext.request = req2;
        RestContext.response = res;
        GetTenantDetailsAPI.getTenancyRenewalSRFromId();
        
        RestRequest req3 = new RestRequest();
        req3.requestURI = '/getTenantDetails';  
        req3.addParameter('bookingUnitId',bookingUnit.Id);
        req3.httpMethod = 'GET';
        RestContext.request = req3;
        RestContext.response = res;
        GetTenantDetailsAPI.getTenancyRenewalSRFromId();
        
        RestRequest req4 = new RestRequest();
        req4.requestURI = '/getTenantDetails';  
        req4.addParameter('bookingUnitId',bookingUnit.Id);
        req4.addParameter('accountId','');
        req4.httpMethod = 'GET';
        RestContext.request = req4;
        RestContext.response = res;
        GetTenantDetailsAPI.getTenancyRenewalSRFromId();
        
        Test.stopTest();
  }
}