global class UpdateActiveFlag4Customer implements Database.Batchable<sObject> {
    //AKISHOR
    String query;
    public static Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
    public static Id buiesnesAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
    global Database.QueryLocator start(Database.BatchableContext BC) {
    
        String recordId =  Label.dailyDpBatchForSingleUser.split('-',2)[1];
        
        query = 'SELECT Id,Party_ID__c,RecordTypeId,OwnerId FROM Account WHERE ( RecordTypeId =: personAccRTId OR RecordTypeId =: buiesnesAccRTId ) ' ;
        query += ' AND Party_ID__c != null AND( Active_Customer__c = NULL OR Active_Customer__c=\'Inactive\')';
        if(Label.dailyDpBatchForSingleUser.split('-',2)[0] == 'ON'){
            query +=' AND ID = :recordId';
        }
     
               
        system.debug( ' query : ' + query );        
        return Database.getQueryLocator(query);
    }
     global void execute( Database.BatchableContext BC, List<Account> accLsLst ) {
        system.debug( ' accLsLst : ' + accLsLst.size());
        List<Account> lstAcc = new List<Account>();
        Set<String> setPartyId = new Set<String>();
        Map<String,List<Booking_Unit__C>> mapPartyIdlstBU  = new Map<String,List<Booking_Unit__C>>();
        Set<String> SetAccountId = new  Set<String>();
        
        
        system.debug( ' accLsLst : ' + accLsLst );
        for( Account accObj : accLsLst ) {
            system.debug( ' accObj : ' + accObj );
            if( accObj.RecordTypeId == personAccRTId 
                && accObj.Party_ID__c != '') {
                setPartyId.add( accObj.Party_ID__c );  
                SetAccountId.add(String.valueOf(accObj.id).substring(0, 15));
                           
            }
        }
        
        system.debug('partyidset='+setPartyId);
        
        if(setPartyId!=null && setPartyId.size()>0){
            
            //query booking unit and fill map of patry Id vs Bu 

            for( Booking_Unit__C objBU :[select id,Party_ID__c, Booking__r.Account__r.Party_Id__c,
                    Unit_Active__c from booking_unit__c where (Booking__r.Account__r.Party_Id__c IN: setPartyId OR Party_ID__c IN: setPartyId)
                    AND Account_Id__c IN : SetAccountId
                    AND Unit_Active__c='Active']) {
                                
                system.debug('partyaccmap' + mapPartyIdlstBU);            
                if(mapPartyIdlstBU.containsKey(objBU.Booking__r.Account__r.Party_Id__c) && mapPartyIdlstBU.get(objBU.Booking__r.Account__r.Party_Id__c) != null ) {
                    List<Booking_Unit__C> lstBU = mapPartyIdlstBU.get(objBU.Booking__r.Account__r.Party_Id__c);
                    lstBU.add(objBU);
                    mapPartyIdlstBU.put(objBU.Booking__r.Account__r.Party_Id__c,lstBU );
                }else if(mapPartyIdlstBU.containsKey(objBU.Party_Id__c) && mapPartyIdlstBU.get(objBU.Party_Id__c) != null ){
                        List<Booking_Unit__C> lstBU = mapPartyIdlstBU.get(objBU.Party_Id__c);
                        lstBU.add(objBU);
                        mapPartyIdlstBU.put(objBU.Party_Id__c,lstBU );
                } else {
                        if(String.isNotBlank(objBU.Booking__r.Account__r.Party_Id__c)){
                            mapPartyIdlstBU.put(objBU.Booking__r.Account__r.Party_Id__c,new List<Booking_Unit__C>{objBU});
                        }else if(String.isNotBlank(objBU.Party_Id__c)){
                            mapPartyIdlstBU.put(objBU.Party_Id__c,new List<Booking_Unit__C>{objBU});
                        }
                        
                }
            } 
        }
        system.debug( 'map: ' + mapPartyIdlstBU);
        
        // Again loop on Account to update Active_Customer__c fields 
        for(Account accObj : accLsLst ) {
            system.debug( 'accObj : ' + accObj );
            if(accObj.RecordTypeId == personAccRTId && accObj.Party_ID__c != '') {
                  if(mapPartyIdlstBU.containskey(accObj.Party_ID__c)){
                      accObj.Active_Customer__c = mapPartyIdlstBU.get(accObj.Party_ID__c)[0].Unit_Active__c;                                     
                  } else{
                      accObj.Active_Customer__c='Inactive';
                  }
                  lstAcc.add(accObj);  
            }
        }
        
        if(lstAcc.size()>0){
            update lstAcc;
        }
    }
    global void finish(Database.BatchableContext BC) {

    }
 }