/*********************************************************************************************
* @Description : Method to contain logic to be executed before Insert.mapping of the DOS HOS HOD
*   Status,Sales Office Based on the Asscoiated PC - Manager DOS , 
*   Associated PC s DOS and HOS Manager Role and 
*   Associated PCs DOS and HOS and HOD Managers Role                            
* @Params      : List<sObject>                                                               
* @Return      : void                                                                        
*********************************************************************************************/
public class DealTeamTrgHandler implements TriggerFactoryInterface  {

    // Mapping Accociated DOS,HOS,HOD,Sales Office ,Status.Based on Role names of the managers related to Associated PC
    public void executeBeforeInsertTrigger(list<sObject> mapNewRecords){
        system.debug('#### TEST');
        try{
            map<Id, sObject> dealTeamTargetMap = new Map<Id,sObject>();
            set<id> associatedPcIds = new set<id>();
            map<id,id> srToShare = new map<id,id>();
            map<Id, Deal_Team__c> dealTeamMap = new Map<Id, Deal_Team__c>();
            
            for(sObject thisSObject : mapNewRecords){
                Deal_Team__c thisTeam = (Deal_Team__c )thisSObject;
                associatedPcIds.add(thisTeam.Associated_PC__c);
                
                /*if(thisteam.Allow_Sharing_with_PC__c){
srToShare.put(thisTeam.Associated_Deal__c,thisTeam.Associated_PC__c);
}*/
                system.debug('SR REQ ID'+thisTeam.Associated_Deal__c);
            }
            
            list<user> PCmanagers = [select id,Sales_Office__c,managerId,Manager.managerId,Manager.Manager.ManagerId,Manager_Role__c,Manager.Manager_Role__c,Manager.manager.Manager_Role__c from user where Id IN:associatedPcIds];
            //system.debug(PCManagers[0].managerId);
            //system.debug(PCManagers[0].Manager.managerId);
            //system.debug(PCManagers[0].Manager.Manager.ManagerId);
            system.debug(PCmanagers);
            //Populate Maps
            map<string,string> mapDos = new map<string,string>();
            map<string,string> maphos = new map<string,string>();
            map<string,string> maphod = new map<string,string>();
            map<string,string> salesOffice = new map<string,string>();
            if(PCmanagers!= null && PCmanagers.size() > 0){
                system.debug('#### inside PC');
                for(user u:PCmanagers){
                    system.debug('### inside loop');
                    //Check From DOS-->HOS-->HOD
                    if(u.ManagerId != null && string.valueof(u.Manager_Role__c).containsIgnoreCase('DOS')){ // PC s Man Role = DOS
                        system.debug('Man role>>>>>>>>>>>'+u.Manager_Role__c);
                        mapDos.put(u.id,u.Managerid);
                        if(string.valueof(u.Manager.Manager_Role__c).containsIgnoreCase('HOS')){ // PC s Man is DOS so DOS Man = HOS ?
                            mapHos.put(u.id,u.Manager.ManagerId);
                            if(u.Manager.Manager.ManagerId != null && string.valueof(u.Manager.Manager.Manager_Role__c).containsIgnoreCase('HOD')){// PC s Man is DOS so DOS Man is HOS so HOS Man = HOD ?
                                maphod.put(u.id,u.Manager.Manager.ManagerId);
                            }else{
                                maphod.put(u.id, u.Manager.ManagerId);
                            }
                        }
                        // DOS and HOD no HOS
                        if(u.Manager.ManagerId != null && string.valueof(u.Manager.Manager_Role__c).containsIgnoreCase('HOD')){// PC s Man is DOS so DOS Man is HOS so HOS Man = HOD ?
                            maphos.put(u.id, u.Manager.ManagerId);
                            maphod.put(u.id,u.Manager.ManagerId);
                        }
                        if(u.Manager.ManagerId != null && !string.valueof(u.Manager.Manager_Role__c).containsIgnoreCase('HOS') && !string.valueOf(u.Manager.Manager_Role__c).containsIgnoreCase('HOD')){
                            maphos.put(u.id, u.ManagerId);
                            maphod.put(u.id, u.ManagerId);
                        }
                        
                    }
                    system.debug('after first check');
                    //Check from HOS-->HOD
                    if(u.Managerid != null && string.valueof(u.Manager_Role__c).containsIgnoreCase('HOS')){ // PC s Man Role = HOS
                        system.debug('#### ' + u.ManagerId);
                        mapDos.put(u.id, u.ManagerId);
                        mapHos.put(u.id,u.Managerid);
                        if(u.Manager.ManagerId != null && string.valueof(u.Manager.Manager_Role__c).containsIgnoreCase('HOD')){// PC s Man is DOS so DOS Man is HOS so HOS Man = HOD ?
                            maphod.put(u.id,u.Manager.ManagerId);
                        }else{
                            maphod.put(u.id, u.ManagerId);
                        }
                    }
                    system.debug('after second check');
                    //Check HOD
                    if(u.Managerid != null && string.valueof(u.Manager_Role__c).containsIgnoreCase('HOD')){ // PC s Man Role = HOD
                        system.debug('#### ' + u.ManagerId);
                        mapDos.put(u.id,u.ManagerId);
                        mapHos.put(u.id,u.Managerid);
                        maphod.put(u.id,u.ManagerId);
                    }
                    system.debug('third check');
                    salesOffice.put(u.id,u.Sales_Office__c);            
                    system.debug(salesOffice);
                }   
                system.debug('mapDos-->'+mapDos);
                system.debug('mapHos-->'+mapHos);
                system.debug('mapHod-->'+mapHOD);
                
                for(sObject thisSObject : mapNewRecords){
                    dealTeamTargetMap.put(thisSObject.Id, thisSObject);
                    Deal_Team__c thisTeam = (Deal_Team__c )thisSObject;
                    if(thisTeam.Associated_DOS__c == null){ 
                        thisTeam.Associated_DOS__c = mapDos.get(thisteam.Associated_PC__c);
                    }
                    if(thisTeam.Associated_HOS__c == null){ 
                        thisTeam.Associated_HOS__c = maphos.get(thisteam.Associated_PC__c); 
                    }
                    if(thisTeam.Associated_HOD__c == null){ 
                        thisTeam.Associated_HOD__c = maphod.get(thisteam.Associated_PC__c);
                    }
                    thisTeam.User_Sales_Office__c = salesOffice.get(thisteam.Associated_PC__c);
                    thisTeam.Status__c = 'Active';
                    dealTeamMap.put(thisTeam.Id, thisTeam);
                    // Added on May 21 2017 by subhash following Kaavya and Riddhis Instructions
                    // IF PC is not having DOS map DOS as HOS 
                    /* if( mapDos.get(thisteam.Associated_PC__c) == null && maphos.get(thisteam.Associated_PC__c) != null){
thisTeam.Associated_DOS__c = maphos.get(thisteam.Associated_PC__c);
}
// IF PC is not having DOS,HOS map DOS as HOD 
if( mapDos.get(thisteam.Associated_PC__c) == null && maphos.get(thisteam.Associated_PC__c) == null && maphod.get(thisteam.Associated_PC__c) != null){
thisTeam.Associated_DOS__c = maphod.get(thisteam.Associated_PC__c);
}   
// Added on May 22 2017 by subhash following Kaavya and Riddhis Instructions 
if( maphod.get(thisteam.Associated_PC__c) == null && maphos.get(thisteam.Associated_PC__c) != null){
thisteam.Associated_HOD__c =  maphos.get(thisteam.Associated_PC__c);   
}*/
                    
                }
                sharingSRonDealToPC(dealTeamMap);  
                addTarget(dealTeamTargetMap, new Map<Id, sObject>());
            }
        }catch(exception e){
            system.debug(e.getMessage());
        }
    }
    
    
    /*********************************************************************************************
* @Description : Method to give the Sharing access for the Asscoiated PC on the SR when the 
Deal team is Created.          

*********************************************************************************************/

    public void sharingSRonDealToPC(map<id,Deal_Team__c> dealTeamMap){
        list<NSIBPM__Service_Request__share> shares = new list<NSIBPM__Service_Request__share>();
        for(Deal_Team__c thisTeam : dealTeamMap.values()){
            Set<Id> shareId = new Set<Id>();
            
            if(String.isNotBlank(thisTeam.Associated_PC__c)){
                NSIBPM__Service_Request__share sharePC = new NSIBPM__Service_Request__share();
                sharePC.ParentId = thisTeam.Associated_Deal__c;
                sharePC.AccessLevel = 'Read';
                sharePC.RowCause = Schema.NSIBPM__Service_Request__Share.RowCause.Property_Consultant_Access__c;
                sharePC.UserOrGroupId = thisTeam.Associated_PC__c;
                shareId.add(thisTeam.Associated_PC__c);
                shares.add(sharePC);
            }
            
            if(!shareId.contains(thisTeam.Associated_DOS__c) && String.isNotBlank(thisTeam.Associated_DOS__c)){
                NSIBPM__Service_Request__share shareDOS = new NSIBPM__Service_Request__share();
                shareDOS.ParentId = thisTeam.Associated_Deal__c;
                shareDOS.AccessLevel = 'Read';
                shareDOS.RowCause = 'Director_Of_Sales_Access__c';
                shareDOS.UserOrGroupId = thisTeam.Associated_DOS__c;
                shareId.add(thisTeam.Associated_DOS__c);
                shares.add(shareDOS);
            }
            if(!shareId.contains(thisTeam.Associated_HOS__c) &&  String.isNotBlank(thisTeam.Associated_HOS__c)){
                NSIBPM__Service_Request__share shareHOS = new NSIBPM__Service_Request__share();
                shareHOS.ParentId = thisTeam.Associated_Deal__c;
                shareHOS.AccessLevel = 'Read';
                shareHOS.RowCause = 'Head_Of_Sales_Access__c';
                shareHOS.UserOrGroupId = thisTeam.Associated_HOS__c;
                shareId.add(thisTeam.Associated_HOS__c);
                shares.add(shareHOS);
            }
            if(!shareId.contains(thisTeam.Associated_HOD__c) &&String.isNotBlank(thisTeam.Associated_HOD__c)){
                NSIBPM__Service_Request__share shareHOD = new NSIBPM__Service_Request__share();
                shareHOD.ParentId = thisTeam.Associated_Deal__c;
                shareHOD.AccessLevel = 'Read';
                shareHOD.RowCause = 'Head_Of_Department_Access__c';
                shareHOD.UserOrGroupId = thisTeam.Associated_HOD__c;
                shareId.add(thisTeam.Associated_HOD__c);
                shares.add(shareHOD);
            }
        }    
        try {
            system.debug('shares------------------'+shares);
            Database.insert(shares);    
        } catch (exception ex) {
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
        
    }
    
/*********************************************************************************************
* @Description : Method to Remove the Sharing access for the Associated PC on the SR when the 
Deal team status is Inactive           
*********************************************************************************************/  
    
    public void executeAfterUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){ 
        try{
            Set<Id> dealTeamUserSet = new Set<Id>();
            // Get Inactive Deal Teams
            List<NSIBPM__Service_Request__Share> deleteSharingRecordList = new List<NSIBPM__Service_Request__Share>();
            set<id> inactivePCids = new set<id>();
            set<id> srIds = new set<id>();
            Set<Id> dealIds = new Set<ID>();
            for(sObject thisSobject:newRecordsMap.values()){
                Deal_Team__c thisTeam = (Deal_Team__c)thisSObject;
                if(thisTeam.status__c == 'Inactive'){
                    inactivePCids.add(thisTeam.Associated_PC__c);
                    srIds.add(thisTeam.Associated_deal__c);
                }
                dealTeamUserSet.add(thisTeam.Associated_PC__c);
                if(String.isNotBlank(String.valueOf(thisTeam.Associated_HOS__c)))
                    dealTeamUserSet.add(thisTeam.Associated_HOS__c);
                if(String.isNotBlank(String.valueOf(thisTeam.Associated_DOS__c)))
                    dealTeamUserSet.add(thisTeam.Associated_DOS__c);
                if(String.isNotBlank(String.valueOf(thisTeam.Associated_HOD__c)))
                    dealTeamUserSet.add(thisTeam.Associated_HOD__c);
            }
            Map<Id, List<NSIBPM__Service_Request__Share>> bookingShareRecordsMap = getSharingRecords(srIds,inactivePCids);  
            system.debug(inactivePCids);
            for(id i:inactivePCids){
                for(NSIBPM__Service_Request__Share thisShareRecord : bookingShareRecordsMap.get(i)){
                    deleteSharingRecordList.add(thisShareRecord);  
                }        
            }
            
            if(!deleteSharingRecordList.isEmpty()){
                system.debug(deleteSharingRecordList+'deleteSharingRecordList-->');
                database.deleteResult[] result = database.delete(deleteSharingRecordList, false);
            }
            
            if(!dealTeamUserSet.isEmpty()){
                //ServiceRequestTrgHandler SRtrg = new ServiceRequestTrgHandler();
                //SRtrg.updateTargetActuals(dealTeamUserSet);
            }
            updateSharingRules(newRecordsMap, oldRecordsMap);
            PopulatevaluesInBookingUnit((map<Id, Deal_Team__c>)newRecordsMap,(map<Id, Deal_Team__c>)oldRecordsMap);
        }catch(exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
        
    }
    
    private void updateSharingRules(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){
        Map<Id, Set<Id>> deleteSharingRecordsMap = new Map<Id, Set<Id>>();
        //Map<Id, List<NSIBPM__Service_Request__Share>> deleteSharingRecordsMap = new Map<Id, List<NSIBPM__Service_Request__Share>>();
        List<NSIBPM__Service_Request__Share> deleteSharingRecordList = new List<NSIBPM__Service_Request__Share>();
        List<NSIBPM__Service_Request__Share> shares = new List<NSIBPM__Service_Request__Share>();
        
             
        for(Deal_Team__c newDealTeam : (List<Deal_Team__c>) newRecordsMap.values()){
            Deal_Team__c oldDealTeam = (Deal_Team__c) oldRecordsMap.get(newDealTeam.Id);
            if(newDealTeam!=null && oldDealTeam!=null){
                if(newDealTeam.Associated_PC__c != oldDealTeam.Associated_PC__c){
                    if(deleteSharingRecordsMap.containsKey(newDealTeam.Associated_Deal__c)){
                        deleteSharingRecordsMap.get(newDealTeam.Associated_Deal__c).add(oldDealTeam.Associated_PC__c);
                    }else{
                        deleteSharingRecordsMap.put(newDealTeam.Associated_Deal__c, new Set<Id>{oldDealTeam.Associated_PC__c});
                    }
                    NSIBPM__Service_Request__share sharePC = new NSIBPM__Service_Request__share();
                    sharePC.ParentId = newDealTeam.Associated_Deal__c;
                    sharePC.AccessLevel = 'Edit';
                    sharePC.RowCause = 'Property_Consultant_Access__c';
                    sharePC.UserOrGroupId = newDealTeam.Associated_PC__c;
                    shares.add(sharePC);
                }
                if(newDealTeam.Associated_DOS__c != oldDealTeam.Associated_DOS__c){
                    if(deleteSharingRecordsMap.containsKey(newDealTeam.Associated_Deal__c)){
                        deleteSharingRecordsMap.get(newDealTeam.Associated_Deal__c).add(oldDealTeam.Associated_DOS__c);
                    }else{
                        deleteSharingRecordsMap.put(newDealTeam.Associated_Deal__c, new Set<Id>{oldDealTeam.Associated_DOS__c});
                    }
                    
                    NSIBPM__Service_Request__share shareDOS = new NSIBPM__Service_Request__share();
                    shareDOS.ParentId = newDealTeam.Associated_Deal__c;
                    shareDOS.AccessLevel = 'Read';
                    shareDOS.RowCause = 'Director_Of_Sales_Access__c';
                    shareDOS.UserOrGroupId = newDealTeam.Associated_DOS__c;
                    shares.add(shareDOS);
                }
                if(newDealTeam.Associated_HOS__c != oldDealTeam.Associated_HOS__c){
                    if(deleteSharingRecordsMap.containsKey(newDealTeam.Associated_Deal__c)){
                        deleteSharingRecordsMap.get(newDealTeam.Associated_Deal__c).add(oldDealTeam.Associated_HOS__c);
                    }else{
                        deleteSharingRecordsMap.put(newDealTeam.Associated_Deal__c, new Set<Id>{oldDealTeam.Associated_HOS__c});
                    }
                    NSIBPM__Service_Request__share shareHOS = new NSIBPM__Service_Request__share();
                    shareHOS.ParentId = newDealTeam.Associated_Deal__c;
                    shareHOS.AccessLevel = 'Read';
                    shareHOS.RowCause = 'Head_Of_Sales_Access__c';
                    shareHOS.UserOrGroupId = newDealTeam.Associated_HOS__c;
                    shares.add(shareHOS);
                } 
                if(newDealTeam.Associated_HOD__c != oldDealTeam.Associated_HOD__c){
                    if(deleteSharingRecordsMap.containsKey(newDealTeam.Associated_Deal__c)){
                        deleteSharingRecordsMap.get(newDealTeam.Associated_Deal__c).add(oldDealTeam.Associated_HOD__c);
                    }else{
                        deleteSharingRecordsMap.put(newDealTeam.Associated_Deal__c, new Set<Id>{oldDealTeam.Associated_HOD__c});
                    }
                    NSIBPM__Service_Request__share shareHOD = new NSIBPM__Service_Request__share();
                    shareHOD.ParentId = newDealTeam.Associated_Deal__c;
                    shareHOD.AccessLevel = 'Read';
                    shareHOD.RowCause = 'Head_Of_Department_Access__c';
                    shareHOD.UserOrGroupId = newDealTeam.Associated_HOD__c;
                    shares.add(shareHOD);
                }
            }
            
        }
        
        for(NSIBPM__Service_Request__Share thisShareRecord : [SELECT Id, ParentId, AccessLevel, UserOrGroupId, RowCause 
                                                              FROM NSIBPM__Service_Request__Share 
                                                              WHERE ParentId IN: deleteSharingRecordsMap.keySet()])
        {
            if(deleteSharingRecordsMap.get(thisShareRecord.ParentId).contains(thisShareRecord.UserOrGroupId)){
                deleteSharingRecordList.add(thisShareRecord);
            }
        }
        
        if(!deleteSharingRecordList.isEmpty()){
            system.debug(deleteSharingRecordList+'deleteSharingRecordList-->');
            database.deleteResult[] result = database.delete(deleteSharingRecordList, false);
        }
        
        if(!shares.isEmpty()){
            Database.insert(shares);
        }
                
    }
    
    /*********************************************************************************************
* @Description : Method to get sharing records from the Service request Share table.              

*********************************************************************************************/  
    private Map<Id, List<NSIBPM__Service_Request__Share>> getSharingRecords(Set<Id> recordIdsSet,set<id> userIds){ 
        Map<Id, List<NSIBPM__Service_Request__Share>> bookingShareRecordsMap = new Map<Id, List<NSIBPM__Service_Request__Share>>();
        for(NSIBPM__Service_Request__Share thisShareRecord : [SELECT Id, ParentId, AccessLevel, UserOrGroupId, RowCause 
                                                              FROM NSIBPM__Service_Request__Share 
                                                              WHERE ParentId IN: recordIdsSet and UserOrGroupId IN:userIds]){
                                                                  if(bookingShareRecordsMap.containsKey(thisShareRecord.UserOrGroupId )){
                                                                      bookingShareRecordsMap.get(thisShareRecord.UserOrGroupId).add(thisShareRecord);      
                                                                  }else{
                                                                      bookingShareRecordsMap.put(thisShareRecord.UserOrGroupId , new List<NSIBPM__Service_Request__Share>{thisShareRecord});
                                                                  }   
                                                              }
        system.debug('#### bookingShareRecordsMap = '+bookingShareRecordsMap);
        return bookingShareRecordsMap; 
    }
    
    public void executeAfterInsertTrigger(Map<Id, sObject> mapNewRecords){ 
        Set<Id> dealTeamUserSet = new Set<Id>();
        Set<Id> dealIds = new Set<ID>();
        PopulatevaluesInBookingUnit((map<id,Deal_Team__c>)mapNewRecords,null);
        map<Id, Deal_Team__c> dealTeamMapafterinsert = new Map<Id, Deal_Team__c>();
        // Added on Jan 5 for sharing fix        
        sharingSRonDealToPC((map<id,Deal_Team__c>)mapNewRecords);
        
        
        /*system.debug('afterInsert');
system.debug(mapNewRecords.values());
for(Deal_Team__c thisTeam : (List<Deal_Team__c>)mapNewRecords.values()){
dealTeamUserSet.add(thisTeam.Associated_PC__c);
if(String.isNotBlank(String.valueOf(thisTeam.Associated_HOS__c)))
dealTeamUserSet.add(thisTeam.Associated_HOS__c);
if(String.isNotBlank(String.valueOf(thisTeam.Associated_DOS__c)))
dealTeamUserSet.add(thisTeam.Associated_DOS__c);
if(String.isNotBlank(String.valueOf(thisTeam.Associated_HOD__c)))
dealTeamUserSet.add(thisTeam.Associated_HOD__c);
}
if(!dealTeamUserSet.isEmpty()){
ServiceRequestTrgHandler SRtrg = new ServiceRequestTrgHandler();
SRtrg.updateTargetActuals(dealTeamUserSet);
}*/
        
    }
    
    public void executeAfterDeleteTrigger(Map<Id,sObject> oldRecordsMap){ 
        Set<Id> dealTeamUserSet = new Set<Id>();
        /*system.debug('afterDelete');
system.debug(oldRecordsMap.values());
Set<Id> dealIds = new Set<ID>();
for(Deal_Team__c thisTeam : (List<Deal_Team__c>)oldRecordsMap.values()){
dealTeamUserSet.add(thisTeam.Associated_PC__c);
if(String.isNotBlank(String.valueOf(thisTeam.Associated_HOS__c)))
dealTeamUserSet.add(thisTeam.Associated_HOS__c);
if(String.isNotBlank(String.valueOf(thisTeam.Associated_DOS__c)))
dealTeamUserSet.add(thisTeam.Associated_DOS__c);
if(String.isNotBlank(String.valueOf(thisTeam.Associated_HOD__c)))
dealTeamUserSet.add(thisTeam.Associated_HOD__c);
}
if(!dealTeamUserSet.isEmpty()){
ServiceRequestTrgHandler SRtrg = new ServiceRequestTrgHandler();
SRtrg.updateTargetActuals(dealTeamUserSet);
}*/
    }
    
    public List<Deal_Team__c> addTarget(Map<id, sObject> newRecordsMap, Map<id, sObject> oldRecordsMap){
        system.debug('#### addTarget');
        system.debug(newRecordsMap);
        String pcUniqKey;
        String dosUniqKey;
        String hosUniqKey;
        String hodUniqKey;
        Id dealRecordTypeID = DamacUtility.getRecordTypeId('NSIBPM__Service_Request__c', 'Deal');
        Map<Integer, String> monthMap = new Map<Integer, String>{1=>'January', 2=>'February', 3=>'March', 4=>'April', 5=>'May', 6=>'June', 
            7=>'July', 8=>'August', 9=>'September', 10=>'October', 11=>'November', 12=>'December'};
                Set<Id> dealTeamIds = new Set<Id>();
        Map<String, List<Id>> dealTeamMap = new Map<String, List<Id>>();
        Map<String, Target__c> uniqTargetMap = new Map<String, Target__c>();

        List<Deal_Team__c> dealTeamList = new List<Deal_Team__c>();
        for(Deal_Team__c dt : (List<Deal_Team__c>)newRecordsMap.values()){
            dealTeamIds.add(dt.Id);
            system.debug(dt.Associated_Deal__c);
            system.debug(dt.Deal_Registration_Date__c);
            if(dt.Associated_Deal__c != null && dt.Deal_Registration_Date__c != null){
                String monthText = monthMap.get(dt.Deal_Registration_Date__c.month());
                String yearText = String.valueOf(dt.Deal_Registration_Date__c.year());
                dt.PC_Target__c = null;
                dt.DOS_Target__c = null;
                dt.HOS_Target__c = null;
                dt.HOD_Target__c = null;
                system.debug('inside If');
                if(dt.Associated_PC__c != null){
                    pcUniqKey = String.valueOf(dt.Associated_PC__c).substring(0,15) + '-' + monthText + '-' + yearText;
                    if(dealTeamMap.containsKey(pcUniqKey)){
                        dealTeamMap.get(pcUniqKey).add(dt.Id);
                    }
                    else{
                        dealTeamMap.put(pcUniqKey, new List<Id>{dt.Id});
                    }
                }
                if(dt.Associated_DOS__c != null){
                    dosUniqKey = String.valueOf(dt.Associated_DOS__c).substring(0,15) + '-' + monthText + '-' + yearText;
                    if(dealTeamMap.containsKey(dosUniqKey)){
                        dealTeamMap.get(dosUniqKey).add(dt.Id);
                    }
                    else{
                        dealTeamMap.put(dosUniqKey, new List<Id>{dt.Id});
                    }
                }
                if(dt.Associated_HOS__c != null && dt.Associated_HOS__c != dt.Associated_DOS__c){
                    hosUniqKey = String.valueOf(dt.Associated_HOS__c).substring(0,15) + '-' + monthText + '-' + yearText;
                    if(dealTeamMap.containsKey(hosUniqKey)){
                        dealTeamMap.get(hosUniqKey).add(dt.Id);
                    }
                    else{
                        dealTeamMap.put(hosUniqKey, new List<Id>{dt.Id});
                    }
                }
                if(dt.Associated_HOD__c != null && dt.Associated_HOD__c != dt.Associated_HOS__c && dt.Associated_HOD__c != dt.Associated_DOS__c){
                    hodUniqKey = String.valueOf(dt.Associated_HOD__c).substring(0,15) + '-' + monthText + '-' + yearText;
                    if(dealTeamMap.containsKey(hodUniqKey)){
                        dealTeamMap.get(hodUniqKey).add(dt.Id);
                    }
                    else{
                        dealTeamMap.put(hodUniqKey, new List<Id>{dt.Id});
                    }
                }
            }
        }
        system.debug(dealTeamMap);
        if(!dealTeamMap.isEmpty()){
            for(Target__c thisTarget : [SELECT Id, Name, Uniq__c, Target__c FROM Target__c WHERE Uniq__c IN: dealTeamMap.keySet()])
            {
                uniqTargetMap.put(thisTarget.Uniq__c, thisTarget);
            }
        }
        system.debug(uniqTargetMap);
        if(!uniqTargetMap.isEmpty()){
            for(String uniqueKey : uniqTargetMap.keySet()){
                Target__c thisTarget = uniqTargetMap.get(uniqueKey);
                if(dealTeamMap.containsKey(uniqueKey)){
                    for(Id thisDealTeamId : dealTeamMap.get(uniqueKey)){
                        Deal_Team__c thisDealTeam = (Deal_Team__c)newRecordsMap.get(thisDealTeamId);
                        String monthText = monthMap.get(thisDealTeam.Deal_Registration_Date__c.month());
                        String yearText = String.valueOf(thisDealTeam.Deal_Registration_Date__c.year());
                        if(thisDealTeam.Associated_PC__c != null){
                            pcUniqKey = String.valueOf(thisDealTeam.Associated_PC__c).substring(0,15) + '-' + monthText + '-' + yearText;
                            if(pcUniqKey.equals(uniqueKey)){
                                thisDealTeam.PC_Target__c = thisTarget.Id;
                            }
                        }
                        if(thisDealTeam.Associated_DOS__c != null){
                            dosUniqKey = String.valueOf(thisDealTeam.Associated_DOS__c).substring(0,15) + '-' + monthText + '-' + yearText;
                            if(dosUniqKey.equals(uniqueKey)){
                                thisDealTeam.DOS_Target__c = thisTarget.Id;
                            }
                        }
                        if(thisDealTeam.Associated_HOS__c != null ){
                            hosUniqKey = String.valueOf(thisDealTeam.Associated_HOS__c).substring(0,15) + '-' + monthText + '-' + yearText;
                            if(hosUniqKey.equals(uniqueKey)){
                                thisDealTeam.HOS_Target__c = thisTarget.Id;
                            }
                        }
                        if(thisDealTeam.Associated_HOD__c != null ){
                            hodUniqKey = String.valueOf(thisDealTeam.Associated_HOD__c).substring(0,15) + '-' + monthText + '-' + yearText;
                            if(hodUniqKey.equals(uniqueKey)){
                                thisDealTeam.HOD_Target__c = thisTarget.Id;
                            }
                        }
                        //newRecordsMap.put(thisDealTeamId, thisDealTeam);
                    }
                }                
            }
        }
    
        system.debug('#### New Records Map');
        system.debug(newRecordsMap);
        return newRecordsMap.values();
    }
    //TOBE Implemented
    //public void executeBeforeInsertTrigger(List<sObject> newRecordsList){}
    public void executeBeforeInsertUpdateTrigger(list<sObject> newAccountsList,map<Id,sObject> mapOldRecords){ }
    public void executeBeforeDeleteTrigger(Map<Id,sObject> oldRecordsMap){ }
    
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){ }
    
    //public void executeAfterUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){ }
    public void executeBeforeUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        system.debug('### before Update');
        /*map<Id, sObject> newDealTeamMap = new Map<Id, sObject>();
        for(Deal_Team__c newDt : (List<Deal_Team__c>)mapNewRecords.values()){
            Deal_Team__c oldDt = (Deal_Team__c)mapOldRecords.get(newDt.Id);
            if(newDt != null && oldDt != null && (newDt.Associated_PC__c != oldDt.Associated_PC__c || newDt.Associated_DOS__c != oldDt.Associated_DOS__c || newDt.Associated_HOS__c != oldDt.Associated_HOS__c || newDt.Associated_HOD__c != oldDt.Associated_HOD__c )){
                system.debug('check');
                newDealTeamMap.put(newDt.Id, (sObject)newDt);
            }
        }
        if(!newDealTeamMap.isEmpty()){
            addTarget(newDealTeamMap, mapOldRecords);
        }*/
        addTarget(mapNewRecords, mapOldRecords);
        
    }
    
    private void PopulatevaluesInBookingUnit(map<Id, Deal_Team__c> newdealTeamMap,map<Id, Deal_Team__c> olddealTeamMap)
    {
        Set<id> dealsIds=new Set<id>();
        Map<id,string> DealIdHOSNameMap=new Map<id,string>();
        Map<id,string> DealIdHODNameMap=new Map<id,string>();
        Map<id,string> DealIdDosNameMap=new Map<id,string>();
        //if(!test.isrunningtest()){
        for(Deal_Team__c dt:[select id,Associated_Deal__c,name,Primary_Team__c,Associated_PC__r.name,Associated_HOS__r.name,Associated_HOD__r.name,Associated_DOS__r.name from Deal_Team__c where id in:newdealTeamMap.keySet() and Primary_Team__c=:true])
        {
            if(trigger.isInsert||((trigger.isupdate)&& (newdealTeamMap.get(dt.id).Associated_DOS__c!=olddealTeamMap.get(dt.id).Associated_DOS__c||newdealTeamMap.get(dt.id).Associated_HOD__c!=olddealTeamMap.get(dt.id).Associated_HOD__c||newdealTeamMap.get(dt.id).Associated_HOS__c!=olddealTeamMap.get(dt.id).Associated_HOS__c)))
            {
                dealsIds.add(dt.Associated_Deal__c); 
                
                DealIdHOSNameMap.put(dt.Associated_Deal__c, dt.Associated_HOS__r.name);
                
                DealIdDosNameMap.put(dt.Associated_Deal__c, dt.Associated_DOS__r.name);
                
                DealIdHODNameMap.put(dt.Associated_Deal__c, dt.Associated_HOD__r.name);
            }
        }
        //}
        
        List<Booking_Unit__c> UnitListTobeUpdate=new List<Booking_Unit__c>();
        boolean isUnitChange=false;
        id srId;
          system.debug('#### dos value loop');
        for(Booking_Unit__c Unit:[select id,Booking__c,Booking__r.Deal_SR__c from Booking_Unit__c  where Booking__r.Deal_SR__c=:dealsIds])
        {
            
            unit.DOS_Name__c=DealIdDosNameMap.get(Unit.Booking__r.Deal_SR__c);
            unit.HOS_Name__c=DealIdHOSNameMap.get(Unit.Booking__r.Deal_SR__c);
            unit.HOD_Name__c=DealIdHODNameMap.get(Unit.Booking__r.Deal_SR__c);
            UnitListTobeUpdate.add(Unit);
        }
        if(!UnitListTobeUpdate.isEmpty())
        database.update(UnitListTobeUpdate);
    }
}