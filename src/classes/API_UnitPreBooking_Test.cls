@isTest
public class API_UnitPreBooking_Test {

    @testSetup 
    static void createDataTestSetup() {
        Location__c loc ; 
        Inventory__c inventory ; 
        Address__c addressDetail ; 
        Property__c propertyDetail ; 
        
	    loc = InitialiseTestData.getLocationDetails('1234','Building');
	    insert loc ;
	    loc = InitialiseTestData.getLocationDetails('2345','Floor');
	    insert loc ;
	    loc = InitialiseTestData.getLocationDetails('3456','Unit');
	    insert loc ;
	    addressDetail = InitialiseTestData.getAddressDetails(9086);
	    //insert addressDetail ; 
	    propertyDetail = InitialiseTestData.getPropertyDetails(7650);
	    insert propertyDetail ; 
	    inventory = InitialiseTestData.getInventoryDetails('3456','1234','2345',9086,7650); 
	    insert inventory ; 
	    inventory.Status__c = 'Released';
	    update inventory ; 

    }
    
    @isTest 
    static void doUnitPreBooking_Test() {
        RestRequest req = new RestRequest(); 
        
        req.requestURI = '/api_UnitPreBooking';  
        req.httpMethod = 'POST';
        
        API_UnitPreBooking.UnitPreBookingRequestDTO unitPreBookingRequestDTO = new API_UnitPreBooking.UnitPreBookingRequestDTO();
        
        unitPreBookingRequestDTO.selectedInventoryIds = new List<String>();
        unitPreBookingRequestDTO.eoiID = '';
        unitPreBookingRequestDTO.isUkBooking = false;
        
        for (Inventory__c inventory : [SELECT Id FROM Inventory__c]) {
            unitPreBookingRequestDTO.selectedInventoryIds.add(inventory.Id);
        }
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
        
        Test.startTest();
        
        API_UnitPreBooking.doPost(unitPreBookingRequestDTO);
        
        Test.stopTest();
    }
}