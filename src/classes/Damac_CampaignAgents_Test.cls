@isTest
public class Damac_CampaignAgents_Test {
    public static testMethod void method1 () {
    Campaign__c camp = new Campaign__c();
        camp.End_Date__c = system.today().addmonths(10);
        camp.Marketing_End_Date__c = system.today().addmonths(10);
        camp.Marketing_Start_Date__c = system.today().addmonths(-10);
        camp.Start_Date__c =  system.today().addmonths(-10);
        camp.PCAssignment__c = true;
    insert camp;
    
    Account acc = new Account();
        acc.name='testname';
        acc.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        acc.Trade_License_Number__c = '1234';
        acc.Inactive__c = false;
        insert acc; 
        
    User loggedInUser = [SELECT Stand_Id__c FROM USER WHERE Id =: userInfo.getUserId()];
    loggedInUser.Stand_Id__c = camp.id;
    loggedInUser.IsActive = true;
    update loggedInUser; 
       
    Assigned_Agent__c agent = New Assigned_Agent__c ();
        agent.Agency__c = acc.id;
        agent.Campaign__c  = camp.id;
        agent.User__c = loggedInUser.id;
    insert agent;
    
    RestRequest req = new RestRequest();
    
    req.params.put('userID',loggedInUser.id );
   
    RestResponse res = new RestResponse();

    RestContext.request = req;
    RestContext.response = res;

    Damac_CampaignAgents obj = New Damac_CampaignAgents ();
    Damac_CampaignAgents.doGET();
    }
}