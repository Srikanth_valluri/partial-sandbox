public class CSRApprovermapping  {


    @InvocableMethod public static void chageApprover(List<CaseSummaryRequest__c> lstCSR){
        
        if( lstCSR != null && lstCSR.size() > 0 ) {
            System.debug('In class call by PB');
            CaseSummaryRequest__c objCSR;
            
            for (CaseSummaryRequest__c objCSR1 : lstCSR) {
                if( objCSR1.Manager_Approval_Status__c == 'Need More Info'
                    || objCSR1.Director_Approval_Status__c == 'Need More Info'
                    || objCSR1.HOD_Approval_Status__c == 'Need More Info'
                    || objCSR1.Committee_Approval_Status__c == 'Need More Info'){
                        objCSR =  objCSR1;
                    }
                
            }
            


                                                
            System.debug('objCSR=='+objCSR);
                                            
            String previousAuthority = (String.isNotBlank(objCSR.Manager_Approval_Status__c) && String.isNotBlank(objCSR.CRE_Id__c) && objCSR.Manager_Approval_Status__c == 'Need More Info') ? 'CRE' :
                                        (String.isNotBlank(objCSR.Director_Approval_Status__c) && String.isNotBlank(objCSR.Manager_Id__c) && objCSR.Director_Approval_Status__c == 'Need More Info' ) ? 'Manager' :
                                        (String.isNotBlank(objCSR.Director_Approval_Status__c) && String.isNotBlank(objCSR.CRE_Id__c) && objCSR.Director_Approval_Status__c == 'Need More Info' ) ? 'CRE' :
                                        (String.isNotBlank(objCSR.HOD_Approval_Status__c) && String.isNotBlank(objCSR.Director_Id__c) &&  objCSR.HOD_Approval_Status__c == 'Need More Info') ? 'Director' :
                                        (String.isNotBlank(objCSR.HOD_Approval_Status__c) && String.isNotBlank(objCSR.Manager_Id__c) &&  objCSR.HOD_Approval_Status__c == 'Need More Info') ? 'Manager' :
                                        (String.isNotBlank(objCSR.HOD_Approval_Status__c) && String.isNotBlank(objCSR.CRE_Id__c) &&  objCSR.HOD_Approval_Status__c == 'Need More Info') ? 'CRE' :
                                        (String.isNotBlank(objCSR.Committee_Approval_Status__c) && String.isNotBlank(objCSR.HOD_Id__c) && objCSR.Committee_Approval_Status__c == 'Need More Info') ? 'HOD' : 
                                        (String.isNotBlank(objCSR.Committee_Approval_Status__c) && String.isNotBlank(objCSR.Director_Id__c) && objCSR.Committee_Approval_Status__c == 'Need More Info') ? 'Director' : 
                                        (String.isNotBlank(objCSR.Committee_Approval_Status__c) && String.isNotBlank(objCSR.Manager_Id__c) && objCSR.Committee_Approval_Status__c == 'Need More Info') ? 'Manager' : 
                                        (String.isNotBlank(objCSR.Committee_Approval_Status__c) && String.isNotBlank(objCSR.CRE_Id__c) && objCSR.Committee_Approval_Status__c == 'Need More Info') ? 'CRE' :''; 

            System.debug('previousAuthority=='+previousAuthority);                      
           //CRE_Id__c,Manager_Id__c,Director_Id__c,Committee_Id__c,
            String previousAuthorityId = previousAuthority == 'CRE'?objCSR.CRE_Id__c:previousAuthority == 'Manager'?objCSR.Manager_Id__c:previousAuthority == 'Director'?objCSR.Director_Id__c:previousAuthority == 'HOD'?objCSR.HOD_Id__c:'';


            String previousAuthorityName = previousAuthority == 'CRE'?objCSR.CRE_Name__c:previousAuthority == 'Manager'?objCSR.Manager_Name__c:previousAuthority == 'Director'?objCSR.Director_Name__c:previousAuthority == 'HOD'?objCSR.HOD_Name__c:'';


                                            
           
                Case objCase = new Case(Id = objCSR.Case__c);
                objCase.Admin_Fee_For_Title_Deed__c  = previousAuthority;
                objCase.Approving_User_Id__c = previousAuthorityId;
                objCase.Approving_User_Name__c = previousAuthorityName;
                objCase.Approving_User_Role__c =  previousAuthority;
                System.debug('bfr objCase=='+objCase);
                update objCase;
                System.debug('aftr objCase=='+objCase);
            

        }
    }

}


// Approving_User_Id_Old__c
// Approving_User_Name_Old__c
// Approving_User_Role_Old__c