public class PayfortPurchasePageController {

    @testVisible private string PAYFORT_MERCHANT_IDENTIFIER;
    @testVisible private string PAYFORT_REQUEST_PHRASE;
    @testVisible private string PAYFORT_RESPONSE_PHRASE;
    @testVisible private string PAYFORT_ACCESS_CODE;
    @testVisible private string PAYFORT_LANGUAGE;
    @testVisible private string RETURNURL;
    @testVisible private string PURCHASE_ENDPOINT;

    public String token_name                {get; set;}
    public String merchant_reference        {get; set;}
    public String displayMsg                {get; set;}
    public Boolean showPurchaseResult       {get; set;}
    public Boolean showFailPurchaseResult   {get; set;}
    public String receiptName               {get; set;}
    public String customer_email            {get; set;}
    public String fortId                    {get; set;}
    public string errorMsg                  {get; set;}
    public String selectedAccountId         {get; set;}
    public Decimal displayAmt               {get; set;}

    public String amount;
    public String currencyInPay;
    public String language;
    
    public String customer_ip;
    public String signature;
    public String ds_url;
    public String issuerCode;
    public String planCode;

    //data for Receipt & Account Update
    public String typeOfUpdate;
    public String statusCode;
    public String responseMessage;
    public String cardNumber;

    public PayfortPurchasePageController() {

    selectedAccountId = CustomerCommunityUtils.customerAccountId;
    System.debug('AccountId in purchase:: '+selectedAccountId);    

    //getting required data from CS
    PayfortGatewayCreds__c objCreds = PayfortGatewayCreds__c.getInstance();
    System.debug('objCreds: '+objCreds);

    PAYFORT_MERCHANT_IDENTIFIER = objCreds.Merchant_Identifier__c;
    PAYFORT_REQUEST_PHRASE = objCreds.Request_Phrase__c;
    PAYFORT_RESPONSE_PHRASE = objCreds.ResponsePhrase__c;
    PAYFORT_ACCESS_CODE = objCreds.AccessCode__c;
    PAYFORT_LANGUAGE = objCreds.Language__c;
    RETURNURL = objCreds.SiteBaseURL__c + '/PayfortPurchasePage';
    PURCHASE_ENDPOINT = objCreds.PurchaseEndpoint__c;
    currencyInPay = objCreds.CurrencyInPay__c;
    language = objCreds.Language__c;
    amount =  ApexPages.currentPage().getParameters().containsKey('amount') 
            ? ApexPages.currentPage().getParameters().get('amount')
            : '';
    if(String.isNotBlank(amount)) {
        displayAmt = Decimal.valueOf(amount)/100;
    }
    System.debug('amount:'+amount);

    Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;
    System.debug('IsSandbox: '+isSandbox);  

    list<Account> lstAccount = [SELECT id
                                     , Name
                                     , Email__c
                                     , Email__pc
                                     , isPersonAccount
                                FROM Account
                                WHERE id =:selectedAccountId
                               ];

    if(lstAccount.size() > 0 && !Test.isRunningTest()) {
        String emailId = lstAccount[0].isPersonAccount ?
                         lstAccount[0].Email__pc :
                         lstAccount[0].Email__c;

        if ( (isSandbox && String.isNotBlank(emailId) && emailId.containsIgnoreCase('shubham.suryawanshi@eternussolutions.com')) || !isSandbox) {
            customer_email = emailId;
        
        }

        if(isSandbox && String.isBlank(customer_email)) {
            customer_email = 'shubham.suryawanshi@eternussolutions.com';
        }                           
    }

    if(Test.isRunningTest()) {
        customer_email = 'test@test.com';
    }

    System.debug('customer_email is: '+customer_email);

    
    String displayMsg;
    showPurchaseResult = false;
    showFailPurchaseResult = false;
    System.debug('params in purchase page :'+ApexPages.currentPage().getParameters());
    System.debug('return url in purchase: '+ApexPages.currentPage().getParameters().get('return_url'));

    String url = ApexPages.currentPage().getUrl();
    System.debug('url: '+url);
    String parms = url.split('\\?')[1];
    List<String> keyValuePairs = parms.split('\\&');
    System.debug('keyValuePairs: '+keyValuePairs);
    keyValuePairs.sort();
    System.debug('keyValuePairs sorted : '+keyValuePairs);    

        if(String.isNotBlank(ApexPages.currentPage().getParameters().get('response_message')) && !String.ValueOf(ApexPages.currentPage().getParameters().get('response_message')).equalsIgnoreCase('Success')) {

            showFailPurchaseResult = true;
            errorMsg = String.ValueOf(ApexPages.currentPage().getParameters().get('response_message'));

            typeOfUpdate = 'PURCHASE_FAIL';
            merchant_reference = ApexPages.currentPage().getParameters().get('merchant_reference');
            statusCode = String.valueOf(ApexPages.currentPage().getParameters().get('status'));
           
        }

        if(String.isNotBlank(ApexPages.currentPage().getParameters().get('token_name')) && ApexPages.currentPage().getParameters().containsKey('service_command') && String.ValueOf(ApexPages.currentPage().getParameters().get('service_command')).equalsIgnoreCase('TOKENIZATION')) {

            //calling SHA-256 signature method for response received;
            if( !(Apexpages.currentpage().getParameters().containsKey('calculateSHA') && String.valueOf(Apexpages.currentpage().getParameters().get('calculateSHA')) == 'NO' ) ) {

                Boolean isValid = ValidateSignatureForResponse(keyValuePairs, true, PAYFORT_RESPONSE_PHRASE);  //ValidateSignatureForResponse is set to true
                System.debug('isValid for tokenization: '+isValid);
                if(!isValid) {
                    showFailPurchaseResult = true;
                    errorMsg = 'Payfort signature mismatch in validation';

                    typeOfUpdate = 'PURCHASE_FAIL';
                    merchant_reference = ApexPages.currentPage().getParameters().get('merchant_reference');
                }
            }
            

            System.debug('In 1st constructor - Purchase');
            System.debug('Token Name: '+ApexPages.currentPage().getParameters().get('token_name'));
            System.debug('Merchant reference :'+ApexPages.currentPage().getParameters().get('merchant_reference'));

            token_name = ApexPages.currentPage().getParameters().get('token_name');
            merchant_reference = ApexPages.currentPage().getParameters().get('merchant_reference');
            

            customer_ip = getIPAddress();
            String cvv;

            if(ApexPages.currentPage().getParameters().containsKey('card_security_code') && String.isNotBlank(ApexPages.currentPage().getParameters().get('card_security_code'))) {

                cvv = ApexPages.currentPage().getParameters().get('card_security_code');
                System.debug('cvv: '+cvv);

                //for installment
                if(ApexPages.currentPage().getParameters().containsKey('issuer_code') && ApexPages.currentPage().getParameters().containsKey('plan_code') && (String.isNotBlank(ApexPages.currentPage().getParameters().get('issuer_code')) && String.isNotBlank(ApexPages.currentPage().getParameters().get('plan_code')) )) {

                    System.debug('Inside installment part of Purchase Page constructor');
                    issuerCode = ApexPages.currentPage().getParameters().get('issuer_code');
                    planCode = ApexPages.currentPage().getParameters().get('plan_code');

                    signature = getSHA256Signature(PAYFORT_REQUEST_PHRASE, PAYFORT_ACCESS_CODE, PAYFORT_MERCHANT_IDENTIFIER, merchant_reference, amount, currencyInPay, language, customer_email, customer_ip, token_name, RETURNURL, String.ValueOf(ApexPages.currentPage().getParameters().get('remember_me')), cvv, issuerCode, planCode);
                }
                else {
                    signature = getSHA256Signature(PAYFORT_REQUEST_PHRASE, PAYFORT_ACCESS_CODE, PAYFORT_MERCHANT_IDENTIFIER, merchant_reference, amount, currencyInPay, language, customer_email, customer_ip, token_name, RETURNURL, String.ValueOf(ApexPages.currentPage().getParameters().get('remember_me')), cvv, '', '');
                }

                 
            }
            else{

                //for installment
                if(ApexPages.currentPage().getParameters().containsKey('issuer_code') && ApexPages.currentPage().getParameters().containsKey('plan_code') && (String.isNotBlank(ApexPages.currentPage().getParameters().get('issuer_code')) && String.isNotBlank(ApexPages.currentPage().getParameters().get('plan_code')) )) {

                    System.debug('Inside installment part of Purchase Page constructor');
                    issuerCode = ApexPages.currentPage().getParameters().get('issuer_code');
                    planCode = ApexPages.currentPage().getParameters().get('plan_code');

                    signature = getSHA256Signature(PAYFORT_REQUEST_PHRASE, PAYFORT_ACCESS_CODE, PAYFORT_MERCHANT_IDENTIFIER, merchant_reference, amount, currencyInPay, language, customer_email, customer_ip, token_name, RETURNURL, String.ValueOf(ApexPages.currentPage().getParameters().get('remember_me')), '', issuerCode, planCode);
                }
                else {
                    signature = getSHA256Signature(PAYFORT_REQUEST_PHRASE, PAYFORT_ACCESS_CODE, PAYFORT_MERCHANT_IDENTIFIER, merchant_reference, amount, currencyInPay, language, customer_email, customer_ip, token_name, RETURNURL, String.ValueOf(ApexPages.currentPage().getParameters().get('remember_me')), '', '', '');
                }

                
            }
            

            //calling PURCHASE callout
            purchaseCallout(String.ValueOf(ApexPages.currentPage().getParameters().get('remember_me')), cvv);
        }

        if(ApexPages.currentPage().getParameters().containsKey('command') && String.ValueOf(ApexPages.currentPage().getParameters().get('command')).equalsIgnoreCase('PURCHASE')) {

            //calling SHA-256 signature method for response received;
            Boolean isValid = ValidateSignatureForResponse(keyValuePairs, false, PAYFORT_RESPONSE_PHRASE); //ValidateSignatureForResponse is set to false;

            System.debug('isValid in Purchase: '+isValid);
            if(!isValid) {
                showFailPurchaseResult = true;
                errorMsg = 'Payfort signature mismatch in validation';

                typeOfUpdate = 'PURCHASE_FAIL';
                merchant_reference = ApexPages.currentPage().getParameters().get('merchant_reference');
            }


            if(String.ValueOf(ApexPages.currentPage().getParameters().get('response_message')).equalsIgnoreCase('Success')) {

                System.debug('In 2nd constructor - Purchase Success');
                showPurchaseResult = true;
                receiptName = String.valueOf(ApexPages.currentPage().getParameters().get('merchant_reference'));
                fortId = String.valueOf(ApexPages.currentPage().getParameters().get('fort_id'));    

                if(ApexPages.currentPage().getParameters().containsKey('remember_me') && String.ValueOf(ApexPages.currentPage().getParameters().get('remember_me')).equalsIgnoreCase('YES')) {

                    //String.isNotBlank(ApexPages.currentPage().getParameters().get('token_name'))

                    typeOfUpdate = 'PURCHASE_WITH_TOKEN';
                    token_name = String.valueOf(ApexPages.currentPage().getParameters().get('token_name'));
                    responseMessage = String.valueOf(ApexPages.currentPage().getParameters().get('response_message'));
                    statusCode = String.valueOf(ApexPages.currentPage().getParameters().get('status'));
                    cardNumber = String.valueOf(ApexPages.currentPage().getParameters().get('card_number'));
                    
                }
                else if( !ApexPages.currentPage().getParameters().containsKey('remember_me') || (String.ValueOf(ApexPages.currentPage().getParameters().get('remember_me')).equalsIgnoreCase('NO')) ) {

                    typeOfUpdate = 'PURCHASE_WITHOUT_TOKEN';
                    responseMessage = String.valueOf(ApexPages.currentPage().getParameters().get('response_message'));
                    statusCode = String.valueOf(ApexPages.currentPage().getParameters().get('status'));
                    
                }
            }
            else {
                showFailPurchaseResult = true;
                showPurchaseResult = false;
            }   

        }   
    }

    public static String getIPAddress() {
         
        String IPAddress = ApexPages.currentPage().getHeaders().get('True-Client-IP');
        //Get IP address when no caching (sandbox, dev, secure urls)
        if (String.isBlank(IPAddress)) {
            IPAddress = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
        }  
        if (String.isBlank(IPAddress)) {
            IPAddress = ApexPages.currentPage().getHeaders().get('X-Forwarded-For');
        } 
        return IPAddress;
    }

    private static boolean ValidateSignatureForResponse(List<String> keyValuePairs, boolean fromTokenization, String PAYFORT_RESPONSE_PHRASE ) {

        String signPattern ='';
        String signInResp;

        if(keyValuePairs.size() > 0) {
            signPattern += PAYFORT_RESPONSE_PHRASE; 
             for(String objStr : keyValuePairs) {
                if(!objStr.containsIgnoreCase('signature=')) {

                    System.debug('objStr: '+objStr);

                    if(fromTokenization && (objStr.containsIgnoreCase('amount=') || objStr.containsIgnoreCase('plan_code=') || objStr.containsIgnoreCase('issuer_code=') )) {

                        continue;
                    }

                    String decodedStr = EncodingUtil.urlDecode(objStr, 'UTF-8');
                    System.debug('decodedStr: '+decodedStr);
                    signPattern += decodedStr;
                }

                if(objStr.containsIgnoreCase('signature=')) {
                    signInResp = objStr.substringAfter('signature=');
                }
            }
            signPattern += PAYFORT_RESPONSE_PHRASE;
        }

        System.debug('signPattern generated : '+signPattern);
        String algorithmName = 'SHA-256';
        Blob hmacData = Crypto.generateDigest(algorithmName, Blob.valueOf(signPattern));
        System.debug('signature generated at our end :'+EncodingUtil.convertToHex(hmacData));
        System.debug('sign from payfort : '+signInResp);

        return EncodingUtil.convertToHex(hmacData) == signInResp ? true : false;

    }

    public Static String getSHA256Signature(String requestPhrase, String access_code, String merchant_identifier, String merchant_reference, String amount, String currencyInPay, String language, String customer_email, String customer_ip, String token_name, String return_url, String remember_me, String cvv, String issuerCode, String  planCode) {

        String signaturePattern;

        //remember me added
        if(remember_me == null) {
            remember_me = 'NO';
        }

        if(String.isNotBlank(cvv)) {

            //Here remember_me parameter is not included
            if(String.isNotBlank(issuerCode) && String.isNotBlank(planCode)) {
                signaturePattern = requestPhrase+'access_code='+access_code+'amount='+amount+'card_security_code='+cvv+'command=PURCHASE'+'currency='+currencyInPay+'customer_email='+customer_email+'customer_ip='+customer_ip+'installments=HOSTEDissuer_code='+issuerCode+'language='+language+'merchant_identifier='+merchant_identifier+'merchant_reference='+merchant_reference+'plan_code='+planCode+'return_url='+return_url+'token_name='+token_name+requestPhrase;

            }
            else{
                signaturePattern = requestPhrase+'access_code='+access_code+'amount='+amount+'card_security_code='+cvv+'command=PURCHASE'+'currency='+currencyInPay+'customer_email='+customer_email+'customer_ip='+customer_ip+'language='+language+'merchant_identifier='+merchant_identifier+'merchant_reference='+merchant_reference+'return_url='+return_url+'token_name='+token_name+requestPhrase;
            }
            
           
        }
        else{

            if(String.isNotBlank(issuerCode) && String.isNotBlank(planCode)) {
                signaturePattern = requestPhrase+'access_code='+access_code+'amount='+amount+'command=PURCHASE'+'currency='+currencyInPay+'customer_email='+customer_email+'customer_ip='+customer_ip+'installments=HOSTEDissuer_code='+issuerCode+'language='+language+'merchant_identifier='+merchant_identifier+'merchant_reference='+merchant_reference+'plan_code='+planCode+'remember_me='+remember_me+'return_url='+return_url+'token_name='+token_name+requestPhrase;
            }
            else {
                signaturePattern = requestPhrase+'access_code='+access_code+'amount='+amount+'command=PURCHASE'+'currency='+currencyInPay+'customer_email='+customer_email+'customer_ip='+customer_ip+'language='+language+'merchant_identifier='+merchant_identifier+'merchant_reference='+merchant_reference+'remember_me='+remember_me+'return_url='+return_url+'token_name='+token_name+requestPhrase;
            }

            
        }

        System.debug('Signature pattern created for SHA-256: '+signaturePattern);
        String algorithmName = 'SHA-256';
        Blob hmacData = Crypto.generateDigest(algorithmName, Blob.valueOf(signaturePattern));
        System.debug('SHA-256 :'+EncodingUtil.convertToHex(hmacData));
        return EncodingUtil.convertToHex(hmacData);
    }

    public void purchaseCallout(String remember_me, String cvv) {

        //create json
        if(remember_me == null) {
            remember_me = 'NO';
        }

        System.debug('cvv in purchaseCallout: '+cvv);
        JSONGenerator gen = JSON.createGenerator(true);    
            gen.writeStartObject();      
            gen.writeStringField('command', 'PURCHASE');
            gen.writeStringField('access_code',PAYFORT_ACCESS_CODE);
            gen.writeStringField('merchant_identifier',PAYFORT_MERCHANT_IDENTIFIER);
            gen.writeStringField('merchant_reference',merchant_reference);
            gen.writeStringField('amount',amount);
            gen.writeStringField('currency',currencyInPay);
            gen.writeStringField('language',language);
            gen.writeStringField('customer_email',customer_email);
            gen.writeStringField('customer_ip',customer_ip);
            gen.writeStringField('token_name',token_name);
            gen.writeStringField('signature',signature);
            gen.writeStringField('return_url',RETURNURL);
            if(String.isBlank(cvv)) {
                gen.writeStringField('remember_me',remember_me);
            }     
            if(String.isNotBlank(cvv)) {
                gen.writeStringField('card_security_code',cvv);
            }
            //for Installment
            if(String.isNotBlank(issuerCode) && String.isNotBlank(planCode)) {
                gen.writeStringField('installments','HOSTED');
                gen.writeStringField('issuer_code',issuerCode);
                gen.writeStringField('plan_code',planCode);
            }
            gen.writeEndObject();    
            String jsonS = gen.getAsString();
        System.debug('jsonMaterials'+jsonS);

        HttpRequest req = new HttpRequest();
        req.setEndpoint(PURCHASE_ENDPOINT);
        System.debug('getEndpoint: '+req.getEndpoint());
        req.setHeader('Content-Type', 'application/json');
        req.setMethod('POST');
        req.setBody(jsonS);
        System.debug('req : '+req);
        System.debug('req.getBody() : '+req.getBody());
        HttpResponse response = new Http().send(req);

        System.debug('Status Code = ' + response.getStatusCode());
        System.debug('body = ' + response.getBody());
        System.debug('toString body: '+response.toString());

        Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
        

        //for validating the signature;
        Map<String, String> mainMap = new Map<String, String>();
        List<String> lstKeyValues = new List<String>();

        for(String objStr : m.keySet()) {
            mainMap.put(objStr,String.valueOf(m.get(objStr)));

            String objKeyValue = objStr+'='+String.valueOf(m.get(objStr));
            System.debug('objStr: '+objKeyValue);
            lstKeyValues.add(objKeyValue);
        }

        System.debug('mainMap: '+mainMap);
        System.debug('lstKeyValues: '+lstKeyValues);
        if(lstKeyValues.size() > 0) {
            lstKeyValues.sort();
            boolean isValid = ValidateSignatureForResponse(lstKeyValues, false, PAYFORT_RESPONSE_PHRASE); 
            System.debug('isValid in Purchase main callout: ' + isValid);

            if(!isValid) {
                showFailPurchaseResult = true;
                errorMsg = 'Payfort signature mismatch in validation';
                typeOfUpdate = 'PURCHASE_FAIL';
                merchant_reference = ApexPages.currentPage().getParameters().get('merchant_reference');
            }
        }

        displayMsg = String.valueOf(m.get('response_message'));

        if(String.isNotBlank(String.valueOf(m.get('3ds_url')))) {
            //showPurchaseResult = true;
            ds_url = String.valueOf(m.get('3ds_url'));
            
        }
        else {
            System.debug('error occured');
            showFailPurchaseResult = true;
            showPurchaseResult = false;
            merchant_reference = String.valueOf(m.get('merchant_reference'));
            errorMsg = String.valueOf(m.get('response_message'));
            statusCode = String.valueOf(m.get('status'));
            typeOfUpdate = 'NO_3DS';
        }

    }

    public PageReference redirectTo3dsURL() {
        if(ds_url != null && ds_url != '') {
            PageReference pgRef = new PageReference(ds_url);
            pgRef.setRedirect(true);

            return pgRef;
        }
        else {
            //To Update Receipt & Account;
            if(String.isNotBlank(typeOfUpdate)) {
                System.debug('typeOfUpdate: '+typeOfUpdate);

                if(typeOfUpdate == 'PURCHASE_FAIL') {
                    UpdateReceiptAndAccount('','',merchant_reference, '', errorMsg, statusCode, '');
                }
                else if(typeOfUpdate == 'PURCHASE_WITH_TOKEN') {
                    
                    UpdateReceiptAndAccount(selectedAccountId, token_name, receiptName, fortId, responseMessage, statusCode, cardNumber);
                }
                else if(typeOfUpdate == 'PURCHASE_WITHOUT_TOKEN') {
                   
                    UpdateReceiptAndAccount('','', receiptName, fortId, responseMessage, statusCode, '');
                }
                else if(typeOfUpdate == 'NO_3DS') {
                    UpdateReceiptAndAccount('','',merchant_reference, '', errorMsg, statusCode, '');
                }
            }

            return null;
        }

    }

    public void UpdateReceiptAndAccount(String accountId , String tokenName, String receiptName, String fortId, String response_msg, String status, String cardNumber) {
        
        if(accountId != '') {
            List<Account> lstAcc = [SELECT id
                                         , Payfort_Token_Name__c
                                         , Payfort_Card_Number__c
                                    FROM Account
                                    WHERE Id =: accountId];
            System.debug('lstAcc: '+lstAcc);

            if(lstAcc.size() > 0) {
                lstAcc[0].Payfort_Token_Name__c = tokenName;
                lstAcc[0].Payfort_Card_Number__c = cardNumber;

                update lstAcc;
            }   
        }

        if(receiptName != '') {
            List<Receipt__c> lstReceipt = [SELECT id
                                                , Payfort_FortID__c
                                                , Payfort_Issuer_Code__c
                                                , Payfort_No_of_Installments__c
                                                , Payfort_Payment_type__c
                                                , Payfort_Plan_Code__c
                                                , Payment_Status__c
                                                , PG_Error_Code__c
                                                , Card_Type__c
                                                , PG_Error_Message__c   
                                                FROM Receipt__c
                                                WHERE Name =: receiptName];

            if(lstReceipt.size() > 0) {
                System.debug('Updating Receipt: '+lstReceipt[0]);

                lstReceipt[0].Payfort_FortID__c = fortId;
                lstReceipt[0].PG_Error_Code__c = status;
                
                if(response_msg.equalsIgnoreCase('Success')) {
                    //for successfull transaction
                    lstReceipt[0].Payment_Status__c = response_msg;
                }
                else {
                    //for failed transaction
                    lstReceipt[0].Payment_Status__c = 'Failed';
                    lstReceipt[0].PG_Error_Message__c = response_msg;
                }
                
                lstReceipt[0].Card_Type__c = Apexpages.currentpage().getParameters().get('payment_option');

                //if installments
                if(Apexpages.currentpage().getParameters().containsKey('installments')) {
                    lstReceipt[0].Payfort_Payment_type__c = 'Installment';
                    lstReceipt[0].Payfort_No_of_Installments__c = Decimal.valueOf(Apexpages.currentpage().getParameters().get('number_of_installments'));
                    lstReceipt[0].Payfort_Plan_Code__c = Apexpages.currentpage().getParameters().get('plan_code');
                    lstReceipt[0].Payfort_Issuer_Code__c = Apexpages.currentpage().getParameters().get('issuer_code');
                }
                else {
                    lstReceipt[0].Payfort_Payment_type__c = 'One-Time Payment';
                }
                System.debug('Receipt for update: '+lstReceipt);
                update lstReceipt;

                //if('Success'.equalsIgnoreCase(lstReceipt[0].Payment_Status__c)) {
                    
                //    // Commented the IPMS Receipt creation 

                //    ////Creating receipt in IPMS
                //    //List<id> lstRecieptId= new List<id>();
                //    //lstRecieptId.add(lstReceipt[0].id);
                //    //createReceipt(lstRecieptId);
                //}
                //else {
                //    System.debug('deleting the receipt as failed: '+lstReceipt[0]);
                //    lstReceipt[0].PG_Error_Message__c = 
                //    //delete lstReceipt[0];
                //}
            }
        }
        
    }

    // Commented the IPMS Receipt creation 

    //@future(callout=true)
    //public static void createReceipt(List<Id> lstRecieptId) {
    //    if (lstRecieptId == NULL) {
    //        return;
    //    }

    //    IpmsSettings settings = getIPMSsetting('IPMS_webservice');
    //    HTTPRequest req = new HTTPRequest();
    //    req.setMethod('POST');

    //    String reqXML = prepareReceipt(lstRecieptId, settings);
    //    System.debug('>>>>>Request Body reqXML>>>>>>'+ reqXML);
    //    reqXML = reqXML.replaceAll('null', '');
    //    reqXML = reqXML.trim();
    //    req.setbody(reqXML);
    //    req.setEndpoint(settings.endurl);
    //    req.setHeader('Content-Type','text/xml');
    //    req.setTimeout(120 * 1000);
    //    HTTP http = new HTTP();
    //    //if(!Test.isrunningTest()){
    //        try{
    //            HTTPResponse res = http.send(req);
    //            System.debug('IPMS Response = ' + res.getbody());

    //            CustomerPaymentStatusUpdate.parseReceiptResponse(res.getBody());
    //        }
    //        catch(Exception ex) {
    //            System.debug('Callout error: '+ ex);
    //            Log__c objLog = new Log__c();
    //            objLog.Description__c ='Ids=='+lstRecieptId+'-Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage();
    //            objLog.Type__c = 'Webservice Callout For Receipt Creation';
    //            insert objLog;
    //        }
    //    //}

    //}

    //public static String prepareReceipt(List<id> lstRecieptId, IpmsSettings settings) {
    //    String recQuery = 'SELECT ';
    //    for (Schema.SObjectField fieldToken : Schema.SObjectType.Receipt__c.fields.getMap().values()) {
    //        Schema.DescribeFieldResult field = fieldToken.getDescribe();
    //        if (field.isAccessible()) {
    //            recQuery += field.getName() + ',';
    //        }
    //    }
    //    recQuery = recQuery.removeEnd(',') + ' FROM Receipt__c WHERE Id IN :lstRecieptId';

    //    List<Receipt__c> rcptlist= Database.query(recQuery);

    //    String reqno='123456';
    //    if(rcptlist.size()>0)
    //        reqno =rcptlist[0].name+'-'+GetFormattedDateTime(system.now());
    //    system.debug('REQ NO==='+reqno);
    //    // String currdate= system.now
    //    String body = '';
    //    body += preparesoapHeader(settings);
    //    body +='<soapenv:Body>';
    //    body +='<proc:InputParameters>';
    //    //body+='<!--Optional:-->';
    //    body +='<proc:P_REQUEST_NUMBER>'+reqno+'</proc:P_REQUEST_NUMBER>';
    //    //body+='<!--Optional:-->';
    //    body +='<proc:P_SOURCE_SYSTEM>SFDC</proc:P_SOURCE_SYSTEM>';
    //    //body+='<!--Optional:-->';
    //    body +='<proc:P_REQUEST_NAME>CREATE RECEIPT</proc:P_REQUEST_NAME>';
    //    //body+='<!--Optional:-->';
    //    body +='<proc:P_REQUEST_MESSAGE>';
    //    //body+='<!--Zero or more repetitions:-->';
    //    for (Receipt__c rec: rcptlist) {
    //        body+='<proc:P_REQUEST_MESSAGE_ITEM>';
    //        //body+='<!--Optional:SFDC ID-->';
    //        body+='<proc:PARAM_ID>'+rec.Id+'</proc:PARAM_ID>';
    //        //body+='<!--Optional:RECEIPT_NUMBER-->';
    //        body+='<proc:ATTRIBUTE1>'+rec.Name+'</proc:ATTRIBUTE1>';
    //        //body+='<!--Optional:REG_ID-->';
    //        body+='<proc:ATTRIBUTE2>'+rec.Registration_Id__c+'</proc:ATTRIBUTE2>';
    //        //body+='<!--Optional:CURRENCY_CODE-->';
    //        body+='<proc:ATTRIBUTE3>'+rec.BU_Currency__c+'</proc:ATTRIBUTE3>'; //AED
    //        //body+='<!--Optional:AMOUNT-->';
    //        body+='<proc:ATTRIBUTE4>'+rec.IPMS_Token_Amount__c+'</proc:ATTRIBUTE4>';
    //        //body+='<!--Optional:RECEIPT_METHOD-->';
    //        body+='<proc:ATTRIBUTE5>Direct Receipt in the Bank</proc:ATTRIBUTE5>';
    //        //body+='<!--Optional:BANK_ACCOUNT_NUM-->';
    //        String banknum ='';
    //        if(rec.Unit_Country__c == 'Saudi Arabia')
    //            banknum=Label.Saudi_Bank_Account_No;
    //        else
    //            banknum='Master/Visa/Amex/Diners -CC';
    //        body+='<proc:ATTRIBUTE6>'+banknum+'</proc:ATTRIBUTE6>';
    //        body+='<proc:ATTRIBUTE7>TOKEN</proc:ATTRIBUTE7>';
    //        body+='</proc:P_REQUEST_MESSAGE_ITEM>';
    //    }
    //    body+='</proc:P_REQUEST_MESSAGE>';
    //    body+='</proc:InputParameters>';
    //    body+='</soapenv:Body>';
    //    body+='</soapenv:Envelope>';


    //    return body.trim();
    //}

    //public static string GetFormattedDateTime(DateTime dt){
    //    String yyyy=string.valueof(dt.year());
    //    String mm=string.valueof(dt.month());
    //    String dd=string.valueof(dt.day());
    //    String hh=string.valueof(dt.hour());
    //    String mi=string.valueof(dt.minute());
    //    String ss=string.valueof(dt.second());
    //    String ms=string.valueof(dt.millisecond());

    //    String formatdate=yyyy+mm+dd+hh+mi+ss+ms;
    //    return formatdate;
    //}

    //public static String preparesoapHeader(IpmsSettings settings){
    //    String body = '';
    //    body+='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xxdc="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/" xmlns:proc="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/process/">';
    //    body+='<soapenv:Header>';
    //    body+='<xxdc:SOAHeader>';
    //    //body+='<!--Optional:-->';
    //    body+='<xxdc:Responsibility>ONT_ICP_SUPER_USER</xxdc:Responsibility>';
    //    //body+='<!--Optional:-->';
    //    body+='<xxdc:RespApplication>ONT</xxdc:RespApplication>';
    //    //body+='<!--Optional:-->';
    //    body+='<xxdc:SecurityGroup>standard</xxdc:SecurityGroup>';
    //    //body+='<!--Optional:-->';
    //    body+='<xxdc:NLSLanguage>american</xxdc:NLSLanguage>';
    //    //body+='<!--Optional:-->';
    //    body+='<xxdc:Org_Id/>';
    //    body+='</xxdc:SOAHeader>';
    //    body+='<wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">';
    //    body+='<wsse:UsernameToken>';
    //    body+='<wsse:Username>' + settings.usrname + '</wsse:Username>';
    //    body+='<wsse:Password>' + settings.pwd + '</wsse:Password>';
    //    body+='</wsse:UsernameToken>';
    //    body+='</wsse:Security>';
    //    body+='</soapenv:Header>';
    //    return body;
    //}

    //public static IpmsSettings getIPMSsetting(String servicename) {
    //    IPMS_Integration_Settings__mdt ipms= [select id,Endpoint_URL__c,Password__c,  Username__c from IPMS_Integration_Settings__mdt where DeveloperName = :servicename LIMIT 1];
    //    return new IpmsSettings(ipms.Endpoint_URL__c, ipms.Username__c, PasswordCryptoGraphy.DecryptPassword(ipms.Password__c));

    //}

    //public class IpmsSettings {
    //    public String endurl;
    //    public String usrname;
    //    public String pwd;

    //    public IpmsSettings(String endurl, String usrname, String pwd) {
    //        this.endurl     = endurl;
    //        this.usrname    = usrname;
    //        this.pwd        = pwd;
    //    }
    //}

}