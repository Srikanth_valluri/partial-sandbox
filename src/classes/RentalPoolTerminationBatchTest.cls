/*
* Description - Test class for 'RentalPoolTerminationBatch'
*
* Version            Date            Author            Description
* 1.0              06/02/2018       Ashish Agarwal    Initial Draft
*/
@isTest
private class RentalPoolTerminationBatchTest {

    static testMethod void myUnitTest() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
           objUnit.Rental_Pool__c= true; 
        }
        insert lstBookingUnits;
        
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId();
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id; 
            objCase.Rental_Pool_Termination_Status__c = 'Letter of Termination Dispatched';
            objCase.Status = 'Closed';
            objCase.Final_Termination_Date__c = system.today();
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        test.startTest();
            Database.executeBatch( new RentalPoolTerminationBatch() );
        test.stopTest();
        
    }
}