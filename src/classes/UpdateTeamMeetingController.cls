public with sharing class UpdateTeamMeetingController {
	@AuraEnabled
    public static string getMeetingType(Id taskId){
        return [select id, Activity_Type_3__c from Task where id =:taskId limit 1].Activity_Type_3__c;
    }	
}