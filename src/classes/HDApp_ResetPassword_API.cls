/**********************************************************************************************************************
Description: This API is used for sending OTP to customer before resetting password from Damac living app & also 
             verfying the OTP entere.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   12-01-2020      | Shubham Suryawanshi | Created initial draft
***********************************************************************************************************************/
@RestResource(urlMapping='/resetPassword/V.1.0/*')
global class HDApp_ResetPassword_API {

    /*Do not move in constant class*/
    public static final Integer GUID_CHECK_TIMEOUT = Integer.valueOf(label.Timeout_duration_for_Rest_Password_GUID);
    public static final String SF_ERROR_REPEATED_PWD = 'UNKNOWN_EXCEPTION: invalid repeated password';
    public static final String SF_ERROR_INVALID_PWD = 'INVALID_NEW_PASSWORD: Your password must include letters and numbers';
    /***********************************************************************************************
    Method Name : resetpassword
    Description :This method is used setting new password
    Parameter(s): None
    Return Type : FinalReturnWrapper (Wrapper)
    ************************************************************************************************/
    @HttpPost    
    global static FinalReturnWrapper resetpassword() {

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        HDApp_Utility.cls_meta_data objMeta = new HDApp_Utility.cls_meta_data();
        cls_data objData = new cls_data();
        
        RestRequest r = RestContext.request;
        System.debug('Request params:' + r.params);
        if(!r.requestURI.equalsIgnoreCase('/resetPassword/v.1.0')) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg, HDApp_Constants.INVALID_RESET_PWD_ENDPOINT, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        if(!r.params.containsKey('username')) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg,HDApp_Constants.NO_USERNAME_ATTR, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('username') && String.isBlank(r.params.get('username'))) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg,HDApp_Constants.NO_USERNAME_PASSED,6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        if(!r.params.containsKey('guid')) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg, HDApp_Constants.NO_GUID_ATTR, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('guid') && String.isBlank(r.params.get('guid'))) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg, HDApp_Constants.NO_GUID_PASSED, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        if(!r.params.containsKey('newPassword')) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg, HDApp_Constants.NO_PASSWORD_ATTR, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('newPassword') && String.isBlank(r.params.get('newPassword'))) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.NO_PASSWORD_FOUND, HDApp_Constants.NO_PASSWORD_FOUND, 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        System.debug('newPassword: '+r.params.get('newPassword') + '*');
        System.debug('username: ' + r.params.get('username'));
        List<User> userList = HDApp_Utility.getUserDetailsFromUsername(r.params.get('username'));
        
        if( userList.isEmpty() || userList.size() < 1 ) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,HDApp_Constants.INVALID_USERNAME,6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        List<OTP__c> otpRecordList = HdApp_ResetPasswordOTPProcess_API.getOTPfromGUID(r.params.get('guid'), userList[0].Account.id, true, false);
        if( otpRecordList.isEmpty() || otpRecordList.size() < 1 ) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,HDApp_Constants.INVALID_GUID,6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        //GUID will be valid only for 10 mins from is createdDatetime
        Boolean isGUIDValid = HdApp_ResetPasswordOTPProcess_API.checkOTPValidity(otpRecordList[0], GUID_CHECK_TIMEOUT);
        if(!isGUIDValid) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.RESET_PASSWORD_TIMEOUT, HDApp_Constants.RESET_PASSWORD_TIMEOUT, 10);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        String isPasswordSet = setNewPasswordforUser(userList[0], r.params.get('newPassword'));
        System.debug('isPasswordSet: ' + isPasswordSet);

        Integer statusCode = isPasswordSet.equalsIgnoreCase('success') ? 1 : isPasswordSet.equalsIgnoreCase(HDApp_Constants.REPEATED_PWD_MSG) || isPasswordSet.equalsIgnoreCase(HDApp_Constants.INAVALID_PWD_MSG)  ? 11 : 2;
        String finalResponseMsg = isPasswordSet.equalsIgnoreCase('success') ? HDApp_Constants.REST_PASSWORD_SUCCESS : isPasswordSet;
        if(isPasswordSet.equalsIgnoreCase('success')) {
            otpRecordList[0].isExpired__c = true;
            update otpRecordList;
        }
        objMeta = HDApp_Utility.ReturnMetaResponse(finalResponseMsg, isPasswordSet, statusCode);
        returnResponse.meta_data = objMeta;
        return returnResponse;
        
    }

    public static String setNewPasswordforUser(User usr, String newPassword) {
        String returnMsg = '';
        try {
            // To set the new password for the user
            if (!Test.isRunningTest())
                System.setPassword (usr.Id, newPassword.trim());

                if(!usr.is_Reset_Password_Initiated__c) {
                    usr.is_Reset_Password_Initiated__c = true;    //used post account unblocking due to invalid attempts
                    usr.Login_Invalid_Attempt_Timestamp__c = null;
                    usr.Community_User_Invalid_Login_Attempts__c = 0;
                    update usr;
                }
                returnMsg = 'success';
        } catch (Exception ex) {
            if( ex.getMessage().equalsIgnoreCase(SF_ERROR_REPEATED_PWD) ) {
                returnMsg = HDApp_Constants.REPEATED_PWD_MSG;
            }
            else if( ex.getMessage().equalsIgnoreCase(SF_ERROR_INVALID_PWD) ) {
                returnMsg = HDApp_Constants.INAVALID_PWD_MSG;
            }
            else {
                returnMsg = ex.getMessage();
            }
        }
        return returnMsg;
    }

    /***********************************************************************************************
    Description : This is return response wrapper class declaration
    ************************************************************************************************/    
    global class FinalReturnWrapper {
        public HDApp_Utility.cls_meta_data meta_data;
        public cls_data data;
    }

    public class cls_data {}
}