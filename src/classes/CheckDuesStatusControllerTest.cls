/*-------------------------------------------------------------------------------------------------
Description: Test class for CheckDuesStatusController

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 11-12-2017       | Lochana Rajput   | 1. Added test method to test CheckDuesStatusController
   =============================================================================================================================
*/
@isTest
private class CheckDuesStatusControllerTest {

    @testSetup
    static void createSetupDate() {
        Id callingListRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Bounced Cheque').getRecordTypeId();
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bounced Cheque SR').getRecordTypeId();
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<SR_Booking_Unit__c> lstSRBookingUnits = new List<SR_Booking_Unit__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRecordTypeId);
        insert objCase;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 5);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 20);
        insert lstBookingUnits;
        lstSRBookingUnits = TestDataFactory_CRM.createSRBookingUnis(objCase.Id,lstBookingUnits);
        insert lstSRBookingUnits;
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
        settingLst2.add(newSetting1);
        insert settingLst2;
        Calling_List__c objCallList = new Calling_List__c();
        objCallList.RecordTypeId = callingListRecordTypeId;
        objCallList.Case__c = objCase.Id;
        objCallList.Account__c = objAcc.Id;
        objCallList.Calling_List_Type__c = 'BC Calling';
        objCallList.Calling_List_Status__c = 'New';
        objCallList.Call_Back_Date__c = system.now();
        objCallList.Customer_Flag__c = true;
        insert objCallList;
    }

    @isTest static void test_checkStatusFunctionality() {
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Test.startTest();
        Calling_List__c objCallList = [SELECT Id FROM Calling_List__c LIMIT 1];
        PageReference pageRef = Page.CheckDuesStatus;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(objCallList);
        System.currentPageReference().getParameters().put('id', objCallList.Id);
        CheckDuesStatusController  controller = new CheckDuesStatusController(sc);
        Pagereference newPg = controller.updateCallingList();
        System.assertEquals('/' + objCallList.Id, String.valueOf(newPg.getURL()));
        objCallList = [SELECT Id,Calling_List_Status__c, Call_Outcome__c FROM Calling_List__c LIMIT 1];
        System.assertEquals('Paid', objCallList.Call_Outcome__c);
        System.assertEquals('Closed', objCallList.Calling_List_Status__c);
        Test.stopTest();
    }
}