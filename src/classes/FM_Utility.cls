/**
 * @File Name          : FM_Utility.cls
 * @Description        :
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/14/2019, 3:09:40 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/7/2019, 5:45:39 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
global without sharing class FM_Utility {

    public static String populateApprovingAuthorities(Boolean isTenant, String requestType) {
        List<FM_Process__mdt> lstMDt = new List<FM_Process__mdt>();
        String query = 'SELECT MasterLabel,Approver_Roles__c,FM_Portal_Actor__r.MasterLabel,Minimum_Outstanding_Charge__c ';
        query += 'FROM FM_Process__mdt ';
        query += 'WHERE MasterLabel =: requestType ';
        query += 'AND FM_Portal_Actor__r.MasterLabel = ';
        query += isTenant ? '\'Tenant\'' :'\'Owner\'';

        lstMDt = Database.query(query);
        System.debug('==lstMDt===' + lstMDt);
        if(lstMDt.size() > 0) {
            return lstMDt[0].Approver_Roles__c;
        }
        return '';
    }
    //Used in 'FM Move out' process builder
    @InvocableMethod
    public static void updateTenantHistory(List<FM_Case__c> lstCases) {
        System.debug('==lstCases===' + lstCases);
        try {
            //Get Tenant history record
            List<Tenant_History__c> lstTH = new List<Tenant_History__c>();
            lstTH = [SELECT Id from Tenant_History__c where Booking_Unit__c =: lstCases[0].Booking_Unit__c LIMIT 1];
            if(lstTH.size() > 0) {
                lstTH[0].Move_out_Date__c = lstCases[0].Actual_move_out_date__c;
                lstTH[0].Is_Active__c = false;
            }
            update lstTH[0];
        }
        catch(Exception excp) {
            System.debug('==excp===' + excp);
        }

    }
    public static String extractName( String strName ) {
        try {
            if(String.isNotBlank(strName)) {
                return strName.substring( strName.lastIndexOf('\\')+1 ) ;
            }
        }
        catch(System.Exception excp) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
        return '';
    }
    public static Blob extractBody( String strBody ) {
        try {
            if(String.isNotBlank(strBody)) {
                strBody = EncodingUtil.base64Decode( strBody ).toString();
                return EncodingUtil.base64Decode( strBody.substring( strBody.lastIndexOf(',')+1 ) );
            }
        }
        catch(System.Exception excp) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
        return NULL;
    }
    /*public static Task createTask(String subject, String assignedToId, String relatedTo,
        String priority, String status) {
        Task objTask = new Task();
        objTask.Subject = subject;
        objTask.OwnerId = assignedToId;
        objTask.WhatId = relatedTo;
        objTask.Status = status;
        objTask.Priority = priority;
        return objTask;
    }*/

    //Method for retrieving a list of approvers from Metadata
    public static list<FM_Approver__mdt> fetchApprovers( String strProcessName, String strProjCity ) {
        if( String.isNotBlank( strProcessName ) ) {
            Set<String> citySet = new Set<String>();
            citySet.add('All');
            if(String.isNotBlank(strProjCity)) {
                citySet.add(strProjCity);
            }
            return [ SELECT Id
                          , Role__c
                          , Level__c
                          , FM_Process__r.Minimum_Outstanding_Charge__c
                       FROM FM_Approver__mdt
                      WHERE FM_Process__r.DeveloperName = :strProcessName
                        AND Is_Active__c = true
                        AND City__c IN :citySet ];
        }
        return new list<FM_Approver__mdt>();
    }

    //Method for getting the Unit Details.
    public static Booking_Unit__c getUnitDetails( String strUnitId ) {
        List<Booking_Unit__c> lstBookingUnit = [
            SELECT  Id, Owner__c, Owner__r.Name, Booking__c, Booking__r.Account__c, Booking__r.Account__r.Name
                    , Inventory__r.Building_Location__c, Tenant__c, Tenant__r.Name, Resident__c , Resident__r.Name
                    , Unit_Name__c, Registration_Id__c, Property_Name__c, DEWA_Utility_Number__c
                    , Inventory__r.Master_Community_EN__c, Property_City__c, No_of_parking__c, Inventory__r.Property_Code__c
                    , JOPD_Area__c
            FROM    Booking_Unit__c
            WHERE   Id = :strUnitId
        ];
        if(!lstBookingUnit.isEmpty()){
            return  lstBookingUnit[0];
        }
        return new Booking_Unit__c();
    }

    //Method for getting the docs list based on the SR Type
    public static list<FM_Documents__mdt> getDocumentsList( String strSRType, String strCity ) {
        set<String> setCities = new set<String>{ 'All' };
        if( String.isNotBlank( strCity ) ) {
            setCities.addAll( strCity.split(',') ) ;
        }
        System.debug('strSRType: '+strSRType);
        System.debug(' setCities : '+setCities);
        System.debug('Query Result : '+[ SELECT Document_Name__c
                 , Is_Fetched__c
                 , is_Contractor__c
                 , is_Consultant__c
                 , Is_Generated__c
                 , Is_Uploaded__c
                 , Submitted_By__c
                 , MasterLabel
                 , Type__c
                 , DeveloperName
                 , Mandatory__c
                 , Duration__c
                 , NOC_Type__c
                 , isMasterCommunity__c
                 FROM FM_Documents__mdt
                 WHERE Process__r.DeveloperName = :strSRType
                 //WHERE Process__r.DeveloperName = 'Request_for_Access_cards_T'
                 AND Is_Active__c = true
                 AND City__c IN :setCities ]);

        return  [ SELECT Document_Name__c
                 , Is_Fetched__c
                 , is_Contractor__c
                 , is_Consultant__c
                 , Is_Generated__c
                 , Is_Uploaded__c
                 , Submitted_By__c
                 , MasterLabel
                 , Type__c
                 , DeveloperName
                 , Mandatory__c
                 , Duration__c
                 , NOC_Type__c
                 , isMasterCommunity__c
                 FROM FM_Documents__mdt
                 WHERE Process__r.DeveloperName = :strSRType
                 //WHERE Process__r.DeveloperName = 'Request_for_Access_cards_T'
                 AND Is_Active__c = true
                 AND City__c IN :setCities ] ;
    }

    //Method for getting the case details.
    public static FM_Case__c getCaseDetails( String strCaseId ) {
        if( String.isNotBlank( strCaseId ) ) {
            List<FM_Case__c> lstCase = [
                SELECT    Id,OwnerId,Current_Approver__c,Is_Task_created_for_Damac_Hills__c,Approving_Authorities__c,Contact_Email__c,Location_of_Work__c,Duration_For_Gate_Pass__c
                        , Account__c, Booking_Unit__c, Company__c, Employing_Company__c, Consultant__c,Person_To_Collect__c,Permit_To_Work_For__c
                        , Contact_person__c, Contact_person_contractor__c, Contractor__c, Email__c
                        , Type_of_Alteration__c, Type__c, Status__c, Request_Type__c,Service_Charge_Enquiry_Type__c
                        , Passport_Number__c, Office_tel_contractor__c, Office_tel__c, Mobile_no__c,Vehicle_Details__c
                        , Mobile_no_contractor__c, Description__c, Unit_Name__c, Request_Type_DeveloperName__c
                        , booking_unit__r.Property_Name__c, Booking_Unit__r.Property_City__c, Payment_Date__c, Payment_Mode__c, Swift_Code__c
                        , Bank_Name__c, Currency__c, Payment_Allocation_Details__c, Total_Amount__c,Number_Of_Gate_Pass_Issued__c
                        , Sender_Name__c, First_Name__c, Last_Name__c, Gender__c,Manager_Comments__c,Tech_Team_Comments__c
                        , Ejari_Number__c, Nationality__c, No_of_Adults__c, No_of_Children__c,CRE_Comments__c
                        , Start_Date__c, End_Date__c, Name, DEWA_Number__c,Access_card_required__c,Move_in_move_out_type__c
                        , Short_Term_Lease__c, Outstanding_service_charges__c, Tenant_Email__c, New_CR__c,Approval_Status__c
                        , Issue_Date__c, Submitted__c, Address__c, Address_Arabic_1__c,Stop_Water_Supply__c,Restore_Water_Supply__c
                        , Address_2__c, Address_Arabic_2__c, Address_3__c, Address_Arabic_3__c
                        , Address_4__c, Address_Arabic_4__c, Subtype__c,Parent_Case__c,Mobile_Country_Code__c
                        , Date_of_Birth__c, Emirates_Id__c, People_of_Determination__c, Email_2__c, Email_3__c
                        , Security_Deposit__c, Cheque_Number__c, SD_Cheque_Status__c, Remarks__c,Type_of_Drawings__c
                        , Temporary_power_water_connection_Amount__c, Security_Deposit_Cheque_Amount__c, Garbage_disposal_Amount__c
                        , Email_4__c, Email_5__c, Mobile_Country_Code_2__c, Mobile_Country_Code_3__c, Mobile_Country_Code_4__c
                        , Mobile_Country_Code_5__c, Mobile_Phone_2__c, Mobile_Phone_3__c, Mobile_Phone_4__c, Mobile_Phone_5__c
                        , Access_card_number__c, Current_Work_Status__c, Designation__c, Expiry_Date__c, CurrencyIsoCode
                        , Type_of_NOC__c, Tenant__c, Move_in_date__c, Move_in_Type__c, Annual_Contract_Amount__c
                        , Work_Permit_Type__c, Long_Duration_Gate_Pass_Fee_12_Months__c, Long_Duration_Gate_Pass_Fee_6_Months__c
                        , VerifiedEmail__c, VerifiedMobile__c,As_Built_Fee__c,Long_Duration_Gate_Pass_Fee__c,Short_Duration_Gate_Pass_Fee__c
                        , Amount_to_be_Waived__c, Penalty_Period__c, NOC_Fees__c, Type_of_Fitout_Work__c,Total_Paid_Amount__c,Total_Penalty_Amount__c
                        , Total_Penalty_Due__c, Origin__c, Admin__c, Company_Name__c, Contractor_Type__c, Number_of_Employees__c, IsOTPVerified__c, NOC_Fees_Including_VAT__c
                FROM      FM_Case__c
                WHERE     Id = :strCaseId];
            if (!lstCase.isEmpty()) {
                return lstCase[0];
            }
        }
        return new FM_Case__c();
    }

    //Method for getting details of multiple cases
    public static list<FM_Case__c> getCaseDetails( list<Id> lstCaseIds ) {
        if( lstCaseIds != NULL && !lstCaseIds.isEmpty() ) {
            return [ SELECT Id,OwnerId,Current_Approver__c,Is_Task_created_for_Damac_Hills__c,Approving_Authorities__c,Contact_Email__c,Location_of_Work__c,Duration_For_Gate_Pass__c
                        , Account__c, Booking_Unit__c, Company__c, Employing_Company__c, Consultant__c,Person_To_Collect__c,Permit_To_Work_For__c
                        , Contact_person__c, Contact_person_contractor__c, Contractor__c, Email__c
                        , Type_of_Alteration__c, Type__c, Status__c, Request_Type__c,Service_Charge_Enquiry_Type__c
                        , Passport_Number__c, Office_tel_contractor__c, Office_tel__c, Mobile_no__c,Vehicle_Details__c
                        , Mobile_no_contractor__c, Description__c, Unit_Name__c, Request_Type_DeveloperName__c
                        , booking_unit__r.Property_Name__c, Booking_Unit__r.Property_City__c, Payment_Date__c, Payment_Mode__c, Swift_Code__c
                        , Bank_Name__c, Currency__c, Payment_Allocation_Details__c, Total_Amount__c,Number_Of_Gate_Pass_Issued__c
                        , Sender_Name__c, First_Name__c, Last_Name__c, Gender__c,Manager_Comments__c,Tech_Team_Comments__c
                        , Ejari_Number__c, Nationality__c, No_of_Adults__c, No_of_Children__c,CRE_Comments__c
                        , Start_Date__c, End_Date__c, Name, DEWA_Number__c,Access_card_required__c,Move_in_move_out_type__c
                        , Short_Term_Lease__c, Outstanding_service_charges__c, Tenant_Email__c, New_CR__c,Approval_Status__c
                        , Issue_Date__c, Submitted__c, Address__c, Address_Arabic_1__c,Stop_Water_Supply__c,Restore_Water_Supply__c
                        , Address_2__c, Address_Arabic_2__c, Address_3__c, Address_Arabic_3__c
                        , Address_4__c, Address_Arabic_4__c, Subtype__c,Parent_Case__c,Mobile_Country_Code__c
                        , Date_of_Birth__c, Emirates_Id__c, People_of_Determination__c, Email_2__c, Email_3__c
                        , Security_Deposit__c, Cheque_Number__c, SD_Cheque_Status__c, Remarks__c,Type_of_Drawings__c
                        , Temporary_power_water_connection_Amount__c, Security_Deposit_Cheque_Amount__c, Garbage_disposal_Amount__c
                        , Email_4__c, Email_5__c, Mobile_Country_Code_2__c, Mobile_Country_Code_3__c, Mobile_Country_Code_4__c
                        , Mobile_Country_Code_5__c, Mobile_Phone_2__c, Mobile_Phone_3__c, Mobile_Phone_4__c, Mobile_Phone_5__c
                        , Access_card_number__c, Current_Work_Status__c, Designation__c, Expiry_Date__c, CurrencyIsoCode
                        , Type_of_NOC__c, Tenant__c, Move_in_date__c, Move_in_Type__c, Annual_Contract_Amount__c
                        , Work_Permit_Type__c, Long_Duration_Gate_Pass_Fee_12_Months__c, Long_Duration_Gate_Pass_Fee_6_Months__c
                        , VerifiedEmail__c, VerifiedMobile__c,As_Built_Fee__c,Long_Duration_Gate_Pass_Fee__c,Short_Duration_Gate_Pass_Fee__c
                        , Amount_to_be_Waived__c, Penalty_Period__c, NOC_Fees__c, Type_of_Fitout_Work__c,Total_Paid_Amount__c,Total_Penalty_Amount__c
                        , Total_Penalty_Due__c, Origin__c, Admin__c,Company_Name__c,Number_of_Employees__c, Contractor_Type__c
                       FROM FM_Case__c
                      WHERE Id IN :lstCaseIds ];
        }
        return new list<FM_Case__c>();
    }

    //Method for retrieving a list of documents related to the FM Case
    public static list<SR_Attachments__c> getDocumentsList( String strCaseId ) {
        if( String.isNotBlank( strCaseId ) ) {
            return [ SELECT Id
                          , Attachment_URL__c
                          , Account__c
                          , Booking_Unit__c
                          , Type__c
                          , Name
                       FROM SR_Attachments__c
                      WHERE FM_Case__c = :strCaseId ];
        }
        return new list<SR_Attachments__c>() ;
    }

    public static list<Location__c> getLocationDetails( set<String> strBuildingName ) {
        if( strBuildingName != NULL && !strBuildingName.isEmpty() ) {
            return [ SELECT Id
                          , Name
                          , Property_Name__r.Name
                          , Major_Work_Charges__c
                          , Minor_Work_Charges__c
                          , CurrencyIsoCode
                          , Artificial_Grass_Landscaping__c
                          , Installation_Charges__c
                          , Drawing_Review_Charges__c
                          , Maximum_Cap__c
                       FROM Location__c
                      WHERE Name IN :strBuildingName ];
        }
        return new list<Location__c>();
    }

    public static list<FM_User__c> getApprovingUsers( set<String> strBuildingName ) {
        if( strBuildingName != NULL && !strBuildingName.isEmpty() ) {
            return [ SELECT Id
                          , FM_User__c
                          , FM_Role__c
                          , Building__r.Name
                          , FM_User__r.Email
                          , FM_User__r.Name
                       FROM FM_User__c
                      WHERE Building__r.Name IN :strBuildingName ];
        }
        return new list<FM_User__c>();
    }

    //Method for creating an object of error log.
    public static Error_Log__c createErrorLog( String accountId,
                                               String unitId,
                                               Id fmCaseId,
                                               String strProcessName ,
                                               String strErrorMsg ) {
        Error_Log__c objError = new Error_Log__c();
        objError.Account__c = accountId ;
        objError.Booking_Unit__c = unitId ;
        objError.FM_Case__c = fmCaseId ;
        objError.Process_Name__c = strProcessName ;
        objError.Error_Details__c = strErrorMsg ;
        return objError;
    }

    public static String getObjectKeyPrefix(String objName){
            return Schema.getGlobalDescribe().get(objName).getDescribe().getKeyPrefix();
    }

    public static Map<Id, Map<String, Id>> getFmUsersByLocationAndRole(Set<Id> buildingIds) {
        Map<Id, Map<String, Id>> mapBuildingToRolesToUserId = new Map<Id, Map<String, Id>>();
        mapBuildingToRolesToUserId.put(NULL, new Map<String, Id>());

        for (Location__c building : [
            SELECT  Id
                    , Name
                    , Building_Name__c
                    , ( SELECT  Id
                                , FM_Role__c
                                , FM_User__c
                        FROM    FM_Users__r)
            FROM    Location__c
            WHERE   Id IN : buildingIds
        ]) {
            Map<String, Id> mapRolesToUserId = new Map<String, Id>();
            System.debug('building.FM_Users__r = ' + building.FM_Users__r);
            for (FM_User__c user : building.FM_Users__r) {
                mapRolesToUserId.put(user.FM_Role__c, user.FM_User__c);
            }
            mapBuildingToRolesToUserId.put(building.Id, mapRolesToUserId);
        }
        return mapBuildingToRolesToUserId;
    }

    public static Map<String,FM_User__c> getFMUserAdmin(Set<String> setLocationCode){
        System.debug('setLocationCode--------'+setLocationCode);
        Map<String,FM_User__c> maplocationCodeFMUser=new Map<String,FM_User__c>();
        for(FM_User__c instanceFMUser:[SELECT id
                                            , FM_User__c,FM_Role__c
                                            , Building__r.Name
                                      FROM    FM_User__c
                                      WHERE   Building__r.Name IN :setLocationCode
                                      AND     FM_Role__c ='FM Admin']){
          System.debug('instanceFMUser----'+instanceFMUser);
          maplocationCodeFMUser.put(instanceFMUser.Building__r.Name,instanceFMUser);
        }
        System.debug('maplocationCodeFMUser--------'+maplocationCodeFMUser);
      return maplocationCodeFMUser;
    }

    public static Map<String,FM_User__c> getFMUserCollection(Set<String> setLocationCode){
        System.debug('setLocationCode--------'+setLocationCode);
        Map<String,FM_User__c> maplocationCodeFMUser=new Map<String,FM_User__c>();
        for(FM_User__c instanceFMUser:[SELECT id
                                            , FM_User__c,FM_Role__c
                                            , Building__r.Name
                                      FROM    FM_User__c
                                      WHERE   Building__r.Name IN :setLocationCode
                                      AND     FM_Role__c = 'FM Collection']){
          System.debug('instanceFMUser----'+instanceFMUser);
          maplocationCodeFMUser.put(instanceFMUser.Building__r.Name,instanceFMUser);
        }
        System.debug('maplocationCodeFMUser--------'+maplocationCodeFMUser);
      return maplocationCodeFMUser;
    }

    public static UploadMultipleDocController.MultipleDocRequest makeWrapperObject( String strBody ,
                                                                                    String strType ,
                                                                                    String strName ,
                                                                                    String strCaseNumber ,
                                                                                    String strCounter ) {
        System.debug('*****strCaseNumber*****'+strCaseNumber);
        String strFileExtension = '';
        if( String.isNotBlank( strName ) && strName.lastIndexOf('.') != -1 ) {
            strFileExtension = strName.substring( strName.lastIndexOf('.')+1 , strName.length() );
        }

        UploadMultipleDocController.MultipleDocRequest objReqWrap = new UploadMultipleDocController.MultipleDocRequest();
        objReqWrap.Category = 'Document';
        objReqWrap.entityName = 'Damac Service Requests';
        objReqWrap.base64Binary = strBody ;
        objReqWrap.fileDescription = strType ;
        objReqWrap.fileId = strCaseNumber + '-' + System.currentTimeMillis() + '-' + strCounter + '-' + strFileExtension ;
        objReqWrap.fileName = strCaseNumber + '-' + System.currentTimeMillis() + '-' + + strCounter + '.' + strFileExtension ;
        objReqWrap.registrationId = strCaseNumber;
        objReqWrap.sourceFileName = objReqWrap.fileName;
        objReqWrap.sourceId = objReqWrap.fileId;
        system.debug('== objReqWrap =='+objReqWrap);
        return objReqWrap ;
    }

    //new method as per new FileName during upload
    /*public static UploadMultipleDocController.MultipleDocRequest makeWrapperObject(
      String strBody, String strName, String strCaseNumber, String strRegistrationId, String strPartyId, String strType,
      String strAccountLastName, String strCounter
    ) {
        System.debug('*****strCaseNumber*****'+strCaseNumber);
        String strFileExtension = '';
        if( String.isNotBlank( strName ) && strName.lastIndexOf('.') != -1 ) {
            strFileExtension = strName.substring( strName.lastIndexOf('.')+1 , strName.length() );
        }

        if(String.isNotBlank(strType) && strType.contains('.')) {
           strType = strType.subStringBefore('.');
        }

        UploadMultipleDocController.MultipleDocRequest objReqWrap = new UploadMultipleDocController.MultipleDocRequest();
        objReqWrap.Category = 'Document';
        objReqWrap.entityName = 'Damac Service Requests';
        objReqWrap.base64Binary = strBody ;
        objReqWrap.fileDescription = strType ;
        objReqWrap.fileId = strCaseNumber + '-' + System.currentTimeMillis() + '-' + strCounter + '-' + strFileExtension ;
        //objReqWrap.fileName = strCaseNumber + '-' + System.currentTimeMillis() + '-' + + strCounter + '.' + strFileExtension ;
        objReqWrap.fileName = 'C'+strCaseNumber+'_R'+strRegistrationId+'_P'+strPartyId+'_D'+strType+'_L'+strAccountLastName+'.'+strFileExtension;
        System.debug('objReqWrap.fileName :' + objReqWrap.fileName);
        objReqWrap.registrationId = strCaseNumber;
        objReqWrap.sourceFileName = objReqWrap.fileName;
        objReqWrap.sourceId = objReqWrap.fileId;
        system.debug('== objReqWrap =='+objReqWrap);
        return objReqWrap ;

    }*/

    public static list<FM_Case__c> fetchFmCaseDetails(set<Id> setCaseIds){
        return [Select Id
                     , Company__c
                     , Employing_Company__c
                     , Consultant__c
                     , Contact_person__c
                     , Contact_person_contractor__c
                     , Contractor__c
                     , Email__c
                     , Type_of_Alteration__c
                     , Type__c
                     , Status__c
                     , Tech_Team_Comments__c
                     , CRE_Comments__c
                     , Manager_Comments__c
                     , Request_Type__c
                     , Passport_Number__c
                     , Office_tel_contractor__c
                     , Office_tel__c
                     , Mobile_no__c
                     , Mobile_no_contractor__c
                     , Description__c
                     , Unit_Name__c
                     , Request_Type_DeveloperName__c
                     , Payment_Date__c
                     , Payment_Mode__c
                     , Swift_Code__c
                     , Bank_Name__c
                     , Currency__c
                     , Payment_Allocation_Details__c
                     , Total_Amount__c
                     , Sender_Name__c
                     , First_Name__c
                     , Last_Name__c
                     , Gender__c
                     , Ejari_Number__c
                     , Nationality__c
                     , No_of_Adults__c
                     , No_of_Children__c
                     , Start_Date__c
                     , End_Date__c, Name
                     , DEWA_Number__c
                     , Short_Term_Lease__c
                     , Outstanding_service_charges__c
                     , Tenant_Email__c
                     , New_CR__c
                     , Issue_Date__c
                     , Submitted__c
                     , Address__c
                     , Address_Arabic_1__c
                     , Address_2__c
                     , Address_Arabic_2__c
                     , Address_3__c
                     , Address_Arabic_3__c
                     , Address_4__c
                     , Address_Arabic_4__c
                     , Subtype__c
                     , Additional_Doc_File_URL__c
                     , Account__c
                     , Account__r.Party_Id__c
                     , Booking_Unit__c
                     , Booking_Unit__r.Registration_ID__c
                     , Booking_Unit__r.Unit_Name__c
                     , Booking_Unit__r.Property_City__c
                     , Mobile_Country_Code__c
                     , Date_of_Birth__c
                     , Service_Charge_Enquiry_Type__c
                     , Emirates_Id__c
                     , People_of_Determination__c
                     , Email_2__c
                     , Email_3__c
                     , Email_4__c
                     , Email_5__c
                     , Mobile_Country_Code_2__c
                     , Mobile_Country_Code_3__c
                     , Mobile_Country_Code_4__c
                     , Mobile_Country_Code_5__c
                     , Mobile_Phone_2__c
                     , Mobile_Phone_3__c
                     , Mobile_Phone_4__c
                     , Mobile_Phone_5__c
                     , Access_card_number__c
                     , Security_Deposit__c
                     , Cheque_Number__c
                     , SD_Cheque_Status__c
                     , Type_of_Drawings__c
                     , Remarks__c
                     , Current_Work_Status__c
                     , Designation__c
                     , Expiry_Date__c
                     , Location_of_Work__c
                     , Restore_Water_Supply__c
                     , Stop_Water_Supply__c
                     , Permit_To_Work_For__c
                     , CurrencyIsoCode
                     , Type_of_NOC__c
                     , Duration_For_Gate_Pass__c
                     , Move_in_date__c
                     , Temporary_power_water_connection_Amount__c
                     , Security_Deposit_Cheque_Amount__c
                     , Garbage_disposal_Amount__c
                     , As_Built_Fee__c
                     , Long_Duration_Gate_Pass_Fee__c
                     , Short_Duration_Gate_Pass_Fee__c
                     , Work_Permit_Type__c
                     , Long_Duration_Gate_Pass_Fee_12_Months__c
                     , Long_Duration_Gate_Pass_Fee_6_Months__c
                     , Total_Paid_Amount__c
                     , Total_Penalty_Amount__c
                     , Total_Penalty_Due__c
                     , Number_of_Employees__c
                     , Company_Name__c
                     , Contractor_Type__c
                From FM_Case__c
                Where Id IN : setCaseIds];
    }

    public static list<Task> fetchTaskDetails(set<Id> setTaskIds){
        return [Select Id
                     , WhatId
                     , CreatedDate
                     , ActivityDate
                     , Process_Name__c
                     , OwnerId
                     , Owner.Name
                     , Subject
                     , Status
                     , IPMS_Role__c
                     , Description
                     , Assigned_User__c
                From Task
                Where Id IN : setTaskIds];
    }

    public static list<SR_Booking_Unit__c> fetchSrBookings(set<Id> setFmCaseIds){
        return [Select Id
                     , FM_Case__c
                     , Booking_Unit__c
                     , Booking_Unit__r.Registration_Id__c
                     , Booking_Unit__r.Unit_Name__c
                From SR_Booking_Unit__c
                Where FM_Case__c IN : setFmCaseIds];
    }

    public static list<fm_case__c> getExistingFMCase( String recordTypeDeveloperName, String strAccountId, String strSelectedUnit ){
        return [select  id
                        , Description__c
                        , Person_To_Collect__c
                        , Contact_person__c
                        , Contact_person_contractor__c
                        , Mobile_no__c
                        , Mobile_no_contractor__c
                        , Contact_Email__c
                        , Office_tel__c
                        , Office_tel_contractor__c
                        , Permit_To_Work_For__c
                        , Account__c
                        , Booking_unit__c
                        , Duration_For_Gate_Pass__c
                        , Email__c
                        , Email_2__c
                        , Mobile_Country_Code__c
                        , Mobile_Country_Code_2__c
                        , Mobile_Country_Code_3__c
                        , Mobile_Country_Code_4__c
                        , Mobile_Country_Code_5__c
                        , Security_Deposit__c
                        , Bank_Name__c
                        , Cheque_Number__c
                        , SD_Cheque_Status__c
                        , Remarks__c
                        , Temporary_power_water_connection_Amount__c
                        , Security_Deposit_Cheque_Amount__c
                        , Garbage_disposal_Amount__c
                        , Parent_case__c
                        , As_Built_Fee__c
                        , Long_Duration_Gate_Pass_Fee__c
                        , Short_Duration_Gate_Pass_Fee__c
                        , Type_of_Drawings__c
                        , Work_Permit_Type__c
                        , Long_Duration_Gate_Pass_Fee_12_Months__c
                        , Long_Duration_Gate_Pass_Fee_6_Months__c
                        , Amount_to_be_Waived__c
                        , Penalty_Period__c
                        , Total_Penalty_Due__c
                        , Total_Penalty_Amount__c
                        , Total_Paid_Amount__c
                        , Company_Name__c
                        , Number_of_Employees__c
                        , Contractor_Type__c
                     from fm_case__c
                     where RecordType.DeveloperName = :recordTypeDeveloperName
                   AND Status__c = 'Closed'
                   AND Approval_Status__c = 'Approved'
                   AND Account__c = :strAccountId
                   AND Booking_Unit__c = :strSelectedUnit
                   order by createdDate DESC limit 1];
    }

    //method for getting FM Additional Details Records Related To FM Case
    public static List<FM_Additional_Detail__c> getRelatedObjectDetails(String fmCaseId,String recordTypeDeveloperName){
      return [select  id
                    , Penalty_Date__c
                    , Charged_Amount__c
                    , Paid_Amount__c
                    , Due_Remaining__c
                    , Transaction_Reference__c
                    , Amount_to_be_waived__c
                    , Percent_to_be_waived__c
                from  FM_Additional_Detail__c
                where Penalty_Case__c =: fmCaseId
              AND  RecordType.DeveloperName = :recordTypeDeveloperName ];
    }
}