public class DAMAC_EditDetailsController {

    public Email_Metrics__c metrics { get; set; }
    public DAMAC_EditDetailsController () {
        metrics = new Email_Metrics__c ();
    }
    
    public void checkConfirm () {
        String recordParameter = apexPages.currentPage().getParameters().get('id');
        if (recordParameter.contains (' ')) {
            recordParameter = recordParameter.replace (' ', '+');
        }
        Id recordId = UtilityHelperCls.decryptMobile (recordParameter);
        if (recordId != NULL) {
            metrics = [SELECT First_name__c, Confirm_Details__c, Last_Name__c, Title__c, Inquiry_Email__c, Inquiry_Phone__c, MObile_Country_code__c
                       FROM Email_Metrics__c WHERE ID =: recordId];
            if (metrics.Confirm_Details__c == false) {
                metrics.Confirm_Details__c = true;
                update metrics;
            }
        }
    }
    public pageReference checkForm () {
        
        String recordParameter = apexPages.currentPage().getParameters().get('id');
        if (recordParameter.contains (' ')) {
            recordParameter = recordParameter.replace (' ', '+');
        }
        Id recordId = UtilityHelperCls.decryptMobile (recordParameter);
        if (recordId != NULL) {
            metrics = [SELECT First_name__c, Confirm_Details__c, Last_Name__c, Title__c, Inquiry_Email__c, 
                       Inquiry_Phone__c, MObile_Country_code__c, SObject_ID__c
                       FROM Email_Metrics__c WHERE ID =: recordId];
            System.debug (metrics);
            if (metrics.Confirm_Details__c) {
                return new pageReference ('/Thankyoupage?id='+UtilityHelperCls.encryptMobile(recordId));
            } else {
                Map<String, Schema.SObjectType> m  = Schema.getGlobalDescribe() ;
                Schema.SObjectType s = m.get('Inquiry__c') ;
                Schema.DescribeSObjectResult r = s.getDescribe() ;
                String keyPrefix = r.getKeyPrefix();
                if (metrics.SObject_ID__c != NULL) {
                    if (metrics.SObject_ID__c.startsWith (keyPrefix)) {
                        Inquiry__c inq = new Inquiry__c ();
                        inq = [ SELECT Email__c, Mobile_Phone__c, first_name__c, 
                               last_name__c, title__c, Mobile_CountryCode__c 
                               FROM Inquiry__c 
                               where id =: metrics.SObject_ID__c LIMIT 1];
                        metrics.Title__c = inq.Title__c;
                        metrics.First_Name__c = inq.First_Name__c;
                        metrics.Last_Name__c = inq.Last_Name__c;
                        metrics.Inquiry_Email__c = inq.Email__c;
                        metrics.Inquiry_Phone__c = inq.Mobile_Phone__c;
                        metrics.Mobile_Country_Code__c = inq.Mobile_CountryCode__c;
                        metrics.Saturday_to_Thursday_9_30_19_30__c = true;
                    }                        
                }
                return null;
            }
        } else 
            return null;
    }
    public pageReference save () {
        try {
            metrics.Confirm_Details__c = true;
            Update metrics;
            return new pageReference ('/Thankyoupage?id='+UtilityHelperCls.encryptMobile(metrics.ID));
        } catch (Exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, e.getMessage ()));
            return null;
        }
    }
}