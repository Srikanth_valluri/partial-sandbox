@isTest 
private class HandoverKeyReleaseNotificationTest {
    
    @testSetup
    static void setup() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;       
        
        NSIBPM__Service_Request__c objReq = TestDataFactory_CRM.createServiceRequest();
        insert objReq ;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp ;
        
        Location__c objLoc = new Location__c();
        objLoc.RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        objLoc.Handover_Team_notification_Email__c = 'test@test.com';
        objLoc.Location_ID__c = '1234';
        insert objLoc ;
            
        Inventory__c objInv = TestDataFactory_CRM.createInventory( objProp.Id );
        objInv.Building_Location__c = objLoc.Id;
        insert objInv ;
        
        List<Booking__c> lstBooking =  TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objReq.Id,1 );
        insert lstBooking ;
        
        List<Booking_Unit__c> lstUnits = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
        for( Booking_Unit__c objUnit : lstUnits ) {
            objUnit.Inventory__c = objInv.Id ;
            objUnit.FM_Email__c = 'test@test.com';
            objUnit.Loams_Email__c = 'test@test.com';
        }
        insert lstUnits ;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        objCase.Booking_Unit__c = lstUnits[0].Id;
        insert objCase ;
        
        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Name = 'Test 01A';
        objAttach.Case__c = objCase.Id;
        objAttach.Attachment_URL__c  = 'https://www.google.com/';
        insert objAttach;
    }
    
    static testMethod void testSendKeyHandoverReadinessNotification() {
        
        List<Booking_Unit__c> lstUnits = [ SELECT Id
                                                , Inventory__r.Building_Location__r.Handover_Team_notification_Email__c
                                                , FM_Email__c
                                                , Loams_Email__c
                                             FROM Booking_Unit__c
                                            LIMIT 1] ;
        
        test.startTest();
            HandoverKeyReleaseNotification.sendKeyHandoverReadinessNotification( lstUnits );
        test.stopTest();
    }
}