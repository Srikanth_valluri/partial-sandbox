/*********************************************************************************************************
* Name               : AsyncBuyerWebservice                                                              *
* Description        : Controller class Send Buyers to IPMS when Buyers are Updated from PC Confirmation *
* Created By         : Srikanth              
* 1.1     27-01-2020         QBurst Added condition to suppress Nationality for Corporate Buyers         *
**********************************************************************************************************/
public class AsyncBuyerWebservice {
    @Future (Callout = TRUE)
    public static void pushBuyerUpdatetoIPMS (SET <id> buyerIds) {
        pushBuyerUpdate (buyerIds, 0);
    }
    public static void pushBuyerUpdateFromButton (SET <id> buyerIds, Decimal changeCount) {
        pushBuyerUpdate (buyerIds, changeCount);
    }
    
    public static void pushBuyerUpdate (SET <id> buyerIds, Decimal changeCount) {        
        System.Debug ('-----'+changeCount );
        Map <String, String> ipmsSettings = getIPMSsetting ('IPMS_webservice');
        HTTPRequest req = new HTTPRequest();
        req.setMethod('POST');
        
        String reqXML = preparePartyUpdate (buyerIds, changeCount);
        reqXML = reqXML.replaceAll('null', '');
        reqXML = reqXML.trim();
        req.setbody (reqXML);
        req.setEndpoint (ipmsSettings.get ('EndpointURL'));
        req.setHeader('Content-Type','text/xml');
        req.setTimeout(120000);
        HTTP http = new HTTP();
        if (Test.isRunningTest()) {
            parsePartyUpdateResponse ('<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/"><env:Header/><env:Body><OutputParameters xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/process/"><X_RESPONSE_MESSAGE/><X_RETURN_STATUS>E</X_RETURN_STATUS><X_RETURN_MESSAGE>[EX-ERROR]</X_RETURN_MESSAGE></OutputParameters></env:Body></env:Envelope>');
        }
        if (!Test.isrunningTest()){
            try{
                HTTPResponse res = http.send(req);
                system.debug (res.getBody());
                parsePartyUpdateResponse (res.getBody());
            }
            catch (Exception ex) {
                Log__c objLog = new Log__c();
                objLog.Description__c = 'Ids=='+buyerIds+'-Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage();
                objLog.Type__c = 'Webservice Callout For Party Update Service';
                insert objLog;             
            }  
        }  
        
    }
    public static void parsePartyUpdateResponse(string body){
        System.Debug (body);
        String reqId,BUid,buyerid,status,statusmsg,stageId,doclist = '';
        
        List <NSIBPM__SR_Doc__c> SRDocList = new List <NSIBPM__SR_Doc__c>();
        
        List <String> BUids = new List <String> ();
        Map <String, String> StatusMap = new Map <String, String> ();
        Map <String, String> StatusMsgMap = new Map <String, String> ();
        
        DOM.Document xmlDOC = new DOM.Document ();
        xmlDOC.load (body);
        DOM.XMLNode rootElement = xmlDOC.getRootElement ();
        
        
        for (Dom.XMLNode child1: rootElement.getChildElements()) {
            for (Dom.XMLNode child2: child1.getChildElements()) {
                for (Dom.XMLNode child3: child2.getChildElements()) {
                    for (Dom.XMLNode child4: child3.getChildElements()) {
                        for (Dom.XMLNode child5: child4.getChildElements()) {
                            if(child5.getName()=='PARAM_ID') 
                                Buyerid=child5.getText();                              
                            if(child5.getName()=='PROC_STATUS')
                                status=child5.getText(); 
                            if(child5.getName()=='PROC_MESSAGE')
                                statusmsg=child5.getText(); 
                        }                          
                        Statusmap.put(buyerid,status);  
                        StatusMsgmap.put(buyerid,statusmsg);
                    }
                }
            }
        }
        try {
            List <Log__c> logsToInsert = new List <Log__c> ();
            List <Buyer__c> buyerlist= [SELECT Address_Changed__c,Contact_Changed__c,
                                        Name_Changed__c,Name_Passport_Changed__c,IPMS_Status__c,
                                        Passport_Changed__c,Status__c 
                                        FROM Buyer__c 
                                        WHERE id in:StatusMap.keyset()];
            for (Buyer__c PB : buyerlist) {
                if (Statusmap.get(PB.id) == 'S') {
                    if (PB.Address_Changed__c)
                        PB.Address_Changed__c=false;
                    if (PB.Contact_Changed__c)
                        PB.Contact_Changed__c=false; 
                    if (PB.Passport_Changed__c)
                        PB.Passport_Changed__c=false;
                    if (PB.Name_Changed__c)
                        PB.Name_Changed__c=false;
                    if (PB.Name_Passport_Changed__c)
                        PB.Name_Passport_Changed__c=false;  
                } else {
                    Log__c objLog = new Log__c();
                    objLog.Description__c = body;
                    objLog.Type__c = 'Error';
                    LogsToInsert.add (objLog);
                }
                PB.IPMS_Status__c='Party Update Service -'+StatusMsgMap.get(PB.id);
            }
            update buyerlist;
            if (logsToInsert.size () > 0)
                insert logsToInsert;
        }
        catch (Exception ex) {
            Log__c objLog = new Log__c();
            objLog.Description__c ='Ids=='+StatusMap.keyset()+'-Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage();
            objLog.Type__c = 'Error updating response for Party Update service';
            insert objLog;             
        }    
        
        
    }
    public static String preparePartyUpdate (Set <id> buyerIds, Decimal changeCount) {
        String buyerQuery = ' SELECT '+getAllFields('Buyer__c') +' FROM Buyer__c WHERE ID IN: buyerIds AND Party_ID__c != null ';
        
        if (changeCount == 0) {
             buyerQuery += ' AND Change_Count__c > 0';
        }
        
        List <Buyer__c> buyerDetailsList = DataBase.query (buyerQuery); 
        
        Set <ID> bookingIds = new Set <ID> ();
        for (Buyer__c buyer :buyerDetailsList) {
            bookingIds.add (buyer.Booking__c);
        }
        System.Debug (bookingIds);
        
        String BUQuery = 'SELECT '+getAllFields ('Booking_Unit__c')+' FROM Booking_Unit__c '
            + ' WHERE Booking__c IN :bookingIds ';
        
        List <Booking_Unit__c> BUlist= DataBase.query (BUQuery); 
        
        String reqno = '123456';
        if (BUlist.size() > 0)
            reqno ='PU-'+BUlist[0].name+'-'+GetFormattedDateTime (system.now());
        
        List <Id>SRIds = new List <Id> ();
        for (Booking_Unit__c BU : BUlist) {
            SRIds.add (BU.SR_Id__c);
        }
        
        List <String> statusval = new List <String> {'New','Updated','Removed'};
            Map <String, String> ipmsSettings = getIPMSsetting ('IPMS_webservice');
        String body = prepareSoapHeader ('', ipmsSettings.get ('UserName'), ipmsSettings.get ('Password')); 
        
        XmlStreamWriter  w = new XmlStreamWriter();
        w.writeStartElement(null, 'soapenv:Body',null ); //1 tag
        w.writeStartElement(null, 'proc:InputParameters',null ); //2 tag
        
        w.writeStartElement(null, 'proc:P_REQUEST_NUMBER',null ); // 3 tag
        w.writeCharacters(reqno); // 3 value
        w.writeEndElement(); //3 tag
        
        w.writeStartElement(null, 'proc:P_SOURCE_SYSTEM',null ); // 3 tag
        w.writeCharacters('SFDC'); // 3 value
        w.writeEndElement(); //3 tag
        
        
        w.writeStartElement(null, 'proc:P_REQUEST_NAME',null );// 4 tag
        w.writeCharacters('UPDATE_PARTY_DETAILS');// 4 tag
        w.writeEndElement(); //4 tag
        
        w.writeStartElement(null, 'proc:P_REQUEST_MESSAGE',null );// 5 tag
        
        for (Booking_Unit__c BU: BUlist) {
            System.Debug (Bu.Name);
            for (Buyer__c PB :buyerDetailsList) {
                List <String> chglist = new List <String> ();
                if (changeCount == 0) {
                    changeCount = PB.Change_Count__c;
                } else {
                    chglist.add('ADDRESS_CHANGE');
                    chglist.add('CONTACT_CHANGE'); 
                    chglist.add('PP_CHANGE');
                    chglist.add('NAME_CHANGE');
                    chglist.add('NAME_PP_CHANGE');
                }
                
                if (PB.Address_Changed__c)
                    chglist.add('ADDRESS_CHANGE');
                if (PB.Contact_Changed__c)
                    chglist.add('CONTACT_CHANGE'); 
                if(PB.Passport_Changed__c)
                    chglist.add('PP_CHANGE');
                if (PB.Name_Changed__c)
                    chglist.add('NAME_CHANGE');
                if (PB.Name_Passport_Changed__c)
                    chglist.add('NAME_PP_CHANGE');

                for (integer i=0; i < changeCount; i++) {
                    w.writeStartElement(null, 'proc:P_REQUEST_MESSAGE_ITEM',null ); // 6 i Tag
                    
                    w.writeStartElement(null, 'proc:PARAM_ID',null ); // 7 i Tag
                    w.writeCharacters(String.valueof(pb.id));// 7 i value
                    w.writeEndElement(); // 7 i tag
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE1',null );
                    w.writeCharacters (pb.Party_ID__c);
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE2',null );
                    w.writeCharacters (chglist[i]);
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE3',null );
                    if (pb.Buyer_Type__c == 'Individual')
                        w.writeCharacters ('PERSON');
                    else if (pb.Buyer_Type__c == 'Corporate')
                        w.writeCharacters ('ORGANIZATION');
                    w.writeEndElement();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE4',null );
                    if (pb.title__c != NULL)
                        w.writeCharacters (pb.Title__c);
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE5',null );
                    if (pb.First_Name__c != NULL)
                        w.writeCharacters (pb.First_Name__c);
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE6',null );
                    if (pb.Middle_Name__c != NULL)
                        w.writeCharacters (pb.Middle_Name__c);
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE7',null );
                    if (pb.Last_Name__c != NULL)
                        w.writeCharacters (pb.Last_Name__c);
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE8',null );
                    if (pb.Organisation_Name__c != null)
                        w.writeCharacters (pb.Organisation_Name__c);
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE9',null );
                    if (pb.NATIONALITY__c != null && pb.Buyer_Type__c!='Corporate')//1.1
                        w.writeCharacters (pb.NATIONALITY__c);
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE10',null );
                    if (pb.Buyer_Type__c == 'Individual') {
                        if (pb.Passport_Number__c != null)
                            w.writeCharacters (pb.Passport_Number__c);
                    }
                    else {
                        if (pb.CR_Number__c != null)
                            w.writeCharacters (pb.CR_Number__c);                        
                        
                    }
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE11',null );
                    if (pb.Buyer_Type__c == 'Individual') {
                        if (pb.Place_of_Issue__c != null)
                            w.writeCharacters (pb.Place_of_Issue__c);
                    }
                    else {  
                        if (pb.CR_Registration_Place__c != null)
                            w.writeCharacters (pb.CR_Registration_Place__c); 
                        
                    }
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE12',null );
                    String expdate='';
                    if (pb.Buyer_Type__c == 'Individual') {
                        if(PB.Passport_Expiry_Date__c != null){
                            
                            String[] strDate;
                            if (PB.Passport_Expiry_Date__c.contains ('-')) {
                                strDate = PB.Passport_Expiry_Date__c.split('-');
                            }
                            if (PB.Passport_Expiry_Date__c.contains ('/')) {
                                strDate = PB.Passport_Expiry_Date__c.split('/');
                            }
                            
                            Integer myIntDate = integer.valueOf(strDate[0]);                            
                            Integer myIntMonth = integer.valueOf(strDate[1]);                           
                            Integer myIntYear = integer.valueOf(strDate[2]);                            
                            Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);                                
                            expdate = GetDatetext(d);                            
                            w.writeCharacters (expDate);
                            
                        } 
                    }
                    if (Test.isRunningTest ()) {
                        pb.Buyer_Type__c = 'Corporate';
                        pb.CR_Registration_Expiry_Date__c = String.valueOf (Date.today().addDays (60));
                    }
                    if (pb.Buyer_Type__c == 'Corporate') {
                        if (PB.CR_Registration_Expiry_Date__c != null){
                            String[] strDate;
                            if (PB.CR_Registration_Expiry_Date__c.contains ('-')) {
                                strDate = PB.CR_Registration_Expiry_Date__c.split('-');
                            }
                            if (PB.CR_Registration_Expiry_Date__c.contains ('/')) {
                                strDate = PB.CR_Registration_Expiry_Date__c.split('/');
                            }
                            Integer myIntDate = integer.valueOf(strDate[0]);                            
                            Integer myIntMonth = integer.valueOf(strDate[1]);                           
                            Integer myIntYear = integer.valueOf(strDate[2]);                            
                            Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);                                
                            expdate = GetDatetext(d);                            
                            w.writeCharacters (expDate);                            
                        } 
                    }
                    
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE13',null );
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE14',null );
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE15',null );
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE16',null );
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE17',null );
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE18',null );
                    w.writeEndElement ();
                    String code = '';
                    if (PB.Phone_Country_Code__c != NULL) {
                        List <String> codes = PB.Phone_Country_Code__c.split(': 00');   
                        code = codes[1];
                    }  
                    w.writeStartElement (null, 'proc:ATTRIBUTE19',null );
                    
                    w.writeCharacters (code);
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE20',null );
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE21',null );
                    if (pb.Phone__c != NULL)
                        w.writeCharacters (pb.Phone__c);
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE22',null );
                    if (pb.Email__c != NULL)
                        w.writeCharacters (pb.Email__c);
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE23',null );
                    if (pb.Address_Line_1__c != NULL)
                        w.writeCharacters (pb.Address_Line_1__c);
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE24',null );
                    if (pb.Address_Line_2__c != null)
                        w.writeCharacters (pb.Address_Line_2__c);
                    else
                        w.writeCharacters ('');
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE25',null );
                    if (pb.Address_Line_3__c != null)
                        w.writeCharacters (pb.Address_Line_3__c);
                    else
                        w.writeCharacters ('');
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE26',null );
                    if (pb.Address_Line_4__c != null)
                        w.writeCharacters (pb.Address_Line_4__c);
                    else
                        w.writeCharacters ('');
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE27',null );
                    if (pb.City__c!= null)
                        w.writeCharacters (pb.City__c);
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE28',null );
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE29',null );
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE30',null );
                    w.writeEndElement ();
                    
                    w.writeStartElement (null, 'proc:ATTRIBUTE31',null );
                    if (pb.Country__c != null)
                        w.writeCharacters (pb.Country__c);
                    w.writeEndElement ();
                    
                    w.writeEndElement ();
                    
                }   
            }
        }
        w.writeEndElement();
        w.writeEndElement();
        w.writeEndElement();
        body += w.getXmlString();
        body +='</soapenv:Envelope>'; 
        System.Debug (body);
        return body.trim ();
        
    }
    
    public static Map<String, String> getIPMSsetting (String servicename){
        IPMS_Integration_Settings__mdt ipms= [SELECT Endpoint_URL__c, Password__c, Username__c
                                              FROM IPMS_Integration_Settings__mdt 
                                              WHERE DeveloperName = :servicename];
        Map <String, String> resultMap = new Map <String, String> ();
        resultMap.put ('EndpointURL', ipms.Endpoint_URL__c);
        resultMap.put ('UserName', ipms.Username__c);
        resultMap.put ('Password', PasswordCryptoGraphy.DecryptPassword(ipms.Password__c));
        return resultMap;
    }
    
    public static string getAllFields (String objectName) {
        String fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        
        if (objectType == null)
            return fields;
        
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            
            fields += sfield.getDescribe().getName ()+ ', ';
            
        }
        return fields.removeEnd(', '); 
    }
    public static String GetFormattedDateTime(DateTime dt){
        String yyyy = String.valueof (dt.year());
        String mm = String.valueof (dt.month());
        String dd = String.valueof (dt.day());
        String hh = String.valueof (dt.hour());
        String mi = String.valueof (dt.minute());
        String ss = String.valueof (dt.second());
        String ms = String.valueof (dt.millisecond());
        
        String formatdate = yyyy+mm+dd+hh+mi+ss+ms;
        return formatdate;
    }
    //Method to Get formatted date
    public static string GetDatetext(Date d){
        String dd,mm,mon,yy,dtext='';
        dd = String.valueof(d.day());
        mm = String.valueof(d.month());
        if (mm == '1')
            mon = 'JAN';
        else if (mm == '2')
            mon = 'FEB';
        else if (mm == '3')
            mon = 'MAR';
        else if (mm == '4')
            mon = 'APR';
        else if (mm == '5')
            mon = 'MAY';
        else if (mm == '6')
            mon = 'JUN';
        else if (mm == '7')
            mon = 'JUL';
        else if (mm == '8')
            mon = 'AUG';
        else if (mm == '9')
            mon = 'SEP';
        else if (mm == '10')
            mon = 'OCT';
        else if (mm == '11')
            mon = 'NOV';
        else if (mm == '12')
            mon = 'DEC';
        
        yy = String.valueof (d.year());
        dtext = dd+'-'+mon+'-'+yy;
        return dtext;
        
    }
    public static string prepareSoapHeader(String body, String userName, String password){
        
        body +='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xxdc="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/" xmlns:proc="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/process/">'
            +'<soapenv:Header>'
            +'<xxdc:SOAHeader>'
            +'<xxdc:Responsibility>ONT_ICP_SUPER_USER</xxdc:Responsibility>'
            +'<xxdc:RespApplication>ONT</xxdc:RespApplication>'
            +'<xxdc:SecurityGroup>standard</xxdc:SecurityGroup>'
            +'<xxdc:NLSLanguage>american</xxdc:NLSLanguage>'
            +'<xxdc:Org_Id/>'
            +'</xxdc:SOAHeader>'
            +'<wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">'
            +'<wsse:UsernameToken>'
            +'<wsse:Username>'+userName+'</wsse:Username>'
            +'<wsse:Password>'+password+'</wsse:Password>'
            +'</wsse:UsernameToken>'
            +'</wsse:Security>'
            +'</soapenv:Header>';
        return body;
    }
}