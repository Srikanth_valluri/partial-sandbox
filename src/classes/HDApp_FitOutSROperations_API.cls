/**********************************************************************************************************************
Description: This API is used for FitOut SR  GetDraft operattions in Damac Living App
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   16-11-2020      | Jyotika Kalra | Created initial draft
***********************************************************************************************************************/
@RestResource(urlMapping='/getfitOutSROps/*')

global class HDApp_FitOutSROperations_API  {

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    
    /***********************************************************************************************
    Method Name : getFitOutDraftSR
    Description : Returns the FitOut SR (FM Case) details for given fmCaseId
    Parameter(s): None
    Return Type : FinalReturnWrapper (Wrapper)
    ************************************************************************************************/
    @HttpGet
    global static FinalReturnWrapper getFitOutDraftSR() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_data objData = new cls_data();
        cls_meta_data objMeta = new cls_meta_data();

        if(!r.params.containsKey('fmCaseId')){
            objMeta = ReturnMetaResponse('Missing parameter : fmCaseId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('fmCaseId') && String.isBlank(r.params.get('fmCaseId'))) {
            objMeta = ReturnMetaResponse('Missing parameter value: fmCaseId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        System.debug('fmCaseId :: ' + r.params.get('fmCaseId'));
        //String  fmCaseId = r.params.get('fmCaseId');
        /**
        List<FM_Case__c> fitOutCaseDetails =  [Select Id,
                                                        Name,
                                                        Booking_Unit__c,
                                                        Type_of_Alteration__c,
                                                        Type_of_NOC__c,
                                                        Description__c ,
                                                        Person_To_Collect__c, 
                                                        Contact_person_contractor__c,
                                                        Email_2__c,
                                                        Mobile_Country_Code_2__c,
                                                        Mobile_no_contractor__c,
                                                        Request_Type_DeveloperName__c,
                                                        Request_Type__c,
                                                        Origin__c,
                                                        Status__c,
                                                         (SELECT id
                                                           , Attachment_url__c
                                                           , Name 
                                                           , Type__c
                                                        FROM Documents__r) 
                                                        FROM FM_Case__c WHERE 
                                                        id =: fmCaseId];
                                                        
        **/ // 
        
        List<FM_Case__c> fitOutCaseDetails = GetTenantRegistrationCase_API.getFMCase(r.params.get('fmCaseId'));

        
        if(fitOutCaseDetails != null && fitOutCaseDetails.size() > 0) {
            
        
            cls_data fitOutCaseWrap = new cls_data();
            fitOutCaseWrap.fm_case_status = fitOutCaseDetails[0].Status__c;
            fitOutCaseWrap.fm_case_id = fitOutCaseDetails[0].Id;
            fitOutCaseWrap.booking_unit_id = fitOutCaseDetails[0].Booking_Unit__c;
            fitOutCaseWrap.type_of_work = fitOutCaseDetails[0].Type_of_Alteration__c;
            fitOutCaseWrap.noc_type = fitOutCaseDetails[0].Type_of_NOC__c;
            fitOutCaseWrap.Description = fitOutCaseDetails[0].Description__c;
            fitOutCaseWrap.collection_by = fitOutCaseDetails[0].Person_To_Collect__c;
            fitOutCaseWrap.contractor_name = fitOutCaseDetails[0].Contact_person_contractor__c;
            fitOutCaseWrap.contractor_email = fitOutCaseDetails[0].Email_2__c;
            fitOutCaseWrap.contractor_mobile_country_code = fitOutCaseDetails[0].Mobile_Country_Code_2__c;
            fitOutCaseWrap.contractor_mobile = fitOutCaseDetails[0].Mobile_no_contractor__c;
            
            
            list<cls_attachment> lstAttachments = new list<cls_attachment>();

            if(fitOutCaseDetails[0].Documents__r.size() > 0) {
                System.debug('fitOutCaseDetails[0].Documents__r'+fitOutCaseDetails[0].Documents__r);
                for(SR_Attachments__c obj : fitOutCaseDetails[0].Documents__r) {
                    cls_attachment objAttach = new cls_attachment();
                    objAttach.id = obj.id;
                    objAttach.name = obj.Name;
                    objAttach.file_extension = obj.Type__c;
                    objAttach.attachment_url = obj.Attachment_URL__c;

                    lstAttachments.add(objAttach);
                }
            }

            fitOutCaseWrap.attachments = lstAttachments;
            
            cls_data instData = (cls_data)JSON.deserialize(JSON.serialize(fitOutCaseWrap) ,cls_data.class );
            System.debug('instData:: ' + instData);

            objData = instData;
            objMeta = ReturnMetaResponse('Success', 1);
        }
        else {
            objMeta = ReturnMetaResponse('No FM case found for given fmCaseId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        returnResponse.meta_data = objMeta;
        returnResponse.data = objData;

        System.debug('returnResponse: ' + returnResponse);

        return returnResponse;
    }
    
    
    /***********************************************************************************************
    Method Name : ReturnMetaResponse
    Description : Generic method to create meta_data response instance
    Parameter(s): String message, Integer statusCode
    Return Type : cls_meta_data (wrapper)
    ************************************************************************************************/
    public static cls_meta_data ReturnMetaResponse(String message, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data();
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = null;
        return retMeta;
    }
    

    
    
    public class cls_data {
          
        public String fm_case_status;   
        public String fm_case_id;
        public String booking_unit_id;
        
        public String type_of_work;
        public String noc_type;
        public String collection_by;
        public String description;
        public String contractor_name;
        public String contractor_email;
        public String contractor_mobile_country_code;
        public String contractor_mobile;
        
        
        public cls_attachment[] attachments;
    }
    
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }
    

    public class cls_attachment {
        public String id;
        public String name;
        public String attachment_url;
        public String file_extension;
    }

}