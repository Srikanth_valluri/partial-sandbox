public with sharing class CancelFundTransferController {
    public Case objCase{get;set;}
    public Boolean isLightningMode{get;set;}
    public CancelFundTransferController(ApexPages.StandardController stdController) {
        objCase = (Case)stdController.getRecord();
    }
    public PageReference cancelCase() {
        String baseURL= String.valueOf(System.URL.getSalesforceBaseUrl());
        System.debug('====baseURL===' + baseURL);

        if(objCase != NULL) {
            objCase = [SELECT Id, Status, RecordType.Name, OwnerId, Approval_Status__c
                        FROM Case
                        WHERE Id =: objCase.Id LIMIT 1];
                        System.debug('====objCase===' + objCase);
            if(objCase.Status == 'Submitted' && objCase.Approval_Status__c != 'Approved') {
                    objCase.Status = 'Cancelled';
                    update objCase;
                    // if(baseURL.containsIgnoreCase('lightning.force.com')) {
                    //  return new PageReference('/one/one.app?#/sObject/'+ objCase.Id + '/view');
                    //
                    // }
                    // else {
                        return new PageReference('/' + objCase.Id);
                    // }
            }
            else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Request cannot be cancelled'));
            }

        }
        return NULL;

    }
}