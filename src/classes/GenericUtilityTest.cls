@isTest
public class GenericUtilityTest {
    @isTest
    public static void testEncodeChar() {
       Test.startTest();
       GenericUtility.EncodeChar('abc+xyz');
       Test.stopTest(); 
    }
    
    @isTest
    public static void  testGetCamelCase() {
        Test.startTest();
       GenericUtility.getCamelCase('AbXXyz');
       Test.stopTest();
    }
    
    @isTest
    public static void testDoFormat() {
       Test.startTest();
       GenericUtility.doFormatting(1000.00, 2, ',', '.');
       Test.stopTest();
    }
    
    @isTest
    public static void testRemoveLeadingZeros() {
       Test.startTest();
       GenericUtility.removeLeadingZeros(new List<String>{'007578888', '0777777'});
       Test.stopTest();
    }
    
    @isTest
    public static void testcreateErrorLog() {
       Test.startTest();
           GenericUtility.createErrorLog('Test error', null, null, null, null);
       Test.stopTest();
    }
    
    @isTest
    public static void testGetMonthNumber() {
       Test.startTest();
           GenericUtility.getMonthNumber('Jan');
       Test.stopTest();
    }
    
    @isTest
    public static void testgetMonthName() {
       Test.startTest();
           GenericUtility.getMonthName(2);
       Test.stopTest();
    }
    
    @isTest
    public static void testcreateSOACreator() {
       Test.startTest();
           GenericUtility.createSOACreator('www.google.com',UserInfo.getUserId(),system.now(),'12244','xyz',NULL,NULL,NULL);
       Test.stopTest();
    }
    @isTest
    public static void testcreateTrackingHistory() {
       GenericUtility.HistoryWrapper historyWrapperInst = new GenericUtility.HistoryWrapper();
       
       Test.startTest();
           GenericUtility genObj = new GenericUtility();
           genObj.createTrackingHistory(new List<GenericUtility.HistoryWrapper>{historyWrapperInst});
       Test.stopTest();
    }
    
    @isTest
    public static void testGetApprAuthoritiesParentAmtLesser(){
       Test.startTest();
         GenericUtility genObj = new GenericUtility();
         genObj.getApprovingAuthorities(100,1000, 'Manager', 'Manager,Director');
       Test.stopTest();  
    }

    @isTest
    public static void testGetApprAuthoritiesParentChildAmtEqual(){
       Test.startTest();
         GenericUtility genObj = new GenericUtility();
         genObj.getApprovingAuthorities(1000,1000, 'Manager', 'Manager,Director');
       Test.stopTest();  
    }

    @IsTest
    static void test1(){
        Case objCase = new Case();
                insert objCase;

        Task objTask = new Task();
        objTask.Subject = 'Resolve Complaint';
        objTask.whatId = objCase.Id;
        objTask.ActivityDate = System.today();
        objTask.Status = 'Not Started';
        objTask.Priority = 'Low';
        insert objTask;
        
        ComplaintEscalationNotifier.DataWrapper objWrap = new ComplaintEscalationNotifier.DataWrapper();
        objWrap.Ids = objTask.Id;
        objWrap.Eamil48Hour = true;
        Test.startTest();
        ComplaintEscalationNotifier.notifyOwner( new List<ComplaintEscalationNotifier.DataWrapper>{ objWrap});
        Test.stopTest();
    }
}