/**************************************************************************************************
* Name               : AP_ContactDetailsFooterController
* Description        : An apex page controller for Contact Details Footer
* Created Date       : 11/09/2018
* Created By         : Nikhil Pote
* Last Modified Date :
* Last Modified By   :
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR                    DATE
* 1.0         Nikhil Pote               11/09/2018
**************************************************************************************************/

public class AP_ContactDetailsFooterController {


    // public Map<String,list<Test_Knowledge__kav>> subCatAndKnowArtMap {get; set;}
    //public Map<String,List<myObject>> subCatAndKnowArtMap {get; set;}

    public static String labelVal;
    public String parentCategory;
    public String suCategoryName;
    public Map<String,String> subCategoryLabelAndNameMap;
    public Map<Id,String> mapKnowledgeIdTitle;
    public List<User> lstUser                                                           {get; set;}
    public List<Decimal> knowScore                                                      {get; set;}
    public List<KnowledgeArticleViewStat> knowledgeStat                                 {get; set;}
    public Map<Decimal,List<myObject> > mapScoreTitle                                   {get; set;}
    public static  Map<String, List<String>> dataCategoriesAndSubCatMap                 {get; set;}
    public List<DataCategoryUtil.RootObject> dataCategoriesStructure                    {get; set;}
    public Map<String,User > mapIdUser                                                  {get; set;}
    public Boolean isPrimary                                                            {get; set;}
    public Boolean isSecondary                                                          {get; set;}
    private static Set<String> articleFieldSets = new Set<String> {
        'Id','Title','Description__c',
        'KnowledgeArticleId','PublishStatus',
        'Language','CreatedDate'
    };

    public AP_ContactDetailsFooterController() {
        isPrimary = false;
        isSecondary = false;
        mapIdUser = new Map<String,User >();
        User objUser = [ SELECT FullPhotoUrl,
                                Account.OwnerId,
                                Account.Secondary_Owner__c,
                                Account.Owner.Name,
                                Account.Secondary_Owner__r.Name
                        FROM User
                          WHERE Id =: userInfo.getUserId()
        ];
        System.debug('objUser>>>>> --' + objUser);
         System.debug('objUser.Account>>>>> --' + objUser.Account);
        lstUser = new List<User>();
        if(objUser.Account != NULL) {
            String userQuery = ' SELECT FullPhotoUrl, Name, FirstName, Lastname, Email, Phone, MobilePhone, '
                             + ' Contact.Account.OwnerId FROM User WHERE Id = \''
                             + objUser.Account.OwnerId
                             + '\'';
            if (objUser.Account.Secondary_Owner__c != null ) {
                userQuery += ' OR Id = \''
                           + objUser.Account.Secondary_Owner__c
                           + '\'';
            }
            lstUser = Database.query(userQuery);
            System.debug('lstUser>>>>> --' + lstUser);
            for(User userObj : lstUser){
                if(userObj.id == objUser.Account.OwnerId){
                    isPrimary = true;
                    mapIdUser.put('primary',userObj);
                }else if(objUser.Account.Secondary_Owner__c != null && userObj.id == objUser.Account.Secondary_Owner__c){
                    isSecondary = true;
                    mapIdUser.put('Secondary',userObj);
                }
            }
            System.debug('mapIdUser>>>>> --' + mapIdUser);
            /*List<User> objPrimUserList = [SELECT FullPhotoUrl, Name, FirstName,Lastname, Email, Phone,
                                                      MobilePhone, Contact.Account.OwnerId
                                                 FROM User
                                                WHERE Id =: objUser.Account.OwnerId];
            if (objPrimUserList != null && !objPrimUserList.isEmpty()) {
                lstUser.add(objPrimUserList[0]);
            }
            if (objUser.Account.Secondary_Owner__c != null ) {
                List<User> objSecUserList = [SELECT FullPhotoUrl, Name, FirstName,Lastname, Email, Phone,
                                                      MobilePhone, Contact.Account.OwnerId
                                                 FROM User
                                                WHERE Id =: objUser.Account.Secondary_Owner__c];
                if (objSecUserList != null && !objSecUserList.isEmpty()) {
                    lstUser.add(objSecUserList[0]);
                }
            }*/

            /*system.debug('objPrimUser--'+objPrimUser);

            user objSecUser = [Select FullPhotoUrl,name,firstname,email,phone,MobilePhone,contact.account.Secondary_Owner__c
                                From User where id =: objUser.contact.account.Secondary_Owner__c];
            system.debug('objSecUser--'+objSecUser);

            lstUser.add(objPrimUser);
            lstUser.add(objSecUser);*/
        }
        if (!Test.isRunningTest()) {
            fetchSubCatAndArticlesOnLoad();
        }
    }

    public void fetchSubCatAndArticlesOnLoad() {
        dataCategoriesAndSubCatMap = new Map<String, List<String>>();
        knowledgeStat = new List<KnowledgeArticleViewStat>();
         mapScoreTitle = new Map<Decimal,List<myObject>>();
        mapKnowledgeIdTitle = new Map<Id,String>();
        knowScore = new list<Decimal>();
       // subCatAndKnowArtMap = new Map<String,List<myObject>>();
        parentCategory = Label.AP_FAQ;
        system.debug('>>>>>parentVal>>>'+parentCategory);
        system.debug('>>>>>managerIdWithInqCountmap>>>'+dataCategoriesAndSubCatMap);
        List<DataCategoryUtil.RootObject> dataCategoriesStructureDisplay = new List<DataCategoryUtil.RootObject>();
            dataCategoriesStructureDisplay = DataCategoryUtil.getDataCategoriesList();
        for(DataCategoryUtil.RootObject dataCategory : dataCategoriesStructureDisplay) {
          for( DataCategoryUtil.ChildCategory childCategory :
                dataCategory.topCategories[0].childCategories
            ) {
                if(dataCategoriesAndSubCatMap.containsKey(dataCategory.name)){
                    //subCategoryLab = dataCategoriesAndSubCatMap.get(dataCategory.name);
                    //subCategoryLab.add(childCategory.label);
                    dataCategoriesAndSubCatMap.get(dataCategory.name).add(childCategory.label);
                }else{
                    dataCategoriesAndSubCatMap.put(dataCategory.name,new List<String> {childCategory.label});
                }
            }
        }
        system.debug('>>>>>managerIdWithInqCountmap>>>'+dataCategoriesAndSubCatMap);

        //Map<Decimal,List<String>> maptitleAndId= new Map<Decimal,List<String>>();
         Set<id> knowledgeid = new Set<id>();
        List<AP_Knowledge__kav> knowledgeDetails = new List<AP_Knowledge__kav>();
         if( String.isNotBlank(parentCategory) && dataCategoriesAndSubCatMap.containsKey(parentCategory)){
            for(String childCategory :  dataCategoriesAndSubCatMap.get(parentCategory)){
                 system.debug('>>parentCategory>>>'+parentCategory);
                system.debug('>>childCategory>>>'+childCategory);
                String filters = createFilter();
                filters += ' WITH DATA CATEGORY '
                    + String.escapeSingleQuotes(parentCategory)
                    + '__c ABOVE '
                    + String.escapeSingleQuotes(String.valueOf(childCategory))+'__c ';
                    system.debug('>>articleFieldSets>>>'+articleFieldSets);
                String queryForKnowledge = queryBuilder(
                                        'AP_Knowledge__kav',
                                        articleFieldSets,
                                        filters,
                                        null,
                                        null,
                                        null
                                    );
                 system.debug('>>>>queryForKnowledge>>>'+queryForKnowledge);
                knowledgeDetails = fetchRecords(queryForKnowledge);
                system.debug('>>>>knowledgeDetails>>>'+knowledgeDetails);

                for(AP_Knowledge__kav knowledge : knowledgeDetails) {
                        system.debug('>>>>knowledge>>>'+knowledge.Title);
                        system.debug('>>>>knowledge>>>'+knowledge.Description__c);
                            knowledgeid.add(knowledge.KnowledgeArticleId);
                            mapKnowledgeIdTitle.put(knowledge.KnowledgeArticleId,knowledge.Title);
                    }
                }
                system.debug('>>>>>knowledgeid.id>>>'+knowledgeid);
                knowledgeStat = [SELECT
                                    Id,NormalizedScore,Parent.Id,ViewCount
                                FROM KnowledgeArticleViewStat
                                WHERE  Parent.Id IN :knowledgeid AND Channel = 'Csp' ORDER BY
                                NormalizedScore Desc Limit 4];
             
                for(KnowledgeArticleViewStat stat : knowledgeStat){
                    knowScore.add(stat.NormalizedScore);
                    String title = mapKnowledgeIdTitle.get(stat.Parent.Id);
                    if(mapScoreTitle.containsKey(stat.NormalizedScore)) {
                        List<myObject> tempList = mapScoreTitle.get(stat.NormalizedScore);
                        tempList.add(new myObject(title,stat.Parent.Id));
                        mapScoreTitle.put(stat.NormalizedScore,tempList);
                    } else {
                          mapScoreTitle.put(stat.NormalizedScore,new List<myObject> {new myObject (title,stat.Parent.Id)});
                   }
                }
                system.debug('>>>>>knowledgeStat>>>'+knowledgeStat);
            }else {
                ApexPages.addmessage(
                    new ApexPages.Message(ApexPages.Severity.Warning, 'Error')
                );
            }
    }


    @testVisible private static String createFilter() {
         String filters =  ' PublishStatus = \''+ label.Online + '\' ';
         //String filters =  ' PublishStatus = Online';
         //filters += ' AND Language = \'' + Constant.LANG_CODE + '\' ';
         return filters;
     }
     public  String queryBuilder(
        String objectName,
        Set<String> fieldName,
        String filters,
        String sortExpression,
        Integer recordLimit,
        Integer recordOffset
    ) {
        system.debug('>>fieldName>>>'+fieldName);
        String fieldsNameFromSet = SetToString(fieldName);
        system.debug('>>fieldsNameFromSet>>>'+fieldsNameFromSet);
        fieldsNameFromSet = fieldsNameFromSet + ',CreatedDate,createdBy.Name,createdBy.firstname,createdBy.lastName';
        return ('SELECT ' +
                fieldsNameFromSet +
                ' FROM ' +  objectName +
            (String.isNotBlank(filters) ?
                (' WHERE ' + filters  ) :''
            ) +
            (String.isNotBlank(sortExpression) ?
                (' ORDER BY ' +  sortExpression) :''
            ) +
            (recordLimit != null ?
                (' LIMIT ' + recordLimit) :''
            ) +
            (recordOffset != null ?
                (' OFFSET ' + recordOffset)
                 :''
                )
        );
    }

    public static List<sObject> fetchRecords(String query) {
        return Database.query(query);
    }

    public static String setToString(Set<String> fieldStr) {
        String setforQuery = '' ;
        //Converted Set to List as Set can't be used as iterable object
        setforQuery = String.join(new List<String>(fieldStr),',');
        setforQuery = setforQuery.substringBeforeLast(',');
        return setforQuery;
    }
    public class myObject {
        public String title { get; set; }
        public String knowledgeId { get; set; }

        public myObject(String title, String knowledgeId) {
            this.title = title;
            this.knowledgeId = knowledgeId;
        }
    }
}