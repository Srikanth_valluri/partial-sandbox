@isTest
public class NationalityPickListInfo_Test{
    public static testMethod void PicklistInfo_Test() {
        //Create Request and Response
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();
        req.requestURI = '/services/apexrest/nationalitypicklist/nationalitypicklist/*';  
        req.httpMethod = 'GET'; //Create get method
        RestContext.response = res;
        RestContext.request = req;
        //Call the method
        NationalityPickListInfo.getNewFundraiser();
        LanguagePickListInfo.getNewFundraiser();
        MobileCountryPickListInfo.getNewFundraiser();
     }
}