@isTest
public with sharing class UpdateBuyerRecordsTest {
    @isTest static void TestMethod1() {
        List<Account> accountList = TestDataFactory.createAccountRecords(new List<Account>{new Account()});
        
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.Delivery_mode__c='Email';
        SR.Deal_ID__c='1001';
        SR.Registration_Date__c=system.today();
        SR.Deal_Type__c = 'Recovery';
        insert SR;

        Booking__c bk= new Booking__c();
        bk.Deal_SR__c=SR.id;
        bk.Booking_channel__c='Office';
        insert bk;

        Buyer__c PB= new Buyer__c();
        PB.Primary_Buyer__c=true;
        PB.Service_Request__c = SR.id;
        PB.Buyer_Type__c='Individual';
        PB.Booking__c=bk.id;
        PB.Phone_Country_Code__c='India: 0091';
        PB.Passport_Expiry_Date__c='25/03/2017';
        PB.CR_Registration_Expiry_Date__c='25/12/2017';
        PB.City__c='Dubai';
        PB.Country__c='United Arab Emirates';
        PB.Address_Line_1__c='street1';
        PB.Address_Changed__c=true;
        PB.Date_of_Birth__c='25/12/1990';
        PB.Email__c='test@test.com';
        PB.First_Name__c='Buyer';
        PB.Last_Name__c='test';
        PB.Nationality__c='Indian'; 
        PB.Passport_Number__c='PP123'; 
        PB.Phone__c='53532255';
        PB.Place_of_Issue__c='Delhi'; 
        PB.Title__c='Mr.';
        PB.Account__c = accountList[0].Id;
        insert PB;
        string recId = PB.id;
        string[] recids = new String[]{recId};
        UpdateBuyerRecords.updateBuyers(recids);
        

    }

    @isTest static void TestMethod2() {
        Id RecTypepersonal = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc = new Account(FirstName = 'testLN',LastName ='teste',recordtypeid = RecTypepersonal,City__pc = 'test');
        insert acc;

        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.Delivery_mode__c='Email';
        SR.Deal_ID__c='1001';
        SR.Registration_Date__c=system.today();
        SR.Deal_Type__c = 'Recovery';
        insert SR;

        Booking__c bk= new Booking__c();
        bk.Deal_SR__c=SR.id;
        bk.Booking_channel__c='Office';
        insert bk;

        Buyer__c PB= new Buyer__c();
        PB.Service_Request__c = SR.id;
        PB.Primary_Buyer__c=true;
        PB.Buyer_Type__c='Individual';
        PB.Booking__c=bk.id;
        PB.Phone_Country_Code__c='India: 0091';
        PB.Passport_Expiry_Date__c='25/03/2017';
        PB.CR_Registration_Expiry_Date__c='25/12/2017';
        PB.City__c='Dubai';
        
        PB.Country__c='United Arab Emirates';
        PB.Address_Line_1__c='street1';
        PB.Address_Changed__c=true;
        PB.Date_of_Birth__c='25/12/1990';
        PB.Email__c='test@test.com';
        PB.First_Name__c='Buyer';
        PB.Last_Name__c='test';
        PB.Nationality__c='Indian'; 
        PB.Passport_Number__c='PP123'; 
        PB.Phone__c='53532255';
        PB.Place_of_Issue__c='Delhi'; 
        PB.Title__c='Mr.';
        PB.Account__c = acc.Id;
        insert PB;
        string recId = PB.id;
        string[] recids = new String[]{recId};
        UpdateBuyerRecords.updateBuyers(recids);

    }
}