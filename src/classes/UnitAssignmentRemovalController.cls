/*********************************************************************************************************
* Name               : UnitAssignmentRemovalController
* Test Class         : 
* Description        : Controller class for UnitAssignment VF Page
* Created Date       : 29/10/2020
* ----------------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         02/11/2020      Initial Draft.
**********************************************************************************************************/
public With Sharing Class UnitAssignmentRemovalController{
    public List<String> propertyNames {get; set;}
    Map<String, Property__c> propertyMap;
    public List<String> unitAssignmentNames {get; set;}
    Map<String, Unit_Assignment__c> unitAssignmentMap;
    public List<String> marketingCampaignNames {get; set;}
    Map<String, Campaign__c> marketingCampaignMap;    
    public String allocType {get; set;}
    public String selectedAssignment {get; set;}
    public String selectedCampaign {get; set;}
    public static string unitDetails {get;set;}
    public static string invuserDetails {get;set;}
    public Map<string,id> inventoryMap {get;set;}
    public Map<id,Inventory_User__c> inventoryUserMap {get;set;}
    
    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public UnitAssignmentRemovalController() {
        propertyNames = new List<String>();
        allocType = 'Unit Assignment';
        selectedAssignment = '';
        selectedCampaign = '';
        inventoryMap = new Map<string,id>();
        inventoryUserMap = new Map<id,Inventory_User__c>();
        propertyMap = new  Map<String, Property__c>();
        unitAssignmentNames = new List<String>();
        unitAssignmentMap = new  Map<String, Unit_Assignment__c>();
        marketingCampaignNames = new List<String>();
        marketingCampaignMap = new  Map<String, Campaign__c>();
        for(Property__c property: [SELECT Id, Property_Name__c, Property_ID__c 
                                       FROM Property__c 
                                       WHERE Property_Id_is_Negative__c = FALSE
                                       AND Property_Name__c != ''
                                       ORDER BY CreatedDate DESC
                                       LIMIT 1000]){
            propertyMap.put(property.Property_Name__c, property);
            propertyNames.add(property.Property_Name__c);
        }
        for(Unit_Assignment__c assignment: [SELECT Id, Name, Unit_Assignment_Name__c 
                                       FROM Unit_Assignment__c 
                                       WHERE End_Date__c = NULL OR End_Date__c > TODAY
                                       ORDER BY CreatedDate DESC
                                       LIMIT 1000]){
            String name = assignment.Name;
            if(assignment.Unit_Assignment_Name__c != null && assignment.Unit_Assignment_Name__c != ''){
                name += ' - ' + assignment.Unit_Assignment_Name__c;
            }            
            unitAssignmentMap.put(name, assignment);
            unitAssignmentNames.add(name);
            system.debug('unitAssignmentMap'+unitAssignmentMap);
        }
        for(Campaign__c campaign: [SELECT Id, Name, Campaign_Name__c 
                                       FROM Campaign__c 
                                       WHERE (End_Date__c = NULL OR End_Date__c > TODAY)
                                       AND Active__c = TRUE
                                       ORDER BY CreatedDate DESC
                                       LIMIT 1000]){
            String name = campaign.Name;
            if(campaign.Campaign_Name__c != null && campaign.Campaign_Name__c != ''){
                name += ' - ' + campaign.Campaign_Name__c;
            }            
            marketingCampaignMap.put(name, campaign);
            marketingCampaignNames.add(name);
        }       
    }   
    public void GetInventoryDetails(){
        List <UnitDetailsWrapper > wrapperList = new List < UnitDetailsWrapper > ();
        allocType = ApexPages.currentPage().getParameters().get('unitAllocId');
        String allocId = ApexPages.currentPage().getParameters().get('markCampaignId');
        system.debug('allocType: ' + allocType);
        system.debug('allocId: ' + allocId);
        List<Id> inventory ;         
        if(allocType == 'Unit Assignment' && allocId !=''){
            selectedAssignment = allocId;
            system.debug('unitAssignmentMap.get(selectedAssignment).Id'+unitAssignmentMap);
           	inventory =  new List<Id>();
            if(unitAssignmentMap != null ){
            if(unitAssignmentMap.get(selectedAssignment).Id != Null){
            for(Inventory_User__c invUser :[SELECt id,Inventory__c FROM Inventory_User__c 
                                            where Unit_Assignment__c =:  unitAssignmentMap.get(selectedAssignment).Id]){
               system.debug('userIds'+invUser.Inventory__c);
               inventory.add(invUser.Inventory__c);
             }
            }
            system.debug('userIds'+inventory);
        } else if(allocType == 'Marketing Campaign'){
            selectedCampaign = allocId;
            inventory =  new List<Id>();
            system.debug('marketingCampaignMap'+marketingCampaignMap);
            system.debug(' marketingCampaignMap.get(selectedCampaign).Id'+ marketingCampaignMap.get(selectedCampaign));
            if(marketingCampaignMap.get(selectedCampaign).Id != null){
            for(Inventory_User__c invUser :[SELECt id,Inventory__c FROM Inventory_User__c 
                                            where Campaign__c =:  marketingCampaignMap.get(selectedCampaign).Id]){
               inventory.add(invUser.Inventory__c);
                
             }
           }
        }
    }
        system.debug('inventory'+inventory);
        system.debug('selectedCampaign: ' + selectedCampaign);
        system.debug('selectedAssignment: ' + selectedAssignment);
        List<Inventory__c> inventoryList= new List<Inventory__C>();
        if(inventory != null){
        for(Inventory__c inv : [SELECT id,Bedroom_Type_Code__c,ACD_Date__c,Selling_Price__c,
                                Unit_Name__c,Status__c FROM Inventory__c WHERE id IN: inventory AND (Status__c = 'Released' OR
                               Status__c = 'Available' OR Status__c = 'Inventory' OR Status__c = 'Restricted')]){
            inventoryList.add(inv);            
            inventoryMap.put(inv.Unit_Name__c,inv.Id);
         }
            }
         for (Inventory__c inv: inventoryList) {
        	UnitDetailsWrapper details = new UnitDetailsWrapper();
            if(inv.Bedroom_Type_Code__c != null && inv.Bedroom_Type_Code__c != ''){
            details.bedroom = inv.Bedroom_Type_Code__c;
        	} else {
            details.bedroom = '-';
        	}
        	if(inv.ACD_Date__c != null){
            details.acd = inv.ACD_Date__c;
        	} else {
            details.acd = '-';
        	}
        	if(inv.Selling_Price__c != null){
            details.price = String.valueOf(inv.Selling_Price__c);
        	} else {
            details.price = '-';
        	}        
        	details.unitId = inv.Id;
        	details.unit = inv.Unit_Name__c;
        	details.status = inv.Status__c;
        	wrapperList.add(details);
         }
     	 system.debug('wrapper: ' + wrapperList);
     	 unitDetails = JSON.serialize(wrapperList);
      	 system.debug('unitDetails: ' + unitDetails);
    }
    
    public void GetInventoryUserDetails(){
        string invName = ApexPages.currentPage().getParameters().get('invName');
        system.debug(invName);
        List<Inventory_User__c> userDetails = new List<Inventory_User__c>();
        for(Inventory_User__c invUser :[SELECT id,Inventory__c,Name,End_Date__c,Start_Date__c,Comments__c
                                        FROM Inventory_User__c
                                        WHERE Inventory__c=:inventoryMap.get(invName) Limit 1]){
             userDetails.add(invUser);        	
            
        }
       List<InventoryUserWrapper> wrapperList = new List<InventoryUserWrapper>();
        for(Inventory_User__c invUser :userDetails){
       		InventoryUserWrapper wrapper = new InventoryUserWrapper();
            if(invUser.Start_Date__c == null ){
                wrapper.startDate = '-'; 
            }else{
            	wrapper.startDate = String.valueOf(invUser.Start_Date__c);
            }
            if(invUser.End_Date__c == null ){
                wrapper.endDate = '-';
            }else{
            	wrapper.endDate = String.valueOf(invUser.End_Date__c);
            }
            if(invUser.Name == '' || invUser.Name == null){
            	wrapper.Name = '-';
            }else{
                wrapper.Name = invUser.Name;
            }
            if(invUser.Comments__c == null || invUser.Comments__c == ''){
                wrapper.comments = '-';
            }else{
            	wrapper.comments = invUser.Comments__c;
            }
            wrapperList.add(wrapper);
        }
        system.debug('wrapperList'+wrapperList);
        invuserDetails = JSON.serialize(wrapperList);         
    }
   
    public void DelteInventoryUser(){
        string invNames = ApexPages.currentPage().getParameters().get('invNames');
        system.debug(invNames);
        List<String> invNameList = invNames.split(',');
        set<id> invIds = new set<id>();
        for(string name : invNameList ){
            invIds.add(inventoryMap.get(name));
        }
        system.debug('invIds'+invIds);
        List<Inventory_User__c> userDetails = new List<Inventory_User__c>();
        for(Inventory_User__c invUser :[SELECT id FROM Inventory_User__c
                                        WHERE Inventory__c IN : invIds]){
             userDetails.add(invUser); 
        }
        system.debug('userDetails'+userDetails);
       // delete userDetails;        
    }
   /************************************************************************************************
    * @Description : Wrapper Class to wrap the  Unit Details                                        *
    * @Params      : void                                                                           *
    * @Return      : void                                                                           *
    ************************************************************************************************/
   public class UnitDetailsWrapper{
        public string bedroom;
        public string acd;
        public string price;
        public string unitId;
        public string unit;
        public string status;
        public UnitDetailsWrapper(){
        }        
        
    }
      /************************************************************************************************
    * @Description : Wrapper Class to wrap the  InventoryUser Details                                        *
    * @Params      : void                                                                           *
    * @Return      : void                                                                           *
    ************************************************************************************************/
   public class InventoryUserWrapper{
        public string startDate;
        public string EndDate;
        public string comments;
        public string Name;
       public InventoryUserWrapper(){}         
    }
    
     
    public void GetInventoryUserDetails1(){
        string invName = ApexPages.currentPage().getParameters().get('invName');
        system.debug(invName);
        List<Inventory_User__c> userDetails = new List<Inventory_User__c>();
        for(Inventory_User__c invUser :[SELECT id,Inventory__c,Name,End_Date__c,Start_Date__c,Comments__c
                                        FROM Inventory_User__c
                                        WHERE Inventory__c=:inventoryMap.get(invName) Limit 1]){
             userDetails.add(invUser);        	
            
        }
       List<InventoryUserWrapper> wrapperList = new List<InventoryUserWrapper>();
        for(Inventory_User__c invUser :userDetails){
       		InventoryUserWrapper wrapper = new InventoryUserWrapper();
            wrapper.startDate = String.valueOf(invUser.Start_Date__c);
            wrapper.endDate = String.valueOf(invUser.End_Date__c);
            wrapper.Name = invUser.Name;
            wrapper.comments = invUser.Comments__c;
            wrapperList.add(wrapper);
        }
        system.debug('wrapperList'+wrapperList);
        invuserDetails = JSON.serialize(wrapperList);         
    }
}