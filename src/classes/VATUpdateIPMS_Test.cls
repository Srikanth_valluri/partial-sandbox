@isTest(SeeAllData=true)
private class VATUpdateIPMS_Test{
  static testMethod void test_updateVAT_UseCase1(){
    VATUpdateIPMS obj01 = new VATUpdateIPMS();
    List<Id> rids = new list<id>();
  
    Agent_Site__c agSite  = new Agent_Site__c();
    Account agency1 = InitialiseTestData.getCorporateAccount('AgencyVAT');
    insert agency1;
    String  body='';
    agSite.Tax_Registration_Number__c='VATTEST92932';
    agSite.Registration_Certificate_Date__c= System.Today();
    agSite.Name= 'UAE';
    agSite.Agency__c= agency1.Id;
    Insert agSite;
    rids.add(agSite.id);
    VATUpdateIPMS.updateVAT(rids);
    
    
    body= SetResponse(agSite.id);
    
    AsyncVATUpdate.parseRegnUpdateResponse(body);
    
 }
    
    private static string SetResponse(String idval){
        string body = '';
        body+='<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">';
        body+='<env:Header/>';
        body+='<env:Body>';
          body+='<OutputParameters xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/process/">';
             body+='<X_RESPONSE_MESSAGE>';
                body+='<X_RESPONSE_MESSAGE_ITEM>';
                   body+='<PARAM_ID>'+idval+'</PARAM_ID>';
                   body+='<PROC_STATUS>S</PROC_STATUS>';
                   body+='<PROC_MESSAGE>VAT Details Update</PROC_MESSAGE>';
                   body+='<ATTRIBUTE1>810117</ATTRIBUTE1>';
                   body+='<ATTRIBUTE2>1274778</ATTRIBUTE2>';
                   body+='<ATTRIBUTE3>1274779</ATTRIBUTE3>'; 
                body+='</X_RESPONSE_MESSAGE_ITEM>';
             body+='</X_RESPONSE_MESSAGE>';
             body+='<X_RETURN_STATUS>S</X_RETURN_STATUS>';
             body+='<X_RETURN_MESSAGE>Process Completed successfully...</X_RETURN_MESSAGE>';
          body+='</OutputParameters>';
        body+='</env:Body>';
        body+='</env:Envelope>';
        body=body.trim();
        body= body.replaceAll('null', '');
        body=body.trim();
        
        return body;
    }
   
}