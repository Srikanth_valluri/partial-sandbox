@isTest
public class AccountSendEmailExtensionTest {
    static testMethod void test1(){
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                      Party_ID__c = '63062',
                                      RecordtypeId = rtId,
                                      Email__pc = 'test@mailinator.com',
                                      Email_2__pc ='test1@mailinator.com',
                                      Email_3__pc ='test2@mailinator.com',
                                      Email_4__pc ='test3@mailinator.com',
                                      Email_5__pc ='test4@mailinator.com');
        insert account;
        AccountSendEmailExtension accountSendEmailObj;
        ApexPages.StandardController controller = new ApexPages.StandardController(account);
        PageReference accountSendEmail = Page.AccountSendEmailPage;
        Test.setCurrentPage(accountSendEmail);
        ApexPages.currentPage().getParameters().put('id', account.Id);
        accountSendEmailObj = new AccountSendEmailExtension(controller);
        accountSendEmailObj.docSearchKey = 'Test';
        accountSendEmailObj.emailSearchKey ='Test';
        accountSendEmailObj.counterForAttachment=0;
        accountSendEmailObj.listSizeForAttachment=10;
        accountSendEmailObj.counter=0;
        accountSendEmailObj.list_size=10;
        accountSendEmailObj.getAttachmentsOnChange();
        accountSendEmailObj.searchDocuments();
        accountSendEmailObj.searchTemplates();
        accountSendEmailObj.beginning();
        //accountSendEmailObj.previous();
        accountSendEmailObj.next();
        accountSendEmailObj.end();
        accountSendEmailObj.getDisablePrevious();
        accountSendEmailObj.getDisableNext();
        accountSendEmailObj.beginningForAttachment();
        //accountSendEmailObj.previousForAttachment();
        accountSendEmailObj.nextForAttachment();
        //accountSendEmailObj.endForAttachment();
        accountSendEmailObj.setEmailTemplate();
        
    }
    static testMethod void testBu1(){
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                      Party_ID__c = '63062',
                                      RecordtypeId = rtId,
                                      Email__pc = 'test@mailinator.com',
                                      Email_2__pc ='test1@mailinator.com',
                                      Email_3__pc ='test2@mailinator.com',
                                      Email_4__pc ='test3@mailinator.com',
                                      Email_5__pc ='test4@mailinator.com');
        insert account;
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        //String fmAmount = String.valueOf(Decimal.valueOf(Label.Robo_Call_FM_Outstanding_Amount) + 100.00);
        Booking_unit__c objBU = new Booking_Unit__c (Booking__c = booking.Id,
                                                     Owner__c = account.Id
                                                     ,Send_Bulk_Email__c =true
                                                     , Registration_Status_Code__c ='xx'
                                                     ,Tenant__c = account.Id
                                                     ,Send_Tenant_Email__c = true
                                                     , DLDN_Notice__c ='afa'
                                                     );
        insert objBU;
        AccountSendEmailExtension accountSendEmailObj;
        ApexPages.StandardController controller = new ApexPages.StandardController(account);
        PageReference accountSendEmail = Page.AccountSendEmailPage;
        Test.setCurrentPage(accountSendEmail);
        ApexPages.currentPage().getParameters().put('id', objBU.Id);
        accountSendEmailObj = new AccountSendEmailExtension(controller);
        accountSendEmailObj.docSearchKey = 'Test';
        accountSendEmailObj.emailSearchKey ='Test';
        accountSendEmailObj.getAttachmentsOnChange();
        accountSendEmailObj.searchDocuments();
        accountSendEmailObj.searchTemplates();
        accountSendEmailObj.setEmailTemplate();
        /*accountSendEmailObj.counterForAttachment=0;
        accountSendEmailObj.listSizeForAttachment=10;
        accountSendEmailObj.counter=0;
        accountSendEmailObj.list_size=10;
        accountSendEmailObj.getAttachmentsOnChange();
        accountSendEmailObj.searchDocuments();
        accountSendEmailObj.searchTemplates();
        accountSendEmailObj.beginning();
        //accountSendEmailObj.previous();
        accountSendEmailObj.next();
        accountSendEmailObj.end();
        accountSendEmailObj.getDisablePrevious();
        accountSendEmailObj.getDisableNext();
        accountSendEmailObj.beginningForAttachment();
        //accountSendEmailObj.previousForAttachment();
        accountSendEmailObj.nextForAttachment();
        //accountSendEmailObj.endForAttachment();
        accountSendEmailObj.setEmailTemplate();*/
        
    }
    
    static testMethod void test2(){
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                      Party_ID__c = '63062',
                                      RecordtypeId = rtId,
                                      Email__pc = 'test@mailinator.com',
                                      Email_2__pc ='test1@mailinator.com',
                                      Email_3__pc ='test2@mailinator.com',
                                      Email_4__pc ='test3@mailinator.com',
                                      Email_5__pc ='test4@mailinator.com');
        insert account;
        AccountSendEmailExtension accountSendEmailObj;
        ApexPages.StandardController controller = new ApexPages.StandardController(account);
        PageReference accountSendEmail = Page.AccountSendEmailPage;
        Test.setCurrentPage(accountSendEmail);
        ApexPages.currentPage().getParameters().put('id', account.Id);
        accountSendEmailObj = new AccountSendEmailExtension(controller);
        accountSendEmailObj.fromAddress = 'Test1@gmail.com';
        accountSendEmailObj.toAddress = 'Test2@gmail.com';
        accountSendEmailObj.bccAddress = 'Test3@gmail.com';
        accountSendEmailObj.ccAddress =  'Test4@gmail.com';
        accountSendEmailObj.subject =  'Test5@gmail.com';
        accountSendEmailObj.replyToAddress =  'Test6@gmail.com';
        accountSendEmailObj.emailBody  =  'Test7@gmail.com';
        accountSendEmailObj.docJosn = '[{"docName":"8060681576676034430.pdf","docBodyOrId":"ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk","docType":"new"}]';
        accountSendEmailObj.sendEmail();
    }
    
    
    static testMethod void test3(){
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                      Party_ID__c = '63062',
                                      RecordtypeId = rtId,
                                      Email__pc = 'test@mailinator.com',
                                      Email_2__pc ='test1@mailinator.com',
                                      Email_3__pc ='test2@mailinator.com',
                                      Email_4__pc ='test3@mailinator.com',
                                      Email_5__pc ='test4@mailinator.com');
        insert account;
        AccountSendEmailExtension accountSendEmailObj;
        ApexPages.StandardController controller = new ApexPages.StandardController(account);
        PageReference accountSendEmail = Page.AccountSendEmailPage;
        Test.setCurrentPage(accountSendEmail);
        /*Folder objFolder  =  [SELECT 
                              Id
                              , Name
                              , Type
                              FROM 
                              Folder
                              WHERE
                              Type = 'Email'
                              LIMIT 1]; 
        List<EmailTemplate> emailTemplateList =  [ SELECT  Id
                                                  , Name
                                                  , Body
                                                  , HtmlValue
                                                  , MarkUp
                                                  , Subject
                                                  , TemplateType
                                                  , Description
                                                  , BrandTemplateId
                                                  FROM
                                                  EmailTemplate
                                                  WHERE FolderId =: objFolder.Id
                                                  LIMIT 1];*/
        
        ApexPages.currentPage().getParameters().put('id', account.Id);
        accountSendEmailObj = new AccountSendEmailExtension(controller);
        accountSendEmailObj.docSearchKey = 'Test';
        accountSendEmailObj.emailSearchKey ='Test';
        accountSendEmailObj.counterForAttachment=0;
        accountSendEmailObj.listSizeForAttachment=10;
        accountSendEmailObj.counter=0;
        accountSendEmailObj.list_size=10;
        accountSendEmailObj.getAttachmentsOnChange();
        accountSendEmailObj.searchDocuments();
        accountSendEmailObj.searchTemplates();
        accountSendEmailObj.beginning();
        //accountSendEmailObj.previous();
        accountSendEmailObj.next();
        accountSendEmailObj.end();
        accountSendEmailObj.getDisablePrevious();
        accountSendEmailObj.getDisableNext();
        accountSendEmailObj.beginningForAttachment();
        //accountSendEmailObj.previousForAttachment();
        accountSendEmailObj.nextForAttachment();
        accountSendEmailObj.getTotal_size();
        accountSendEmailObj.getPageNumber();
        accountSendEmailObj.getTotalPages();
        //accountSendEmailObj.endForAttachment();
        //accountSendEmailObj.selectedEmailTemplate = emailTemplateList[0].Id;
        Map<Id,EmailTemplate> mapOfIdToEmailTemplate = new Map<Id,EmailTemplate>();
        
        
    }
    
    
    static testMethod void test4(){
        Id businessAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account account = new Account( Name = 'Test Name' ,
                                      First_Name__c='Test FirstName',
                                      Last_Name__c='Test LastName',
                                      RecordtypeId = businessAccRTId,
                                      Email__c  ='test1@mailinator.com',
                                      Email_2__c  ='test2@mailinator.com',
                                      Email_3__c  ='test3@mailinator.com'
                                     );
        insert account;
        AccountSendEmailExtension accountSendEmailObj;
        ApexPages.StandardController controller = new ApexPages.StandardController(account);
        PageReference accountSendEmail = Page.AccountSendEmailPage;
        Test.setCurrentPage(accountSendEmail);
        /*Folder objFolder  =  [SELECT 
                              Id
                              , Name
                              , Type
                              FROM 
                              Folder
                              WHERE
                              Type = 'Email'
                              LIMIT 1]; 
        List<EmailTemplate> emailTemplateList =  [ SELECT  Id
                                                  , Name
                                                  , Body
                                                  , HtmlValue
                                                  , MarkUp
                                                  , Subject
                                                  , TemplateType
                                                  , Description
                                                  , BrandTemplateId
                                                  FROM
                                                  EmailTemplate
                                                  WHERE FolderId =: objFolder.Id
                                                  LIMIT 1];*/
        
        ApexPages.currentPage().getParameters().put('id', account.Id);
        accountSendEmailObj = new AccountSendEmailExtension(controller);
        accountSendEmailObj.docSearchKey = 'Test';
        accountSendEmailObj.emailSearchKey ='Test';
        accountSendEmailObj.counterForAttachment=0;
        accountSendEmailObj.totalSizeForAttachment=40;
        accountSendEmailObj.listSizeForAttachment=10;
        accountSendEmailObj.counter=0;
        accountSendEmailObj.list_size=10;
        accountSendEmailObj.getAttachmentsOnChange();
        accountSendEmailObj.searchDocuments();
        accountSendEmailObj.searchTemplates();
        accountSendEmailObj.beginning();
        //accountSendEmailObj.previous();
        accountSendEmailObj.next();
        accountSendEmailObj.end();
        accountSendEmailObj.getDisablePrevious();
        accountSendEmailObj.getDisableNext();
        accountSendEmailObj.beginningForAttachment();
        //accountSendEmailObj.previousForAttachment();
        accountSendEmailObj.nextForAttachment();
        accountSendEmailObj.getTotal_size();
        accountSendEmailObj.getPageNumber();
        accountSendEmailObj.getTotalPages();
        accountSendEmailObj.getDisablePreviousForAttachment();
        accountSendEmailObj.getDisableNextForAttachment();
        accountSendEmailObj.getTotalSizeForAttachment();
        accountSendEmailObj.getTotalPagesForAttachment();
        accountSendEmailObj.getPageNumberForAttachment();
        //accountSendEmailObj.docSerchToNull();
        //accountSendEmailObj.selectedEmailTemplate = emailTemplateList[0].Id;
        Map<Id,EmailTemplate> mapOfIdToEmailTemplate = new Map<Id,EmailTemplate>();
        
        
    }
    
    static testMethod void test5(){
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                      Party_ID__c = '63062',
                                      RecordtypeId = rtId,
                                      Email__pc = 'test@mailinator.com',
                                      Email_2__pc ='test1@mailinator.com',
                                      Email_3__pc ='test2@mailinator.com',
                                      Email_4__pc ='test3@mailinator.com',
                                      Email_5__pc ='test4@mailinator.com');
        insert account;
        AccountSendEmailExtension accountSendEmailObj;
        ApexPages.StandardController controller = new ApexPages.StandardController(account);
        PageReference accountSendEmail = Page.AccountSendEmailPage;
        Test.setCurrentPage(accountSendEmail);
        ApexPages.currentPage().getParameters().put('id', account.Id);
        accountSendEmailObj = new AccountSendEmailExtension(controller);
        accountSendEmailObj.fromAddress = 'Test1@gmail.com';
        accountSendEmailObj.toAddress = 'Test2@gmail.com';
        accountSendEmailObj.bccAddress = 'Test3@gmail.com';
        accountSendEmailObj.ccAddress =  'Test4@gmail.com';
        accountSendEmailObj.subject =  'Test5@gmail.com';
        accountSendEmailObj.replyToAddress =  'Test6@gmail.com';
        accountSendEmailObj.emailBody  =  'Test7@gmail.com';
        
        Document docmentObj = new Document();
        docmentObj.Name = 'TEST.png';
        docmentObj.Body =  EncodingUtil.base64Decode('ZGF0YTphcHBsaWNhdGlvbi9wZG');
        docmentObj.Type = 'TEST';
        docmentObj.FolderId = UserInfo.getUserId();
        insert docmentObj;
        
        
        List<Document> documentList = [SELECT Id
                                       , Name
                                       , Body
                                       , Type
                                       FROM
                                       Document Limit 1];
        accountSendEmailObj.docJosn = '[{"docName":"8060681576676034430.pdf","docBodyOrId":" '+documentList[0].Id+' ","docType":"existing"}]';
        accountSendEmailObj.sendEmail();
    }
    
    static testMethod void test6(){
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                      Party_ID__c = '63062',
                                      RecordtypeId = rtId,
                                      Email__pc = 'test@mailinator.com',
                                      Email_2__pc ='test1@mailinator.com',
                                      Email_3__pc ='test2@mailinator.com',
                                      Email_4__pc ='test3@mailinator.com',
                                      Email_5__pc ='test4@mailinator.com');
        insert account;
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        //String fmAmount = String.valueOf(Decimal.valueOf(Label.Robo_Call_FM_Outstanding_Amount) + 100.00);
        Booking_unit__c objBU = new Booking_Unit__c (Booking__c = booking.Id,
                                                     Owner__c = account.Id
                                                     ,Send_Bulk_Email__c =true
                                                     , Registration_Status_Code__c ='xx'
                                                     ,Tenant__c = account.Id
                                                     ,Send_Tenant_Email__c = true
                                                     , DLDN_Notice__c ='afa'
                                                     );
        insert objBU;
        AccountSendEmailExtension accountSendEmailObj;
        ApexPages.StandardController controller = new ApexPages.StandardController(account);
        PageReference accountSendEmail = Page.AccountSendEmailPage;
        Test.setCurrentPage(accountSendEmail);
        ApexPages.currentPage().getParameters().put('id', objBU.Id);
        accountSendEmailObj = new AccountSendEmailExtension(controller);
        Map<Id,EmailTemplate> mapOfIdToEmailTemplate = new Map<Id,EmailTemplate>();
        List<EmailTemplate> objTemplateList = [ SELECT
                                                   Id
                                                   , Name
                                                   , Body
                                                   , HtmlValue
                                                   , MarkUp
                                                   , Subject
                                                   , TemplateType
                                                   , Description
                                                   , BrandTemplateId
                                               FROM
                                               EmailTemplate
                                               LIMIT 1];
                                                   accountSendEmailObj.selectedEmailTemplate  = objTemplateList[0].Id;                                              
        for(EmailTemplate obj :objTemplateList ) {
            mapOfIdToEmailTemplate.put(obj.Id,obj);
        }
        accountSendEmailObj.mapOfEmailTemplate = mapOfIdToEmailTemplate;                                        
            
            accountSendEmailObj.setEmailTemplate();
        
    }
    
    static testMethod void test7(){
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                      Party_ID__c = '63062',
                                      RecordtypeId = rtId,
                                      Email__pc = 'test@mailinator.com',
                                      Email_2__pc ='test1@mailinator.com',
                                      Email_3__pc ='test2@mailinator.com',
                                      Email_4__pc ='test3@mailinator.com',
                                      Email_5__pc ='test4@mailinator.com');
        insert account;
        AccountSendEmailExtension accountSendEmailObj;
        ApexPages.StandardController controller = new ApexPages.StandardController(account);
        PageReference accountSendEmail = Page.AccountSendEmailPage;
        Test.setCurrentPage(accountSendEmail);
        ApexPages.currentPage().getParameters().put('id', account.Id);
        accountSendEmailObj = new AccountSendEmailExtension(controller);
        accountSendEmailObj.fromAddress = 'Test1@gmail.com';
        accountSendEmailObj.toAddress = '';
        accountSendEmailObj.bccAddress = 'Test3@gmail.com';
        accountSendEmailObj.ccAddress =  'Test4@gmail.com';
        accountSendEmailObj.subject =  'Test5@gmail.com';
        accountSendEmailObj.replyToAddress =  'Test6@gmail.com';
        accountSendEmailObj.emailBody  =  'Test7@gmail.com';
        accountSendEmailObj.docJosn = '[{"docName":"8060681576676034430","docBodyOrId":"ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk","docType":"abc"}]';
        accountSendEmailObj.sendEmail();
    }
     
        static testMethod void test8(){
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                      Party_ID__c = '63062',
                                      RecordtypeId = rtId,
                                      Email__pc = 'test@mailinator.com',
                                      Email_2__pc ='test1@mailinator.com',
                                      Email_3__pc ='test2@mailinator.com',
                                      Email_4__pc ='test3@mailinator.com',
                                      Email_5__pc ='test4@mailinator.com');
        insert account;
        AccountSendEmailExtension accountSendEmailObj;
        ApexPages.StandardController controller = new ApexPages.StandardController(account);
        PageReference accountSendEmail = Page.AccountSendEmailPage;
        Test.setCurrentPage(accountSendEmail);
        ApexPages.currentPage().getParameters().put('id', account.Id);
        accountSendEmailObj = new AccountSendEmailExtension(controller);
        Map<Id,EmailTemplate> mapOfIdToEmailTemplate = new Map<Id,EmailTemplate>();
        List<EmailTemplate> objTemplateList = [ SELECT
                                                   Id
                                                   , Name
                                                   , Body
                                                   , HtmlValue
                                                   , MarkUp
                                                   , Subject
                                                   , TemplateType
                                                   , Description
                                                   , BrandTemplateId
                                               FROM
                                               EmailTemplate
                                               WHERE TemplateType='Custom'
                                               LIMIT 1];
                                                   accountSendEmailObj.selectedEmailTemplate  = objTemplateList[0].Id;                                              
        for(EmailTemplate obj :objTemplateList ) {
            mapOfIdToEmailTemplate.put(obj.Id,obj);
        }
        accountSendEmailObj.mapOfEmailTemplate = mapOfIdToEmailTemplate;                                        
            
            accountSendEmailObj.setEmailTemplate();
        
    }
    
    static testMethod void test9(){
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                      Party_ID__c = '63062',
                                      RecordtypeId = rtId,
                                      Email__pc = 'test@mailinator.com',
                                      Email_2__pc ='test1@mailinator.com',
                                      Email_3__pc ='test2@mailinator.com',
                                      Email_4__pc ='test3@mailinator.com',
                                      Email_5__pc ='test4@mailinator.com');
        insert account;
        AccountSendEmailExtension accountSendEmailObj;
        ApexPages.StandardController controller = new ApexPages.StandardController(account);
        PageReference accountSendEmail = Page.AccountSendEmailPage;
        Test.setCurrentPage(accountSendEmail);
        ApexPages.currentPage().getParameters().put('id', account.Id);
        accountSendEmailObj = new AccountSendEmailExtension(controller);
        Map<Id,EmailTemplate> mapOfIdToEmailTemplate = new Map<Id,EmailTemplate>();
        List<EmailTemplate> objTemplateList = [ SELECT
                                                   Id
                                                   , Name
                                                   , Body
                                                   , HtmlValue
                                                   , MarkUp
                                                   , Subject
                                                   , TemplateType
                                                   , Description
                                                   , BrandTemplateId
                                               FROM
                                               EmailTemplate
                                               WHERE TemplateType='Visualforce'
                                               LIMIT 1];
                                                   accountSendEmailObj.selectedEmailTemplate  = objTemplateList[0].Id;                                              
        for(EmailTemplate obj :objTemplateList ) {
            mapOfIdToEmailTemplate.put(obj.Id,obj);
        }
        accountSendEmailObj.mapOfEmailTemplate = mapOfIdToEmailTemplate;                                        
            
            accountSendEmailObj.setEmailTemplate();
        
    }
    @isTest
    public static void testgetStringValue() {
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                      Party_ID__c = '63062',
                                      RecordtypeId = rtId,
                                      Email__pc = 'test@mailinator.com',
                                      Email_2__pc ='test1@mailinator.com',
                                      Email_3__pc ='test2@mailinator.com',
                                      Email_4__pc ='test3@mailinator.com',
                                      Email_5__pc ='test4@mailinator.com');
        insert account;
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        //String fmAmount = String.valueOf(Decimal.valueOf(Label.Robo_Call_FM_Outstanding_Amount) + 100.00);
        Booking_unit__c objBU = new Booking_Unit__c (Booking__c = booking.Id,
                                                     Owner__c = account.Id
                                                     ,Send_Bulk_Email__c =true
                                                     , Registration_Status_Code__c ='xx'
                                                     ,Tenant__c = account.Id
                                                     ,Send_Tenant_Email__c = true
                                                     , DLDN_Notice__c ='afa'
                                                     );
        insert objBU;
        case objCase = new Case ( Booking_Unit__c = objBU.id);
        Test.startTest();
            AccountSendEmailExtension.getStringValue('Booking__c', (sObject)objBU);
        Test.stopTest();
    }
    
    
}