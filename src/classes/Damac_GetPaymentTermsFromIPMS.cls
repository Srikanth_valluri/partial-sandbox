public class Damac_GetPaymentTermsFromIPMS {

    public static Map <String, String> getPaymentTerms (String buildingID) {
        String servicename = 'IPMS_Payment_Terms';
        IPMS_Integration_Settings__mdt ipms= [select Endpoint_URL__c,Password__c,  Username__c from IPMS_Integration_Settings__mdt where DeveloperName = :serviceName];
        String body = '{"RETRIEVE_Input":{ "RESTHeader":{ "Responsibility":"ONT_ICP_SUPER_USER", "RespApplication":"ONT", "SecurityGroup":"STANDARD", "NLSLanguage":"AMERICAN" }, "InputParameters":{ "P_REQUEST_NUMBER":"A2232","P_SOURCE_SYSTEM":"SFDC","P_REQUEST_NAME":"GET_MASTER_PAYMENT_TERMS","P_REQUEST_MESSAGE": {"PARAM_ID":"'+buildingID+'"}}}}';
        HTTPRequest req = new HTTPRequest ();
        req.SetMethod ('POST');
        
        req.setEndpoint (ipms.Endpoint_URL__c);
        req.setBody (body);
        req.setHeader ('Content-Type', 'application/json');
        req.setHeader ('Authorization', 'Basic b3JhY2xlX3VzZXI6Y3JwMXVzZXI=');
        req.setTimeOut (120000);
        HTTP http = new HTTP ();
        HTTPResponse res = http.send (req);
        System.Debug (res.getBody());
        Map <String, String> result = new Map <String, String> ();
        result = parseResponse (res.getBody ());
        System.Debug (result);
        return result;
    }
    public static Map <String, String> parseResponse (string jsonString) {
        Map <String, String> result = new Map <String, String> ();
        Damac_paymentTermsJSON obj = Damac_paymentTermsJSON.parse(jsonString);
        Damac_paymentTermsJSON.OutputParameters resp = obj.OutputParameters;
        if (resp != NULL) {
            if (resp.X_RESPONSE_MESSAGE != NULL) {
                for (Damac_paymentTermsJSON.X_RESPONSE_MESSAGE_ITEM item :resp.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM) {
                    result.put (item.ATTRIBUTE1, item.ATTRIBUTE2);
                }
            }
        }
        return result;        
    }
}