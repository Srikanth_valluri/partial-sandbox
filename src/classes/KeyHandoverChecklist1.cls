public without sharing class KeyHandoverChecklist1 {
    String strCaseId;
    public UnitDetailsService.BookinUnitDetailsWrapper objWrap;
    static set<String> setNationalities;
    public map<Id,Case> mapId_Case;
    public boolean blnError {get;set;}
    public list<SR_Attachments__c> lstCaseAttachment;
    
    public KeyHandoverChecklist1(ApexPages.StandardController sc){
        blnError = false;
        strCaseId = sc.getId();
        init();
    }
    
    public void init(){
        mapId_Case = new map<Id,Case>([Select Id
                                           , RecordtypeId
                                           , Recordtype.DeveloperName
                                           , Unit_Name__c
                                           , ParentId
                                           , AccountId
                                           , Account.Party_Id__c
                                           , Account.Nationality__c
                                           , Account.Nationality__pc
                                           , Account.Country__c
                                           , Account.Country__pc
                                           , Account.IsPersonAccount
                                           , Booking_Unit__c
                                           , Booking_Unit__r.Registration_ID__c
                                           , Booking_Unit__r.Property_City__c
                                           , Booking_Unit__r.Property_Name__c
                                           , Booking_Unit__r.Building_ID__c
                                           , Booking_Unit__r.Permitted_Use__c
                                           , Booking_Unit__r.Bedroom_Type__c
                                           , Booking_Unit__r.Unit_Type__c
                                           , Booking_Unit__r.Booking__c
                                    From Case 
                                    Where Id =: strCaseId]);
        
        if(!mapId_Case.isEmpty()){
            lstCaseAttachment = new list<SR_Attachments__c>();
            /*
            // This is a child case
            if(mapId_Case.get(strCaseId).ParentId != null){
                
                if(mapId_Case.get(strCaseId).Booking_Unit__r.Booking__c != null
                && mapId_Case.get(strCaseId).Booking_Unit__r.Registration_ID__c != null){
                    
                    fetchUnitDetails(mapId_Case.get(strCaseId).Booking_Unit__r.Registration_ID__c);
                    system.debug('*****objWrap*****'+objWrap);
                    if(objWrap != null){
                        system.debug('objWrap.strDispute*****'+objWrap.strDispute);
                        system.debug('objWrap.strEnforcement*****'+objWrap.strEnforcement);
                        system.debug('objWrap.strLitigation*****'+objWrap.strLitigation);
                        system.debug('objWrap.strCounterCase*****'+objWrap.strCounterCase);
                        
                        // Below values are temporary for testing purpose
                        
                        objWrap.strFMPCC = 'Y';
                        objWrap.strPCC = 'Y';
                        objWrap.strBCCAvailable = 'Y';
                        
                        // temporary values section ends here
                        
                        String strFlags = '';
                        if(String.isNotBlank(objWrap.strDispute)
                        && objWrap.strDispute.equalsIgnoreCase('Y')){
                            strFlags = 'Dispute, ';
                        }
                        if(String.isNotBlank(objWrap.strEnforcement)
                        && objWrap.strEnforcement.equalsIgnoreCase('Y')){
                            strFlags = strFlags + 'Enforcement, ';
                        }
                        if(String.isNotBlank(objWrap.strLitigation)
                        && objWrap.strLitigation.equalsIgnoreCase('Y')){
                            strFlags = strFlags + 'Litigation, ';
                        }
                        if(String.isNotBlank(objWrap.strCounterCase)
                        && objWrap.strCounterCase.equalsIgnoreCase('Y')){
                            strFlags = strFlags + 'Counter Case, ';
                        }
                        
                        if(strFlags != ''){
                            strFlags = strFlags.removeEnd(', ');
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Error : '+ strFlags+' flag(s) exists for the selected unit. You will be unable to proceed.');
                            ApexPages.addMessage(myMsg);
                            blnError = true;
                        }else{
                            if(String.isNotBlank(objWrap.strAreaVariation)
                            && decimal.valueOf(objWrap.strAreaVariation) > 12){
                                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Error : Area variation for the selected unit exceeds 12%.');
                                ApexPages.addMessage(myMsg);
                                blnError = true;
                            }else if(String.isNotBlank(objWrap.strFMPCC)
                            && objWrap.strFMPCC.contains('N')){
                                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Error : FM PCC is not complete for the selected unit.');
                                ApexPages.addMessage(myMsg);
                                blnError = true;
                            }
                            else if(String.isNotBlank(objWrap.strPCC)
                            && objWrap.strPCC.contains('N')){
                                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Error : PCC is not complete for the selected unit.');
                                ApexPages.addMessage(myMsg);
                                blnError = true;
                            }
                            if(String.isNotBlank(objWrap.strUnderTermination)
                            && objWrap.strUnderTermination.contains('Y')){
                                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Error : Selected unit is under termnation.');
                                ApexPages.addMessage(myMsg);
                                blnError = true;
                            }
                            else if(String.isNotBlank(objWrap.strBCCAvailable)
                            && objWrap.strBCCAvailable.contains('N')){
                                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Error : BCC is not available for the selected unit.');
                                ApexPages.addMessage(myMsg);
                                blnError = true;
                            }else{
                                System.debug('*****All flags valid*****');
                            }
                        } // end of no DELC flags
                    }else{
                        system.debug('*****In the future raise error here*****');
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Error : No unit data exist for the selected unit.');
                        ApexPages.addMessage(myMsg);
                    }
                }else{
                    system.debug('*****Raise error that Booking or RegId doesn not exist*****');
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Error : The selected unit does not have a RegId or a booking associated with it.');
                    ApexPages.addMessage(myMsg);
                    blnError = true;
                }
                
            }else{
                // This is a parent case
            }
            */
        } // map not empty
    } // end of init method
    
    public pagereference cancel(){
        PageReference pgRef = new PageReference('/'+strCaseId);
        return pgRef;
    }
    
    public pageReference confirmationFromUser(){
        /*
        List<SR_Attachments__c> lstOldDocs = [Select Id
                                                     , Case__c 
                                              from SR_Attachments__c
                                              where Case__c =:strCaseId
                                              and ((Not Name Like '%Handover Checklist%')
                                              and (Not Name Like '%Letter of Discharge%')
                                              and (Not Name Like '%Key Release%'))];
        */
        List<SR_Attachments__c> lstOldDocs = [Select Id
                                                     , Case__c 
                                              from SR_Attachments__c
                                              where Case__c =:strCaseId];
        PageReference pgRef = new PageReference('/'+strCaseId);
        setNationalities = new set<String>();
        setNationalities.addAll(Label.GCCNationalities.split(','));
        
        if(!mapId_Case.isEmpty()){
            system.debug('*****parentId*****'+mapId_Case.get(strCaseId).ParentId);
            if(mapId_Case.get(strCaseId).ParentId == null){
                Map<Id,boolean> mapBuyerExists = new map<Id,boolean>();
                set<Id> setBookingIds = new set<Id>();
                for(Case objC : [Select Id
                                        , ParentId
                                        , RecordtypeId
                                        , Recordtype.DeveloperName
                                        , Booking_Unit__c
                                        , Booking_Unit__r.Booking__c 
                                 From Case 
                                 Where ParentId =: strCaseId
                                 and RecordType.DeveloperName ='Handover']){
                    if(objC.Booking_Unit__r.Booking__c != null){
                        setBookingIds.add(objC.Booking_Unit__r.Booking__c);
                    }
                }
                if(!setBookingIds.isEmpty()){
                    Integer jbCount = 1;
                    for(Buyer__c objBuyer : [Select Id
                                               , Booking__c
                                               , Booking__r.Account__c
                                               , Primary_Buyer__c
                                               , Account__c
                                               , Account__r.Name
                                               , Account__r.IsPersonAccount
                                               , Account__r.Country__c
                                               , Account__r.Country__pc
                                               , Account__r.Nationality__c
                                               , Account__r.Nationality__pc
                                          From Buyer__c 
                                          Where Booking__r.Account__c =: mapId_Case.get(strCaseId).AccountId
                                          and Booking__c IN : setBookingIds]){
                                          // SHOULD I also chk if Case.Account = Buyer.Account and Primary is True?
                        system.debug('objBuyer.Account__c*****'+objBuyer.Account__c);
                        if(!mapBuyerExists.containsKey(objBuyer.Account__c)){
                            system.debug('*****INSIDE*****');
                            String customerType = objBuyer.Account__r.isPersonAccount ? 'INDIVIDUAL' : 'CORPORATE';
                            String strApplicableNationality = setCustomerType(objBuyer.Account__r);
                            String subProcess = '';
                            String customerName = '';
                            if(objBuyer.Account__r.isPersonAccount){
                                if(objBuyer.Primary_Buyer__c){
                                    subProcess = 'INDIVIDUAL';
                                    customerName = 'Buyer';
                                }else{
                                    subProcess = 'INDIVIDUALJB';
                                    customerName = 'JB '+jbCount;
                                    jbCount++;
                                }
                            }else{
                                if(objBuyer.Primary_Buyer__c){
                                   subProcess = 'CORPORATE';
                                   customerName = 'Buyer';                                 
                                }else{
                                    subProcess = 'CORPORATEJB';
                                    customerName = 'JB '+jbCount;
                                    jbCount++;
                                }
                            }
                            getKeyHandover(
                            mapId_Case.get(strCaseId).Booking_Unit__r.Registration_ID__c,
                            subProcess, 
                            mapId_Case.get(strCaseId).Booking_Unit__r.Property_City__c, 
                            mapId_Case.get(strCaseId).Booking_Unit__r.Property_Name__c, 
                            mapId_Case.get(strCaseId).Booking_Unit__r.Building_ID__c, 
                            mapId_Case.get(strCaseId).Booking_Unit__r.Permitted_Use__c, 
                            mapId_Case.get(strCaseId).Booking_Unit__r.Bedroom_Type__c, 
                            '', 
                            mapId_Case.get(strCaseId).Booking_Unit__r.Unit_Type__c, 
                            strApplicableNationality, 
                            customerType, 
                            'No', 
                            strCaseId, 
                            null,
                            objBuyer.Account__c,
                            customerName);
                            // earlier last parameter was objBuyer.Account__r.Name
                            // updated on 17/09/2018
                            
                            mapBuyerExists.put(objBuyer.Account__c,true);
                        }
                    }
                }
            }else{
                // Do this for the child case
                system.debug('*****This is a child case*****');
                CaseTriggerHandlerHO objCls = new CaseTriggerHandlerHO();
                objCls.afterInsert(mapId_Case);
                /*
                String customerType = mapId_Case.get(strCaseId).Account.isPersonAccount ? 'INDIVIDUAL' : 'CORPORATE';
                String strApplicableNationality = setCustomerType(mapId_Case.get(strCaseId).Account);
                String subProcess = '';
                if(mapId_Case.get(strCaseId).Account.isPersonAccount){
                    subProcess = 'INDIVIDUAL'; 
                }else{
                    subProcess = 'CORPORATE';
                }
                
                getKeyHandover(
                mapId_Case.get(strCaseId).Booking_Unit__r.Registration_ID__c,
                subProcess, 
                mapId_Case.get(strCaseId).Booking_Unit__r.Property_City__c, 
                mapId_Case.get(strCaseId).Booking_Unit__r.Property_Name__c, 
                mapId_Case.get(strCaseId).Booking_Unit__r.Building_ID__c, 
                mapId_Case.get(strCaseId).Booking_Unit__r.Permitted_Use__c, 
                mapId_Case.get(strCaseId).Booking_Unit__r.Bedroom_Type__c, 
                '', 
                mapId_Case.get(strCaseId).Booking_Unit__r.Unit_Type__c, 
                strApplicableNationality, 
                customerType, 
                'No', 
                strCaseId, 
                mapId_Case.get(strCaseId).Booking_Unit__c,
                null,
                '');
                */
            }
        }
        if(!lstCaseAttachment.isEmpty()){
            insert lstCaseAttachment;
        }
        if(!lstOldDocs.isEmpty()){
            delete lstOldDocs;
        }
        return pgRef;
    }
    
    public string setCustomerType(Account objA){
        system.debug('************objA*******');
        String strApplicableNationality = '';
        if((!objA.isPersonAccount && String.isNotBlank(objA.Nationality__c) && objA.Nationality__c == 'UAE'
            || objA.isPersonAccount && String.isNotBlank(objA.Nationality__pc) && objA.Nationality__pc == 'UAE') 
            && (!objA.isPersonAccount && String.isNotBlank(objA.Country__c) && objA.Country__c == 'United Arab Emirates'
            || objA.isPersonAccount && String.isNotBlank(objA.Country__pc) && objA.Country__pc == 'United Arab Emirates')){
                strApplicableNationality = 'UAENATIONALUAERESIDENT';
            }
            else if((!objA.isPersonAccount && String.isNotBlank(objA.Nationality__c) && setNationalities.contains(objA.Nationality__c)
            || objA.isPersonAccount && String.isNotBlank(objA.Nationality__pc) && setNationalities.contains(objA.Nationality__pc))
            && (!objA.isPersonAccount && String.isNotBlank(objA.Country__c) && objA.Country__c == 'United Arab Emirates'
            || objA.isPersonAccount && String.isNotBlank(objA.Country__pc) && objA.Country__pc == 'United Arab Emirates')){
                strApplicableNationality = 'GCCNATIONALUAERESIDENT';
            }
            else if((!objA.isPersonAccount && String.isNotBlank(objA.Nationality__c) && !setNationalities.contains(objA.Nationality__c)
            || objA.isPersonAccount && String.isNotBlank(objA.Nationality__pc) && !setNationalities.contains(objA.Nationality__pc))
            && (!objA.isPersonAccount && String.isNotBlank(objA.Country__c) && objA.Country__c == 'United Arab Emirates'
            || objA.isPersonAccount && String.isNotBlank(objA.Country__pc) && objA.Country__pc == 'United Arab Emirates')){
                strApplicableNationality = 'NONGCCNATIONALUAERESIDENT';
            }
            else if((!objA.isPersonAccount && String.isNotBlank(objA.Nationality__c) && objA.Nationality__c == 'UAE'
            || objA.isPersonAccount && String.isNotBlank(objA.Nationality__pc) && objA.Nationality__pc == 'UAE') 
            && (!objA.isPersonAccount && String.isNotBlank(objA.Country__c) && objA.Country__c != 'United Arab Emirates'
            || objA.isPersonAccount && String.isNotBlank(objA.Country__pc) && objA.Country__pc != 'United Arab Emirates')){
                strApplicableNationality = 'UAENATIONALOUTSIDEUAE';
            }
            else if((!objA.isPersonAccount && String.isNotBlank(objA.Nationality__c) && setNationalities.contains(objA.Nationality__c)
            || objA.isPersonAccount && String.isNotBlank(objA.Nationality__pc) && setNationalities.contains(objA.Nationality__pc))
            && (!objA.isPersonAccount && String.isNotBlank(objA.Country__c) && objA.Country__c != 'United Arab Emirates'
            || objA.isPersonAccount && String.isNotBlank(objA.Country__pc) && objA.Country__pc != 'United Arab Emirates')){
                strApplicableNationality = 'GCCNATIONALOUTSIDEUAE';
            }
            else if((!objA.isPersonAccount && String.isNotBlank(objA.Nationality__c) && !setNationalities.contains(objA.Nationality__c)
            || objA.isPersonAccount && String.isNotBlank(objA.Nationality__pc) && !setNationalities.contains(objA.Nationality__pc))
            && (!objA.isPersonAccount && String.isNotBlank(objA.Country__c) && objA.Country__c != 'United Arab Emirates'
            || objA.isPersonAccount && String.isNotBlank(objA.Country__pc) && objA.Country__pc != 'United Arab Emirates')){
                strApplicableNationality = 'NONGCCNATIONALOUTSIDEUAE';
            }
        return strApplicableNationality;
    }
    
    public void getKeyHandover(String regId, String strSubProcessName, String strProjectCity, 
                                     String strProject, String strBuildingCode, String strPermittedUse, 
                                     String strBedroomType, String strAppUnits, String strUnitType, 
                                     String strNationality, String strTypeOfCustomer, String strPOA,
                                     String caseId, string bookingUnitId, String accountId, String custoName){
        System.debug('regId*****'+regId);
        System.debug('strSubProcessName*****'+strSubProcessName);
        System.debug('strProjectCity*****'+strProjectCity);
        System.debug('strProject*****'+strProject);
        System.debug('strBuildingCode*****'+strBuildingCode);
        System.debug('strPermittedUse*****'+strPermittedUse);
        System.debug('strBedroomType*****'+strBedroomType);
        System.debug('strAppUnits*****'+strAppUnits);
        System.debug('strUnitType*****'+strUnitType);  
        System.debug('strNationality*****'+strNationality);  
        System.debug('strTypeOfCustomer*****'+strTypeOfCustomer);  
        System.debug('strPOA*****'+strPOA);  
        DocumentationForKeyHandoverService.DoumentationForKeyHandoverRuleHttpSoap11Endpoint calloutObj = 
        new DocumentationForKeyHandoverService.DoumentationForKeyHandoverRuleHttpSoap11Endpoint ();
        calloutObj.timeout_x = 120000;
        //KayHandover resObj = new KayHandover();
        try{
            String response = calloutObj.DoumentationForKeyHandover(regId, 'Handover', strSubProcessName, strProjectCity, strProject, strBuildingCode, strPermittedUse,
            strBedroomType, strAppUnits, strUnitType, strNationality, strTypeOfCustomer, strPOA);
            //resObj = (KayHandover)JSON.deserialize(response, KeyHandoverChecklist.KayHandover.class);
            system.debug('keyhandover response === '+ response);
            map<String, String> mapDocName  = new map<String, String>();
            map<String, boolean> mapDocCustomerLevel = new map<String, boolean>();
            for(RuleEngineDocuments__c docInst: [select Id
                                                      , Document_Name__c
                                                      , Document_Name_Displayed__c
                                                      , Customer_Level_Document__c
                                                 from RuleEngineDocuments__c
                                                 where Document_Name__c != null
                                                 and Document_Name_Displayed__c != null
                                                 and (Customer_Level_Document__c = true
                                                 or Handover_Process_Document__c = true)]) {
                if(String.isNotBlank(docInst.Document_Name__c)
                && String.isNotBlank(docInst.Document_Name_Displayed__c)){
                    mapDocName.put(docInst.Document_Name__c, docInst.Document_Name_Displayed__c);
                    mapDocCustomerLevel.put(docInst.Document_Name__c, docInst.Customer_Level_Document__c);
                }
            }
            map<String,Object> mapDeserializeDoc = new map<String,Object>();
            mapDeserializeDoc = (map<String,Object>)JSON.deserializeUntyped(response);
            response = response.remove('{');
            response = response.remove('}');
            response = response.remove('"');
            system.debug('response==='+response);
            map<String,String> mapKey_Value = new map<String,String>();
            system.debug('response******'+response);
            boolean blnAllow = false;
            Integer indexVal = 0;
            for(String st : response.split(',')){
                system.debug('*****st*****'+st);
                String strPre = st.substringBefore(':').trim();
                String strPost = st.subStringAfter(':').trim();
                if(!blnAllow && strPre.equalsIgnoreCase('allowed')
                && strPost.equalsIgnoreCase('Yes')
                && indexVal == 0){
                    blnAllow = true;
                }else if(!blnAllow && strPre.equalsIgnoreCase('allowed')
                && !strPost.equalsIgnoreCase('Yes')
                && indexVal == 0){
                    // system.debug('*****CREATE ERROR LOG IN THE FUTURE*****');
                    break;
                }
                mapKey_Value.put(strPre,strPost);
                indexVal++;
                /*
                system.debug('strPre :*******'+strPre);
                system.debug('strPost :*******'+strPost);
                if(indexVal >=2 && indexVal <= 17) {
                    mapKey_Value.put(strPre,strPost);
                }
                
                if(!blnAllow && indexVal == 1){
                    break;
                }
                indexVal++;
                */
            }
            if(blnAllow){
                system.debug('mapKey_Value*************'+mapKey_Value);
                //list<SR_Attachments__c> lstCaseAttachment = new list<SR_Attachments__c>();
                if(accountId == null){
                    SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                    objCaseAttachment.Case__c = caseId;
                    objCaseAttachment.IsRequired__c = true;
                    objCaseAttachment.Name = 'PCC';
                    objCaseAttachment.Booking_Unit__c = bookingUnitId;
                    lstCaseAttachment.add(objCaseAttachment);
                    
                    SR_Attachments__c objCaseAttachment1 = new SR_Attachments__c();
                    objCaseAttachment1.Case__c = caseId;
                    objCaseAttachment1.IsRequired__c = true;
                    objCaseAttachment1.Name = 'FM PCC';
                    objCaseAttachment1.Booking_Unit__c = bookingUnitId;
                    lstCaseAttachment.add(objCaseAttachment1);
                }
                for(String docType: mapKey_Value.keySet()){
                    system.debug('docType*****'+docType);
                    system.debug('map.get*****'+mapKey_Value.get(docType));
                    if(mapDocName.containsKey(docType)
                    && (mapKey_Value.get(docType) == 'Mandatory'
                    || mapKey_Value.get(docType) == 'Optional')){
                        system.debug('Came inside*****');
                        SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                        objCaseAttachment.Case__c = caseId;
                        if(mapKey_Value.get(docType) == 'Mandatory'){
                            objCaseAttachment.IsRequired__c = true;
                        }else{
                            objCaseAttachment.IsRequired__c = false;
                        }
                        String strName = mapDocName.get(docType);
                        if(!String.isEmpty(custoName)){
                            strName = strName + ' for '+custoName;
                        }
                        objCaseAttachment.Name = strName;
                        objCaseAttachment.Booking_Unit__c = bookingUnitId;
                        objCaseAttachment.Account__c = accountId;
                        system.debug('accountId*****'+accountId);
                        system.debug('mapDocCustomerLevel.containsKey(docType)*****'+mapDocCustomerLevel.containsKey(docType));
                        system.debug('mapDocCustomerLevel.get(docType)*****'+mapDocCustomerLevel.get(docType));
                        if((accountId != null && mapDocCustomerLevel.containsKey(docType)
                        && mapDocCustomerLevel.get(docType))
                        || (accountId == null && mapDocCustomerLevel.containsKey(docType) 
                        && mapDocCustomerLevel.get(docType) == false)){
                            lstCaseAttachment.add(objCaseAttachment);
                            system.debug('*****SR Attachments aded to list*****');
                        }
                    }
                }
                /*
                if(!lstCaseAttachment.isEmpty()){
                    insert lstCaseAttachment;
                }
                */
            } // if RE returns allowed
        }catch(Exception ex){
            system.debug('*****Exception*****'+ex.getMessage());
            system.debug('*****Line No*****'+ex.getLineNumber());
            system.debug('*****Cause*****'+ex.getCause());
            system.debug('*****Stack Trace*****'+ex.getStackTraceString());
        }
    }
    /*
    public void fetchUnitDetails(String regId){
        objWrap = UnitDetailsService.getBookingUnitDetails(regId);
    }
    */
}