/*********************************************************************************************************
* Name               : cancelBinManagementController
* Test Class         : 
* Description        : Controller class for cancelBinManagement VF Page
* Created Date       : 09/11/2020
* -----------------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         03/12/2020      Initial Draft.
**********************************************************************************************************/
global With Sharing class cancelBinManagementController {
    public String invListJSON {get; set;}
    public String selectedListJSON {get; set;}
    public String invRowList {get; set;}
    public List<String> invIdList;
    public Map<String, InventoryWrapper> wrapperMap;
    public Map<String, String> invIdRowCountMap;
     /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public cancelBinManagementController() {
        invRowList = '';
        invIdList = new List<String>();
        wrapperMap = new Map<String, InventoryWrapper>();
        invIdRowCountMap = new Map<String, String>();
        Integer no = 0;
         for(Inventory__c inv: [SELECT Id, Name, unit_name__c, Building_Name__c,
                                 Parking__c, Bedroom_Type__c, Furnished__c,
                                 Area__c, Special_Price__c, Space_Type_Lookup_Code__c,
                                 Type__c, Marketing_Name__c, ACD_Date__c,
                                 (SELECT Id, Registration_Date__c,Registration_Id__c,
                                 
                                         Registration_Status__c 
                                     FROM Booking_Units__r 
                                     ORDER BY Registration_Date__c DESC LIMIT 1) 
                                FROM Inventory__c 
                                WHERE Status__c = 'Cancel Bin']){
            InventoryWrapper wrapper = new InventoryWrapper();
            Integer days = 0;
            wrapper.invId = inv.Id;
            
            wrapper.no = String.valueOf(no);
            invIdRowCountMap.put(wrapper.no, inv.Id);
            if(inv.unit_name__c != null && inv.unit_name__c != ''){
                wrapper.unit = inv.unit_name__c;
            }
            for(Booking_Unit__c bu: inv.Booking_Units__r){
                if(bu.Registration_Id__c != null && bu.Registration_Id__c != ''){
                    wrapper.reg_id = bu.Registration_Id__c;
                }
                if(bu.Registration_Status__c != null && bu.Registration_Status__c != ''){
                    wrapper.status = bu.Registration_Status__c;
                }
                if(bu.Registration_Date__c != null){
                    wrapper.reg_date = bu.Registration_Date__c.format();
                    days = bu.Registration_Date__c.daysBetween(Date.Today());
                    wrapper.days = String.valueOf(days);
                }
            }
            if(inv.Building_Name__c != null && inv.Building_Name__c != ''){
                wrapper.building = inv.Building_Name__c;
            }
            if(inv.Parking__c != null && inv.Parking__c != ''){
                wrapper.parking = inv.Parking__c;
            }
            if(inv.Bedroom_Type__c != null && inv.Bedroom_Type__c != ''){
                wrapper.bedroom = inv.Bedroom_Type__c;
            }
            if(inv.Furnished__c){
                wrapper.furnished = 'Yes';
            } else{
                wrapper.furnished = 'No';
            }
            if(inv.Area__c != null && inv.Area__c != ''){
                wrapper.area = inv.Area__c;
            }
            if(inv.Special_Price__c != null){
                wrapper.price = String.valueOf(inv.Special_Price__c);
            }
            if(inv.Area__c != null && inv.Area__c != ''){
                wrapper.area = inv.Area__c;
            }
            if(inv.Space_Type_Lookup_Code__c != null && inv.Space_Type_Lookup_Code__c != ''){
                wrapper.space_type = inv.Space_Type_Lookup_Code__c;
            }
            if(inv.Type__c != null && inv.Type__c != ''){
                wrapper.type = inv.Type__c;
            }
            if(inv.Marketing_Name__c != null && inv.Marketing_Name__c != ''){
                wrapper.marketing_name = inv.Marketing_Name__c;
            }
            if(inv.ACD_Date__c != null && inv.ACD_Date__c != ''){
                wrapper.acd = inv.ACD_Date__c;
            }
            wrapper.compliance_type = 'Not Available';
            
            wrapperMap.put(inv.id, wrapper); 
            no++; 
        }
        system.debug('wrapperMap: ' + wrapperMap);
        invListJSON = JSON.serialize(wrapperMap.values());
        selectedListJSON = invListJSON;
    }
    
    public void selectInvList(){
        invRowList = ApexPages.currentPage().getParameters().get('invRowList');
        system.debug('invRowList: ' + invRowList);
        List<InventoryWrapper> selectedList = new List<InventoryWrapper>();
        if(invRowList != null && invRowList != ''){
            for(String rowNo: invRowList.split(',')){
                system.debug('rowNo: ' + rowNo);
                if(rowNo != null && rowNo != '' 
                        && invIdRowCountMap.containsKey(rowNo) 
                        && invIdRowCountMap.get(rowNo) != null){
                     invIdList.add(invIdRowCountMap.get(rowNo));
                    
                }
            }
            for(String invId: invIdList){
                if(wrapperMap.containsKey(invId) && wrapperMap.get(invId) != null) {
                    selectedList.add(wrapperMap.get(invId));
                }
            }
        }
        selectedListJSON = JSON.serialize(selectedList);
        system.debug('selectedListJSON: ' + selectedListJSON);
    }
    
    public void updateInventory(){
        List<Inventory__c> invToUpdate = new List<Inventory__c>();
        if(invIdList != null ){
            for(String invId: invIdList){
                Inventory__c inv = new Inventory__c();
                inv.Id = invId;
                inv.Status__c = 'Inventory';
                invToUpdate.add(inv);
            }
        }
        
        if(invToUpdate.size() > 0){
            try{
                update invToUpdate;
            }catch(exception e){
                system.debug('Error while updating Inventories: ' + e);
            }
        }
    }
    
    
    public class InventoryWrapper{
        public String no;
        public String unit;
        public String reg_id;
        public String reg_date;
        public String building;
        public String parking;
        public String bedroom;
        public String furnished;
        public String space_type;
        public String type;
        public String marketing_name;
        public String acd;
        public String area;
        public String price;
        public String days;
        public String status;
        public String compliance_type;
        public string invId;

        public InventoryWrapper(){
            invId  = '';
            no = '-';
            unit = '-';
            reg_id = '-';
            reg_date = '-';
            building = '-';
            parking = '-';
            bedroom = '-';
            furnished = '-';
            area = '-';
            price = '-';
            days = '-';
            status = '-';
            compliance_type = '-';
        }
    }
}