/*
 * Description : This class is used to Schedule UpdateEmailOnPortalCxBatch batch at every midnight
 * Revision History:
 *
 *  Version          Author              Date           Description
 *  1.0              Arjun Khatri        12/07/2018      Initial Draft
 *                                                      
 */
global class ScheduleUpdateEmailOnPortalCxBatch implements Schedulable {
  global void execute(SchedulableContext sc) {
      UpdateEmailOnPortalCxBatch batchInstance = new UpdateEmailOnPortalCxBatch(); 
      database.executebatch(batchInstance,90);
  }
}