/**
 * @File Name          : Damac_AssignCarController.cls
 * @Author             : Srikanth Valluri
 * Ver       Date            Author                 Modification
 * 1.0    11/12/2019   Srikanth Valluri     Initial Version
**/

public with sharing class Damac_AssignCarController {

    public List <Driver__c> availableDrivers { get;set ;}
    public List <Cars__c> availableCars { get; set; }
    public Damac_AssignCarController() {
        init ();
    }

    @RemoteAction
    public static List <String> searchCars(String value)
    {
        List <Driver__c> availableDrivers = [SELECT
                                User__r.Name, IN_Active__c, Name, Mobile__c, Assigned_Car__c, Assigned_Car__r.Name
                            FROM
                                Driver__c
                            WHERE
                                IN_Active__c = FALSE];
        Set <ID> carsAssigned = new Set <ID> ();
        for (Driver__c driver :availableDrivers){
            if (driver.Assigned_Car__c != NULL) {
                carsAssigned.add (driver.Assigned_Car__c);
            }
        }

        String query = '';
        if (value == '') {
            query ='Select name from Cars__c WHERE ID NOT IN: carsAssigned LIMIT 20'; 
        } else {
            value= '%'+value+'%';
            query = 'Select name from Cars__c where name LIKE :value AND ID NOT IN: carsAssigned';
        }
        List <String> carNames = new List <String> ();
        for (Cars__c l: Database.query(query))
        {
            carNames.add(l.name);
        }
        return carNames;
    }
    public void init() {
        availableDrivers = new List <Driver__c>();
        availableCars = new List <Cars__c> ();
        availableDrivers = [SELECT
                                User__r.Name, IN_Active__c, Name, Mobile__c, Assigned_Car__c, Assigned_Car__r.Name
                            FROM
                                Driver__c
                            WHERE
                                IN_Active__c = FALSE];
        Set <ID> carsAssigned = new Set <ID> ();
        for (Driver__c driver :availableDrivers){
            if (driver.Assigned_Car__c != NULL) {
                carsAssigned.add (driver.Assigned_Car__c);
            }
        }
        availableCars = [SELECT
                            Name
                        FROM 
                            Cars__c
                        WHERE 
                            ID NOT IN: carsAssigned];
    }

    public void assignCarToDriver () {
        try {
            String carName = ApexPages.currentPage().getParameters().get('selectedCar');
            String driverID = ApexPages.currentPage().getParameters().get('selectedDriver');
            Driver__c driver = [SELECT 
                                    Assigned_Car__c, User__c, User__r.Name, User__r.MobilePhone
                                FROM 
                                    Driver__c 
                                WHERE
                                    ID =: driverId];
            Cars__c car = new Cars__c ();
            List <Driver__c> carsAssigned = new List <Driver__c>();
            if (carName != '' && carName != NULL) {
                car = [SELECT
                            Name 
                        FROM 
                            Cars__c 
                        WHERE 
                            Name =: carName];
                
                carsAssigned = [SELECT 
                                    Name, IN_Active__c 
                                FROM 
                                    Driver__c 
                                WHERE 
                                    Assigned_Car__c =: car.Id 
                                    AND 
                                    IN_Active__c = TRUE];
            }
            if (carsAssigned.size () == 0){
                if (driver.Assigned_Car__c == NULL) {
                    driver.Assigned_Car__c = car.id;
                    Update driver;
                } else {
                    driver.IN_Active__c = true;
                    update driver;

                    Driver__c newDriver = new Driver__c ();
                    newDriver.User__c = driver.User__c;
                    newDriver.Assigned_Car__c = car.Id;
                    newDriver.IN_Active__c = false;
                    newDriver.Name = driver.User__r.Name;
                    newDriver.Mobile__c = driver.User__r.MobilePhone;
                    newDriver.Other_Mobile__c = driver.User__r.MobilePhone;
                    insert newDriver;
                }
                init ();
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'This car is already allocated.'));
            }
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
        }
    }

    public void deactivateDriver () {
        try {
            String driverID = ApexPages.currentPage().getParameters().get('selectedDriver');
            Driver__c driver = [SELECT Assigned_Car__c, User__c FROM Driver__c WHERE ID =: driverId];
            driver.IN_Active__c = true;
            update driver;
            init ();
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
        }
    }
    
}