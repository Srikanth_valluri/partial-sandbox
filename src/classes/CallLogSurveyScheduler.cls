/*****************************************************************************************
 * Description - Scheduler of CallLogSurveyBatch Class
 *----------------------------------------------------------------------------------------
 * Version        Date          Author              Description
 * 1.0            30/04/2020    Aishwarya Todkar    Initial Draft
 *****************************************************************************************/
global class CallLogSurveyScheduler implements Schedulable {
    public void execute(SchedulableContext sc) {
        Database.executebatch(new CallLogSurveyBatch(), 1);
    }
}