/***************************************************************************************************************
Description: Test class for AssignBCtoLegalBatchScheduler.
________________________________________________________________________________________________________________
Version Date(DD-MM-YYYY)    Author              Description
----------------------------------------------------------------------------------------------------------------
1.0     20-08-2020          Aishwarya Todkar    Initial Draft
****************************************************************************************************************/
@isTest
public class AssignBCtoLegalBatchSchedulerTest {
      static testMethod void testExecute() {
        Test.startTest();
            AssignBCtoLegalBatchScheduler objScheduler = new AssignBCtoLegalBatchScheduler();
            String schCron = '0 30 8 1/1 * ? *';
            String jobId = System.Schedule('Reccord Updated',schCron,objScheduler);
        Test.stopTest();
        
    }
}