@isTest
Public Class OverdueRebateCalculatorTest {
  
    @isTest
    public static void testAkoya() {
        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        objAcc.Mobile__c ='1234567890';
        insert objAcc;
                
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Id  bildingLocRecTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.recordTypeId = bildingLocRecTypeId;
        loc.Location_Name__c = 'test';
        loc.Address__c = 'test';
        loc.Contact__c = 'test';
        loc.Timings__c = 'test';
        loc.Map_URL__c = 'https://test';
        insert loc;
        
        Property__c objProperty = TestDataFactory_CRM.createProperty();
        insert objProperty;
        
        Inventory__c objInventory = TestDataFactory_CRM.createInventory(objProperty.Id);
        objInventory.Building_Location__c = loc.id;
        objInventory.property_Name__c = 'akoya oxygen';
        objInventory.Master_Community_EN__c = 'akoya oxygen';
        insert objInventory;
        
        Booking_Unit_Active_Status__c buStatusObj = new Booking_Unit_Active_Status__c();
        buStatusObj.Name = 'Agreement Rejected By Sales Admin';
        buStatusObj.Status_Value__c  = 'Agreement Rejected By Sales Admin';
        insert buStatusObj;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Inventory__c = objInventory.Id;
        objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Allow_Overdue__c  = true;
        objBookingUnit.Registration_DateTime__c = system.today().addDays(-200);
        objBookingUnit.Requested_Price_AED__c = 90000;
        objBookingUnit.Anticipated_Completion_Date__c = Date.newInstance(2020, 12, 31 );
        insert objBookingUnit;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Overdue Rebate/Discount').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        objCase.Overdue_Amount__c = '40000';
        objCase.Initial_Paid__c = 30;
        objCase.Booking_Unit__c = objBookingUnit.id;
        insert objCase ;
        
        System.debug('Inventory--'+ [Select id,Master_Community_EN__c from Inventory__c]); 
        System.debug('BU--' + [Select id,Allow_Overdue__c,Inventory__c,Requested_Price_AED__c,Registration_ID__c from Booking_Unit__c]);
        
        System.debug('Case--' + [Select id, Initial_Paid__C,Booking_Unit__c
                                        ,Booking_Unit__r.Allow_Overdue__c,Booking_Unit__r.Inventory__c
                                        ,Booking_Unit__r.Requested_Price_AED__c,Booking_Unit__r.Registration_ID__c
                                   from case]);
        Test.startTest();
        OverdueRebateCalculator.applyRebateOnUnits( new List<id> { objCase.Id } );
        Test.stopTest();
    }
    
    @isTest
    public static void testAkoya_3per() {
        // objCase.Booking_Unit__r.Registration_DateTime__c
        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        objAcc.Mobile__c ='1234567890';
        insert objAcc;
                
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Id  bildingLocRecTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.recordTypeId = bildingLocRecTypeId;
        loc.Location_Name__c = 'test';
        loc.Address__c = 'test';
        loc.Contact__c = 'test';
        loc.Timings__c = 'test';
        loc.Map_URL__c = 'https://test';
        insert loc;
        
        Property__c objProperty = TestDataFactory_CRM.createProperty();
        insert objProperty;
        
        Inventory__c objInventory = TestDataFactory_CRM.createInventory(objProperty.Id);
        objInventory.Building_Location__c = loc.id;
        objInventory.property_Name__c = 'akoya oxygen';
        objInventory.Master_Community_EN__c = 'akoya oxygen';
        insert objInventory;
        
        Booking_Unit_Active_Status__c buStatusObj = new Booking_Unit_Active_Status__c();
        buStatusObj.Name = 'Agreement Rejected By Sales Admin';
        buStatusObj.Status_Value__c  = 'Agreement Rejected By Sales Admin';
        insert buStatusObj;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Inventory__c = objInventory.Id;
        objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Allow_Overdue__c  = true;
        objBookingUnit.Registration_DateTime__c = system.today().addDays(-200);
        objBookingUnit.Requested_Price_AED__c = 10000;
        objBookingUnit.Anticipated_Completion_Date__c = Date.newInstance(2022, 12, 31 );
        insert objBookingUnit;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Overdue Rebate/Discount').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        objCase.Overdue_Amount__c = '20000';
        objCase.Initial_Paid__c = 10;
        objCase.Booking_Unit__c = objBookingUnit.id;
        insert objCase ;
        
        System.debug('Inventory--'+ [Select id,Master_Community_EN__c from Inventory__c]); 
        System.debug('BU--' + [Select id,Allow_Overdue__c,Inventory__c,Requested_Price_AED__c,Registration_ID__c from Booking_Unit__c]);
        
        System.debug('Case--' + [Select id, Initial_Paid__C,Booking_Unit__c
                                        ,Booking_Unit__r.Allow_Overdue__c,Booking_Unit__r.Inventory__c
                                        ,Booking_Unit__r.Requested_Price_AED__c,Booking_Unit__r.Registration_ID__c
                                   from case]);
        Test.startTest();
        OverdueRebateCalculator.applyRebateOnUnits( new List<id> { objCase.Id } );
        Test.stopTest();
    }

    @isTest
    public static void testNonAkoya() {
        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        objAcc.Mobile__c ='1234567890';
        insert objAcc;
        //totalPaidPercentage < 40 
        /**
            outstandingAmount = Decimal.valueOf( objCase.Overdue_Amount__c );
            initialPaidAmount = objCase.Initial_Paid_Amount__c;
            totalPaidAmount = outstandingAmount + initialPaidAmount;
            requestedPrice = objCase.Booking_Unit__r.Requested_Price_AED__c;
            totalPaidPercentage = ( totalPaidAmount / requestedPrice ) * 100
         */
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Id  bildingLocRecTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.recordTypeId = bildingLocRecTypeId;
        loc.Location_Name__c = 'test';
        loc.Address__c = 'test';
        loc.Contact__c = 'test';
        loc.Timings__c = 'test';
        loc.Map_URL__c = 'https://test';
        insert loc;
        
        Property__c objProperty = TestDataFactory_CRM.createProperty();
        insert objProperty;
        
        Inventory__c objInventory = TestDataFactory_CRM.createInventory(objProperty.Id);
        objInventory.Building_Location__c = loc.id;
        objInventory.property_Name__c = 'UAE';
        objInventory.Master_Community_EN__c = 'non akoya oxygen';
        insert objInventory;
        
        Booking_Unit_Active_Status__c buStatusObj = new Booking_Unit_Active_Status__c();
        buStatusObj.Name = 'Agreement Rejected By Sales Admin';
        buStatusObj.Status_Value__c  = 'Agreement Rejected By Sales Admin';
        insert buStatusObj;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Inventory__c = objInventory.Id;
        objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Allow_Overdue__c  = true;
        objBookingUnit.Registration_DateTime__c = system.today().addDays(-200);
        objBookingUnit.Requested_Price_AED__c = 100;
        objBookingUnit.Anticipated_Completion_Date__c = Date.newInstance(2020, 12, 31);
        insert objBookingUnit;
        Test.startTest();
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Overdue Rebate/Discount').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        objCase.Overdue_Amount__c = '10';
        objCase.Initial_Paid__c = 10;
        objCase.Booking_Unit__c = objBookingUnit.id;
        insert objCase ;
        
        System.debug('Case--' + [Select id, Initial_Paid__C,Booking_Unit__c
                                        ,Booking_Unit__r.Allow_Overdue__c,Booking_Unit__r.Inventory__c
                                        ,Booking_Unit__r.Requested_Price_AED__c,Booking_Unit__r.Registration_ID__c
                                   from case]);
        OverdueRebateCalculator.applyRebateOnUnits( new List<id> { objCase.Id } );
        Test.stopTest();
    }

    @isTest
    public static void testNonAkoya2() {
        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        objAcc.Mobile__c ='1234567890';
        insert objAcc;
                
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Id  bildingLocRecTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.recordTypeId = bildingLocRecTypeId;
        loc.Location_Name__c = 'test';
        loc.Address__c = 'test';
        loc.Contact__c = 'test';
        loc.Timings__c = 'test';
        loc.Map_URL__c = 'https://test';
        insert loc;
        
        Property__c objProperty = TestDataFactory_CRM.createProperty();
        insert objProperty;
        
        Inventory__c objInventory = TestDataFactory_CRM.createInventory(objProperty.Id);
        objInventory.Building_Location__c = loc.id;
        objInventory.property_Name__c = 'UAE';
        objInventory.Master_Community_EN__c = 'non akoya oxygen';
        insert objInventory;
        
        Booking_Unit_Active_Status__c buStatusObj = new Booking_Unit_Active_Status__c();
        buStatusObj.Name = 'Agreement Rejected By Sales Admin';
        buStatusObj.Status_Value__c  = 'Agreement Rejected By Sales Admin';
        insert buStatusObj;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Inventory__c = objInventory.Id;
        objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Allow_Overdue__c  = true;
        objBookingUnit.Registration_DateTime__c = system.today().addDays(-200);
        objBookingUnit.Requested_Price_AED__c = 100;
        objBookingUnit.Anticipated_Completion_Date__c = Date.newInstance(2020, 12, 31);
        insert objBookingUnit;
        Test.startTest();
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Overdue Rebate/Discount').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        objCase.Overdue_Amount__c = '40';
        objCase.Initial_Paid__c = 10;
        objCase.Booking_Unit__c = objBookingUnit.id;
        insert objCase ;
        
        System.debug('Case--' + [Select id, Initial_Paid__C,Booking_Unit__c
                                        ,Booking_Unit__r.Allow_Overdue__c,Booking_Unit__r.Inventory__c
                                        ,Booking_Unit__r.Requested_Price_AED__c,Booking_Unit__r.Registration_ID__c
                                   from case]);
        OverdueRebateCalculator.applyRebateOnUnits( new List<id> { objCase.Id } );
        Test.stopTest();
    }

    @isTest
    public static void testBSW_ABZ() {
        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        objAcc.Mobile__c ='1234567890';
        insert objAcc;
                
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Id  bildingLocRecTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.recordTypeId = bildingLocRecTypeId;
        loc.Location_Name__c = 'test';
        loc.Address__c = 'test';
        loc.Contact__c = 'test';
        loc.Timings__c = 'test';
        loc.Map_URL__c = 'https://test';
        insert loc;
        
        Property__c objProperty = TestDataFactory_CRM.createProperty();
        insert objProperty;
        
        Inventory__c objInventory = TestDataFactory_CRM.createInventory(objProperty.Id);
        objInventory.Building_Location__c = loc.id;
        objInventory.property_Name__c = 'akoya oxygen';
        objInventory.Master_Community_EN__c = 'akoya oxygen';
        insert objInventory;
        
        Booking_Unit_Active_Status__c buStatusObj = new Booking_Unit_Active_Status__c();
        buStatusObj.Name = 'Agreement Rejected By Sales Admin';
        buStatusObj.Status_Value__c  = 'Agreement Rejected By Sales Admin';
        insert buStatusObj;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'ABZ/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Inventory__c = objInventory.Id;
        objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Allow_Overdue__c  = true;
        objBookingUnit.Registration_DateTime__c = system.today().addDays(-200);
        objBookingUnit.Requested_Price_AED__c = 90000;
        objBookingUnit.Anticipated_Completion_Date__c = Date.newInstance(2020, 12, 31 );
        insert objBookingUnit;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Overdue Rebate/Discount').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        objCase.Overdue_Amount__c = '40000';
        objCase.Initial_Paid__c = 30;
        objCase.Booking_Unit__c = objBookingUnit.id;
        insert objCase ;
        
        System.debug('Inventory--'+ [Select id,Master_Community_EN__c from Inventory__c]); 
        System.debug('BU--' + [Select id,Allow_Overdue__c,Inventory__c,Requested_Price_AED__c,Registration_ID__c from Booking_Unit__c]);
        
        System.debug('Case--' + [Select id, Initial_Paid__C,Booking_Unit__c
                                        ,Booking_Unit__r.Allow_Overdue__c,Booking_Unit__r.Inventory__c
                                        ,Booking_Unit__r.Requested_Price_AED__c,Booking_Unit__r.Registration_ID__c
                                   from case]);
        Test.startTest();
        OverdueRebateCalculator.applyRebateOnUnits( new List<id> { objCase.Id } );
        Test.stopTest();
    }

    @isTest
    public static void testapplySameRebate() {
        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        objAcc.Mobile__c ='1234567890';
        insert objAcc;
                
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Id  bildingLocRecTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.recordTypeId = bildingLocRecTypeId;
        loc.Location_Name__c = 'test';
        loc.Address__c = 'test';
        loc.Contact__c = 'test';
        loc.Timings__c = 'test';
        loc.Map_URL__c = 'https://test';
        insert loc;
        
        Property__c objProperty = TestDataFactory_CRM.createProperty();
        insert objProperty;
        
        Inventory__c objInventory = TestDataFactory_CRM.createInventory(objProperty.Id);
        objInventory.Building_Location__c = loc.id;
        objInventory.property_Name__c = 'akoya oxygen';
        objInventory.Master_Community_EN__c = 'akoya oxygen';
        insert objInventory;
        
        Booking_Unit_Active_Status__c buStatusObj = new Booking_Unit_Active_Status__c();
        buStatusObj.Name = 'Agreement Rejected By Sales Admin';
        buStatusObj.Status_Value__c  = 'Agreement Rejected By Sales Admin';
        insert buStatusObj;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'ABZ/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Inventory__c = objInventory.Id;
        objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Allow_Overdue__c  = true;
        objBookingUnit.Registration_DateTime__c = system.today().addDays(-200);
        objBookingUnit.Requested_Price_AED__c = 90000;
        objBookingUnit.Anticipated_Completion_Date__c = Date.newInstance(2020, 12, 31 );
        insert objBookingUnit;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Overdue Rebate/Discount').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        objCase.Overdue_Amount__c = '40000';
        objCase.Initial_Paid__c = 30;
        objCase.Booking_Unit__c = objBookingUnit.id;
        objCase.Promotion_Type__c = '5';
        insert objCase ;
        
        System.debug('Inventory--'+ [Select id,Master_Community_EN__c from Inventory__c]); 
        System.debug('BU--' + [Select id,Allow_Overdue__c,Inventory__c,Requested_Price_AED__c,Registration_ID__c from Booking_Unit__c]);
        
        System.debug('Case--' + [Select id, Initial_Paid__C,Booking_Unit__c
                                        ,Booking_Unit__r.Allow_Overdue__c,Booking_Unit__r.Inventory__c
                                        ,Booking_Unit__r.Requested_Price_AED__c,Booking_Unit__r.Registration_ID__c
                                   from case]);
        Test.startTest();
        OverdueRebateCalculator.applyRebateOnUnits( new List<id> { objCase.Id } );
        Test.stopTest();
    }
}