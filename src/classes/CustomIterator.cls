global class CustomIterator implements Iterator<sobject>{

    List<sobject> results {get;set;}
    Integer index {get;set;}
    
    global CustomIterator(list<sobject> data){
        results = data;
        index = 0;
    }
    
    global boolean hasNext(){
        return results != null && !results.isEmpty() && index < results.size(); 
    }
    
    global sobject next(){
        return results[index++];
    }

}