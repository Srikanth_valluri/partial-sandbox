global class ZiwoTasks2WaybeoQueueable implements Database.Batchable<sObject>, database.allowsCallouts {

    global database.queryLocator start(Database.BatchableContext bc){
        /*
        Map<string, Scheduler_Settings__c> settings = Scheduler_settings__c.getAll();
        Scheduler_Settings__c ziwoSettings = settings.containsKey('Tasks2WaybeoLogs') ? settings.get('Tasks2WaybeoLogs'): new Scheduler_Settings__c(name = 'Tasks2WaybeoLogs');
        DateTime LastRunTime = (ziwoSettings.Last_Run_Time__c == null ? System.now().addMinutes(-15) : ziwoSettings.Last_Run_Time__c);
        ziwoSettings.Last_Run_Time__c = System.now();
        upsert ziwoSettings;
        */
        DateTime currTime = System.now().addMinutes(-10);
        string query = 'Select id, CrtObjectId__c from Task where CrtObjectId__c != NULL AND Call_Start_Time__c = NULL'; 
        query += ' AND (Event_Type__c = \'Ziwo Outbound\' OR Event_Type__c = \'Ziwo Inbound\') AND Recorded_Call__c = null';
        if(!test.isrunningTest())
            query += ' AND createdDate <=: currTime';
        //System.debug(database.getQueryLocator(query));
        return database.getQueryLocator(query);
        
    }

    global void execute(Database.BatchableContext bc, list<Task> scope){    
        set<string> callIDs = new set<string>();
        for(Task tObj : scope){
            callIDs.add(tObj.crtObjectId__c);
        }
        
        Damac_ZiwoCallHistory.syncCaptureCallHistory(callIds);
    }


    global void finish(Database.BatchableContext bc){ 
        database.executeBatch(new ZiwoLogs2TasksQueueable(), integer.valueOf(System.label.Ziwo_Task_Update_Batch_Count));
    }
    
    
}