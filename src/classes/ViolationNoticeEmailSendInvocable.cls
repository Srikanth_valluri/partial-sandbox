/**
 * @File Name          : ViolationNoticeEmailSendInvocable.cls
 * @Description        : This Method is called from "Violation Process 
                            : On create of Select Notice Type and Issue Violation Notice task" Prcoess Builder
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 9/30/2019, 7:01:42 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    7/29/2019, 3:08:30 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public without sharing class ViolationNoticeEmailSendInvocable { 
     private static final Id VIOLATION_NOTICE_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('FM_Case__c', 'Violation Notice');
     public static string fromAddress_LOAMEmail;
     public static List<EmailMessage> emailMessageLst;
    //This Method is called on 'Select Notice Type and Issue Violation Notice' task closed by PM.
    @InvocableMethod 
    public static void initiateEmailAndSMS(List<Id> caseIdList){
       
        System.debug('-inside initiateEmailAndSMS-caseIdList--'+caseIdList);
        System.debug('--VIOLATION_NOTICE_RECORD_TYPE_ID--'+VIOLATION_NOTICE_RECORD_TYPE_ID);
       
       // Call method to send an email with all the abve attachment
        if(caseIdList != NULL){
            
            // Send email
            sendEmailForViolation(caseIdList);
        }
    }

    // Method to fetch fm case record
    public static FM_Case__c getFmCases(List<Id> caseIdList){
        List<FM_Case__c> fmViolationCaseRecord = new List<FM_Case__c>();
        
        // Fetch Fm  vioalition case
        fmViolationCaseRecord = [
            SELECT Id,
                    Name,
                    Notice_Type__c,
                    OwnerId,
                    Owner.Email,
                    CreatedbyId,
                    Createdby.email,
                    RecordTypeId,
                    Tenant__c,
                    Tenant_Email__c,
                    Booking_Unit__c,
                    Email__c,
                    Unit_Name__c,
                    Approval_Status__c,
                    Booking_Unit__r.Name,
                    Tenant__r.Party_ID__c,
                    Tenant__r.Mobile_Phone_Encrypt__pc,
                    Account__r.Mobile_Phone_Encrypt__pc, 
                    Property_Manager_Name__c,
                    Property_Manager_Number__c,
                    Booking_Unit__r.Unit_Name__c,
                    Account_Email__c,
                    Property_Manager_Email__c,
                    Building_Email__c
               FROM FM_Case__c 
              WHERE RecordTypeId = :VIOLATION_NOTICE_RECORD_TYPE_ID
              AND Id IN :caseIdList
              AND 
                ( Notice_Type__c = 'First Notice'
                 OR Approval_Status__c = 'Approved' )
              
        ];
        String location;
        if(!fmViolationCaseRecord.isEmpty() ){
            location  = fmViolationCaseRecord[0].Booking_Unit__r.Unit_Name__c.substringBefore('/');
            if(String.isNotBlank(location)) {
            Location__c objLoc = [ SELECT Id
                                        , Name
                                        , Loams_Email__c
                                   FROM Location__c
                                   WHERE Name = :Location ];
                if(String.isNotBlank(objLoc.Loams_Email__c)){
                    fromAddress_LOAMEmail = objLoc.Loams_Email__c;
                }
            }
            return fmViolationCaseRecord[0];
            
        }
        return NULL;
    }
    
    
    /*This method is used to send an email to Owner and Tenant bcc to case initiator,
     This email will contain Violation Notice, Violation List, Incident Report and Violation Images.*/
    @future(callout=true)
    public static void sendEmailForViolation(
        List<Id> caseIdList){
        FM_Case__c violationCaseRecord = new FM_Case__c();
        emailMessageLst = new List<EmailMessage>();
        // Fetch FM case
        if(caseIdList != NULL && !caseIdList.isEmpty()){
           violationCaseRecord = getFmCases(caseIdList);
        }
       // List<String>lsthtmlBody = new List<String>();
       // Contact objCon = [SELECT Id from Contact LIMIT 1];
        String toAddress = '';
        String strCCAddress = '';
        String bccAddress = '';
        String contentValue = '';
        //String subject = '';
        List<Attachment> finalAttachmentList = new List<Attachment>();
        
        //Get template name
        List<EmailTemplate> lstTemplate = [
            SELECT  ID, 
                    Subject, 
                    Body, 
                    name,
                    HtmlValue, 
                    TemplateType,
                    BrandTemplateId
            FROM EmailTemplate
            WHERE DeveloperName =: Label.Violation_Process_Email_Template
        ];
        System.debug('lstTemplate::::::::::'+lstTemplate);
        List<SR_Attachments__c> srAttachmentList = new List<SR_Attachments__c>();
        
        // Fetch FM case to get its Violation Notice,incident report and Violation Images
        if(violationCaseRecord != NULL){
            srAttachmentList = [
                SELECT id
                    ,Name
                    ,IsValid__c
                    ,Document_Type__c
                    ,Attachment__c 
                    ,Attachment_URL__c
                    ,FM_Case__c
                FROM SR_Attachments__c
                WHERE FM_Case__c =: violationCaseRecord.Id 
                AND (NOT Name LIKE 'FMN%')
                AND Attachment_URL__c != NULL
            ];
        }
        System.debug('srAttachmentList::::::::::from sr attachment::::::*******'+srAttachmentList);
       
        
        // get Violation List from the document to attach it in the email
        Document violationListDocument = getViolationListFromDocument();
        Attachment attachObjFromDoc = new Attachment();
        if(violationListDocument != NULL){
            attachObjFromDoc.Name = violationListDocument.Name+'.pdf';
            attachObjFromDoc.Body = violationListDocument.Body;
            attachObjFromDoc.ContentType = 'application/pdf';
            finalAttachmentList.add(attachObjFromDoc);
        }
        
        // Fetch Violation Notices From attachment 
        List<Attachment> violationNoticeList;
        if(violationCaseRecord != NULL){
            violationNoticeList = getAttachedViolationNotices(violationCaseRecord.Id);
        }
        if(violationNoticeList != NULL && !violationNoticeList.isEmpty()){
            finalAttachmentList.addAll(violationNoticeList);
        }
        System.debug('-------finalAttachmentList:::::::'+finalAttachmentList);
        // Call a sendgrid to send an email to Owner of the case Initiator  
        if(violationCaseRecord != NULL ){
            
            // to Account email
            if( String.isNotBlank(violationCaseRecord.Account_Email__c)) {
            //String.isNotBlank(violationCaseRecord.Tenant_Email__c) &&  
                toAddress = violationCaseRecord.Account_Email__c;
                //strCCAddress = violationCaseRecord.Tenant_Email__c;
            }
            
            // cc to Proeprty manager
            if(String.isNotBlank(violationCaseRecord.Property_Manager_Email__c)
            && String.isNotBlank(violationCaseRecord.Tenant_Email__c)  ){
               // bccAddress = violationCaseRecord.Owner.Email; 
               strCCAddress = violationCaseRecord.Property_Manager_Email__c + ',';
               strCCAddress += violationCaseRecord.Tenant_Email__c;
            }
            system.debug('strCCAddress === ' + strCCAddress);
            // bcc to sf.copy
            if(String.isNotBlank(Label.BCC_Email)){
                bccAddress = Label.BCC_Email; 
            }   
            // send Email to Owner only if tenant is not present
            else if(String.isNotBlank(violationCaseRecord.Account_Email__c)){
               toAddress = violationCaseRecord.Account_Email__c;
            }
            contentValue = lstTemplate[0].body;
            String body = '';

            body += '<html></br> </br><br/><br/>';
                    
            
            for(SR_Attachments__c srAttachment : srAttachmentList){
                System.debug('srAttachment.Attachment_URL__c::::::'+srAttachment.Attachment_URL__c);
               
                if(srAttachment.Name != Label.Incident_Report_Doc_Name){
                    body+= 'Violation Image:<a href="'+srAttachment.Attachment_URL__c+'">'+srAttachment.Name+'</a><br/>';
                   
                }
                else{
                    body+= Label.Incident_Report_Doc_Name+':'+'<a href="'+srAttachment.Attachment_URL__c+'">'+srAttachment.Name+'</a><br/>';
                    
                }
            }
            body +=  '</body></html><br/>Regards,LOAMS';
            
           contentValue += body;
        }
                 
        // Callout to sendgrid to send an email   
        SendGridEmailService.SendGridResponse objSendGridResponse =  
            SendGridEmailService.sendEmailService(
            toAddress,
            '', 
            strCCAddress, 
            '', 
            bccAddress, 
            '', 
            lstTemplate[0].Subject,
            '',
            fromAddress_LOAMEmail,
            '',
            Label.Service_Charges_replyTo_Address,
            '',
            'text/html',
            contentValue,
            '',
            //new list<Attachment> ()
           finalAttachmentList
        );
        system.debug('toAddress === ' + toAddress);
         system.debug('strCCAddress === ' + strCCAddress);
          system.debug('bccAddress === ' + bccAddress);
        system.debug('contentValue === ' + contentValue);
         system.debug('finalAttachmentList === ' + finalAttachmentList);
        System.debug('objSendGridResponse:::::::::::'+objSendGridResponse);
         if (objSendGridResponse.ResponseStatus == 'Accepted') {
            EmailMessage mail = new EmailMessage();
            mail.Subject =  lstTemplate[0].Subject;
            mail.MessageDate = System.Today();
            mail.Status = '3';//'Sent';
            mail.relatedToId= violationCaseRecord.Account__c;
            mail.ToAddress = toAddress;
            mail.FromAddress = fromAddress_LOAMEmail;
            mail.TextBody = contentValue;
            mail.Sent_By_Sendgrid__c = true;
            mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
            mail.Booking_Unit__c = violationCaseRecord.Booking_Unit__c;
            //mail.CcAddress = ccAddress;
            //mail.BccAddress = objMail.bccAddress;
             system.debug('Mail obj == ' + mail);
             emailMessageLst.add(mail);
           
        }
        // Send SMS 
        sendSMSForviolation(contentValue,violationCaseRecord);
        if(emailMessageLst.size() > 0){
            insert emailMessageLst;
        }
        
    }

    // This method is used to get all the attached violation notices
    public static List<Attachment> getAttachedViolationNotices(Id fmCaseId){
        List<Attachment> violationNoticeList = [
            SELECT Id,
                    Body,
                    ContentType,
                    Name,
                    ParentId
               FROM Attachment
              WHERE ParentId = :fmCaseId
       ];
        
        System.debug('violationNoticeList:-------'+violationNoticeList);
        if(violationNoticeList != NULL && !violationNoticeList.isEmpty()){
            return violationNoticeList;
        }
        else{
            return NULL;
        }
    }

    // This method is used to fetch violation list
    public static Document getViolationListFromDocument(){

        // query on document to get the violation list
        List<Document> violationListDocument = [
            SELECT Id
                 , Name
                 , Body
              FROM Document
             WHERE Id = :Label.Violation_List_document_Id
            
             LIMIT 1
        ];
        System.debug('violationListDocument:------'+violationListDocument);
        if(violationListDocument != NULL && !violationListDocument.isEmpty()){
            return violationListDocument[0];
        }
        return NULL;
    }

    // This method is used to send the sms 
    
    public static void sendSMSForviolation(String htmlBody,FM_Case__c violationCaseRecord ){
        List<SMS_History__c> lstSMS = new List<SMS_History__c>();
        List<UploadMultipleDocController.MultipleDocRequest> lstReq =
            new List<UploadMultipleDocController.MultipleDocRequest>();
        List<SR_Attachments__c> lstAttachments = new List<SR_Attachments__c>();
        Map<Id, String> mapFMCase_PartyId = new Map<Id, String>();
        
        //Saving violation notice on IPMS to display on portal
            //SMS for Owner
            SMS_History__c objOwnerSMS = new SMS_History__c();
            if(violationCaseRecord != NULL){
                objOwnerSMS.FM_Case__c = violationCaseRecord.Id;
            
                String violationSMSStr = String.format(
                    Label.Violation_SMS_Template, 
                    new List<String>{violationCaseRecord.Unit_Name__c} 
                );
                objOwnerSMS.Message__c = violationSMSStr;
                
                System.debug('==objOwnerSMS.Message__c=' + objOwnerSMS.Message__c);
                objOwnerSMS.Phone_Number__c = violationCaseRecord.Account__r.Mobile_Phone_Encrypt__pc;
                //objOwnerSMS.Phone_Number__c = '00971559270964';
                
                objOwnerSMS.Name = 'Violation Notice: '+violationCaseRecord.Name;
                System.debug('objOwnerSMS:::::::'+objOwnerSMS);
                lstSMS.add(objOwnerSMS);
            
                //SMS for Tenant
                if(String.isNotBlank(violationCaseRecord.Tenant__r.Mobile_Phone_Encrypt__pc)) {
                    
                    SMS_History__c objTenantSMS = new SMS_History__c();
                    objTenantSMS.FM_Case__c = violationCaseRecord.Id;
                    objTenantSMS.Message__c = violationSMSStr;
                    objTenantSMS.Phone_Number__c = violationCaseRecord.Tenant__r.Mobile_Phone_Encrypt__pc;
                    objTenantSMS.Name = 'Violation Notice: '+violationCaseRecord.Name;
                    System.debug('objTenantSMS:::::::'+objTenantSMS);
                    lstSMS.add(objTenantSMS);
                }
                mapFMCase_PartyId.put(violationCaseRecord.Id, violationCaseRecord.Tenant__r.Party_ID__c);
                SR_Attachments__c objAtt = new SR_Attachments__c();
                objAtt.Name = violationCaseRecord.Name + '-'
                            + String.valueOf(System.currentTimeMillis())  +'.violation notice';
                
                objAtt.FM_Account__c = violationCaseRecord.Tenant__c;
                objAtt.FM_Case__c = violationCaseRecord.Id;
                objAtt.Document_Type__c  = 'Violation Notice';
                objAtt.Booking_Unit__c = violationCaseRecord.Booking_Unit__c;
                lstAttachments.add(objAtt);
                UploadMultipleDocController.MultipleDocRequest reqObj = new UploadMultipleDocController.MultipleDocRequest();
                reqObj.fileDescription = 'Violation Notice against unit ' + violationCaseRecord.Booking_Unit__r.Unit_Name__c;
                reqObj.category = 'Document';
                reqObj.entityName = 'Damac Service Requests';
                blob objBlob = Blob.valueOf(htmlBody);
                if(objBlob != null){
                    reqObj.base64Binary =  EncodingUtil.base64Encode(objBlob);
                }
                reqObj.fileId = violationCaseRecord.Name + '-'+ String.valueOf(System.currentTimeMillis()) +'.violation notice';
                reqObj.fileName = violationCaseRecord.Name + '-'
                            + String.valueOf(System.currentTimeMillis())  +'-violation notice.html';
                reqObj.registrationId = violationCaseRecord.Name;
                reqObj.sourceFileName = 'IPMS-'+violationCaseRecord.Tenant__r.party_ID__C+'-Violation Notice';
                reqObj.sourceId = 'IPMS-'+violationCaseRecord.Tenant__r.party_ID__C+'-Violation Notice';
                lstReq.add(reqObj);
            
            if(lstReq.size() > 0) {
                UploadMultipleDocController.data respObj = new UploadMultipleDocController.data();
                        respObj= UploadMultipleDocController.getMultipleDocUrl(lstReq);
                if(respObj != NULL) {
                    if(respObj.status == 'Exception'){
                        System.debug('====EXCEPTION ====' + respObj.message);
                        // errorLogger(respObj.message, '', '');
                        // return null;
                }
                else if(respObj.Data == null || respObj.Data.size()==0){
                    System.debug('====EXCEPTION ==respObj.Data==' + respObj.Data);

                    // errorLogger('Problems while getting response from document upload', '', '');
                    // return null;
                }
                else {
                    Set<SR_Attachments__c> setSRAttachments_Valid = new Set<SR_Attachments__c>();
                    for(SR_Attachments__c att : lstAttachments) {
                        for(UploadMultipleDocController.MultipleDocResponse objData : respObj.data) {
                            if(mapFMCase_PartyId.containsKey(att.FM_Case__c)) {
                                if('IPMS-'+mapFMCase_PartyId.get(att.FM_Case__c)+'-Violation Notice' == objData.PARAM_ID){
                                    if(objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)){
                                        att.Attachment_URL__c = objData.url;
                                        // att.FM_Case__c = objFMCase.Id;
                                        setSRAttachments_Valid.add(att);
                                    }
                                    else{
                                        System.debug('=====objData.PARAM_ID==' + objData.PARAM_ID);
                                        // errorLogger('Problems while getting response from document '+objData.PARAM_ID, '', '');
                                    }
                                }
                            }

                        }
                    }//for
                    List<SR_Attachments__c> lstAttachmentsToInsert = new List<SR_Attachments__c>();
                    lstAttachmentsToInsert.addAll(setSRAttachments_Valid);
                    insert lstAttachmentsToInsert;
                }
            }
        }//req if
            insert lstSMS;
            system.debug('insert zalela sms'+lstSMS);
        }
    }
}