@isTest
public class SendSurveyNotificationTest {
    @isTest
    static void  testSurveyNotification() {
        Test.startTest();
        
        List<SendSurveyNotification.FMCaseDataWrap> argList = new List<SendSurveyNotification.FMCaseDataWrap>();
        
        SendSurveyNotification.FMCaseDataWrap arg1 = new SendSurveyNotification.FMCaseDataWrap();
        arg1.accountId = '6001w000006Xz7E';
        arg1.unitId = '5001w000006Xz7E';
        arg1.unitName = 'ABC/12/A1234';
        arg1.partyId = '123';
        arg1.regId = '456';
        arg1.fmCaseId = '7001w000006Xz7E';
        arg1.fmCaseNumber = 'FMN-12345';
        arg1.srType = 'Move_In';
        argList.add(arg1);
        
        SendSurveyNotification.FMCaseDataWrap arg2 = new SendSurveyNotification.FMCaseDataWrap();
        arg2.accountId = '6001w000006Xz7E';
        arg2.unitId = '5001w000006Xz7E';
        arg2.unitName = 'ABC/12/A1234';
        arg2.partyId = '123';
        arg2.regId = '456';
        arg2.fmCaseId = '7001w000006Xz7E';
        arg2.fmCaseNumber = 'FMN-12345';
        arg2.srType = 'Passport_Detail_Update';
        argList.add(arg2);
        
        SendSurveyNotification.FMCaseDataWrap arg3 = new SendSurveyNotification.FMCaseDataWrap();
        arg3.accountId = '6001w000006Xz7E';
        arg3.unitId = '5001w000006Xz7E';
        arg3.unitName = 'ABC/12/A1234';
        arg3.partyId = '123';
        arg3.regId = '456';
        arg3.fmCaseId = '7001w000006Xz7E';
        arg3.fmCaseNumber = 'FMN-12345';
        arg3.srType = 'Change_of_Contact_Details';
        argList.add(arg3);
        
        SendSurveyNotification.FMCaseDataWrap arg4 = new SendSurveyNotification.FMCaseDataWrap();
        arg4.accountId = '6001w000006Xz7E';
        arg4.unitId = '5001w000006Xz7E';
        arg4.unitName = 'ABC/12/A1234';
        arg4.partyId = '123';
        arg4.regId = '456';
        arg4.fmCaseId = '7001w000006Xz7E';
        arg4.fmCaseNumber = 'FMN-12345';
        arg4.srType = 'NOC_For_FitOut';
        argList.add(arg4);
        
        SendSurveyNotification.FMCaseDataWrap arg5 = new SendSurveyNotification.FMCaseDataWrap();
        arg5.accountId = '6001w000006Xz7E';
        arg5.unitId = '5001w000006Xz7E';
        arg5.unitName = 'ABC/12/A1234';
        arg5.partyId = '123';
        arg5.regId = '456';
        arg5.fmCaseId = '7001w000006Xz7E';
        arg5.fmCaseNumber = 'FMN-12345';
        arg5.srType = 'Move_Out';
        argList.add(arg5);
        
        SendSurveyNotification.FMCaseDataWrap arg6 = new SendSurveyNotification.FMCaseDataWrap();
        arg6.accountId = '6001w000006Xz7E';
        arg6.unitId = '5001w000006Xz7E';
        arg6.unitName = 'ABC/12/A1234';
        arg6.partyId = '123';
        arg6.regId = '456';
        arg6.fmCaseId = '7001w000006Xz7E';
        arg6.fmCaseNumber = 'FMN-12345';
        arg6.srType = 'Request_For_Access_Card';
        argList.add(arg6);
        
        SendSurveyNotification.FMCaseDataWrap arg7 = new SendSurveyNotification.FMCaseDataWrap();
        arg7.accountId = '6001w000006Xz7E';
        arg7.unitId = '5001w000006Xz7E';
        arg7.unitName = 'ABC/12/A1234';
        arg7.partyId = '123';
        arg7.regId = '456';
        arg7.fmCaseId = '7001w000006Xz7E';
        arg7.fmCaseNumber = 'FMN-12345';
        arg7.srType = 'Tenant_Renewal';
        argList.add(arg7);
        SendSurveyNotification.sendPushNotification(argList);
        
        Test.stopTest();
    }
}