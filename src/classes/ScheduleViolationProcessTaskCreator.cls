/**
 * @File Name          : ScheduleViolationProcessTaskCreator.cls
 * @Description        : This class is used to create task for violation process and scheduled from taskTrigger
 * @Author             : Dipika Rajput
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/20/2019, 12:33:19 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/19/2019, 6:19:39 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public without sharing class ScheduleViolationProcessTaskCreator implements Schedulable{
    
    public Task taskObj;
    public Id OwnerIdInst;
    
    // Constructor of the class to get the parameters from TaskTriggerHandlerManager class
    public ScheduleViolationProcessTaskCreator(Task taskinstance,Id ownerId){
        taskObj = taskinstance;
        OwnerIdInst = ownerId;
    }
    public void execute(SchedulableContext ctx) {
        System.debug('taskObj:::::::::'+taskObj+'--OwnerIdInst--'+OwnerIdInst);
        createViolationRectifiedTask(taskObj,OwnerIdInst);
    }

    // This method is used to create Violation rectified task for violation process
    public static void createViolationRectifiedTask(Task taskinstance,Id ownerId){
        Task taskForAdmin = new Task();
        taskForAdmin.Assigned_User__c='FM Admin';
        taskForAdmin.Priority='Normal';
        taskForAdmin.WhatID=taskinstance.whatid;
        taskForAdmin.status='Not Started';
        taskForAdmin.subject=Label.Violation_Admin_Task;
        taskForAdmin.Process_Name__c = taskinstance.Process_Name__c;
        taskForAdmin.ActivityDate = System.today();
        taskForAdmin.ownerid = ownerId;
        insert taskForAdmin;
    }
}