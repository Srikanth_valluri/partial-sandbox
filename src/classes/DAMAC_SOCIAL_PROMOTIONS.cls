/*
    Created By              : SF Support
    Created Date            : 08/03/2020
    Last Modified By        : Srikanth V
    Last Modified Date      : 09/03/2020
    Desription              : To create social Post records for the customer, to send the customer sendgrid id via API
    Request JSON            : {
                                "id": "15891600D1n0000DmEkJpAAK",
                                "Twitter": {    
                                    "twitter_id": "12345",
                                },
                                "Facebook": {
                                    "facebook_id": "test234233",
                                }
                            }
    
*/

@RestResource(urlMapping='/socialpromos/*')
global without sharing class DAMAC_SOCIAL_PROMOTIONS { 

    // HTTP Post method to create social post records based on the customer sendgrid id
    @HTTPPost 
    global static void doPOST () {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        String requestBody = req.requestBody.toString();
        System.debug (requestBody);
        
        Map <String, Object> resObject = (Map<String, Object>) JSON.deserializeUntyped (requestBody);
        String customerId = String.valueOf (resObject.get ('id'));
        String returnResponse = '{"error": "customer not found"}';
                
        // Querrying the customer based on sendgrid id
        List <Account> accounts = new List <Account> ();
        accounts = [SELECT Name, Email__c, Sendgrid_Id__c
                        FROM Account 
                        WHERE Sendgrid_Id__c =: customerId 
                            AND recordtype.name = 'Person Account'
                        LIMIT 1];
        if (accounts.size () > 0) {
            Account acc = accounts[0];
            Map <String, Social_Post__c> socialPostMap = new Map <String, Social_Post__c> ();
            
            // Querrying for meatada to map the social post fields and JSON attributes
            for (Social_Post_Fields_Mapping__mdt data : [SELECT Json_Attribute_Name__c, SF_API_Name__c, Type__c FROM Social_Post_Fields_Mapping__mdt 
                                                            WHERE Type__c != NULL])
            {            
                if (resObject.containsKey (data.Type__c)) {
                    Map <String, Object> socialObject = (Map <String, Object>) JSON.deserializeUntyped (JSON.serialize (resObject.get (data.Type__c)));
                    // Mapping social post object with metadata stored
                    for (String key :socialObject.keySet ()) {
                        Social_Post__c post = new Social_Post__c ();
                        //post.whoId = acc.Id;
                        post.Account__c = acc.id;
                        post.Name = data.Type__c;
                        if (socialPostMap.containsKey (data.Type__c)) {
                            post = socialPostMap.get (data.Type__c);
                        }
                        if (key == data.Json_Attribute_Name__c)
                            post.put (data.SF_API_Name__c, ''+socialObject.get (key));
                           
                        socialPostMap.put (data.Type__c, post);
                    }
                }
            }
            insert socialPostMap.values ();
            returnResponse = '{"Success" : "true"}';
        }
        
        RestContext.response.addHeader('Content-Type', 'application/json');        
        RestContext.response.responseBody = Blob.valueOf(returnResponse);
        
        
    }

    // GET API to send the customer details based on the sendgrid id request param
    @HttpGET
    global static void doGET(){
        String customerId = RestContext.request.params.get('cid');
        string returnResponse = '{"error": "customer not found"}';

        list<Account> FoundAccount = new list<Account>();
        FoundAccount = [select id,Name,Email__c,Sendgrid_Id__c from Account where Sendgrid_Id__c=:customerId and recordtype.name='Person Account' LIMIT 1];
        
        if(FoundAccount.size() > 0 ){
            custInfo wrapper = new custInfo();
            wrapper.id = FoundAccount[0].Sendgrid_Id__c;
            wrapper.name = FoundAccount[0].name;
            wrapper.email = FoundAccount[0].Email__c;
            wrapper.status = 'active';            
            returnResponse =   JSON.serialize(wrapper);
        }
        RestContext.response.addHeader('Content-Type', 'application/json');        
        RestContext.response.responseBody = Blob.valueOf(returnResponse);
        
        /*
        socialPersona p = new socialPersona();
        p.ParentId = FoundAccount[0].id;
        p.Name = 'Twitter';
        p.provider = 'twitter';
        insert p;
        
        
        socialPost p = new socialpost();
        p.whoid = FoundAccount[0].id;
        p.Name = 'Twitter';
        p.provider = 'twitter';
        insert p;
        */
    }
    
    
    public class custInfo {
        public string id;
        public string name;
        public string email;
        public string status;
        
    }

}