@isTest
public class UpdateVehicleDetailsControllerTest {

    @isTest
    static void testMethod1() {

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;
        CustomerCommunityUtils.customerAccountId = account.Id;


        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(account);
        insert sr;

        Booking__c booking = new Booking__c(
                                    Deal_SR__c = sr.Id,
                                    Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                             Owner__c = account.Id,
                                             Resident__c =  account.Id,
                                             Registration_Status_Code__c = 'LE',
                                             Handover_Flag__c = 'Y',
                                             Early_Handover__c = true,
                                             Booking__c = booking.Id,
                                             Unit_Name__c = 'Test Unit Name',
                                             Registration_ID__c = '3901');
         insert bookingUnit;

        FM_Additional_Detail__c fmAddDetails = new FM_Additional_Detail__c( IsNewInfo__c = true,
                                                                              Vehicle_Account__c = account.Id,
                                                                              Booking_Unit__c = bookingUnit.Id,
                                                                              Vehicle_Number__c = '1324');
        insert fmAddDetails;


        PageReference myVfPage = Page.CommunityPortal;

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('strAccountId',account.Id);
        //ApexPages.currentPage().getParameters().put('strSelectedUnit',bookingUnit.Id);

        Test.startTest();
        UpdateVehicleDetailsController obj = new UpdateVehicleDetailsController();


        obj.selectUnit();

        //obj.initializeAddDetailsMap();


        //Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        //obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        //obj.strDocumentName='Test_Document_Vehicle_Detaills.htm';
        //obj.uploadDocument();
        Test.stopTest();
    }

    @isTest
    static void testUploaddoc() {

        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;
        CustomerCommunityUtils.customerAccountId = account.Id;


        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(account);
        insert sr;

        Booking__c booking = new Booking__c(
                                    Deal_SR__c = sr.Id,
                                    Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                     Owner__c = account.Id,
                                     Resident__c =  account.Id,
                                     Registration_Status_Code__c = 'LE',
                                     Handover_Flag__c = 'Y',
                                     Early_Handover__c = true,
                                     Booking__c = booking.Id,
                                     Unit_Name__c = 'Test Unit Name',
                                     Registration_ID__c = '3901');
        insert bookingUnit;

        FM_Additional_Detail__c fmAddDetails = new FM_Additional_Detail__c( IsNewInfo__c = true,
                                                                              Vehicle_Account__c = account.Id,
                                                                              Booking_Unit__c = bookingUnit.Id,
                                                                              Vehicle_Number__c = '1324');
        insert fmAddDetails;


        //PageReference myVfPage = Page.CommunityPortal;

        //Test.setCurrentPage(myVfPage);
        //ApexPages.currentPage().getParameters().put('strAccountId',account.Id);
        //ApexPages.currentPage().getParameters().put('strSelectedUnit',bookingUnit.Id);
        
        //Test.setMock( WebServiceMock.class, new MoveOutMockClass() );

        //Set mock response
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());


        Test.starttest();
        UpdateVehicleDetailsController obj = new UpdateVehicleDetailsController();

        //PageReference myVfPage = Page.CommunityPortal;

        //Test.setCurrentPage(myVfPage);
        //ApexPages.currentPage().getParameters().put('strAccountId',account.Id);

        //obj.intAddNumOfVehicles = 1;
        //obj.initializeAddDetailsMap();
        //obj.initializeNewAddDetails();
        //obj.strSelectedUnit = bookingUnit.Id;
        //obj.strAccountId = account.Id;

        obj.intAddNumOfVehicles = 1;
        obj.initializeNewAddDetails();

        String strDocumentBody = 'ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        String strDocumentName = 'Test_Document_Vehicle_Detaills.htm';

        obj.strDocumentBody=strDocumentBody;
        obj.strDocumentName=strDocumentName;
        UpdateVehicleDetailsController.uploadDocument(account.id, bookingUnit.id, strDocumentName, strDocumentBody);

        //obj.intAddNumOfVehicles = 1;
        //obj.initializeNewAddDetails();

        //obj.getshowSavebtn();

        obj.saveVehicleDetails();
        //obj.deleteAttachment();
        Test.StopTest();
    }

    @isTest 
    static void testFileToOCRService() {
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;
        CustomerCommunityUtils.customerAccountId = account.Id;


        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(account);
        insert sr;

        Booking__c booking = new Booking__c(
                                    Deal_SR__c = sr.Id,
                                    Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                     Owner__c = account.Id,
                                     Resident__c =  account.Id,
                                     Registration_Status_Code__c = 'LE',
                                     Handover_Flag__c = 'Y',
                                     Early_Handover__c = true,
                                     Booking__c = booking.Id,
                                     Unit_Name__c = 'Test Unit Name',
                                     Registration_ID__c = '3901');
        insert bookingUnit;

        FM_Additional_Detail__c fmAddDetails = new FM_Additional_Detail__c( IsNewInfo__c = true,
                                                                              Vehicle_Account__c = account.Id,
                                                                              Booking_Unit__c = bookingUnit.Id,
                                                                              Vehicle_Number__c = '1324');
        insert fmAddDetails;

        Test.startTest();
        
        String testStr1 = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAVC7+Yn3mqFIVqALCeTO/+jt8/9yo3TzP';
        String testStr2 = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAVC7+Yn3mqFIVqALCeTO/+jt8/9yo3TzP';

        List<String> lstStr = new List<String>();
        lstStr.add(testStr1);
        lstStr.add(testStr2);

        String jsonStr =  '   {  '  + 
         '       "status": "Success",  '  + 
         '       "result": "Success",  '  + 
         '       "errorCode": "0",  '  + 
         '       "errorDescription": null,  '  + 
         '       "serverTime": 10512,  '  + 
         '       "fields": [  '  + 
         '           {  '  + 
         '               "key": "Traffic Plate no",  '  + 
         '               "value": "597M"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "T. C. No",  '  + 
         '               "value": "12568000"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Owner",  '  + 
         '               "value": "DAVID VARGHESE VAZHAPPILLY DAVIS VARGHESE"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Nationality",  '  + 
         '               "value": "India"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Exp. Date",  '  + 
         '               "value": "23/08/2020"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Reg. Date",  '  + 
         '               "value": "05/09/2013"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Ins. Exp",  '  + 
         '               "value": "23/09/2020"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Policy",  '  + 
         '               "value": "BA202119000385"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Mortage",  '  + 
         '               "value": ""  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Model",  '  + 
         '               "value": "2013"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Origin",  '  + 
         '               "value": "United Kingdom"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Empty Weight",  '  + 
         '               "value": "1600"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "No. of Pass",  '  + 
         '               "value": "4"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Veh Type",  '  + 
         '               "value": "FORD MUSTANG"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "G. V. W.",  '  + 
         '               "value": "2000K"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Engine No",  '  + 
         '               "value": "NIL"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Chassie No",  '  + 
         '               "value": "1ZVBP8F21872"  '  + 
         '           }  '  + 
         '       ]  '  + 
         '  }  ' ; 

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));

        

          UpdateVehicleDetailsController.convertToBlob(lstStr);

          UpdateVehicleDetailsController obj = new UpdateVehicleDetailsController();
          obj.strResponseMap = '{&quot;status&quot;:&quot;Success&quot;,&quot;result&quot;:&quot;Success&quot;,&quot;errorCode&quot;:&quot;0&quot;,&quot;errorDescription&quot;:null,&quot;serverTime&quot;:10319,&quot;fields&quot;:[{&quot;key&quot;:&quot;Traffic Plate no&quot;,&quot;value&quot;:&quot;597M&quot;},{&quot;key&quot;:&quot;T. C. No&quot;,&quot;value&quot;:&quot;12568000&quot;},{&quot;key&quot;:&quot;Owner&quot;,&quot;value&quot;:&quot;DAVID VARGHESE VAZHAPPILLY DAVIS VARGHESE&quot;},{&quot;key&quot;:&quot;Nationality&quot;,&quot;value&quot;:&quot;India&quot;},{&quot;key&quot;:&quot;Exp. Date&quot;,&quot;value&quot;:&quot;23/08/2020&quot;},{&quot;key&quot;:&quot;Reg. Date&quot;,&quot;value&quot;:&quot;05/09/2013&quot;},{&quot;key&quot;:&quot;Ins. Exp&quot;,&quot;value&quot;:&quot;23/09/2020&quot;},{&quot;key&quot;:&quot;Policy&quot;,&quot;value&quot;:&quot;BA202119000385&quot;},{&quot;key&quot;:&quot;Mortage&quot;,&quot;value&quot;:&quot;&quot;},{&quot;key&quot;:&quot;Model&quot;,&quot;value&quot;:&quot;2013&quot;},{&quot;key&quot;:&quot;Origin&quot;,&quot;value&quot;:&quot;United Kingdom&quot;},{&quot;key&quot;:&quot;Empty Weight&quot;,&quot;value&quot;:&quot;1600&quot;},{&quot;key&quot;:&quot;No. of Pass&quot;,&quot;value&quot;:&quot;4&quot;},{&quot;key&quot;:&quot;Veh Type&quot;,&quot;value&quot;:&quot;FORD MUSTANG&quot;},{&quot;key&quot;:&quot;G. V. W.&quot;,&quot;value&quot;:&quot;2000K&quot;},{&quot;key&quot;:&quot;Engine No&quot;,&quot;value&quot;:&quot;NIL&quot;},{&quot;key&quot;:&quot;Chassie No&quot;,&quot;value&quot;:&quot;1ZVBP8F21872&quot;}]}';

          obj.indexVal = 0;
          obj.intAddNumOfVehicles = 1;
          obj.initializeNewAddDetails();

          obj.PopulateFormData();

        Test.stopTest();
        
        //2
        

    } 
    
    @isTest
    static void testFiletoOCRService2() {
    
    Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;
        CustomerCommunityUtils.customerAccountId = account.Id;


        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(account);
        insert sr;

        Booking__c booking = new Booking__c(
                                    Deal_SR__c = sr.Id,
                                    Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                     Owner__c = account.Id,
                                     Resident__c =  account.Id,
                                     Registration_Status_Code__c = 'LE',
                                     Handover_Flag__c = 'Y',
                                     Early_Handover__c = true,
                                     Booking__c = booking.Id,
                                     Unit_Name__c = 'Test Unit Name',
                                     Registration_ID__c = '3901');
        insert bookingUnit;

        FM_Additional_Detail__c fmAddDetails = new FM_Additional_Detail__c( IsNewInfo__c = true,
                                                                              Vehicle_Account__c = account.Id,
                                                                              Booking_Unit__c = bookingUnit.Id,
                                                                              Vehicle_Number__c = '1324');
        insert fmAddDetails;
    
        Test.startTest();
        
        String testStr1 = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAVC7+Yn3mqFIVqALCeTO/+jt8/9yo3TzP==';
        String testStr2 = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAVC7+Yn3mqFIVqALCeTO/+jt8/9yo3TzP==';

        List<String> lstStr = new List<String>();
        lstStr.add(testStr1);
        lstStr.add(testStr2);

        String jsonStr =  '   {  '  + 
         '       "status": "Success",  '  + 
         '       "result": "Success",  '  + 
         '       "errorCode": "0",  '  + 
         '       "errorDescription": null,  '  + 
         '       "serverTime": 10512,  '  + 
         '       "fields": [  '  + 
         '           {  '  + 
         '               "key": "Traffic Plate no",  '  + 
         '               "value": "597M"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "T. C. No",  '  + 
         '               "value": "12568000"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Owner",  '  + 
         '               "value": "DAVID VARGHESE VAZHAPPILLY DAVIS VARGHESE"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Nationality",  '  + 
         '               "value": "India"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Exp. Date",  '  + 
         '               "value": "23/08/2020"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Reg. Date",  '  + 
         '               "value": "05/09/2013"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Ins. Exp",  '  + 
         '               "value": "23/09/2020"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Policy",  '  + 
         '               "value": "BA202119000385"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Mortage",  '  + 
         '               "value": ""  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Model",  '  + 
         '               "value": "2013"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Origin",  '  + 
         '               "value": "United Kingdom"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Empty Weight",  '  + 
         '               "value": "1600"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "No. of Pass",  '  + 
         '               "value": "4"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Veh Type",  '  + 
         '               "value": "FORD MUSTANG"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "G. V. W.",  '  + 
         '               "value": "2000K"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Engine No",  '  + 
         '               "value": "NIL"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Chassie No",  '  + 
         '               "value": "1ZVBP8F21872"  '  + 
         '           }  '  + 
         '       ]  '  + 
         '  }  ' ; 

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));

        

          UpdateVehicleDetailsController.convertToBlob(lstStr);

          UpdateVehicleDetailsController obj = new UpdateVehicleDetailsController();
          obj.strResponseMap = '{&quot;status&quot;:&quot;Success&quot;,&quot;result&quot;:&quot;Success&quot;,&quot;errorCode&quot;:&quot;0&quot;,&quot;errorDescription&quot;:null,&quot;serverTime&quot;:10319,&quot;fields&quot;:[{&quot;key&quot;:&quot;Traffic Plate no&quot;,&quot;value&quot;:&quot;597M&quot;},{&quot;key&quot;:&quot;T. C. No&quot;,&quot;value&quot;:&quot;12568000&quot;},{&quot;key&quot;:&quot;Owner&quot;,&quot;value&quot;:&quot;DAVID VARGHESE VAZHAPPILLY DAVIS VARGHESE&quot;},{&quot;key&quot;:&quot;Nationality&quot;,&quot;value&quot;:&quot;India&quot;},{&quot;key&quot;:&quot;Exp. Date&quot;,&quot;value&quot;:&quot;23/08/2020&quot;},{&quot;key&quot;:&quot;Reg. Date&quot;,&quot;value&quot;:&quot;05/09/2013&quot;},{&quot;key&quot;:&quot;Ins. Exp&quot;,&quot;value&quot;:&quot;23/09/2020&quot;},{&quot;key&quot;:&quot;Policy&quot;,&quot;value&quot;:&quot;BA202119000385&quot;},{&quot;key&quot;:&quot;Mortage&quot;,&quot;value&quot;:&quot;&quot;},{&quot;key&quot;:&quot;Model&quot;,&quot;value&quot;:&quot;2013&quot;},{&quot;key&quot;:&quot;Origin&quot;,&quot;value&quot;:&quot;United Kingdom&quot;},{&quot;key&quot;:&quot;Empty Weight&quot;,&quot;value&quot;:&quot;1600&quot;},{&quot;key&quot;:&quot;No. of Pass&quot;,&quot;value&quot;:&quot;4&quot;},{&quot;key&quot;:&quot;Veh Type&quot;,&quot;value&quot;:&quot;FORD MUSTANG&quot;},{&quot;key&quot;:&quot;G. V. W.&quot;,&quot;value&quot;:&quot;2000K&quot;},{&quot;key&quot;:&quot;Engine No&quot;,&quot;value&quot;:&quot;NIL&quot;},{&quot;key&quot;:&quot;Chassie No&quot;,&quot;value&quot;:&quot;1ZVBP8F21872&quot;}]}';

          obj.indexVal = 0;
          obj.intAddNumOfVehicles = 1;
          obj.initializeNewAddDetails();

          obj.PopulateFormData();

        Test.stopTest();
    }
    
    @isTest
    static void testFiletoOCRService3() {
    
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;
        CustomerCommunityUtils.customerAccountId = account.Id;


        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(account);
        insert sr;

        Booking__c booking = new Booking__c(
                                    Deal_SR__c = sr.Id,
                                    Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                     Owner__c = account.Id,
                                     Resident__c =  account.Id,
                                     Registration_Status_Code__c = 'LE',
                                     Handover_Flag__c = 'Y',
                                     Early_Handover__c = true,
                                     Booking__c = booking.Id,
                                     Unit_Name__c = 'Test Unit Name',
                                     Registration_ID__c = '3901');
        insert bookingUnit;

        FM_Additional_Detail__c fmAddDetails = new FM_Additional_Detail__c( IsNewInfo__c = true,
                                                                              Vehicle_Account__c = account.Id,
                                                                              Booking_Unit__c = bookingUnit.Id,
                                                                              Vehicle_Number__c = '1324');
        insert fmAddDetails;
    
        Test.startTest();
        
        String testStr1 = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAVC7+Yn3mqFIVqALCeTO/+jt8/9yo3TzP=';
        String testStr2 = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAVC7+Yn3mqFIVqALCeTO/+jt8/9yo3TzP=';

        List<String> lstStr = new List<String>();
        lstStr.add(testStr1);
        lstStr.add(testStr2);

        String jsonStr =  '   {  '  + 
         '       "status": "Success",  '  + 
         '       "result": "Success",  '  + 
         '       "errorCode": "0",  '  + 
         '       "errorDescription": null,  '  + 
         '       "serverTime": 10512,  '  + 
         '       "fields": [  '  + 
         '           {  '  + 
         '               "key": "Traffic Plate no",  '  + 
         '               "value": "597M"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "T. C. No",  '  + 
         '               "value": "12568000"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Owner",  '  + 
         '               "value": "DAVID VARGHESE VAZHAPPILLY DAVIS VARGHESE"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Nationality",  '  + 
         '               "value": "India"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Exp. Date",  '  + 
         '               "value": "23/08/2020"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Reg. Date",  '  + 
         '               "value": "05/09/2013"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Ins. Exp",  '  + 
         '               "value": "23/09/2020"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Policy",  '  + 
         '               "value": "BA202119000385"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Mortage",  '  + 
         '               "value": ""  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Model",  '  + 
         '               "value": "2013"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Origin",  '  + 
         '               "value": "United Kingdom"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Empty Weight",  '  + 
         '               "value": "1600"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "No. of Pass",  '  + 
         '               "value": "4"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Veh Type",  '  + 
         '               "value": "FORD MUSTANG"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "G. V. W.",  '  + 
         '               "value": "2000K"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Engine No",  '  + 
         '               "value": "NIL"  '  + 
         '           },  '  + 
         '           {  '  + 
         '               "key": "Chassie No",  '  + 
         '               "value": "1ZVBP8F21872"  '  + 
         '           }  '  + 
         '       ]  '  + 
         '  }  ' ; 

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));

        

          UpdateVehicleDetailsController.convertToBlob(lstStr);

          UpdateVehicleDetailsController obj = new UpdateVehicleDetailsController();
          obj.strResponseMap = '{&quot;status&quot;:&quot;Success&quot;,&quot;result&quot;:&quot;Success&quot;,&quot;errorCode&quot;:&quot;0&quot;,&quot;errorDescription&quot;:null,&quot;serverTime&quot;:10319,&quot;fields&quot;:[{&quot;key&quot;:&quot;Traffic Plate no&quot;,&quot;value&quot;:&quot;597M&quot;},{&quot;key&quot;:&quot;T. C. No&quot;,&quot;value&quot;:&quot;12568000&quot;},{&quot;key&quot;:&quot;Owner&quot;,&quot;value&quot;:&quot;DAVID VARGHESE VAZHAPPILLY DAVIS VARGHESE&quot;},{&quot;key&quot;:&quot;Nationality&quot;,&quot;value&quot;:&quot;India&quot;},{&quot;key&quot;:&quot;Exp. Date&quot;,&quot;value&quot;:&quot;23/08/2020&quot;},{&quot;key&quot;:&quot;Reg. Date&quot;,&quot;value&quot;:&quot;05/09/2013&quot;},{&quot;key&quot;:&quot;Ins. Exp&quot;,&quot;value&quot;:&quot;23/09/2020&quot;},{&quot;key&quot;:&quot;Policy&quot;,&quot;value&quot;:&quot;BA202119000385&quot;},{&quot;key&quot;:&quot;Mortage&quot;,&quot;value&quot;:&quot;&quot;},{&quot;key&quot;:&quot;Model&quot;,&quot;value&quot;:&quot;2013&quot;},{&quot;key&quot;:&quot;Origin&quot;,&quot;value&quot;:&quot;United Kingdom&quot;},{&quot;key&quot;:&quot;Empty Weight&quot;,&quot;value&quot;:&quot;1600&quot;},{&quot;key&quot;:&quot;No. of Pass&quot;,&quot;value&quot;:&quot;4&quot;},{&quot;key&quot;:&quot;Veh Type&quot;,&quot;value&quot;:&quot;FORD MUSTANG&quot;},{&quot;key&quot;:&quot;G. V. W.&quot;,&quot;value&quot;:&quot;2000K&quot;},{&quot;key&quot;:&quot;Engine No&quot;,&quot;value&quot;:&quot;NIL&quot;},{&quot;key&quot;:&quot;Chassie No&quot;,&quot;value&quot;:&quot;1ZVBP8F21872&quot;}]}';

          obj.indexVal = 0;
          obj.intAddNumOfVehicles = 1;
          obj.initializeNewAddDetails();

          obj.PopulateFormData();

        Test.stopTest();
    }
    
    
}