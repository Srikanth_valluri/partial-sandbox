public with sharing class CustomerNotificationBatchHelper {
    
    public static String makeQueryString() { 
        //return 'SELECT Id, Account__c, Email__c, Mobile_Phone__c, Due_Date__c, Registration_ID__c, Account__r.IsPersonAccount, Account__r.Email__c, Account__r.Email__pc, Booking_Unit__c FROM Calling_List__c WHERE Id = \'a2U7E000000Bb8O\'';
        return 'SELECT Id, Account__c, Mobile_Phone__c, Due_Date__c, Actual_Outstanding__c, Email__c, Registration_ID__c, Account__r.IsPersonAccount, Account__r.Email__c, Account__r.Email__pc, Booking_Unit__c FROM Calling_List__c WHERE RecordType.DeveloperName = \'Collections_Calling_List\' AND Actual_Outstanding__c > 0';
    }
    
    public static CustomerNotificationBatch.CustomerNotificationInfoWrapper getMapDaysEmailTemplate( 
                                            list<Collection_Customer_Notification_Setting__mdt> lstSetting,
                                            CustomerNotificationBatch.CustomerNotificationInfoWrapper objInfoWrap ) {
                                                
        if( lstSetting != null && !lstSetting.isEmpty() ) {
            for( Collection_Customer_Notification_Setting__mdt objSetting : lstSetting ) {
                if( String.isNotBlank( objSetting.Email_Template_API_Name__c ) && objSetting.Number_of_Days__c != null ) {
                    if( objInfoWrap.mapDaysNotifications.containsKey( Integer.valueOf( objSetting.Number_of_Days__c ) ) ) {
                        objInfoWrap.mapDaysNotifications.get( Integer.valueOf( objSetting.Number_of_Days__c ) ).add( objSetting );
                    }
                    else {
                        objInfoWrap.mapDaysNotifications.put( Integer.valueOf( objSetting.Number_of_Days__c), new list<Collection_Customer_Notification_Setting__mdt>{ objSetting } );
                    }
                    objInfoWrap.setEmailTemplateNames.add( objSetting.Email_Template_API_Name__c );
                }
            }
            
        }
        return objInfoWrap ;
    }
    
    public static CustomerNotificationBatch.CustomerNotificationInfoWrapper getEmailTemplateMap( map<Id, EmailTemplate> mapEmailTemplate,
                                                                  CustomerNotificationBatch.CustomerNotificationInfoWrapper objInfoWrap ) {
        
        if( mapEmailTemplate != null && !mapEmailTemplate.isEmpty() ) {
            for( EmailTemplate objTemplate : mapEmailTemplate.values() ) {
                objInfoWrap.mapDNEmailTemplate.put( objTemplate.DeveloperName, objTemplate ); 
            }
        }
        return objInfoWrap; 
    }
    
    public static SMS_History__c sendSMSToCustomer( SMS_History__c objSMSHist ) {
		
		String strUserName = '';
		String strPassword = '';
		String strSID = '';
		
		if( objSMSHist.FM_Case__c != null ) {
			strUserName = 'loams.';
			strPassword = '1@#$%qwert';
			strSID = 'LOAMS';
		}else {
			strUserName = 'salesforce';
			strPassword = 'D@$al3sF0rc387!';
			strSID = 'DAMAC GRP';               
		}
		
        /*String strUserName= 'salesforce';
        String strPassword= 'D@$al3sF0rc387!';*/
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST' ); // Method Type
        req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx');

        String PhoneNumber = objSMSHist.Phone_Number__c;
        if(String.isNotBlank ( PhoneNumber ) ) { 
            if(PhoneNumber.startsWith('00')) {
                PhoneNumber = PhoneNumber.removeStart('00');
            }else if(PhoneNumber.startsWith('0')) {
                PhoneNumber = PhoneNumber.removeStart('0');
            }
        system.debug( ' PhoneNumber : ' + PhoneNumber);
        }
        
        strSID = SMSClass.getSenderName(strUserName, PhoneNumber, false);

        req.setBody('user='+ strUserName + '&passwd=' + strPassword +'&message=' 
                    + GenericUtility.encodeChar(objSMSHist.Message__c) + '&mobilenumber=' 
                    + PhoneNumber + '&sid='+strSID+ '&MTYPE=LNG&DR=Y'); // Request Parameters
        
        Http http = new Http();
        try {
            system.debug( ' req : ' + req);
            HTTPResponse res = http.send( req );
            system.debug( ' res : ' + res);
            if( String.isNotBlank( res.getBody() ) ) {
                system.debug( ' res.getBody() : ' + res.getBody());
                if(String.valueOf(res.getBody()).contains('OK:')){
                    objSMSHist.Description__c = res.getBody();
                    objSMSHist.Is_SMS_Sent__c = true ;
                    objSMSHist.sms_Id__c = res.getBody().substringAfter('OK:');
                    return objSMSHist ;
                }
                else {
                    objSMSHist.Description__c = res.getBody();
                    //objSMSHist.Is_SMS_Sent__c = true ;
                    return objSMSHist ;
                }
            }
        }
        catch( Exception e ) {
            objSMSHist.Description__c = e.getMessage();
            //objSMSHist.Is_SMS_Sent__c = true ;
            return objSMSHist ;
        }
        return null;
    }
    
    
    
    /*
    public static SMS_History__c sendSMSToCustomer( String strMobileNumber, String strMessageBody, Calling_List__c objCall ) {
        String strUserName= 'salesforce';
        String strPassword= 'D@$al3sF0rc387!';
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST' ); // Method Type
        req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx');
        req.setBody('user='+ strUserName + '&passwd=' + strPassword +'&message=' + strMessageBody + '&mobilenumber=' + strMobileNumber + '&sid=DAMAC GRP'); // Request Parameters
        
        Http http = new Http();
        try {
            HTTPResponse res = http.send( req );
            if( String.isNotBlank( res.getBody() ) ) {
                SMS_History__c objSMSHist = new SMS_History__c();
                objSMSHist.Message__c = strMessageBody;
                objSMSHist.Phone_Number__c = strMobileNumber ;
                //objSMSHist.Customer__c = objCall.Account__c;
                objSMSHist.Calling_List__c = objCall.Id;
                objSMSHist.Description__c = res.getBody();
                return objSMSHist ;
            }
        }
        catch( Exception e ) {
            system.debug('== Exception occured =='+e.getMessage());
            SMS_History__c objSMSHist = new SMS_History__c();
            objSMSHist.Message__c = strMessageBody;
            objSMSHist.Phone_Number__c = strMobileNumber ;
            //objSMSHist.Customer__c = objCall.Account__c;
            objSMSHist.Calling_List__c = objCall.Id;
            objSMSHist.Description__c = e.getMessage() + ' \nStack Trace : ' +e.getStackTraceString();
            return objSMSHist ;
        }
        return null;
    }
    */
  
}