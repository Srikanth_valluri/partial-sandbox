/**
* @File Name          : CdDailerRestService.cls
* @Description        : 
* @Author             : Mosib@espl.com
* @Group              : 
* @Last Modified By   : Mosib@espl.com
* @Last Modified On   : 16/6/2020, 5:50:18 PM
* @Modification Log   : 
* Ver       Date            Author                 Modification
* 1.0       16/6/2020,       Mosib@espl.com         Initial Version
**/

public with sharing class CdDailerRestService {
    
    @testVisible private static final String GET                    = 'GET';

    public CdDailerRestService() {
    }
    
   
    public static String getRequest(String EncrytMob ,String clId ) {
        
        String UserName = UserInfo.getUserName();//Returns the context user's login name.
        //Ameyo_Integration__c dailerSettings = Ameyo_Integration__c.getInstance();
        //String EncryptMob='d2e9b1d67707350066ccf382d9efe778';
        //dailerSettings.
        //6e17d812481aaba4d196d3a12152a84d ?command=clickToDialWithToken&data=
        //a3A25000000NxXA  // +'"searchable": "'+clId+'","shouldAddCustomer": "false",'
        
        /*String Endpoint = 'https://cdialer.damacgroup.com:8443/ameyowebaccess/command/?command=clickToDialWithToken&data='
        +'{"userId": "'+UserName+'","campaignId": "7","phone": "'+EncrytMob+'",'
        +'"shouldAddCustomer": "false",'
        +'"additionalParams": {"phone": "'+EncrytMob+'","id": "'+clId+'"}}';*/
        
        String Endpoint = 'https://cdialer.damacgroup.com:8443/ameyowebaccess/command/?command=clickToDialWithToken&data='
        +'{%22userId%22:%22'+UserName+'%22,%22campaignId%22:%227%22,%22phone%22:%22'+EncrytMob+'%22,'
        +'%22shouldAddCustomer%22:%22false%22,'
        +'%22additionalParams%22:{%22phone%22:%22'+EncrytMob+'%22,%22id%22:%22'+clId+'%22}}';
        
        
        System.debug('Endpoint==='+Endpoint);
        
                HttpRequest request = new HttpRequest();
                request.setHeader( 'hash-key' ,'ca450c5b2fea0c071cbabc9ce70bf6e' );
                request.setHeader( 'host' , 'cdialer.damacgroup.com' );
                request.setHeader( 'requesting-host' , 'cdialer.damacgroup.com' );
                request.setHeader( 'Content-Type' , 'application/json' );
                request.setHeader( 'policy-name' , 'token-based-authorization-policy' );
                request.setEndpoint( Endpoint );
                request.setMethod( 'POST');
               /** request.setHeader( 'hash-key' , dailerSettings.hash_key__c );
                    request.setHeader( 'host' , dailerSettings.host__c );
                    request.setHeader( 'requesting-host' , dailerSettings.requesting_host__c );
                    request.setHeader( 'Content-Type' , dailerSettings.Content_Type__c );
                    request.setHeader( 'policy-name' , dailerSettings.policy_name__c );
                **/
                
                request.setTimeout(120000) ;
                System.debug('request==='+request);
                HttpResponse response = new Http().send(request); 
                System.debug('response'+response);
                if(response.getStatusCode() == 200) {
                    
                    return NULL;
                }else{
                    return NULL;
                }
            } 
   
}