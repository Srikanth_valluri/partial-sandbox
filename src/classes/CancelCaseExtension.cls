/*
 * Description: Class used to cancel Non-CRE open Tasks from IPMS and Salesforce.
 * Author : Vivek Shinde
 */

public class CancelCaseExtension {

    public Case objCase;

    public CancelCaseExtension(ApexPages.StandardController controller) {
        objCase = (Case)controller.getRecord();
    }

    /**
     * Method used to cancel the Tasks and Case from IPMS and Salesforce
     */
    public pageReference cancelNonCRETasks() {
        List<Task> lstTasksToUpdate = new List<Task>();
        Set<String> setTaskStatus = new Set<String> {'Completed', 'Cancelled', 'Closed', 'Reject' , 'Reject-Duplicate', 'Aborted'};

        if (String.isNotBlank(objCase.Id)) {
            
            // Fetch the Case details
            objCase = [SELECT Id, CaseNumber, RecordType.Name, Status, Owner.Name, Account.Party_ID__c, CreatedDate FROM Case WHERE Id =: objCase.Id];

            // Fetch the Open and pending Non-CRE Tasks
            List<Task> lstTasks = [SELECT Id, Subject, Status, CreatedDate, ActivityDate, Description FROM Task WHERE WhatId =: objCase.Id AND Assigned_User__c != 'CRE' AND Status NOT IN : setTaskStatus];

            if (lstTasks != null && !lstTasks.isEmpty()) {

                List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
                
                try {
                    // Iterate over the fetched tasks and perform callout to IPMS and cancel the tasks
                    for (Task objTask : lstTasks) {
                        
                        List<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5> lstObjBeans =
                            new List<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5>();

                        // Creation of Header Bean
                        TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objHeaderBean =
                            new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
                        objHeaderBean = IPMSTaskUtility.setCommonBeanFields(objHeaderBean);
                        objHeaderBean = IPMSTaskUtility.setHeaderBean(objHeaderBean, objCase);
                        objHeaderBean.ATTRIBUTE3 = 'Cancelled';
                        lstObjBeans.add(objHeaderBean);

                        // Creation of Task Bean
                        TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objTaskBean =
                            new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
                        objTaskBean = IPMSTaskUtility.setCommonBeanFields(objTaskBean);
                        objTaskBean = IPMSTaskUtility.setTaskBean(objTaskBean, objCase, objTask);
                        objTaskBean.ATTRIBUTE3 = 'Cancelled';
                        lstObjBeans.add(objTaskBean);

                        // Perform callout to IPMS
                        TaskCreationWSDL.TaskHttpSoap11Endpoint objClass = new TaskCreationWSDL.TaskHttpSoap11Endpoint();
                        objClass.timeout_x = 120000;
                        String strResponse = objClass.SRDataToIPMSMultiple('2-' + String.valueOf(System.currentTimeMillis()),
                            'CREATE_SR', 'SFDC', lstObjBeans);

                        IPMSTaskUtility.TaskCreationResponse objResponse = (IPMSTaskUtility.TaskCreationResponse)JSON.deserialize(strResponse, IPMSTaskUtility.TaskCreationResponse.class);

                        // Check if IPMS callout is successful then set the Task status to Cancelled else create Error Log record
                        if (objResponse != null && String.isNotBlank(objResponse.status) && objResponse.status.equalsIgnoreCase('S')) {
                            objTask.Status = 'Cancelled';
                            lstTasksToUpdate.add(objTask);
                        } else {
                            objTask.Task_Error_Details__c = objResponse != null && String.isNotBlank(objResponse.message) ?
                                (objResponse.message.length() > 255 ? objResponse.message.substring(0, 254) : objResponse.message) : '';
                            if(objResponse != null && String.isNotBlank(objResponse.message)) {
                                lstErrorLog.add(new Error_Log__c(Case__c = objCase.Id, Error_Details__c = objResponse.message));
                            }
                            ApexPages.addmessage(new ApexPages.message(
                                ApexPages.severity.Error, 'Some problem has been encountered. Please contact System Administrator for further details.'));
                            return null;
                        }
                    }
                    if (lstErrorLog.isEmpty() && lstTasksToUpdate != null && !lstTasksToUpdate.isEmpty()) {
                        update lstTasksToUpdate;
                        objCase.Status = 'Cancelled';
                        update objCase;
                    }
                    if(!lstErrorLog.isEmpty()) {
                        insert lstErrorLog;
                    } 
                } catch (Exception objException) {
                    Error_Log__c objErrorLog = new Error_Log__c(Case__c = objCase.Id, Error_Details__c = objException.getMessage());
                    insert objErrorLog;
                    ApexPages.addmessage(new ApexPages.message(
                        ApexPages.severity.Error,'Some problem has been encountered. Please contact System Administrator for further details.'));
                    return null;
                }
            } else {
                // As there are no open Tasks, Case status set to Cancelled
                objCase.Status = 'Cancelled';
                update objCase;
            }
        }
       
        // Redirect to Case detail page
        PageReference  caseDetailPage = new PageReference ('/'+objCase.Id);
        return caseDetailPage;
    }
}