@RestResource(urlMapping='/sendOTP/*')
global class SendOTPVerificationCodes_API {

    public static string OTP_SENT = 'OTP sent successfully';

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        //200 => 'OK',
        //400 => 'Bad Request',
        //401 => 'Unauthorized',
        //500 => 'Internal Server Error' 
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

     @HttpPost 
     global static FinalReturnWrapper SendOTP() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);
        
        //FinalReturnWrapper objFinalWrap = new FinalReturnWrapper();
        cls_meta_data objMeta = new cls_meta_data();
        String guidCreated;
        //List<BUWrapper> lstBUWrapper = new List<BUWrapper>();
        List<FM_Case__C> lstFMCase = new List<FM_Case__C>();

        if(r.params.containsKey('fmCaseId') && String.isNotBlank(r.params.get('fmCaseId')) ) {
           
            try {
                lstFMCase = [SELECT  Id
                                   , First_Name__c
                                   , Last_Name__c 
                                   , Mobile_Country_Code__c
                                   , Mobile_no__c
                                   , Tenant_Email__c
                                   , Booking_Unit__c
                                   , Tenant__c
                            FROM FM_Case__c
                            WHERE id = :r.params.get('fmCaseId')];

                System.debug('lstFMCase: ' + lstFMCase);
            }
            catch(System.QueryException e){
                System.debug('exception :' + e.getMessage());
                //objMeta.is_success = false;
                objMeta.message = e.getMessage();
                objMeta.status_code = 6;

            }
        
            if(lstFMCase.size() > 0) {

                String emailCode    = String.valueOf((Math.random() * 10000).intValue()).leftPad(4, '0');
                String smsCode      = String.valueOf((Math.random() * 10000).intValue()).leftPad(4, '0');

                String isOTPsent = SendOTPviaEmailandSMS(lstFMCase[0], emailCode, smsCode);

                System.debug('isOTPsent: ' + isOTPsent);

                if(isOTPsent == OTP_SENT) {
                    guidCreated = InsertOTPwithGUID(lstFMCase[0].id, emailCode, smsCode);
                }
                //objMeta.is_success = true;
                objMeta.message = isOTPsent;
                objMeta.status_code = isOTPsent == OTP_SENT ? 1 : 6;
            }
            else {
                //objMeta.is_success = true;
                objMeta.message = 'No FM Case found for given fmCaseId';
                objMeta.status_code = 3;
            }
            
        }
        else {
            if( !r.params.containsKey('fmCaseId') ) {
                //objMeta.is_success = false;
                objMeta.message = 'Missing parameter: fmCaseId';
                objMeta.status_code = 3;
            }
            else if( String.isBlank(r.params.get('fmCaseId')) ) {
                //objMeta.is_success = false;
                objMeta.message = 'Missing parameter value : fmCaseId';
                objMeta.status_code = 3;
            }
        }

        objMeta.title = mapStatusCode.get(objMeta.status_code);

        cls_data objData = new cls_data();
        objData.GUID = guidCreated;

        FinalReturnWrapper objReturn = new FinalReturnWrapper();
        objReturn.data = objData;
        objReturn.meta_data = objMeta;
        
        System.debug('objReturn: ' + objReturn);
        
        return objReturn;

    }

    public static String SendOTPviaEmailandSMS(FM_Case__c objFMCase, String emailCode, String smsCode) {


        String responseMessage = OTP_SENT;
        //For email
        String tenantName = '';
        if(objFMCase.First_Name__c != NULL && objFMCase.Last_Name__c != NULL) {
            tenantName = objFMCase.First_Name__c + ' ' + objFMCase.Last_Name__c;
        }

        List<OrgWideEmailAddress> orgWideAddressHelloDamac = [
            SELECT  Id
                    , Address
                    , DisplayName
            FROM    OrgWideEmailAddress
            WHERE   Id = :Label.CommunityPortalOrgWideAddressId
        ];

        String emailSubject = 'Community Portal New Tenant Registration Verification Code';

        String emailBody = 'Hello ' + tenantName + ',';
        emailBody += '\n\n' + emailCode + ' is the code for verifying your email id.';
        emailBody += '\nPlease enter this code along with the code received through SMS (on your'
                        +' registered mobile number) on the My Community portal to Submit the'
                        +' Tenant Registration Request.';
        //emailBody += '\n\nEmail Code: 1234\n';
        //emailBody += 'SMS Code: 5678\n';
        //emailBody += 'Verification Code to be entered on Portal: 12345678\n';
        emailBody += '\n\nRegards,\nMy Community - LOAMS';

        SendGridEmailService.SendGridResponse response = SendGridEmailService.sendEmailService(
            objFMCase.Tenant_Email__c, tenantName, '', '', '', '', emailSubject, '',
            orgWideAddressHelloDamac[0].Address, orgWideAddressHelloDamac[0].DisplayName, '', '', 'text/plain',
            emailBody, '', new List<Attachment>()
        );

        String sendGridRes = Test.isRunningTest() ? 'Accepted' : response.ResponseStatus;

        if(sendGridRes == 'Accepted') {
            EmailMessage mail = new EmailMessage();
            mail.Subject = emailSubject;
            mail.MessageDate = System.Today();
            mail.Status = '3';
            mail.RelatedToId = objFMCase.Id;//Put FM case id
            mail.ToAddress = objFMCase.Tenant_Email__c;
            mail.FromAddress = orgWideAddressHelloDamac[0].Address;
            mail.TextBody = emailBody;
            mail.CcAddress = '';
            mail.BccAddress = '';
            mail.Sent_By_Sendgrid__c = true;
            mail.SentGrid_MessageId__c = response.messageId;
            mail.Booking_Unit__c = objFMCase.Booking_Unit__c;
            mail.Account__c = objFMCase.Tenant__c;
            insert mail;
        }
        else {
            responseMessage = 'TR Email OTP Verification request failed from SendGrid';

            Error_Log__c objError = new Error_Log__c();
            objError.Account__c = objFMCase.Booking_Unit__r.Booking__r.Account__c;
            objError.FM_Case__c = objFMCase.Id;
            objError.Error_Details__c = 'TR Email OTP Verification request failed from SendGrid';
            objError.Process_Name__c = 'Generic Email';
            insert objError;
        }
        //Email end

        //For SMS
        SMS_History__c verificationSms = new SMS_History__c();
        verificationSms.FM_Case__c = objFMCase.Id;
        verificationSms.Message__c = smsCode + ' is the code for verifying your mobile number. '
                + 'Please enter this code and the email verification code to submit the Tenant Registration Request '
                + 'on My Community Portal';
        verificationSms.Phone_Number__c =
                (objFMCase.Mobile_Country_Code__c == NULL
                    ? ''
                    : objFMCase.Mobile_Country_Code__c.substringAfterLast(':').trim())
                + objFMCase.Mobile_no__c;

        verificationSms.Name = 'Portal Tenant Registration Verification for '
                + [SELECT Id, Name FROM FM_Case__c WHERE Id = :objFMCase.Id].Name;
        if(Test.isRunningTest()) {

        }
        else {
            //System.debug('verificationSms :'+verificationSms);
            try {
                System.debug('verificationSms: ' + verificationSms);
                insert verificationSms;
            }
            catch(exception e) {
                responseMessage = e.getMessage();
            }
            
        }
        // SMS end

        System.debug('responseMessage:: ' + responseMessage);
        return responseMessage;

    }

    public static String InsertOTPwithGUID(String fmCaseId, String emailOtp, String smsOtp ) {

        //Logic to expire existing OTPs on the FM Case
        List<OTP__c> existingOTPs = [SELECT id
                                          , isExpired__c
                                     FROM OTP__c
                                     WHERE FM_Case__c =: fmCaseId
                                     AND FM_Process_Name__c = 'Tenant Registration'];
        System.debug('existingOTPs: ' + existingOTPs);
        
        for(OTP__c objOTP : existingOTPs) {
            objOTP.isExpired__c = true;
        }                             

        System.debug('existingOTPs after marking true: ' + existingOTPs);

        if(existingOTPs.size() > 0) {
            update existingOTPs;
        }

        //

        String guid = getGUID();

        OTP__c objOTP = new OTP__c();
        objOTP.GUID__c = guid;
        objOTP.FM_SMS_OTP__c = smsOtp;
        objOTP.FM_Email_OTP__c = emailOtp;
        objOTP.FM_Process_Name__c = 'Tenant Registration';
        objOTP.FM_Case__c = fmCaseId;
        //objOTP.Time_after_15_min = 


        System.debug('objOTP: ' + objOTP);
        insert objOTP;

        return guid;

    }

    public static String getGUID() {

        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        System.debug('guid generated:: ' + guid);

        return guid;
    }
   

     global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        public String GUID;
    }

    public class cls_meta_data {
        //public Boolean is_success {get; set;}
        public String message {get; set;}
        public Integer status_code {get; set;}
        public String title {get; set;}
        public String developer_message {get; set;}
    }

}