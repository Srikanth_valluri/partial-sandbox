@isTest
private class FmIpmsReceiptCreationBatchTest {
	private static final String GATEWAY_NAME = LoamsCommunityController.GATEWAY_NAME;
	private static final String GATEWAY_URL = 'https://www.payment-gateway.com';
	private static final String GATEWAY_MERCHANT_ID = 'merchant_id';
	private static final String GATEWAY_ACCESS_CODE = 'access_code';
	private static final String GATEWAY_ENCRYPTION_KEY = 'encryption_key';//'ac24310eb79685df';
	private static final String START_DATE_TIMESTAMP;

	static {
	    Date today = Date.today();
	    START_DATE_TIMESTAMP = today.year() + '-'
	            + ('' + today.month()).leftPad(2, '0')
	            + '-' + ('' + today.day()).leftPad(2, '0')
	            + 'T00:00:00Z';
	}

	@testSetup
	private static void setupTestData() {
	    PaymentGateway__c gateway = new PaymentGateway__c(
	        Name = GATEWAY_NAME,
	        Url__c = GATEWAY_URL,
	        MerchantId__c = GATEWAY_MERCHANT_ID,
	        AccessCode__c = GATEWAY_ACCESS_CODE,
	        EncryptionKey__c = GATEWAY_ENCRYPTION_KEY
	    );
	    insert gateway;
	}

	@isTest
	static void testBatch() {
	    FM_Receipt__c fmReceipt = new FM_Receipt__c();
	    fmReceipt.Payment_Type__c = 'FM Guest Payment';
	    fmReceipt.CustomerEmail__c = 'test@mail.com';
	    fmReceipt.Call_IPMS_Receipt_Creation__c = true;
	    insert fmReceipt;

	    CCAvenuePaymentGateway.OrderStatusResult orderStatusResult = new CCAvenuePaymentGateway.OrderStatusResult();
	    orderStatusResult.status = '0';
	    orderStatusResult.order_bank_response = 'Approved';

	    String mockReceiptSuccessJson = '{"responseId": "260645","responseTime": "Sun Oct 14 13:13:50 GST 2018","requestName": "CREATE_RECEIPT","extRequestName": "RECEIPT-12345","responseMessage": "CREATE_RECEIPT Completed, Generate the Receipt to View the Receipt File","actions": [{"action": "GENERATE_RECEIPT","method": "GET","url": "https://ptctest.damacgroup.com/DCOFFEE/report/RECEIPT/IPMS/NO_KEY/RECEIPT_ID/2137546"}],"complete": true}';

	    FmIpmsRestServices.Response receiptResponse = new FmIpmsRestServices.Response();
	    receiptResponse.complete = true;
	    receiptResponse.actions = new List<FmIpmsRestServices.Action>();
	    FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
	    action.url = 'https://test.damacgroup.com/test.pdf';
	    receiptResponse.actions.add(action);

	    Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
	        new Map<String, FmHttpCalloutMock.Response> {
	            Label.CCAvenueStatusAPIURL
	                =>
	                new FmHttpCalloutMock.Response(
	                    200, 'Success',
	                    CCAvenuePaymentGateway.STATUS + '=0&' + CCAvenuePaymentGateway.ENC_RESPONSE + '='
	                        + CCAvenuePaymentGateway.getInstance(GATEWAY_NAME).encrypt(
	                            JSON.serialize(orderStatusResult)
	                        )
	                )
	            ,
	            FmIpmsRestServices.CREATE_RECEIPT
	                =>
	                new FmHttpCalloutMock.Response(200, 'S', mockReceiptSuccessJson)
	            ,
	            'https://ptctest.damacgroup.com/DCOFFEE/report/RECEIPT/IPMS/NO_KEY/RECEIPT_ID/2137546'
	                =>
	                new FmHttpCalloutMock.Response(200, 'S', JSON.serialize(receiptResponse))
	            ,
	            'https://test.damacgroup.com/test.pdf'
	                =>
	                new FmHttpCalloutMock.Response(200, 'S', 'https://test.damacgroup.com/test.pdf')

	        }
	    ));

	    new FmIpmsReceiptCreationBatch();

	    Test.startTest();
	        Database.executeBatch(new FmIpmsReceiptCreationBatch(), 1);
	    Test.stopTest();
	}

	@isTest
	static void testInvalidBatchSize() {
	    insert new List<FM_Receipt__c> {
	        new FM_Receipt__c(
	            Payment_Type__c = 'FM Guest Payment',
	            CustomerEmail__c = 'test@mail.com',
	            Call_IPMS_Receipt_Creation__c = true
	        ),
	        new FM_Receipt__c(
	            Payment_Type__c = 'FM Guest Payment',
	            CustomerEmail__c = 'test@mail.com',
	            Call_IPMS_Receipt_Creation__c = true
	        )
	    };

	    Test.startTest();
	        Database.executeBatch(new FmIpmsReceiptCreationBatch(), 2);
	    Test.stopTest();
	}

	@isTest
	static void testBatchException() {
	    FM_Receipt__c fmReceipt = new FM_Receipt__c();
	    fmReceipt.Payment_Type__c = 'FM Guest Payment';
	    fmReceipt.CustomerEmail__c = 'test@mail.com';
	    fmReceipt.Call_IPMS_Receipt_Creation__c = true;
	    insert fmReceipt;

	    Test.startTest();
	        Database.executeBatch(new FmIpmsReceiptCreationBatch(), 1);
	    Test.stopTest();
	}

	@isTest
	static void testStatusError() {
	    FM_Receipt__c fmReceipt = new FM_Receipt__c();
	    fmReceipt.Payment_Type__c = 'FM Guest Payment';
	    fmReceipt.CustomerEmail__c = 'test@mail.com';
	    fmReceipt.Call_IPMS_Receipt_Creation__c = true;
	    insert fmReceipt;

	    CCAvenuePaymentGateway.OrderStatusResult orderStatusResult = new CCAvenuePaymentGateway.OrderStatusResult();
	    orderStatusResult.status = '1';
	    orderStatusResult.order_bank_response = 'Approved';

	    Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
	        new Map<String, FmHttpCalloutMock.Response> {
	            Label.CCAvenueStatusAPIURL
	                =>
	                new FmHttpCalloutMock.Response(
	                    200, 'Success',
	                    CCAvenuePaymentGateway.STATUS + '=0&' + CCAvenuePaymentGateway.ENC_RESPONSE + '='
	                        + CCAvenuePaymentGateway.getInstance(GATEWAY_NAME).encrypt(
	                            JSON.serialize(orderStatusResult)
	                        )
	                )
	        }
	    ));
	    Test.startTest();
	       Database.executeBatch(new FmIpmsReceiptCreationBatch(), 1);
	    Test.stopTest();
	}

	@isTest
	static void testDeserializeError() {
	    FM_Receipt__c fmReceipt = new FM_Receipt__c();
	    fmReceipt.Payment_Type__c = 'FM Guest Payment';
	    fmReceipt.CustomerEmail__c = 'test@mail.com';
	    fmReceipt.Call_IPMS_Receipt_Creation__c = true;
	    insert fmReceipt;

	    CCAvenuePaymentGateway.OrderStatusResult orderStatusResult = new CCAvenuePaymentGateway.OrderStatusResult();
	    orderStatusResult.status = '0';
	    orderStatusResult.order_bank_response = 'Approved';

	    Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
	        new Map<String, FmHttpCalloutMock.Response> {
	            Label.CCAvenueStatusAPIURL
	                =>
	                new FmHttpCalloutMock.Response(
	                    200, 'Success',
	                    CCAvenuePaymentGateway.STATUS + '=0&' + CCAvenuePaymentGateway.ENC_RESPONSE + '='
	                        + CCAvenuePaymentGateway.getInstance(GATEWAY_NAME).encrypt(
	                            'test'
	                        )
	                )
	        }
	    ));

	    Test.startTest();
	        Database.executeBatch(new FmIpmsReceiptCreationBatch(), 1);
	    Test.stopTest();
	}

	/*@isTest
	static void testEmailReceiptError() {
	    FM_Receipt__c fmReceipt = new FM_Receipt__c();
	    fmReceipt.Payment_Type__c = 'FM Guest Payment';
	    fmReceipt.CustomerEmail__c = 'test@mail.com';
	    fmReceipt.Call_IPMS_Receipt_Creation__c = true;
	    insert fmReceipt;

	    CCAvenuePaymentGateway.OrderStatusResult orderStatusResult = new CCAvenuePaymentGateway.OrderStatusResult();
	    orderStatusResult.status = '0';
	    orderStatusResult.order_bank_response = 'Approved';

	    String mockReceiptSuccessJson = '{"responseId": "260645","responseTime": "Sun Oct 14 13:13:50 GST 2018","requestName": "CREATE_RECEIPT","extRequestName": "RECEIPT-12345","responseMessage": "CREATE_RECEIPT Completed, Generate the Receipt to View the Receipt File","actions": [{"action": "GENERATE_RECEIPT","method": "GET","url": "https://ptctest.damacgroup.com/DCOFFEE/report/RECEIPT/IPMS/NO_KEY/RECEIPT_ID/2137546"}],"complete": true}';

	    FmIpmsRestServices.Response receiptResponse = new FmIpmsRestServices.Response();
	    receiptResponse.complete = true;
	    receiptResponse.actions = new List<FmIpmsRestServices.Action>();
	    FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
	    action.url = '';
	    receiptResponse.actions.add(action);

	    Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
	        new Map<String, FmHttpCalloutMock.Response> {
	            Label.CCAvenueStatusAPIURL
	                =>
	                new FmHttpCalloutMock.Response(
	                    200, 'Success',
	                    CCAvenuePaymentGateway.STATUS + '=0&' + CCAvenuePaymentGateway.ENC_RESPONSE + '='
	                        + CCAvenuePaymentGateway.getInstance(GATEWAY_NAME).encrypt(
	                            JSON.serialize(orderStatusResult)
	                        )
	                )
	            ,
	            FmIpmsRestServices.CREATE_RECEIPT
	                =>
	                new FmHttpCalloutMock.Response(200, 'S', mockReceiptSuccessJson)
	            ,
	            'https://ptctest.damacgroup.com/DCOFFEE/report/RECEIPT/IPMS/NO_KEY/RECEIPT_ID/2137546'
	                =>
	                new FmHttpCalloutMock.Response(200, 'S', JSON.serialize(receiptResponse))
	        }
	    ));

	    Test.startTest();
	        System.schedule(
	            'FmPaymentStatusCheckJob', '0 0 * * * ?', new FmIpmsReceiptCreationBatch()
	        );
	    Test.stopTest();
	}*/

}