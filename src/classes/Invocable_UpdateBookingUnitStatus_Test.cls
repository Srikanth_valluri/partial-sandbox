/**************************************************************************************************
* Name               : Invocable_UpdateBookingUnitStatus_Test
* Description        : Test Class for Invocable_UpdateBookingUnitStatus Class
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0                         16/06/2019      Created
**************************************************************************************************/
@isTest
public class Invocable_UpdateBookingUnitStatus_Test {
    @testSetup 
    static void setupData() {
        //IPMS_Code_Settings__c custom settings
        List<IPMS_Code_Settings__c> IPMSCodeList = new List<IPMS_Code_Settings__c>();
        IPMS_Code_Settings__c code1 = new IPMS_Code_Settings__c(Name = 'Awaiting SPA Execution', Step_Type__c = 'SPA Execution', 
                                                                Step_Status__c = 'Awaiting SPA Execution', Status_Code__c = 'LX', 
                                                                Check_on_DP_OK__c = False, Check_on_Doc_OK__c = False, 
                                                                Doc_OK__c = true, DP_OK__c = true);
        IPMSCodeList.add(code1);
        insert IPMSCodeList;

        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;

        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.Id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;

        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.Id;
        nstp1.Step_No__c = 50;
        nstp1.Step_Status__c = 'Awaiting SPA Execution';
        nstp1.Step_Type__c = 'SPA Execution';
        insert nstp1;

        Booking__c booking = new Booking__c(Account__c = acc.Id, Deal_SR__c = sr.Id);
        insert booking;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id, Unit_Name__c = 'Test name 1',
                                                          Registration_ID__c = '1600', Registration_Status__c = 'Active Status',
                                                          Unit_Selling_Price_AED__c = 100);
        insert bookingUnit;
    }

    @isTest
    static void test_method1() {
        New_Step__c stp = [SELECT Id FROM New_Step__c LIMIT 1];
        Invocable_UpdateBookingUnitStatus.UpdateBookingUnitStatus(new List<Id>{stp.Id});
    }
}