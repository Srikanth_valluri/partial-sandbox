/******************************************************************************
* Description - Test class developed for HandoverSnagRService
*
* Version            Date            Author                    Description
* 1.0                19/12/17        Naresh Kaneriya (Accely)   Initial Draft
********************************************************************************/
@isTest
public class HandoverSnagRServiceTest{


@isTest static void getTest(){

HandoverSnagRService.SnagRResponseWrapper  objSnagRResponseWrapper = new  HandoverSnagRService.SnagRResponseWrapper();

        objSnagRResponseWrapper.ID = 'Test';
        objSnagRResponseWrapper.GUID = 'Test';
        objSnagRResponseWrapper.DrawingID = 'Test';
        objSnagRResponseWrapper.DrawingTitle = 'Test';
        objSnagRResponseWrapper.Status = 'Test';
        objSnagRResponseWrapper.CurrentStatus = 'Test';
        objSnagRResponseWrapper.UserDescription = 'Test';
        objSnagRResponseWrapper.LocationID = 'Test';
        objSnagRResponseWrapper.Location = 'Test';
        objSnagRResponseWrapper.LocationExtRef = 'Test';
        objSnagRResponseWrapper.LocationGUID = 'Test';
        objSnagRResponseWrapper.XCoord = 'Test';
        objSnagRResponseWrapper.YCoord = 'Test';
        objSnagRResponseWrapper.PriorityID = 'Test';
        objSnagRResponseWrapper.Priority = 'Test';
        objSnagRResponseWrapper.DueBy = 'Test';
        objSnagRResponseWrapper.DaysLeft = 'Test';
        objSnagRResponseWrapper.GroupID = 'Test';
        objSnagRResponseWrapper.GroupName = 'Test';
        objSnagRResponseWrapper.GroupInitials = 'Test';
        objSnagRResponseWrapper.DateClosed = 'Test';
        objSnagRResponseWrapper.BigPhoto = 'Test';
        objSnagRResponseWrapper.Lattitude = 'Test';
        objSnagRResponseWrapper.Longitude = 'Test';
        objSnagRResponseWrapper.CreatedByID = 'Test';
        objSnagRResponseWrapper.CreatedBy = 'Test';
        objSnagRResponseWrapper.CreatedDate = 'Test';
        objSnagRResponseWrapper.SDCategoryID = 'Test';
        objSnagRResponseWrapper.SDCategory = 'Test';
        objSnagRResponseWrapper.SDCode = 'Test';
        objSnagRResponseWrapper.SDType = 'Test';
        objSnagRResponseWrapper.SDID = 'Test';
        objSnagRResponseWrapper.SDDescription = 'Test';
        objSnagRResponseWrapper.DateSync = 'Test';
        objSnagRResponseWrapper.MPGUID = 'Test';
        objSnagRResponseWrapper.FCGUID = 'Test';
        objSnagRResponseWrapper.FutureDefectGUID = 'Test';
        objSnagRResponseWrapper.PreviousDefectGUID = 'Test';
        objSnagRResponseWrapper.ProjectTitle = 'Test';
        objSnagRResponseWrapper.ContractNo = 'Test';
        objSnagRResponseWrapper.Role = 'Test';
        objSnagRResponseWrapper.UGUserID = 'Test';
        objSnagRResponseWrapper.HistoryCount = 'Test';
        objSnagRResponseWrapper.LastChanged = 'Test';
        objSnagRResponseWrapper.PhotoExists = 'Test';
        
         Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,SnagRWSDL.defectsResponse_element>();
        SnagRWSDL.defectsResponse_element response1 = new SnagRWSDL.defectsResponse_element();
        response1.return_x =''+
    '   [  '+
    '      {  '+
    '         "BigPhoto":"null",'+
    '         "ContractNo":"null",'+
    '         "CreatedBy":"null",'+
    '         "CreatedByID":"82",'+
    '         "CreatedDate":"2017-12-14T05:45:00Z",'+
    '         "CurrentStatus":"Open",'+
    '         "DateClosed":"null",'+
    '         "DateSync":"2017-12-14T05:47:00Z",'+
    '         "DaysLeft":"3",'+
    '         "DrawingID":"44",'+
    '         "DrawingTitle":"GPNA22 6",'+
    '         "DueBy":"2017-12-22T07:16:00Z",'+
    '         "FCGUID":"null",'+
    '         "FutureDefectGUID":"null",'+
    '         "GUID":"BC89D352-2A50-4F77-B08A-BB1C1AC6CF16",'+
    '         "GroupID":"1",'+
    '         "GroupInitials":"DPT",'+
    '         "GroupName":"DAMAC Projects Team",'+
    '         "HistoryCount":"0",'+
    '         "ID":"1091",'+
    '         "LastChanged":"2017-12-14T05:45:00Z",'+
    '         "Lattitude":"25.0310096740723",'+
    '         "Location":"GPNA22 6 Balcony",'+
    '         "LocationExtRef":"null",'+
    '         "LocationGUID":"2a08d1d0-f2f9-40b1-96dc-3114c87fa947",'+
    '         "LocationID":"189",'+
    '         "Longitude":"55.2578277587891",'+
    '         "MPGUID":"null",'+
    '         "PhotoExists":"true",'+
    '         "PreviousDefectGUID":"null",'+
    '         "Priority":"Medium",'+
    '         "PriorityID":"2",'+
    '         "ProjectTitle":"DAMAC Training Project",'+
    '         "Role":"1",'+
    '         "SDCategory":"Aluminum",'+
    '         "SDCategoryID":"1",'+
    '         "SDCode":"STD002",'+
    '         "SDDescription":"Glass Partitions/Doors - Fixing & Joint finishing of Aluminum Framing with wall and floor",'+
    '         "SDID":"9",'+
    '         "SDType":"Walls",'+
    '         "Status":"Open",'+
    '         "UGUserID":"53",'+
    '         "UserDescription":"Scratches on the floor ,,need to be fixed totally",'+
    '         "XCoord":922,'+
    '         "YCoord":"1852"'+
    '      }'+
    '   ]'+
    '   '+
    '';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock()); 
       new HandoverSnagRService().getDefects('BU-070892'); 
        Test.stopTest();  
   }
}