@isTest
public class Invocable_ConvertInquiry_Test {
    
    static testMethod void myUnitTest() {
        /*
List<NSIBPM__SR_Status__c> createSrStatusList = InitialiseTestData.createSrStatusRecords(
new List<NSIBPM__SR_Status__c>{
new NSIBPM__SR_Status__c(Name = 'Draft', NSIBPM__Code__c = 'Draft'), 
new NSIBPM__SR_Status__c(Name = 'Submitted', NSIBPM__Code__c = 'Submitted')});
*/
        List<Inquiry_Conversion_Mapping__c> mappingList = new List<Inquiry_Conversion_Mapping__c>();
        Inquiry_Conversion_Mapping__c mapping = new Inquiry_Conversion_Mapping__c();
        mapping.Name = 'First_Name__c';
        mapping.Person_Account_Field_Name__c = 'FirstName';
        mapping.Business_Account_Field_Name__c = null;
        mapping.Business_contact_Field_Name__c = 'FirstName';
        mappingList.add(mapping);
        
        Inquiry_Conversion_Mapping__c mapping0 = new Inquiry_Conversion_Mapping__c();
        mapping0.Name = 'Last_Name__c';
        mapping0.Person_Account_Field_Name__c = 'LastName';
        mapping0.Business_Account_Field_Name__c = null;
        mapping0.Business_contact_Field_Name__c = 'LastName';
        mappingList.add(mapping0);
        
        
        Inquiry_Conversion_Mapping__c mapping1 = new Inquiry_Conversion_Mapping__c();
        mapping1.Name = 'Organisation_Name__c';
        mapping1.Person_Account_Field_Name__c = null;
        mapping1.Business_Account_Field_Name__c = 'Name';
        mapping1.Business_contact_Field_Name__c = null;
        mappingList.add(mapping1);
        
        Inquiry_Conversion_Mapping__c mapping2 = new Inquiry_Conversion_Mapping__c();
        mapping2.Name = 'ownerid';
        mapping2.Person_Account_Field_Name__c = 'OwnerId';
        mapping2.Business_Account_Field_Name__c = 'OwnerId';
        mapping2.Business_contact_Field_Name__c = 'OwnerId';
        mappingList.add(mapping2);
        
        Inquiry_Conversion_Mapping__c mapping3 = new Inquiry_Conversion_Mapping__c();
        mapping3.Name = 'Party_ID__c';
        mapping3.Person_Account_Field_Name__c = 'Party_ID__c';
        mapping3.Business_contact_Field_Name__c = 'Party_ID__c';
        mappingList.add(mapping3);
        INSERT mappingList;
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        INSERT acc;
        
        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.id;
        sr.Agency_Type__c = 'Corporate';
        INSERT sr;
        
        List<Booking__c> createBookingList = InitialiseTestData.createBookingRecords(new List<Booking__c>{new Booking__c(Deal_SR__c = sr.Id)});
        
        Location__c loc = new Location__c();
        loc.Location_ID__c = '123';
        insert loc;
        Inventory__c inv = new Inventory__c();
        inv.Unit_Location__c = loc.id;
        INSERT inv;
        
        List<Booking_Unit__c> createBookingUnitList = InitialiseTestData.createBookingUnitRecords(new List<Booking_Unit__c>{new Booking_Unit__c(Booking__c = createBookingList[0].Id, Inventory__c = inv.Id)});
        
        Inquiry__c inq = InitialiseTestData.getInquiryDetails('Inquiry',1234);
        insert inq;
        buyer__c b1 = new buyer__c();
        b1.Buyer_Type__c =  'Individual';
        b1.Address_Line_1__c =  'Ad1';
        b1.Country__c =  'United Arab Emirates';
        b1.City__c = 'Dubai' ;
        b1.Date_of_Birth__c = string.valueof(system.today().addyears(-30)) ;
        b1.Email__c = 'test@test.com';
        b1.First_Name__c = 'firstname' ;
        b1.Last_Name__c =  'lastname';
        b1.Nationality__c = 'Indian' ;
        b1.Passport_Expiry_Date__c = string.valueof(system.today().addyears(20)) ;
        b1.Passport_Number__c = 'J0565556' ;
        b1.Phone__c = '569098767' ;
        b1.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b1.Place_of_Issue__c =  'India';
        b1.Title__c = 'Mr';
        b1.booking__c = createBookingList[0].Id;
        b1.Inquiry__c = inq.id;
        INSERT b1;
        
        /*        List<NSIBPM__Status__c> createStatus = new List<NSIBPM__Status__c>();
createStatus = InitialiseTestData.createStatusRecords(
new List<NSIBPM__Status__c>{
new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_ADDITIONAL_INFO', Name = 'AWAITING_PC_CONFIRMATION'),
new NSIBPM__Status__c(NSIBPM__Code__c = 'MORE_INFO_UPDATED', Name = 'Approved',NSIBPM__Type__c='End'),
new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_FFA_AA', Name = 'Rejected',NSIBPM__Type__c='End'),
new NSIBPM__Status__c(NSIBPM__Code__c = 'FFA_AA_UPLOADED', Name = 'FFA_AA_UPLOADED')});

NSIBPM__SR_Steps__c srStep= new NSIBPM__SR_Steps__c();
insert srStep;

List<NSIBPM__Step__c> createStepList = InitialiseTestData.createTestStepRecords(
new List<NSIBPM__Step__c>{
new NSIBPM__Step__c(NSIBPM__SR__c = createServiceRequestList[0].id, NSIBPM__Status__c = createStatus[2].Id, NSIBPM__SR_Step__c = srStep.id),
new NSIBPM__Step__c(NSIBPM__SR__c = createServiceRequestList[0].id, NSIBPM__Status__c = createStatus[2].Id, NSIBPM__SR_Step__c = srStep.id)});
*/
        
        List<New_Step__c> newStepsList = new List<New_Step__c>();
        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.id;
        nstp1.Step_No__c = 2;
        nstp1.Step_Status__c = 'Awaiting Token Deposit';
        nstp1.Step_Type__c = 'Token Payment';
        newStepsList.add(nstp1);
        insert newStepsList;
        
        Invocable_ConvertInquiry.ConvertInquiry(new List<Id> {nstp1.Id});
        Invocable_ConvertInquiry.FutureConvertInquiry(sr.id);
    }
    
}