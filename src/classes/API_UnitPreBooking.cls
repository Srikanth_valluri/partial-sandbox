/************************************************************************************************
 * @Name              : API_UnitPreBooking
 * @Test Class Name   : API_UnitPreBooking_Test
 * @Description       : RestReource Class for creating a Draft Booking  
 * 
 *    POST Method
 *    Pass in the body the list of the inventory IDs
 *    API calls DAMAC_Unit_PreBooking class to submit the draft booking 
 * 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         MZaklama       01/04/2020       Created
***********************************************************************************************/
@RestResource(urlMapping='/api_UnitPreBooking')
global class API_UnitPreBooking {

    @HttpPost
    global static void doPost(UnitPreBookingRequestDTO requestDTO) {
        String selectedInvIds = String.join(requestDTO.selectedInventoryIds, ',');
        
        UnitPreBookingResponseDTO responseDTO = new UnitPreBookingResponseDTO();
        responseDTO.srID = AP_UnitSearchController.checkSelectedUnits(selectedInvIds, requestDTO.eoiID);
        
        if(String.isBlank(responseDTO.srID)) {
            responseDTO.srID = DAMAC_Unit_PreBooking.SendPreBookId(selectedInvIds, requestDTO.eoiID, requestDTO.isUKBooking, '');
        }
        List <selectedUnits> units = new List <selectedUnits> ();
        if (responseDTO.srID != NULL) {
            System.debug (responseDTO.srID);
            for (Selected_Units__c sUnit : [SELECT ID, (SELECT templateIdPN__c, Promotion_Option__c FROM Available_Options__r)
                                            FROM Selected_Units__c 
                                            WHERE Service_Request__c =: responseDTO.srID 
                                            AND Service_Request__c != NULL]) 
            {
                selectedUnits unit = new selectedUnits ();
                unit.selectedUnitId = sUnit.Id;
                List <availableOptions> options = new List <availableOptions> ();
                for (Available_Options__c option :sUnit.Available_Options__r) {
                    availableOptions obj = new availableOptions ();
                    obj.id = option.Id;
                    obj.promotionId = option.templateIdPN__c;
                    obj.promotionName = option.Promotion_Option__c;
                    options.add (obj);
                }
                unit.promotions = options;
                units.add (unit);
            }
            responseDTO.units = units;
            
            responseDTO.srNumber = [SELECT Name FROM NSIBPM__Service_Request__c WHERE Id = :responseDTO.srID].Name;
            
            NSIBPM__Service_Request__c srRecord = new NSIBPM__Service_Request__c  ();
            srRecord.Created_by_Agents_App__c = true;
            srRecord.Created_By_AgentPortal__c = false;
            srRecord.Id = responseDTO.srID;
            TriggerFactoryCls.BYPASS_UPDATE_TRIGGER = true;
            update srRecord;
        }
        
        
        
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(responseDTO));
    }
    
    global class UnitPreBookingRequestDTO {
        public List<String> selectedInventoryIds {get; set;}
        public String eoiID {get; set;}
        public Boolean isUkBooking {get; set;}
    }
    
    global class UnitPreBookingResponseDTO {
        public String srID {get; set;}
        public String srNumber {get; set;}
        public List <selectedUnits> units { get; set; }
    }
    global class selectedUnits {
        public String selectedUnitId { get; set; }
        public List <availableOptions> promotions { get; set; }
    }
    global class availableOptions {
        public String id { get; set; }
        public String promotionId { get; set; }
        public String promotionName { get; set; }
    }
}