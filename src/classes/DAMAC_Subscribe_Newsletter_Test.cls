/*****************************************************************************************************************
* Name              : DAMAC_Subscribe_Newsletter_Test
* Description       : Test Class for DAMAC_Subscribe_Newsletter  
* -------------------------------------------------------------------------------------------------------------
* VERSION       AUTHOR          DATE            COMMENTS
* 1.0            QBurst      09/03/2020          Created
*****************************************************************************************************************/

@isTest
public class DAMAC_Subscribe_Newsletter_Test {

  @isTest
    static void testSuccess(){
        Campaign__c thisCampaign = InitialiseTestData.createCampaign();
        insert thisCampaign;
        String myJSON = '{"email": "testemailfortestclass987@test.com", "r1_title": "Newsletter Subscription",'
                        + ' "Opted Out Date": "2008-02-01 01:29:00",'
                        + ' "campaign_id" : "' + thisCampaign.Id + '"}';
        Email_Request__c req = new Email_Request__c ();
        insert req;
        List<Website_Subscriber_Field_Mappings__c> mappingList 
                        = new List<Website_Subscriber_Field_Mappings__c>();        
        Website_Subscriber_Field_Mappings__c mapping1 = new Website_Subscriber_Field_Mappings__c();
        mapping1.Name = 'r1_title';
        mapping1.Newsletter_Subscription__c = true;
        mapping1.Field_API__c = 'R1_Title__c';
        mapping1.Field_Type__c = 'Text';
        mappingList.add(mapping1);
        Website_Subscriber_Field_Mappings__c mapping2 = new Website_Subscriber_Field_Mappings__c();
        mapping2.Name = 'campaign_id';
        mapping2.Newsletter_Subscription__c = true;
        mapping2.Field_API__c = 'Campaign_Id__c';
        mapping2.Field_Type__c = 'Text';
        mapping2.Lookup_Field_API__c = 'Marketing_Campaign__c';
        mapping2.Lookup_Object_API__c = 'Campaign__c';
        mappingList.add(mapping2);
        Website_Subscriber_Field_Mappings__c mapping3 = new Website_Subscriber_Field_Mappings__c();
        mapping3.Name = 'Opted Out Date';
        mapping3.Newsletter_Subscription__c = true;
        mapping3.Field_Type__c = 'DateTime';
        mapping3.Field_API__c = 'Opted_Out_Date__c';
        mappingList.add(mapping3);
        insert mappingList;

        Email_Request_Mappings__c emailReq = new Email_Request_Mappings__c();
        emailReq.Name = 'Newsletter Subscription';
        emailReq.Email_Request_Id__c = req.Id;
        insert emailReq;
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/subscribenewsletter';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json'); 
        request.requestBody = Blob.valueof(myJSON);
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Subscribe_Newsletter.createWebsiteSubscriber();
        Test.stopTest();
    }
    
    @isTest
    static void testEmailMissing(){
        String myJSON = '{"wrongparam": "testemailfortestclass@test.com"}';
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/subscribenewsletter';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json'); 
        request.requestBody = Blob.valueof(myJSON);
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Subscribe_Newsletter.createWebsiteSubscriber();
        Test.stopTest();
    }
}