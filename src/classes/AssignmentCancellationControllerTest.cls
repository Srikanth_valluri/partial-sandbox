/****************************************************************************************
* Description - Test class developed for AssignmentCancellationController
*
* Version            Date            Author                              Description
* 1.0                22/11/17        Naresh Kaneriya (Accely)           Initial Draft
****************************************************************************************/
@isTest(SeeAllData=false)
private class AssignmentCancellationControllerTest{
    
    
    public static testmethod void Test_checkEligibility(){ 
         test.StartTest();
         
         Id RecordTypeIdCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
         Account AccData=TestDataFactory_CRM.createPersonAccount();
         Insert AccData;
         
         NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
         insert objSR ;
         
         List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( AccData.Id, objSR.Id, 1);
         insert lstBookings ;
         
         List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        //lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].NOC_Issued_Date__c = date.today();
        insert lstBookingUnits;
         
         list<Buyer__c> lstBuy = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 1, AccData.Id);
         insert lstBuy;
        
         list<Buyer__c> lstB = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 1, AccData.Id);
         Case CaseData=TestDataFactory_CRM.createCase(AccData.Id,RecordTypeIdCase);
         CaseData.Booking_Unit__c = lstBookingUnits[0].Id;
         CaseData.Buyer__c = lstBuy[0].Id;
         insert CaseData;
         
         Task objTask = new Task();
            objTask.OwnerId = UserInfo.getUserID();
            objTask.WhatId = CaseData.Id;
            objTask.Subject = 'Dummy';
            objTask.ActivityDate = date.today().addDays(1);
            objTask.Assigned_User__c = 'CRE Manager';
            objTask.Process_Name__c = 'Assignment';
            objTask.Priority = 'High';
            objTask.Status = 'Not Started';
            insert objTask;
         
         Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
         ApexPages.StandardController sc = new ApexPages.StandardController(CaseData);
         AssignmentCancellationController instance1 = new AssignmentCancellationController(sc);
         instance1.checkEligibility();
         
         /*
         CaseData.Status = 'New Buyer Approval';
         update CaseData ;

         
         Task tsk = new Task();
         tsk.WhatId = CaseData.id;
         tsk.OwnerId = UserInfo.getUserId();
         tsk.ActivityDate = System.today()+2;
         tsk.status = 'Not Started';
         tsk.Subject = 'Subject';
         insert tsk; 
         
         instance1.checkEligibility();
         */
         test.StopTest();  
    }
       
    public static testMethod void errors(){
        test.StartTest();
         
         Id RecordTypeIdCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
         Account AccData=TestDataFactory_CRM.createPersonAccount();
         Insert AccData;
         
         NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
         insert objSR ;
         
         List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( AccData.Id, objSR.Id, 1);
         insert lstBookings ;
         
         List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        //lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].NOC_Issued_Date__c = date.today();
        insert lstBookingUnits;
         
         list<Buyer__c> lstBuy = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 1, AccData.Id);
         insert lstBuy;
        
         list<Buyer__c> lstB = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 1, AccData.Id);
         Case CaseData=TestDataFactory_CRM.createCase(AccData.Id,RecordTypeIdCase);
         CaseData.Booking_Unit__c = lstBookingUnits[0].Id;
         CaseData.Buyer__c = lstBuy[0].Id;
         insert CaseData;
         
         Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        list<Error_Log__c> lstER = new list<Error_Log__c>();
         lstER.add(AssignmentCancellationController.createErrorLogRecord(AccData.Id,lstBookingUnits[0].Id,CaseData.Id));
         AssignmentCancellationController.insertErrorLog(lstER);
    }
    
    public static testmethod void test_checkEligibility_Negative(){ 
         test.StartTest();
         
         Id RecordTypeIdCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
         Account AccData=TestDataFactory_CRM.createPersonAccount();
         Insert AccData;
         
         NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
         insert objSR ;
         
         List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( AccData.Id, objSR.Id, 1);
         insert lstBookings ;
         
         List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        //lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].NOC_Issued_Date__c = date.today();
        insert lstBookingUnits;
         
         list<Buyer__c> lstBuy = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 1, AccData.Id);
         insert lstBuy;
        
         list<Buyer__c> lstB = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 1, AccData.Id);
         Case CaseData=TestDataFactory_CRM.createCase(AccData.Id,RecordTypeIdCase);
         CaseData.Booking_Unit__c = lstBookingUnits[0].Id;
         CaseData.Buyer__c = lstBuy[0].Id;
         CaseData.Status = 'Rejected';
         insert CaseData;
         
         Task objTask = new Task();
            objTask.OwnerId = UserInfo.getUserID();
            objTask.WhatId = CaseData.Id;
            objTask.Subject = 'Dummy';
            objTask.ActivityDate = date.today().addDays(1);
            objTask.Assigned_User__c = 'CRE Manager';
            objTask.Process_Name__c = 'Assignment';
            objTask.Priority = 'High';
            objTask.Status = 'Not Started';
            insert objTask;
         
         Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
         ApexPages.StandardController sc = new ApexPages.StandardController(CaseData);
         AssignmentCancellationController instance1 = new AssignmentCancellationController(sc);
         instance1.checkEligibility();

         test.StopTest();  
    }   
    
}