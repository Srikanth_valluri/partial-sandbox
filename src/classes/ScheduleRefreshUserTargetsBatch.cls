/****************************************************************************************************
* Name          : ScheduleRefreshUserTargetsBatch                                                            *
* Description   : Scheduler to invoke the RefreshUserTargetsBatch class                             *
* Created Date  : 27/03/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    12/04/2018      Initial Draft.                                          *
****************************************************************************************************/
global class ScheduleRefreshUserTargetsBatch implements Schedulable {
    global void execute(SchedulableContext SC) {
        RefreshUserTargetsBatch userTargetsBatch = new RefreshUserTargetsBatch();
       database.executebatch(userTargetsBatch);
   }
}