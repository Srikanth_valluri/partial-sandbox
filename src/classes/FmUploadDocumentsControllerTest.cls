@isTest
private class FmUploadDocumentsControllerTest {

    @isTest
    static void testQueryBuildingNoFmUser() {
        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;


        Test.startTest();
            FmUploadDocumentsController.queryBuildings(NULL);
            //System.assert(FmUploadDocumentsController.queryBuildings(NULL).isEmpty());
        Test.stopTest();
    }

    @isTest
    static void testqueryBuildings() {
        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC',
            Location_Type__c = 'Building',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        insert new FM_User__c(Building__c = location.Id, FM_User__c = UserInfo.getUserId());

        Test.startTest();
            System.assert(!FmUploadDocumentsController.queryBuildings(NULL).isEmpty());
        Test.stopTest();
    }

    @isTest
    static void testqueryBuildingsDiffUser() {

        UserRole objRole = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert objRole;

        User objUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = objRole.Id
        );
        insert objUser;

        System.runAs(objUser){
             Location__c location = new Location__c(
                Name = 'Test Location',
                Location_ID__c = 'LOC',
                Location_Type__c = 'Building',
                GeoLocation__latitude__s = 0,
                GeoLocation__longitude__s =0
            );
            insert location;

            insert new FM_User__c(Building__c = location.Id, FM_User__c = objUser.Id);
            Test.startTest();
            System.assert(!FmUploadDocumentsController.queryBuildings(NULL).isEmpty());
            Test.stopTest();
        }

    }

    @isTest
    private static void testUpsertFmDocUploadBatch() {
        Test.startTest();
            FmUploadDocumentsController.upsertFmDocUploadBatch(new FM_Document_Upload_Batch__c(
                Document_Type__c = 'Notice',
                Document_Level__c = 'Global',
                FM_Recipient__c = 'Everyone',
                Notification_Email_Message__c = 'Test',
                Notification_Email_Subject__c = 'Test',
                Notification_SMS_Message__c = 'Test SMS',
                Approval_Status__c = 'Pending'
            ));
        Test.stopTest();
    }

    @isTest
    private static void testUploadDocumentNullParam() {
        Test.startTest();
            try {
                FmUploadDocumentsController.uploadDocument(NULL);
            } catch(Exception e) {}
        Test.stopTest();
    }

    @isTest
    private static void testUploadDocument() {

        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
        MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response_x = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
        response_x.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-12345--Test1.pdf Processed] PK Value#:2-2-005058 CRFfile","PARAM_ID":"IPMS-12345-Test1.pdf","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-12345-Test1.pdf"}],"message":"Process Completed Returning 2 Response Message(s)...","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());

        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;

        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());

        FM_Document_Upload_Batch__c uploadBatch = new FM_Document_Upload_Batch__c(
            Document_Type__c = 'Notice',
            Document_Level__c = 'Global',
            FM_Recipient__c = 'Everyone',
            Notification_Email_Message__c = 'Test',
            Notification_Email_Subject__c = 'Test',
            Notification_SMS_Message__c = 'Test SMS',
            Approval_Status__c = 'Pending'
        );
        insert uploadBatch;
        Attachment attachment = new Attachment(
            Name = 'Test.txt',
            Body = Blob.valueOf('test'),
            ParentId = uploadBatch.Id
        );
        insert attachment;

        FmUploadDocumentsController.Document doc = new FmUploadDocumentsController.Document();
        doc.docType     = 'Notice';
        doc.strBlob     = 'blob';
        doc.docName     = 'Test';
        doc.fileName    = 'Test.pdf';
        doc.description = 'Description';
        doc.isValid     = true;
        doc.startDate   = Date.today();
        doc.endDate     = Date.today();
        doc.buildingId  = NULL;
        doc.docId       = attachment.Id;

        Test.startTest();
            System.assertNotEquals(NULL, FmUploadDocumentsController.uploadDocument(JSON.serialize(doc)));
        Test.stopTest();
    }

    @isTest
    private static void testSendTestEmail() {

        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
        MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response_x = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
        response_x.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-12345--Test1.pdf Processed] PK Value#:2-2-005058 CRFfile","PARAM_ID":"IPMS-12345-Test1.pdf","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-12345-Test1.pdf"}],"message":"Process Completed Returning 2 Response Message(s)...","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());

        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;

        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());

        FM_Document_Upload_Batch__c uploadBatch = new FM_Document_Upload_Batch__c(
            Document_Type__c = 'Notice',
            Document_Level__c = 'Global',
            FM_Recipient__c = 'Everyone',
            Notification_Email_Message__c = 'Test',
            Notification_Email_Subject__c = 'Test',
            Notification_SMS_Message__c = 'Test SMS',
            Approval_Status__c = 'Pending'
        );
        insert uploadBatch;
        Attachment attachment = new Attachment(
            Name = 'Test.txt',
            Body = Blob.valueOf('test'),
            ParentId = uploadBatch.Id
        );
        insert attachment;

        insert new SR_Attachments__c(
            Name = 'Test',
            Document_Type__c = 'Notice',
            Description__c = 'Description',
            IsValid__c = true,
            Building__c = NULL,
            Property__c = NULL,
            Valid_Till__c = Date.today(),
            Attachment_URL__c  = 'www.damacproperties.com',
            FM_Document_Upload_Batch__c = uploadBatch.Id
        );

        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());


        Test.startTest();
            System.assertNotEquals(
                NULL, FmUploadDocumentsController.sendTestEmailNotification(uploadBatch.Id, 'test@mail.com')
            );
        Test.stopTest();
    }

    @isTest
    private static void sendTestSmsNotificationTest() {

        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
        MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response_x = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
        response_x.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-12345--Test1.pdf Processed] PK Value#:2-2-005058 CRFfile","PARAM_ID":"IPMS-12345-Test1.pdf","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-12345-Test1.pdf"}],"message":"Process Completed Returning 2 Response Message(s)...","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());

        FM_Document_Upload_Batch__c uploadBatch = new FM_Document_Upload_Batch__c(
            Document_Type__c = 'Notice',
            Document_Level__c = 'Global',
            FM_Recipient__c = 'Everyone',
            Notification_Email_Message__c = 'Test',
            Notification_Email_Subject__c = 'Test',
            Notification_SMS_Message__c = 'Test SMS',
            Approval_Status__c = 'Pending'
        );
        insert uploadBatch;
        Attachment attachment = new Attachment(
            Name = 'Test.txt',
            Body = Blob.valueOf('test'),
            ParentId = uploadBatch.Id
        );
        insert attachment;

        insert new SR_Attachments__c(
            Name = 'Test',
            Document_Type__c = 'Notice',
            Description__c = 'Description',
            IsValid__c = true,
            Building__c = NULL,
            Property__c = NULL,
            Valid_Till__c = Date.today(),
            Attachment_URL__c  = 'www.damacproperties.com',
            FM_Document_Upload_Batch__c = uploadBatch.Id
        );

        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());

        Test.startTest();
            FmUploadDocumentsController.sendTestSmsNotification(uploadBatch.Id, '+971559270945');
        Test.stopTest();
    }

}