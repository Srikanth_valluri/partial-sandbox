/*******************************************************************************************************
*   Description : Notifies to customer via SMS to remind service charges.                              *
*------------------------------------------------------------------------------------------------------*
* Revision History:                                                                                    *
* Version       Author              Date            Description                                        *
* 1.0           Arjun Khatri        05/03/2019      Initial Draft                                      *
* 1.1           Arjun Khatri        03/07/2019      1.Added criteria of 'ByPassFMReminder'             *
* 1.2           Arjun Khatri        04/07/2019      1.Handled special characters in sms contents       *
*                                                   2.Removed leading 0 from phone numbers             *
********************************************************************************************************/
global class FMServiceChargesSMSBatch implements Database.Batchable<sObject>
                                                    , Database.Stateful
                                                    , Database.AllowsCallouts {
    
    Id personRecTypeId, businessRecTypeId;
    
    Decimal dueAmount = 0.0;
    Static String user = Label.FM_SMS_Service_Username, passwd = Label.FM_SMS_Service_Password, strSID = Label.FM_SMS_Service_SenderId;// 'epms.test@damacgroup.com';

    EmailTemplate FMAMountSmsTemplate_UAE, FMAMountSmsTemplate_Qatar ,FMAMountSmsTemplate_JORDAN;
    Set<String> setProperties ;
    
    public  FMServiceChargesSMSBatch() {
        personRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        dueAmount = Decimal.valueOf(Label.FM_Service_Charges_Amount);
        
        
        setProperties = FM_Service_Charges_properties__c.getAll().keySet();
        system.debug('setProperties=='+setProperties);

        FMAMountSmsTemplate_UAE = [SELECT 
                                ID, Subject, Body, HtmlValue, TemplateType
                            FROM 
                                EmailTemplate 
                            WHERE 
                                DeveloperName = 'FM_Service_Charges_SMS_UAE' limit 1];
        FMAMountSmsTemplate_Qatar = [SELECT 
                                ID, Subject, Body, HtmlValue, TemplateType
                            FROM 
                                EmailTemplate 
                            WHERE 
                                DeveloperName = 'FM_Service_Charges_SMS_Qatar' limit 1];
        FMAMountSmsTemplate_JORDAN = [SELECT 
                                ID, Subject, Body, HtmlValue, TemplateType
                            FROM 
                                EmailTemplate 
                            WHERE 
                                DeveloperName = 'FM_Service_Charges_SMS_Jordan' limit 1];
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
         System.debug('***SMS  Start ****');
         String strQuery = 'SELECT Id, FM_Outstanding_Amount__c,Account_Id__c, Booking__c, Booking__r.Account__c, ' +
                        ' Booking__r.Account__r.recordTypeId, Booking__r.Account__r.Mobile_Phone_Encrypt__pc , ' +
                        ' Booking__r.Account__r.Mobile__c, Booking__r.Account__r.Name, Booking__r.Account__r.IsPersonAccount, '+
                        ' Customer_Name__c,Unit_Name__c, Property_Country__c, Inventory__r.Property_Country__c, Booking__r.Account__r.First_Name_Arabic__c'+
                        ' FROM Booking_Unit__c' +
                        ' WHERE FM_Outstanding_Amount__c != NULL AND  FM_Outstanding_Amount__c != \'\' ' + 
                        ' AND Booking__c != NULL AND Booking__r.Account__c != NULL AND Property_Name_Inventory__c NOT IN : setProperties '+
                        /*' AND (Booking__r.Account__r.ZBEmailStatus__c = \'\' OR Booking__r.Account__r.ZBEmailStatus__c = \'Valid\') ' +*/ 
                        ' AND (Property_Country__c != null  OR Inventory__r.Property_Country__c != null)' +
                        ' AND (Booking__r.Account__r.Mobile_Phone_Encrypt__pc != NULL OR Booking__r.Account__r.Mobile__c != NULL) ' +
                        ' AND (Booking__r.Account__r.recordTypeId =\'' + personRecTypeId + '\'' +
                        ' OR Booking__r.Account__r.recordTypeId = \'' + businessRecTypeId + '\') ' +
                        ' AND SC_From_RentalIncome__c = False AND  CM_Units__c = NULL AND Handover_Flag__c = \'Y\' ' +
                        ' AND Unit_Active__c = \'Active\'  AND ByPassFMReminder__c = false AND FM_Outstanding_Amount_Formula__c >=: dueAmount ';
                        
                        if( String.isNotBlank ( Label.FM_Service_Rec_Id_For_Test ) 
                         && Label.FM_Service_Rec_Id_For_Test.contains('On') && String.isNotBlank( Label.FM_Service_Rec_Id_For_Test.substringAfter('-') ) ) {
                            strQuery = strQuery + ' AND Id = \''+Label.FM_Service_Rec_Id_For_Test.substringAfter('-')+ '\'  ';
                        }
                        
         System.debug('strQuery == ' + strQuery);
         return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<Booking_Unit__c > listBU){ 
        System.debug('***sms execute****');
        System.debug('***listBU ===' + listBU);
        List<SMS_History__c> smsLstFinal  = new List<SMS_History__c>();
        for(Booking_Unit__c objBU :listBU) {
            
            //validate FM Outstanding Amount
            if(Decimal.valueOf(objBU.FM_Outstanding_Amount__c) > dueAmount) {
                
                // TODO added on basis of Label
                String countryLabel = Label.FMServiceChargesNotifierBatch_Country_Label;
                countryLabel.toLowerCase();
                List<String> countryList = new List<String>();
                countryList = countryLabel.split(',');
                Set<String> countrySet = new Set<String>(countryList);
    

                //Validate UAE unit
                if((objBU.Property_Country__c != null 
                    && countrySet.contains(objBU.Property_Country__c.toLowerCase())) 
                ||(objBU.Inventory__r.Property_Country__c != null 
                    &&countrySet.contains(objBU.Inventory__r.Property_Country__c.toLowerCase()))
                ) {
                        if(FMAMountSmsTemplate_UAE != null
                            && (objBU.Property_Country__c.equalsIgnoreCase('United Arab Emirates') 
                                || objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('United Arab Emirates'))
                            ){
                                //Call method to send an sms to units only in UAE
                                smsLstFinal = sendSms(objBU, FMAMountSmsTemplate_UAE);
                        }  
                        else if( FMAMountSmsTemplate_Qatar != null
                            && (objBU.Property_Country__c.equalsIgnoreCase('Saudi Arabia') || objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('Saudi Arabia')
                                || objBU.Property_Country__c.equalsIgnoreCase('QATAR') || objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('QATAR')
                                ) ){
                            //Call method to send an sms to units only in QATAR or AL JAWHARAH
                            smsLstFinal = sendSms(objBU, FMAMountSmsTemplate_Qatar);
                        } 
                        else if( FMAMountSmsTemplate_JORDAN != null
                            && (objBU.Property_Country__c.equalsIgnoreCase('JORDAN')  || objBU.Property_Country__c.equalsIgnoreCase('Lebanon')
                                || objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('JORDAN') || objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('Lebanon')) ){
                            //Call method to send an sms to units only in QATAR or AL JAWHARAH
                            smsLstFinal = sendSms(objBU, FMAMountSmsTemplate_JORDAN);  
                        }

                }// End  if




                //Validate QATAR unit
                /*else if(((objBU.Property_Country__c != null 
                        && (objBU.Property_Country__c.equalsIgnoreCase('Saudi Arabia') 
                            || objBU.Property_Country__c.equalsIgnoreCase('QATAR')))
                    ||(objBU.Inventory__r.Property_Country__c != null 
                        && (objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('Saudi Arabia') 
                            || objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('QATAR'))))
                && Label.FM_Service_Charges_SMS_Qatar != null) {

                        //Call method to send an sms to units only in QATAR or AL JAWHARAH
                        sendSms(objBU, FMAMountSmsTemplate_Qatar);
                }//END QATAR if

                //Validate JORDAN Unit
                else if(((objBU.Property_Country__c != null 
                        && (objBU.Property_Country__c.equalsIgnoreCase('JORDAN') 
                            || objBU.Property_Country__c.equalsIgnoreCase('Lebanon')))
                    ||(objBU.Inventory__r.Property_Country__c != null 
                        && (objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('JORDAN') 
                            || objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('Lebanon'))))
                    && Label.FM_Service_Charges_SMS_Jordan_Lebenon != null) {

                        //Call method to send an sms to units only in QATAR or AL JAWHARAH
                        sendSms(objBU, FMAMountSmsTemplate_JORDAN);
                }//End JORDAN unit if*/
            }//End FM Outstanding Amount if
        }//End for loop
        System.debug('smsLstFinal=== ' + smsLstFinal);

    
        if(smsLstFinal.size() > 0 ) {
            if(!Test.isRunningTest()) {upsert smsLstFinal;}      
            
        }    
    }//End Execute Method
    
/*********************************************************************************
 * Method Name : sendSms
 * Description : send SMS to units
 * Return Type : void
 * Parameter(s): Booking Unit
**********************************************************************************/     
    List<SMS_History__c> sendSms(Booking_Unit__c objBU, EmailTemplate FMAMountSmsTemplate) {
        List<SMS_History__c> smsLst = new List<SMS_History__c>(); 
        System.debug('smsContents == ' + FMAMountSmsTemplate);
        if(FMAMountSmsTemplate != Null) {
            String smsContents = '';
            String customerName = GenericUtility.getCamelCase(objBU.Booking__r.Account__r.Name); 
            String contentValue = FMAMountSmsTemplate.htmlValue.stripHtmlTags();
            String contentBody = FMAMountSmsTemplate.body.replaceAll('\\<.*?\\>', '');
            if(contentValue.contains('{!Booking_Unit__c.Customer_Name__c}')) { 
                contentValue = contentValue.replace('{!Booking_Unit__c.Customer_Name__c}', customerName);
                contentBody = contentBody.replace('{!Booking_Unit__c.Customer_Name__c}' , customerName);                                    
            }

            if(contentValue.contains('{!Booking_Unit__c.FM_Outstanding_Amount__c}')  && String.isNotBlank( objBU.FM_Outstanding_Amount__c )  ) { 
                contentValue = contentValue.replace('{!Booking_Unit__c.FM_Outstanding_Amount__c}'
                                                    , objBU.FM_Outstanding_Amount__c);
                contentBody = contentBody.replace('{!Booking_Unit__c.FM_Outstanding_Amount__c}'
                                                    , objBU.FM_Outstanding_Amount__c);                                    
            }
            
            
            if(contentValue.contains('{!Booking_Unit__c.Unit_Name__c}') && objBU.Unit_Name__c != NULL){
                contentValue = contentValue.replace('{!Booking_Unit__c.Unit_Name__c}', objBU.Unit_Name__c);
                contentBody = contentBody.replace('{!Booking_Unit__c.Unit_Name__c}', objBU.Unit_Name__c);
            }
            
            if(contentValue.contains('<>')) {
                if(String.isBlank(objBU.Booking__r.Account__r.First_Name_Arabic__c) 
                || String.isBlank(objBU.Booking__r.Account__r.Last_Name_Arabic__c)) {
                    contentValue = contentValue.replace('<>', customerName); 
                    contentBody = contentBody.replace('<>', customerName); 
                }else {
                    String arabicName = objBU.Booking__r.Account__r.First_Name_Arabic__c + ' ' +  objBU.Booking__r.Account__r.Last_Name_Arabic__c;
                    contentValue = contentValue.replace('<>', arabicName);
                    contentBody = contentBody.replace('<>', arabicName);
                }
            }
            
            //System.debug('contentValue == ' + contentValue);
            //System.debug('contentBody == ' + contentBody);
            
            String tempContent = 'Dear Customer Name, kindly pay overdue Service Charges towards Unit Number to avoid penalties. \n'+
                                'Pay at '+System.Label.DAMAC_LIVING+' or refer to bank details on the statement for bank/wire transfer.\n\n'+ 
                                'Once paid pls email Proof of Payment to servicecharges@loams.ae'+
                                'LOAMS \n+97147049000';
            System.debug('tempContent == ' + tempContent.length());
            String PhoneNumber = objBU.Booking__r.Account__r.IsPersonAccount ? objBU.Booking__r.Account__r.Mobile_Phone_Encrypt__pc : objBU.Booking__r.Account__r.Mobile__c;
            if(PhoneNumber.startsWith('00')) { PhoneNumber = PhoneNumber.removeStart('00');} 
            else if(PhoneNumber.startsWith('0')) { PhoneNumber = PhoneNumber.removeStart('0'); }
            
            //contentBody = contentBody.substringBefore('LOAMS');
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            req.setMethod('POST' ); // Method Type
            req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx'); 
            string msgCont = GenericUtility.encodeChar(contentValue);
            system.debug('msgCont = ' + msgCont);
            
            strSID = SMSClass.getSenderName(user, PhoneNumber, false);
            
            req.setBody('user='+ user + '&passwd=' + passwd +'&message=' 
                        + msgCont + '&mobilenumber=' + PhoneNumber 
                        + '&sid='+ strSID + '&MTYPE=LNG&DR=Y'); // Request Parameters
            res = http.send(req);
            system.debug('req Body---'+req.getBody());
            system.debug('Response---'+res);
            system.debug('Response body---'+res.getBody());
            
            if(res.getBody() != null){
                 
                // Parse Response
                SMS_History__c smsObj = new SMS_History__c();
                smsObj.Message__c = contentValue;
                smsObj.Phone_Number__c = PhoneNumber;
                smsObj.Customer__c = objBU.Booking__r.Account__c;
                //smsObj.Booking_Unit__c = objBU.Id;
                smsObj.Description__c = res.getBody();
                System.debug('smsObj::::else:'+smsObj);
                    
                if(String.valueOf(res.getBody()).contains('OK:')){
                    smsObj.Is_SMS_Sent__c = true;
                    smsObj.sms_Id__c = res.getBody().substringAfter('OK:');
                }
                smsLst.add(smsObj);
            }
        }
        return smsLst;
    }
    
    global void finish(Database.BatchableContext BC){
        System.debug('***sms finish****');
    }
}