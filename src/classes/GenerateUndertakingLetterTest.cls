/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GenerateUndertakingLetterTest {

    static testMethod void TestNegative(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.NOC_Processing_Pending__c = true;
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Plot NOC').RecordTypeId;
        insert objCase;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateUndertakingLetter controller = new GenerateUndertakingLetter(sc);
        controller.callDrawloop();
        controller.returnToCase();
        Test.StopTest();
   }
    
    static testMethod void TestPositive(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Inventory__c objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        insert lstBookingUnits;
        
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.NOC_Processing_Pending__c = true;
        objCase.New_Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Plot NOC').RecordTypeId;
        insert objCase;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateUndertakingLetter controller = new GenerateUndertakingLetter(sc);
        controller.callDrawloop();
        controller.returnToCase();
        Test.StopTest();
   }
}