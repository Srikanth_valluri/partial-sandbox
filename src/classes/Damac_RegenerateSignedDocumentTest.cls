@isTest
public class Damac_RegenerateSignedDocumentTest {
    static testMethod void regenerateSignedDoc(){
        Account acc = new Account ();
        acc.Name = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '969696';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        insert acc;
        Inquiry__c inquiry = insertInquiry();
        Form_Request__c formRequest = insertFormRequest(acc.Id, inquiry.Id);
        
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal', false, null);
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        sr.Token_Amount_AED__c = 40000;
        sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Change Agent').getRecordTypeId();
        sr.Booking_Wizard_Level__c = null;
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE';
        sr.Mode_of_Payment__c = 'Cash';
        sr.Form_Request__c = formRequest.id;
        sr.agency__c = acc.id;
        insert sr;
        Unit_Documents__c objUnitDocument = new Unit_Documents__c();
        objUnitDocument.Document_Name__c = 'test';
        objUnitDocument.Document_Description__c = 'test wedcf d';
        objUnitDocument.Document_type__c = 'Registration';
        objUnitDocument.Optional__c = false;
        objUnitDocument.Status__c = 'Pending Upload';
        objUnitDocument.User_Doc__c = false;
        objUnitDocument.Service_Request__c = sr.id;
        insert objUnitDocument;

        ESign_Details__c esign = new ESign_Details__c();
        esign.Form_request__c = formRequest.id;
        esign.Unit_Documents__c = objUnitDocument.Id;
        esign.Document_Id__c= '1234567890';
        esign.Status__c = 'Signed';
        insert esign;
        Test.startTest();
            Damac_RegenerateSignedDocument.generateDocument(sr.Id);
        Test.stopTest();
    }
    private static Form_Request__c insertFormRequest(Id accId, Id inquiryId) {
        Form_Request__c formRequest = new Form_Request__c();
        formRequest.Corporate__c = accId;
        formRequest.Inquiry__c = inquiryId;
        formRequest.Status__c = 'Sign Now';
        insert formRequest;
        return formRequest;
    }
    
    private static Inquiry__c insertInquiry() {
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c = 'First';
        inquiry.Last_Name__c = 'Last';
        inquiry.Inquiry_Status__c = 'Active';
        inquiry.Email__c = 'test@email.com';
        insert inquiry;
        return inquiry;
    }
}