public without sharing class BookingUnitTriggerHandlerHO{
    public void beforeUpdate(map<Id,Booking_Unit__c> newMap, map<Id,Booking_Unit__c> oldMap){
        set<Id> setBookingUnitIds = new set<Id>();
        for(Booking_Unit__c objBU : newMap.values()){
            if( String.isBlank( String.valueOf(objBU.DLP_Start_Date__c) ) )
            {
                if( ((objBU.Handover_Flag__c == 'Y' && objBU.Handover_Date__c != null) 
                    && (objBU.Handover_Flag__c != oldMap.get(objBU.Id).Handover_Flag__c 
                        || objBU.Handover_Date__c != oldMap.get(objBU.Id).Handover_Date__c ))
                || ((objBu.Early_Handover__c == true && objBU.Early_Handover_Date__c != null) 
                    && (objBU.Early_Handover__c != oldMap.get(objBU.Id).Early_Handover__c 
                        || objBU.Early_Handover_Date__c != oldMap.get(objBU.Id).Early_Handover_Date__c))
                || ((objBu.Lease_Handover__c == true && objBU.Lease_Handover_Date__c != null) 
                    && (objBU.Lease_Handover__c != oldMap.get(objBU.Id).Lease_Handover__c 
                        || objBU.Lease_Handover_Date__c != oldMap.get(objBU.Id).Lease_Handover_Date__c))
                ){
                    //objBU.Early_Handover__c = false;
                    // NOT EHO and Notice SET DLP = HO NOTICE + 365
                    if(objBU.Handover_Flag__c == 'Y' && objBU.Handover_Notice_Sent_Date__c != null){
                        objBU.DLP_Start_Date__c = objBU.Handover_Notice_Sent_Date__c;
                        objBU.DLP_End_Date__c = objBU.Handover_Notice_Sent_Date__c.addYears(1);
                        objBU.DLP_Validity__c = '';
                    }else if(objBU.Handover_Flag__c == 'Y' && objBU.Handover_Notice_Sent_Date__c == null){
                        objBU.DLP_Start_Date__c = objBU.Handover_Date__c;
                        objBU.DLP_End_Date__c = objBU.Handover_Date__c.addYears(1);

                    }else if(objBU.Early_Handover__c && objBU.Lease_Commencement_Date__c != null){
                        /* // IF EHO and Lease Date Available SET DLP = Lease_Commencement_Date__c + 365
                        // DATE must be in the format 2015/11/19 00:00:00 (YYYY/MM/DD HH:MM:SS)
                        String str = objBU.Lease_Commencement_Date__c;
                        str = str.subStringBefore(' ');
                        list<String> lstStr = str.split('/');
                        expDate = date.newinstance(integer.valueOf(lstStr[0]),integer.valueOf(lstStr[1]),integer.valueOf(lstStr[2]));
                        */
                        objBU.DLP_Start_Date__c = objBU.Lease_Commencement_Date_Formula__c;
                        objBU.DLP_End_Date__c = objBU.Lease_Commencement_Date_Formula__c.addYears(1);
                        objBU.DLP_Validity__c = '';

                    }else if(objBU.Early_Handover__c && objBU.Lease_Commencement_Date__c == null){
                        objBU.DLP_Start_Date__c = objBU.Early_Handover_Date__c;
                        objBU.DLP_End_Date__c = objBU.Early_Handover_Date__c.addYears(1);
                        
                    }else if( (objBU.Lease_Handover__c  && objBU.Lease_Handover_Date__c != null) ){
                    objBU.DLP_Start_Date__c = objBU.Lease_Handover_Date__c;
                    objBU.DLP_End_Date__c = objBU.Lease_Handover_Date__c.addYears(1);

                    }/*else{
                        setBookingUnitIds.add(objBU.Id);
                    }*/
                }else if( (objBU.Handover_Flag__c == 'Y' && objBU.Handover_Date__c == null)
                        || (objBU.Early_Handover__c  && objBU.Early_Handover_Date__c == null)
                        || (objBU.Lease_Handover__c  && objBU.Lease_Handover_Date__c == null)){
                    objBU.DLP_Start_Date__c = System.today();
                    objBU.DLP_End_Date__c = System.today().addYears(1);
                }
            }// and of outer If
        } // end of for loop
        /*if(!setBookingUnitIds.isEmpty()){
            map<Id,String> mapBU_LastInstallment = new map<Id,String>();
            map<Id,Date> mapBU_LastInstallmentDate = new map<Id,Date>();
            for(Payment_Terms__c objPT : [Select p.Name
                                               , p.Milestone_Event__c
                                               , p.Id
                                               , p.Description__c
                                               , p.Installment__c
                                               , p.Payment_Plan__c
                                               , p.Payment_Date__c
                                               , p.Payment_Plan__r.Booking_Unit__c
                                          From Payment_Terms__c p
                                          where p.Payment_Plan__r.Booking_Unit__c IN : setBookingUnitIds
                                          order by Installment__c]){
                if(objPT.Payment_Plan__r.Booking_Unit__c != null
                && objPT.Milestone_Event__c != null){
                    mapBU_LastInstallment.put(objPT.Payment_Plan__r.Booking_Unit__c, objPT.Milestone_Event__c);
                }
                if(objPT.Payment_Plan__r.Booking_Unit__c != null
                && objPT.Payment_Date__c != null){
                    mapBU_LastInstallmentDate.put(objPT.Payment_Plan__r.Booking_Unit__c, objPT.Payment_Date__c);
                }
            }
            if(!mapBU_LastInstallment.isEmpty()){
                set<String> setString = new set<String>();
                for(Milestones_For_Handover__c objMStone : [Select Id
                                                                 , Name 
                                                            From Milestones_For_Handover__c]){
                    setString.add(objMStone.Name);
                }
                
                for(Id bookingUnitId : mapBU_LastInstallment.keySet()){
                    if(!setString.contains(mapBU_LastInstallment.get(bookingUnitId))){
                        newMap.get(bookingUnitId).DLP_Start_Date__c = mapBU_LastInstallmentDate.get(bookingUnitId);
                        newMap.get(bookingUnitId).DLP_End_Date__c = newMap.get(bookingUnitId).DLP_Start_Date__c.addYears(1);
                        newMap.get(bookingUnitId).DLP_Validity__c = '';
                    }
                }
            }
        }*/
    } // end of beforeUpdate
}