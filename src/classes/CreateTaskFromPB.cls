public class CreateTaskFromPB {
     @InvocableMethod
    public static void updateCreateTask(List<Id> caseLst) {
        if(caseLst != Null && caseLst.size() > 0) {
            futureCreateInformCustomer(caseLst);
        }
    }
    
    
    @future(callout=true)
    public static void futureCreateInformCustomer (List<Id> CasePdcIds ) {
       List<Task> taskListToInsert = new List<Task>();
       List<Case> caseList = [Select Id,OwnerId,CurrencyIsoCode from Case where Id IN: CasePdcIds];
       for(Case objCase : caseList) {
          /*Task objTask = new Task();
                                        objTask.Subject = System.Label.Inform_Customer_to_pick_up_cheque;
                                        objTask.Assigned_User__c = 'DLP Team';
                                        objTask.ActivityDate = Date.today()+2;
                                        objTask.OwnerId = objCase.OwnerId;
                                        objTask.Priority = 'Normal';
                                        objTask.CurrencyIsoCode = objCase.CurrencyIsoCode;
                                        objTask.Status = 'Not Started';
                                        objTask.WhatId = objCase.Id;
                                        taskListToInsert.add(objTask);*/
        Task objTask = DEWARefundTasks.createTask(System.Label.Inform_Customer_to_pick_up_cheque,objCase,'DLP Team');
        taskListToInsert.add(objTask);
        
       }
        
         if(taskListToInsert != Null && taskListToInsert.size() > 0) {
                    insert taskListToInsert;
                    System.debug('After Insert Task>>'+taskListToInsert);
         }
    }

}