/**************************************************************************************************
 * @Name              : InventoryUpdateSchedulable
 * @Test Class Name   : InventorySchedulableTest
 * @Description       : Schedulable class to invoke Queueable class to update Inventory Details in IPMS
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst          25/08/2020       Created
**************************************************************************************************/
public class InventoryUpdateSchedulable implements Schedulable {
    public list<Inventory__c> invList {get;set;}
    public Map<Id, String> invOldStatusMap {get; set;}
    
    public InventoryUpdateSchedulable(List<Inventory__c> invList, Map<Id, String> invOldStatusMap){
        this.invList = invList;
        this.invOldStatusMap = invOldStatusMap;
    }
    public void execute(SchedulableContext sc) {
        System.enqueueJob(new InventoryUpdateIPMSQueueable(invList, invOldStatusMap));
        // Abort the job once the job is queued
        System.abortJob(sc.getTriggerId());
    }
}