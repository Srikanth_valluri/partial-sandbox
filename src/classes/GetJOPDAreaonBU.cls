global class GetJOPDAreaonBU implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'Select JOPD_Area__c, Registration_ID__c, Inventory__c, Inventory__r.Building_Location__c, Inventory__r.Total_Area_JOPD__c From Booking_Unit__c Where Registration_ID__c != null';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Booking_Unit__c> scope){
        System.debug('===scope==' +scope);
        List<Error_Log__c> lstErorLogs = new List<Error_Log__c>();
        list<Booking_Unit__c> lstBookingUnits = new list<Booking_Unit__c>();
        set<Id> setBuildingIds = new set<Id>();
        list<Location__c> lstLocation = new list<Location__c>();
        map<Id, Set<String>> mapLocationIdTask = new map<Id, Set<String>>();
        list<Task> lstTask = new list<Task>();
        for (Booking_Unit__c bookingUnitRecord : scope) {
            if (bookingUnitRecord.JOPD_Area__c == null) {
                //Updated logic to get JOPD area from IP
                try{
                    CallEarlyHandoverToGetHandoverDetails.EarlyHandoverMQService1Response objResponse;
                    objResponse = CallEarlyHandoverToGetHandoverDetails.CallEarlyHandoverMQService1GetHandoverDetails(bookingUnitRecord.Registration_ID__c);
                    system.debug('objResponse '+objResponse );
                    // if (bookingUnitRecord.Inventory__c != null && bookingUnitRecord.Inventory__r.Total_Area_JOPD__c != null) {
                    //     bookingUnitRecord.JOPD_Area__c = bookingUnitRecord.Inventory__r.Total_Area_JOPD__c;
                    //     lstBookingUnits.add(bookingUnitRecord);
                    // }
                    if(objResponse != NULL) {

                        if(String.isNotBlank(objResponse.JOPD_Total_Area)) {
                            bookingUnitRecord.JOPD_Area__c = objResponse.JOPD_Total_Area;
                            lstBookingUnits.add(bookingUnitRecord);

                        }
                        else {
                            lstErorLogs.add(new Error_Log__c(Booking_Unit__c=bookingUnitRecord.Id, Error_Details__c='JOPD area is blank or null',
                            Process_Name__c='Handover'));
                        }

                    }
                    else {
                        lstErorLogs.add(new Error_Log__c(Booking_Unit__c=bookingUnitRecord.Id, Error_Details__c='No response received from IPMS',
                            Process_Name__c='Handover'));
                    }
                }//try
                catch(Exception excp) {
                    lstErorLogs.add(new Error_Log__c(Booking_Unit__c=bookingUnitRecord.Id, Error_Details__c=excp.getMessage(),
                            Process_Name__c='Handover'));
                }
            }//if
            if (bookingUnitRecord.Inventory__c != null && bookingUnitRecord.Inventory__r.Building_Location__c != null) {
                setBuildingIds.add(bookingUnitRecord.Inventory__r.Building_Location__c);
            }
        }//for
        try{
            if (lstBookingUnits != null && lstBookingUnits.size() > 0) {
                update lstBookingUnits;
            }

            //Commented on 05-01-2018
            /*if (setBuildingIds != null && setBuildingIds.size() > 0) {
                for (Location__c objLoc : [Select Id,
                                                  As_Built_Drawings_Uploaded__c,
                                                  RERA_Audit_Flag__c,
                                                  Approved_By_Authority__c,
                                                  Snag_Status__c,
                                                  Communications_Added__c,
                                                  Civil_Defence_Available__c,
                                                  Utilities_Available__c,
                                                  Provided_Access_to_Building__c,
                                                  Total_Area_JOPD__c ,
                                                  BCC_Generated__c
                                            From Location__c
                                            Where Id IN: setBuildingIds]) {
                    lstLocation.add(objLoc);
                }

                for (Task objTask : [Select Id, Subject, Status, WhatId From Task
                                        Where WhatId IN: setBuildingIds]) {
                    if (mapLocationIdTask.containsKey(objTask.WhatId)) {
                        Set<String> setSubject = mapLocationIdTask.get(objTask.WhatId);
                        setSubject.add(objTask.Subject);
                        mapLocationIdTask.put(objTask.WhatId, setSubject);
                    } else {
                        mapLocationIdTask.put(objTask.WhatId, new set<String> {objTask.Subject});
                    }
                }

            }

            for (Location__c objLoc : lstLocation){
                set<String> setTaskSubjects = mapLocationIdTask.get(objLoc.id);
                if (objLoc.As_Built_Drawings_Uploaded__c == true && objLoc.RERA_Audit_Flag__c == 'Complete'
                    && objLoc.Approved_By_Authority__c == true && objLoc.Snag_Status__c == 'Completed' && objLoc.Communications_Added__c == true
                    && objLoc.Civil_Defence_Available__c == true && objLoc.Utilities_Available__c == true && objLoc.BCC_Generated__c == true
                    && objLoc.Provided_Access_to_Building__c == true) {
                        if (setTaskSubjects == null || (setTaskSubjects != null && !setTaskSubjects.contains('Acceptance to Proceed with Handover'))){
                            Task objTask = new Task();
                            objTask.Subject = 'Acceptance to Proceed with Handover';
                            objTask.Status = 'Not Started';
                            objTask.Priority = 'Medium';
                            objTask.WhatId = objLoc.id;
                            objTask.Process_Name__c = 'Handover';
                            objTask.Assigned_User__c = 'Handover HOD';
                            lstTask.add(objTask);
                        }
                    }
            }

            if (lstTask != null && lstTask.size()>0) {
                insert lstTask;
            }*/
            if(lstErorLogs.size() > 0) {
                insert lstErorLogs;
            }
        }//try
        catch(Exception excp) {
            insert new Error_Log__c(Error_Details__c=excp.getMessage(),Process_Name__c='Handover');
        }


    }

    global void finish(Database.BatchableContext BC){

    }
}