public without sharing class MortgageProcessControllerClone_CC extends MortgageProcessControllerClone{
    public MortgageProcessControllerClone_CC() {
        super();
        
    }

    public override PageReference CloseCase(){
        //System.debug('=CloseCase called=============mortgageCaseObj ==== ' + mortgageCaseObj);
        mortgageCaseObj.Status ='Closed';
        mortgageCaseObj.Mortgage_Case_Close_Reason__c = 'Token deposit is not yet paid.';
        update mortgageCaseObj;

        PageReference pageRef = new PageReference('/Customer');
        pageRef.setRedirect(true);
        return pageRef;
        //return null ;
    }

    public override PageReference mortgageSubmitCase(){
        System.debug('--Mortgage Case Submit--');   
        mortgageCaseObj.Status=MortgageProcessUtility.MORTGAGE_SUBMIT_STATUS;
        saveCase();

        /*PageReference pageRef = new PageReference('/'+mortgageCaseObj.Id);
        pageRef.setRedirect(true);
        return pageRef;*/
        return null ;
    }

    public override void saveCase(){ 
        try{
            
            system.debug('try----------SaveCase'+selectedBookingUnitId);
            mortgageEligibilityCheck();
            String poaFileName = '';
            String codFileName = '';
            //if(!selectedBookingUnitId.equalsIgnoreCase('--Select Unit--')){
                if( String.isBlank( mortgageCaseObj.AccountId ) ) {  
                    mortgageCaseObj.AccountId = selectedAcc;  
                }
                if( String.isBlank( mortgageCaseObj.Booking_Unit__c ) && !selectedBookingUnitId.equalsIgnoreCase('--Select Unit--')) { 
                    mortgageCaseObj.Booking_Unit__c = selectedBookingUnitId;  
                }

                System.debug('Upsert Mortage mortgageCaseObj  '+mortgageCaseObj);   
                Integer i = 1;
                List<UploadCOCDMultipleDocController.MultipleDocRequest> lstMultipleDocReq = new List<UploadCOCDMultipleDocController.MultipleDocRequest>();
                if(String.isNotBlank( crfAttachmentBody ) && String.isNotBlank( crfAttachmentName  )){
                    UploadCOCDMultipleDocController.MultipleDocRequest objMultipleDocRequest =
                    new UploadCOCDMultipleDocController.MultipleDocRequest();
                    objMultipleDocRequest.category = 'Document';
                    objMultipleDocRequest.entityName = 'Damac Service Requests';
                    objMultipleDocRequest.base64Binary = EncodingUtil.base64Encode(extractBody(crfAttachmentBody));
                    objMultipleDocRequest.fileDescription = extractType(crfAttachmentName);
                    objMultipleDocRequest.fileId = mortgageCaseObj.CaseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(crfAttachmentName);
                    objMultipleDocRequest.fileName = mortgageCaseObj.CaseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(crfAttachmentName);
                    i++;
                    objMultipleDocRequest.partyId = mortgageCaseObj.CaseNumber;
                    objMultipleDocRequest.sourceId = 'IPMS-'+mortgageCaseObj.CaseNumber+'-'+extractName(crfAttachmentName);
                    objMultipleDocRequest.sourceFileName = 'IPMS-'+mortgageCaseObj.CaseNumber+'-'+extractName(crfAttachmentName);
                    codFileName = objMultipleDocRequest.sourceFileName;
                    lstMultipleDocReq.add(objMultipleDocRequest);
                }
                if(String.isNotBlank( poaAttachmentBody ) && String.isNotBlank( poaAttachmentName )){
                    UploadCOCDMultipleDocController.MultipleDocRequest objMultipleDocRequest =
                    new UploadCOCDMultipleDocController.MultipleDocRequest();
                    objMultipleDocRequest.category = 'Document';
                    objMultipleDocRequest.entityName = 'Damac Service Requests';
                    objMultipleDocRequest.base64Binary = EncodingUtil.base64Encode(extractBody(poaAttachmentBody));
                    objMultipleDocRequest.fileDescription = extractType(poaAttachmentName);
                    i++;
                    objMultipleDocRequest.fileId = mortgageCaseObj.CaseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(poaAttachmentName);
                    objMultipleDocRequest.fileName = mortgageCaseObj.CaseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(poaAttachmentName);

                    objMultipleDocRequest.partyId = mortgageCaseObj.CaseNumber;
                    objMultipleDocRequest.sourceId = 'IPMS-'+mortgageCaseObj.CaseNumber+'-'+extractName(poaAttachmentName);
                    objMultipleDocRequest.sourceFileName = 'IPMS-'+mortgageCaseObj.CaseNumber+'-'+extractName(poaAttachmentName);
                    poaFileName = objMultipleDocRequest.sourceFileName;
                    lstMultipleDocReq.add(objMultipleDocRequest);
                }

                if(lstMultipleDocReq.size() > 0) {
                    UploadCOCDMultipleDocController.data objResponseData = new UploadCOCDMultipleDocController.data();
                    objResponseData = UploadCOCDMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);
                    //System.debug('objResponseData DOC*****'+objResponseData);
                    if(objResponseData != null && objResponseData.status == 'S'
                    && objResponseData.Data != null && objResponseData.Data.size() > 0){
                        for(UploadCOCDMultipleDocController.MultipleDocResponse objData : objResponseData.Data){
                            if(String.isNotBlank(objData.url) && String.isNotBlank(poaFileName) && (objData.PARAM_ID.containsIgnoreCase(poaFileName))){
                                ///system.debug('*In if**POAfile********');
                                if( String.isNotBlank( poaAttachmentBody ) && String.isNotBlank( poaAttachmentName ) ){
                                    uploadAttachment( 'Power of Attorney', extractBody(poaAttachmentBody), mortgageCaseObj.Id, extractType(poaAttachmentName), MortgageProcessUtility.MORTGAGE_POA_NAME, objData.url);
                                    poaAttachmentBody = null;
                                    poaAttachmentName = null;
                                }
                            } else if(String.isNotBlank(objData.url) && String.isNotBlank(codFileName) && (objData.PARAM_ID.containsIgnoreCase(codFileName))){
                                if( String.isNotBlank( crfAttachmentBody ) && String.isNotBlank( crfAttachmentName ) ){
                                    uploadAttachment( extractName(crfAttachmentName), extractBody(crfAttachmentBody), mortgageCaseObj.Id, extractType(crfAttachmentName), MortgageProcessUtility.MORTGAGE_CRF_NAME, objData.url);
                                    poaAttachmentBody = null;
                                    poaAttachmentName = null;
                                }
                            }
                        }
                    }
                }

                //System.debug('Upsert Mortage Case');
                CaseID = mortgageCaseObj.Id;
                upsert mortgageCaseObj;
                mortgageCaseObj = [SELECT Id, AccountId, CaseNumber
              ,Length_of_Business__c,Length_of_Service__c,Years_in_UAE__c,Share_in_N_P__c,Share_in_Capital__c,
               Employer_Name__c,Loan_Amount_Requested__c,Designation__c,
               EOI_reference_Number__c, IsPOA__c, POA_Name__c, Effective_LOB__c,Nature_of_Business__c ,
               POA_Relation_With_Owner__c, POA_Expiry_Date__c, POA_Issued_By__c, Age__c,
                Financial_Status__c,Owner.Name,Account.Name,
                     Other_Sources_of_Income__c, Residency_Status__c, First_Property__c, 
                     Mortgage_Bank_Name__c, Mortgage_Bank_Address__c, Mortgage_Transaction_Type__c, Monthly_Salary__c, 
                     Mortgage_Additional_Income_1__c, Mortgage_Additional_Income_3__c, 
                     Mortgage_Loan_1__c, Mortgage_Loan_3__c, 
                     //Mortgage_CC_1__c,Mortgage_CC_2__c, Mortgage_CC_3__c, Mortgage_CC_4__c,Mortgage_CC_5__c,
                     Mortgage_Number_of_Months__c, Mortgage_Allowances__c,
                     Mortgage_Additional_Income_2__c, Mortgage_Additional_Income_4__c,
                     Mortgage_Loan_2__c, Mortgage_Loan_4__c, 
                     Mortgage_Rental_Income__c, Mortgage_Rate_of_Interest__c, Mortgage_Start_Date__c, 
                     Mortgage_Value__c, Mortgage_End_Date__c, Status, 
                     Mortgage_Case_Close_Reason__c, Booking_Unit__c, Mortgage_Off_Ready_Plan__c, 
                     Customer_Eligible_For_Mortgage__c, OCR_verified__c, Salaried__c,Self_Employed__c, 
                     Booking_Unit__r.Name,Booking_Unit__r.Unit_Name__c
                FROM Case
               WHERE Id = :mortgageCaseObj.id];
                Task objTask = TaskUtility.getTask((SObject)mortgageCaseObj,
                    'Verify documents and Complete details', 'CRE', 'Mortgage',
                    system.today().addDays(1));
                    objTask.OwnerId = Label.DefaultCaseOwnerId;
                    //objTask.Status = 'Completed';
                    insert objTask;
           // }
        }
        catch(Exception e){
            System.debug('Exception saveCase=========== ' + e);
            Error_Log__c errorLogObj = new Error_Log__c(Error_Details__c=e.getMessage(),Case__c=mortgageCaseObj.Id, Process_Name__c='Mortgage');
            insert errorLogObj;
        } 
        /*PageReference pageRef = new PageReference('/'+mortgageCaseObj.Id);
        pageRef.setRedirect(true);
        return pageRef;*/
    }

    public override void getUnitListOnLoad(){

        
        /*query on case to get submitted cases */
        List<Case> submittedCase = new List<Case>();
            submittedCase = [SELECT id
                                , Status
                                , AccountId
                            FROM Case
                            WHERE AccountId = :selectedAcc
                            AND Status = 'Submitted'
                            AND RecordType.Name = 'Mortgage'];
        //system.assert(false,'<<<<<< : '+submittedCase );
        if(submittedCase != null && submittedCase.size() > 1){
            caseNotCreated = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Case is already Submitted for this customer'));
        }else{
            caseNotCreated = true;
        }
    
        Id caseId = Apexpages.currentpage().getparameters().get('CaseId');
        //System.debug('=================getUnitListOnLoad called=========caseId========' + caseId);
        if(String.isNotBlank(caseId)) {
            //System.debug('===============if==getUnitListOnLoad called=========caseId========' + caseId);
            mortgageCaseObj = getCaseRecord(caseId);
            //System.debug('***if========' + mortgageCaseObj);
            if(String.isNotBlank(mortgageCaseObj.Booking_Unit__c)) {
                lstUnits.add(new SelectOption(mortgageCaseObj.Booking_Unit__c,mortgageCaseObj.Booking_Unit__r.Unit_Name__c));
            } else if(String.isNotBlank(mortgageCaseObj.EOI_reference_Number__c)) {
                lstUnits = new List<SelectOption>();
            } else if( String.isBlank(mortgageCaseObj.EOI_reference_Number__c) && String.isBlank(mortgageCaseObj.Booking_Unit__c)){
                getActiveUnitList();
            }
            if(String.isNotBlank(mortgageCaseObj.Financial_Status__c)) {
                financtialStatus = mortgageCaseObj.Financial_Status__c;
                updateDocumentList();
            }
            if( String.isBlank(mortgageCaseObj.AccountId) ){
                errorMessage= 'This SR cannot be proceeded as there is no customer associated with it. Please create a new COD SR.';
                ApexPages.addmessage(new ApexPages.message( ApexPages.severity.Error,errorMessage));
                iserror=true;
                return;
            }
            ListMortgage = [Select Id, Case__c, Attachment_URL__c                                  
                            From SR_Attachments__c
                           where Case__c = :caseId
                           AND Name LIKE '%CRF%'];
            if(ListMortgage.size() > 0) 
                crfUrl = ListMortgage[0].Attachment_URL__c;

            //System.debug('============lstExistingDocs ListMortgage CRF doc=======' + ListMortgage);                                           
        } else {
            //System.debug('=========else========getUnitListOnLoad called=========selectedAcc========' + selectedAcc);
            currentAccount = [SELECT Name, Name_Arabic__c, Id, IsPersonAccount, Party_ID__c, Party_Type__c, Country__c, Country_Arabic__c, Address_Line_1__c, Address_Line_1_Arabic__c,
                                        Address_Line_2__c, Address_Line_2_Arabic__c, Address_Line_3__c, Address_Line_3_Arabic__c, Address_Line_4__c, Address_Line_4_Arabic__c,
                                        City__c, City_Arabic__c, State__c,State_Arabic__c, Postal_Code_Arabic__c, Email__c, Mobile_Country_Code__c, Mobile__c,
                                        Home_Phone_Country_Code__c, Home_Phone_Encrypt__c,Work_Phone_Country_Code__c,Other_Phone_Encrypt__c,Middle_Name__c, Middle_Name_Arabic__c, Last_Name__c,
                                        Last_Name_Arabic__c, Country__pc,Country_Arabic__pc,Address_Line_1__pc,Address_Line_2__pc,Address_Line_3__pc,Address_Line_4__pc, CR_Registration_Expiry_Date__c,
                                        Address_Line_1_Arabic__pc,Address_Line_2_Arabic__pc,Address_Line_3_Arabic__pc,Address_Line_4_Arabic__pc,City__pc,City_Arabic__pc,BillingPostalCode,
                                        Email__pc,Mobile_Country_Code__pc,Mobile_Phone_Encrypt__pc,Passport_Number__pc,Passport_Expiry_Date__pc,Passport_Issue_Place__pc,Passport_Issue_Place_Arabic__pc,
                                        Nationality__pc,Nationality_Arabic__pc,PersonTitle,Title_Arabic__pc,FirstName,First_Name_Arabic__pc,MiddleName,Middle_Name_Arabic__pc,LastName,Last_Name_Arabic__pc,
                                        Mobile_Country_Code_2__pc,Mobile_Country_Code_2__c, Mobile_Phone_Encrypt_2__pc, Mobile_Phone_2__c, Mobile_Country_Code_3__pc,Mobile_Country_Code_3__c,
                                        Mobile_Phone_Encrypt_3__pc, Mobile_Phone_3__c,Mobile_Country_Code_4__pc,Mobile_Country_Code_4__c, Mobile_Phone_Encrypt_4__pc, Mobile_Phone_4__c,
                                        Mobile_Country_Code_5__pc,Mobile_Country_Code_5__c, Mobile_Phone_Encrypt_5__pc, Mobile_Phone_5__c,Email_2__pc, Email_2__c
                                     FROM Account WHERE Id =: selectedAcc ];
            // Check if active unit present
            getActiveUnitList();
            //getUnits();

            // Create Mortgage SR
            mortgageCaseObj = new Case(Type=MortgageProcessUtility.MORTGAGE_RECTYPE_NAME,
                                       RecordTypeId=MortgageProcessUtility.getRecordTypeId(),
                                       Status=MortgageProcessUtility.MORTGAGE_INITIAL_STATUS,
                                       Customer_Eligible_For_Mortgage__c = true,
                                       AccountId = selectedAcc,
                                       Origin = 'Portal'
                                       //OCR_verified__c = false
                                       );
            insert mortgageCaseObj;
            //system.debug('======Mortgage Case created : ' + mortgageCaseObj);
        }
    }
}