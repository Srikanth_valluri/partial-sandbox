/*
 Class Name      : Damac_PushNotifications
 Created Date    : 22/07/2020
 Created By      : Srikanth V
 Description     : To send the push notifications to Damac Central API
 */
public class Damac_PushNotifications{
    // To send the push notification to Damac Central API
    @Future(Callout = TRUE)
    public static void insertPushMessage(Set<ID> notificationRecordIds){
        List<log__c> logList = new List<log__c>();

        DAMAC_Central_Push_Notifications__c credentials = DAMAC_Central_Push_Notifications__c.getInstance(UserInfo.getUserID());

        String loginRequestBody = loginRequestBody(credentials);
        String accessToken = '';
        HTTPResponse loginResponse = login(credentials, loginRequestBody);
        System.debug(loginResponse.getBody());
        if (loginResponse.getStatusCode() == 200){
            accessToken = loginResponseBody(loginResponse.getBody());
        }

        for (Push_Notifications__c record : [SELECT Dest_Application_Id__c, Email__c, Deal_Exception_Request__c, Message__c, Title__c, Source_Application_Id__c, Template_Id__c, detail_type__c
                                             FROM Push_Notifications__c
                                             WHERE ID IN :notificationRecordIds]){
            if (accessToken != '' && accessToken != null){
                String reqBody = notificationRequestBody(record);
                HTTPRequest req = new HTTPRequest();
                req.setEndpoint(credentials.API_URL__c + 'PushNotification/PushMessage');
                req.setHeader('Content-Type', 'application/JSON');
                req.setHeader('Api-Token', credentials.API_Token__c);
                req.setHeader('Access-Token', accessToken);
                req.setMethod('POST');
                req.setBody(reqBody);
                req.setTimeOut(120000);

                HTTP http = new HTTP();
                HTTPResponse res = new HTTPResponse();
                if (!Test.isRunningTest())
                    res = http.send(req);
                logList.add(createLog(record.Id, loginRequestBody.left(32768), loginResponse.getBody().left(131072), 'SUCCESS', 'Login'));
                String notificationStatus = 'SUCCESS';
                if (res.getStatusCode() != 200){
                    notificationStatus = 'ERROR';
                }
                logList.add(createLog(record.Id, reqBody.left(32768), res.getBody().left(131072), notificationStatus, 'Notification'));
            } else{
                logList.add(createLog(record.Id, loginRequestBody.left(32768), loginResponse.getBody().left(131072), 'ERROR', 'Login'));
            }

        }
        if (logList.size() > 0){
            insert logList;
        }
    }

    // To prepare the request body json format for PUSH notification
    public static String notificationRequestBody(Push_Notifications__c record){
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('email', record.Email__c);
        gen.writeNumberField('template_id', record.Template_Id__c.intvalue());
        gen.writeStringField('message', record.Message__c);
        gen.writeStringField('title', record.Title__c);
        
        
        gen.writeFieldName('payload');
        gen.writeStartObject();
        gen.writeStringField('email', record.Email__c);
        gen.writeStringField('detail_type', record.detail_type__c);
        gen.writeStringField('unique_identifier', record.Deal_Exception_Request__c);
        gen.writeEndObject();
        
        string sobjectType = (record.detail_type__c == 'dealreinstatement' ? 'NSIBPM__Service_Request__c' : 'Deal_Exception_Request__c');
        list<user> approvers = [Select id from User where email =: record.Email__c and accountId != null and isActive = true];
        list<ProcessInstanceStep> lstSteps = [Select id 
                                            from ProcessInstanceStep 
                                            where StepStatus = 'Pending' and OriginalActorId in: approvers 
                                            and (ProcessInstance.TargetObject.Type = 'NSIBPM__Service_Request__c' or 
                                            ProcessInstance.TargetObject.Type = 'Deal_Exception_Request__c')];
        gen.writeNumberField('total_pending_count',lstSteps.size());
        gen.writeNumberField('source_application_id', record.Source_Application_Id__c.intvalue());
        gen.writeNumberField('dest_application_id', record.Dest_Application_Id__c.intvalue());
        gen.writeEndObject();
        return gen.getAsString();
    }

    // To generate the access token
    public static HTTPResponse login(DAMAC_Central_Push_Notifications__c credentials, String reqBody){
        HTTPRequest req = new HTTPRequest();
        req.setEndpoint(credentials.API_URL__c + 'authenticate/AuthenticateUserWithDetail');
        req.setHeader('Api-Token', credentials.Api_Token__c);
        req.setMethod('POST');
        req.setBody(reqBody);
        req.setTimeOut(120000);
        req.setHeader('Content-Type', 'application/JSON');
        HTTP http = new HTTP();
        HTTPResponse res = new HTTPResponse();
        if (!Test.isRunningTest())
            res = http.send(req);
        else{
            res.setStatusCode(200);
            res.setBody('{"data" : {"access_token" : "testToken"}}');
        }
        return res;
    }

    // To parse the response from login method and return the access token
    public static String loginResponseBody(String response){
        Damac_PushNotifications obj = Damac_PushNotifications.parseLoginResponse(response);
        return obj.data.access_token;
    }

    // To prepare the request body json format for Login
    public static String loginRequestBody(DAMAC_Central_Push_Notifications__c credentials){
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('email', credentials.Email__c);
        gen.writeStringField('password', credentials.Password__c);
        gen.writeStringField('device_source', credentials.device_source__c);
        gen.writeStringField('device_os_version', credentials.device_os_version__c);
        gen.writeStringField('app_version', credentials.app_version__c);
        gen.writeStringField('device_model', credentials.device_model__c);
        gen.writeNumberField('app_id', credentials.app_id__c.intvalue());
        gen.writeBooleanField('is_authorization_required', credentials.is_authorization_required__c);
        gen.writeEndObject();
        return gen.getAsString();
    }

    // Wrapper object classes to parse the login response
    public cls_data data;
    public class cls_data{
        public String access_token;    //D1C7CC190B49C9DD8AACEBF71CFB1A4F
    }

    public static Damac_PushNotifications parseLoginResponse(String json){
        return (Damac_PushNotifications)System.JSON.deserialize(json, Damac_PushNotifications.class);
    }

    public static log__c createLog(string parentId, String request, String response, String Status, String type){
        log__c log = new log__c();
        log.Push_Notification__c = parentId;
        log.Description__c = request;
        log.Response__c = response;
        log.status__c = status;
        log.Type__c = type;
        return log;
    }
}