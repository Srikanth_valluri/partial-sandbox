/************************************************************************************************
 * @Name              : DAMAC_Agents_LeadCampaignNameTest
 * @Description       : Test Class for DAMAC_Agents_LeadCampaignNamePicklist
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         18/06/2020       Created
***********************************************************************************************/

@isTest
public class DAMAC_Agents_LeadCampaignNameTest{
    
    
    @isTest
    static void testAPI(){
    
         Account acc = new Account(
                      Name = 'Test Corporate');
        insert acc;
        Contact con = new Contact(
                        FirstName = 'John',
                        LastName = 'Doe',
                        AccountId = acc.Id
                    );
        insert con;
        Campaign__c c = new Campaign__c(
                        
                        Campaign_Name__c = 'MPD.RS-AgntEvt-ZMB-Lus-Jun27.RS-AgntEvt-Wlkin-Nov20',
                        start_Date__c = system.today().addDays(-1),
                        End_Date__c = system.today().addDays(1),
                        Marketing_Start_Date__c = system.today().addDays(-1),
                        Marketing_End_Date__c = system.today().addDays(1),
                        RecordTypeID = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId(),
                        Marketing_Active__c = TRUE,
                        Credit_Control_Active__c = TRUE,
                        Sales_Admin_Active__c = TRUE,
                        Eligible_for_Agent_Portal__c =true
                        
                        );
        insert c;
       
        Assigned_Agent__c ac = new Assigned_Agent__c(
            
                               Campaign__c = c.Id,
                               User__c = UserInfo.getUserId(),
                               Agency__c = acc.Id,
                               Contact__c = con.Id
            
                             );
        insert ac;
        
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/GetCampaignNames/';
        request.httpMethod = 'GET';      
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_LeadCampaignNamePicklist.doGet();
        
        
        request.requestUri ='/GetCampaignNamesWrongURL/';
        request.httpMethod = 'GET';      
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_LeadCampaignNamePicklist.doGet();
        Test.stopTest();
    }
    @isTest
    static void testAPI2(){
		Id superUserProfileId = [select Id from Profile where name = 'Customer Community - Super User'].Id;
        Id UserRoleId = [select Id from UserRole where name = 'Chairman'].Id;
        Id RecordTyId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        
                
        User thisUser = [SELECT Id FROM User WHERE Id = : userInfo.getUserId() LIMIT 1];
        thisUser.UserRoleId = UserRoleId;
        update thisUser;          
        
   system.runAs(thisUser){       
       	Account superUserAccount = new Account(Name = 'Test Super User',  RecordTypeId = RecordTyId);
        insert superUserAccount;
        Contact superUserCon = new Contact();
        superUserCon.LastName = 'testSuperUser';
        superUserCon.AccountId = superUserAccount.Id;
        superUserCon.Email = 'superuser@test.com';
        superUserCon.Portal_Administrator__c = true;
        superUserCon.Owner__c = true;
        //superUserCon.User_Type__c =	'Active High Volume Portal';
        insert superUserCon;
        Id AccId = [select Id from Account Limit 1].Id;
        Contact ContId = [select Id from Contact where AccountId =: AccId limit 1];
        User portalUser = new User(alias = 'test457', email= 'superuser@test.com',
                                       emailencodingkey='UTF-8', lastname='User 457', languagelocalekey='en_US',
                                       localesidkey='en_US', profileid = superUserProfileId, country='United Arab Emirates',
                                       IsActive =true, ContactId = ContId.Id, timezonesidkey='America/Los_Angeles', 
                                       username = 'superuser@test.com');
       
        insert portalUser;
       
       Account acc = new Account(
                      Name = 'Test Corporate');
        insert acc;
        Contact con = new Contact(
                        FirstName = 'John',
                        LastName = 'Doe',
                        AccountId = acc.Id
                    );
        insert con;
        Campaign__c c = new Campaign__c(
                        
                        Campaign_Name__c = 'MPD.RS-AgntEvt-ZMB-Lus-Jun27.RS-AgntEvt-Wlkin-Nov20',
                        start_Date__c = date.newInstance(2010, 01, 01),
                        End_Date__c = date.newInstance(2030, 01, 01),
                        Marketing_Start_Date__c = date.newInstance(2010, 01, 01),
                        Marketing_End_Date__c = date.newInstance(2030, 01, 01),
                        RecordTypeID = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId(),
                        Marketing_Active__c = TRUE,
                        Credit_Control_Active__c = TRUE,
                        Sales_Admin_Active__c = TRUE,
                        Eligible_for_Agent_Portal__c =	TRUE            			                        
                        );
        insert c;
       
        Assigned_Agent__c ac = new Assigned_Agent__c(
            
                               Campaign__c = c.Id,
                               User__c = UserInfo.getUserId(),
                               Agency__c = acc.Id,
                               Contact__c = con.Id
            
                             );
        insert ac;
       List<Assigned_Agent__c>	acList=new	List<Assigned_Agent__c>();
       acList.add(ac);
       
       system.runAs(portalUser){   
        
           
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/GetCampaignNames/';
        request.httpMethod = 'GET';      
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_LeadCampaignNamePicklist.doGet();
        Test.stopTest();
           
        
   	}
        
}
}
}