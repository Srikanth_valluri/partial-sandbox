public class Invocable_ResetSalesAudit {
    
    @InvocableMethod
    public static void ResetSalesAudit(List<Id> stepId) {
        for(New_Step__c step : [SELECT Id, Parent_SR__c, Service_Request__c FROM New_Step__c WHERE Id IN :stepId]) {
            EvaluateCustomCode(step);
        }
    }
    
    public static String EvaluateCustomCode(New_Step__c step) {
        String sSalesAuditFlag = '';
        String stepSt = '';
        
        String retStr = 'Success';
        Id sSRId = step.Parent_SR__c;
        
        System.Debug('Step Aayo' + step.Parent_SR__c);
        
        List<Id> BookingIds= new List<id>();
        try {
            for(Booking__c Bookings :[SELECT Id FROM Booking__c WHERE Deal_SR__c =: step.Parent_SR__c]) {
                BookingIds.add(Bookings.id);
            }
            
            if(BookingIds.size() > 0) {
                system.debug('#### invoking CC_AgentRegUpdate');
                system.enqueueJob(new AsyncSalesAuditUpd(BookingIds, 'SalesAuditUpdate', sSalesAuditFlag));
            }
            
        } catch (Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
        }
        
        return retStr;
    }
}