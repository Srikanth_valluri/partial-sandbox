public without sharing class LastLoginDetailForCallingList{
    public datetime lastLogin {get;set;}
    public Id accountId {get; set;}
    public LastLoginDetailForCallingList(ApexPages.StandardController stdCon){
        lastLogin = null;
        //findLastLoginInfo(stdCon.getId());
        getDataofAccountFromCL(stdCon.getId());
    }
    public void getDataofAccountFromCL(Id callingListId){
        List<Calling_List__c> callingList = [
            SELECT Id,
                Name,
                Account__c
            FROM Calling_List__c
            WHERE Id = : callingListId
        ];
        System.debug('--callingList[0]--'+callingList[0]);
        accountId = callingList[0].Account__c;
        System.debug('--callingList[0]--accountId::'+accountId);
        findLastLoginInfo();
        
    }
    public void findLastLoginInfo(){
        list<Account> lstA = [Select Id
                                     , isPersonAccount
                                     , PersonContactId 
                              from Account 
                              where Id =: accountId
                              and isPersonAccount = true
                              and PersonContactId != null];
        system.debug('lstA*****'+lstA);
        if(!lstA.isEmpty()){
            list<User> lstUser = [SELECT LastLoginDate
                                         , ContactId 
                                  FROM User 
                                  WHERE ContactId =: lstA[0].PersonContactId];
            system.debug('lstUser*****'+lstUser);
            if(!lstUser.isEmpty()){
                lastLogin = lstUser[0].LastLoginDate;
            }
            system.debug('lastLogin*****'+lastLogin);
        }
    }
}