@isTest
private class NotificationServiceTest {

    @testSetup static void setupTestData() {
        insert new Notification_Services__c(
            Name = 'Case Open-Closed Notifications',
            Notification_Service_Class__c = 'CaseNotificationService'
        );
    }

    @isTest
    static void testService() {
        Test.startTest();
        NotificationService.Notification notification = new NotificationService.Notification();
        List<NotificationService.Notification> lstNotification = NotificationService.getNotifications(notification);
        Test.stopTest();

        System.assert(String.isBlank(notification.accountId));
        System.assert(String.isBlank(notification.recordId));
        System.assert(String.isBlank(notification.title));
        System.assert(String.isBlank(notification.status));
        System.assert(notification.isRead == NULL);
        System.assert(String.isBlank(notification.response));
        System.assert(lstNotification.isEmpty());
    }

}