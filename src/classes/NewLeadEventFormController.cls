public without sharing class NewLeadEventFormController {

    public Inquiry__c inq { get; set; }
    public List <String> lstNationality { get; set; }
    public List <String> lstMobileCountryCode { get; set; }
    public List <String> preferredLanguage { get; set; }
    public String statusMessage { get; set; } 
    public string selectedCountryCode { get; set; }
    public String selectedNationality { get; set; }
    public String selectedLanguage { get; set;}
    public string agencyName {get;set;}
    public string consultantName{get; set;}
    public String agentName { get; set;}
    public string selectedConsultantId{get; set;}
    
    public NewLeadEventFormController () {
        lstNationality = new List <String> ();
        lstMobileCountryCode = new List <String> ();
        preferredLanguage = new List <String> ();
        
        lstNationality = getDynamicPicklistValues('Inquiry__c', 'Nationality__c');
        lstMobileCountryCode = getDynamicPicklistValues('Inquiry__c', 'Mobile_CountryCode__c');
        preferredLanguage = getDynamicPicklistValues('Inquiry__c', 'Preferred_Language__c');
        
        init ();
    }
    
    public void init () {
        selectedNationality = '';
        selectedCountryCode = '';
        selectedLanguage = '';
        inq = new Inquiry__c ();
        statusMessage = '';
    }
    
    public static List<String> getDynamicPicklistValues(String ObjectApi_name,String Field_name){
        List<String> lstPickvals=new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType();
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : pick_list_values) {
            lstPickvals.add(a.getValue());
        }
        return lstPickvals;
    }
    
    public List <String> getRequiredFields () {
        List <String> fields = new List <String> ();
        fields.add ('First_Name__c');
        fields.add ('Last_Name__c');
        fields.add ('Nationality__c');
        fields.add ('Preferred_Language__c');
        fields.add ('Mobile_CountryCode__c');
        fields.add ('Mobile_Phone_Encrypt__c');
        fields.add ('Email__c'); 
        //fields.add ('Agency_Name__c'); 
        //fields.add ('Agent_Name__c');       
        return fields;
    }
    
    public static String getFieldLabel (String objectName, String fieldAPI)
    {
        String fieldLabel = '';
        SObjectType type = Schema.getGlobalDescribe().get(objectName);
        Map<String,Schema.SObjectField> mfields = type.getDescribe().fields.getMap();
        for(String strField :mfields.keySet())
        {
            SObjectField fl = mfields.get(strField);
            if (fieldAPI == fl.getDescribe().getName())
            {
                fieldLabel = fl.getDescribe().getLabel();
               
            }
        }
        return fieldLabel;
    }
    //1.2 starts
    //Remote Action to get agencies based on keyword
    @RemoteAction 
    public static List<Account> getAgencyDetails(String searchKey){
        List<Account> agencyList = new List<Account>();
        List<Id> accountIdsList = new List<Id>();
        for(Agency_PC__c thisAgency : [SELECT Id, user__c, Agency__c
                                       FROM Agency_PC__c 
                                       WHERE user__c =: UserInfo.getUserId() AND 
                                       (Agency__r.RecordTypeId =: DamacUtility.getRecordTypeId('Account', 'Individual Agency'))]){
                                           accountIdsList.add(thisAgency.Agency__c);   
                                       }
        system.debug('#### accountIdsList = '+accountIdsList);
        for(Account thisAccount : [SELECT Id, Name, RecordTypeId, RecordType.Name,
                                   (SELECT Id, Name, Org_ID_formula__c, Org_ID__c
                                    FROM Agent_Sites__r 
                                    WHERE End_Date__c = NULL) 
                                   FROM Account 
                                   WHERE Name LIKE: '%'+searchKey+'%' AND
                                   (Id IN: accountIdsList OR
                                    (RecordTypeId =: DamacUtility.getRecordTypeId('Account', 'Corporate Agency') AND 
                                     Blacklisted__c = false AND 
                                     Terminated__c = false))]){
                                         agencyList.add(thisAccount);    
                                     }
        return agencyList;
    }
    //Remote action to get agents based on the searched agency
    @RemoteAction 
    Public static List<User> getCorporateAgents(String selectedCorporateAgency){
        List<User> SuperUserList = new List<User>();
        //Id sPC = UserInfo.getUserId();
        try {
            SuperUserList = [SELECT Id, name, ContactId,Contact.Name FROM User WHERE accountId =: selectedCorporateAgency 
                            AND Profile.Name = 'Customer Community - Super User' and isActive = true];
        }
        Catch (Exception e) {

        }
        //return agentsList;
        return SuperUserList;
    }
    //1.2 ends
    public void submitInquiry(){ 
        statusMessage = '';
        System.debug('>>>>>>>>Agent Name>>>>>>>>>>'+inq.agent_Name__c);
        System.debug (inq);
        inq.Mobile_countryCode__c = selectedCountryCode;
        inq.nationality__c = selectedNationality;
        inq.Preferred_language__c = selectedLanguage;
        for (String field : getRequiredFields()) {
            System.debug(inq.get(field));
            if (inq.get (field) == null || inq.get (field) == '') {
                statusMessage = 'Required fields missing : <b>'+getFieldLabel('Inquiry__c', field)+'</b>';
                break;
            }
        }
        
        if (statusMessage == '') {
            
            List<User> agentUserList = new List<User>();
            System.debug('>>>>>>>>Agent Name>>>>>>>>>>'+inq.agent_Name__c);
            //Query if Agent Name is populated
            if(inq.agent_Name__c != null)
                agentUserList = [SELECT Id, Contactid FROM User WHERE id =: inq.agent_Name__c LIMIT 1];
            
            if (agentUserList.size() > 0) {
                //Assign CIL RecordType  
                inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByDeveloperName().get('CIL').getRecordTypeId();              
                //Add Agent User Id
                inq.OwnerId = agentUserList[0].Id;                   
                //Adding Campaign value from the custom label  
                inq.Campaign__c = label.Marketing_Campaign_Id;
                inq.agent_Name__c = agentUserList[0].ContactId;
            }
            inq.Inquiry_Source__c = 'Walk in';
            inq.Inquiry_Status__c = 'New';
            List <String> emailValues = inq.Email__c.split ('@');
            inq.Email_Id_Receptionist__c = inq.Email__c;            
            inq.Email_Id_Receptionist__c = Damac_PhoneEncrypt.encryptPhoneNumber(inq.Email__c);
            inq.Primary_Contacts__c = 'Mobile Phone';
            if(selectedConsultantId != null)
                inq.property_Consultant__c = selectedConsultantId;
            System.debug (inq);
            try {
                insert inq;
                inq = new Inquiry__c();
                statusMessage = 'Success';
            }
            catch(System.DmlException e) {
                statusMessage = e.getMessage ()+' - '+e.getLineNumber();
            }
        }
    }
   
    @RemoteAction 
    Public static List<User> getPropertyConsultant(String selectedProperty){
        
        List<User> propertyUsers = new List<User>();
        //Id sPC = UserInfo.getUserId();
        try {
            propertyUsers = [SELECT Id, name, ContactId,Contact.Name FROM User WHERE Name LIKE: '%'+selectedProperty+'%' 
                            AND Profile.Name = 'Property Consultant' and isActive = true];
        }
        Catch (Exception e) {

        }
        //return agentsList;
        return propertyUsers;
    }

}