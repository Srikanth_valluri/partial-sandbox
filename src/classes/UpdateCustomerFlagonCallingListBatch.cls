global class UpdateCustomerFlagonCallingListBatch implements Database.Batchable<sObject> {
    String query;
    public static Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
     
    global Database.QueryLocator start(Database.BatchableContext BC) {
       // Id idInst = '0017E00000e8ChR';
       // query = ' SELECT Id FROM Account where id = \''+String.escapeSingleQuotes(idInst)+'\'';
         //query = ' SELECT Id,(SELECT Name,id,Account__c,RecordTypeId,Customer_Flag__c FROM Calling_List__r WHERE IsHideFromUI__c = false AND (CreatedDate = Today OR Customer_Flag__c = True)) FROM Account';
          //query = ' SELECT Id,(SELECT Name,id,Account__c,RecordTypeId,Customer_Flag__c FROM Calling_List__r WHERE IsHideFromUI__c = false AND (CreatedDate = Today OR Customer_Flag__c = True)) FROM Account';
        
        query = 'SELECT Id FROM Account ';
        // WHERE Id = \'0010Y00000Q36S9\'
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> accLsLst) {
        
        
        Map<String,TriggerOnOffCustomSetting__c> triggerSettingMap = TriggerOnOffCustomSetting__c.getAll();
        system.debug( ' triggerSettingMap : ' + triggerSettingMap);
        TriggerOnOffCustomSetting__c callingTriggerInst = triggerSettingMap.get('CallingListTrigger');
        
        List<TriggerOnOffCustomSetting__c>updateTriggerSetting = new List<TriggerOnOffCustomSetting__c>();
        Boolean isOn = callingTriggerInst.OnOffCheck__c;
        if(isOn){
            callingTriggerInst.OnOffCheck__c = false;
            updateTriggerSetting.add(callingTriggerInst);
        }
        /*calling list trigger is disable*/
        update updateTriggerSetting;
        //system.debug( ' updateTriggerSetting : ' + updateTriggerSetting);
        //if( !isOn ) {
        //if( updateTriggerSetting[0].OnOffCheck__c == false ) {
            Id collectionCallingList = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
            Id FmCallingList = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
            Id hoCallingList = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Handover Calling List').RecordTypeId;
            Id ehoCallingList = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Early Handover Calling List').RecordTypeId;
            Id eliteCallingList = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Elite Calling List').RecordTypeId;
            set<Id> recTypeIDs = new set<Id>{collectionCallingList,FmCallingList,hoCallingList,ehoCallingList,eliteCallingList};
            Set<Id>accIdSet = new Set<Id>();
            for( Account accObj :accLsLst ){
                accIdSet.add(accObj.Id);
            }
            
            String query = 'SELECT Id,Name,Account__c,RecordTypeId,CreatedDate,IsHideFromUI__c,Customer_Flag__c,OwnerId FROM Calling_List__c WHERE Account__c IN :accIdSet AND IsHideFromUI__c = false ';
            
            if( triggerSettingMap.get('RunCustomerFlagForFMCL').OnOffCheck__c == true && triggerSettingMap.get('RunCustomerFlagForCollectionCL').OnOffCheck__c == false 
                && triggerSettingMap.get('RunCustomerFlagForHOCL').OnOffCheck__c == false && triggerSettingMap.get('RunCustomerFlagForEHOCL').OnOffCheck__c == false 
                && triggerSettingMap.get('RunCustomerFlagForEliteCL').OnOffCheck__c == false ) {
                query += 'AND RecordTypeId =: FmCallingList';
            }else if( triggerSettingMap.get('RunCustomerFlagForFMCL').OnOffCheck__c == false && triggerSettingMap.get('RunCustomerFlagForCollectionCL').OnOffCheck__c == true 
                && triggerSettingMap.get('RunCustomerFlagForHOCL').OnOffCheck__c == false && triggerSettingMap.get('RunCustomerFlagForEHOCL').OnOffCheck__c == false 
                && triggerSettingMap.get('RunCustomerFlagForEliteCL').OnOffCheck__c == false ) {
                query += 'AND RecordTypeId =: collectionCallingList';
            }else if( triggerSettingMap.get('RunCustomerFlagForFMCL').OnOffCheck__c == false && triggerSettingMap.get('RunCustomerFlagForCollectionCL').OnOffCheck__c == false 
                && triggerSettingMap.get('RunCustomerFlagForHOCL').OnOffCheck__c == true && triggerSettingMap.get('RunCustomerFlagForEHOCL').OnOffCheck__c == false 
                && triggerSettingMap.get('RunCustomerFlagForEliteCL').OnOffCheck__c == false ) {
                query += 'AND RecordTypeId =: hoCallingList';
            }else if( triggerSettingMap.get('RunCustomerFlagForFMCL').OnOffCheck__c == false && triggerSettingMap.get('RunCustomerFlagForCollectionCL').OnOffCheck__c == false 
                && triggerSettingMap.get('RunCustomerFlagForHOCL').OnOffCheck__c == false && triggerSettingMap.get('RunCustomerFlagForEHOCL').OnOffCheck__c == true 
                && triggerSettingMap.get('RunCustomerFlagForEliteCL').OnOffCheck__c == false ) {
                query += 'AND RecordTypeId =: ehoCallingList';
            }else if( triggerSettingMap.get('RunCustomerFlagForFMCL').OnOffCheck__c == false && triggerSettingMap.get('RunCustomerFlagForCollectionCL').OnOffCheck__c == false 
                && triggerSettingMap.get('RunCustomerFlagForHOCL').OnOffCheck__c == false && triggerSettingMap.get('RunCustomerFlagForEHOCL').OnOffCheck__c == false 
                && triggerSettingMap.get('RunCustomerFlagForEliteCL').OnOffCheck__c == true ) {
                query += 'AND RecordTypeId =: eliteCallingList';
            }else if( triggerSettingMap.get('RunCustomerFlagForFMCL').OnOffCheck__c == true && triggerSettingMap.get('RunCustomerFlagForCollectionCL').OnOffCheck__c == true  
                && triggerSettingMap.get('RunCustomerFlagForHOCL').OnOffCheck__c == true && triggerSettingMap.get('RunCustomerFlagForEHOCL').OnOffCheck__c == true 
                && triggerSettingMap.get('RunCustomerFlagForEliteCL').OnOffCheck__c == true ) {
                query += 'AND RecordTypeId IN :recTypeIDs ';
            }else if( triggerSettingMap.get('RunCustomerFlagForFMCL').OnOffCheck__c == false && triggerSettingMap.get('RunCustomerFlagForCollectionCL').OnOffCheck__c == true  
                && triggerSettingMap.get('RunCustomerFlagForHOCL').OnOffCheck__c == false && triggerSettingMap.get('RunCustomerFlagForEHOCL').OnOffCheck__c == false 
                && triggerSettingMap.get('RunCustomerFlagForEliteCL').OnOffCheck__c == true ) {
                query += 'AND ( RecordTypeId =: collectionCallingList OR RecordTypeId =: eliteCallingList)';
            }
            System.debug('query  ^: '+query);
            
            /*List<Calling_List__c> callingLst = [SELECT Id,
                                                       Name,
                                                       Account__c,
                                                       RecordTypeId,
                                                       CreatedDate,
                                                       IsHideFromUI__c,
                                                       Customer_Flag__c FROM Calling_List__c
                                                 WHERE Account__c IN :accIdSet
                                                   AND IsHideFromUI__c = false
                                                   AND RecordTypeId IN (SELECT Id FROM RecordType WHERE Name = 'Collections Calling List')
                                                   
                                                ];*/
            List<Calling_List__c> callingLst = Database.query(query);
            System.debug('callingLst::$%^^222222222^:'+callingLst);
             System.debug('callingLst::$%^^size^:'+callingLst.size());
            //UpdateCustomerFlagonCallingListHandler handlerObj = new UpdateCustomerFlagonCallingListHandler();
            UpdateCustomerFlagonCallingListHandler.updateFlagonCallingList(callingLst);
        //}
    }
    global void finish(Database.BatchableContext BC) {
        /*calling list trigger is enable*/
        Map<String,TriggerOnOffCustomSetting__c> triggerSettingMap = TriggerOnOffCustomSetting__c.getAll();
        TriggerOnOffCustomSetting__c callingTriggerInst = triggerSettingMap.get('CallingListTrigger');
        List<TriggerOnOffCustomSetting__c>updateTriggerSetting = new List<TriggerOnOffCustomSetting__c>();
        Boolean isOn = callingTriggerInst.OnOffCheck__c;
        if(!isOn){
            callingTriggerInst.OnOffCheck__c = true;
            updateTriggerSetting.add(callingTriggerInst);
        }
        update updateTriggerSetting;
        //System.scheduleBatch(new CalculateNumberOfUnitsForCallingLstBatch(),'Update Number Of Units '+System.Now(),15);

    }
}