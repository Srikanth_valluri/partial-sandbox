/*
* Description - Test class developed for 'SendFMSMSOnAccountCntrl'
*
*/
@isTest
public class SendFMSMSOnAccountCntrlTest {
    
    @testSetup static void setup() {
        EmailTemplate et1 = new EmailTemplate (developerName = 'test_EmailTemplateName', IsActive=true, FolderId = UserInfo.getUserId(),TemplateType= 'text', Name = 'test'); // plus any other fields that you want to set
        insert et1;

    }
    
    static testMethod void testMethod1() {
    
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;

        ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
               
        PageReference pageRef = Page.SendFMSMSOnAccount;
        pageRef.getParameters().put('Id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        SendFMSMSOnAccountCntrl obj = new SendFMSMSOnAccountCntrl(controller);
    }

    static testMethod void testMethod2() {
        
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, Name = 'Test Name' ,First_Name__c='Test FirstName', Last_Name__c='Test LastName', Type='Other');
        insert objAcc;
        
        /*Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.PersonEmail = 'a@a.com'; 
        objAcc.Mobile_Phone_Encrypt__c = '243213424'; 
        objAcc.Mobile_Phone_2__c = '242343'; 
        objAcc.Mobile_Phone_3__c = '231123'; 
        objAcc.Mobile_Phone_4__c = '24234'; 
        objAcc.Mobile_Phone_5__c = '2342345235'; 
        insert objAcc;*/

        Contact objCon = new Contact(Lastname='Test Lastname',AccountId = objAcc.Id);
        objCon.Mobile_Phone__c = '009123121323';
        objCon.Mobile_Phone_2__c = '1213322323';
        objCon.Mobile_Phone_3__c = '12132323';
        objCon.Mobile_Phone_4__c = '121324323';
        objCon.Mobile_Phone_5__c = '12123323';
         
        insert objCon;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;

        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;

        ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
               
        PageReference pageRef = Page.SendFMSMSOnAccount;
        pageRef.getParameters().put('Id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        SendFMSMSOnAccountCntrl obj = new SendFMSMSOnAccountCntrl(controller);
        List<string> lstPhnumber = new List<string>{'00971550524145'};
        
        
        obj.init();
        obj.addElements();
        obj.removeElements();
        obj.callSMSService();
        List<SelectOption> SO = obj.getMyPersonalTemplateOptions();

        EmailTemplate et = [Select Id from EmailTemplate Where developerName = 'test_EmailTemplateName' Limit 1];
        obj.accountId = objAcc.Id; 
        obj.selectedTemplateId = et.Id;
        obj.showContent();
    }
    
}