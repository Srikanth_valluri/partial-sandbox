@isTest
public class StepApprovalEmailTemplateControllerTest {
    public static List<NSIBPM__Service_Request__c> createServiceRequestList = new List<NSIBPM__Service_Request__c>();
    private static List<NSIBPM__SR_Template__c> createSrTemplateList = new List<NSIBPM__SR_Template__c>();  
    private static List<NSIBPM__Status__c> createStatus = new List<NSIBPM__Status__c>();
    private static List<NSIBPM__Transition__c> createStepTransitionList = new List<NSIBPM__Transition__c>();
    private static List<NSIBPM__Step__c> createStepList = new List<NSIBPM__Step__c>();
    private static List<Group> createGroupList = new List<Group>();
    private static List<NSIBPM__SR_Status__c> createSrStatusList = new List<NSIBPM__SR_Status__c>(); 
    private static List<User> createTestUser = new List<User>();
    
    private static void init(){
        
        createTestUser = TestDataFactory.createTestUserRecords(new List<User>{new User(Email='abcd@damac.com')});
        system.runAs(new User(Id = UserInfo.getUserId())){
            createSrTemplateList = InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
            createSrStatusList = InitialiseTestData.createSrStatusRecords(new List<NSIBPM__SR_Status__c>{new NSIBPM__SR_Status__c(Name = 'Completed', NSIBPM__Code__c = 'Completed')});
            createServiceRequestList = 
                InitialiseTestData.createtestServiceRequestRecords(
                    new List<NSIBPM__Service_Request__c>{
                        new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration'),
                                                       NSIBPM__SR_Template__c = createSrTemplateList[0].Id)});
            createServiceRequestList[0].HOS_Email__c = 'abcd@damac.com';
            update createServiceRequestList;
            createStatus = InitialiseTestData.createStatusRecords(
                new List<NSIBPM__Status__c>{
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_ADDITIONAL_INFO', Name = 'AWAITING_ADDITIONAL_INFO'),
                        new NSIBPM__Status__c(NSIBPM__Code__c = 'MORE_INFO_UPDATED', Name = 'MORE_INFO_UPDATED'),
                        new NSIBPM__Status__c(NSIBPM__Code__c = 'DOCS_SENT', Name = 'DOCS_SENT'),
                        new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_FFA_AA', Name = 'AWAITING_FFA_AA'),
                        new NSIBPM__Status__c(NSIBPM__Code__c = 'FFA_AA_UPLOADED', Name = 'FFA_AA_UPLOADED')});
            
            NSIBPM__SR_Steps__c srStep1 = new NSIBPM__SR_Steps__c();
            insert srStep1;
            
            NSIBPM__SR_Steps__c srStep2 = new NSIBPM__SR_Steps__c(Approver_Role__c='GM');
            insert srStep2;
            
            createStepList = InitialiseTestData.createTestStepRecords(
                new List<NSIBPM__Step__c>{
                    new NSIBPM__Step__c(NSIBPM__SR__c = createServiceRequestList[0].id, NSIBPM__Status__c = createStatus[0].Id, NSIBPM__SR_Step__c = srStep1.id),
                        new NSIBPM__Step__c(NSIBPM__SR__c = createServiceRequestList[0].id, NSIBPM__Status__c = createStatus[1].Id, NSIBPM__SR_Step__c = srStep2.id)});
            
            update createStepList;
            createStepTransitionList = 
                InitialiseTestData.createTransitionRecords(
                    new List<NSIBPM__Transition__c> {new NSIBPM__Transition__c(NSIBPM__From__c = createStatus[1].Id, NSIBPM__To__c = createStatus[0].Id)});
            InitialiseTestData.createStepTransitionRecords(
                new List<NSIBPM__Step_Transition__c>{
                    new NSIBPM__Step_Transition__c(NSIBPM__SR_Step__c = srStep1.Id, NSIBPM__Transition__c = createStepTransitionList[0].Id, NSIBPM__SR_Status_Internal__c = createSrStatusList[0].Id, NSIBPM__SR_Status_External__c = createSrStatusList[0].Id)});
            
        }
    }
    
    static testMethod void myUnitTest(){
        init();
        User testUser = new User();
        StepApprovalEmailTemplateController stpObj = new StepApprovalEmailTemplateController();
        stpObj.StepId = createStepList[0].Id;
        if(stpObj.step != null){
            testUser = stpObj.managerUser;
        }
    }
    
    static testMethod void myUnitTest1(){
        init();
        User testUser = new User();
        StepApprovalEmailTemplateController stpObj = new StepApprovalEmailTemplateController();      
        stpObj.ManagerEmail = null;
        stpObj.StepId = null;
        NSIBPM__Step__c stp = stpObj.step;
        User user1 = stpObj.managerUser;
    }
    
    static testMethod void myUnitTest2(){
        init();
        User testUser = new User();
        StepApprovalEmailTemplateController stpObj = new StepApprovalEmailTemplateController();
        stpObj.StepId = createStepList[1].Id;
        if(stpObj.step != null){
            testUser = stpObj.managerUser;
        }
    }
}