/*
* Description - Test class developed for 'InlineBookingUnitExtension'
*
* Version            Date            Author            Description
* 1.0                14/12/17        Monali            Initial Draft
*/
@isTest
private class GeneratePCCTest {
    static testMethod void testMethod1() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Type = 'Change of Contact Details';
        objCase.IsPOA__c = false;
        objCase.OCR_verified__c = true;
        objCase.OQOOD_Fee_Applicable__c = false;
        objCase.Approval_Status__c = 'Approved' ;
        objCase.Status = 'Working';
        insert objCase;

        Task objTask = new Task();
        objTask.WhatId = objCase.Id;
        objTask.Process_Name__c = 'Early Handover';
        objTask.Status = 'Not Started';
        objTask.Subject = 'Generate PCC documents and Release it to customer';
        insert objTask;
        Test.startTest();
        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());

        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverMQServices.generatePCCResponse_element>();
        HandoverMQServices.generatePCCResponse_element response1 = new HandoverMQServices.generatePCCResponse_element();
        response1.return_x = '{"data":[{"PROC_STATUS":"E","PROC_MESSAGE":"[EX_ERROR][PCC-DP] Mesg=Invoices/Debit Memo/Credit Memo remain open with the customer","ATTRIBUTE3":null,"ATTRIBUTE2":null,"ATTRIBUTE1":null,"PARAM_ID":"45253"}],"message":"Service Process Completed...","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );


        PageReference pageRef = Page.GeneratePCC;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GeneratePCC controller = new GeneratePCC(sc);
        pageRef = controller.init();
        Test.stopTest();

    }
}