@istest
public class DAMAC_EditDetailsController_Test {
    static Email_metrics__c metrics;
    static void init () {
        Sendgrid_Credentials__c credentials = new Sendgrid_Credentials__c ();
        credentials.Endpoint_URL__c = 'test.com';
        credentials.API_Key__c = 'testcom';
        credentials.From_name__c = 'test';
        credentials.From_Email__c = 'test@test.com';
        insert credentials;
        
        Inquiry__c newInquiry = new Inquiry__c();
        newInquiry.First_Name__c = 'Testsearch';
        newInquiry.Last_Name__c = 'Inquiry';
        newInquiry.Mobile_Phone_Encrypt__c = '05789088';
        newInquiry.Mobile_Phone__c = '05789088';
        newInquiry.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        newInquiry.Email__c = 'test.lastname@eternussolutions.com';
        newInquiry.Preferred_Language__c = 'English';
        newInquiry.Inquiry_Source__c = 'Social' ;
        newInquiry.Inquiry_Status__c = 'New' ;
        insert newInquiry;
        
        Email_Request__c req = new Email_Request__c ();
        req.Template_for__c = 'Sales Offer Inquiry';
        req.Email_template__c = 'b70c2c19-b7d8-4eb4-acd1-e3f0883f438b';
        req.Substitution_tags__c = '-Name-';
        insert req;
        
        metrics = new Email_Metrics__c ();
        metrics.Email_Request__c = req.id;
        metrics.SObject_ID__c = newInquiry.id;
        metrics.Inquiry__c = newInquiry.id;
        Damac_Constants.skip_EmailMetricsTrigger = true;
        insert metrics;
    }
    
    static testmethod void callMethods() {
        init ();
        apexPages.currentPage().getParameters().put('id', UtilityHelperCls.encryptMobile(metrics.id));
        DAMAC_EditDetailsController obj = new DAMAC_EditDetailsController ();
        obj.checkConfirm ();
        obj.checkForm ();
        obj.save ();
    }
    
    static testmethod void callMethods2() {
        init ();
        apexPages.currentPage().getParameters().put('id', UtilityHelperCls.encryptMobile(metrics.id));
        DAMAC_EditDetailsController obj = new DAMAC_EditDetailsController ();
        obj.checkForm ();
        obj.checkConfirm ();
        
        obj.save ();
    }
    static testmethod void callMethodsFail () {
        init ();
        apexPages.currentPage().getParameters().put('id', UtilityHelperCls.encryptMobile(metrics.id));
        DAMAC_EditDetailsController obj = new DAMAC_EditDetailsController ();
        obj.metrics.Inquiry_Email__c = 'test';
        obj.checkConfirm ();
        obj.checkForm ();
        obj.save ();
    
    }
}