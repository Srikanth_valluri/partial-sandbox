/* * * * * * * * * * * * * *
*  Class Name:   CancelPromotionSRTest
*  Purpose:      Unit test class for CancelPromotionSR
*  Author:       Hardik Mehta - ESPL
*  Company:      ESPL
*  Created Date: 11-Feb-2018
*  Type:         Test Class
* * * * * * * * * * * * */
@isTest
public with sharing class CancelPromotionSRTest 
{
  
  public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';
  
    /* * * * * * * * * * * * *
    *  Method Name:  testCase
    *  Purpose:      This method is used to check the functionality for cancelCase method
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-Feb-2017
    * * * * * * * * * * * * */  
    public static testmethod void testCase()
    {
         List<Booking__c> bookingList = new List<Booking__c>();
         List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
         List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
    
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id promotionRecordTypeID = getRecordTypeIdForPromotion();
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , promotionRecordTypeID);
        objCase.Status = 'Submitted';
        insert objCase;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
        }
        system.debug('>>>>>'+bookingUnitList);
        insert bookingUnitList;

        Task taskObj = new Task();
        taskObj.Status = 'In Progress';
        taskObj.Subject = 'Manager Approval Pending';
        taskObj.WhatId = objCase.id;
        insert taskObj;

        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        CancelPromotionSR  objCancel = new CancelPromotionSR(stdController);
        objCancel.caseId = objCase.Id;
        objCancel.cancelCase();

        objCase.Status = 'Closed';
        update objCase;
        objCancel.cancelCase();

        objCancel.caseId = '5007E000006A45LQAS';
        objCancel.cancelCase();
    
    }

  /* * * * * * * * * * * * *
  *  Method Name:  getRecordTypeIdForPromotion
  *  Purpose:      This method is used to get record type ID for Promotions
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 11-Feb-2018
  * * * * * * * * * * * * */
  private static Id getRecordTypeIdForPromotion()
  {
    Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
    Id promotionRecordTypeID = caseRecordTypes.get('Promotions').getRecordTypeId();
    return promotionRecordTypeID;
  }

}