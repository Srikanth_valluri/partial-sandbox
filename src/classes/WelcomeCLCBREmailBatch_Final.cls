/**
 * @File Name          : WelcomeCLCBREmailBatch_Final.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10/28/2019, 3:04:53 PM
 * @Modification Log   : 
 * Ver       Date            Author              Modification
 * 1.0    10/21/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
global class WelcomeCLCBREmailBatch_Final implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

    public static final String STR_ADDRESS = 'no-replysf@damacgroup.com';
    
    String listQuery;

    public WelcomeCLCBREmailBatch_Final(){

    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
    
        Id recordTypeIdWlc = [SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Welcome_Calling_List'
            AND IsActive = TRUE LIMIT 1].Id;
            
        listQuery=   'SELECT Id,'+
                         ' Name,'+
                         ' RecordTypeId,'+
                         ' CBR_Assignment_Date__c,'+
                         ' Unit_Name__c,'+                         
                         ' Notification_Time_for_outcome__c,Calling_List_Type__c,'+
                         ' IsHideFromUI__c,'+
                         ' Customer_Name__c,'+ 
                         ' OwnerId,'+
                         ' Welcome_Call_Outcome__c,'+
                         ' Account__c,'+
                         ' Account__r.Name,'+
                         ' Registration_ID__c'+
                   ' FROM Calling_List__c'+
                   ' WHERE CBR_Assignment_Date__c!= null'+ ' AND RecordTypeId = \''+recordTypeIdWlc+ '\'' + 
                   ' AND Calling_List_Type__c !=\'DP-Achraf\'' +
                   ' AND Calling_List_Status__c !=\'Closed\'' +
                   ' AND Welcome_Call_Outcome__c = null';
        System.debug('-->> listQuery: '+ listQuery);           
        return Database.getQueryLocator(listQuery);
    }
    global void execute(Database.BatchableContext BC, List<Calling_List__c> callingList) {
        System.debug('callingList:***'+callingList);
        if(System.Label.Bypass_WelcomeCLCBREmailBatchLogic == 'N'){

            List<User> ownerEmailLst = [
                SELECT Id
                    , Email
                    , FirstName
                    , LastName
                FROM User 
                WHERE Id =:callingList[0].OwnerId
            ];
            System.debug('-->> ownerEmailLst: '+ ownerEmailLst);                       
            for(Calling_List__c callObj  : callingList){
                System.debug('-->> Date.valueOf callObj.CBR_Assignment_Date: '+ Date.valueOf(callObj.CBR_Assignment_Date__c));
                System.debug('-->> System.Today(): '+ System.Today());
                DateTime dT1 = System.now();
                Date myDate1 = date.newinstance(dT1.year(), dT1.month(), dT1.day());
                DateTime dT2 = callObj.CBR_Assignment_Date__c;
                Date myDate2 = date.newinstance(dT2.year(), dT2.month(), dT2.day());
                Integer numberOfDays = (myDate2).daysBetween(myDate1);
                System.debug('-->> numberOfDays: '+ numberOfDays);
                if(numberOfDays == 1){
                    System.debug('-->> Inside If: ');
                    creatEmailMessage(callObj, ownerEmailLst, false);
                } else if (numberOfDays >= 2 && callObj.Welcome_Call_Outcome__c == NULL) {
                    System.debug('-->> Else If: ');
                    creatEmailMessage(callObj, ownerEmailLst, true);
                }
            }
        }
        
        
    }
    global void finish(Database.BatchableContext BC) {
    }

    public static void creatEmailMessage(Calling_List__c callObj, List<User> ownerEmailLst, Boolean addCCAddress){
        String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue = '',contentBody = '';
        String subject, strAccountId, strRelatedToId, strDPIId, bccAddress ='', strCCAddress;
   
        fromAddress = STR_ADDRESS;

        bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
        contentType = 'text/html';
        strCCAddress = '';
        
        
        toAddress = ownerEmailLst[0].Email;
        String bodyStr = 'Hi, <br /><br />';

        bodyStr += 'This is to notify you that the callback date for ';
        bodyStr += callObj.Customer_Name__c + ' is Today.';
        
        strAccountId = '';
        strRelatedToId = callObj.Id;

        if(addCCAddress){
            if(System.Label.WelcomeCLCBREmailBatchCCUser != Null){
                strCCAddress = System.Label.WelcomeCLCBREmailBatchCCUser;
            }
            subject = 'Alert! Welcome call overdue -'+callObj.Name;
            bodyStr += '<br /> This is to notify you that you have missed the SLA of calling within same day for the welcome call for '+ callObj.Account__r.Name +'.';
        } else {
            subject = 'Alert! No action taken - Welcome call  – 1 day old -'+callObj.Name;
            bodyStr += '<br /> This is to notify you that you have not taken action on the welcome call for '+ callObj.Account__r.Name +' for the last 2 days.';
        }

        bodyStr += '<br /> Please update the task immediately.';
        bodyStr += '<br /> Calling List Number: '+ callObj.Name;
        bodyStr += '<br /> Registration ID: '+ callObj.Registration_ID__c;
        if(String.isNotBlank(callObj.Unit_Name__c)){
            bodyStr += '<br /> Unit Name: '+callObj.Unit_Name__c;
        }
                
        bodyStr += '<br /> <br /> Regards, <br /> Salesforce Team';

        callSendGridFromBatch(toAddress, strCCAddress, bccAddress, subject, fromAddress, replyToAddress, contentType, bodyStr, strAccountId, bodyStr, strRelatedToId);
    }

    public static void callSendGridFromBatch(String toAddress, String strCCAddress, String bccAddress, String subject, String fromAddress, String replyToAddress, String contentType, String contentValue, String strAccountId, String contentBody, String strRelatedToId){
        
        List<EmailMessage> lstEmails = new List<EmailMessage>();
        List<Attachment> lstAttach = new List<Attachment>();
        System.debug('-->> lstAttach: ' + lstAttach);
        // Callout to sendgrid to send an email
        SendGridEmailService.SendGridResponse objSendGridResponse =
            SendGridEmailService.sendEmailService(
                toAddress
                , ''
                , strCCAddress
                , ''
                , bccAddress
                , ''
                , subject
                , ''
                , fromAddress
                , ''
                , replyToAddress
                , ''
                , contentType
                , contentValue
                , ''
                , lstAttach
            );

            System.debug('-->> objSendGridResponse: ' + objSendGridResponse);
            
            if (objSendGridResponse.ResponseStatus == 'Accepted') {
                EmailMessage mail = new EmailMessage();
                mail.Subject = subject;
                mail.MessageDate = System.Today();
                mail.Status = '3';//'Sent';
                mail.RelatedToId = strRelatedToId;
                // mail.Account__c  = strAccountId;
                mail.Type__c = 'Reminder Email';
                mail.ToAddress = toAddress;
                mail.FromAddress = fromAddress;
                mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                mail.Sent_By_Sendgrid__c = true;
                mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                mail.CcAddress = strCCAddress;
                mail.BccAddress = bccAddress;
                lstEmails.add(mail);
                system.debug('Mail obj == ' + mail);
            }
        system.debug('lstEmails = '+ lstEmails );
        if( lstEmails != null && lstEmails.size() > 0 ) {
            insert lstEmails;
        }
    }

}