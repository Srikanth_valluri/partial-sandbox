/**********************************************************************************************
 * @Name              : DAMAC_DealExceptionRequest_Test
 * @Description       : Test Class for DAMAC_DealExceptionRequestController
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.1         QBurst         29/06/2020        Created
***********************************************************************************************/
@istest
public with sharing class DAMAC_DealExceptionRequest_Test {


    static testmethod void DAMAC_DealExceptionRequestController_Test_M4(){
        Account acc = new Account ();
        acc.LastName = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '767676';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        insert acc;
        
        DAMAC_Central_Push_Notifications__c obj = new DAMAC_Central_Push_Notifications__c();
        obj.Email__c = 'test@test.com';
        obj.Password__c = '1232112';
        obj.device_source__c = 'test';
        obj.device_os_version__c = 'test';
        obj.app_version__c = '1.2';
        obj.device_model__c = 'test';
        obj.Api_Token__c = 'test';
        obj.Project_connect_API_Token__c = 'test';
        obj.app_id__c = 1;
        obj.is_authorization_required__c = false;
        insert obj;
        
        List<Checklist_Items__c> items = new List<Checklist_Items__c>();
        Checklist_Items__c item = new Checklist_Items__c();
        item.Type__c = 'Deal Exception Request';
        item.Value__c = 'Discount';
        item.Name = 'Test';
        item.Priority__c = 1;
        items.add(item);
        insert items;
        
        Unit_Assignment__c uAObj = new Unit_Assignment__c();
         uAObj.Start_Date__c = Date.parse('11/12/17');
         uAObj.End_Date__c =  Date.parse('11/12/18');
         uAObj.Unit_Assignment_Name__c = 'Test US1';
         uAObj.Reason_For_Unit_Assignment__c = 'Test reason';
         uAObj.Active__c = false;
         insert uAObj;

        Agent_Site__c agency = new Agent_Site__c();
        agency.Name = 'UAE';
        agency.Agency__c = acc.id;
        insert agency;
        
        Deal_Exception_Request__c der = new Deal_Exception_Request__c();
        der.Token_Amount_AED__c = 40000;
        der.agency__c = acc.Id;
        der.Token_Transfer__c = 'Yes';
        der.RecordTypeId = Schema.SObjectType.Deal_Exception_Request__c.getRecordTypeInfosByName().get('Deal Exception').getRecordTypeId();
        der.Booking_Units__c = 'BU-000117,BU-000118';
        insert der;

        Inquiry__c inq = new Inquiry__c ();
        inq.Activity_Counter__c =101;
        inq.Inquiry_Status__c='Active';
        inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        inq.ownerId = userinfo.getUserId();
        inq.Promoter_Name__c = 'Test';
        inq.Telesales_Executive__c = UserInfo.getUserId();
        insert inq;
        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        loc.property_id__c = '123';
        insert loc;

        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c = 1;
        newProperty.Property_Code__c = 'VIR';
        newProperty.Property_Name__c = 'VIRIDIS @ AKOYA OXYGEN';
        newProperty.District__c = 'AL YUFRAH 2';
        newProperty.AR_Transaction_Type__c = 'INV VIR';
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR';
        newProperty.Brokerage_Distribution_Set__c = '11600';
        newProperty.Sales_Commission_Dist_Set__c = '11601';
        newProperty.Currency_Of_Sale__c = 'AED';
        newProperty.Signature_Col_Customer_Stmt__c = 'Front Line Investment Management Co. LLC';
        newProperty.EOI_Enabled__c = true;
        insert newProperty;

        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.Id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.Id;
        lstInv[0].building_location__c = loc.Id;
        lstInv[0].property_id__c = newProperty.Id;
        lstInv[0].Unit_Assignment__c = uAObj.Id;
        lstInv[0].Tagged_To_Unit_Assignment__c = true;
        insert lstInv;

        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account a = new Account();
        a.LastName= 'Test Account';
        a.RecordTypeId = accountRTId;
        insert a;

        Currency_Rate__c rate = new Currency_Rate__c();
        rate.To_Currency__c = 'AED';
        rate.Conversion_Date__c = system.today();
        rate.conversion_rate__c = 1;
        rate.From_Currency__c = 'AED';
        insert rate;
        
         Sales_Margins__c salesMargin = new Sales_Margins__c();
        salesMargin.Marketing_Name__c = 'BAIT AL ASEEL';
        salesMargin.Bedroom_Type__c = '3';
        salesMargin.Building_Location__c = 'VCT';
        salesMargin.ACD__c = '2021-12-30';
        salesMargin.Property_Name__c = 'VICTORIA @AKOYA OXYGEN';
        salesMargin.View_Type__c = 'Back';
        salesMargin.Is_Active__c = true;
        salesMargin.Avg_System_PSF__c = 881.19;
        salesMargin.Floor_Location__c = 'VCT';
        salesMargin.Apply_Agent_Discount__c = true;
        insert salesMargin;
        

        Inventory__c invent = new Inventory__c();
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Sales_Margins__c = salesMargin.Id;
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1';
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.Id;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Released';
        invent.special_price__c = 1000000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234'; 
        invent.Floor_Package_ID__c = ''; 
        invent.building_location__c = loc.Id;
        invent.property_id__c = newProperty.Id;
        invent.Unit_Assignment__c = uAObj.Id;
        invent.Tagged_To_Unit_Assignment__c = true;
        invent.Unit_Categorization__c = 'Villa';
        insert invent;

        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today(); 
        pp.Building_Location__c =  loc.Id;
        pp.term_id__c = '1234';
        insert pp;

        List<Payment_Terms__c> ptList = new List<Payment_Terms__c>();
        Payment_Terms__c pt = new Payment_Terms__c();
        pt.Payment_Plan__c = pp.Id;
        pt.Percent_Value__c = '5';
        pt.Installment__c = 'I001';
        ptList.add(pt);
        Payment_Terms__c pt2 = new Payment_Terms__c();
        pt2.Payment_Plan__c = pp.Id;
        pt2.Percent_Value__c = '5';
        pt2.Installment__c = 'I002';
        ptList.add(pt2);
        Payment_Terms__c pt3 = new Payment_Terms__c();
        pt3.Payment_Plan__c = pp.Id;
        pt3.Percent_Value__c = '5';
        pT3.Installment__c = 'I003';
        ptList.add(pt3);
        Payment_Terms__c pt4 = new Payment_Terms__c();
        pt4.Payment_Plan__c = pp.Id;
        pt4.Percent_Value__c = '5';
        pt4.Installment__c = 'I004';
        ptList.add(pt4);
        insert ptList;
        
       
        Deal_Exception_Unit__c deUnit = new Deal_Exception_Unit__c();
        deUnit.Deal_Exception_Request__c = der.Id;
        deUnit.Inventory__c = invent.Id;
        deUnit.payment_plan__c = pp.Id;
        deUnit.Sales_Margins__c = salesMargin.Id;
        deUnit.Proposed_Payment_Plan__c = pp.Id;
        deUnit.Project_Cost_per_SQFT__c = 25000;
        deUnit.Area_SFT__c = 1;
        insert  deUnit;
        
        Unit_Documents__c testUnitDocuments = new Unit_Documents__c();
        testUnitDocuments.Deal_Exception_Request__c = der.Id;
        testUnitDocuments.Document_Name__c = 'Passport Copy';
        insert testUnitDocuments;
        
        Attachment objAttachment = new Attachment();
        objAttachment.Name = 'Test Document';
        objAttachment.Body = blob.valueOf('test');
        objAttachment.ContentType='image/png';
        objAttachment.ParentId = testUnitDocuments.id;
        insert objAttachment;

        user rmUser = new User ();
        rmUser.ProfileId = [SELECT Id FROM Profile WHERE Name LIKE '%Property Consultant%' LIMIT 1].Id;
        rmUser.UserName = 'testEmail@testclass.com';
        rmUser.Email = 'testEmail@testclass.com';
        rmUser.Alias = 'tCE';
        rmUser.EmailEncodingKey = 'UTF-8';
        rmUser.LastName = 'Testing';
        rmUser.LanguageLocaleKey = 'en_US';
        rmUser.LocaleSidKey = 'en_US';
        rmUser.TimeZoneSidKey = 'America/Los_Angeles';
        insert rmUser;

        apexpages.currentpage().getparameters().put('inventoryId', invent.Id);
        apexpages.currentpage().getparameters().put('id', der.Id);
        PageReference pageRef = Page.DealExceptionRequest_Create;
        pageRef.getParameters().put('id', String.valueOf(der.Id));
        pageRef.getparameters().put('inventoryId', invent.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(der);

        test.starttest();
        List <payment_terms__c> terms = new List <payment_terms__c> ();
        terms.add (pt);
        DAMAC_DealExceptionRequestController cls = new DAMAC_DealExceptionRequestController(sc);
        cls.tempPaymentTerms = terms;
        cls.derRecordId = der.Id;
        cls.inventoryId = invent.Id;
        cls.inquirySelected = inq.Id;
        cls.accountSelected = acc.Id;
        cls.searchCrNumber = 'test';
        cls.searchCrRegistrationExpiryDate = system.today().format();
        cls.searchOrganizationName = 'test';
        cls.agency = 'test';
        cls.agentContact = 'test';
        cls.inquiry = 'test';
        cls.Accounttosearch = 'test';
        cls.isSuccess = true;
        cls.startOver();
        cls.onPageLoad();
        cls.PaymentPlanID = pp.Id;
        cls.getPaymentTerms ();
     //   cls.updatePaymentTerms ();
        cls.structurecreated = true;
        cls.paymentPlanMap.put (pp.Id, pp); 

        
        apexpages.currentpage().getparameters().put ('selectedId', pp.Id); 
        cls.getPaymentPlan (pp.Id);
        cls.getPaymentTerms ();  
        cls.saveDER();
        cls.copyinquiryToDER();
        cls.copyAccountToDER();
        
        cls.currentDEUId = deUnit.Id;
        cls.tempPaymentTerms = ptList;
        
        cls.bookingUnitList.add('BU-000117'); 
        cls.bookingUnitList.add('BU-000119'); 
        cls.bookingUnitList.add('BU-000120'); 
       // cls.deleteTempPaymentTerm();
        cls.updatePaymentTerms();
        cls.addNewPaymentTerm();
        
        apexpages.currentpage().getparameters().put ('newBU', 'BU-000117'); 
        apexpages.currentpage().getparameters().put ('buRemoveDetails', 'BU-000117'); 
        apexpages.currentpage().getparameters().put ('termRowNo', '0'); 
       
        
        
       // DAMAC_DealExceptionRequestController.doUploadAttachment('unitDocId', 'attachmentId', 'description');
        cls.updateBUValues('BU-000118,BU-000119');
        cls.applyProposedPlanToAll();
        cls.updateBookingUnit();
        cls.removeBookingUnit();
        cls.removeFromString('BU-000119');
        DAMAC_DealExceptionRequestController.doUploadAttachment(testUnitDocuments.Id, objAttachment.Id,  'description');
        cls.checkProofOfPayment();
        apexpages.currentpage().getparameters().put ('proofOfPayment', 'Yes'); 
        DAMAC_DealExceptionRequestController.doUploadAttachment(testUnitDocuments.Id, objAttachment.Id,  'description');
        cls.checkProofOfPayment();
        Unit_Documents__c testUnitDocuments2 = new Unit_Documents__c();
        testUnitDocuments2.Deal_Exception_Request__c = der.Id;
        testUnitDocuments2.Document_Name__c = 'Proof of Payment';
        insert testUnitDocuments2;
        cls.checkProofOfPayment();
        cls.submitDER();
        cls.reopenRecord();
        cls.deleteTempPaymentTerm();
        cls.getInstalDesc(12);
        cls.fetchCheckList();
        cls.updateChecklist();
        cls.reassignRequest();
        cls.approveForwardRequest();
        cls.approveRequest();
        cls.rejectDER();
        List<Id> derIdList = new List<Id>();
        derIdList.add(der.Id);
        DealExceptionTriggerHandler.sendDERApprovalNotification(derIdList);
        test.stoptest();
    }
}