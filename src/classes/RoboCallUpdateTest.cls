@isTest
public with sharing class RoboCallUpdateTest {

    @IsTest
    static void methodName1(){

        Robocall_Campaign__c rc =  new Robocall_Campaign__c();
        insert rc;
         
        Added_Call__c ac = new Added_Call__c( Response_Captured__c = true
                    , Recording_URL__c = 'www.google.com'
                    , Call_Duration__c='9'
                    , Hang__c = '900'
                    , Status__c = 'Connected'
                    , Pick_Up_Date__c = System.today()
                    , Start_Date__c = System.today()
                    , Call_Date__c = System.today());
        ac.Campaign_Robo__c = rc.id;
        insert ac;

        Test.startTest();
        RoboCallUpdate.RoboCallUpdate('2019-10-10','www.google.com','Visitor','2019-10-10','2019-10-10',
        ac.Id,'263635','Closed');
        
        Test.stopTest();
    }
    
    @IsTest
    static void methodName2(){

        Robocall_Campaign__c rc =  new Robocall_Campaign__c();
        insert rc;
         
        Added_Call__c ac = new Added_Call__c();
        ac.Campaign_Robo__c = rc.id;
        ac.Response_Captured__c = True;
        insert ac;

        Test.startTest();
        RoboCallUpdate.RoboCallUpdate('2019-10-10','www.google.com','Visitor','2019-10-10','2019-10-10',
        ac.Id,'263635','Connected');
        
        Test.stopTest();   
    }
}