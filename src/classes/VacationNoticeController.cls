public virtual without sharing class VacationNoticeController {
    public list<SelectOption> lstUnits { get; set; }

    public map<String, list<FM_Additional_Detail__c>> mapAdditionalDetails { get; set; }

    public Booking_Unit__c objUnit { get; set; }
    public FM_Case__c objFMCase { get; set; }
    public string strFMCaseId{get;set;}
    public String strSRType { get; set; }
    public Boolean isEdittable{get;set;}
    public String strSelectedUnit { get; set; }
    public String strDetailType { get; set; }
    public Integer indexOfNewChildToRemove { get; set; }
    public Boolean isMandatory{get;set;}
    public list<FM_Documents__mdt> lstDocuments { get; set; }
    public list<SR_Attachments__c> lstRecentAttachment { get; set; }
    public list<SR_Attachments__c> lstUploadedDocs { get; set; }

    @testVisible protected Map<Id, Booking_Unit__c> mapAllUnits ;
    @testVisible protected String strAccountId ;
    @testVisible protected Map<String, FM_Documents__mdt> mapProcessDocuments ;
    @testVisible protected Map<String, SR_Attachments__c> mapUploadedDocs ;

    public transient String strDocumentBody { get; set; }
    public transient String strDocumentName { get; set; }
    public transient String deleteAttRecId { get; set; }

    public VacationNoticeController() {
         isEdittable=true;
         strFMCaseId = ApexPages.currentPage().getParameters().get('Id');
         isMandatory=false;
         if(String.isBlank( strFMCaseId )){
            strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
            strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
            strSRType = ApexPages.currentPage().getParameters().get('SRType');
         }
        system.debug('== strAccountId =='+strAccountId );
        system.debug('==*****strSRType*****=='+strSRType);
        if(String.isNotBlank(strSelectedUnit)
           || String.isNotBlank(strFMCaseId)){
            init();
        }
        
    }

    protected VacationNoticeController(Boolean shouldCall) {}

    public void init() {
        fetchUnitDetails();
    }

    public void fetchUnitDetails() {
        initializeFMCase();
        System.debug('objFMCase.Submitted__c======'+objFMCase.Submitted__c);
        if(objFMCase.Submitted__c==true){
            isEdittable=false;
        }
        System.debug('isEdittable======'+isEdittable);
        if(strFMCaseId == null){
            //Initialize the Additional details map.
            List<FM_Additional_Detail__c> lstExistingAdditionalDetail =[select Id
                                                                             , Name__c
                                                                             , Relationship__c
                                                                             , Phone__c
                                                                             , Email__c
                                                                             , Emergency_Contact_Case__c
                                                                          From FM_Additional_Detail__c
                                                                          Where Emergency_Contact_Account__c =: strAccountId ];
                                                                        
            if(!lstExistingAdditionalDetail.isEmpty()){
                mapAdditionalDetails = new map<String, list<FM_Additional_Detail__c>>();
                mapAdditionalDetails.put('Emergency Contact',lstExistingAdditionalDetail);
            }else{
                mapAdditionalDetails = new map<String, list<FM_Additional_Detail__c>>();
                FM_Additional_Detail__c objDetail = new FM_Additional_Detail__c();
                objDetail.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
                objDetail.Emergency_Contact_Case__c = objFMCase.Id ;
                objDetail.Emergency_Contact_Account__c = objFMCase.Account__c;
                mapAdditionalDetails.put( 'Emergency Contact' , new list<FM_Additional_Detail__c> { objDetail } ) ;
            }
        }else{
            system.debug('*****entered else*****');
            strSelectedUnit = objFMCase.Booking_Unit__c;
            System.debug('strSelectedUnit======'+strSelectedUnit);
        }
        objUnit = getUnitDetails(strSelectedUnit) ;
        System.debug('objUnit===='+objUnit);
        System.debug('strSRType===='+strSRType);
        if( String.isNotBlank( strSRType ) && objUnit != NULL ) {
            processDocuments();
        }
        System.debug('=====lstDocuments===='+lstDocuments);
    }

    public static Booking_Unit__c getUnitDetails( String strUnitId ) {
        System.debug('strUnitId======='+strUnitId);
        if (String.isBlank(strUnitId)) {
            return NULL;
        }
        return  FM_Utility.getUnitDetails(strUnitId);
    }

    private void initializeFMCase() {
        system.debug('strFMCaseId*****'+strFMCaseId);
        if(strFMCaseId == null){
            objFMCase = new FM_Case__c();
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Vacation Notice').getRecordTypeId();
            objFMCase.Account__c = strAccountId ;
            objFMCase.Booking_Unit__c = strSelectedUnit ;
            objFMCase.Status__c = 'New';
            objFMCase.Request_Type__c = 'Vacation Notice';
            objFMCase.Request_Type_DeveloperName__c = strSRType;
            
            /*FM_Additional_Detail__c objDetailInit = new FM_Additional_Detail__c();
            objDetailInit.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
            objDetail.Emergency_Contact_Account__c = objFMCase.Account__c;                                                            
            mapAdditionalDetails.put( 'Emergency Contact' , new list<FM_Additional_Detail__c> { objDetail } ) ;*/
        }else{
            system.debug('Inside existing case*****');
            objFMCase = FM_Utility.getCaseDetails(strFMCaseId);
            strAccountId = objFMCase.Account__c ;
            strSelectedUnit = objFMCase.Booking_Unit__c ;
            strSRType = objFMCase.Request_Type_DeveloperName__c ;
            list<FM_Additional_Detail__c> lstAD = [Select Id
                                                        , Name__c
                                                        , Relationship__c
                                                        , Phone__c
                                                        , Email__c
                                                        , Emergency_Contact_Case__c
                                                   From FM_Additional_Detail__c
                                                   Where Emergency_Contact_Case__c =: strFMCaseId];
            if(!lstAD.isEmpty()){
                mapAdditionalDetails = new map<String, list<FM_Additional_Detail__c>>();
                mapAdditionalDetails.put('Emergency Contact',lstAD);
            }else{
               mapAdditionalDetails = new map<String, list<FM_Additional_Detail__c>>();

            FM_Additional_Detail__c objDetail = new FM_Additional_Detail__c();
            objDetail.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
            objDetail.Emergency_Contact_Case__c = objFMCase.Id ;
            objDetail.Emergency_Contact_Account__c = objFMCase.Account__c;
            mapAdditionalDetails.put( 'Emergency Contact' , new list<FM_Additional_Detail__c> { objDetail } ) ;
            }
        }
    }

    public void addDetails() {
        if( String.isNotBlank( strDetailType ) && mapAdditionalDetails != NULL &&
            mapAdditionalDetails.containsKey( strDetailType ) ) {
            System.debug('>>>>>mapAdditionalDetails>>>>.'+mapAdditionalDetails);
            FM_Additional_Detail__c objDetail = new FM_Additional_Detail__c();
            objDetail.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
            objDetail.Emergency_Contact_Case__c = objFMCase.Id ;
            objDetail.Emergency_Contact_Account__c = objFMCase.Account__c;
            mapAdditionalDetails.get( strDetailType ).add( objDetail ) ;
        }
    }

    public void removeDetails() {
        if( String.isNotBlank( strDetailType ) && mapAdditionalDetails != NULL &&
            mapAdditionalDetails.containsKey( strDetailType ) ) {
            //mapAdditionalDetails.get( strDetailType ).remove( mapAdditionalDetails.get( strDetailType ).size() - 1 );
            list<FM_Additional_Detail__c> lstDetails = mapAdditionalDetails.get( strDetailType ) ;
            if( lstDetails.size() > 1 ) {
                mapAdditionalDetails.get( strDetailType ).remove( indexOfNewChildToRemove );
            }
        }
    }

    public PageReference createVacationNoticeCase() {
        if( objFMCase != NULL ) {
            createMethod();
            PageReference objPage = new PageReference('/' + objFMCase.Id );
            return objPage ;
        }
        return NULL ;
    }

    public void createCaseShowUploadDoc(){
        createMethod();
        System.debug('objFMCase-----'+objFMCase);
        System.debug('oBjFMCase.id------'+oBjFMCase.id);
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            processDocuments();
        }
        strFMCaseId=objFMCase.id;
        System.debug('strFMCaseId----'+strFMCaseId);

    }

    protected virtual void setCaseOrigin() {
        objFMCase.Origin__c='Walk-In';
    }

    public void createMethod(){
            setCaseOrigin();
            upsert objFMCase ;
            list<FM_Additional_Detail__c> lstAdditionalDetails = new list<FM_Additional_Detail__c>();
             for( FM_Additional_Detail__c objDet : mapAdditionalDetails.get( 'Emergency Contact' ) ) {
                System.debug('>>>>>objDet>>>>.'+objDet);
                if(objDet.Name__c != null && objDet.Phone__c != null && objDet.Relationship__c != null && objDet.Email__c != null)
                objDet.Emergency_Contact_Case__c = objFMCase.Id ;
                objDet.Emergency_Contact_Account__c = objFMCase.Account__c;
            }
            lstAdditionalDetails.addAll( mapAdditionalDetails.get( 'Emergency Contact' ) );
            upsert lstAdditionalDetails ;
    }
    public PageReference submitVacationNotice() {
        
        if( objFMCase != NULL ) {
            createMethod();
            System.debug('objFMCase--------'+objFMCase);
            objFMCase.Status__c='Submitted';
            objFMCase.Submitted__c=true;
            String locationCode=objUnit.Unit_name__c.split('/')[0];
            List<FM_User__c> lstfmUser = new List<FM_User__c>();
            lstfmUser = [select FM_User__r.Email from FM_User__c where FM_Role__c='FM Admin' AND Building__r.Name=:locationCode];
            if(!lstfmUser.isEmpty()){
                objFMCase.FM_Admin_Email__c=lstfmUser[0].FM_User__r.Email;
            }
            System.debug('objFMCase===='+objFMCase);
            upsert objFMCase ;
            PageReference objPage = new PageReference('/' + objFMCase.Id );
            return objPage ;
        }
        return NULL ;
    }

    public PageReference returnBackToCasePage() {
        System.debug('objFMCase.id-------3---------'+objFMCase.id);
        if( objFMCase != NULL ) {
            PageReference objPage = new PageReference('/' + objFMCase.Id );
            return objPage ;
        }
        return NULL;
    }

    @testVisible
    private void processDocuments() {
        lstUploadedDocs = NULL ;
        lstDocuments = NULL ;
        List<String> lstOfString=new List<String>();
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            mapUploadedDocs = new map<String, SR_Attachments__c>();
            for( SR_Attachments__c objAttach : fetchUploadedDocs() ) {
                mapUploadedDocs.put( objAttach.Name, objAttach );
            }
            if( mapUploadedDocs != NULL && !mapUploadedDocs.isEMpty() ) {
                lstUploadedDocs = new list<SR_Attachments__c>();
                lstUploadedDocs.addAll( mapUploadedDocs.values() );
            }
        }
        mapProcessDocuments = new map< String, FM_Documents__mdt >();
        lstDocuments = new list<FM_Documents__mdt>();
        for( FM_Documents__mdt objDocMeta : FM_Utility.getDocumentsList( strSRType, objUnit.Property_City__c ) ) {
            System.debug('objDocMeta===='+objDocMeta);
            mapProcessDocuments.put( objDocMeta.DeveloperName, objDocMeta );
            System.debug('mapProcessDocuments===='+mapProcessDocuments);
            System.debug('mapUploadedDocs===='+mapUploadedDocs);
            if( mapUploadedDocs != NULL && !mapUploadedDocs.containsKey( objDocMeta.MasterLabel ) ) {
                lstDocuments.add( objDocMeta );
            }
            if(objDocMeta.Mandatory__c==true){
                lstOfString.add('true');
            }
        }
        if(lstOfString.size()>0){
            isMandatory=true;
        }
    }

    public void deleteAttachment() {
        if( String.isNotBlank( deleteAttRecId ) ) {
           delete new SR_Attachments__c( Id = deleteAttRecId );
           processDocuments();
        }
    }

    @testVisible
    private void insertCustomAttachment( Id fmCaseId, UploadMultipleDocController.data objResponse ) {
        list<SR_Attachments__c> lstCustomAttachments = new list<SR_Attachments__c>();
        strDocumentName = strDocumentName.substring( 0, strDocumentName.lastIndexOf('.') );
        FM_Documents__mdt objDocMeta = mapProcessDocuments != null && mapProcessDocuments.containsKey( strDocumentName ) ?
                                       mapProcessDocuments.get( strDocumentName ) :
                                       NULL ;

        for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            SR_Attachments__c objCustAttach = new SR_Attachments__c();
            objCustAttach.Account__c = strAccountId ;
            objCustAttach.Attachment_URL__c = objFile.url;
            objCustAttach.Booking_Unit__c = strSelectedUnit ;
            objCustAttach.FM_Case__c = fmCaseId ;
            objCustAttach.Name = objDocMeta != NULL ? objDocMeta.MasterLabel : '' ;
            objCustAttach.Type__c = objDocMeta != NULL ? objDocMeta.Type__c : '' ;
            objCustAttach.isValid__c = true ;
            lstCustomAttachments.add( objCustAttach );
            strDocumentName = '';
            strDocumentBody = '';
        }
        if( !lstCustomAttachments.isEmpty() ) {
            insert lstCustomAttachments ;
            system.debug('==lstCustomAttachments=='+lstCustomAttachments);
            processDocuments() ;
            system.debug('==lstUploadedDocs=='+lstUploadedDocs);
            system.debug('==lstDocuments=='+lstDocuments);
        }
    }

    public list<SR_Attachments__c> fetchUploadedDocs() {
        if( String.isNotBlank( objFMCase.Id ) ) {
            return [ SELECT Id
                          , Type__c
                          , isValid__c
                          , Attachment_URL__c
                          , Name
                       FROM SR_Attachments__c
                      WHERE FM_Case__c =: objFMCase.Id ];
        }
        return new list<SR_Attachments__c>();
    }

    public void uploadDocument() {
        system.debug('== strDocumentBody =='+strDocumentBody);
        system.debug('== strDocumentName =='+strDocumentName);
        initializeFMCase();
        System.debug('objFMCase.id======'+objFMCase.id);
        System.debug('objFMCase======'+objFMCase);
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            System.debug('=========='+objFMCase != NULL && objFMCase.Id != NULL);
            UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
            list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
            if( String.isNotBlank( strDocumentName ) && String.isNotBlank( strDocumentBody ) ) {
                lstWrapper.add( PenaltyWaiverHelper.makeWrapperObject( EncodingUtil.Base64Encode( RentalPoolTerminationHelper.extractBody( strDocumentBody ) ) ,
                                                                       strDocumentName ,
                                                                       strDocumentName ,
                                                                       objUnit.Registration_Id__c, objFMCase.Id, '1' ) );

            }
            if( !lstWrapper.isEmpty() ) {
                objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper );
                system.debug('== objResponse document upload =='+objResponse);
                if( objResponse != NULL && objResponse.data != NULL ) {
                    insertCustomAttachment( objFMCase.Id, objResponse );
                }
            }
        }
    }
}