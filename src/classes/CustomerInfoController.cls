public without sharing class CustomerInfoController {

    //public transient Account accountObj                               {get; set;}
    //public list<Booking_Unit__c> lstBKGUnit                 {get; set;}
    //public transient Double dblTotalrevenue                           {get; set;}
    //public transient Double dblPendingAmount                          {get; set;}
    //public transient DateTime dtNextPaymentDate                       {get; set;}
    public transient list<PortfolioWrapper>lstPortWrapper             {get; set;}
    public transient String strRecordId {
        get;
        set {
            strRecordId = value;
            fetchAccount();
        }
    }

    public CustomerInfoController(){
        //string StrRecordId1 = Apexpages.currentPage().getparameters().get('Id');
        //Account accountObj =  new Account();
        //dblTotalrevenue = 0.0;
        //dblPendingAmount = 0.0;
        //dblTotalrevenue = 0.0;
        lstPortWrapper = new list<PortfolioWrapper>();
    }

    public static Map<Id, List<Payment_Terms__c>> generateMapBUID_PaymentTerms(set<Payment_Plan__c> setPP){
        Map<Id, List<Payment_Terms__c>> mapBUID_PT = new Map<Id, List<Payment_Terms__c>>();
        for(Payment_Plan__c objPP :  [SELECT Id
                                           , Status__c
                                           , Booking_Unit__c
                                           , (SELECT Id
                                                   , Payment_Amount__c
                                                   , Payment_Date__c
                                                   , Booking_Unit__c
                                              FROM Payment_Terms__r
                                              order by Payment_Date__c desc)  
                                      FROM Payment_Plan__c 
                                      WHERE Id IN: setPP]){
            system.debug('>>>> objPP : '+objPP);
            if(!mapBUID_PT.containsKey(objPP.Booking_Unit__c)){
                mapBUID_PT.put(objPP.Booking_Unit__c, objPP.Payment_Terms__r);
            }else{
                List<Payment_Terms__c> lstPT = mapBUID_PT.get(objPP.Booking_Unit__c);
                mapBUID_PT.remove(objPP.Booking_Unit__c);
                lstPT.add(objPP.Payment_Terms__r);
                mapBUID_PT.put(objPP.Booking_Unit__c,objPP.Payment_Terms__r);
            }
      }
      system.debug('>>>>mapBUID_PT = '+mapBUID_PT);
      return mapBUID_PT;
    }

    public void fetchAccount(){
        //Map<String,Booking_Unit_Active_Status__c> mapBookingUnitStatus = Booking_Unit_Active_Status__c.getAll();
        set<String> mapBookingUnitStatus = Booking_Unit_Active_Status__c.getAll().keySet();
        Map<Id, List<Payment_Terms__c>> mapBUID_PT = new Map<Id, List<Payment_Terms__c>>();
        list<Booking_Unit__c> lstBKGUnit = new list<Booking_Unit__c>();
        lstPortWrapper = new list<PortfolioWrapper>();
        set<Payment_Plan__c> setPP = new set<Payment_Plan__c>();
        if(StrRecordId!= null){
            system.debug('---22---'+CustomerCommunityUtils.customerAccountId);
            system.debug('---11-'+StrRecordId);
            lstBKGUnit = [Select Booking_Unit_Type__c,
                                 Area__c,
                                 Name,
                                 Token_Amount_val__c,
                                 CreatedDate,
                                 Token_Paid__c,
                                 Status__c,
                                 Registration_Status__c,
                                 Unit_Name__c,
                                 RERA_Project__c,
                                 Unit_Type__c,
                                 Requested_Price__c,
                                 Booking__r.Total_Units_Booked__c,
                                 Booking__r.Total_Booking_Amount__c,
                                 Booking__r.Status_Text__c,
                                 Booking__r.DP_Overdue__c,
                                 Booking__r.DP_Due_Date__c,
                                 Booking__r.No_Of_Units__c,
                                 Booking__r.Status__c,
                                 Booking__r.Completed_Date__c,
                                 Booking__r.Account__c,
                                 Booking__r.CreatedDate,
                                 Booking__r.CurrencyIsoCode,
                                 Booking__c,
                                 Virtual_Bank_Account__c,
                                 Virtual_IBAN__c,
                                 Inventory__c,
                                 Inventory__r.Property__c,
                                 Inventory__r.Property__r.Name,
                                 Booking__r.Total_Token_Amount_AED__c,
                                 (SELECT Id
                                       , Status__c
                                  FROM Payment_Plans__r
                                  Where Status__c != 'InActive')
                            From Booking_Unit__c
                           where Booking__r.Account__c = :CustomerCommunityUtils.customerAccountId 
                           AND Registration_Status__c IN: mapBookingUnitStatus
                           limit 1];
            system.debug('----CustomerCommunityUtils.customerAccountId----'+CustomerCommunityUtils.customerAccountId);
            system.debug('----mapBookingUnitStatus----'+mapBookingUnitStatus);            
            system.debug('----lstBKGUnit----'+lstBKGUnit);
        }
        for(Booking_Unit__c objBkgUnit: lstBKGUnit){
            if(setPP.size() > 0){
                setPP.add(objBkgUnit.Payment_Plans__r);
            }
        }
        if(setPP != null){
            mapBUID_PT = generateMapBUID_PaymentTerms(setPP);
        }

        system.debug('lstBKGUnit'+lstBKGUnit);
        for(Booking_Unit__c objBkgUnit: lstBKGUnit){
           PortfolioWrapper objWrapper = new PortfolioWrapper();
           objWrapper.unitId = objBkgUnit.Id;
           Double dblTotalrevenue = 0.0;
           dblTotalrevenue += (objBkgUnit.Requested_Price__c == NULL ? 0 : objBkgUnit.Requested_Price__c);
           system.debug('dblTotalrevenue'+dblTotalrevenue);

           if(mapBUID_PT != null && mapBUID_PT.get(objBkgUnit.Id) != null){
                for(Payment_Terms__c objPT : mapBUID_PT.get(objBkgUnit.Id)){
                    if(objPT.Payment_Date__c!= null
                        && objPT.Payment_Date__c >= date.today()){
                        objWrapper.dateNextpaymentdate = objPT.Payment_Date__c;
                        objWrapper.dblNextpaymentAmount = objPT.Payment_Amount__c;
                    }
                    objWrapper.dblNextpaymentAmount += objPT.Payment_Amount__c;
                } 
           }    
           
           if(objBkgUnit.Inventory__c != null
           && objBkgUnit.Inventory__r.Property__c != null){
                objWrapper.strPropoertyName = objBkgUnit.Inventory__r.Property__r.Name;
           }
           objWrapper.unitName = objBkgUnit.Unit_Name__c;
           objWrapper.project = objBkgUnit.RERA_Project__c;
           objWrapper.virtualBankAccount = objBkgUnit.Virtual_Bank_Account__c;
           objWrapper.virtualIBANNumber = objBkgUnit.Virtual_IBAN__c;
           //objWrapper.virtualAccountNumberBooking = objBkgUnit.Booking__r.Virtual_Account_Number__c;
           //objWrapper.strUnitType = objBkgUnit.Booking_Unit_Type__c;
           objWrapper.strUnitType = objBkgUnit.Unit_Type__c;
           objWrapper.dateBookingDate = objBkgUnit.CreatedDate;
           objWrapper.dblBookingprice = objBkgUnit.Token_Amount_val__c;
           objWrapper.dblArea = objBkgUnit.Area__c;
           if(objBkgUnit.Token_Paid__c == true){
                objWrapper.dblAmountpaid = objBkgUnit.Token_Amount_val__c;
           }
           objWrapper.strStatus = objBkgUnit.Registration_Status__c;
           lstPortWrapper.add(objWrapper);
           system.debug('lstPortWrapper--'+lstPortWrapper);
        }
        for(PortfolioWrapper objPortFolio : lstPortWrapper){
            datetime objDT;
            system.debug('objPortFolio'+objPortFolio);
            if(objPortFolio.dateNextpaymentdate != null)
                objDT = objPortFolio.dateNextpaymentdate;
                system.debug('objDT'+objDT);
                /*if(objDT != NULL && objDT >= date.today() && dtNextPaymentDate!= null){
                    if(dtNextPaymentDate >= objDT){
                        dtNextPaymentDate = objDT;
                    }
                }else{
                    dtNextPaymentDate = objDT;
                }*/
          }
    }

    public class PortfolioWrapper{
        public String unitId                        {get;set;}
        public string strPropoertyName              {get;set;}
        public String unitName                      {get;set;}
        public string project                       {get;set;}
        public String strUnitType                   {get;set;}
        public datetime dateBookingDate             {get;set;}
        public double dblBookingprice               {get;set;}
        public double dblArea                       {get;set;}
        public double dblAmountpaid                 {get;set;}
        public string strStatus                     {get;set;}
        public Date dateCompletiondate              {get;set;}
        public Date dateNextpaymentdate             {get;set;}
        public string dblNextpaymentAmount          {get;set;}
        public string virtualBankAccount {get;set;}
        public string virtualIBANNumber {get;set;}
        public string virtualAccountNumberBooking {get;set;}

        public PortfolioWrapper(){
            strPropoertyName='';
            project = '';
            strUnitType = '';
            dblArea=0.0;
            dblAmountpaid =0.0;
            strStatus='';
            dblNextpaymentAmount='';
        }
    }
}