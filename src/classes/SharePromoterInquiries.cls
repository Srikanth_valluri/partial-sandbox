/****************************************************************************************************
* Name          : SharePromoterInquiries                                                            *
* Description   : Class to create Inquiry Share records if Inquiry is Created by Promoter           *
* Created Date  : 06-06-2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER     AUTHOR            DATE            COMMENTS                                                *
* 1.0     Craig Lobo        06-06-2018      Initial Draft.                                          *
****************************************************************************************************/

// SHARING DONE FROM INFORMATICA

global without sharing class SharePromoterInquiries {

    public SharePromoterInquiries() {
        
    }

/*
    public static Set<String> inqUserIdSet = new Set<String>();

    public static void insertInquiryShareRecords(List<Inquiry__c> pInquiryIdList) {
        Map<Id, Id> inqIdCreatorIdMap = new Map<Id, Id>();
        Map<Id, List<Id>> creatorIdInqIdListMap = new Map<Id, List<Id>>();
        List<Inquiry__Share> inquiryShareList = new List<Inquiry__Share>();
        List<String> promoterProfileNamesList = Label.PromoterCommunityProfile.split(',');
        System.debug('SharePromoterInquiriesWithManagers>>>> ');
        Set<Id> inquiryIdSet = new Set<Id>();
        Set<String> inqUserIdSet = new Set<String>();

        for (Inquiry__c inq : pInquiryIdList) {
            if (creatorIdInqIdListMap.containsKey(inq.CreatedById)) {
                System.debug('GET>>>> ');
                creatorIdInqIdListMap.get(inq.CreatedById).add(inq.Id);
            } else {
                System.debug('PUT>>>> ');
                creatorIdInqIdListMap.put(inq.CreatedById, new List<String>{inq.Id});
            }
            inquiryIdSet.add(inq.Id);
        }
        System.debug('creatorIdInqIdListMap>>>> '+ creatorIdInqIdListMap);


        for(User userObj : [SELECT Id
                                 , Name
                                 , ContactId
                                 , ManagerId
                                 , Profile.Name
                                 , Manager.ManagerId
                                 , Manager.Manager.ManagerId
                                 , Manager.Manager.Manager.ManagerId
                                 , Manager.Manager.Manager.Manager.ManagerId
                              FROM User 
                             WHERE Id IN :creatorIdInqIdListMap.keySet()
        ]) {
            System.debug('userObj>>>> ' + userObj);
            System.debug('userObj>>>> ' + userObj.Profile.Name);
            if (promoterProfileNamesList.contains(userObj.Profile.Name)) {
                if (creatorIdInqIdListMap.containsKey(userObj.Id) 
                    && creatorIdInqIdListMap.get(userObj.Id) != null 
                ) {
                    for (Id inqId : creatorIdInqIdListMap.get(userObj.Id)) {
                        System.debug('LOOP inqId>>>> ' + inqId);
                        if (String.isNotBlank(userObj.ManagerId)) {
                            inquiryShareList.add(createInquiryShare(inqId, userObj.ManagerId));
                            if(String.isNotBlank(userObj.Manager.ManagerId)) {
                                inquiryShareList.add(createInquiryShare(inqId, userObj.Manager.ManagerId));
                                ///if(String.isNotBlank(userObj.Manager.Manager.ManagerId)) {
                                //    inquiryShareList.add(createInquiryShare(inqId, userObj.Manager.Manager.ManagerId));
                                //    if(String.isNotBlank(userObj.Manager.Manager.Manager.ManagerId)) {
                                //        inquiryShareList.add(createInquiryShare(inqId, userObj.Manager.Manager.Manager.ManagerId));
                                //        if(String.isNotBlank(userObj.Manager.Manager.Manager.Manager.ManagerId)) {
                                //           inquiryShareList.add(createInquiryShare(inqId, userObj.Manager.Manager.Manager.Manager.ManagerId));
                                //        }
                                //    }
                                //}
                            }
                        }
                    }
                }
            }
        }
        System.debug('inquiryShareList>>>> ' + inquiryShareList);

        // DML
        try {
            insert inquiryShareList;
        } catch(Exception e) {
            System.debug('EXCEPTION >>>> ' + e);
        }

    }*/

    /**
     * Method to Create Inquiry Share records 
     */
    /*public static Inquiry__Share createInquiryShare(Id pInqId, Id pUserId) {
        System.debug('pInqId>>>> ' + pInqId);
        System.debug('pUserId>>>> ' + pUserId);
        System.debug('inqUserIdSet.contains>>>> ' + inqUserIdSet.contains(pInqId + '-' + pUserId));
        //if (!inqUserIdSet.contains(pInqId + '-' + pUserId)) {
            Inquiry__Share shareObj = new Inquiry__Share();
            System.debug('shareObj>>>> ');
            shareObj.ParentId = pInqId;
            System.debug('ParentId>>>> ');
            shareObj.UserOrGroupId = pUserId;
            System.debug('UserOrGroupId>>>> ');
            shareObj.AccessLevel = 'Read';
            System.debug('AccessLevel>>>> ');
            shareObj.RowCause = Schema.Inquiry__Share.RowCause.Promoter__c;
            return shareObj;
        //}
        //return null;
    }*/

}