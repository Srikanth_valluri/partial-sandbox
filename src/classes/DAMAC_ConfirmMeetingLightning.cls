global class DAMAC_ConfirmMeetingLightning {
    
    @AuraEnabled
    public static String updateInquiryWithMeetingDetails(String inquiryId){
        String message = '';
        system.debug('inquiryId: ' + inquiryId);
        Inquiry__c inq = [SELECT Id, Name, Meeting_Due_Date__c, Is_Meeting_scheduled__c,
                                    Mortgage_Required__c, Would_you_like_assistance_on_mortgage__c,
                                    Mortgage_Agent__c, Sales_Office_Location_Url__c,
                                    Online_Meeting_Start_Date_Time__c, Sales_Office__c,
                                    Schedule_Conditions__c, Is_Meeting_Confirmed__c,
                                    Confirmation_Conditions__c
                                  FROM Inquiry__c WHERE Id =: inquiryId];
        
        
        String today = date.today().format();
        if(((inq.Sales_Office_Location_Url__c != '' && inq.Meeting_Due_Date__c != null)
                    || inq.Online_Meeting_Start_Date_Time__c != null)
                && inq.Is_Meeting_scheduled__c && inq.Schedule_Conditions__c){
            Date startDateVal;
            if((inq.Meeting_Due_Date__c != null)){
                startDateVal = inq.Meeting_Due_Date__c.date();
            }
            if((inq.Online_Meeting_Start_Date_Time__c != null)){
                startDateVal = inq.Online_Meeting_Start_Date_Time__c.date();
            }
            String[] salesoffices;
            if(Label.Sales_Office_Meeting_Exception!= null)
                salesoffices = Label.Sales_Office_Meeting_Exception.split(',');
            System.debug(inq.Sales_Office__c);
            System.debug(salesoffices);
            System.debug(salesoffices.contains(inq.Sales_Office__c));
            System.debug(system.today() <= startDateVal);
            if((system.today() == startDateVal ) || (system.today() <= startDateVal && salesoffices != null && salesoffices.contains(inq.Sales_Office__c))){
                try{
                    inq.Is_Meeting_Confirmed__c = true;
                    inq.Confirmation_Conditions__c = true;
                    update inq;
                    message = 'Meeting Confirmed Successfully';
                } catch (exception e){
                    system.debug('Exception while updating Inquiry: ' + e);
                    message = 'Error while updating: ' + e;
                    return message;
                }
                return message;
            } else{
                message = 'Could not confirm as the meeting date is different from confirmation date!';
                return message;
            }
        } else{
            message = 'Could not confirm the meeting';
            return message;
        }
    }
    
}