@isTest
private class FmcUtilsTest {

    @isTest
    static void testIsOwner() {
        Account account = new Account(Name = 'Test Account');
        insert account;

        CustomerCommunityUtils.customerAccountId = account.Id;

        Test.startTest();
            System.assert(!FmcUtils.isOwner());
        Test.stopTest();
    }

    @isTest
    static void testIsTenant() {
        Account account = new Account(Name = 'Test Account');
        insert account;

        CustomerCommunityUtils.customerAccountId = account.Id;

        Test.startTest();
            System.assert(!FmcUtils.isTenant());
        Test.stopTest();
    }

    @isTest
    static void testIsBoardMember() {
        Account account = new Account(Name = 'Test Account');
        insert account;

        CustomerCommunityUtils.customerAccountId = account.Id;

        Test.startTest();
            System.assert(!FmcUtils.isBoardMember());
        Test.stopTest();
    }

    @isTest
    static void testIsCurrentView() {
        Test.startTest();
            System.assert(!FmcUtils.isCurrentView('Home'));
        Test.stopTest();
    }

    @isTest
    static void testFormatAsIpmsDateNullDate() {
        Test.startTest();
            Date nullDate = NULL;
            System.assertEquals(NULL, FmcUtils.formatAsIpmsDate(nullDate));
        Test.stopTest();
    }

    @isTest
    static void testFormatAsIpmsDateNullDatetime() {
        Test.startTest();
            Datetime nullDatetime = NULL;
            System.assertEquals(NULL, FmcUtils.formatAsIpmsDate(nullDatetime));
        Test.stopTest();
    }

    @isTest
    static void testFormatAsIpmsDateValidInput() {
        Test.startTest();
            System.assertNotEquals(NULL, FmcUtils.formatAsIpmsDate(Date.today()));
        Test.stopTest();
    }

    @isTest
    private static void testQueryLocationsForAccount() {
        Account account = new Account(Name = 'Test Account');
        insert account;

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        insert new List<SR_Attachments__c> {
            new SR_Attachments__c(
                Building__c = location.Id,
                Document_Type__c = 'Form'
            )
        };

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(account.Id, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_Name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        Test.startTest();
            FmcUtils.queryLocationsForAccount(account.Id);
        Test.stopTest();
    }

    @isTest
    private static void testQueryOwnedUnitsForAccount() {
        Account account = new Account(Name = 'Test Account');
        insert account;

        Test.startTest();
            FmcUtils.queryOwnedUnitsForAccount(account.Id);
        Test.stopTest();
    }

    @isTest
    private static void testQueryLeasedUnitsForAccount() {
        Account account = new Account(Name = 'Test Account');
        insert account;

        Test.startTest();
            FmcUtils.queryLeasedUnitsForAccount(account.Id);
        Test.stopTest();
    }

    @isTest
    private static void testQueryUnitsForAccount() {
        Account account = new Account(Name = 'Test Account');
        insert account;

        Test.startTest();
            FmcUtils.queryUnitsForAccount(account.Id);
        Test.stopTest();
    }

}