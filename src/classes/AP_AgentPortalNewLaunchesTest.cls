@isTest(SeeAllData=false)
public class AP_AgentPortalNewLaunchesTest{

  public static Contact adminContact;
    public static User portalUser;
    public static Account adminAccount;
    public static User portalOnlyAgent;
    public static Contact agentContact;
    private  static Inventory__c inventory ; 
   private static Address__c addressDetail ; 
   private  static Property__c propertyDetail ;
    
    
     @testsetup
     public static void TestData(){
    
         addressDetail = InitialiseTestData.getAddressDetails(9086);
         insert addressDetail ; 
        
         propertyDetail = InitialiseTestData.getPropertyDetails(7650);
         insert propertyDetail ; 
        
         inventory = InitialiseTestData.getInventoryDetails('3456','1234','2345',9086,7650); 
         inventory.Status__c = 'Released';
         inventory.Unit_Type__c = 'Test';
         inventory.Address__c = addressDetail.Id;    
         insert inventory ; 
        
}
    
    static void init(){

        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole00');
        insert userRoleObj;

        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'te156', email='xy11z1@email.com',
                emailencodingkey='UTF-8', lastname='User 41156', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xy11z1@email.com',UserRoleId = userRoleObj.Id);

         
        System.RunAs(adminUser) 
        {
            adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;

            adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            adminContact.Eligible_For_VAT__c = true;
            insert adminContact;

            agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            
            Attachment objAttachment = new Attachment();
            objAttachment.Name = 'Test Document';
            objAttachment.Body = blob.valueOf('test');
            objAttachment.ContentType='image/png';
            objAttachment.ParentId = adminAccount.Id;
            insert objAttachment;

            portalUser = InitialiseTestData.getPortalUser('xyz@123.com', adminContact.Id, 'Admin');
            portalOnlyAgent = InitialiseTestData.getPortalUser('abc@sa.com', agentContact.Id, 'Agent');

            ApexPages.currentPage().getParameters().put('floorPkgName', 'floorPkgName');
            Apexpages.currentPage().getParameters().put('Location','Location');
            ApexPages.currentPage().getParameters().put('sfdc.tabName','sfdc.tabName');
            ApexPages.currentPage().getParameters().put('Id','07M9E000002LIgU');
            ApexPages.currentPage().getParameters().put('District','District');
            ApexPages.currentPage().getParameters().put('MarketingName','MarketingName');
            ApexPages.currentPage().getParameters().put('MinPrice','200');
            ApexPages.currentPage().getParameters().put('MaxPrice','5000');
            ApexPages.currentPage().getParameters().put('UnitBedrooms','UnitBedrooms');
            ApexPages.currentPage().getParameters().put('Bedrooms','Bedrooms');
            ApexPages.currentPage().getParameters().put('Type','Type');
            ApexPages.currentPage().getParameters().put('UnitType','UnitType');
        }
     }
     
     public static testMethod void NewLaunchesTest(){
     init();
         
         
       Test.startTest();
            AP_AgentPortalNewLaunches obj;
            system.RunAs(portalUser)
            {
                Page_Flow__c PFobj = new Page_Flow__c();
                PFobj.Name = 'VAT Agent Update';
                PFobj.Master_Object__c = 'NSIBPM__Service_Request__c';
                PFobj.Master_Page__c = 'Process_Flow';
                insert PFobj;       
                obj = new AP_AgentPortalNewLaunches();
            
                Attachment objAttachment = [Select Id, Name, Body, ContentType From Attachment Where Name = 'Test Document'];
            obj.propertyId =  ApexPages.currentPage().getParameters().put('Id','Id');
            obj.priceRangeValues = null;
            obj.areaSqftMin = '345';
            obj.areaSqftMax = '23';
            obj.inventoryPriceConfiguration = null ;
            obj.locationSelectedFromURL = 'locationSelectedFromURL';
            obj.propertyName = 'propertyName';
            obj.tabName = 'tabName';
             /*obj.vatRegDate = '2017-10-11';
             obj.vatRegNumber = '1234';
             obj.vatBusinessType = '1234';*/
             
             obj.submitVatSR ();
            
            obj.getAllInventories();  
            obj.filterInventories();
            obj.getPropertyStatus();
            obj.getCountries();
            obj.setCountries('UAE');  
                obj.att = objAttachment;
            obj.uploadDoc();
            obj.getatt();
            Test.stopTest(); 
            }
     
     
 }
     
 
     
     public static testmethod void getVillaTypeTest(){
         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Project_Category__c  = 'Ready';
         inv.Status__c   = 'Released';
         update inv;
        
            Test.stopTest(); 
                Page_Flow__c PFobj = new Page_Flow__c();
                PFobj.Name = 'VAT Agent Update';
                PFobj.Master_Object__c = 'NSIBPM__Service_Request__c';
                PFobj.Master_Page__c = 'Process_Flow';
                insert PFobj;
            AP_AgentPortalNewLaunches obj = new AP_AgentPortalNewLaunches();
            obj.getVillaType();
         
     }
     
     public static testmethod void getPopularSearchesStudioTest(){
         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Property_Status__c  = 'Ready';
         inv.Unit_Type__c   = 'Studio';
         update inv;
        
            Test.stopTest(); 
          Page_Flow__c PFobj = new Page_Flow__c();
                PFobj.Name = 'VAT Agent Update';
                PFobj.Master_Object__c = 'NSIBPM__Service_Request__c';
                PFobj.Master_Page__c = 'Process_Flow';
                insert PFobj;
            AP_AgentPortalNewLaunches obj = new AP_AgentPortalNewLaunches();
            obj.getPopularSearchesStudio();
          
         
     }
     
     public static testmethod void getPopularSearchesDubaiReadyAptTest(){
         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Property_Status__c  = 'Ready';
         inv.Unit_Type__c   = 'Apartments';
         inv.Property_City__c    = 'Dubai';
         update inv;
         Test.stopTest(); 
          Page_Flow__c PFobj = new Page_Flow__c();
                PFobj.Name = 'VAT Agent Update';
                PFobj.Master_Object__c = 'NSIBPM__Service_Request__c';
                PFobj.Master_Page__c = 'Process_Flow';
                insert PFobj; 
         AP_AgentPortalNewLaunches obj = new AP_AgentPortalNewLaunches();
         obj.getPopularSearchesDubaiReadyApt();
     }
     
     public static testmethod void getPopularSearchesDubaiReadyVillasTest(){
         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Property_Status__c  = 'Ready';
         inv.Unit_Type__c   = 'Villas';
         inv.Property_City__c    = 'Dubai';
         update inv;
         Test.stopTest(); 
          Page_Flow__c PFobj = new Page_Flow__c();
                PFobj.Name = 'VAT Agent Update';
                PFobj.Master_Object__c = 'NSIBPM__Service_Request__c';
                PFobj.Master_Page__c = 'Process_Flow';
                insert PFobj; 
         AP_AgentPortalNewLaunches obj = new AP_AgentPortalNewLaunches();
         obj.getPopularSearchesDubaiReadyVillas();
        }
     
     public static testmethod void getPopularSearchesBusinessBayTest(){
         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Property_Status__c = 'Ready';
         inv.Bedroom_Type__c = '1 BR';
         inv.Master_Developer_EN__c = 'Business Bay';
         update inv;
         Test.stopTest(); 
          Page_Flow__c PFobj = new Page_Flow__c();
                PFobj.Name = 'VAT Agent Update';
                PFobj.Master_Object__c = 'NSIBPM__Service_Request__c';
                PFobj.Master_Page__c = 'Process_Flow';
                insert PFobj; 
         AP_AgentPortalNewLaunches obj = new AP_AgentPortalNewLaunches();
         obj.getPopularSearchesBusinessBay();
        }
       public static testmethod void getPopularSearchesTwoBHKAptTest(){
         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Property_Status__c = 'Ready';
         inv.Bedroom_Type__c = '2 BR';
         inv.Unit_Type__c  = 'Apartments';
         update inv;
         Test.stopTest(); 
          Page_Flow__c PFobj = new Page_Flow__c();
                PFobj.Name = 'VAT Agent Update';
                PFobj.Master_Object__c = 'NSIBPM__Service_Request__c';
                PFobj.Master_Page__c = 'Process_Flow';
                insert PFobj; 
         AP_AgentPortalNewLaunches obj = new AP_AgentPortalNewLaunches();
         obj.getPopularSearchesTwoBHKApt();
         }
     
         public static testmethod void getPopularSearchesOneMillionVillas(){
         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Property_Status__c  = 'Ready';
         inv.Unit_Type__c   = 'Hotel Apartments';
         inv.Property_City__c   = 'Dubai';
         update inv;
         
         Test.stopTest(); 
          Page_Flow__c PFobj = new Page_Flow__c();
                PFobj.Name = 'VAT Agent Update';
                PFobj.Master_Object__c = 'NSIBPM__Service_Request__c';
                PFobj.Master_Page__c = 'Process_Flow';
                insert PFobj; 
         AP_AgentPortalNewLaunches obj = new AP_AgentPortalNewLaunches();
         obj.getPopularSearchesOneMillionVillas();
        
     }
     
      public static testmethod void getAgentsOfferForYouTest(){
           Page_Flow__c PFobj = new Page_Flow__c();
                PFobj.Name = 'VAT Agent Update';
                PFobj.Master_Object__c = 'NSIBPM__Service_Request__c';
                PFobj.Master_Page__c = 'Process_Flow';
                insert PFobj; 
         AP_AgentPortalNewLaunches obj = new AP_AgentPortalNewLaunches();
         obj.getNewLaunches();
         obj.getAgentsOfferForYou();
         
     }
      public static testmethod void InnerClassTest(){
          Page_Flow__c PFobj = new Page_Flow__c();
                PFobj.Name = 'VAT Agent Update';
                PFobj.Master_Object__c = 'NSIBPM__Service_Request__c';
                PFobj.Master_Page__c = 'Process_Flow';
                insert PFobj; 
        AP_AgentPortalNewLaunches.NewPopularSearches obj = new AP_AgentPortalNewLaunches.NewPopularSearches(); 
         obj.StudioURL = 'Test';
         obj.DubaiReadyApartment = 'Test';
         obj.ReadyVillas = 'Test';
         obj.BusinessBay = 'Test';
         obj.Apartment = 'Test';
         obj.OneMillionVillas = 'Test';
         obj.HotelRooms = 'Test';
         obj.HotelApartments = 'Test';
         
     }


}