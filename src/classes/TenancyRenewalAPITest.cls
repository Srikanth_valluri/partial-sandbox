@isTest
public class TenancyRenewalAPITest {
  @isTest
    static void TestRenewalDraft() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
        
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
        
        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;
        
        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;
        
        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Tenant_Renewal_T',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId(),
                                           Request_Type__c = 'Tenant Renewal',
                                           Booking_Unit__c = bookingUnit.Id);
        insert fmCase;
        
        String requestJson =  '   {                '  + 
        '      "action": "draft",                  '  + 
        '      "id": "",                      '  + 
        '      "booking_unit_id": "'+bookingUnit.Id+'",      '  + 
        '      "account_id":"'+account.Id+'",          '  +
      '      "first_name":"testf1",              '  +
      '      "last_name":"testl1",              '  +
      '      "gender":"Male",                  '  +
      '      "date_of_birth":"2020-11-03",          '  +
      '      "no_of_adults":1,                '  +
      '      "no_of_children":0,                '  +
      '      "nationality":"UAE",                '  +
      '      "passport_number":"1234",            '  +
      '      "passport_expiry_date":"2020-11-03",        '  +
      '      "emirates_id":"111111111",            '  +
      '      "mobile_code":"United Arab Emirates: 00971",    '  +
      '      "mobile_number":"1233443453",          '  +
      '      "email_address":"test1@test.com",        '  +
      '      "lease_start_date":"2020-11-01",          '  +
      '      "lease_end_date":"2020-11-05",          '  +
      '      "ejari_number":"2020",              '  +
        '      "is_person_with_spl_needs": false,        '  + 
        '      "is_having_pets": true,              '  + 
        '      "additional_members": [                          '  + 
        '          {                        '  + 
        '              "id": "",                 '  + 
        '              "member_first_name": "Shubham",      '  + 
        '              "member_last_name": "Suryawanshi",      '  + 
        '              "member_gender": "Male",                 '  + 
        '              "member_date_of_birth": "1996-07-15",    '  + 
        '              "member_nationality": "",                '  + 
        '              "member_passport_number": "123",      '  + 
        '              "member_emirates_id": "",          '  + 
        '              "member_mobile_number": "",        '  + 
        '              "member_email_address": ""          '  + 
        '          }  '  + 
        '      ],  '  + 
        '      "emergency_contact_details": [                                          '  + 
        '          {  '  + 
        '            "id": "",                                                             '  + 
        '            "emergency_full_name": "Shubham Suryawanshi",  '  + 
        '            "emergency_relationship": "Test",                                          '  + 
        '            "emergency_mobile_number": "",  '  + 
        '            "emergency_email_address": ""  '  + 
        '          }  '  + 
        '      ]  '  + 
        '  }  ' ;
    
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/InsertTenancyRenewal';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(requestJson);
        RestContext.request = req;
        RestContext.response = res;
        
        TenancyRenewalAPI.apiMainMethod();
        
        Test.stopTest();
    }
  
    @isTest
    static void TestRenewalSubmit() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
    
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
    
        Booking__c booking = new Booking__c(Deal_SR__c = sr.Id,Account__c = account.Id);
        insert booking;
    
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;
    
        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;
    
        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;
    
        String bookingUnitId = String.valueOf(bookingUnit.Id);
        
        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Tenant_Renewal_T',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId(),
                                           Request_Type__c = 'Tenant Renewal',
                                           Booking_Unit__c = bookingUnit.Id);
        insert fmCase;
        
    String fmCaseId = String.valueOf(fmCase.Id);
        
        String requestJson =  '   {                '  + 
        '      "action": "submit",                '  + 
        '      "id": "'+fmCaseId+'",                '  + 
        '      "booking_unit_id": "'+bookingUnitId+'",      '  + 
        '      "account_id":"'+account.Id+'",          '  +
      '      "first_name":"testf1",              '  +
      '      "last_name":"testl1",              '  +
      '      "gender":"Male",                  '  +
      '      "date_of_birth":"2020-11-03",          '  +
      '      "no_of_adults":1,                '  +
      '      "no_of_children":0,                '  +
      '      "nationality":"UAE",                '  +
      '      "passport_number":"1234",            '  +
      '      "passport_expiry_date":"2020-11-03",        '  +
      '      "emirates_id":"111111111",            '  +
      '      "mobile_code":"United Arab Emirates: 00971",    '  +
      '      "mobile_number":"1233443453",          '  +
      '      "email_address":"test1@test.com",        '  +
      '      "lease_start_date":"2020-11-01",          '  +
      '      "lease_end_date":"2020-11-05",          '  +
      '      "ejari_number":"2020",              '  +
        '      "is_person_with_spl_needs": false,        '  + 
        '      "is_having_pets": true,              '  + 
        '      "additional_members": [                          '  + 
        '          {                        '  + 
        '              "id": "",                 '  + 
        '              "member_first_name": "",          '  + 
        '              "member_last_name": "",          '  + 
        '              "member_gender": "",                   '  + 
        '              "member_date_of_birth": "",    '  + 
        '              "member_nationality": "",                '  + 
        '              "member_passport_number": "",        '  + 
        '              "member_emirates_id": "",          '  + 
        '              "member_mobile_number": "",        '  + 
        '              "member_email_address": "test3@test.com" '  + 
        '          }  '  + 
        '      ],  '  + 
        '      "emergency_contact_details": [                                          '  + 
        '          {  '  + 
        '            "id": "",                                                             '  + 
        '            "emergency_full_name": "Shubham Suryawanshi",  '  + 
        '            "emergency_relationship": "Test",                                          '  + 
        '            "emergency_mobile_number": "",  '  + 
        '            "emergency_email_address": ""  '  + 
        '          }  '  + 
        '      ]  '  + 
        '  }  ' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/InsertTenancyRenewal';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(requestJson);
        RestContext.request = req;
        RestContext.response = res;
        
        TenancyRenewalAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
  static void apiMainMethodTestNoJSON() {
    String jsonString = ''; 
    
    Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/InsertMoveOutCase';  
        req.requestBody = Blob.valueOf(jsonString);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        TenancyRenewalAPI.apiMainMethod();
        Test.stopTest();
  }
    
  @isTest
  static void apiMainMethodTestNoAction() {
    String jsonString =  '   {  '  + 
    '     "action": "",  '  + 
    '       "id": "",  '  + 
    '       "booking_unit_id": "",  '  + 
    '       "account_id" : ""  '  + 
    '  }  ' ;
        
    Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/InsertMoveOutCase';  
        req.requestBody = Blob.valueOf(jsonString);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        TenancyRenewalAPI.apiMainMethod();
        Test.stopTest();
  }

  @isTest
  static void apiMainMethodTestNoAccount() {
    String jsonString =  '   {  '  + 
    '     "action": "draft",  '  + 
    '       "id": "",  '  + 
    '       "booking_unit_id": "1234",  '  + 
    '       "account_id" : ""  '  + 
    '  }  ' ;
        
    Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/InsertMoveOutCase';  
        req.requestBody = Blob.valueOf(jsonString);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        TenancyRenewalAPI.apiMainMethod();
        Test.stopTest();
  }
    
    @isTest
    static void apiMainMethodTestNoFmCase() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
    
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
    
        Booking__c booking = new Booking__c(Deal_SR__c = sr.Id,Account__c = account.Id);
        insert booking;
    
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;
    
        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;
    
        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;
    
        String bookingUnitId = String.valueOf(bookingUnit.Id);
        
        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Tenant_Renewal_T',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId(),
                                           Request_Type__c = 'Tenant Renewal',
                                           Booking_Unit__c = bookingUnit.Id);
        insert fmCase;
        
    String fmCaseId = String.valueOf(fmCase.Id);
        
        String requestJson =  '   {                '  + 
        '      "action": "submit",                '  + 
        '      "id": "",                      '  + 
        '      "booking_unit_id": "'+bookingUnitId+'",      '  + 
        '      "account_id":"'+account.Id+'",          '  +
      '      "first_name":"testf1",              '  +
      '      "last_name":"testl1",              '  +
      '      "gender":"Male",                  '  +
      '      "date_of_birth":"2020-11-03",          '  +
      '      "no_of_adults":1,                '  +
      '      "no_of_children":0,                '  +
      '      "nationality":"UAE",                '  +
      '      "passport_number":"1234",            '  +
      '      "passport_expiry_date":"2020-11-03",        '  +
      '      "emirates_id":"111111111",            '  +
      '      "mobile_code":"United Arab Emirates: 00971",    '  +
      '      "mobile_number":"1233443453",          '  +
      '      "email_address":"test1@test.com",        '  +
      '      "lease_start_date":"2020-11-01",          '  +
      '      "lease_end_date":"2020-11-05",          '  +
      '      "ejari_number":"2020",              '  +
        '      "is_person_with_spl_needs": false,        '  + 
        '      "is_having_pets": true,              '  + 
        '      "additional_members": [                          '  + 
        '          {                        '  + 
        '              "id": "",                 '  + 
        '              "member_first_name": "Shubham",      '  + 
        '              "member_last_name": "Suryawanshi",      '  + 
        '              "member_gender": "Male",                 '  + 
        '              "member_date_of_birth": "1996-07-15",    '  + 
        '              "member_nationality": "",                '  + 
        '              "member_passport_number": "123",      '  + 
        '              "member_emirates_id": "",          '  + 
        '              "member_mobile_number": "",        '  + 
        '              "member_email_address": ""          '  + 
        '          }  '  + 
        '      ],  '  + 
        '      "emergency_contact_details": [                                          '  + 
        '          {  '  + 
        '            "id": "",                                                             '  + 
        '            "emergency_full_name": "Shubham Suryawanshi",  '  + 
        '            "emergency_relationship": "Test",                                          '  + 
        '            "emergency_mobile_number": "",  '  + 
        '            "emergency_email_address": ""  '  + 
        '          }  '  + 
        '      ]  '  + 
        '  }  ' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/InsertTenancyRenewal';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(requestJson);
        RestContext.request = req;
        RestContext.response = res;
        
        TenancyRenewalAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void apiMainMethodTestInvalidPassport() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
    
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
    
        Booking__c booking = new Booking__c(Deal_SR__c = sr.Id,Account__c = account.Id);
        insert booking;
    
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;
    
        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;
    
        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;
    
        String bookingUnitId = String.valueOf(bookingUnit.Id);
        
        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Tenant_Renewal_T',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId(),
                                           Request_Type__c = 'Tenant Renewal',
                                           Booking_Unit__c = bookingUnit.Id);
        insert fmCase;
        
    String fmCaseId = String.valueOf(fmCase.Id);
        
        String requestJson =  '   {                '  + 
        '      "action": "submit",                '  + 
        '      "id": "'+fmCaseId+'",                '  + 
        '      "booking_unit_id": "'+bookingUnitId+'",      '  + 
        '      "account_id":"'+account.Id+'",          '  +
      '      "first_name":"testf1",              '  +
      '      "last_name":"testl1",              '  +
      '      "gender":"Male",                  '  +
      '      "date_of_birth":"2020-11-03",          '  +
      '      "no_of_adults":1,                '  +
      '      "no_of_children":0,                '  +
      '      "nationality":"UAE",                '  +
      '      "passport_number":"1234",            '  +
      '      "passport_expiry_date":"2020-11-03",        '  +
      '      "emirates_id":"111111111111111",          '  +
      '      "mobile_code":"United Arab Emirates: 00971",    '  +
      '      "mobile_number":"1233443453",          '  +
      '      "email_address":"test1@test.com",        '  +
      '      "lease_start_date":"2020-11-01",          '  +
      '      "lease_end_date":"2020-11-05",          '  +
      '      "ejari_number":"2020",              '  +
        '      "is_person_with_spl_needs": false,        '  + 
        '      "is_having_pets": true,              '  + 
        '      "additional_members": [                          '  + 
        '          {                        '  + 
        '              "id": "",                 '  + 
        '              "member_first_name": "",          '  + 
        '              "member_last_name": "",          '  + 
        '              "member_gender": "",                   '  + 
        '              "member_date_of_birth": "",    '  + 
        '              "member_nationality": "",                '  + 
        '              "member_passport_number": "",        '  + 
        '              "member_emirates_id": "",          '  + 
        '              "member_mobile_number": "",        '  + 
        '              "member_email_address": "test3@test.com" '  + 
        '          }  '  + 
        '      ],  '  + 
        '      "emergency_contact_details": [                                          '  + 
        '          {  '  + 
        '            "id": "",                                                             '  + 
        '            "emergency_full_name": "Shubham Suryawanshi",  '  + 
        '            "emergency_relationship": "Test",                                          '  + 
        '            "emergency_mobile_number": "",  '  + 
        '            "emergency_email_address": ""  '  + 
        '          }  '  + 
        '      ]  '  + 
        '  }  ' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/InsertTenancyRenewal';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(requestJson);
        RestContext.request = req;
        RestContext.response = res;
        
        TenancyRenewalAPI.apiMainMethod();
        
        Test.stopTest();
    }
    
    @isTest
    static void apiMainMethodTestInvalidEmail() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
    
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
    
        Booking__c booking = new Booking__c(Deal_SR__c = sr.Id,Account__c = account.Id);
        insert booking;
    
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;
    
        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;
    
        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;
    
        String bookingUnitId = String.valueOf(bookingUnit.Id);
        
        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Tenant_Renewal_T',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId(),
                                           Request_Type__c = 'Tenant Renewal',
                                           Booking_Unit__c = bookingUnit.Id);
        insert fmCase;
        
    String fmCaseId = String.valueOf(fmCase.Id);
        
        String requestJson =  '   {                '  + 
        '      "action": "submit",                '  + 
        '      "id": "'+fmCaseId+'",                '  + 
        '      "booking_unit_id": "'+bookingUnitId+'",      '  + 
        '      "account_id":"'+account.Id+'",          '  +
      '      "first_name":"testf1",              '  +
      '      "last_name":"testl1",              '  +
      '      "gender":"Male",                  '  +
      '      "date_of_birth":"2020-11-03",          '  +
      '      "no_of_adults":1,                '  +
      '      "no_of_children":0,                '  +
      '      "nationality":"UAE",                '  +
      '      "passport_number":"1234",            '  +
      '      "passport_expiry_date":"2020-11-03",        '  +
      '      "emirates_id":"111111111111111",          '  +
      '      "mobile_code":"United Arab Emirates: 00971",    '  +
      '      "mobile_number":"1233443453",          '  +
      '      "email_address":"test1.com",            '  +
      '      "lease_start_date":"2020-11-01",          '  +
      '      "lease_end_date":"2020-11-05",          '  +
      '      "ejari_number":"2020",              '  +
        '      "is_person_with_spl_needs": false,        '  + 
        '      "is_having_pets": true,              '  + 
        '      "additional_members": [                          '  + 
        '          {                        '  + 
        '              "id": "",                 '  + 
        '              "member_first_name": "",          '  + 
        '              "member_last_name": "",          '  + 
        '              "member_gender": "",                   '  + 
        '              "member_date_of_birth": "",    '  + 
        '              "member_nationality": "",                '  + 
        '              "member_passport_number": "",        '  + 
        '              "member_emirates_id": "",          '  + 
        '              "member_mobile_number": "",        '  + 
        '              "member_email_address": "test3@test.com" '  + 
        '          }  '  + 
        '      ],  '  + 
        '      "emergency_contact_details": [                                          '  + 
        '          {  '  + 
        '            "id": "",                                                             '  + 
        '            "emergency_full_name": "Shubham Suryawanshi",  '  + 
        '            "emergency_relationship": "Test",                                          '  + 
        '            "emergency_mobile_number": "",  '  + 
        '            "emergency_email_address": ""  '  + 
        '          }  '  + 
        '      ]  '  + 
        '  }  ' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/InsertTenancyRenewal';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(requestJson);
        RestContext.request = req;
        RestContext.response = res;
        
        TenancyRenewalAPI.apiMainMethod();
        
        Test.stopTest();
    }
}