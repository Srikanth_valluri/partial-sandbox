/**************************************************************************************************
* Name               : AgentPortalPropertyListControllerTest                                               
* Description        : An apex page controller for AgentPortalPropertyListController                                         
* Created Date       : Naresh Kaneriya                                                                        
* Created By         : 01/09/2017                                                                 
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         Naresh kaneriya         01/09/2017                                                           
**************************************************************************************************/
@isTest
public class AgentPortalPropertyListControllerTest {
     
    public static Contact adminContact;
    public static User portalUser;
    public static Account adminAccount;
    public static User portalOnlyAgent;
    
    static void init(){
        AgentPortalPropertyListController obj = new AgentPortalPropertyListController();
        obj.Notification = 'Test';
        obj.NotificationCount  = '4';
        
        AgentPortalPropertyListController.ProjectWrapper objProject =  new AgentPortalPropertyListController.ProjectWrapper('test Market' ,'test property' , 'test district' , 'test propertyId');
        objProject.marketingName = 'test Market';
        objProject.propertyName = 'test property';
        objProject.district = 'test district';
        objProject.propertyId = 'test propertyId';
        
       // objProject.ProjectWrapper(objProject.marketingName,objProject.propertyName,objProject.district,objProject.propertyId);
        
        adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        insert adminAccount;
        
        adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        insert adminContact;
        
        Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
        insert agentContact;
        
        portalUser = InitialiseTestData.getPortalUser('test@test.com', adminContact.Id, 'Admin');
        portalOnlyAgent = InitialiseTestData.getPortalUser('test1@test.com', agentContact.Id, 'Agent');
        
        System.runAs(portalUser){
           Property__c property = InitialiseTestData.insertProperties();
            InitialiseTestData.createInventoryUser(property);
        }
    }
    
    @isTest static void showProjects(){
        Test.startTest();
        init();
        System.runAs(portalUser){
            AgentPortalPropertyListController AgentPortalPropertyListController = new AgentPortalPropertyListController();
        }
        Test.stopTest();
    }
    
    @isTest static void filterProjects(){
        Test.startTest();
        init();
        
        System.runAs(portalUser){
            ApexPages.currentPage().getParameters().put('locationNamesSelected','\'Dubai\'');
            ApexPages.currentPage().getParameters().put('projectTypeSelected','\'Residential\'');
            ApexPages.currentPage().getParameters().put('BedroomsSelected','\'1\'');
            ApexPages.currentPage().getParameters().put('sfdc.tabName','\'1\'');
            ApexPages.currentPage().getParameters().put('Bedrooms','1');
            ApexPages.currentPage().getParameters().put('type','1');
            ApexPages.currentPage().getParameters().put('Location','1');
            AgentPortalPropertyListController AgentPortalPropertyListController = new AgentPortalPropertyListController();
            AgentPortalPropertyListController.filterProjects();
            
        }
        
        Test.stopTest();
    }
    
    
    
      @isTest static void getProjectListsTest(){
        Test.startTest();
        init();
        
         Address__c   addressDetail = InitialiseTestData.getAddressDetails(9086);
         addressDetail.Latitude__c = 'Latitude';
         addressDetail.Latitude__c = 'Longitude';
         insert addressDetail ; 
        
         Location__c loc =  InitialiseTestData.getLocationDetails('01p0Y000009neJwQAI' , 'Test');
         insert loc ;
         
         Property__c   propertyDetail = InitialiseTestData.getPropertyDetails(7650);
         insert propertyDetail ; 
         
         
        
        Inventory__c  inventory = InitialiseTestData.getInventoryDetails('3456','1234','2345',9086,7650); 
         inventory.Status__c = 'Released';
         inventory.Marketing_Name__c = 'Test'; 
         inventory.Address__c = addressDetail.Id; 
         inventory.Unit_Location__c = loc.Id;
         insert inventory ; 
        
         Set<Id> Ids =  new Set<Id>();
         Ids.add(inventory.Id);
        System.runAs(portalUser){
          
            AgentPortalPropertyListController obj = new AgentPortalPropertyListController();
            obj.getProjectLists(Ids);
            obj.getMarkers();  
            
        }
        
        Test.stopTest();
    }
    
}