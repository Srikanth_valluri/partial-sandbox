/*
----------------------------------------------------------------------
* Name               : phoneValidation
* Description        : This is to Call Nexmo WS to Validate Numbers.
* Created Date       : 17/01/2018                                                                
* Created By         : Alok Chauhan                                                       
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE                                                              
* 1.0         Alok Chauhan    17/01/2018 
--------------------------------------------------------------------------
*/
global class phoneValidation{
    public static String isValid;
    public static Map<String, Object> responseMap;
    public static Map<String, Object> sendNEXMOCallout(string phoneNum){
        try{
            
            String endPointURL = System.Label.NexmoURL;
            String apiKey = System.Label.NexmoAPI_Key;
            String apiSecret= System.Label.NexmoAPI_Secret;
            
            String requestURL = '{"api_key":"'+apiKey+'","api_secret":"'+apiSecret+'","number":"'+phoneNum+'"}';
            
            Blob finalVal = Blob.valueOf(requestURL);
            
            Httprequest request = new HttpRequest();
            Http http = new Http();
            
            request.setMethod('POST');
            request.setEndpoint(endPointURL);
            request.setHeader('Content-Type', 'application/json');
            request.setTimeout(120000); 
            request.setBody(requestURL);          
            System.debug(request);
            //Making call to external REST API
            HttpResponse response = http.send(request);  
            responseMap = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
            
            isValid=(String)responseMap.get('valid_number');
            
            System.debug('responseBody: '+response.getBody());
        }catch(Exception e){
            System.debug('Error::'+e.getMessage());
        }
        return(responseMap);
    }
}