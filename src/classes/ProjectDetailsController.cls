/*********************************************************************************************************
* Name               : ProjectDetailsController
* Test Class         : 
* Description        : Controller class for ProjectDetails VF Page
* Created Date       : 09/11/2020
* -----------------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         24/11/2020      Initial Draft.
**********************************************************************************************************/
global With Sharing class ProjectDetailsController {
    public String propertyId {get; set;}
    public Property__c property {get;set;}
    public string propDetails {get;set;}
    public string buildingDetails {get;set;}
    public string selectedInvName {get; set;}
    public string invLogDetailsJSON {get; set;}
    public Boolean logExists {get; set;}
	public Boolean terms {get; set;}    
    public string prjctName {get;set;}
    public string prjctId {get;set;}
    public string totalBuilding {get;set;}
    public string totalUnit {get;set;}
    public string totalInv {get;set;}
    public string totalSld {get;set;}
    public string totalFloor {get;set;}
    public string totalRet {get;set;}
    public string totalRes {get;set;}
    public string totalAval {get;set;}
    public string totalCancel {get;set;}
    public string floorDetails {get;set;}
    public string unitDetails {get;set;}
    public string inv {get;set;}
    public string minAreaStr {get;set;}
    public string maxAreaStr {get;set;}
    public string minCostStr {get;set;}
    public string maxCostStr {get;set;}
    public Decimal price {get;set;}
    public string bedRoom {get;set;}
    public string plotNo {get;set;}
    public string markNo {get;set;}
    public string markName {get;set;}
    public Boolean furnished {get;set;}
    public Boolean allowSell {get;set;}
    public string parking {get;set;}
    public string dewaNo {get;set;}
    public Decimal compPer {get;set;}
    public string storeRoom {get;set;}
    public Boolean isAccessible {get;set;}
    public Boolean shell {get;set;}
    public string unitType {get;set;}
    public string viewType {get;set;}
    public string bccDate {get;set;}
    public string municipalityNo {get;set;}
    public Inventory__c selectedInv {get;set;}
    public Inventory__c selectedInventory {get;set;}
    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public ProjectDetailsController() {
        propertyId = ApexPages.currentPage().getParameters().get('Id');
        terms = false;
        List<invLogDetailsWrapper> wrapperLogList = new List<invLogDetailsWrapper>();
        invLogDetailsJSON = JSON.serialize(wrapperLogList);
        List<BuildingDetailsWrapper> buildingWrapperList = new List<BuildingDetailsWrapper>();
        buildingDetails = JSON.serialize(buildingWrapperList);
        List<FloorDetailsWrapper> floorWrapperList = new List<FloorDetailsWrapper>();
        floorDetails = JSON.serialize(floorWrapperList);
        property__c property = new property__c();
        propDetails = JSON.serialize(property);
        Inventory__c invDetails  = new Inventory__c();
        inv = JSON.serialize(invDetails);
        
        if(propertyId != null && propertyId != ''){
            GetPropertyDetails(propertyId);
         }
    }
    
   /*********************************************************************************************
    * @Description : Method to get the Details of Property
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/    
    public void GetPropertyDetails(string propId){
        system.debug('propId'+propId);
       property = [SELECT id,CurrencyIsoCode,Property_Name__c, Property_City__c, District__c,RERA_Project__c,
                   Property_Country__c,Company__c,Area__c,UOM__c, Latitude__c, Longitude__c 
                   FROM Property__c Where id =: propId];
       system.debug('property'+property);
       prjctName = property.Property_Name__c;
       prjctId =property.id;
       propDetails = JSON.serialize(property);
       GetBuildingDetails(prjctName);
    }
    
    /*********************************************************************************************
    * @Description : Method to get the Details of Building
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/    
    public void GetBuildingDetails(string propName){
        system.debug('propName'+propName);
        Map<Id, Location__c> locationMap = new Map<Id, Location__c>();
        Map<Id, Location__c> locationFloorMap = new Map<Id, Location__c>();
        Map<Id, Boolean> locationPayPlanMap = new Map<Id, Boolean>();
        Map<Id, Boolean> propertyPayPlanMap = new Map<Id, Boolean>();
        Map<Id, Integer> locationFloorCountMap = new Map<Id, Integer>();
        Map<Id, List<Inventory__c>> locationInventoryMap = new Map<Id, List<Inventory__c>>();
        Map<Id,  List<Inventory__c>> floorInventoryMap = new Map<Id, List<Inventory__c>>();
        Map<Id, Boolean> locationPayPlanFloorMap = new Map<Id, Boolean>();
        List<BuildingDetailsWrapper> wrapperList = new List<BuildingDetailsWrapper>();
        Double minArea, maxArea, minCost, maxCost;
        Integer totalBuildings = 0;
        
        for(Location__c loc: [SELECT Id, Name, Building_Name__c 
                               FROM Location__c 
                               WHERE Location_Type__c = 'Building'
                               AND Property_Name__r.Property_Name__c =: propName]){
            locationMap.put(loc.Id, loc);
            locationPayPlanMap.put(loc.Id, false);
            totalBuildings++;
        }
        system.debug('locationMap: '+ locationMap);
        for(Payment_Plan_Association__c planAssoc : [SELECT Id, Payment_Plan__c, Location__c
                                                     FROM Payment_Plan_Association__c
                                                     WHERE Location__c =: property.Id]){
           terms = true;
        }
         for(Payment_Plan_Association__c planAssoc : [SELECT Id, Payment_Plan__c, Location__c
                                                     FROM Payment_Plan_Association__c
                                                     WHERE Location__c IN: locationMap.keyset()]){
            locationPayPlanMap.put(planAssoc.Location__c, true);
        }
        for(Location__c loc: [SELECT Id, Name, Building_Name__c , Building_Number__c,Floor_Name__c
                       FROM Location__c 
                       WHERE Location_Type__c = 'Floor'
                       AND Building_Number__c IN: locationMap.keyset()]){
            Integer count = 0;
            if(locationFloorCountMap.containsKey(loc.Building_Number__c)){
                count = locationFloorCountMap.get(loc.Building_Number__c);
            }
            count++;
            locationFloorCountMap.put(loc.Building_Number__c, count);
            locationFloorMap.put(loc.Id,loc); 
            locationPayPlanFloorMap.put(loc.Id, false);
        }
         for(Payment_Plan_Association__c planAssoc : [SELECT Id, Payment_Plan__c, Location__c
                                                     FROM Payment_Plan_Association__c
                                                     WHERE Location__c IN: locationFloorMap.keyset()]){
            locationPayPlanFloorMap.put(planAssoc.Location__c, true);
        }
        system.debug('locationFloorCountMap: '+ locationFloorCountMap);
        system.debug('locationFloorMap: '+ locationFloorMap);
        for(Inventory__c inv: [SELECT Id, Status__c, Building_Location__c ,Floor_Location__c,Area__c,Special_Price__c
                                FROM Inventory__c 
                                WHERE Building_Location__c IN: locationMap.keyset()]){
             List<Inventory__c> invList = new List<Inventory__c>();
             if(locationInventoryMap.containsKey(inv.Building_Location__c)){
                 invList = locationInventoryMap.get(inv.Building_Location__c);
             }
                                    
             invList.add(inv);
             locationInventoryMap.put(inv.Building_Location__c, invList);
             if(minCost == null){
                  minCost = inv.Special_Price__c;                     
             }
             if(maxCost == null){
                  maxCost = inv.Special_Price__c;                      
             }                                    
             if( minArea == null && inv.Area__c != null && Double.valueOf(inv.Area__c.removeEnd('SFT')) > 0){
                minArea = Double.valueOf(inv.Area__c.removeEnd('SFT'));
             }
             if( maxArea == null){
                maxArea = Double.valueOf(inv.Area__c.removeEnd('SFT'));
             }
            if(inv.Area__c != null && Double.valueOf(inv.Area__c.removeEnd('SFT')) < minArea
                        && Double.valueOf(inv.Area__c.removeEnd('SFT')) > 0){
                  minArea = Double.valueOf(inv.Area__c.removeEnd('SFT'));                      
            }
            if(inv.Area__c != null && Double.valueOf(inv.Area__c.removeEnd('SFT')) > maxArea){
                  maxArea = Double.valueOf(inv.Area__c.removeEnd('SFT'));                      
            }if(inv.Special_Price__c < minCost){
                  minCost =inv.Special_Price__c;                      
            }
            if(inv.Special_Price__c > maxCost){
                  maxCost = inv.Special_Price__c;                      
            }
         }
         minAreaStr = minArea.format();
         maxAreaStr = maxArea.format();
         minCostStr = minCost.format();
         maxCostStr = maxCost.format();
         for(Inventory__c inv: [SELECT Id, Status__c, Floor_Location__c 
                                FROM Inventory__c 
                                WHERE Floor_Location__c IN: locationFloorMap.keyset()]){
             List<Inventory__c> invList = new List<Inventory__c>();
             if(floorInventoryMap.containsKey(inv.Floor_Location__c)){
                 invList = floorInventoryMap.get(inv.Floor_Location__c);
             }
             invList.add(inv);
             floorInventoryMap.put(inv.Floor_Location__c, invList);    
         }
        system.debug('floorInventoryMap: '+ floorInventoryMap);
        system.debug('locationInventoryMap: '+ locationInventoryMap);
        SetFloorDetails(locationFloorMap,floorInventoryMap,locationPayPlanFloorMap);
        SetUnitDetails(propName);
        Integer totalUnits = 0;
        
        Integer totalSold = 0;
        Integer totalFloors = 0;
        Integer totalRestricted = 0;
        Integer totalRetired = 0;
        Integer totalAvailable = 0;
        Integer totalInventories = 0;
        Integer totalCancelBin = 0;
        for(Location__c loc: locationMap.values()){
             Integer units = 0;
             Integer inventory = 0;
             Integer available = 0;
             Integer sold = 0;
             Integer restricted = 0;
             Integer retired = 0;
             Integer floors = 0;
             Integer cancelBin = 0;
             Boolean payPlan = false;
             if(locationFloorCountMap.containsKey(loc.Id)){
                 floors = locationFloorCountMap.get(loc.Id);
             }
            if(locationPayPlanMap.containsKey(loc.Id)){
                 payPlan = locationPayPlanMap.get(loc.Id);
             }
            if(locationInventoryMap.containsKey(loc.Id)){
                 for(Inventory__c inv: locationInventoryMap.get(loc.Id)){
                     units++;
                     if(inv.Status__c == 'AVAILABLE'){
                         available++;
                     } else if(inv.Status__c == 'Inventory'){
                         inventory++;
                     } else if(inv.Status__c == 'Sold'){
                         sold++;
                     } else if(inv.Status__c == 'Restricted'){
                         restricted++;
                     }else if(inv.Status__c == 'Retired'){
                         retired++;
                     }else if(inv.Status__c == 'Cancel Bin'){
                         cancelBin++;
                     }
                 }
             }
            totalUnits += units;
            totalSold += sold;
            totalInventories += inventory;
            totalrestricted += restricted;
            totalretired += retired;
            totalavailable += available;
            totalFloors += floors;
            totalCancelBin += cancelBin;
            BuildingDetailsWrapper details = new BuildingDetailsWrapper();
            details.building = loc.Building_Name__c;
            details.buildingId = loc.Id;
            details.sold = sold;
            details.restricted = restricted;
            details.available = available;
            details.inventory = inventory;
            details.floors = floors;
            details.retired = retired;
            details.units = units;
            if(payPlan){
                details.plan = 'available';
            } else{
                details.plan = 'plan NOT available';
            }
            totalUnit = JSON.serialize(totalUnits);
            totalBuilding = JSON.serialize(totalBuildings);
            
            totalRes = Json.serialize(totalrestricted);
            totalRet = JSON.serialize(totalretired);
            totalSld = JSON.serialize(totalSold);
            totalInv = JSON.serialize(totalInventories);
            totalFloor = JSON.serialize(totalFloors);
            totalAval = JSON.serialize(totalavailable);
            totalCancel = JSON.serialize(totalCancelBin);
            wrapperList.add(details);
         }    
         buildingDetails = JSON.serialize(wrapperList);
        system.debug('buildingDetails: ' + buildingDetails);       
    }
    
    /*********************************************************************************************
    * @Description : Method to set the Details of Floor
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/  
    public void SetFloorDetails(Map<Id, Location__c> locationFloorMap,Map<Id,  List<Inventory__c>> floorInventoryMap,
                                Map<Id, Boolean> locationPayPlanFloorMap){
        List<FloorDetailsWrapper> wrapperList = new List<FloorDetailsWrapper>();
        system.debug('locationFloorMap'+locationFloorMap);
        system.debug('floorInventoryMap'+floorInventoryMap);
         for(Location__c loc: locationFloorMap.values()){
             Integer units = 0;
             Integer inventory = 0;
             Integer available = 0;
             Integer sold = 0;
             Integer restricted = 0;
             Integer retired = 0;            
             string status ='';
             Boolean payPlan = false;
             if(locationPayPlanFloorMap.containsKey(loc.Id)){
                 payPlan = locationPayPlanFloorMap.get(loc.Id);
             }
             if(floorInventoryMap.containsKey(loc.Id)){
                if(floorInventoryMap.get(loc.id) != null){
                 for(Inventory__c inv: floorInventoryMap.get(loc.Id)){
                     units++;
                     status = inv.Status__c;
                     if(inv.Status__c == 'AVAILABLE'){
                         available++;
                     } else if(inv.Status__c == 'Inventory'){
                         inventory++;
                     } else if(inv.Status__c == 'Sold'){
                         sold++;
                     } else if(inv.Status__c == 'Restricted'){
                         restricted++;
                     }else if(inv.Status__c == 'Retired'){
                         retired++;
                     }
                 }
                }
             }
             FloorDetailsWrapper floordetails = new FloorDetailsWrapper();
             floordetails.available = available;
             floordetails.inventory = inventory;
             floordetails.floorName = loc.Name;
             floordetails.restricted = restricted;
             floordetails.floorId = loc.Id;
             floordetails.sold = sold;
             floordetails.units = units;
             floordetails.status = status;
             floordetails.retired = retired;
             if(payPlan){
                  floordetails.plan = 'available';
             }else{
                 floordetails.plan = ' not available';
             }
             wrapperList.add(floordetails);
         }   
          
         floorDetails = JSON.serialize(wrapperList);
         system.debug('floorDetails: ' + floorDetails);
    }
    
    /*********************************************************************************************
    * @Description : Method to set the Details of Unit
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/ 
    public void SetUnitDetails(String propName){
         Map<Id, Location__c> locationMap = new Map<Id, Location__c>();
         Map<Id, Boolean> locationPayPlanMap = new Map<Id, Boolean>();
         Map<Id, Inventory__c> locationInventoryMap = new Map<Id, Inventory__c>();
         List<UnitDetailsWrapper> wrapperList = new List<UnitDetailsWrapper>();
         for(Inventory__c inv: [SELECT Id, Status__c, Unit_Location__c,Special_Price__c,Area__c,
                                Plot_Area_sft__c,Bedrooms__c, Bedroom_Type__c, Floor_2__c, Unit_Name__c
                                FROM Inventory__c 
                                WHERE property__r.Property_Name__c =: propName]){
             locationInventoryMap.put(inv.Unit_Location__c, inv);  
             locationPayPlanMap.put(inv.Unit_Location__c, false);  
         }
         for(Payment_Plan_Association__c planAssoc : [SELECT Id, Payment_Plan__c, Location__c
                                                     FROM Payment_Plan_Association__c
                                                     WHERE Location__c IN: locationInventoryMap.keyset()]){
             locationPayPlanMap.put(planAssoc.Location__c, true);
         }
         system.debug('locationInventoryMap: '+ locationInventoryMap);
        
         for(Inventory__c inv: locationInventoryMap.values()){
             Decimal gra = 0.0;
             Decimal plotArea = 0.0;
             string area = '';
             Decimal unitPrice = 0.0;
             Boolean payPlan = false;
             string status,floor = '';
             string bedRoom = '';
             string id = '';
             if(locationPayPlanMap.containsKey(inv.Unit_Location__c)){
                 system.debug('inv==@'+inv.Unit_Name__c);
                 payPlan = locationPayPlanMap.get(inv.Unit_Location__c);
                 system.debug('payPlan==@'+payPlan);
             }

            
             if(inv.Plot_Area_sft__c != null && inv.Plot_Area_sft__c  > 0){
                 plotArea = inv.Plot_Area_sft__c;
             }
             if(inv.Special_Price__c != null){
                 unitPrice = inv.Special_Price__c;
             }
             area = inv.Area__c;
             status = inv.Status__c;
             bedRoom = inv.Bedroom_Type__c;
             UnitDetailsWrapper details = new UnitDetailsWrapper();
             details.unitId = inv.Id;
             if(payPlan){
                 details.plan = 'available';
             }else{
                 details.plan = 'not-available';
             }
             if(bedRoom == '' || bedRoom == null){
                 bedRoom = '-';
             }
             if(status == '' || status == null){
                 status = '-';
             }
             if(floor == '' || floor == null){
                 floor = '-';
             }
             if(area == '' || area == null){
                 area = '-';
             }
             details.bedRoom = bedRoom;
             details.status = status;             
             details.floor = inv.Floor_2__c;
             details.plot_area = plotArea;
             details.unit_price = unitPrice.format();
             details.unit = inv.Unit_Name__c;
             details.area = area;
             details.locId = inv.Unit_Location__c;
             wrapperList.add(details);
         }    
         unitDetails = JSON.serialize(wrapperList);
         system.debug('unitDetails: ' + unitDetails);
    }

     /************************************************************************************************
    * @Description : method to fetch Inventory Log
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
    public void getInventoryLogDetails() {
        String invId = ApexPages.currentPage().getParameters().get('invId');
        selectedInvName  = ApexPages.currentPage().getParameters().get('selInvName1');
        system.debug('invId: ' + invId);
        system.debug('selectedInvName: ' + selectedInvName);
        logExists = false;
        List<invLogDetailsWrapper> wrapperLogList = new List<invLogDetailsWrapper>();
        for(Inventory_Log__c log: [select id, Name, Field_API_Name__c, Field_Name__c,
                            Inventory__c, User__c, Updated_Date__c, User__r.Name,
                            New_Value__c, Old_Value__c
                       FROM Inventory_Log__c 
                       WHERE Inventory__c =: invId
                       AND Log_Object__c = 'Inventory'
                       ORDER BY Updated_Date__c DESC]){
            invLogDetailsWrapper wrapper = new invLogDetailsWrapper();
            wrapper.attribute = log.Field_Name__c;
            wrapper.fromValue = log.Old_Value__c;
            wrapper.toValue = log.New_Value__c;
            wrapper.updated_on = log.Updated_Date__c.format();
            wrapper.updated_by = log.User__r.Name;
            if(wrapper.fromValue == ''){
                wrapper.fromValue = '-';
            }
            if(wrapper.toValue == ''){
                wrapper.toValue = '-';
            }
            wrapperLogList.add(wrapper);    
        }
        if(wrapperLogList.size() > 0){
            logExists = true;
        }
                   
        invLogDetailsJSON = JSON.serialize(wrapperLogList);
        system.debug('invLogDetailsJSON: ' + invLogDetailsJSON);
    }

    /*********************************************************************************************
    * @Description : Method to get the Details of Inventory
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/    
    public void FetchInventory(){  
       selectedInvName = '';
       String invenId = ApexPages.currentPage().getParameters().get('invId');
       system.debug('invenId'+invenId);
       selectedInv = [SELECT id,Unit_Name__c,Unit_Plan__c,Plot_Plan__c,Floor_Plan__c FROM Inventory__c WHERE id =: invenId] ;
       selectedInvName = selectedInv.Unit_Name__c;
       system.debug('inv'+selectedInv);
    }
    /*********************************************************************************************
    * @Description : Method to get the Details to Edit
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/    
    public void GetDetailsForEdit(){ 
        String unit = ApexPages.currentPage().getParameters().get('unitId');
        system.debug('unit'+unit);
         selectedInventory = [SELECT id, Special_Price__c, Plot_Number__c, Parking__c,
                                 Marketing_Name__c, Bedrooms__c, Bedroom_Type__c, Marketing_Number__c,
                                 DEWA_Number__c, Store_Room__c, Furnished__c, Shell_Core__c, 
                                 View_Type__c, Completed_Percentage__c, BccDate__c,
                                 Allowed_To_Sell__c, Type__c, Municipality_Number__c, 
                                 Accessible_Unit__c, Plot_Area_sft__c, Unit_Name__c,
                                 GFA_Area_sft__c, Area__c, Balcony_Area_sft__c, 
                                 Balcony_Area_JOPD_sft__c, Total_Area_JOPD_sft__c
                         FROM Inventory__c WHERE id =: unit];
        system.debug('inv'+selectedInventory);
        inv = JSON.serialize(selectedInventory);
        parking = selectedInventory.Parking__c;
        price = selectedInventory.Special_Price__c;
        municipalityNo = selectedInventory.Municipality_Number__c;
        storeRoom = selectedInventory.Store_Room__c;
        shell = selectedInventory.Shell_Core__c;
        markNo = selectedInventory.Marketing_Number__c;
        furnished = selectedInventory.Furnished__c;
        dewaNo = selectedInventory.DEWA_Number__c;
        plotNo = selectedInventory.Plot_Number__c;
        isAccessible = selectedInventory.Accessible_Unit__c;
        bedRoom = selectedInventory.Bedrooms__c;
        markName = selectedInventory.Marketing_Name__c;
        viewType = selectedInventory.View_Type__c;
        compPer = selectedInventory.Completed_Percentage__c;
        allowSell = selectedInventory.Allowed_To_Sell__c;
        if(selectedInventory.BccDate__c != null){
            bccDate = JSON.serialize(selectedInventory.BccDate__c);
        }
        system.debug('price'+price);
      
    }
    /*********************************************************************************************
    * @Description : Method to Update the Invetory
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/  
    public void UpdateInventoryDetails(){
      //String unit = ApexPages.currentPage().getParameters().get('unitId');
      system.debug('unit: ' + selectedInventory.Id);
      system.debug('price: ' + price);
      system.debug('plotNo: ' + plotNo);
      system.debug('municipalityNo: ' + municipalityNo);
      Inventory__c inv = [SELECT Id, Special_Price__c, Plot_Number__c, Parking__c,
                              Marketing_Name__c, Bedrooms__c, Marketing_Number__c,
                              DEWA_Number__c, Store_Room__c, Furnished__c, 
                              Shell_Core__c, View_Type__c, Completed_Percentage__c, 
                              Bedroom_Type__c, BccDate__c, Allowed_To_Sell__c, 
                              Type__c, Municipality_Number__c, Accessible_Unit__c, 
                              Plot_Area_sft__c, Unit_Name__c, GFA_Area_sft__c, 
                              Area__c, Balcony_Area_sft__c, Balcony_Area_JOPD_sft__c, 
                              Total_Area_JOPD_sft__c
                         FROM Inventory__c WHERE id =: selectedInventory.Id];
     inv.Special_Price__c = price;
     inv.Marketing_Name__c = markName;
     inv.Bedroom_Type__c = bedRoom;
     inv.Plot_Number__c = plotNo;
     inv.Completed_Percentage__c = compPer;
     inv.Marketing_Number__c = markNo;
     inv.Type__c = unitType;
     inv.Municipality_Number__c = municipalityNo; 
     inv.Accessible_Unit__c = isAccessible;
     inv.Shell_Core__c =  shell;
     inv.Store_Room__c = storeRoom;
     inv.DEWA_Number__c = dewaNo;
    // inv.BccDate__c = bccDate;
     inv.Allowed_To_Sell__c = allowSell;
     inv.View_Type__c = viewType;
     System.debug('inv'+inv);
     update inv;           
    }
      public class invLogDetailsWrapper{
        public string attribute;
        public string fromValue;
        public string toValue;
        public string updated_on;
        public string updated_by;
        public invLogDetailsWrapper(){}
    }
   /*********************************************************************************************
    * @Description : Wrapper class for Building Details
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/ 
    public class BuildingDetailsWrapper{
        public string building;
        public string buildingId;
        public string plan;
        public string floorName;
        public Integer units;
        public Integer inventory;
        public Integer available;
        public Integer sold;
        public Integer restricted;
        public Integer retired;
        public Integer floors;
        public BuildingDetailsWrapper(){
        }
    }
    
    /*********************************************************************************************
    * @Description : Wrapper class for Floor Details
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/ 
     public class FloorDetailsWrapper{
        
        public string floorId;
        public string plan;
        public string floorName;
        public Integer units;
        public Integer inventory;
        public Integer available;
        public Integer sold;
        public Integer restricted;
        public Integer retired;
        public Integer floors;
        public string  status;
        public FloorDetailsWrapper(){
        }
    }
    
    /*********************************************************************************************
    * @Description : Wrapper class for Unit Details
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/ 
     public class UnitDetailsWrapper{
        public string unit;
        public string unitId;
        public string plan;
        public string bedRoom;
        public string status;
        public string floor;
        public Decimal gra;
        public Decimal plot_area;        
        public string area;
        public string unit_price;
        public string locId;
        public UnitDetailsWrapper(){}
     }
}