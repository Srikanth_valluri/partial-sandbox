/************************************************************************************************
 * Description: Batch to send Emergency SMS to resident                                         *
 ===============================================================================================*
 * Ver      Date            Author                  Modification                                *
 *==============================================================================================*
 * 1.0                                              Initial Draft                               *
*************************************************************************************************/
public class EmergencySmsBatch implements Database.Batchable<sObject>
                                                                , Database.AllowsCallouts {

    static String user = '', passwd = '', strSID = '';
    List<Booking_Unit__c> lstBookingUnits;

    public EmergencySmsBatch(List<Booking_Unit__c> lstBU) {
        lstBookingUnits = new List<Booking_Unit__c>(lstBU);
    }

    public Database.Querylocator start( Database.BatchableContext bc ) {
        String query = 'SELECT Id, Emergency_SMS__c, Resident__r.Mobile_Phone_Encrypt__pc '
                    + ' From Booking_Unit__c '
                    + ' WHERE Id In : lstBookingUnits '
                    + ' AND Resident__c != null '
                    + ' AND Resident__r.Mobile_Phone_Encrypt__pc != null ';
        System.debug('Query = ' + query);
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<Booking_Unit__c> lstBU ) {
        System.debug('lstBU = ' + lstBU);
        List<SMS_History__c> smsLst = new List<SMS_History__c>();
        
        if( lstBU != null && lstBU.size() > 0 ) {
            for( Booking_Unit__c objBU : lstBU ) {
                String phoneNumber = objBU.Resident__r.Mobile_Phone_Encrypt__pc;
                System.debug('phoneNumber =-=' + phoneNumber );
                if(String.isNotBlank(phoneNumber)) {
                    //Call method to remove leading zeros from phone number
                    List<String> lstPhoneNumber = GenericUtility.removeLeadingZeros( new List<String>{phoneNumber} );
                    phoneNumber = lstPhoneNumber != null && lstPhoneNumber.size() > 0 
                                ? lstPhoneNumber[0]
                                :  phoneNumber;
                    System.debug('O excluded phoneNumber =-=' + phoneNumber );
                    String contentValue = 'Dear Resident,';
                    contentValue =contentValue + '\nThere is a fire in the building, please vaccate the building immediatly.';
                    contentValue =contentValue +'\n Regards\n';
                    contentValue =contentValue +'LOAMS';
                    
                    //Call method to send emergency sms
                    if( String.isNotBlank(phoneNumber) ){
                        HttpResponse res = EmergencySMSUtility.sendEmergencySms(phoneNumber, contentValue);
                        if( res.getBody() != null ) {
        
                            // Create SMS history
                            SMS_History__c smsObj = new SMS_History__c();
                            smsObj.Message__c = contentValue;
                            smsObj.Phone_Number__c = PhoneNumber;
                            smsObj.Customer__c = objBU.Resident__c;
                            smsObj.Booking_Unit__c = objBU.Id;
                            smsObj.Description__c = res.getBody();
                            System.debug('smsObj::::else:'+smsObj);
                                
                            if(String.valueOf(res.getBody()).contains('OK:')) {
                                smsObj.Is_SMS_Sent__c = true;
                                smsObj.sms_Id__c = res.getBody().substringAfter('OK:');
                            }
                            smsLst.add(smsObj);
                        }//End res if
                    }
                } 
            }//END For Loop

            if(smsLst.size() > 0 ) {
                insert smsLst;
            }
        }//END lstBU if
    }

    public void finish( Database.BatchableContext bc ) {
    }
}