@isTest
/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  23/11/2017  Initial Draft
*/
public class spaGenerationTest {
    
    static testMethod void Test1(){
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new spaGenerationMock());
               
        List<spaProcessXxdcProcessServiceWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5> lstObj = new List<spaProcessXxdcProcessServiceWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5>();
        spaProcessXxdcProcessServiceWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5 obj1 = new spaProcessXxdcProcessServiceWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5();
        lstObj.add(obj1);
        
        spaGeneration.SPAHttpSoap11Endpoint  obj = new spaGeneration.SPAHttpSoap11Endpoint ();
        String response = obj.processSPA('123','Test','SFDC',lstObj);
        System.debug('--response--'+response);
    }
    
}