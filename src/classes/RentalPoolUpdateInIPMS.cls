public without sharing class RentalPoolUpdateInIPMS{
    /*
    For Agreement RP send (CREATE_RP_RECORD, Y, Date, CaseNo, RegId)
    For Terminate RP send (TERMINATE_RP_RECORD,'Y', Date, CaseNo, RegId)
    */
    public static string rentalPoolUpdateCallout(String reqName, String flagVal, String dateVal, String caseNo, String regId){
        list<RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5> listDocAttributes 
          = new list<RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5>();
        RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5 docAttributeObj 
          = new RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5();
        docAttributeObj.ATTRIBUTE1 = flagVal;
        docAttributeObj.ATTRIBUTE2 = dateVal.toUpperCase();
        docAttributeObj.ATTRIBUTE3 = '2-'+caseNo;
        docAttributeObj.ATTRIBUTE4 = '';
        docAttributeObj.PARAM_ID = regId;
        system.debug('docAttributeObj==='+docAttributeObj);
        listDocAttributes.add(docAttributeObj);
        system.debug('listDocAttributes==='+listDocAttributes);
        RentalPoolAgreementDocumentWSDL.RPAHttpSoap11Endpoint rentalObj = new RentalPoolAgreementDocumentWSDL.RPAHttpSoap11Endpoint();
        rentalObj.timeout_x = 120000;
        system.debug('rentalObj==='+rentalObj);
        system.debug('reqNumber==='+String.valueOf(System.currentTimeMillis()));
        String rentalDocResponse = rentalObj.RentalPoolAgreement(String.valueOf(System.currentTimeMillis()),reqName,'SFDC',listDocAttributes);
        system.debug('rentalDocResponse==='+rentalDocResponse);
        return rentalDocResponse;
    }
}