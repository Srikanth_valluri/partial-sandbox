/********************************************************************************************************************
    Description : Batch to send survey email and sms
    ---------------------------------------------------------------------------------------------------------------------
    Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
    ---------------------------------------------------------------------------------------------------------------------
    1.0     | 19-02-2020        |Aishwarya Todkar    | Initial Draft

    1.1     | 28-04-2020        |Aishwarya Todkar    | Updated query     
    *********************************************************************************************************************/
    global with sharing class CallLogSurveyBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
            
        static String templateName;

        global Database.QueryLocator start(Database.BatchableContext BC) {
            
            //Get CRE profiles from custom settings
            Set<String> crePrfileSet = new Set<String>();
            for( CRE_Profiles__c cs : CRE_Profiles__c.getAll().values() ) {
                if( cs.Process_Name__c.equalsIgnoreCase( 'Call Log Survey' ) )
                    crePrfileSet.add( cs.Name );
            }

            String query =  'SELECT '
                            + ' Id '
                            + ', Account__c '
                            + ', Account__r.Name '
                            + ', Account__r.Person_Business_Email__c '
                            + ', Account__r.Mobile_Person_Business__c '
                            + ', Called_Number__c '
                            + ', Calling_Number__c ' 
                            + ', Call_Recording_URL__c ' 
                            + ', Call_Type__c '
                            + ', OwnerId'
                            + ', Owner.Name '
                            + ' FROM '
                            + ' Call_Log__c '
                            + ' WHERE '
                            + ' CRM_Survey_Sent__c = false '
                            + ' AND Account__c != null'
                            + ' AND CRE_Profile_Id__c != null'
                            + ' AND Called_Number__c != null'
                            + ' AND Calling_Number__c != null'
                            + ' AND Call_Type__c != null'
                            + ' AND ( Call_Type__c = \'Inbound\' OR Call_Type__c = \'Outbound\')';
                            
                if( !Test.isRunningTest() ) {
                    query += ' AND CreatedDate = ' + Label.Call_Log_Created_Date;
                    if( crePrfileSet != null && crePrfileSet.size() > 0  ) {
                        query += ' AND CRE_Profile_Id__c IN:crePrfileSet';
                    }
                }

                if( String.isNotBlank ( Label.Record_Id_To_Test ) 
                && Label.Record_Id_To_Test.toLowerCase().contains('on') 
                && String.isNotBlank( Label.Record_Id_To_Test.substringAfter('-') ) 
                && Label.Record_Id_To_Test.substringAfter('-').startsWith( AttachmentHandlerUploadDocument.keyPrefix( 'Call_Log__c' ))
                && !Test.isRunningTest() ) {
                    query = query + ' AND Id = \'' + Label.Record_Id_To_Test.substringAfter('-') + '\'  ';
                }
            system.debug('crePrfileSet >>> ' + crePrfileSet); 
            system.debug('query >>>> ' + query); 
            return Database.getQueryLocator(query);
        }

        global void execute(Database.BatchableContext BC,List<Call_Log__c> callLogList) {
            System.debug( 'callLogList size- ' + callLogList.size() );
            System.debug( 'callLogList - ' + callLogList);
            List<EmailMessage> lstEmails = new List<EmailMessage>();
            List<SMS_History__c> listSMS = new List<SMS_History__c>();
            List<Call_Log__c> listCallLogsToUpdate = new List<Call_Log__c>();
            

            if( !callLogList.isEmpty() ) {
                List<EmailTemplate> listEmailTemplate ;
                SendGrid_Email_Details__mdt objSendGridCs;
                List<SendGrid_Email_Details__mdt> listSendGridDetails = CRM_SurveyEmailsSender.getSendGridDetails( 'Call Log Survey' );
                if( listSendGridDetails != null &&  listSendGridDetails.size() > 0 ) {
                    
                    objSendGridCs = listSendGridDetails[0];
                    System.debug( 'objSendGridCs:::  ' + objSendGridCs);
                    //Get Email Template
                    listEmailTemplate = new List<EmailTemplate>( [SELECT 
                                                                                            Id
                                                                                            , Name
                                                                                            , Subject
                                                                                            , Body
                                                                                            , HtmlValue
                                                                                            , TemplateType
                                                                                            , DeveloperName
                                                                                        FROM 
                                                                                            EmailTemplate 
                                                                                        WHERE 
                                                                                            Name =: objSendGridCs.Email_Template__c 
                                                                                        LIMIT 1 
                                                                                    ] );
                    system.debug('listEmailTemplate  === ' + listEmailTemplate );
            
                }
                for( Call_Log__c objLog : callLogList ) {
                    
                    try {

                        //Sending survey emails
                        if( listEmailTemplate != null && listEmailTemplate.size() > 0 ) {
                    
                            lstEmails.addAll( sendSurveyEmail( objLog,  listEmailTemplate[0] ,objSendGridCs ));
                        }

                        //Sending survey sms
                        listSMS.add( sendSurveySMS( objLog ) );

                        objLog.CRM_Survey_Sent__c = true;
                        listCallLogsToUpdate.add( objLog );
                    }
                    catch( Exception e) {
                        System.debug( 'Exception -- ' + e.getMessage());
                    }

                } //End callLogList loop

                if( lstEmails != null && lstEmails.size() > 0 ) {
                    if( !Test.isRunningTest() ) {
                        database.insert ( lstEmails );
                    }
                }
                if( listSMS != null && listSMS.size() > 0 ) {
                    if( !Test.isRunningTest() ) {
                        database.insert ( listSMS );
                    }
                }
                if( listCallLogsToUpdate != null && listCallLogsToUpdate.size() > 0 ) {
                    database.update( listCallLogsToUpdate );
                }
            }
        }
    /*********************************************************************************************************************
    * Description  : Method to send survey emails
    * Parameter(s) : Call Log Record
    * Return       : List<EmailMessage>
    *********************************************************************************************************************/
        public static List<EmailMessage> sendSurveyEmail( Call_Log__c objLog
                                                            , EmailTemplate emailTemplateObj
                                                            , SendGrid_Email_Details__mdt objSendGridCs) {
        
            if( emailTemplateObj != null ) {
                
                templateName =  emailTemplateObj.DeveloperName;

                String toAddress = objLog.Account__r.Person_Business_Email__c;

                String toName = String.isNotBlank( objSendGridCs.To_Name__c ) ? objSendGridCs.To_Name__c : '';

                String ccAddress = String.isNotBlank( objSendGridCs.CC_Address__c ) ? objSendGridCs.CC_Address__c : '';

                String ccName = String.isNotBlank( objSendGridCs.CC_Name__c ) ? objSendGridCs.CC_Name__c : '';

                String bccAddress = String.isNotBlank( objSendGridCs.Bcc_Address__c ) ? objSendGridCs.Bcc_Address__c : '';

                String bccName = String.isNotBlank( objSendGridCs.Bcc_Name__c ) ? objSendGridCs.Bcc_Name__c : '';

                String fromAddress = String.isNotBlank( objSendGridCs.From_Address__c ) ? objSendGridCs.From_Address__c : '';

                String fromName = String.isNotBlank( objSendGridCs.From_Name__c ) ? objSendGridCs.From_Name__c : '';

                String replyToAddress = String.isNotBlank( objSendGridCs.Reply_To_Address__c ) ? objSendGridCs.Reply_To_Address__c : '';

                String replyToName = String.isNotBlank( objSendGridCs.Reply_To_Name__c ) ? objSendGridCs.Reply_To_Name__c : '';

                String contentType = 'text/html';
                
                String subject = emailTemplateObj.Subject;

                String contentValue = CRM_SurveyEmailsSender.replaceMergeFields( 
                                                        emailTemplateObj.HtmlValue      
                                                        , objLog.Id                     // Record Id
                                                        , objLog.OwnerId                // CRE Id
                                                        , objLog.Account__c             // Account Id
                                                        , objLog.Owner.Name             // CRE Name
                                                        , objLog.Account__r.Name        // Customer Name
                                                        , templateName                  // Template Name
                                                        );

                String contentBody = CRM_SurveyEmailsSender.replaceMergeFields( 
                                                        emailTemplateObj.Body   
                                                        , objLog.Id                     // Record Id
                                                        , objLog.OwnerId                // CRE Id
                                                        , objLog.Account__c             // Account Id
                                                        , objLog.Owner.Name             // CRE Name
                                                        , objLog.Account__r.Name        // Customer Name
                                                        , templateName                  // Template Name
                                                        );
                
                List<EmailMessage> lstEmails;
                //Call method to send an email via SendGrid
                if(toAddress != '') {
                            
                    lstEmails = CRM_SurveyEmailsSender.sendEmailsBySendGrid( toAddress, toName
                                                                            , ccAddress, ccName
                                                                            , bccAddress, bccName
                                                                            , subject, ''
                                                                            , fromAddress, fromName
                                                                            , replyToAddress, replyToName
                                                                            , contentType, contentValue
                                                                            , '', contentBody
                                                                            , null, objLog.Account__c );
                    return lstEmails;
                }
            }                                                             
            return null;
        }

    /*********************************************************************************************************************
    * Description  : Method send survey SMS
    * Parameter(s) : Call Log Record
    * Return       : None
    *********************************************************************************************************************/
        public static SMS_History__c sendSurveySMS( Call_Log__c objLog ) {
            String siteUrl = CRM_SurveyEmailsSender.replaceMergeFields( 
                                                                    Label.CRM_Survey_Site_Url   // Site Url
                                                                    , objLog.Id                 // Record Id
                                                                    , objLog.OwnerId            // CRE Id
                                                                    , objLog.Account__c         // Account Id
                                                                    , objLog.Owner.Name         // CRE Name
                                                                    , objLog.Account__r.Name    // Customer Name
                                                                    , templateName              // Template Name
                                                                    );

            String shortUrl; 
            if( !Test.isRunningTest() ) {
                RebradnlyResponseWrapper objResWrap = RebrandlyService.getShortenUrl( siteUrl );     
                shortUrl = objResWrap.shortUrl;
            }
            else {
                shortUrl = 'www.google.com';
            }
            String smsContents = Label.Call_Log_Survey_SMS;
            String PhoneNumber = objLog.Account__r.Mobile_Person_Business__c;
            if( PhoneNumber.startsWith( '00' ) ) {
                PhoneNumber = PhoneNumber.removeStart( '00' );
            }
            else if( PhoneNumber.startsWith( '0' ) ) {
                PhoneNumber = PhoneNumber.removeStart( '0' );
            }
            smsContents = smsContents.replace('{Survey Link}', shortUrl);
            String userName = Label.Damac_CRM_SMS_Service_User_name;
            String password = Label.Damac_CRM_SMS_Service_Password;
            //String strSID = Label.Damac_CRM_SMS_Service_Sender_Id;
            String strSID = SMSClass.getSenderName(userName, phoneNumber, false);
    
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            req.setMethod('POST' ); // Method Type
            req.setEndpoint( Label.SMSCountryAPIBaseURL ); 
            string msgCont = GenericUtility.encodeChar( smsContents );
            system.debug('msgCont = ' + msgCont);
            
            
            req.setBody('user='+ userName + '&passwd=' + password +'&message=' 
                        + msgCont + '&mobilenumber=' + phoneNumber 
                        + '&sid='+ strSID + '&MTYPE=LNG&DR=Y'); // Request Parameters
            res = http.send(req);
            system.debug('req Body---'+req.getBody());
            system.debug('Response---'+res);
            system.debug('Response body---'+res.getBody());
            
            if( res.getBody() != null ){
                    
                // Parse Response
                SMS_History__c smsObj = new SMS_History__c();
                smsObj.Message__c = smsContents;
                smsObj.Phone_Number__c = phoneNumber;
                smsObj.Customer__c = objLog.Account__c ;
                //smsObj.Booking_Unit__c = objBU.Id;
                smsObj.Description__c = res.getBody();

                if(String.valueOf(res.getBody()).contains('OK:')){
                    smsObj.Is_SMS_Sent__c = true;
                    smsObj.sms_Id__c = res.getBody().substringAfter('OK:');
                }
                System.debug('smsObj::::else:'+smsObj);
                return smsObj;
                
            }
            return null;
        }

        global void finish(Database.BatchableContext BC){

        }
    }