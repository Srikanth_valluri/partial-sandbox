@isTest
private class getFileWhatsappTest {

	@TestSetup
    static void makeData(){
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
    }

    @isTest
    static void testMethod1() {	
    	Test.startTest();
    	Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositiveForWhtsApp());
    	PageReference currentPage = Page.getFileWhatsapp;
    	currentPage.getParameters().put('id', '01NUEFKAQZLBYOSPWUERG2WXC7FPDB23C2');
    	Test.setCurrentPage(currentpage);
    	getFileWhatsapp objCntrlr = new getFileWhatsapp();

    	Test.Stoptest();

    }
}