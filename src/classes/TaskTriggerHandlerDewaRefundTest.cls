@isTest
public class TaskTriggerHandlerDewaRefundTest {
    @isTest
    public static void testMethod1(){
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        Case objCase = new Case();
        objCase.Account = acc;
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Type = 'Offer & Acceptance Letter';  
        objCase.Account_Email__c = 'test@gmail.com';
        objCase.Refund_Amount__c = 1400;
        insert objCase;
        
        String Label = Label.Book_Refund_Amount_As_Per_Inputs_From_Claim;
        
        Task objTask = new Task();
        objTask.Subject = 'Calculate/Verify Refund Amount Bills Unpaid';
        objTask.WhatId = objCase.Id;
        objTask.OwnerId = userinfo.getUserId();
        objTask.Status = 'Closed';
        objTask.Priority = 'Normal';
        insert objTask;
        List<Task> lstTask = new List<Task>();
        lstTask.add(objTask);
        Test.startTest();
         TaskTriggerHandlerDewaRefund triggerHandlerDewa = new TaskTriggerHandlerDewaRefund();
         triggerHandlerDewa.validationForRefundAmount(lstTask);
        Test.stopTest();
    }
    
        @isTest
    public static void testMethod2(){
    
     String Label = Label.Book_Refund_Amount_As_Per_Inputs_From_Claim;
     
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        Case objCase = new Case();
        objCase.Account = acc;
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Type = 'Offer & Acceptance Letter';  
        objCase.Account_Email__c = 'test@gmail.com';
        //objCase.Refund_Amount__c = '';
        insert objCase;
        
        SR_Attachments__c objSr = new SR_Attachments__c();
        objSr.Case__c = objCase.Id;
        objSr.Name = 'DEWA Bill';
        insert objSr;
        
        Task objTask = new Task();
        objTask.Subject = 'Attach DEWA Bills and Ejari/Bank Documents';
        objTask.WhatId = objCase.Id;
        objTask.OwnerId = userinfo.getUserId();
        objTask.Status = 'Completed';
        objTask.Priority = 'Normal';
        insert objTask;
        
        List<Task> lstTask = new List<Task>();
        lstTask.add(objTask);
        Test.startTest();
         TaskTriggerHandlerDewaRefund triggerHandlerDewa = new TaskTriggerHandlerDewaRefund();
         triggerHandlerDewa.uploadDocsValidation(lstTask);
        Test.stopTest();
    }
    
        @isTest
    public static void testMethod3(){
     String Label = Label.Book_Refund_Amount_As_Per_Inputs_From_Claim;
     
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        Case objCase = new Case();
        objCase.Account = acc;
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Type = 'Offer & Acceptance Letter';  
        objCase.Account_Email__c = 'test@gmail.com';
        //objCase.Refund_Amount__c = '';
        insert objCase;
        
        SR_Attachments__c objSr = new SR_Attachments__c();
        objSr.Case__c = objCase.Id;
        objSr.Name = 'DEWA Bill';
        insert objSr;
        map<Id,Task> insertMap = new map<Id,Task>();
        map<Id,Task> updateMap = new map<Id,Task>();

        Task tsk = new Task();
        tsk.Subject = System.Label.Pay_Customer_Disburse_Cheque_Fund_Transfer_DEWA;
        tsk.WhatId = objCase.Id;
        tsk.OwnerId = userinfo.getUserId();
        tsk.Status = 'Not Started';
        tsk.Priority = 'Normal';
        insert tsk;
        Task tsk1 = new Task();
        tsk1.Subject = System.Label.Book_Refund_Amount_As_Per_Inputs_From_Claim;
        tsk1.WhatId = objCase.Id;
        tsk1.OwnerId = userinfo.getUserId();
        tsk1.Status = 'Not Started';
        tsk1.Priority = 'Normal';
        insert tsk1;
        tsk.Status='Completed';
        update tsk;
    }
}