Global class DAMAC_AsyncSendSMS implements Queueable, Database.AllowsCallouts {


    public ID smsCampaignId;
    public String objectName;
    
    public DAMAC_AsyncSendSMS (ID campaignId, String objName) {
        objectName = objName;
        smsCampaignId = campaignId;
    }
    
    public void execute(QueueableContext context) {
        try {
            sendSMS (smsCampaignId, objectName);
        } catch (Exception e) {
        
        }
    }
    
    
    
    public void sendSMS (ID smsCampaingId, String objectName) {
        Secure_SMS__mdt smsCredentials = [SELECT User_Name__c, password__c, 
                                        Endpoint__c, sender_name__c,
                                        Asynchronous_Submission__c 
                                        FROM Secure_SMS__mdt 
                                        WHERE DeveloperName='SMS_BULKSMS' LIMIT 1];
        
        Campaign_SMS__c smsObj = [SELECT SMS_Message__c, Scheduled_Time_for_SMS_Request__c FROM Campaign_SMS__c WHERE id =: smsCampaignId LIMIT 1];
        
        String endpointURL = smsCredentials.Endpoint__c+'username='+smsCredentials.User_Name__c+'&password='+smsCredentials.password__c;
        String reqBody = generateRequestBody (smsCampaingId, objectName, smsCredentials, smsObj);
        System.Debug (reqBOdy);
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();        
        req.setEndpoint (endpointURL); 
        req.setMethod ('POST');
        req.setheader ('Content-Type','application/json'); 
        req.setbody (reqBody);  
        HttpResponse res = new HttpResponse ();
        
        if (!Test.isRunningTest ()) {
            res = h.send(req);
        }
        if (Test.isRunningTest ()) {
            res.setStatusCode(200);
            res.setBody('Success');
        } 
        
        String smsResponse = res.getBody(); 
        if (res.getstatuscode() == 200){
            createLog (smsObj.SMS_Message__c, smsResponse, true, smsCampaingId);
        } else {
            createLog (smsObj.SMS_Message__c, smsResponse, false, smsCampaingId);
        }
        
    }
    
    public static String generateRequestBody (String smsCampaignId,
                                             String objectName, 
                                             Secure_SMS__mdt smsCredentials,
                                             Campaign_SMS__c smsObj) {
        List <String> mobileNumbersList = new List <String> ();
        
        
        Attachment att = new Attachment ();
        att = [ SELECT Body FROM Attachment WHERE ParentID =: smsCampaignId Order By CreatedDate Desc LIMIT 1];
        String body = att.body.toString ();
                                                 if (Test.isRunningTest()) {
                                                     body = 'test,';
                                                 }
        if (body.contains (',')) {
            Integer nextIndex = 0;
            String val = '';
            Integer count = body.countMatches (',');
            for (integer i = 0; i <= count; i++) {                
                if (count == i) {
                    val = body.substringAfterLast (',');
                } else {
                    if (nextIndex != 0) { 
                        val = body.substring (nextIndex, body.indexOf (',', nextIndex));
                    }
                    else {
                        val = body.substring (nextIndex, body.indexOf (','));
                    }
                }
                try {
                    if (val != '')
                        mobileNumbersList.add (UtilityHelperCls.decryptMobile(val));    
                } catch (Exception e) {}
                nextIndex = nextIndex + val.length ()+1;
            } 
            
        }
        
        else {            
            String finalVal = '';
            if (!Test.isRunningTest ()) {
                try {
                    finalVal = UtilityHelperCls.decryptMobile(body);
                }
                catch (Exception e) {}
            }
            mobileNumbersList.add (finalVal);
        }
        
        System.Debug (mobileNumbersList.size ());
        
        DAMAC_SMS_REQUEST_OBJ reqObj = new DAMAC_SMS_REQUEST_OBJ ();
        reqObj.Message = smsObj.SMS_Message__c;
        reqObj.SenderName = smsCredentials.sender_name__c;
        reqObj.ScheduledDate = smsObj.Scheduled_Time_for_SMS_Request__c;
        reqObj.RemoveDuplicates = true;
        if (String.isNotBlank(objectName)) {
            reqObj.ReferenceName = objectName+'_'+String.valueof(smsObj.Scheduled_Time_for_SMS_Request__c);
        }
        else {
            reqObj.ReferenceName = 'MARKETING_'+String.valueof(smsObj.Scheduled_Time_for_SMS_Request__c);
        }
        reqObj.ReturnIndividualResponse = false;
        reqObj.AsynchronousSubmission = smsCredentials.Asynchronous_Submission__c;
        
        String requestBody = '';
        
        // Add admin number by default
        if (label.Campaign_SMS_Admin_Number != null){
            mobileNumbersList.add(label.Campaign_SMS_Admin_Number);
        }
        reqObj.MobileNumbers = mobileNumbersList;
        
        requestBody = JSON.serialize (reqObj);
        System.Debug (requestBody);
        return requestBody;
    }
    
    public void createLog (String message, String smsResponse, Boolean success, Id smsCampaignId){
        DAMAC_SMS_Get_BulkId obj = new DAMAC_SMS_Get_BulkId ();
        String bulkid = '';
        if (!Test.isRunningTest()) {
            obj = DAMAC_SMS_Get_BulkId.parse (smsResponse);
            bulkid = String.valueOf (obj.Data.BulkId);
        }   
        
        Campaign_sms__c smsLog = [SELECT Error_From_Provider__c, SMS_Response__c, SMS_Message__c, SMS_Bulk_Id__c, SMS_Queued__c 
                                FROM Campaign_sms__c WHERE id =: smsCampaignId];
        smsLog.SMS_Response__c = smsResponse;
        smsLog.SMS_Message__c = message;
        smslog.SMS_Bulk_Id__c = bulkid;
        
        if (success) 
            smslog.SMS_Queued__c = success;
        else 
            smslog.Error_From_Provider__c = false;        
        
        update smsLog;
        
        Attachment att = new Attachment ();
        att = [ SELECT Id FROM Attachment WHERE ParentID =: smsCampaignId];
        try {
            DAMAC_Constants.skip_AttacmentTrigger = true;
            delete att;
        }
        catch (Exception e) {}
        //if (bulkid != null && bulkid != '')
        //System.enqueueJob (new Async_Rest_SMS_Job_Status(bulkid));
        
    }
}