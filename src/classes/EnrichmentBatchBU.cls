/****************************************************************************************
Description: Batch Class to call Enrichment Adfolks service from BU.
----------------------------------------------------------------------------------------
Version     Date           Author               Description                                 
1.0         12/10/2020     Arjun Khatri         Initial Draft
*****************************************************************************************/
public class EnrichmentBatchBU implements Database.Batchable<sObject>,Database.AllowsCallouts {

    public string sQuery;
    
    /* Method Description : start method of Batch to query all records 
    */    
    public Database.QueryLocator start(Database.BatchableContext BC){
        String strQuery = '';
        if(sQuery != null){
            strQuery = sQuery;
        }else{
            strQuery = 'SELECT Id, Booking__r.Account__c,Booking__r.Account__r.Person_Business_Email__c'+
                    ' FROM Booking_Unit__c' +
                    ' WHERE Booking__r.Account__r.Party_ID__c != NULL' +
                    ' AND Booking__r.Account__r.Active_Customer__c != NULL'+
                    ' AND Booking__r.Account__r.Active_Customer__c = \'Active\''+
                    ' AND Unit_Active__c = \'Active\'';
            if( String.isNotBlank ( Label.EnrichmentBatchBUPropName ) ) {
                strQuery = strQuery + ' AND Inventory__r.Property_Name_2__c = \''+Label.EnrichmentBatchBUPropName+ '\'  ';
            }
        }
        
         System.debug('strQuery == ' + strQuery);
         return Database.getQueryLocator(strQuery);
    }
    
    
    /* Method Description : Execute method of Batch to call Enrichment Adfolks 
    *                       service 
    */
    public void execute(Database.BatchableContext BC, List<Booking_Unit__c> scope){
       for ( Booking_Unit__c objBooking_Unit : scope ) {
           if ( objBooking_Unit.Booking__r.Account__c != null && objBooking_Unit.Booking__r.Account__r.Person_Business_Email__c != null) {
               EnrichmentService.sendHttpRequest(objBooking_Unit.Booking__r.Account__c, objBooking_Unit.Booking__r.Account__r.Person_Business_Email__c);
           }
           
       }
    }
    
    
    /* Method Description : Finish method of Batch.
    */
    public void finish(Database.BatchableContext BC){
    }
}