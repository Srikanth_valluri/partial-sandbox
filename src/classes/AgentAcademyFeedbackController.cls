/************************************************************************************************
 *
 * @Name              : AgentAcademyFeedbackController 
 * @Test Class Name   : AgentAcademyFeedbackControllerTest
 * @Description       : An apex page controller that supports Agents to submit their Feedback
 * @Date-DD/MM/YYYY   : 31/12/2020
 * @Author            : Bigworks Team
 *
***********************************************************************************************/

public class AgentAcademyFeedbackController {

    public User loggedInUser{get; set;}
    public Agent_Feedback__c agentFeedback { get; set; }
        
    public list<SelectOption> trainersOptions{get; set;}
    
    public String warningMessage { get; set; }
    public String successMessage { get; set; }

    public AgentAcademyFeedbackController (){
        loggedInUser = [Select id, Account.Name,accountId,Name from User where id =: UserInfo.getUserId()];
        successMessage = '';
        agentFeedback = new Agent_Feedback__c();
        agentFeedback.Agency__c = loggedInUser.AccountId;
        agentFeedback.Agent__c = loggedInUser.Id;
        
        trainersOptions = new list <SelectOption>();
        trainersOptions.add (new SelectOption ('','-Select-'));
        List<String> trainerIds = Label.Agent_Trainers.split(',');
        for(User eachUser : [SELECT Name FROM User WHERE Id IN: trainerIds ]){
            trainersOptions.add(new SelectOption(eachUser.Id, eachUser.Name));            
        }
    }
    
    public void submitFeedback(){
        warningMessage = '';
        successMessage = '';
        try {
            insert agentFeedback ;
            successMessage = 'Your Feedback was submitted';
        } catch(Exception e) {
           warningMessage = e.getMessage(); 
        }
        
    }
   
}