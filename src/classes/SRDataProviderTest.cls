@isTest
private class SRDataProviderTest {
    @isTest
    static void testGetData() {
        DataDisplayConfig config = new DataDisplayConfig();
        config.recordId = NULL;
        config.fieldList = new List<Map<String, String>> {
            new Map<String, String> {
                'field'=> 'Id',
                'title'=> 'Id'
            }
        };
        config.detailFieldList = new List<Map<String, String>> {
            new Map<String, String> {
                'field'=> 'CaseNumber',
                'title'=> 'CaseNumber'
            }
        };
        config.objectName = 'Case';
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        System.runAs(communityUser) {
            new SRDataProvider().getData(config);
        }
    }
}