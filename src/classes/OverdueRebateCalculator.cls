/********************************************************************************************************
* Description - Class to calculate and apply rebate on units                                            *
*                                                                                                       *
* Version   Date            Author              Description                                             *
* 1.0       13/11/2019      Aishwarya Todkar    Initial Draft.                                          *  
* 1.1       29/12/2019      Aishwarya Todkar    Removed currently paid criteria                         *
* 1.3       02/01/2020      Aishwarya Todkar    Added logic to calculate rebate for                     *
                                                BSW and ABZ units                                       *
* 1.4       14/01/2020      Aishwarya Todkar    Added logic to apply same rebate for                    * 
                                                all units as requested by Aditya                        *
* 1.5       11/05/2020      Aishwarya Todkar    Added more (6% & 8% ) promotions.                       *
*********************************************************************************************************/
Public Class OverdueRebateCalculator  {

    static Decimal outstandingAmount;
    static Decimal requestedPrice;
    static Decimal initialPaidAmount;
    static Decimal totalPaidAmount;
    static Decimal totalPaidPercentage;
    static Decimal currentlyPaidPercent;
    static Decimal initialPaidPercentage;

/*********************************************************************************
 * Method Name : applyRebateOnUnits
 * Description : method to apply rebate on units
 * Return Type : void
 * Parameter(s): List of Case Id
**********************************************************************************/
    //@invocableMethod
    public static Boolean applyRebateOnUnits( List<Id> listCaseId ) {
        if( listCaseId != null && listCaseId.size() > 0 ) {
            List<Case> listCases = new List< Case >( 
                                            [ SELECT
                                                    Id
                                                    , Initial_Paid__c
                                                    , Overdue_Amount__c
                                                    , Initial_Paid_Amount__c
                                                    , Paid_After_offer__c
                                                    , Paid_Amount_After_offer__c
                                                    , Final_Rebate_Amout_Applied__c
                                                    , Booking_Unit__r.Unit_Name__c
                                                    , Booking_Unit__r.Overdue_Rebate_Applied_Category__c
                                                    , Booking_Unit__r.Requested_Price_AED__c
                                                    , Booking_Unit__r.Registration_ID__c
                                                    , Booking_Unit__r.Registration_DateTime__c
                                                    , Booking_Unit__r.Anticipated_Completion_Date__c
                                                    , Booking_Unit__r.Inventory__r.Property_Country__c
                                                    , Booking_Unit__r.Inventory__r.Master_Community_EN__c
                                                    , Promotion_Type__c
                                                    , Rebate_Applied_Category__c
                                                FROM
                                                    Case
                                                WHERE
                                                    recordType.Name = 'Overdue Rebate/Discount'
                                                AND 
                                                    Id =: listCaseId[0]
                                                AND
                                                    Booking_Unit__c != null
                                                AND
                                                    Initial_Paid__c != null
                                                AND
                                                    Booking_Unit__c != null
                                                    /*Booking_Unit__r.Allow_Overdue__c = true
                                                AND
                                                    Booking_Unit__r.Inventory__c != null
                                                AND
                                                    Booking_Unit__r.Inventory__r.Master_Community_EN__c != null*/
                                                AND
                                                    Booking_Unit__r.Requested_Price_AED__c != null
                                                AND
                                                    Booking_Unit__r.Registration_ID__c != null
                                            ] );
            if( listCases.size() > 0 ) {
                calculateCategoryOfCase( listCases[0] );
                return true;
            }
            else {
                //FinanceTaskClosureCtrl objFinanceTask = new FinanceTaskClosureCtrl();
                //objFinanceTask.showDetails = false;
                ApexPages.addMessage( 
                        new ApexPages.Message(ApexPages.Severity.ERROR
                                            , 'Unit not found.Please make sure unit is allowed for overdue rebate.')
                );
                return false;
            }
        }
        return false;
    }

/*********************************************************************************
 * Method Name : calculateCategoryOfCase
 * Description : Calculate category of Case
 * Return Type : void
 * Parameter(s): Case
**********************************************************************************/
    public static void calculateCategoryOfCase( Case objCase ) {
        if( objCase != null 
        && objCase.Overdue_Amount__c != null 
        && objCase.Initial_Paid_Amount__c != null) {

            outstandingAmount = Decimal.valueOf( objCase.Overdue_Amount__c );
            initialPaidAmount = objCase.Initial_Paid_Amount__c;
            totalPaidAmount = outstandingAmount + initialPaidAmount;
            requestedPrice = objCase.Booking_Unit__r.Requested_Price_AED__c;
            totalPaidPercentage = ( totalPaidAmount / requestedPrice ) * 100;
            currentlyPaidPercent  =  ( outstandingAmount / requestedPrice ) * 100;
            initialPaidPercentage = objCase.Initial_Paid__c;

            System.debug('outstandingAmount='+outstandingAmount);
            System.debug('initialPaidAmount='+initialPaidAmount);
            System.debug('totalPaidAmount'+totalPaidAmount);
            System.debug('requestedPrice='+requestedPrice);
            System.debug('totalPaidPercentage='+ totalPaidPercentage);
            System.debug('currentlyPaidPercent='+ currentlyPaidPercent);

           Boolean applySamePromotion = false;

            if( objCase.Promotion_Type__c != null ) {
                
                System.debug('objCase.Promotion_Type__c='+ objCase.Promotion_Type__c);
                //Same Promotion for all
                Map<String, String> mapOverdueConfigurations = getOverdueConfigurations();
                if( mapOverdueConfigurations != null
                && mapOverdueConfigurations.size() > 0 ) {
                    
                    /*String PromotionType =  mapOverdueConfigurations.containsKey( 'Promotion% For All')
                                    ? mapOverdueConfigurations.get( 'Promotion% For All' ) : '';
                    System.debug('Config PromotionType='+ PromotionType);*/
                    Set<String> setPromotionType = new Set<String>();
                    for( Collection_Promotion_Config__mdt promotion : [ SELECT
                                                                            Discount_Category__c
                                                                        FROM
                                                                            Collection_Promotion_Config__mdt
                                                                        WHERE
                                                                            Process_Name__c = 'Old Overdue Rebate/Discount'] ) {
                        setPromotionType.add( promotion.Discount_Category__c.toLowerCase());
                    }
                    Boolean PromotionFlag = mapOverdueConfigurations.containsKey( 'Promotion Flag')
                                            && mapOverdueConfigurations.get( 'Promotion Flag' ).equalsIgnoreCase( 'Y' )
                                            ? true : false ;
                    System.debug('PromotionFlag==' + PromotionFlag);
                    /*applySamePromotion = PromotionType.equalsIgnoreCase( objCase.Promotion_Type__c) 
                                        && PromotionFlag ? true : false;*/
                    applySamePromotion = setPromotionType.contains( objCase.Promotion_Type__c.toLowerCase() ) 
                                        && PromotionFlag ? true : false;
                    System.debug('applySamePromotion==' + applySamePromotion);
                }
            }

            if( applySamePromotion ) {
                applySameRebate( objCase );
            } 
            else {
                if( /*totalPaidPercentage >= 50 
                && */outstandingAmount != null
                && initialPaidAmount != null
                && totalPaidAmount != null
                && totalPaidPercentage != null
                && initialPaidPercentage != null ) {

                    if( objCase.Booking_Unit__r.Unit_Name__c.toLowerCase().startsWith('bsw') 
                    || objCase.Booking_Unit__r.Unit_Name__c.toLowerCase().startsWith('abz') ) {

                        //Basswood and Albizia unit
                        calculateRebateForBSW_ABZ( objCase );
                    }
                    else if( objCase.Booking_Unit__c != null
                    && objCase.Booking_Unit__r.Inventory__c != null
                    && objCase.Booking_Unit__r.Inventory__r.Master_Community_EN__c != null
                    && objCase.Booking_Unit__r.Inventory__r.Master_Community_EN__c.equalsIgnoreCase( 'AKOYA OXYGEN' ) ) {
                        
                        //Akoya Units
                        calculateRebateForAkoya( objCase );
                    }
                    else if( objCase.Booking_Unit__c != null
                    && objCase.Booking_Unit__r.Inventory__c != null
                    && objCase.Booking_Unit__r.Inventory__r.Master_Community_EN__c != null
                    && !objCase.Booking_Unit__r.Inventory__r.Master_Community_EN__c.equalsIgnoreCase( 'AKOYA OXYGEN' )
                    && ( objCase.Booking_Unit__r.Inventory__r.Property_Country__c.equalsIgnoreCase( 'United Arab Emirates' ) 
                    || objCase.Booking_Unit__r.Inventory__r.Property_Country__c.equalsIgnoreCase( 'UAE' ) ) ) {
                        
                        //Non Akoya Units
                        calculateRebateForNonAkoya( objCase );
                    }
                    else {
                        //To Do
                    }
                }
            }//End Else
        }
    }
/*******************************************************************************************************************************
 * Method Name : applySameRebate
 * Description : Applies same rebate for all units
 * Return Type : void
 * Parameter(s): Case
********************************************************************************************************************************/
    public static void applySameRebate( Case objCase ) {
        System.debug( 'applySameRebate==');
        List<Collection_Promotion_Config__mdt> listConfig = 
            new List<Collection_Promotion_Config__mdt>( [ SELECT
                                                            Discount_Category__c
                                                            , Discount__c
                                                        FROM
                                                            Collection_Promotion_Config__mdt
                                                        WHERE
                                                            Discount_Category__c =: objCase.Promotion_Type__c
                                                    ] );
        if( listConfig.size() > 0 ) {
            Decimal rebatePercentage = listConfig[0].Discount__c;
            String rebateCategory = listConfig[0].Discount_Category__c;
            Decimal rebateAmount =  outstandingAmount  * ( rebatePercentage / 100 );
            updateCase( objCase, currentlyPaidPercent, rebatePercentage, rebateAmount, rebateCategory, null );
        }
        /*Map<String, String> mapOverdueConfigurations = getOverdueConfigurations();
        if( mapOverdueConfigurations != null
            && mapOverdueConfigurations.size() > 0 ) {
            Decimal rebatePercentage = mapOverdueConfigurations.containsKey( 'Promotion% For All ')
                            ? Decimal.valueOf( mapOverdueConfigurations.get( 'Promotion% For All' ) ) : 5;

            String rebateCategory = mapOverdueConfigurations.containsKey( 'Promotion% For All Category' )
                            ? mapOverdueConfigurations.get( 'Promotion% For All Category' )
                            :'5%RebateForAllUnits';

            Decimal rebateAmount =  outstandingAmount  * ( rebatePercentage / 100 );
            System.debug('rebatePercentage==' + rebatePercentage);
            System.debug('rebateAmount==' + rebateAmount);
            System.debug('rebateCategory==' + rebatePercentage);
            updateCase( objCase, currentlyPaidPercent, rebatePercentage, rebateAmount, rebateCategory, null );
        }*/
    }

/*********************************************************************************
 * Method Name : calculateRebateForAkoya
 * Description : Calculates rebate for Akoya Oxygen units
 * Return Type : void
 * Parameter(s): Case
**********************************************************************************/
    public static void calculateRebateForAkoya( Case objCase ) {

        /*UnitDetailsService.BookinUnitDetailsWrapper objBUDetailWrapper = 
            calloutToGetUnitDetails( objCase.Booking_Unit__r.Registration_ID__c);*/
        Map<String, String> mapOverdueConfigurations = getOverdueConfigurations();
        System.debug( 'Akoya unit = ' );
        if( objCase != null
        && objCase.Booking_Unit__c != null 
        && objCase.Booking_Unit__r.Anticipated_Completion_Date__c != null
        && mapOverdueConfigurations != null
        && mapOverdueConfigurations.size() > 0 ) {
            
            Decimal rebatePercentage;
            Decimal rebateAmount;
            String rebateCategory = '';
            Decimal rebatePercentage_optional;
            Date startDate = Date.newInstance( 2019, 7, 1 );
            Date endDate = Date.newInstance( 2021, 12, 31 );
            Date anticipatedDate = objCase.Booking_Unit__r.Anticipated_Completion_Date__c;
            anticipatedDate = Date.newInstance( anticipatedDate.year(), anticipatedDate.month(), anticipatedDate.day());
            System.debug( 'startDate = ' + startDate );
            System.debug( 'endDate = ' + endDate );
            System.debug( 'anticipatedDate = ' + anticipatedDate );
            
            if( anticipatedDate > startDate && anticipatedDate < endDate ) { 
                System.debug( 'between anticipatedDate = ' );
            }
            else{
                System.debug( 'not between anticipatedDate = ' );
            }
            if( anticipatedDate > startDate && anticipatedDate < endDate ) {  
                if( initialPaidPercentage != null 
                && initialPaidPercentage > 20 && initialPaidPercentage <= 50 ) {

                    System.debug( 'Rebate 12% or 15%');

                    Decimal threshold = mapOverdueConfigurations.containsKey( 'Requested Price Threshold' ) 
                                        ? Decimal.valueOf( mapOverdueConfigurations.get('Requested Price Threshold') )
                                        : 1300000;  
                    //12% / 15%
                    Decimal rebate_12_Per = mapOverdueConfigurations.containsKey( 'Rebate % 3' ) 
                                            ? Decimal.valueOf( mapOverdueConfigurations.get( 'Rebate % 3' ) )
                                            : 12 ;
                    Decimal rebate_15_Per = mapOverdueConfigurations.containsKey( 'Rebate % 4' )
                                            ? Decimal.valueOf( mapOverdueConfigurations.get( 'Rebate % 4' ) )
                                            : 15 ;
                    
                    rebatePercentage = requestedPrice <=  threshold ? rebate_12_Per : rebate_15_Per;

                    rebateCategory = rebatePercentage == rebate_12_Per 
                                    ? mapOverdueConfigurations.containsKey( 'AKOYA - 12%' ) 
                                    ? mapOverdueConfigurations.get('AKOYA - 12%') 
                                    : 'AO-9Sep2019Memo-Lessthan1.3mn –upto50%-Rebate 12%' 
                                    : mapOverdueConfigurations.containsKey( 'AKOYA - 15%' ) 
                                    ? mapOverdueConfigurations.get('AKOYA - 15%')
                                    : 'AO-9Sep2019Memo-Morethan1.3mn –upto50%-Rebate 15%';
                }
            }
            else if( initialPaidPercentage != null 
                && initialPaidPercentage < 30 ) {
                //3%
                System.debug( 'Rebate 3%');
                if( objCase.Booking_Unit__r.Registration_DateTime__c != null ) {
                    
                    DateTime dT = objCase.Booking_Unit__r.Registration_DateTime__c;
                    Date regDate = date.newinstance( dT.year(), dT.month(), dT.day() );

                    system.debug('daysBetween = ' + regDate.daysBetween(system.today()));
                    if( regDate.daysBetween( system.today() ) > 180  ) {
                        rebatePercentage = mapOverdueConfigurations.containsKey( 'Rebate % 1' ) 
                                        ? Decimal.valueOf( mapOverdueConfigurations.get( 'Rebate % 1' ) )
                                        : 3;
                        rebateCategory = mapOverdueConfigurations.containsKey( 'AKOYA - 3%' ) 
                                        ? mapOverdueConfigurations.get('AKOYA - 3%') 
                                        : 'AO-29May2019Memo-Rebate 3%';
                    }
                }
            }
            System.debug( 'rebatePercentage = ' + rebatePercentage);
            
            if( rebatePercentage != null ) {
                if( totalPaidPercentage <= 50 ) {
                    //Decimal afterOfferPaidAmount = outstandingAmount * ( currentlyPaidPercent/100 );
                    //System.debug( 'afterOfferPaidAmount== ' + afterOfferPaidAmount);
                    rebateAmount = outstandingAmount * ( rebatePercentage / 100 );
                }
                else {
                    Decimal targetPercent = mapOverdueConfigurations.get('Target 1') != null ? Decimal.valueOf( mapOverdueConfigurations.get('Target 1') ) : 50;
                    Decimal rebateOnAmount = requestedPrice * ( ( targetPercent - initialPaidPercentage ) / 100 );
                    rebateAmount = rebateOnAmount *  ( rebatePercentage / 100 );
                }
            }//End rebatePercentage if
            System.debug( 'rebateAmount = ' + rebateAmount);

            if( rebateAmount != null ) {
                updateCase( objCase, currentlyPaidPercent, rebatePercentage, rebateAmount, rebateCategory, rebatePercentage_optional );
            }
        }//End objCase if
    }

/*********************************************************************************
 * Method Name : calculateRebateForNonAkoya
 * Description : Calculates rebate for Non Akoya Oxygen units of UAE
 * Return Type : void
 * Parameter(s): Case
**********************************************************************************/
    public static void calculateRebateForNonAkoya( Case objCase ) {
        
        System.debug( ' ==== Non Akoya === ');
        String rebateCategory = '';
        Decimal rebatePercentage;
        Decimal rebateAmount;
        Decimal rebatePercentage_optional;


        Map<String, String> mapOverdueConfigurations = getOverdueConfigurations();

        if( objCase != null 
        && objCase.Booking_Unit__c != null
        && objCase.Booking_Unit__r.Registration_DateTime__c != null 
        && mapOverdueConfigurations != null
        && mapOverdueConfigurations.size() > 0 ) {
 
            DateTime dT = objCase.Booking_Unit__r.Registration_DateTime__c;
            Date regDate = date.newinstance( dT.year(), dT.month(), dT.day() );

            system.debug('daysBetween = ' + regDate.daysBetween(system.today()));
            
            if( regDate.daysBetween( system.today() ) > 180 ) {
                system.debug(' more than 180 days overdue');
                
                if( initialPaidPercentage < 20 ) {
                    System.debug( 'Currently paid less than 20 --- ' );
                    if( totalPaidPercentage < 40 ) {
                        System.debug( 'total less than 40 --- ' );
                        rebatePercentage = Decimal.valueOf( mapOverdueConfigurations.get( 'Rebate % 1' ) );
                        rebateAmount =  outstandingAmount * ( rebatePercentage / 100 );
                        rebateCategory = mapOverdueConfigurations.containsKey( 'Non AKOYA- Not Achieving Target - 1' ) 
                                        ? mapOverdueConfigurations.get( 'Non AKOYA- Not Achieving Target - 1' )
                                        : 'NonAOUAE-Lessthan20%paid-Not Achieving Target40%-Rebate 3%';
                    }
                    else {

                        System.debug( 'total more than 40 --- ' );
                        rebatePercentage =  mapOverdueConfigurations.containsKey( 'Rebate % 2') 
                                                    ? Decimal.valueOf( mapOverdueConfigurations.get( 'Rebate % 2' ) )
                                                    : 5; //5%

                        rebatePercentage_optional = mapOverdueConfigurations.containsKey( 'Rebate % 1') 
                                                    ? Decimal.valueOf( mapOverdueConfigurations.get( 'Rebate % 1' ) )
                                                    : 3;//3%
                        Decimal rebateAmountOn_5,rebateAmountOn_3;
                        rebateAmountOn_5 = (requestedPrice * ( ( 40 - initialPaidPercentage ) / 100 )) * 0.05;
                        System.debug( 'totalPaidPercentage=='+totalPaidPercentage);
                        if( totalPaidPercentage > 50 ) {

                            rebateAmountOn_3 = ( requestedPrice * 0.1 ) * 0.03;
                        }
                        else {
                            rebateAmountOn_3 =  requestedPrice* ((totalPaidPercentage - 40 )/100);// ( ( requestedPrice * 0.5 ) - ( requestedPrice * 0.4 ) ) * 0.03;
                        }
                        System.debug('rebateAmountOn_5 - ' +rebateAmountOn_5);
                        System.debug('rebateAmountOn_3 - ' +rebateAmountOn_3);
                        //-----------
                        /*Decimal rebateFor_5 =  40 - initialPaidPercentage;
                        Decimal rebateAmountOn_5 = ( outstandingAmount * ( rebateFor_5 /100 ) ) * ( rebatePercentage / 100 );

                        Decimal rebateFor_3 =  totalPaidPercentage < 50 ? currentlyPaidPercent - rebateFor_5 : 10;
                        Decimal rebateAmountOn_3 = ( outstandingAmount * ( rebateFor_3 /100 ) ) * ( rebatePercentage_optional / 100 );*/

                        rebateAmount = rebateAmountOn_5 + rebateAmountOn_3;
                        rebateCategory = mapOverdueConfigurations.containsKey( 'Non AKOYA- Not Achieving Target - 2' ) 
                                        ? mapOverdueConfigurations.get( 'Non AKOYA- Not Achieving Target - 2' )
                                        : 'NonAOUAE-Lessthan20%paid-Achieving Target40%-Rebate 5%till40%-Rebate 3%from40%to50%';
                    }
                }
                else if( initialPaidPercentage >= 20 && initialPaidPercentage <= 40) {
                    System.debug( 'Currently paid between 20 to 40 --- ' );
                    rebatePercentage = mapOverdueConfigurations.containsKey( 'Rebate % 1') 
                                        ? Decimal.valueOf( mapOverdueConfigurations.get( 'Rebate % 1' ) )
                                        : 3;//3%
                    rebateAmount =  outstandingAmount  * ( rebatePercentage / 100 );
                    rebateCategory = mapOverdueConfigurations.containsKey( 'Non AKOYA-Achieving Target - 1' ) 
                                        ? mapOverdueConfigurations.get( 'Non AKOYA-Achieving Target - 1' )
                                        : 'NonAOUAE-20%to40%paid-Rebate3%';
                }

                System.debug( 'rebateAmount = ' + rebateAmount);
                System.debug( 'rebatePercentage = ' + rebatePercentage);
                
                if( rebateAmount != null  && rebatePercentage != null ) {
                        updateCase( objCase, currentlyPaidPercent, rebatePercentage, rebateAmount, rebateCategory, null );
                }
            }//End 180 days overdue if
        }//End objCase if
    }

/*********************************************************************************
 * Method Name : calculateRebateForBSW_ABZ
 * Description : Calculates rebate for Non Akoya Oxygen units of UAE
 * Return Type : void
 * Parameter(s): Case
**********************************************************************************/
    public static void calculateRebateForBSW_ABZ( Case objCase ) {

        String rebateCategory = '';
        Decimal rebatePercentage;
        Decimal rebateAmount;
        Decimal rebateTarget;
        Map<String, String> mapOverdueConfigurations = getOverdueConfigurations();

        System.debug('Basswood and Albizia Units ');

        if( mapOverdueConfigurations != null && mapOverdueConfigurations.size() > 0 
        && mapOverdueConfigurations.containsKey('BSW/ABZ Paid % Target') 
        && String.isNotBlank( mapOverdueConfigurations.get('BSW/ABZ Paid % Target') )
        && mapOverdueConfigurations.containsKey('BSW/ABZ Rebate %') 
        && String.isNotBlank( mapOverdueConfigurations.get('BSW/ABZ Rebate %') ) ) {

            rebatePercentage = Decimal.valueOf( mapOverdueConfigurations.get( 'BSW/ABZ Rebate %' ) );
            rebateTarget = Decimal.valueOf( mapOverdueConfigurations.get('BSW/ABZ Paid % Target') );
            rebateCategory = mapOverdueConfigurations.containsKey('BSW/ABZ Category')? mapOverdueConfigurations.get('BSW/ABZ Category'): '' ;

            if( initialPaidPercentage <= rebateTarget ) {
                rebateAmount = requestedPrice * ( rebatePercentage / 100 );
            }
            if( rebateAmount != null  && rebatePercentage != null ) {
                updateCase( objCase, currentlyPaidPercent, rebatePercentage, rebateAmount, rebateCategory, null );
            }
        }
    }

   /* public static UnitDetailsService.BookinUnitDetailsWrapper calloutToGetUnitDetails( String regId ) {
        
        if( String.isNotBlank( regId ) ) {

            try {
                
                UnitDetailsService.BookinUnitDetailsWrapper objBUDetailWrapper = new UnitDetailsService.BookinUnitDetailsWrapper();
                objBUDetailWrapper = UnitDetailsService.getBookingUnitDetails( regId );
                
                if( objBUDetailWrapper != null ) {
                    return objBUDetailWrapper;
                }
                system.debug('objBUDetailWrapper response - ' + objBUDetailWrapper);
            }
            catch ( Exception e ){
                    
                    //create error log
                //lstResObj.ATTRIBUTE13 = 'Exception : ' + e.getMessage() ;
            }
            return null;
        }
        return null;
    }
*/

/*********************************************************************************
 * Method Name : updateCase
 * Description : Updates calculated values on Case and Booking Unit
 * Return Type : void
 * Parameter(s): Case, currently Paid Percent, rebate Percentage
                , rebate Percentage(optional), rebate Category, rebate Amount
**********************************************************************************/
    public static void updateCase( Case objCase
                                , Decimal currentlyPaidPercent
                                , Decimal rebatePercentage
                                , Decimal rebateAmount
                                , String rebateCategory 
                                , Decimal rebatePercentage_optional ) {
        
        System.debug( 'Updating case and bu ');
        if( objCase != null 
        && rebatePercentage != null 
        && rebateAmount != null
        && currentlyPaidPercent != null 
        && String.isNotBlank( rebateCategory ) ) {

            objCase.Overdue_Discount__c = rebatePercentage.setScale(3);
            objCase.Rebate_Amount__c = rebateAmount.setScale(3);
            objCase.Paid_After_offer__c = currentlyPaidPercent.setScale(3);
            objCase.Rebate_Applied_Category__c = rebateCategory;
            if( rebatePercentage_optional != null )
                objCase.Overdue_Discount_Optional__c = rebatePercentage_optional;

            Booking_Unit__c objBu = new Booking_Unit__c( Id = objCase.Booking_Unit__c
                                             , Overdue_Rebate_Applied_Category__c = rebateCategory);
            update objCase;
            update objBu;
        }
    }

/*********************************************************************************
 * Method Name : getOverdueConfigurations
 * Description : To get configured details from custom setting
 * Return Type : Map
 * Parameter(s): None
**********************************************************************************/
    public static Map<String, String> getOverdueConfigurations() {
        Map< String, String > mapOverdueConfigurations 
            = new Map< String, String>();
        for(Overdue_Rebate_Configuration__mdt objRebateConfg : [ SELECT
                                                                    Id
                                                                    , DeveloperName
                                                                    , MasterLabel
                                                                    , Value__c
                                                                FROM
                                                                    Overdue_Rebate_Configuration__mdt ] ) {
            mapOverdueConfigurations.put( objRebateConfg.MasterLabel, objRebateConfg.Value__c );
        }
        System.debug('mapOverdueConfigurations==' + mapOverdueConfigurations);
        return mapOverdueConfigurations;
    }
}//End Of Class