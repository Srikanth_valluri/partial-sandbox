global class DailyBatchforEmailsScheduler implements Schedulable {

  public DailyBatchforEmailsScheduler() {}

  public DailyBatchforEmailsScheduler (String jobName) {
    System.schedule(jobName, '0 0 * * * ?', new DailyBatchforEmailsScheduler ());
  }
  public void execute(SchedulableContext sc) {
    Database.executebatch(new DailyBatchforEmails(), 90);
  }
}