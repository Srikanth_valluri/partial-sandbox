public class FirstFlightService {
    public static AirwayBillTrackListResponse getFirstFlightTracking(String strTrackingAWB, String accountNo, String country){
        Credentials_Details__c creds = Credentials_Details__c.getInstance( 'First Flight Tracking' );
        JSON2ApexTrackingInput objJSON2Apex = new JSON2ApexTrackingInput();
        //objJSON2Apex.TrackingAWB = '123';
        //objJSON2Apex.AccountNo = 'test';
        //objJSON2Apex.Country = 'India';
        String countryCode = '';
        countryCode = getLabel(country);
        String body = '{ "TrackingAWB": "'+strTrackingAWB+'", "AccountNo": "'+accountNo+'", "Country": "'+countryCode+'", "UserName": "'+creds.User_Name__c+'", "Password": "'+creds.Password__c+'" }';
        System.debug('JSObodyNData: '+body);
        if(body != Null){
            HTTPResponse resp = returnResponseFromService(creds.Endpoint__c, 'POST', creds.User_Name__c, creds.Password__c, body); //h.send(request);
            AirwayBillTrackListResponse responseObj = ( AirwayBillTrackListResponse )System.JSON.deserialize(resp.getBody(), AirwayBillTrackListResponse.class);
            system.debug('responseObj ---->'+responseObj);
            if(resp.getStatusCode() != 200){
                Error_Log__c objError = new Error_Log__c(Service_Request_Body__c = body, Service_Response_Body__c = resp.getBody());
                Insert objError;
            }
            return responseObj;
        }
        return null;
    }
    
    public static List<Courier_Delivery__c> generateAirwayBill(List<Courier_Delivery__c> couriList){
        Credentials_Details__c creds = Credentials_Details__c.getInstance( 'First Flight Create AirwayBill');
        List<Courier_Delivery__c> lstCourierToInsert = new List<Courier_Delivery__c>();
        List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
        List<Id> lstAccIds = new List<Id>();
        List<Id> caseIds = new List<Id>();
        //List<Case> cases = new List<Case>();
        map<Id, Case> MapIdToCaseToUpdate = new Map<Id, Case>();
        List<Id> buIds = new List<Id>();
        //List<Booking_Unit__c> BUs = new List<Booking_Unit__c>();
        Map<Id, Booking_Unit__c> mapIdToBookingUnitsToUpdate = new Map<Id, Booking_Unit__c>();
        system.debug('couriList in callout method--->'+couriList);
        for(Courier_Delivery__c objCourier: couriList){
            lstAccIds.add(objCourier.Account__c);
            caseIds.add(objCourier.Case__c);
            buIds.add(objCourier.UnitName__c);
        }
        Map<Id, Account> mapAccount = new Map<Id, Account>([SELECT Id, Name, Mobile_Person_Business__c, Person_Business_Email__c, Party_ID__c FROM Account WHERE Id IN: lstAccIds]);
        Map<Id, Case> mapCase = new Map<Id, Case>([SELECT Id, Courier_Request_Already_Created__c FROM Case WHERE Id IN: caseIds]);
        Map<Id, Booking_Unit__c> mapBookingUnit = new Map<Id, Booking_Unit__c>([SELECT Id,Courier_Request_Already_Created__c,Unit_Name__c FROM Booking_Unit__c WHERE Id IN: buIds]);
        List<Courier_Sender_Details__mdt> listSD = new List<Courier_Sender_Details__mdt>();
        listSD = [SELECT MasterLabel
                            , Address__c
                            , Email_Address__c
                            , Person_Name__c
                            , Phone__c
                            , PostCode__c
                            , Company__c
                            , City__c
                       FROM Courier_Sender_Details__mdt
                       WHERE MasterLabel =: 'Sender Details' LIMIT 1];
        for(Courier_Delivery__c objCourier: couriList){
            String partyId = '';
            String emailId = '';
            String mobile = '';
            String cityCode = '';
            String countryCode = '';
            partyId = mapAccount.get(objCourier.Account__c).Party_ID__c;
            emailId = mapAccount.get(objCourier.Account__c).Person_Business_Email__c;
            mobile = mapAccount.get(objCourier.Account__c).Mobile_Person_Business__c;
            cityCode = getLabel(objCourier.City__c);
            countryCode = getLabel(objCourier.Country__c);
            system.debug('cityCode---->'+cityCode);
            AirwayBillData objAirwayBillData = new AirwayBillData();
            
            objAirwayBillData.AirWayBillCreatedBy = '';
            objAirwayBillData.CODAmount = '0';
            objAirwayBillData.CODCurrency = '';
            objAirwayBillData.Destination = cityCode;
            objAirwayBillData.DutyConsigneePay = '0';
            System.debug('objCourier.Unit_Name__c>>'+objCourier.Unit_Name__c);
            System.debug('objCourier.Type_of_Document__c>>'+objCourier.Type_of_Document__c);
            System.debug('mapBookingUnit>>'+mapBookingUnit);
            objAirwayBillData.GoodsDescription =  String.isNotBlank(objCourier.Unit_Names__c)&& String.isNotBlank(objCourier.Type_of_Document__c)?objCourier.Unit_Names__c+'-'+objCourier.Type_of_Document__c:'';
            objAirwayBillData.NumberofPeices = objCourier.Number_of_Pieces__c != null ? Integer.valueOf(objCourier.Number_of_Pieces__c) : 0;
            objAirwayBillData.Origin = cityCode;
            objAirwayBillData.ProductType = 'DOX';
            objAirwayBillData.ReceiversAddress1 = objCourier.Address_1__c;
            objAirwayBillData.ReceiversAddress2 = '';
            objAirwayBillData.ReceiversCity = objCourier.City__c != null ? objCourier.City__c : '';
            objAirwayBillData.ReceiversSubCity = '';
            objAirwayBillData.ReceiversCountry = objCourier.Country__c != null ? objCourier.Country__c : '';
            objAirwayBillData.ReceiversCompany = mapAccount.containsKey(objCourier.Account__c) != null ? mapAccount.get(objCourier.Account__c).Name : '';
            objAirwayBillData.ReceiversContactPerson = mapAccount.containsKey(objCourier.Account__c) != null ? mapAccount.get(objCourier.Account__c).Name : '';
            objAirwayBillData.ReceiversEmail = emailId != null ? emailId : '';
            objAirwayBillData.ReceiversGeoLocation = '';
            objAirwayBillData.ReceiversMobile = mobile != null ? mobile : '';
            objAirwayBillData.ReceiversPhone = objCourier.Phone_1__c != null ? objCourier.Phone_1__c : '';
            objAirwayBillData.ReceiversPinCode = objCourier.Postal_Code__c != null ? objCourier.Postal_Code__c : '';
            objAirwayBillData.ReceiversProvince = '';
            if(listSD.size() > 0) {
                objAirwayBillData.SendersAddress1 = listSD[0].Address__c;
                objAirwayBillData.SendersContactPerson = listSD[0].Person_Name__c;
                objAirwayBillData.SendersEmail = listSD[0].Email_Address__c;
                objAirwayBillData.SendersPhone = String.valueOf(listSD[0].Phone__c);
                objAirwayBillData.SendersPinCode = listSD[0].PostCode__c;
                objAirwayBillData.SendersCompany = listSD[0].Company__c;
                objAirwayBillData.SendersCity = listSD[0].City__c;
                            }
            
            objAirwayBillData.SendersAddress2 = '';
            
            objAirwayBillData.SendersSubCity = '';
            objAirwayBillData.SendersCountry = '';
            
            objAirwayBillData.SendersGeoLocation = '';
            objAirwayBillData.SendersMobile = '';
            objAirwayBillData.ServiceType = 'NOR';
            objAirwayBillData.ShipmentDimension = '';
            objAirwayBillData.ShipmentInvoiceCurrency = '';
            objAirwayBillData.ShipmentInvoiceValue = 0;
            objAirwayBillData.ShipperReference = '';
            objAirwayBillData.ShipperVatAccount = '';
            objAirwayBillData.SpecialInstruction = objCourier.Remarks__c != null ? objCourier.Remarks__c : 'Confidencial Documents';
            objAirwayBillData.Weight = 1;
            
            
            String JSONData = JSON.serialize(objAirwayBillData);
            System.debug('JSONData: '+JSONData);
            System.debug('objCourier: '+objCourier);
            System.debug('creds: '+creds);
            String body = '{"AirwayBillData": '+JSONData+', "AccountNo":  "'+creds.Resource__c+'" ,"Country": "'+countryCode+'", "UserName": "'+creds.User_Name__c+'", "Password": "'+creds.Password__c+'"}';
            System.debug('body: '+body);
            if(body != Null){
                HTTPResponse resp = returnResponseFromService(creds.Endpoint__c, 'POST', creds.User_Name__c, creds.Password__c, body); //h.send(request);
                System.debug('resp: '+resp);
                System.debug('resp.body: '+resp.getBody());
                PickupRequestResponse responseObj = ( PickupRequestResponse )System.JSON.deserialize(resp.getBody(), PickupRequestResponse.class);
                System.debug('responseObj: '+responseObj);
                if(resp.getStatusCode() == 200){
                    objCourier.Courier_Status__c = responseObj.Description;
                    objCourier.Request_Number__c = responseObj.AirwayBillNumber;
                    objCourier.Airway_Bill__c = responseObj.AirwayBillNumber;
                    lstCourierToInsert.add(objCourier);
                    system.debug('Inside getStatusCode if--->'+lstCourierToInsert);
                    if(objCourier.Case__c != null && mapCase.containsKey(objCourier.Case__c)){
                        Case caseInst = mapCase.get(objCourier.Case__c);
                        caseInst.Courier_Request_Already_Created__c = true;
                        //caseInst.Airway_Bill_Number__c = objCourier.Airway_Bill__c;
                        //cases.add(caseInst);
                        MapIdToCaseToUpdate.put(caseInst.Id, caseInst);
                    }
                    if(objCourier.UnitName__c != null && mapBookingUnit.containsKey(objCourier.UnitName__c)){
                        Booking_Unit__c buInst = mapBookingUnit.get(objCourier.UnitName__c);
                        buInst.Courier_Request_Already_Created__c = true;
                        //BUs.add(buInst);
                        mapIdToBookingUnitsToUpdate.put(buInst.Id, buInst);
                    }
                }else{
                    Error_Log__c objError = GenericUtility.createErrorLog(resp.getBody(), objCourier.Account__c, objCourier.UnitName__c, '', objCourier.Case__c);
                    objError.Service_Request_Body__c = body;
                    objError.Service_Response_Body__c = resp.getBody();
                    lstErrorLog.add(objError);
                }
            }
        }
        if(!lstErrorLog.isEmpty()){
            Insert lstErrorLog;
        }
        if(!MapIdToCaseToUpdate.isEmpty()){
            //Update setCaseIds;
            Update MapIdToCaseToUpdate.values();
        }
        if(!mapIdToBookingUnitsToUpdate.isEmpty()){
            //Update BUs;
            Update mapIdToBookingUnitsToUpdate.values();
        }
        return lstCourierToInsert;
    }
    
    public static String getLabel(String myLabel ){
        List<Country_City_Code_Mapping__mdt> listSfRules = new List<Country_City_Code_Mapping__mdt>();
        listSfRules = [SELECT City_Code__c
                            , DeveloperName
                            , MasterLabel
                       FROM Country_City_Code_Mapping__mdt
                       WHERE MasterLabel =: myLabel LIMIT 1];
        /*Component.Apex.OutputText output = new Component.Apex.OutputText();
        output.expressions.value = '{!$Label.'+ myLabel + '}';
        return String.valueOf(output.value);*/
        if(listSfRules.size() > 0) {
            return listSfRules[0].City_Code__c;
        } else {
            return null;
        }
    }
   
    /*public static list<Courier_Delivery__c> getSchedulePickup(List<Courier_Delivery__c> couriList){
        Credentials_Details__c creds = Credentials_Details__c.getInstance( 'First Flight Schedule Pickup');
        List<Courier_Delivery__c> lstCourierToUpdate = new List<Courier_Delivery__c>();
        List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
        List<Id> lstAccIds = new List<Id>();
        for(Courier_Delivery__c objCourier: couriList){
            lstAccIds.add(objCourier.Account__c);
        }
        Map<Id, Account> mapAccount = new Map<Id, Account>([SELECT Id, Name, Party_ID__c FROM Account WHERE Id IN: lstAccIds]);
        for(Courier_Delivery__c objCourier: couriList){
            Id partyId = mapAccount.get(objCourier.Account__c).Party_ID__c;
            BookingData objBookingData = new BookingData();
            objBookingData.AppoximateWeight = 0;
            objBookingData.BookingAddress1 = objCourier.Address_1__c;
            objBookingData.BookingAddress2 = objCourier.Address_2__c;
            objBookingData.BookingCity = objCourier.City__c;
            objBookingData.BookingCompanyName = mapAccount.get(objCourier.Account__c).Name;
            objBookingData.BookingContactPerson = '';
            objBookingData.BookingCountry = objCourier.Country__c;
            objBookingData.BookingCreatedBy = '';
            objBookingData.BookingEmail = '';
            objBookingData.BookingMobileNo = Null;
            objBookingData.BookingPhoneNo = objCourier.Phone_1__c;
            objBookingData.BusinessClosingTime = Null;
            objBookingData.Destination = '';
            objBookingData.GoodsDescription = '';
            objBookingData.NumberofPeices = Null;
            objBookingData.NumberofShipments = Null;
            objBookingData.Origin = '';
            objBookingData.PackageType = '';
            objBookingData.ShipperReference = '';
            
            String JSONData = JSON.serialize(objBookingData);
            System.debug('JSONData: '+JSONData);
            String body = '{ "BookingData": '+JSONData+', "AccountNo": "'+partyId+'", "Country": "'+objCourier.Country__c+'" }';
            System.debug('body: '+body);
            if(body != Null){
                HTTPResponse resp = returnResponseFromService(creds.Endpoint__c, 'POST', creds.User_Name__c, creds.Password__c, body); //h.send(request);
                System.debug('resp: '+resp);
                System.debug('resp.body: '+resp.getBody());
                PickupRequestResponse responseObj = ( PickupRequestResponse )System.JSON.deserialize(resp.getBody(), PickupRequestResponse.class);
                System.debug('responseObj: '+responseObj);
                if(resp.getStatusCode() == 200){
                    objCourier.Courier_Status__c = responseObj.Description;
                    objCourier.Request_Number__c = responseObj.PickupRequestNo;
                    lstCourierToUpdate.add(objCourier);
                    system.debug('Inside getStatusCode if--->'+lstCourierToUpdate);
                }else{
                    Error_Log__c objError = GenericUtility.createErrorLog(resp.getBody(), objCourier.Account__c, objCourier.UnitName__c, '', objCourier.Case__c);
                    objError.Service_Request_Body__c = body;
                    objError.Service_Response_Body__c = resp.getBody();
                    lstErrorLog.add(objError);
                }
            }
        }
        //if(!lstCourierToUpdate.isEmpty()){
            //Update lstCourierToUpdate;
        //}
        
        if(!lstErrorLog.isEmpty()){
            Insert lstErrorLog;
        }
        return lstCourierToUpdate;
    }*/
    
    public static Blob getAirwayBillInPDF(String strAirwayBillNo, String CountryCode){
        Credentials_Details__c creds = Credentials_Details__c.getInstance( 'First Flight AWB PDF' );
        String body = '{'+
            '"AccountNo":"'+creds.Resource__c+'",'+
            '"AirwayBillNumber":"'+strAirwayBillNo+'",'+
            '"Country":"'+CountryCode+'",'+
            '"Password":"'+creds.Password__c+'",'+
            '"RequestUser":"'+creds.Resource__c+'",'+
            '"UserName":"'+creds.User_Name__c+'"'+
            '}';
        HTTP http = new HTTP();
        HTTPRequest request = new HTTPRequest();
        request.setEndpoint(creds.Endpoint__c);
        request.setMethod('POST');
        request.setHeader('Content-type', 'application/json');
        request.setBody(body);
        request.setTimeout(120000);
        System.debug('request: '+request);
        HTTPResponse resp = http.send(request);
        System.debug('===>resp'+resp);
        System.debug('===>resp'+resp.getBody());
        AirWayBillWrapper objResponse = new AirWayBillWrapper();
        objResponse = (AirWayBillWrapper)System.JSON.deserialize(resp.getBody(), AirWayBillWrapper.class);
        System.debug('objResponse: '+objResponse);
        if(resp.getStatusCode()==200){
            Blob billBody = EncodingUtil.base64Decode(objResponse.ReportDoc);
            system.debug('billBody>>>'+billBody);
            return billBody;
        }
        return null;
    }
    
    public static HTTPResponse returnResponseFromService(String endpoint,String method,String userName,String password, String body) {
        String authorization  = 'Basic';
        HTTP h = new HTTP();
        HTTPRequest request = new HTTPRequest();
        request.setEndpoint(endpoint);
        request.setMethod(method);
        request.setHeader('Accept', 'application/json');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Accept-Language', 'en-US');
        Blob headerValue = Blob.valueOf(userName + ':' +password);
        String authorizationHeader = authorization + EncodingUtil.base64Encode(headerValue);
        request.setHeader('Authorization', authorizationHeader);
        request.setBody(body);
        request.setTimeout(120000);
        System.debug('request: '+request);
        HTTPResponse resp = h.send(request);
        return resp;
    }
    
    public class JSON2ApexTrackingInput {
        public String TrackingAWB;
        public String UserName;
        public String Password;
        public String AccountNo;
        public String Country;
        
        public JSON2ApexTrackingInput parse(String json) {
            return (JSON2ApexTrackingInput) System.JSON.deserialize(json, JSON2ApexTrackingInput.class);
        }
    }

    public class AirwayBillTrackList {
        public String AirWayBillNo;
        public String Destination;
        public String ForwardingNumber;
        public String Origin;
        public Integer ShipmentProgress;
        public String ShipperReference;
        public List<TrackingLogDetails> TrackingLogDetails;
        public String Weight;
    }

    public List<AirwayBillTrackList> AirwayBillTrackList;
    public Integer Code;
    public String Description;

    public class TrackingLogDetails {
        public String ActivityDate;
        public String ActivityTime;
        public String DeliveredTo;
        public String Location;
        public String Remarks;
        public String Status;
    }

    //
// Generated by JSON2Apex http://json2apex.herokuapp.com/
//

//public class JSON2Apex {

    public BookingData BookingData;
    public String AccountNo;
    public String Country;
    public String Password;
    public String UserName;

    public class BookingData {
        public Integer AppoximateWeight;
        public String BookingAddress1;
        public String BookingAddress2;
        public String BookingCompanyName;
        public String BookingContactPerson;
        public String BookingCreatedBy;
        public String BookingCity;
        public String BookingCountry;
        public String BookingEmail;
        public String BookingMobileNo;
        public String BookingPhoneNo;
        public String BusinessClosingTime;
        public String Destination;
        public String GoodsDescription;
        public Integer NumberofPeices;
        public Integer NumberofShipments;
        public String Origin;
        public String PackageType;
        public String ProductType;
        public String ReceiversAddress1;
        public String ReceiversAddress2;
        public String ReceiversCity;
        public String ReceiversCompany;
        public String ReceiversContactPerson;
        public String ReceiversEmail;
        public String ReceiversGeoLocation;
        public String ReceiversMobile;
        public String ReceiversPhone;
        public String ReceiversPinCode;
        public String ReceiversSubCity;
        public String SendersAddress1;
        public String SendersAddress2;
        public String SendersCity;
        public String SendersCompany;
        public String SendersContactPerson;
        public String SendersDepartment;
        public String SendersEmail;
        public String SendersGeoLocation;
        public String SendersMobile;
        public String SendersPhone;
        public String SendersPinCode;
        public String SendersSubCity;
        public String ServiceType;
        public String ShipmentReadyDate;
        public String ShipmentReadyTime;
        public String ShipperReference;
        public String SpecialInstruction;
    }
//}
    
    public class PickupRequestResponse {
        public Integer Code;
        public String Description;
        public String AirwayBillNumber;
        
    }

    public class  AirwayBillData {
        public String AirWayBillCreatedBy;
        public String CODAmount;
        public String CODCurrency;
        public String Destination;
        public String DutyConsigneePay;
        public String GoodsDescription;
        public Integer NumberofPeices;
        public String Origin;
        public String ProductType;
        public String ReceiversAddress1;
        public String ReceiversAddress2;
        public String ReceiversCity;
        public String ReceiversSubCity;
        public String ReceiversCountry;
        public String ReceiversCompany;
        public String ReceiversContactPerson;
        public String ReceiversEmail;
        public String ReceiversGeoLocation;
        public String ReceiversMobile;
        public String ReceiversPhone;
        public String ReceiversPinCode;
        public String ReceiversProvince;
        public String SendersAddress1;
        public String SendersAddress2;
        public String SendersCity;
        public String SendersSubCity ;
        public String SendersCountry;
        public String SendersCompany;
        public String SendersContactPerson;
        public String SendersEmail;
        public String SendersGeoLocation;
        public String SendersMobile;
        public String SendersPhone;
        public String SendersPinCode;
        public String ServiceType;
        public String ShipmentDimension;
        public String ShipmentInvoiceCurrency;
        public Integer ShipmentInvoiceValue;
        public String ShipperReference;
        public String ShipperVatAccount;
        public String SpecialInstruction;
        public Integer Weight;
    }

    public  AirwayBillData  AirwayBillData;
    
    public class AirWayBillWrapper{
        public string Description;
        public Integer Code;
        public string ReportDoc;
    }
    
}