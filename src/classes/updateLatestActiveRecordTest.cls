/************************************************************************************************
 * @Name              : updateLatestActiveRecordTest 
 * @Description       : Test Class for updateLatestActiveRecord
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         QBurst         18/05/2020       Created
***********************************************************************************************/

@IsTest
public with sharing class updateLatestActiveRecordTest{

    @IsTest
    public static void  testmethod1(){
        
    
        
        Account acc = new Account();
        acc.Name = 'New Test Account';
        insert acc;
        
        New_Partnership_Program__c nPP = new New_Partnership_Program__c();
        nPP.Account__c = acc.Id; 
        nPP.Active__c = true;
        insert nPP;
        
       System.assertEquals(true, nPP.Active__c); 
     
    
    }



}