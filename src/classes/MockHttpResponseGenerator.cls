@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        DLD_DAMAC_LOGIN resp = new DLD_DAMAC_LOGIN ();
        DLD_DAMAC_LOGIN.data obj = new DLD_DAMAC_LOGIN.data ();
        obj.token = 'Test';
        resp.data = obj;
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(JSON.serialize (resp));
        res.setStatusCode(200);
        return res;
    }
}