@isTest
public class CreateWeeklyOrMonthlyReviewCntrlTest {
    @IsTest
    static void positiveTestCase1WithDraftStatusAndId(){
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                                    emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                    ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com');
        insert adminUser;
        
        User adminUser1 = new User(alias = 'test456', email='xyz2@email.com',
                                    emailencodingkey='UTF-8', lastname='User 4562', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                    ,timezonesidkey='America/Los_Angeles', username='xyz2@email.com');
        insert adminUser1;
        User adminUser2 = new User(alias = 'test456', email='xyz3@email.com',
                                    emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                    ,timezonesidkey='America/Los_Angeles', username='xyz3@email.com');
        insert adminUser2;
        WeeklyReviewHODAndManagerMapping__c weeklycustSetting = new WeeklyReviewHODAndManagerMapping__c();
        weeklycustSetting.HOD_Id__c = adminUser.Id;
        weeklycustSetting.Name = adminUser.Id;
        weeklycustSetting.Manager_Id__c = adminUser1.Id+','+adminUser2.Id;
        insert weeklycustSetting;
        Profiles_allowed_to_view_DCR__c prSetting = new Profiles_allowed_to_view_DCR__c(Name = 'Profiles', Profile_Names__c = 'System Administrator');
        insert prSetting;
        Id recordTypeId = Schema.SObjectType.Daily_Collection_Review__c.getRecordTypeInfosByDeveloperName().get('Weekly_Review').getRecordTypeId();
        Daily_Collection_Review__c objDaily = new Daily_Collection_Review__c();
        objDaily.Weekly_Review_Initiator__c = adminUser.Id;
        objDaily.Meeting_Date__c = System.today();
        objDaily.Weekly_Review_Status__c  = 'In draft';
        objDaily.Weekly_Review_Manager__c = adminUser2.Id;
        objDaily.RecordTypeId = recordTypeId;
        objDaily.Review_For__c = 'Month'; 
        objDaily.Months__c= 'Sep';          
        insert objDaily;
        System.runAs(adminUser) {
                CreateWeeklyOrMonthlyReviewCntrl obj = new CreateWeeklyOrMonthlyReviewCntrl();
                PageReference pageObj = Page.CreateWeeklyOrMonthlyReview;
                Test.setCurrentPage(pageObj);
                System.currentPageReference().getParameters().put('id', objDaily.Id);
                obj.createWSR();
                obj.saveResult();
                obj.submitSaveRedirect();
                obj.sendUserEmail();
                obj.submitResult();
        }                                   
    }
    
    @IsTest
    static void positiveTestCase2WithDraftStatus(){
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                                    emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                    ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com');
        insert adminUser;
        
        User adminUser1 = new User(alias = 'test456', email='xyz2@email.com',
                                    emailencodingkey='UTF-8', lastname='User 4562', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                    ,timezonesidkey='America/Los_Angeles', username='xyz2@email.com');
        insert adminUser1;
        User adminUser2 = new User(alias = 'test456', email='xyz3@email.com',
                                    emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                    ,timezonesidkey='America/Los_Angeles', username='xyz3@email.com');
        insert adminUser2;
        WeeklyReviewHODAndManagerMapping__c weeklycustSetting = new WeeklyReviewHODAndManagerMapping__c();
        weeklycustSetting.HOD_Id__c = adminUser.Id;
        weeklycustSetting.Name = adminUser.Id;
        weeklycustSetting.Manager_Id__c = adminUser1.Id+','+adminUser2.Id;
        insert weeklycustSetting;
        Profiles_allowed_to_view_DCR__c prSetting = new Profiles_allowed_to_view_DCR__c(Name = 'Profiles', Profile_Names__c = 'System Administrator');
        insert prSetting;
        Id recordTypeId = Schema.SObjectType.Daily_Collection_Review__c.getRecordTypeInfosByDeveloperName().get('Weekly_Review').getRecordTypeId();
        Daily_Collection_Review__c objDaily = new Daily_Collection_Review__c();
        objDaily.Weekly_Review_Initiator__c = adminUser.Id;
        objDaily.Meeting_Date__c = System.today();
        objDaily.Weekly_Review_Status__c  = 'In draft';
        objDaily.Weekly_Review_Manager__c = adminUser2.Id;
        objDaily.RecordTypeId = recordTypeId;
        objDaily.Review_For__c = 'Month';
        objDaily.Months__c= 'Sep';        
        insert objDaily;
        System.runAs(adminUser) {
                CreateWeeklyOrMonthlyReviewCntrl obj = new CreateWeeklyOrMonthlyReviewCntrl();
                PageReference pageObj = Page.CreateWeeklyOrMonthlyReview;
                Test.setCurrentPage(pageObj);
                obj.createWSR();
                obj.fillReview();
                
        }                                   
    }
    
       
    @IsTest
    static void positiveTestCase3WithDraftStatus(){
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                                    emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                    ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com');
        insert adminUser;
        
        User adminUser1 = new User(alias = 'test456', email='xyz2@email.com',
                                    emailencodingkey='UTF-8', lastname='User 4562', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                    ,timezonesidkey='America/Los_Angeles', username='xyz2@email.com');
        insert adminUser1;
        User adminUser2 = new User(alias = 'test456', email='xyz3@email.com',
                                    emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                    ,timezonesidkey='America/Los_Angeles', username='xyz3@email.com');
        insert adminUser2;
        WeeklyReviewHODAndManagerMapping__c weeklycustSetting = new WeeklyReviewHODAndManagerMapping__c();
        weeklycustSetting.HOD_Id__c = adminUser.Id;
        weeklycustSetting.Name = adminUser.Id;
        weeklycustSetting.Manager_Id__c = adminUser1.Id+','+adminUser2.Id;
        insert weeklycustSetting;
        Profiles_allowed_to_view_DCR__c prSetting = new Profiles_allowed_to_view_DCR__c(Name = 'Profiles', Profile_Names__c = 'System Administrator');
        insert prSetting;
        Id recordTypeId = Schema.SObjectType.Daily_Collection_Review__c.getRecordTypeInfosByDeveloperName().get('Weekly_Review').getRecordTypeId();
        Daily_Collection_Review__c objDaily = new Daily_Collection_Review__c();
        objDaily.Weekly_Review_Initiator__c = adminUser.Id;
        objDaily.Meeting_Date__c = System.today();
        objDaily.Weekly_Review_Status__c  = 'In draft';
        objDaily.Weekly_Review_Manager__c = adminUser2.Id;
        objDaily.RecordTypeId = recordTypeId;
        objDaily.Review_For__c = 'Month';
        objDaily.Months__c= 'Sep';        
        insert objDaily;
        System.runAs(adminUser) {
                CreateWeeklyOrMonthlyReviewCntrl obj = new CreateWeeklyOrMonthlyReviewCntrl();
                PageReference pageObj = Page.CreateWeeklyOrMonthlyReview;
                Test.setCurrentPage(pageObj);
                System.currentPageReference().getParameters().put('id', objDaily.Id);
                obj.createWSR();
                //obj.fillReview();
                obj.generatePDF();
        }                                   
    }
}