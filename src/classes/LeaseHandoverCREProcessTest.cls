@isTest
private class LeaseHandoverCREProcessTest {
    
    static Booking_Unit__c BUObj;
    static Account objAcc;
    
    static void testData() {
        objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        objAcc.Country__c = 'United Arab Emirates';
        insert objAcc ;
        
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='AB/019/TH500', Bedroom_Type__c = '1BR',
        Registration_ID__c = '74712', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100,
        Unit_Selling_Price__c = 3456899, Anticipated_Completion_Date__c = system.Today() );
        insert BUObj;
        
        list<Buyer__c> lstBuyer= TestDataFactory_CRM.createBuyer(objBooking.Id, 1, objAcc.Id);
        insert lstBuyer;
        
        Booking_Unit_Active_Status__c objBUStatus = new Booking_Unit_Active_Status__c();
        objBUStatus.Name = 'Active Status';
        objBUStatus.Status_Value__c = 'Active Status';
        insert objBUStatus;
        
        Payment_Plan__c objPaymentPlan = new Payment_Plan__c();
        objPaymentPlan.Booking_Unit__c = BUObj.Id;
        objPaymentPlan.Status__c = 'Active';
        objPaymentPlan.Effective_To__c = System.Today() + 1;
        insert objPaymentPlan;
    }
    
    static testMethod void ExistingCase() {
        testData();
        
         Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
         Case objCase = new Case();
         objCase.recordTypeId = caseRecordTypeId;
         objCase.AccountId = objAcc.Id;
         objCase.Booking_Unit__c = BUObj.Id;
         insert objCase;    
         
        Test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockHandover());        
        Pagereference pageRef = Page.Lease_HandoverProcessPage;
        Test.setCurrentPageReference(pageRef);  
        LeaseHandoverCREProcess objClass = new LeaseHandoverCREProcess();
        objClass.accountId = objAcc.Id;
        objClass.strSRType = 'Lease_Handover';
        objClass.getName();
        objClass.strSelectedUnit = BUObj.Id;  
        objClass.autoPopulateBUDetails();
        Test.stopTest();    
    }
    
    static testMethod void TestMethod1() {
        testData();   
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
                
        Pagereference pageRef = Page.Lease_HandoverProcessPage;
        Test.setCurrentPageReference(pageRef);  
        LeaseHandoverCREProcess objClass = new LeaseHandoverCREProcess();
        objClass.accountId = objAcc.Id;
        objClass.strSRType = 'Lease_Handover';
        objClass.getName();
        objClass.strSelectedUnit = BUObj.Id; 
        objClass.autoPopulateBUDetails();
        objClass.SOAtobeGenerated();
        objClass.getlstUnits();
        objClass.GenerateDocuments();
        objClass.ViewAreaVariation();
        objClass.isValid = true;
        //objClass.callDrawloop();
        //objClass.callRequest();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockHandover());
        objClass.fileAttachmentBody = 'test body';
        objClass.fileAttachmentName = 'Passport';
        //objClass.saveDraft();
        objClass.saveSubmit();
        Test.stopTest();    
    }
    
    static testMethod void TestMethod3() {
        testData();
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
         Case objCase = new Case();
         objCase.recordTypeId = caseRecordTypeId;
         objCase.AccountId = objAcc.Id;
         objCase.Booking_Unit__c = BUObj.Id;
         insert objCase;
        
        Test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockHandover());        
        Pagereference pageRef = Page.Lease_HandoverProcessPage;
        Test.setCurrentPageReference(pageRef);  
        LeaseHandoverCREProcess objClass = new LeaseHandoverCREProcess();
        objClass.accountId = objAcc.Id;
        objClass.strSelectedUnit = BUObj.Id;
        objClass.strCaseID = objCase.Id;
        objClass.strSRType = 'Lease_Handover';
        objClass.getName();
        Test.stopTest();
    }
    
    static testMethod void TestMethod2() {
        testData();
        
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
         Case objCase = new Case();
         objCase.recordTypeId = caseRecordTypeId;
         objCase.AccountId = objAcc.Id;
         objCase.Booking_Unit__c = BUObj.Id;
         insert objCase;
         
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());        
        Pagereference pageRef = Page.Lease_HandoverProcessPage;
        Test.setCurrentPageReference(pageRef);  
        LeaseHandoverCREProcess objClass = new LeaseHandoverCREProcess();
        objClass.accountId = objAcc.Id;
        objClass.objBookingUnit = BUObj;
        objClass.caseId = objCase.Id;
        objClass.strSRType = 'Lease_Handover';
        //objClass.callDrawloop();
        //objClass.callRequest();
        //objClass.callRGSAgreement();
        objClass.closePopup();
        Test.stopTest();
    }

    @IsTest
    static void testCreateErrorLogRecord(){
        
        testData();
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
         Case objCase = new Case();
         objCase.recordTypeId = caseRecordTypeId;
         objCase.AccountId = objAcc.Id;
         objCase.Booking_Unit__c = BUObj.Id;
         insert objCase;

        Test.startTest();
        Error_Log__c  objerr = new LeaseHandoverCREProcess().createErrorLogRecord( objAcc.Id, BUObj.Id, objCase.Id);
        new LeaseHandoverCREProcess().insertErrorLog(objErr);
        Test.stopTest();
        
    }

    @IsTest
    static void testcallRequest(){
        
        testData();
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
         Case objCase = new Case();
         objCase.recordTypeId = caseRecordTypeId;
         objCase.AccountId = objAcc.Id;
         objCase.Booking_Unit__c = BUObj.Id;
         insert objCase;

        Attachment objAtt = new Attachment(ParentId= objCase.Id, Name='Cover Letter', Body=blob.valueOf('test'));
        insert objAtt;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        LeaseHandoverCREProcess ctrl = new LeaseHandoverCREProcess();
        ctrl.strCaseID = objCase.Id;
        ctrl.DocumentURL();
        ctrl.callRequest();
        Test.stopTest();
        
    }
}