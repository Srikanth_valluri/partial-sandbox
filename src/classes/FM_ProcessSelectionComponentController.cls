/*
 * Description - Controller class for FM_ProcessSelectionComponent visualforce component
 *
 * Version        Date            Author            Description
 * 1.0          15/05/2018       Ashish Agarwal    Initial Draft
 */
public without sharing class FM_ProcessSelectionComponentController {


    public FM_ProcessSelectionComponentController(){
    }

    // JS Remoting action called when searching for a movie name
    @RemoteAction
    public static List<Account> searchAccount(String searchTerm) {
        System.debug('Account Name is: '+searchTerm );
        List<Account> lstAccount = Database.query('Select Id, Name,IsPersonAccount from Account where name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'');
        return lstAccount;
    }
}