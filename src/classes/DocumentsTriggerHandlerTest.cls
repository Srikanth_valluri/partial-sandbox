@isTest
public class DocumentsTriggerHandlerTest{
    static List<Booking_Unit_Active_Status__c> lstActives;
    static list<RuleEngineDocuments__c> lstRuleDocs;
    static Account objAcc;
    static NSIBPM__Service_Request__c objSR;
    static List<Booking__c> lstBookings;
    static Property__c objProp;
    static Inventory__c objInv;
    static List<Booking_Unit__c> lstBookingUnits;
    static List<Option__c> lstOptions;
    static list<Buyer__c> lstJB;
    static Payment_Plan__c objPP;
    
    static void init() {
        lstActives = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActives;
        
        lstRuleDocs = TestDataFactory_CRM.createRuleDocs();
        insert lstRuleDocs;
        
        // Insert Account        
        objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        system.debug('******Acc Id*************'+objAcc.Id);
        
        //Insert Service Request
        objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        system.debug('******SR Id*************'+objSR.Id);
        
        //Insert Bookings
        lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings ;
        system.debug('******bookings Id*************'+lstBookings);
        
        objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        insert lstBookingUnits;
        system.debug('******Unit Id*************'+lstBookingUnits);
        
        lstOptions = TestDataFactory_CRM.createOptions(lstBookingUnits);
        insert lstOptions;
        
        lstJB = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 2, objAcc.Id);
        insert lstJB;
    }
    
    static testMethod void testOne(){
        init();
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Case Cas = TestDataFactory_CRM.createCase(objAcc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = lstBookingUnits[0].Id ;
        Cas.Type = 'NOCVisa';
        Cas.Document_Verified__c = true;
        Cas.Total_Snags_Closed__c = 3;
        insert Cas;
        
        SR_Attachments__c objSR1 = new SR_Attachments__c();
        objSR1.Name = '12 Signed Letter of Discharge 12';
        objSR1.isValid__c = true;
        objSR1.Attachment_URL__c = 'www.google.com';
        objSR1.Case__c = Cas.Id;
        insert objSR1;
        
        SR_Attachments__c objSR2 = new SR_Attachments__c();
        objSR2.Name = '12 Signed Handover Checklist 12';
        objSR2.isValid__c = false;
        objSR2.Attachment_URL__c = 'www.google.com';
        objSR2.Case__c = Cas.Id;
        insert objSR2;
        
        objSR2.isValid__c = true;
        update objSR2;
    }
}