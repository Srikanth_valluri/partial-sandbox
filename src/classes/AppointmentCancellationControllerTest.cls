@isTest 
private with sharing class AppointmentCancellationControllerTest {
    
    static testmethod void testInit() {
    	NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
    	insert objDealSR ;
    	
    	Account objAcc = TestDataFactory_CRM.createPersonAccount();
    	insert objAcc ;
    	
    	List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objDealSR.Id, 1 );
    	insert lstBooking ;
    	
    	List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
    	insert lstBookingUnit ;
    	
    	TriggerOnOffCustomSetting__c objSetting = new TriggerOnOffCustomSetting__c();
    	objSetting.Name = 'CallingListTrigger';
    	objSetting.OnOffCheck__c = false ;
    	insert objSetting ;
    	
    	List<Calling_List__c> lstCalling = TestDataFactory_CRM.createCallingList( 'Appointment_Scheduling', 1 , lstBookingUnit[0] );
    	insert lstCalling ;
    	
    	test.startTest();
    		ApexPages.standardController objSC = new ApexPages.standardController( lstCalling[0] );
    		AppointmentCancellationController objController = new AppointmentCancellationController( objSC );
    	test.stopTest();
    }
    
    static testMethod void testCancellation() {
    	NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
    	insert objDealSR ;
    	
    	Account objAcc = TestDataFactory_CRM.createPersonAccount();
    	insert objAcc ;
    	
    	List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objDealSR.Id, 1 );
    	insert lstBooking ;
    	
    	List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
    	insert lstBookingUnit ;
    	
    	TriggerOnOffCustomSetting__c objSetting = new TriggerOnOffCustomSetting__c();
    	objSetting.Name = 'CallingListTrigger';
    	objSetting.OnOffCheck__c = false ;
    	insert objSetting ;
    	
    	List<Calling_List__c> lstCalling = TestDataFactory_CRM.createCallingList( 'Appointment_Scheduling', 1 , lstBookingUnit[0] );
    	for( Calling_List__c objCall : lstCalling ) {
    		objCall.Appointment_Status__c = 'Requested';
    		objCall.Account_Email__c = 'test@test.com';
    		objCall.Handover_Team_Email__c = 'test@test.com';
    		objCall.Assigned_CRE__c = UserInfo.getUserId();
    	}
    	insert lstCalling ;
    	
    	test.startTest();
    		ApexPages.standardController objSC = new ApexPages.standardController( lstCalling[0] );
    		AppointmentCancellationController objController = new AppointmentCancellationController( objSC );
    		objController.lstAppointmentWrapper[0].isSelected = true ;
    		objController.cancelAppointment();
    	test.stopTest();
    }
    
}