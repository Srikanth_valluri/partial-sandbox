/*****************************************************************************************************
* Description - Test class developed for 'EligibleForHandoverUpdated'
*
* Version            Date            Author               Description
* 1.0               05/02/2018                   		Initial Draft
 *******************************************************************************************************/
@isTest
public class EligibleForHandoverUpdatedTest {
  public static testmethod void testEligibleForHandover(){
	Test.startTest();
	    EligibleForHandoverUpdated.EligibleForHandoverNoticeRuleHttpSoap11Endpoint obj = new   EligibleForHandoverUpdated.EligibleForHandoverNoticeRuleHttpSoap11Endpoint();
	    List<HODocuments2.APPSXXDC_PROCESS_SERX1794747X1X5> regTerms =  new List<HODocuments2.APPSXXDC_PROCESS_SERX1794747X1X5>();
	    
	    SOAPCalloutServiceMock.returnToMe =  new Map<String, EligibleForHandoverUpdated.EligibleForHandoverNoticeResponse_element>();
	    EligibleForHandoverUpdated.EligibleForHandoverNoticeResponse_element response_x = new EligibleForHandoverUpdated.EligibleForHandoverNoticeResponse_element();
	    response_x.return_x = 'S';
	    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
	    
	    test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
	    
	    String respose = obj.EligibleForHandoverNotice( 'RegistrationId', 'processName',
													     'subProcessName', 'modeOfRequest', 'noOfMajorSnagsInApartment',
													     'accessPresent', 'utilitiesAvailable', 'percAptsSnagged',
													     'EHOCase', 'DaysToEarliestViewing', 'extra1', 'extra2',
													     'extra3', 'extra4', 'extra5', 'extra6', 'extra7',
													     'extra8', 'extra9', 'extra10', 'extra11', 'extra12',
													     'extra13', 'extra14', 'extra15', 'extra16', 'extra17',
													     'extra18', 'extra19', 'extra20');
	      //String respose = obj.HandOverPack('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM',regTerms);      
	    System.assertEquals(respose,'S');
	 Test.stopTest();
  }
}