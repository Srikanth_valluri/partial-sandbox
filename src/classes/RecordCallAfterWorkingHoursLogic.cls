/**
 * Logic class for RecordCallAfterWorkingHoursWebService
 */
public class RecordCallAfterWorkingHoursLogic {

    /**
     * Method to process request and send the response
     */
    public static void processRecordCallAfterWorkingHoursRequest() {
        RecordCallAfterWorkingHoursResponse response = new RecordCallAfterWorkingHoursResponse();

        // Get request body
        String requestBody = RestContext.request.requestBody.toString();
        System.debug('=========== requestBody : ' + requestBody);

        try {
            // Parse request body
            RecordCallAfterWorkingHoursRequestBody request =
                (RecordCallAfterWorkingHoursRequestBody) JSON.deserializeStrict(
                    requestBody,
                    RecordCallAfterWorkingHoursRequestBody.Class
                );

            Account account = getAccountForCallingNumber(request.callingNumber);
            Calling_List__c callingList;
            DateTime callDate = CtiTntegrationUtility.getDateTimeForUserTimeZone(request.calledDateTime);
            if (callDate == null) {
                response.errorMessage = 'Please enter valid date format (MM/DD/YYYY HH:MM:SS)';
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
                Restcontext.response.statusCode = 400;
                return;
            }
            if (request.callback) {
                callingList = createCallBackRequest(account, request,callDate);
            } else {
                callingList = createMissedCallInCallingList(account, request,callDate);
            }
            /*if(callingList.Call_Date__c == null){
                response.errorMessage = 'Please enter valid date format (MM/DD/YYYY HH:MM:SS)';
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
                Restcontext.response.statusCode = 400;
                return;
            }*/
        } catch (Exception ex) {
            response.errorMessage = ex.getMessage();
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
            Restcontext.response.statusCode = 400;
            System.debug('============= response : ' + JSON.serialize(response));
            return;
        }

        response.status = 'Call Record Saved';

        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
        Restcontext.response.statusCode = 200;
    }

    /**
     * Create call back request
     */
    public static Calling_List__c createCallBackRequest(
        Account account,
        RecordCallAfterWorkingHoursRequestBody request,
        DateTime callDate
    ) {
        Calling_List__c callingList = new Calling_List__c();
        callingList.RecordTypeId =
            Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Call Back Request').getRecordTypeId();
        callingList.Calling_List_Type__c = 'Call Back Request';
        //callingList.Call_Date__c = DateTime.parse(request.calledDateTime);
        /*DateTime callDate = getDateTimeForUserTimeZone(request.calledDateTime);
        if(callDate == null) {
            return callingList;
        }*/
        callingList.Call_Date__c = callDate;
        callingList.Called_Number__c = request.calledNumber;
        callingList.Calling_Number__c = request.callingNumber;
        callingList.Country_of_calling_number__c =
            CtiTntegrationUtility.getCallingCountryFromCallingNumber(request.callingNumber);

        if (account != null) {
            callingList.Account__c = account.Id;
            callingList.Customer_Name__c = account.Name;
            callingList.Customer_Category__c = account.Customer_Type__c;
            callingList.Party_ID__c = account.Party_ID__c;
            callingList.Call_Back_Yes_No__c = 'Yes';
            if (account.Record_Type_Name__c == 'Person Account') {
                callingList.Mobile_Phone__c = account.Mobile_Phone_Encrypt__pc;
                callingList.Mobile_Phone_2__c = account.Mobile_Phone_Encrypt_2__pc;
                callingList.Mobile_Phone_3__c = account.Mobile_Phone_Encrypt_3__pc;
                callingList.Mobile_Phone_4__c = account.Mobile_Phone_Encrypt_4__pc;
                callingList.Mobile_Phone_5__c = account.Mobile_Phone_Encrypt_5__pc;
            }
        }

        // Set owner
        callingList = setCallingListOwner(callingList, account);

        insert callingList;
        return callingList;
    }

    /**
     * Create call back request
     */
    public static Calling_List__c createMissedCallInCallingList(
            Account account,
            RecordCallAfterWorkingHoursRequestBody request,
            DateTime callDate
    ) {
        Calling_List__c callingList = new Calling_List__c();
        callingList.RecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('After office missed calls').getRecordTypeId();
        callingList.Calling_List_Type__c = 'After office missed calls';
        //callingList.Call_Date__c = DateTime.parse(request.calledDateTime);
        /*DateTime callDate = getDateTimeForUserTimeZone(request.calledDateTime);
        if(callDate == null) {
            return callingList;
        }*/
        callingList.Call_Date__c = callDate;
        callingList.Called_Number__c = request.calledNumber;
        callingList.Calling_Number__c = request.callingNumber;
        callingList.Country_of_calling_number__c =
            CtiTntegrationUtility.getCallingCountryFromCallingNumber(request.callingNumber);
        if (account != null) {
            callingList.Account__c = account.Id;
            callingList.Customer_Name__c = account.Name;
            callingList.Customer_Category__c = account.Customer_Type__c;
            callingList.Party_ID__c = account.Party_ID__c;
            callingList.Call_Back_Yes_No__c = 'No';
            if (account.Record_Type_Name__c == 'Person Account') {
                callingList.Mobile_Phone__c = account.Mobile_Phone_Encrypt__pc;
                callingList.Mobile_Phone_2__c = account.Mobile_Phone_Encrypt_2__pc;
                callingList.Mobile_Phone_3__c = account.Mobile_Phone_Encrypt_3__pc;
                callingList.Mobile_Phone_4__c = account.Mobile_Phone_Encrypt_4__pc;
                callingList.Mobile_Phone_5__c = account.Mobile_Phone_Encrypt_5__pc;
            }
        }

        // Set owner
        callingList = setCallingListOwner(callingList, account);

        insert callingList;
        return callingList;
    }

    /**
     * Method to set owner of calling list record
     */
    public static Calling_List__c setCallingListOwner(
            Calling_List__c callingListInst,
            Account account
    ) {

        // Check if registered customer and if Primary/Secondary/Tertiary Cre avaliable
        if (account != null) {
            if (String.isNotBlank(account.Primary_CRE__c)) {
                callingListInst.OwnerId = account.Primary_CRE__c;
            } else if (String.isNotBlank(account.Secondary_CRE__c)) {
                callingListInst.OwnerId = account.Secondary_CRE__c;
            } else if (String.isNotBlank(account.Tertiary_CRE__c)) {
                callingListInst.OwnerId = account.Tertiary_CRE__c;
            }
        }

        // If no owner found from above logic set owner as Contact Centre Queue
        if (String.isBlank(callingListInst.OwnerId)) {
            Group contactCentreQueue =
                [SELECT Id, Name FROM Group WHERE Type = 'Queue' AND DeveloperName = 'Contact_Center_Queue'];
            callingListInst.OwnerId = contactCentreQueue.Id;
        }
        System.debug('>>>>>>>>>> callingListInst.OwnerId : ' + callingListInst.OwnerId);

        return callingListInst;
    }

    /**
     * Method get account for the calling number
     */
    public static Account getAccountForCallingNumber(String callingNumber) {
/*
        List<Account> accounts =
            [
                SELECT
                    Id,
                    Phone
                FROM
                    Account
                WHERE
                    Mobile_Phone_Encrypt__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_2__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_3__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_4__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_5__pc =: callingNumber
            ];
*/
        List<Account> accounts = CtiTntegrationUtility.getRegisteredAccount(callingNumber);
        if (accounts.size() > 0) {
            return accounts[0];
        } else {
            return null;
        }
    }

    /**
    * Method to get datetime for passed string in user timezone.
    * @param: dateTimeString: Date time value in string format
    * @return: DateTime
    **/
    /*public static DateTime getDateTimeForUserTimeZone(String dateTimeString) {

        DateTime newDateTime;
        if(String.isBlank(dateTimeString)) {
            return newDateTime;
        }
        list<String> spiltStrList = dateTimeString.split(' ');
        if(spiltStrList.size() >= 2) {
            list<String> dateComponentsList = spiltStrList[0].split('/');
            list<String> timeComponentsList = spiltStrList[1].split(':');

            if(dateComponentsList.size() == 3 && timeComponentsList.size() == 3) {
                newDateTime = DateTime.newInstance(Integer.valueOf(dateComponentsList[2]),
                                                   Integer.valueOf(dateComponentsList[0]),
                                                   Integer.valueOf(dateComponentsList[1]),
                                                   Integer.valueOf(timeComponentsList[0]),
                                                   Integer.valueOf(timeComponentsList[1]),
                                                   Integer.valueOf(timeComponentsList[2]));
            }

            return newDateTime;
        }
        return newDateTime;
    }*/

    /**
     * Wrapper class for RecordCallAfterWorkingHoursWebService request body
     */
    public class RecordCallAfterWorkingHoursRequestBody {
        public String calledDateTime {get; set;}
        public String calledNumber {get; set;}
        public String callingNumber {get; set;}
        public Boolean callback {get; set;}

        public RecordCallAfterWorkingHoursRequestBody() {
            this.calledDateTime = '';
            this.calledNumber = '';
            this.callingNumber = '';
            this.callback = false;
        }
    }

    /**
     * Wrapper class for RecordCallAfterWorkingHoursWebservice response body
     */
    public class RecordCallAfterWorkingHoursResponse {
        public String status;
        public String errorCode;
        public String errorMessage;

        public RecordCallAfterWorkingHoursResponse() {
            this.status = '';
            this.errorCode = '';
            this.errorMessage = '';
        }
    }
}