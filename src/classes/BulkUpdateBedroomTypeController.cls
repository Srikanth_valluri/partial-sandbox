/*********************************************************************************************************
* Name               : BulkUpdateBedroomTypeController
* Test Class         : 
* Description        : Controller class for BulkUpdateBedroomType VF Page
* Created Date       : 09/11/2020
* -----------------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst        129/11/2020      Initial Draft.
**********************************************************************************************************/
global With Sharing Class BulkUpdateBedroomTypeController{
    Public Blob contentFile {get;set;}
    public List<String> fileLines; 
    public String loadMsg {get; set;}
    public List<String> csvFileLines{get;set;}
    Public String asString{get;set;}
    public string invListJSON {get; set;}
    public string invNameBedroomTypeMapJson {get; set;}
    Public Map<String, String> invNameBedroomTypeMap;
    Public List<Inventory__c> invToUpdate {get; set;}

   
    /*********************************************************************************************
    * @Description : Controller class.
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public BulkUpdateBedroomTypeController() {
        csvFileLines = new List<String>();
        fileLines = new List<String>();
        loadMsg = '';
        invNameBedroomTypeMap = new Map<String, String>();
        invToUpdate = new List<Inventory__c>();
        invListJSON = JSON.serialize(invToUpdate);
        invNameBedroomTypeMapJson = JSON.serialize(invNameBedroomTypeMap);
    }
    
    public void updateInventory(){
        loadMsg = '';
        if(invToUpdate.size() > 0){
            List<Inventory_Log__c> invLogList = new List<Inventory_Log__c>();
            for(Inventory__c inv: invToUpdate){
                String newBedroomType = invNameBedroomTypeMap.get(inv.Unit_Name__c).trim();
                if(inv.Bedroom_Type__c != newBedroomType){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Bedroom_Type__c', 
                                   inv.Bedroom_Type__c, newBedroomType, 'Bulk Update Bedroom Type - csv upload', ''));
                    inv.Bedroom_Type__c = invNameBedroomTypeMap.get(inv.Unit_Name__c).trim();
                }
            }
            try{
                if(invLogList.size() > 0){
                    update invToUpdate;
                    insert invLogList;
                }
                invToUpdate = new List<Inventory__c>();
                for(Inventory__c inv: [SELECT Id, Name, Unit_Name__c, Price__c,
                                        Special_Price__c, Building_Location__r.Building_Name__c,
                                        Building_Location__c, Space_Type_Lookup_Code__c, Bedroom_Type__c,
                                        View_Type__c, Type_Mktg__c, Unit_Area_sft__c, status__c,
                                        Property_Name_2__c, Marketing_Name__c, Total_Area_JOPD_sft__c
                                       FROM Inventory__c
                                       WHERE Unit_Name__c IN: invNameBedroomTypeMap.keyset()]){
                    invToUpdate.add(inv);
                }
                system.debug('invToUpdate: ' + invToUpdate);
                if(invToUpdate.size() > 0){
                    loadMsg = 'Inventories Updated Successfully';
                    invListJSON = JSON.serialize(invToUpdate);
                }
            } catch (exception e){
                loadMsg = 'An Error Occured while Updating Inventories, please contact the administrator.';
                System.debug('Excetpion: ' + e.getMessage() + ': ' +  e.getLineNumber());
            }
        }
    }
    
    public void importDataFromCSVController(){
        loadMsg = '';
        String fileString =  system.ApexPages.currentPage().getParameters().get('fileBlob');
        contentFile = EncodingUtil.base64Decode(fileString);
        system.debug('contentFile:' + contentFile); 
        invNameBedroomTypeMap = new Map<String, String>();
        try{
            if(contentFile != null){
                asString = blobToString(contentFile,'ISO-8859-1');
                fileLines= AsString.split('\n');
                system.debug('fileLines Size: ' + fileLines);
                if(fileLines[0] != null && fileLines[0] != ''){
                    String unitHeader = fileLines[0].split(',')[0];
                    String bedroomTypeHeader = fileLines[0].split(',')[1];
                    system.debug('unitHeader: ' + unitHeader);
                    system.debug('bedroomTypeHeader: ' + bedroomTypeHeader);
                    if(unitHeader == null || unitHeader == '' || unitHeader.toLowercase().trim() != 'unit'
                           || bedroomTypeHeader == null || bedroomTypeHeader == '' 
                           || bedroomTypeHeader.toLowercase().trim() != 'bedroom type' ){
                        loadMsg = 'Please Upload a valid File';
                        system.debug('Invalid File - Invalid Headers');
                        
                    } else{
                        system.debug('Valid File');
                        for(Integer i=1; i<fileLines.size(); i++){
                            List<String> csvRecordData = new List<String>();
                            
                            csvRecordData = fileLines[i].split(',');
                            System.debug('csvRecordData | ' + csvRecordData);
                            if(csvRecordData[0] != null && csvRecordData[0] != '' 
                                    && csvRecordData[1] != null && csvRecordData[1].trim() != '') {
                                invNameBedroomTypeMap.put(csvRecordData[0], csvRecordData[1].trim());
                            }
                           
                        }
                        System.debug('invNameBedroomTypeMap: ' + invNameBedroomTypeMap);
                        if(invNameBedroomTypeMap.keyset().size() > 0){
                            invNameBedroomTypeMapJson = JSON.serialize(invNameBedroomTypeMap);
                            invToUpdate = new List<Inventory__c>();
                            for(Inventory__c inv: [SELECT Id, Name, Unit_Name__c, Price__c,
                                                    Special_Price__c, Building_Location__r.Building_Name__c,
                                                    Building_Location__c, Space_Type_Lookup_Code__c, Bedroom_Type__c,
                                                    View_Type__c, Type_Mktg__c, Unit_Area_sft__c, status__c
                                                   FROM Inventory__c
                                                   WHERE Unit_Name__c IN: invNameBedroomTypeMap.keyset()]){
                                invToUpdate.add(inv);
                            }
                            system.debug('invToUpdate: ' + invToUpdate);
                            if(invToUpdate.size() > 0){
                                loadMsg = 'File Uploaded Successfully';
                                invListJSON = JSON.serialize(invToUpdate);
                                
                            }
                        }
                    }
                }  else{
                     loadMsg = 'Please Upload a valid File';
                 }
             } else{
                 loadMsg = 'Please Upload a valid File';
             }
          } catch (Exception e) {
            loadMsg = 'An Error Occured while Uploading the File, please contact the administrator.';
            System.debug('Excetpion: ' + e.getMessage() + ': ' +  e.getLineNumber());
        } 
        system.debug('loadMsg: ' + loadMsg);
        
    }

    
    public static String blobToString(Blob input, String inCharset){
        
        String hex = EncodingUtil.convertToHex(input);
        System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() >> 1;
        String[] bytes = new String[bytesCount];
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i << 1, 2);
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
    } 
}