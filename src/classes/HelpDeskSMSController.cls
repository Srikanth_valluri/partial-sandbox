/**
 * @File Name          : HelpDeskSMSController.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 1/30/2020, 3:53:30 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    1/20/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class HelpDeskSMSController{ 
    private static final Id HELPDESK_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('FM_Case__c', 'Helpdesk');
    @InvocableMethod
    public static void callTheSMSService(List<Id> fmCaseIdList){
        List<FM_Case__c> fmHelpDeskCaseRecord = new List<FM_Case__c>();
        System.debug('fmCaseIdList:::'+fmCaseIdList);
        if(fmCaseIdList != NULL){
         fmHelpDeskCaseRecord = [
            SELECT Id,
                    Name,
                    Notice_Type__c,
                    OwnerId,
                    Owner.Email,
                    CreatedbyId,
                    Createdby.email,
                    RecordTypeId,
                    Tenant__c,
                    Tenant_Email__c,
                    Booking_Unit__c,
                    Email__c,
                    Account__c,
                    Unit_Name__c,
                    Approval_Status__c,
                    Email_Sent_Dates__c,
                    Booking_Unit__r.Name,
                    Tenant__r.Party_ID__c,
                    Tenant__r.Mobile_Phone_Encrypt__pc,
                    Account__r.Mobile_Phone_Encrypt__pc, 
                    Account__r.Mobile_Person_Business__c, 
                    Property_Manager_Name__c,
                    Property_Manager_Number__c,
                    Booking_Unit__r.Unit_Name__c,
                    Account_Email__c,
                    CAFM_Task_State__c,
                    Status__c,
                    Property_Manager_Email__c,
                    Building_Email__c,
                    Issue_Rectified_Batch_Run_Date__c
               FROM FM_Case__c 
              WHERE RecordTypeId = :HELPDESK_RECORD_TYPE_ID
              AND Id IN : fmCaseIdList
              AND ( Status__c = 'Closed'
                    OR  Status__c = 'Submitted'
                    OR CAFM_Task_State__c = 'Assigned')
            ];
        }
        // Replace the fM case Id in sms content body
        System.debug('fmHelpDeskCaseRecord:::::::'+fmHelpDeskCaseRecord);
        List<String> parameters = new List<String>();
        if(fmHelpDeskCaseRecord != NULL){
            parameters.add(fmHelpDeskCaseRecord[0].Name);
        }

        // Send the sms if CAFM task state is ASsigned
        if(fmHelpDeskCaseRecord != NULL && fmHelpDeskCaseRecord[0].CAFM_Task_State__c == 'Assigned'){
            String smsBody =  Label.Assigned_Cafm_Task_SMS_Template;
            String replacedSmsContent = String.format(smsBody, parameters);
            System.debug('replacedSmsContent::in assigned:'+replacedSmsContent);
            
            //Call method to send SMS
            SendSMSAccountService.Sendtextmessage(
                new List<String>{fmHelpDeskCaseRecord[0].Account__r.Mobile_Person_Business__c}
                , replacedSmsContent
                , fmHelpDeskCaseRecord[0].Account__c
                , true
                , fmHelpDeskCaseRecord[0].Id
            );
        }

        // Send the sms if Status is closed
         else if(fmHelpDeskCaseRecord != NULL && fmHelpDeskCaseRecord[0].Status__c == 'Closed'){
            String smsBody =  Label.SMS_for_Closed_Help_Desk_SR;
            String replacedSmsContent = String.format(smsBody, parameters);
            System.debug('replacedSmsContent::at closed:'+replacedSmsContent); 
            
            //Call method to send SMS
            SendSMSAccountService.Sendtextmessage(
                new List<String>{fmHelpDeskCaseRecord[0].Account__r.Mobile_Person_Business__c}
                , replacedSmsContent
                , fmHelpDeskCaseRecord[0].Account__c
                , true
                , fmHelpDeskCaseRecord[0].Id
            );
        }
        // Send the sms if Status is closed
         else if(fmHelpDeskCaseRecord != NULL && fmHelpDeskCaseRecord[0].Status__c == 'Submitted'){
            String smsBody =  Label.Help_Desk_SMS_on_Submission;
            String replacedSmsContent = String.format(smsBody, parameters);
            System.debug('replacedSmsContent:in submission::'+replacedSmsContent); 
            
            //Call method to send SMS
            SendSMSAccountService.Sendtextmessage(
                new List<String>{fmHelpDeskCaseRecord[0].Account__r.Mobile_Person_Business__c}
                , replacedSmsContent
                , fmHelpDeskCaseRecord[0].Account__c
                , true
                , fmHelpDeskCaseRecord[0].Id
            );
        }
    } 
}