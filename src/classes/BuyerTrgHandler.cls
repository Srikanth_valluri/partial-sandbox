/**************************************************************************************************
* Name               : BuyerTrgHandler                                                            *
* Test Class         : BuyerTrgHandlerTest                                                        *
* Description        : This is the trigger handler class for Buyer object.                        *
* Created Date       : 16/04/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR      DATE                                                                    *
* 1.0         Kaavya      16/04/2017                                                              *
* 1.1         Rahul       15/08/2017  Added CreateAndUpdateDealRelationship method to create      *
*                                       record in the Deal Relationship object.                   *
* 1.2         QBurst      26/02/2020  Added convertJointBuyer method to convert JBs into account  *
*                                           and create Unit Documents on Insert                   *
* 1.3         QBurst      26/02/2020    Commented out REST callout which updates IPMS Status      *
**************************************************************************************************/
public class BuyerTrgHandler implements TriggerFactoryInterface  {

    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after update.                        *
    * @Params      : Map<Id, sObject>, Map<Id, sObject>                                          *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        /* Calling method to sync the associated account details in case the values are updated on buyer record in salesforce. */
        syncAccount(mapNewRecords,mapOldRecords);
    }

    /*********************************************************************************************
    * @Description : Method to contain logic to be executed before update.                       *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeBeforeUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        List<Buyer__c> updBuyerList = new List<Buyer__c>();
        List<id> PBookingIds = new List<id>();
        Set<id> JBookingIds = new Set<id>();
        // Added By Monali 14-06-2018 START
        try{
          system.debug('Inside trigger==');
            for(Id thisKey : mapNewRecords.keySet()){
                Buyer__c newBuyer = (Buyer__c)mapNewRecords.get(thisKey);
                Buyer__c oldBuyer = (Buyer__c)mapOldRecords.get(thisKey);
                system.debug('OldBuyer===' + oldBuyer);
                system.debug('NewBuyer===' + newBuyer);
                Boolean PU = false;
                if(newBuyer.Registered__c){
                    //Tracking field changes for Buyer
                    system.debug('Inside if****');
                    //Tracking Address changes
                    if(newBuyer.Address_Line_1__c != oldBuyer.Address_Line_1__c || newBuyer.Address_Line_2__c != oldBuyer.Address_Line_2__c 
                            || newBuyer.Address_Line_3__c != oldBuyer.Address_Line_3__c || newBuyer.Address_Line_4__c != oldBuyer.Address_Line_4__c 
                            || newBuyer.City__c != oldBuyer.City__c || newBuyer.Country__c != oldBuyer.Country__c){
                        newBuyer.Address_Changed__c = true;
                        if(newBuyer.Primary_Buyer__c) {
                            PU = true;
                        }
                    }
                    //Tracking Email and Phone no changes
                    if(newBuyer.Email__c != oldBuyer.Email__c || newBuyer.Phone_Country_Code__c != oldBuyer.Phone_Country_Code__c 
                                ||newBuyer.Phone__c != oldBuyer.Phone__c){
                        newBuyer.Contact_Changed__c = true;   
                        if(newBuyer.Primary_Buyer__c) {
                            PU = true;
                        }
                    }
                    //Tracking Name & nationality changes
                    if(newBuyer.Title__c != oldBuyer.Title__c || newBuyer.First_Name__c != oldBuyer.First_Name__c 
                                || newBuyer.Last_Name__c != oldBuyer.Last_Name__c || newBuyer.Nationality__c != oldBuyer.Nationality__c 
                                || newBuyer.Organisation_Name__c != oldBuyer.Organisation_Name__c) {  
                        newBuyer.Name_Changed__c = true;
                        if(newBuyer.Primary_Buyer__c){
                            PU = true;
                        }
                        if(newBuyer.Title__c != oldBuyer.Title__c || newBuyer.First_Name__c != oldBuyer.First_Name__c 
                                    || newBuyer.Last_Name__c != oldBuyer.Last_Name__c) {
                            newBuyer.Name_Only_Changed__c = true; 
                        }
                        if(newBuyer.Nationality__c != oldBuyer.Nationality__c) {
                            newBuyer.Nationality_Changed__c = true;
                        }
                        if(newBuyer.Organisation_Name__c != oldBuyer.Organisation_Name__c) {
                            newBuyer.Org_Name_Changed__c = true; 
                        }
                    }
                    //Tracking Passport details changes if individual and Org registration details if corporate
                    if(newBuyer.Passport_Number__c != oldBuyer.Passport_Number__c || newBuyer.Place_of_Issue__c != oldBuyer.Place_of_Issue__c
                            || newBuyer.Passport_Expiry_Date__c != oldBuyer.Passport_Expiry_Date__c
                            || newBuyer.CR_Registration_Expiry_Date__c != oldBuyer.CR_Registration_Expiry_Date__c 
                            || newBuyer.CR_Registration_Place__c != oldBuyer.CR_Registration_Place__c){
                        newBuyer.Passport_Changed__c = true;
                        if(newBuyer.Primary_Buyer__c) {
                            PU = true;
                        }
                    }
                    // if both name & nationality and passport details changes
                    if(newBuyer.Name_Changed__c && newBuyer.Passport_Changed__c) {
                        newBuyer.Name_Passport_Changed__c = true;
                        newBuyer.Name_Changed__c = false;
                        newBuyer.Passport_Changed__c = false;
                    }
                    if(newBuyer.Primary_Buyer__c && PU ){
                        PBookingIds.add(newBuyer.Booking__c);
                    } else{ 
                        // Tracking changes for Joint Buyer
                        if(newBuyer.Primary_Buyer__c == false && newBuyer.Is_3rd_Party__c == false){
                            if(newBuyer.Status__c == 'Active' && oldBuyer.Status__c == 'Active'){
                                newBuyer.Status__c = 'Updated';
                            }
                            if(newBuyer.Status__c == 'Updated' || newBuyer.Status__c == 'Removed'){
                                JBookingIds.add(newBuyer.Booking__c);
                            }
                        }
                    }
                }
            }
        } catch(exception ex){
            system.debug('#### Exception at line number = ' + ex.getLineNumber() + ' , Exception message = ' + ex.getMessage());
        }
    }

    //Sync Buyer to Account
    public static void syncAccount(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        List<Inquiry_Conversion_Mapping__c> lstAccBuyrMappings = Inquiry_Conversion_Mapping__c.getall().values();
        boolean isSyncReq = false;
        //check for field update from the list of fields under Inquiry_Conversion_Mapping__c >> Buyer__c
        for(Id thisKey : mapNewRecords.keySet()){
            Buyer__c newBuyer = (Buyer__c)mapNewRecords.get(thisKey);
            Buyer__c oldBuyer = (Buyer__c)mapOldRecords.get(thisKey);
            for(Inquiry_Conversion_Mapping__c buyrField : lstAccBuyrMappings){
                if(buyrField.Buyer__c != null && newBuyer.get(buyrField.Buyer__c) != oldBuyer.get(buyrField.Buyer__c)){
                    isSyncReq = true;
                    break;
                }
            }
        }
        System.debug('>>> isSyncReq :'+isSyncReq);
        if(isSyncReq){
            try{
                boolean isIndividual = false;
                List<Account> lstAccntUpdates = new list<Account>();
                for(Id thisKey : mapNewRecords.keySet()){
                    Buyer__c newBuyer = (Buyer__c)mapNewRecords.get(thisKey);
                    if(newBuyer.Account__c != null && newBuyer.Buyer_Type__c != null){
                        System.debug('>>> newBuyer.Account__c :' + newBuyer.Account__c + '  -->>  ' + newBuyer.Buyer_Type__c);
                        isIndividual = (newBuyer.Buyer_Type__c == 'Individual') ? true : false;
                        Account acc = new Account(id = newBuyer.Account__c);
                        for(Inquiry_Conversion_Mapping__c buyrField : lstAccBuyrMappings){
                            System.debug('>>> buyrField :' + buyrField.Buyer__c + ' --->>> ' + buyrField.Person_Account_Field_Name__c 
                                        + ' --->> ' + buyrField.Business_Account_Field_Name__c + ' isIndividual ' + isIndividual);
                            if(!string.isBlank(buyrField.Buyer__c) && newBuyer.get(buyrField.Buyer__c) != null){
                                try{
                                    if(isIndividual && !string.isBlank(buyrField.Person_Account_Field_Name__c)){
                                        system.debug('buyer field value=' + newBuyer.get(buyrField.Buyer__c));
                                        acc.put(buyrField.Person_Account_Field_Name__c,newBuyer.get(buyrField.Buyer__c));
                                    } else if(!string.isBlank(buyrField.Business_Account_Field_Name__c)){
                                        acc.put(buyrField.Business_Account_Field_Name__c,newBuyer.get(buyrField.Buyer__c));
                                    }
                                } catch(exception ex){
                                    System.debug('>>> SyncAccount Error :' + ex.getMessage() 
                                        + ' >> Please make sure the mappings are proper in custom settings Inquiry_Conversion_Mapping__c.');
                                }
                            }
                        }
                        lstAccntUpdates.add(acc);
                    }
                }
                if(lstAccntUpdates != null && !lstAccntUpdates.isempty()){
                    update lstAccntUpdates;
                }
            } catch(exception ex){
                System.debug('>>> SyncAccount Error :' + ex.getMessage() 
                    + ' >> Please make sure the mappings are proper in custom settings Inquiry_Conversion_Mapping__c.');
            }
        }
    }

    
    public void executeAfterInsertTrigger(Map<Id, sObject> mapNewRecords){ 
        convertJointBuyer(mapNewRecords.keyset()); // 1.2 
    }

    // 1.2 starts
    /*********************************************************************************************
    * @Description : Method to convert joint buyers into Accounts and create unit docs           *
    * @Params      : Set<Id> - Set of Joint Buyer Ids                                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public  static void convertJointBuyer(Set<Id> BuyerIdList){
        Map<Id, Buyer__c> jbIdMap = new Map<Id, Buyer__c>();
        for(Buyer__c newBuyer : [SELECT Id, Booking__r.Deal_SR__c, Booking__c,
                                    Booking__r.Deal_SR__r.RecordType.DeveloperName
                                 FROM Buyer__c
                                 WHERE Id IN: BuyerIdList
                                 AND Primary_Buyer__c = FALSE] ){
            jbIdMap.put(newBuyer.Id, newBuyer);
        }
        system.debug('jbIdMap: ' + jbIdMap);
        if(!jbIdMap.keyset().isEmpty()){
            BuyerConversionProcess.convertBuyer(jbIdMap.keyset());
            for(Buyer__c jBuyer: jbIdMap.values()){
                Invocable_ManageBuyerDocs.createDocsForBuyer(jBuyer.Booking__r.Deal_SR__c, jBuyer.Booking__r.Deal_SR__r.RecordType.DeveloperName);
            }
        }
    }
    // 1.2 ends

    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> oldRecordsMap){ }
    public void executeBeforeDeleteTrigger(Map<Id,sObject> oldRecordsMap){ }  
    public void executeAfterDeleteTrigger(Map<Id,sObject> oldRecordsMap){ }
    public void executeBeforeInsertUpdateTrigger(list<sObject> BuyerList,map<Id,sObject> mapOldRecords){ }
    public void executeBeforeInsertTrigger(List<sObject> BuyerList){ }
}