@RestResource(urlMapping='/SMSResponse/*')
global class Damac_SecureSMSCallback {

    @HttpGet
    global static void SMSResponse () {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        System.Debug ('Request params'+req.params);
        String msgId = req.params.get ('qMsgRef');
        String msgResponse = req.params.get ('qStatus');
        String qPartNo = req.params.get ('qPartNo');
        String qSubmitDate = req.params.get ('qSubmitDate');
        String qDoneDate = req.params.get ('qDoneDate');
        String qString = req.params.get ('qString');
        String inquiryId = req.params.get ('InquiryId');
        
        if(inquiryId !='' && inquiryId!= NULL){
            string objprefix = String.valueOf(inquiryId).substring(0,3);
            String objectName = findObjectNameFromRecordIdPrefix(objprefix);
            System.debug(objectName);
            sobject inquiry;
            if (msgId != NULL && msgId != '') {
                string queryString = '';
                queryString = 'SELECT ';
                if (objectName.toLowerCase () == 'inquiry__c') {
                    queryString += ' Mobile2_SMS_MSG_Id__c, ';
                }
                queryString += ' SMS_MSG_Id__c, SMS_Response_Status__c FROM '+objectName+' WHERE ID=:inquiryId LIMIT 1';
                inquiry = database.query(queryString);
                
                if (objectName.toLowerCase() == 'inquiry__c') {
                    if (msgId == inquiry.get ('SMS_MSG_Id__c')) {                
                        inquiry.put('SMS_Response_Status__c',msgResponse);
                        inquiry.put('SMS_Part_No__c',qPartNo);
                        inquiry.put('SMS_Submit_At__c',DateTime.newInstance(Integer.valueOf (qSubmitDate.subString (0,2))+2000, Integer.valueOf (qSubmitDate.subString (2,4)), Integer.valueOf (qSubmitDate.subString (4, 6)), Integer.valueOf (qSubmitDate.subString (6, 8)), Integer.valueOf (qSubmitDate.subString (8, 10)), 00).addHours (-4));
                        inquiry.put('SMS_Done_At__c',DateTime.newInstance(Integer.valueOf (qDoneDate.subString (0,2))+2000, Integer.valueOf (qDoneDate.subString (2,4)), Integer.valueOf (qDoneDate.subString (4, 6)), Integer.valueOf (qDoneDate.subString (6, 8)), Integer.valueOf (qDoneDate.subString (8, 10)), 00).addHours (-4));
                    } 
                    if (msgId == inquiry.get ('Mobile2_SMS_MSG_Id__c')) {
                        inquiry.put('SMS_2_Response_Status__c',msgResponse);
                        inquiry.put('SMS_2_Part_No__c',qPartNo);
                        inquiry.put('SMS_2_Submit_At__c',DateTime.newInstance(Integer.valueOf (qSubmitDate.subString (0,2))+2000, Integer.valueOf (qSubmitDate.subString (2,4)), Integer.valueOf (qSubmitDate.subString (4, 6)), Integer.valueOf (qSubmitDate.subString (6, 8)), Integer.valueOf (qSubmitDate.subString (8, 10)), 00).addHours (-4));
                        inquiry.put('SMS_2_Done_At__c',DateTime.newInstance(Integer.valueOf (qDoneDate.subString (0,2))+2000, Integer.valueOf (qDoneDate.subString (2,4)), Integer.valueOf (qDoneDate.subString (4, 6)), Integer.valueOf (qDoneDate.subString (6, 8)), Integer.valueOf (qDoneDate.subString (8, 10)), 00).addHours (-4));
                    }
                } else {
                    inquiry.put('SMS_Response_Status__c',msgResponse);
                    inquiry.put('SMS_Part_No__c',qPartNo);
                    inquiry.put('SMS_Submit_At__c',DateTime.newInstance(Integer.valueOf (qSubmitDate.subString (0,2))+2000, Integer.valueOf (qSubmitDate.subString (2,4)), Integer.valueOf (qSubmitDate.subString (4, 6)), Integer.valueOf (qSubmitDate.subString (6, 8)), Integer.valueOf (qSubmitDate.subString (8, 10)), 00).addHours (-4));
                    inquiry.put('SMS_Done_At__c',DateTime.newInstance(Integer.valueOf (qDoneDate.subString (0,2))+2000, Integer.valueOf (qDoneDate.subString (2,4)), Integer.valueOf (qDoneDate.subString (4, 6)), Integer.valueOf (qDoneDate.subString (6, 8)), Integer.valueOf (qDoneDate.subString (8, 10)), 00).addHours (-4));
                }
                Update inquiry;
                /*
                sobject = [SELECT Push_To_Inquiry__c, SMS_MSG_Id__c, SMS_Response_Status__c FROM Stand_Inquiry__c 
                                WHERE ID  =: inquiryId];
                stdInquiry.SMS_Response_Status__c = msgResponse;
                if (msgResponse.toLowerCase() == 'delivered') {
                    stdInquiry.Push_To_Inquiry__c = TRUE;
                }
                stdInquiry.SMS_Part_No__c = qPartNo;
                stdInquiry.SMS_Submit_At__c = DateTime.newInstance(Integer.valueOf (qSubmitDate.subString (0,2))+2000, Integer.valueOf (qSubmitDate.subString (2,4)), Integer.valueOf (qSubmitDate.subString (4, 6)), Integer.valueOf (qSubmitDate.subString (6, 8)), Integer.valueOf (qSubmitDate.subString (8, 10)), 00).addHours (-4);
                stdInquiry.SMS_Done_At__c = DateTime.newInstance(Integer.valueOf (qDoneDate.subString (0,2))+2000, Integer.valueOf (qDoneDate.subString (2,4)), Integer.valueOf (qDoneDate.subString (4, 6)), Integer.valueOf (qDoneDate.subString (6, 8)), Integer.valueOf (qDoneDate.subString (8, 10)), 00).addHours (-4);
                Update stdInquiry;
                */
            }
        }
    }
    
    
    
    public static String findObjectNameFromRecordIdPrefix(String recordIdOrPrefix){
        String objectName = '';
        try{
            //Get prefix from record ID
            //This assumes that you have passed at least 3 characters
            String myIdPrefix = String.valueOf(recordIdOrPrefix).substring(0,3);
             
            //Get schema information
            Map<String, Schema.SObjectType> gd =  Schema.getGlobalDescribe(); 
             
            //Loop through all the sObject types returned by Schema
            for(Schema.SObjectType stype : gd.values()){
                Schema.DescribeSObjectResult r = stype.getDescribe();
                String prefix = r.getKeyPrefix();
                System.debug('Prefix is ' + prefix);
                 
                //Check if the prefix matches with requested prefix
                if(prefix!=null && prefix.equals(myIdPrefix)){
                    objectName = r.getName();
                    System.debug('Object Name! ' + objectName);
                    break;
                }
            }
        }catch(Exception e){
            System.debug(e);
        }
        return objectName;
    }

}