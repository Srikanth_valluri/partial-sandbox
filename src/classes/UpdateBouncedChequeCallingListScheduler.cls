/*-------------------------------------------------------------------------------------------------
Description: Scheduler for UpdateBouncedChequeCallingListBatch batch

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 05-12-2017       | Lochana Rajput   | To schedule UpdateBouncedChequeCallingListBatch batch to run at every midnight
   =============================================================================================================================
*/
public without sharing class UpdateBouncedChequeCallingListScheduler implements Schedulable {

    public UpdateBouncedChequeCallingListScheduler() {}

    /*public UpdateBouncedChequeCallingListScheduler(String jobName) {
        UpdateBouncedChequeCallingListScheduler objScheduler = new UpdateBouncedChequeCallingListScheduler();
        System.schedule(jobName, '0 0 0 * * ? *', objScheduler);
    }*/
    
    public void execute(SchedulableContext sc) {
        Database.executebatch(new UpdateBouncedChequeCallingListBatch(), 1);
    }
}