public without sharing class ZoomServiceUtility {
    
    public static void createMeeting(Task objTask){
        try{
            Inquiry__c objInquiry;
            for(Inquiry__c obj : [select Id,Name,Online_Meeting_Start_Date_Time__c from Inquiry__c where Id=:objTask.WhatId]){
                objInquiry = obj;
            }

            ZoomRequest zoomReq = new ZoomRequest();
            zoomReq.topic = 'Meeting with Property Advisor ref:'+objInquiry.Name;
            zoomReq.type = 2;
            zoomReq.start_time = getTimeZoomTime(objInquiry.Online_Meeting_Start_Date_Time__c);
            zoomReq.duration = getMins(objInquiry.Online_Meeting_Start_Date_Time__c, objInquiry.Online_Meeting_End_Date_Time__c);
            zoomReq.agenda = 'Meeting with Property Advisor ref:'+objInquiry.Name;
            
            RequestSettings objReqSettings = new RequestSettings();
            objReqSettings.host_video = true;
            objReqSettings.registration_type = 1;
            objReqSettings.auto_recording = 'cloud';
            zoomReq.settings = objReqSettings;

            string reqBody = JSON.serialize(zoomReq); 
            HttpRequest req = new HttpRequest();
            req.setEndpoint('https://api.zoom.us/v2/users/gDrRyn8oQcm86VZQwluicQ/meetings');
            req.setMethod('POST');
            req.setHeader('ACCEPT','application/json');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6ImhnLVhVVW9tUlRLTXp2LXVxTXE5S1EiLCJleHAiOjE2NDI5NjA5MjAsImlhdCI6MTYxMTQxOTY4OX0.4f4ecu_dbnaIF-J9rkWoe36C14hDJhaINldc4U6wPh8');
            req.setTimeout(120000);
            req.setBody(reqBody);
            Http http = new Http();
            HTTPResponse res;
            res = http.send(req);

            if(res != null && res.getBody() != null){
                ZoomResponse response = (ZoomResponse)System.JSON.deserialize(res.getBody(), ZoomResponse.class);

            }

        }catch(Exception ex){

        }
    }

    public static string getMeetingInvitation(string meetingId){
        HttpRequest req = new HttpRequest();
            req.setEndpoint('https://api.zoom.us/v2/meetings/'+meetingId+'/invitation');
            req.setMethod('GET');
            req.setHeader('ACCEPT','application/json');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6ImhnLVhVVW9tUlRLTXp2LXVxTXE5S1EiLCJleHAiOjE2NDI5NjA5MjAsImlhdCI6MTYxMTQxOTY4OX0.4f4ecu_dbnaIF-J9rkWoe36C14hDJhaINldc4U6wPh8');
            req.setTimeout(120000);
            Http http = new Http();
            HTTPResponse res;
            res = http.send(req);
            map<string, string> mapRes = (map<string, string>)System.JSON.deserialize(res.getBody(), map<string, string>.class);
            return mapRes.get('invitation');
    }


    //RecorndId can be User, Contact
    public static void addParticipant(string email, string firstName, string lastName, string meetingId){
        try{
            
            Registrants objReg = new Registrants();
            objReg.email = email;
            objReg.first_name = firstName;
            objReg.last_name = lastName;

            string reqBody = JSON.serialize(objReg); 
            HttpRequest req = new HttpRequest();
            req.setEndpoint('https://api.zoom.us/v2/meetings/'+meetingId+'/registrants');
            req.setMethod('POST');
            req.setHeader('ACCEPT','application/json');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6ImhnLVhVVW9tUlRLTXp2LXVxTXE5S1EiLCJleHAiOjE2NDI5NjA5MjAsImlhdCI6MTYxMTQxOTY4OX0.4f4ecu_dbnaIF-J9rkWoe36C14hDJhaINldc4U6wPh8');
            req.setTimeout(120000);
            req.setBody(reqBody);
            Http http = new Http();
            HTTPResponse res;
            res = http.send(req);

            if(res != null && res.getBody() != null){
                RegistrantReponse response = (RegistrantReponse)System.JSON.deserialize(res.getBody(), RegistrantReponse.class);

            }


        }catch(Exception ex){

        }
    }

    public static string getTimeZoomTime(Datetime dt){
        return dt.format('yyyy-MM-dd HH:mm:ss');
    }
    public static Integer getMins(Datetime st, Datetime et){
        Long dt1Long = st.getTime();
        Long dt2Long = et.getTime();
        Long milliseconds = dt2Long - dt1Long;
        Long seconds = milliseconds / 1000;
        Integer minutes = (Integer)(seconds / 60);
        //Long hours = minutes / 60;
        //Long days = hours / 24;
        return minutes;
    }

    public class ZoomRequest {
        public string topic;
        public Integer type;
        public string start_time;
        public Integer duration;
        public string schedule_for;
        public string timezone;
        public string password;
        public string agenda;
        public RecurrenceDetails recurrence;
        public RequestSettings settings;
    }
    public class RecurrenceDetails{
        public Integer type;
        public Integer repeat_interval;
        public string weekly_days;
        public Integer monthly_day;
        public Integer monthly_week;
        public Integer monthly_week_day;
        public Integer end_times;
        public string end_date_time;
    }

    public class RequestSettings{
        public boolean host_video;
        public boolean participant_video;
        public boolean cn_meeting;
        public boolean in_meeting;
        public boolean join_before_host;
        public boolean mute_upon_entry;
        public boolean watermark;
        public boolean use_pmi;
        public Integer approval_type;
        public Integer registration_type;
        public string audio;
        public string auto_recording;
        public boolean enforce_login;
        public string enforce_login_domains;
        public string alternative_hosts;
        public list<string> global_dial_in_countries;
        public boolean registrants_email_notification;
    }


    public class ZoomResponse {
        public String agenda {get;set;}
        public String created_at {get;set;}
        public Integer duration {get;set;}
        public String host_email {get;set;}
        public String host_id {get;set;}
        public decimal id {get;set;}
        public String join_url {get;set;}
        public SettingsResponse settings {get;set;}
        public String start_time {get;set;}
        public String start_url {get;set;}
        public String status {get;set;}
        public String timezone {get;set;}
        public String topic {get;set;}
        public Integer type {get;set;}// in json: type
        public String uuid {get;set;}        
    }

    public class GlobalDialInNumbers {
        public String country {get;set;}
        public String country_name {get;set;}
        public String number_z {get;set;}// in json: number
        public String type {get;set;}// in json: type
    }

    public class SettingsResponse {
        public Boolean allow_multiple_devices {get;set;}
        public String alternative_hosts {get;set;}
        public Integer approval_type {get;set;}
        public ApprovedDeniedCountriesRegions approved_or_denied_countries_or_regions {get;set;}
        public String audio {get;set;}
        public String auto_recording {get;set;}
        public Boolean close_registration {get;set;}
        public Boolean cn_meeting {get;set;}
        public String encryption_type {get;set;}
        public Boolean enforce_login {get;set;}
        public String enforce_login_domains {get;set;}
        public List<String> global_dial_in_countries {get;set;}
        public List<GlobalDialInNumbers> global_dial_in_numbers {get;set;}
        public Boolean host_video {get;set;}
        public Boolean in_meeting {get;set;}
        public Integer jbh_time {get;set;}
        public Boolean join_before_host {get;set;}
        public Boolean meeting_authentication {get;set;}
        public Boolean mute_upon_entry {get;set;}
        public Boolean participant_video {get;set;}
        public Boolean registrants_confirmation_email {get;set;}
        public Boolean registrants_email_notification {get;set;}
        public Boolean request_permission_to_unmute_participants {get;set;}
        public Boolean show_share_button {get;set;}
        public Boolean use_pmi {get;set;}
        public Boolean waiting_room {get;set;}
        public Boolean watermark {get;set;}
    }
    public class ApprovedDeniedCountriesRegions {
        public Boolean enable {get;set;}
    }

    public class Registrants {
        public String email {get;set;}
        public String first_name {get;set;}
        public String last_name {get;set;}
        public String address {get;set;}
        public String city {get;set;}
        public String country {get;set;}
        public String zip {get;set;}
        public String state {get;set;}
        public String phone {get;set;}
        public String industry {get;set;}
        public String org {get;set;}
        public String job_title {get;set;}
        public String purchasing_time_frame {get;set;}
        public String role_in_purchase_process {get;set;}
        public String no_of_employees {get;set;}
        public String comments {get;set;}
        public list<CustomQuestions> custom_questions {get;set;}
    }
    public class CustomQuestions {
        public String title {get;set;}
        public String value {get;set;}
    }

    public class RegistrantReponse {
        public String registrant_id {get;set;}
        public String id {get;set;}
        public String topic {get;set;}
        public String start_time {get;set;}
        public String join_url {get;set;}
    }

}