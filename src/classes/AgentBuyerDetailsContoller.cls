// CreatedBy : Naresh Kaneriya

public class AgentBuyerDetailsContoller{

public String BuyerId ;
public String SrNumber {get;set;}
public List<Buyer__c> BuyerList ;
public AgentBuyerDetailsContoller(){

BuyerId = ApexPages.currentPage().getParameters().get('BuyerId');
BuyerList  = new List<Buyer__c>();
System.debug('Buyer Id---------------------- ' +BuyerId);

}

 
public List<Buyer__c> getBuyerDetails(){
    
    
    System.debug('Buyer Id-------------Methodddd--------- ' +BuyerId);
    
     BuyerList  = [select id , 
                     Buyer_Type__c,  
                     Title__c,
                     First_Name__c,
                     Last_Name__c,
                     Nationality__c,
                     Passport_Number__c,
                     Place_of_Issue__c,
                     Date_of_Birth__c,
                     Email__c,
                     National_ID__c,
                     Primary_Buyer__c,
                     Phone_Country_Code__c,
                     Address_Line_1__c,
                     Address_Line_2__c,
                     Address_Line_3__c,
                     Address_Line_4__c,
                     Country__c,
                     City__c,
                     Title_Arabic__c, 
                     First_Name_Arabic__c,
                     Last_Name_Arabic__c,
                     Nationality_Arabic__c,
                     Passport_Expiry_Date__c,
                     Place_of_Issue_Arabic__c,
                     Organisation_Name__c,
                     Organisation_Name_Arabic__c,
                     CR_Number__c,
                     CR_Registration_Place__c,
                     CR_Registration_Place_Arabic__c,
                     CR_Registration_Expiry_Date__c,
                     Address_Line_1_Arabic__c,
                     Address_Line_2_Arabic__c,
                     Address_Line_3_Arabic__c,
                     Address_Line_4_Arabic__c,
                     Country_Arabic__c,
                     City_Arabic__c,
                     Booking__r.Deal_SR__r.Name
                     from Buyer__c where Id =: BuyerId order By CreatedDate DESC];
     
     SrNumber = BuyerList[0].Booking__r.Deal_SR__r.Name;                  
                    
                     return BuyerList  ;
                     
    
}

          public pagereference Back(){
            Pagereference pg =  new Pagereference('/apex/AgentServiceRequestDetails?SRId='+SrNumber ); 
            pg.setRedirect(true);
            return pg;
           }


}