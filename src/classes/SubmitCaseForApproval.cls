global with sharing class SubmitCaseForApproval {
    @InvocableMethod
    global static void submitCase( list<SR_Attachments__c> lstCustomAttachment ) {
        system.debug('SubmitCaseForApproval class called..');
        if( !lstCustomAttachment.isEmpty() ) {
            boolean isValid = false ;
            for( SR_Attachments__c objCust : [ SELECT Id , isValid__c
                                                   , Case__r.Amount_to_be_waived__c, Case__r.Status
                                                  FROM SR_Attachments__c 
                                                 WHERE Case__c = :lstCustomAttachment[0].Case__c ] ) {
                system.debug('== custom attachment object =='+objCust);
                if( objCust.Case__r.Status.equalsIgnoreCase('Submitted') ) {
                  if( objCust.isValid__c == false ) {
                      isValid = false ;
                      break;
                  }else {isValid = true ;}
                }else {break;}
            }
            system.debug('isValid =='+isValid );
            if( isValid ) {
                //makeCall( lstCustomAttachment[0].Id, lstCustomAttachment[0].Case__c ); 
                system.debug('lstCustomAttachment == '+lstCustomAttachment[0]);
                Case objCase = [ SELECT Id
                                     , Roles_from_Rule_Engine__c , Submit_for_Approval__c
                                     , Approving_Authorities__c , RecordTypeId 
                                FROM Case
                                WHERE Id = :lstCustomAttachment[0].Case__c LIMIT 1];
                objCase.Submit_for_Approval__c = true ;
                // 10 Nov 20 : Added if for PW as we are making call to SF instead of MQ for rules
                if( objCase.RecordTypeId == Schema.SObjectType.Case.getRecordTypeInfosByName().get('Penalty Waiver').getRecordTypeId() ) { 
                    string strAuthorities = SfRuleEngine.filterSfRuleEngine('Penalty Waiver', objCase.Id);
                    system.debug('== strAuthorities =='+strAuthorities);
                    if( String.isNotBlank( strAuthorities ) ) {
                        if( strAuthorities.containsIgnoreCase('System Approved') ) {
                            objCase.Approval_Status__c = 'Approved';objCase.Roles_from_Rule_Engine__c = strAuthorities.removeEnd(',') ;
                        } else {objCase.Roles_from_Rule_Engine__c = strAuthorities.removeEnd(',') ;objCase.Approving_Authorities__c = strAuthorities.removeEnd(',') ;}
                    }else {objCase.Approval_Status__c = 'Approved';}
                } else {
                    if( String.isNotBlank( objCase.Roles_from_Rule_Engine__c ) ) {
                      objCase.Submit_for_Approval__c = true ;
                      if( objCase.Roles_from_Rule_Engine__c.containsIgnoreCase('System Approved') ) {
                        objCase.Approval_Status__c = 'Approved';
                        //PenaltyWaiverService.updateDetails( new set<Id>{ objCase.Id } );
                      }else {objCase.Approving_Authorities__c = objCase.Roles_from_Rule_Engine__c.remove('CRM-');}
                    }objCase.Approving_Authorities__c = String.isNotBlank(objCase.Roles_from_Rule_Engine__c) ? objCase.Roles_from_Rule_Engine__c.remove('CRM-') : 'N/A';
                }
                update objCase ;
            }
        }
    }
}