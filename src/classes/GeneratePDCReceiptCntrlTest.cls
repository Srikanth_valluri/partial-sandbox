@isTest
public class GeneratePDCReceiptCntrlTest {
    
    @isTest
    static void testCLDetails1(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        Case objCase = new Case();
        objCase.AccountId = objAccount.Id;
        insert objCase;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c = 'test';
        objBU.Registration_ID__c = 'test';
        objBU.Booking__c = objBooking.Id;
        insert objBU;
        
        TriggerOnOffCustomSetting__c objTrigger = new TriggerOnOffCustomSetting__c();
        objTrigger.Name = 'CallingListTrigger';
        objTrigger.OnOffCheck__c = true;
        insert objTrigger;
        
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Unit_Name__c = 'test';       
        objCallingList.Account__c = objAccount.Id;
        objCallingList.Booking_Unit__c  = objBU.Id;
        insert objCallingList;
        
        Case_Post_Dated_Cheque__c objCasePost = new Case_Post_Dated_Cheque__c();
        objCasePost.Receipt_No__c =  '12345';
        objCasePost.Receipt_Date__c = System.today();
        objCasePost.New_maturity_date__c = System.today(); 
        objCasePost.New_Amount__c = 100;
        objCasePost.CurrencyIsoCode =  'AED';
        objCasePost.Reason__c = 'testReason';
        objCasePost.IPMS_Cash_Receipt_Id__c = '123456';
        objCasePost.Receipt_Classification__c = 'Early Handover';
        objCasePost.Mode_of_Payment__c = 'Cash';
        objCasePost.New_Drawer__c = ' testDrawer';
        objCasePost.New_Bank__c = 'testBank';
        objCasePost.Case__c = objCase.Id;
        objCasePost.Account__c = objAccount.Id;
        objCasePost.Calling_List__c = objCallingList.Id;
        objCasePost.Booking_Unit__c = objBU.Id;
        insert objCasePost;
        
        insert new IpmsRestServices__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
            Username__c = 'oracle_user',
            Password__c = 'crp1user',
            Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
            BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
            Timeout__c = 120000
        );
        
        String jsonStr =  '   {  '  +
            '       "responseId": "3072019-10305298",  '  +
            '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
            '       "status": "S",  '  +
            '       "responseMessage": "Process Completed",  '  +
            '       "elapsedTimeMs": 8497,  '  +
            '       "responseLines": [  '  +
            '           {  '  +
            '               "documentName": "DPSOA",  '  +
            '               "language": "EN",  '  +
            '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
            '           },  '  +
            '           {  '  +
            '               "documentName": "DPSOA_AR",  '  +
            '               "language": "AR",  '  +
            '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
            '           }  '  +
            '       ],  '  +
            '       "complete": true  '  +
            '  }  ' ;
        
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));
        List<String> responseStrlst = new List<String>();
        List<String> strlstForAssert = new List<String>{'https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd', 'https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a'};
            
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(objCasePost);
        GeneratePDCReceiptCntrl obj = new GeneratePDCReceiptCntrl(sc);
        obj.init();
        responseStrlst = FmIpmsRestCoffeeServices.getAllReceiptUrl(String.valueOf(objCasePost.IPMS_Cash_Receipt_Id__c));
        Test.stopTest();
    }
    
    @isTest
    static void testCLDetails2(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        Case objCase = new Case();
        objCase.AccountId = objAccount.Id;
        insert objCase;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c = 'test';
        objBU.Registration_ID__c = 'test';
        objBU.Booking__c = objBooking.Id;
        insert objBU;
        
        TriggerOnOffCustomSetting__c objTrigger = new TriggerOnOffCustomSetting__c();
        objTrigger.Name = 'CallingListTrigger';
        objTrigger.OnOffCheck__c = true;
        insert objTrigger;
        
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Unit_Name__c = 'test';       
        objCallingList.Account__c = objAccount.Id;
        objCallingList.Booking_Unit__c  = objBU.Id;
        insert objCallingList;
        
        Case_Post_Dated_Cheque__c objCasePost1 = new Case_Post_Dated_Cheque__c();
        objCasePost1.Receipt_No__c =  '12345';
        objCasePost1.Receipt_Date__c = System.today();
        objCasePost1.New_maturity_date__c = System.today(); 
        objCasePost1.New_Amount__c = 100;
        objCasePost1.CurrencyIsoCode =  'AED';
        objCasePost1.Reason__c = 'testReason';
        objCasePost1.IPMS_Cash_Receipt_Id__c = '';
        objCasePost1.Receipt_Classification__c = 'Early Handover';
        objCasePost1.Mode_of_Payment__c = 'Cash';
        objCasePost1.New_Drawer__c = ' testDrawer';
        objCasePost1.New_Bank__c = 'testBank';
        objCasePost1.Case__c = objCase.Id;
        objCasePost1.Account__c = objAccount.Id;
        objCasePost1.Calling_List__c = objCallingList.Id;
        objCasePost1.Booking_Unit__c = objBU.Id;
        insert objCasePost1;
        
        String jsonStr =  '   {  '  +
            '       "responseId": "3072019-10305298",  '  +
            '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
            '       "status": "S",  '  +
            '       "responseMessage": "Process Completed",  '  +
            '       "elapsedTimeMs": 8497,  '  +
            '       "responseLines": [  '  +
            '           {  '  +
            '               "documentName": "DPSOA",  '  +
            '               "language": "EN",  '  +
            '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
            '           },  '  +
            '           {  '  +
            '               "documentName": "DPSOA_AR",  '  +
            '               "language": "AR",  '  +
            '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
            '           }  '  +
            '       ],  '  +
            '       "complete": true  '  +
            '  }  ' ;
        
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));
        List<String> responseStrlst = new List<String>();
        List<String> strlstForAssert = new List<String>{'https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd', 'https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a'};
          
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(objCasePost1);
            GeneratePDCReceiptCntrl obj = new GeneratePDCReceiptCntrl(sc);
             obj.init();
            responseStrlst = FmIpmsRestCoffeeServices.getAllReceiptUrl('');
        Test.stopTest();
        
    }
}