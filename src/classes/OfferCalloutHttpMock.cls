@isTest
global Class OfferCalloutHttpMock implements HttpCalloutMock{
    
    global HttpResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type','application/json');
        String strResponse = '{"responseId":"2072019-112320366","responseTime":"Tue Jul 02 11:23:20 GMT+04:00 2019","responseMessage":"Offer Created with Offer Id = 94144","elapsedTimeMs":4884,"responseLines":[{"offerId":94144,"registrationId":105533,"offerText":"New Offer Created on 2-Jul-2019","offerCode":"P","offerValue":10000,"adjustInSalePrice":"34234","startDate":"01-Jan-2019","endDate":"01-Dec-2024"}],"complete":true}';
        res.setBody(strResponse );
        res.setStatusCode(200);
        return res;
    }   
}