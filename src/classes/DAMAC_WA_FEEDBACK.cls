public class DAMAC_WA_FEEDBACK{
  @InvocableMethod(label='Feedback' description='Capture WhatsApp Feedback')
  public static void sendFeedbackRequest(List<ID> ids) {
    list<office_meeting__c> inqLst = [select id,inquiry__r.Mobile_Phone__c from office_meeting__c where id IN:Ids];
   // list<inquiry__c> inqLst = [select id,Mobile_Phone__c from inquiry__c where id in:ids];
    Whats_App_Flows__c Findflow =[select id,Name,Menu__c from Whats_App_Flows__c where name='Feedback'];
    Id useflow = Schema.SObjectType.Whats_App_Flows__c.getRecordTypeInfosByName().get('User Flow').getRecordTypeId();
    
    // Assuming Feedback will be sent One at a time.
    //for(inquiry__c i:inqLst){
    for(office_meeting__c i:inqLst){
        string MobileNum = i.inquiry__r.Mobile_Phone__c.removestart('00');
        
        Whats_App_Flows__c flowThread = new Whats_App_Flows__c ();
        flowThread.Name = MobileNum;
        flowThread.Flow_For__c =  MobileNum+'-'+i+string.valueof(system.now());
        flowThread.User_Selected_Flow__c  = Findflow.id;
        flowThread.recordtypeid = useflow;        
        flowThread.Waiting_For_Input__c= true;        
        flowThread.Active_Flow__c = true;
        flowThread.Salesforce_Record_Id__c = i.id;
        upsert flowThread Flow_For__c; 
                                       
        
        // Send First Message
        DAMAC_WA_REST_FLOWS.doCallout(Findflow.menu__c,Findflow.id+'-'+flowThread.id,MobileNum);  
        
    }
    //list<string> nulllst = new list<string>();
    //return nulllst;
  }
}