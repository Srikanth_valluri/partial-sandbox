/***********************************************************************************************
* Description - Test class developed for HandoverDocumentGeneration
*
* Version            Date            Author                    Description
* 1.0                17/12/17        Naresh Kaneriya (Accely)   Initial Draft
*********************************************************************************************/


@isTest
public class HandoverDocumentGenerationTest{

@isTest static void getTest(){
      HandoverDocumentGenerationDtoComXsd.DocGenDTO objDocGenDTO=  new HandoverDocumentGenerationDtoComXsd.DocGenDTO();
      Test.startTest();
      HandoverDocumentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint obj =  new HandoverDocumentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint (); 
      SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverDocumentGeneration.DocGenerationResponse_element>();
      HandoverDocumentGeneration.DocGenerationResponse_element response1 = new HandoverDocumentGeneration.DocGenerationResponse_element();
      response1.return_x = 'S';
      SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
      Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
      obj.DocGeneration('TemplateName',objDocGenDTO);
      Test.stopTest();

}

}