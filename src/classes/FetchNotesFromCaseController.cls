public with sharing class FetchNotesFromCaseController {
  public String  accountId{get;set;}
  public static List<Account>AccountList{get;set;}
  public static  List<Case>lstOfAllCase{get;set;}
  public static Map<Id,Case>mapOFIdsToCase{get;set;}
  public static  List<Note>caseToNotesLst{get;set;}
  public static Map<Id,List<ContentDocumentLink>>caseToNoteLst{get;set;}
  public static list<ContentDocumentLink> contentDocLinkLst{get;set;}
  public static List<CaseToNotesWrapper>lstCaseToNotesWrapper{get;set;}
  public FetchNotesFromCaseController(ApexPages.StandardController controller) {
    fetchNotes();
    }
      /*This Method is used to fetch all the Notes of all the calling list*/
    public void fetchNotes(){
      lstCaseToNotesWrapper = new List<CaseToNotesWrapper>();
       AccountList = new List<Account>();
       contentDocLinkLst = new list<ContentDocumentLink>();
       caseToNotesLst = new List<Note>();
       lstOfAllCase = new List<Case>();
       mapOFIdsToCase = new Map<Id,Case>();
       caseToNoteLst = new Map<Id,List<ContentDocumentLink>>();
       //accountToCallingListMap = new  Map<account,List<Case>>();
         accountId = ApexPages.currentPage().getParameters().get('id');
         System.debug('accountId:'+accountId);
         AccountList = [SELECT Id,
                     name,
                     (SELECT Id,
                         CaseNumber,
                         RecordType.Name
                        FROM Cases)
                   FROM Account
                   WHERE Id = :accountId];
       system.debug( 'AccountList : ' + AccountList); 
       system.debug( 'AccountList ----: ' + AccountList[0].Cases); 
         for(Account accInst : AccountList){
           if(accInst.Cases != null && !accInst.Cases.isEmpty()){
             lstOfAllCase.addAll(accInst.Cases);
           }
         }
       system.debug( 'lstOfAllCase : ' + lstOfAllCase );         
         if(lstOfAllCase != null && !lstOfAllCase.isEmpty()){
           for(Case callInst : lstOfAllCase){
             mapOFIdsToCase.put(callInst.Id,callInst);
           }
         }
       system.debug( 'mapOFIdsToCase : ' + mapOFIdsToCase );               
           set<Id>callIdLst = new Set<Id>();
           List<Id>notesIdLst = new List<Id>();
         if(mapOFIdsToCase != null && !mapOFIdsToCase.isEmpty()){
        
          callIdLst.addAll(mapOFIdsToCase.keySet());
                   contentDocLinkLst =
                    [SELECT Id ,
                          LinkedEntityId,
                          ContentDocumentId,
                        ContentDocument.title,
                         ContentDocument.Description  
                     FROM ContentDocumentLink 
                     WHERE LinkedEntityId IN : callIdLst];
       System.debug('contentDocLinkLst::::!!'+contentDocLinkLst);
     }
       system.debug( 'callIdLst: ' + callIdLst);        
     for(ContentDocumentLink linkObj : contentDocLinkLst){
       //CaseToNotesWrapper.add();
       notesIdLst.add(linkObj.contentDocumentId);
       lstCaseToNotesWrapper.add(new CaseToNotesWrapper(
                         mapOFIdsToCase.get(linkObj.LinkedEntityId),
                         null,
                         linkObj.contentDocumentId));
     }
      system.debug( 'lstCaseToNotesWrapper: ' + lstCaseToNotesWrapper); 
     Map<Id,ContentNote>IdToNoteMap = new Map<Id,ContentNote>([SELECT Id,
                        Title,
                        TextPreview 
                     FROM ContentNote 
                      WHERE Id IN :notesIdLst]);
     /*List<ContentNote>notesLst = [SELECT Id,
                        Title,
                        TextPreview 
                     FROM ContentNote 
                      WHERE Id IN :notesIdLst];*/
     
                      
    System.debug('IdToNoteMap ::::############'+IdToNoteMap );
        for(Integer index = 0; index < lstCaseToNotesWrapper.size(); index ++) {
           if(IdToNoteMap.containsKey(lstCaseToNotesWrapper[index].NoteId)){
           lstCaseToNotesWrapper[index].NoteLst = IdToNoteMap.get(lstCaseToNotesWrapper[index].NoteId);
           }
      }
      System.debug('lstCaseToNotesWrapper^^^^^^^^^'+lstCaseToNotesWrapper);
         
       
    }
    
    public class CaseToNotesWrapper{
      public Case callObj{get;set;}
      public ContentNote NoteLst{get;set;}
      public Id NoteId{get;set;}
      //List<ContentNote>NoteLst = new List<ContentNote>();
      public CaseToNotesWrapper(Case callObjW,ContentNote NoteLstW,Id NoteIdW){
        callObj = callObjW;
        NoteLst = NoteLstW;
          NoteId = NoteIdW;
      }
    }

}