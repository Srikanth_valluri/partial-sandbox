global class UpdateHoEhoFlagInIPMS implements Schedulable{
    
    global List<Case> lstCase;
    global List<String> lstRegIds;
    
    
    //@InvocableMethod
    global static void UpdateFlag(List<Case> lstCase, List<String> lstRegIds) {
        system.debug('lstCase < : '+lstCase);
        system.debug('lstRegIds < : '+lstRegIds);
        
        UpdateHoEhoFlagInIPMS m = new UpdateHoEhoFlagInIPMS();
        m.lstCase = lstCase;
        m.lstRegIds = lstRegIds;
        DateTime dt = system.now().addMinutes(1);
        String day = string.valueOf(dt.day());
        String month = string.valueOf(dt.month());
        String hour = string.valueOf(dt.hour());
        String minute = string.valueOf(dt.minute());
        String second = string.valueOf(dt.second());
        String year = string.valueOf(dt.year());
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
    
        String jobID = system.schedule('HO OR EHO Flag Updation Job '+lstCase[0].Id + system.now(), strSchedule, m);
    }

    global void execute(SchedulableContext ctx) {
        UpdFlag(lstCase,lstRegIds);
    }
    
    //@future
    global static void UpdFlag(List<Case> lstCase, List<String> lstRegIds) {
        if( lstCase.size() > 0 &&  lstRegIds.size() > 0 ) {
            if( lstCase[0].Recordtype.DeveloperName == 'Handover' ) {
                CallEarlyHandoverMQService1.CallEarlyHandoverMQService1Name('Y','N',lstRegIds);
            }else if( lstCase[0].Recordtype.DeveloperName == 'Early_Handover'){
                CallEarlyHandoverMQService1.CallEarlyHandoverMQService1Name('N','Y',lstRegIds);
            }
        }
    } 
}