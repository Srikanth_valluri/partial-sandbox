@isTest
public class TargetTriggerHandlerTest {
    private static final Id DEAL_SR_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('NSIBPM__Service_Request__c', 'Deal');
    
    static testMethod void testMethod1(){
        List<User> createdUsers = TestDataFactory.createTestUserRecords(new List<User>{new User(UserRoleId='00E9E000000MTyT', ManagerId = UserInfo.getUserId(), IPMS_Employee_ID__c = '1000'),
            new User(UserRoleId='00E9E000000MTyO', ManagerId = UserInfo.getUserId(), IPMS_Employee_ID__c = '1001'), 
            new User(UserRoleId='00E9E000000MTxz', ManagerId = UserInfo.getUserId(), IPMS_Employee_ID__c = '1002'), 
            new User(UserRoleId='00E9E000000MTxk', ManagerId = UserInfo.getUserId(), IPMS_Employee_ID__c = '1003'), 
            new User(UserRoleId='00E9E000000MTxf', ManagerId = UserInfo.getUserId(), IPMS_Employee_ID__c = '1004')});
        
        createdUsers[0].ManagerId = createdUsers[2].Id;
        createdUsers[1].ManagerId = createdUsers[2].Id;
        createdUsers[2].ManagerId = createdUsers[3].Id;
        createdUsers[3].ManagerId = createdUsers[4].Id;
        createdUsers[4].ManagerId = UserInfo.getUserId();
        
        update createdUsers;
        
        Test.startTest();
        {
            system.runAs(new User(Id = userInfo.getUserId())){
                
                List<NSIBPM__SR_Status__c> createdSRStatus = TestDataFactory.createSrStatusRecords(new List<NSIBPM__SR_Status__c>{new NSIBPM__SR_Status__c()});
                system.debug(createdSRStatus);
                List<NSIBPM__SR_Template__c> createdSRTemplates = TestDataFactory.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{
                    new NSIBPM__SR_Template__c(Name='Deal',	NSIBPM__SR_RecordType_API_Name__c = 'Deal')});
                
                List<NSIBPM__Service_Request__c> createdSRs = TestDataFactory.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                    new NSIBPM__Service_Request__c(OwnerId = createdUsers[0].Id, recordTypeId = DEAL_SR_RECORD_TYPE_ID, NSIBPM__Internal_SR_Status__c = createdSRStatus[0].Id, 
                                                   NSIBPM__SR_Template__c = createdSRTemplates[0].Id, Registration_Date__c = Date.newInstance(2017,5,25)),
                        new NSIBPM__Service_Request__c(OwnerId = createdUsers[0].Id, recordTypeId = DEAL_SR_RECORD_TYPE_ID, NSIBPM__Internal_SR_Status__c = createdSRStatus[0].Id, 
                                                       NSIBPM__SR_Template__c = createdSRTemplates[0].Id, Registration_Date__c = Date.newInstance(2017,6,25)),
                        new NSIBPM__Service_Request__c(OwnerId = createdUsers[0].Id, recordTypeId = DEAL_SR_RECORD_TYPE_ID, NSIBPM__Internal_SR_Status__c = createdSRStatus[0].Id, 
                                                       NSIBPM__SR_Template__c = createdSRTemplates[0].Id, Registration_Date__c = Date.newInstance(2017,7,25)),
                        new NSIBPM__Service_Request__c(OwnerId = createdUsers[1].Id, recordTypeId = DEAL_SR_RECORD_TYPE_ID, NSIBPM__Internal_SR_Status__c = createdSRStatus[0].Id, 
                                                       NSIBPM__SR_Template__c = createdSRTemplates[0].Id, Registration_Date__c = Date.newInstance(2017,5,25)),
                        new NSIBPM__Service_Request__c(OwnerId = createdUsers[1].Id, recordTypeId = DEAL_SR_RECORD_TYPE_ID, NSIBPM__Internal_SR_Status__c = createdSRStatus[0].Id, 
                                                       NSIBPM__SR_Template__c = createdSRTemplates[0].Id, Registration_Date__c = Date.newInstance(2017,6,25)),
                        new NSIBPM__Service_Request__c(OwnerId = createdUsers[1].Id, recordTypeId = DEAL_SR_RECORD_TYPE_ID, NSIBPM__Internal_SR_Status__c = createdSRStatus[0].Id, 
                                                       NSIBPM__SR_Template__c = createdSRTemplates[0].Id, Registration_Date__c = Date.newInstance(2017,7,25))});
                Set<id> SRIds = new Set<id>{createdSRs[0].id, createdSRs[1].id, createdSRs[2].id, createdSRs[3].id, createdSRs[4].id, createdSRs[5].id };
                
                for(NSIBPM__Service_Request__c thisSR : [SELECT Id, Name, NSIBPM__Internal_Status_Name__c, NSIBPM__Internal_SR_Status__r.Name FROM NSIBPM__Service_Request__c WHERE Id IN :SRIds]){
                    system.debug(thisSR);
                }
                List<Deal_Team__c> createTestDealTeam = TestDataFactory.createTestDealTeam(new List<Deal_Team__c>{
                    new Deal_Team__c(Associated_PC__c=createdUsers[0].Id, Associated_DOS__c = createdUsers[2].Id, Associated_Deal__c = createdSRs[0].Id),
                        new Deal_Team__c(Associated_PC__c=createdUsers[1].Id, Associated_DOS__c = createdUsers[2].Id, Associated_Deal__c = createdSRs[1].Id),
                        new Deal_Team__c(Associated_PC__c=createdUsers[0].Id, Associated_DOS__c = createdUsers[2].Id, Associated_HOS__c = createdUsers[3].Id, Associated_Deal__c = createdSRs[2].Id),
                        new Deal_Team__c(Associated_PC__c=createdUsers[1].Id, Associated_DOS__c = createdUsers[2].Id, Associated_HOS__c = createdUsers[3].Id, Associated_HOD__c = createdUsers[3].Id, Associated_Deal__c = createdSRs[3].Id),
                        new Deal_Team__c(Associated_PC__c=createdUsers[0].Id, Associated_DOS__c = createdUsers[2].Id, Associated_HOS__c = createdUsers[3].Id, Associated_HOD__c = createdUsers[4].Id, Associated_Deal__c = createdSRs[4].Id),
                        new Deal_Team__c(Associated_PC__c=createdUsers[0].Id, Associated_DOS__c = createdUsers[2].Id, Associated_Deal__c = createdSRs[5].Id)                    
                });
                
                List<Target__c> createdTargets = TestDataFactory.createTestTargetRecords(new List<Target__c>{
                    new Target__c(User__c=createdUsers[0].Id, Month__c='May'), 
                        new Target__c(User__c = createdUsers[0].Id, Month__c='June'), 
                        new Target__c(User__c = createdUsers[0].Id, Month__c = 'July'), 
                        new Target__c(User__c = createdUsers[1].Id, Month__c='June'),
                        new Target__c(User__c = createdUsers[1].Id, Month__c='May'), 
                        new Target__c(User__c = createdUsers[1].Id, Month__c = 'July'), 
                        new Target__c(User__c = createdUsers[2].Id, Month__c='June'),
                        new Target__c(User__c = createdUsers[2].Id, Month__c='May'), 
                        new Target__c(User__c = createdUsers[2].Id, Month__c = 'July'),
                        new Target__c(User__c = createdUsers[3].Id, Month__c='June'),
                        new Target__c(User__c = createdUsers[3].Id, Month__c='May'), 
                        new Target__c(User__c = createdUsers[3].Id, Month__c = 'July')
                        });
                createdTargets[1].User__c = createdUsers[4].id;
                update createdTargets;
                
            }
        }
        Test.stopTest();
 
    }
}