@isTest
public class AP_UnitSearchControllerTest {
    @testSetup static void setupData() {
        Campaign__c camp = new Campaign__c();
        camp.Campaign_Name__c='Test Campaign';
        camp.start_date__c = System.today();
        camp.end_date__c = System.Today().addDays(30);
        camp.Marketing_start_date__c = System.today();
        camp.Marketing_end_date__c = System.Today().addDays(30);
        camp.Language__c = 'English';
        camp.Marketing_Active__c = true;
        camp.Credit_Control_Active__c  = true;
        camp.Sales_Admin_Active__c  = true;
        insert camp;

        Inquiry__c newInquiry = new Inquiry__c();
        newInquiry.Campaign__c = camp.Id;
        newInquiry.First_Name__c = 'Testsearch';
        newInquiry.Last_Name__c = 'Inquiry';
        newInquiry.Mobile_Phone_Encrypt__c = '05789088';
        newInquiry.Mobile_Phone__c = '05789088';
        newInquiry.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        newInquiry.Email__c = 'test.lastname@eternussolutions.com';
        newInquiry.Preferred_Language__c = 'English';
        newInquiry.Inquiry_Source__c = 'Social' ;
        newInquiry.Inquiry_Status__c = 'New' ;
        insert newInquiry;

        EOI_Process__c eOI = new EOI_Process__c();
        eOI.Inquiry__c = newInquiry.id; 
        eOI.Mode_of_Token_Payment__c = 'Cash';     
        eOI.No_of_Units__c = '1 Unit';
        insert eOI;

        Currency_Rate__c rate = new Currency_Rate__c();
        rate.To_Currency__c = 'AED';
        rate.Conversion_Date__c = system.today();
        rate.conversion_rate__c = 1;
        rate.From_Currency__c = 'AED';        
        insert rate;        

        List<Inventory__c> invList=TestDataFactory.CreateInventoryRecords(new List<Inventory__c>{new Inventory__c()});
        invList[0].Unit__c = 'DH/14/1407';
        invList[0].Bedroom_Type__c='1 BR';
        invList[0].View_Type__c = 'View';
        invList[0].District__c = 'District';
        invList[0].Currency_of_Sale__c = 'AED';
        invList[0].Special_Price__c = 2000;
        //invList[0].Price_Per_Sqft__c = 150;
        invList[0].Special_Price_Tax_Amount__c = 10;
        invList[0].status__c='Released';
        invList[0].EOI__c = eOI.Id;
        invList[0].Floor_Package_Type__c='Floor';
        update invList;

    }
    @isTest static void Search1() {
        EOI_Process__c eOI = [SELECT Id,Mode_of_Token_Payment__c FROM EOI_Process__c WHERE Mode_of_Token_Payment__c = 'Cash'];
        Marketing_Documents__c project = [SELECT Id,Marketing_Name__c FROM Marketing_Documents__c WHERE Name = 'test'];
        List<Inventory__c> invList = [SELECT Id,Building_id__c, Unit__c FROM Inventory__c WHERE Unit__c = 'DH/14/1407'];
        //Campaign__c camp = [SELECT Id, Campaign_Name__c FROM Campaign__c ];
        Test.setCurrentPageReference(new PageReference('Page.AP_UnitSearch'));
        System.currentPageReference().getParameters().put('EOIid', eOI.Id);
        System.currentPageReference().getParameters().put('project',project.id );
        System.currentPageReference().getParameters().put('unitName',invList[0].Unit__c );
        System.currentPageReference().getParameters().put('bedroomType','1 BR' );
        System.currentPageReference().getParameters().put('Currency','AED' );
        System.currentPageReference().getParameters().put('lessORMore','more' );
        System.currentPageReference().getParameters().put('cost','2000' );
        System.currentPageReference().getParameters().put('city','ABU DHABI,DUBAI' );
        System.currentPageReference().getParameters().put('selectedEOI',eOI.Id);
        System.currentPageReference().getParameters().put('selectedInventories',invList[0].Id);
        System.currentPageReference().getParameters().put('currencyset','AED');
        Test.startTest();
        AP_UnitSearchController obj = new AP_UnitSearchController();
        AP_UnitSearchController.getAccounts('Test');
        AP_UnitSearchController.getInquiries('Testsearch');
        AP_UnitSearchController.searchUnits ('Test');
        //AP_UnitSearchController.sendemailmodalact(Id offerid,invList[0].Id,Id shareprojectdetailid);
        obj.gotoComparePage();
        obj.getMyAllocations();
        obj.getPkgNames();
        obj.getProjctFtrs();
        obj.getViewTypes();
        obj.getBedRooms();
        obj.getAcd();
        obj.getPropertyNames();
        obj.getUnits();
        obj.getUnitTypes();
        obj.getLocation();
        obj.getCities();
        obj.getPropStatus();
        obj.getItems();
        obj.populatefromtocurrencymap();
        obj.prepareData();
        obj.convertToSelectedCurency();
        obj.populateCampaignsection(invList);
        obj.getToCurrencyValues();
        obj.isS1();
        obj.promotionsAndOffers();
        obj.changecurrency();
        obj.filterData();
        obj.pageload();
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'product';
        obj.filter = 'product';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'projectCluster';
        obj.filter = 'projectCluster';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'city';
        obj.filter = 'city';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'viewType';
        obj.filter = 'viewType';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'bedRoom';
        obj.filter = 'bedRoom';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'unitType';
        obj.filter = 'unitType';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'status';
        obj.filter = 'status';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'location';
        obj.filter = 'location';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'minprice';
        obj.filter = 'minprice';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'maxprice';
        obj.filter = 'maxprice';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'minsft';
        obj.filter = 'minsft';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'maxsft';
        obj.filter = 'maxsft';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'minPriceSft';
        obj.filter = 'minPriceSft';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'maxPriceSft';
        obj.filter = 'maxPriceSft';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'myallocationIdsSelected';
        obj.filter = 'myallocationIdsSelected';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'units';
        obj.filter = 'units';
        obj.clearFilterLists();
        obj.clearLists();

        obj.clearFilter = 'acd';
        obj.filter = 'acd';
        obj.clearFilterLists();
        obj.clearLists();
        Test.stopTest();
    }

    @isTest static void Search2() {
        EOI_Process__c eOI = [SELECT Id,Mode_of_Token_Payment__c FROM EOI_Process__c WHERE Mode_of_Token_Payment__c = 'Cash'];
        Marketing_Documents__c project = [SELECT Id,Marketing_Name__c FROM Marketing_Documents__c WHERE Name = 'test'];
        List<Inventory__c> invList = [SELECT Id,
                                             Unit__c,
                                             Price_Per_Sqft__c,
                                             Currency_of_Sale__c,
                                             Special_Price__c,
                                             Special_Price_Tax_Amount__c,
                                             Floor_Package_Name__c,
                                             Floor_Package_Type__c
                                        FROM Inventory__c 
                                        WHERE Unit__c = 'DH/14/1407'];
        //Campaign__c camp = [SELECT Id, Campaign_Name__c FROM Campaign__c ];
        Test.setCurrentPageReference(new PageReference('Page.AP_UnitSearch'));
        System.currentPageReference().getParameters().put('EOIid', eOI.Id);
        System.currentPageReference().getParameters().put('project',project.id );
        System.currentPageReference().getParameters().put('unitName',invList[0].Unit__c );
        System.currentPageReference().getParameters().put('bedroomType','1 BR' );
        System.currentPageReference().getParameters().put('Currency','AED' );
        System.currentPageReference().getParameters().put('lessORMore','less' );
        System.currentPageReference().getParameters().put('cost','2000' );
        System.currentPageReference().getParameters().put('city','ABU DHABI,DUBAI' );
        System.currentPageReference().getParameters().put('selectedEOI',eOI.Id);
        System.currentPageReference().getParameters().put('selectedInventories',invList[0].Id);
        System.currentPageReference().getParameters().put('currencyset','AED');
        System.currentPageReference().getParameters().put('limitVal','10');
        System.currentPageReference().getParameters().put('sortOrder','ASC'); //sortOrderVal
        System.currentPageReference().getParameters().put('sortCol','Product'); //sortColVal
        System.currentPageReference().getParameters().put('searchText','Test');
        Test.startTest();
        AP_UnitSearchController obj = new AP_UnitSearchController();
        AP_UnitSearchController.checkSelectedUnits(invList[0].Id,eOI.Id);
        //AP_UnitSearchController.sendemailmodalact(Id offerid,invList[0].Id,Id shareprojectdetailid);
        obj.populatefromtocurrencymap();
        obj.changeRecCountFunMethod();
        obj.changeSortOrder();
        //obj.searchRecs();
        obj.getprv();
        obj.nextbtn();
        obj.prvbtn();  
        obj.getnxt();   
        obj.sortColVal = 'Project';
        obj.dataPagination();

        obj.sortColVal = 'Units';
        obj.dataPagination();

        obj.sortColVal = 'Bedroom_Type';
        obj.dataPagination();

        obj.sortColVal = 'Bedrooms';
        obj.dataPagination();

        obj.sortColVal = 'Area';
        obj.dataPagination();

        obj.sortColVal = 'Price';
        obj.dataPagination();

        obj.myallocationIdsSelected = 'Nothing Selected';
        obj.dataPagination();

        obj.filteredinventory.add(invList[0]);
        obj.convertToSelectedCurency();
        Test.stopTest();
    }

    @isTest static void Search3() {
        Currency_Rate__c rate1 = new Currency_Rate__c();
        rate1.To_Currency__c = 'AED';
        rate1.Conversion_Date__c = system.today();
        rate1.conversion_rate__c = 1;
        rate1.From_Currency__c = 'AED';        
        insert rate1;

        EOI_Process__c eOI = [SELECT Id,Mode_of_Token_Payment__c FROM EOI_Process__c WHERE Mode_of_Token_Payment__c = 'Cash'];
        Marketing_Documents__c project = [SELECT Id,Marketing_Name__c FROM Marketing_Documents__c WHERE Name = 'test'];
        List<Inventory__c> invList = [SELECT Id,
                                             Unit__c,
                                             Price_Per_Sqft__c,
                                             Currency_of_Sale__c,
                                             Special_Price__c,
                                             Special_Price_Tax_Amount__c,
                                             Floor_Package_Name__c,
                                             Floor_Package_Type__c
                                        FROM Inventory__c 
                                        WHERE Unit__c = 'DH/14/1407'];
        //Campaign__c camp = [SELECT Id, Campaign_Name__c FROM Campaign__c ];
        Test.setCurrentPageReference(new PageReference('Page.AP_UnitSearch'));
        System.currentPageReference().getParameters().put('EOIid', eOI.Id);
        System.currentPageReference().getParameters().put('project',project.id );
        System.currentPageReference().getParameters().put('unitName',invList[0].Unit__c );
        System.currentPageReference().getParameters().put('bedroomType','1 BR' );
        System.currentPageReference().getParameters().put('Currency','AED' );
        System.currentPageReference().getParameters().put('lessORMore','less' );
        System.currentPageReference().getParameters().put('cost','2000' );
        System.currentPageReference().getParameters().put('city','ABU DHABI,DUBAI' );
        System.currentPageReference().getParameters().put('selectedEOI',eOI.Id);
        System.currentPageReference().getParameters().put('selectedInventories',invList[0].Id);
        System.currentPageReference().getParameters().put('currencyset','AED');
        System.currentPageReference().getParameters().put('limitVal','10');
        System.currentPageReference().getParameters().put('sortOrder','ASC'); //sortOrderVal
        System.currentPageReference().getParameters().put('sortCol','Product'); //sortColVal
        System.currentPageReference().getParameters().put('searchText','Test');
        Test.startTest();
        AP_UnitSearchController obj = new AP_UnitSearchController();
        AP_UnitSearchController.checkSelectedUnits(invList[0].Id,eOI.Id);
        invList[0].Currency_of_Sale__c = '';
        invList[0].Special_Price__c = 0;
        //invList[0].Price_Per_Sqft__c = 150;
        invList[0].Special_Price_Tax_Amount__c = 0;
        update invList;

        obj.filteredinventory.add(invList[0]);
        obj.populatefromtocurrencymap();
        obj.convertToSelectedCurency();

        obj.tocurrencyselectedtemp = 'Test';
        obj.filteredinventory.add(invList[0]);
        obj.convertToSelectedCurency();
        Test.stopTest();
    }

    @isTest static void Search4() {
        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        
        Account a = new Account();
        a.LastName= 'Test Account';
        a.RecordTypeId = accountRTId;
        a.Agency_Short_Name__c = 'testShrName';
        a.Party_ID__c = '789456';
        a.Email__c = 'test@gmail.com';
        a.Mobile__c = '7894561230';
        a.Title__c = 'Ms.';
        a.Nationality__c = 'Australian';
        a.Passport_Number__c = '123654';
        insert a;

        Currency_Rate__c rate1 = new Currency_Rate__c();
        rate1.To_Currency__c = 'AED';
        rate1.Conversion_Date__c = system.today();
        rate1.conversion_rate__c = 1;
        rate1.From_Currency__c = 'AED';        
        insert rate1;

        EOI_Process__c eOI = [SELECT Id,Mode_of_Token_Payment__c FROM EOI_Process__c WHERE Mode_of_Token_Payment__c = 'Cash'];
        Marketing_Documents__c project = [SELECT Id,Marketing_Name__c FROM Marketing_Documents__c WHERE Name = 'test'];
        List<Inventory__c> invList = [SELECT Id,
                                             Unit__c,
                                             Price_Per_Sqft__c,
                                             Currency_of_Sale__c,
                                             Special_Price__c,
                                             Special_Price_Tax_Amount__c,
                                             Floor_Package_Name__c,
                                             Floor_Package_Type__c
                                        FROM Inventory__c 
                                        WHERE Unit__c = 'DH/14/1407'];

        Inquiry__c inq = [SELECT Id, Name, First_name__c, Last_Name__c, Email__c 
                          FROM Inquiry__c 
                         WHERE First_Name__c = 'Testsearch'];

        SalesOffer_Record__c salesObj = new SalesOffer_Record__c();
        salesObj.Selected_Inventory_Ids__c = String.valueOf(invList[0].id);
        salesObj.Type__c = 'DownLoad Availaibility';
        //salesObj.Trasaction_User__c = UserInfo.getUserId() ;
        insert salesObj;

        Share_Project_Details__c shareProject = new Share_Project_Details__c();
        shareProject.Account__c = a.id;
        shareProject.Sent_To_Email__c ='test@test.com';
        insert shareProject;

        //Campaign__c camp = [SELECT Id, Campaign_Name__c FROM Campaign__c ];
        Test.setCurrentPageReference(new PageReference('Page.AP_UnitSearch'));
        System.currentPageReference().getParameters().put('EOIid', eOI.Id);
        System.currentPageReference().getParameters().put('project',project.id );
        System.currentPageReference().getParameters().put('unitName',invList[0].Unit__c );
        System.currentPageReference().getParameters().put('bedroomType','1 BR' );
        System.currentPageReference().getParameters().put('Currency','AED' );
        System.currentPageReference().getParameters().put('lessORMore','less' );
        System.currentPageReference().getParameters().put('cost','2000' );
        System.currentPageReference().getParameters().put('city','ABU DHABI,DUBAI' );
        System.currentPageReference().getParameters().put('selectedEOI',eOI.Id);
        System.currentPageReference().getParameters().put('selectedInventories',invList[0].Id);
        System.currentPageReference().getParameters().put('currencyset','AED');
        System.currentPageReference().getParameters().put('limitVal','10');
        System.currentPageReference().getParameters().put('sortOrder','ASC'); //sortOrderVal
        System.currentPageReference().getParameters().put('sortCol','Product'); //sortColVal
        System.currentPageReference().getParameters().put('searchText','Test');

        System.currentPageReference().getParameters().put('salesofferrecordid',salesObj.Id);
        System.currentPageReference().getParameters().put('entityId',inq.Id);
        System.currentPageReference().getParameters().put('shareprojectdetailid',shareProject.Id);


        Test.startTest();
        AP_UnitSearchController obj = new AP_UnitSearchController();
        AP_UnitSearchController.checkSelectedUnits(invList[0].Id,eOI.Id);
        obj.sendemailfun();
        AP_UnitSearchController.sendemailmodalact(salesObj.Id,inq.Id,shareProject.Id);
        Test.stopTest();
    }

    @isTest static void Search5() {
        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        
        Account a = new Account();
        a.LastName= 'Test Account';
        a.RecordTypeId = accountRTId;
        a.Agency_Short_Name__c = 'testShrName';
        a.Party_ID__c = '789456';
        a.Email__c = 'test@gmail.com';
        a.Mobile__c = '7894561230';
        a.Title__c = 'Ms.';
        a.Nationality__c = 'Australian';
        a.Passport_Number__c = '123654';
        insert a;

        Currency_Rate__c rate1 = new Currency_Rate__c();
        rate1.To_Currency__c = 'AED';
        rate1.Conversion_Date__c = system.today();
        rate1.conversion_rate__c = 1;
        rate1.From_Currency__c = 'AED';        
        insert rate1;

        EOI_Process__c eOI = [SELECT Id,Mode_of_Token_Payment__c FROM EOI_Process__c WHERE Mode_of_Token_Payment__c = 'Cash'];
        Marketing_Documents__c project = [SELECT Id,Marketing_Name__c FROM Marketing_Documents__c WHERE Name = 'test'];
        List<Inventory__c> invList = [SELECT Id,
                                             Unit__c,
                                             Price_Per_Sqft__c,
                                             Currency_of_Sale__c,
                                             Special_Price__c,
                                             Special_Price_Tax_Amount__c,
                                             Floor_Package_Name__c,
                                             Floor_Package_Type__c
                                        FROM Inventory__c 
                                        WHERE Unit__c = 'DH/14/1407'];
        invList[0].Floor_Package_Type__c='';
        update invList;

        Inquiry__c inq = [SELECT Id, Name, First_name__c, Last_Name__c, Email__c 
                          FROM Inquiry__c 
                         WHERE First_Name__c = 'Testsearch'];

        SalesOffer_Record__c salesObj = new SalesOffer_Record__c();
        salesObj.Selected_Inventory_Ids__c = String.valueOf(invList[0].id);
        salesObj.Type__c = 'DownLoad Availaibility';
        //salesObj.Trasaction_User__c = UserInfo.getUserId() ;
        insert salesObj;

        Share_Project_Details__c shareProject = new Share_Project_Details__c();
        shareProject.Account__c = a.id;
        shareProject.Sent_To_Email__c ='test@test.com';
        insert shareProject;

        Campaign__c camp = [SELECT Id, Campaign_Name__c FROM Campaign__c ];
        Test.setCurrentPageReference(new PageReference('Page.AP_UnitSearch'));
        System.currentPageReference().getParameters().put('EOIid', eOI.Id);
        System.currentPageReference().getParameters().put('project',project.id );
        System.currentPageReference().getParameters().put('unitName',invList[0].Unit__c );
        System.currentPageReference().getParameters().put('bedroomType','1 BR' );
        System.currentPageReference().getParameters().put('Currency','AED' );
        System.currentPageReference().getParameters().put('lessORMore','less' );
        System.currentPageReference().getParameters().put('cost','2000' );
        System.currentPageReference().getParameters().put('city','ABU DHABI,DUBAI' );
        System.currentPageReference().getParameters().put('selectedEOI',eOI.Id);
        System.currentPageReference().getParameters().put('selectedInventories',invList[0].Id);
        System.currentPageReference().getParameters().put('currencyset','AED');
        System.currentPageReference().getParameters().put('limitVal','10');
        System.currentPageReference().getParameters().put('sortOrder','ASC'); //sortOrderVal
        System.currentPageReference().getParameters().put('sortCol','Product'); //sortColVal
        System.currentPageReference().getParameters().put('searchText','Test');

        System.currentPageReference().getParameters().put('salesofferrecordid',salesObj.Id);
        System.currentPageReference().getParameters().put('entityId',inq.Id);
        System.currentPageReference().getParameters().put('shareprojectdetailid',shareProject.Id);


        Test.startTest();
        AP_UnitSearchController obj = new AP_UnitSearchController();
        AP_UnitSearchController.checkSelectedUnits(invList[0].Id,eOI.Id);
        obj.populatefromtocurrencymap();
        obj.promotionsAndOffers();
        obj.myallocationIdsSelected = camp.Id;
        obj.getRecCount('NopageLoadCall');
        obj.filterData();
        obj.dataPagination();
        obj.strSelectedIds = '';
        obj.invs = '';
        obj.selectedInventoryIdsForBooking = '';
        obj.selectedProductProjectname = '';
        obj.limitVal = 1;
        obj.searchText = 'Test';
        obj.selInventories = '';
        obj.promoOfferCheckInnerMap = new Map<String,Boolean>();
        obj.searchAcc = new Account();
        obj.searchInq = new Inquiry__c();
        obj.uaType = '';
        Test.stopTest();
    }
}