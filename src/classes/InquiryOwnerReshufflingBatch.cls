global class InquiryOwnerReshufflingBatch implements Database.Batchable<sObject>, Database.stateFul {

    Inquiry_Assignment_Rules__c assignmentRule;
    List <ID> completedRules = new List <ID> ();
    global InquiryOwnerReshufflingBatch (List <ID> ruleIds) {
        completedRules = ruleIds;
    }
    public static string getAllfields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ())
            fields += f+ ', ';
        return fields.removeEnd(', ');         
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        try {
            String scheduledAt = NULL;
            
            Datetime now = Datetime.now();
            Integer hours = now.hour();
            if ( hours < 12 && hours > 0) {
                scheduledAt = 'Morning Job';
            }
            if (hours > 12 && hours > 0) {
                scheduledAt = 'Night Job';
            }
            if (Test.isRunningTest ()) {
                scheduledAt = NULL;
            }
            
            String fields = getAllfields('Inquiry_Assignment_Rules__c');
            String executeOn = 'Reassign';
            string queryCondition = 'SELECT '+fields+' FROM Inquiry_Assignment_Rules__c '
                                +' WHERE Active__c = True and Execute_on__c =:executeOn '
                                +' AND ID NOT IN: completedRules AND (HOD_Filter_Logic__c  = True OR Filter_Logic__c != NULL)'
                                +' AND Schedule_As__c =: scheduledAt '
                                +' order by Priority__c Asc LIMIT 1';
            
            assignmentRule = Database.query (queryCondition);
            System.Debug (assignmentRule.ID);
            ID parentID = assignmentRule.ID;
            completedRules.add (assignmentRule.ID);
            List <Inquiry_Assignment_Rules__c> childRules = new List <Inquiry_Assignment_Rules__c> ();
            childRules = Database.query ('SELECT '+fields+' FROM Inquiry_Assignment_Rules__c '
                                +' WHERE Active__c = True AND Parent_Reassign_Rule__c =: parentID Order by Order__c Asc');
                                
            String userLogic = assignmentRule.filter_logic__c;
            if (assignmentRule.Rule_for_HOD_Filter_Logic__c != NULL) {
                if (userLogic == NULL) {
                    userLogic = '';
                }
                userLogic = assignmentRule.Rule_for_HOD_Filter_Logic__c+' '+userLogic;
            }
            String recordTypeName = assignmentRule.Inquiry_Record_Type__c;
            String inquiryStatus = 'Qualified';
            String query = 'SELECT '+getAllfields('Inquiry__c')+' FROM Inquiry__c WHERE RecordType.Name = :recordTypeName AND Inquiry_Status__c !=: inquiryStatus AND Reassigned_Date__c != TODAY AND ';
            
            String output;
            Map <String, Inquiry_Assignment_Rules__c> rulesMap = new Map <String, Inquiry_Assignment_Rules__c> ();
            for (Inquiry_Assignment_Rules__c con: childRules){
                rulesMap.put (String.valueOf(con.order__c), con);
            }
            if (String.isNotBlank (userLogic)) {            
                query += +' ( '+ formatQuery (userLogic, rulesMap)+' ) ';
                system.debug('>>>FINAL QUERY'+query);
                if(assignmentRule.query_Limit__c != null){
                    query += ' LIMIT '+assignmentRule.query_Limit__c;
                }
                
            } 
            if (Test.isRunningTest()) {
                query = ' SELECT OwnerId FROM Inquiry__c';
            }
            System.debug(query+'>>>>>>>>>>>.');
            assignmentRule.Rule_Running_Status__c = 'Started';
            assignmentRule.Rule_Executed_Date__c = System.now ();
            Update assignmentRule;
            return Database.getQueryLocator(query);
        }
        catch (Exception e) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new List <String> {Label.Reshuffling_Batch_Notification_Email});
            
            mail.setSubject('Inquiry Owner Reshuffling Batch');
            String body = 'Hi, There is an exception in Inquiry owner reshuffling Start Method. Pleae find the exception details.<br/>'
                            +e.getMessage()+' at line Number :'+e.getLineNumber();
            mail.setHtmlBody(body);
            Messaging.sendEmail(new List <Messaging.SingleEmailMessage> {mail});
            return null;
        }
        
    }
    public String formatQuery (String queryLogic, Map <String, Inquiry_Assignment_Rules__c> rules) {
        String query = '';
        for (String key : queryLogic.split (' ')) {
            if (key.isNumeric ()) {
                System.Debug (key );
                if (rules.containsKey (key))
                    query += +' '+rules.get (key).Query_Condition__c+' ';
            } else
                query += key;
        }
        System.debug (query);
        return query;
    }
    global void execute (Database.BatchableContext BC, List <Inquiry__c> scope) {
        try {
            assignmentRule.Rule_Running_Status__c = 'Processing';
            Update assignmentRule;
            Boolean queueAssignment = assignmentRule.Queue_Assignment__c;
            Boolean skipPowerLine = assignmentRule.Skip_Powerline__c;
            Boolean assignToCreatedBy = assignmentRule.Assign_to_Created_by__c;
            string inquiryStatus = assignmentRule.inquiry_Status__c;
            boolean resetExcludeReassignFlag = assignmentRule.Reset_Exclude_from_Reassigment__c;
            string recordTypeToUpdate = assignmentRule.Update_Record_Type_To__c; 
            
            ID queueId = assignmentRule.OwnerID;
            Map <Inquiry__c, ID> inquiryOwnerMap = new Map <Inquiry__c, ID> ();
            Map <inquiry__c, list<User>> eligibleUsers = new Map <inquiry__c, list<User>>  ();
            List <Inquiry__c> inqToUpdate = new List <Inquiry__c> ();
            List <Inquiry__c> inqlist = new List <Inquiry__c> ();
            
            String createInquiry = assignmentRule.Create_Action__c;
            String allocationModel = assignmentRule.Allocation_model__c; 
            List <Inquiry__c> inquiries = new List <Inquiry__c> ();
            System.debug (scope);
            if (createInquiry == NULL) 
                inquiries = scope;
                
            System.Debug (assignmentRule.Update_Record_Type_To__c);
            if (Test.isRunningTest()) 
                createInquiry = 'Inquiry';
                
            if (createInquiry == 'Inquiry' ) {
                List <Inquiry__c> inqToInsert = new List <Inquiry__c> ();
                List <Inquiry__c> inqStatusToUpdate = new List <Inquiry__c> ();
                Id inqRecordTypeId = assignmentRule.Update_Record_Type_To__c;
                List <InquiryFieldMapping__mdt> mappings = [SELECT Field__c, Value__c, FieldType__c FROM InquiryFieldMapping__mdt];
                for (Inquiry__c inq : scope) {
                    inq.Inquiry_Status__c = assignmentRule.Inquiry_Status__c;
                    inq.Inquiry_Assignment_Rules__c = assignmentRule.ID;
                    inq.Is_Time_based_conversion__c = true;
                    
                    if (allocationModel == NULL) {
                        Inquiry__c newInq = mapInquiryFields(inq, mappings);
                        newInq.Inquiry_Status__c = 'New';
                        newInq.Original_PreInq_Status__c = inq.Inquiry_Status__c;
                        if (!Test.isRunningTest())
                            newInq.RecordTypeId = inqRecordTypeId;
                        inqToInsert.add (newInq);
                    }
                    else {
                        inq.Allocation_model__c = allocationModel;
                        inq.Rule_for_HOD__c = assignmentRule.Rule_For_HOD__C;
                    }
                    inqStatusToUpdate.add (inq);
                }
                if (inqToInsert.size () > 0) {
                    
                    insert inqToInsert;
                    Update inqStatusToUpdate ;
                    inquiries = inqToInsert;
                }
                if (inqStatusToUpdate.size () > 0 && inqToInsert.size () == 0) {
                    
                    update inqStatusToUpdate;
                    inquiries = new List <Inquiry__c> ();
                }
            }
            
            System.debug (inquiries);
            for (Inquiry__c inq : inquiries) {
                inq.Previous_Owner__c = inq.ownerID;
                inq.Not_Eligible_for_Reassignment__c = resetExcludeReassignFlag;
                if(inquiryStatus != null && createInquiry == NULL){
                    inq.inquiry_status__c = inquiryStatus;
                }
                
                if(recordTypeToUpdate != null){ // Added on Oct 10 - subhash
                    inq.recordtypeid = recordTypeToUpdate;
                }
                
                if (assignToCreatedBy == FALSE) {
                    inq.ownerId = assignmentRule.On_Create_Queue_Id__c;
                    if (inq.OwnerID == NULL) 
                        inq.OwnerId = assignmentRule.OwnerID;   
                    inquiryOwnerMap.put(inq, inq.OwnerId);
                    if (queueAssignment == FALSE) {
                        inq.Assignment_Queue_ID__c = 'temp_'+assignmentRule.OwnerID;
                        inqlist.add (inq);
                    }
                    if (queueAssignment == TRUE) {
                        inq.Assignment_Queue_ID__c = 'temp_'+assignmentRule.OwnerID;
                        inq.OwnerId = assignmentRule.OwnerID;   
                        inquiryOwnerMap.put(inq, inq.OwnerId);
                    }
                } 
            }    
            if (inqlist.size () > 0) {
                if (skipPowerLine == FALSE) {
                    PreInquiryGetEligiblePCS obj = new PreInquiryGetEligiblePCS ();
                    eligibleUsers = obj.getEligiblePCSfromQueue (inqlist, 'Reassign', new List <Inquiry_Assignment_Rules__c> {assignmentRule});
                    System.Debug (eligibleUsers);
                    inquiryOwnerMap = InquiryReassignment.assignOwner(eligibleUsers);
                } else {
                    if (queueId != NULL) {
                        List <ID> groupMemberIdsList = new List <ID> ();
                        List <User> groupMemberList = new List <User> ();
                        for (groupMember g : [select userorgroupid, groupid from groupmember where groupid=:queueId]) {
                            if (String.valueOf (g.userorgroupid).startsWith ('005'))
                                groupMemberIdsList.add (g.userorgroupid);
                        }
                        groupMemberList = [SELECT Is_Blacklisted__c, isUserOnLeave__c FROM User 
                                            WHERE isUserOnLeave__c != TRUE 
                                            AND Is_Blacklisted__c != TRUE 
                                            AND ID IN: groupMemberIdsList];
                        for (Inquiry__c inq : Scope) {
                            inq.Assignment_Queue_ID__c = 'temp_'+queueId ;
                            eligibleUsers.put (inq, groupMemberList);
                        }
                        inquiryOwnerMap = InquiryReassignment.assignOwner(eligibleUsers);
                    }
                }
            }
            for (Inquiry__c inq : inquiryOwnerMap.keySet ()) {
                inq.ownerID = inquiryOwnerMap.get (inq);
                inq.Inquiry_Assignment_Rules__c = assignmentRule.ID;
                if (createInquiry == NULL)
                    inq.Reassigned_Date__c = Date.Today ();
                if (inq.OwnerID != NULL)
                    inqToUpdate.add (inq);
            }
            
            System.Debug (inqToUpdate);
            DAMAC_Constants.skip_InquiryTrigger = TRUE;
            if (inqToUpdate.size () > 0)
                Update inqToUpdate;
        }
        catch (Exception e) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new List <String> {Label.Reshuffling_Batch_Notification_Email});
            
            mail.setSubject('Inquiry Owner Reshuffling Batch');
            String body = 'Hi, There is an exception in Inquiry owner reshuffling. Pleae find the exception details.<br/>'
                            +e.getMessage()+' at line Number :'+e.getLineNumber();
            mail.setHtmlBody(body);
            Messaging.sendEmail(new List <Messaging.SingleEmailMessage> {mail});
        }
        
    }
    
    global void finish (Database.BatchableContext BC) {
        System.Debug (completedRules);
        assignmentRule.Rule_Running_Status__c = 'Completed';
        Update assignmentRule;

        if ([SELECT ID FROM Inquiry_Assignment_Rules__c 
                WHERE Active__c = TRUE 
                AND Execute_on__c = 'Reassign' 
             AND filter_logic__c != NULL AND ID Not IN : completedRules].size () > 0){
                 if(!test.isRunningTest())
                    Database.executeBatch (new InquiryOwnerReshufflingBatch (completedRules), 1);
         }
        
    }
    
    public Inquiry__c mapInquiryFields(Inquiry__c inq, List <InquiryFieldMapping__mdt> mappings) {
        System.debug(':::::inq:::::'+inq);
        Inquiry__c newInq = new Inquiry__c();
        
        for(InquiryFieldMapping__mdt fieldMapping : mappings) {
            Object value = NULL;
            if (fieldMapping.Value__c != NULL) {
                if(fieldMapping.FieldType__c == 'Text' || fieldMapping.FieldType__c == '') {
                    value = fieldMapping.Value__c;
                }
                if(fieldMapping.FieldType__c == 'DateTime') {
                    value = DateTime.ValueOf(fieldMapping.Value__c);
                }
                if(fieldMapping.FieldType__c == 'Currency' || 
                    fieldMapping.FieldType__c == 'Number' || 
                    fieldMapping.FieldType__c == 'Percent') {
                    value = Decimal.ValueOf(fieldMapping.Value__c);
                }
                if(fieldMapping.FieldType__c == 'Boolean') {
                    if(fieldMapping.Value__c.toLowerCase() == 'true') {
                        value = TRUE;
                    }else{
                        value = false;
                    }
                }
                newInq.put (fieldMapping.Field__c, value);
            }
            else
                newInq.put (fieldMapping.Field__c, inq.get(fieldMapping.Field__c));
        }
        return newInq;
    }
}