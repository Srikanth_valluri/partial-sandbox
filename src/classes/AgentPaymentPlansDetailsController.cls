// CreatedBy :  Naresh
public class AgentPaymentPlansDetailsController{


public String Id ;
public String SrNumber{get;set;}
List<Payment_Terms__c> PaymentTerm ;

public AgentPaymentPlansDetailsController(){

Id =  ApexPages.currentPage().getParameters().get('Id');
PaymentTerm =  new List<Payment_Terms__c>();
System.debug('Payment Plan ---- Id  '+Id);

}

public List<Payment_Terms__c> getPaymentTerm(){ 
    System.debug('Payment Plan Method====---- Id  '+Id);
    try{
    PaymentTerm = [select id ,
                          Name,
                          Installment__c,
                          Description__c,
                          Milestone_Event__c,
                          Percent_Value__c,
                          Payment_Plan__r.Booking_Unit__r.Booking__r.Deal_SR__r.Name
                          from Payment_Terms__c where Payment_Plan__c =: Id ORDER BY CreatedDate DESC ];
    SrNumber = PaymentTerm[0].Payment_Plan__r.Booking_Unit__r.Booking__r.Deal_SR__r.Name  ;  
    }
    catch(Exception Ex){
    }                
                          return PaymentTerm ;
    
    
}

  public pagereference Back(){
            Pagereference pg =  new Pagereference('/apex/AgentServiceRequestDetails?SRId='+SrNumber); 
            pg.setRedirect(true);
            return pg;
           }



}