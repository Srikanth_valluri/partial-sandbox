/*---------------------------------------------------------------------------------------------------------------------
Description: Task Trigger Handler to validate task closure
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By      | Comments
-----------------------------------------------------------------------------------------------------------------------
1.1     | 05-03-2020        | Aishwarya Todkar      | Validated LHO-Hotel task closure

1.2     | 18-03-2020        | Aishwarya Todkar      | Validated Rebate Task closure

1.3     | 24-09-2020        | Ruchika Choudhary     | Validated Final Bank Approval Date

1.4     | 15-10-2020        | Aishwarya Todkar      | Validated Resolve complaint task

1.5     | 08-11-2020        | Aishwarya Todkar      | Validated happiness fields in Resolve complaint task

1.6     | 10-12-2020        | Aishwarya Todkar      | Validated Valuations initiation task closure
=======================================================================================================================*/
public with sharing class TaskTriggerHandlerSuggestion {
    
    public static void checkValidInfoOnCase(map<Id,Task> newMap, map<Id,Task> oldMap) {
        map<Id, Id> mapFMCaseTaskId = new map<Id,Id>();
        map<Id, Task> mapIdTask = new map<Id, Task>();
        Map<Id, Task> mapCaseIdToTask = new Map<Id, Task>();
        Map<Id, Task> mapCaseIdToRebateTask = new Map<Id, Task>();
        Map<Id, Task> mapCaseIdToBankApprovalTask = new Map<Id, Task>();
        Map<Id, Task> mapCaseToResolveComplaintTask = new Map<Id, Task>();
        Map<Id, Task> mapCaseToValuationTask = new Map<Id, Task>();

        for (Task objTask : newMap.values()) {
            if (String.isNotBlank(objTask.Subject) 
                && objTask.Subject.equalsIgnoreCase(Label.FM_Suggestion_Task_Subject)
                && objTask.Assigned_User__c == 'Property Manager'
                && objTask.Process_Name__c == 'Suggestion'
                && String.isNotBlank(objTask.Status)
                && objTask.Status.equalsIgnoreCase('Completed')
                && !oldMap.get(objTask.Id).Status.equalsIgnoreCase('Completed')
                && String.valueOf(objTask.WhatId).startsWith(Label.Fm_Case_Prefix)
            ){
                system.debug('ala aat ========');
                mapFMCaseTaskId.put(objTask.WhatId, objTask.Id);
                mapIdTask.put(objTask.Id, objTask);
            }
            /***********************************************************************************************************************
            Validating Task LHO-Hotel closure - Added by Aishwarya Todkar on 5/3/2020 
            ************************************************************************************************************************/
            else if( objTask.WhatId != null
            && String.valueOf( objTask.WhatId ).startsWith( '500' )
            && String.isNotBlank( objTask.Subject )
            && objTask.Subject.equalsIgnoreCase( 'Upload, Verify and Execute UMA and HO documents')
            && objTask.Process_Name__c == 'Lease Handover'
            && String.isNotBlank( objTask.Status )
            && ( objTask.Status.equalsIgnoreCase( 'Completed' ) || objTask.Status.equalsIgnoreCase( 'Closed' ) ) 
            && !oldMap.get(objTask.Id).Status.equalsIgnoreCase('Completed') 
            && !oldMap.get(objTask.Id).Status.equalsIgnoreCase('Closed') ) {
                
                mapCaseIdToTask.put(objTask.WhatId, objTask);
            }
            /**************************************************************************************************************************
            Validating Rebate Task closure - Added by Aishwarya Todkar on 18/3/2020 
            ***************************************************************************************************************************/
            else if( objTask.WhatId != null
            && String.valueOf( objTask.WhatId ).startsWith( '500' )
            && String.isNotBlank( objTask.Subject )
            && ( objTask.Subject.equalsIgnoreCase( 'Send Email') || objTask.Subject.equalsIgnoreCase( 'Upload Signed Letter' ) )
            && objTask.Process_Name__c == 'Rebate On Advance'
            && String.isNotBlank( objTask.Status )
            && ( objTask.Status.equalsIgnoreCase( 'Completed' ) || objTask.Status.equalsIgnoreCase( 'Closed' ) ) 
            && !oldMap.get(objTask.Id).Status.equalsIgnoreCase('Completed') 
            && !oldMap.get(objTask.Id).Status.equalsIgnoreCase('Closed') ) {
                
                mapCaseIdToRebateTask.put(objTask.WhatId, objTask);
            }
            /**************************************************************************************************************************
            Validating Final Bank Approval Date  - Added by Ruchika Choudhary on 24/9/2020 
            ***************************************************************************************************************************/
            else if( objTask.WhatId != null
            && String.valueOf( objTask.WhatId ).startsWith( '500' )
            && String.isNotBlank(objTask.Subject)
            && ( objTask.Subject.equalsIgnoreCase( Label.Mortgage_10 )
            && String.isNotBlank( objTask.Status )
            && ( objTask.Status.equalsIgnoreCase( 'Completed' ) || objTask.Status.equalsIgnoreCase( 'Closed' ) ) 
            && !oldMap.get(objTask.Id).Status.equalsIgnoreCase('Completed') 
            && !oldMap.get(objTask.Id).Status.equalsIgnoreCase('Closed') ) ) {
                
                mapCaseIdToBankApprovalTask.put(objTask.WhatId, objTask);
            }
            
            /**************************************************************************************************************************
            Validating Resolve Complaint Task  - Added by Aishwarya Todkar on 15/10/2020 
            ***************************************************************************************************************************/
            
            else if( objTask.WhatId != null
            && String.valueOf( objTask.WhatId ).startsWith( '500' )
            && String.isNotBlank(objTask.Subject)
            && ( objTask.Subject.equalsIgnoreCase('Resolve Complaint')
            && String.isNotBlank( objTask.Status )
            && ( objTask.Status.equalsIgnoreCase( 'Completed' ) || objTask.Status.equalsIgnoreCase( 'Closed' ) ) 
            && !oldMap.get(objTask.Id).Status.equalsIgnoreCase('Completed') 
            && !oldMap.get(objTask.Id).Status.equalsIgnoreCase('Closed') ) ) {
                
                mapCaseToResolveComplaintTask.put(objTask.WhatId, objTask);
                
            }
            /**************************************************************************************************************************
            Validating Valuations Initiation Task  - Added by Aishwarya Todkar on 10/12/2020 
            ***************************************************************************************************************************/
            
            else if( objTask.WhatId != null
            && String.valueOf( objTask.WhatId ).startsWith( '500' )
            && String.isNotBlank(objTask.Subject)
            && ( objTask.Subject.equalsIgnoreCase( Label.Mortgage_10_1 )
            && String.isNotBlank( objTask.Status )
            && ( objTask.Status.equalsIgnoreCase( 'Completed' ) || objTask.Status.equalsIgnoreCase( 'Closed' ) ) 
            && !oldMap.get(objTask.Id).Status.equalsIgnoreCase('Completed') 
            && !oldMap.get(objTask.Id).Status.equalsIgnoreCase('Closed') ) ) {
                
                mapCaseToValuationTask.put(objTask.WhatId, objTask);
                
            }
        }
        
        if (mapFMCaseTaskId != null && mapFMCaseTaskId.keyset() != null 
            && !mapFMCaseTaskId.keySet().isEmpty()){
            for (FM_Case__c objFMCase : [Select Id
                                            , Internal_comments__c
                                            , Feedback_to_customer__c
                                         From FM_Case__c
                                         Where Id IN: mapFMCaseTaskId.keyset()]){
                if (string.isBlank(objFMCase.Internal_comments__c) 
                    || string.isBlank(objFMCase.Feedback_to_customer__c)){
                        String err = 'Please fill details in Internal Comments and Customer Feedback, before closing this task';
                    mapIdTask.get(mapFMCaseTaskId.get(objFMCase.Id)).addError( err );
                }
            }
        }

        /***********************************************************************************************************************
            Validating Task LHO-Hotel closure - Added by Aishwarya Todkar on 5/3/2020 
        ************************************************************************************************************************/
        if( mapCaseIdToTask != null && mapCaseIdToTask.keySet().size() > 0 ) {
            Map<Id, Boolean> mapCaseToDocExist = new Map<Id,Boolean>();
            for( SR_Attachments__c objSrAtt : [ SELECT
                                                    Id
                                                    , Case__c
                                                    , Attachment_Url__c
                                                FROM
                                                    SR_Attachments__c
                                                WHERE
                                                    Name LIKE '%Signed POA%'
                                                AND
                                                    Case__c In : mapCaseIdToTask.keySet()
                                                ] ) {
                if( String.isNotBlank( objSrAtt.Attachment_Url__c ) ) {
                    //if( mapCaseToDocExist.containsKey(  objSrAtt.Case__c ) && mapCaseToDocExist) {}
                    mapCaseToDocExist.put( objSrAtt.Case__c, true);
                }
                else if( !mapCaseToDocExist.containsKey(  objSrAtt.Case__c ) ) {
                    mapCaseToDocExist.put( objSrAtt.Case__c, false);
                }
            }//End for

            for( Case objCase : [SELECT
                                    Id
                                    , Bank_Name_dtpc__c
                                FROM 
                                    Case
                                WHERE
                                    Id IN : mapCaseIdToTask.keySet()
                                ]) {
                
                System.debug('LHO objCase== ' + objCase);
                //Validate bank details
                if( String.isBlank( objCase.Bank_Name_dtpc__c ) ) {
                    if( mapCaseIdToTask.containsKey( objCase.Id ) )
                        mapCaseIdToTask.get( objCase.Id ).addError( 'Please update Bank details.' );
                }

                //Validate POA notarized
                if( mapCaseToDocExist.containsKey( objCase.Id ) && !mapCaseToDocExist.get( objCase.Id ) ) {
                    if( mapCaseIdToTask.containsKey( objCase.Id ) )
                        mapCaseIdToTask.get( objCase.Id ).addError( 'Please upload Signed POA' );
                }
            }
        }
        /******************************************* LHO-Hotel Task Validation End ************************************************/

        /**************************************************************************************************************************
            Validating Rebate closure - Added by Aishwarya Todkar on 18/3/2020 
        ***************************************************************************************************************************/

        if( mapCaseIdToRebateTask != null && mapCaseIdToRebateTask.keySet().size() > 0 ) {
            Map<Id, Boolean> mapCaseToDocExist = new Map<Id,Boolean>();
            for( SR_Attachments__c objSrAtt : [ SELECT
                                                    Id
                                                    , Case__c
                                                    , Attachment_Url__c
                                                FROM
                                                    SR_Attachments__c
                                                WHERE
                                                    Name LIKE '%Signed Advance Payment Rebate Letter%'
                                                AND
                                                    Case__c In : mapCaseIdToRebateTask.keySet()
                                                ] ) {
                if( String.isNotBlank( objSrAtt.Attachment_Url__c ) ) {
                    mapCaseToDocExist.put( objSrAtt.Case__c, true);
                }
                else if( !mapCaseToDocExist.containsKey(  objSrAtt.Case__c ) ) {
                    mapCaseToDocExist.put( objSrAtt.Case__c, false);
                }
            }//End for

            for( Case objCase : [SELECT
                                    Id
                                    , Email_Sent__c
                                    , Discount_Type__c
                                FROM 
                                    Case
                                WHERE
                                    Id IN : mapCaseIdToRebateTask.keySet()
                                AND
                                    RecordTypeId != null
                                AND
                                    RecordType.Name = 'Rebate On Advance'
                                AND
                                    Discount_Type__c != null
                                ]) {
                
                //Validate Upload siged letter if Conditional
                if( objCase.Discount_Type__c.equalsIgnoreCase( 'Conditional' )
                && mapCaseToDocExist.containsKey( objCase.Id ) 
                && !mapCaseToDocExist.get( objCase.Id )
                && mapCaseIdToRebateTask.containsKey( objCase.Id ) ) {
                    mapCaseIdToRebateTask.get( objCase.Id ).addError( 'Please upload Signed Advance Payment Letter.' );
                }
                //Validate email sent if unconditional
                else if( objCase.Discount_Type__c.equalsIgnoreCase( 'Unconditional' )
                && !objCase.Email_Sent__c 
                && mapCaseIdToRebateTask.containsKey( objCase.Id ) )
                    mapCaseIdToRebateTask.get( objCase.Id ).addError( 'Please send email before closing task!' );
            }
        }
        /******************************************* Rebate Task Validation End ******************************************/
    
        /**************************************************************************************************************************
            Validating Final Bank Approval Date - Added by Ruchika Choudhary on 24/9/2020 
        ***************************************************************************************************************************/
        if (mapCaseIdToBankApprovalTask != null && mapCaseIdToBankApprovalTask.keyset() != null 
            && !mapCaseIdToBankApprovalTask.keySet().isEmpty()){
            for (Case objFMCase : [Select Id
                                            , Final_Bank_Approval_Date__c
                                         From Case
                                         Where Id IN: mapCaseIdToBankApprovalTask.keyset()]){
                if (objFMCase.Final_Bank_Approval_Date__c == null){
                    mapCaseIdToBankApprovalTask.get(objFMCase.Id).addError('Please enter Final Bank Approval date before closing task. ');
                }
            }
        }
        
        /**************************************************************************************************************************
             Validating Resolve Complaint Task  - Added by Aishwarya Todkar on 15/10/2020
        ***************************************************************************************************************************/
        if (mapCaseToResolveComplaintTask != null && mapCaseToResolveComplaintTask.keyset() != null 
            && !mapCaseToResolveComplaintTask.keySet().isEmpty()){
            for (Case objCase : [ SELECT Id
                                        , CRE_Analysis__c
                                        , Resolution__c
                                        , Happiness_Complaint_Type__c
                                        , Happiness_Complaint_SubType__c
                                 FROM 
                                     Case
                                 WHERE 
                                     Id IN: mapCaseToResolveComplaintTask.keyset() ] ) {
                if( String.isBlank(objCase.CRE_Analysis__c) || String.isBlank(objCase.Resolution__c) 
                || String.isBlank(objCase.Happiness_Complaint_Type__c) || String.isBlank(objCase.Happiness_Complaint_Type__c) ){
                    String err = 'Please update Action Taken, Resolution, Happiness Complaint Type and Happiness Complaint '
                                + 'SubType on case before closing the task.';
                    mapCaseToResolveComplaintTask.get(objCase.Id).addError( err );
                }
            }
        }
        /******************************************* Resolve Complaint Validation End ******************************************/

        /**************************************************************************************************************************
             Validating Valuations Initiation Task  - Added by Aishwarya Todkar on 10/12/2020
        ***************************************************************************************************************************/
        if (mapCaseToValuationTask != null && mapCaseToValuationTask.keyset() != null 
            && !mapCaseToValuationTask.keySet().isEmpty()){
            for (Case objCase : [ SELECT Id
                                        , Valuation_Completion_Date__c
                                 FROM 
                                     Case
                                 WHERE 
                                     Id IN: mapCaseToValuationTask.keyset() ] ) {
                if( objCase.Valuation_Completion_Date__c == null ){
                    String err = 'Please update valuation accompanied date!';
                    mapCaseToValuationTask.get(objCase.Id).addError( err );
                }
            }
        }
        /******************************************* Valuations Initiation Validation End ******************************************/
    }
}