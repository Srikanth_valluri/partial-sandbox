/*
* Description - Test class developed for 'CancelServiceRequest'
*
* Version            Date            Author            Description
* 1.0              28/02/2018        Ashish           Initial Draft
*/

@isTest 
private class CancelServiceRequestTest {

    static testMethod void testCancelServiceRequest() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        insert objCase ;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        test.startTest();
        	CancelServiceRequest objServ = new CancelServiceRequest( stdController );
        	objServ.cancelServiceRequest();
        	
        	objServ.caseId = null ; 
        	objServ.cancelServiceRequest();
        test.stopTest();  
        
    }
}