@isTest
private class FmIpmsRestCoffeeServicesTest {

    @TestSetup
    static void customSettingData() {

        insert new IpmsRestServices__c(
                   SetupOwnerId = UserInfo.getOrganizationId(),
                   BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                   Username__c = 'oracle_user',
                   Password__c = 'crp1user',
                   Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                   BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                   Timeout__c = 120000
                   );
    }

    //@isTest
    //static void getAccessTokenTest() {
    //    Test.startTest();
    //        System.assertEquals(Null, FmIpmsRestCoffeeServices.getAccessToken() );
    //    Test.stopTest();
    //}

    @isTest
    static void getUnitSoaNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestCoffeeServices.getUnitSoa(NULL));
        Test.stopTest();
    }

    @isTest
    static void getUnitSoaValidTokenTest() {

        String jsonStr =   '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ' ;

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));
        String responseBody;

        Test.startTest();
            responseBody = FmIpmsRestCoffeeServices.getUnitSoa('1234');
        Test.stopTest();
        System.assertEquals('https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a', responseBody);
    }

    @isTest
    static void getUnitSoaInvalidTokenTest() {

        FmHttpCalloutMock.Response loginResponse = new FmHttpCalloutMock.Response( 200 , 'Success' ,
            '   {  '  +
            '     '  +
            '       "token": "eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1NTS2MLE1NjUx2l1IoCqIClhSVIoLQ4tSgvMTcVqM8i2NMt3NxCN8ggKCJM18MwMMpNNyjYM8gv1EypFgCrDSnJXQAAAA.msD8bcPXX_LvL7aBvkry5gUkHdbt_jDCjFLhWh_gHVw",  '  +
            '     '  +
            '       "tokenType": "Bearer",  '  +
            '     '  +
            '       "validTill": "01-Jun-2019 at 18:22:15"  '  +
            '     '  +
            '  }  '
        );

        FmHttpCalloutMock.Response unitSoaMockResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
        '   "timestamp": "2019-06-19T12:05:33.818+0000",  '  +
        '   "status": 401,  '  +
        '   "error": "Unauthorized",  '  +
        '   "message": "Unauthorized",  '  +
        '   "path": "/COFFEE/customer/DPSOA"  '  +
        '  }  ');

        String UNIT_SOA_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.UNIT_SOA, new List<String> {'3901'});
        String APEX_LOGIN_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.APEX_LOGIN, new List<String> {'8MABLQM-KJ8I8-1XA58-WWCM1S1'} );

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            UNIT_SOA_str => unitSoaMockResponse,
            APEX_LOGIN_str => loginResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        String responseBody;

        Test.startTest();
            responseBody = FmIpmsRestCoffeeServices.getUnitSoa('3901');
        Test.stopTest();

        System.assertEquals(Null, responseBody);
    }

    /* -------------------UnitSoaInLanguage  --------------------- */

    @isTest
    static void getUnitSoaInLanguageNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestCoffeeServices.getUnitSoaInLanguage(NULL));
        Test.stopTest();
    }

    @isTest
    static void getUnitSoaInLanguageValidTokenTest() {
        String jsonStr =  '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ' ;



       Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));
       String responseBody;

       Test.startTest();
           responseBody = FmIpmsRestCoffeeServices.getUnitSoaInLanguage('1234');
       Test.stopTest();
       System.assertEquals('https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a', responseBody);
    }

    @isTest
    static void getUnitSoaInLanguageInvalidTokenTest() {

        FmHttpCalloutMock.Response loginResponse = new FmHttpCalloutMock.Response( 200 , 'Success' ,
            '   {  '  +
            '     '  +
            '       "token": "eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1NTS2MLE1NjUx2l1IoCqIClhSVIoLQ4tSgvMTcVqM8i2NMt3NxCN8ggKCJM18MwMMpNNyjYM8gv1EypFgCrDSnJXQAAAA.msD8bcPXX_LvL7aBvkry5gUkHdbt_jDCjFLhWh_gHVw",  '  +
            '     '  +
            '       "tokenType": "Bearer",  '  +
            '     '  +
            '       "validTill": "01-Jun-2019 at 18:22:15"  '  +
            '     '  +
            '  }  '
        );

        FmHttpCalloutMock.Response unitSoaInLangMockResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
        '   "timestamp": "2019-06-19T12:05:33.818+0000",  '  +
        '   "status": 401,  '  +
        '   "error": "Unauthorized",  '  +
        '   "message": "Unauthorized",  '  +
        '   "path": "/COFFEE/customer/DPSOA"  '  +
        '  }  ');

        String UNIT_SOA_Lang_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.UNIT_SOA_LANG, new List<String> {'3901'});
        String APEX_LOGIN_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.APEX_LOGIN, new List<String> {'8MABLQM-KJ8I8-1XA58-WWCM1S1'} );

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            UNIT_SOA_Lang_str => unitSoaInLangMockResponse,
            APEX_LOGIN_str => loginResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
        String responseBody;

        Test.startTest();
            responseBody = FmIpmsRestCoffeeServices.getUnitSoaInLanguage('3901');
        Test.stopTest();
    }

    /* -------------------BulkSoa   --------------------- */

    @isTest
    static void getBulkSoaNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestCoffeeServices.getBulkSoa(NULL));
        Test.stopTest();
    }

    @isTest
    static void getBulkSoaValidTokenTest() {
        String jsonStr =  '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ' ;

       Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));
       String responseBody;

       Test.startTest();
           responseBody = FmIpmsRestCoffeeServices.getBulkSoa('1234');
       Test.stopTest();
       System.assertEquals('https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a', responseBody);
    }

    @isTest
    static void getBulkSoaInvalidTokenTest() {

        FmHttpCalloutMock.Response loginResponse = new FmHttpCalloutMock.Response( 200 , 'Success' ,
            '   {  '  +
            '     '  +
            '       "token": "eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1NTS2MLE1NjUx2l1IoCqIClhSVIoLQ4tSgvMTcVqM8i2NMt3NxCN8ggKCJM18MwMMpNNyjYM8gv1EypFgCrDSnJXQAAAA.msD8bcPXX_LvL7aBvkry5gUkHdbt_jDCjFLhWh_gHVw",  '  +
            '     '  +
            '       "tokenType": "Bearer",  '  +
            '     '  +
            '       "validTill": "01-Jun-2019 at 18:22:15"  '  +
            '     '  +
            '  }  '
        );

        FmHttpCalloutMock.Response bulkSoaMockResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
        '   "timestamp": "2019-06-19T12:05:33.818+0000",  '  +
        '   "status": 401,  '  +
        '   "error": "Unauthorized",  '  +
        '   "message": "Unauthorized",  '  +
        '   "path": "/COFFEE/customer/DPSOA"  '  +
        '  }  ');

        String BULK_SOA_LANG_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.BULK_SOA, new List<String> {'63062'});
        String APEX_LOGIN_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.APEX_LOGIN, new List<String> {'8MABLQM-KJ8I8-1XA58-WWCM1S1'} );

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            BULK_SOA_LANG_str => bulkSoaMockResponse,
            APEX_LOGIN_str => loginResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
        String responseBody;

        Test.startTest();
            responseBody = FmIpmsRestCoffeeServices.getBulkSoa('63062');
        Test.stopTest();
    }

    /* -------------------BulkSoaInLanguage   --------------------- */
    @isTest
    static void getBulkSoaInLanguageNullTest() {
        Test.startTest();
            System.assertEquals(NULL, FmIpmsRestCoffeeServices.getBulkSoaInLanguage(NULL));
        Test.stopTest();
    }

    @isTest
    static void getBulkSoaInLanguageValidTokenTest() {
        String jsonStr =  '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ' ;

       Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));
       String responseBody;

       Test.startTest();
           responseBody = FmIpmsRestCoffeeServices.getBulkSoaInLanguage('1234');
       Test.stopTest();
       System.assertEquals('https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a', responseBody);
    }

    @isTest
    static void getBulkSoaInLanguageInvalidTokenTest() {

        FmHttpCalloutMock.Response loginResponse = new FmHttpCalloutMock.Response( 200 , 'Success' ,
            '   {  '  +
            '     '  +
            '       "token": "eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1NTS2MLE1NjUx2l1IoCqIClhSVIoLQ4tSgvMTcVqM8i2NMt3NxCN8ggKCJM18MwMMpNNyjYM8gv1EypFgCrDSnJXQAAAA.msD8bcPXX_LvL7aBvkry5gUkHdbt_jDCjFLhWh_gHVw",  '  +
            '     '  +
            '       "tokenType": "Bearer",  '  +
            '     '  +
            '       "validTill": "01-Jun-2019 at 18:22:15"  '  +
            '     '  +
            '  }  '
        );

        FmHttpCalloutMock.Response bulkSoaInLangMockResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
        '   "timestamp": "2019-06-19T12:05:33.818+0000",  '  +
        '   "status": 401,  '  +
        '   "error": "Unauthorized",  '  +
        '   "message": "Unauthorized",  '  +
        '   "path": "/COFFEE/customer/DPSOA"  '  +
        '  }  ');

        String BULK_SOA_IN_LANG_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.BULK_SOA_LANG, new List<String> {'63062'});
        String APEX_LOGIN_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.APEX_LOGIN, new List<String> {'8MABLQM-KJ8I8-1XA58-WWCM1S1'} );

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            BULK_SOA_IN_LANG_str => bulkSoaInLangMockResponse,
            APEX_LOGIN_str => loginResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
        String responseBody;

        Test.startTest();
            responseBody = FmIpmsRestCoffeeServices.getBulkSoaInLanguage('63062');
        Test.stopTest();
    }
    /*---------------------------------------------------------------*/
    @isTest
    static void getAllUnitSoaInLanguageUrlNullTest() {
        Test.startTest();
             System.assertEquals(Null, FmIpmsRestCoffeeServices.getAllUnitSoaInLanguageUrl(Null) );
        Test.stopTest();
    }

    @isTest
    static void getAllUnitSoaInLanguageUrlTest() {
        String jsonStr =  '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ';

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));

        List<String> responseStrlst = new List<String>();

        List<String> strlstForAssert = new List<String>{'https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd', 'https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a'};

        Test.startTest();
            responseStrlst = FmIpmsRestCoffeeServices.getAllUnitSoaInLanguageUrl('1234');
        Test.stopTest();

        System.assertEquals(strlstForAssert, responseStrlst);

    }

    /*----------------------------------------------------------------*/

    @isTest
    static void getAllBulkSoaInLanguageUrlNullTest() {
        Test.startTest();
             System.assertEquals(Null, FmIpmsRestCoffeeServices.getAllBulkSoaInLanguageUrl(Null) );
        Test.stopTest();
    }

    @isTest
    static void getAllBulkSoaInLanguageUrlTest() {
        String jsonStr =  '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ' ;

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));

        List<String> responseStrlst = new List<String>();

        List<String> strlstForAssert = new List<String>{'https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd', 'https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a'};

        Test.startTest();
            responseStrlst = FmIpmsRestCoffeeServices.getAllBulkSoaInLanguageUrl('1234');
        Test.stopTest();

        System.assertEquals(strlstForAssert, responseStrlst);
    }

    /*----------------------------------------------------------------*/
    //@isTest
    //static void getInvoiceByRegIdInLangNullTest() {
    //    Test.startTest();
    //        System.assertEquals(Null, FmIpmsRestCoffeeServices.getInvoiceByRegIdInLang(Null) );
    //    Test.stopTest();
    //}

    //@isTest
    //static void getInvoiceByRegIdInLangTest() {
    //    String jsonStr =  '   {  '  +
         //'       "responseId": "3072019-10305298",  '  +
         //'       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         //'       "status": "S",  '  +
         //'       "responseMessage": "Process Completed",  '  +
         //'       "elapsedTimeMs": 8497,  '  +
         //'       "responseLines": [  '  +
         //'           {  '  +
         //'               "documentName": "DPSOA",  '  +
         //'               "language": "EN",  '  +
         //'               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         //'           },  '  +
         //'           {  '  +
         //'               "documentName": "DPSOA_AR",  '  +
         //'               "language": "AR",  '  +
         //'               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         //'           }  '  +
         //'       ],  '  +
         //'       "complete": true  '  +
         //'  }  ' ;

    //    Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));

    //    List<String> responseStrlst = new List<String>();

    //    List<String> strlstForAssert = new List<String>{'https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd', 'https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a'};

    //    Test.startTest();
    //        responseStrlst = FmIpmsRestCoffeeServices.getInvoiceByRegIdInLang('1234');
    //    Test.stopTest();

    //    System.assertEquals(strlstForAssert, responseStrlst);
    //}

    /*----------------------------------------------------------------*/
    @isTest
    static void getInvoiceByCustTrxIdInLangNullTest() {
        Test.startTest();
            System.assertEquals(Null, FmIpmsRestCoffeeServices.getInvoiceByCustTrxIdInLang(Null) );
        Test.stopTest();
    }

    @isTest
    static void getInvoiceByCustTrxIdInLangTest() {
        String jsonStr =  '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ' ;

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));

        List<String> responseStrlst = new List<String>();

        List<String> strlstForAssert = new List<String>{'https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd', 'https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a'};

        Test.startTest();
            responseStrlst = FmIpmsRestCoffeeServices.getInvoiceByCustTrxIdInLang('1234');
        Test.stopTest();

        System.assertEquals(strlstForAssert, responseStrlst);
    }

    /*----------------------------------------------------------------*/

    @isTest
    static void getAllReceiptUrlNullTest() {
        Test.startTest();
            System.assertEquals(Null, FmIpmsRestCoffeeServices.getAllReceiptUrl(Null) );
        Test.stopTest();
    }

    static void getAllReceiptUrlTest() {
        String jsonStr =  '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ' ;

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));

        List<String> responseStrlst = new List<String>();

        List<String> strlstForAssert = new List<String>{'https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd', 'https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a'};

        Test.startTest();
            responseStrlst = FmIpmsRestCoffeeServices.getAllReceiptUrl('1234');
        Test.stopTest();

        System.assertEquals(strlstForAssert, responseStrlst);
    }

}