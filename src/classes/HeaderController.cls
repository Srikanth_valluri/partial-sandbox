public with sharing class HeaderController{

    public String requiredCls        {get; set;}
    public String selectedStand      {get; set;}
    public List<SelectOption> stands {get; set;}
    public string selectedStandStr   {get;set;}
    public String selectstan         {get; set;}
    map<String,String> mapStandVal_Label;
    public List<User> lstuser ;

    public HeaderController() {
        lstuser = new List<User>();
        mapStandVal_Label = new map<String,String>();
        stands = new List<SelectOption>();
        for(Campaign__c campaign : [SELECT Name, Id, Campaign_Name__c FROM Campaign__c WHERE Active__c = true AND Campaign_Type_New__c = 'Stands' AND Campaign_Name__c != NULL ] ) {
            stands.add(new SelectOption(campaign.Id, campaign.Campaign_Name__c));
            mapStandVal_Label.put(campaign.Id, campaign.Campaign_Name__c);
        }
        
        /*List<StandName__c> lstStand = StandName__c.getall().values();
        if(lstStand.size() >0) {
            selectedStand = lstStand[0].Name;
        }else{
            
        }*/
        
        lstuser = [Select Id,Stand_Id__c FROM User WHERE Id=:UserInfo.getUserId() limit 1];

        if(lstuser.size() >0) {
            if(lstuser[0].Stand_Id__c != null) {
                selectedStand = mapStandVal_Label.get(lstuser[0].Stand_Id__c);
            }
        }else{

        }
        
    }
    
    public void selectStand() {
      //  List<StandName__c> mcs = StandName__c.getall().values();
        lstuser = [Select   Id,
                            Stand_Id__c 
                    FROM 
                            User 
                    WHERE 
                            Id=:UserInfo.getUserId() limit 1];

        if(lstuser.size() > 0) {
            lstuser[0].Stand_Id__c = selectedStandStr;
            update lstuser;
        }
        selectedStand = mapStandVal_Label.get(selectedStandStr);


        /*if(mcs.size() >0) {
            if( mcs[0].Name == ''){
                mcs[0].Name = mapStandVal_Label.get(selectedStandStr);
                mcs[0].Id__c = selectedStandStr;
            }else{
                mcs[0].Name = mapStandVal_Label.get(selectedStandStr);
                mcs[0].Id__c = selectedStandStr;
            }
            update mcs;
        }else {
            StandName__c objStnd = new StandName__c();
            objStnd.Name = mapStandVal_Label.get(selectedStandStr);
            objStnd.Id__c = selectedStandStr;
            insert objStnd;
            System.debug('mcs');
        }

        List<StandName__c> lstStand = StandName__c.getall().values();
        selectedStand = lstStand[0].Name;*/

        
         
    }
    
     public PageReference forwardToStartPage() {
        String communityUrl = ApexPages.currentPage().getHeaders().get('Host');
        //String customHomePage = '/apex/hello';
        if (UserInfo.getUserType().equals('Guest')) {
            return new PageReference('/promoters/PromoterLogin?status=inactive' );
        }else {
            //return Network.communitiesLanding();
            return null;
        }
      }


}