@isTest
private class Damac_TrackVehicle_Test {
    
    static testMethod void getTestResults() {
        
        Fleet_Root_API__c cs = new Fleet_Root_API__c();
        cs.Name = 'test';
        cs.API_Key__c = 'gA1yQQg85Dhicph7MG5zyH4RkTbiSI+7yRMTZQf65M4+860aU3v14dFnUPFwpNEvROLYkgs7Gxi3nCljDk08L2D5mLw/Y8bnj9rfUMqrN/Sbp4tYN/8JOA==';
        cs.Endpoint_URL__c = 'http://trackapi.fleetroot.com/';
        insert cs;
        
        Cars__c cars = new Cars__c();
        cars.name = 'testcar';
        cars.Vehicle_Id__c='123';
        insert cars;
        
        Trip__c testTrip = new Trip__c();
        testTrip.Car__c = cars.Id;
        insert testTrip;
        
        Trip_Events__c testTrip_Events = new Trip_Events__c();
        
        testTrip_Events.Latitude__c = 'test';
        testTrip_Events.Longitude__c='test123';
        testTrip_Events.Cars__c= cars.Id;
        testTrip_Events.Check_In_Reason__c ='Customer Picked';
        testTrip_Events.Trip__c= testTrip.Id;
        
        testTrip_Events.Time__c = '2019-07-02T05:06:43+04:00';
        insert testTrip_Events;
        Trip_Events__c testTrip_Events1 = new Trip_Events__c();
        
        testTrip_Events1.Latitude__c = 'test';
        testTrip_Events1.Longitude__c='test123';
        testTrip_Events1.Cars__c= cars.Id;
        testTrip_Events1.Check_In_Reason__c ='CheckOut';
        testTrip_Events1.Trip__c= testTrip.Id;
        
        testTrip_Events1.Time__c = '2019-07-02T05:06:43+04:00';
        system.debug('time__c'+ testTrip_Events1.Time__c);
        insert testTrip_Events1;
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testTrip);
        Damac_TrackVehicle testDamac_TrackVehicle = new Damac_TrackVehicle(sc);
        Damac_TrackVehicle.locationDetails testDamac_locationDetails = new Damac_TrackVehicle.locationDetails();
        
    }
}