@isTest
public class GenericControllerTest {
    @isTest
    static void testReturnFiles(){
        Case objCase = new Case();
        insert objCase;
        
        SR_Attachments__c objSRAttachment = new SR_Attachments__c();
        objSRAttachment.Name = 'test';
        objSRAttachment.Case__c = objCase.Id;
        insert objSRAttachment;
        
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        
        
        GenericController obj = new GenericController();
        GenericController.FileInfo objfileInfo = new GenericController.FileInfo();
        objfileInfo.Title = 'testseperatortest2';
        objfileInfo.VersionData = Blob.valueOf('Unit Test Attachment Body');
        
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
        MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response_x = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
        response_x.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-12345--Test1.pdf Processed] PK Value#:2-2-005058 Additionalfile","PARAM_ID":"testseperatortest2.pdf","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-12345-Test1.pdf"}],"message":"Process Completed Returning 2 Response Message(s)...","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());
        
        Test.startTest();
            List<GenericController.FileInfo> listFileInfo = new List<GenericController.FileInfo>();
            listFileInfo.add(objfileInfo);
            UploadMultipleDocController.strLabelValue  = 'N';
            
            GenericController.returnFiles(objCase.Id);
            GenericController.syncWithOutlook(listFileInfo, objCase.Id);
            //GenericController.saveFiles(listFileInfo);
        Test.stopTest();
    }
    
    @isTest
    static void testRejectTask(){
        Case objCase = new Case();
        insert objCase;
        
        Task objTask = new Task();
        objTask.Subject = 'Verify Proof of Payment Details in IPMS';
        objTask.Status = 'New';
        objTask.WhatId = objCase.Id;
        objTask.Assigned_User__c = 'Finance';
        objTask.Process_Name__c = 'POP';
        insert objTask;
        
        SR_Attachments__c objSRAttach = new SR_Attachments__c();
        objSRAttach.Name = 'test';
        objSRAttach.Case__c = objCase.Id;
        insert objSRAttach;
        
        List<String> lstStr = new List<String>();
        lstStr.add(objSRAttach.Id);
        
        Test.startTest();
        GenericController.rejectTask(objCase.Id, 'Duplicate');
        GenericController.updateSRAttachemnt(lstStr);
        Test.stopTest();
        
    }
     @isTest
    static void updateCaseStatus1(){
        Case objCase = new Case();
        insert objCase;
        
       Test.startTest();
        GenericController.updateCaseStaus(true, 'subBtn',objCase.Id);
        
        Test.stopTest();
        
    }
     @isTest
    static void updateCaseStatus2(){
        Case objCase = new Case();
        insert objCase;
        
        Test.startTest();
        GenericController.updateCaseStaus(false, 'subBtn',objCase.Id);
        
        Test.stopTest();
        
    }
    
    
     @isTest 
    static void testFetchARCallout() {
        
        //String JSONMsg = '{"GET_UNIDENTIFIED_RECEIPT_Input":{ "RESTHeader":{ "Responsibility":"ONT_ICP_SUPER_USER", "RespApplication":"ONT", "SecurityGroup":"STANDARD", "NLSLanguage":"AMERICAN" }, "InputParameters":{ "P_BANK_REF_NUMBER":"","P_BANK_ACCOUNT_ID":"10402","P_DEPOSIT_DATE":"2016-03-16","P_NOOF_DAYS": "50","P_AMOUNT" :"5000","P_DP_RANGE_AMOUNT" : "3000"}}}';
            
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        //req.requestURI = '/login';  
        req.addParameter('username', 'oracle_user');
        req.addParameter ('password', 'crp1user');
        //req.addParameter ('domain', 'test.salesforce.com');
        
        req.httpMethod = 'Post';
        //req.requestBody = Blob.valueof(JSONMsg);
        
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        //GenericController.fetchARCallout(); 
        Test.stopTest();
        
    }
    
}