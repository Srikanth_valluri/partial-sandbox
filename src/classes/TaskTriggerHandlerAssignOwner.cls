/*
 * Version    Owner        Description                                Last ModifiedDate
 * 1.0        Vivian       Initial Draft                              20/Feb/2018
 * 1.1        Vivian       Trigger must fire only for Tasks           01/Oct/2018
                           related to Case object
 *
 */
public without sharing class TaskTriggerHandlerAssignOwner{
    public void beforeInsert(list<Task> lstTask){
        set<Id> setUserId = new set<Id>();
        for(Task objT : lstTask){
            if(objT.WhatId != null 
            && String.valueOf(objT.WhatId).startsWith('500')){
                setUserId.add(objT.OwnerId);
            }
        }
        
        if(!setUserId.isEmpty()){
            map<Id,User> mapId_User = new map<Id,User>([Select u.IsPortalEnabled
                                                             , u.IsActive
                                                             , u.Id
                                                             , u.ContactId 
                                                        From User u 
                                                        where ContactId != null
                                                        and isActive = true
                                                        and Id IN : setUserId]);
            // If tasks were assigned to portal users
            if(!mapId_User.isEmpty()){
                for(Task objT : lstTask){
                    if(mapId_User.containsKey(objT.OwnerId)
                    && mapId_User.get(objT.OwnerId) != null){
                        objT.OwnerId = Label.DefaultCaseOwnerId;
                    }
                } // end of for loop
            } // end of mapId_User not empty
        } // end of setUserId not empty
    } // end of beforeInsert
}