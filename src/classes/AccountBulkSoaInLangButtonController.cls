public class AccountBulkSoaInLangButtonController {

    public Id recordId {get; set;}
    public String responseUrl {get; set;}
    public String accessToken {get; set;}
    public String pdfFile {get; set;}
    public Blob pdfBlob {get; set;}
    //public Blob emailBody {get; set;}

    public AccountBulkSoaInLangButtonController(ApexPages.StandardController controller) {
      recordId = controller.getId();
        System.debug('Account Id in consturctor: '+recordId);
    }

    public PageReference getBulkSoaLanguageUrl() {
        FmIpmsRestCoffeeServices.updateCustomSetting = false;

        Account account = [SELECT Id, Party_ID__c, RecordType.Name
                           FROM Account
                           WHERE Id =: recordId];
        String partyId = account.Party_ID__c;
        System.debug('partyId is: '+partyId);

        responseUrl = FmIpmsRestCoffeeServices.getBulkSoaInLanguage(partyId);
        System.debug('responseUrl is: '+responseUrl);

        if(String.isBlank(responseUrl) || responseUrl == null || responseUrl == 'Null') {
            System.debug('In ApexMessage Method');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'There was an error processing your request');
            ApexPages.addMessage(msg);
            return null;
        }

        //Http callout

        accessToken = FmIpmsRestCoffeeServices.accessToken;
        System.debug('AccessToken is' + accessToken);

        HttpRequest req = new HttpRequest();
        req.setEndpoint(responseUrl);
        req.setHeader('Accept', 'application/json');
        req.setMethod('GET');
        req.setHeader('Authorization','Bearer' + accessToken);
        req.setTimeout(120000);
        HttpResponse response = new Http().send(req);
        System.debug('Status Code = ' + response.getStatusCode());
        System.debug('Pdf Body = ' + response.getBodyAsBlob());


        if(response.getStatusCode() == 200) {
            pdfBlob =  response.getBodyAsBlob();
            pdfFile = EncodingUtil.base64Encode(pdfBlob);
            //TODO 
            GenericUtility.createSOACreator(responseUrl,UserInfo.getUserId(),system.now(),'','Generate Spl Language Bulk SOA',account.Id,NULL,NULL);

        }

        FmIpmsRestCoffeeServices.updateCustomSetting = true;
        FmIpmsRestCoffeeServices.updateBearerToken();      //calling updateMethod of Custom Setting after all callouts execution

        return null;
    }
}