@isTest
global with sharing class SOAPCalloutServiceMockRPTermination implements WebServiceMock {

    public integer intResponseNumber ;

    public SOAPCalloutServiceMockRPTermination () {

    }

    public SOAPCalloutServiceMockRPTermination ( integer intNum ) {
        intResponseNumber = intNum ;
    }

    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        if(request instanceof AOPTMQClass.getMasterMilestone_element)
        {
          AOPTMQClass.getMasterMilestoneResponse_element responseNew = new AOPTMQClass.getMasterMilestoneResponse_element();
          responseNew.return_x = '{"MILESTONE_TAB_TYPE":[{"MILESTONE_EVENT":"Completion of Basement &amp; Reaching Ground Level","ARABIC_MILESTONE_EVENT":"الانتهاء من الطابق السفلي والوصول إلى المستوى الأرضي"},{"MILESTONE_EVENT":"On 10% of Project Completion","ARABIC_MILESTONE_EVENT":"عند استكمال 10 ٪ من  المشروع"},{"MILESTONE_EVENT":"On 15% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند إستكمال 15 ٪ من المشروع"},{"MILESTONE_EVENT":"On 20% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند إستكمال 20 ٪ من المشروع"},{"MILESTONE_EVENT":"On 25% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند إستكمال 25 ٪ من المشروع"},{"MILESTONE_EVENT":"On 30% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند إستكمال 30 ٪ من المشروع"},{"MILESTONE_EVENT":"On 35% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند استكمال 35 ٪ من  المشروع"},{"MILESTONE_EVENT":"On 40% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند إستكمال 40 ٪ من المشروع"},{"MILESTONE_EVENT":"On 45% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند استكمال 45 ٪ من  المشروع"},{"MILESTONE_EVENT":"On 5% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند إنجاز 5 ٪ من المشروع"},{"MILESTONE_EVENT":"On 50% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند إستكمال 50 ٪ من المشروع"},{"MILESTONE_EVENT":"On 60% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند إستكمال 60 ٪ من المشروع"},{"MILESTONE_EVENT":"On 65% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند إنجاز 65 ٪  من المشروع"},{"MILESTONE_EVENT":"On 70% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند إستكمال 70 ٪ من المشروع"},{"MILESTONE_EVENT":"On 75% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند إستكمال 75 ٪ من المشروع"},{"MILESTONE_EVENT":"On 80% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند إستكمال 80 ٪ من المشروع"},{"MILESTONE_EVENT":"On 85% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند إستكمال 85 ٪ من المشروع"},{"MILESTONE_EVENT":"On 90% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند إستكمال 90 ٪ من المشروع"},{"MILESTONE_EVENT":"On 95% Of Project Completion","ARABIC_MILESTONE_EVENT":"عند إستكمال 95 ٪ من المشروع"},{"MILESTONE_EVENT":"On Completion","ARABIC_MILESTONE_EVENT":"عند الاكمال"},{"MILESTONE_EVENT":"On Completion 12th Floor Structure","ARABIC_MILESTONE_EVENT":"عند الانتهاء من هيكل الطابق 12"},{"MILESTONE_EVENT":"On Completion 6th Floor Structure","ARABIC_MILESTONE_EVENT":"عند الانتهاء من هيكل الطابق 6"},{"MILESTONE_EVENT":"On Completion 8th Floor Structure","ARABIC_MILESTONE_EVENT":"عند الانتهاء من هيكل الطابق 8"},{"MILESTONE_EVENT":"On Completion of 2nd Floor Structure","ARABIC_MILESTONE_EVENT":"عند الانتهاء من هيكل الطابق 2"},{"MILESTONE_EVENT":"On Completion of 5th floor Structure","ARABIC_MILESTONE_EVENT":"عند الانتهاء من هيكل الطابق 5"},{"MILESTONE_EVENT":"On Completion the Structure","ARABIC_MILESTONE_EVENT":"عند استكمال الهيكل"},{"MILESTONE_EVENT":"On completion of  15th Floor Structure","ARABIC_MILESTONE_EVENT":"عند الانتهاء من هيكل الطابق 15"},{"MILESTONE_EVENT":"On completion of Podium Structure","ARABIC_MILESTONE_EVENT":"عند الانتهاء من هيكل البوديوم"},{"MILESTONE_EVENT":"On or Before","ARABIC_MILESTONE_EVENT":"عند أو قبل"}],"message":"Successfully Fetched Data","status":"S"}';
          response.put('response_x', responseNew);
        }
        else if(request instanceof AOPTMQClass.getMilestonePaymentDetails_element)
        {
          AOPTMQClass.getMilestonePaymentDetailsResponse_element responseNew = new AOPTMQClass.getMilestonePaymentDetailsResponse_element();
          responseNew.return_x = '{"REG_TERM_PYMNT_TABLE":[{"REGISTRATION_ID":"74712","DUE_DATE":"06-DEC-2015","MILESTEON_PERCENT_VALUE":"24","PAID_AMOUNT":"98822.84","PAID_PERCENTAGE":"99.962","DUE_AMOUNT":"37.96","LINE_ID":"808914","MILESTONE_EVENT_AR":"فورا","DESCRIPTION":"DEPOSIT","MILESTONE_EVENT":"Immediate","INVOICE_AMOUNT":"98860.8","INSTALLMENT":"DP","TERM_ID":"145504"},{"REGISTRATION_ID":"74712","DUE_DATE":null,"MILESTEON_PERCENT_VALUE":"0","PAID_AMOUNT":null,"PAID_PERCENTAGE":"0","DUE_AMOUNT":null,"LINE_ID":"808915","MILESTONE_EVENT_AR":null,"DESCRIPTION":"1ST INSTALMENT","MILESTONE_EVENT":null,"INVOICE_AMOUNT":null,"INSTALLMENT":"I001","TERM_ID":"145505"},{"REGISTRATION_ID":"74712","DUE_DATE":"03-JUN-2016","MILESTEON_PERCENT_VALUE":"10","PAID_AMOUNT":"0","PAID_PERCENTAGE":"0","DUE_AMOUNT":"49430.4","LINE_ID":"808916","MILESTONE_EVENT_AR":"عند أو قبل","DESCRIPTION":"2ND INSTALMENT","MILESTONE_EVENT":"On or Before","INVOICE_AMOUNT":"49430.4","INSTALLMENT":"I002","TERM_ID":"145506"},{"REGISTRATION_ID":"74712","DUE_DATE":"02-AUG-2016","MILESTEON_PERCENT_VALUE":"10","PAID_AMOUNT":"0","PAID_PERCENTAGE":"0","DUE_AMOUNT":"49430.4","LINE_ID":"808917","MILESTONE_EVENT_AR":"عند أو قبل","DESCRIPTION":"3RD INSTALMENT","MILESTONE_EVENT":"On or Before","INVOICE_AMOUNT":"49430.4","INSTALLMENT":"I003","TERM_ID":"145507"},{"REGISTRATION_ID":"74712","DUE_DATE":"30-NOV-2016","MILESTEON_PERCENT_VALUE":"10","PAID_AMOUNT":"0","PAID_PERCENTAGE":"0","DUE_AMOUNT":"49430.4","LINE_ID":"808918","MILESTONE_EVENT_AR":"عند أو قبل","DESCRIPTION":"4TH INSTALMENT","MILESTONE_EVENT":"On or Before","INVOICE_AMOUNT":"49430.4","INSTALLMENT":"I004","TERM_ID":"145508"},{"REGISTRATION_ID":"74712","DUE_DATE":null,"MILESTEON_PERCENT_VALUE":"50","PAID_AMOUNT":null,"PAID_PERCENTAGE":"0","DUE_AMOUNT":null,"LINE_ID":"808919","MILESTONE_EVENT_AR":"عند أو قبل","DESCRIPTION":"5TH INSTALMENT","MILESTONE_EVENT":"On or Before","INVOICE_AMOUNT":null,"INSTALLMENT":"I005","TERM_ID":"145509"}],"message":"Successfully Fetched Details for RegID74712","status":"S"}';
          response.put('response_x', responseNew);
        }
        else if(request instanceof AOPTMQClass.PaymentPlanCreation_element)
        {
          AOPTMQClass.PaymentPlanCreationResponse_element responseNew = new AOPTMQClass.PaymentPlanCreationResponse_element();
          //responseNew.return_x = '{"message":"Exception Checking if Payment Plan is already updated in Final Table...SR ID:2000041","status":"S"}';
          responseNew.return_x = '{"message":"Payment Plan is populated","status":"S"}';
          response.put('response_x', responseNew);
        }
        else if(request instanceof AOPTMQClass.EarlyHandoverPaymentPlanCreation_element)
        {
          AOPTMQClass.EarlyHandoverPaymentPlanCreationResponse_element responseNew = new AOPTMQClass.EarlyHandoverPaymentPlanCreationResponse_element();
          //responseNew.return_x = '{"message":"Exception Checking if Payment Plan is already updated in Final Table...SR ID:2000041","status":"S"}';
          responseNew.return_x = '{"message":"Payment Plan is populated","status":"S"}';
          response.put('response_x', responseNew);
        }
        else if(request instanceof AOPTMQClass.PaymentPlanReversal_element)
        {
          AOPTMQClass.PaymentPlanReversalResponse_element responseNew = new AOPTMQClass.PaymentPlanReversalResponse_element();
          //responseNew.return_x = '{"message":"Exception Checking if Payment Plan is already updated in Final Table...SR ID:2000041","status":"S"}';
          responseNew.return_x = '{"message":"Payment Plan is populated","status":"S"}';
          response.put('response_x', responseNew);
        }
        else if(request instanceof AssignmentProcessWSDL.getPendingDues_element){
          AssignmentProcessWSDL.getPendingDuesResponse_element responseNew
          = new AssignmentProcessWSDL.getPendingDuesResponse_element();
          responseNew.return_x = '{"Balance as per SOA":"10","FM Balance as per SOA":"10","Facility Dues":"0","Quarterly Dues":"0","Dues and Overdues":"0","status":"S"}';
          response.put('response_x', responseNew);
        }
        else if(request instanceof AOPTMQClass.DocumentAttachment_element)
        {
          AOPTMQClass.DocumentAttachmentResponse_element responseNew = new AOPTMQClass.DocumentAttachmentResponse_element();
          //responseNew.return_x = '{"message":"Exception Checking if Payment Plan is already updated in Final Table...SR ID:2000041","status":"S"}';
          //responseNew.return_x = '{"message":"Payment Plan is populated","status":"S"}';
          responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-12345--Test1.pdf Processed] PK Value#:2-2-005058","PARAM_ID":"IPMS-12345-Test1.pdf","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-12345-Test1.pdf"}],"message":"Process Completed Returning 2 Response Message(s)...","status":"S"}';
          response.put('response_x', responseNew);
        }
        else if(request instanceof AOPTMQClass.PaymentPlanReversalCurrent_element)
        {
          AOPTMQClass.PaymentPlanReversalCurrentResponse_element responseNew = new AOPTMQClass.PaymentPlanReversalCurrentResponse_element();
          responseNew.return_x = '{"message":"Payment Plan is populated","status":"S"}';
          response.put('response_x', responseNew);
        }
        else if(request instanceof AOPTMQClass.RegistrationDetails_element)
        {
          AOPTMQClass.RegistrationDetailsResponse_element responseNew = new AOPTMQClass.RegistrationDetailsResponse_element();
          responseNew.return_x = '{"X_HAND_OVER":"N","X_DISPUTE_VALUE":null,"X_PARAM_VALUE":"30-NOV-2017","X_MORTGAGE":"N","message":"Values Fetched for Registration ID : 74212","X_EARLY_HAND_OVER":null,"X_OQOOD_FLAG":"N","X_RERA_PERCENT":"95.1","status":"S"}';
          response.put('response_x', responseNew);
        }
        else if(request instanceof AOPTMQClass.PaymentPlanHistory_element)
        {
          AOPTMQClass.PaymentPlanHistoryResponse_element responseNew = new AOPTMQClass.PaymentPlanHistoryResponse_element();
          responseNew.return_x = '{"message":"Payment Plan Does not Exists for Given SR Number and RegistrationID","status":"E"}';
          response.put('response_x', responseNew);
        }
        else if(request instanceof GenerateSOAService.GenCustomerStatement_element)
        {
          GenerateSOAService.GenCustomerStatementResponse_element responseNew = new GenerateSOAService.GenCustomerStatementResponse_element();
          if( intResponseNumber == 1 ) {
              responseNew.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338397 and Request Id :40187735 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"92061","REQUEST_ID":"40187735","STAGE_ID":"338397","URL":"https://sftest.deeprootsurface.com/docs/e/40187735_92061_SOA.pdf"}';
              response.put('response_x', responseNew);
          }
          else if(intResponseNumber == 2) {
            responseNew.return_x = '';
            response.put('response_x', responseNew);
          }
        }
        else if(request instanceof MultipleDocUploadService.DocumentAttachmentMultiple_element)
        {
          MultipleDocUploadService.DocumentAttachmentMultipleResponse_element responseNew = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
          if( intResponseNumber == 1 ) {
            responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-12345--Test1.pdf Processed] PK Value#:2-2-005058","PARAM_ID":"IPMS-12345-Test1.pdf","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-12345-Test1.pdf"}],"message":"Process Completed Returning 2 Response Message(s)...","status":"S"}';
          }
          else if( intResponseNumber == 2 ) {
            responseNew.return_x = '';
          }
          if( intResponseNumber == 3 ) {
            responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-12345--Test1.pdf Processed] PK Value#:2-2-005058","PARAM_ID":"IPMS-12345-Test1.pdf","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-12345-Test1.pdf"}],"message":"Process Completed Returning 2 Response Message(s)...","status":"E"}';
          }
          response.put('response_x', responseNew);
        }
        else if( request instanceof TaskCreationWSDL.SRDataToIPMSMultiple_element ) {
            TaskCreationWSDL.SRDataToIPMSMultipleResponse_element responseNew = new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();

            responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-74428-40076796_78152_CRF.pdf Processed] PK Value#:2-2-005128","PARAM_ID":"IPMS-74428-40076796_78152_CRF.pdf","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-74428-40076796_78152_CRF.pdf"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
            if( intResponseNumber == 1 )
            {
              responseNew.return_x = '{"data":[{"PROC_STATUS":"E","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-12345--Test1.pdf Processed] PK Value#:2-2-005058","PARAM_ID":"IPMS-12345-Test1.pdf","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-12345-Test1.pdf"}],"message":"Process Completed Returning 2 Response Message(s)...","status":"E"}';
            }
            response.put('response_x', responseNew);
        }
        else if( request instanceof unitDetailsController.getUnitDetailValues_element ) {
            unitDetailsController.getUnitDetailValuesResponse_element responseNew = new unitDetailsController.getUnitDetailValuesResponse_element();
            responseNew.return_x = '["{\"ATTRIBUTE1\":\"74712\",\"ATTRIBUTE2\":\"\",\"ATTRIBUTE3\":\"LE\",\"ATTRIBUTE4\":\"N\",\"ATTRIBUTE5\":\"0\",\"ATTRIBUTE6\":\"\",\"ATTRIBUTE7\":\"\",\"ATTRIBUTE8\":\"253760\",\"ATTRIBUTE9\":\"253760\",\"ATTRIBUTE10\":\"0\",\"ATTRIBUTE11\":\"2525\",\"ATTRIBUTE12\":\"N\",\"ATTRIBUTE13\":\"Y\",\"ATTRIBUTE14\":\"N\",\"ATTRIBUTE15\":\"\",\"ATTRIBUTE16\":\"N\",\"ATTRIBUTE17\":\"N\",\"ATTRIBUTE18\":\"123745600\",\"ATTRIBUTE19\":\"0\",\"ATTRIBUTE20\":\"6.81\",\"ATTRIBUTE21\":\"1\",\"ATTRIBUTE22\":\"Y\",\"ATTRIBUTE23\":\"1523\",\"ATTRIBUTE24\":\"0\",\"ATTRIBUTE25\":\"1523\",\"ATTRIBUTE26\":\"PLATINUM\",\"ATTRIBUTE27\":\"N\",\"ATTRIBUTE28\":\"N\",\"ATTRIBUTE29\":\"OFF-PLAN\",\"ATTRIBUTE30\":\"N\",\"ATTRIBUTE31\":\"N\",\"ATTRIBUTE32\":\"\",\"ATTRIBUTE33\":\"440\",\"ATTRIBUTE34\":\"N\",\"ATTRIBUTE35\":\"51794\",\"ATTRIBUTE36\":\"\",\"ATTRIBUTE37\":\"\",\"ATTRIBUTE38\":\"\",\"ATTRIBUTE39\":\"27-MAR-2016\",\"ATTRIBUTE40\":\"AYKCB/40/4011\",\"ATTRIBUTE41\":\"253760\",\"ATTRIBUTE42\":\"0\",\"ATTRIBUTE43\":\"2525\",\"ATTRIBUTE44\":\"0\",\"ATTRIBUTE45\":\"N\",\"ATTRIBUTE46\":\"N\",\"ATTRIBUTE47\":\"N\",\"ATTRIBUTE48\":\"N\",\"ATTRIBUTE49\":\"Y\",\"ATTRIBUTE50\":\"N\",\"ATTRIBUTE51\":\"Agreement executed by DAMAC\",\"ATTRIBUTE52\":\"00\",\"ATTRIBUTE53\":\"6.81\",\"ATTRIBUTE54\":\"17-AUG-2020\",\"ATTRIBUTE55\":\"31-OCT-2021\",\"ATTRIBUTE56\":\"0\",\"ATTRIBUTE57\":\"0\",\"ATTRIBUTE58\":\"51794\",\"ATTRIBUTE59\":\"20\",\"ATTRIBUTE60\":\".2\",\"ATTRIBUTE61\":\"0\",\"ATTRIBUTE62\":\"0\",\"ATTRIBUTE63\":\"\",\"ATTRIBUTE64\":\"\",\"ATTRIBUTE65\":\"N\",\"ATTRIBUTE66\":\"N\",\"ATTRIBUTE67\":\"N\",\"ATTRIBUTE68\":\"0\",\"ATTRIBUTE69\":\"N\",\"ATTRIBUTE70\":\"\",\"ATTRIBUTE71\":\"\",\"ATTRIBUTE72\":\"1\",\"ATTRIBUTE73\":\"PARK_BAY\",\"ATTRIBUTE74\":\"\",\"ATTRIBUTE75\":\"0\",\"ATTRIBUTE76\":\"0\",\"ATTRIBUTE77\":\"\",\"ATTRIBUTE78\":\"1015040\",\"ATTRIBUTE79\":\"\",\"ATTRIBUTE80\":\"\",\"ATTRIBUTE81\":\"\",\"ATTRIBUTE82\":\"SA \u2013 Execution Pending awaiting Deposit\",\"ATTRIBUTE83\":\"Damac Canal One Property Development LLC\",\"ATTRIBUTE84\":\"N\",\"ATTRIBUTE85\":\"1600\",\"ATTRIBUTE86\":\"0\",\"ATTRIBUTE87\":\"20\",\"ATTRIBUTE88\":\"253760\",\"ATTRIBUTE89\":\"1444\",\"ATTRIBUTE90\":\"AYKCB\",\"ATTRIBUTE91\":\"AYKON CITY TOWER - B\",\"ATTRIBUTE92\":\"\",\"ATTRIBUTE93\":\"\",\"ATTRIBUTE94\":\"1074105\",\"ATTRIBUTE95\":\"N...\",\"ATTRIBUTE96\":\"793\",\"ATTRIBUTE97\":\"Normal\",\"ATTRIBUTE98\":\"27-MAR-2016\",\"ATTRIBUTE99\":\"APARTMENT\",\"ATTRIBUTE100\":\"\",\"ATTRIBUTE101\":\"Y\",\"ATTRIBUTE102\":\"1268800\",\"ATTRIBUTE103\":\"Y\",\"ATTRIBUTE104\":\"Y\",\"ATTRIBUTE105\":\"0\",\"ATTRIBUTE106\":\"27-MAR-2016\",\"ATTRIBUTE107\":\"N\",\"ATTRIBUTE108\":\"AYKON CITY\",\"ATTRIBUTE109\":\"DUBAI\",\"ATTRIBUTE110\":\"One Bedroom\",\"ATTRIBUTE111\":\"HOTEL APARTMENTS\",\"ATTRIBUTE112\":\"\",\"ATTRIBUTE113\":\"\",\"ATTRIBUTE114\":\"\",\"ATTRIBUTE115\":\"\",\"ATTRIBUTE116\":\"\",\"ATTRIBUTE117\":\"\",\"ATTRIBUTE118\":\"\",\"ATTRIBUTE119\":\"\",\"ATTRIBUTE120\":\"\"}"]';
            if( intResponseNumber == 1 ) //for aopt booking unit details
            {
            responseNew.return_x = '["{\"ATTRIBUTE1\":\"74712\",\"ATTRIBUTE2\":\"\",\"ATTRIBUTE3\":\"LE\",\"ATTRIBUTE4\":\"N\",\"ATTRIBUTE5\":\"0\",\"ATTRIBUTE6\":\"\",\"ATTRIBUTE7\":\"\",\"ATTRIBUTE8\":\"253760\",\"ATTRIBUTE9\":\"253760\",\"ATTRIBUTE10\":\"0\",\"ATTRIBUTE11\":\"2525\",\"ATTRIBUTE12\":\"N\",\"ATTRIBUTE13\":\"",\"ATTRIBUTE14\":\"N\",\"ATTRIBUTE15\":\"\",\"ATTRIBUTE16\":\"N\",\"ATTRIBUTE17\":\"N\",\"ATTRIBUTE18\":\"123745600\",\"ATTRIBUTE19\":\"0\",\"ATTRIBUTE20\":\"6.81\",\"ATTRIBUTE21\":\"1\",\"ATTRIBUTE22\":\"Y\",\"ATTRIBUTE23\":\"1523\",\"ATTRIBUTE24\":\"0\",\"ATTRIBUTE25\":\"1523\",\"ATTRIBUTE26\":\"PLATINUM\",\"ATTRIBUTE27\":\"N\",\"ATTRIBUTE28\":\"N\",\"ATTRIBUTE29\":\"OFF-PLAN\",\"ATTRIBUTE30\":\"N\",\"ATTRIBUTE31\":\"N\",\"ATTRIBUTE32\":\"\",\"ATTRIBUTE33\":\"440\",\"ATTRIBUTE34\":\"N\",\"ATTRIBUTE35\":\"51794\",\"ATTRIBUTE36\":\"\",\"ATTRIBUTE37\":\"\",\"ATTRIBUTE38\":\"\",\"ATTRIBUTE39\":\"27-MAR-2016\",\"ATTRIBUTE40\":\"AYKCB/40/4011\",\"ATTRIBUTE41\":\"253760\",\"ATTRIBUTE42\":\"0\",\"ATTRIBUTE43\":\"2525\",\"ATTRIBUTE44\":\"0\",\"ATTRIBUTE45\":\"N\",\"ATTRIBUTE46\":\"N\",\"ATTRIBUTE47\":\"N\",\"ATTRIBUTE48\":\"N\",\"ATTRIBUTE49\":\"Y\",\"ATTRIBUTE50\":\"N\",\"ATTRIBUTE51\":\"Agreement executed by DAMAC\",\"ATTRIBUTE52\":\"00\",\"ATTRIBUTE53\":\"6.81\",\"ATTRIBUTE54\":\"17-AUG-2020\",\"ATTRIBUTE55\":\"31-OCT-2021\",\"ATTRIBUTE56\":\"0\",\"ATTRIBUTE57\":\"0\",\"ATTRIBUTE58\":\"51794\",\"ATTRIBUTE59\":\"105\",\"ATTRIBUTE60\":\".2\",\"ATTRIBUTE61\":\"0\",\"ATTRIBUTE62\":\"0\",\"ATTRIBUTE63\":\"\",\"ATTRIBUTE64\":\"\",\"ATTRIBUTE65\":\"N\",\"ATTRIBUTE66\":\"N\",\"ATTRIBUTE67\":\"N\",\"ATTRIBUTE68\":\"0\",\"ATTRIBUTE69\":\"N\",\"ATTRIBUTE70\":\"\",\"ATTRIBUTE71\":\"\",\"ATTRIBUTE72\":\"1\",\"ATTRIBUTE73\":\"PARK_BAY\",\"ATTRIBUTE74\":\"\",\"ATTRIBUTE75\":\"0\",\"ATTRIBUTE76\":\"0\",\"ATTRIBUTE77\":\"\",\"ATTRIBUTE78\":\"1015040\",\"ATTRIBUTE79\":\"\",\"ATTRIBUTE80\":\"\",\"ATTRIBUTE81\":\"\",\"ATTRIBUTE82\":\"SA \u2013 Execution Pending awaiting Deposit\",\"ATTRIBUTE83\":\"Damac Canal One Property Development LLC\",\"ATTRIBUTE84\":\"N\",\"ATTRIBUTE85\":\"1600\",\"ATTRIBUTE86\":\"0\",\"ATTRIBUTE87\":\"20\",\"ATTRIBUTE88\":\"253760\",\"ATTRIBUTE89\":\"1444\",\"ATTRIBUTE90\":\"AYKCB\",\"ATTRIBUTE91\":\"AYKON CITY TOWER - B\",\"ATTRIBUTE92\":\"\",\"ATTRIBUTE93\":\"\",\"ATTRIBUTE94\":\"1074105\",\"ATTRIBUTE95\":\"N...\",\"ATTRIBUTE96\":\"793\",\"ATTRIBUTE97\":\"Normal\",\"ATTRIBUTE98\":\"27-MAR-2016\",\"ATTRIBUTE99\":\"APARTMENT\",\"ATTRIBUTE100\":\"\",\"ATTRIBUTE101\":\"Y\",\"ATTRIBUTE102\":\"1268800\",\"ATTRIBUTE103\":\"Y\",\"ATTRIBUTE104\":\"Y\",\"ATTRIBUTE105\":\"0\",\"ATTRIBUTE106\":\"27-MAR-2016\",\"ATTRIBUTE107\":\"N\",\"ATTRIBUTE108\":\"AYKON CITY\",\"ATTRIBUTE109\":\"DUBAI\",\"ATTRIBUTE110\":\"One Bedroom\",\"ATTRIBUTE111\":\"HOTEL APARTMENTS\",\"ATTRIBUTE112\":\"\",\"ATTRIBUTE113\":\"\",\"ATTRIBUTE114\":\"\",\"ATTRIBUTE115\":\"\",\"ATTRIBUTE116\":\"\",\"ATTRIBUTE117\":\"\",\"ATTRIBUTE118\":\"\",\"ATTRIBUTE119\":\"\",\"ATTRIBUTE120\":\"\"}"]';
            }
            /*else if( intResponseNumber == 2 ) //for penalty waiver booking details
            {
              responseNew.return_x = '["{\"ATTRIBUTE1\":\"78152\",\"ATTRIBUTE2\":\"\",\"ATTRIBUTE3\":\"LE\",\"ATTRIBUTE4\":\"N\",\"ATTRIBUTE5\":\"0\",\"ATTRIBUTE6\":\"\",\"ATTRIBUTE7\":\"\",\"ATTRIBUTE8\":\"253760\",\"ATTRIBUTE9\":\"253760\",\"ATTRIBUTE10\":\"0\",\"ATTRIBUTE11\":\"2525\",\"ATTRIBUTE12\":\"N\",\"ATTRIBUTE13\":\"Y\",\"ATTRIBUTE14\":\"N\",\"ATTRIBUTE15\":\"\",\"ATTRIBUTE16\":\"N\",\"ATTRIBUTE17\":\"N\",\"ATTRIBUTE18\":\"123745600\",\"ATTRIBUTE19\":\"0\",\"ATTRIBUTE20\":\"6.81\",\"ATTRIBUTE21\":\"1\",\"ATTRIBUTE22\":\"Y\",\"ATTRIBUTE23\":\"1523\",\"ATTRIBUTE24\":\"0\",\"ATTRIBUTE25\":\"1523\",\"ATTRIBUTE26\":\"PLATINUM\",\"ATTRIBUTE27\":\"N\",\"ATTRIBUTE28\":\"N\",\"ATTRIBUTE29\":\"OFF-PLAN\",\"ATTRIBUTE30\":\"N\",\"ATTRIBUTE31\":\"N\",\"ATTRIBUTE32\":\"\",\"ATTRIBUTE33\":\"440\",\"ATTRIBUTE34\":\"N\",\"ATTRIBUTE35\":\"51794\",\"ATTRIBUTE36\":\"\",\"ATTRIBUTE37\":\"\",\"ATTRIBUTE38\":\"\",\"ATTRIBUTE39\":\"27-MAR-2016\",\"ATTRIBUTE40\":\"AYKCB/40/4011\",\"ATTRIBUTE41\":\"253760\",\"ATTRIBUTE42\":\"0\",\"ATTRIBUTE43\":\"2525\",\"ATTRIBUTE44\":\"0\",\"ATTRIBUTE45\":\"N\",\"ATTRIBUTE46\":\"N\",\"ATTRIBUTE47\":\"N\",\"ATTRIBUTE48\":\"N\",\"ATTRIBUTE49\":\"Y\",\"ATTRIBUTE50\":\"N\",\"ATTRIBUTE51\":\"Agreement executed by DAMAC\",\"ATTRIBUTE52\":\"00\",\"ATTRIBUTE53\":\"6.81\",\"ATTRIBUTE54\":\"17-AUG-2020\",\"ATTRIBUTE55\":\"31-OCT-2021\",\"ATTRIBUTE56\":\"0\",\"ATTRIBUTE57\":\"0\",\"ATTRIBUTE58\":\"51794\",\"ATTRIBUTE59\":\"20\",\"ATTRIBUTE60\":\".2\",\"ATTRIBUTE61\":\"0\",\"ATTRIBUTE62\":\"0\",\"ATTRIBUTE63\":\"\",\"ATTRIBUTE64\":\"\",\"ATTRIBUTE65\":\"N\",\"ATTRIBUTE66\":\"N\",\"ATTRIBUTE67\":\"N\",\"ATTRIBUTE68\":\"0\",\"ATTRIBUTE69\":\"N\",\"ATTRIBUTE70\":\"\",\"ATTRIBUTE71\":\"\",\"ATTRIBUTE72\":\"1\",\"ATTRIBUTE73\":\"PARK_BAY\",\"ATTRIBUTE74\":\"\",\"ATTRIBUTE75\":\"0\",\"ATTRIBUTE76\":\"0\",\"ATTRIBUTE77\":\"\",\"ATTRIBUTE78\":\"1015040\",\"ATTRIBUTE79\":\"\",\"ATTRIBUTE80\":\"\",\"ATTRIBUTE81\":\"\",\"ATTRIBUTE82\":\"SA \u2013 Execution Pending awaiting Deposit\",\"ATTRIBUTE83\":\"Damac Canal One Property Development LLC\",\"ATTRIBUTE84\":\"N\",\"ATTRIBUTE85\":\"1600\",\"ATTRIBUTE86\":\"0\",\"ATTRIBUTE87\":\"20\",\"ATTRIBUTE88\":\"253760\",\"ATTRIBUTE89\":\"1444\",\"ATTRIBUTE90\":\"AYKCB\",\"ATTRIBUTE91\":\"AYKON CITY TOWER - B\",\"ATTRIBUTE92\":\"\",\"ATTRIBUTE93\":\"\",\"ATTRIBUTE94\":\"1074105\",\"ATTRIBUTE95\":\"N...\",\"ATTRIBUTE96\":\"793\",\"ATTRIBUTE97\":\"Normal\",\"ATTRIBUTE98\":\"27-MAR-2016\",\"ATTRIBUTE99\":\"APARTMENT\",\"ATTRIBUTE100\":\"\",\"ATTRIBUTE101\":\"Y\",\"ATTRIBUTE102\":\"1268800\",\"ATTRIBUTE103\":\"Y\",\"ATTRIBUTE104\":\"Y\",\"ATTRIBUTE105\":\"0\",\"ATTRIBUTE106\":\"27-MAR-2016\",\"ATTRIBUTE107\":\"N\",\"ATTRIBUTE108\":\"AYKON CITY\",\"ATTRIBUTE109\":\"DUBAI\",\"ATTRIBUTE110\":\"One Bedroom\",\"ATTRIBUTE111\":\"HOTEL APARTMENTS\",\"ATTRIBUTE112\":\"\",\"ATTRIBUTE113\":\"\",\"ATTRIBUTE114\":\"\",\"ATTRIBUTE115\":\"\",\"ATTRIBUTE116\":\"\",\"ATTRIBUTE117\":\"\",\"ATTRIBUTE118\":\"\",\"ATTRIBUTE119\":\"\",\"ATTRIBUTE120\":\"\"}"]';
            }
            */
            response.put('response_x', responseNew);
        }
        else if( request instanceof actionComUpdated.PenaltyWaiverDetails_element ) {
            actionComUpdated.PenaltyWaiverDetailsResponse_element responseNew = new actionComUpdated.PenaltyWaiverDetailsResponse_element();
            if( intResponseNumber == 1 ) {
                responseNew.return_x = '{"allowed":"Yes","message":null,"recommendingAuthorityOne":"CRM-Manager","recommendingAuthorityTwo":"CRM-Director","recommendingAuthorityThree":"CRM-Director","recommendingAuthorityFour":"CRM-Director","approvingAuthorityOne":"CRM-HOD","approvingAuthorityTwo":"CRM-Director","approvingAuthorityThree":"CRM-Director","adminFeePsf":null,"adminFeeFlat":null,"adminFeePercentage":null}';
            }
            else if( intResponseNumber == 2 ) {
                responseNew.return_x = '{"allowed":"Yes","message":null,"recommendingAuthorityOne":null,"recommendingAuthorityTwo":null,"recommendingAuthorityThree":null,"recommendingAuthorityFour":null,"approvingAuthorityOne":null,"approvingAuthorityTwo":null,"approvingAuthorityThree":null,"adminFeePsf":null,"adminFeeFlat":null,"adminFeePercentage":null}';
            }
            else if( intResponseNumber == 3 ) {
                responseNew.return_x = '{"allowed":"No","message":"Penalty Waiver not allowed","recommendingAuthorityOne":null,"recommendingAuthorityTwo":null,"recommendingAuthorityThree":null,"recommendingAuthorityFour":null,"approvingAuthorityOne":null,"approvingAuthorityTwo":null,"approvingAuthorityThree":null,"adminFeePsf":null,"adminFeeFlat":null,"adminFeePercentage":null}';
            }
            response.put('response_x', responseNew);
        }
        else if(request instanceof Refunds.getExcessAmount_element) {
            Refunds.getExcessAmountResponse_element responseNew = new Refunds.getExcessAmountResponse_element();
            responseNew.return_x = '{"Status":"S","PROC_STATUS":null,"PROC_MESSAGE":null,"Message":"Process Completed Returning 0 Response Message(s)...","PARAM_ID":"81965","Excess_Amount":"1000"}';
            response.put('response_x', responseNew);
        }
        else if(request instanceof AOPTDocumentGeneration.DocGeneration_element) {
            AOPTDocumentGeneration.DocGenerationResponse_element responseNew = new AOPTDocumentGeneration.DocGenerationResponse_element();
            responseNew.return_x = 'https://sftest.deeprootsurface.com/docs/t/IPMS-1039780-Offer%20&%20Acceptance%20Letter-signed.pdf';
            response.put('response_x', responseNew);
        }
        else if(request instanceof RefundsRule.TokenRefundsTransfers_element) {
            RefundsRule.TokenRefundsTransfersResponse_element responseNew = new RefundsRule.TokenRefundsTransfersResponse_element();
            if(intResponseNumber == 1) {
                responseNew.return_x = '{"allowed":"Yes","message":null,"recommendingAuthorityOne":"CRM-Manager","recommendingAuthorityTwo":null,"recommendingAuthorityThree":null,"recommendingAuthorityFour":null,"approvingAuthorityOne":"CRM-Director","approvingAuthorityTwo":null,"approvingAuthorityThree":null,"percToBeRefundedTransferred":null,"deductionFeePsf":null,"deductionFeeFlat":null,"deductionFeePercentage":null}';
            response.put('response_x', responseNew);
            }
            else if(intResponseNumber == 2) {
            responseNew.return_x = '{"allowed":"No","message":null,"recommendingAuthorityOne":"CRM-Manager","recommendingAuthorityTwo":null,"recommendingAuthorityThree":null,"recommendingAuthorityFour":null,"approvingAuthorityOne":"CRM-Director","approvingAuthorityTwo":null,"approvingAuthorityThree":null,"percToBeRefundedTransferred":null,"deductionFeePsf":null,"deductionFeeFlat":null,"deductionFeePercentage":null}';
            response.put('response_x', responseNew);
            }
            else {
                response.put('response_x', null);
            }
        }
        else if(request instanceof GenerateCRFService.GetCustomerRequestForm_element) {
            GenerateCRFService.GetCustomerRequestFormResponse_element responseNew = new GenerateCRFService.GetCustomerRequestFormResponse_element();
            if(intResponseNumber == 1) {
                responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=340080 and Request Id :40211238 ...","ATTRIBUTE3":"340080","ATTRIBUTE2":"40211238","ATTRIBUTE1":"https://sftest.deeprootsurface.com/docs/e/40211238_81965_CRF.pdf","PARAM_ID":"81965"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
                response.put('response_x', responseNew);
            }
            else if(intResponseNumber == 2) {
                responseNew.return_x = '';
                response.put('response_x', responseNew);
            }

        }
        else if(request instanceof GenerateSOAService.GenCustomerStatement_element) {
            GenerateSOAService.GenCustomerStatementResponse_element responseNew = new GenerateSOAService.GenCustomerStatementResponse_element();
            if(intResponseNumber == 1) {
                responseNew.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=340083 and Request Id :40217924 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"81965","REQUEST_ID":"40217924","STAGE_ID":"340083","URL":"https://sftest.deeprootsurface.com/docs/e/40217924_81965_SOA.pdf"}';
                response.put('response_x', responseNew);
            }
            else if(intResponseNumber == 2) {
                responseNew.return_x = '';
                response.put('response_x', responseNew);
            }

        }
        else if( request instanceof actionComUpdated.UpdatePenaltyWaived_element ) {
            actionComUpdated.UpdatePenaltyWaivedResponse_element responseNew = new actionComUpdated.UpdatePenaltyWaivedResponse_element();
            responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"Waiver amount 100 exceeds total due amount 14401","PARAM_ID":"74428","Transaction_Number":null}],"message":"[WARNING] Check the PROC_MESSAGE attribute for Actual Error Message(s), Error Message Count = 1","status":"S"}';
            response.put('response_x', responseNew);
        }
   }
}