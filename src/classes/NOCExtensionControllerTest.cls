@isTest
public class NOCExtensionControllerTest{
    public static testMethod void testOne(){
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);
        
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        list<Booking__c> listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(null, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        insert listCreateBookingUnit;

        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        insert objCase;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        PageReference pageRef = Page.NOC_Extension;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        NOCExtensionController objClass = new NOCExtensionController(sc);
        objClass.extendNOC();
        objClass.Confirm();
        objClass.Cancel();
        Test.stopTest();
    }
    
    public static testMethod void testTwo(){
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);
        
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        list<Booking__c> listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(null, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        insert listCreateBookingUnit;

        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        insert objCase;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(4));
        PageReference pageRef = Page.NOC_Extension;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        NOCExtensionController objClass = new NOCExtensionController(sc);
        objClass.extendNOC();
        objClass.Confirm();
        Test.stopTest();
    }
    
    public static testMethod void testThree(){
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);
        
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        list<Booking__c> listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(null, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        insert listCreateBookingUnit;

        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        insert objCase;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(5));
        PageReference pageRef = Page.NOC_Extension;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        NOCExtensionController objClass = new NOCExtensionController(sc);
        objClass.extendNOC();
        objClass.Confirm();
        Test.stopTest();
    }
    
    public static testMethod void testFour(){
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);
        
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        list<Booking__c> listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(null, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        insert listCreateBookingUnit;

        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        insert objCase;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(3));
        PageReference pageRef = Page.NOC_Extension;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        NOCExtensionController objClass = new NOCExtensionController(sc);
        objClass.extendNOC();
        objClass.Confirm();
        Test.stopTest();
    }
    
    public static testMethod void testAssignmentPostingSuccess(){
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);
        
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        list<Booking__c> listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(null, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        insert listCreateBookingUnit;

        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        insert objCase;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(4,2));
        PageReference pageRef = Page.NOC_Extension;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        NOCExtensionController objClass = new NOCExtensionController(sc);
        objClass.extendNOC();
        objClass.Confirm();
        Test.stopTest();
    }
    
    public static testMethod void testAssignmentPostingFailure(){
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);
        
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        list<Booking__c> listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(null, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        insert listCreateBookingUnit;

        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        insert objCase;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(4,1));
        PageReference pageRef = Page.NOC_Extension;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        NOCExtensionController objClass = new NOCExtensionController(sc);
        objClass.extendNOC();
        objClass.Confirm();
        Test.stopTest();
    }
}