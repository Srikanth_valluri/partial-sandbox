public class CreateHandoverCases{//AKISHOR

    public string BUid {get;set;}
        public CreateHandoverCases(ApexPages.StandardController controller) { BUId = apexpages.currentpage().getparameters().get('id');}
        
          
        public pagereference CreateCase(){  
        
        
         
            List<Booking_Unit__c> BULst= [SELECT ID,Account_Id__c,Registration_Status_Code__c FROM Booking_Unit__c WHERE ID =:BUId limit 1];
            system.debug('\n--BUlst--'+BULst);
            Id HOCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
            Id HOParent=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover Parent').getRecordTypeId();
            if(!BULst.isEmpty()){           
                if(BULst[0].Registration_Status_Code__c=='LE') {
                     Case sCase1=new Case();
                     sCase1.RecordtypeId = HOParent;//'0122500000019fx';
                     sCase1.Booking_Unit__c = BUId;
                     sCase1.AccountId=BULst[0].Account_Id__c; 
                     insert sCase1;
                     System.debug('sCase1 == ' + sCase1);
                     Case sCase = new Case();
                     sCase.RecordtypeId=HOCase;//'0122500000018qf';
                     sCase.Booking_Unit__c= BUId;
                     sCase.AccountId=BULst[0].Account_Id__c; 
                     sCase.ParentId=sCase1.Id;     
                     system.debug('\n--lstCase--'+sCase);
                     insert sCase;
                     System.debug('sCase== ' + [select ownerId from case where id =: sCase.Id]);
                    
                    try {
                        Task objTask = new Task();
                        objTask.OwnerId = [select ownerId from case where id =: sCase.Id].ownerId; //sCase.ownerId;
                        objTask.WhatId = sCase.id;
                        objTask.Assigned_user__c = 'CRE';
                        objTask.Subject = 'Generate and Upload Handover Checklist';
                        //objTask.WhoId = sCase.AccountId;
                        objTask.Status = 'Not Started';
                        objTask.Priority ='High';
                        objTask.Process_Name__c = 'Handover';
                        objTask.ActivityDate = system.today().addDays(3);
                        System.debug('ObjTask 1 == ' + objTask);
                        insert objTask;
                        System.debug('ObjTask 2 == ' + objTask);
                    }
                    catch(Exception ex) {
                        /*ApexPages.addmessage(
                            new ApexPages.message(
                                ApexPages.severity.Error
                                , ex.getMessage())
                        );*/
                        Error_Log__c objError = new Error_Log__c();
                        objError.Error_Details__c = ex.getMessage();
                        objError.Booking_Unit__c = BULst[0].Id;
                    }
                    
                    Id HOCList=Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Handover Calling List').getRecordTypeId();              
                     Calling_List__c cList=new Calling_List__c();
                     cList.RecordtypeId=HOCList;//'012250000000x0D';
                     cList.Calling_List_Status__c='New';
                     cList.Booking_Unit__c=BUId;
                     cList.Account__c=BULst[0].Account_Id__c;
                     cList.Case__c=sCase1.Id;
                     insert cList;
                     pagereference page = new Pagereference('/'+BUId);
                      page.setRedirect(true);
                     return page;                 
                     }                       
            }
            return null;             
        }
    }