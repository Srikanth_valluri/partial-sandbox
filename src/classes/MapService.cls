Global Class MapService{
    //List<MapWrapper> mapWrapperLst;
    
    public MapService(DAMAC_Project_Details controller) {

    }
    public MapService(Damac_Project_Details_Mobile controller) {

    }
    public MapService(AP_ProjectDetailsController controller) {

    }
    
    
    @RemoteAction
    global static List<MapWrapper> callMap(String key,Double lat,Double lng) {
        MapServiceJsonToApex responseObject;
        String fourSquareUrl = System.label.FourSquareMapUrl;
        fourSquareUrl= fourSquareUrl+'&ll='+lat+','+lng;
        fourSquareUrl= fourSquareUrl+'&query='+key;
        fourSquareUrl= fourSquareUrl+'&v=20180201';
        fourSquareUrl= fourSquareUrl+'&limit=50';
        System.debug('@@foursquareurl@@' + fourSquareUrl);
        List<MapWrapper> mapWrapperLst = new List<MapWrapper>();
        System.debug('Remote called');
        System.debug('Latitude'+ lat);
        System.debug('longitude'+ lng);
        System.debug('Key '+key );
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        //request.setEndpoint('https://api.foursquare.com/v2/venues/explore?ll=18.55,73.80&query=ATM&client_id=BCB4QGOMJSI5LRLIXAK2ZDKK3A4IRMDNXU33T4H4WZH02QZJ&client_secret=VZBBBDX5GNLA23ZFF2F3MXV4DXKA32HNAZGP0TYDJ24NFIMA&v=20180201');
        request.setEndpoint(fourSquareUrl);
        //request.setEndpoint('https://api.foursquare.com/v2/venues/explore?client_id=BCB4QGOMJSI5LRLIXAK2ZDKK3A4IRMDNXU33T4H4WZH02QZJ&client_secret=VZBBBDX5GNLA23ZFF2F3MXV4DXKA32HNAZGP0TYDJ24NFIMA&ll=18.55,73.8&query=Hotel&v=20180201');
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        if (response.getStatusCode() == 200) {
            System.debug('Response' + response.getBody());
            responseObject = (MapServiceJsonToApex)JSON.deserialize(response.getBody(),MapServiceJsonToApex.class);
            System.debug('@@@@Serialized json object@@@@' + responseObject);
            System.debug('@@Responsenodegroups@@@@'+ responseObject.response.groups);
            List<MapServiceJsonToApex.Items> items =responseObject.response.groups[0].items;
            for(MapServiceJsonToApex.Items obj: items){
                System.debug('@@obj@@'+obj);
                System.debug('@@lat@@'+obj.venue.name);
                System.debug('@@lat@@'+obj.venue.location.lat);
                System.debug('@@longitude@@'+obj.venue.location.lng);
                System.debug('@@formated address@@'+obj.venue.location.formattedAddress);
                System.debug('@@phone@@'+obj.venue);
                if(obj.venue.hours != null){
                   MapWrapper wrapperObj= new MapWrapper(obj.venue.location.lat,obj.venue.location.lng,obj.venue.name,obj.venue.location.formattedAddress,obj.venue.contact.phone,obj.venue.hours.status);     
                   mapWrapperLst.add(wrapperObj);
                }else {
                   MapWrapper wrapperObj= new MapWrapper(obj.venue.location.lat,obj.venue.location.lng,obj.venue.name,obj.venue.location.formattedAddress,obj.venue.contact.phone);
                   mapWrapperLst.add(wrapperObj);
                }
                
                
            }
            return mapWrapperLst;
            
        }else{
            System.debug('Error in the seervice from foursquaredeveloper');
            return null;
        }
        
        
        //return null;
        
    }

}