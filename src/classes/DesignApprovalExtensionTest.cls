/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class DesignApprovalExtensionTest {
	
	@testSetup
    static void createSetupDate() {
        insert TestDataFactory_CRM.createPersonAccount();
    }
    
    static testMethod void myUnitTest() {
		List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,IsPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        lstBookingUnits[0].Registration_ID__c = '74712';
        lstBookingUnits[0].Unit_Name__c = 'BUTest';
        lstBookingUnits[0].Unit_type__c = 'PLOT';
        lstBookingUnits[0].Handover_Flag__c  = 'N';     
        insert lstBookingUnits;
		
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Plot NOC').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);        
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.POA_Expiry_Date__c = System.Today();
        
        
        SR_Attachments__c pccDoc = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'Building Design Plan');
        
        list<SR_Attachments__c> lstAttach = new list<SR_Attachments__c>();
        Test.startTest();
        	insert objCase;
        	insert pccDoc;
        	lstAttach.add(pccDoc);
        	PageReference pageRef = Page.PlotHandoverProcessPage;
        	Test.setCurrentPage(pageRef);
        	pageRef.getparameters().put('id', objCase.id);  
        	pageRef.getparameters().put('pmtUser', 'yes');
			
			Apexpages.StandardController sc = new Apexpages.StandardController(objCase);
        	DesignApprovalExtension ext = new  DesignApprovalExtension(sc);
        	ext.lstSRAttachments = lstAttach;
        	ext.loadSRAttachments();
        	ext.submitApproval();
        	ext.rejectApproval();
        Test.stopTest();
    }
    
    static testMethod void myUnitTest2() {
		List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,IsPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        lstBookingUnits[0].Registration_ID__c = '74712';
        lstBookingUnits[0].Unit_Name__c = 'BUTest';
        lstBookingUnits[0].Unit_type__c = 'PLOT';
        lstBookingUnits[0].Handover_Flag__c  = 'N';     
        insert lstBookingUnits;
		
        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Plot NOC').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);        
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.POA_Expiry_Date__c = System.Today();
        
        
        SR_Attachments__c pccDoc = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'Building Design Plan');
        
        list<SR_Attachments__c> lstAttach = new list<SR_Attachments__c>();
        Test.startTest();
        	insert objCase;
        	insert pccDoc;
        	lstAttach.add(pccDoc);
        	PageReference pageRef = Page.PlotHandoverProcessPage;
        	Test.setCurrentPage(pageRef);
        	pageRef.getparameters().put('id', objCase.id);  
        	pageRef.getparameters().put('pmtUser', 'no');
			
			Apexpages.StandardController sc = new Apexpages.StandardController(objCase);
        	DesignApprovalExtension ext = new  DesignApprovalExtension(sc);
        	ext.lstSRAttachments = lstAttach;
        	ext.loadSRAttachments();
        	ext.submitApproval();
        	ext.rejectApproval();
        Test.stopTest();
    }
}