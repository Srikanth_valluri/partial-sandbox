@RestResource(urlMapping='/srCase/owner/contact')
global class SR_OwnerContactRESTresource {
    public static Map<Integer, String> statusCodeMap;
    static {
        statusCodeMap = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong',
        7 => 'Draft Exists',
        8 => 'Info',
        9 => 'SR Exist'};
    }
    
    @HTTPPost
    global static void doPost(Boolean is_draft, String account_id, String draft_sr_id, String country, 
    String state, String city, String address, String postal_code, String primary_email_id, 
    String country_code, String primary_mobile_number, String cocd_form_url, String additional_doc_file_url) {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg, successMsg;
        
        if(String.isBlank(account_id)){
            errorMsg = 'Owner Account ID for the service request is not provided.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
            return;
        }
        
        if(!is_draft && (String.isBlank(country) || String.isBlank(city) || String.isBlank(address) || 
           String.isBlank(primary_email_id) || String.isBlank(country_code) || 
           String.isBlank(primary_mobile_number))){
            errorMsg = 'Please provide value for all the mandatoty fields.';
            getErrorResponse(8, statusCodeMap.get(8), errorMsg, errorMsg);
            return;
        }
        
        Account ownerAccount;
        try{
            ownerAccount = [SELECT id, Name, Name_Arabic__c, IsPersonAccount, Party_ID__c, Party_Type__c, 
                 Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c, Primary_Language__c, 
                 Country__c, Country_Arabic__c, Address_Line_1__c, Address_Line_1_Arabic__c, 
                 Address_Line_2__c, Address_Line_2_Arabic__c, Address_Line_3__c, Address_Line_3_Arabic__c, 
                 Address_Line_4__c, Address_Line_4_Arabic__c, City__c, City_Arabic__c, State__c, 
                 State_Arabic__c, Postal_Code_Arabic__c, Email__c, Mobile_Country_Code__c, 
                 Mobile_Phone_Encrypt__c, Country__pc, Country_Arabic__pc, Address_Line_1__pc, 
                 Address_Line_2__pc, Address_Line_3__pc, Address_Line_4__pc, Address_Line_1_Arabic__pc, 
                 Address_Line_2_Arabic__pc, Address_Line_3_Arabic__pc, Address_Line_4_Arabic__pc, City__pc, 
                 City_Arabic__pc, BillingPostalCode, Email__pc, Mobile_Country_Code__pc, Mobile_Phone_Encrypt__pc 
                 FROM Account WHERE Id = :account_id];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            errorMsg = 'The Owner Account ID provided does not match any records.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, ex.getMessage());
            return;
        }
        
        if(NULL == ownerAccount){
            errorMsg = 'The Owner Account ID provided does not match any records.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
            return;
        }
        
       if(!is_draft && String.isBlank(cocd_form_url)){
            errorMsg = 'Please provide \'Change of contact detail form\' URL to submit request.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
            return;
        }
        
        Set<String> allowedStatusSet = new Set<String>{'Closed','Cancelled','Rejected'};
        Set<String> set_SR_RecTypes = 
            new Set<String>{'Passport_Detail_Update','Change_of_Details','Name_Nationality_Change','Change_of_Joint_Buyer'};
        Case currentSR_COCD;
        Boolean currSR_isNew = false;
        List<Case> srRequests_existing = [SELECT id, caseNumber, Type, SR_Type__c, Origin, isHelloDamacAppCase__c, status, 
                        RecordTypeId, RecordType.Name, RecordType.DeveloperName, AccountId, Account.Name, 
                        Is_Primary_Customer_Updated__c, Address__c, Address_Arabic_1__c, 
                        Address_2__c, Address_Arabic_2__c, Address_3__c, Address_Arabic_3__c, Address_4__c, 
                        Address_Arabic_4__c, City__c, City_Arabic__c, State__c, State_Arabic__c, 
                        Postal_Code_Arabic__c, Country__c, Country_Arabic__c, Contact_Email__c, Postal_Code__c, 
                        Mobile_Country_Code__c, Contact_Mobile__c, Home_Phone_Country_Code__c, Home_Phone__c, 
                        Work_Phone_Country_Code__c, Work_Phone__c, OCR_verified__c, CRF_File_URL__c, 
                        Additional_Doc_File_URL__c, OD_File_URL__c 
                        FROM Case 
                        WHERE AccountId = :account_id 
                        AND status NOT IN :allowedStatusSet 
                        AND RecordType.DeveloperName IN :set_SR_RecTypes];
        /* AND RecordType.Name = 'Change of Details' AND SR_Type__c = 'Change of Contact Details' AND status = 'Draft Request' */
        if(NULL != srRequests_existing && srRequests_existing.size() > 0 ){
            for(Case draftSR : srRequests_existing){
                String draftSR_ID = String.valueOf(draftSR.id);
                if(String.isNotBlank(draft_sr_id) && draftSR_ID.equals(draft_sr_id) && 
                   draftSR.RecordType.Name == 'Change of Details' && 
                   draftSR.SR_Type__c == 'Change of Contact Details' && 
                   draftSR.status == 'Draft Request'){
                    currentSR_COCD = draftSR;
                    // break;
                }
                else {
                    errorMsg = 'Another Service Request already exist for this account.';
                    Integer statCod = draftSR.status.equals('Draft Request') ? 7 : 9;
                    getErrorResponse(statCod, statusCodeMap.get(statCod), errorMsg, errorMsg);
                    return;
                }
            }
            
            /* Case when provided `draft_sr_id` does not match any records present */
            if(NULL == currentSR_COCD){
                if(String.isNotBlank(draft_sr_id)){
                    errorMsg = 'Provided Draft SR ID is invalid.';
                    system.debug(errorMsg);
                    getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
                    return;
                }
                else {
                    errorMsg = 'Another Service Request already exist for this account.';
                    getErrorResponse(9, statusCodeMap.get(9), errorMsg, errorMsg);
                    return;
                }
            }
        } /* if(NULL != srRequests_existing && srRequests_existing.size() > 0 ) */
        else {
            /* Case when no previous draft request SR cases are present. */
            if(String.isNotBlank(draft_sr_id)){
                errorMsg = 'Provided Draft SR ID is invalid.';
                getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
                return;
            }
            
            currSR_isNew = true;
            currentSR_COCD = new Case();
            currentSR_COCD.AccountId = account_id;
            currentSR_COCD.recordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get(
                                'Change of Details').getRecordTypeId();
            currentSR_COCD.type = 'Change of Contact Details';
            currentSR_COCD.SR_Type__c = 'Change of Contact Details';
            currentSR_COCD.Origin = 'Portal'; /* Mobile App */
            currentSR_COCD.isHelloDamacAppCase__c = true;
        }
        
        currentSR_COCD.status = is_draft ? 'Draft Request' : 'Submitted';
        
        Boolean isPersonAccount = ownerAccount.IsPersonAccount;
        currentSR_COCD.Mobile_Country_Code__c = country_code;
        currentSR_COCD.Contact_Mobile__c = primary_mobile_number;
        currentSR_COCD.Contact_Email__c = primary_email_id;
        currentSR_COCD.Country__c = country;
        currentSR_COCD.Address__c = address;
        currentSR_COCD.City__c = city;
        if(String.isNotBlank(state))
            currentSR_COCD.State__c = state;
        if(String.isNotBlank(postal_code))
            currentSR_COCD.Postal_Code__c = postal_code;
        if(String.isNotBlank(cocd_form_url))
            currentSR_COCD.CRF_File_URL__c = cocd_form_url;
        if(String.isNotBlank(additional_doc_file_url))
            currentSR_COCD.Additional_Doc_File_URL__c = additional_doc_file_url;
        
        setCaseOwner(ownerAccount, currentSR_COCD); /* set case owner from account */
        
        try{
            if(currSR_isNew) insert currentSR_COCD;
            else update currentSR_COCD;
        }
        catch(Exception ex){
            System.debug(ex.getMessage());
            errorMsg = 'Failed to update SR Case.';
            system.debug(errorMsg);
            getErrorResponse(2, statusCodeMap.get(2), errorMsg, ex.getMessage());
        }
        
        Case updatedSR_case; /* to fetch updated caseNumber, owner, and status ..etc */
        try{
            updatedSR_case = [SELECT id, caseNumber, Type, SR_Type__c, Origin, isHelloDamacAppCase__c, 
                        RecordTypeId, RecordType.Name, status, AccountId, Account.Name, 
                        Is_Primary_Customer_Updated__c, Address__c, Address_Arabic_1__c, 
                        Address_2__c, Address_Arabic_2__c, Address_3__c, Address_Arabic_3__c, Address_4__c, 
                        Address_Arabic_4__c, City__c, City_Arabic__c, State__c, State_Arabic__c, 
                        Postal_Code_Arabic__c, Country__c, Country_Arabic__c, Contact_Email__c, Postal_Code__c, 
                        Mobile_Country_Code__c, Contact_Mobile__c, Home_Phone_Country_Code__c, Home_Phone__c, 
                        Work_Phone_Country_Code__c, Work_Phone__c, OCR_verified__c, CRF_File_URL__c, 
                        Additional_Doc_File_URL__c, OD_File_URL__c 
                        FROM Case WHERE id = :currentSR_COCD.id];
        }
        catch(Exception ex){
            System.debug(ex.getMessage());
            successMsg = 'Succesfully updated SR request for Contact Detail Change.';
            getSuccessResponse(ownerAccount, currentSR_COCD, successMsg);
        }
        
        successMsg = 'Succesfully updated SR request for Contact Detail Change.';
        if(NULL != updatedSR_case)
            getSuccessResponse(ownerAccount, updatedSR_case, successMsg);
        else
            getSuccessResponse(ownerAccount, currentSR_COCD, successMsg);
    }
    
    @HttpGet
    global static void doGet() {
        SR_contextWrap currentSRContext = getContextSR();
        if(NULL == currentSRContext) {
            return; /* error response body already built in getContextSR() */
        }
        
        String message = '';
        if(NULL == currentSRContext.currentSR_Owner || NULL == currentSRContext.currentSR_Case) {
            message = 'Error while fetching Draft SR Case record.';
            getErrorResponse(6, statusCodeMap.get(6), message, NULL);
        }
        else {
            message = 'Succesfully fetched SR Case for Owner Contact Detail Change.';
            getSuccessResponse(currentSRContext.currentSR_Owner, currentSRContext.currentSR_Case, message);
        }
    }
    
    @HttpDelete
    global static void doDeleteDraft() {
        SR_contextWrap currentSRContext = getContextSR();
        if(NULL == currentSRContext) {
            return; /* error response body already built in getContextSR() */
        }
        
        String message = '';
        if(NULL == currentSRContext.currentSR_Owner || NULL == currentSRContext.currentSR_Case) {
            message = 'Error while fetching Draft SR Case record.';
            getErrorResponse(6, statusCodeMap.get(6), message, NULL);
            return;
        }
        
        Case currentSR_passport = currentSRContext.currentSR_Case;
        currentSR_passport.Status = 'Cancelled';
        
        try{
            update currentSR_passport;
            /* delete currentSR_passport; */
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            message = 'Error while trying to cancel the Draft SR request';
            getErrorResponse(2, statusCodeMap.get(2), message, (ex.getMessage() + ' : ' + ex.getStackTraceString()));
            return;
        }
        
        message = 'Succesfully cancelled the draft SR Case for Owner Contact Detail Change.';
        getSuccessResponse(currentSRContext.currentSR_Owner, currentSR_passport, message);
    }
    
    private static SR_contextWrap getContextSR() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg, successMsg;
        
        String caseNum = req.params.containskey('case_number') ? req.params.get('case_number') : '';
        if(String.isBlank(caseNum)){
            errorMsg = 'Please provide case_number.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
            return NULL;
        }
        
        String ownerAccountId = req.params.containskey('account_id') ? req.params.get('account_id') : '';
        if(String.isBlank(ownerAccountId)){
            errorMsg = 'Please provide account_id.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
            return NULL;
        }
        
        Account ownerAccount;
        try{
            ownerAccount = [SELECT id, Name, Name_Arabic__c, IsPersonAccount, Party_ID__c, Party_Type__c, 
                 Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c, Primary_Language__c, 
                 Country__c, Country_Arabic__c, Address_Line_1__c, Address_Line_1_Arabic__c, 
                 Address_Line_2__c, Address_Line_2_Arabic__c, Address_Line_3__c, Address_Line_3_Arabic__c, 
                 Address_Line_4__c, Address_Line_4_Arabic__c, City__c, City_Arabic__c, State__c, 
                 State_Arabic__c, Postal_Code_Arabic__c, Email__c, Mobile_Country_Code__c, 
                 Mobile_Phone_Encrypt__c, Country__pc, Country_Arabic__pc, Address_Line_1__pc, 
                 Address_Line_2__pc, Address_Line_3__pc, Address_Line_4__pc, Address_Line_1_Arabic__pc, 
                 Address_Line_2_Arabic__pc, Address_Line_3_Arabic__pc, Address_Line_4_Arabic__pc, City__pc, 
                 City_Arabic__pc, BillingPostalCode, Email__pc, Mobile_Country_Code__pc, Mobile_Phone_Encrypt__pc 
                 FROM Account WHERE Id = :ownerAccountId];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            errorMsg = 'The account_id provided does not match any records.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, ex.getMessage());
            return NULL;
        }
        
        if(NULL == ownerAccount){
            errorMsg = 'The account_id provided does not match any records.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
            return NULL;
        }
        
        Case currentSR_COCD;
        try{
            currentSR_COCD = [SELECT id, caseNumber, Type, SR_Type__c, Origin, isHelloDamacAppCase__c, 
                        RecordTypeId, RecordType.Name, status, AccountId, Account.Name, 
                        Is_Primary_Customer_Updated__c, Address__c, Address_Arabic_1__c, 
                        Address_2__c, Address_Arabic_2__c, Address_3__c, Address_Arabic_3__c, Address_4__c, 
                        Address_Arabic_4__c, City__c, City_Arabic__c, State__c, State_Arabic__c, 
                        Postal_Code_Arabic__c, Country__c, Country_Arabic__c, Contact_Email__c, Postal_Code__c, 
                        Mobile_Country_Code__c, Contact_Mobile__c, Home_Phone_Country_Code__c, Home_Phone__c, 
                        Work_Phone_Country_Code__c, Work_Phone__c, OCR_verified__c, CRF_File_URL__c, 
                        Additional_Doc_File_URL__c, OD_File_URL__c 
                        FROM Case 
                        WHERE AccountId = :ownerAccountId 
                        AND RecordType.Name = 'Change of Details' 
                        AND SR_Type__c = 'Change of Contact Details' 
                        AND status = 'Draft Request' 
                        AND caseNumber = :caseNum LIMIT 1];
        }
        catch(Exception ex){
            System.debug(ex.getMessage());
            errorMsg = 'No Matching draft SR records available for provided parameters.';
            getErrorResponse(2, statusCodeMap.get(2), errorMsg, ex.getMessage());
            return NULL;
        }
        if(NULL == currentSR_COCD){
            errorMsg = 'The Case Number provided does not match any draft SR records.';
            getErrorResponse(2, statusCodeMap.get(2), errorMsg, errorMsg);
            return NULL;
        }
        
        SR_contextWrap currentSRContext = new SR_contextWrap();
        currentSRContext.currentSR_Owner = ownerAccount;
        currentSRContext.currentSR_Case = currentSR_COCD;
        return currentSRContext;
    }
    
    private static void setCaseOwner(Account ownerAccount, Case sr_caseRecord) {
        if (NULL != ownerAccount.Primary_CRE__c) {
            sr_caseRecord.OwnerId = ownerAccount.Primary_CRE__c;
        } else if (NULL != ownerAccount.Secondary_CRE__c) {
            sr_caseRecord.OwnerId = ownerAccount.Secondary_CRE__c;
        } else if (NULL != ownerAccount.Tertiary_CRE__c) {
            sr_caseRecord.OwnerId = ownerAccount.Tertiary_CRE__c;
        } else {
            String queueName = (String.isNotBlank(ownerAccount.Primary_Language__c) && 
                ownerAccount.Primary_Language__c.equalsIgnoreCase('Arabic')) ? 
                'Non Elite Arabs Queue' : 
              'Non Elite Non Arabs Queue';
            
            QueueSobject caseOwnQueue;
            try{
              caseOwnQueue = [SELECT Id, Queue.Name, QueueId FROM QueueSobject 
                                        WHERE SobjectType = 'Case' 
                                        AND Queue.Name = :queueName LIMIT 1];
            }
            catch(Exception ex){
              system.debug(ex.getMessage());
                return;
            }
            
            if(NULL != caseOwnQueue) sr_caseRecord.OwnerId = caseOwnQueue.QueueId;
        }
    }
    
    private static void getSuccessResponse(Account currentSR_Owner, Case currentSR_COCD, String successMsg){
        ResponseWrapper responseWrapper = new ResponseWrapper();
        
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = 1;
        responseMetaData.title = statusCodeMap.get(1);
        responseMetaData.message = successMsg;
        responseMetaData.developer_message = successMsg;
        responseWrapper.meta_data = responseMetaData;
        
        cls_data responseData = new cls_data();
        responseData.sr_case_id = currentSR_COCD.id;
        responseData.sr_case_number = currentSR_COCD.caseNumber;
        responseData.sr_case_status = currentSR_COCD.status == 'Working' ? 'Submitted' : currentSR_COCD.status;
        responseData.sr_case_submission_date = String.valueOf(Date.today());
        //responseData.sr_request_type = currentSR_COCD.SR_Type__c;
        responseData.sr_request_type = 'Update primary contact details';
        responseData.sr_origin = currentSR_COCD.Origin;
        responseData.sr_account_owner = currentSR_Owner.Name;
        responseData.sr_account_is_person = currentSR_Owner.IsPersonAccount;
        responseData.sr_country_code = currentSR_COCD.Mobile_Country_Code__c;
        responseData.sr_primary_mobile_number = currentSR_COCD.Contact_Mobile__c;
        responseData.sr_primary_email_id = currentSR_COCD.Contact_Email__c;
        responseData.sr_country = currentSR_COCD.Country__c;
        responseData.sr_address = currentSR_COCD.Address__c;
        responseData.sr_city = currentSR_COCD.City__c;
        responseData.sr_state = currentSR_COCD.State__c;
        responseData.postal_code = currentSR_COCD.Postal_Code__c;
        responseData.sr_cocd_form_url = currentSR_COCD.CRF_File_URL__c;
        responseData.sr_additional_doc_file_url = currentSR_COCD.Additional_Doc_File_URL__c;
        responseWrapper.data = responseData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    private static void getErrorResponse(Integer statusCode, String title, 
    String responseMessage, String devMessage) {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = statusCode;
        responseMetaData.title = title;
        responseMetaData.message = responseMessage;
        responseMetaData.developer_message = devMessage;
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    private class SR_contextWrap {
        Account currentSR_Owner;
        Case currentSR_Case;
    }
    
    /* Wrapper classes for returning reponse */
    public class ResponseWrapper {
        public cls_meta_data meta_data;
        public cls_data data;
    }

    public class cls_meta_data {
        public Integer status_code;
        public String message;
        public String title;
        public String developer_message;   
    }

    public class cls_data {
        public String sr_case_id;
        public String sr_case_number;
        public String sr_case_status;
        public String sr_case_submission_date;
        public String sr_request_type;
        public String sr_origin;
        public String sr_account_owner;
        public Boolean sr_account_is_person;
        public String sr_country_code;
        public String sr_primary_mobile_number;
        public String sr_primary_email_id;
        public String sr_country;
        public String sr_address;
        public String sr_city;
        public String sr_state;
        public String postal_code;
        public String sr_cocd_form_url;
        public String sr_additional_doc_file_url;
    }
}