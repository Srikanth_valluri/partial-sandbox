/************************************************************************************************************************
* Name               : ReshufflePreinquiries                                                                            *
* Description        : Re-Assign the Pre-Inquiry Owner                                                                  *
* Created Date       : 05-02-2018                                                                                       *
* Created By         : ESPL                                                                                             *
* ----------------------------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                                    *
* 1.0         Craig Lobo    05-02-2018      Initial Draft.                                                              *
* 1.1         Alok Chauhan  08-02-2018      Modified Code Logic                                                         *
* 1.2         Craig Lobo    12-02-2018      Added check for User query (Is Active, Not on Leave, Not Blacklisted)       *
* 1.3         Alok Chauhan  15-03-2018      Added check to prevent shuffling of some criteria
************************************************************************************************************************/

public without sharing class ReshufflePreinquiries  {
    
    public ReshufflePreinquiries () {
        
    }

    /*********************************************************************************************
    * @Description : Method contains the logic to Re-Assign the Pre-Inquiry Owner                *
    * @Params      : List<Inquiry__c>, Integer, String                                           *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void reAssignPreInquiryOwner(
        List<Inquiry__c> pInquiryList, 
        Integer pReAssignmentPass,
        String pQueueName 
    ) {

        /*Decimal batchSize = 0;
        Decimal inquiryCount = 0;
        Decimal reAssignmentPassValue;
        List<Id> tasUserIdList = new List<Id>();
        List<Group> queueList = new List<Group>();
        List<Inquiry__c> updateInquiryList = new List<Inquiry__c>();
        Map<Id,Id> userIdManagerIdMap = new Map<Id,Id>();

        if (String.isNotBlank(pQueueName)) {
            queueList = [SELECT Id, Name FROM Group WHERE Type = 'Queue' AND DeveloperName = :pQueueName];
        }
        system.debug('####queueList = ' + queueList); 

        for (TS_Assignment_Settings__c csInstance : TS_Assignment_Settings__c.getall().values()) {
            tasUserIdList.add(csInstance.Owner_Id__c);
        }

        // v1.2 Craig Lobo 12-02-2018 Added checks while quering the User record 
        // Get the UserId and ManagerId in the Map
        for (User userInstance : [ SELECT Id, ManagerId, IsActive, isUserOnLeave__c, Is_Blacklisted__c 
                                     FROM User 
                                    WHERE Id IN :tasUserIdList
                                      AND IsActive = true
                                      AND isUserOnLeave__c = false
                                      AND Is_Blacklisted__c = false
        ]) {

            userIdManagerIdMap.put(userInstance.Id, userInstance.ManagerId);
        }
        // v1.2 Craig Lobo 12-02-2018
  
        system.debug('####userIdManagerIdMap.Size()) = ' + userIdManagerIdMap.Size()); 
        system.debug('####pInquiryList.Size() = ' + pInquiryList.Size()); 
        if (userIdManagerIdMap.Size() > 0 
            && pInquiryList != null 
            && pInquiryList.Size() > 0 
        ) {
            Decimal inquirySize = pInquiryList.Size();
            Decimal usersSize = userIdManagerIdMap.Size();
            batchSize = (inquirySize/usersSize).round(System.RoundingMode.CEILING);

        }
        System.Debug('Batch_Size>' + batchsize);
         
        List<string> exOwners= new List<string>();
        exOwners.addall(Label.ExcludeTSAOwner.split(','));
        
        List<string> exCamp= new List<string>();
        exCamp.addall(Label.ExcludeTSACamp.split(',')); 
         
        // Logic to update the Inquiry Owner
        if(batchSize > 0) {
            for(Id userId : userIdManagerIdMap.keyset()) {
                System.Debug('userId>>>>' + userId);
                inquiryCount = 0; 
                for (Integer i = pInquiryList.size() - 1; i >= 0 ; --i) {
                    System.Debug('inquiryCount > ' + inquiryCount);
                    System.Debug('batchsize > ' + batchsize);
                    System.Debug('inquiryCount <= batchsize > ' + (inquiryCount <= batchsize));
                    if (inquiryCount < batchsize) {
                        System.Debug('AAAAYAYAYA' + pInquiryList[i]);
                       
                        IF(pInquiryList[i].Owner_Manager_Id__c != userIdManagerIdMap.get(userId) && pInquiryList[i].Campaign__r.Exclude_TSA_Reshuffle__c==false
                           && !exOwners.Contains(pInquiryList[i].OwnerId)){
                        
                        System.Debug('pReAssignmentPass = ' + pReAssignmentPass);
                            System.Debug('pReAssignmentPass > ' + (pInquiryList[i].Re_Assignment_Pass_No__c < pReAssignmentPass));
                            if (pReAssignmentPass != null 
                                && (pInquiryList[i].Re_Assignment_Pass_No__c == null 
                                    ||pInquiryList[i].Re_Assignment_Pass_No__c == 0
                                    || pInquiryList[i].Re_Assignment_Pass_No__c < pReAssignmentPass
                                )
                            ) {
                                System.Debug('PRE OWNER USER = ' + pInquiryList[i]);
                                pInquiryList[i].OwnerId = userId;
                                System.Debug('NEW OWNER USER = ' + pInquiryList[i]);
                                if(pInquiryList[i].Re_Assignment_Pass_No__c == NULL){
                                    pInquiryList[i].Re_Assignment_Pass_No__c = 1;
                                } else{
                                    pInquiryList[i].Re_Assignment_Pass_No__c = pInquiryList[i].Re_Assignment_Pass_No__c + 1;
                                }
                                updateInquiryList.add(pInquiryList[i]);
                                pInquiryList.remove(i);
                                inquiryCount ++;
                            } else if (queueList.size() > 0) {
                                System.Debug(' Q pReAssignmentPass = ' + pInquiryList[i].Re_Assignment_Pass_No__c);
                                System.Debug('PRE OWNER Q = ' + pInquiryList[i]);
                                pInquiryList[i].OwnerId = queueList[0].Id;
                                System.Debug('NEW OWNER Q = ' + pInquiryList[i]);
                                updateInquiryList.add(pInquiryList[i]);
                                pInquiryList.remove(i);
                                inquiryCount++;
                            }
                        } // Manager Id check
                    } else{
                        System.Debug('Break>> ' + updateInquiryList);
                        //inquiryCount = 0;
                        break;
                    } // Switch to next user check
                } 
            } // User loop
        } // batchsize check

        // DML: Update the Pre-Inquiries
        System.Debug('List to Update Size >> ' + updateInquiryList.Size());
        System.Debug('List to Update>> ' + updateInquiryList);
        update updateInquiryList;
        */
    }

}