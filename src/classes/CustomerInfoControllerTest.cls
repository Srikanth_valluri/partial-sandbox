@isTest
private class CustomerInfoControllerTest {

    @testSetup static void setup() {
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, Name, AccountId, ContactId FROM User WHERE Id = :communityUser.Id];
        System.debug('communityUser.AccountId = ' + communityUser.AccountId);
        NSIBPM__Service_Request__c dealSr = TestDataFactory_CRM.createServiceRequest();
        insert dealSr;
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(communityUser.AccountId, dealSr.Id, 1);
        insert lstBooking;
        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;
        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        insert inventory;
        List<Booking_Unit__c> lstUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 4);
        list<Booking_Unit_Active_Status__c> mapBookingUnitStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert mapBookingUnitStatus ;
        
        for (Booking_Unit__c unit : lstUnit) {
            unit.Inventory__c = inventory.Id;
            unit.Token_Paid__c = true;
            unit.Registration_Status__c = 'Agreement executed by DAMAC';
        }
        insert lstUnit;
        List<Payment_Terms__c> lstPaymentTerms = new List<Payment_Terms__c>();
        List<Payment_Plan__c> lstPP = new List<Payment_Plan__c>();
        for (Booking_Unit__c unit : lstUnit) {
            lstPP.add(new Payment_Plan__c(
                Status__c = 'Active',
                Booking_Unit__c = unit.Id
            ));
        }
        insert lstPP;
        for (Payment_Plan__c unitPP : lstPP) {
            for (Integer i = 1; i <= 3; i++) {
                lstPaymentTerms.add(new Payment_Terms__c(
                    Payment_Amount__c = String.valueOf(i * 100),
                    Payment_Date__c = Date.today().addDays(i),
                    Booking_Unit__c = unitPP.Booking_Unit__c,
                    Payment_Plan__c = unitPP.Id
                ));
            }
        }
        insert lstPaymentTerms;
    }

    @isTest
    static void testController() {
        User user = [   SELECT      Id, AccountId, ContactId FROM User 
                        WHERE       Profile.Name = :CommunityTestDataFactory.COMMUNITY_USER_PROFILE 
                        ORDER BY    CreatedDate DESC
                        LIMIT       1];
        System.debug('user.AccountId = ' + user.AccountId);
        System.runAs(user) {
            CustomerInfoController controller = new CustomerInfoController();
            controller.strRecordId = user.AccountId;
            //System.assert(!controller.lstPortWrapper.isEmpty());
            for (CustomerInfoController.PortfolioWrapper wrapper : controller.lstPortWrapper) {
                System.debug(wrapper.dateCompletiondate);
            }
        }
    }
}