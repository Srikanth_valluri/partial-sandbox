@isTest
private class MoveOutRemovePortalAccessForTenantTest
{
  static Account a = new Account();
  static Account a1 = new Account();
  static Booking__c  bk = new  Booking__c();
  static NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
  static Booking_Unit__c bu = new Booking_Unit__c();
  static Booking_Unit__c bu1 = new Booking_Unit__c();
  static Location__c locObj = new Location__c();
  static FM_Case__c fmObj = new FM_Case__c();
  static FM_User__c userObj = new FM_User__c();
  static FM_Additional_Detail__c addDetails = new FM_Additional_Detail__c();
  static SR_Attachments__c srObj = new SR_Attachments__c();
  static Task taskObj = new Task();

  
    static testMethod void testMethodSubmit(){


		RecordType personAccountRecordType =  [SELECT Id,name FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
			system.debug('personAccountRecordType====='+personAccountRecordType);
			a.FirstName = 'Test';
			a.LastName = 'Test';
			a.recordtypeid = personAccountRecordType.id;
		a.party_ID__C = '1039032';
		insert a;
		account acc = new account();
		acc = [select id,PersonContactId from account where id=: a.id];
		system.debug('acc=='+acc);
		Id p = [select id from profile where name='Customer Community Login User'].id;
		User user = new User(alias = 'test123', email='test123ewtwet@noemail.com',
				emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
				localesidkey='en_US', profileid = p, country='United States',IsActive =true,
				ContactId = acc.PersonContactId,
				timezonesidkey='America/Los_Angeles', username='testerewttwet@noemail.com');
		
		insert user;

		Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
		sr.NSIBPM__Customer__c = a.id;
		sr.RecordTypeId = RecType;
		insert sr;	

		bk.Account__c = a.id;
		bk.Deal_SR__c = sr.id;
		insert bk;

		bu.Booking__c = bk.id;
		bu.Unit_Name__c = 'LSB/10/B1001';
		bu.Owner__c = a.id;
		bu.Property_City__c = 'Dubai';
		insert bu;
		bu1.Booking__c = bk.id;
		bu1.Unit_Name__c = 'LSB/10/B1001';
		bu1.Owner__c = a1.id;
		bu1.Property_City__c = 'Dubai';
		insert bu1;

		locObj.Name  = 'LSB';
		locObj.Location_ID__c = '83488';
		insert locObj;

		userObj.Building__c = locObj.id;
		userObj.FM_Role__c = 'FM Admin';
		insert userObj;

		Id RecordTypeIdFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move Out').getRecordTypeId();
		fmObj.Booking_Unit__c = bu.id;
		fmObj.Expected_move_out_date__c = Date.Today();
		fmObj.Move_in_date__c = Date.Today();
		fmObj.Tenant__c = acc.id;
		fmObj.Actual_move_out_date__c = system.today();
		fmObj.Account__c = a.id;
		fmObj.Access_card_required__c = 'Yes';
		fmObj.recordtypeid = RecordTypeIdFMCase;
		insert fmObj;
		list<fm_case__c> lstFMcase = new list<fm_case__c>();
		lstFMcase.add(fmObj);

		srObj.Name = 'Test';
		srObj.FM_Case__c = fmObj.id;
		insert srObj;

		addDetails.Resident_Account__c = a.id;
		addDetails.Household_Account__c = a.id;
		addDetails.Pet_Account__c = a.id;
		addDetails.Vehicle_Account__c = a.id;
		addDetails.Emergency_Contact_Account__c = a.id;
		insert addDetails;

		taskObj.Description = 'Test'; //string
		taskObj.Status ='Completed';
		taskObj.Subject = Label.Move_in_date_task;
		taskObj.WhatId = fmObj.id; //record id
		insert taskObj;
			MoveOutRemovePortalAccessForTenant moveOutObj = new MoveOutRemovePortalAccessForTenant();        
			MoveOutRemovePortalAccessForTenant.removePortalAccessForTenant(lstFMcase);
    }
}