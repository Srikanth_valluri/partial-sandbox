/************************************************************************************************
 * @Name              : VirtualHandoverDLD_MS_Graph_API
 * @Test Class Name   : VirtualHandoverDLD_MS_Graph_API_Test
 * @Description       : RestResource Class for accessing token response from Microsoft Graph API 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         Rohit         19/08/2020       Created
***********************************************************************************************/
@RestResource(urlMapping='/DLDTrusteeMeeting/*')
global without sharing class VirtualHandoverDLD_MS_Graph_API {
    @HttpGet
    global static void doGet(){    
        API_Integration_Value__c apiValue = [SELECT Id, Refresh_Token__c, Client_Id__c, Client_Secret__c,
                                                    Endpoint_URL__c, Redirect_URI__c, Scope__c,
                                                    Tenant_Id__c, Username__c, Password__c
                                                FROM API_Integration_Value__c WHERE name = 'Virtual Handover Graph API' 
                                                AND  Username__c = 'DLDTrustee.Meeting@damacproperties.com' LIMIT 1];
        String code = RestContext.request.params.get('code');
        Http http = new Http();
        HttpRequest tokenReq = new HttpRequest();
        String endPoint = apiValue.Endpoint_URL__c + apiValue.Tenant_Id__c + '/oauth2/v2.0/token';
        String body = 'client_id=' + EncodingUtil.urlEncode(apiValue.Client_Id__c, 'UTF-8')
                    + '&client_secret=' + EncodingUtil.urlEncode(apiValue.Client_Secret__c, 'UTF-8')
                    + '&scope=' + apiValue.Scope__c
                    + '&code=' + EncodingUtil.urlEncode(code, 'UTF-8')
                    + '&redirect_uri=' + EncodingUtil.urlEncode(apiValue.Redirect_URI__c, 'UTF-8')
                    + '&grant_type=authorization_code';
        tokenReq.setEndpoint(endPoint);
        system.debug('body: ' + body);
        tokenReq.setBody(body);
        tokenReq.setMethod('POST');
        tokenReq.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        HttpResponse tokenRes = new HttpResponse();
        try{
            String responseBody = '';
            if(!Test.isRunningTest()){
                tokenRes = http.send(tokenReq);
                responseBody = tokenRes.getBody();
            } else{
                 responseBody = '{"token_type": "Bearer","scope": "Calendars.Read","expires_in": 3599,"ext_expires_in": 3599,'
                                + '"access_token": " accesstoken","refresh_token": "refreshtoken"}';     
            }
            system.debug('fetch token response body: ' + responseBody);
            VirtualHandover_MSTeams_Token_JSON token = VirtualHandover_MSTeams_Token_JSON.parse(responseBody);
            if(token.refresh_token != null && token.refresh_token != ''){
                apiValue.Refresh_Token__c = token.refresh_token;
                update apiValue;
            }
        } catch(exception e){
            system.debug('Exception: ' + e);
        }
    }
}