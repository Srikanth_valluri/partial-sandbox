/*
 * Description - Class used for fetching details of POA
 *
 * Version        Date            Author            Description
 * 1.0            26/02/18        Vivek Shinde      Initial Draft
 */
public without sharing class POAUtility {
    
    // Method to return POA details for a SR Type
    /*public static Map<Id, POA__c> getPOADetails(Id idAccount, Id idBU, String strSRType) {
        Map<Id, POA__c> mapPOA = new Map<Id, POA__c>();
        Map<String, String> mapPOARights = new Map<String, String>();
        
        for(POA_Right__mdt objRight : [SELECT MasterLabel, QualifiedApiName, Applicable_SR_Types__c, POA_Right_Full_Name__c 
                                       FROM POA_Right__mdt]) {
            mapPOARights.put(objRight.POA_Right_Full_Name__c, objRight.Applicable_SR_Types__c);
        }
        
        Set<Id> setPOAId = new Set<Id>();
        for(POA_Unit__c objPU: [Select Id, POA__c, POA_Right__c From POA_Unit__c 
                                Where Booking_Unit__c =: idBU And 
                                POA_Related_Account__r.Related_to_Account__c =: idAccount AND
                                POA__r.POA_Verification_Status__c = 'POA Verified by Legal' AND
                                POA__r.POA_Valid_Till__c >= TODAY]) {
            if(mapPOARights.containsKey(objPU.POA_Right__c) && 
                String.isNotBlank(mapPOARights.get(objPU.POA_Right__c)) &&
                mapPOARights.get(objPU.POA_Right__c).contains(strSRType)) {
                setPOAId.add(objPU.POA__c);
            }
        }
        
        if(!setPOAId.isEmpty()) {
            mapPOA = new Map<Id, POA__c>([Select Id, First_Name__c, Last_Name__c, POA_Valid_Till__c, Email_Address__c, Mobile_Number__c,
                      POA_Number__c, POA_Nationality__c, POA_Issued_By__c, Passport_Number__c
                      From POA__c Where Id IN: setPOAId]);
        }
        system.debug('--mapPOA--'+mapPOA);
        return mapPOA;
    }*/
    
    // Method to fetch POA document
    /*public static SR_Attachments__c getPOADocument(Id idPOA, Id idAccount) {
        List<POA_Related_Account__c> lstPRA = [Select Id, POA__c, Related_to_Account__c,
            (Select Id, Name, Attachment_URL__c, Type__c, View__c From Documents__r Where Type__c = 'POA')
            From POA_Related_Account__c Where POA__c =: idPOA And Related_to_Account__c =: idAccount];
        if(lstPRA != null && !lstPRA.isEmpty() && lstPRA[0].Documents__r != null && !lstPRA[0].Documents__r.isEmpty()) {
            return lstPRA[0].Documents__r[0];
        }
        return null;
    }*/
    
    // Method to get POA list for an account
    public static List<POA> getPOAListForAccount(Id idAccount) { 
        List<POA> lstPOA = new List<POA>();
        for(POA_Related_Account__c objPRA: [Select Id, POA__c, Related_to_Account__c,
            POA_Account__r.FirstName, POA_Account__r.LastName, POA_Valid_Till__c 
            From POA_Related_Account__c Where Related_to_Account__c =: idAccount And POA_Account__c != null]) {
            POA objPOA = new POA(objPRA.Id, objPRA.POA_Account__r.FirstName, objPRA.POA_Account__r.LastName, objPRA.POA_Valid_Till__c);
            //objPOA.idPOA = objPRA.Id;
            //objPOA.strFirstName = objPRA.POA_Account__r.FirstName;
            //objPOA.strLastName = objPRA.POA_Account__r.LastName;
            //objPOA.dtValidTill = objPRA.POA_Valid_Till__c;
            lstPOA.add(objPOA);
        }
        
        /*List<POA__c> lstPOA = new List<POA__c>();
        Set<Id> setPOAId = new Set<Id>();
        for(POA_Related_Account__c objPRA: [Select Id, POA__c, Related_to_Account__c 
            From POA_Related_Account__c Where Related_to_Account__c =: idAccount]) {
            setPOAId.add(objPRA.POA__c);
        }
        if(!setPOAId.isEmpty()) {
            lstPOA = [Select Id, First_Name__c, Last_Name__c, POA_Valid_Till__c, Email_Address__c, Mobile_Number__c,
                      POA_Number__c, POA_Nationality__c, POA_Issued_By__c, Passport_Number__c
                      From POA__c Where Id IN: setPOAId And
                      POA_Verification_Status__c = 'POA Verified by Legal' AND POA_Valid_Till__c >= TODAY];
        }*/
        return lstPOA;
    }
    
    // Method to fetch existing POAs for an account
    /*public static List<POA__c> getExistingPOA(Id idAccount) {
        List<POA__c> lstPOA = [Select Id, First_Name__c, Last_Name__c, POA_Valid_Till__c, Email_Address__c, Mobile_Number__c
                               From POA__c Where Related_to_Account__c =: idAccount];
        return lstPOA;
    }*/
    
    public Class POA {
        public Id idPOA {get; set;}
        public String strFirstName {get; set;}
        public String strLastName {get; set;}
        public Date dtValidTill {get; set;}
        
        public POA(Id idPOA, String strFirstName, String strLastName, Date dt) {
            this.idPOA = idPOA;
            this.strFirstName = strFirstName;
            this.strLastName = strLastName;
            this.dtValidTill = dt;
        }
    }
}