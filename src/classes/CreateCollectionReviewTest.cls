@isTest
public class CreateCollectionReviewTest {
    
    Static List<User> createUsers() {
        List<User> userList = new List<User>();
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser1 = new User(alias = 'test1', email='xyz1@email.com',
                  emailencodingkey='UTF-8', lastname='User 1', languagelocalekey='en_US',
                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                  ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com');
        userList.add(adminUser1);
        User adminUser2 = new User(alias = 'test2', email='xyz2@email.com',
                  emailencodingkey='UTF-8', lastname='User 2', languagelocalekey='en_US',
                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                  ,timezonesidkey='America/Los_Angeles', username='xyz2@email.com');
        userList.add(adminUser2);
        User adminUser3 = new User(alias = 'test3', email='xyz3@email.com',
                  emailencodingkey='UTF-8', lastname='User 3', languagelocalekey='en_US',
                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                  ,timezonesidkey='America/Los_Angeles', username='xyz3@email.com');
        userList.add(adminUser3);
        User adminUser4 = new User(alias = 'test4', email='xyz4@email.com',
                  emailencodingkey='UTF-8', lastname='User 4', languagelocalekey='en_US',
                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                  ,timezonesidkey='America/Los_Angeles', username='xyz4@email.com');
        userList.add(adminUser4);
        User adminUser5 = new User(alias = 'test5', email='xyz5@email.com',
                  emailencodingkey='UTF-8', lastname='User 5', languagelocalekey='en_US',
                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                  ,timezonesidkey='America/Los_Angeles', username='xyz5@email.com');
        userList.add(adminUser5);
        return userList;
    }

    @isTest
    static void testCreateDCR_2_Level_Mapping() {
        List<User> userList = createUsers();
        
        insert userList;
        DCR_Mapping__c dcrMappingInstance = new DCR_Mapping__c(Name = userList[0].Id , DCR_Initiator__c = userList[0].Id ,
                                                        Level_1_Approver__c = userList[1].Id , Level_2_Approver__c = userList[2].Id ,
                                                        CRE_Team_Ids__c = userList[3].Id +','+userList[4].Id);
        insert dcrMappingInstance;

        System.runAs(userList[0]) {
            Test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.CreateDailyCollectionReview'));
            //System.currentPageReference().getParameters().put('id',);
            CreateDailyCollectionReview objDamac = new CreateDailyCollectionReview();
            objDamac.createDCR();
            Test.stopTest();
        
       
            System.debug('objDamac.dcrInstance.Id-->'+objDamac.dcrInstance.Id);
            System.assert(objDamac.dcrInstance.Id != Null);
            System.debug('objDamac.detailList-->'+objDamac.detailList);
            System.assert(objDamac.detailList.size() == 2);
        }
    }

    @isTest
    static void testCreateDCR_1_Level_Mapping() {
        List<User> userList = createUsers();
        insert userList;
        DCR_Mapping__c dcrMappingInstance = new DCR_Mapping__c(Name = userList[0].Id , DCR_Initiator__c = userList[0].Id ,
                                                        Level_1_Approver__c = userList[1].Id ,
                                                        CRE_Team_Ids__c = userList[3].Id +','+userList[4].Id);
        insert dcrMappingInstance;

        System.runAs(userList[0]) {
            Test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.CreateDailyCollectionReview'));
            //System.currentPageReference().getParameters().put('id',);
            CreateDailyCollectionReview objDamac = new CreateDailyCollectionReview();
            objDamac.createDCR();
            Test.stopTest();
        
       
            System.debug('objDamac.dcrInstance.Id-->'+objDamac.dcrInstance.Id);
            System.assert(objDamac.dcrInstance.Id != Null);
            System.debug('objDamac.detailList-->'+objDamac.detailList);
            System.assert(objDamac.detailList.size() == 2);
        }
    }


    @isTest
    static void testSubmit_InitiatedTL(){
        Id recordTypeId = Schema.SObjectType.Daily_Collection_Review__c.getRecordTypeInfosByDeveloperName().get('Daily_Review').getRecordTypeId();
        List<User> userList = createUsers();
        insert userList;
        DCR_Mapping__c dcrMappingInstance = new DCR_Mapping__c(Name = userList[0].Id , DCR_Initiator__c = userList[0].Id ,
                                                        Level_1_Approver__c = userList[1].Id , Level_2_Approver__c = userList[2].Id ,
                                                        CRE_Team_Ids__c = userList[3].Id +','+userList[4].Id);
        insert dcrMappingInstance;
        Daily_Collection_Review__c objDamacCollection = new Daily_Collection_Review__c();
        objDamacCollection.Meeting_Date__c = System.Today();
        objDamacCollection.DCR_initiator__c = userList[0].Id;
        objDamacCollection.Team_Lead__c = userList[0].Id;
        objDamacCollection.CRE_Manager__c = userList[1].Id;
        objDamacCollection.HOD__c = userList[2].Id;
        objDamacCollection.Review_Initiated_By_TL__c = true;
        objDamacCollection.RecordTypeId = recordTypeId;
        insert objDamacCollection;
        Daily_Collection_Review_Detail__c objDamacCollectionDetail = new Daily_Collection_Review_Detail__c(DCR__c = objDamacCollection.Id);
        insert objDamacCollectionDetail;
        
            Test.startTest();
            System.runAs(userList[0]) {
                Test.setCurrentPageReference(new PageReference('Page.CreateDailyCollectionReview'));
                System.currentPageReference().getParameters().put('id', objDamacCollection.Id);
                CreateDailyCollectionReview objDamac = new CreateDailyCollectionReview();
                objDamac.createDCR();
                objDamac.btnValue = 'Save';
                objDamac.submitReview();
               
                objDamac.btnValue = 'Submit';
                objDamac.createDCR();
                objDamac.submitReview();
                
                Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
                objDamac.sendUserEmail();
                System.debug('Send Email');
                System.assert(objDamac.dcrInstance.Id != Null);
              

            }
            System.runAs(userList[1]) {
                
                objDamacCollection.M_Attempted_all_followups__c  = 'Modified - Remarks mentioned';
                update objDamacCollection;
                Test.setCurrentPageReference(new PageReference('Page.CreateDailyCollectionReview'));
                System.currentPageReference().getParameters().put('id', objDamacCollection.Id);
                CreateDailyCollectionReview objDamac1 = new CreateDailyCollectionReview();
                objDamac1.createDCR();
                objDamac1.btnValue = 'Save';
                objDamac1.submitReview();
                
                objDamac1.btnValue = 'Submit';
                objDamac1.createDCR();
                objDamac1.submitReview();
                
           
                Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
                objDamac1.sendUserEmail();
                System.debug('Send Email');
                System.assert(objDamac1.dcrInstance.Id != Null);

            }
            System.runAs(userList[2]) {
                
                
                Test.setCurrentPageReference(new PageReference('Page.CreateDailyCollectionReview'));
                System.currentPageReference().getParameters().put('id', objDamacCollection.Id);
                CreateDailyCollectionReview objDamac1 = new CreateDailyCollectionReview();
                objDamac1.createDCR();
            
                objDamacCollection.H_Quality_check_on_Emails_replied__c  = 'Modified - Remarks mentioned';
                update objDamacCollection;
                objDamac1.createDCR();
                objDamac1.btnValue = 'Save';
                objDamac1.submitReview();
                objDamac1.btnValue = 'Submit';
                objDamac1.createDCR();
                objDamac1.submitReview();
                Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
                objDamac1.sendUserEmail();
                System.debug('Send Email');
				System.assert(objDamac1.dcrInstance.Id != Null);
            }
        Test.stopTest();
         Daily_Collection_Review__c resDCRInstance = [SELECT Team_Lead_Saved_Review__c
                                                      		 , TL_submitted_review__c
                                                             , Manager_submitted_review__c
                                                             , Manager_Saved_Review__c
                                                             , HOD_submitted_review__c
                                                             , HOD_Saved_Review__c
                                                             , DCR_Status__c 
                                                      FROM Daily_Collection_Review__c 
                                                      WHERE id = :objDamacCollection.Id];
        System.assertEquals(resDCRInstance.Team_Lead_Saved_Review__c, true);
        System.assertEquals(resDCRInstance.TL_submitted_review__c, true);
        System.assertEquals(resDCRInstance.Manager_submitted_review__c, true);
        System.assertEquals(resDCRInstance.Manager_Saved_Review__c, true);
        System.assertEquals(resDCRInstance.HOD_submitted_review__c, true);
        System.assertEquals(resDCRInstance.HOD_Saved_Review__c, true);
        System.assertEquals(resDCRInstance.DCR_Status__c, 'Submitted by HOD');
    }
    
    @isTest
    static void testSubmit_InitiatedManager(){
        Id recordTypeId = Schema.SObjectType.Daily_Collection_Review__c.getRecordTypeInfosByDeveloperName().get('Daily_Review').getRecordTypeId();
        List<User> userList = createUsers();
        insert userList;
        DCR_Mapping__c dcrMappingInstance = new DCR_Mapping__c(Name = userList[0].Id , DCR_Initiator__c = userList[0].Id ,
                                                        Level_1_Approver__c = userList[1].Id ,
                                                        CRE_Team_Ids__c = userList[3].Id +','+userList[4].Id);
        insert dcrMappingInstance;
        Daily_Collection_Review__c objDamacCollection = new Daily_Collection_Review__c();
        objDamacCollection.Meeting_Date__c = System.Today();
        objDamacCollection.DCR_initiator__c = userList[0].Id;
        objDamacCollection.CRE_Manager__c = userList[0].Id;
        objDamacCollection.HOD__c = userList[1].Id;
        objDamacCollection.Review_Initiated_By_manager__c = true;
        objDamacCollection.RecordTypeId = recordTypeId;
        insert objDamacCollection;
        Daily_Collection_Review_Detail__c objDamacCollectionDetail = new Daily_Collection_Review_Detail__c(DCR__c = objDamacCollection.Id);
        insert objDamacCollectionDetail;
        	
            Test.startTest();
            System.runAs(userList[0]) {
                Test.setCurrentPageReference(new PageReference('Page.CreateDailyCollectionReview'));
                System.currentPageReference().getParameters().put('id', objDamacCollection.Id);
                CreateDailyCollectionReview objDamac = new CreateDailyCollectionReview();
                objDamac.createDCR();
                objDamac.btnValue = 'Save';
                objDamac.submitReview();
                objDamac.btnValue = 'Submit';
                objDamac.createDCR();
                objDamac.submitReview();
                Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
                objDamac.sendUserEmail();
                System.debug('Send Email');
                System.assert(objDamac.dcrInstance.Id != Null);
              

            }
            
            System.runAs(userList[1]) {
                
                
                Test.setCurrentPageReference(new PageReference('Page.CreateDailyCollectionReview'));
                System.currentPageReference().getParameters().put('id', objDamacCollection.Id);
                CreateDailyCollectionReview objDamac1 = new CreateDailyCollectionReview();
                objDamac1.createDCR();
            
                objDamacCollection.HOD_Saved_Review__c   = true;
                objDamacCollection.H_Quality_check_on_Emails_replied__c  = 'Modified - Remarks mentioned';
                update objDamacCollection;
                objDamac1.createDCR();
                objDamac1.btnValue = 'Save';
                objDamac1.submitReview();
                objDamac1.btnValue = 'Submit';
                objDamac1.createDCR();
                objDamac1.submitReview();
                Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
                objDamac1.sendUserEmail();
                System.debug('Send Email');
				System.assert(objDamac1.dcrInstance.Id != Null);
            }
        Test.stopTest();
        Daily_Collection_Review__c resDCRInstance = [SELECT Manager_submitted_review__c
                                                            , Manager_Saved_Review__c
                                                            , HOD_submitted_review__c
                                                            , HOD_Saved_Review__c
                                                            , DCR_Status__c 
                                                      FROM Daily_Collection_Review__c 
                                                      WHERE id = :objDamacCollection.Id];
        
        System.assertEquals(resDCRInstance.Manager_submitted_review__c, true);
        System.assertEquals(resDCRInstance.Manager_Saved_Review__c, true);
        System.assertEquals(resDCRInstance.HOD_submitted_review__c, true);
        System.assertEquals(resDCRInstance.HOD_Saved_Review__c, true);
        System.assertEquals(resDCRInstance.DCR_Status__c, 'Submitted by HOD');
    }
    
    @isTest
    static void testExportExcel(){
        Id recordTypeId = Schema.SObjectType.Daily_Collection_Review__c.getRecordTypeInfosByDeveloperName().get('Daily_Review').getRecordTypeId();
        List<User> userList = createUsers();
        insert userList;
        DCR_Mapping__c dcrMappingInstance = new DCR_Mapping__c(Name = userList[1].Id , DCR_Initiator__c = userList[1].Id ,
                                                        Level_1_Approver__c = userList[2].Id ,
                                                        CRE_Team_Ids__c = userList[3].Id +','+userList[4].Id);
        insert dcrMappingInstance;
        Daily_Collection_Review__c objDamacCollection = new Daily_Collection_Review__c();
        objDamacCollection.Meeting_Date__c = System.Today();
        objDamacCollection.DCR_initiator__c = userList[1].Id;
        objDamacCollection.CRE_Manager__c = userList[1].Id;
        objDamacCollection.HOD__c = userList[2].Id;
        objDamacCollection.Review_Initiated_By_manager__c = true;
        objDamacCollection.RecordTypeId = recordTypeId;
        insert objDamacCollection;
        Daily_Collection_Review_Detail__c objDamacCollectionDetail = new Daily_Collection_Review_Detail__c(DCR__c = objDamacCollection.Id);
        insert objDamacCollectionDetail;
        
            System.runAs(userList[0]) {
                Test.setCurrentPageReference(new PageReference('Page.CreateDailyCollectionReview'));
                System.currentPageReference().getParameters().put('id', objDamacCollection.Id);
                CreateDailyCollectionReview objDamac = new CreateDailyCollectionReview();
                PageReference result = objDamac.exportToExcel();
                System.assertNotEquals(null, result);
            }       	
        	
    }
    
    @isTest
    static void testAdmLoggedIn(){
        Id recordTypeId = Schema.SObjectType.Daily_Collection_Review__c.getRecordTypeInfosByDeveloperName().get('Daily_Review').getRecordTypeId();
        List<User> userList = createUsers();
        insert userList;
        DCR_Mapping__c dcrMappingInstance = new DCR_Mapping__c(Name = userList[1].Id , DCR_Initiator__c = userList[1].Id ,
                                                        Level_1_Approver__c = userList[2].Id ,
                                                        CRE_Team_Ids__c = userList[3].Id +','+userList[4].Id);
        insert dcrMappingInstance;
        Profiles_allowed_to_view_DCR__c prSetting = new Profiles_allowed_to_view_DCR__c(Name = 'Profiles', Profile_Names__c = 'System Administrator');
        insert prSetting;
        Daily_Collection_Review__c objDamacCollection = new Daily_Collection_Review__c();
        objDamacCollection.Meeting_Date__c = System.Today();
        objDamacCollection.DCR_initiator__c = userList[1].Id;
        objDamacCollection.CRE_Manager__c = userList[1].Id;
        objDamacCollection.HOD__c = userList[2].Id;
        objDamacCollection.Review_Initiated_By_manager__c = true;
        objDamacCollection.RecordTypeId = recordTypeId;
        insert objDamacCollection;
        Daily_Collection_Review_Detail__c objDamacCollectionDetail = new Daily_Collection_Review_Detail__c(DCR__c = objDamacCollection.Id);
        insert objDamacCollectionDetail;    
        
            System.runAs(userList[0]) {
                Test.setCurrentPageReference(new PageReference('Page.CreateDailyCollectionReview'));
                System.currentPageReference().getParameters().put('id', objDamacCollection.Id);
                CreateDailyCollectionReview objDamac = new CreateDailyCollectionReview();
                objDamac.createDCR();
                System.assertEquals(true, objDamac.isSysAdmin);
            }       	
        	
    }
   
}