global class DAMAC_ziwo_listener_Inbound {

    /*
        Name:ReadStreamingEventinboundCall
        Desc:Capture the Call Id and create task , If No Inquiry Found create one and assign to PC - PC Idenfied from DID
             and do chatter post to the PC
    */
    @remoteAction
    global static string ReadStreamingEventinbound( string callId , string customerNumber , string agentNumber , string direction ){
        try{
            List <String> didList = new List <String> ();
            for (Ziwo_Dids__c did  :[SELECT Name FROM Ziwo_Dids__c WHERE Name != NULL]) {
                didList.add (did.name);
            }
            
            Boolean didFound = false;
            if (didList.size () > 0) {
                if (didList.contains (agentNumber)) {
                    didFound = true;
                }
            }
            String userIdAndMobilePhone = getTheuser(agentNumber);
            String userId = userIdAndMobilePhone.substringBefore('|');  
            
            System.debug (userid);
            System.debug (callId +'--'+customerNumber +'==='+agentNumber +'==='+direction);
            
            String AgentMobilePhoneforSMS =  userIdAndMobilePhone.substringAfter('|');
            
            Map <String, Ziwo_Object_Settings__mdt> objectMappings = new Map <String, Ziwo_Object_Settings__mdt> ();
            for (Ziwo_Object_Settings__mdt mapping :[SELECT object_API__c, Default_Phone_Field_API__c,
                                                                Name_Field_API__c, Phone_Field_API__c 
                                                        FROM Ziwo_Object_Settings__mdt WHERE object_API__c != NULL]) 
            {
                objectMappings.put (mapping.object_API__c, mapping);
            }
            
            Boolean recordFound = false;
            // Find Account based on the customer Number
            List <Account> toFindAcc = new List <Account> ();
            list<Inquiry__c> toFindInq = new list<inquiry__c>();
            List <Calling_list__c> toFindCallingList = new List <Calling_list__c> ();
            if (objectMappings.containsKey ('Account') && didFound == false) {
                String customerNumberAcc = customerNumber;
                //.removeStart ('00');
                String query = 'SELECT '+objectMappings.get ('Account').Name_Field_API__c
                                +' FROM Account WHERE (';
                
                String phoneAPIs = objectMappings.get ('Account').Phone_Field_API__c;
                List <String> phoneFields = new List <String> ();
                if (objectMappings.get ('Account').Default_Phone_Field_API__c != null) {
                    query += objectMappings.get ('Account').Default_Phone_Field_API__c +' =: customerNumberAcc OR ';
                }
                if (phoneApis != null) {
                    if (phoneAPIS.contains (',')) {
                        for (String key : phoneAPIS.split (',')) {
                            phoneFields .add (key);
                            query += key+' =: customerNumberAcc OR ';
                        }
                    }
                    else 
                        query += phoneAPIS+' =: customerNumberAcc OR ';
                }                 
                query = query.removeEND ('OR ')+')';
                
                query += ' LIMIT 1';
                System.debug (query);
                                        
                toFindAcc = Database.query (query); 
            
                if (toFindAcc.size () > 0) {//If account Found
                
                    recordFound = true;
                    id taskid = createtask(callId,direction, toFindAcc[0].id, userId);
                        
                    chatterPostCallout (userId,AgentMobilePhoneforSMS,taskid ,direction, String.valueOF(toFindAcc[0].get (objectMappings.get ('Account').Name_Field_API__c)));     
                    //doChatterPost(userId,AgentMobilePhoneforSMS,taskid,direction,toFindInq[0].Full_Name__c);
                    
                    if(agentNumber != ''){
                        sendSMS(userId, AgentMobilePhoneforSMS, taskId, String.valueOF(toFindAcc[0].get (objectMappings.get ('Account').Name_Field_API__c)));
                    }
                }
            }
            // If no Account found, then check for Inquiry   
            if (recordFound == false) {
                id inquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_RT).getRecordTypeId();
                if (didFound == true ) {
                    inquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_PreInq_RT ).getRecordTypeId();
                }
                if (objectMappings.containsKey ('Inquiry__c')) {
                    String query = 'SELECT '+objectMappings.get ('Inquiry__c').Name_Field_API__c
                                +' FROM Inquiry__c WHERE Is_Owner_Queue__c = FALSE AND RecordTypeId =: inquiryRecordTypeId AND Owner_DID__c =: agentNumber AND (';
                
                    String phoneAPIs = objectMappings.get ('Inquiry__c').Phone_Field_API__c;
                    List <String> phoneFields = new List <String> ();
                    if (objectMappings.get ('Inquiry__c').Default_Phone_Field_API__c != null) {
                        query += objectMappings.get ('Inquiry__c').Default_Phone_Field_API__c +' =: customerNumber OR ';
                    }
                    if (phoneApis != null) {
                        if (phoneAPIS.contains (',')) {
                            for (String key : phoneAPIS.split (',')) {
                                phoneFields .add (key);
                                query += key+' =: customerNumber OR ';
                            }
                        }
                        else 
                            query += phoneAPIS+' =: customerNumber OR ';
                    }                 
                    query = query.removeEND ('OR ')+')';
                    
                    query += ' LIMIT 1';
                    System.debug (query);
                                            
                    toFindInq = Database.query (query); 
                    if( toFindInq.size() > 0 ){ // If Inquiry is Found
                        recordFound = true;
        
                        id taskid = createtask(callId,direction, toFindInq[0].id, userId);
                        
                        chatterPostCallout (userId,AgentMobilePhoneforSMS,taskid ,direction, String.valueOF(toFindInq[0].get (objectMappings.get ('Inquiry__c').Name_Field_API__c)));     
                        //doChatterPost(userId,AgentMobilePhoneforSMS,taskid,direction,toFindInq[0].Full_Name__c);
                        
                        if(agentNumber != ''){
                            sendSMS(userId, AgentMobilePhoneforSMS, taskId, String.valueOF(toFindInq[0].get (objectMappings.get ('Inquiry__c').Name_Field_API__c)));
                        }
                        
                    } 
                }
            } 
            // If no Account, Inquiry found, then check for Calling List
            if (recordFound == false && didFound == false) {
                String customerNumberCallingList = customerNumber;
                //.removeStart ('00');
                if (objectMappings.containsKey ('Calling_List__c')) {
                    String query = 'SELECT '+objectMappings.get ('Calling_List__c').Name_Field_API__c
                                +' FROM Calling_List__c WHERE (';
                
                    String phoneAPIs = objectMappings.get ('Calling_List__c').Phone_Field_API__c;
                    List <String> phoneFields = new List <String> ();
                    if (objectMappings.get ('Calling_List__c').Default_Phone_Field_API__c != null) {
                        query += objectMappings.get ('Calling_List__c').Default_Phone_Field_API__c +' =: customerNumberCallingList OR ';
                    }
                    if (phoneApis != null) {
                        if (phoneAPIS.contains (',')) {
                            for (String key : phoneAPIS.split (',')) {
                                phoneFields .add (key);
                                query += key+' =: customerNumberCallingList OR ';
                            }
                        }
                        else 
                            query += phoneAPIS+' =: customerNumberCallingList OR ';
                    }                 
                    query = query.removeEND ('OR ')+')';
                    
                    query += ' LIMIT 1';
                    System.debug (query);
                                            
                    toFindCallingList = Database.query (query ); 
                    if( toFindCallingList.size() > 0 ){ // If Inquiry is Found
                        recordFound = true;
        
                        id taskid = createtask(callId,direction, toFindInq[0].id, userId);
                        
                        chatterPostCallout (userId,AgentMobilePhoneforSMS,taskid ,direction, String.valueOF(toFindCallingList[0].get (objectMappings.get ('Calling_List__c').Name_Field_API__c)));     
                        //doChatterPost(userId,AgentMobilePhoneforSMS,taskid,direction,toFindInq[0].Full_Name__c);
                        
                        if(agentNumber != ''){
                            sendSMS(userId, AgentMobilePhoneforSMS, taskId, String.valueOF(toFindCallingList[0].get (objectMappings.get ('Calling_List__c').Name_Field_API__c)));
                        }
                        
                    }
                }
            }
            if (recordFound == false) { // If Account, Inquiry, Calling list is not Found
                id inquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_RT).getRecordTypeId();
                
                // Create Inquiry and assign to RM based on DID
                DAMAC_Constants.skip_InquiryTrigger = true;
                Inquiry__c newInq = new Inquiry__c();
                    newInq.First_Name__c = 'New Incoming'; 
                    newInq.last_name__c = 'Number';
                    newInq.ownerId = userId;
                try {
                    newInq.Campaign__c = [SELECT ID FROM Campaign__c WHERE Campaign_Type_New__c = 'Ziwo Inbound' Order BY CreatedDate DESC LIMIT 1].ID;
                } catch (Exception e) {}
            
                newInq.recordTypeId = inquiryRecordTypeId ;
                newInq.Mobile_CountryCode__c = MobileCountryShortCode.MobileCountryShortCodeOutput(customerNumber);            
                newInq.Mobile_Phone__c = customerNumber;
                newInq.Mobile_Phone_Encrypt__c = UtilityHelperCls.encryptMobile(newInq.Mobile_Phone__c);
                newInq.Primary_Contacts__c = 'Mobile Phone';
                newInq.email__c = 'noemail@ziwo.com';
                newInq.Inquiry_source__c = 'Inbound';
                insert newInq;
                
                id taskid = createtask(callId ,direction,newInq.id,userId);
                
                chatterPostCallout (userId,AgentMobilePhoneforSMS,taskid ,direction, 'New customer' );
                    
                //doChatterPost(userId,AgentMobilePhoneforSMS,taskid ,direction,'New customer' );
                if(agentNumber != ''){
                    sendSMS(userId,AgentMobilePhoneforSMS ,taskId, 'New customer');
                }
                
            }
            return 'success';
        }
        catch(Exception e){
            return e.getMessage()+'-'+e.getLineNumber();
        }
    }
    
    // Method to check of environement is Production ORG or not
    public static String IsProductionOrg() { 
        Organization org = [select IsSandbox from Organization where Id =:UserInfo.getOrganizationId()];
        return (org.IsSandbox == true) ? 'https://test.salesforce.com': 'https://login.salesforce.com';
    }
    
    // Method to initiate the chatter post from site guest user 
    @Future (Callout = true)
    public static void chatterPostCallout (String userId, String agentMobilePhoneforSMS, String taskId, String direction, String type) {
        Ziwo_Settings__c settings = Ziwo_Settings__c.getInstance (UserInfo.getUserID ());
        
        
        Http h = new HTTP ();
        HTTPresponse res = new HTTPResponse ();
        
        String loginURL = IsProductionOrg();
        HttpRequest session = new HTTPRequest ();
        session.setEndpoint (loginURL +'/services/oauth2/token?grant_type=password&client_id='
                                +settings.SF_Client_Id__c+'&client_secret='+settings.SF_Client_Secret__c
                                +'&username='+settings.SF_user_name__c+'&password='+settings.SF_User_password__c);
        session.setMethod ('POST');
        if (!Test.isRunningTest ())
            res = h.send (session);
        else {
            res.setStatusCode(200);
            res.setBody ('{"access_token" : "test", "instance_url" : "https://test.com"}');
        }
        if (res.getStatusCode () == 200) {
            Map <String, Object> response = (Map <String, Object>) JSON.deserializeUntyped(res.getBody ());
            String accessToken = String.valueOf (response.get ('access_token'));
            String instanceURL = String.valueOf (response.get ('instance_url'));
            res = new HTTPResponse ();
            
            String reqBody = '{"userId" : "'+userId+'", "agentMobilePhoneforSMS" : "'+agentMobilePhoneforSMS+'", "taskId" : "'+taskId
                            +'", "direction" : "'+direction+'", "type" : "'+type+'"}';
            
            HttpRequest req = new HTTPRequest ();
            req.setEndpoint (instanceURL+'/services/apexrest/ziwoChatterPOST');        
            req.setBody (reqBody);
            
            req.setHeader ('Authorization', 'OAuth '+accesstoken);
            req.setMethod ('POST');
            req.setTimeOut (120000);
            req.setHeader('Content-Type', 'application/json');
            if (!Test.isRunningTest())
                res = h.send (req);
            else {
                RestRequest request = new RestRequest();
                request.requestUri ='https://test.salesforce.com/services/apexrest/ziwoChatterPOST';
                request.httpMethod = 'POST';
                request.requestBody = Blob.valueof(reqBody);
                RestContext.request = request;
                DAMAC_ChatterPost.chatterPOST ();
            }
        }
    }
    
    /*
        Name:getTheuser
        Desc:Get the User Id Based on his DID of the call 
        Assumption: All PCs will hava a DID tagged to them on the User record
    */
    public static string getTheuser(string agentNumber){
        string toReturn = '';
        user u = [select id,Ziwo_DID__c,MobilePhone from user where Ziwo_DID__c=:agentNumber LIMIT 1 ];
        toReturn = u.id+'|'+u.MobilePhone;
        return toReturn;
    }
    
    
    
    /*
        Name:createtask
        Desc:To crate task under the inquiry assigned to a PC
    */
    
    public static id createtask(string callId,string direction,string ParentId,string userId){        
        
        task t = new task();
        t.CrtObjectId__c = callId;        
        t.whatid = ParentId;//Map Inquiry
        t.ownerId = userId; //Map Inquiry
        
        if(direction == 'inbound'){
            t.Activity_Type_3__c = 'Call-Inbound'; 
            t.subject = 'Call - Inbound';
        }
        if(direction == 'outbound'){
            t.Activity_Type_3__c = 'Call - Outbound'; 
            t.subject = 'Call - Outbound';
        }
        t.event_type__c = 'Ziwo Inbound';
        upsert t CrtObjectId__c;
        return t.id;        
    }
    
    /*
        Name:SendSMS
        Desc: Send SMS for Incoming Calls
    */

    @future(callout = true)
    global static void sendSMS(id userId,string mobilePhone , id taskId,string custName){
        Secure_SMS__mdt msgSettings = new Secure_SMS__mdt ();
        msgSettings = [SELECT User_Name__c, Password__c, Endpoint__c, Sender_Name__c FROM Secure_SMS__mdt WHERE Label = 'SMS_SingleAPI'];

        //string mobilePhone = '971543114112';
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        mobilePhone = mobilePhone.removeStart('00');
        
        //Added by Charan on 2nd Nov 2020
        mobilePhone = mobilePhone.removeStart('+');
        string senderName = system.label.Common_Sender_Name;
        if(mobilePhone.startsWith('971'))
            senderName = System.Label.UAE_Sender_Name;
        if(mobilePhone.startsWith('966'))
            senderName = System.Label.Saudi_Sender_Name;
        
        string smsMessage = 'You have a New Incoming Call from '+custName+', you can wrapup the call by clicking the link '+baseUrl+'/'+taskid;
        String reqBody = '{"MobileNumbers" : "'+mobilePhone+'", "Message" : "'+smsMessage+'", '
                            +'"SenderName" : "'+senderName +'", "CallbackQueryString" : "InquiryId='+taskid+'","ReportRequired":false}';
        String endPoint = msgSettings.Endpoint__c+'?UserName='+msgSettings.User_Name__c+'&Password='+msgSettings.Password__c;
        system.debug(reqBody);
        HttpRequest req = new HttpRequest ();
        req.setEndPoint (endPoint);
        req.setMethod ('POST');
        req.setBody (reqBody);
        req.setTimeout(120000);
        req.setHeader ('Content-Type', 'application/JSON');
        
        HTTp http = new HTTP ();
        
        HttpResponse res = new HttpResponse ();
        if (!Test.isRunningTest ()) {
            res = http.send (req);
        }

    
    }
    
    /*
        Name:doChatterPost
        Desc:Create a chatter Post Mentioning the user so that he gets Notified and can wrap up the call
    */
    public static void doChatterPost(id userid , string agentMobile , id taskId, string direction,string custName){
        Custom_Chatter_Feed__c chatterFeed = new Custom_Chatter_Feed__c ();
        try {
            chatterFeed = [SELECT Name FROM Custom_Chatter_Feed__c WHERE OwnerId =: userId LIMIT 1];
        } catch (Exception e) {}
        upsert chatterFeed;
        
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
        mentionSegmentInput.id = userId;
        messageBodyInput.messageSegments.add(mentionSegmentInput);
        
        String codeSnippet = '\n New '+direction+' call from '+custName+' is completed';
        
        
        textSegmentInput.text = codeSnippet;
        messageBodyInput.messageSegments.add(textSegmentInput);
        
        feedItemInput.body = messageBodyInput;
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        ConnectApi.LinkCapabilityInput linkInput = new ConnectApi.LinkCapabilityInput();
        //linkInput.url = '/'+taskId;
        //linkInput.url = baseUrl+'/apex/ziwo_inbound_wrapup?id='+taskId;
        linkInput.url='https://damacholding--c.eu18.visual.force.com/apex/ziwo_inbound_wrapup?id='+taskId;
        linkInput.urlName = 'Wrap up the call';
        
        ConnectApi.FeedElementCapabilitiesInput feedElementCapabilitiesInput = new ConnectApi.FeedElementCapabilitiesInput();
        feedElementCapabilitiesInput.link = linkInput;
        feedItemInput.capabilities = feedElementCapabilitiesInput;
        
        
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = chatterFeed.Id;
               
        if (!Test.isRunningTest ()){ 
            ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
        }
        
    }
}