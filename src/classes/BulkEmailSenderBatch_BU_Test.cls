@isTest
public class BulkEmailSenderBatch_BU_Test {

    @isTest
    static void test1() {

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account(LastName = 'Test Account',Party_ID__c = '63062',RecordtypeId = rtId,Send_Bulk_Email__c = true,Email__pc= 'test@mailinator.com',Email__c = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        //String fmAmount = String.valueOf(Decimal.valueOf(Label.Robo_Call_FM_Outstanding_Amount) + 100.00);
        Booking_unit__c objBU = new Booking_Unit__c (Booking__c = booking.Id,
                                                     Owner__c = account.Id
                                                     ,Send_Bulk_Email__c =true
                                                     , Registration_Status_Code__c ='xx'
                                                     ,Tenant__c = account.Id
                                                     ,Send_Tenant_Email__c = true
                                                     , DLDN_Notice__c ='afa'
                                                     );
        insert objBU;
        
        String strContents = '{!Unit_Name__c},{!Property_Name_Inventory__c},{!District__c},{!Property_Country__c},'+
                            '{!Customer_Name__c},{!FM_Outstanding_Amount__c},{!SurveyCx},'+
                            '{!Booking_Unit__c.Primary_Buyer_s_Name__c},{!Booking_Unit__c.Unit_Name__c},'+
                            '{!Booking_Unit__c.Property_Name_Inventory__c},{!Booking_Unit__c.Beneficiary_Name__c},'+
                            '{!Booking_Unit__c.Beneficiary_Bank__c},{!Booking_Unit__c.Bank_Branch__c},'+
                            '{!Booking_Unit__c.Beneficiary_Number__c},{!Booking_Unit__c.Bank_Swift__c},'+
                            '{!Booking_Unit__c.IBAN_Num__c},{!Booking_Unit__c.DLDN_Notice__c}';
        
        List<Booking_unit__c> bookingUnitList = [SELECT Id,
                                                        Unit_Name__c,
                                                        Property_Name_Inventory__c,
                                                        District__c,
                                                        Property_Country__c,
                                                        Customer_Name__c,
                                                        FM_Outstanding_Amount__c,
                                                        SurveyCx__c,
                                                        Primary_Buyer_s_Name__c,
                                                        Beneficiary_Bank__c,
                                                        Bank_Branch__c,
                                                        Beneficiary_Name__c,
                                                        Beneficiary_Number__c,
                                                        Bank_Swift__c,
                                                        IBAN_Num__c,
                                                        Booking__r.Account__r.Name,
                                                        DLDN_Notice__c
                                                  FROM  Booking_unit__c
                                                  LIMIT 1
                                                        ];

        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        Test.startTest();
        Database.executeBatch( new BulkEmailSenderBatch_BU() );
        BulkEmailSenderBatch_BU bulkEmailSenderBatch = new BulkEmailSenderBatch_BU();
        bulkEmailSenderBatch.replaceMergeFields(bookingUnitList[0],strContents);
        Test.stopTest();
    }
    
    @isTest
    static void test2() {

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account(LastName = 'Test Account',Party_ID__c = '63062',RecordtypeId = rtId,Send_Bulk_Email__c = true,Email__pc = 'test@mailinator.com',Email__c = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        //String fmAmount = String.valueOf(Decimal.valueOf(Label.Robo_Call_FM_Outstanding_Amount) + 100.00);
        Booking_unit__c objBU = new Booking_Unit__c (Booking__c = booking.Id,
                                                     Owner__c = account.Id
                                                     ,Send_Bulk_Email__c =true
                                                     , Registration_Status_Code__c ='xx'
                                                     , Unit_Name__c = 'Test'
                                                     ,District__c = 'Test'
                                                     ,Property_Country__c = 'Test'
                                                     ,SurveyCx__c = 'Test',
                                                     Tenant__c = account.Id,
                                                     Send_Tenant_Email__c = true
                                                     , DLDN_Notice__c ='afa'
                                                     );
        insert objBU;
        

        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        String strContents = '{!Unit_Name__c},{!Property_Name_Inventory__c},{!District__c},{!Property_Country__c},'+
                            '{!Customer_Name__c},{!FM_Outstanding_Amount__c},{!SurveyCx},'+
                            '{!Booking_Unit__c.Primary_Buyer_s_Name__c},{!Booking_Unit__c.Unit_Name__c},'+
                            '{!Booking_Unit__c.Property_Name_Inventory__c},{!Booking_Unit__c.Beneficiary_Name__c},'+
                            '{!Booking_Unit__c.Beneficiary_Bank__c},{!Booking_Unit__c.Bank_Branch__c},'+
                            '{!Booking_Unit__c.Beneficiary_Number__c},{!Booking_Unit__c.Bank_Swift__c},'+
                            '{!Booking_Unit__c.IBAN_Num__c},{!Booking_Unit__c.DLDN_Notice__c}';
        
        List<Booking_unit__c> bookingUnitList = [SELECT Id,
                                                        Unit_Name__c,
                                                        Property_Name_Inventory__c,
                                                        District__c,
                                                        Property_Country__c,
                                                        Customer_Name__c,
                                                        FM_Outstanding_Amount__c,
                                                        SurveyCx__c,
                                                        Primary_Buyer_s_Name__c,
                                                        Beneficiary_Bank__c,
                                                        Bank_Branch__c,
                                                        Beneficiary_Name__c,
                                                        Beneficiary_Number__c,
                                                        Bank_Swift__c,
                                                        IBAN_Num__c,
                                                        Booking__r.Account__r.Name,
                                                        DLDN_Notice__c
                                                  FROM  Booking_unit__c
                                                  LIMIT 1
                                                        ];
        Test.startTest();
        BulkEmailSenderBatch_BU bulkEmailSenderBatch = new BulkEmailSenderBatch_BU();
        bulkEmailSenderBatch.replaceMergeFields(bookingUnitList[0],strContents);
        Test.stopTest();
    }
}