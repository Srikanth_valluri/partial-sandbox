/**
 * @File Name          : GenerateDPInvoiceDrawloopDocumentBatch.cls
 * @Description        : This Batch is used to create attachment by fetching drawloop cover 
 *                       letter for DPInvoice.
 * @Author             : Dipika Rajput
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 1/16/2020, 1:43:00 PM
 * @Modification Log   : 
 *==============================================================================================
 * Ver         Date                     Author                    Modification
 * 
 *==============================================================================================
 * 1.0    6/25/2019                   Dipika Rajput           Initial Version
**/
public without sharing class GenerateDPInvoiceDrawloopDocumentBatch implements Database.Batchable<sObject>,
            Database.AllowsCallouts, Database.Stateful {
    
    private String query;
    public String SERVER_URL;
    public String SESSION_ID;
    public String recordId;
    public Database.Querylocator start( Database.BatchableContext bc ) {
        
        // Calling Login method to access drawloop. 
        login();  
        recordId =  Label.GenerateDPInvoiceDrawloopDocumentBatchLabel.split('-',2)[1];
        query = ' SELECT Id,'+ 
                ' Cover_Letter__c, '+
                ' SOA__c,'+
                ' TAX_Invoice__c,'+
                ' BookingUnits__r.Registration_ID__c,'+
                'Type_of_Milestone__c,Milestone__c,' +
                ' COCD_Letter__c '+
                ' FROM DP_Invoices__c '+
                ' Where '  ;
                 if(Label.GenerateDPInvoiceDrawloopDocumentBatchLabel.split('-',2)[0] == 'ON'){
                    query +=' ID = :recordId';
                }
                else{
                     query +=' Cover_Letter__c = NULL AND Type_of_Milestone__c != NUll AND Milestone__c != NUll';
                }
               
                
        System.debug('Dp Invoice query ::: '+Database.getQueryLocator(query));
        return Database.getQueryLocator(query);
    }
    public void execute( Database.BatchableContext bc, list<DP_Invoices__c> dpInvoiceRecords ) {
        system.debug('SESSION_ID*********'+SESSION_ID);
        system.debug('dpInvoiceRecords*********'+dpInvoiceRecords);
        // Call a method to create attachment of cover letter using drawLoop for DP Invoice.
        createAttachmentOfCoverLetter(dpInvoiceRecords);
    }
    public void finish( Database.BatchableContext bc ) {
        
        
    }

    // This Method is used to create attachment of cover letter on dp invoice.
    public void createAttachmentOfCoverLetter(list<DP_Invoices__c> dpInvoiceRecords){
        Riyadh_Rotana_Drawloop_Doc_Mapping__c dateCS 
            = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getValues('Cover Letter Date Based');
        Riyadh_Rotana_Drawloop_Doc_Mapping__c constructionCS 
            = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance('Cover Letter Construction Based');
        if(SESSION_ID != null && SESSION_ID != ''){
            system.debug('dp Invoice records scope*********'+dpInvoiceRecords);
            for( DP_Invoices__c dpInvoiceObj : dpInvoiceRecords ){
            try {
                Loop.loopMessage lm = new Loop.loopMessage();
                Map<string, string> variables = new map<String,String>(); 
                System.debug(' dateCS : ' + dateCS);
                System.debug(' constructionCS : ' + constructionCS);
                    System.debug(' out side for ');
                    String typeOfMilestone = dpInvoiceObj.Type_of_Milestone__c.toLowercase();
                    
                    if(String.isNotBlank(typeOfMilestone) 
                        &&typeOfMilestone.contains('date') && dateCS != Null
                        && String.isNotBlank(dateCS.Delivery_Option_Id__c)
                        && String.isNotBlank(dateCS.Drawloop_Document_Package_Id__c) ){
                        System.debug(' for Date ');
                        variables.put('deploy', dateCS.Delivery_Option_Id__c); 
                        lm.requests.add(new Loop.loopMessage.loopMessageRequest(
                                        dpInvoiceObj.Id,
                                        dateCS.Drawloop_Document_Package_Id__c,
                                        variables)
                        );

                    }else if ( String.isNotBlank(typeOfMilestone) 
                                && typeOfMilestone.contains('construction') && constructionCS != Null
                                && String.isNotBlank(constructionCS.Delivery_Option_Id__c)
                                && String.isNotBlank(constructionCS.Drawloop_Document_Package_Id__c) ){
                        System.debug(' for Construction ');
                        variables.put('deploy', constructionCS.Delivery_Option_Id__c); 
                        lm.requests.add(new Loop.loopMessage.loopMessageRequest(
                                        dpInvoiceObj.Id,
                                        constructionCS.Drawloop_Document_Package_Id__c,
                                        variables)
                        );
                        
                    }
                
                
                    String response = lm.sendAllRequests();
                    system.debug('== response ==' +response );
                    lm.batchNotification = Loop.loopMessage.Notification.ON_COMPLETE;
                    system.debug('== lm.batchNotification ==' +lm.batchNotification );
                   
                } 
                catch (exception e){
                    system.debug('== e exception==' +e.getMessage() );
                }
            }
        }
    }
    //Method used for login purpose to get the session id
    public void login(){
        Admin_Login_for_Drawloop__c mc = Admin_Login_for_Drawloop__c.getOrgDefaults();
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://' + mc.Domain__c + '.salesforce.com/services/Soap/u/28.0');
        system.debug('mc.Domain__c'+mc.Domain__c);
        system.debug('mc.Username__c'+mc.Username__c);
        system.debug('mc.Password__c'+mc.Password__c);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', '""');
        request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + mc.Username__c + '</username><password>' + mc.Password__c + '</password></login></Body></Envelope>');

        HttpResponse response = h.send(request);
        system.debug('----Response Body-----'+response.getBody());

        Dom.XmlNode resultElmt = response.getBodyDocument().getRootElement()
        .getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/')
        .getChildElement('loginResponse','urn:partner.soap.sforce.com')
        .getChildElement('result','urn:partner.soap.sforce.com');

        SERVER_URL = resultElmt.getChildElement('serverUrl','urn:partner.soap.sforce.com').getText().split('/services')[0];
        SESSION_ID = resultElmt.getChildElement('sessionId','urn:partner.soap.sforce.com').getText();

        system.debug('--SERVER_URL---'+SERVER_URL);
        system.debug('--SESSION_ID---'+SESSION_ID);
        if(Test.isRunningTest()){
            SESSION_ID = 'none';
        }
    }

}