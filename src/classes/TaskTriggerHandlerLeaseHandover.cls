public with sharing class TaskTriggerHandlerLeaseHandover {

    public void taskForExternalUser(list<Task> listTask) {
        Integer counterVal;
        set<Id> setCaseId = new set<Id>(); 
        set<Id> setTaskCaseId = new set<Id>(); 
        set<Id> setTaskForCallout1 = new set<Id>();
        set<Id> setTaskForCallout2 = new set<Id>();  
        set<String> setTaskForUser = new set<String>();
        
        for (Task objTask: listTask){
            if (String.valueOf(objTask.WhatId).startsWith('500')){
                setCaseId.add(objTask.WhatId);
            }
        }
        
        if(Label.UsersOutsideSalesforce != null) {
            setTaskForUser.addAll(Label.UsersOutsideSalesforce.split(','));
            system.debug('setTaskForUser==='+setTaskForUser);
            counterVal = 1;
            for(Task taskInst: listTask){
                if(taskInst.Process_Name__c != null
                && taskInst.Assigned_User__c != null
                && taskInst.WhatId != null
                && setTaskForUser.contains(taskInst.Assigned_User__c)
                && String.valueOf(taskInst.WhatId).startsWith('500')){
                    if(counterVal <= 100) {
                        setTaskForCallout1.add(taskInst.Id);
                    }
                    else {
                        setTaskForCallout2.add(taskInst.Id);
                    }
                    setTaskCaseId.add(taskInst.WhatId);
                    counterVal++;
                }
            }
            system.debug('setTaskCaseId==='+setTaskCaseId);
            system.debug('setTaskForCallout1==='+setTaskForCallout1);
            system.debug('setTaskForCallout2==='+setTaskForCallout2);
            if(!setTaskForCallout1.isEmpty() && !setTaskCaseId.isEmpty()) {
                createTaskOnIPMS(setTaskForCallout1, setTaskCaseId);
            }
            if(!setTaskForCallout2.isEmpty() && !setTaskCaseId.isEmpty()) {
                createTaskOnIPMS(setTaskForCallout2, setTaskCaseId);
            }
        }
    }
    
    public void checkTaskCompletion(list<Task> listTask, map<Id, Task> mapOldTask) {
        Integer counterVal;
        set<String> setTaskForUser = new set<String>();
        set<Id> setTaskForIPMSUpdate = new set<Id>();
        set<Id> setTaskCaseId = new set<Id>();
        list<Task> lstCancelledTask = new list<Task>();
        //get the assigned user for IPMS 
        if(Label.UsersOutsideSalesforce != null) {
            setTaskForUser.addAll(Label.UsersOutsideSalesforce.split(','));
            system.debug('setTaskForUser==='+setTaskForUser);
        }
        counterVal = 1;
        for(Task taskInst: listTask) {
            //check if Update IPMS is true then push task to IPMS
             if(taskInst.Process_Name__c != null
                && taskInst.Assigned_User__c != null
                && taskInst.WhatId != null
                && taskInst.Process_Name__c.contains('Lease Handover')
                && setTaskForUser.contains(taskInst.Assigned_User__c)
                && String.valueOf(taskInst.WhatId).startsWith('500')
                && ((!taskInst.Pushed_to_IPMS__c && taskInst.Update_IPMS__c 
                && taskInst.Update_IPMS__c != mapOldTask.get(taskInst.Id).Update_IPMS__c)
                || (taskInst.Status != mapOldTask.get(taskInst.Id).Status))){
                 if(counterVal <= 100) {
                    setTaskForIPMSUpdate.add(taskInst.Id);
                    setTaskCaseId.add(taskInst.WhatId);
                } else {
                    createTaskOnIPMS(setTaskForIPMSUpdate, setTaskCaseId);
                    setTaskCaseId = new set<Id>();
                    setTaskForIPMSUpdate = new set<Id>();
                    setTaskForIPMSUpdate.add(taskInst.Id);
                    setTaskCaseId.add(taskInst.WhatId);
                }
                counterVal++;
            } 
            if(taskInst.WhatId != null && String.valueOf(taskInst.WhatId).startsWith('500')
            && taskInst.Status.equalsIgnoreCase('Cancelled') && taskInst.Status != mapOldTask.get(taskInst.Id).Status
            && taskInst.Process_Name__c != null && taskInst.Process_Name__c.equalsIgnoreCase('Lease Handover') ){
                lstCancelledTask.add(taskInst);
            }           
        }
        if(!lstCancelledTask.isEmpty()){
            taskForExternalUser(lstCancelledTask);
        }
    }
    
    
    @future(callout=true)
    public static void createTaskOnIPMS(set<Id> setTaskForCallout, set<Id> setTaskCaseId) {
         try{
            system.debug('Task cfreation called********************');
            list<Error_Log__c> listErrorLog = new list<Error_Log__c>();
            
            map<Id, Case> mapCaseDetails = new map<Id, Case>([select Id, Status, Account.Party_ID__c,
                                Booking_Unit__r.Registration_ID__c, CaseNumber, Relationship_with_Seller__c,
                                Case_Type__c, Buyer_Type__c, Booking_Unit__r.Unit_Details__c,
                                Booking_Unit__r.Inventory__r.Property_City__c, Account.First_Name__c,
                                Booking_Unit__r.Inventory__r.Property_Status__c, Account.Last_Name__c,
                                Booking_Unit__r.Inventory__r.Property_Country__c, Account.Nationality__c,
                                Booking_Unit__r.Inventory__r.Property__r.DIFC__c, Account.Passport_Number__c,
                                Buyer__r.Passport_Expiry_Date__c, Buyer__r.IPMS_Buyer_ID__c, Seller__r.Party_ID__c,
                                Seller__r.FirstName, Seller__r.LastName, Seller__r.Nationality__pc,
                                AccountId, Purpose_of_POA__c, Monthly_Rent__c, Completed_Milestones_Percent__c,
                                Parking_Details_JSON__c, Security_Cheque_Amount__c, Lease_Commencement_Date__c, 
                                Lease_End_Date__c 
                                from Case where Id IN: setTaskCaseId]);
        
        list<Task> lsTaskToUpdate = new list<Task>();
        for(Task objTask : [Select t.WhoId
                                 , t.WhatId
                                 , t.Type
                                 , t.Status
                                 , t.OwnerId
                                 , t.Id
                                 , t.Subject
                                 , t.CreatedDate
                                 , t.Description
                                 , t.Assigned_User__c
                                 , t.ActivityDate
                                 , t.Owner.Name
                                 , t.Task_Error_Details__c
                                 , t.Pushed_to_IPMS__c
                                 , t.Process_Name__c
                                 , t.Document_URL__c                                 
                                 From Task t
                                 where t.Id IN : setTaskForCallout]){

                list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5> listObjBeans = 
                new list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5>();
                
                TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objHeaderBean = 
                new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
                String reqNo = string.valueOf(System.currentTimeMillis());
                objHeaderBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
                system.debug('param id*****'+mapCaseDetails.get(objTask.WhatId).CaseNumber);
                objHeaderBean.ATTRIBUTE1 = 'HEADER';
                objHeaderBean.ATTRIBUTE2 = objTask.Process_Name__c;
                objHeaderBean.ATTRIBUTE3 = objTask.Status;
                system.debug('objTask.Status*****'+objTask.Status);
                objHeaderBean.ATTRIBUTE4 = objTask.Owner.Name;
                objHeaderBean.ATTRIBUTE5 = mapCaseDetails.get(objTask.WhatId).Account.Party_ID__c;
                system.debug('party*****'+mapCaseDetails.get(objTask.WhatId).Account.Party_ID__c);
                objHeaderBean.ATTRIBUTE6 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c;
                system.debug('regid*****'+mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c);
                objHeaderBean.ATTRIBUTE7 = String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
                system.debug('date*****'+String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()));
                objHeaderBean.ATTRIBUTE8 = '';
                objHeaderBean.ATTRIBUTE9 = '';
                objHeaderBean.ATTRIBUTE10 = '';//set new Buyer Id here instead of objTask.WhatId;
                objHeaderBean.ATTRIBUTE11 = objTask.WhatId;
                objHeaderBean.ATTRIBUTE12 = '';
                objHeaderBean.ATTRIBUTE13 = '';
                objHeaderBean.ATTRIBUTE14 = '';
                objHeaderBean.ATTRIBUTE15 = '';
                objHeaderBean.ATTRIBUTE16 = '';
                objHeaderBean.ATTRIBUTE17 = '';
                objHeaderBean.ATTRIBUTE18 = '';
                objHeaderBean.ATTRIBUTE19 = '';
                objHeaderBean.ATTRIBUTE20 = '';
                objHeaderBean.ATTRIBUTE21 = '';
                objHeaderBean.ATTRIBUTE22 = '';
                objHeaderBean.ATTRIBUTE23 = '';
                objHeaderBean.ATTRIBUTE24 = '';
                objHeaderBean.ATTRIBUTE25 = '';
                objHeaderBean.ATTRIBUTE26 = '';
                objHeaderBean.ATTRIBUTE27 = '';
                objHeaderBean.ATTRIBUTE28 = '';
                objHeaderBean.ATTRIBUTE29 = '';
                objHeaderBean.ATTRIBUTE30 = '';
                objHeaderBean.ATTRIBUTE31 = '';
                objHeaderBean.ATTRIBUTE32 = '';
                objHeaderBean.ATTRIBUTE33 = '';
                objHeaderBean.ATTRIBUTE34 = '';
                if (objTask.Document_URL__c != null) {
                    objHeaderBean.ATTRIBUTE35 = objTask.Document_URL__c;
                }
                if (mapCaseDetails != null && mapCaseDetails.get(objTask.WhatId) != null) {
                    objHeaderBean.ATTRIBUTE36 = mapCaseDetails.get(objTask.WhatId).Purpose_of_POA__c;
                }
                objHeaderBean.ATTRIBUTE37 = '';
                objHeaderBean.ATTRIBUTE38 = '';
                objHeaderBean.ATTRIBUTE39 = '';
                //objHeaderBean.ATTRIBUTE40 = '';
                objHeaderBean.ATTRIBUTE41 = '';
                objHeaderBean.ATTRIBUTE42 = '';
                objHeaderBean.ATTRIBUTE43 = '';
                objHeaderBean.ATTRIBUTE44 = '';
                objHeaderBean.ATTRIBUTE45 = '';
                objHeaderBean.ATTRIBUTE46 = '';
                objHeaderBean.ATTRIBUTE47 = '';
                objHeaderBean.ATTRIBUTE48 = '';
                objHeaderBean.ATTRIBUTE49 = '';
                objHeaderBean.ATTRIBUTE50 = '';
                listObjBeans.add(objHeaderBean);
                
                // create TASK bean
                String taskOwner;
                if (objTask.Assigned_User__c == 'Legal') {
                    taskOwner = 'LEGAL';
                 } else if (objTask.Assigned_User__c == 'Finance') {
                     taskOwner = 'FINANCEEXECUTIVE';
                 } else if (objTask.Assigned_User__c == 'AR') {
                     taskOwner = 'FINANCEEXECUTIVE';
                 } else if (objTask.Assigned_User__c == 'Contracts') {
                     taskOwner = 'CONTRACTSHEAD';
                 } else if (objTask.Assigned_User__c == 'Compliance') {
                     taskOwner = 'COMPLIANCEHEAD';
                 } else if (objTask.Assigned_User__c == 'Compliance Manager') {
                     taskOwner = 'COMPLIANCEMANAGER';
                 } else if (objTask.Assigned_User__c == 'CDC') {
                     taskOwner = 'CDCEXECUTIVE';
                 } else if (objTask.Assigned_User__c == 'Sales Admin') {
                     taskOwner = 'SALESADMINMANAGER';
                 } else if (objTask.Assigned_User__c == 'FM') {
                     taskOwner = 'FACILITIESAPPROVAL';
                 } else if (objTask.Assigned_User__c == 'BD') {
                     taskOwner = 'BUSINESSDEVELOPMENT';
                 }
                 
                 TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objTaskBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
                objTaskBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
                objTaskBean.ATTRIBUTE1 = 'TASK';
                objTaskBean.ATTRIBUTE2 = objTask.Subject;
                system.debug('subject*****'+objTask.Subject);
                objTaskBean.ATTRIBUTE3 = objTask.Status;
                system.debug('subject*****'+objTask.Status);
                objTaskBean.ATTRIBUTE4 = taskOwner;
                objTaskBean.ATTRIBUTE5 = '';
                objTaskBean.ATTRIBUTE6 = '';
                objTaskBean.ATTRIBUTE7 = String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
                system.debug('created date*****'+String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()));
                objTaskBean.ATTRIBUTE8 = objTask.Id;
                system.debug('task id*****'+objTask.Id);
                Datetime dt = objTask.ActivityDate;
                objTaskBean.ATTRIBUTE9 = String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase());
                system.debug('due date*****'+String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase()));
                objTaskBean.ATTRIBUTE10 = '';
                objTaskBean.ATTRIBUTE11 = objTask.WhatId;
                objTaskBean.ATTRIBUTE12 = objTask.Subject;
                objTaskBean.ATTRIBUTE13 = objTask.Status;
                objTaskBean.ATTRIBUTE14 = objTask.Type;
                objTaskBean.ATTRIBUTE15 = objTask.Description;
                objTaskBean.ATTRIBUTE16 = objTask.Assigned_User__c;
                objTaskBean.ATTRIBUTE17 = '';
                objTaskBean.ATTRIBUTE18 = '';
                objTaskBean.ATTRIBUTE19 = '';
                objTaskBean.ATTRIBUTE20 = '';
                objTaskBean.ATTRIBUTE21 = '';
                objTaskBean.ATTRIBUTE22 = '';
                objTaskBean.ATTRIBUTE23 = '';
                objTaskBean.ATTRIBUTE24 = '';
                objTaskBean.ATTRIBUTE25 = '';
                objTaskBean.ATTRIBUTE26 = '';
                objTaskBean.ATTRIBUTE27 = '';
                objTaskBean.ATTRIBUTE28 = '';
                objTaskBean.ATTRIBUTE29 = '';
                objTaskBean.ATTRIBUTE30 = '';
                objTaskBean.ATTRIBUTE31 = '';
                objTaskBean.ATTRIBUTE32 = '';
                objTaskBean.ATTRIBUTE33 = '';
                objTaskBean.ATTRIBUTE34 = '';
                objTaskBean.ATTRIBUTE35 = '';
                objTaskBean.ATTRIBUTE36 = '';
                objTaskBean.ATTRIBUTE37 = '';
                objTaskBean.ATTRIBUTE38 = '';
                objTaskBean.ATTRIBUTE39 = '';
                objTaskBean.ATTRIBUTE41 = '';
                objTaskBean.ATTRIBUTE42 = '';
                objTaskBean.ATTRIBUTE43 = '';
                objTaskBean.ATTRIBUTE44 = '';
                objTaskBean.ATTRIBUTE45 = '';
                objTaskBean.ATTRIBUTE46 = '';
                objTaskBean.ATTRIBUTE47 = '';
                objTaskBean.ATTRIBUTE48 = '';
                objTaskBean.ATTRIBUTE49 = '';
                objTaskBean.ATTRIBUTE50 = '';
                listObjBeans.add(objTaskBean);
                
                // create UNIT bean
                TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objUnitBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
                objUnitBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
                objUnitBean.ATTRIBUTE1 = 'UNITS';
                objUnitBean.ATTRIBUTE2 = objTask.Process_Name__c;
                objUnitBean.ATTRIBUTE3 = '';
                objUnitBean.ATTRIBUTE4 = '';
                objUnitBean.ATTRIBUTE5 = '';
                objUnitBean.ATTRIBUTE6 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c;
                system.debug('reg*****'+mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c);
                objUnitBean.ATTRIBUTE7 = '';
                objUnitBean.ATTRIBUTE8 = '';
                objUnitBean.ATTRIBUTE9 = '';
                objUnitBean.ATTRIBUTE10 = '';
                objUnitBean.ATTRIBUTE11 = '';
                objUnitBean.ATTRIBUTE12 = '';
                objUnitBean.ATTRIBUTE13 = '';
                objUnitBean.ATTRIBUTE14 = '';
                objUnitBean.ATTRIBUTE15 = '';
                objUnitBean.ATTRIBUTE16 = '';
                objUnitBean.ATTRIBUTE17 = '';
                objUnitBean.ATTRIBUTE18 = '';
                objUnitBean.ATTRIBUTE19 = '';
                objUnitBean.ATTRIBUTE20 = '';
                objUnitBean.ATTRIBUTE21 = '';
                objUnitBean.ATTRIBUTE22 = '';
                objUnitBean.ATTRIBUTE23 = '';
                objUnitBean.ATTRIBUTE24 = '';
                objUnitBean.ATTRIBUTE25 = '';
                objUnitBean.ATTRIBUTE26 = '';
                objUnitBean.ATTRIBUTE27 = '';
                objUnitBean.ATTRIBUTE28 = '';
                objUnitBean.ATTRIBUTE29 = '';
                objUnitBean.ATTRIBUTE30 = '';
                objUnitBean.ATTRIBUTE31 = '';
                objUnitBean.ATTRIBUTE32 = '';
                objUnitBean.ATTRIBUTE33 = '';
                objUnitBean.ATTRIBUTE34 = '';
                objUnitBean.ATTRIBUTE35 = '';
                objUnitBean.ATTRIBUTE36 = '';
                objUnitBean.ATTRIBUTE37 = '';
                objUnitBean.ATTRIBUTE38 = '';
                objUnitBean.ATTRIBUTE39 = '';
                objUnitBean.ATTRIBUTE41 = '';
                objUnitBean.ATTRIBUTE42 = '';
                objUnitBean.ATTRIBUTE43 = '';
                objUnitBean.ATTRIBUTE44 = '';
                objUnitBean.ATTRIBUTE45 = '';
                objUnitBean.ATTRIBUTE46 = '';
                objUnitBean.ATTRIBUTE47 = '';
                objUnitBean.ATTRIBUTE48 = '';
                objUnitBean.ATTRIBUTE49 = '';
                objUnitBean.ATTRIBUTE50 = '';
                listObjBeans.add(objUnitBean);
                
                TaskCreationWSDL.TaskHttpSoap11Endpoint objClass = new TaskCreationWSDL.TaskHttpSoap11Endpoint();
                String response = objClass.SRDataToIPMSMultiple('0001', 'CREATE_SR', 'SFDC', listObjBeans);
                system.debug('resp*****'+response);
                if(String.isNotBlank(response)) {
                    innerClass IC = (innerClass)JSON.deserialize(response, innerClass.class);
                    system.debug('IC*****'+IC);
                    if(IC.status.EqualsIgnoreCase('E')){
                        objTask.Pushed_to_IPMS__c = false;
                        objTask.Task_Error_Details__c = IC.status;
                        system.debug('objTask=='+objTask);
                        Error_Log__c objErr = createErrorLogRecord(mapCaseDetails.get(objTask.WhatId).Seller__c, mapCaseDetails.get(objTask.WhatId).Booking_Unit__c, objTask.WhatId);
                        objErr.Error_Details__c = IC.message;
                        listErrorLog.add(objErr);
                    }else{
                        objTask.Pushed_to_IPMS__c = true;
                    }
                }
                else {
                    Error_Log__c objErr = createErrorLogRecord(mapCaseDetails.get(objTask.WhatId).Seller__c, mapCaseDetails.get(objTask.WhatId).Booking_Unit__c, objTask.WhatId);
                    objErr.Error_Details__c = 'Error : No Response from IPMS for Task Creation';
                    listErrorLog.add(objErr);
                }
                lsTaskToUpdate.add(objTask);
            } // end of for loop
            
            if(!lsTaskToUpdate.isEmpty()){
                update lsTaskToUpdate;
            }
            if(!listErrorLog.isEmpty()) {
                insertErrorLog(listErrorLog);
            }     
        } catch (Exception ex){
            system.debug('**Exception***'+ex);
        }
    }
    
    public static Error_Log__c createErrorLogRecord(Id accId, Id bookingUnitId, Id caseId){
        Error_Log__c objErr = new Error_Log__c();
        objErr.Account__c = accId;
        objErr.Booking_Unit__c = bookingUnitId;
        objErr.Case__c = caseId;
        return objErr;
    }
    
    public static void insertErrorLog(list<Error_Log__c> listObjErr){
        try{
            insert listObjErr;
        }catch(Exception ex){
            //errorMessage = 'Error : '+ ex.getMessage();
            system.debug('Error Log ex*****'+ex);
        }
    }
     public class innerClass{
        public string message;
        public string status;
        
        public innerClass(){
            
        }
    }
}