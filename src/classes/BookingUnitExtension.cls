public with sharing class BookingUnitExtension {
    public String strCurrentACD                                {get; set;}
    public String strAreaVariation_sqft                        {get; set;}
    public String strAreaVariation_AED                         {get; set;}
    public String strOverdue                                   {get; set;}
    public String bookingUnitId                                {get; set;}
    public String strReraConstructionStatus                    {get; set;}
    public String strSpaAcd                                    {get; set;}
    public String strRecoveryFlag                              {get; set;}
    public String strRentalPool                                {get; set;}
    public String strLitigation                                {get; set;}
    public String strHandover                                  {get; set;} 
    public String strEarlyHandover                             {get; set;}
    public String strMortgage                                  {get; set;}
    public String strDldRegistration                           {get; set;}
    public String strCashcall                                  {get; set;}
    public String strAmountOverdue                             {get; set;}
    public String strAmountCollected                           {get; set;}
    public String strPdcOnHand                                 {get; set;}
    public String strLastPaymentDate                           {get; set;}
    public String strPaymentMade                               {get; set;}
    public String strCollectableAsPerRera                      {get; set;}
    public String strOverduesPerCollectableAmount              {get; set;}
    public String strPenalties                                 {get; set;}
    public String strParkings                                  {get; set;}
    public String strParkingBay                                {get; set;}
    public String strTypeOfParking                             {get; set;}
    public String strRentalPoolStartDate                       {get; set;}
    public String strRentalPoolEndDate                         {get; set;}
    public String strFMPCC                                     {get; set;}
    public String strDlpValidity                               {get; set;}
    public String strHandoverDate                              {get; set;}
    public String strEarlyHandoverDate                         {get; set;}
    public String strPDCResponse                               {get; set;}
    public List<Buyer__c> lstJointBuyers                       {get; set;}
    public List<pdcDetailsWrapper> lstPDCDetails               {get; set;} 
    public List<PaymentWrapper> paymentDetails                 {get; set;}  
    public String strPaymentResponse                           {get; set;}  
    public List<Error_Log__c> insertErrorLog ;                                   
    Decimal decArea;
    public String errorMessage;
    public Booking_Unit__c currentBookingUnit;
    public Decimal totalOfDebit                                  {get;set;}
    public Decimal totalOfCredit                                 {get;set;}
    public BookingUnitExtension(ApexPages.StandardController controller) {
        //init();
    }   

    public void init(){
        strCurrentACD = ''; 
        strAreaVariation_sqft = '';
        strAreaVariation_AED = '';
        strOverdue = '';
        strReraConstructionStatus = '';
        strSpaAcd = '';
        strRecoveryFlag = 'No';
        strRentalPool = 'No';
        strLitigation = 'No';
        strHandover = 'No';
        strEarlyHandover = 'No';
        strMortgage = '';
        decArea = null;
        strDldRegistration = '';
        strCashcall ='';                                  
        strAmountOverdue = '';                              
        strAmountCollected ='';
        strPdcOnHand ='';
        strLastPaymentDate ='';                            
        strPaymentMade ='';                               
        strCollectableAsPerRera ='';                      
        strOverduesPerCollectableAmount = '';  
        strParkings ='';                                   
        strParkingBay ='';                                
        strTypeOfParking ='';                             
        strRentalPoolStartDate ='';                        
        strRentalPoolEndDate ='';                         
        strFmPcc ='';                                     
        strDlpValidity ='';                                
        strHandoverDate ='';                              
        strEarlyHandoverDate =''; 
        strPDCResponse = '';
        strPaymentResponse = '';
        lstJointBuyers = new List<Buyer__c>();

        bookingUnitId = ApexPages.currentPage().getParameters().get('id');
        bookingUnitId = String.valueOf(bookingUnitId).substring(0, 15);
        System.Debug('=====bookingUnitId : ' + bookingUnitId);

        /*Booking_Unit__c currentBookingUnit = [ SELECT Id,
                                                      Name,
                                                      Account_Id__c,
                                                      Registration_ID__c,
                                                      Property_Name__c,
                                                      Unit_Name__c,
                                                      Requested_Price__c,
                                                      Selling_Price__c,
                                                      Booking__c,
                                                      Booking__r.Id,
                                                      Booking__r.Name
                                                 FROM Booking_Unit__c
                                                WHERE Id = :bookingUnitId
                                                  //AND Registration_ID__c != null
                                                  ];*/
        currentBookingUnit =  getBookingUnit(bookingUnitId);                                                  
        System.Debug('=====currentBookingUnit : ' + currentBookingUnit);

        for (Buyer__c objBuyer : [SELECT Id, 
                                         Name, 
                                         First_Name__c, 
                                         Last_Name__c, 
                                         Account__c, 
                                         Buyer_ID__c,
                                         Booking__c, 
                                         Primary_Buyer__c,
                                         Account__r.Party_Id__c
                                    FROM Buyer__c
                                   WHERE Account__c != null 
                                     AND Booking__c = :currentBookingUnit.Booking__r.Id
                                     AND Primary_Buyer__c = false]
        ) {
            System.Debug('=====objBuyer : ' + objBuyer);
            lstJointBuyers.add(objBuyer);
        }
        System.Debug('=====lstJointBuyers : ' + lstJointBuyers);

        try {
        UnitDetailsService.BookinUnitDetailsWrapper objBookinUnitDetailsWrapper = UnitDetailsService.getBookingUnitDetails(currentBookingUnit.Registration_ID__c);
        System.Debug('=====objBookinUnitDetailsWrapper : ' + objBookinUnitDetailsWrapper);
        if (objBookinUnitDetailsWrapper != null ){
            if(objBookinUnitDetailsWrapper.strCurrentACD != null) {
                strCurrentACD = objBookinUnitDetailsWrapper.strCurrentACD;
            }

            if (objBookinUnitDetailsWrapper.strAreaVariation_sqft != null) {
                strAreaVariation_sqft = objBookinUnitDetailsWrapper.strAreaVariation_sqft;
            }

            if (objBookinUnitDetailsWrapper.strAreaVariation_AED != null) {
                strAreaVariation_AED = objBookinUnitDetailsWrapper.strAreaVariation_AED;
            }

            if (objBookinUnitDetailsWrapper.strOverdue != null) {
                strOverdue = objBookinUnitDetailsWrapper.strOverdue;
            }

            if (objBookinUnitDetailsWrapper.strReraConstructionStatus != null) {
                strReraConstructionStatus = objBookinUnitDetailsWrapper.strReraConstructionStatus;
            }
            if (objBookinUnitDetailsWrapper.strRecoveryFlag != null) {
                if(objBookinUnitDetailsWrapper.strRecoveryFlag == 'Y') {
                    strRecoveryFlag = 'Yes';
                }
            }
            if (objBookinUnitDetailsWrapper.strRentalPool != null) {
                if(objBookinUnitDetailsWrapper.strRentalPool == 'Y') {
                    strRentalPool = 'Yes';
                }
            }
            if (objBookinUnitDetailsWrapper.strLitigation != null) {
                if(objBookinUnitDetailsWrapper.strLitigation == 'Y') {
                    strLitigation = 'Yes';
                }
            }
            if (objBookinUnitDetailsWrapper.strHOFlag != null) {
                if (objBookinUnitDetailsWrapper.strHOFlag == 'Y'){
                    strHandover = 'Yes';
                } 
            }

            if (objBookinUnitDetailsWrapper.strEHOFlag != null) {
                if (objBookinUnitDetailsWrapper.strEHOFlag == 'Y'){
                    strEarlyHandover = 'Yes';
                } else if (objBookinUnitDetailsWrapper.strEHOFlag == 'N') {
                    strEarlyHandover = 'No';
                }
            }

            System.Debug('=====objBookinUnitDetailsWrapper.strAmountOverdue : ' + objBookinUnitDetailsWrapper.strAmountOverdue);
            if (objBookinUnitDetailsWrapper.strAmountOverdue != null) {
                /*if (objBookinUnitDetailsWrapper.strAmountOverdue == 'Y'){
                    strAmountOverdue = 'Yes';
                } else if (objBookinUnitDetailsWrapper.strAmountOverdue == 'N') {
                    strAmountOverdue = 'No';
                }*/
                strAmountOverdue = objBookinUnitDetailsWrapper.strAmountOverdue;
            }

            System.Debug('=====objBookinUnitDetailsWrapper.strAmountCollected : ' + objBookinUnitDetailsWrapper.strAmountCollected);
            if (objBookinUnitDetailsWrapper.strAmountCollected != null) {
                /*if (objBookinUnitDetailsWrapper.strAmountCollected == 'Y'){
                    strAmountCollected = 'Yes';
                } else if (objBookinUnitDetailsWrapper.strAmountCollected == 'N') {
                    strAmountCollected = 'No';
                }*/
                strAmountCollected = objBookinUnitDetailsWrapper.strAmountCollected;
            }

            System.Debug('=====objBookinUnitDetailsWrapper.strPenalties : ' + objBookinUnitDetailsWrapper.strPenalties);
            if (objBookinUnitDetailsWrapper.strPenalties != null) {
                /*if (objBookinUnitDetailsWrapper.strPenalties == 'Y'){
                    strPenalties = 'Yes';
                } else if (objBookinUnitDetailsWrapper.strPenalties == 'N') {
                    strPenalties = 'No';
                }*/
                strPenalties = objBookinUnitDetailsWrapper.strPenalties;
            }

            if (objBookinUnitDetailsWrapper.strFMPCC != null) {
                strFMPCC = objBookinUnitDetailsWrapper.strFMPCC;
            }

            /*if (objBookinUnitDetailsWrapper.strRentalPoolStartDate != null) {
                strRentalPoolStartDate = objBookinUnitDetailsWrapper.strRentalPoolStartDate;
            }

            if (objBookinUnitDetailsWrapper.strRentalPoolEndDate != null) {
                strRentalPoolEndDate = objBookinUnitDetailsWrapper.strRentalPoolEndDate;
            }*/

            if (objBookinUnitDetailsWrapper.strNoOfExistingParkingWithUnit != null) {
                strParkings = objBookinUnitDetailsWrapper.strNoOfExistingParkingWithUnit;
            }
            
            if (objBookinUnitDetailsWrapper.strBayNoOfExistingParkingWithUnit != null) {
                strParkingBay = objBookinUnitDetailsWrapper.strBayNoOfExistingParkingWithUnit;
            }

            if (objBookinUnitDetailsWrapper.strTypeOfExistingParkingWithUnit != null) {
                strTypeOfParking = objBookinUnitDetailsWrapper.strTypeOfExistingParkingWithUnit;
            }
            
            if (objBookinUnitDetailsWrapper.strOriginalACD != null) {
                strSpaAcd = objBookinUnitDetailsWrapper.strOriginalACD;
            }
        }
        } catch (Exception ex) {
                system.debug('Exception in Unit Details Service: ' + ex.getMessage());
        }
        /*PDC Response*/
        //strPDCResponse = assignmentEndpoints.fetchPDCDetails(currentBookingUnit);
        //System.Debug('======strPDCResponse=====' + strPDCResponse);
        //Boolean PDCDetailFlag = ParsePDCDetails(currentBookingUnit);
        System.Debug('======lstPDCDetails=====' + lstPDCDetails);
        //ParseCustomerPayment(currentBookingUnit);
        //System.Debug('======paymentDetails=====' + paymentDetails);
    }

    public void ParsePDCDetails() {
        bookingUnitId = ApexPages.currentPage().getParameters().get('id');
        bookingUnitId = String.valueOf(bookingUnitId).substring(0, 15);

        /*Booking_Unit__c objUnit = [ SELECT Id,
                                                      Name,
                                                      Account_Id__c,
                                                      Registration_ID__c,
                                                      Property_Name__c,
                                                      Unit_Name__c,
                                                      Requested_Price__c,
                                                      Selling_Price__c,
                                                      Booking__c,
                                                      Booking__r.Id,
                                                      Booking__r.Name
                                                 FROM Booking_Unit__c
                                                WHERE Id = :bookingUnitId
                                                  //AND Registration_ID__c != null
                                                  ];*/
        Booking_Unit__c objUnit =  getBookingUnit(bookingUnitId);

        Boolean PDCFlag = false;
        lstPDCDetails = new list<pdcDetailsWrapper>();
        insertErrorLog = new List<Error_Log__c>();
        strPDCResponse = assignmentEndpoints.fetchPDCDetails(objUnit);
        system.debug('strPDCResponse==='+strPDCResponse);
        if(String.isNotBlank(strPDCResponse)){
            map<String,Object> mapDeserializePDC = (map<String,Object>)JSON.deserializeUntyped(strPDCResponse);
            if(mapDeserializePDC.get('status') == 'S'){
                String StringPDCData = JSON.serialize(mapDeserializePDC.get('data'));
                lstPDCDetails = 
                    (List<pdcDetailsWrapper>)JSON.deserialize(StringPDCData, List<pdcDetailsWrapper>.class);
                system.debug('lstPDCDetails==='+lstPDCDetails);
                PDCFlag = true;
                //strPDCResponse = 'Success';
                strPDCResponse = '';
            }
            else if(mapDeserializePDC.get('status') == 'E'){
                errorMessage = 'Error : '+mapDeserializePDC.get('message');
                Error_Log__c objErr = createErrorLogRecord(objUnit.Id);
                objErr.Error_Details__c = errorMessage;
                //insert objErr;   
                insertErrorLog.add(objErr);
                strPDCResponse = 'Server Down... Please try after sometime.';
            }
        }
        else {
            errorMessage = 'Error : No Response from IPMS for PDC Details';
            Error_Log__c objErr = createErrorLogRecord(objUnit.Id);
            objErr.Error_Details__c = errorMessage;
            //insert objErr;
            insertErrorLog.add(objErr);
            strPDCResponse = 'Error : No Response from IPMS for PDC Details.';
        }

        if(insertErrorLog != null || insertErrorLog.size() >0) {
            insert insertErrorLog;
        }
        //return PDCFlag;
    }

    public void ParseCustomerPayment() {
        bookingUnitId = ApexPages.currentPage().getParameters().get('id');
        bookingUnitId = String.valueOf(bookingUnitId).substring(0, 15);

        /*Booking_Unit__c objUnit = [ SELECT Id,
                                          Name,
                                          Account_Id__c,
                                          Registration_ID__c,
                                          Property_Name__c,
                                          Unit_Name__c,
                                          Requested_Price__c,
                                          Selling_Price__c,
                                          Booking__c,
                                          Booking__r.Id,
                                          Booking__r.Name
                                     FROM Booking_Unit__c
                                    WHERE Id = :bookingUnitId
                                      //AND Registration_ID__c != null
                                      ];*/
        Booking_Unit__c objUnit =  getBookingUnit(bookingUnitId);

        system.debug('===ParseCustomer PaymentMethod===' + objUnit);
        paymentDetails = new list<PaymentWrapper>();
        insertErrorLog = new List<Error_Log__c>();
        strPaymentResponse = CustomerPaymentMade.getCustomerPaymentMade(objUnit.Registration_ID__c);
        system.debug('direct response ParseCustomerPayment strPaymentResponse==='+strPaymentResponse);
        if(String.isNotBlank(strPaymentResponse)){
            map<String,Object> mapDeserializePayment = (map<String,Object>)JSON.deserializeUntyped(strPaymentResponse);
            if(mapDeserializePayment.get('status') == 'S'){
                String stringPaymentData = JSON.serialize(mapDeserializePayment.get('data'));
                paymentDetails = 
                    (List<PaymentWrapper>)JSON.deserialize(stringPaymentData, List<PaymentWrapper>.class);
                system.debug('paymentDetails==='+paymentDetails);
                //PDCFlag = true;
                //strPaymentResponse = 'Success';
                strPaymentResponse = '';
            }
            else if(mapDeserializePayment.get('status') == 'E'){
                errorMessage = 'Error : '+mapDeserializePayment.get('message');
                Error_Log__c objErr = createErrorLogRecord(objUnit.Id);
                objErr.Error_Details__c = errorMessage;
                //insert objErr;   
                insertErrorLog.add(objErr);
                strPaymentResponse = 'Server Down... Please try after sometime.';
            }
        }
        else {
            errorMessage = 'Error : No Response from IPMS.';
            Error_Log__c objErr = createErrorLogRecord(objUnit.Id);
            objErr.Error_Details__c = errorMessage;
            //insert objErr;
            insertErrorLog.add(objErr);
            strPaymentResponse = 'Error : No Response from IPMS.';
        }
        system.Debug('======strPaymentResponse : ' + strPaymentResponse);
        if(insertErrorLog != null || insertErrorLog.size() >0) {
            insert insertErrorLog;
        }
        /**/
        totalOfDebit = 0.0;
        totalOfCredit = 0.0;
        for(PaymentWrapper wrapperObj : paymentDetails){
            System.debug('wrapperObj::'+wrapperObj);
            System.debug('wrapperObj::'+wrapperObj.ATTRIBUTE9);
             if(String.isNotBlank(wrapperObj.ATTRIBUTE9)){
                 totalOfDebit += Double.valueOf(wrapperObj.ATTRIBUTE9);
             }
             if(String.isNotBlank(wrapperObj.ATTRIBUTE10)){
                totalOfCredit += Double.valueOf(wrapperObj.ATTRIBUTE10);
             }
        }
        totalOfDebit = totalOfDebit.setScale(2);
        totalOfCredit = totalOfCredit.setScale(2);
        System.debug('totalOfDebit::'+totalOfDebit);
        System.debug('totalOfCredit::'+totalOfCredit);
    } 

    public class pdcDetailsWrapper {
        public String ATTRIBUTE1 {get;set;}
        public String ATTRIBUTE2 {get;set;}
        public String ATTRIBUTE3 {get;set;}
        public String ATTRIBUTE4 {get;set;}
        public String ATTRIBUTE5 {get;set;}
        public String ATTRIBUTE6 {get;set;}
        public String ATTRIBUTE7 {get;set;}
        public String ATTRIBUTE8 {get;set;}

        public pdcDetailsWrapper(){
            
        }
    }

    public class PaymentWrapper {
        public String ATTRIBUTE1 {get;set;}
        public String ATTRIBUTE2 {get;set;}
        public String ATTRIBUTE3 {get;set;}
        public String ATTRIBUTE4 {get;set;}
        public String ATTRIBUTE5 {get;set;}
        public String ATTRIBUTE6 {get;set;}
        public String ATTRIBUTE7 {get;set;}
        public String ATTRIBUTE8 {get;set;}
        public String ATTRIBUTE9 {get;set;}
        public String ATTRIBUTE10 {get;set;}
        public String ATTRIBUTE11 {get;set;}
        
        public paymentWrapper(){
            
        }
    }

    public Error_Log__c createErrorLogRecord(string buID){
        Error_Log__c obj = new Error_Log__c();
        obj.Booking_Unit__c = buID;
        return obj;
    } 

    public Booking_Unit__c getBookingUnit(String bookingUnitId){
        Booking_Unit__c currentBookingUnit = [ SELECT Id,
                                                      Name,
                                                      Account_Id__c,
                                                      Registration_ID__c,
                                                      Property_Name__c,
                                                      Unit_Name__c,
                                                      Requested_Price__c,
                                                      Selling_Price__c,
                                                      Booking__c,
                                                      Booking__r.Id,
                                                      Booking__r.Name,
                                                      Booking__r.Account__c,
                                                      Booking__r.Account__r.Email__c,
                                                      Booking__r.Account__r.Name
                                                 FROM Booking_Unit__c
                                                WHERE Id = :bookingUnitId
                                                  ];
        return currentBookingUnit;
    }

}