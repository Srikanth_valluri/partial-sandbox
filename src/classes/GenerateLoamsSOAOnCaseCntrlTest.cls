/**
 * @File Name          : GenerateLoamsSOAOnCaseCntrlTest.cls
 * @Description        : Test class for the apex class GenerateLoamsSOAOnCaseCntrl
 * @Author             : Raveena Jain
 * @Group              :
 * @Last Modified By   : Raveena Jain
 * @Last Modified On   : 
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0        27/09/2020            Raveena Jain                 Initial Version
**/
@isTest
public class GenerateLoamsSOAOnCaseCntrlTest  {
    static final String TEST_URL = 'https://test.damacgroup.com/test.pdf';
    
    static testMethod void testGenerateFMPartySOA1() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
        }
        insert lstBookingUnits;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Charge Reset').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Booking_Unit__c=lstBookingUnits[0].Id;
        insert objCase;
        
        Test.startTest();
        PageReference popPage = Page.GenerateLoamsSOAForCase;
        Test.setCurrentPage(popPage);
        popPage.getParameters().put('Id',String.valueOf(objCase.Id));
        ApexPages.StandardController sc = new ApexPages.standardController( objCase );
        GenerateLoamsSOAOnCaseCntrl objController = new GenerateLoamsSOAOnCaseCntrl(sc);
        
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);
        
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));
        
        String responseBody;
        
        objController.generateFMSOA();
        Test.stopTest();
    }
    
    static testMethod void testGenerateFMPartySOA2() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Account
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
        }
        insert lstBookingUnits;
        
		Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Charge Reset').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Booking_Unit__c=lstBookingUnits[0].Id;
        insert objCase;
        
        Test.startTest();
        PageReference popPage = Page.GenerateLoamsSOAForCase;
        Test.setCurrentPage(popPage);
        popPage.getParameters().put('Id',String.valueOf(objCase.Id));
        ApexPages.StandardController sc = new ApexPages.standardController( objCase );
        GenerateLoamsSOAOnCaseCntrl  objController = new GenerateLoamsSOAOnCaseCntrl (sc);
        
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);
        
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));
        
        String responseBody;
        
        objController.generateFMSOA();
        Test.stopTest();
    }   
    
    static testMethod void test_GenerateFMPropertyWiseSOA() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = 'AD/201/2015';
        }
        insert lstBookingUnits;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Charge Reset').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Booking_Unit__c=lstBookingUnits[0].Id;
        insert objCase;
        
        Test.startTest();
        PageReference popPage = Page.GenerateLoamsSOAForCase;
        Test.setCurrentPage(popPage);
        popPage.getParameters().put('Id',String.valueOf(objCase.Id));
        ApexPages.StandardController sc = new ApexPages.standardController( objCase );
        GenerateLoamsSOAOnCaseCntrl objController = new GenerateLoamsSOAOnCaseCntrl(sc);
        
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);
        
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));
        
        String responseBody;
        objController.generateFMPartySOA();
        Test.stopTest();
    }
    
    static testMethod void test_GenerateAndEmailFMUnitSOA() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__pc = 'test@test.com';
        objAcc.Email__c = 'test1@test.com';
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = 'AD/201/2015';
        }
        insert lstBookingUnits;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Charge Reset').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Booking_Unit__c=lstBookingUnits[0].Id;
        insert objCase;
        
        Test.startTest();
        PageReference popPage = Page.GenerateLoamsSOAForCase;
        Test.setCurrentPage(popPage);
        popPage.getParameters().put('Id',String.valueOf(objCase.Id));
        ApexPages.StandardController sc = new ApexPages.standardController( objCase );
        GenerateLoamsSOAOnCaseCntrl objController = new GenerateLoamsSOAOnCaseCntrl(sc);
        
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);
        
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));
        
        String responseBody;
        
        objController.generateAndEmailFMUnitSOA();
        Test.stopTest();
    }
    
    static testMethod void test_GenerateAndEmailFMUnitSOA1() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__pc = 'test@test.com';
        objAcc.Email__c = 'test1@test.com';
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = 'AD/201/2015';
        }
        insert lstBookingUnits;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Charge Reset').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Booking_Unit__c=lstBookingUnits[0].Id;
        insert objCase;
        
        Test.startTest();
        PageReference popPage = Page.GenerateLoamsSOAForCase;
        Test.setCurrentPage(popPage);
        popPage.getParameters().put('Id',String.valueOf(objCase.Id));
        ApexPages.StandardController sc = new ApexPages.standardController( objCase );
        GenerateLoamsSOAOnCaseCntrl objController = new GenerateLoamsSOAOnCaseCntrl(sc);
        
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);
        
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));
        
        String responseBody;
        objController.CancelGeneration();
        Test.stopTest();
    }
    
    
    static testMethod void test_GenerateAndEmailFMUnitSOA2() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__pc = 'test@test.com';
        objAcc.Email__c = 'test1@test.com';
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = 'AD/201/2015';
        }
        insert lstBookingUnits;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Charge Reset').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Booking_Unit__c=lstBookingUnits[0].Id;
        insert objCase;
        
        Test.startTest();
        PageReference popPage = Page.GenerateLoamsSOAForCase;
        Test.setCurrentPage(popPage);
        popPage.getParameters().put('Id',String.valueOf(objCase.Id));
        ApexPages.StandardController sc = new ApexPages.standardController( objCase );
        GenerateLoamsSOAOnCaseCntrl objController = new GenerateLoamsSOAOnCaseCntrl(sc);
        
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);
        
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));
        
        String responseBody;
        GenerateDocForFMOnBUCntrl.errorLogger('Test',null,lstBookingUnits[0].Id,null);
        Test.stopTest();
    }
    
    static testMethod void test_GenerateAndEmailFMUnitSOA3() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__pc = 'test@test.com';
        objAcc.Email__c = 'test1@test.com';
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Unit_Name__c = 'AD/201/2015';
        }
        insert lstBookingUnits;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Charge Reset').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Booking_Unit__c=lstBookingUnits[0].Id;
        insert objCase;
        
        Test.startTest();
        PageReference popPage = Page.GenerateLoamsSOAForCase;
        Test.setCurrentPage(popPage);
        popPage.getParameters().put('Id',String.valueOf(objCase.Id));
        ApexPages.StandardController sc = new ApexPages.standardController( objCase );
        GenerateLoamsSOAOnCaseCntrl objController = new GenerateLoamsSOAOnCaseCntrl(sc);
        
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);
        
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));
        
        String responseBody;
        objController.CancelGeneration();
        Test.stopTest();
    }
    
    static testMethod void test_GenerateAndEmailFMPartySOA1() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__pc = 'test@test.com';
        objAcc.Email__c = 'test1@test.com';
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = 'AD/201/2015';
            objUnit.Party_ID__c = '65432';
        }
        insert lstBookingUnits;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Charge Reset').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Booking_Unit__c=lstBookingUnits[0].Id;
        insert objCase;
        
        Test.startTest();
        PageReference popPage = Page.GenerateLoamsSOAForCase;
        Test.setCurrentPage(popPage);
        popPage.getParameters().put('Id',String.valueOf(objCase.Id));
        ApexPages.StandardController sc = new ApexPages.standardController( objCase );
        GenerateLoamsSOAOnCaseCntrl objController = new GenerateLoamsSOAOnCaseCntrl(sc);
        
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);
        
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));
        
        String responseBody;
        objController.generateAndEmailFMPartySOA();
        Test.stopTest();
    }
    
    static testMethod void test_GenerateAndEmailFMPartySOA2() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__pc = 'test@test.com';
        objAcc.Email__c = 'test1@test.com';
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = 'AD/201/2015';
        }
        insert lstBookingUnits;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Charge Reset').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Booking_Unit__c=lstBookingUnits[0].Id;
        insert objCase;
        
        Test.startTest();
        PageReference popPage = Page.GenerateLoamsSOAForCase;
        Test.setCurrentPage(popPage);
        popPage.getParameters().put('Id',String.valueOf(objCase.Id));
        ApexPages.StandardController sc = new ApexPages.standardController( objCase );
        GenerateLoamsSOAOnCaseCntrl objController = new GenerateLoamsSOAOnCaseCntrl(sc);
        
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);
        
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));
        
        String responseBody;
        objController.generateAndEmailFMPartySOA();
        Test.stopTest();
    }
    static testMethod void test_errorLogger() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__pc = 'test@test.com';
        objAcc.Email__c = 'test1@test.com';
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = 'AD/201/2015';
        }
        insert lstBookingUnits;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Charge Reset').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Booking_Unit__c=lstBookingUnits[0].Id;
        insert objCase;
        
        Test.startTest();
        PageReference popPage = Page.GenerateLoamsSOAForCase;
        Test.setCurrentPage(popPage);
        GenerateLoamsSOAOnCaseCntrl.errorLogger('test Error Message',null,lstBookingUnits[0].Id,objAcc.Id);
        Test.stopTest();
    }
}