/*
* Description - Class used as Utlity for main RentalPoolTerminationController class.
*
* Version            Date            Author            Description
* 1.0                                Ashish           Initial Draft
*/


public with sharing class RentalPoolTerminationUtility {
    
    public static map<Id, Booking_Unit__c> getAllRelatedBookingUnits( Id accountId ) {

        set<String> setActiveStatus = new set<String>();
        for( Booking_Unit_Active_Status__c unitStatusInst : Booking_Unit_Active_Status__c.getall().values() ) {
            if( String.isNotBlank( unitStatusInst.Status_Value__c ) ) {
                setActiveStatus.add( unitStatusInst.Status_Value__c );
            }
        }
        
        return new map<Id, Booking_Unit__c>([SELECT Id
                                                  , Registration_ID__c
                                                  , Unit_Details__c
                                                  , Inventory__r.Building_Code__c
                                                  , Registration_status__c
                                                  , Booking__r.Account__r.Nationality__c
                                                  , Booking__r.Account__r.AnnualRevenue
                                                  , Booking__r.Account__r.Email__pc
                                                  , Booking__r.Account__r.Email__c
                                                  , Booking__r.Account__r.IsPersonAccount
                                                  , Unit_Name__c
                                                  , Rental_Pool__c
                                                  , Registration_DateTime__c
                                                  , HOS_Name__c
                                                  , Manager_Name__c
                                                  , Property_Consultant__c
                                                  , CurrencyIsoCode 

                                                  //Unit Details
                                                  , Property_Name_Inventory__c
                                                  , Inventory__r.Property_City__c
                                                  , Inventory__r.Bedroom_Type__c
                                                  , Inventory__r.Unit_Type__c
                                                  , Inventory__r.Master_Community_EN__c
                                                  , Booking_Unit_Type__c
                                                  , Unit_Type__c
                                                  , Permitted_Use__c
                                                  , Inventory__r.Property__c
                                                  , Inventory__r.Property_Status__c
                                                  , Booking_Type__c
                                                  , Plot_Price__c
                                                  , Booking__r.CreatedDate
                                                  , Agreement_Date__c
                                                  , Inventory__r.Unit_Plan__c
                                                  , Inventory__r.Floor_Plan__c
                                                  , JOPD_Area__c
                                                  , ( SELECT Id
                                                            , OptionsName__c
                                                            , PromotionName__c
                                                            , SchemeName__c
                                                            , CampaignName__c
                                                        FROM Options__r
                                                    ORDER BY CreatedDate DESC
                                                       LIMIT 1 )
                                                  , ( SELECT Id
                                                            , Name
                                                         FROM Payment_Plans__r
                                                        WHERE Status__c IN ( 'Active', null )
                                                     ORDER BY CreatedDate DESC
                                                        LIMIT 1 )
                                                  
                                                  //Flags
                                                  , Mortgage__c
                                                  , DP_OK__c
                                                  , Doc_OK__c

                                                  //Unit Status
                                                  , OQOOD_Reg_Flag__c
                                                  , Early_Handover__c
                                                  , Handover_Flag__c
                                                  , PCC_Release__c

                                                  //Additional Details
                                                  , Area__c
                                                  , Area_Varied__c
                                                  , Unit_Selling_Price__c
                                                  , Inventory_Area__c

                                                  //?
                                                  , Bedroom_Type__c
                                                  , Token_Amount_val__c
                                                  , Rental_Pool_Agreement__c
                                                  , Token_Amount__c
                                                  , Requested_Token_Amount__c
                                                  , Booking__r.Account__c
                                                  , Penalty_Amount__c,Building_Name__c,District__c
                                               FROM Booking_Unit__c
                                              WHERE Booking__r.Account__c = :accountId
                                                //AND Handover_Complete__c = true
                                                AND Rental_Pool__c = true
                                                //AND Rental_Pool_Start_Date__c != null
                                                //AND Rental_Pool_End_Date__c = null 
                                                //AND Booking_Unit_Type__c = 'Service Apartment'
                                                AND Registration_status__c IN :setActiveStatus
                                                ] );
    }
    
    public static Case getCaseDetails( Id caseId ) {
        return [ SELECT Id
                      , CaseNumber
                      , Origin
                      , Status
                      , Subject
                      , Type
                      , RP_Terminate_Unit_From__c
                      , Mode_of_Collection__c
                      , Executed_RPA_Collected__c
                      , Is_CRF_Verified__c
                      , Is_POA_Verified__c
                      , IsPOA__c
                      , POA_Name__c
                      , POA_Relationship_with_Buyer__c
                      , POA_Expiry_Date__c
                      , POA_Issued_By__c
                      , Is_CRF_Uploaded__c
                      , Purpose_of_POA__c
                      , Project_Name__c
                      , RecordType.Name
                      , RecordType.DeveloperName
                      , AccountId
                      , Booking_Unit__c
                      , Booking_Unit__r.Registration_Id__c
                      , Booking_Unit__r.Unit_Name__c 
                      , OwnerId
                      , Rental_Pool_Termination_Status__c
                      , Final_Settlement_Amount__c
                      , Final_Termination_Date__c
                      , Account.Name
                      , Account.IsPersonAccount
                      , Account.Country__pc
                      , Account.City__pc
                      , Account.City__c
                      , Account.Country__c
                   FROM Case
                  WHERE Id = :caseId ];
    }
    
    public static Booking_Unit__c getUnitDetails( Id unitId ) {
        return [SELECT Id
                                                  , Registration_ID__c
                                                  , Unit_Details__c
                                                  , Inventory__r.Building_Code__c
                                                  , Registration_status__c
                                                  , Booking__r.Account__r.Nationality__c
                                                  , Booking__r.Account__r.AnnualRevenue
                                                  , Booking__r.Account__r.Email__pc
                                                  , Booking__r.Account__r.Email__c
                                                  , Booking__r.Account__r.IsPersonAccount
                                                  , Unit_Name__c
                                                  , Rental_Pool__c
                                                  , Registration_DateTime__c
                                                  , HOS_Name__c
                                                  , Manager_Name__c
                                                  , Property_Consultant__c
                                                  , CurrencyIsoCode 

                                                  //Unit Details
                                                  , Property_Name_Inventory__c
                                                  , Inventory__r.Property_City__c
                                                  , Inventory__r.Bedroom_Type__c
                                                  , Inventory__r.Unit_Type__c
                                                  , Inventory__r.Master_Community_EN__c
                                                  , Booking_Unit_Type__c
                                                  , Unit_Type__c
                                                  , Permitted_Use__c
                                                  , Inventory__r.Property__c
                                                  , Inventory__r.Property_Status__c
                                                  , Booking_Type__c
                                                  , Plot_Price__c
                                                  , Booking__r.CreatedDate
                                                  , Agreement_Date__c
                                                  , Inventory__r.Unit_Plan__c
                                                  , Inventory__r.Floor_Plan__c
                                                  , JOPD_Area__c
                                                  , ( SELECT Id
                                                            , OptionsName__c
                                                            , PromotionName__c
                                                            , SchemeName__c
                                                            , CampaignName__c
                                                        FROM Options__r
                                                    ORDER BY CreatedDate DESC
                                                       LIMIT 1 )
                                                  , ( SELECT Id
                                                            , Name
                                                         FROM Payment_Plans__r
                                                        WHERE Status__c IN ( 'Active', null )
                                                     ORDER BY CreatedDate DESC
                                                        LIMIT 1 )
                                                  
                                                  //Flags
                                                  , Mortgage__c
                                                  , DP_OK__c
                                                  , Doc_OK__c

                                                  //Unit Status
                                                  , OQOOD_Reg_Flag__c
                                                  , Early_Handover__c
                                                  , Handover_Flag__c
                                                  , PCC_Release__c

                                                  //Additional Details
                                                  , Area__c
                                                  , Area_Varied__c
                                                  , Unit_Selling_Price__c
                                                  , Inventory_Area__c

                                                  //?
                                                  , Bedroom_Type__c
                                                  , Token_Amount_val__c
                                                  , Rental_Pool_Agreement__c
                                                  , Token_Amount__c
                                                  , Requested_Token_Amount__c
                                                  , Booking__r.Account__c
                                                  , Penalty_Amount__c
                                                  , Building_Name__c,District__c
                                               FROM Booking_Unit__c
                                              WHERE Id = :unitId ] ;
    }
    
    public static list<SR_Attachments__c> getCaseAttachments( Id caseId ) {
        return [ SELECT Id
                      , Case__c
                      , Name
                      , View__c
                      , Attachment__c
                      , Type__c
                      , IsValid__c
                      , Attachment_URL__c
                   FROM SR_Attachments__c 
                  WHERE Case__c = :caseId
               ORDER BY CreatedDate DESC ];
    }
    
    public static Task getTerminationTaskOfCRE( Id caseId ) {
        return [ SELECT Id
                      , Status
                   FROM Task 
                  WHERE WhatId = :caseId
                    AND Subject LIKE '%Letter of Termination%'][0];
    }
}