public with sharing class ComplaintProcessHandler {

	@InvocableMethod
	public static void HandleProcess (List<Case> lstSR) {
		List<Case> sendNotificationToMACase = new List<Case>();
		List<Case> sendNotificationToCustomerCase = new List<Case>();
		for( Case caseObj :  lstSR){
			system.debug( 'caseObj.Status ======= ' + caseObj.Status);
			if( caseObj.Status != 'Closed' ){
				sendNotificationToMACase.add( caseObj );
			} else{
				sendNotificationToCustomerCase.add( caseObj );
			}
		}
			if( !sendNotificationToMACase.isEmpty() ){
				sendNotificationToMA( sendNotificationToMACase );	
			}

			if ( !sendNotificationToCustomerCase.isEmpty()  ){
				sendNotificationToCustomer( sendNotificationToCustomerCase );
			}
			
	}


	public static void sendNotificationToMA(List<Case> lstSR){

			List<String> toAddresses = new List<String>();
			//Set<String> accountIds = new Set<String>();
			//Set<String> bookingUnitIds = new Set<String>();
			Set<String> PCIds = new Set<String>();

			for( Case sr : lstSR){
					//accountIds.add(sr.AccountId);
					//bookingUnitIds.add(sr.Booking_Unit__c);
					PCIds.add(sr.OwnerId);
			}

			/*Map<Id, Account> AccountId_AccountMap = new Map<Id, Account>([SELECT Id, Address_Line_1__c, Address_Line_2__c,
																																		Address_Line_3__c, Address_Line_4__c, City__pc,
																																		State__c, Country__pc, Zip_Postal_Code__c,
																																		PersonMobilePhone, Phone, PersonEmail
																																		FROM Account WHERE Id IN: accountIds]);
			*/
			//this map will hold the unit details
			/*Map<Id, Booking_Unit__c> BookingUnitId_BookingUnitMap = new Map<Id, Booking_Unit__c>([SELECT Id, Property_Name_Inventory__c,
																								Unit_Location__c, Unit_Selling_Price__c,
																								(Select Id,Effective_From__c,Effective_To__c,
																								Payment_Term__c, Name FROM Payment_Plans__r),
																								Inventory__r.Unit_Location__r.Status__c
																								FROM Booking_Unit__c WHERE Id IN: bookingUnitIds ]);
			*/
			//this map will hold the property consultant details
			Map<Id, User> UserId_UserDetailMap = new Map<Id, User>([SELECT Id, Name, Email, Phone, MobilePhone, Address, Manager.Email, Manager.Name
																	FROM User where Id IN : PCIds]);

			List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
			for(Case sr : lstSR ){
					Messaging.SingleEmailMessage mailToDirector = new Messaging.SingleEmailMessage();

					mailToDirector.setSubject('New Complaint Case: '+sr.CaseNumber );
					
					if( Test.isRunningTest() ){
						mailToDirector.setToAddresses(new String[]{'test@test.com'});
					} else{
						toAddresses.add( UserId_UserDetailMap.get(sr.OwnerId).Manager.Email );
						if (sr.Origin == 'Portal'){
							toAddresses.add( UserId_UserDetailMap.get(sr.OwnerId).Email );
						}
						mailToDirector.setToAddresses( toAddresses );
					}
					
					
					//mailToDirector.setToAddresses(new String[]{'amit.joshi@eternussolutions.com'});
					String mailBodyDir = Test.isRunningTest() ?  '' : 'Hi '+UserId_UserDetailMap.get(sr.OwnerId).Manager.Name+',<br/>';
					mailBodyDir += '<p>There is a new Complaint request raised. The reference number is <b>'+sr.CaseNumber+'.</b></p>';
					mailBodyDir += 'Thank you,<br/>DAMAC Team.';
					mailToDirector.setHtmlBody( mailBodyDir );
					mails.add(mailToDirector);

					Messaging.SingleEmailMessage mailToCustomer = new Messaging.SingleEmailMessage();
					mailToCustomer.setSubject('Your Complaint Logged: '+sr.CaseNumber );

					if( Test.isRunningTest() ){
						mailToDirector.setToAddresses(new String[]{'test@test.com'});
					} else{
						mailToCustomer.setToAddresses(new String[]{UserId_UserDetailMap.get(sr.OwnerId).Email});
					}
					//mailToCustomer.setToAddresses(new String[]{'amit.joshi@eternussolutions.com'});
					String mailBodyCust = 'Hi,<br/>';
					mailBodyCust += '<p>Your complaint has been registered and our representative is working on it.</p>';
					mailBodyCust += 'The Reference Number is: <b>'+sr.CaseNumber+'</b></p><br/><br/>';
					mailBodyCust += 'Thank you,<br/>DAMAC Team.';
					mailToCustomer.setHtmlBody( mailBodyCust );
					mails.add(mailToCustomer);

					/*if (sr.Origin == 'Portal'){
						Messaging.SingleEmailMessage mailToCustomer = new Messaging.SingleEmailMessage();
						mailToCustomer.setSubject('Your Complaint Logged: '+sr.CaseNumber );
						mailToCustomer.setToAddresses(new String[]{UserId_UserDetailMap.get(sr.OwnerId).Email});
						//mailToCustomer.setToAddresses(new String[]{'amit.joshi@eternussolutions.com'});
						String mailBodyCust = 'Hi,<br/>';
						mailBodyCust += '<p>Your complaint has been registered and our representative is working on it.</p>';
						mailBodyCust += 'The Reference Number is: <b>'+sr.CaseNumber+'</b></p><br/><br/>';
						mailBodyCust += 'Thank you,<br/>DAMAC Team.';
						mailToCustomer.setHtmlBody( mailBodyCust );
						mails.add(mailToCustomer);
					}*/
			}

			if( !Test.isRunningTest() ){ Messaging.sendEmail(mails); }
	}

	public static void sendNotificationToCustomer(List<Case> lstSR){
		String messageBody = 'Your Complaint has been resolved.';
		List<Error_Log__c> errorLogList = new List<Error_Log__c>();
		Set<Id> accountIdSet = new Set<Id>();
		for( Case caseObj : lstSR ){
			if( CaseObj.AccountId != null){
				accountIdSet.add( CaseObj.AccountId );
			}
		}

		if( !accountIdSet.isEmpty() ){
			Map<Id, Account> accountsMap = new Map<Id, Account>([SELECT Id, Mobile_Phone_Encrypt__pc, Mobile__c, IsPersonAccount FROM Account WHERE Id IN: accountIdSet ]);
			for( Case caseObj : lstSR ){
				if( CaseObj.AccountId != null){
					String phoneNum = accountsMap.get( CaseObj.AccountId ).IsPersonAccount ? accountsMap.get( CaseObj.AccountId ).Mobile_Phone_Encrypt__pc : accountsMap.get( CaseObj.AccountId ).Mobile__c;
					if( String.isNotBlank( phoneNum )){
						String response = ComplaintProcessUtility.sendSMSNotification( messageBody, phoneNum );
						if( !'SMS message(s) sent'.equalsIgnoreCase( response ) ){
							errorLogList.add( logError( CaseObj.Id, response ) );
						}
					}
					
				}
			}

		}

		if( !errorLogList.isEmpty() ){
			insert errorLogList;
		}

	}

	public static Error_Log__c logError(Id caseId, String errorMsg){
		return new Error_Log__c( Case__c=caseId, Error_Details__c=errorMsg, Process_Name__c='Complaints');
	}
}