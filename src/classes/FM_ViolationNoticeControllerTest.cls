/*-------------------------------------------------------------------------------------------------
Description: Test class for FM_ViolationNoticeController
============================================================================================================================
    Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
    1.0     | 07-08-2018       | Lochana Rajput   | 1. Send email with PDF as attachment containing to Instructors containing
=============================================================================================================================
*/

@isTest
private class FM_ViolationNoticeControllerTest {

	@isTest static void test_raiseViolationSR() {

		Account objAcc = new Account();
		objAcc = TestDataFactory_CRM.createPersonAccount();
		insert objAcc;
		Location__c objLoc = new Location__c();
		objLoc = TestDataFactoryFM.createLocation();
		insert objLoc;
		Violation_Rule__c objVR = new Violation_Rule__c();
		objVR.Building__c = objLoc.Id;
		objVR.Category__c	='General Violations';
		objVR.Payble_Fine__c = 100;
		objVR.Remedial_Period__c = '7 days';
		objVR.Violation_Of_Rule__c = 'Abusing personnel working in the community';
		insert objVR;
		FM_User__c objFMUser = new FM_User__c(FM_User__c=UserInfo.getUserId(),
								FM_Role__c = 'Property Manager',
								Building__c = objLoc.Id);
		insert objFMUser;
		NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAcc);
		insert objSR;
		Booking__c objBooking = TestDataFactoryFM.createBooking(objAcc, objSR);
		insert objBooking;
		Booking_unit__c objBU = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
		insert objBU;

		List<Violation_Notice__c> lstVRs = new List<Violation_Notice__c>();
		lstVRs.add(new Violation_Notice__c(
			Name = 'Rule 1',
			Active__c = true,
			Category__c	='General Violations',
			Payble_Fine__c = 100,
			Remedial_Period__c = '7 days',
			Violation_Of_Rule__c = 'Abusing personnel working in the community'
		));
		insert lstVRs;
		Test.startTest();
		PageReference pageRef = Page.FM_ViolationNoticePage;
		pageRef.getParameters().put('UnitId', String.valueOf(objBU.Id));
		pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
		pageRef.getParameters().put('SRType', 'Violation_Notice_T');
		Test.setCurrentPage(pageRef);
		// FM_ViolationNoticeController.fetchViolationRules(objLoc.Id);
		User objUser = [SELECT Id from User where id=: UserInfo.getUserId()];
		System.runAs(objUser) {
			FM_ViolationNoticeController controller = new FM_ViolationNoticeController();
			controller.selectedCategory = 'General Violations';
			controller.getViolations();
			Violation_Rule__c objVR1 = [SELECT Id from Violation_Rule__c
										WHERE Building__c=:objLoc.Id];

			controller.selectedViolation = objVR.Id;
			controller.getDetails();
			controller.submitSR();
		}
		Test.stopTest();
	}

	@isTest static void test_exception() {

		Account objAcc = new Account();
		objAcc = TestDataFactory_CRM.createPersonAccount();
		insert objAcc;
		Location__c objLoc = new Location__c();
		objLoc = TestDataFactoryFM.createLocation();
		insert objLoc;
		Violation_Rule__c objVR = new Violation_Rule__c();
		objVR.Building__c = objLoc.Id;
		objVR.Category__c	='General Violations';
		objVR.Payble_Fine__c = 100;
		objVR.Remedial_Period__c = 'seven days';
		objVR.Violation_Of_Rule__c = 'Abusing personnel working in the community';
		insert objVR;
		FM_User__c objFMUser = new FM_User__c(FM_User__c=UserInfo.getUserId(),
								FM_Role__c = 'Property Manager',
								Building__c = objLoc.Id);
		insert objFMUser;
		NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAcc);
		insert objSR;
		Booking__c objBooking = TestDataFactoryFM.createBooking(objAcc, objSR);
		insert objBooking;
		Booking_unit__c objBU = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
		insert objBU;

		List<Violation_Notice__c> lstVRs = new List<Violation_Notice__c>();
		lstVRs.add(new Violation_Notice__c(
			Name = 'Rule 1',
			Active__c = true,
			Category__c	='General Violations',
			Payble_Fine__c = 100,
			Remedial_Period__c = 'seven days',
			Violation_Of_Rule__c = 'Abusing personnel working in the community'
		));
		insert lstVRs;
		Test.startTest();
		PageReference pageRef = Page.FM_ViolationNoticePage;
		pageRef.getParameters().put('UnitId', String.valueOf(objBU.Id));
		pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
		pageRef.getParameters().put('SRType', 'Violation_Notice_T');
		Test.setCurrentPage(pageRef);
		// FM_ViolationNoticeController.fetchViolationRules(objLoc.Id);
		User objUser = [SELECT Id from User where id=: UserInfo.getUserId()];
		System.runAs(objUser) {
			FM_ViolationNoticeController controller = new FM_ViolationNoticeController();
			controller.selectedCategory = 'General Violations';
			controller.getViolations();
			Violation_Rule__c objVR1 = [SELECT Id from Violation_Rule__c
										WHERE Building__c=:objLoc.Id];

			controller.selectedViolation = objVR.Id;
			controller.getDetails();
			controller.submitSR();
		}
		Test.stopTest();
	}
	@isTest static void test_deleteDocument() {
		FM_Case__c fmObj = new FM_Case__c();
		Id RecordTypeIdFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Violation Notice').getRecordTypeId();
	    fmObj.recordtypeid = RecordTypeIdFMCase;
	    insert fmObj;
		SR_Attachments__c srObj = new SR_Attachments__c();
		srObj.Name = 'Test';
    	srObj.FM_Case__c = fmObj.id;
        insert srObj;
		Test.startTest();
		FM_ViolationNoticeController controller = new FM_ViolationNoticeController();
		controller.docIdToDelete = srObj.Id+',Insurance';
		controller.deleteDocument();
		Test.stopTest();
	}
	@isTest static void test_uploadDocument() {
		Account objAcc = new Account();
		objAcc = TestDataFactory_CRM.createPersonAccount();
		insert objAcc;
		Location__c objLoc = new Location__c();
		objLoc = TestDataFactoryFM.createLocation();
		insert objLoc;
		Violation_Rule__c objVR = new Violation_Rule__c();
		objVR.Building__c = objLoc.Id;
		objVR.Category__c	='General Violations';
		objVR.Payble_Fine__c = 100;
		objVR.Remedial_Period__c = '7 days';
		objVR.Violation_Of_Rule__c = 'Abusing personnel working in the community';
		insert objVR;
		FM_User__c objFMUser = new FM_User__c(FM_User__c=UserInfo.getUserId(),
								FM_Role__c = 'Property Manager',
								Building__c = objLoc.Id);
		insert objFMUser;
		NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAcc);
		insert objSR;
		Booking__c objBooking = TestDataFactoryFM.createBooking(objAcc, objSR);
		insert objBooking;
		Booking_unit__c objBU = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
		insert objBU;

		FM_Case__c fmObj = new FM_Case__c();
		Id RecordTypeIdFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Violation Notice').getRecordTypeId();
	    fmObj.recordtypeid = RecordTypeIdFMCase;
	    insert fmObj;
		SR_Attachments__c srObj = new SR_Attachments__c();
		srObj.Name = 'Test';
    	srObj.FM_Case__c = fmObj.id;
        insert srObj;
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testdmacorg.com');
			FM_ViolationNoticeController controller = new FM_ViolationNoticeController();
			
			PageReference myVfPage = Page.FM_ViolationNoticePage;
	        Test.setCurrentPage(myVfPage);
			ApexPages.currentPage().getParameters().put('fileName','TestFile');
			system.debug('objBU.id=='+objBU.id);
			ApexPages.currentPage().getParameters().put('UnitId',objBU.id);
			ApexPages.currentPage().getParameters().put('AccountId',objAcc.id);
			
			controller.getViolations();
			controller.getDetails();
			Test.setMock( WebServiceMock.class, new MoveOutMockClass());
			controller.strDocumentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
	        controller.strDocumentName = 'VfRemotingJs';
			
		Test.startTest();

		controller.uploadDocument();
		controller.addDocument();
		controller.removeDocument();
		Test.stopTest();
	}

}