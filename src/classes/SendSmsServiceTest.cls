@isTest
private class SendSmsServiceTest
{
    @isTest
    static void positiveScenario() {

        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;

        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accountobj[0].Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;

        //String recordTypeDevName = ;
        List<Calling_List__c> listCallingList = TestDataFactory_CRM.createCallingList( 'Collections_Calling_List', 1 , bookingUnitsObj[0] );
        insert listCallingList;

        list<String> phoneNos = new list<String>();
        phoneNos.add('90908789795');
        Test.startTest();
          Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
          SendSmsService.Sendtextmessage( phoneNos ,'test message content', String.valueOf( listCallingList[0].Id ));
        Test.stopTest();
    }


    @isTest
    static void negativeScenario() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;

        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accountobj[0].Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;

        List<Calling_List__c> listCallingList = TestDataFactory_CRM.createCallingList( 'Collections_Calling_List', 1 , bookingUnitsObj[0] );
        insert listCallingList;

        //Id RecordTypeIdCase = [
      //      SELECT Id
      //      FROM RecordType
      //      WHERE SObjectType=:'Case' 
      //      AND DeveloperName=:'Change_of_Detail'
      //      AND IsActive = TRUE LIMIT 1
      //  ].Id;

        //Case caseObj = TestDataFactory_CRM.createCase( accountobj[0].Id, null );
        list<String> phoneNos = new list<String>();
        phoneNos.add( listCallingList[0].Id+':90908789795');
        Test.startTest();
          Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(2));
          SendSmsService.Sendtextmessage( phoneNos ,'test message content', String.valueOf( listCallingList[0].Id ));
        Test.stopTest();
    }
}