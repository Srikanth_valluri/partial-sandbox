public without sharing class FM_SendSMSBatch implements Database.Batchable<SObject>, 
                                                        Database.AllowsCallouts, 
                                                        Database.Stateful {
    
    private List<SMS_History__c> lstSMSHistory ;
    
    public FM_SendSMSBatch() {
        lstSMSHistory = new list<SMS_History__c>();
    }
    
    public Database.Querylocator start( Database.BatchableContext bc ) {
        return Database.getQueryLocator( 'SELECT Id, Phone_Number__c, FM_Case__r.Mobile_Country_Code__c, FM_Case__r.Mobile_no__c, Message__c, Is_SMS_Sent__c FROM SMS_History__c WHERE Is_SMS_Sent__c = false AND FM_Case__c != NULL AND FM_Case__r.RecordType.DeveloperName = \'Tenant_Registration\' AND CreatedDate = TODAY' );  
    }
    
    public void execute( Database.BatchableContext bc, list<SMS_History__c> lstSMSToSend ) {
        if( lstSMSToSend != null && !lstSMSToSend.isEmpty() ) {
            
            for( SMS_History__c objSMS : lstSMSToSend ) {
                if( objSMS.Is_SMS_Sent__c == false && String.isNotBlank( objSMS.Message__c ) ) {
                    lstSMSHistory.add( sendSMS( objSMS ) );
                }
            }
        }
    }
  
    public void finish( Database.BatchableContext bc ) {
        if( lstSMSHistory != null && !lstSMSHistory.isEmpty() ) {
            update lstSMSHistory ;
        }
        
        if( !test.isRunningTest() ) {
            system.scheduleBatch( new FM_SendSMSBatch(), 
                                  'TR Sending SMS Batch ' + system.now(), 
                                  5, 100);
        }
    }
    
    public SMS_History__c sendSMS( SMS_History__c objSMSHistory ) {
        
        String strUserName= Label.FM_SMS_service_username;
        String strPassword= Label.FM_SMS_service_password;
        
        String PhoneNumber = objSMSHistory.Phone_Number__c;
        if(PhoneNumber.startsWith('00')) {
            PhoneNumber = PhoneNumber.removeStart('00');
        }else if(PhoneNumber.startsWith('0')) {
            PhoneNumber = PhoneNumber.removeStart('0');
        }
        
        String strSID = SMSClass.getSenderName(strUserName, PhoneNumber, false);

        HttpRequest req = new HttpRequest();
        req.setMethod('POST' ); // Method Type
        req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx');
        req.setBody('user='+ strUserName + '&passwd=' + strPassword +'&message=' + GenericUtility.encodeChar(objSMSHistory.Message__c)  + '&mobilenumber=' + PhoneNumber + '&sid=' + strSID + '&MTYPE=LNG&DR=Y'); // Request Parameters

        Http http = new Http();
        try {
            HTTPResponse res = http.send( req );
            if( String.isNotBlank( res.getBody() ) ) {
                objSMSHistory.Description__c = res.getBody();
                objSMSHistory.Is_SMS_Sent__c = true ;
                if(String.valueOf(res.getBody()).contains('OK:')){
                    objSMSHistory.sms_Id__c = res.getBody().substringAfter('OK:');
                    objSMSHistory.Is_SMS_Sent__c = true ;
                }
            }
        }
        catch( Exception e ) {
            objSMSHistory.Description__c = e.getMessage();
        }
        return objSMSHistory ;
    }
}