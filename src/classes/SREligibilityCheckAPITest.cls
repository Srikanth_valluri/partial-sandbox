@isTest
public class SREligibilityCheckAPITest {
    @TestSetup
    static void TestData() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
        
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;
        
        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Tenant Community Login User%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
        
        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Villa';
        insert invObj;
        
        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'Y',
                                      Inventory__c = invObj.id,
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;        
    }
    
    @isTest
    static void testTenancyRenewal() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        Id rtRenewalId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId();
        FM_Case__c objTenRenewal = new FM_Case__c();
        objTenRenewal.Origin__c = 'Portal';
        objTenRenewal.isHelloDamacAppCase__c = true;
        objTenRenewal.Tenant__c = objAcc.Id;
        objTenRenewal.RecordtypeId = rtRenewalId;
        objTenRenewal.Status__c = 'In Progress';
        objTenRenewal.Request_Type__c = 'Tenant Renewal';
        objTenRenewal.Booking_Unit__c = objBU.Id;
        
        Test.startTest();
        
        insert objTenRenewal;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/checkSREligibility';
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('accountId',objAcc.Id);
        req.addParameter('srTypeId','6');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        SREligibilityCheckAPI.checkSREligibility();
        
        Test.stopTest();
    }
    
    @isTest
    static void testBookingUnitNull() {
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/checkSREligibility';
        req.addParameter('accountId','');
        req.addParameter('srTypeId','6');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        SREligibilityCheckAPI.checkSREligibility();
        
        Test.stopTest();
    }
    
    @isTest
    static void testBookingUnitBlank() {
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/checkSREligibility';
        req.addParameter('bookingUnitId','');
        req.addParameter('accountId','');
        req.addParameter('srTypeId','6');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        SREligibilityCheckAPI.checkSREligibility();
        
        Test.stopTest();
    }
    
    @isTest
    static void testInvalidBookingUnit() {
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/checkSREligibility';
        req.addParameter('bookingUnitId','1234');
        req.addParameter('accountId',objAcc.Id);
        req.addParameter('srTypeId','6');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        SREligibilityCheckAPI.checkSREligibility();
        
        Test.stopTest();
    }
    
    @isTest
    static void testAccountIdNull() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/checkSREligibility';
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('srTypeId','6');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        SREligibilityCheckAPI.checkSREligibility();
        
        Test.stopTest();
    }
    
    @isTest
    static void testAccountIdBlank() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/checkSREligibility';
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('accountId','');
        req.addParameter('srTypeId','6');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        SREligibilityCheckAPI.checkSREligibility();
        
        Test.stopTest();
    }
    
    @isTest
    static void testSrTypeIdNull() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/checkSREligibility';
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('accountId',objAcc.Id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        SREligibilityCheckAPI.checkSREligibility();
        
        Test.stopTest();
    }
    
    @isTest
    static void testSrTypeIdBlank() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/checkSREligibility';
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('accountId',objAcc.Id);
        req.addParameter('srTypeId','');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        SREligibilityCheckAPI.checkSREligibility();
        
        Test.stopTest();
    }
    
    @isTest
    static void testFitoutSREligibility() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/checkSREligibility';
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('accountId',objAcc.Id);
        req.addParameter('srTypeId','8');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        SREligibilityCheckAPI.checkSREligibility();
        
        Test.stopTest();
    }
    
    @isTest
    static void testWorkPermitSREligibility() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/checkSREligibility';
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('accountId',objAcc.Id);
        req.addParameter('srTypeId','11');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        SREligibilityCheckAPI.checkSREligibility();
        
        Test.stopTest();
    }
}