/****************************************************************************************************
 Name          : FetchProjectDetailsWebService                                                            
 Description   : WebService to Fetch all the Project and Inventory Details   
 Created Date  : 01-08-2018                                                                        
 Created By    : ESPL                                                                              
 --------------------------------------------------------------------------------------------------
 VER   AUTHOR             DATE        COMMENTS                                                    
 1.0   Twinkle P          01-08-2018  Initial Draft. 
 1.1   Twinkle P          13-08-2018  Added Project Image, Description, Country & Location                                         
****************************************************************************************************/
@RestResource(urlMapping='/fetchProjectDetails/*')
global class FetchProjectDetailsWebService {

    /**  
     * Method Get All The Project Details and Inventory Details
     */
    @HttpGet
    global static List<ResponseResult> getProjectsDetails() {
        List<Property__c> properties = [ SELECT  Id,Name,Property_ID__c,Property_Code__c,
                                                RERA_Project_Number__c,Latitude__c,Longitude__c,
                                                Property_Plan__c, Description__c,Property_Country__c,
                                                (  
                                                    SELECT  Id,
                                                            Marketing_Name__c,
                                                            Anticipated_Completion_Date__c ,
                                                            District__c,
                                                            Property_City__c,
                                                            Property_Country__c,
                                                            Marketing_Name_Doc__r.Marketing_Plan__c
                                                    FROM Inventories__r 
                                                    WHERE Status__c = 'Released' LIMIT 1 
                                                ) 
                                        FROM Property__c 
                                        LIMIT 2000
                                    ];

        Map<Id,Id> mapOfInvAndMarketing = new Map<Id,Id>();
        for(Property__c objProp : properties) {
            List<Inventory__c> inventoriesList = new List<Inventory__c>();
            inventoriesList.addAll(objProp.Inventories__r);
            if(!inventoriesList.isEmpty()) {
                mapOfInvAndMarketing.put(inventoriesList[0].Id, inventoriesList[0].Marketing_Name_Doc__c);
                
            }
            
        }
        List<Project_Information__c> projInformations = new List<Project_Information__c>();
        if(!mapOfInvAndMarketing.isEmpty()) {
            projInformations = [SELECT Id,Features__c,Detail__c ,Marketing_Documents__c,URL__c
                                FROM  Project_Information__c 
                                WHERE 
                                ( 
                                    Features__c = 'About' 
                                    OR
                                    Features__c  = '360Degree WalkThrough' 
                                )
                                AND Marketing_Documents__c =: mapOfInvAndMarketing.values()];
        }
        Map<Id,Project_Information__c> mapOfMarketingAndDescription = new Map<Id,Project_Information__c>();
        for(Project_Information__c objPropInfo : projInformations) {
            mapOfMarketingAndDescription.put(objPropInfo.Marketing_Documents__c, objPropInfo);
        }

        List<ResponseResult> resultList = new List<ResponseResult>();
        for(Property__c objProp : properties) {
            List<Inventory__c> inventoriesList = new List<Inventory__c>();
            inventoriesList.addAll(objProp.Inventories__r);
            String marketingName = inventoriesList.isEmpty() ? '' : inventoriesList[0].Marketing_Name__c;
            String districtVal = inventoriesList.isEmpty() ? '' : inventoriesList[0].District__c;
            String cityVal = inventoriesList.isEmpty() ? '' : inventoriesList[0].Property_City__c;
            String countryVal = inventoriesList.isEmpty() ? '' : inventoriesList[0].Property_Country__c;

            String locationForProperty = inventoriesList.isEmpty() ? '' : districtVal +','+cityVal+','+countryVal ;

            String anticipatedCompletionDate = inventoriesList.isEmpty() ? '' : inventoriesList[0].Anticipated_Completion_Date__c;
            String imageURL = inventoriesList.isEmpty()  ? '' : inventoriesList[0].Marketing_Name_Doc__r.Marketing_Plan__c ;
            //String descriptionStr = String.isNotBlank(objProp.Description__c) ? objProp.Description__c : '';
            String descriptionStr = '';
            String walkThroughLinkStr = '';
            String countryStr = String.isNotBlank(objProp.Property_Country__c) ? objProp.Property_Country__c : '';
            if(!inventoriesList.isEmpty()) {
                Id marketDocumentId = mapOfInvAndMarketing.get(inventoriesList[0].Id);
                descriptionStr      = mapOfMarketingAndDescription.containsKey(marketDocumentId) ? 
                                      mapOfMarketingAndDescription.get(marketDocumentId).Detail__c : '';
                walkThroughLinkStr  = mapOfMarketingAndDescription.containsKey(marketDocumentId) && 
                                      mapOfMarketingAndDescription.get(marketDocumentId).Features__c != null &&  
                                      mapOfMarketingAndDescription.get(marketDocumentId).Features__c.equalsIgnoreCase('360Degree WalkThrough') ? 
                                      mapOfMarketingAndDescription.get(marketDocumentId).URL__c : '';
            }
            system.debug('walkThroughLinkStr---'+walkThroughLinkStr);
            resultList.add(
                new ResponseResult ( 
                                    objProp.Name,
                                    String.valueOf(objProp.Property_ID__c ),
                                    objProp.Property_Code__c,
                                    objProp.RERA_Project_Number__c,
                                    objProp.Latitude__c,
                                    objProp.Longitude__c,
                                    marketingName,
                                    anticipatedCompletionDate,
                                    imageURL,
                                    descriptionStr,
                                    countryStr,
                                    locationForProperty,
                                    walkThroughLinkStr
                                )
            );
        }
        
        return resultList;
    }
    //Wrapper Class for JSON Response
    global class ResponseResult {
        public String PROJECT_NAME;
        public String PROJECT_ID;
        public String PROJECT_CODE;
        public String DLD_PROJECT_NO;
        public Decimal LAT;
        public Decimal LONGITUDE;
        public String MARKETING_NAME;
        public String HANDOVER_DATE;
        public String PROPERTY_IMAGE;
        public String PROPERTY_DESCRIPTION;
        public String PROPERTY_COUNTRY;
        public String PROPERTY_LOCATION;
        public String WALK_THROUGH_LINK;
        global ResponseResult( 
                            String PROJECT_NAME,
                            String PROJECT_ID, 
                            String PROJECT_CODE,
                            String DLD_PROJECT_NO,
                            Decimal LAT, 
                            Decimal LONGITUDE,
                            String MARKETING_NAME,
                            String HANDOVER_DATE,
                            String PROPERTY_IMAGE,
                            String PROPERTY_DESCRIPTION,
                            String PROPERTY_COUNTRY,
                            String PROPERTY_LOCATION,
                            String WALK_THROUGH_LINK
        ) {
            this.PROJECT_NAME = PROJECT_NAME;
            this.PROJECT_ID = PROJECT_ID;
            this.PROJECT_CODE = PROJECT_CODE;
            this.DLD_PROJECT_NO = DLD_PROJECT_NO;
            this.LAT = LAT;
            this.LONGITUDE = LONGITUDE;
            this.MARKETING_NAME = MARKETING_NAME;
            this.HANDOVER_DATE = HANDOVER_DATE;
            this.PROPERTY_IMAGE = PROPERTY_IMAGE;
            this.PROPERTY_DESCRIPTION = PROPERTY_DESCRIPTION;
            this.PROPERTY_COUNTRY = PROPERTY_COUNTRY;
            this.PROPERTY_LOCATION = PROPERTY_LOCATION;
            this.WALK_THROUGH_LINK = WALK_THROUGH_LINK;
        }
    }

    ///services/apexrest/fetchProjectDetails
}