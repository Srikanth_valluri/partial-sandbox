/************************************************************************************************
 * @Name              : DAMACFactoryUtils
 * @Test Class Name   : DAMACFactoryUtils
 * @Description       : Utility Handler for DAMAC Factory
 * Modification Log
 * VERSION     AUTHOR           DATE            Update Log
 * 1.0         Srikanth Valluri 28/07/2020       Created
 ***********************************************************************************************/
global class DAMACFactoryUtils{
	//Get Details based on Project code and billing code parameters
	global static RestContextHandler handleInventorySearch(string projectCode, string buildingCode){
		RestContextHandler handler = new RestContextHandler(true);
		projectCode = DAMAC_Utility.trimData(projectCode);
		list<string> codes = new list<string>();
		if(buildingCode != null && buildingCode != ''){
			buildingCode = DAMAC_Utility.trimData(buildingCode);
			codes = buildingCode.split(',');
		}

		List<String> inventoryStatusList = new List<String>{'Released', 'Restricted', 'Inventory', 'Cancel Bin', 'Restricted_Apartments_In_Inventory', 'Apartments_In_Inventory'};
		List<Inventory__c> inventories = new list<inventory__c>();
		if(!codes.IsEmpty())
			inventories = [SELECT Unit_Area__c, Unit_Name__c, Conn_Project_Code__c, Building_Code__c, Unit_Area_sft__c, Selling_Price__c
									FROM Inventory__c
									WHERE Status__c IN :inventoryStatusList AND Conn_Project_Code__c = :projectCode AND Building_Code__c in : codes];
		else
			inventories = [SELECT Unit_Area__c, Unit_Name__c, Conn_Project_Code__c, Building_Code__c, Unit_Area_sft__c, Selling_Price__c
									FROM Inventory__c
									WHERE Status__c IN :inventoryStatusList AND Conn_Project_Code__c = :projectCode];
		handler.response.data = inventories;
		handler.response.success = true;
		handler.response.message = '';
		return handler;
	}

	
}