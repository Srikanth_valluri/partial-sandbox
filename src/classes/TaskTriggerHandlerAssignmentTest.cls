/****************************************************************************************
* Description - Test class developed for TaskTriggerHandlerAssignment
*
* Version            Date            Author                              Description
* 1.0                22/11/17        Naresh Kaneriya (Accely)           Initial Draft
****************************************************************************************/
@isTest
private class TaskTriggerHandlerAssignmentTest{
   
    @testSetup
    static  void myUnitTest() {
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        System.assert(objAccount!=null);

        NSIBPM__Service_Request__c objSr = TestDataFactory_CRM.createServiceRequest();
        insert objSr;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount(objAccount.Id, objSr.Id, 1);
        insert lstBookings;

        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        insert lstBookingUnits;

        List<Buyer__c> lstBuyer = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 1, objAccount.Id);
        insert lstBuyer;

        Account objAccountSeller = TestDataFactory_CRM.createPersonAccount();
        objAccountSeller.FirstName = 'FN Seller';
        objAccountSeller.party_ID__C = '54321';
        insert objAccountSeller;

        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id, caseRecordTypeId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Seller__c = objAccountSeller.Id;
        insert objCase;
       
        Task TaskToinsert =  new Task();
        TaskToinsert.WhatId = objCase.Id;
        TaskToinsert.Subject  = 'Test Subject';
        TaskToinsert.ActivityDate  = System.today()+2;
        TaskToinsert.Assigned_User__c  = UserInfo.getUserId();
        TaskToinsert.Process_Name__c  = 'Assignment';
        TaskToinsert.Priority = 'High';
        TaskToinsert.Status = 'Not Started';
        insert TaskToinsert;  
       
    }
     
     
        // Test Method: createTaskOnIPMS
    public static testmethod void Test_createTaskOnIPMS(){
        Test.starttest();
        set<Id> settaskforcallout = new set<Id>();
        set<Id> settaskcaseid = new set<Id>();
        
        Case createCase = getCase();
        Task tsk = getTask();
        System.assertEquals(tsk.Process_Name__c, 'Assignment');
        
        settaskforcallout.add(tsk.id);
        settaskcaseid.add(createCase.Id);
        
        SOAPCalloutServiceMock.returnToMe =  new Map<String,TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();
        TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response_x =  new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
        response_x.return_x ='{'+
        ' "message" : "Test",'+
        ' "status" : "S"'+
        ''+
        '}';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        
        Test.setMock(WebServiceMock.Class, new SOAPCalloutServiceMock());
        TaskTriggerHandlerAssignment.createTaskOnIPMS(settaskforcallout, settaskcaseid);
        
        Test.stoptest();  
    }
    
    // To cover Inner Class
     public testMethod static void TitleDeedTest(){
         
        Test.starttest();
        set<Id> settaskforcallout = new set<Id>();
        set<Id> settaskcaseid = new set<Id>();
        
        Case createCase = getCase();
        Task tsk = getTask();
        System.assertEquals(tsk.Process_Name__c, 'Assignment');
        
        settaskcaseid.add(createCase.Id);
        
        SOAPCalloutServiceMock.returnToMe =  new Map<String,TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();
        TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response_x =  new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
        response_x.return_x ='{'+
        ' "message" : "Test",'+
        ' "status" : "S"'+
        ''+
        '}';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        
        Test.setMock(WebServiceMock.Class, new SOAPCalloutServiceMock());
        TaskTriggerHandlerAssignment obj = new TaskTriggerHandlerAssignment();
        obj.verifyTitleDeed(settaskcaseid);
        
        Test.stoptest();
           
     }
     
     // To cover Inner Class
     public testMethod static void InnerTest(){
         
        TaskTriggerHandlerAssignment.innerClass InnerClass =  new TaskTriggerHandlerAssignment.innerClass();
        InnerClass.message = 'message';
        InnerClass.status = 'status';
         
        Error_Log__c objErr = new Error_Log__c();
        objErr.Account__c = getAccount().Id;
        objErr.Case__c = getCase().Id;
        objErr.Booking_Unit__c = getBu().Id;
        insert objErr ;
        
        list<Error_Log__c> listObjErr =  new list<Error_Log__c> ();
        listObjErr.Add(objErr);
          
        TaskTriggerHandlerAssignment.createErrorLogRecord(getAccount().Id , getBu().Id,getCase().Id); 
        TaskTriggerHandlerAssignment.insertErrorLog(listObjErr);
           
     }
     
        // Test Method: updateCase
    public static testmethod void Test_updateCase(){
        Test.starttest();
        TaskTriggerHandlerAssignment obj = new TaskTriggerHandlerAssignment();
        map<Id, list<Task>> mapcasetask = new map<Id,list<Task>>();
        
        list<Task> tskList =  new List<Task>();
        Task tsk = getTask();
        tsk.Subject = Label.DIFC_Task_NOC_Preparation ;
        Update tsk;
        tskList.add(tsk);
        mapcasetask.put(getCase().id , tskList);
        obj.updateCase(mapcasetask);  
        
        
        tsk.Subject = Label.Dubai_Ready_Task_Document_Upload ;
        Update tsk;
        tskList.add(tsk);
        mapcasetask.put(getCase().id , tskList);
        obj.updateCase(mapcasetask); 
        
        tsk.Subject = Label.DIFC_Task_Buyer_SOA ;
        Update tsk;
        tskList.add(tsk);
        mapcasetask.put(getCase().id , tskList);
        obj.updateCase(mapcasetask); 
        
        
        tsk.Subject = Label.Dubai_Ready_Task_Transfer_Account ;
        Update tsk;
        tskList.add(tsk);
        mapcasetask.put(getCase().id , tskList);
        obj.updateCase(mapcasetask); 

        tsk.Subject = Label.Task_Originals_Submitted ;
        Update tsk;
        //System.assertEquals(tsk.Subject, 'Collate documents and submit originals to contracts');
        tskList.add(tsk);
        mapcasetask.put(getCase().id , tskList);
        obj.updateCase(mapcasetask);
        
        tsk.Subject = Label.Task_Funds_to_New_Account ;
        Update tsk;
        tskList.add(tsk);
        mapcasetask.put(getCase().id , tskList);
        obj.updateCase(mapcasetask);
        
        
        tsk.Subject = Label.Dubai_Ready_Task_Confirmed_Buyer_Details ;
        Update tsk;
        //System.assertEquals(tsk.Subject, 'Verify the new title deed / customer name with DLD website & update system with new buyer details');
        tskList.add(tsk);
        mapcasetask.put(getCase().id , tskList);
        obj.updateCase(mapcasetask);
    
        tsk.Subject = Label.Abu_Dhabi_Task_Lease_Archival ;
        Update tsk;
        tskList.add(tsk);
        mapcasetask.put(getCase().id , tskList);
        obj.updateCase(mapcasetask);
        
        tsk.Subject = Label.DIFC_Task_Print_NOC ;
        Update tsk;
        //System.assertEquals(tsk.Subject, 'Print the NOC /Signed & Stamped to be Handover to customer');
        tskList.add(tsk);
        mapcasetask.put(getCase().id , tskList);
        obj.updateCase(mapcasetask);
    
        Test.stoptest(); 
    }

      
        // Test Method: checkTaskCompletion
    public static testmethod void Test_checkTaskCompletion(){
        Test.starttest();
        list<Task> listtask = new list<Task>();
        map<Id, Task> mapoldtask = new map<Id,Task>();
        
        Task OldTask = getTask();
        mapoldtask.put(OldTask.Id ,OldTask);
          
        // New Updated Task
        OldTask.Status = 'Completed';
        OldTask.Subject = 'BUYER POA Verification Pending';
        update OldTask ;
        listtask.add(OldTask);
        new TaskTriggerHandlerAssignment().checkTaskCompletion(listtask, mapoldtask);
        Test.stoptest(); 
    }

        // Test Method: checkTaskCompletion
    public static testmethod void Test_checkTaskCompletionElse(){
        Test.starttest();
        list<Task> listtask = new list<Task>();
        map<Id, Task> mapoldtask = new map<Id,Task>();
        
        Task OldTask = getTask();
        mapoldtask.put(OldTask.Id ,OldTask);
          
        // New Updated Task
        OldTask.Status = 'Completed';
        OldTask.Subject = Label.Dubai_OffPlan_Task_PRC_Doc;
        update OldTask ;
        listtask.add(OldTask);
        new TaskTriggerHandlerAssignment().checkTaskCompletion(listtask, mapoldtask);
        Test.stoptest(); 
    }
        // Test Method: checkTaskCompletion
    public static testmethod void Test_checkTaskCompletionElseIf(){
        Test.starttest();
        list<Task> listtask = new list<Task>();
        map<Id, Task> mapoldtask = new map<Id,Task>();
        
        Task OldTask = getTask();
        mapoldtask.put(OldTask.Id ,OldTask);
          
        // New Updated Task
        OldTask.Status = 'Completed';
        OldTask.Subject = Label.Task_SPA_Execution;
        update OldTask ;
        listtask.add(OldTask);
        new TaskTriggerHandlerAssignment().checkTaskCompletion(listtask, mapoldtask);
        Test.stoptest(); 
    }
    
    // Test Method: checkTaskCompletion
    public static testmethod void Test_checkTaskCompletionElse2(){
        Test.starttest();
        list<Task> listtask = new list<Task>();
        map<Id, Task> mapoldtask = new map<Id,Task>();
        
        Task OldTask = getTask();
        mapoldtask.put(OldTask.Id ,OldTask);
          
        // New Updated Task
        OldTask.Status = 'Completed';
        OldTask.Subject = Label.Abu_Dhabi_Task_Lease_Agreement;
        update OldTask ;
        listtask.add(OldTask);
        new TaskTriggerHandlerAssignment().checkTaskCompletion(listtask, mapoldtask);
        Test.stoptest(); 
    }
     // To get Account Records
  public static Account getAccount(){
    return [SELECT id,Name,Name_Arabic__c,Country__c,Party_Type__c,Country_Arabic__c,Party_ID__c,Address_Line_1_Arabic__c,Address_Line_2__c,
                  Address_Line_2_Arabic__c,Address_Line_3__c,Address_Line_3_Arabic__c,Address_Line_4__c,Address_Line_4_Arabic__c,City__c,
                  City_Arabic__c,State__c,State_Arabic__c,Postal_Code_Arabic__c,Email__c,Mobile_Country_Code__c,Mobile_Phone_Encrypt__c,
                  Home_Phone_Country_Code__c,Home_Phone_Encrypt__c,Work_Phone_Country_Code__c,Other_Phone_Encrypt__c,Middle_Name__c,
                  Middle_Name_Arabic__c,Last_Name__c,Last_Name_Arabic__c,Country__pc,Country_Arabic__pc,Address_Line_1__pc,Address_Line_2__pc,
                  Address_Line_3__pc,Address_Line_4__pc,CR_Registration_Expiry_Date__c,Address_Line_1_Arabic__pc,Address_Line_2_Arabic__pc,
                  Address_Line_3_Arabic__pc,Address_Line_4_Arabic__pc,City__pc,City_Arabic__pc,BillingPostalCode,Email__pc,Mobile_Country_Code__pc,
                  Mobile_Phone_Encrypt__pc,Passport_Number__pc,Passport_Expiry_Date__pc,Passport_Issue_Place__pc,Passport_Issue_Place_Arabic__pc,
                  Nationality__pc,Nationality_Arabic__pc,Nationality__c,Nationality_Arabic__c,Title_Arabic__pc,FirstName,First_Name_Arabic__pc,
                  MiddleName,Middle_Name_Arabic__pc,LastName,Last_Name_Arabic__pc,IsPersonAccount FROM Account WHERE FirstName='Test FirstName'];
  }
    
  // To get Case Records    
  public static Case getCase(){
    return [SELECT id,Customer_First_Name__c,AccountId,RecordType.Name,Customer_First_Name_Arabic__c,Country__c,Country_Arabic__c,
    Address__c,Address_Arabic_1__c,Address_2__c,Address_Arabic_2__c,Address_3__c,Address_Arabic_3__c,Address_4__c,Address_Arabic_4__c,City__c,
    City_Arabic__c,State__c,State_Arabic__c,Postal_Code_Arabic__c,Contact_Email__c,Mobile_Country_Code__c,Contact_Mobile__c,
    Home_Phone_Country_Code__c,Home_Phone__c,Work_Phone_Country_Code__c,Work_Phone__c,Customer_Middle_Name__c,Customer_Middle_Name_Arabic__c,
    Customer_Last_Name__c,Customer_Last_Name_Arabic__c,CRF_File_URL__c,Passport_Issue_Date__c,Postal_Code__c,New_CR__c,Passport_Issue_Place__c,
    Passport_Issue_Place_Arabic__c,Changed_Nationality__c,POA_File_URL__c,Changed_Nationality_Arabic__c,
    Title__c,Title_Arabic__c FROM Case WHERE Credit_Note_Amount__c=5000];
  }
  public static Task getTask(){
      
      return [SELECT id,WhatId,Type,Status,OwnerId,IPMS_Role__c,
                 Subject,CreatedDate,Description,Assigned_User__c,ActivityDate,Owner.Name,Process_Name__c,
                 Task_Error_Details__c , Pushed_to_IPMS__c , Document_URL__c FROM Task where Process_Name__c  = 'Assignment'];
  }  

   public static Booking_Unit__c getBu(){
    return [Select Id from Booking_Unit__c where Unit_Selling_Price_AED__c  = 100 limit 1];
 } 

}