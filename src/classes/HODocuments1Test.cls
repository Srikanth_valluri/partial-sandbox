/*****************************************************************************************************
* Description - Test class developed for 'HODocuments1'
*
* Version            Date            Author               Description
* 1.0               04/02/2018                   		Initial Draft
 *******************************************************************************************************/
@isTest
public class HODocuments1Test {
  public static testmethod void testHODocuments1(){
    
    Test.startTest();
    HODocuments1.HOInvoicePackHttpSoap11Endpoint obj = new HODocuments1.HOInvoicePackHttpSoap11Endpoint();
    List<HODocuments2.APPSXXDC_PROCESS_SERX1794747X1X5> regTerms =  new List<HODocuments2.APPSXXDC_PROCESS_SERX1794747X1X5>();
    
    SOAPCalloutServiceMock.returnToMe =  new Map<String, HODocuments1.HandOverInvoiceResponse_element>();
    HODocuments1.HandOverInvoiceResponse_element response_x = new HODocuments1.HandOverInvoiceResponse_element();
    response_x.return_x = 'S';
    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    
    test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
    
    String respose = obj.HandOverInvoice('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM',regTerms);
      //String respose = obj.HandOverPack('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM',regTerms);      
    System.assertEquals(respose,'S');
    Test.stopTest();
    
  }   
  public static testmethod void testHODocuments2(){
    
    Test.startTest();
    HODocuments1.HOInvoicePackHttpSoap11Endpoint obj = new HODocuments1.HOInvoicePackHttpSoap11Endpoint();
    List<HODocuments2.APPSXXDC_PROCESS_SERX1794747X1X5> regTerms =  new List<HODocuments2.APPSXXDC_PROCESS_SERX1794747X1X5>();
    
    SOAPCalloutServiceMock.returnToMe =  new Map<String, HODocuments1.HandOverPackResponse_element>();
    HODocuments1.HandOverPackResponse_element response_x = new HODocuments1.HandOverPackResponse_element();
    response_x.return_x = 'S';
    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    
    test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
    
    //String respose = obj.HandOverInvoice('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM',regTerms);
      String respose = obj.HandOverPack('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM',regTerms);      
    System.assertEquals(respose,'S');
    Test.stopTest();
    
  }   
}