@isTest
public class AnalizeCXCallBatchTest {
    @isTest
    static void test1() {
        User userObj = new User(
                             ProfileId = [SELECT Id FROM Profile WHERE Name = 'Collection - CRE'].Id,
                             LastName = 'test',
                             Email = 'puser000@test.com',
                             Username = 'puser000@test.com' + System.currentTimeMillis(),
                             CompanyName = 'TEST',
                             Title = 'title',
                             isActive = true,
                             Alias = 'alias',
                             Extension = '7023301',
                             TimeZoneSidKey = 'Asia/Dubai',
                             EmailEncodingKey = 'UTF-8',
                             LanguageLocaleKey = 'en_US',
                             LocaleSidKey = 'en_IN'
        );
        insert userObj ;
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com',
                                       Active_Customer__c = 'Active');
        insert account;
        Account account1 = new Account( LastName = 'Test Account1',
                                       Party_ID__c = '630622',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test1@mailinator.com',
                                       CreatedDate = Datetime.newInstance(2020, 2, 2, 12, 30, 2),
                                       Active_Customer__c = 'Active'
                                      );
        insert account1;
        Account account2 = new Account( LastName = 'Test Account2',
                                       Party_ID__c = '630623',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test2@mailinator.com',
                                       CreatedDate = Datetime.newInstance(2020, 3, 2, 12, 30, 2),
                                       Active_Customer__c = 'Active'
                                      );
        insert account2;
        List<Call_Log__c> lstCL = new List<Call_Log__c>();
        Call_Log__c objCall_Log = new Call_Log__c();
        objCall_Log.Calling_Number__c = '123456789';
        objCall_Log.CRE__c = userObj.Id;
        objCall_Log.Account__c = account.Id;
        objCall_Log.Call_Type__c = 'Outbound';
        objCall_Log.CreatedDate = Datetime.newInstance(2020, 3, 2, 12, 30, 2);
        lstCL.add(objCall_Log);
        Call_Log__c objCall_Log1 = new Call_Log__c();
        objCall_Log1.Calling_Number__c = '123456789';
        objCall_Log1.CRE__c = userObj.Id;
        objCall_Log1.Account__c = account.Id;
        objCall_Log1.Call_Type__c = 'Outbound';
        objCall_Log1.CreatedDate = Datetime.newInstance(2020, 3, 2, 12, 30, 2);
        lstCL.add(objCall_Log1);
        Call_Log__c objCall_Log2 = new Call_Log__c();
        objCall_Log2.Calling_Number__c = '123456789';
        objCall_Log2.CRE__c = userObj.Id;
        objCall_Log2.Account__c = account.Id;
        objCall_Log2.Call_Type__c = 'Outbound';
        objCall_Log2.CreatedDate = Datetime.newInstance(2020, 3, 2, 9, 30, 2);
        lstCL.add(objCall_Log2);
        insert lstCL;
        Test.startTest();
        Database.executeBatch( new AnalizeCXCallBatch());
        Test.stopTest();
        List<Account> accountList = [SELECT Id,Preferred_Time_To_Call__c from Account where Id =: account.Id ];
        System.debug('InsideTest'+accountList);
        //system.assert(accountList[0].Preferred_Time_To_Call__c != Null);
        

    }
}