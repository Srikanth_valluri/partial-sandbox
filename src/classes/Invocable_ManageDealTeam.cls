public class Invocable_ManageDealTeam {
    @InvocableMethod
    public static void ManageDealTeam(List<Id> stepId) {
        for(New_Step__c step : [SELECT Id, Service_Request__c FROM New_Step__c WHERE Id IN :stepId]) {
            EvaluateCustomCode(step);
        }
    }
    
    public static String EvaluateCustomCode(New_Step__c step){
        String retStr = 'Success';
        try {
            NSIBPM__Service_Request__c objSR = SRUtility.getSRDetails(step.Service_Request__c);
            
            if(objSR.Record_Type_Name__c  == 'Change_Agent') {
                ManageDealTeam.ChangeAgent(objSR);
            } 
            /*else if(objSR.Record_Type_Name__c  == 'Change_Manager') {
                ManageDealTeam.ChangeMgr(objSR);                
            } else if(objSR.Record_Type_Name__c  == 'Change_PC') {
                ManageDealTeam.ChangePC(objSR);
            }
*/
            
            return retStr;
        }
        catch(exception ex){
            return ex.getMessage();
        }
    }
}