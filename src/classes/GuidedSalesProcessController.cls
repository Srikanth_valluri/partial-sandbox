/**************************************************************************************************
* Name               : GuidedSalesProcessController                                                 
* Description        : Guided Sales process                                             
* Created Date       : NSI - Sivasankar                                                                        
* Created By         : 26/Feb/2017                                                                 
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         NSI - Sivasankar          26/Feb/2017
* 1.1         NSI - Sivasankar          17/Apr/2017 - Removing the validation on field selection atleast three fields 
* 1.2         NSI - Sivasankar          17/Apr/2017 - 576 # Need to Remove dependency filter on the GSP wizard. All fields to be independent of each other                                             
***************************************************************************************************/
public with sharing class GuidedSalesProcessController extends PaginationUtility{
    
    public GuidedSalesProcessController (){}
}