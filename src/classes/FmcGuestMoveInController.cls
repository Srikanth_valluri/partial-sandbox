public without sharing class FmcGuestMoveInController {

    @RemoteAction
    public static List<Location__c> lookupBuilding(String name) {
        if (String.isBlank(name)) {
            return NULL;
        }
        name = '%' + String.escapeSingleQuotes(name) + '%';
        return [
            SELECT  Id
                    , Name
                    , Building_Name__c
                    , Location_Code__c
            FROM    Location__c
            WHERE   (Building_Name__c LIKE :name OR  Name LIKE :name)
                AND (NOT Building_Name__c  LIKE '%STORAGE%')
                AND (Location_Type__c = 'Building' OR Location_Type__c = NULL)
            LIMIT   200
        ];
    }

    @RemoteAction
    public static List<Booking_Unit__c> lookupUnit(String name, String locationId) {
        if (String.isBlank(name) || String.isBlank(locationId)) {
            return NULL;
        }
        name = '%' + String.escapeSingleQuotes(name) + '%';
        Set<String> setActiveStatuses = Booking_Unit_Active_Status__c.getAll().keySet();

        Set<String> invalidHandoverStatus = new Set<String> {NULL, ''};
        invalidHandoverStatus.addAll(Label.FM_Booking_Unit_Handover_Status.split(','));

        return [
            SELECT  Id
                    , Unit_Name__c
            FROM    Booking_Unit__c
            WHERE   Inventory__r.Building_Location__c = :locationId
                    AND Keys_Released__c = TRUE
                    AND Handover_Status__c NOT IN :invalidHandoverStatus
                    AND Unit_Name__c LIKE :name
                    AND Registration_Status__c IN :setActiveStatuses
                    AND (Property_Status__c = 'Ready' OR Inventory__r.Property_Status__c = 'Ready')
            LIMIT   200
        ];
    }

}