/**
 * @File Name          : SendGridEmailServiceTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10/16/2019, 5:01:49 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/16/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@istest
public class SendGridEmailServiceTest {
    static testMethod void testMethod1(){
        list<Attachment> lstAttach = new list<Attachment>();
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        objAcc.Country__c = 'United Arab Emirates';
        insert objAcc ;
        
        Attachment objAttach = new Attachment();
        objAttach.Body = Blob.valueof('Test Mail');
        objAttach.Name = 'Test Email';
        objAttach.ParentId = objAcc.Id;
        insert objAttach;
        
        lstAttach.add(objAttach);
        test.startTest();  
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        SendGridEmailService.SendGridResponse response = SendGridEmailService.sendEmailService('abc@xyz.com', 'abc', 'xyz@rty.com', 'xyz',
            'wer@qas.com', 'wer', 'test subject', '', 'qas@sfdc.com', 'qas', 'ert@we.com', 'ert', 'plain/text', 'Hi Test', '', lstAttach);
        Test.stopTest();
    }
    static testMethod void testMethod2(){
        list<Attachment> lstAttach = new list<Attachment>();
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        objAcc.Country__c = 'United Arab Emirates';
        insert objAcc ;
        
        Attachment objAttach = new Attachment();
        objAttach.Body = Blob.valueof('Test Mail');
        objAttach.Name = 'Test Email';
        objAttach.ParentId = objAcc.Id;
        insert objAttach;
        
        lstAttach.add(objAttach);
        test.startTest();  
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        SendGridEmailService.SendGridResponse response = SendGridEmailService.sendEmailService('abc@xyz.com', 'abc', 'xyz@rty.com,xyyxxzz@test.com', 'xyz',
            'wer@qas.com', 'wer', 'test subject', '', 'qas@sfdc.com', 'qas', 'ert@we.com', 'ert', 'plain/text', 'Hi Test', '', lstAttach);
        Test.stopTest();
    }
}