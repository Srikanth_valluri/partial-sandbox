/*
* Description - Test class developed for AdditionalParkingTaskPBHandler
*
* Version          Date            Author            Description
* 1.0              18/02/2018      hardik Mehta      Initial Draft
*/
@isTest
private class PromotionsTaskPBHandlerTest {

    static testMethod void testCreateTasksForPromotions() 
    {
        UserRole objUserRole = [Select Id from UserRole where name = 'Collection - CRE'];

        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];

        User usr1 = createTestUser(objUserRole.Id , pf.Id, 'Test FirstName1', 'Test LastName1',null);
        insert usr1;
        User usr2 = createTestUser(objUserRole.Id , pf.Id, 'Test FirstName', 'Test LastName',usr1);

        System.runAs(usr2)
        {
            // Insert Accont
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc ;
            
            //Insert Service Request
            NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
            insert objSR ;
            
            //Insert Bookings
            List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
            insert lstBookings ;
            
            //Insert Booking Units
            List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
            for( Booking_Unit__c objUnit : lstBookingUnits ) {
               objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
            }
            insert lstBookingUnits;
            
            //Insert Options
             List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
             insert lstOptions ;
            
            //Insert Customer Setting Records
            List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
            insert lstActiveStatus ;
            
            //Insert Cases for the units
            list<Case> lstCases = new list<Case>();
            Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
            for( Booking_Unit__c objUnit : lstBookingUnits ) {
                Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
                objCase.Booking_Unit__c = objUnit.Id;
                lstCases.add( objCase );
            }
            insert lstCases ;
            
            test.startTest();
              Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );
              PromotionsTaskPBHandler.createTasksForPromotions( new list<Task>{ TestDataFactory_CRM.createTask( (Sobject)lstCases[0], 'Upload the Signed Promotions Offer Letter', 'Test', 'Promotions', system.today() ) } );

              PromotionsTaskPBHandler.errorLoggerNew('Test', lstCases[0].Id, lstBookingUnits[0].Id );
            test.stopTest();
        }
    } 

    /* * * * * * * * * * * * *
    *  Method Name:  createTestUser
    *  Purpose:      This method is used to create test user
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 20-Feb-2018
    * * * * * * * * * * * * */
    public static User createTestUser(Id roleId , Id profID, String fName, String lName, User objUser)
    {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
       
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User tuser = new User(  firstname = fName,
                                lastName = lName,
                                email = uniqueName + '@test' + orgId + '.org',
                                Username = uniqueName + '@test' + orgId + '.org',
                                EmailEncodingKey = 'ISO-8859-1',
                                Alias = uniqueName.substring(18, 23),
                                TimeZoneSidKey = 'America/Los_Angeles',
                                LocaleSidKey = 'en_US',
                                LanguageLocaleKey = 'en_US',
                                ProfileId = profId,
                                UserRoleId = roleId
                             );
        if(objUser != null)
        {
            tuser.ManagerId = objUser.Id;
        }
        return tuser;
    }
}