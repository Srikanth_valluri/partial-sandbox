/************************************************************************************************
 * @Name              : Name:DAMACApprovalUtils
 * @Test Class Name   : DAMAC_Approval_Factory_Test
 * @Description       : Utility Handler for Approval Factory
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         Charan Vuyyuru 14/07/2020       Created
 * 1.1         QBurst 21/07/2020       Modified
 ***********************************************************************************************/

global class DAMACApprovalUtils {

    //List of Reinstatements
    global static RestContextHandler getReinstatements(){
        RestContextHandler handler = new RestContextHandler(true);   
        
        list<NSIBPM__Service_Request__c> requests = [Select id, NSIBPM__Contact__r.Name, OwnerId, Name, Owner.Name, 
                                                    List_of_Units__c, RM_Team__c, Agency__r.name, Deal_Rejected_Date__c, DP_Payment__c,
                                                    Rejection_Reason__c, Rejection_Comments__c, Total_Booking_Amount__c, Token_Amount_AED__c, Registration_Date__c,
                                                        (Select id, OwnerId, Owner.Type, Owner.Name, Owner.Email, Service_Request__c, Change_Status__c, Step_No__c, Is_Closed__c, 
                                                        Step_Status__c, Step_Type__c, Comments__c, CreatedDate, lastmodifiedBy.name, lastModifiedDate  
                                                        from Steps__r 
                                                        where  Step_No__c >= 70 
                                                        order by createdDate asc) 
                                                    from NSIBPM__Service_Request__c 
                                                    where Reinstatement_Status__c = 'In Progress' and DP_Payment__c = '< 50' 
                                                    and Internal_Status__c = 'HOS_APPROVED_POP' 
                                                    order by createdDate desc];
        
        list<Reinstatement> reinstatements = new list<Reinstatement>();
        for(NSIBPM__Service_Request__c sr: requests){
            Reinstatement obj = new Reinstatement();
            obj.requestNo = sr.Name;
            obj.requestType = 'Deal Re-Instatement';
            obj.requestId = sr.Id;
            obj.unitCodes = sr.List_of_Units__c;
            obj.ownerName = sr.Owner.Name;
            obj.agencyName = sr.Agency__r.Name;
            obj.approvals = getApprovals(sr.Steps__r);            
            for(New_step__c step: sr.Steps__r){
                if(step.Step_No__c == 70)
                    obj.reinstatedDate = step.CreatedDate;
            }
            reinstatements.add(obj);
        }                                                    
        
        handler.response.data = reinstatements;
        handler.response.success = true;
        handler.response.message = '';
        return handler;
    }

    global class Reinstatement{
        global string requestNo;
        global string requestId;
        global string requestType;
        global string unitCodes;
        global string ownerName;
        global string agencyName;
        global datetime reinstatedDate;
        global list<ApprovalStep> approvals;
    }

    global class ReinstatementDetail{
        global string requestNo;
        global string requestId;
        global string requestType;
        global string unitCodes;
        global string ownerName;
        global string dealRejectionReason;//V1.1
        global date rejectedDate;        
        global date registeredDate;
        global decimal totalPrice;
        global decimal tokenAmount;
        global decimal totalCollected;
        global string dpPayment;
        global string dosName;
        global string hosName;
        global string hodName;
        global string clientName;
        global string agentName;
        global string agencyName;
        global datetime reinstatedDate;
        global list<ApprovalStep> approvals;
        global list<Booking_unit__c> units;
        //V1.1 Newly added variables 
        global decimal totalAmount;
    }

    global class ApprovalStep{ 
        global string approverName;
        global string approverEmail;
        global string action;
        global string comments;
        global string stepId;
        global datetime approvedDate;
    }

    //Get Details based on Request ID
    global static RestContextHandler getDetails(string requestId){
        RestContextHandler handler = new RestContextHandler(true);
        requestId = DAMAC_Utility.trimData(requestId);
        
        NSIBPM__Service_Request__c sr = [Select id, NSIBPM__Contact__r.Name, OwnerId, Name, Owner.Name, 
                                        List_of_Units__c, RM_Team__c, Agency__r.name, Deal_Rejected_Date__c, DP_Payment__c,
                                        Rejection_Reason__c, Rejection_Comments__c, Total_Booking_Amount__c, Token_Amount_AED__c, Registration_Date__c,
                                        (Select id, OwnerId, Owner.Type, Owner.Name, Owner.Email, Service_Request__c, Change_Status__c, Step_No__c, Is_Closed__c,
                                        Step_Status__c, Step_Type__c, Comments__c, CreatedDate, lastmodifiedBy.name, lastModifiedDate  
                                        from Steps__r where  Step_No__c >= 70  order by createdDate asc) 
                                        from NSIBPM__Service_Request__c 
                                        where id=: requestId];

        Booking__c book = [Select id 
                                from Booking__c 
                                where Deal_SR__c =: requestId];   
        List<Buyer__c> lstBuyers = [Select id, First_Name__c, Last_name__c 
                                    from Buyer__c 
                                    where Primary_Buyer__c = true and Booking__c=: book.Id];  
                                    
        string unitsCondition = 'where Booking__c = \''+book.Id+'\'';
        string unitsQuery = 'Select '+string.join(DAMAC_Utility.readFieldSet('AA_BU_Fields', 'Booking_unit__c'),',')+' from Booking_unit__c '+unitsCondition;
        list<Booking_unit__c> lstUnits = database.Query(unitsQuery);

        list<Deal_Team__c> teams = [Select id, Associated_DOS__r.name, Associated_HOS__r.name, Associated_HOD__r.name 
                                        from Deal_Team__c 
                                        where Associated_Deal__c =: requestId and Associated_PC__c =: sr.OwnerId order by createdDate desc];
        
        //V1.1 collecting DP Percent in a list
        list<Calling_List__c> callingList = [SELECT DP_Percent__c FROM Calling_List__c WHERE Booking_Unit__c IN: lstUnits ];
        
        ReinstatementDetail obj = new ReinstatementDetail();
        obj.requestNo = sr.Name;
        obj.requestType = 'Deal Re-Instatement';
        obj.requestId = sr.Id;
        obj.unitCodes = sr.List_of_Units__c;
        obj.ownerName = sr.Owner.Name;
        obj.dpPayment = sr.DP_Payment__c;
        obj.rejectedDate = sr.Deal_Rejected_Date__c;
        obj.registeredDate = sr.Registration_Date__c;
        obj.tokenAmount = sr.Token_Amount_AED__c;
        obj.agentName = sr.NSIBPM__Contact__r.name;
        obj.agencyName = sr.Agency__r.Name;
        obj.totalPrice = 0;
        obj.totalCollected = 0;
        //V1.1 Assigned Rejection reason value to the variable
        obj.dealRejectionReason = sr.Rejection_Reason__c;
        
        if(!lstBuyers.isEmpty()){
            obj.clientName = lstBuyers[0].First_Name__c + ' '+lstBuyers[0].Last_name__c;
        }
        
        if(!teams.isEmpty()){
            obj.dosName = teams[0].Associated_DOS__r.name;
            obj.hosName = teams[0].Associated_HOS__r.name;
            obj.hodName = teams[0].Associated_HOD__r.name;
        }

        if(!lstUnits.isEmpty()){
            set<Id> paymentPlanIds = new set<Id>();
            for(Booking_unit__c unit: lstUnits){
                obj.totalPrice += (unit.Requested_Price__c != null ? unit.Requested_Price__c :0);
                obj.totalCollected += (unit.Total_Collections__c != null ? unit.Total_Collections__c : 0);
                paymentPlanIds.add(unit.Payment_Plan_Id__c);
            }

            map<id, string> mpDPPEr = new map<id, string>();
            for(Payment_Terms__c term: [Select id, Percent_Value__c, Description__c, Payment_Plan__c from Payment_Terms__c 
                                    where Payment_Plan__c in: paymentPlanIds
                                    and Description__c = 'DEPOSIT']){
                mpDPPEr.put(term.Payment_Plan__c, term.Percent_Value__c); 
            }

            for(Booking_unit__c unit: lstUnits){
                if(mpDPPEr.containsKey(unit.Payment_Plan_Id__c))
                    unit.DP_Percentage__c = decimal.valueof(mpDPPEr.get(unit.Payment_Plan_Id__c));
            }

            obj.Units = lstUnits;
        }

        obj.approvals = getApprovals(sr.Steps__r);
        for(New_step__c step: sr.Steps__r){
            if(step.Step_No__c == 70)
                obj.reinstatedDate = step.CreatedDate;
        }
        
        //Fetching relevant data to get total amount from related Case
        String bookingUnitId = [SELECT Id, Booking__c,Booking__r.Deal_SR__c,Booking__r.Deal_SR__r.Name 
                                            FROM Booking_Unit__c where Booking__r.Deal_SR__c =: requestId LIMIT 1].Id;
        
        List<Case> popCaseTotalAMount = new list<Case>();                                            
        if(bookingUnitId != '' && bookingUnitId != NULL)
            popCaseTotalAMount = [SELECT Total_Amount__c FROM Case 
                                    WHERE RecordTypeName__c = 'POP' AND Booking_Unit__c =: bookingUnitId LIMIT 1];
        obj.totalAmount = !popCaseTotalAMount.isEmpty() ? popCaseTotalAMount[0].Total_Amount__c : 0;
        
        handler.response.data = obj;
        handler.response.success = true;
        handler.response.message = '';
        return handler;
    }

    //Get Appprovals Data
    global static list<ApprovalStep> getApprovals(List<New_step__c> steps){
        list<ApprovalStep> approvals = new list<ApprovalStep>();
        String approved = 'approved';
        String rejected = 'rejected';       
        set<Id> groupIds = new set<Id>();
        set<Id> userIds = new set<Id>();
        for(New_step__c step: steps){
            if(step.Owner.Type == 'Queue')
                groupIds.add(step.OwnerId);
            userIds.add(step.lastmodifiedById);
        }
        Map<id, User> mpUsers = new map<Id, User>();
        map<id, string> mpUserEmails = new map<id, string>();
        if(!groupIds.isEmpty()){
            set<Id> approverUserIds = new set<Id>();
            for(GroupMember gm :[Select Id, UserOrGroupId from GroupMember where Group.type='Queue' and GroupId in: groupIds]){
                approverUserIds.add(gm.UserOrGroupId);
            }

            mpUsers = new map<id, User>([Select id, email from user where id in: approverUserIds or id in: userIds]);
            for(GroupMember gm :[Select Id, UserOrGroupId, groupId from GroupMember where Group.type='Queue' and GroupId in: groupIds]){
                if(mpUsers.containskey(gm.UserOrGroupId))
                    mpUserEmails.put(gm.groupId, mpUserEmails.containsKey(gm.groupId) ? mpUserEmails.get(gm.groupId)+','+mpUsers.get(gm.UserOrGroupId).Email : mpUsers.get(gm.UserOrGroupId).Email);
            }
        }

        for(New_step__c step: steps){
            ApprovalStep aStep = new ApprovalStep();
            aStep.approverName = (step.Step_Status__c == 'Under GM Review' ? step.Owner.Name : step.lastmodifiedBy.name);            
            //aStep.action = step.Step_Status__c;
            //V1.1 Aligning action with three basic values as per the Step_Status__c field
            aStep.action = step.Is_Closed__c ? 'Approved' : 'Pending';
            //(step.Change_Status__c == 'Step Closed' ? 'Approved' : '');
            aStep.comments = (step.Comments__c != null ? step.Comments__c : '');
            aStep.approvedDate = step.lastModifiedDate;
            aStep.stepId = step.Id;
            System.debug('>>>>>>step.OwnerId>>>>'+step.lastmodifiedById);
            if(aStep.action == 'Approved')
                aStep.approverEmail = mpUsers.get(step.lastmodifiedById).Email;
            else
                aStep.approverEmail = step.Owner.Type != 'Queue' ? step.Owner.Email :mpUserEmails.get(step.OwnerId);
            //aStep.approverEmail = step.Owner.Email;
            approvals.add(aStep);
        }   
        return approvals;
    }

    //Manage Approval based on Action
    global static RestContextHandler handleApproval(string stepId, string action, string comments){

        RestContextHandler handler = new RestContextHandler(true);
        stepId = DAMAC_Utility.trimData(stepId);
        action = DAMAC_Utility.trimData(action);
        comments = DAMAC_Utility.trimData(comments);
        
        New_Step__c currentStep = [Select id, Step_No__c, Service_Request__c, Service_Request__r.Is_UK_Deal__c, Service_Request__r.Is_VAT_SR__c, 
                                Step_Type__c, Service_Request__r.RecordType.DeveloperName, Step_Status__c from New_Step__c where id=: stepId];
        List<String> UKDealList = new List<String>{'All'};
        if(currentStep.Service_Request__r.Is_UK_Deal__c)
            UKDealList.add('UK Only');
        else
            UKDealList.add('Non UK');
        
        
        map<id, Step_Status_Transition__c> stepStatusTransitionMap = new map<id, Step_Status_Transition__c>([SELECT Id, From_Step_Status__c, To_Step_Status__c, 
                                                                                                                    Step_Type__c, SR_External_Status__c, 
                                                                                                                    SR_Internal_Status__c, Is_Closed__c
                                                                                                                    FROM Step_Status_Transition__c 
                                                                                                                    WHERE Step_Type__c =: currentStep.Step_Type__c 
                                                                                                                    AND From_Step_Status__c =: currentStep.Step_Status__c
                                                                                                                    AND Step_No__c = :currentStep.Step_No__c
                                                                                                                    AND Is_VAT_SR__c = :currentStep.Service_Request__r.Is_VAT_SR__c
                                                                                                                    AND Service_Request_Type__c = :currentStep.Service_Request__r.RecordType.DeveloperName
                                                                                                                    AND UK_Deal__c IN: UKDealList
                                                                                                                ]);
        
        string transitionId = (action.toLowerCase() == 'approved' || action.toLowerCase() == 'approve') ? System.Label.Reinstatement_Approved_Transition_ID : System.Label.Reinstatement_Rejected_Transition_ID;

        if(stepStatusTransitionMap.containsKey(transitionId)){
            Step_Status_Transition__c transition = stepStatusTransitionMap.get(transitionId);
            currentStep.Step_Status__c = transition.To_Step_Status__c;
            currentStep.Is_Closed__c = transition.Is_Closed__c;
            currentStep.Comments__c = comments; 
            
            NSIBPM__Service_Request__c serviceRequest = new NSIBPM__Service_Request__c(Id = currentStep.Service_Request__c);
            if(String.isNotBlank(transition.SR_Internal_Status__c))
                serviceRequest.Internal_Status__c = transition.SR_Internal_Status__c; 
            if(String.isNotBlank(transition.SR_External_Status__c))
                serviceRequest.External_Status__c = transition.SR_External_Status__c;
                
            update currentStep;
            update serviceRequest;
            handler.response.success = true;
            handler.response.message = 'Action submitted successfully.';
        }
        else{
            handler.response.success = false;
            handler.response.message = 'No transition found. Please contact IT support.';
        }

        handler.response.data = '';
        return handler;

    }
}