public with sharing class AOPTMQService 
{
    // method to make callout to fetch the master milestone events for payment terms
    public static MileStoneEventsWrapper.MileStoneEvents getMasterMilestone(String strRegID)
    {   
        MileStoneEventsWrapper.MileStoneEvents objMileStoneEvents = new MileStoneEventsWrapper.MileStoneEvents();
        try
        {
            AOPTMQClass.AOPTHttpSoap11Endpoint objHttpsoap = new AOPTMQClass.AOPTHttpSoap11Endpoint();
            objHttpsoap.timeout_x = 120000;
            String strResponse = objHttpsoap.getMasterMilestone(strRegID);
            system.debug('strResponse '+strResponse);
            objMileStoneEvents = MileStoneEventsWrapper.parse(strResponse);
            system.debug('objMileStoneEvents '+objMileStoneEvents);

            if(String.isNotBlank(objMileStoneEvents.status) && objMileStoneEvents.status.equalsIgnoreCase('e'))
            {
                errorLogger('Error getMasterMilestone response '+objMileStoneEvents.message);

            }
        }
        catch(Exception exp)
        {
            errorLogger('Error getMasterMilestone '+exp.getMessage()+' - '+exp.getStackTraceString());
            objMileStoneEvents.customErrorMsg = 'Error Fetching Master MileStones FROM IPMS.';
        }
        return objMileStoneEvents;
    }

    // method to make callout to fetch the milestone payment details from MQ
    public static MileStonePaymentDetailsWrapper.MileStonePaymentDetails getMilestonePaymentDetails(String strRegID)
    {
        AOPTMQClass.AOPTHttpSoap11Endpoint objHttpsoap = new AOPTMQClass.AOPTHttpSoap11Endpoint();
        objHttpsoap.timeout_x = 120000;
        MileStonePaymentDetailsWrapper.MileStonePaymentDetails objMileStonePaymentDetails = new MileStonePaymentDetailsWrapper.MileStonePaymentDetails();
        try
        {
            String strResponse = objHttpsoap.getMilestonePaymentDetails(strRegID);
            system.debug('strResponse '+strResponse);
            objMileStonePaymentDetails = MileStonePaymentDetailsWrapper.parse(strResponse);
            system.debug('objMileStonePaymentDetails '+objMileStonePaymentDetails);
            if(String.isNotBlank(objMileStonePaymentDetails.status) && objMileStonePaymentDetails.status.equalsIgnoreCase('e'))
            {
                errorLogger('Error getMilestonePaymentDetails response '+objMileStonePaymentDetails.message);
                
            }
        }
        catch(Exception exp)
        {
            errorLogger('Error getMilestonePaymentDetails '+exp.getMessage()+' - '+exp.getStackTraceString());
            objMileStonePaymentDetails.customErrorMsg = 'Error fetching Milestone Payment Details from IPMS.';
        }
        return objMileStonePaymentDetails;
    }
    
    // Method to call rule engine to check AOPT eligibility and fetch approving authorities
    public static AOPTRuleResponse checkAOPTEligibility(String strRegId, String strSubProcessName, String strProjectCity, 
        String strProject, String strBuildingCode, String strBedroomType, String strUnitType, String strPermittedUse, 
        String strEHOFlag, String strHOFlag, String strNationality, String strPortfolioValue, String strProposedPaymentPlanType,
        String strPerCompletionInstallmentInProposedPlan, String strDiffCurAcdAndLastNonCompletionInstallmentInProposedPlan,
        String strMaximumMovementInAnInstallmentDays, String strNumberOfInstallmentsMoved, String strPriorAopt, 
        String strPaymentsEquallyDistributed, String strPaymentPlanExtendedBeyondAcd, String strOriginalPaymentPlanType) {
            
        AOPTRule.AOPTRuleHttpSoap11Endpoint objReq = new AOPTRule.AOPTRuleHttpSoap11Endpoint();
        objReq.timeout_x = 120000;
        system.debug('strSubProcessName '+strSubProcessName);
        try {
            String strResponse = objReq.AoptDetails(strRegId, strSubProcessName, strProjectCity, strProject, strBuildingCode, 
                strBedroomType, strUnitType, strPermittedUse, strEHOFlag, strHOFlag, strNationality, strPortfolioValue, 
                strProposedPaymentPlanType, strPerCompletionInstallmentInProposedPlan, strDiffCurAcdAndLastNonCompletionInstallmentInProposedPlan, 
                strMaximumMovementInAnInstallmentDays, strNumberOfInstallmentsMoved, strPriorAopt, strPaymentsEquallyDistributed, 
                strPaymentPlanExtendedBeyondAcd, strOriginalPaymentPlanType, '', '', '', '', '', '', '', '');
                
            if(String.isNotBlank(strResponse)) {
                Map<String, Object> mapResponse = (Map<String, Object>)JSON.deserializeUntyped(strResponse);
                system.debug('mapResponse '+mapResponse);
                if(mapResponse.get('allowed') != null) {
                    String strAllowed = (String)mapResponse.get('allowed');
                    /*if(String.isNotBlank(strAllowed) && strAllowed.equalsIgnoreCase('Yes') &&
                       mapResponse.get('approvingAuthorityOne') != null) {
                        String strAppAuth = (String)mapResponse.get('approvingAuthorityOne');
                        String strAppAuth2 = (String)mapResponse.get('approvingAuthorityTwo');
                        String strAppAuth3 = (String)mapResponse.get('approvingAuthorityThree');
                        
                        if(String.isNotBlank(strAppAuth2)) {
                            strAppAuth += ',' + strAppAuth2;
                        }
                        
                        if(String.isNotBlank(strAppAuth3)) {
                            strAppAuth += ',' + strAppAuth3;
                        }
                        return new AOPTRuleResponse(strAllowed, null, strAppAuth);
                    }*/
                    if(String.isNotBlank(strAllowed) && strAllowed.equalsIgnoreCase('Yes')) {
                        String strAppAuth = '';
                        
                        //strAppAuth = RoleUtility.getSalesforceRole(strAppAuth);
                        
                        String strRecAuth1 = (String)mapResponse.get('recommendingAuthorityOne');
                        String strRecAuth2 = (String)mapResponse.get('recommendingAuthorityTwo');
                        String strRecAuth3 = (String)mapResponse.get('recommendingAuthorityThree');
                        String strRecAuth4 = (String)mapResponse.get('recommendingAuthorityFour');
                        String strAppAuth1 = (String)mapResponse.get('approvingAuthorityOne');
                        String strAppAuth2 = (String)mapResponse.get('approvingAuthorityTwo');
                        String strAppAuth3 = (String)mapResponse.get('approvingAuthorityThree');
                        
                        if(String.isNotBlank(strRecAuth1)) {
                            //strRecAuth1 = RoleUtility.getSalesforceRole(strRecAuth1);
                            strAppAuth = strRecAuth1 + ',';
                        }
                        
                        if(String.isNotBlank(strRecAuth2)) {
                            //strRecAuth2 = RoleUtility.getSalesforceRole(strRecAuth2);
                            strAppAuth += strRecAuth2 + ',';
                        }
                        
                        if(String.isNotBlank(strRecAuth3)) {
                            //strRecAuth3 = RoleUtility.getSalesforceRole(strRecAuth3);
                            strAppAuth += strRecAuth3 + ',';
                        }
                        
                        if(String.isNotBlank(strRecAuth4)) {
                            //strRecAuth4 = RoleUtility.getSalesforceRole(strRecAuth4);
                            strAppAuth += strRecAuth4 + ',';
                        }
                        if(String.isNotBlank(strAppAuth1)) {
                            //strAppAuth1 = RoleUtility.getSalesforceRole(strAppAuth1);
                            strAppAuth += strAppAuth1 + ',';
                        }
                        
                        if(String.isNotBlank(strAppAuth2)) {
                            //strAppAuth2 = RoleUtility.getSalesforceRole(strAppAuth2);
                            strAppAuth += strAppAuth2 + ',';
                        }
                        
                        if(String.isNotBlank(strAppAuth3)) {
                            //strAppAuth3 = RoleUtility.getSalesforceRole(strAppAuth3);
                            strAppAuth += strAppAuth3 + ',';
                        }
                        
                        strAppAuth = strAppAuth.removeEnd(',');
                        strAppAuth = strAppAuth.replace('CRM-', '');
                        
                        String strMessage = mapResponse.get('message') != null ? (String)mapResponse.get('message') : '';
                        return new AOPTRuleResponse(strAllowed, strMessage, strAppAuth);
                    }
                    else if(String.isNotBlank(strAllowed) && strAllowed.equalsIgnoreCase('No')) {
                        String strMessage = (String)mapResponse.get('message');
                        return new AOPTRuleResponse('No', strMessage, null);
                    }
                    else {
                        return new AOPTRuleResponse('Error', null, null);
                    }
                }
            }
            else {
                return new AOPTRuleResponse('Error', null, null);
            }
        }
        catch(Exception excGen) {
            System.debug('--excGen.getMessage()---'+excGen.getMessage());
            //errorLogger('checkAOPTEligibility '+excGen.getMessage());
            return new AOPTRuleResponse('Error', null, null);
        }
        return null;
    }

    public static String createPaymentPlanIPMS(String strRegID,String strCaseNumber,String strSRType,paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5 [] lstPaymentTermInIPMS)
    { 
      //AOPTMQClass.AOPTHttpSoap11Endpoint objReq = new AOPTMQClass.AOPTHttpSoap11Endpoint();
      //objReq.timeout_x = 120000;
      
      AOPTMQClass.AOPTHttpSoap11Endpoint objHttpsoap = new AOPTMQClass.AOPTHttpSoap11Endpoint();
      objHttpsoap.timeout_x = 120000;
      String strResponse = objHttpsoap.PaymentPlanCreation(strRegID,'2-'+strCaseNumber,strSRType,lstPaymentTermInIPMS);
      return strResponse;
    }

    //method to generate the offer and acceptance letter for booking units
    public static String generateOfferAccetanceLetter(AOPTDocumentGenerationXsd.DocGenDTO objDocGenDTO)
    {
      AOPTDocumentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint objGenerateLetter = new AOPTDocumentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint();
      objGenerateLetter.timeout_x = 120000;
      String strResponse = objGenerateLetter.DocGeneration( 'AOPT_Payment_Plan_Addendum' , objDocGenDTO );
      return strResponse;
    }
    
    public Class AOPTRuleResponse {
        public String strAllowed;
        public String strMessage;
        public String strApprovingAuthority;
        
        public AOPTRuleResponse(String strAllowed, String strMessage, String strApprovingAuthority) {
            this.strAllowed = strAllowed;
            this.strMessage = strMessage;
            this.strApprovingAuthority = strApprovingAuthority;
        }
    }

    private static void errorLogger(string strErrorMessage)
    {
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        objError.Process_Name__c = 'AOPT';
        insert objError;
    }
}