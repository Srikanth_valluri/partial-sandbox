/**************************************************************************************************
* Name               : TravelDetailsUpdateUtilEmailTest
* Description        : Test class for TravelDetailsUpdateUtilEmail batch and scheduler class.
* Created Date       : 28/02/2020
* Created By         : QBurst
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst          28/02/2020      Initial Draft.
**************************************************************************************************/
@isTest
private class TravelDetailsUpdateUtilEmailTest {

    static testMethod void testSchedulerMethod() {
        Test.startTest();
        TravelDetailsUpdateUtilEmailScheduler tdEmailSch = new TravelDetailsUpdateUtilEmailScheduler();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Travel Details Email Scheduler', sch, tdEmailSch); 
        
        Test.stopTest();
    }
    
    static testmethod void testBatchMethod_1(){
        Inquiry__c inqObj= new Inquiry__c(
            Inquiry_Source__c='Agent Referral',
            Mobile_Phone_Encrypt__c='456123',
            Mobile_CountryCode__c='American Samoa: 001684',
            Mobile_Phone__c='1234',
            Email__c='mk@gmail.com',
            First_Name__c='Test',
            Last_Name__c='Last',
            Meeting_Type__c = 'Scheduled Tour'
         );
         
         Email_Notification_To_CC_List__c custSetting = new Email_Notification_To_CC_List__c();
         custSetting.Name = 'Travel Details Update Utilization';
         custSetting.To_Addresses__c = 'test1@testemail.com, test2@testemal.com';
         custSetting.CC_Addresses__c = 'test3@testemail.com, test4@testemal.com';
         custSetting.BCC_Addresses__c = 'test5@testemail.com, test6@testemal.com';
         insert custSetting;
      //   insert inqObj;
            
         Travel_Details__c td = new Travel_Details__c();
        // td.Inquiry__c = inqObj.id;
         td.From_Date__c = Date.newInstance(2018,07,06);
         td.To_Date__c = Date.newInstance(2018,07,08);
         td.Status__c = 'Procured';
         insert td;
         
         TravelDetailsUpdateUtilEmailBatch obj = new TravelDetailsUpdateUtilEmailBatch();
         DataBase.executeBatch(obj); 
     }
}