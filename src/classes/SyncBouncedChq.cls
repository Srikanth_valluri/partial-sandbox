/****************************************************************************************************************
    * Version       Author          Date        Description                                                         *
    *---------------------------------------------------------------------------------------------------------------*
    * 1.0          Aditya 13/07/2020   For Bounced Chq                            *
    *****************************************************************************************************************/

    @RestResource(urlMapping='/SyncBouncedChq/*')
    global with sharing class SyncBouncedChq{   
        @HttpPost
        global static ResponseWrapper SyncBouncedChq( List<String> lstRegId
                                               , String actionType
                                               , String PartyId 
                                               , String ChqNo, Decimal ChqAmnt
                                               , Date ChqDate , String BankName
                                               , String ReceiptId
                                               , String NewReceiptId
                                               ,String BouncedReason
                                               ) { 
           String responseMsg = '';      
           
           EmailTemplate emailTemplateObj = [SELECT Id,Name
                                                    , Subject
                                                    , Body
                                                    , HtmlValue
                                                    , TemplateType
                                                    , BrandTemplateId
                                                    FROM EmailTemplate 
                                                    WHERE Name = 'Bounce Check Email Template'  LIMIT 1 ];
                                                    
                                               
                                                    
           ResponseWrapper objResponseWrapper = new ResponseWrapper();
           if(actionType == 'CreateSR'){
           String Insrt='';
            System.debug('-->>PId: '+ PartyId);         
            
                if(PartyId!='' && ChqNo!='' ) {
                    List<Calling_List__c> CLst= new List<Calling_List__c>();
                    List<Case> CaseList=new List<Case>();
                    List<Account> AcntLst;
                    List<Booking_Unit__c> BULst=[Select Id,
                    Calling_List__c,Unit_Name__c,
                            Case__C from Booking_Unit__c 
                            WHERE Registration_Id__c In : lstRegId];
                    
                    
                    String contentBody = emailTemplateObj.body;
                    contentBody = String.isNotBlank(ChqNo)?contentBody.replace('{!chqNo}', ChqNo):contentBody.replace('{!chqNo}', '');
                    contentBody = String.isNotBlank(String.valueOf(ChqAmnt))?contentBody.replace('{!amount}', String.valueOf(ChqAmnt)):contentBody.replace('{!amount}', '');
                    contentBody = String.isNotBlank(BULst[0].Unit_Name__c)?contentBody.replace('{!unitNo}', BULst[0].Unit_Name__c):contentBody.replace('{!unitNo}', '');
                    contentBody = String.isNotBlank(BouncedReason)?contentBody.replace('{!financeRemarks}', BouncedReason):contentBody.replace('{!financeRemarks}', '');
                    contentBody = String.isNotBlank(String.valueOf( ChqDate))?contentBody.replace('{!date}',String.valueOf( ChqDate)):contentBody.replace('{!date}', '');       
                            
                            
                            
                            
                            
                            
                    AcntLst=[Select Id
                                    ,Mobile_Person_Business__c
                                ,Person_Business_Email__c
                    from Account where Party_Id__c =: PartyId limit 1];
                    
                    Id caseRecTypeId1 = Schema.SObjectType.Case.getRecordTypeInfosByName().
                                                    get('Bounced Cheque SR').getRecordTypeId();
                    if(AcntLst.size()>0 && BULst .size()>0) {
                        Case Cs = new Case();
                        Cs.AccountId = AcntLst[0].Id;
                        Cs.Booking_Unit__c = BULst[0].Id;//need to change to BUId
                        Cs.RecordTypeId = caseRecTypeId1;
                        Cs.Cheque_Number__c = ChqNo;
                        Cs.Cheque_Amount__c = ChqAmnt;
                        Cs.Receipt_Id__c = ReceiptId;
                        Cs.Cheque_Bank_Name__c = BankName;
                        Cs.Bounced_Cheque_Date_of_Expiry__c=date.valueOf(ChqDate);
                        CaseList.add(Cs);
                        List<Database.SaveResult> insertResults = new List<Database.SaveResult>();
                        if(CaseList.size()>0)
                        {
                            insertResults = Database.insert(CaseList, false);
                        } 
                        
                        Id CLRecTypeId1 = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().
                                                        get('Bounced Cheque').getRecordTypeId();
                        
                        Calling_List__c CL= new Calling_List__c();
                        CL.RecordTypeId= CLRecTypeId1 ;
                        CL.Calling_List_Type__c = 'BC Calling';
                        CL.Registration_Id__c=lstRegId[0];
                        CL.Account__c=AcntLst[0].Id;
                        CL.Case__c=CaseList[0].Id;
                        CLst.add(CL);
                        if(CLst.size()>0){
                            try{
                                insert CLst;
                                 if (insertResults.get(0).isSuccess()){
                                    List<case> lstnewCase = [select Id,CaseNumber

                                    From Case Where Id =: CaseList[0].Id   ];
                                    objResponseWrapper.SRNum = lstnewCase [0].CaseNumber;
                                    objResponseWrapper.statusCode = '200';
                                    objResponseWrapper.errorMessage = 'SR created Sucessfully ';
                                    sendNotificatonToCX(PartyId,
                                                    chqNo,
                                                    String.valueOf(ChqAmnt),
                                                    BULst[0].Unit_Name__c,
                                                    BouncedReason,
                                                    String.valueOf(ChqDate)
                                                );
                                    if(emailTemplateObj != NUll && String.isNotBlank(emailTemplateObj.Body)
                                                        && String.isNotBlank(AcntLst[0].Mobile_Person_Business__c)){
                                        SendSMSAccountService.Sendtextmessage(
                                        new List<String>{AcntLst[0].Mobile_Person_Business__c},
                                        contentBody,AcntLst[0].Id,false,'');
                                    }
                                    
                                }
                            }
                            catch(exception e){
                                objResponseWrapper.SRNum = '';
                                objResponseWrapper.statusCode ='400';
                                objResponseWrapper.errorMessage ='SR not created';
                            }
                            
                        
                        }
                        List<SR_Booking_Unit__c> SRunits = new List<SR_Booking_Unit__c>();
                        
                        for(Booking_Unit__c objBU: BULst ){
                            SR_Booking_Unit__c objSRBU = new SR_Booking_Unit__c();
                            objSRBU.Case__c = CaseList[0].Id;
                            objSRBU.Booking_Unit__c = objBU.Id;
                            SRunits.add(objSRBU);
                        }
                        insert SRunits;
                    }else{
                        objResponseWrapper.SRNum = '';
                        objResponseWrapper.statusCode ='400';
                        objResponseWrapper.errorMessage ='Plz pass correct party and RegId';
                    }
                    
                }else{
                    objResponseWrapper.SRNum = '';
                    objResponseWrapper.statusCode ='400';
                    objResponseWrapper.errorMessage ='Plz pass Party ID';
                }
                    
                

                //return objResponseWrapper;   
            }else if(actionType == 'UpdateSR'){
            
                if(String.isNotBlank(ReceiptId) && String.isNotBlank(BouncedReason)){                                     
                List<case> lstCase =[Select id,Receipt_Id__c,Bounced_Reason__c,CaseNumber,
                                    (Select id,OwnerId,CE_Comments__c from Calling_List__r)
                                        FROM 
                                    Case WHERE Receipt_Id__c =: ReceiptId
                                            AND Status != 'Closed'] ;
                if(lstCase.size() < 1){
                    objResponseWrapper.SRNum = '';
                    objResponseWrapper.statusCode = '400';
                    objResponseWrapper.errorMessage = ' NO Active SR for recieved System ';
                }else if (lstCase.size() > 0){
                    lstCase[0].Bounced_Reason__c = BouncedReason;
                    try{
                        if(lstCase[0].Calling_List__r.size()>0){
                           lstCase[0].Calling_List__r[0].CE_Comments__c ='bounce cheque reason updated';
                            update lstCase[0].Calling_List__r;
                        }
                        update lstCase;
                        objResponseWrapper.SRNum = lstCase[0].CaseNumber;
                        objResponseWrapper.statusCode = '200';
                        objResponseWrapper.errorMessage = 'SR updated Sucessfully ';
                    }catch(exception e){
                        objResponseWrapper.SRNum = lstCase[0].CaseNumber;
                        objResponseWrapper.statusCode ='400';
                        objResponseWrapper.errorMessage ='SR not updated';
                    }
                    
                }
                }
                
            }
            else if(actionType == 'CloseSR'){
            
                if(String.isNotBlank(ReceiptId) && String.isNotBlank(NewReceiptId)){                                     
                List<case> lstCase =[Select id,Receipt_Id__c,
                                        CRE_Comments__c,SR_with_Legal__c,
                                        status,CaseNumber,
                                        New_Receipt_Id__c,
                                        (Select id,OwnerId,CE_Comments__c from Calling_List__r)
                                        FROM 
                                    Case WHERE Receipt_Id__c =: ReceiptId
                                            AND Status != 'Closed'] ;
                if(lstCase.size() < 1){
                    objResponseWrapper.SRNum = '';
                    objResponseWrapper.statusCode = '400';
                    objResponseWrapper.errorMessage = ' NO Active SR for recieved System ';
                }else if (lstCase.size() > 0){
                    lstCase[0].CRE_Comments__c = 'new Cheque is submitted plz refer new New Receipt Id ';
                    lstCase[0].status = 'Closed';
                    lstCase[0].New_Receipt_Id__c = NewReceiptId;
                    List<Task > tskLst = new  List<Task >();
                    if(lstCase[0].SR_with_Legal__c){
                        Task  objTask = new Task();
                        objTask.Subject = 'Withdraw legal action';
                        objTask.Assigned_User__c = 'CRE';
                        //objTask.CurrencyIsoCode = 'UAE Dirham';
                        objTask.ActivityDate = Date.today() + 1;
                        objTask.OwnerId = Label.UserIdLegal;
                        objTask.Priority = 'High';
                        objTask.Process_Name__c = '';
                        objTask.Status = 'Not Started';
                        objTask.WhatId = lstCase[0].Id;
                        tskLst.add(objTask);
                        
                    }
                    
                    try{
                        if(lstCase[0].Calling_List__r.size()>0){
                           lstCase[0].Calling_List__r[0].CE_Comments__c ='new cheque is submitted';
                            update lstCase[0].Calling_List__r;
                        }
                        if( tskLst.size() > 0){
                        insert tskLst;}
                        
                        update lstCase;
                        objResponseWrapper.SRNum = lstCase[0].CaseNumber;
                        objResponseWrapper.statusCode = '200';
                        objResponseWrapper.errorMessage = 'SR updated Sucessfully ';
                    }catch(exception e){
                        objResponseWrapper.SRNum = lstCase[0].CaseNumber;
                        objResponseWrapper.statusCode ='400';
                        objResponseWrapper.errorMessage ='SR not updated';
                    }
                    
                }
                }
                
            }else if(actionType == 'Paid'){
            
                if(String.isNotBlank(ReceiptId)){                                     
                List<case> lstCase =[Select id,Receipt_Id__c,
                                        CRE_Comments__c,SR_with_Legal__c,
                                        status,CaseNumber,
                                        New_Receipt_Id__c,
                                        (Select id,OwnerId,CE_Comments__c from Calling_List__r)
                                        FROM 
                                    Case WHERE Receipt_Id__c =: ReceiptId
                                            AND Status != 'Closed'] ;
                if(lstCase.size() < 1){
                    objResponseWrapper.SRNum = '';
                    objResponseWrapper.statusCode = '400';
                    objResponseWrapper.errorMessage = ' NO Active SR for recieved System ';
                }else if (lstCase.size() > 0){
                    lstCase[0].CRE_Comments__c = 'new Cheque is submitted plz refer new New Receipt Id ';
                    lstCase[0].status = 'Closed';
                    lstCase[0].New_Receipt_Id__c = NewReceiptId;
                    List<Task > tskLst = new  List<Task >();
                    if(lstCase[0].SR_with_Legal__c){
                        Task objTask = new Task();
                        objTask.Subject = 'Withdraw legal action';
                        objTask.Assigned_User__c = 'CRE';
                        //objTask.CurrencyIsoCode = 'UAE Dirham';
                        objTask.ActivityDate = Date.today() + 1;
                        objTask.OwnerId = Label.UserIdLegal;
                        objTask.Priority = 'High';
                        objTask.Process_Name__c = '';
                        objTask.Status = 'Not Started';
                        objTask.WhatId = lstCase[0].Id;
                        tskLst.add(objTask);
                    }
                    
                    try{
                        if(lstCase[0].Calling_List__r.size()>0){
                           lstCase[0].Calling_List__r[0].CE_Comments__c ='Paid';
                            update lstCase[0].Calling_List__r;
                        }
                        if( tskLst.size() > 0){
                            insert tskLst;}
                        update lstCase;
                        objResponseWrapper.SRNum = lstCase[0].CaseNumber;
                        objResponseWrapper.statusCode = '200';
                        objResponseWrapper.errorMessage = 'SR updated Sucessfully ';
                    }catch(exception e){
                        objResponseWrapper.SRNum = lstCase[0].CaseNumber;
                        objResponseWrapper.statusCode ='400';
                        objResponseWrapper.errorMessage ='SR not updated';
                    }
                    
                }
                }
                
            }
            return objResponseWrapper;
        }
        @future(callout=true)
        public static  void  sendNotificatonToCX(String partyId,
                                                String chqNo,
                                                String amount,
                                                String unitNo,
                                                String financeRemarks,
                                                string strdate){
        
            List<Account> AcntLst=[Select Id
                            ,Mobile_Person_Business__c
                            ,Person_Business_Email__c 
            from Account where Party_Id__c =: PartyId limit 1];
            
            List<Attachment> lstAttach = new List<Attachment>();
            
            EmailTemplate emailTemplateObj = [SELECT Id,Name
                                                    , Subject
                                                    , Body
                                                    , HtmlValue
                                                    , TemplateType
                                                    , BrandTemplateId
                                                    FROM EmailTemplate 
                                                    WHERE Name = 'Bounce Check Email Template'
                                                LIMIT 1 ];
                                                    
            
            Attachment[] attachmentLst = [ SELECT Id,ContentType,Name, Body  From Attachment where parentId = : emailTemplateObj.Id];
            lstAttach.addAll(  attachmentLst );
            
            

            String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';
            String subject, strAccountId, strDPIId, bccAddress ='', strCCAddress;
            
            strAccountId = AcntLst[0].Id;

            
            toAddress = String.isNotBlank(AcntLst[0].Person_Business_Email__c) ?
                AcntLst[0].Person_Business_Email__c : '' ;
            
            System.debug('toAddress = ' + toAddress);
            // need to verify 
            fromAddress = 'atyourservice@damacproperties.com';
            bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
            contentType = 'text/html';
            strCCAddress = '';
            system.debug(' emailTemplateObj : '+ emailTemplateObj );
            if( toAddress != '' && emailTemplateObj != null ) {
                
                if(emailTemplateObj.body != NULL){
                    contentBody =  emailTemplateObj.body;
                }
                if(emailTemplateObj.htmlValue != NULL){
                    contentValue = emailTemplateObj.htmlValue;
                }
                if(string.isblank(contentValue)){
                    contentValue='No HTML Body';
                }
                subject = emailTemplateObj.Subject != NULL ? emailTemplateObj.Subject : 'Bounce Cheque';
                //chqNo amount unitNo financeRemarks strdate
                contentValue = String.isNotBlank(chqNo)?contentValue.replace('{!chqNo}', chqNo): contentValue.replace('{!chqNo}', '');
                contentValue = String.isNotBlank(amount)?contentValue.replace('{!amount}', amount):contentValue.replace('{!amount}', '');
                contentValue = String.isNotBlank(unitNo)?contentValue.replace('{!unitNo}', unitNo):contentValue.replace('{!unitNo}', '');
                contentValue = String.isNotBlank(financeRemarks)?contentValue.replace('{!financeRemarks}', financeRemarks):contentValue.replace('{!financeRemarks}', '');
                contentValue = String.isNotBlank(strdate)?contentValue.replace('{!date}', strdate):contentValue.replace('{!date}', '');
                    
                   
                
                contentBody = String.isNotBlank(chqNo)?contentBody.replace('{!chqNo}', chqNo):contentBody.replace('{!chqNo}', '');
                contentBody = String.isNotBlank(amount)?contentBody.replace('{!amount}', amount):contentBody.replace('{!amount}', '');
                contentBody = String.isNotBlank(unitNo)?contentBody.replace('{!unitNo}', unitNo):contentBody.replace('{!unitNo}', '');
                contentBody = String.isNotBlank(financeRemarks)?contentBody.replace('{!financeRemarks}', financeRemarks):contentBody.replace('{!financeRemarks}', '');
                contentBody = String.isNotBlank(strdate)?contentBody.replace('{!date}', strdate):contentBody.replace('{!date}', '');
               
               
                
                    // Callout to sendgrid to send an email
                    SendGridEmailService.SendGridResponse objSendGridResponse =
                        SendGridEmailService.sendEmailService(
                            toAddress
                            , ''
                            , strCCAddress
                            , ''
                            , bccAddress
                            , ''
                            , subject
                            , ''
                            , fromAddress
                            , ''
                            , replyToAddress
                            , ''
                            , contentType
                            , contentValue
                            , ''
                            , lstAttach
                        );

                    System.debug('objSendGridResponse == ' + objSendGridResponse);
                    //Create Email Activity
                    String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                    if (responseStatus == 'Accepted') {
                        EmailMessage mail = new EmailMessage();
                        mail.Subject = subject;
                        mail.MessageDate = System.Today();
                        mail.Status = '3';// 
                        mail.RelatedToId = strAccountId;
                        mail.Account__c  = strAccountId;
                        mail.Type__c = 'Bounce Cheque';
                        mail.ToAddress = toAddress;
                        mail.FromAddress = fromAddress;
                        mail.TextBody = contentBody;
                        mail.HTMLBody = contentValue;
                        mail.Sent_By_Sendgrid__c = true;
                        mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                        mail.CcAddress = strCCAddress;
                        mail.BccAddress = bccAddress;
                        
                        insert mail;
                    }   
            
            }
        
        }


        //for sending case # back in response
        global class ResponseWrapper {
            global String SRNum;
            global String statusCode;
            global String errorMessage;
           
            global ResponseWrapper() {
                this.SRNum = '';
                this.statusCode = '';
                this.errorMessage = '';
            }
           }
    }