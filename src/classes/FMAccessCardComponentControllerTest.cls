@isTest
private class FMAccessCardComponentControllerTest {
    static Booking_unit__c objBu;
    static Account objAcc;
    @testSetup
    static void createCommonTestData() {
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        objAcc = TestDataFactoryFM.createAccount();
        objAcc.party_ID__C = '1039032';
        insert objAcc;

        Id personAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acctInsOwner=new Account(RecordTypeID = personAccountId,PersonEmail='a@g.com',FirstName = 'Test FName Owner',
                                    LastName = 'Test LName',Passport_Number__c='12345678',Mobile__c='1212121212',Address_Line_1__c='test');
        insert acctInsOwner;
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        objSR = TestDataFactoryFM.createServiceRequest(objAcc);
        insert objSR;
        Booking__c objBooking = TestDataFactoryFM.createBooking(acctInsOwner, objSR);
        insert objBooking;
        objBu = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
        insert objBu;
        Property__c propObj = TestDataFactoryFM.createProperty();
        insert propObj;
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Property_name__c = propObj.id;
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        User u = TestDataFactoryFM.createUser(profileId.id);
        u.IPMS_Employee_ID__c = 'IPMS123';
        u.Sales_Office__c = 'AKOYA'; 
        u.ManagerId = UserInfo.getUserId();
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;
    }
	@isTest static void test_newSR() {
        PageReference myVfPage = Page.FMAccessCardProcessPage;
        Test.setCurrentPage(myVfPage);
        objBu = [SELECT Id from Booking_Unit__c limit 1];
        objAcc = [SELECT Id from Account limit 1];
        System.debug('===objBu==='+objBu);
        ApexPages.currentPage().getParameters().put('UnitId',objBu.id);
        ApexPages.currentPage().getParameters().put('AccountId',objAcc.id);
        ApexPages.currentPage().getParameters().put('SRType','Request_for_Access_cards');
        FMAccessCardComponentController controller = new FMAccessCardComponentController();
	}

	@isTest static void test_uploadDocument() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = [SELECT Id FROM User WHERE Profile.name = 'System Administrator' AND isActive = True limit 1];
        FMAccessCardComponentController controller;
        System.runAs(u){
        PageReference myVfPage = Page.FMAccessCardProcessPage;
        Test.setCurrentPage(myVfPage);
        objBu = [SELECT Id from Booking_Unit__c limit 1];
        objAcc = [SELECT Id from Account limit 1];
        System.debug('===objBu==='+objBu);
        ApexPages.currentPage().getParameters().put('UnitId',objBu.id);
        ApexPages.currentPage().getParameters().put('AccountId',objAcc.id);
        ApexPages.currentPage().getParameters().put('SRType','Request_for_Access_cards');

        //Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());
        controller = new FMAccessCardComponentController();
        controller.createCaseShowUploadDoc();
        controller.strDocName = 'VfRemoting.Js';
        controller.strDocBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        }
        Test.startTest();
        controller.uploadDocument();
        controller.saveAsDraft();
        Test.stopTest();
	}
    @isTest static void test_deleteDocument() {
        //Inserting SR_Attachments__c
        SR_Attachments__c objAtt = new SR_Attachments__c(Name='NOC for Landlord');
        insert objAtt;


        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = [SELECT Id FROM User WHERE Profile.name = 'System Administrator' AND isActive = True limit 1];
        FMAccessCardComponentController controller;
        System.runAs(u){
        PageReference myVfPage = Page.FMAccessCardProcessPage;
        Test.setCurrentPage(myVfPage);
        objBu = [SELECT Id from Booking_Unit__c limit 1];
        objAcc = [SELECT Id from Account limit 1];
        System.debug('===objBu==='+objBu);
        ApexPages.currentPage().getParameters().put('UnitId',objBu.id);
        ApexPages.currentPage().getParameters().put('AccountId',objAcc.id);
        ApexPages.currentPage().getParameters().put('SRType','Request_for_Access_cards');

        //Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());
        controller = new FMAccessCardComponentController();
        controller.createCaseShowUploadDoc();
        controller.strDocName = 'VfRemoting.Js';
        controller.strDocBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        }
        Test.startTest();
        controller.uploadDocument();
        FM_Case__c obj = [SELECT Id from FM_Case__c LIMIT 1];
        SR_Attachments__c srAtt = [SELECT Id,Name from SR_Attachments__c LIMIT 1];
        controller.docIdToDelete = obj.Id+','+srAtt.Name;
        controller.deleteDocument();
        // controller.submitSR();
        Test.stopTest();
	}
    @isTest static void test_submitSR() {
        FMAccessCardComponentController controller;
        PageReference myVfPage = Page.FMAccessCardProcessPage;
        Test.setCurrentPage(myVfPage);
        objBu = [SELECT Id from Booking_Unit__c limit 1];
        objAcc = [SELECT Id from Account limit 1];
        System.debug('===objBu==='+objBu);
        ApexPages.currentPage().getParameters().put('UnitId',objBu.id);
        ApexPages.currentPage().getParameters().put('AccountId',objAcc.id);
        ApexPages.currentPage().getParameters().put('SRType','Request_for_Access_cards');
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        controller = new FMAccessCardComponentController();
        Test.startTest();
        controller.submitSR();
        Test.stopTest();
	}

    @isTest static void test_getCaseDetails() {

       
        FM_Case__c objFM = new FM_Case__c(Type_of_Access_Card__c = 'New Request', Request_Type_DeveloperName__c = 'Request_for_Access_cards');
        insert objFM;

        SR_Attachments__c objAtt1 = new SR_Attachments__c(FM_Case__c = objFM.id, Name = 'NOC from landlord', Attachment_URL__c = 'https://partial-damacproperties.cs80.force.com/Documents/apex/GetFile?id=01NUEFKARTBLDZYKOA5RHZEBPWOFQIHS4C');
        insert objAtt1;

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = [SELECT Id FROM User WHERE Profile.name = 'System Administrator' AND isActive = True limit 1];
        FMAccessCardComponentController objController;

        System.runAs(u){
            PageReference myVfPage = Page.FMAccessCardProcessPage;
            Test.setCurrentPage(myVfPage);
            objBu = [SELECT Id from Booking_Unit__c limit 1];
            objAcc = [SELECT Id from Account limit 1];
            System.debug('===objBu==='+objBu);
            ApexPages.currentPage().getParameters().put('UnitId',objBu.id);
            ApexPages.currentPage().getParameters().put('AccountId',objAcc.id);
            ApexPages.currentPage().getParameters().put('SRType','Request_for_Access_cards');
            objController = new FMAccessCardComponentController();
            Test.startTest();
            objController.strFMCaseId = objFM.id;
            objController.getCaseDetails();
            Test.stopTest();
        }
        
        
    }
}