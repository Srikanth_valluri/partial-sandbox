/************************************************************************************************
 * @Name              : VirtualHandoverDeleteEventController
 * @Test Class Name   : VirtualHandoverDeleteEventControllerTest
 * @Description       : Controller class for deleting Teams Meeting event.
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         Rohit         13/08/2020       Created
***********************************************************************************************/

public class VirtualHandoverDeleteEventController {
    public String strEvantId;
    public String strRecordId;
    List<Calling_List__c> lst;
    public VirtualHandoverDeleteEventController(ApexPages.StandardController objController){
    	strEvantId = '';
        strRecordId = objController.getId();
        lst = [Select Virtual_Meeting_Link__c , Remarks_Pratik__c, Remarks_Vivek__c
                                      From Calling_List__c
                                   Where Id = :strRecordId];
        
        
        if(!lst.isEmpty()){
         strEvantId = lst[0].Remarks_Pratik__c;   
        }
    }
    
    /************************************************************************************************
    * @Description : Deletes the meeting link, email list and event id for Calling List Record
    * @Params      : None
    * @Return      : None 
    ************************************************************************************************/

    public pageReference deleteEvent(){
     	VirtualHandover_MSTeamsIntegration.deleteEvent(strEvantId);
        for(Integer i=0; i<lst.size(); i++){
            lst[i].Virtual_Meeting_Link__c = '';
            lst[i].Remarks_Pratik__c = '';
            lst[i].Remarks_Vivek__c = '';
        }
        try{
            update lst;
        }
        catch(Exception e){
            System.debug('Exception-->' +e);
        }
        String strReturnUrl = '/'+strRecordId;
        PageReference pg = new PageReference(strReturnUrl);
        pg.setRedirect(true);
        return pg;    
    }
}