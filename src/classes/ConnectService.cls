/***********************************************************************************************************
Description: Class to push details to Connect on insertion and updation of Task
-----------------------------------------------------------------------------------------------------------*
Version     Date                Author              Description                                 
1.0         12 Nov 2020         Komal Shitole       Initial Draft
************************************************************************************************************/

public class ConnectService {

    public static string getBearerToken() {
        Credentials_details__c creds = Credentials_details__c.getInstance('Connect Service Token');
        if( creds != null && String.isNotBlank(creds.Endpoint__c) && String.isNotBlank(creds.Password__c) && String.isNotBlank(creds.User_Name__c)) { 
            HTTP httpObj = new HTTP();
            HTTPRequest request = new HTTPRequest();
            request.setEndpoint(creds.Endpoint__c);
            request.setMethod('POST');
            request.setHeader('Api-Token', System.Label.Connect_Service_API_Token);  
            request.setHeader('Content-Type', 'application/json'); 
            String requestBody = '{"email": "'+creds.User_Name__c+'","password": "'+creds.Password__c+'","device_source": "ios/android","device_os_version": "13.3","app_version": "1.2","device_model": "iPhone x/Samnsung Galaxy","app_id": 2,"is_authorization_required": true}';
            request.setBody(requestBody);
            request.setTimeout(120000);  
            HTTPResponse resp = httpObj.send(request);
            System.debug('resp.getBody()>>>>>'+resp.getBody());         
            if( resp != null && resp.getStatusCode() == 200 && resp.getBody() != null) {
                DataWrapper objData = (DataWrapper)JSON.deserialize(resp.getBody(), DataWrapper.class);
                String accessToken = objData.data.access_token;
                System.debug('accessToken:: ' + accessToken); 
                return accessToken;         
            }
        }
        
        return null;
    }

 /********************************************************************************************************
    Method Description :  Method to get Task and Case Ids to push details to Connect
    Return Type : void 
    Parameters : List<Task>, String requestType(Start/Update)
 ********************************************************************************************************/
    public void pushDetailsToConnect(List<Task> taskList) {
        Set<Id> setCaseId = new Set<Id>();
        Set<Id> setTaskId = new Set<Id>();
        map<Id,String> mapOfCaseIdToRequestType = new map<Id,String>();
        String requestType;
        for(Task objTask : taskList) {
               if(String.isNotBlank(objTask.WhatId) && String.valueOf(objTask.WhatId).startsWith('500') && String.isNotBlank(objTask.Subject)) {
                           if(objTask.Subject.equals(System.Label.Validate_Request_and_Calculations)
                              ) { 
                                   //setTaskId.add(objTask.Id);
                                   setCaseId.add(objTask.WhatId);
                                   mapOfCaseIdToRequestType.put(objTask.WhatId,'start');
                            }
                            else if(objTask.Subject.equals(System.Label.Provide_Input_For_Refund_Amount_To_Be_Booked) ||
                                    objTask.Subject.equals(System.Label.Decide_whether_to_Back_Charge_or_to_be_absorbed_by_DAMAC) ||
                                    objTask.Subject.equals(System.Label.Book_Refund_Amount_As_Per_Inputs_From_Claim) ||
                                    objTask.Subject.equals(System.Label.Pay_Customer_Disburse_Cheque_Fund_Transfer_DEWA) ||
                                    objTask.Subject.equals(System.Label.Procurement_Approval)
                                    ) {
                                    //setTaskId.add(objTask.Id);
                                    setCaseId.add(objTask.WhatId);
                                    mapOfCaseIdToRequestType.put(objTask.WhatId,'update');
                           }
               }
        }
        
        if(!setCaseId.isEmpty()) {
            System.debug('Inside if');
            pushDetailsToConnectFromSf(setCaseId,mapOfCaseIdToRequestType,'');
        }
    }
    @future(callout=true)
    public static void pushDetailsToConnectFromSf(Set<Id> setCaseId, Map<Id,String> mapOfCaseIdToRequestType, String strAppResponse) {
        List<Case> caseListToBeUpdated = new List<Case>();
        List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
        System.debug('Inside pushDetailsToConnect');
        //System.debug('setTaskId'+setTaskId);
        System.debug('setCaseId'+setCaseId);
        //Map<Id, Case> mapCase = getCaseMap(setCaseId);
        Map<Id,Account> mapOfBookingUnitIdToAccount = getBookingUnitIdToAccountMap(setCaseId);
        List<caseDataWrapper> caseDataWrapperList = new List<caseDataWrapper>();
        for(Case objCase: getCaseMap(setCaseId)) { 
            String requestType = mapOfCaseIdToRequestType.get(objCase.Id);      
            //Case objCase = mapCase.get(objTask.WhatId);
            if(objCase.Booking_Unit__r.Inventory__r.Building_Code__c != Null) {
                caseDataWrapperList.add(returnCaseDetails('BuildingCode',objCase.Booking_Unit__r.Inventory__r.Building_Code__c));
            }
            if(objCase.BIC_IBAN__c != Null) {
                caseDataWrapperList.add(returnCaseDetails('BIC_IBAN__c',objCase.BIC_IBAN__c));
            }
             if(objCase.Bank_Name__c != Null) {
                caseDataWrapperList.add(returnCaseDetails('Bank_Name__c',objCase.Bank_Name__c));
            }
             if(objCase.Bank_Code__c != Null) {
                caseDataWrapperList.add(returnCaseDetails('Bank_Code__c',objCase.Bank_Code__c));
            }
            if(objCase.DEWA_Transfered_Date__c != Null) {
                caseDataWrapperList.add(returnCaseDetails('Dewa_Acc_Transfer_Date',formatDate(String.valueOf(objCase.DEWA_Transfered_Date__c))));
            }
            if(objCase.Client_Dewa_No__c != Null) {
                caseDataWrapperList.add(returnCaseDetails('Dewa_Number',objCase.Client_Dewa_No__c));
            }
            if(objCase.Move_In_Date__c != Null) {
                caseDataWrapperList.add(returnCaseDetails('Move_In_Date',formatDate(String.valueof(objCase.Move_In_Date__c))));
            }
            if(objCase.Refund_Amount__c != Null) {
                caseDataWrapperList.add(returnCaseDetails('Refund_Amount',String.valueOf(objCase.Refund_Amount__c)));
            }
            if(objCase.Unit_Name__c != Null) {
                caseDataWrapperList.add(returnCaseDetails('Unit Name',objCase.Unit_Name__c));
            }
            if(objCase.Subject != Null) {
                caseDataWrapperList.add(returnCaseDetails('Subject',objCase.Subject));
            }
            if(objCase.Booking_Unit__c != Null && mapOfBookingUnitIdToAccount != Null && mapOfBookingUnitIdToAccount.containsKey(objCase.Booking_Unit__c) &&
            mapOfBookingUnitIdToAccount.get(objCase.Booking_Unit__c) != Null
            ) {
                caseDataWrapperList.add(returnCaseDetails('Customer_Name',mapOfBookingUnitIdToAccount.get(objCase.Booking_Unit__c).Name));
            }
        
                                                                                                                    
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('folio', objCase.CaseNumber);
            gen.writeStringField('request_type',requestType);
            if(requestType == 'update') {
                gen.writeStringField('serial_number', objCase.CaseNumber);
            }
            else {
                gen.writeStringField('serial_number','');
            }
            gen.writeStringField('app_response', strAppResponse);
            //gen.writeStartObject();     
            gen.writeFieldName('attachments');
            gen.writeStartArray();
            if(objCase != Null && objCase.SR_Attachments__r != Null && objCase.SR_Attachments__r.size() > 0) {
                for(SR_Attachments__c objSr : objCase.SR_Attachments__r) {
                gen.writeStartObject();
                gen.writeStringField('file_name', objSr.Name);
                gen.writeStringField('file_url', objSr.Attachment_URL__c);
                gen.writeEndObject();
                } 
            } 
            gen.writeEndArray();
            gen.writeFieldName('data_fields');
            gen.writeStartArray();
            if(caseDataWrapperList != Null && caseDataWrapperList.size() > 0) {
                for(caseDataWrapper objCaseWrap : caseDataWrapperList) {
                    gen.writeStartObject();
                    gen.writeStringField('field_key', objCaseWrap.fieldKey);
                    gen.writeStringField('field_value', objCaseWrap.fieldValue);
                    gen.writeEndObject();
                }
            }
            gen.writeEndArray();
            gen.writeEndObject();
            String jsonBody = gen.getAsString();
            System.debug('jsonBody>>>>'+jsonBody);
            String accessToken = getBearerToken();
            System.debug('accessToken>>>>>'+accessToken);
            Credentials_details__c creds = Credentials_details__c.getInstance('Connect Service');
            if( creds != null && String.isNotBlank(creds.Endpoint__c)) { 
                HTTP httpObj = new HTTP();
                HTTPRequest request = new HTTPRequest();
                request.setEndpoint(creds.Endpoint__c);
                request.setMethod('POST');
                request.setHeader('Api-Token',System.Label.Connect_Service_API_Token);  
                request.setHeader('Access-Token', accessToken); 
                request.setHeader('Content-Type', 'application/json'); 
                request.setBody(jsonBody);
                request.setTimeout(120000);  
                HTTPResponse resp = httpObj.send(request);
                System.debug('resp Body'+resp.getBody());
                System.debug('resp Status Code'+resp.getStatusCode());
                if(requestType == 'start') {
                    ResponseWrapper responseObj = (ResponseWrapper)System.JSON.deserialize(resp.getBody(), ResponseWrapper.class);
                    if(resp.getStatusCode() == 200 && responseObj.data.response != Null) {
                        caseListToBeUpdated.add(new Case(id= objCase.Id,Post_Id__c=responseObj.data.response));
                    }
                }
                Error_Log__c objE = GenericUtility.createErrorLog(resp.getBody(), '', 
                                                                          objCase.Booking_Unit__c, '', 
                                                                          objCase.Id);
                objE.Service_Request_Body__c = jsonBody;
                objE.Service_Response_Body__c = resp.getBody();
                lstErrorLog.add(objE);
           }
        
        }
        if(!lstErrorLog.isEmpty()){
            Insert lstErrorLog;
        }
        if(!caseListToBeUpdated.isEmpty()){
            Update caseListToBeUpdated;
        }

    }
        public static List<Task> getTasks( Set<Id> setTaskId ) {
            return new List<Task>( [ SELECT WhoId, WhatId, Type, Status, OwnerId, Id, Subject, CreatedDate, Description,
                                    Assigned_User__c, ActivityDate, Owner.Name, Document_URL__c, Booking_Unit__c,
                                    Booking_Unit__r.Registration_Id__c, POA_Right__c
                                    FROM Task
                                    WHERE Id IN : setTaskId ] );
        }
        public static List<Case> getCaseMap(Set<Id> setCaseId) {
            return new List<Case>([Select Id,
                                      CaseNumber,
                                      RecordType.DeveloperName,
                                      RecordType.Name,
                                      Booking_Unit__c,
                                      Booking_Unit__r.Inventory__r.Building_Code__c,
                                      DEWA_Transfered_Date__c,
                                      Client_Dewa_No__c,
                                      Move_In_Date__c,
                                      Refund_Amount__c,
                                      BIC_IBAN__c,
                                      Bank_Name__c,
                                      Bank_Code__c,
                                      Unit_Name__c,
                                      Post_Id__c,
                                      Subject,
                                      (Select Id, Name,Attachment_URL__c From SR_Attachments__r)
                                      From Case
                                      Where Id IN: setCaseId
                                      AND RecordType.DeveloperName = 'DEWA_Refund'
                                     ]);
        }
        
        public static Map<Id, Account> getBookingUnitIdToAccountMap(Set<Id> setCaseId) {
            Set<Id> accountIds = new Set<Id>();
            Map<Id,Account> mapOfAccountIdToAccount = new Map<Id,Account>();
            Map<Id,Account> mapOfBuIdToAccount = new Map<Id,Account>();
            
            List<Case> caseList  = [Select Id,
                                           Booking_Unit__c,
                                           Booking_Unit__r.Account_Id__c 
                                    FROM Case
                                    Where Id IN: setCaseId
                                    AND RecordType.DeveloperName = 'DEWA_Refund'
                                    ];
            for(Case objCase : caseList) {
                if(objCase.Booking_Unit__c != Null && objCase.Booking_Unit__r.Account_Id__c != Null) {
                    accountIds.add(objCase.Booking_Unit__r.Account_Id__c);
                }       
            }
            
            if(accountIds != Null && accountIds.size() > 0) {
                List<Account> accountList = [Select Id,
                                                    Name
                                            FROM Account
                                            WHERE Id IN:accountIds];
                for(Account acccObj : accountList) {
                    mapOfAccountIdToAccount.put(acccObj.Id,acccObj);            
                }
            }
            
            for(Case instanceCase : caseList) {
                if(instanceCase.Booking_Unit__c != Null && instanceCase.Booking_Unit__r.Account_Id__c != Null) {
                    if(mapOfAccountIdToAccount != Null && mapOfAccountIdToAccount.containsKey(instanceCase.Booking_Unit__r.Account_Id__c) && mapOfAccountIdToAccount.get(instanceCase.Booking_Unit__r.Account_Id__c) != Null) {
                        mapOfBuIdToAccount.put(instanceCase.Booking_Unit__c,mapOfAccountIdToAccount.get(instanceCase.Booking_Unit__r.Account_Id__c));
                    }
                }
            }   
            return mapOfBuIdToAccount;
                            
         }
        
        //Method used to convert date format from yyyy-mm-dd to dd-mmm-yyyy to SF format.
        public static  String formatDate( String strDate ){
            Map<String,String> myMap = new Map<String,String>{
            '01' => 'JAN',
            '02' => 'FEB', 
            '03' => 'MAR',
            '04' => 'APR',
            '05' => 'MAY', 
            '06' => 'JUN',
            '07' => 'JUL',
            '08' => 'AUG', 
            '09' => 'SEP',
            '10' => 'OCT',
            '11'=> 'NOV', 
            '12' => 'DEC'
            };
            
            List<String > lstStr = strDate.Split('-');
            return lstStr[2]+'-'+myMap.get(lstStr[1])+'-'+lstStr[0];
            
        }
        public static caseDataWrapper returnCaseDetails(String fieldKey, String fieldValue) {
            caseDataWrapper objCaseWrapper = new caseDataWrapper();
            objCaseWrapper.fieldKey = fieldKey;
            objCaseWrapper.fieldValue = fieldValue;
            return objCaseWrapper;
        }
        
        
        public class caseDataWrapper {
            public String fieldKey;
            public String fieldValue;
        }
        
        public class ResponseWrapper {
            public Data data;
            public String meta_data;
    
        }
         public class Data {
            public String response;
        }
        
         public class DataWrapper {
            public cls_data data;
         }

         public class cls_data {
            public String access_token;
         }

                                                                                                                    
}