/*
 * Description: Class used to create SR Attachment and send email on Key release auth Form generation through Drawloop
 */
/**********************************************************************************************
Version Date(DD-MM-YYYY)    Author              Description
-----------------------------------------------------------------------------------------------
1.1     04-03-2020          Aishwarya Todkar    1.Added Lease Handover recordtype
*************************************************************************************************/
public class ProcessKeyReleaseResponse{
    
    //@InvocableMethod 
    public static void ProcessResponse( List<Attachment> lstNewAttachment ){
        set<Id> setAttachId = new set<Id>();
        set<Id> setCaseIds = new set<Id>();
        
        if( lstNewAttachment != null && lstNewAttachment.size() > 0 ) {
            for(Attachment objAttachment : lstNewAttachment){
                system.debug('objAttachment.Name*********'+objAttachment.Name);
                system.debug('objAttachment.ParentId*********'+objAttachment.ParentId);
                if(String.valueof(objAttachment.Name).startsWith('Key Release Authorization')
                   && String.ValueOf(objAttachment.ParentId).startsWith('500')){
                    setCaseIds.add(objAttachment.ParentId);
                    setAttachId.add(objAttachment.Id);
                }
            }   
        }
        
        if( setCaseIds  != null && setCaseIds.size() > 0 ) {
            createSRAttachment( setCaseIds, setAttachId );
        }
    }

    @future(Callout=true)
    public static void createSRAttachment( set<Id> setCaseIds , set<Id> setAttachId ) {

    
        if( setCaseIds  != null && setCaseIds.size() > 0 ) {
            
        List<Case> lstCase = [select Id, AccountId,Account.Email__pc,Account.IsPersonAccount,Account.Email__c
                        , Booking_Unit__c,Snags_Reported__c,Handover_Status__c
                        , RecordTypeId,Account.party_ID__C,Early_Handover_Status__c
                        , Booking_Unit__r.Unit_Name__c
                        , Status 
                        , Booking_Unit__r.Total_due_Overdue__c
                        , Booking_Unit__r.Amount_Paid__c
                        , Booking_Unit__r.Requested_Price__c
                        , Booking_Unit__r.CurrencyIsoCode
                        , Booking_Unit__r.Registration_Status__c
                        , Booking_Unit__r.Property_City__c
                        , CaseNumber
                        , Parent.CaseNumber
                        , ParentId
                        , Client_Dewa_No__c 
                        , Booking_Unit__r.Registration_ID__c 
                        , RecordType.DeveloperName
                        , RecordType.Name
                        , Document_Verified__c
                        , Parent.Document_Verified__c
                        , Payment_Verified__c
                        , Total_Major_Snags__c
                        , Total_Snags_Closed__c
                        , Total_Snags_Open__c
                        from Case 
                        where id IN:setCaseIds
                        AND ( RecordType.DeveloperName = 'Handover' 
                            OR RecordType.DeveloperName = 'Early_Handover' 
                            OR RecordType.DeveloperName= 'Lease_Handover')
                        AND ( Status != 'Closed' 
                            OR Status != 'Rejeted' 
                            OR Status != 'Cancelled' ) 
                        LIMIT 1];      
            
            List<Attachment> lstAttachments = [SELECT Id,Body,Name,ParentId FROM Attachment WHERE ParentId IN: setCaseIds AND Id IN: setAttachId AND Name = 'Key Release Authorization Form.pdf' ORDER BY CreatedDate DESC LIMIT 1];
            system.debug('lstAttachments : ' + lstAttachments);                            
            if( lstAttachments != null && lstAttachments.size() > 0 ) {
                
                List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest = new  List<UploadMultipleDocController.MultipleDocRequest>();
                List<SR_Attachments__c> lstSRAttachment = new List<SR_Attachments__c>();
                integer intIncrementor = 0;
                List<SR_Attachments__c> lstSRAttachmentToDelete = new List<SR_Attachments__c>();                

                
                system.debug('lstCase : '+lstCase);
                if( lstCase != null && lstCase.size() > 0 ) {
                    Id keyId = fetchDocument('Key Release Form' , lstCase[0]);
                    Id signedKeyId = fetchDocument('Signed Key Release Form',lstCase[0]);
                    for(  Case objCase : lstCase ) {
                        if( objCase.Booking_Unit__r.Unit_Name__c != '' ) {
                            if( lstAttachments[0].Body != null 
                              && String.isNotBlank( lstAttachments[0].Name )) {
                                UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
                                objDocRequest.category =  'Document';
                                objDocRequest.entityName = 'Damac Service Requests'; 
                                objDocRequest.fileDescription  =  'Key Release Form';


                                
                                intIncrementor++;
                                objDocRequest.fileId = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+lstAttachments[0].Name;
                                objDocRequest.fileName = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+lstAttachments[0].Name;
                                
                                System.debug('---System.currentTimeMillis()--'+System.currentTimeMillis());
                                System.debug('---intIncrementor--'+intIncrementor);
                                System.debug('---objDocRequest.fileName--'+objDocRequest.fileName);
                                
                                if(objCase != null){
                                    objDocRequest.registrationId =  objCase.CaseNumber;
                                }
                                objDocRequest.sourceFileName  = 'IPMS-'+objCase.Account.party_ID__C+'-'+lstAttachments[0].Name;
                                objDocRequest.sourceId  =  'IPMS-'+objCase.Account.party_ID__C+'-'+lstAttachments[0].Name;                        
                                if( lstAttachments[0].Body != null ){
                                    blob objBlob = lstAttachments[0].Body;
                                    if(objBlob != null){
                                        objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
                                    }
                                }
                                //objDocRequest.base64Binary  =  crfAttachmentBody;
                                lstMultipleDocRequest.add(objDocRequest);
                            }
                                                        
                            //lstCaseToUpdate.add(objCase);
                            
                            SR_Attachments__c objAttach = new SR_Attachments__c();
                            objAttach.Case__c  = objCase.Id;
                            objAttach.Booking_Unit__c  = objCase.Booking_Unit__c;
                            objAttach.isValid__c = true;
                            objAttach.IsRequired__c = true;
                            objAttach.Name = objCase.CaseNumber+' Key Release Form ' +system.now();
                            objAttach.Id = keyId;
                            objAttach.Need_Correction__c =  false;
                                                
                            
                            lstSRAttachment.add(objAttach);     
                        }
                    }
                    system.debug('lstMultipleDocRequest : ' +lstMultipleDocRequest );
                    if( lstMultipleDocRequest.Size() > 0 ) {
                        UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();
                        if( !Test.isRunningTest() ) { 
                            MultipleDocData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocRequest);
                        }else {
                            
                            List<UploadMultipleDocController.MultipleDocResponse> data = new List<UploadMultipleDocController.MultipleDocResponse>();
                            UploadMultipleDocController.MultipleDocResponse objMultipleDocResponse = new UploadMultipleDocController.MultipleDocResponse();
                            objMultipleDocResponse.PROC_STATUS = 'S';
                            objMultipleDocResponse.url = 'www.google.com';
                            data.add(objMultipleDocResponse);
                            
                            MultipleDocData.Data = data;
                        }
                        System.debug('--MultipleDocData--'+MultipleDocData);
                        if( MultipleDocData != null && MultipleDocData.status == 'Exception' ) {
                            //for( Case objCase : lstCase) {
                                errorLogger(MultipleDocData.message,lstCase[0].Id,lstCase[0].Booking_Unit__c);
                            //}
                        }                                   
                        if(MultipleDocData != null && (MultipleDocData.Data == null || MultipleDocData.Data.Size() == 0) ) {
                            //for( Case objCase : lstCase) {
                                errorLogger(MultipleDocData.message,lstCase[0].Id,lstCase[0].Booking_Unit__c);
                            //}                           
                        }

                        if( MultipleDocData != null ) {
                            system.debug(' lstSRAttachment : ' + lstSRAttachment);
                            for( Integer objInt = 0; objInt < lstSRAttachment.Size(); objInt++ ) {
                                UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[objInt];
                                if( objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url) ) {
                                    lstSRAttachment[objInt].Attachment_URL__c = objData.url;
                                    lstCase[0].Additional_Doc_File_URL__c = objData.url;
                                    lstCase[0].Account_Email__c = lstCase[0].Account.IsPersonAccount ? lstCase[0].Account.Email__pc : lstCase[0].Account.Email__c;
                                    
                                }
                            }
                            system.debug('lstSRAttachment : ' +lstSRAttachment );
                            if( lstSRAttachment.Size() > 0 ){
                                
                                SR_Attachments__c objCaseAttachment1 = new SR_Attachments__c();
                                objCaseAttachment1.Case__c = lstCase[0].id;
                                objCaseAttachment1.Name = lstCase[0].CaseNumber+' Signed Key Release Form ' +system.now();
                                objCaseAttachment1.Booking_Unit__c = lstCase[0].Booking_Unit__c;
                                objCaseAttachment1.Id = signedKeyId;
                                lstSRAttachment.add(objCaseAttachment1);                                
                                
                                upsert lstSRAttachment;
                                //sendEmailOnDocumentGeneration(lstBookingUnitIds );
                                
                                list<String> lstRegIds = new list<String>();
                                system.debug('*****REGID*****'+lstCase[0].Booking_Unit__r.Registration_ID__c);
                                lstRegIds.add(lstCase[0].Booking_Unit__r.Registration_ID__c);
                                
                                if ( !Test.isRunningTest()) {
                                    UpdateHoEhoFlagInIPMS.UpdateFlag(lstCase,lstRegIds);
                                }
                                /*if( lstCase[0].Recordtype.DeveloperName == 'Handover' ) {
                                    CallEarlyHandoverMQService1.CallEarlyHandoverMQService1Name('Y','N',lstRegIds);
                                }else if( lstCase[0].Recordtype.DeveloperName == 'Early_Handover'){
                                    CallEarlyHandoverMQService1.CallEarlyHandoverMQService1Name('N','Y',lstRegIds);
                                }*/
                                
                                Booking_Unit__c objBU = new Booking_Unit__c();
                                objBU.Id = lstCase[0].Booking_Unit__c;
                                if( lstCase[0].Recordtype.DeveloperName == 'Handover' ) {
                                    objBU.Handover_Flag__c = 'Y';
                                    objBU.Handover_Date__c = date.today();
                                }else if( lstCase[0].Recordtype.DeveloperName == 'Early_Handover'){
                                    objBU.Early_Handover__c = true;
                                    objBU.Early_Handover_Date__c = date.today();
                                }
                                objBU.Okay_to_release_keys__c = true;                               

                                list<Case> lstCase1 = new list<Case>();
                                for (Case objCase1: lstCase) {
                                    if (objCase1.RecordType.DeveloperName == 'Handover') {
                                        objCase1.Handover_Status__c = 'Key Release Form Generated';
                                    }else if (objCase1.RecordType.DeveloperName == 'Early_Handover') {
                                        objCase1.Early_Handover_Status__c = 'Key Release Form Generated';
                                    }
                                    lstCase1.add(objCase1);
                                }
                                update lstCase1;
                                update objBU;

                                 list<Task> lstTask = new list<Task>();
                                 for (Task objTask: [Select Id, Status, Subject, Process_Name__c 
                                                     From Task 
                                                     Where Status != 'Completed'
                                                     And Process_Name__c  = 'Early Handover'
                                                     And Subject = 'Verify HO checklist and Generate Key Release form'
                                                     And WhatId IN: setCaseIds]) {
                                    objTask.Status = 'Completed';
                                    lstTask.add(objTask);                                
                                }
                                if (lstTask != null && lstTask.size() > 0) {
                                    update lstTask;
                                }                                   
                                
                            }  
                        }
                    }
                    

                    /*if( lstCaseToUpdate != null && lstCaseToUpdate.size() > 0 ) {
                        update lstCaseToUpdate;
                    }*/
                    if( lstCase != null && lstCase.size() > 0 ) {
                        update lstCase;
                    }
                }
            }
        }
    }
    
    public static Id fetchDocument(String docName,Case objCase){
        docName= '%'+docName+'%';
        list<SR_Attachments__c> lstSR = [Select Id, Name, Case__c 
                                        from SR_Attachments__c 
                                        where Case__c =:objCase.Id
                                        and Name Like :docName limit 1];
        if(lstSR != null && !lstSR.isEmpty()){
            return lstSR[0].Id;
        }else{
            return null;
        }
    }
    
    private static void errorLogger(string strErrorMessage, string strCaseID,string strBookingUnitID){
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        if(String.isNotBlank(strCaseID) && strCaseID.startsWith('500')){
            objError.Case__c = strCaseID;
        }
        if(String.isNotBlank(strBookingUnitID)){
            objError.Booking_unit__c = strBookingUnitID;
        }
        insert objError;
    }
    
}