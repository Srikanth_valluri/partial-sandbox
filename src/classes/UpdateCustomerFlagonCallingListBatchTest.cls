/*
* Description - Test class developed for UpdateCustomerFlagonCallingListBatch 
*
*/
@isTest
private class UpdateCustomerFlagonCallingListBatchTest {

    static Id fmCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
    static Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
    static Id hoCallingList = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Handover Calling List').RecordTypeId;
    static Id ehoCallingList = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Early Handover Calling List').RecordTypeId;
    static Id eliteCallingList = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Elite Calling List').RecordTypeId;

    static testMethod void test_UpdateCustomerFlagonCallingListBatch1 () {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
          
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForCollectionCL',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForFMCL',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForHOCL',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForEHOCL',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForEliteCL',OnOffCheck__c = true));
        
        insert settingLst2;
        
        UpdateCustomerFlagonCallingListBatch batchCls = new UpdateCustomerFlagonCallingListBatch ();
        
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        

        
        List<Calling_List__c> lstCallingLists = createCallingList( collectioncallingRecordTypeId , 5 ,objAcc.Id );
        insert lstCallingLists;
        
        Test.startTest();
            Database.executeBatch(batchCls);
        Test.stopTest();
    }
    
    static testMethod void test_UpdateCustomerFlagonCallingListBatch2 () {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
          
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForCollectionCL',OnOffCheck__c = false));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForFMCL',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForHOCL',OnOffCheck__c = false));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForEHOCL',OnOffCheck__c = false));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForEliteCL',OnOffCheck__c = false));
        
        insert settingLst2;
        
        UpdateCustomerFlagonCallingListBatch batchCls = new UpdateCustomerFlagonCallingListBatch ();
        
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        

        
        List<Calling_List__c> lstCallingLists = createCallingList( fmCallingRecordTypeId , 5 ,objAcc.Id );
        insert lstCallingLists;
        
        Test.startTest();
            Database.executeBatch(batchCls);
        Test.stopTest();
    }

    static testMethod void test_UpdateCustomerFlagonCallingListBatch3 () {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
          
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForCollectionCL',OnOffCheck__c = false));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForFMCL',OnOffCheck__c = false));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForHOCL',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForEHOCL',OnOffCheck__c = false));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForEliteCL',OnOffCheck__c = false));
        
        insert settingLst2;
        
        UpdateCustomerFlagonCallingListBatch batchCls = new UpdateCustomerFlagonCallingListBatch ();
        
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        

        
        List<Calling_List__c> lstCallingLists = createCallingList( hoCallingList , 5 ,objAcc.Id );
        insert lstCallingLists;
        
        Test.startTest();
            Database.executeBatch(batchCls);
        Test.stopTest();
    }
    
    static testMethod void test_UpdateCustomerFlagonCallingListBatch4 () {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
          
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForCollectionCL',OnOffCheck__c = false));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForFMCL',OnOffCheck__c = false));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForHOCL',OnOffCheck__c = false));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForEHOCL',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForEliteCL',OnOffCheck__c = false));
        
        insert settingLst2;
        
        UpdateCustomerFlagonCallingListBatch batchCls = new UpdateCustomerFlagonCallingListBatch ();
        
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        

        
        List<Calling_List__c> lstCallingLists = createCallingList( ehoCallingList , 5 ,objAcc.Id );
        insert lstCallingLists;
        
        Test.startTest();
            Database.executeBatch(batchCls);
        Test.stopTest();
    }
    
    static testMethod void test_UpdateCustomerFlagonCallingListBatch5() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
          
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForCollectionCL',OnOffCheck__c = false));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForFMCL',OnOffCheck__c = false));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForHOCL',OnOffCheck__c = false));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForEHOCL',OnOffCheck__c = false));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForEliteCL',OnOffCheck__c = true));
        
        insert settingLst2;
        
        UpdateCustomerFlagonCallingListBatch batchCls = new UpdateCustomerFlagonCallingListBatch ();
        
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        

        
        List<Calling_List__c> lstCallingLists = createCallingList( eliteCallingList , 5 ,objAcc.Id );
        insert lstCallingLists;
        
        Test.startTest();
            Database.executeBatch(batchCls);
        Test.stopTest();
    }
    
    /*
     @ Description : To create calling list reocord
     @ Return      : list of calling list to be created
    */
    public static List<Calling_List__c> createCallingList( Id RecordTypeIdCollection, Integer counter , Id acctId ) {

        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        for( Integer i=0; i<counter; i++ ) {
            lstCallingLists.add(new Calling_List__c(IsHideFromUI__c = false, Account__c = acctId  , Customer_Flag__c  = false ,RecordTypeId = RecordTypeIdCollection ) );
        }
        return lstCallingLists;
    }
}