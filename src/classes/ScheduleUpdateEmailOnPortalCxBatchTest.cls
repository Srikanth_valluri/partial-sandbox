/*
* Description - Test class developed for ScheduleUpdateEmailOnPortalCxBatch
*
* Version            Date            Author            Description
* 1.0              12/07/2017        Arjun Khatri      Initial Draft
*/
@isTest
private class ScheduleUpdateEmailOnPortalCxBatchTest {
    static testMethod void testExecute() {
        Test.startTest();
            ScheduleUpdateEmailOnPortalCxBatch objScheduler = new ScheduleUpdateEmailOnPortalCxBatch();
            String schCron = '0 0 0 * * ? *';
            String jobId = System.Schedule('ScheduleUpdateEmailOnPortalCxBatch Reccord Updated',schCron,objScheduler);
        Test.stopTest();
        CronTrigger cronTrigger = [
            SELECT Id
                 , CronExpression
                 , TimesTriggered
                 , NextFireTime
            FROM CronTrigger
           WHERE id = :jobId
        ];
        System.assertEquals( schCron,  cronTrigger.CronExpression  , 'Cron expression should match' );
        System.assertEquals( 0, cronTrigger.TimesTriggered  , 'Time to Triggered batch should be 0' );
        List<AsyncapexJob> apexJobs = [
            SELECT Id, ApexClassID ,JobType , Status 
            FROM AsyncapexJob
            WHERE JobType = 'BatchApex'];
        System.assertEquals( false , apexJobs.isEmpty()  , 'Expected at least one Async apex Job' );
        System.assertEquals( 'BatchApex', apexJobs[0].JobType  , 'Job Type should match ' );
    }
}