/****************************************************************************************************
Name: AgentPortalController                                                                         *
Developer name: Pratiksha Narvekar                                                                  *
Created Date : 6-08-2017                                                                            *
----------------------------------------------------------------------------------------------------*
*   Ver     Name            Date        Description                                                 *
*   V1.1    Craig           28-03-2018  Updated the Announcement query                              *
****************************************************************************************************/

public class AgentPortalAnnounsmentController {
 
    private integer totalRecs = 0;     
    private integer index = 0;
    private integer blockSize = 3; 
    public  static Integer AnnounsmentCount{set;get;}
    public static Decimal NotificationCount{set;get;}
    public Static Decimal EventsCount{set;get;}
    public static String UserTier{set;get;}
    public String UserName{get;set;}
    public String UserProfile{get;set;}
    public DateTime UserCreatedDate{get;set;}
    public List<Agent_Commission__c> agentCommissionList{set;get;}
    public Decimal totalUnitPrice{set;get;}
    public Decimal totalCommissionAmount{set;get;}
    public Integer fieldSetSize{set;get;}
    public User uRec{get; set;}
    public Account acc{get; set;}
    public Contact con{get; set;}
    public boolean showheader{get; set;}     
    public Notification__c notification{set;get;}
    public List<Notification__c> notificationList{set;get;}
    public Integer unreadNotificationCount{set;get;}
    public boolean showNotificationDetail{set;get;}
    public static decimal totalnotification{get;set;}
    @TestVisible private Contact contactInfo;
    @TestVisible private Id accountId{get;set;}
    
    //Constructor
    public AgentPortalAnnounsmentController(){
       UserName = userinfo.getName();
         totalRecs = [select count() from Announcement__c]; 
         AnnounsmentCount = totalRecs;
             
    showNotificationDetail = false;

    Contact contactInfo = AgentPortalUtilityQueryManager.getContactInformation();
    notificationList = new List<Notification__c>();
    if(null != contactInfo && (contactInfo.Portal_Administrator__c ||
    contactInfo.Authorised_Signatory__c)){
      String condition1 = '(Contact__r.id =\''+contactInfo.Id+'\' OR Account__r.id =\''+contactInfo.accountID+
                '\') AND Active__c  =true '+
                                ' ORDER BY CreatedDate DESC';
            system.debug(condition1);
      notificationList = AgentPortalUtilityQueryManager.getNotifications(condition1);
    }
    else if(null != contactInfo){
      String condition = 'Contact__r.id =\''+contactInfo.Id+'\' AND Active__c  =true'+ 
                                ' ORDER BY CreatedDate DESC';
            system.debug(condition);
      notificationList = AgentPortalUtilityQueryManager.getNotifications(condition);
    }
    
    system.debug('***Notification List'+notificationList);
    totalnotification = notificationList.size();
    NotificationCount = totalnotification;
    unreadNotificationCount = AgentPortalUtilityQueryManager.unreadNotificationCount;
    }
    /*..New Announcement..*/
       
  public List<Notification__c> getNotification(){
     AgentPortalNotificationController obj = new AgentPortalNotificationController();
     List<Notification__c> ObjNotification = obj.getNotification();
     return ObjNotification ;     
  }

  public List<Announcement__c> getAnnouncementRequestDetail(){
        List<Announcement__c> listAnnounsment = new List<Announcement__c>();
        listAnnounsment =[select ID,
                                 Name,
                                 Active__c,
                                 Agency_Tier__c,
                                 Agency_Type__c,
                                 Are_Agencies_Notified__c,
                                 Description__c,
                                 Start_Date__c,
                                 End_Date__c,
                                 Createdby.Name,
                                 CreatedDate,
                                 Title__c 
                         from Announcement__c
                         WHERE Active__c = true
                           AND End_Date__c >= :System.Today()
                         ORDER BY CreatedDate DESC
                         NULLS LAST LIMIT 5000];
       System.debug('...listAnnounsment...'+listAnnounsment);
       return listAnnounsment;
  }
        public void beginning()
    {
        index = 0;
    }
    
    public void previous()
    {
        index = index - blockSize;
    }
    
    public void next()
    {
        index = index + blockSize;
    }

    public void end()
    {
        index = totalrecs - math.mod(totalRecs,blockSize);
    }        
    
    public boolean getprev()
    {
        if(index == 0)
        return true;
        else
        return false;
    }  
    
    public boolean getnxt()
    {
        if((index + blockSize) > totalRecs)
        return true;
        else
        return false;
    } 
        public PageReference redirect()
    {
    PageReference pr = new PageReference('/apex/AgentPortalSingleAnnouncements');
    pr.getParameters().put('key','value');
    pr.setRedirect(true); 
    return pr;
    }
      public void viewNotificationDetail(){
    
    system.debug(ApexPages.currentPage().getParameters().containsKey('nid'));
    if(ApexPages.currentPage().getParameters().containsKey('nid') &&
      null != ApexPages.currentPage().getParameters().get('nid')){
      showNotificationDetail = true;
      readNotification(ApexPages.currentPage().getParameters().get('nid'));
    }
  }
  /**************************************************************************************************
    Method:      viewCurrentNotificationDetail
    Description:    page action to get the notification id from url and get the description 
**************************************************************************************************/
  public void viewCurrentNotificationDetail(){
    
    system.debug(ApexPages.currentPage().getParameters().containsKey('notificationId'));
    if(ApexPages.currentPage().getParameters().containsKey('notificationId') &&
      null != ApexPages.currentPage().getParameters().get('notificationId')){
      readNotification(ApexPages.currentPage().getParameters().get('notificationId'));
    }
  }

/**************************************************************************************************
    Method:      readNotification
    Description:    get the notification title and description and mark it as read
**************************************************************************************************/
  @TestVisible
  private void readNotification(String notificationId){
    notification = new Notification__c();
    if(null != notificationId)
      notification = AgentPortalUtilityQueryManager.getNotificationDescription(notificationId);

    if(null != notification && notification.Read__c == false){
      AgentPortalUtilityQueryManager.markNotificationAsRead(notificationId);
    }
  }
}