@isTest
public class Damac_CheckRMController_TEST {
    public static testMethod void  method1 () {
     User u = [SELECT ID, Extension,Profile.Name FROM User where ID =: USERInfo.getUserID ()  ];
      
            u.Extension = '0715';
            u.Profile.Name = 'Property Consultant';
            u.Languages_Known__c = 'Hindi';
            u.Sales_Office__c  = 'RIYADH';
            u.New_Team__c = 'Direct Team';
            update u;
         
    Id inquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        Inquiry__c inq = New Inquiry__c ();
            inq.First_Name__c = 'testInq';
            inq.Preferred_Language__c = 'English';
            inq.Last_Name__c ='inqLast';
            inq.Inquiry_Source__c = 'Prospecting';
            inq.Primary_Contacts__c = 'Mobile Phone';
            inq.Mobile_CountryCode__c = 'India: 0091';
            inq.Mobile_Phone__c = '1236547890';
            inq.Mobile_Phone_Encrypt__c = '1223467886';
            inq.Email__c = 'test@gmail.com';
            inq.recordTypeId = inquiryRecordTypeId;
            inq.Sales_Office__c = 'RIYADH';
            
        insert inq;
         Inquiry_User_Assignment_Rules__c rule = New Inquiry_User_Assignment_Rules__c ();
            rule.User_Profile__c = 'Property Consultant';
             rule.setupOwnerId = u.id;
            
        insert rule;
        Office_Meeting__c  officeMeeting = New Office_Meeting__c ();
            officeMeeting.Inquiry__c = inq.id;
            officeMeeting.Bypass_Validation__c = true;
            officeMeeting.Outcome__c  = 'show';
            officeMeeting.Recommended_RM__c = u.id;
            officeMeeting.Comments__c = 'testData';
        try {
        insert officeMeeting;
        } catch(Exception e) {}
        
        Campaign__c camp = new Campaign__c ();
        camp.Start_Date__c = system.today().adddays(-3);
        camp.End_Date__c =  system.today().adddays(3);
        camp.Marketing_Start_Date__c =  system.today().adddays(-3);
        camp.Marketing_End_Date__c =  system.today().adddays(3);
        camp.PCAssignment__c = true;
       // camp.Active__c = true;
        insert camp;
        
        Inquiry_Assignment_Algorithm__c inqAssign = New Inquiry_Assignment_Algorithm__c ();
            inqAssign.Name = 'test';
            inqAssign.RM_User_ID__c  = u.id;
            inqAssign.RM_Assigned__c = true;
            
        insert inqAssign;
        
        ApexPages.currentPage().getParameters().put('id',officeMeeting.Id);
        Damac_CheckRMController obj = New Damac_CheckRMController();
        obj.checkRMAssignment ();
        
        id userid =  userinfo.getuserid();
        obj.checkUser([Select name from user where id=:userid]);
        obj.checkAlreadyAssignedRMs ();
        obj.checkForRM (officeMeeting);
        obj.checkForCampaignRM (officeMeeting, camp.id);
        obj.cancel ();

        Damac_CheckRMController.rmInnerClass obj1 = new Damac_CheckRMController.rmInnerClass ();
        obj.meeting = officeMeeting;
        obj1.selected = true;
        obj1.u = u;
        obj1.rank = 1;
        obj.rmsList.add(obj1);
        
        obj.updateRecommendedRM ();
        
        try {
            OfficeMeetingNotificationController obj2 = new OfficeMeetingNotificationController ();
            obj2.officeMeetngId = officeMeeting.id;
            obj2.getMeeting ();
        } catch (Exception e) {}
    }
}