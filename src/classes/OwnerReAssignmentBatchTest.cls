@isTest
public class OwnerReAssignmentBatchTest {
    @isTest
    static void testBath(){
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Email__c = 'test@gmail.com';
   
        TriggerOnOffCustomSetting__c setting = new TriggerOnOffCustomSetting__c();
        setting.Name = 'CallingListTrigger';
        setting.OnOffCheck__c = true;
        insert setting;
        
        Profile objProfile = [
            SELECT Id 
            FROM Profile 
            WHERE Name='Standard User'
        ];
        User objUser = new User();
        objUser.alias = 'test';
        objUser.username = 'test1234@testorg.com';
        objUser.email = 'test1234@testorg.com';
        objUser.firstName = 'test1';
        objUser.lastName = 'test2';
        objUser.localeSidKey = 'cs';
        objUser.languageLocaleKey = 'en_US';
        objUser.emailEncodingKey = 'Big5';
        objUser.timeZoneSidKey = 'Africa/Cairo';
        objUser.currencyIsoCode = '';
        objUser.profileId = objProfile.Id;
        insert objUser;  
        
        System.runAs( objUser ){
            insert objCallingList;
        }
        Set<String> stringSet = new Set<String>();
        stringSet.add(objCallingList.Id);
        
        Test.startTest();
            Database.executeBatch(new OwnerReAssignmentBatch(stringSet, UserInfo.getUserID(), 'Calling_List__c'));
        Test.stopTest();
    }
}