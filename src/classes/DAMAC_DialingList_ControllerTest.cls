@isTest 
public class DAMAC_DialingList_ControllerTest
{
    static Inquiry_User_Assignment_Rules__c rule = new Inquiry_User_Assignment_Rules__c ();
    static Inquiry__c inq = New Inquiry__c ();
    static Dialing_List__c dial = new Dialing_List__c ();
    static Inquiry_Country_Code_Categories__c categories = new Inquiry_Country_Code_Categories__c ();
    static void init () {
        categories = new Inquiry_Country_Code_Categories__c ();
        categories.Category__c = 'INTERNATIONAL';
        categories.Name = 'India: 0091';
        categories.Short_Code__c = '0091';
        categories.UTC__c = '+';
        categories.UTC_Hrs_Difference__c = '05:30';
        insert categories;
        
        rule = new Inquiry_User_Assignment_Rules__c ();
        rule.Available_for_Dailing__c = TRUE;
        rule.SetupOwnerId = UserInfo.getUserID ();
        insert rule;
        
        inq = New Inquiry__c ();
        inq.First_Name__c = 'testInq';
        inq.Preferred_Language__c = 'English';
        inq.Last_Name__c ='inqLast';
        inq.Inquiry_Source__c = 'Prospecting';
        inq.Primary_Contacts__c = 'Mobile Phone';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.Mobile_Phone__c = '1236547890';
        inq.Mobile_Phone_Encrypt__c = '1223467886';
        inq.Email__c = 'test@gmail.com';
        inq.Call_id__c = '123';
        insert inq;
        
        Task t = new Task ();
        t.Subject='Donni';
        t.Status='New';
        t.SQL_ID__C = '123';
        t.Priority='Normal';
        t.CallType='Outbound';
        insert t;
        
        dial = new Dialing_List__c ();
        dial.Name = inq.Name;
        dial.dial_date__c = Date.TODAY();
        
        dial.Inquiry__c = inq.id;
        dial.Priority__c = 1;
        insert dial;
    }
    static testMethod void testMethod1() {
        Test.startTest ();
        init ();
        
        DAMAC_DialingList_Controller obj = new DAMAC_DialingList_Controller ();
        obj.init ();
        DAMAC_DialingList_Controller.checkDetails ('123', '123', '7008', inq.id, 'Outbound', 'Dialing', '123', dial.id,'Dialer Event');
        DAMAC_DialingList_Controller.unlockDialingCallId ('123', 'Marked for Retry');
        DAMAC_DialingList_Controller.unlockDialingCallId ('123', 'Dialing');
        DAMAC_DialingList_Controller.unlockDialing (dial.id, 'Marked for Retry');
        DAMAC_DialingList_Controller.unlockDialing (dial.id, 'Dialing');
        obj.isUseronline = true;
        obj.updateAvailiableStatus ();
        obj.isUseronline = false;
        obj.updateAvailiableStatus ();
        obj.reset();
        obj.totalDialingListfortoday();
        DAMAC_DialingList_Controller.upsertDialerLog  (true, 'Away');
        apexpages.currentpage().getParameters().put ('inqId', inq.ID);
        obj.onLoadInquiryDetailsForListenerPage ();
        test.stopTest ();
        
    }
    static testMethod void testMethod2() {
        test.startTest ();
        init ();
        
        DAMAC_DialingList_Controller obj = new DAMAC_DialingList_Controller ();
        obj.init ();
        obj.inq = inq;
        DAMAC_DialingList_Controller.checkDetails ('123', '123', '7008', '', 'Outbound', 'Dialing', '123', dial.id,'Dialer Event');
        DAMAC_DialingList_Controller.unlockDialingCallId ('123', 'Marked for Retry');
        DAMAC_DialingList_Controller.unlockDialingCallId ('123', 'Dialing');
        DAMAC_DialingList_Controller.unlockDialing (dial.id, 'Marked for Retry');
        DAMAC_DialingList_Controller.unlockDialing (dial.id, 'Dialing');
        obj.isUseronline = true;
        obj.updateAvailiableStatus ();
        obj.isUseronline = false;
        obj.updateAvailiableStatus ();
        obj.reset();
        obj.totalDialingListfortoday();
        DAMAC_DialingList_Controller.upsertDialerLog  (true, 'Away');
        apexpages.currentpage().getParameters().put ('inqId', inq.ID);
        obj.onLoadInquiryDetailsForListenerPage ();
        ApexPages.StandardController controller = new ApexPages.StandardController (inq);
        DAMAC_DialingList_Controller objStadard = new DAMAC_DialingList_Controller (controller);
        test.stopTest ();
    }
}