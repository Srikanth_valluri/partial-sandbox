@isTest
public class InventoriesEOIController_TC {

    @isTest static void addPCsToEOITest(){
        Campaign__c camp = new Campaign__c();
        camp.Marketing_Start_Date__c = System.today().adddays(-10);
        camp.Start_Date__c = System.today().adddays(-10);        
        camp.End_Date__c =  System.today().adddays(10);
        camp.Marketing_End_Date__c =  System.today().adddays(10);
        camp.Campaign_Name__c = 'T3 - c';   
        camp.Country__c = 'United Arab Emirates';   
        camp.City__c = 'Abu Dhabi';
        camp.Campaign_Type_New__c = 'Stands'; 
        camp.Campaign_Category_New__c = 'Mall';
        insert camp;
        
        ID recordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry Decrypted').getRecordTypeId();      
        Inquiry__c inq = new Inquiry__c ();
        inq.Mobile_phone__c = '98765456789';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYU';
        inq.Class__c = 'test';
        inq.Campaign__c = camp.Id;
        inq.Inquiry_Status__c = 'Active';
        inq.Inquiry_Source__c ='Agent Referral';  
        inq.RecordtypeId = recordTypeId;
        inq.Class__c = 'test';
        inq.Phone_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Nationality__c = 'UAE';
        insert inq;
        
        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c  = 1;
        newProperty.Property_Code__c    = 'VIR' ;
        newProperty.Property_Name__c    = 'VIRIDIS @ AKOYA OXYGEN' ;
        newProperty.District__c = 'AL YUFRAH 2' ;
        newProperty.AR_Transaction_Type__c  = 'INV VIR' ;
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR' ;
        newProperty.Brokerage_Distribution_Set__c   = '11600' ;
        newProperty.Sales_Commission_Dist_Set__c    = '11601' ;
        newProperty.Currency_Of_Sale__c = 'AED' ;
        newProperty.Signature_Col_Customer_Stmt__c  = 'Front Line Investment Management Co. LLC' ;
        newProperty.EOI_Enabled__c = true;
        insert newProperty;
        
        Inventory__c invent = new Inventory__c();
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1' ;
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.ID;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Apartments_In_Inventory';
        insert invent; 
        
        EOI_Process__c eoi = new EOI_Process__c ();
        eoi.No_of_Units__c = 'Single Unit – Token AED 40,000';
        eoi.Mode_of_Token_Payment__c = 'Cash';
        eoi.Inquiry__c = inq.id;
        eoi.Receipt_number__c = '12345';
        EOI.Property__c = newProperty.id;
        
        insert eoi;
        
        Attachment att = new Attachment ();
        att.name = 'test.doc';
        att.ContentType = 'application/msword';
        att.Body = Blob.valueOf ('111');
        att.ParentId = eoi.Id;
        insert att;
        
        
        
        ApexPages.StandardController controller = new ApexPages.StandardController (eoi);
        InventoriesEOIController obj = new InventoriesEOIController (controller);
        InventoriesEOIController.InventoryWrapper obj1= new InventoriesEOIController.InventoryWrapper (invent.Id, invent, true);
        obj.inventoryWrapperList.add (obj1);
        obj.AddOrRemoveInventories ();
        Set <ID> invId = new Set <ID> ();
        invId.add (invent.ID);
        
        InventoriesEOIController.sendToIPMS (eoi.id, invId, true);
        eoi.Status__c = 'Submitted';
        update eoi;
        
    }
    
    @isTest static void removePCsToEOITest(){
        Campaign__c camp = new Campaign__c();
        camp.Marketing_Start_Date__c = System.today().adddays(-10);
        camp.Start_Date__c = System.today().adddays(-10);        
        camp.End_Date__c =  System.today().adddays(10);
        camp.Marketing_End_Date__c =  System.today().adddays(10);
        camp.Campaign_Name__c = 'T3 - c';   
        camp.Country__c = 'United Arab Emirates';   
        camp.City__c = 'Abu Dhabi';
        camp.Campaign_Type_New__c = 'Stands'; 
        camp.Campaign_Category_New__c = 'Mall';
        insert camp;
        
        ID recordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry Decrypted').getRecordTypeId();      
        Inquiry__c inq = new Inquiry__c ();
        inq.Mobile_phone__c = '98765456789';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYU';
        inq.Class__c = 'test';
        inq.Campaign__c = camp.Id;
        inq.Inquiry_Status__c = 'Active';
        inq.Inquiry_Source__c ='Agent Referral';  
        inq.RecordtypeId = recordTypeId;
        inq.Class__c = 'test';
        inq.Phone_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Nationality__c = 'UAE';
        insert inq;
        
        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c  = 1;
        newProperty.Property_Code__c    = 'VIR' ;
        newProperty.Property_Name__c    = 'VIRIDIS @ AKOYA OXYGEN' ;
        newProperty.District__c = 'AL YUFRAH 2' ;
        newProperty.AR_Transaction_Type__c  = 'INV VIR' ;
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR' ;
        newProperty.Brokerage_Distribution_Set__c   = '11600' ;
        newProperty.Sales_Commission_Dist_Set__c    = '11601' ;
        newProperty.Currency_Of_Sale__c = 'AED' ;
        newProperty.Signature_Col_Customer_Stmt__c  = 'Front Line Investment Management Co. LLC' ;
        newProperty.EOI_Enabled__c = true;
        insert newProperty;
        
        EOI_Process__c eoi = new EOI_Process__c ();
        eoi.No_of_Units__c = 'Single Unit – Token AED 40,000';
        eoi.Mode_of_Token_Payment__c = 'Cash';
        eoi.Inquiry__c = inq.id;
        EOI.Property__c = newProperty.id;
        eoi.Status__c = 'Submitted';
        eoi.eoi_for_project__c = newProperty.Property_Name__c;

        insert eoi;

        
        
        Inventory__c invent = new Inventory__c();
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        //invent.Marketing_Name__c = 'Damac Heights';
        invent.Marketing_Name__c = newProperty.Property_Name__c;
        invent.Address_Id__c = '1' ;
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.ID;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Apartments_In_Inventory';
        invent.Eoi__c = eoi.Id;
        invent.Tagged_To_EOI__c = true;
        insert invent; 
        
        apexpages.currentpage().getparameters().put ('Remove', 'true');
        ApexPages.StandardController controller = new ApexPages.StandardController (eoi);
        InventoriesEOIController obj = new InventoriesEOIController (controller);
        InventoriesEOIController.InventoryWrapper obj1= new InventoriesEOIController.InventoryWrapper (invent.Id, invent, true);
        obj.inventoryWrapperList.add (obj1);
        obj.AddOrRemoveInventories ();
        
        Set <ID> invId = new Set <ID> ();
        invId.add (invent.ID);
        
        InventoriesEOIController.sendToIPMS (eoi.id, invId, true);
        
        eoi.Status__c = 'Approved';
        update eoi;
    }
}