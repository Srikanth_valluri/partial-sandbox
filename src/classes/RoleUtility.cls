public with sharing class RoleUtility {
    
    //Method to return Salesforce role for relevant IPMS role
    public static String getSalesforceRole(String strIPMSRole) {
        String strSFRole = strIPMSRole;
        if(strIPMSRole.containsIgnoreCase('Manager')) {  //'CRM-Manager'
            strSFRole = 'Manager';
        }
        else if(strIPMSRole.containsIgnoreCase('Director')) { //'CRM-Director'
            strSFRole = 'Director';
        }
        else if(strIPMSRole.containsIgnoreCase('HOD')) {  //'CRM-HOD'
            strSFRole = 'HOD';
        }
        else if(strIPMSRole.containsIgnoreCase('Committee')) {  //'CRM-Committee'
            strSFRole = 'Committee';
        }
        /*else if(strIPMSRole.containsIgnoreCase('SVP Operations')) {  
            strSFRole = 'SVP Operations';
        }
        else if(strIPMSRole.containsIgnoreCase('Sales Admin AVP')) {  
            strSFRole = 'Sales Admin AVP';
        }*/
        return strSFRole;
    }
}