/**********************************************************************************************************************
Description: API to send verification OTP to user for submitting Work Permit SR
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By      | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   12-01-2021      | Anand Venkitakrishnan | Created the initial draft
***********************************************************************************************************************/

@RestResource(urlMapping='/workPermitOTP/*')
global class HDApp_WorkPermitOTP_API {
	public static String responseMessage = 'Please enter the verification code sent on your registered mobile number';
    
    @HttpGet
    global static FinalReturnWrapper getSRSubmissionOTP() {
        
        RestRequest r = RestContext.request;
        System.debug('Request Headers:'+r.headers);
        System.debug('Request params:'+r.params);
        
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        HDApp_Utility.cls_meta_data objMeta = new HDApp_Utility.cls_meta_data();
        
        if(!r.requestURI.equalsIgnoreCase('/workPermitOTP/srSubmission')) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'Please use correct API endpoint',3);
            return returnResponse;
        }
        else if(!r.params.containsKey('accountId') || (r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId')))) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No accountId found in request',3);
            return returnResponse;
        }
        else if(!r.params.containsKey('bookingUnitId') || (r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId')))) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No bookingUnitId found in request',3);
            return returnResponse;
        }
        else if(!r.params.containsKey('fmCaseId') || (r.params.containsKey('fmCaseId') && String.isBlank(r.params.get('fmCaseId')))) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No fmCaseId found in request',3);
            return returnResponse;
        }
        
        Integer statusCode;
        String guidCreated;
        
        try {
            String accountId = r.params.get('accountId');
            String bookingUnitId = r.params.get('bookingUnitId');
            String fmCaseId = r.params.get('fmCaseId');
            
            List<FM_Case__c> listFMCase = [SELECT Id,Name 
                                           FROM FM_Case__c 
                                           WHERE Id =: fmCaseId];
            
            if(listFMCase != NULL && !listFMCase.isEmpty()) {
                List<Account> listAccount = [SELECT Id,Name,IsPersonAccount,Mobile__c,Mobile_Phone_Encrypt__pc,Customer_Masked_Mobile__c,Corporate_Customer_Mobile__c
                                             FROM Account 
                                             WHERE Id =:accountId];
                
                if(listAccount!= null && listAccount.size() > 0 && (listAccount[0].Mobile__c != null || listAccount[0].Mobile_Phone_Encrypt__pc != null)) {
                    //Generate OTP code
                    String smsCode = String.valueOf((Math.random() * 10000).intValue()).leftPad(4, '0');
                    System.debug('smsCode:'+smsCode);
                    
                    //Create SMS History record
                    SMS_History__c verificationSms = new SMS_History__c();
                    verificationSms.Customer__c = accountId;
                    //verificationSms.FM_Case__c = fmCaseId;
                    verificationSms.Booking_Unit__c = bookingUnitId;
                    verificationSms.Phone_Number__c = listAccount[0].IsPersonAccount ? listAccount[0].Mobile_Phone_Encrypt__pc : listAccount[0].Mobile__c;
                    verificationSms.Message__c = smsCode + ' is the code for verifying your mobile number. '
                        						+ 'Please enter this code to submit the Work Permit Request '
                        						+ 'in Damac Living App';
                    
                    String strSMSName = 'Portal Work Permit Verification for '+listAccount[0].Name;
                    if(strSMSName.length() >= 80) {
                        strSMSName = 'Portal Work Permit Verification for '+listAccount[0].Id;
                    }
                    verificationSms.Name = strSMSName;
                    
                    System.debug('verificationSms:'+verificationSms);
                    insert verificationSms;
                    
                    guidCreated = insertOTPwithGUID(fmCaseId,smsCode);
                    System.debug('guidCreated:'+guidCreated);
                    
                    responseMessage += ' ' + verificationSms.Phone_Number__c + '. OTP: ' + smsCode;
                    
                    statusCode = 1;
                }
                else {
                    statusCode = 6;
                }
            }
            else {
                objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No FM Case found for given Id',6);
                
                returnResponse.meta_data = objMeta;
                
                return returnResponse;
            }
        }
        catch(Exception e) {
            System.debug('Exception:'+e.getMessage());
            
            statusCode = 6;
        }
        
        if(statusCode == 1) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.successMsg,responseMessage, 1);
        }
        else {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'Error in generating OTP',6);
        }
        
        cls_data objData = new cls_data();
        objData.guid = guidCreated;
        
        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;
        
        System.debug('returnResponse:'+returnResponse);
        
        return returnResponse;
    }
    
    public static String insertOTPwithGUID(String fmCaseId,String smsOtp) {
        //Logic to expire existing OTPs on the FM Case
        List<OTP__c> existingOTPs = [SELECT Id,isExpired__c 
                                     FROM OTP__c 
                                     WHERE FM_Case__c =: fmCaseId 
                                     AND FM_Process_Name__c =: HDApp_Constants.wpFMProcessName];
        System.debug('existingOTPs:'+existingOTPs);
        
        for(OTP__c objOTP : existingOTPs) {
            objOTP.isExpired__c = true;
        }
        
        System.debug('existingOTPs after expiring:'+existingOTPs);
        
        if(existingOTPs.size() > 0) {
            update existingOTPs;
        }
        
        String guid = getGUID();
		
        OTP__c objOTP = new OTP__c();
        objOTP.GUID__c = guid;
        objOTP.FM_SMS_OTP__c = smsOtp;
        objOTP.FM_Process_Name__c = HDApp_Constants.wpFMProcessName;
        objOTP.FM_Case__c = fmCaseId;
		
        System.debug('objOTP:'+objOTP);
        insert objOTP;
		
        return guid;
    }
	
    public static String getGUID() {
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        System.debug('guid generated:'+guid);
		
        return guid;
    }
    
    global class FinalReturnWrapper {
        public cls_data data;
        public HDApp_Utility.cls_meta_data meta_data;
    }
    
    public class cls_data {
        public String guid;
    }
}