@isTest
private class AgentDocumentViewer_Test {

static testMethod void myUnitTest() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        objSR.Agency__c = objAccount.Id;
        insert objSR;
        
        NSIBPM__SR_Doc__c objSRDoc = new NSIBPM__SR_Doc__c();
        objSRDoc.NSIBPM__Service_Request__c = objSR.Id;
        insert objSRDoc;
        
        Attachment objAttachment = new Attachment();
        objAttachment.Name = 'Test Document';
        objAttachment.Body = blob.valueOf('test');
        objAttachment.ContentType='image/png';
        objAttachment.ParentId = objAccount.Id;
        
        Attachment objAttachment1 = new Attachment();
        objAttachment1.Name = 'Test Document';
        objAttachment1.Body = blob.valueOf('test');
        objAttachment1.ParentId = objSRDoc.Id;
        objAttachment1.ContentType='image/png';
        insert objAttachment1;
        
        Apexpages.currentPage().getParameters().put('id',objSR.Id);
        AgentDocumentViewer objDocumentViewer = new AgentDocumentViewer();
        System.assertNOTEquals(null,objDocumentViewer.strObjectId);
        //System.assertEquals('Default Actions',objBR.Action_Type__c);
        objDocumentViewer.strObjectId = objSR.Id;
        objDocumentViewer.FileName = 'Test Doc.pdf';
        objDocumentViewer.document = objAttachment;
        objDocumentViewer.Cancel();
        objDocumentViewer.strSRDocId = objSRDoc.Id;
        objDocumentViewer.document = objAttachment;
        objDocumentViewer.strComments = 'Test Comments';
        objDocumentViewer.strExternalComments = 'Test Comments';
        objDocumentViewer.Upload();
        objDocumentViewer.AgrDocId = objSRDoc.Id;
        objDocumentViewer.EditDoc();
        objDocumentViewer.SaveDoc();
        objDocumentViewer.setstrObjectName('test');
        objDocumentViewer.getstrObjectName();
        objDocumentViewer.PrepareGenDocs();
        //objDocumentViewer.Back_To_SR();
    }
}