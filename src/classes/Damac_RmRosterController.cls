public without sharing class Damac_RmRosterController {

    public List <RM_Roster__c> existingRecords { get; set; }
    public List <String> availableRms { get; set; }
    
    public RM_Roster__c record { get; set; }
    public String selectedRms { get; set; }
    public Boolean isError { get; set; }
    public String msg { get; set; }
    public User userDetails { get; set; }
    public List <SelectOption> rms { get; set; }
    public Damac_RmRosterController (ApexPages.standardController stdController) {
        msg = '';
        isError = false;
        init ();        
    }
    
    public void init () {
        
        record = new RM_Roster__c ();
        record.Date__c = Date.Today ();
        selectedRms = '';
        getRMs ();
        userDetails = new User ();
        userDetails = [SELECT Name, Profile.name FROM User WHERE Id =: UserInfo.getUserID ()];
        
        existingRecords = new List <RM_Roster__c> ();
        existingRecords = [SELECT Date__c, Selected__c, Available_RM__c, RM_Nationality__c,
                            No_Of_Trips__c, Available_RM__r.Name, Available_RM__r.Email,
                            (SELECT Tour_Date_Time__c FROM RM_Roster_Time_Slots__r Order BY Tour_Date_Time__c ASC) 
                        FROM RM_Roster__c WHERE Date__c >= TODAY ORDER BY Date__c ASC LIMIT 50]; 
    }
    
    public void filterRecords () {
        //selectedRms = selectedRms.removeStart ('[').removeEnd (']').deleteWhitespace();
        
        if (record.Date__c != null && selectedRms != '' && selectedRms != null) {
            existingRecords = new List <RM_Roster__c> ();
            existingRecords = [SELECT Date__c, Selected__c, Available_RM__c, RM_Nationality__c,
                                No_Of_Trips__c, Available_RM__r.Name, Available_RM__r.Email,
                            (SELECT Tour_Date_Time__c FROM RM_Roster_Time_Slots__r Order BY Tour_Date_Time__c ASC)  
                        FROM RM_Roster__c 
                        WHERE RM_Name__c =:selectedRms AND Date__c >= TODAY AND Date__c =: record.Date__c ORDER BY Date__c, lastmodifiedDate ASC LIMIT 50]; 
        } else if (record.Date__c != null) {
            existingRecords = new List <RM_Roster__c> ();
            existingRecords = [SELECT Date__c, Selected__c, Available_RM__c, RM_Nationality__c,
                            No_Of_Trips__c, Available_RM__r.Name, Available_RM__r.Email,
                        (SELECT Tour_Date_Time__c FROM RM_Roster_Time_Slots__r Order BY Tour_Date_Time__c ASC)  
                    FROM RM_Roster__c 
                    WHERE Date__c =: record.Date__c  AND Date__c >= TODAY ORDER BY Date__c, lastmodifiedDate ASC LIMIT 50]; 
        } else if (selectedRms != '' && selectedRms != null) {
            existingRecords = new List <RM_Roster__c> ();
            existingRecords = [SELECT Date__c, Selected__c, Available_RM__c, RM_Nationality__c,
                            No_Of_Trips__c, Available_RM__r.Name, Available_RM__r.Email,
                        (SELECT Tour_Date_Time__c FROM RM_Roster_Time_Slots__r Order BY Tour_Date_Time__c ASC)  
                    FROM RM_Roster__c 
                    WHERE RM_Name__c =:selectedRms  AND Date__c >= TODAY ORDER BY Date__c, lastmodifiedDate ASC LIMIT 50];         
        }
        
    }
    
    public void getAvailableRms() {
        availableRms = new List <String> ();
        List<SelectOption> options = new List<SelectOption>();
        for (User u :[ SELECT Name, Email FROM USER WHERE IsActive = TRUE AND Profile.Name LIKE '%Property Consultant%' ])
        {
            availableRms.add(u.Name);
        }
    }
    
    
    
    public void getRMs () {
        rms = new List <SelectOption> ();
        Set<Id> userIds = new Set<Id>();
        system.debug (record.Date__c);
        
        for(RM_Roster__c  eachRM : [SELECT Id,Available_RM__c  FROM RM_Roster__c WHERE Date__c =: record.Date__c]){
            userIds.add(eachRM.Available_RM__c);
        }
        
        List<SelectOption> options = new List<SelectOption>();
        for (User u :[ SELECT Name, Email FROM USER WHERE IsActive = TRUE AND Profile.Name LIKE '%Property Consultant%' AND (NOT(ID IN : userIds))])
        {
            options.add(new SelectOption(u.id, u.Name));
        }
        rms = options;
        
        existingRecords = [SELECT Date__c, Selected__c, Available_RM__c, RM_Nationality__c,
                            No_Of_Trips__c, Available_RM__r.Name, Available_RM__r.Email,
                        (SELECT Tour_Date_Time__c FROM RM_Roster_Time_Slots__r Order BY Tour_Date_Time__c ASC)  
                    FROM RM_Roster__c 
                    WHERE Date__c =: record.Date__c AND Date__c >= TODAY ORDER BY Date__c, lastmodifiedDate ASC LIMIT 50]; 
    }
    
    public void saveRMRoster () {
        System.debug (selectedRms);
        
        if (record.Date__c != null && selectedRms != null && selectedRms != '[]') {
            if (record.Date__c < Date.Today()) {
                isError = true;
                msg = 'Date should be greater or equal to TODAY.';
            } else {
                selectedRms = selectedRms.removeStart ('[').removeEnd (']').deleteWhitespace();
                List <String> rmIds = new List <String> ();
                if (selectedRms.contains(',')) {
                    rmIds = selectedRms.split (',');
                } else {
                    rmIds.add (selectedRms);
                }
                List <RM_Roster__c> recordsToInsert = new List <RM_Roster__c> ();
                for (String rmId :rmIds) {
                    RM_Roster__c rec = new RM_Roster__c ();
                    rec.Date__c = record.Date__c;
                    rec.Available_RM__c = rmId.trim();
                    Date currentDate = record.Date__c;
                    String dateString = currentDate.format();
                    rec.UniqueId__c = dateString +'-'+rmId.trim();
                    recordsToInsert.add (rec);
                }
                Database.UpsertResult[] results = Database.upsert(recordsToInsert, false);
                
                
                isError = false;
                msg = 'RM Roster created/updated successfully.';
                init ();
            }
        }
        else {
            isError = true;
            msg = 'Please fill all required fields.';
        }
        
    }
    
    public static void assignTimeSlots (Map <Id, DateTime> rmTourDates) {
        Set <Date> tourDate = new Set <Date> ();
        for (ID key :rmTourDates.keySet ()) {
            if (rmTourDates.get (key) == null) {
                rmTourDates.put (key, DateTime.Now());
            }
            tourDate.add (rmTourDates.get (key).date());
        }
        System.debug (tourDate);
        
        List <RM_Roster__c> rosterToUpdate = new List <RM_Roster__c> ();
        List <RM_Roster_Time_Slot__c> timeSlotsToInsert = new List <RM_Roster_Time_Slot__c> ();
        
        for (RM_Roster__c roster : [SELECT Available_Rm__c, Selected__c FROM RM_Roster__c WHERE Available_RM__c IN :rmTourDates.keySet () AND Date__c IN :tourDate]) {
            roster.Selected__c = true;
            rosterToUpdate.add (roster); 
            
            RM_Roster_Time_Slot__c slot = new RM_Roster_Time_Slot__c ();
            slot.Tour_Date_Time__c =  rmTourDates.get (roster.Available_Rm__c);
            slot.RM_Roster__c = roster.Id;
            timeSlotsToInsert.add (slot);
        }
        if (rosterToUpdate.size () > 0) {
            update rosterToUpdate;
        }
        if (timeSlotsToInsert.size () > 0) {
            insert timeSlotsToInsert;
        }
    }
    
    
}