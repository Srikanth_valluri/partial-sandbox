/**
 * @File Name          : TestTestWelcomeCLCBREmailBatch_Final.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10/28/2019, 3:22:37 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    10/22/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
private class TestWelcomeCLCBREmailBatch_Final{

    @testSetup static void setup(){

    }

    @isTest static void testMethod1() {
        // code_block   
        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        insert objAccount;
        System.debug('-->> objAccount ## : ' + objAccount );

        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;

        Id recordTypeIdWlc = [SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Welcome_Calling_List'
            AND IsActive = TRUE LIMIT 1].Id;

        Calling_List__c objCalling_List = new Calling_List__c(
            RecordTypeId = recordTypeIdWlc,
            CBR_Assignment_Date__c = System.Today() - 1,
            Unit_Name__c = 'Test Unit',
            Notification_Time_for_outcome__c = System.Now(),
            IsHideFromUI__c = false,
            Customer_Name__c = 'Test Customer',
            Registration_ID__c = 'test',
            Calling_List_Status__c = 'New',
            Account__c = objAccount.Id
        );
        insert objCalling_List;
        System.debug('-->> objTask : ' + objCalling_List );

        Test.startTest();
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            WelcomeCLCBREmailBatch_Final objWelcomeCLCBREmailBatch_Final = new WelcomeCLCBREmailBatch_Final();
            Database.executeBatch(objWelcomeCLCBREmailBatch_Final , 1);
        Test.stopTest();

    }

    @isTest static void testMethod2() {
        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        insert objAccount;
        System.debug('-->> objAccount ## : ' + objAccount );

        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;

        Id recordTypeIdWlc = [SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Welcome_Calling_List'
            AND IsActive = TRUE LIMIT 1].Id;

        Calling_List__c objCalling_List = new Calling_List__c(
            RecordTypeId = recordTypeIdWlc,
            CBR_Assignment_Date__c = System.Today() - 2,
            Unit_Name__c = 'Test Unit',
            Notification_Time_for_outcome__c = System.Now(),
            IsHideFromUI__c = false,
            Customer_Name__c = 'Test Customer',
            Registration_ID__c = 'test',
            Calling_List_Status__c = 'New',
            Account__c = objAccount.Id
        );
        insert objCalling_List;
        System.debug('-->> objTask : ' + objCalling_List );
        
        Test.startTest();
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            WelcomeCLCBREmailBatch_Final objWelcomeCLCBREmailBatch_Final = new WelcomeCLCBREmailBatch_Final();
            Database.executeBatch(objWelcomeCLCBREmailBatch_Final , 1);
        Test.stopTest();
    }
    
    @isTest static void testMethod3() {
        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        insert objAccount;
        System.debug('-->> objAccount ## : ' + objAccount );

        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;

        Id recordTypeIdWlc = [SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Welcome_Calling_List'
            AND IsActive = TRUE LIMIT 1].Id;

        Calling_List__c objCalling_List = new Calling_List__c(
            RecordTypeId = recordTypeIdWlc,
            CBR_Assignment_Date__c = System.Today() - 6,
            Unit_Name__c = 'Test Unit',
            Notification_Time_for_outcome__c = System.Now(),
            IsHideFromUI__c = false,
            Customer_Name__c = 'Test Customer',
            Registration_ID__c = 'test',
            Calling_List_Status__c = 'New',
            Account__c = objAccount.Id
        );
        insert objCalling_List;
        System.debug('-->> objTask : ' + objCalling_List );

        // code_block   
        Test.startTest();
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            WelcomeCLCBREmailBatchSchedule objWelcomeCLCBREmailBatchSchedule = new WelcomeCLCBREmailBatchSchedule();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test Account Sales Calculator Check', sch, objWelcomeCLCBREmailBatchSchedule);
        Test.stopTest();
    }
}