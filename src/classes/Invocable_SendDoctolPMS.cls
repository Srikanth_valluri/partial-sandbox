/**************************************************************************************************
* Name               : Invocable_SendDoctolPMS
* Test Class         : Invocable_SendDoctolPMS_Test
* Description        : This is the custom code class for sending the documents details to IPMS         
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0                         16/06/2019      Created
* 1.1         QBurst          08/04/2020      Commented out SOAP call for SR Docs
**************************************************************************************************/
public class Invocable_SendDoctolPMS {
    @InvocableMethod
    public static void UpdateBuyer(List<Id> stepId) {
        for (New_Step__c step :[SELECT Id, Service_Request__c FROM New_Step__c WHERE Id IN :stepId]) {
            evaluateCustomCode(step);
        }
    }

    public static String evaluateCustomCode(New_Step__c step) {
        String retStr = 'Success';
        List<Id> srIds = new List<Id> ();
        try {
            srIds.add(step.Service_Request__c);
       //     system.enqueueJob(new AsyncReceiptWebservice(srIds, 'SendDoc')); // 1.1
        } catch(Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
        }
        return retStr;
    }
}