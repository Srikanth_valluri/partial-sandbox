public without sharing class ChangeTaskAssignment 
{

    public Task objTask;
    public String strPageID;
    
    public ChangeTaskAssignment (ApexPages.StandardController stdCon){
    }

    public void createChangeTaskAssignment()
    {   
        strPageID = ApexPages.currentPage().getParameters().get('id');
        objTask = [
            SELECT Id
                 , OwnerId
                 , ActivityDate  
              FROM Task
             WHERE Id = :strPageID
        ];
        System.Debug('-->> objTask Before : ' + objTask);
        if(objTask.ownerId != NULL){
            objTask.ownerId = UserInfo.getUserId();                           
        }
        try{
            update objTask;
        } catch(Exception e){
            System.Debug('-->> Exception : ' + e);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Something went wrong. Please check the logs.');
            ApexPages.addMessage(myMsg);
        }
        System.Debug('-->> objTask: ' + objTask);
        //return returnToAccount();
    }
    
    public pageReference returnToAccount(){
        System.debug('-->> returnToAccount: ');
        pagereference newpg = new Pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+objTask.Id);
        newpg.setRedirect(true);
        system.debug('newpg********'+newpg);
        return newpg;
    }
    
    
}