/*
* Description - Test class developed for 'RentalPoolTerminationUtility'
*
* Version            Date            Author            Description
* 1.0              29/02/2018        Ashish           Initial Draft
*/

@isTest 
private class RentalPoolTerminationUtilityTest {

    static testMethod void testGetAllRelatedBookingUnits() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        test.startTest();
        	RentalPoolTerminationUtility.getAllRelatedBookingUnits( objAcc.Id );
        test.stopTest();
        
    }
    
    static testMethod void testGetCaseAttachments() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        insert objCase ;
        
        test.startTest();
        	RentalPoolTerminationUtility.getCaseAttachments( objCase.Id );
        test.stopTest();
        
    }
    
    static testMethod void testGetCaseDetails() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        insert objCase ;
        
        test.startTest();
        	RentalPoolTerminationUtility.getCaseDetails( objCase.Id );
        test.stopTest();
        
    }
    
    static testMethod void testGetUnitDetails() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        NSIBPM__Service_Request__c objInq = TestDataFactory_CRM.createServiceRequest();
        insert objInq ;
        
        list<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objInq.Id,1);
        insert lstBookings ;
        
        List<Booking_Unit__c> lstUnits = TestDataFactory_CRM.createBookingUnits(lstBookings,1);
        insert lstUnits ;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        insert objCase ;
        
        test.startTest();
        	RentalPoolTerminationUtility.getUnitDetails( lstUnits[0].Id );
        test.stopTest();
        
    }
    
}