/*******************************************************************************************************
Description : Class to hold deserialzed reponse of Rebrandly service
--------------------------------------------------------------------------------------------------------
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
--------------------------------------------------------------------------------------------------------
1.0     | 19-02-2020        |Aishwarya Todkar    | Initial Draft
*****************************************************************************************************/
public class RebradnlyResponseWrapper{
	public String id;	//409c1e54554c40b9bf83151383bdc6b9
	public String title;	//Link sjvkfzc
	public String slashtag;	//sjvkfzc
	public String destination;	//https://www.youtube.com/channel/UCHK4HD0ltu1-I212icLPt3g
	public String createdAt;	//2020-02-19T09:07:39.000Z
	public String updatedAt;	//2020-02-19T09:07:39.000Z
	public String status;	//active
	public cls_tags[] tags;
	public Integer clicks;	//0
	public boolean isPublic;
	public String shortUrl;	//rebrand.ly/sjvkfzc
	public String domainId;	//8f104cc5b6ee4a4ba7897b06ac2ddcfb
	public String domainName;	//rebrand.ly
	public cls_domain domain;
	public boolean https;
	public boolean favourite;
	public cls_creator creator;
	public boolean integrated;
	class cls_tags {
	}
	class cls_domain {
		public String id;	//8f104cc5b6ee4a4ba7897b06ac2ddcfb
		public String ref;	///domains/8f104cc5b6ee4a4ba7897b06ac2ddcfb
		public String fullName;	//rebrand.ly
		public boolean active;
	}
	class cls_creator {
		public String id;	//010f4f33e9004e8c9840f37eb21a7d71
		public String fullName;	//Aishwarya Todkar
		public String avatarUrl;	//https://s.gravatar.com/avatar/5053d3f94b164eea6bbf974a9c94615c?size=80&d=retro&rating=g
	}
	public static RebradnlyResponseWrapper parse(String json){
		return (RebradnlyResponseWrapper) System.JSON.deserialize(json, RebradnlyResponseWrapper.class);
	}
}