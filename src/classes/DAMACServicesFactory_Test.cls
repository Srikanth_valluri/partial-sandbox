@isTest
public class DAMACServicesFactory_Test {
    @testsetup
    public static void makeData(){
        Inv_Fld_Mapping__c fldmap = new Inv_Fld_Mapping__c();
        
        fldmap.name = '01';
        fldmap.Is_Property__c = true;
        fldmap.Is_Number_Conversion_Req__c = true;
        fldmap.Field_API_Name__c = 'Property_ID__c';
        fldmap.Inv_Field_API_Name__c = 'Property_ID__c';
        //fldmap
        insert fldmap;        
        
        Property__c  propertyDetail = InitialiseTestData.getPropertyDetails(7650);
        insert propertyDetail ; 
        
        Inventory__c inventory = InitialiseTestData.getInventoryDetails('3456','1234','2345',9086,7650);
        inventory.Conn_Project_Code__c= '123'; 
        inventory.Building_Code__c = 'B123';
        insert inventory ; 
        
        inventory.Status__c = 'Blocked';
        update inventory ; 
        
        inventory.Status__c = 'Released';
        update inventory ;
    }
    
    //No Api key found
    public static testmethod void m0(){
        string endpoint = '/services/apexrest/DAMACFactory/searchinventory';
        DAMAC_Approval_Factory_Test.initializeRestContext(null, endpoint);
        Map<String, String> params = new Map<String, String>();
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
        test.startTest();
        DAMACServicesFactory.doGET();
        test.stopTest();
    }

    //wrong Api key found
    public static testmethod void m1(){
        string endpoint = '/services/apexrest/DAMACFactory/searchinventory';
        DAMAC_Approval_Factory_Test.initializeRestContext(null, endpoint);
        Map<String, String> params = new Map<String, String>();
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
        RestContext.request.headers.put('serviceId', Damac_PhoneEncrypt.encryptPhoneNumber('Hello DAMAC1'));
        test.startTest();
        DAMACServicesFactory.doGET();
        test.stopTest();
    }
    //With API KEy
    public static testmethod void m2(){
        string endpoint = '/services/apexrest/DAMACFactory/searchinventory';
        DAMAC_Approval_Factory_Test.initializeRestContext(null, endpoint);
        Map<String, String> params = new Map<String, String>();
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
        RestContext.request.headers.put('serviceId', Damac_PhoneEncrypt.encryptPhoneNumber('Hello DAMAC'));
        test.startTest();
        DAMACServicesFactory.doGET();
        test.stopTest();
    }

    //With API KEy
    public static testmethod void m3(){
        string endpoint = '/services/apexrest/DAMACFactory/searchinventory';
        DAMAC_Approval_Factory_Test.initializeRestContext(null, endpoint);
        Map<String, String> params = new Map<String, String>();
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
        RestContext.request.headers.put('serviceId', Damac_PhoneEncrypt.encryptPhoneNumber('Hello DAMAC'));
        test.startTest();
        RestContext.request.params.put('projectCode', '123');
        RestContext.request.params.put('buildingCode', 'B123');
        DAMACServicesFactory.doGET();
        test.stopTest();
    }
    //With API KEy
    public static testmethod void m4(){
        string endpoint = '/services/apexrest/DAMACFactory/searchinventory';
        DAMAC_Approval_Factory_Test.initializeRestContext(null, endpoint);
        Map<String, String> params = new Map<String, String>();
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
        RestContext.request.headers.put('serviceId', Damac_PhoneEncrypt.encryptPhoneNumber('Hello DAMAC'));
        test.startTest();
        RestContext.request.params.put('projectCode', '123');
        DAMACServicesFactory.doGET();
        test.stopTest();
    }

}