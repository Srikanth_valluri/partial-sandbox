/**************************************************************************************************
* Name               : AccountSalesCalculatorTest                                                 *
* Description        : Test class for AccountSalesCalculator batch class.                         *
* Created Date       : 21/02/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         NSI - Vineet      21/02/2017      Initial Draft.                                    *
* 1.1         Monali Nagpure    14/02/2018      Updated to get enough code coverage               *
**************************************************************************************************/ 
@isTest
private class AccountSalesCalculatorTest {
	private static final Id DEAL_SR_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('NSIBPM__Service_Request__c', 'Deal');
    @testSetup static void setupData() {
        Account acc = InitialiseTestData.getCorporateAccount('Test Account Deal');
        acc.Eligible_For_Tier_Program__c = true;
        insert acc;
    }

    static testmethod void test1() {

        Account getAccBeforeUpdate = [SELECT Id, 
			                                 Name, 
			                                 Quarter_1_Sales__c, 
			                                 Quarter_2_Sales__c, 
			                                 Quarter_3_Sales__c, 
			                                 Quarter_4_Sales__c 
			                            From Account 
			                           WHERE Name 
			                           LIKE 'Test Account Deal' 
			                          LIMIT 1];


        NSIBPM__Service_Request__c serviceRequest = new NSIBPM__Service_Request__c();
        serviceRequest.DP_ok__c = true;
        serviceRequest.Doc_ok__c = true;
        serviceRequest.Agency__c = getAccBeforeUpdate.Id;
        serviceRequest.Registration_Date__c = Date.newInstance(2018,5,25);
        serviceRequest.NSIBPM__Customer__c = getAccBeforeUpdate.Id;
        serviceRequest.RecordTypeId = DEAL_SR_RECORD_TYPE_ID;
        insert serviceRequest;

        Test.startTest();
        AccountSalesCalculator obj = new AccountSalesCalculator();
    	DataBase.executeBatch(obj); 

        Test.stopTest();
    }

    static testmethod void test2() {
        Account getAccBeforeUpdate = [SELECT Id, 
                                 Name, 
                                 Quarter_1_Sales__c, 
                                 Quarter_2_Sales__c, 
                                 Quarter_3_Sales__c, 
                                 Quarter_4_Sales__c 
                            From Account 
                           WHERE Name 
                           LIKE 'Test Account Deal' 
                          LIMIT 1];

		NSIBPM__Service_Request__c serviceRequest = new NSIBPM__Service_Request__c();
        serviceRequest.DP_ok__c = true;
        serviceRequest.Doc_ok__c = true;
        serviceRequest.Agency__c = getAccBeforeUpdate.Id;
        serviceRequest.Registration_Date__c = Date.newInstance(2017,5,25);
        serviceRequest.NSIBPM__Customer__c = getAccBeforeUpdate.Id;
        serviceRequest.RecordTypeId = DEAL_SR_RECORD_TYPE_ID;
        insert serviceRequest;

        Test.startTest();
        AccountSalesCalculator obj = new AccountSalesCalculator();
    	DataBase.executeBatch(obj); 
        
        Test.stopTest();

    }


}// End of class.