@istest
public class ZIWOCallHistoryScheduler_Test{

    static testmethod void m1(){
        Ziwo_settings__c settings = new Ziwo_settings__c();
        settings.name = 'test';
        settings.Access_token__c = '233452345';
        settings.Last_logged_in_Time__c = system.now();
        insert settings;
        
        Task tObj = new Task();
        tObj.CrtObjectId__c = '1234';
        tObj.subject = 'test';
        tObj.Event_Type__c = 'Ziwo Outbound';
        tObj.Ameyo_Dialer_Disposition__c = 'CALLING LIST';
        insert tObj;
        
        Waybeo_logs__c log = new Waybeo_logs__c();
        log.Ziwo_CallID__c = '1234';
        log.Ziwo_talkTime__c ='200';
        log.Ziwo_callerIDNumber__c = '2341342352';
        log.Ziwo_direction__c = 'outbound';
        insert log;
        
        String CRON_EXP = '0 0 0 3 9 ? 2022';
        Test.startTest();
        String jobId = System.schedule('ZIWOCallHistoryScheduler',
                                        CRON_EXP, 
                                        new ZIWOCallHistoryScheduler());
        Test.StopTest();
        
    }
}