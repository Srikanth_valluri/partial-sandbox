/**********************************************************************************************************************
Description: This API is for returning payment mode data required for POP SR creation
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By      | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   15-12-2020    | Anand Venkitakrishnan | Created initial draft
***********************************************************************************************************************/

@RestResource(urlMapping='/popPaymentModes/*')
global  class POPPaymentModesAPI {
    public static final String PROPERTY_INSTALLMENT = 'installment';
    public static final String SERVICE_CHARGE = 'service_charge';
    
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };
    
    @HttpGet
    global static FinalReturnWrapper getPaymentModes() {
        
        RestRequest r = RestContext.request;
        System.debug('Request Headers:'+r.headers);
        System.debug('Request params:'+r.params);
        
        cls_meta_data objMeta = new cls_meta_data();
        FinalReturnWrapper objReturn = new FinalReturnWrapper();
        
        if(!r.params.containsKey('paymentType')) {
            objMeta.message = 'Missing parameter : paymentType';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            
            objReturn.meta_data = objMeta;
            return objReturn;
        }
        else if(r.params.containsKey('paymentType') && String.isBlank(r.params.get('paymentType'))) {
            objMeta.message = 'Missing parameter value: paymentType';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
      
            objReturn.meta_data = objMeta;
            return objReturn;
        }
        
        List<PickListValuesWrapper> pickListValuesWrapperList = new List<PickListValuesWrapper>();
        List<String> currencies = new List<String>();
        
        String paymentType = r.params.get('paymentType');
        
        if(paymentType.equals(PROPERTY_INSTALLMENT)) {
            List<String> cashOptions = getselectOptions('Case','Cash_Deposit_Slip_Details__c');
            System.debug('cashOptions:'+cashOptions);
            
            List<String> chequeOptions = getselectOptions('Case','Cheque_from_Account__c');
            System.debug('chequeOptions:'+chequeOptions);
            
            for(String paymentMode : getselectOptions('Case','Payment_Mode__c')) {
                PickListValuesWrapper pickListValuesWrapper = new PickListValuesWrapper();
                pickListValuesWrapper.payment_mode_name = paymentMode;
                
                if(paymentMode.equals('Cash')) {
                    pickListValuesWrapper.payment_mode_details_name = 'Cash Deposit Slip';
                    pickListValuesWrapper.payment_mode_details_lookups = cashOptions;
                }
                else if(paymentMode.equals('Cheque')) {
                    pickListValuesWrapper.payment_mode_details_name = 'Cheque from Account';
                    pickListValuesWrapper.payment_mode_details_lookups = chequeOptions;
                }
                
                pickListValuesWrapperList.add(pickListValuesWrapper);
            }
            
            currencies = getselectOptions('Case','CurrencyIsoCode');
        }
        else if(paymentType.equals(SERVICE_CHARGE)) {
            for(String paymentMode : getselectOptions('FM_Case__c','Payment_Mode__c')) {
                PickListValuesWrapper pickListValuesWrapper = new PickListValuesWrapper();
                pickListValuesWrapper.payment_mode_name = paymentMode;
                pickListValuesWrapperList.add(pickListValuesWrapper);
            }
            
            currencies = getselectOptions('FM_Case__c','CurrencyIsoCode');
        }
        
        System.debug('pickListValuesWrapperList:'+pickListValuesWrapperList);
        System.debug('currencies:'+currencies);
        
        objMeta.message = pickListValuesWrapperList.size() > 0 ? 'Successful' : 'No picklist Values';
        objMeta.status_code = 1;
        objMeta.title = mapStatusCode.get(1);
        objMeta.developer_message = null;
        
        cls_data objData = new cls_data();
        objData.payment_mode = pickListValuesWrapperList;
        objData.payment_currency = currencies;
        System.debug('objData:'+objData);
        
        objReturn.data = objData;
        objReturn.meta_data = objMeta;
        System.debug('objReturn: ' + objReturn);
        
        return objReturn;
    }
    
    public static List<String> getselectOptions(String sObjectName, string fieldName) {  
        List<String> allOpts = new List<String>();
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(sObjectName);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        List<Schema.PicklistEntry> values = fieldMap.get(fieldName).getDescribe().getPickListValues();  
        
        for(Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        System.debug('allOpts:'+allOpts);
        
        return allOpts;
    }
  
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }
  
    public class PickListValuesWrapper {
        public String payment_mode_name;
        public String payment_mode_details_name;
        public List<String> payment_mode_details_lookups;
    }
    
    public class cls_data {
        public PickListValuesWrapper[] payment_mode;
        public List<String> payment_currency;
    }
  
    public class cls_meta_data {
        public String message; 
        public Integer status_code;
        public String title; 
        public String developer_message; 
    }
}