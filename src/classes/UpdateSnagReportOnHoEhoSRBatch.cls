public class UpdateSnagReportOnHoEhoSRBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful {
    
    public list<SMS_History__c> lstSMSHistory;
    
    
    //public static Id snagSRRecordTypeId = 
        //Schema.SObjectType.Case.getRecordTypeInfosByName().get('Snag Email').getRecordTypeId();
    
   /* public UpdateSnagReportOnHoEhoSRBatch() {
        lstSMSHistory = new list<SMS_History__c>();
    }*/
    public Database.QueryLocator start(Database.BatchableContext BC) {
        String strToAdd ='site.inspection@damacgroup.com';
    String strLabel = System.Label.ToDelete;
        String query = 'SELECT Id,Subject,IsSnagRead__c,ParentId,Incoming,ToAddress,IsBUSnagRead__c ' +
                ' FROM EmailMessage WHERE ParentId != null AND Subject != \'\' ' + 
        ' AND ToAddress Like \'%' + strToAdd + '%\' AND CreatedDate = THIS_YEAR AND IsSnagRead__c = false order by CreatedDate DESC';
        
                // AND ParentId =: strLabel order by CreatedDate ';// And IsBUSnagRead__c = false LIMIT 9999 ';
                
        //' AND ParentId =: strLabel ;
            
        system.debug( ' query : ' + query );
        
        return Database.getQueryLocator(query);
    }
    
    public void execute( Database.BatchableContext BC, List<EmailMessage> emailMessagelst ) {
        system.debug( ' emailMessagelst : ' + emailMessagelst.size() );
        Set<String> setBUName = new Set<String>();
        Map<Id,String> mapEmailMsgIdBU = new Map<Id,String>();
        Map<String, Case> mapUnitNameCase = new Map<String, Case>();
        List<EmailMessage> lstToUpdateSnagEmail = new List<EmailMessage>();
        List<Case> lstCaseToUpdate = new List<Case>();
        List<Booking_Unit__c> lstBUToUpdate = new List<Booking_Unit__c>();
        Map<Id,Booking_Unit__c> mapIdToBUtoUpdate =  new Map<Id, Booking_Unit__c>();
        List<Messaging.SingleEmailMessage> messages =   new List<Messaging.SingleEmailMessage>();
        List<Error_Log__c> listErrorLogs = new List<Error_log__c>();
        lstSMSHistory = new list<SMS_History__c>();
        Boolean runSnagLogic = false;
    Map<String, String> mapUnitNameStatus = new Map<String, String>();
    Map<String, Date> mapUnitNameDate = new Map<String, Date>();
    Boolean blnSendEmail = false;
    
        EmailTemplate SNAGReportTemplate = [SELECT
                                                ID, Subject, Body, HtmlValue 
                                            FROM 
                                                EmailTemplate
                                            WHERE 
                                                DeveloperName = 'Snag_Report' limit 1];
                                                
        OrgWideEmailAddress[] owea = [select 
                                        Id 
                                    from 
                                        OrgWideEmailAddress 
                                    where Address = 'atyourservice@damacproperties.com'];
        String strToAddress = '';
        for( EmailMessage objEmailMsg : emailMessagelst ) {
            if(objEmailMsg.ParentId != null 
            && String.valueOf(objEmailMsg.ParentId).startsWith('500')
            && objEmailMsg.Incoming 
            && String.isNotBlank ( objEmailMsg.Subject) 
            && String.isNotBlank ( objEmailMsg.ToAddress) ) {
      //&& objEmailMsg.ToAddress == 'site.inspection@damacgroup.com'
            //&& objEmailMsg.IsSnagRead__c == false ) {
        strToAddress = objEmailMsg.ToAddress;
        if( strToAddress.contains('site.inspection@damacgroup.com') ) {
          //system.debug( ' objEmailMsg.Subject before : ' + objEmailMsg.Subject );
          if( objEmailMsg.Subject.toLowerCase().contains('-') ) {
            //system.debug( ' objEmailMsg.Subject after : ' + objEmailMsg.Subject );
             List<String> lstSubjectSplit = objEmailMsg.Subject.split('\\]-');
             if( lstSubjectSplit != null && lstSubjectSplit.size()>=4 && lstSubjectSplit[3] != null ) {
              //system.debug( ' lstSubjectSplit[3] before : ' + lstSubjectSplit[3] );
              lstSubjectSplit[3] = lstSubjectSplit[3].replace('[','');
              lstSubjectSplit[3] = lstSubjectSplit[3].replace(']','');
              //system.debug( ' lstSubjectSplit[3] after : ' + lstSubjectSplit[3] );
              //system.debug( ' lstSubjectSplit[3] after trim : ' + lstSubjectSplit[3].trim() );
              if( lstSubjectSplit[3] != null ) {
                setBUName.add(lstSubjectSplit[3].trim());
                mapEmailMsgIdBU.put(objEmailMsg.Id,lstSubjectSplit[3].trim());
                
                if( lstSubjectSplit[1] != null ) {
                  lstSubjectSplit[1] = lstSubjectSplit[1].replace('[','');
                  lstSubjectSplit[1] = lstSubjectSplit[1].replace(']','');
                  if( lstSubjectSplit[1] != null ) {
                    mapUnitNameStatus.put(lstSubjectSplit[3].trim(),lstSubjectSplit[1].trim());
                  }
                }
                if( lstSubjectSplit[2] != null ) {
                  lstSubjectSplit[2] = lstSubjectSplit[2].replace('[','');
                  lstSubjectSplit[2] = lstSubjectSplit[2].replace(']','');
                  if( lstSubjectSplit[2] != null ) {
                    if(  lstSubjectSplit[2].contains( ' ' ) ) {
                      lstSubjectSplit[2] = lstSubjectSplit[2].substringBefore(' ');
                    }
                    mapUnitNameDate.put(lstSubjectSplit[3].trim(),Date.parse(lstSubjectSplit[2].trim()));
                  }
                }
              }
              objEmailMsg.IsSnagRead__c = true;
              //objEmailMsg.IsBUSnagRead__c = true;
              lstToUpdateSnagEmail.add(objEmailMsg);
             }
          }
        }
            }
        }
    system.debug( ' mapUnitNameStatus : ' + mapUnitNameStatus );
    system.debug( ' mapUnitNameDate : ' + mapUnitNameDate );
    system.debug( ' setBUName : ' + setBUName );
        system.debug( ' mapEmailMsgIdBU : ' + mapEmailMsgIdBU );
    
        if( mapEmailMsgIdBU != null && mapEmailMsgIdBU.size() > 0 ) {
            Map<String, Attachment> mapBUAttachment = new Map<String,Attachment>();
            
            for( Attachment objAtt : [ SELECT Id,Body,Name,ParentId FROM Attachment WHERE ParentId IN:mapEmailMsgIdBU.keySet() ]) {
                if( objAtt.Body != null ) {
                    mapBUAttachment.put(mapEmailMsgIdBU.get(objAtt.ParentId),objAtt);
                }
            }
            System.debug('mapBUAttachment = ' + mapBUAttachment);
            if( mapBUAttachment != null && mapBUAttachment.size() > 0 ) {
                
                List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest = new  List<UploadMultipleDocController.MultipleDocRequest>();
                List<SR_Attachments__c> lstSRAttachment = new List<SR_Attachments__c>();
                integer intIncrementor = 0;
                List<SR_Attachments__c> lstSRAttachmentToDelete = new List<SR_Attachments__c>();                
        
        set<SR_Attachments__c> setSR_Attachments = new set<SR_Attachments__c>();
        
                //Getting Active status values for booking units
                List<Booking_Unit_Active_Status__c> csActiveValues = Booking_Unit_Active_Status__c.getall().values();
                List<String> lstActiveStatus = new List<String>();
                for(Booking_Unit_Active_Status__c cs : csActiveValues) {
                    lstActiveStatus.add(cs.Status_Value__c);
                }               
                
                List<Booking_Unit__c> lstBUs = [Select Id,SRF_Date__c,SRF_Status__c
                                                    , Unit_Name__c,Snag_Received__c
                                                    , Property_Name_Inventory__c
                                                    , SNAG_Report_Sent__c
                                                    , Booking__r.Account__c
                                                    , Booking__r.Account__r.party_ID__C
                                                    , Booking__r.Account__r.Mobile_Phone_Encrypt__pc
                                                    , Booking__r.Account__r.Email__pc
                                                    , Booking__r.Account__r.Name
                                                    /*, (SELECT 
                                                                Id
                                                                , Name  
                                                            FROM 
                                                                Case_Attachments__r 
                                                            WHERE Name = 'Snag Report' 
                                                                AND Attachment_URL__c != '') */
                          ,
                          (Select Id
                                                        , Snags_Reported__c
                                                        , SNAG_Report_Sent__c
                                                        , CaseNumber,Booking_Unit__c
                                                        , RecordTypeId
                                                        , RecordType.DeveloperName
                                                        , Status
                                                        /*, (SELECT 
                                                                Id
                                                                , Name  
                                                            FROM 
                                                                SR_Attachments__r 
                                                            WHERE Name = 'Snag Report' 
                                                                AND Attachment_URL__c != '')*/
                                                        FROM
                                                            cases__r
                                                        WHERE 
                                                            RecordType.DeveloperName = 'Handover' 
                                                            OR RecordType.DeveloperName = 'Early_Handover'
                                                        ORDER BY 
                                                            LastModifiedDate DESC
                                                        LIMIT 1
                                                        )
                                                        FROM
                                                            Booking_Unit__c
                                                        WHERE
                                                            /*Registration_Status__c IN : lstActiveStatus
                                                        AND*/
                                                            Unit_Active__c = 'Active'
                                                        AND
                                                            Unit_Name__c IN: mapBUAttachment.keySet()
                                                    ];
                system.debug('lstBUs : '+lstBUs);
                Set<Id> setCaseId = new Set<Id>();
        if(lstBUs != null && lstBUs.size() > 0) {
          for(Booking_Unit__c objBuTemp : lstBUs) {
            if(objBuTemp.cases__r.size() > 0) {
              setCaseId.add(objBuTemp.cases__r[0].Id);
            }
            
            system.debug('objBuTemp.SRF_Date__c  : '+objBuTemp.SRF_Date__c );
            system.debug('mapUnitNameDate.get( objBuTemp.Unit_Name__c ) : '+mapUnitNameDate.get( objBuTemp.Unit_Name__c ));
            
            if( mapUnitNameDate != null && mapUnitNameDate.size() > 0 
            && mapUnitNameDate.containsKey( objBuTemp.Unit_Name__c ) ) {
              //commented below code As requested by kanu on 24 June 2019
              /*if( objBuTemp.SRF_Date__c != null && objBuTemp.SRF_Date__c < mapUnitNameDate.get( objBuTemp.Unit_Name__c ) ) {
                runSnagLogic = true;
              }else if( objBuTemp.SRF_Date__c == null ) {
                runSnagLogic = true;
              }*/
              runSnagLogic = true;
            }
            
          }
        
          system.debug( 'runSnagLogic : ' + runSnagLogic );
          if( runSnagLogic ) {
            Map<Id, Case> mapBUIdToCase;
            if(setCaseId.size() > 0) {
              mapBUIdToCase = new Map<Id, Case>();
              for(Case objCaseTemp : [Select Id
                            , Booking_Unit__c
                            , (SELECT 
                                Id
                                , Name  
                              FROM 
                                SR_Attachments__r 
                              WHERE Name = 'Snag Report' 
                                AND Attachment_URL__c != '')
                            FROM
                              case
                            WHERE
                              Id In : setCaseId]) {
                mapBUIdToCase.put(objCaseTemp.Booking_Unit__c, objCaseTemp);
              }
            }
            System.debug('mapBUIdToCase==' + mapBUIdToCase);
            
            Map<String, Booking_Unit__c> mapUnitNameToBU;
              
            mapUnitNameToBU = new Map<String, Booking_Unit__c>();
            
            for(Booking_Unit__c objBU : lstBUs ) {
              if( objBU.Unit_Name__c != '' ) {
                mapUnitNameToBU.put(objBU.Unit_Name__c, objBU);
                if( mapBUAttachment.get(objBU.Unit_Name__c).Body != null
                && String.isNotBlank( mapBUAttachment.get(objBU.Unit_Name__c).Name )) {
                  UploadMultipleDocController.MultipleDocRequest objDocRequest = 
                    new UploadMultipleDocController.MultipleDocRequest();
                  objDocRequest.category =  'Document';
                  objDocRequest.entityName = 'Damac Service Requests'; 
                  objDocRequest.fileDescription  =  'Snag Report';     
                  
                  intIncrementor++;
                  objDocRequest.fileId = objBU.Id + '-' + String.valueOf(System.currentTimeMillis()+intIncrementor)
                              + '.' + mapBUAttachment.get(objBu.Unit_Name__c).Name;
                  objDocRequest.fileName = objBU.Id + '-' + String.valueOf(System.currentTimeMillis() + intIncrementor) + 
                              '.'+mapBUAttachment.get(objBU.Unit_Name__c).Name;
                  objDocRequest.registrationId = objBU.Id;
                  objDocRequest.sourceFileName  = 'IPMS-' + objBU.Booking__r.Account__r.party_ID__C + '-' + 
                                  mapBUAttachment.get(objBU.Unit_Name__c).Name;
                  objDocRequest.sourceId  =  'IPMS-' + objBU.Booking__r.Account__r.party_ID__C + '-' + 
                                mapBUAttachment.get(objBU.Unit_Name__c).Name;
                  if( mapBUAttachment.get(objBU.Unit_Name__c).Body != null ){
                    blob objBlob = mapBUAttachment.get(objBU.Unit_Name__c).Body;
                    if(objBlob != null){
                      objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
                    }
                  }
                  //objDocRequest.base64Binary  =  crfAttachmentBody;
                  lstMultipleDocRequest.add(objDocRequest);
                }
                
                
                objBU.Snags_Reported__c = true;
                objBU.Run_Snag_Process__c = true;
                objBU.Snags_Report_Available__c = true;
                if(!objBU.Snag_Received__c) { 
                  objBU.Snag_Received__c = true;
                  blnSendEmail = true;
                }
                //mapIdToBUtoUpdate.put(objBU.Id, objBU);
                
                if( mapUnitNameDate != null && mapUnitNameDate.size() > 0 
                && mapUnitNameDate.containsKey( objBU.Unit_Name__c ) ) {
                  objBU.SRF_Date__c  = mapUnitNameDate.get( objBU.Unit_Name__c );
                }

                if( mapUnitNameStatus != null && mapUnitNameStatus.size() > 0 
                && mapUnitNameStatus.containsKey( objBU.Unit_Name__c ) ) {
                  objBU.SRF_Status__c  = mapUnitNameStatus.get( objBU.Unit_Name__c );
                }
                
                
                lstBUToUpdate.add(objBU);
                
                 /* if( objBU.cases__r.size() > 0 && objBU.cases__r[0].SR_Attachments__r.size() > 0 ) {
                  lstSRAttachmentToDelete.addAll(objCase.SR_Attachments__r);
                  objBU.cases__r[0].Snags_Reported__c = true;
                  objBU.cases__r[0].Run_Snag_Process__c = true;
                  lstCaseToUpdate.add(objBU.cases__r[0]);
                }*/
                
                /*if( objBU.Case_Attachments__r.size() > 0 ) {
                  setSR_Attachments.addAll(objBU.Case_Attachments__r);
                }*/
                
                if( mapBUIdToCase != null && mapBUIdToCase.size() > 0 && mapBUIdToCase.containsKey(objBU.id) 
                && mapBUIdToCase.get(objBU.Id).SR_Attachments__r.size() > 0){
                  //setSR_Attachments.addAll(mapBUIdToCase.get(objBU.Id).SR_Attachments__r);
                  lstSRAttachmentToDelete.addAll(mapBUIdToCase.get(objBU.Id).SR_Attachments__r);
                  objBU.cases__r[0].Snags_Reported__c = true;
                  objBU.cases__r[0].Run_Snag_Process__c = true;
                  lstCaseToUpdate.add(objBU.cases__r[0]);
                }
                
                SR_Attachments__c objAttach = new SR_Attachments__c();
                if( objBU.cases__r.size() > 0 ) {
                  objAttach.Case__c  = objBU.cases__r[0].Id;
                }
                objAttach.Booking_Unit__c = objBU.Id;
                objAttach.isValid__c = false;//true;
                objAttach.IsRequired__c = true;
                objAttach.Name = 'Snag Report';
                //objAttach.Attachment_URL__c = 'www.salesforce.com';
                objAttach.Need_Correction__c =  false;
                lstSRAttachment.add(objAttach);  
              }
            }
            if( lstMultipleDocRequest.Size() > 0 ) {
              UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();
              if( !Test.isRunningTest() ) { 
                MultipleDocData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocRequest);
              }else {
                
                List<UploadMultipleDocController.MultipleDocResponse> data = 
                  new List<UploadMultipleDocController.MultipleDocResponse>();
                UploadMultipleDocController.MultipleDocResponse objMultipleDocResponse = 
                  new UploadMultipleDocController.MultipleDocResponse();
                objMultipleDocResponse.PROC_STATUS = 'S';
                objMultipleDocResponse.url = 'www.google.com';
                data.add(objMultipleDocResponse);
                
                MultipleDocData.Data = data;
              }
              if( MultipleDocData != null && MultipleDocData.status == 'Exception' ) {
                for( Booking_Unit__c objBU : lstBUs) {
                  listErrorLogs.add(errorLogger(MultipleDocData.message,'',objBU.Id));
                }
              }                                   
              if(MultipleDocData != null && (MultipleDocData.Data == null || MultipleDocData.Data.Size() == 0) ) {
                for( Booking_Unit__c objBU : lstBUs) {
                  listErrorLogs.add(errorLogger(MultipleDocData.message,'',objBU.Id));
                }                           
              }

              if( MultipleDocData != null ) {
                for( Integer objInt = 0; objInt < lstSRAttachment.Size(); objInt++ ) {
                  if( MultipleDocData.Data != null ) {
                    UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[objInt];
                    if( objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url) ) {
                      lstSRAttachment[objInt].Attachment_URL__c = objData.url;
                    }
                  }
                }
                system.debug('lstSRAttachment : ' +lstSRAttachment );
                if( lstSRAttachment.Size() > 0 ){
                  insert lstSRAttachment;
                }  
              }
            }
            
            /*if ( setSR_Attachments != null && setSR_Attachments.size() > 0 ) {
              List<SR_Attachments__c> lstSRAttachmentToDelete = new List<SR_Attachments__c>(setSR_Attachments);
              if ( lstSRAttachmentToDelete != null && lstSRAttachmentToDelete.size() > 0 ) {
                Delete lstSRAttachmentToDelete;
              }
            }*/
            if ( lstSRAttachmentToDelete != null && lstSRAttachmentToDelete.size() > 0 ) {
              Delete lstSRAttachmentToDelete;
            }

            set<Id> setBUId = new set<Id>();
            if( lstToUpdateSnagEmail != null && lstToUpdateSnagEmail.size() > 0 
              && blnSendEmail ) {
              String UnitName;
              Booking_Unit__c objBU;
              for (EmailMessage objEmailMsg: lstToUpdateSnagEmail ) {
                if( objEmailMsg.Subject.toLowerCase().contains('-') ) {
                  List<String> lstSubjectSplit = objEmailMsg.Subject.split('-');
                  if( lstSubjectSplit[3] != null ) {
                    lstSubjectSplit[3] = lstSubjectSplit[3].replace('[','');
                    lstSubjectSplit[3] = lstSubjectSplit[3].replace(']','');
                    UnitName = lstSubjectSplit[3].trim();
                  }
                  if (!string.isBlank(UnitName)) {
                    objBU = mapUnitNameToBU.get(UnitName);                                 
                  }
                  if(objBU != null && objBU.SNAG_Report_Sent__c == false) {
                    setBUId.add(objBU.Id);
                    lstSMSHistory.add(
                      new SMS_History__c(
                        Customer__c = objBU.Booking__r.Account__c
                        , Is_SMS_Sent__c = false
                        , Phone_Number__c = objBU.Booking__r.Account__r.Mobile_Phone_Encrypt__pc
                        , Message__c = Label.SNAG_Report_SMS_Content)
                    );
                    if (objBU.Booking__r.Account__r.Email__pc != null) {
                      Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                      String mailMessage;
                      if ( owea.size() > 0 ) {
                        message.setOrgWideEmailAddressId(owea.get(0).Id);
                      }
                      mailMessage = SNAGReportTemplate.Body;
                      if (objBU.Booking__r.Account__r.Name != null) {
                        mailMessage = mailMessage.replace('{!Case.Account}',
                                objBU.Booking__r.Account__r.Name);
                      } else {
                        mailMessage = mailMessage.replace('{!Case.Account}', '');
                      }
                      mailMessage = mailMessage.replace('{!Case.Unit_Name__c}', UnitName);
                      if (objBU.Property_Name_Inventory__c != null) {
                        mailMessage = mailMessage.replace('{!Case.Project_Name__c}'
                              , objBu.Property_Name_Inventory__c);
                      } else {
                        mailMessage = mailMessage.replace('{!Case.Project_Name__c}', '');
                      }
                      message.subject = SNAGReportTemplate.Subject.replace('{!Case.Unit_Name__c}', UnitName);
                      message.setPlainTextBody(mailMessage);
                      message.setSaveAsActivity(true);
                      message.setWhatId(objBU.Booking__r.Account__c);
                      message.toAddresses = new String[] {objBU.Booking__r.Account__r.Email__pc};
                      messages.add(message);
                    }
                  }
                }
              }
            }// end of logic to send mail
            if ( blnSendEmail && messages != null && messages.size() >0 ) {
              Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);//Sending emails
            }
            list<Booking_Unit__c> lstMailSentBU = new list<Booking_Unit__c>();
            
            if(lstBUToUpdate != null && lstBUToUpdate.size() > 0){
              
              for (Booking_Unit__c updateBU : lstBUToUpdate) {
                if (setBuId.contains(updateBU.Id)) {
                  updateBU.SNAG_Report_Sent__c = true;
                  lstMailSentBU.add(updateBU);
                }
              }
              update lstBUToUpdate;
            }   
            
            /*for(Id buId :  setBUId) {
              if(mapIdToBUtoUpdate.containsKey(buId)){
                mapIdToBUtoUpdate.get(buId).SNAG_Report_Sent__c = true;
              }
            }
            
            if(mapIdToBUtoUpdate.size() > 0) {
              update mapIdToBUtoUpdate.getAllValues();
            }
            */
            if ( blnSendEmail && lstMailSentBU != null) {
              update lstMailSentBU;
            }
            
            
            if(listErrorLogs.size() > 0) {
              insert listErrorLogs;
            }
            
          }
          
          if( lstToUpdateSnagEmail != null && lstToUpdateSnagEmail.SIZE() > 0 ) {
            update lstToUpdateSnagEmail;
          }
        }
        
            }
        }
    }

    private static Error_Log__c errorLogger(string strErrorMessage, string strCaseID,string strBookingUnitID){
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        if(String.isNotBlank(strCaseID) && strCaseID.startsWith('500')){
            objError.Case__c = strCaseID;
        }
        if(String.isNotBlank(strBookingUnitID)){
            objError.Booking_unit__c = strBookingUnitID;
        }
        //insert objError;
        return objError;
    }   
    
    public void finish(Database.BatchableContext BC) {
        system.debug('&&&&lstSMSHistory'+lstSMSHistory);
        /*if (lstSMSHistory != null && lstSMSHistory.size() >0) {
            insert lstSMSHistory;
            System.debug('===lstSMSHistory===' + lstSMSHistory);
            Database.executeBatch(new CustomerNotificationSendSMSBatch(lstSMSHistory),90);
        }*/
    }
}