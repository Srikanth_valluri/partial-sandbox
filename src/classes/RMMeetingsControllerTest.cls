@isTest
private class RMMeetingsControllerTest {
    
    static testMethod void getTestResults() {
                
        User currentUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        
        Id recordTypeId = Schema.sObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        
        Inquiry__c testInquiry = new Inquiry__c();
        testInquiry.First_Name__c = 'test';
        testInquiry.Meeting_Due_Date__c = System.today();
        testInquiry.RecordTypeId = recordTypeId;
        testInquiry.Is_Meeting_Confirmed__c = true;
        insert testInquiry;
        
        Task newTask = new Task();
        newTask.Activity_Type_3__c = 'Meeting at Office';
        newTask.Activity_Outcome__c = NULL;
        newTask.OwnerId = UserInfo.getUserId();
        newTask.Start_Date__c = System.today();
        newTask.End_Date__c = System.today()+1;
        newTask.Task_Due_Date__c = System.today()+2;
        newTask.RM_Comments__c = 'Test Comments';
        //newTask.What.Type = 'Inquiry__c';
        newTask.WhatId = testInquiry.Id;
        insert newTask;
        List<Task> newTaskList = new List<Task>();
        newTaskList.add(newTask);
        Account acc = new Account ();
        acc.Name = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '767676';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        insert acc;
        contact con = new Contact();
        con.LastName= 'TestCon';
        con.AccountId = acc.id;
        insert con;
        ApexPages.currentPage().getParameters().put('eventId', newTask.Id);
        ApexPages.currentPage().getParameters().put('selectedFId', '1');
        ApexPages.currentPage().getParameters().put('inqId', testInquiry.Id);
        ApexPages.currentPage().getParameters().put('userId', UserInfo.getUserId());
        ApexPages.currentPage().getParameters().put('location', 'DUBAI');
        RMMeetingsController.searchInquiryName('test');
        RMMeetingsController.searchInquiryNameForNeedAnalysis('test');
        RMMeetingsController cls = new RMMeetingsController();
        cls.inq.Interested_Location__c = 'DUBAI, London';
        cls.inqNumber = testInquiry.Id;
        cls.getLocation();
        cls.getBHKOptions ();
        cls.getDevelopement();
        cls.searchInquiry();
        cls.updateStartTime();
        RMMeetingsController.getCorporateAgents(acc.id);
        cls.updateStartTime1();
        cls.UpdateEndTime();
        cls.addInquiryFamily();
        cls.deleteSelectedFamilyMemeber();
        cls.createFollowUpEvent();
        newTask.HOS_Comments__c = 'test';
        newTask.DOS_Comments__c = 'test';
        cls.updateHOSComments ();
        
        RMMeetingsController.getAgencyDetails ('tst');
        delete newTaskList;
        ApexPages.currentPage().getParameters().put('eventId', '');
        cls.searchInquiry();
    }
}