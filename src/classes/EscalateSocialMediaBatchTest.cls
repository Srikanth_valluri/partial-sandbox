@isTest
public with sharing class EscalateSocialMediaBatchTest {
    static testMethod void testScenario1() {
    Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().
                                            get('SocialMedia').getRecordTypeId();
                
        Id complaintRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Complaint').getRecordTypeId();                         
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        
        Group objGroup = new Group(Name='test group', type='Queue');
        insert objGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
        QueuesObject objQueue = new QueueSObject(QueueID = objGroup.id, SobjectType = 'Case');
        insert objQueue;
        }
        
        
        
        Case objCase = new Case();
        objCase.Account = acc;
        objCase.OwnerId = objGroup.Id;
        objCase.RecordTypeId = complaintRecTypeId ;
        objCase.Status = 'Submitted';
        objCase.Approval_Status__c = 'Approved';
        objCase.Type = 'Question';  
        objCase.Account_Email__c = 'test@gmail.com';
        objCase.CC_Escl_Date__c = system.now() -1; 
        objCase.CRE_Escl_Date__c = system.now() -1; 
        objCase.Mgr_Escl_Date__c = system.now() -1;
        insert objCase;
        Database.executeBatch(new EscalateSocialMediaBatch(),200);
    }
    
    static testMethod void testScenario2() {
    Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().
                                            get('SocialMedia').getRecordTypeId();
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        Case objCase = new Case();
        objCase.Account = acc;
        objCase.RecordTypeId = caseRecTypeId;
        objCase.Status = 'Submitted';
        objCase.Approval_Status__c = 'Approved';
        objCase.Type = 'Question';  
        objCase.Account_Email__c = 'test@gmail.com';
        objCase.CC_Escl_Date__c = system.now() -1; 
        objCase.CRE_Escl_Date__c = system.now() -1; 
        objCase.Mgr_Escl_Date__c = system.now() -1;
        insert objCase;
        Database.executeBatch(new EscalateSocialMediaBatch(),200);
    }

    static testMethod void testScenario3() {
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().
                                                get('Complaint').getRecordTypeId();
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        Case objCase = new Case();
        objCase.Account = acc;
        objCase.RecordTypeId = caseRecTypeId;
        objCase.Status = 'Submitted';
        objCase.Approval_Status__c = 'Approved';
        objCase.Type = 'Question';  
        objCase.Account_Email__c = 'test@gmail.com';
        objCase.OwnerId = Id.valueOf(Label.Happiness_Queue_Id);
        insert objCase;
        Test.setCreatedDate(objCase.Id, System.today().addDays( -1 ));  
        Database.executeBatch(new EscalateSocialMediaBatch(),200);
    }
    static testMethod void testScenario4() {
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().
                                                get('SocialMedia').getRecordTypeId();
            Account acc = new Account();
            acc.Name = 'Test';
            acc.Address_Line_1__c = 'XYZ';
            acc.Country__c = 'Canada';
            acc.City__c = 'Barcelona';
            acc.State__c = 'lmn';
            acc.Zip_Postal_Code__c = '111';
            insert acc;
            
            Case objCase = new Case();
            objCase.Account = acc;
            objCase.RecordTypeId = caseRecTypeId;
            objCase.Status = 'Submitted';
            objCase.Approval_Status__c = 'Approved';
            objCase.Type = 'Question';  
            objCase.Account_Email__c = 'test@gmail.com';
            //objCase.CC_Escl_Date__c = system.now() -1; 
            //objCase.CRE_Escl_Date__c = system.now() -1; 
            objCase.Mgr_Escl_Date__c = system.now() -1;
            insert objCase;
            Database.executeBatch(new EscalateSocialMediaBatch(),200);
        }

    static testMethod void testComplaintCase() {
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().
                                                get('Complaint').getRecordTypeId();
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        Case objCase = new Case();
        objCase.Account = acc;
        objCase.RecordTypeId = caseRecTypeId;
        objCase.Status = 'Submitted';
        objCase.Approval_Status__c = 'Approved';
        objCase.Type = 'Question';  
        objCase.Account_Email__c = 'test@gmail.com';
        objCase.OwnerId = Id.valueOf(Label.Happiness_Queue_Id);
        insert objCase;
        Test.setCreatedDate(objCase.Id, System.now() -1);  
        Database.executeBatch(new EscalateSocialMediaBatch(),200);
    }
    
}