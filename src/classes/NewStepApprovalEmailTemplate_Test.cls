@isTest
public class NewStepApprovalEmailTemplate_Test {
    
    @testSetup
    static void setupData(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;
        
        List<Step_Status_Transition__c> StepTransitionList = new List<Step_Status_Transition__c>();
        Step_Status_Transition__c statusTrans1 = 
            new Step_Status_Transition__c(Step_Type__c = 'Manager Approval', Step_No__c = 5, From_Step_Status__c = 'Under Manager Review',
                                          Is_Closed__c = True, Service_Request_Type__c = 'Deal', To_Step_Status__c = 'Manager Approved', 
                                          SR_Internal_Status__c = 'MANAGER_APPROVED', SR_External_Status__c = 'MANAGER_APPROVED');
        StepTransitionList.add(statusTrans1);
        
        Step_Status_Transition__c statusTrans2 = 
            new Step_Status_Transition__c(Step_Type__c = 'Manager Approval', Step_No__c = 5, From_Step_Status__c = 'Under Manager Review',
                                          Is_Closed__c = True, Service_Request_Type__c = 'Deal', To_Step_Status__c = 'Rejected', 
                                          SR_Internal_Status__c = 'REJECTED', SR_External_Status__c = 'REJECTED');
        StepTransitionList.add(statusTrans2);
        
        Step_Status_Transition__c statusTrans3 = 
            new Step_Status_Transition__c(Step_Type__c = 'Manager Approval', Step_No__c = 5, From_Step_Status__c = 'Under Manager Review',
                                          Is_Closed__c = True, Service_Request_Type__c = 'Deal', To_Step_Status__c = 'Requested for Info', 
                                          SR_Internal_Status__c = 'REQUESTED_FOR_INFO', SR_External_Status__c = 'REQUESTED_FOR_INFO');
        StepTransitionList.add(statusTrans3);
        insert StepTransitionList;
        
        List<New_Step__c> NewStepList = new List<New_Step__c>();
        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.id;
        nstp1.Step_No__c = 5;
        nstp1.Step_Status__c = 'Under Manager Review';
        nstp1.Step_Type__c = 'Manager Approval';
        nstp1.Manager_Email__c = 'xxx@yy.com';
        nstp1.HOS_Email__c = 'xxx@yy.com';
        NewStepList.add(nstp1);            
        insert NewStepList;
    }
    
    @isTest
    static void ManagerStepApproval_Test(){
        New_Step__c stp = [SELECT Name, Service_Request__c FROM New_Step__c Limit 1];
        NewStepApprovalEmailTemplateController stpApproval = new NewStepApprovalEmailTemplateController();
        New_Step__c step2 = stpApproval.step;
        User managerUser = stpApproval.managerUser;
        stpApproval.ManagerType = 'Manager';
        stpApproval.StepId = stp.Id;
        New_Step__c step3 = stpApproval.step;
        managerUser = stpApproval.managerUser;
    }
    
    @isTest
    static void HOSStepApproval_Test(){
        New_Step__c stp = [SELECT Name, Service_Request__c FROM New_Step__c Limit 1];
        NewStepApprovalEmailTemplateController stpApproval = new NewStepApprovalEmailTemplateController();
        User managerUser = stpApproval.managerUser;
        stpApproval.ManagerType = 'HOS';
        stpApproval.StepId = stp.Id;
        New_Step__c step2 = stpApproval.step;
    }
    
    @isTest
    static void GMStepApproval_Test(){
        New_Step__c stp = [SELECT Name, Service_Request__c FROM New_Step__c Limit 1];
        NewStepApprovalEmailTemplateController stpApproval = new NewStepApprovalEmailTemplateController();
        User managerUser = stpApproval.managerUser;
        stpApproval.ManagerType = 'GM';
        stpApproval.StepId = stp.Id;
        New_Step__c step2 = stpApproval.step;
    }
    
}