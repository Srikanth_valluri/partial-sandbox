@isTest
private class Damac_generateBasicSRDocumentsTest {

     @isTest static void test_method_one() {
        Account agency1 = InitialiseTestData.getCorporateAccount('Agency129');
        insert agency1;
        
        List<NSIBPM__SR_Template__c> SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
        List<NSIBPM__SR_Template__c> SRTemplateListNew = new List<NSIBPM__SR_Template__c>();
        for(NSIBPM__SR_Template__c tempObj :SRTemplateList){
            tempObj.NSIBPM__SR_RecordType_API_Name__c = 'Agent_Update';
            SRTemplateListNew.add(tempObj);
        }
        update SRTemplateListNew;
        List<NSIBPM__Service_Request__c> SRList  = InitialiseTestData.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                    new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Update'),
                                                    NSIBPM__SR_Template__c = SRTemplateList[0].Id)});

        NSIBPM__Service_Request__c serviceRequest = SRList[0];
        serviceRequest.NSIBPM__Customer__c = agency1.id;

        
        upsert serviceRequest;
        
        Damac_generateBasicSRDocuments.generateDocuments (serviceRequest.id);
     }
}