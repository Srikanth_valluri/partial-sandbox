@istest
public class InquiryServiceTest{
	
	private static List<Campaign__c> insertCampaignRecordsList = new List<Campaign__c>();
	private static List<NSIBPM__Service_Request__c> srRecordsList = new List<NSIBPM__Service_Request__c>();
	private static List<Booking__c> bookingRecordsList = new List<Booking__c>();
	private static List<Booking_Unit__c> buRecordsList = new List<Booking_Unit__c>();
	private static void init(){
		insertCampaignRecordsList = 
        	TestDataFactory.createCampaignRecords(
        		new List<Campaign__c> { new Campaign__c(), 
        								new Campaign__c(), 
										new Campaign__c(), 
										new Campaign__c(), 
										new Campaign__c(),
										new Campaign__c() });
        if(insertCampaignRecordsList != null && insertCampaignRecordsList.size() > 0){
        	insertCampaignRecordsList[0].Parent_Campaign__c = insertCampaignRecordsList[1].Id;
        	insertCampaignRecordsList[1].Parent_Campaign__c = insertCampaignRecordsList[2].Id;
        	insertCampaignRecordsList[2].Parent_Campaign__c = insertCampaignRecordsList[3].Id;
        	insertCampaignRecordsList[3].Parent_Campaign__c = insertCampaignRecordsList[4].Id;
        	insertCampaignRecordsList[4].Parent_Campaign__c = insertCampaignRecordsList[5].Id; 
        	
        	update insertCampaignRecordsList; 
        }
        system.debug('#### Inserted Campaign Records List = '+insertCampaignRecordsList);
        srRecordsList = 
        	TestDataFactory.createTestServiceRequestRecords(
        		new List<NSIBPM__Service_Request__c> {new NSIBPM__Service_Request__c(Doc_ok__c = true, DP_ok__c = true), 
													  new NSIBPM__Service_Request__c(Doc_ok__c = true, DP_ok__c = true)});
		bookingRecordsList = 
        	TestDataFactory.createBookingRecords(
        		new List<Booking__c> {new Booking__c(Deal_SR__c = srRecordsList[0].Id), 
									  new Booking__c(Deal_SR__c = srRecordsList[1].Id)});
		buRecordsList = 
        	TestDataFactory.createBookingUnitRecords(
        		new List<Booking_Unit__c> {new Booking_Unit__c(Booking__c = bookingRecordsList[0].Id), 
        								   new Booking_Unit__c(Booking__c = bookingRecordsList[0].Id),
        								   new Booking_Unit__c(Booking__c = bookingRecordsList[1].Id),
        								   new Booking_Unit__c(Booking__c = bookingRecordsList[1].Id),
        								   new Booking_Unit__c(Booking__c = bookingRecordsList[1].Id)});	
	}
	
	
	static testmethod void TestCampaignUserMethod(){
		init();
		InquiryService cls = new InquiryService();
		
		User pcUser = InitialiseTestData.getPropertyConsultantUsers('pc2@damactest.com');
        pcUser.Extension = '0099';
        pcUser.Languages_Known__c = 'English';        
        insert pcUser ;
        
        User pcUser1 = InitialiseTestData.getPropertyConsultantUsers('pc3@damactest.com');
        pcUser1.Extension = '0098';
        pcUser1.Languages_Known__c = 'English';        
        insert pcUser1 ;
		
		List<Assigned_PC__c> assignedPcList = new List<Assigned_PC__c>(); 
        assignedPcList.add(InitialiseTestData.assignPCToCampaign(pcUser.Id, insertCampaignRecordsList[0].Id));
        assignedPcList.add(InitialiseTestData.assignPCToCampaign(pcUser1.Id, insertCampaignRecordsList[0].Id));
        insert assignedPcList;
        cls.getAllCampaignUser(new Set<Id>{insertCampaignRecordsList[0].Id});	
        
        delete assignedPcList;
        assignedPcList = new List<Assigned_PC__c>(); 
        assignedPcList.add(InitialiseTestData.assignPCToCampaign(pcUser.Id, insertCampaignRecordsList[1].Id));
        assignedPcList.add(InitialiseTestData.assignPCToCampaign(pcUser1.Id, insertCampaignRecordsList[1].Id));
        insert assignedPcList;
        cls.getAllCampaignUser(new Set<Id>{insertCampaignRecordsList[0].Id});
        
        delete assignedPcList;
        assignedPcList = new List<Assigned_PC__c>(); 
        assignedPcList.add(InitialiseTestData.assignPCToCampaign(pcUser.Id, insertCampaignRecordsList[2].Id));
        assignedPcList.add(InitialiseTestData.assignPCToCampaign(pcUser1.Id, insertCampaignRecordsList[2].Id));
        insert assignedPcList;
        cls.getAllCampaignUser(new Set<Id>{insertCampaignRecordsList[0].Id});
        
        delete assignedPcList;
        assignedPcList = new List<Assigned_PC__c>(); 
        assignedPcList.add(InitialiseTestData.assignPCToCampaign(pcUser.Id, insertCampaignRecordsList[3].Id));
        assignedPcList.add(InitialiseTestData.assignPCToCampaign(pcUser1.Id, insertCampaignRecordsList[3].Id));
        insert assignedPcList;
        cls.getAllCampaignUser(new Set<Id>{insertCampaignRecordsList[0].Id});
        
        delete assignedPcList;
        assignedPcList = new List<Assigned_PC__c>(); 
        assignedPcList.add(InitialiseTestData.assignPCToCampaign(pcUser.Id, insertCampaignRecordsList[4].Id));
        assignedPcList.add(InitialiseTestData.assignPCToCampaign(pcUser1.Id, insertCampaignRecordsList[4].Id));
        insert assignedPcList;
        cls.getAllCampaignUser(new Set<Id>{insertCampaignRecordsList[0].Id});
        
        delete assignedPcList;
        assignedPcList = new List<Assigned_PC__c>(); 
        assignedPcList.add(InitialiseTestData.assignPCToCampaign(pcUser.Id, insertCampaignRecordsList[5].Id));
        assignedPcList.add(InitialiseTestData.assignPCToCampaign(pcUser1.Id, insertCampaignRecordsList[5].Id));
        insert assignedPcList;
        cls.getAllCampaignUser(new Set<Id>{insertCampaignRecordsList[0].Id});
	}
	
	/* Putting see all test data as true to cover object history query used in the code.
	   The only way to cover it by fetching the history records existing in the system. */
	@isTest(SeeAllData=true)
	static void TestInquiryHistoryMethod(){
		InquiryService cls = new InquiryService(); 
		Set<Id> inquiryIdSet = new Set<Id>();
		for(Inquiry__History thisHistory : [SELECT Field, Id, NewValue, OldValue, ParentID 
											FROM Inquiry__History 
											WHERE Field = 'Owner' LIMIT 10]){
			inquiryIdSet.add(thisHistory.ParentID);		
		}
        cls.getNurturingInquiryDetails(inquiryIdSet);	
	}
	
    static testmethod void InquiryService_methods(){
    	init();
    	
    	InquiryService cls = new InquiryService();
    	
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('pc2@damactest.com');
        pcUser.Extension = '0099';
        pcUser.Languages_Known__c = 'English';        
        insert pcUser ;
        
        User pcUser1 = InitialiseTestData.getPropertyConsultantUsers('pc3@damactest.com');
        pcUser1.Extension = '0098';
        pcUser1.Languages_Known__c = 'English';        
        insert pcUser1 ;
        
        
        Id RSRecordTypeId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId();
        Virtual_Number__c v = new Virtual_Number__c();
        v.Active__c = true;
        v.name = '123';
        v.End_Date__c = System.today().addDays(60);
        v.Start_Date__c = System.today().addDays(-60);
        insert v;
        
        JO_Campaign_Virtual_Number__c vn = new JO_Campaign_Virtual_Number__c();
        vn.Related_Campaign__c = insertCampaignRecordsList[0].id;
        vn.Related_Virtual_Number__c = v.id;
        insert vn;
        
        //
        Account a = new Account();
        a.Name = 'Test Account';        
        insert a;
        
        Id PerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account a1 = new Account();
        a1.FirstName = 'Test Account';
        a1.LastName = 'Acc';
        a1.Phone_Key__c = 'ABC';
        a1.Email_Key__c = 'TEST@TEST.COM';
        a1.recordtypeid = PerRecordTypeId;
        insert a1;
        
        NSIBPM__SR_Template__c SRTemplate = new NSIBPM__SR_Template__c();
        srtemplate.NSIBPM__SR_RecordType_API_Name__c = 'Deal';
        insert SRTemplate;
        
        List<Deal_Team__c> dealTeamList = 
        	new List<Deal_Team__c>{
        			new deal_team__c(Associated_PC__c = pcuser.Id, 
        							 Associated_Deal__c  = srRecordsList[0].Id),
        			new deal_team__c(Associated_PC__c = pcuser.Id, 
        							 Associated_Deal__c  = srRecordsList[1].Id)}; 
        insert dealTeamList;
        
        List<Inquiry__c> inquiryList = 
        	TestDataFactory.createInquiryRecords(
        		new List<Inquiry__c> {new Inquiry__c(Email__c = 'test@test.com', 
        											 Mobile_CountryCode__c = 'United Arab Emirates: 00971', 
        											 Mobile_Phone_Encrypt__c = '557030780',
        											 Campaign__c = insertCampaignRecordsList[0].Id), 
        							  new Inquiry__c(Email__c = 'test@test.com', 
        							  				 Mobile_CountryCode__c = 'United Arab Emirates: 00971', 
        							  				 Mobile_Phone_Encrypt__c = '557030781',
        							  				 Campaign__c = insertCampaignRecordsList[0].Id),
        							  new Inquiry__c(Mobile_CountryCode__c = 'United Arab Emirates: 00971', 
        							  				 Mobile_Phone_Encrypt__c = '557030789',
        							  				 Campaign__c = insertCampaignRecordsList[0].Id),
        							  new Inquiry__c(Mobile_CountryCode__c = 'United Arab Emirates: 00971', 
        							  				 Mobile_Phone_Encrypt__c = '557030789',
        							  				 Campaign__c = insertCampaignRecordsList[0].Id)});	
        
        Property__c prop = new Property__c();
        prop.Active_Property__c = true;
        prop.Property_ID__c = 123;
        insert prop;
        
        List<Assigned_Property_User__c> apropList = 
        	new List<Assigned_Property_User__c>{
        		new Assigned_Property_User__c(Assigned_user__c = pcuser.Id, Associated_Property__c = prop.Id), 
        		new Assigned_Property_User__c(Assigned_user__c = pcuser1.Id, Associated_Property__c = prop.Id)};
        insert apropList;        

        InquiryService.getUserDetailsByProfile(pcUser.profileid);        
        
        map<id,list<id>> parentSortedEligibleUsersMap = new map<id,list<id>>();
        list<id> subList = new list<id>();
        sublist.add(pcUser.id);
        parentSortedEligibleUsersMap.put(pcuser.id,sublist);
        cls.getPcPerformanceMap(parentSortedEligibleUsersMap);
        
        //
        Map<Id, Integer> pcWithMeetingCount = new Map<Id, Integer>();
        pcWithMeetingCount.put(pcuser.id,10);
        cls.getPcMeetingBandMap(10,pcWithMeetingCount);
        
        //
        cls.upgradeScore('');
        cls.upgradeScore('Hot');
        cls.upgradeScore('Cold');
        cls.upgradeScore('Warm');
        
        //  
        cls.downgradeScore('');      
        cls.downgradeScore('Hot');
        cls.downgradeScore('Cold');
        cls.downgradeScore('Warm');
        
        Set<String> phkey = new Set<String>{(inquiryList[0].First_name__c+'__'+inquiryList[0].Last_name__c+'__'+inquiryList[0].Mobile_CountryCode__c+'__00971'+inquiryList[0].Mobile_Phone_Encrypt__c).toUpperCase(), 
        								    (inquiryList[1].First_name__c+'__'+inquiryList[1].Last_name__c+'__'+inquiryList[1].Mobile_CountryCode__c+'__00971'+inquiryList[1].Mobile_Phone_Encrypt__c).toUpperCase(),
        								    (inquiryList[2].First_name__c+'__'+inquiryList[2].Last_name__c+'__'+inquiryList[2].Mobile_CountryCode__c+'__00971'+inquiryList[2].Mobile_Phone_Encrypt__c).toUpperCase(),
        								    (inquiryList[3].First_name__c+'__'+inquiryList[3].Last_name__c+'__'+inquiryList[3].Mobile_CountryCode__c+'__00971'+inquiryList[3].Mobile_Phone_Encrypt__c).toUpperCase()};
        Set<String> emkey = new Set<String>{(inquiryList[0].First_name__c+'__'+inquiryList[0].Last_name__c+'__'+inquiryList[0].Email__c).toUpperCase(), 
        								    (inquiryList[1].First_name__c+'__'+inquiryList[1].Last_name__c+'__'+inquiryList[1].Email__c).toUpperCase(),
        								    (inquiryList[2].First_name__c+'__'+inquiryList[2].Last_name__c+'__'+inquiryList[2].Email__c).toUpperCase(),
        								    (inquiryList[3].First_name__c+'__'+inquiryList[3].Last_name__c+'__'+inquiryList[3].Email__c).toUpperCase()}; 
        Set<String> mobilekey = new Set<String>{
                inquiryList[0].Mobile_Phone__c,
                inquiryList[1].Mobile_Phone__c,
                inquiryList[2].Mobile_Phone__c,
                inquiryList[3].Mobile_Phone__c
        };
        cls.getMatchingAccount(new Set<String>{a1.Phone_Key__c}, new Set<String>{a1.Email_Key__c});
                
        //
        cls.getMatchingInquiry(phKey, emKey, new Set<Id>{insertCampaignRecordsList[0].Id, 
        												 insertCampaignRecordsList[1].Id, 
        												 insertCampaignRecordsList[2].Id, 
        												 insertCampaignRecordsList[3].Id, 
        												 insertCampaignRecordsList[4].Id, 
        												 insertCampaignRecordsList[5].Id},mobilekey);
        //
        list<inquiry__c> elgInq = new list<inquiry__c>();
        elgInq.add(inquiryList[0]);
        elgInq.add(inquiryList[1]);
        cls.getMatchingInquiryRecord(inquiryList[0], elgInq);
        
        //
        set<id> aProps = new set<id>();
        aProps.add(prop.id);
        cls.getAllPropertyUser(aProps);
        
        //
        set<string> phkey1 = new set<string>();
        set<string> emkey1 = new set<string>();
        phKey1.add(a1.Phone_Key__c );
        emkey1.add(a1.Email_Key__c );
        cls.possibleMatchesOfInquiry(emkey1,phKey1);
    }
    
    static testmethod void testComparableMethod(){
    	init();
    	List<Inquiry__c> inquiryList = 
        	TestDataFactory.createInquiryRecords(
        		new List<Inquiry__c> {new Inquiry__c(Email__c = 'test@test.com', 
        											 Mobile_CountryCode__c = 'United Arab Emirates: 00971', 
        											 Mobile_Phone_Encrypt__c = '557030780',
        											 Campaign__c = insertCampaignRecordsList[0].Id), 
        							  new Inquiry__c(Email__c = 'test@test.com', 
        							  				 Mobile_CountryCode__c = 'United Arab Emirates: 00971', 
        							  				 Mobile_Phone_Encrypt__c = '557030781',
        							  				 Campaign__c = insertCampaignRecordsList[0].Id),
        							  new Inquiry__c(Mobile_CountryCode__c = 'United Arab Emirates: 00971', 
        							  				 Mobile_Phone_Encrypt__c = '557030789',
        							  				 Campaign__c = insertCampaignRecordsList[0].Id),
        							  new Inquiry__c(Mobile_CountryCode__c = 'United Arab Emirates: 00971', 
        							  				 Mobile_Phone_Encrypt__c = '557030789',
        							  				 Campaign__c = insertCampaignRecordsList[0].Id)});
    	List<InquiryService.UserLoadWrapper> compList = 
        	new List<InquiryService.UserLoadWrapper>{new InquiryService.UserLoadWrapper(inquiryList[0].Id, 1, 1, 1, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[1].Id, 2, 1, 1, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[2].Id, 3, 1, 1, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[3].Id, 4, 1, 1, 1, 1)};
        compList.sort();
        compList = 
        	new List<InquiryService.UserLoadWrapper>{new InquiryService.UserLoadWrapper(inquiryList[0].Id, 1, 11, 1, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[1].Id, 1, 12, 1, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[2].Id, 1, 13, 1, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[3].Id, 1, 14, 1, 1, 1)};
        compList.sort();
        compList = 
        	new List<InquiryService.UserLoadWrapper>{new InquiryService.UserLoadWrapper(inquiryList[0].Id, 1, 1, 11, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[1].Id, 1, 1, 12, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[2].Id, 1, 1, 13, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[3].Id, 1, 1, 14, 1, 1)};
        compList.sort();
        compList = 
        	new List<InquiryService.UserLoadWrapper>{new InquiryService.UserLoadWrapper(inquiryList[0].Id, 1, 1, 1, 11, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[1].Id, 1, 1, 1, 12, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[2].Id, 1, 1, 1, 13, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[3].Id, 1, 1, 1, 14, 1)};
        compList.sort();
        compList = 
        	new List<InquiryService.UserLoadWrapper>{new InquiryService.UserLoadWrapper(inquiryList[0].Id, 1, 1, 1, 1, 11),
        											 new InquiryService.UserLoadWrapper(inquiryList[1].Id, 1, 1, 1, 1, 12),
        											 new InquiryService.UserLoadWrapper(inquiryList[2].Id, 1, 1, 1, 1, 13),
        											 new InquiryService.UserLoadWrapper(inquiryList[3].Id, 1, 1, 1, 1, 14)};
        compList.sort();	
        
        compList = 
        	new List<InquiryService.UserLoadWrapper>{new InquiryService.UserLoadWrapper(inquiryList[0].Id, 14, 1, 1, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[1].Id, 13, 1, 1, 2, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[2].Id, 12, 1, 1, 2, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[3].Id, 11, 1, 1, 2, 1)};
        compList.sort();
        compList = 
        	new List<InquiryService.UserLoadWrapper>{new InquiryService.UserLoadWrapper(inquiryList[0].Id, 1, 14, 1, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[1].Id, 1, 13, 1, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[2].Id, 1, 12, 1, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[3].Id, 1, 11, 1, 1, 1)};
        compList.sort();
        compList = 
        	new List<InquiryService.UserLoadWrapper>{new InquiryService.UserLoadWrapper(inquiryList[0].Id, 1, 1, 14, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[1].Id, 1, 1, 13, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[2].Id, 1, 1, 12, 1, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[3].Id, 1, 1, 11, 1, 1)};
        compList.sort();
        compList = 
        	new List<InquiryService.UserLoadWrapper>{new InquiryService.UserLoadWrapper(inquiryList[0].Id, 1, 1, 1, 14, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[1].Id, 1, 1, 1, 13, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[2].Id, 1, 1, 1, 12, 1),
        											 new InquiryService.UserLoadWrapper(inquiryList[3].Id, 1, 1, 1, 11, 1)};
        compList.sort();
        compList = 
        	new List<InquiryService.UserLoadWrapper>{new InquiryService.UserLoadWrapper(inquiryList[0].Id, 1, 1, 1, 1, 14),
        											 new InquiryService.UserLoadWrapper(inquiryList[1].Id, 1, 1, 1, 1, 13),
        											 new InquiryService.UserLoadWrapper(inquiryList[2].Id, 1, 1, 1, 1, 12),
        											 new InquiryService.UserLoadWrapper(inquiryList[3].Id, 1, 1, 1, 1, 11)};
        compList.sort();	
    }
}// End of test class.