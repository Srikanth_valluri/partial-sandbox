global class HandoverCheckEligibilityBatchScheduler implements Schedulable {

  public HandoverCheckEligibilityBatchScheduler() {}

  public HandoverCheckEligibilityBatchScheduler (String jobName) {
    System.schedule(jobName, '0 0 4 * * ? *', new HandoverCheckEligibilityBatchScheduler ());
  }
  public void execute(SchedulableContext sc) {
    Database.executebatch(new HandoverCheckEligibilityBatch(), 90);
  }
}