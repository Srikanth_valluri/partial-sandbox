/********************************************************************************
* Description - Test class developed for DisplaySourceAndTargetUnitDetailsCntrl *
*                                                                               *
* Version            Date            Author                    Description      *
* 1.0                13/01/2019      Arjun Khatri              Initial Draft    *
********************************************************************************/

public without sharing class DisplaySourceAndTargetUnitDetailsCntrl {
    
    //Case Id
    String strCaseID;
    
    //List of Sales Service Requests
    public List<Sales_Service_Request__c> lstSSR;
    
    //List of Source unit details
    public List<SR_Booking_Unit__c> lstSourceUnitDetails {get;set;}
    
    //List of Target Unit Details
    public List<Fund_Transfer_Unit__c> lstTargetUnitDetails {get;set;}
    
    
    public DisplaySourceAndTargetUnitDetailsCntrl( ApexPages.standardController controller ) {
       
    }//End constructor
    
    public pagereference init(){
    
        lstSSR = new List<Sales_Service_Request__c>();
        lstSourceUnitDetails = new List<SR_Booking_Unit__c>();
        lstTargetUnitDetails = new List<Fund_Transfer_Unit__c>();
        strCaseID = ApexPages.currentPage().getParameters().get('Id');
        
        if(String.isNotBlank(strCaseID) ) {
            
            //Get Source and target details of Sales Service Request
            lstSSR = [SELECT Id, Name, Booking_Unit__r.Registration_DateTime__c
                           , (SELECT Id,Name,Account_Name__c
                                  , Unit_Name_Formula__c
                                  , Property_Name__c 
                                  , Bedroom_Type__c
                                  , Requested_Price_Special_Price__c
                                  , Token_Amount__c
                                  , Registration_ID__c
                                  , Unit_Status__c
                                  , Booking_Unit__r.Registration_DateTime__c
                              		, Booking_Unit__r.Dos_Name__c
                              		, Booking_Unit__r.HOS_Name__c
                              		, Booking_Unit__r.HOD_Name__c
									, Booking_Unit__r.Sales_Office__c
                              		, Booking_Unit__r.Booking__r.Deal_SR__r.Comments__c
                              		, Booking_Unit__r.Booking__r.Deal_SR__r.Deal_Rejected_Date__c
                                FROM Booking_Units__r
                             	ORDER BY CreatedDate DESC
                             	LIMIT 1)
                            , (SELECT Id,Name,New_Account_Holder__r.Name  
                                   , Target_Unit_Name__c
                                   , Target_Property_Name__c 
                                   , Target_Bedroom_Type__c
                                   , Target_Requested_Price__c
                                   , Target_Token_Amount__c
                                   , Registration_Id__c
                                   , Target_Registration_Status__c
                               	   , New_Unit__r.Registration_DateTime__c
                               		, New_Unit__r.Dos_Name__c
                              		, New_Unit__r.HOS_Name__c
                              		, New_Unit__r.HOD_Name__c
									, New_Unit__r.Sales_Office__c
                              		, New_Unit__r.Booking__r.Deal_SR__r.Comments__c
                              		, New_Unit__r.Booking__r.Deal_SR__r.Deal_Rejected_Date__c
                                FROM Fund_Transfer_Units__r
                              	ORDER BY CreatedDate DESC
                             	LIMIT 1)
                            FROM Sales_Service_Request__c 
                           WHERE id =:strCaseID];
            //system.assert(false,'<<<<<<<<<< objSSR: '+objSSR.Fund_Transfer_Units__r);
            if( lstSSR.size() > 0 && !lstSSR.isEmpty()) {
                
                //Fill Source Unit Details
                if( lstSSR[0].Booking_Units__r.size() > 0 && !lstSSR[0].Booking_Units__r.isEmpty() ) {
                    lstSourceUnitDetails.addAll(lstSSR[0].Booking_Units__r);
                }
                
                //Fill Target Unit Details
                if( lstSSR[0].Fund_Transfer_Units__r.size() > 0 && !lstSSR[0].Fund_Transfer_Units__r.isEmpty() ) {
                    lstTargetUnitDetails.addAll(lstSSR[0].Fund_Transfer_Units__r);
                }
            }
        }//End If
        else {
            ApexPages.addmessage(
                new ApexPages.message(
                    ApexPages.severity.ERROR,'Please Provide Sales Service Request Id.'
                )
            );
        }
        return null;
    }
}