@isTest(SeeAllData=true)
private class createCustomStep_Test{
  static testMethod void test_UseCase1(){
    test.startTest();

     List<NSIBPM__SR_Template__c> SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
        
        Id RecordTypeIdContact = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        
        NSIBPM__Service_Request__c sr = InitializeSRDataTest.getSerReq('Deal',true,null);
        sr.recordtypeid=RecordTypeIdContact;
        sr.NSIBPM__SR_Template__c = SRTemplateList[0].Id;
        insert sr;
        system.debug('-->'+sr.id);
        
        
        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id,null,null);
        insert stp;
        system.debug('-->'+stp.id);
        
   
        booking__c b = new booking__c();
        b.Deal_SR__c = sr.id;
        insert b;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.booking__c = b.id;        
        insert bu;
        
        List<Id> srIds=new list<id>();
        srIds.add(sr.id);
    createCustomStep.createStep(srIds);
     test.stopTest();
  }
  
   
}