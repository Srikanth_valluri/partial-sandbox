/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GenerateKeyReleaseFormLHOTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        objAcc.Country__c = 'United Arab Emirates';
        insert objAcc ;
        
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='AB/019/TH500', Bedroom_Type__c = '1BR',
        Registration_ID__c = '74712', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100,
        Unit_Selling_Price__c = 3456899, Anticipated_Completion_Date__c = system.Today(), 
        OK_to_release_Keys_LHO__c = true);
        insert BUObj;
        
        Test.StartTest();
        PageReference pageRef = Page.KeyReleaseFormLHO;
        pageRef.getParameters().put('id', String.valueOf(BUObj.Id)); 
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(BUObj);
        GenerateKeyReleaseFormLHO controller = new GenerateKeyReleaseFormLHO(sc);
        controller.checkEligiblity();
        DateTime dayToday = system.today();
        controller.strDate = dayToday.format('dd-MM-yyyy');
        controller.saveUnit();
        controller.returnToUnit();        
    }
}