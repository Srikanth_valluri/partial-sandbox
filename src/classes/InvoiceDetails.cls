/****************************************************************************************************************
* Version   Author              Date        Description                                                         *
*---------------------------------------------------------------------------------------------------------------*
* 1.1       Arjun Khatri        31/7/2019   Added method createCollReminders method                             *
* 1.2       Aishwarya Todkar    20-10-2020  Captured Milestone and milestone Type                               *
*****************************************************************************************************************/

@RestResource(urlMapping='/DPInvoiceDetails/*')
global with sharing class InvoiceDetails{//***********AKISHOR******************//
    @HttpPost
    global static String PostInvoiceDetails( Date InvDate , String RegistrationId 
                                           , String UnitName , String PartyId 
                                           , String CallType , String DueAmount
                                           , Date DueDate , String CustName
                                           , String Description , String HoldEmail
                                           , String TrxnNumber , String BankName
                                           , String AccountName , String AccountNo
                                           , String IBAN , String Branch
                                           , String Swiftcode, String Milestone, String Milestonetype ) { 
       String responseMsg = '';      
       ResponseWrapper objResponseWrapper = new ResponseWrapper();
       String Insrt='';
        System.debug('-->>RegId: '+ RegistrationId);         
        
        if(RegistrationId!='' && CallType !='' && TrxnNumber!='') {
            List<DP_Invoices__c> InvLst= new List<DP_Invoices__c>();
            List<Booking_Unit__c> BUId=new List<Booking_Unit__c>();
            List<Account> AcntLst;
            AcntLst=[Select 
                        Id 
                    from 
                        Account 
                    where 
                        Party_Id__c =: PartyId limit 1];
            BUId=[Select 
                        Id 
                    from 
                        Booking_Unit__c 
                    where 
                        Registration_Id__c =: RegistrationId 
                    AND 
                        Unit_Name__c !='' limit 1];
            List <DP_Invoices__c> CurrInvLst= [Select 
                                                    Id 
                                                from 
                                                    DP_Invoices__c 
                                                where 
                                                    Trxn_Number__c=:TrxnNumber 
                                                AND 
                                                    Registration_ID__c =: RegistrationId 
                                                AND 
                                                    Call_Type__c=:CallType limit 1];
            System.debug('CurrInvLst = ' + CurrInvLst);
            System.debug('HoldEmail = ' + HoldEmail);
            if(CurrInvLst.size() == 0 && HoldEmail !='Yes')
            { 
                Insrt = 'Y';
                DP_Invoices__c Inv = new DP_Invoices__c();
                Inv.Registration_ID__c = RegistrationId;
                Inv.Call_Type__c = CallType;
                Inv.Inv_Due__c = DueAmount;
                Inv.Party_Id__c = PartyId;
                Inv.Unit_Name__c = UnitName;
                Inv.Trxn_Number__c = TrxnNumber;
                Inv.Invoice_Date__c = InvDate;
                Inv.Email_To_Sent__c = HoldEmail;
                Inv.Due_Date__c = DueDate;
                Inv.Description__c = Description;
                Inv.Customer_Name__c = CustName;
                Inv.BookingUnits__c = BUId[0].Id;
                Inv.Accounts__c = AcntLst[0].Id;
                Inv.AccountName__c = AccountName;
                Inv.AccountNo__c = AccountNo;
                Inv.BankName__c = BankName;
                Inv.BranchName__c = Branch;
                Inv.IBAN__c = IBAN;
                Inv.SWIFT__c = Swiftcode;
                Inv.Type_of_Milestone__c = Milestonetype;
                Inv.Milestone__c = Milestone;
                InvLst.add(Inv);
            }
            else if(HoldEmail =='Yes' && CurrInvLst.size() > 0)
            {
                Insrt='N';
                DP_Invoices__c InvL = new DP_Invoices__c ();
                InvL.Email_To_Sent__c='Hold';
                InvL.Id=CurrInvLst[0].Id;
                InvLst.add(InvL);
            }
            //if(CurrInvLst.size() == 0 ) {
                try{
                String Stat='';
                    if(Insrt=='Y' && CurrInvLst.size() == 0 ) {
                        Database.insert(InvLst);
                        Stat = 'Added';
                        responseMsg ='Invoice Details Added Successfully';

                        //Call method to create Collection reminders
                        List<Collection_Reminders__c> lstCollReminders = createCollReminders(InvLst);
                        System.debug('lstCollReminders == ' + lstCollReminders);
                        if(lstCollReminders != null && lstCollReminders.size() > 0) {
                            insert lstCollReminders;
                        }
                    } else if(Insrt=='N' && CurrInvLst.size() > 0 ) {
                        Database.update(InvLst);
                        Stat = 'Updated';
                        responseMsg ='Invoice Details Updated Successfully';
                    }
                    //responseMsg ='Invoice Details '+Stat+' Successfully';
                    objResponseWrapper.status = 'Invoice Details '+Stat+' Successfully';
                    objResponseWrapper.statusCode = '200';
                    //RestContext.response.responseBody = JSON.serialize(objResponseWrapper);
                    //Blob.valueOf(JSON.serialize(objResponseWrapper));
                    //Restcontext.response.statusCode = 200; 
                    Insrt='';               
                } catch( Exception ex ) {
                    system.debug( ex.getMessage() );
                    objResponseWrapper.errorMessage = ex.getMessage();
                    objResponseWrapper.statusCode = '400';
                    responseMsg ='Invoice Addition Failed, Please check with Administrator';//ex.getMessage();
                    //RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                    //Restcontext.response.statusCode = 400;
                    System.debug('============= response : ' + JSON.serialize(objResponseWrapper));
                    //return;                 
                }   
            //}
            
    }//end of regid If
    //return JSON.serialize(objResponseWrapper);
    return responseMsg;
}
    //for sending response    
    public class ResponseWrapper {
        public String status;
        public String statusCode;
        public String errorMessage;
       
        public ResponseWrapper() {
            this.status = '';
            this.statusCode = '';
            this.errorMessage = '';
        }
       }

/****************************************************************************************************
Method Name     : createCollReminders                                                               *
Description     : Created Collection reminders on creation of Invoices                              *
Date            : 31/7/2019                                                                         *
Parameter(s)    : List of Invoices                                                                  *
Return          : List of Collection Reminders                                                      *
*****************************************************************************************************/      
    public static List<Collection_Reminders__c> createCollReminders(List<DP_Invoices__c> lstInvoices) {
        List<Collection_Reminders__c> lstReminders = new List<Collection_Reminders__c>();

        if(lstInvoices != null) {
            for(DP_Invoices__c objDP : lstInvoices) {
            
                //On due reminder
                lstReminders.add(
                    new Collection_Reminders__c(
                        InvoiceDetails__c = objDP.Id
                        , Email_Schedule__c = 'On Due'
                        , Email_Triggering_Date__c = objDP.Due_Date__c
                    )
                );

                // One week before reminder
                lstReminders.add(
                    new Collection_Reminders__c(
                        InvoiceDetails__c = objDP.Id
                        , Email_Schedule__c = 'One Week Before'
                        , Email_Triggering_Date__c = objDP.Due_Date__c.addDays(-7)
                    )
                );

                //One week after reminder
                lstReminders.add(
                    new Collection_Reminders__c(
                        InvoiceDetails__c = objDP.Id
                        , Email_Schedule__c = 'One Week After'
                        , Email_Triggering_Date__c = objDP.Due_Date__c.addDays(7)
                    )
                );

                //One month after reminder
                lstReminders.add(
                    new Collection_Reminders__c(
                        InvoiceDetails__c = objDP.Id
                        , Email_Schedule__c = 'One Month After'
                        , Email_Triggering_Date__c = objDP.Due_Date__c.addDays(30)
                    )
                );
            }//End For loop
        }//End list null check if

        return lstReminders;
    }//End createCollReminders method
}