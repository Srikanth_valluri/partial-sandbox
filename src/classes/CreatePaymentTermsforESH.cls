public class CreatePaymentTermsforESH {
    
    @future(Callout=true)
    public static void EarlySettlementPaymentPlanCreation(String caseId) {
        Case ObjCase = [Select Id, Booking_Unit__c, Booking_Unit__r.Registration_ID__c, AccountId, caseNumber, NewPaymentTermJSON__c From Case Where Id =: caseId];
          list<MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE> lstNewPaymentTerm =
              (List<MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE>)JSON.deserialize(ObjCase.NewPaymentTermJSON__c, list<MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE>.class);
              list<paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5> lstHandMQS = new list<paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5>();
              if (lstNewPaymentTerm != null && lstNewPaymentTerm.size() > 0) {
                   for (MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE objPT : lstNewPaymentTerm) {
                       paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5 objHandMQS = new paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5();
                       objHandMQS.REGISTRATION_ID = ObjCase.Booking_Unit__r.Registration_ID__c;
                       objHandMQS.DESCRIPTION = objPT.DESCRIPTION;
                       objHandMQS.INSTALLMENT = objPT.INSTALLMENT;
                       objHandMQS.LINE_ID = objPT.LINE_ID;
                       objHandMQS.MILESTONE_EVENT = objPT.MILESTONE_EVENT;
                       objHandMQS.PAYMENT_AMOUNT = objPT.INVOICE_AMOUNT; 
                       objHandMQS.PERCENT_VALUE = objPT.MILESTEON_PERCENT_VALUE;
                       objHandMQS.TERM_ID = objPT.TERM_ID;
                       if (objPT.TERM_ID == null) {
                           objHandMQS.TRANSFER_AR_INTER_FLAG = 'N';
                       } else {
                           objHandMQS.TRANSFER_AR_INTER_FLAG = 'Y';
                       }
                       if (!string.isBlank(objPT.DUE_DATE)) {
                           //Date objdate = date.parse(objPT.DUE_DATE);
                           
                           //system.debug('!!!!!!!objdate'+objdate);
                           //objHandMQS.EXPECTED_DATE = String.valueOf(DateTime.newInstance(objdate.year(),objdate.month(),objdate.day()).format('d-MMM-YYYY'));
                           //objHandMQS.PAYMENT_DATE = String.valueOf(DateTime.newInstance(objdate.year(),objdate.month(),objdate.day()).format('d-MMM-YYYY'));
                           objHandMQS.PAYMENT_DATE = getConvertDateTime(objPT.DUE_DATE);
                           system.debug('!!!!objHandMQS.PAYMENT_DATE'+objHandMQS.PAYMENT_DATE);
                       }
                       lstHandMQS.add(objHandMQS);
                   }              
              }
              
        String P_REGISTRATION_ID = ObjCase.Booking_Unit__r.Registration_ID__c;
        String P_SR_NUMBER = '2-'+ObjCase.CaseNumber;
        String P_SR_TYPE = 'AMENDMENT_OF_PAYMENT_TERMS';

        //HandoverMQServices.HandoverHttpSoap11Endpoint service =  new HandoverMQServices.HandoverHttpSoap11Endpoint();
        //service.timeout_x = 120000;
        String res = AOPTMQService.createPaymentPlanIPMS(P_REGISTRATION_ID ,
                                                                P_SR_NUMBER,
                                                                P_SR_TYPE ,
                                                                lstHandMQS
                                                            ); 
                                                            
         system.debug('!!!!!!!!res'+res);
         
         PaymentTermWrapperClass response = (PaymentTermWrapperClass)JSON.deserialize(res,
                                                    PaymentTermWrapperClass.class
                                                );  
         system.debug('!!!!!!!!response'+response); 
         
          if (response.status == 'S') {
            String PPold;
            List<Payment_Plan__c> lstPP = new List<Payment_Plan__c>();
            for (Payment_Plan__c objPaymentPlan : [Select Id, Status__c, Booking_Unit__c From Payment_Plan__c Where Booking_Unit__c =: ObjCase.Booking_Unit__c]){
                objPaymentPlan.Status__c = 'InActive';
                lstPP.add(objPaymentPlan);
                PPold = objPaymentPlan.id;
             }
             if (lstPP != null && lstPP.size()>0){
                 update lstPP;
             }
             
            Payment_Plan__c newPaymentPlan = new Payment_Plan__c();
            newPaymentPlan.Status__c = 'Active';
            newPaymentPlan.Booking_Unit__c = ObjCase.Booking_Unit__c;
            newPaymentPlan.Parent_Payment_Plan__c = PPold;
            insert newPaymentPlan;
            
            List<Payment_Terms__c> newPaymentTerms = new List<Payment_Terms__c>();
            if (lstNewPaymentTerm  != null && lstNewPaymentTerm.size() > 0) {
                for (MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE objPT : lstNewPaymentTerm) {
                    Payment_Terms__c objPaymentTerm = new Payment_Terms__c();
                    objPaymentTerm.Installment__c = objPT.INSTALLMENT;
                    objPaymentTerm.Description__c = objPT.DESCRIPTION;
                    objPaymentTerm.Milestone_Event__c = objPT.MILESTONE_EVENT;
                    objPaymentTerm.Milestone_Event_Arabic__c = objPT.MILESTONE_EVENT;
                    objPaymentTerm.Percent_Value__c = objPT.MILESTEON_PERCENT_VALUE; 
                     if (!String.isBlank(objPT.DUE_DATE) ) { 
                        objPaymentTerm.Payment_Date__c = Date.parse(objPT.DUE_DATE);
                    }
                    objPaymentTerm.Payment_Amount__c = objPT.INVOICE_AMOUNT;
                    objPaymentTerm.Payment_Plan__c = newPaymentPlan.id;
                    newPaymentTerms.add(objPaymentTerm);
                }             
            }
            if (newPaymentTerms != null && newPaymentTerms.size() > 0){
              insert newPaymentTerms;
            }
        } else if (response.status == 'E') {
            Error_Log__c objErr = new Error_Log__c();
            objErr.Account__c = objCase.AccountId;
            objErr.Booking_Unit__c = objCase.Booking_Unit__c;
            objErr.Case__c = objCase.id;
            objErr.Error_Details__c = response.message;
            objErr.Process_Name__c = 'Early Handover';
            insert objErr;
        }                 
    }
    
    public class PaymentTermWrapperClass {
        String message {get;set;}
        String status  {get;set;}
    }

    public static string getConvertDateTime(string strDT) {
        Map<string,string> MapMonthList = new Map<string,string>();
        MapMonthList.put('01','JAN');
        MapMonthList.put('1','JAN');
        MapMonthList.put('02','FEB');
        MapMonthList.put('2','FEB');
        MapMonthList.put('03','MAR');
        MapMonthList.put('3','MAR');
        MapMonthList.put('04','APR');
        MapMonthList.put('4','APR');
        MapMonthList.put('05','MAY');
        MapMonthList.put('5','MAY');
        MapMonthList.put('06','JUN');
        MapMonthList.put('6','JUN');
        MapMonthList.put('07','JUL');
        MapMonthList.put('7','JUL');
        MapMonthList.put('08','AUG');
        MapMonthList.put('8','AUG');
        MapMonthList.put('09','SEP');
        MapMonthList.put('9','SEP');
        MapMonthList.put('10','OCT');
        MapMonthList.put('11','NOV');
        MapMonthList.put('12','DEC');
        //system.debug('strDT'+strDT);
        String[] strDTDivided = strDT.split('/');
        //system.debug('strDTDivided '+strDTDivided.get(1) );
        string month = String.ValueOf(MapMonthList.get(strDTDivided.get(1)));
        string day = strDTDivided.get(0).replace('/', '');
        string year = strDTDivided.get(2);
        
        //string stringDate = year + '-' + month + '-' + day;
        string stringDate = day + '-' + month + '-' + year;
        //system.debug('stringDate '+stringDate );
        return stringDate;
    }


}