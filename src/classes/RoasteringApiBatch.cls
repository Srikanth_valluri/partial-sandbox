/***************************************************************************************************************
Description: Batch class to get attendance,shift and holiday schedule for the user.
________________________________________________________________________________________________________________
Version Date(DD-MM-YYYY)    Author              Description
----------------------------------------------------------------------------------------------------------------
1.0     16-07-2020          Komal Shitole       Initial Draft
****************************************************************************************************************/
public class RoasteringApiBatch implements Database.Batchable<sObject>,Database.AllowsCallouts{
    Map<String, AttendanceDailyWrapper> mapOfUserDateToAtendance;
    Map<String, ShiftDetailsWrapper> mapOfShiftIdToShiftDetails;
    Map<String, HolidayScheduleWrapper> mapOfHolidayKeyToHoliday;
    Map<String,User> mapOfIpmsIdToUser;
    public Database.QueryLocator start(Database.BatchableContext BC){
        //List<String> lstDepartment = Label.Department_Names.split(',');

        if(Test.isRunningTest()) {
            return Database.getQueryLocator([SELECT  Id,
                                                     Name,
                                                     isActive,
                                                     HR_Employee_ID__c,
                                                     Manager.LastName
                                             FROM User 
                                             WHERE (Department = 'CRM' OR Department = 'Collection' OR Department = 'Handover')
                                             AND HR_Employee_ID__c != Null
                                             AND isActive = true
                                             AND IsPortalEnabled = false
                                             LIMIT 1
                                         ]);
        }
        else {
              return Database.getQueryLocator([SELECT  Id,
                                                       Name,
                                                       isActive,
                                                       HR_Employee_ID__c,
                                                       Manager.LastName
                                              FROM User 
                                              WHERE Fetch_Attendance_Details__c = true
                                              AND isActive = true
                                              AND HR_Employee_ID__c != Null
                                              AND IsPortalEnabled = false
                                             ]);
       }
    }
    public void execute(Database.BatchableContext BC, List<User> userList){
        System.debug('userList---->'+userList);
        List<Attendance__c> attendanceListToBeUpdated = new List<Attendance__c>();
        List<Attendance__c> attendanceListToBeInserted = new List<Attendance__c>();
        List<Holiday_Schedule__c> holidayScheduleUpdateList = new List<Holiday_Schedule__c>();
        List<Holiday_Schedule__c> holidayScheduleInsertList = new List<Holiday_Schedule__c>();
        Set<String> shiftIds = new Set<String>();
        Map<String,Id> mapOfShiftdToUserId = new Map<String,Id>();
        Set<String> ipmsIds = new Set<String>();
        Map<String,Attendance__c> mapOfUserIdDateToAttendance = new Map<String,Attendance__c>();
        mapOfUserDateToAtendance = new Map<String, AttendanceDailyWrapper>();
        mapOfShiftIdToShiftDetails = new Map<String, ShiftDetailsWrapper>();
        mapOfHolidayKeyToHoliday = new Map<String, HolidayScheduleWrapper>();
        Map<String, Holiday_Schedule__c> mapOfUserIdYearDateToHoliday = new Map<String, Holiday_Schedule__c>();
        mapOfIpmsIdToUser = new Map<String,User>();
        AttendanceDailyWrapper attendanceRes;
        HolidayScheduleWrapper holidayRes;
        ShiftDetailsWrapper shiftRes;
        HTTPResponse httpResponseAttendane;
        HTTPResponse httpResponseHoliday;
        HTTPResponse httpResponseShift;
        List<Error_Log__c> errorLogList = new List<Error_Log__c>();
        for(User userObj:userList) {          
            if(String.isNotBlank(userObj.HR_Employee_ID__c)) {
                System.debug('userObj.HR_Employee_ID__c'+userObj.HR_Employee_ID__c);
                ipmsIds.add(userObj.HR_Employee_ID__c);
                mapOfIpmsIdToUser.put(userObj.HR_Employee_ID__c,userObj);
                System.debug('ipmsIds'+ipmsIds);
                    Date startDate = Date.Today() - Integer.valueOf(Label.StartValueFromToday);
                    Date endDate = Date.Today() + Integer.valueOf(Label.EndValueFromToday);
                try {
                    httpResponseAttendane = RoasteringApiAttendance.getAttendanceDetails(userObj.HR_Employee_ID__c,startDate,endDate);
                    if(httpResponseAttendane != Null) {
                        if(httpResponseAttendane.getStatusCode() == 200 && httpResponseAttendane.getBody() != null && (httpResponseAttendane.getBody()).containsIgnoreCase('attendance-daily')) {
                            attendanceRes = (AttendanceDailyWrapper)JSON.deserialize(httpResponseAttendane.getBody().replaceAll('-','_'),AttendanceDailyWrapper.class);
                            System.debug('attendanceRes--'+ attendanceRes);
                            //return attendanceRes;
                        }
                        else {
                            Error_Log__c objErrorlog = new Error_Log__c();objErrorlog.Error_Details__c = httpResponseAttendane.getBody();objErrorlog.User__c = userObj.Id;
                            errorLogList.add(objErrorlog);                            
                        }
                    }
                   }
                 catch(Exception e) {
                     Error_Log__c objErrorlog = new Error_Log__c();objErrorlog.Error_Details__c = httpResponseAttendane.getBody();objErrorlog.User__c = userObj.Id;
                     errorLogList.add(objErrorlog); 
                 }
                    
              
                System.debug('attendanceRes---->'+attendanceRes);
                if(attendanceRes != Null && attendanceRes.attendance_daily != Null && attendanceRes.attendance_daily.size() > 0) {
                    for(AttendanceDailyWrapper objAttendance : attendanceRes.attendance_daily ) {
                        if(objAttendance.userid != Null && objAttendance.processdate != Null ) {
                            mapOfUserDateToAtendance.put(String.ValueOf(objAttendance.userid)+'-'+objAttendance.processdate,objAttendance);
                            shiftIds.add(objAttendance.workingshift);
                            //mapOfShiftdToUserId.put(objAttendance.workingshift,userObj.Id);
                        }
                    }                    
                }
                System.debug('mapOfUserDateToAtendance--->'+mapOfUserDateToAtendance);
                try {                  
                    httpResponseHoliday = RoasteringApiHoliday.getHolidaySchedule(userObj.HR_Employee_ID__c,String.valueOf(Date.Today().Year()));
                    if(httpResponseHoliday != Null) {
                         if(httpResponseHoliday.getStatusCode() == 200 && httpResponseHoliday.getBody() != null && (httpResponseHoliday.getBody()).containsIgnoreCase('holiday')) {
                              holidayRes = (HolidayScheduleWrapper)JSON.deserialize(httpResponseHoliday.getBody().replaceAll('-','_'),HolidayScheduleWrapper.class);
                             System.debug('holidayRes--'+ holidayRes);
                         }
                         else {
                                Error_Log__c objErrorlog = new Error_Log__c();objErrorlog.Error_Details__c = httpResponseHoliday.getBody();objErrorlog.User__c = userObj.Id;
                                errorLogList.add(objErrorlog);                            
                         }
                         
                    }
                 }
                 catch(Exception e) {
                    Error_Log__c objErrorlog = new Error_Log__c();objErrorlog.Error_Details__c = httpResponseHoliday.getBody();objErrorlog.User__c = userObj.Id;
                    errorLogList.add(objErrorlog);   
                 
                 }
                System.debug('holidayRes---->'+holidayRes);
                if(holidayRes != Null && holidayRes.holiday != Null && holidayRes.holiday.size() > 0) {
                    for(HolidayScheduleWrapper  objHoliday : holidayRes.holiday) {
                        if(objHoliday.user_id != Null && objHoliday.year != Null && objHoliday.holiday_date != Null) {
                            mapOfHolidayKeyToHoliday.put(String.ValueOf(objHoliday.user_id)+'-'+objHoliday.year+'-'+objHoliday.holiday_date ,objHoliday);
                        }                        
                    }                    
                }
                System.debug('mapOfHolidayKeyToHoliday--->'+mapOfHolidayKeyToHoliday);                        
         }           
        }
        
        List<Attendance__c> attendanceRecordList = [Select Id,
                                                           User_Id__c,
                                                           Date__c 
                                                   FROM  Attendance__c 
                                                   WHERE User_Id__c IN:ipmsIds 
                                                   AND User_Id__c != Null 
                                                   AND Date__c != Null];
        for(Attendance__c attendaceObj:attendanceRecordList) {
            mapOfUserIdDateToAttendance.put(attendaceObj.User_Id__c+'-'+attendaceObj.Date__c.format(),attendaceObj);            
        }
        List<Holiday_Schedule__c> holidayRecordList = [Select Id,
                                                              User_Id__c,
                                                              Holiday_Start_Date__c,
                                                              Holiday_End_Date__c,
                                                              Year__c
                                                       FROM Holiday_Schedule__c 
                                                       WHERE User_Id__c IN:ipmsIds 
                                                       AND User_Id__c != Null 
                                                       AND Holiday_Start_Date__c != Null 
                                                       AND Holiday_End_Date__c != Null 
                                                       AND Year__c != Null ];
        for(Holiday_Schedule__c holidayObj:holidayRecordList) {
            mapOfUserIdYearDateToHoliday.put(holidayObj.User_Id__c+'-'+holidayObj.Year__c+'-'+datetime.newInstance(holidayObj.Holiday_Start_Date__c.year(),holidayObj.Holiday_Start_Date__c.month(),holidayObj.Holiday_Start_Date__c.day()).format('MM/dd/yyyy')+'_'+datetime.newInstance(holidayObj.Holiday_End_Date__c.year(),holidayObj.Holiday_End_Date__c.month(),holidayObj.Holiday_End_Date__c.day()).format('MM/dd/yyyy'),holidayObj);            
        }
        
        System.debug('mapOfUserIdDateToAttendance---->'+mapOfUserIdDateToAttendance);
        System.debug('mapOfUserIdYearDateToHoliday---->'+mapOfUserIdYearDateToHoliday);
        if(shiftIds != Null && shiftIds.size() > 0) {
            for(String objShift : shiftIds) {
                httpResponseShift = RoasteringApiShift.getShiftDetails(objShift);
                try {
                    if(httpResponseShift != Null) {
                        if(httpResponseShift.getStatusCode() == 200 && httpResponseShift.getBody() != null && (httpResponseShift.getBody()).containsIgnoreCase('shift-details')) {
                            shiftRes = (ShiftDetailsWrapper)JSON.deserialize(httpResponseShift.getBody().replaceAll('-','_'),ShiftDetailsWrapper.class);
                            System.debug('shiftRes--'+ shiftRes);
                        //return shiftRes;
                        }
                         else {Error_Log__c objErrorlog = new Error_Log__c();objErrorlog.Error_Details__c =httpResponseShift.getBody();
                                //objErrorlog.User__c = userList[0].Id;
                                errorLogList.add(objErrorlog);                            
                         }    
                    }
                }
                catch(Exception e) {Error_Log__c objErrorlog = new Error_Log__c();objErrorlog.Error_Details__c =httpResponseShift.getBody();
                                //objErrorlog.User__c = userList[0].Id;
                                errorLogList.add(objErrorlog);  
                }
                //RoasteringService.ShiftDetailsWrapper  collRes1 = RoasteringService.getShiftDetails(objShift);
                if(shiftRes != Null && shiftRes.shift_details != Null && shiftRes.shift_details.size() > 0) {
                    for(ShiftDetailsWrapper objShiftdetail : shiftRes.shift_details) {
                     if(objShiftdetail.shift_id != Null) {
                      mapOfShiftIdToShiftDetails.put(objShiftdetail.shift_id,objShiftdetail);
                     }
                    
                    }
                }
            }
        }
        if(mapOfUserDateToAtendance != Null && mapOfUserDateToAtendance.size() > 0) {
            for(String attandanceObj : mapOfUserDateToAtendance.keyset()) {
                if(mapOfUserIdDateToAttendance.containsKey(attandanceObj) && mapOfUserIdDateToAttendance.get(attandanceObj).Id != Null) {
                    System.debug('Record is already present update that only');
                    Attendance__c attandanceRecordObj = new Attendance__c(id=mapOfUserIdDateToAttendance.get(attandanceObj).Id);
                    Attendance__c attendanceUpdateRecord = returnAttendanceRecord(attandanceRecordObj,attandanceObj);
                    attendanceListToBeUpdated.add(attendanceUpdateRecord);
                }
                else {
                    System.debug('Record is not present so insert the record');
                    Attendance__c attandanceRecordObj = new Attendance__c();
                    Attendance__c attendanceInsertRecord = returnAttendanceRecord(attandanceRecordObj,attandanceObj);
                    attendanceListToBeInserted.add(attendanceInsertRecord);
                    
                }
            }
            
            System.debug('attendanceListToBeInserted---->'+attendanceListToBeInserted);
            System.debug('attendanceListToBeUpdated---->'+attendanceListToBeUpdated);
            
        }
        
        if(mapOfHolidayKeyToHoliday !=Null && mapOfHolidayKeyToHoliday.size() > 0) {
            for(String holidayObj : mapOfHolidayKeyToHoliday.keyset()) {
            if(mapOfUserIdYearDateToHoliday.containsKey(holidayObj) && mapOfUserIdYearDateToHoliday.get(holidayObj).Id != Null) {
                Holiday_Schedule__c holidayScheduleObj = new Holiday_Schedule__c(id=mapOfUserIdYearDateToHoliday.get(holidayObj).Id);
                Holiday_Schedule__c holidayUpdateScheduleObj = returnHolidaySchedule(holidayScheduleObj,holidayObj);
                holidayScheduleUpdateList.add(holidayUpdateScheduleObj);
            }
            else {
                Holiday_Schedule__c holidayScheduleObj = new Holiday_Schedule__c();
                holidayScheduleObj.User_Id__c = mapOfHolidayKeyToHoliday.get(holidayObj).user_id  ;
                Holiday_Schedule__c holidayInsertScheduleObj = returnHolidaySchedule(holidayScheduleObj,holidayObj);
                holidayScheduleInsertList.add(holidayInsertScheduleObj);
                
            }
            
            }
        }
        System.debug('holidayScheduleInsertList---->'+holidayScheduleInsertList);
        System.debug('holidayScheduleUpdateList---->'+holidayScheduleUpdateList);
        if(attendanceListToBeInserted != Null && attendanceListToBeInserted.size()> 0) {
            try {
                insert attendanceListToBeInserted;
            }
            catch(Exception e) {Error_Log__c objErrorlog = new Error_Log__c();objErrorlog.Error_Details__c = 'Error Occured while inserting the AttendanceRecord';
                errorLogList.add(objErrorlog);
            }
        }
        if(attendanceListToBeUpdated != Null && attendanceListToBeUpdated.size()> 0) {
            try {
                update attendanceListToBeUpdated;
            }
             catch(Exception e) {Error_Log__c objErrorlog = new Error_Log__c();objErrorlog.Error_Details__c = 'Error Occured while updating the AttendanceRecord';
                errorLogList.add(objErrorlog);
            }
        }
        if(holidayScheduleUpdateList != Null && holidayScheduleUpdateList.size()> 0) {
            try {
                update holidayScheduleUpdateList;
            }
            catch(Exception e) {Error_Log__c objErrorlog = new Error_Log__c();objErrorlog.Error_Details__c = 'Error Occured while inserting the HolidayScheduleRecord';
                errorLogList.add(objErrorlog);
            }
        }
        if(holidayScheduleInsertList != Null && holidayScheduleInsertList.size()> 0) {
            try {
                insert holidayScheduleInsertList;
             }
            catch(Exception e) {Error_Log__c objErrorlog = new Error_Log__c();objErrorlog.Error_Details__c = 'Error Occured while updating the HolidayScheduleRecord';
                errorLogList.add(objErrorlog);
            }
        }
        if(errorLogList != Null && errorLogList.size() > 0) {
            try {
               insert errorLogList;
            }
            catch(Exception e) {
               System.debug('Inserting error log error'+e.getMessage()); 
            }
        }
    }
    public void finish(Database.BatchableContext BC){
    }
    
    public Attendance__c returnAttendanceRecord(Attendance__c attandanceRecordObj,String attandanceObj) {
        attandanceRecordObj.User_Id__c = mapOfUserDateToAtendance.get(attandanceObj).userid;
        attandanceRecordObj.User_Name__c = mapOfUserDateToAtendance.get(attandanceObj).username;
        attandanceRecordObj.Work_Hours__c = mapOfUserDateToAtendance.get(attandanceObj).worktime;
        attandanceRecordObj.First_Half__c = mapOfUserDateToAtendance.get(attandanceObj).firsthalf;
        attandanceRecordObj.Second_Half__c = mapOfUserDateToAtendance.get(attandanceObj).secondhalf;
        attandanceRecordObj.Late_In__c= mapOfUserDateToAtendance.get(attandanceObj).latein;
        attandanceRecordObj.Early_Out__c = mapOfUserDateToAtendance.get(attandanceObj).earlyout;
        attandanceRecordObj.Date__c = Date.parse(mapOfUserDateToAtendance.get(attandanceObj).processdate);
        attandanceRecordObj.First_In__c = String.isNotBlank(mapOfUserDateToAtendance.get(attandanceObj).punch1)?convertStringToDateTime(mapOfUserDateToAtendance.get(attandanceObj).punch1):Null;
        attandanceRecordObj.Last_Out__c = String.isNotBlank(mapOfUserDateToAtendance.get(attandanceObj).punch2)?convertStringToDateTime(mapOfUserDateToAtendance.get(attandanceObj).punch2):Null;
        if(mapOfIpmsIdToUser != Null && mapOfIpmsIdToUser.size() > 0 && mapOfIpmsIdToUser.containsKey(mapOfUserDateToAtendance.get(attandanceObj).userid) && mapOfIpmsIdToUser.get(mapOfUserDateToAtendance.get(attandanceObj).userid).Id != Null) {
            attandanceRecordObj.User__c = mapOfIpmsIdToUser.get(mapOfUserDateToAtendance.get(attandanceObj).userid).Id;
        }
        if(mapOfShiftIdToShiftDetails != Null &&
         mapOfShiftIdToShiftDetails.size() > 0 &&
          mapOfShiftIdToShiftDetails.containsKey(mapOfUserDateToAtendance.get(attandanceObj).workingshift) && mapOfShiftIdToShiftDetails.get(mapOfUserDateToAtendance.get(attandanceObj).workingshift) != Null) {
            attandanceRecordObj.Shift_Id__c = mapOfShiftIdToShiftDetails.get(mapOfUserDateToAtendance.get(attandanceObj).workingshift).shift_id;
            attandanceRecordObj.Shift_Name__c = mapOfShiftIdToShiftDetails.get(mapOfUserDateToAtendance.get(attandanceObj).workingshift).shift_name;
            attandanceRecordObj.Shift_Start__c = mapOfShiftIdToShiftDetails.get(mapOfUserDateToAtendance.get(attandanceObj).workingshift).shift_start;
            attandanceRecordObj.Late_In_Grace__c = mapOfShiftIdToShiftDetails.get(mapOfUserDateToAtendance.get(attandanceObj).workingshift).late_in_grace;
            attandanceRecordObj.Early_Out_Grace__c = mapOfShiftIdToShiftDetails.get(mapOfUserDateToAtendance.get(attandanceObj).workingshift).early_out_grace;
            attandanceRecordObj.Shift_End__c = mapOfShiftIdToShiftDetails.get(mapOfUserDateToAtendance.get(attandanceObj).workingshift).shift_end;  
        }
        return attandanceRecordObj;
    }
    
    public Holiday_Schedule__c returnHolidaySchedule(Holiday_Schedule__c holidayScheduleObj,String holidayObj) {
        holidayScheduleObj.User_Id__c = mapOfHolidayKeyToHoliday.get(holidayObj).user_id;
        holidayScheduleObj.User_Name__c = mapOfHolidayKeyToHoliday.get(holidayObj).user_name;
        if(mapOfIpmsIdToUser !=Null && 
        mapOfIpmsIdToUser.size() > 0 && 
        mapOfIpmsIdToUser.containsKey(mapOfHolidayKeyToHoliday.get(holidayObj).user_id)) {
            holidayScheduleObj.User__c = mapOfIpmsIdToUser.get(mapOfHolidayKeyToHoliday.get(holidayObj).user_id).Id;
        }
        holidayScheduleObj.Year__c= String.valueOf(mapOfHolidayKeyToHoliday.get(holidayObj).year);
        holidayScheduleObj.Holiday_Name__c = mapOfHolidayKeyToHoliday.get(holidayObj).holiday_name;
        holidayScheduleObj.Schedule_Id__c = String.valueOf(mapOfHolidayKeyToHoliday.get(holidayObj).schedule_id);
        holidayScheduleObj.Schedule_Name__c = mapOfHolidayKeyToHoliday.get(holidayObj).schedule_name;
        holidayScheduleObj.Holiday_Start_Date__c = convertDateInddMMyyyyFormat((mapOfHolidayKeyToHoliday.get(holidayObj).holiday_date).substringBefore('_')) ;
        holidayScheduleObj.Holiday_End_Date__c = convertDateInddMMyyyyFormat((mapOfHolidayKeyToHoliday.get(holidayObj).holiday_date).substringAfter('_')) ; 
        return holidayScheduleObj;
    }
    
/***************************************************************************************************************
Description: Method to return DateTime from String
Return Type: DateTime 
Parameters : String Datetime
****************************************************************************************************************/
     public DateTime convertStringToDateTime(String myDate) {
        String stringDate = myDate.substring(6,10) + '-' + 
                            myDate.substring(3,5) + '-' +
                            myDate.substring(0,2) + ' ' +
                            myDate.substring(11,19);
        System.debug('stringDate-->'+stringDate);
        DateTime dateTimeObj = datetime.valueOf(stringDate);
        System.debug('dateTimeObj'+dateTimeObj);
        return dateTimeObj;
    }
    
/***************************************************************************************************************
Description: Method to return Date in dd/MM/yyyy format from String
Return Type: Date 
Parameters : String Date
****************************************************************************************************************/
    public Date convertDateInddMMyyyyFormat(String holidayDate) {
        List<String> splittedDate = holidayDate.split('/');
        System.debug(splittedDate);
        String newFomratDate = splittedDate[1]+'/'+splittedDate[0]+'/'+splittedDate[2];
        Date parsedHolidayDate = Date.parse(newFomratDate);
        return parsedHolidayDate;
    }
    
        public class AttendanceDailyWrapper {
        public String userid {get;set;} 
        public String workingshift {get;set;} 
        public String firsthalf {get;set;} 
        public String secondhalf {get;set;} 
        public String username {get;set;} 
        public String processdate {get;set;} 
        public String punch1 {get;set;} 
        public String punch2 {get;set;} 
        public String workingshift1 {get;set;} 
        public String latein {get;set;} 
        public String earlyout {get;set;} 
        public String overtime {get;set;} 
        public String worktime {get;set;}  
        public List<AttendanceDailyWrapper> attendance_daily {get;set;} 
    } 
    
     public class ShiftDetailsWrapper {
        public String shift_id {get;set;} 
        public String shift_name {get;set;} 
        public String shift_start {get;set;} 
        public String shift_end {get;set;} 
        public String break_start {get;set;} 
        public String break_end {get;set;} 
        public String late_in_grace {get;set;} 
        public String early_out_grace {get;set;} 
        public Integer type_Z {get;set;} 
        public List<ShiftDetailsWrapper> shift_details {get;set;}
      }
      
    public class HolidayScheduleWrapper {
        public String user_id {get;set;} 
        public String user_name {get;set;} 
        public String short_name {get;set;} 
        public Integer schedule_id {get;set;} 
        public String schedule_name {get;set;} 
        public Integer year {get;set;} 
        public String holiday_name {get;set;} 
        public String holiday_date {get;set;} 
        public List<HolidayScheduleWrapper> holiday {get;set;} 
    }
    

    
    
    
}