/*
* Description - Test class developed for actionComUpdated
*
* Version            Date            Author            Description
* 1.0              21/11/2017      Ashish Agarwal     Initial Draft
*/


@isTest
private class actionComUpdatedTest {

    static testMethod void testTotalPenaltyWaiver() {
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, actionComUpdated.TotalPenaltyWaiverResponse_element>();
            actionComUpdated.TotalPenaltyWaiverResponse_element response1 = new actionComUpdated.TotalPenaltyWaiverResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	actionComUpdated.PenaltyWaiverHttpSoap11Endpoint objCall = new actionComUpdated.PenaltyWaiverHttpSoap11Endpoint();
        	objCall.TotalPenaltyWaiver('123', 'Test', 'SFDC', '123456');
        test.stopTest();
    }
    
    static testMethod void testPenaltyWaiverDetails() {
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, actionComUpdated.PenaltyWaiverDetailsResponse_element>();
            actionComUpdated.PenaltyWaiverDetailsResponse_element response1 = new actionComUpdated.PenaltyWaiverDetailsResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	actionComUpdated.PenaltyWaiverHttpSoap11Endpoint objCall = new actionComUpdated.PenaltyWaiverHttpSoap11Endpoint();
        	objCall.PenaltyWaiverDetails('Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 0,0,0,0 );
        test.stopTest(); 
    }
    
    static testMethod void testGeneratePenaltyStatment() {
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, actionComUpdated.GeneratePenaltyStatmentResponse_element>();
            actionComUpdated.GeneratePenaltyStatmentResponse_element response1 = new actionComUpdated.GeneratePenaltyStatmentResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	actionComUpdated.PenaltyWaiverHttpSoap11Endpoint objCall = new actionComUpdated.PenaltyWaiverHttpSoap11Endpoint();
        	objCall.GeneratePenaltyStatment('Test', 'Test', 'Test', 'Test', 'Test', 'Test');
        test.stopTest(); 
    }
    
    static testMethod void testUpdatePenaltyWaived() {
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, actionComUpdated.UpdatePenaltyWaivedResponse_element>();
            actionComUpdated.UpdatePenaltyWaivedResponse_element response1 = new actionComUpdated.UpdatePenaltyWaivedResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	actionComUpdated.PenaltyWaiverHttpSoap11Endpoint objCall = new actionComUpdated.PenaltyWaiverHttpSoap11Endpoint();
        	objCall.UpdatePenaltyWaived('Test', 'Test', 'Test', new list<PenaltyWaiverDetails.APPSXXDC_PROCESS_SERX1794747X1X5>() );
        test.stopTest(); 
    }
}