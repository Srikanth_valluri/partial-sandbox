global class Damac_UKURRF_WithSolicitorCntlr{
    public Form_Request__c formRequest { get; set; }
    public List <Form_request_Inventories__c> inventories { get; set; }
    public String todayDate { get; set; }
    public String currentUser {get;set;}
    public String currentUserEmail {get; set;}
    public String userCurrentTime { get; set; }
    public Integer dd { get; set; }
    public Integer mon { get; set; }
    public Integer year { get; set; }
    public Damac_UKURRF_WithSolicitorCntlr() {
         Id formRequestId = Apexpages.currentPage().getParameters().get('recId');
         formRequest = new Form_Request__c ();
         Date today = Date.today();
         todayDate = today.format();
         userCurrentTime = System.Now().format()+':'+System.Now().Second();
        currentUser = UserInfo.getName();
        currentUserEmail = UserInfo.getUserEmail();
        dd = System.today().day();
        mon = System.today().month();
        year = System.today().year(); 
         formRequest = Database.query ('SELECT '+getAllFields('Form_request__c')+',Inquiry__r.Last_Name__c,Inquiry__r.First_Name__c FROM Form_request__c WHERE Id =: formRequestId ');
         inventories = new List <Form_request_Inventories__c> ();
        for (Form_request_Inventories__c inv :[SELECT Promotions_Offered__c, Promotion_Name__c, 
                                                Inventory__r.Currency_of_Sale__c, Inventory__r.Property_Name__c, Inventory__r.Marketing_Name__c,
                                               Inventory__c, Inventory__r.Special_Price__c, Inventory__r.Total_Price_inc_VAT__c,
                                               Inventory__r.Unit_name__c, Inventory__r.is_mixed_use__c,
                                               Form_request__c, Gross_price__c, Permitted_Usage__c,Additional_requests__c,Anticipated_completion_date__c
                                               ,Apartment_area__c, Apartment_No__c, Apartment_price__c, Apartment_Type__c, Introduced_by__c, Reservation_expiry_date__c  
                                               ,Type_of_Buyer__c ,Inventory__r.Building_ID__c,Payment_Plan_Name__c      
                                               FROM Form_request_Inventories__c
                                               WHERE Form_request__c =: formRequest.ID]) 
        {
            
            inventories.add (inv);
        }
        if (formRequest.Email__c != null) {
            formRequest.Email__c = formRequest.Email__c.replaceAll('(^[^@]{3}|(?!^)\\G)[^@]', '$1*');
        }
        if (formRequest.Other_Buyer_Individual_Email__c != null) {
            formRequest.Other_Buyer_Individual_Email__c = formRequest.Other_Buyer_Individual_Email__c.replaceAll('(^[^@]{3}|(?!^)\\G)[^@]', '$1*');
        }
        if (formRequest.Corporate_Email__c != null) {
            formRequest.Corporate_Email__c = formRequest.Corporate_Email__c.replaceAll('(^[^@]{3}|(?!^)\\G)[^@]', '$1*');
        }
        if (formRequest.Other_Buyer_Corporate_Email__c!= null) {
            formRequest.Other_Buyer_Corporate_Email__c = formRequest.Other_Buyer_Corporate_Email__c.replaceAll('(^[^@]{3}|(?!^)\\G)[^@]', '$1*');
        }
        
        if (formRequest.Telephone_Number__c != null) {
            formRequest.Telephone_Number__c = formRequest.Telephone_Number__c.replaceAll('\\d(?=\\d{3})', '*');
            
            if (formRequest.Mobile_Country_Code__c != null) {
                formRequest.Telephone_Number__c  = formRequest.Mobile_Country_Code__c+' '+formRequest.Telephone_Number__c ;
            }
        }
        if (formRequest.Mobile_Number__c != null) {
            formRequest.Mobile_Number__c = formRequest.Mobile_Number__c.replaceAll('\\d(?=\\d{3})', '*');
            if (formRequest.Corporate_Country_Code__c != null) {
                formRequest.Mobile_Number__c = formRequest.Corporate_Country_Code__c +' '+formRequest.Mobile_Number__c ;
            }
        }
        
        
        if (formRequest.Other_Individual_Telephone__c != null) {
            formRequest.Other_Individual_Telephone__c = formRequest.Other_Individual_Telephone__c.replaceAll('\\d(?=\\d{3})', '*');
            
            if (formRequest.Other_Mobile_Country_Code__c != null) {
                formRequest.Other_Individual_Telephone__c = formRequest.Other_Mobile_Country_Code__c+' '+formRequest.Other_Individual_Telephone__c ;
            }
        }
        if (formRequest.Other_Corporate_Telephone__c != null) {
            formRequest.Other_Corporate_Telephone__c = formRequest.Other_Corporate_Telephone__c.replaceAll('\\d(?=\\d{3})', '*');
            if (formRequest.Other_Corporate_Country_Code__c!= null) {
                formRequest.Other_Corporate_Telephone__c = formRequest.Other_Corporate_Country_Code__c+' '+formRequest.Other_Corporate_Telephone__c ;
            }
        }
    }
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        } 
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            fields += sfield.getDescribe().getName () + ', ';
        }
        return fields.removeEnd(', ');
    }

}