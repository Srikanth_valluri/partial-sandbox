public without sharing class FmNotificationService {

    public static List<NotificationService.Notification> getNotifications(NotificationService.Notification request) {
        List<NotificationService.Notification> notificationList = new List<NotificationService.Notification>();
        List<FMNotificationServices__c> notificationServices = getNotificationProviders().values();

        for(FMNotificationServices__c serviceConfig :  notificationServices){
            Type classType = Type.forName( serviceConfig.NotificationServiceClass__c );
            IsNotificationService serviceProvider = (IsNotificationService)classType.newInstance();
            List<NotificationService.Notification> lstNotification = serviceProvider.getNotifications(request);
            for (NotificationService.Notification notification : lstNotification) {
                notification.className = serviceConfig.NotificationServiceClass__c;
            }
            notificationList.addAll(lstNotification);
        }
        return notificationList;
    }

    public static List<NotificationService.Notification> markRead(List<NotificationService.Notification> lstRequest) {
        if (lstRequest == NULL) {
            return lstRequest;
        }
        Map<String, List<NotificationService.Notification>> mapClassNameToRequests
                = new Map<String, List<NotificationService.Notification>>();
        Map<String, IsNotificationService> mapClassNameToService = new Map<String, IsNotificationService>();

        for (NotificationService.Notification req : lstRequest) {
            List<NotificationService.Notification> classRequests = mapClassNameToRequests.get(req.className);
            if (classRequests == NULL) {
                classRequests = new List<NotificationService.Notification>();
            }
            classRequests.add(req);
            mapClassNameToRequests.put(req.className, classRequests);

            if (!mapClassNameToService.containsKey(req.className)) {
                mapClassNameToService.put(
                    req.className, (IsNotificationService) Type.forName(req.className).newInstance()
                );
            }
        }

        lstRequest = new List<NotificationService.Notification>();

        for (String className : mapClassNameToService.keySet()) {
            lstRequest.addAll(mapClassNameToService.get(className).markRead(mapClassNameToRequests.get(className)));
        }

        return lstRequest;
    }

    private static Map<String, FMNotificationServices__c> getNotificationProviders() {
        return FMNotificationServices__c.getAll();
    }

}