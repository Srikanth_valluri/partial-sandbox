Public class InventoriesEOIController {


    public static final String query = ' SELECT '
                                    + ' Id, Name, Property_Name_2__c, Unit_Type__c, '
                                    + ' Unit_Location__c, Unit_Location__r.Name, Marketing_Name_Doc__r.Name, Floor__c, '
                                    + ' Property__c, Unit__c, Floor_Package_Name__c, ' 
                                    + ' Property_City__c, Marketing_Name__c, Property_Name__c, '
                                    + ' Bedroom_Type__c, Bedrooms__c, IPMS_Bedrooms__c, '
                                    + ' Release_Date__c, Status__c, Area__c, Special_Price_calc__c, '
                                    + ' (SELECT ID FROM Inventory_Users__r LIMIT 1) '
                                    + ' FROM Inventory__c ';
                                    
                                    
    private List <Inventory__c> inventoriesList;
    public List <InventoryWrapper> inventoryWrapperList { get; set; }
    public Boolean removeInventories;
    public Id eoiRecId;
    
    public InventoriesEOIController (ApexPages.StandardController controller) {
        eoiRecId = controller.getRecord().Id;
        
        EOI_Process__c eoiRecord = new EOI_Process__c ();
        eoiRecord = [ SELECT Status__c from EOI_Process__c WHERE ID =: eoiRecId LIMIT 1];
        inventoryWrapperList = new List <InventoryWrapper> ();
        inventoriesList = new List <Inventory__c> ();
        removeInventories = false;
        if (apexpages.currentpage().getparameters().containsKey ('Remove')) {
            removeInventories = true;
        }
        if (eoiRecord.Status__c != 'Draft' && eoiRecord.Status__c != 'Submitted' && eoiRecord.Status__c != 'Rejected') {
            reloadPage ();
        }
        else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, System.label.Allocate_Remove_EOI));
        }
    }
    
   
    
    public void AddOrRemoveInventories () {
        try {
            EOI_Process__c uAsgn = [Select Allocated_Unit_Numbers__c, Agency_Contact__c , Agency_Contact__r.Email, 
                                    CreatedBy.ID, Deal_Value__c from EOI_Process__c 
                                    where id= :eoiRecId];
            User u = new User ();
            try {
                if (uAsgn.Agency_Contact__c != NULL)
                    u = [SELECT Email FROM User WHERE ContactID =: uAsgn.Agency_Contact__c];
            }
            catch (Exception e) {}
            inventoriesList = new List<Inventory__c>();
            List <Inventory_User__c> invUserList = new List <Inventory_User__c> ();
            Set <ID> invIds = new Set <ID> ();
            
            for (InventoryWrapper invWrapLoopObj : inventoryWrapperList) { 
                if (invWrapLoopObj.isSelected == true) {
                    Inventory__c invRec = invWrapLoopObj.invObj;
                    invIds.add (invRec.Id);
                    if (removeInventories == false) {
                        invRec.Tagged_To_EOI__c = true;
                        invRec.EOI__c = eoiRecId;
                        inventoriesList.add(invRec);
                        Inventory_User__c invUser = new Inventory_User__c ();
                        invUser.Inventory__c = invRec.Id;
                        invUser.User__c = uAsgn.CreatedBy.ID;
                        invUser.EOI__c = eoiRecId;
                        invUser.Unique_Key__c = invRec.Id+'###'+uAsgn.CreatedBy.ID;
                        invUserList.add (invUser);
                    
                        if (uAsgn.CreatedBy.ID != u.ID) {
                            Inventory_User__c invUser1 = new Inventory_User__c ();
                            invUser1.Inventory__c = invRec.Id;
                            invUser1.User__c = u.ID;
                            invUser1.EOI__c = eoiRecId;
                            invUser1.Unique_Key__c = invRec.Id+'###'+u.ID;
                            invUserList.add (invUser1);
                        }
                    
                    } else {
                        
                        invRec.Tagged_To_EOI__c = False;
                        invRec.EOI__c = NULL;
                        
                        inventoriesList.add (invrec);
                    }
                    
                }
            }
            if (!inventoriesList.isEmpty()) {
                List <Inventory_Users_History__c> invHistory = new List <Inventory_Users_History__c> ();
                update inventoriesList;
                if (removeInventories == false) {
                    System.Debug (invUserList);
                    insert invUserList;
                } else {
                    invUserList = [ SELECT User__c, Inventory__c, EOI__c FROM Inventory_User__c WHERE EOI__C =: eoiRecId 
                                    AND Inventory__c IN : invIds 
                                    AND EOI__C != NULL ];
                    for (Inventory_User__c userVal :invUserList) {
                        Inventory_Users_History__c invUserHistory = new Inventory_Users_History__c ();
                        invUserHistory.User__c = userVal.User__c;
                        invUserHistory.Inventory__c = userVal.Inventory__c;
                        invUserHistory.EOI_Process__c = userVal.Eoi__c;
                        invHistory.add (invUserHistory);
                    }
                    
                    delete invUserList;
                    insert invHistory;
                    
                }
                aggregateResult[] groupedResults=[SELECT SUM(Selling_Price__c)sumPrice 
                                                  FROM 
                                                   Inventory__c 
                                                  WHERE 
                                                   EOI__C = :eoiRecId  
                                                  AND 
                                                    EOI__C  != null
                                                   ];  
                
                
                for (AggregateResult ar : groupedResults)  {
                    uAsgn.Deal_Value__c = (Decimal)ar.get('sumPrice');
                }
                uAsgn.Status__c = 'Allocated';
                if (removeInventories) {
                    String invQueryString   = query
                                    +  ' WHERE EOI__C != NULL AND EOI__C =: eoiRecId ORDER BY Id Asc Limit 999';
                    if (database.query (invQueryString).size () == 0) {
                        uAsgn.Status__c = 'Pending Allocation';
                    }
                }
                if (!removeInventories && uAsgn.Agency_Contact__c != NULL)
                    uAsgn.Send_EMail_Notification__c = true;
                uAsgn.Agency_Contact_Email__c = uAsgn.Agency_Contact__r.Email;
                        
                uAsgn.CC_Email__c = Label.EOI_CC_Email;
                String allocatedUnitNumbers = '';
                for (Inventory__c inv : [SELECT Unit_Location__r.Name FROM Inventory__c WHERE EOI__c != NULL AND EOI__c =:EOIrecID]) {
                    allocatedUnitNumbers += inv.Unit_Location__r.Name+', ';
                } 
                allocatedUnitNumbers = allocatedUnitNumbers.removeEND (', ');
                uAsgn.Allocated_Unit_Numbers__c = allocatedUnitNumbers;
                update uAsgn ;
                
                //sendToIPMS (eoiRecId, invIds, removeInventories);
                if (!removeInventories) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, System.label.Add_Inventory));
                } else {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, System.label.remove_Inventory));
                }
                reloadPage ();
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.label.Select_Inventory ));
            }
        } catch (Exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }
    }
    @Future (Callout = TRUE)
    public static void sendToIPMS (id eoiId, SET <ID> inventoryIds, Boolean removeInventories) {
        /*
        List <Inventory__c> assignedInventories = new List <Inventory__c> ();
        assignedInventories = [SELECT Release_ID__c,Unit_Assignment_Name__c,Unit_Assignment_Start_Date__c,Unit_Assignment_End_Date__c
                                FROM Inventory__c 
                                WHERE ID IN :inventoryIds];
        EOI_Process__c eoiRecord = new EOI_Process__c ();
        eoirecord = [ SELECT Name, CreatedById, CreatedBy.IPMS_Employee_ID__c FROM EOI_Process__c WHERE ID =: eoiId];
        if (assignedInventories.size() > 0) {
            createJSON (assignedInventories, eoiId, eoiRecord, removeInventories);
        }
        */
    }
    public static void createJSON (List <Inventory__c> units, String eoiRecId, EOI_Process__c eoi, Boolean removeInventory){ 
        /*
        EOIUpdate bodyClass = new EOIUpdate ();
        bodyClass.sourceSystem= 'SFDC';
        bodyClass.extRequestId = 'UA'+eoiRecId;
        List <EOIUpdate.inventories> items = new List <EOIUpdate.inventories>();
        for (Inventory__c iu :units){
            EOIUpdate.inventories item = new EOIUpdate.inventories ();            
            item.releaseId = iu.Release_ID__c;
            item.campaignId = eoiRecId;
            item.campaignName = eoi.Name;
            item.campaignStartDate = '';
            item.campaignEndDate = '';
            item.pcId = eoi.CreatedBy.IPMS_Employee_ID__c;
            if (removeInventory)
                item.pcId = '';
            items.add (item);
        }
        bodyClass.inventoryUnits = items;        
        String eoiReq = JSON.serializepretty(bodyClass);
        
        SendToIPMS (eoiReq, eoiRecId);
        */
    }
    
    public static void SendToIPMS (String eoiReqBody, String eoiId){
        /*
        try{
            IPMS_Integration_Settings__mdt ipms_Rest = [SELECT Endpoint_URL__c,
                        Password__c,  
                        Username__c 
                        FROM IPMS_Integration_Settings__mdt 
                        WHERE DeveloperName = 'IPMS_UPD_REST' LIMIT 1];
                        
            //String EndPointURL = ipms_Rest.Endpoint_URL__c+'process/campaign';
            String EndPointURL = 'http://94.200.40.200:8080/DCOFFEE/process/campaign';
            String SourceSystem = ipms_Rest.Username__c;            
            
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint (EndPointURL);
            req.setMethod ('POST');
            req.setheader ('Content-Type', 'application/json');
            req.setbody (eoiReqBody);
            System.Debug (EndPointURL);
            System.Debug ('---Request Body---'+eoiReqBody);
            HttpResponse res = h.send(req);
            System.Debug (res.getBody ());
            
            if(res.getStatusCode() == 201 ) {
                createLog (eoiId, res.getBody(), 'Success');
            } else {
                createLog (eoiId, res.getBody(), 'Error');
            }
        } catch(exception e){
        
            createLog (eoiId, String.valueof (e.getmessage()), 'Error');
        }
        */
    }
    public static void createLog (string parentId, String ErrorMessage, String type) {
        /*
        log__c log = new log__c ();
        log.EOI__c = parentId;
        log.Type__c = type;
        log.Description__c = ErrorMessage;
        insert log;
        */
    }
    
    
    /*
    public void reloadPage () {
        EOI_Process__c uAsgn =[Select Allocated_Unit_Numbers__c, CreatedBy.ID, Deal_Value__c,Property__c, Property_id__c from EOI_Process__c where id= :eoiRecId ]; 
        if (uAsgn.Property__c != null) {   
            string propId = uAsgn.Property_id__c;
            String invQueryString   = query;
            if (!removeInventories) {
                invQueryString += ' WHERE EOI__C = NULL '
                    //+ ' AND Release_ID__c != null '
                    + ' AND Tagged_to_EOI__c = false '
                    + ' AND Is_Assigned__c = false '
                    + ' AND ( Status__c = \'Apartments_In_Inventory\' OR Status__c = \'Restricted_Apartments_In_Inventory\' ) '
                    +  'AND Property_ID__c =\'' +propId+'\' ORDER BY Id Asc Limit 999';
            } else {
                invQueryString += 'WHERE EOI__C != NULL AND EOI__C =: eoiRecId ORDER BY Id Asc Limit 999';
            }
            system.debug('>>'+invQueryString);                        
            inventoryWrapperList = getInvenories (invQueryString);
        }
        
    }
    */
    
    public void reloadPage () {
        EOI_Process__c uAsgn =[Select Allocated_Unit_Numbers__c, CreatedBy.ID, Deal_Value__c,Property__c, Property_id__c,EOI_for_Project__c from EOI_Process__c where id= :eoiRecId ]; 
        if (uAsgn.EOI_for_Project__c != null) {   
            string propId = uAsgn.EOI_for_Project__c;
            String invQueryString   = query;
            if (!removeInventories) {
                invQueryString += ' WHERE EOI__C = NULL '
                    //+ ' AND Release_ID__c != null '
                    + ' AND Tagged_to_EOI__c = false '
                    + ' AND Is_Assigned__c = false '
                    + ' AND ( Status__c = \'Inventory\' OR Status__c = \'Restricted\' ) '
                    +  'AND Property_Name_2__c =\'' +propId+'\' ORDER BY Id Asc Limit 999';
            } else {
                invQueryString += 'WHERE EOI__C != NULL AND EOI__C =: eoiRecId ORDER BY Id Asc Limit 999';
            }
            system.debug('>>'+invQueryString);                        
            inventoryWrapperList = getInvenories (invQueryString);
        }
        
    }
    
    public List <InventoryWrapper> getInvenories (String queryString) {
        
        List <InventoryWrapper> inventoryWrapList = new List <InventoryWrapper>();
        List <Inventory_User__c> inventoryUsers = new List <Inventory_User__c> ();
        if (String.isNotBlank (queryString)) {
            for (Inventory__c invLoopObj : DataBase.Query (queryString)) { 
                inventoryUsers = invLoopObj.Inventory_Users__r;
                if (!removeInventories) {
                    if (inventoryUsers.size () == 0) {
                        InventoryWrapper invWrap = new InventoryWrapper(invLoopObj.Id, invLoopObj, false);
                        inventoryWrapList.add(invWrap);
                    }
                }
                else {
                    InventoryWrapper invWrap = new InventoryWrapper(invLoopObj.Id, invLoopObj, false);
                    inventoryWrapList.add(invWrap);
                }
            }            
        }
        return inventoryWrapList;

    }
    public Class InventoryWrapper {
        public Id recordId                                  { get; set; }
        public Inventory__c invObj                          { get; set; }
        public Boolean isSelected                           { get; set; }

        public InventoryWrapper(Id pRecordId, Inventory__c pInvObj, Boolean pIsSelected) {
            recordId = pRecordId;
            invObj = pInvObj;
            isSelected = pIsSelected;
        }
    }
}