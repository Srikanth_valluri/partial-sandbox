public class CaseExtensionController {
    @AuraEnabled
    public static Id getCaseExtensionId(String CaseId) {
      Id caseExtensionRecordId;
      if(String.isNotBlank(CaseId)) {
          List<Case> lstCase = [Select Id,RecordType.DeveloperName FROM Case where Id =:CaseId AND RecordType.DeveloperName IN ('EOI_Refund','Customer_Refund')];
          if(lstCase != Null && lstCase.size() > 0) {
              List<Case_Extension__c> objCaseExtension = [Select Id,Case__c from Case_Extension__c where Case__c =:lstCase[0].Id AND RecordType.DeveloperName IN ('EOI_Refund','Customer_Refund') Limit 1 ];
                if(objCaseExtension != Null && objCaseExtension.size() > 0) {
                    caseExtensionRecordId = objCaseExtension[0].Id;
                }
          }
      }
       return caseExtensionRecordId;
    }

}