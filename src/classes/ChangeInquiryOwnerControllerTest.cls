@isTest(seeAllData=false)
public class ChangeInquiryOwnerControllerTest {
@isTest static void changeInquiryOwnerTest()
     {
         Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
         Inquiry__c inqObj= new Inquiry__c(RecordTypeId = agenTeamRT,
         Pre_InquiryId__c='123456',Mobile_Phone_Encrypt__c = '456123'
                                           ,Mobile_CountryCode__c = 'American Samoa: 001684',
                                           Mobile_Phone__c = '1234',
                                           Email__c = 'mk@gmail.com',
                                           First_Name__c = 'Test',Last_Name__c='Last',
                                           CR_Number__c = '0987',
                                           Inquiry_Status__c = 'Potential Agent',
                                           Organisation_Name__c = 'Oliver');
         insert inqObj;
         ApexPages.StandardController sc = new ApexPages.standardController(inqObj);
         ChangeInquiryOwnerController changeInquiryObj = new ChangeInquiryOwnerController(sc);
         changeInquiryObj.updateInquiryOwnerPage();
		 changeInquiryObj.save();
     }
     @isTest static void changeInquiryOwnerTestNew()
     {
         Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
         Inquiry__c inqObj= new Inquiry__c(RecordTypeId = agenTeamRT,
                                           Pre_InquiryId__c = '123456',
                                           Mobile_Phone_Encrypt__c = '456123',
                                           Mobile_CountryCode__c = 'American Samoa: 001684',
                                           Bypass_Inquiry_Status__c = true,
                                           Mobile_Phone__c = '1234',
                                           Email__c = 'mk@gmail.com',
                                           First_Name__c = 'Test',
                                           Last_Name__c = 'Last',
                                           CR_Number__c = '0987',
                                           Inquiry_Status__c = 'Agent Converted',
                                           Organisation_Name__c = 'Oliver');
         insert inqObj;
         ApexPages.StandardController sc = new ApexPages.standardController(inqObj);
         ChangeInquiryOwnerController changeInquiryObj = new ChangeInquiryOwnerController(sc);
         changeInquiryObj.updateInquiryOwnerPage();
		 changeInquiryObj.save();
     }
      @isTest static void changeInquiryOwnerTestMethod()
     {
         Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
         Inquiry__c inqObj= new Inquiry__c(RecordTypeId = agenTeamRT,
                                           Pre_InquiryId__c = '123456',
                                           Mobile_Phone_Encrypt__c = '456123',
                                           Mobile_CountryCode__c = 'American Samoa: 001684',
                                           Mobile_Phone__c = '1234',
                                           Email__c = 'mk@gmail.com',
                                           First_Name__c = 'Test',
                                           Last_Name__c = 'Last',
                                           CR_Number__c = '0987',
                                           Inquiry_Status__c = 'Agent-Not a Lead',
                                           Organisation_Name__c = 'Oliver');
         insert inqObj;
         ApexPages.StandardController sc = new ApexPages.standardController(inqObj);
         ChangeInquiryOwnerController changeInquiryObj = new ChangeInquiryOwnerController(sc);
         changeInquiryObj.updateInquiryOwnerPage();
		     changeInquiryObj.save();
         changeInquiryObj.changerOwnerForTSE();
     }
}