@isTest
global with sharing class BitlyCalloutMockResponse implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {

        String responseBody = '{data={global_hash=2HNNzVY, hash=2HNNzFs,'
                            + 'long_url=https://fullcopy-damacproperties.cs87.force.com/ProjectDetails?'
                            + 'id=a1G0Y000000Vs6NUAS&uId=0058E000002lqNoQAI,'
                            + 'new_hash=0, url=https://dam.ac/2HNNzFs}, status_code=200, status_txt=OK}';

        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(responseBody);
        res.setStatusCode(200);
        return res;
    }
}