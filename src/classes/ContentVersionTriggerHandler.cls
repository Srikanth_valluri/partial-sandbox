public with sharing class ContentVersionTriggerHandler implements TriggerFactoryInterface{
    public ContentVersionTriggerHandler() {
        
    }

    public void executeBeforeInsertTrigger(list<sObject> lstNewRecords){}
    public void executeBeforeUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
    public void executeBeforeDeleteTrigger(Map<Id, sObject> mapOldRecords){}
    public void executeBeforeInsertUpdateTrigger(list<sObject> lstNewRecords, Map<Id,sObject> mapOldRecords){}
    
    //After Trigger Methods
    public void executeAfterInsertTrigger(Map<Id, sObject> mapNewRecords){
    }
    public void executeAfterUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){

        DocumentHelper.updateUnitDocAfterFileInsert((Map<Id, ContentVersion>)mapNewRecords);
    }
    public void executeAfterDeleteTrigger(Map<Id, sObject> mapOldRecords){}
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
}