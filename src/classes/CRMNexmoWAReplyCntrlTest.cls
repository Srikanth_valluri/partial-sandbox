@isTest
public class CRMNexmoWAReplyCntrlTest {
    @isTest
    static void testInit(){
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        //objAccount.Name = 'tset';
        objAccount.Mobile__c = '78945';
        objAccount.Mobile_Phone_Encrypt__pc = 'test';
        insert objAccount;
        
        CRM_Nexmo_Whats_App_Request__c objCRMNexmo = new CRM_Nexmo_Whats_App_Request__c();
        //objCRMNexmo.Name = 'test';
        objCRMNexmo.Last_Message_Sent__c = System.now();
        objCRMNexmo.Account__c = objAccount.Id;
        insert objCRMNexmo;
        
                
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(objCRMNexmo);
            PageReference pageRef = Page.crmNexmoWhatsapp;
            Test.setCurrentPage(pageRef); 
            pageRef.getParameters().put('Id', objCRMNexmo.Id);
            
            CRMNexmoWAReplyCntrl  testCRMNexmo = new CRMNexmoWAReplyCntrl(sc);
            testCRMNexmo.init();
        
        Test.stopTest();
    }
}