/*
Class Name : CSR_CasesPendingAprroval_API
Description : API class to get information regarding pending CSR cases for an approver
Test Class : CSR_CasesPendingAprroval_APITest

========================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
--------------------------------------------------------------------------------------------------------
1.0     |   11-11-2020      | Subin Antony        | Initial Draft
*******************************************************************************************************/
@RestResource(urlMapping='/csrCase/pending')
global class CSR_CasesPendingAprroval_API {
    public static Map<Integer, String> statusCodeMap;
    static {
        statusCodeMap = new Map<Integer, String>{
            1 => 'Success',
            2 => 'Failure',
            3 => 'Bad Request',
            6 => 'Something went wrong',
            7 => 'Info'
        };
    }
    
    @HttpGet
    global static void doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg, successMsg;
        
        String approverEmail = req.params.containskey('approver_email') ? req.params.get('approver_email') : '';
        if(String.isBlank(approverEmail)){
            errorMsg = 'Please provide approver_email.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
            return;
        }
        
        User approver;
        try{
            approver = [SELECT id, Name, email, profileId, profile.name FROM User WHERE email = :approverEmail LIMIT 1];
        }
        catch(Exception ex){
            system.debug(ex.getMessage() + ' : ' + ex.getStackTraceString());
            approver = NULL;
        }
        
        if(NULL == approver){
            errorMsg = 'The approver_email provided does not match any user records.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
            return;
        }
        System.debug('...................: approver: ' + approver.name);
        System.debug('...................: approverID: ' + approver.id);
        System.debug('...................: approverProfile: ' + approver.profile.name);
        
        Set<String> allowedStatusSet = new Set<String>{'Draft Request','Approved','Closed','Cancelled','Rejected'};
        List<Case> csrCases;
        try {
            csrCases = [SELECT id, caseNumber, recordType.name, recordType.developerName, status, 
                        Approving_User_Role__c, Approving_User_Name__c, Approving_User_Id__c, 
                        Account.Name, AccountId, CreatedDate, CreatedBy.name 
                        FROM Case WHERE recordType.name = 'Case Summary - Client Relation' 
                        AND status = 'Submitted' 
                        AND Approving_User_Id__c = :approver.id ORDER BY CreatedDate ASC]; /* status NOT IN :allowedStatusSet */
        }
        catch(Exception ex) {
            system.debug(ex.getMessage() + ' : ' + ex.getStackTraceString());
            errorMsg = 'Error while fetching assigned cases.';
            getErrorResponse(6, statusCodeMap.get(6), errorMsg, (ex.getMessage() + ' : ' + ex.getStackTraceString()));
            return;
        }
        System.debug('...................: CSR case count: ' + (NULL == csrCases ? 0 : csrCases.size()));
        System.debug('...................: CSR cases: ' + csrCases);
        
        String responseMessage;
        ResponseWrapper responseWrapper = new ResponseWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = 1;
        responseMetaData.title = statusCodeMap.get(1);
        
        cls_data responseData = new cls_data();
        
        if(NULL == csrCases || csrCases.size() < 1) {
            responseMessage = 'No cases pending for approval.';
            
            responseData.pending_approval_count = 0;
            responseData.pending_cases = new List<CaseDetail>();
        }
        else {
            responseMessage = 'Successfully fetched the cases pending approvl.';
            
            List<CaseDetail> csrPendingCaseList = new List<CaseDetail>();
            for(Case csrCase : csrCases) {
                CaseDetail csrPendingCase = new CaseDetail();
                csrPendingCase.case_id = csrCase.id;
                csrPendingCase.case_number = csrCase.caseNumber;
                csrPendingCase.case_status = csrCase.status;
                csrPendingCase.case_creation_date = csrCase.CreatedDate.format('yyyy-mm-dd'); // TODO
                
                csrPendingCaseList.add(csrPendingCase);
            }
            
            responseData.pending_approval_count = csrCases.size();
            responseData.pending_cases = csrPendingCaseList;
        }
        
        responseMetaData.message = responseMessage;
        responseMetaData.developer_message = null;
        responseWrapper.meta_data = responseMetaData;
        
        responseWrapper.data = responseData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    private static void getErrorResponse(Integer statusCode, String title, String responseMessage, String devMessage) {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = statusCode;
        responseMetaData.title = title;
        responseMetaData.message = responseMessage;
        responseMetaData.developer_message = devMessage;
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }

    /* Wrapper classes for returning reponse */
    public class ResponseWrapper {
        public cls_meta_data meta_data;
        public cls_data data;
    }

    public class cls_meta_data {
        public Integer status_code;
        public String message;
        public String title;
        public String developer_message;   
    }

    public class cls_data {
        public Integer pending_approval_count;
        public List<CaseDetail> pending_cases;
    }

    public class CaseDetail {
        public String case_id;
        public String case_number;
        public String case_status;
        public String case_creation_date;
    }
}