public class StepApprovalEmailTemplateController {
    
    public Id StepId{get; set;}
    public NSIBPM__Step__c step{
        get{
            if(StepId != null){
                if(step != null){
                    return step;
                }else{
                    for(NSIBPM__Step__c stp:[SELECT Id, Name, NSIBPM__Summary__c, RecordTypeId, NSIBPM__Step_Status__c, NSIBPM__SR__r.Total_Booking_Amount__c, NSIBPM__SR__r.OwnerId, NSIBPM__SR__r.Owner.Name, DOS_Email__c,
                                             RecordType.DeveloperName, OwnerId, Owner.Name, RecordType.Name, NSIBPM__Parent_Step__c, NSIBPM__SR__r.List_of_Units__c, NSIBPM__SR__r.NSIBPM__Record_Type_Name__c, HOS_Email__c,
                                             NSIBPM__SR__c, NSIBPM__SR__r.Name, NSIBPM__SR__r.NSIBPM__SR_Template__c, NSIBPM__Status__c, NSIBPM__SR_Step__c, NSIBPM__SR_Step__r.Approver_Role__c, Manager_Email__c, HOD_Email__c,
                                             NSIBPM__Rejection_Reason__c, NSIBPM__Status__r.Name ,NSIBPM__SR_Step__r.NSIBPM__Step_Template_Code__c, NSIBPM__SR_Step__r.NSIBPM__SR_Template__r.NSIBPM__SR_RecordType_API_Name__c,
                                             NSIBPM__SR__r.Agency_Name__c, NSIBPM__SR__r.Agency_Type__c, NSIBPM__SR__r.Id, NSIBPM__SR__r.First_Name__c, NSIBPM__SR__r.Last_Name__c, NSIBPM__SR__r.Agency__r.Name
                                             FROM NSIBPM__Step__c 
                                             WHERE Id =: StepId AND 
                                             NSIBPM__SR__c != null AND 
                                             NSIBPM__SR__r.NSIBPM__SR_Template__c != null AND 
                                             IsDeleted = false])
                    {
                        step = stp;
                    }
                    system.debug(step);
                    system.debug(step.NSIBPM__SR_Step__r.NSIBPM__SR_Template__r.NSIBPM__SR_RecordType_API_Name__c);
                    if(step.NSIBPM__SR_Step__r.Approver_Role__c != 'GM')
                    	ManagerEmail = String.valueOf(step.get((string.valueOf(step.NSIBPM__SR_Step__r.Approver_Role__c) != null ? string.valueOf(step.NSIBPM__SR_Step__r.Approver_Role__c) : 'HOS')+ '_Email__c'));
                    else{
                        for(User u : [SELECT Id, Name, Profile.Name, UserRole.Name, Email FROM User WHERE UserRole.Name = 'General Manager' LIMIT 1]){
                            ManagerEmail = u.Email;
                            break;
                        }
                    }
                    system.debug(ManagerEmail);
                    return step;
                   
                }
            }else{
                return new NSIBPM__Step__c();
            }
        }
        set;
    }
    public String ManagerEmail{get; set;}
    public User managerUser{
        get{
            if(ManagerEmail != null && ManagerEmail != ''){
                if(managerUser != null){
                    return managerUser;
                }else{
                    for(User u : [SELECT Id, Name, FirstName, LastName,LastModifiedDate FROM User WHERE Email =: ManagerEmail ORDER BY LastModifiedDate DESC LIMIT 1]){
                        managerUser = u;
                    }
                    system.debug(managerUser);
                    return managerUser;
                }
            }else{
                return new User();
            }
        }
        set;
    }
    public Integer iListSize {get; set;}
    public StepApprovalEmailTemplateController(){
        system.debug('####');
        iListSize = 0;
        ManagerEmail='';
    }
    }