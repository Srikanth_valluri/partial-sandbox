/*********************************************************************************
* Description - HTTP Mock callout for class GenerateDPSoaRest.                   *
*--------------------------------------------------------------------------------*
* Version            Date            Author         Description                  *
* 1.0                14/02/2019      Arjun Khatri   Initial Draft                *
**********************************************************************************/
@isTest
global Class DPSoaHttpMock implements HttpCalloutMock{
    integer resNumber;
    
    public DPSoaHttpMock() {}
    public DPSoaHttpMock(integer num) {
        resNumber = num;
    }
    global HTTPResponse respond(HTTPRequest req){
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type','application/json');
        if(resNumber == 1) {
            res.setStatusCode(200);
            res.setBody('{"responseId":"12244","responseTime":"Thu Feb 14 14:26:10 GST 2019","actions":[{"action":"VIEW_FILE","method":"GET","url":"testurl"}],"complete":true}');
        }
        else if(resNumber == 2) {
            res.setStatusCode(400);
            res.setBody('Can not generate SOA!');
        }
        else if(resNumber == 3) {
            res.setStatusCode(200);
            res.setBody('{"responseId":"12244","responseTime":"Thu Feb 14 14:26:10 GST 2019","actions":[],"complete":true}');
        }
        return res;
    }

}