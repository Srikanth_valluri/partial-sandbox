public without sharing class CreateShipmentBookingUnitsListCtrl{
    public String selectedCS{get;set;}
    public List<SelectOption> couServices {get;set;}
    ApexPages.StandardSetController setCon;
    public Map<Id,List<CourierDetails>> accCaseMap{get;set;}
    public Map<Id,List<CourierDetails>> accCaseMapWithSC{get;set;}
    public List<Id> accIdsWithSC{get;set;}
    public List<Id> accIds{get;set;}
    public Id jobId{get;set;}
    

    public static list<string> lstCourierOpenStatus = new list<string>{
        'Success'
    };

    public CreateShipmentBookingUnitsListCtrl(ApexPages.StandardSetController controller)
    {
        selectedCS = '';
        //jobId = new id();;
        couServices = new List<SelectOption>();
        couServices.add(new SelectOption('First Flight', 'First Flight'));
        //couServices.add(new SelectOption('Aramex', 'Aramex'));
        couServices.add(new SelectOption('Aramex', 'Aramex Regular Account'));
        couServices.add(new SelectOption('Aramex CDS', 'Aramex CDS'));
        setCon = controller;
        accCaseMap = new Map<Id,List<CourierDetails>>();
        accIds = new List<Id>();
        accCaseMapWithSC = new Map<Id,List<CourierDetails>>();
        accIdsWithSC = new List<Id>();
        List<Booking_Unit__c> lstBookingUnitsTemp = new List<Booking_Unit__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        lstBookingUnitsTemp = (Booking_Unit__c[])setCon.getSelected();
        system.debug('lstBookingUnitsTemp is '+lstBookingUnitsTemp);
        //List<Case> caseList = new List<Case>([SELECT Id,CaseNumber,Booking_Unit__c,Booking_Unit__r.Unit_Name__c,AccountId,RecordTypeName__c,Airway_Bill_Number__c,Account.Name,Courier_Request_Already_Created__c  FROM Case WHERE Id IN :caseIds]);
        //System.debug('Case List==> ' +caseList);
        
        for(Booking_Unit__c objBU : [select Id, Account_Id__c, Customer_Name__c, Unit_Name__c, Booking__r.Account__r.Name,
                Booking__r.Account__r.Master_Address__c, Booking__r.Account__r.Phone_1__c, Booking__r.Account__r.Country__c,
                Booking__r.Account__c,Booking__r.Account__r.Country__pc,Booking__r.Account__r.Mobile_Phone_Encrypt__pc,Booking__r.Account__r.IsPersonAccount,
                (select Id,Booking_Unit__r.Customer_Name__c,Booking_Unit__r.Account_Id__c, Booking_Unit__r.Unit_Name__c,
                Courier_Delivery__r.Type_of_Document__c, Courier_Delivery__r.Airway_Bill__c
                    from Courier_Items__r where Courier_Status__c IN : lstCourierOpenStatus ) 
                from Booking_Unit__c 
                where Id IN : lstBookingUnitsTemp])
        {
            if(objBU.Courier_Items__r.isEmpty()) {
                if(!accCaseMap.containsKey(objBU.Booking__r.Account__c)){
                    accCaseMap.put(objBU.Booking__r.Account__c, new list<CourierDetails>());
                }
                CourierDetails objCD = new CourierDetails();
                objCD.BookingUnit = objBU;
                objCD.CourierDelivery = new Courier_Delivery__c();
                objCD.AccountName = objBU.Booking__r.Account__r.Name;
                accCaseMap.get(objBU.Booking__r.Account__c).add(objCD);                
            } else {
                for(Courier_Item__c objCI : objBU.Courier_Items__r){
                    if(!accCaseMapWithSC.containsKey(objBU.Account_Id__c)){
                        accCaseMapWithSC.put(objBU.Account_Id__c, new list<CourierDetails>());
                    }
                    CourierDetails objCD = new CourierDetails();
                    objCD.BookingUnit = objBU;
                    objCD.CourierItem = objCI;
                    objCD.AccountName = objBU.Booking__r.Account__r.Name;
                    accCaseMapWithSC.get(objBU.Booking__r.Account__c).add(objCD);
                }                
            }
        }
        if(!accCaseMap.isEmpty())
            accIds.addAll(accCaseMap.keySet());
        if(!accCaseMapWithSC.isEmpty())
            accIdsWithSC.addAll(accCaseMapWithSC.keySet());
        system.debug('accCaseMap-->'+accCaseMap);
        system.debug('accIds-->'+accIds);
        system.debug('accCaseMapWithSC-->'+accCaseMapWithSC);
        system.debug('accIdsWithSC-->'+accIdsWithSC);
        
    }

    public void createRequest()
    {
        if(!accCaseMap.isEmpty()){
            for(Id AccountId : accCaseMap.keySet()){
                CourierDetails objCD = accCaseMap.get(AccountId)[0];
                if(objCD.CourierDelivery.Type_of_Document__c == null){
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select Type of Document to be delivered to '+objCD.AccountName));
                }
            }
        }
        if(ApexPages.hasMessages())
            return;
        CreateShipmentRequestBatch batchInst = new CreateShipmentRequestBatch(accCaseMap,selectedCS);
        jobId = Database.executeBatch(batchInst,1);
        
    }

    public class CourierDetails {
        public Booking_Unit__c BookingUnit {get;set;}
        public Courier_Delivery__c CourierDelivery {get;set;}
        public Courier_Item__c CourierItem {get;set;}
        public string AccountName {get;set;}
    }

}