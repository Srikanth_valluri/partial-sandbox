/****************************************************************************************
Description: Batch class to delete SignNow Template Settings created in last 1 day
----------------------------------------------------------------------------------------*
Version     Date          Author              Description                               *
1.0         19/08/2020    Akshata Anvekar     Initial Draft                             *
*****************************************************************************************/
global class DeleteSignNowTemplateSettingsBatch implements Database.Batchable<sObject> {
public final String strFilterCriteria = System.Label.SignNowTemplateSettingsBatch_Filter_Criteria;
public final String strCronExp = System.Label.SignNowTemplateSettingsBatch_Cron_Expression;
    
    /* Start method to fetch Template Settings
     * created yesterday
     * Input Parameters : Database.BatchableContext
     * Return Type : Database.QueryLocator
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id '+
            			'FROM cuda_signnow__CN_TemplateSetting__c '+
            			'WHERE CreatedDate = '+strFilterCriteria;
        return Database.getQueryLocator(query);
    }
    
    /* Start method to fetch Template Settings
     * created in last 7 days
     * Input Parameters : Database.BatchableContext, Template Settings List
     */
    global void execute(Database.BatchableContext BC, List<cuda_signnow__CN_TemplateSetting__c> lstTemplateSettings) {
        try {
            if(!lstTemplateSettings.isEmpty()){
            	// Delete the Template Settings
            	delete lstTemplateSettings;    
            }
        } catch(Exception exp) {
            System.debug('Exception==>'+exp);
        }
         
    }   
    
    /* Finish method to schedule batch 
     * after 24 hrs
     * Input Parameters : Database.BatchableContext
     */
    global void finish(Database.BatchableContext BC) {
        //Below code will fetch the job Id
        AsyncApexJob a = [Select a.TotalJobItems, a.Status, a.NumberOfErrors, a.JobType, a.JobItemsProcessed, a.ExtendedStatus, a.CreatedById, a.CompletedDate From AsyncApexJob a WHERE id = :BC.getJobId()];//get the job Id
        
        if(a.Status == 'Completed'){
        
            List<CronTrigger> scheduledJobs = [Select id from cronTrigger where CronJobDetail.name = 'Delete Template Settings Scheduler'];
            
            if(scheduledJobs.isEmpty()){
                //Decimal minutes=Decimal.valueOf(strCronExp);
                Decimal minutes=24*60;
                Decimal chunkSize=200;
                System.scheduleBatch(new DeleteSignNowTemplateSettingsBatch(), 'Delete Template Settings Scheduler', minutes.intValue(), chunkSize.intValue());
            }
    	}
    }
}