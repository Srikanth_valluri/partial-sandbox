/******************************************************************************
* Description - Test class developed for MakeEarlyHandoverMQService1Callout
*
* Version            Date            Author                    Description
* 1.0                17/12/17        Naresh Kaneriya (Accely)   Initial Draft
********************************************************************************/

@isTest
public class MakeEarlyHandoverMQService1CalloutTest{

  @isTest static void getTest(){
      Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
      Id LeaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
	  SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.UPDATE_EARLY_HO_FLAGResponse_element>();
      EarlyHandoverMQService1.UPDATE_EARLY_HO_FLAGResponse_element response1 = new EarlyHandoverMQService1.UPDATE_EARLY_HO_FLAGResponse_element();
      response1.return_x ='{'+
		'			"lstReg" : ['+
		'			{'+
		'				"ATTRIBUTE1":"HandoverStat",'+
		'            "ATTRIBUTE10":"Test",'+
		'            "ATTRIBUTE11":"Test",'+
		'            "ATTRIBUTE12":"Test",'+
		'            "ATTRIBUTE13":"Test",'+
		'            "ATTRIBUTE14":"Test",'+
		'            "ATTRIBUTE15":"Test",'+
		'            "ATTRIBUTE16":"Test",'+
		'            "ATTRIBUTE17":"Test",'+
		'            "ATTRIBUTE18":"Test",'+
		'            "ATTRIBUTE19":"Test",'+
		'            "ATTRIBUTE2":"EHOStat",'+
		'            "ATTRIBUTE20":"Test",'+
		'            "ATTRIBUTE21":"Test",'+
		'            "ATTRIBUTE22":"Test",'+
		'            "ATTRIBUTE23":"Test",'+
		'            "ATTRIBUTE24":"Test",'+
		'            "ATTRIBUTE25":"Test",'+
		'            "ATTRIBUTE26":"Test",'+
		'            "ATTRIBUTE27":"Test",'+
		'            "ATTRIBUTE28":"Test",'+
		'            "ATTRIBUTE29":"Test",'+
		'            "ATTRIBUTE3":"Test",'+
		'            "ATTRIBUTE30":"Test",'+
		'            "ATTRIBUTE31":"Test",'+
		'            "ATTRIBUTE32":"Test",'+
		'            "ATTRIBUTE33":"Test",'+
		'            "ATTRIBUTE34":"Test",'+
		'            "ATTRIBUTE35":"Test",'+
		'            "ATTRIBUTE36":"Test",'+
		'            "ATTRIBUTE37":"Test",'+
		'            "ATTRIBUTE38":"Test",'+
		'            "ATTRIBUTE39":"Test",'+
		'            "ATTRIBUTE4":"Test",'+
		'            "ATTRIBUTE41":"Test",'+
		'            "ATTRIBUTE42":"Test",'+
		'            "ATTRIBUTE43":"Test",'+
		'            "ATTRIBUTE44":"Test",'+
		'            "ATTRIBUTE45":"Test",'+
		'            "ATTRIBUTE46":"Test",'+
		'            "ATTRIBUTE47":"Test",'+
		'            "ATTRIBUTE48":"Test",'+
		'            "ATTRIBUTE49":"Test",'+
		'            "ATTRIBUTE5":"Test",'+
		'            "ATTRIBUTE50":"Test",'+
		'            "ATTRIBUTE6":"Test",'+
		'            "ATTRIBUTE7":"Test",'+
		'            "ATTRIBUTE8":"Test",'+
		'            "ATTRIBUTE9":"Test",'+
		'            "PARAM_ID":"reg1"'+
		'			}'+
		'			 ],'+
		'			 "P_REQUEST_NUMBER" : "1234",'+
		'			 "P_REQUEST_NAME" : "Test",'+
		'			 "P_SOURCE_SYSTEM" : "SFDC"'+
		'		}';
      SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
      Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        Test.startTest();
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
     	
     	Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);
        
        Case Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Type = 'Early Handover';
        insert Cas;
        
        Case lhoCase = TestDataFactory_CRM.createCase(Acc.Id,LeaseRecordTypeId);
        lhoCase.Type = 'Lease Handover';
        lhoCase.Booking_Unit__c = BU.Id;
        lhoCase.Call_Outcome__c = 'Lease Handover without Rental Guarantee';
        insert lhoCase;
        System.assert(Cas != null);
         
          
        
        List<Id> lstCaseId  =  new List<Id>();
        lstCaseId.add(Cas.Id);
        lstCaseId.add(lhoCase.Id);
        MakeEarlyHandoverMQService1Callout.invokeapexcallout(lstCaseId);
        Test.stopTest();
  
  }


}