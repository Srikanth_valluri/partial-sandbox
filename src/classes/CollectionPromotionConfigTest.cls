@isTest
public Class CollectionPromotionConfigTest {
    
    @isTest
    public static void testAllRules() {
        Account objAcc = new Account(Name='test'
                                    , Customer_Satisfaction__c = 2);
        insert objAcc;
                
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        objBooking.Poll_for_SOA_Count__c = 10;
        insert objBooking;
        
        Booking_Unit_Active_Status__c buStatusObj = new Booking_Unit_Active_Status__c();
        buStatusObj.Name = 'Agreement Rejected By Sales Admin';
        buStatusObj.Status_Value__c  = 'Agreement Rejected By Sales Admin';
        insert buStatusObj;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_DateTime__c = System.now().addDays( 185 );
        objBookingUnit.Requested_Price__c = 200;
        insert objBookingUnit;
        
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Waiver').getRecordTypeId();

        Case objCase = new case( AccountId = objAcc.Id
                                , recordTypeId = recTypeId
                                , Waiver_Amount_AED__c = 20
                                , Booking_Unit__c = objBookingUnit.Id);
        insert objCase;
        
        Test.startTest();
        CollectionPromotionConfig.filterSfRuleEngine('Test ( Do Not Delete )',objCase.Id);
        Test.stopTest();
    }
    
    @isTest
    public static void testGetPromotionCategory() {
        List<Collection_Promotion_Config__mdt> listPromConfig = new List<Collection_Promotion_Config__mdt>();  
            listPromConfig = [SELECT
                                    Discount__c
                                    , Discount_Category__c
                                    , Discount_Type__c
                                    , Offer_Target__c
                                    , Process_Name__c
                                    , Rule_Number__c
                                    , Object_API_Name__c
                                    , Offer_From__c
                                FROM
                                    Collection_Promotion_Config__mdt
                                LIMIT 1];
           Test.startTest();
           CollectionPromotionConfig.getPromotionCategory( listPromConfig[0].Rule_Number__c, listPromConfig[0].Process_Name__c, listPromConfig[0].Object_API_Name__c);
            Test.stopTest();
    }
}