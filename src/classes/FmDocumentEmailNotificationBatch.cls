global without sharing class FmDocumentEmailNotificationBatch implements Database.Batchable<String>, Database.AllowsCallouts {

    String query;

    Map<String, Set<Id>> mapRecipientEmailToSetBatchId;
    Map<Id, FM_Document_Upload_Batch__c> mapDocBatchWithDocument;
    Map<String, Set<Id>> mapRecipientEmailToSetPropertyId;
    Map<String, Set<Id>> mapRecipientEmailToSetBuildingId;
    Map<String, String> mapRecipientEmailToBuildingEmail;
    Map<String, String> mapRecipientEmailToPropertyName;
    Map<String, Id> mapRecipientAddressToAccountId;

    global FmDocumentEmailNotificationBatch(
        Map<String, Set<Id>> mapRecipientEmailToSetBatchId,
        Map<Id, FM_Document_Upload_Batch__c> mapDocBatchWithDocument,
        Map<String, Set<Id>> mapRecipientEmailToSetPropertyId,
        Map<String, Set<Id>> mapRecipientEmailToSetBuildingId,
        Map<String, String> mapRecipientEmailToBuildingEmail,
        Map<String, String> mapRecipientEmailToPropertyName,
        Map<String, Id> mapRecipientAddressToAccountId
    ) {
        this.mapRecipientEmailToSetBatchId = mapRecipientEmailToSetBatchId;
        this.mapDocBatchWithDocument = mapDocBatchWithDocument;
        this.mapRecipientEmailToSetPropertyId = mapRecipientEmailToSetPropertyId;
        this.mapRecipientEmailToSetBuildingId = mapRecipientEmailToSetBuildingId;
        this.mapRecipientEmailToBuildingEmail = mapRecipientEmailToBuildingEmail;
        this.mapRecipientEmailToPropertyName = mapRecipientEmailToPropertyName;
        this.mapRecipientAddressToAccountId = mapRecipientAddressToAccountId;
        //System.debug('mapRecipientEmailToSetBatchId = ' + mapRecipientEmailToSetBatchId);
    }

    global Iterable<String> start(Database.BatchableContext BC) {
        return new List<String>(mapRecipientEmailToSetBatchId.keySet());
    }

    global void execute(Database.BatchableContext BC, List<String> scope) {
        List<EmailMessage> lstEmail = new List<EmailMessage>();
        List<Error_Log__c> lstErrors = new List<Error_Log__c>();
        Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;
        List<OrgWideEmailAddress> orgWideAddressHelloDamac = [
            SELECT  Id
                    , Address
                    , DisplayName
            FROM    OrgWideEmailAddress
            WHERE   Id = :Label.NoReplyAtLoams
        ];

        if (Test.isRunningTest()) {
            Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        }

        for (String recipientEmail : scope) {
            //System.debug('recipientEmail = ' + recipientEmail);
            if (isSandbox
                && !recipientEmail.containsIgnoreCase('davidvarghese1989@gmail.com')
                && !recipientEmail.containsIgnoreCase('@damacgroup.com')
                && !recipientEmail.containsIgnoreCase('@eternussolutions.com')
                && !recipientEmail.containsIgnoreCase('@mailinator.com')) {
                continue;
            }

            Set<Id> setBatchId = mapRecipientEmailToSetBatchId.get(recipientEmail);

            String mailSubject = 'New FM Document Upload Notification';

            String htmlBody = '';

            htmlBody += '<head>' +
                            '<style type="text/css">' +
                                'table, thead, tbody, tr, th, td {' +
                                    'border:1px solid;' +
                                '}' +
                                'table {' +
                                    'border-collapse: collapse;' +
                                    'font-family: "Trebuchet MS", Helvetica, sans-serif;' +
                                '}' +
                                'thead {' +
                                    'background: #D3D3D3;' +
                                    'background: #36304a;' +
                                '}' +
                                'th {' +
                                    'color: #fff;' +
                                    'font-weight: normal;' +
                                '}' +
                                'tbody tr:nth-child(even) {' +
                                    'background-color: #f5f5f5;' +
                                '}' +
                                'tbody tr {' +
                                    'color: #555;' +
                                '}' +
                            '</style>' +
                        '</head>';

            String processName, fmCaseId;

            for (Id batchId : setBatchId) {
                FM_Document_Upload_Batch__c docBatch = mapDocBatchWithDocument.get(batchId);
                processName = docBatch.Process_Name__c;
                fmCaseId = docBatch.Related_to_Id__c;
                //System.debug('docBatch.FM_Recipient__c = ' + docBatch.FM_Recipient__c);
                //System.debug('docBatch.Document_Level__c = ' + docBatch.Document_Level__c);

                if (String.isNotBlank(docBatch.Notification_Email_Subject__c)) {
                    mailSubject = docBatch.Notification_Email_Subject__c;
                }

                Map<String, Set<Id>> mapEmailToSetId = new Map<String, Set<Id>>();
                if ('Property Specific'.equalsIgnoreCase(docBatch.Document_Level__c)) {
                    mapEmailToSetId = mapRecipientEmailToSetPropertyId;
                } else if ('Building Specific'.equalsIgnoreCase(docBatch.Document_Level__c)) {
                    mapEmailToSetId = mapRecipientEmailToSetBuildingId;
                }

                htmlBody += String.isNotBlank(docBatch.Notification_Email_Message__c)
                            ? (docBatch.Notification_Email_Message__c) : '';

                htmlBody += '<br/><br/><br/>';

                htmlBody += '<table>' +
                                '<thead>' +
                                    '<tr>' +
                                        '<th>Sr. No</th>' +
                                        '<th>Property Name</th>' +
                                        '<th>Building Name</th>' +
                                        '<th>Document Name</th>' +
                                        '<th>Document Type</th>' +
                                        '<th>Document URL</th>' +
                                    '</tr>' +
                                '</thead>';
                htmlBody += '<tbody>';

                Integer docNumber = 1;
                for (SR_Attachments__c doc : docBatch.Documents__r) {
                    Id relatedId = 'Property Specific'.equalsIgnoreCase(docBatch.Document_Level__c)
                                    ? doc.Property__c
                                    : ('Building Specific'.equalsIgnoreCase(docBatch.Document_Level__c)
                                        ? doc.Building__c : NULL
                                    );

                    Set<Id> setRelatedId = mapEmailToSetId.get(recipientEmail);
                    if (setRelatedId == NULL || !setRelatedId.contains(relatedId)) {
                        continue;
                    }

                    //System.debug('doc.Property__c = ' + ((doc.Property__c == NULL) ? '' : doc.Property__r.Name));
                    //System.debug('doc.Building__c = ' + ((doc.Building__c == NULL) ? '' : doc.Building__r.Name));

                    htmlBody +=
                            '<tr>' +
                                '<td>' + (docNumber++) + '</td>' +
                                '<td>'
                                    + ('Property Specific'.equalsIgnoreCase(docBatch.Document_Level__c)
                                        ? doc.Property__r.Name
                                        : ('Building Specific'.equalsIgnoreCase(docBatch.Document_Level__c)
                                            ? doc.Building__r.Property_Name__r.Name : ''
                                        )
                                    ) +
                                    '</td>' +
                                '<td>'
                                    + ('Building Specific'.equalsIgnoreCase(docBatch.Document_Level__c)
                                        ? (doc.Building__r.Name +
                                            (String.isBlank(doc.Building__r.Building_Name__c)
                                                ? '' : (' - ' + doc.Building__r.Building_Name__c))
                                        ) : '') +
                                '</td>' +
                                '<td>' + doc.Name + '</td>' +
                                '<td>' + doc.Document_Type__c + '</td>' +
                                '<td><a href="' + doc.Attachment_URL__c + '">' + doc.Attachment_URL__c + '</a></td>' +
                            '</tr>';
                }

                htmlBody += '</tbody>' +
                        '</table>';

                //SendGridIterable.sendGridEmail objWrapper = new SendGridIterable.sendGridEmail();
                String toAddress = recipientEmail;
                String toName = '';
                String ccAddress = '';
                String ccName = '';
                String bccAddress = '';
                String bccName = '';
                String subject = mailSubject;
                String substitutions = '';
                String fromAddress = orgWideAddressHelloDamac[0].Address;
                String fromName = mapRecipientEmailToPropertyName.containsKey(recipientEmail) ? mapRecipientEmailToPropertyName.get(recipientEmail)
                    : orgWideAddressHelloDamac[0].DisplayName;
                String replyToAddress = '';
                String replyToName = mapRecipientEmailToPropertyName.containsKey(recipientEmail) ? mapRecipientEmailToPropertyName.get(recipientEmail)
                    : orgWideAddressHelloDamac[0].DisplayName;
                String contentType = 'text/html';
                String contentValue = htmlBody;
                String templateId = '';
                String relatedId = String.isNotBlank(fmCaseId) ? fmCaseId : NULL;
                String buId = NULL;

                if (String.isNotBlank(recipientEmail) && mapRecipientEmailToBuildingEmail.containsKey(recipientEmail)) {
                    //System.Debug('===mapRecipientEmailToBuildingEmail===' + mapRecipientEmailToBuildingEmail);
                    //System.Debug('===recipientEmail===' + recipientEmail);
                    String loamsEmail = mapRecipientEmailToBuildingEmail.get(recipientEmail);
                    if(String.isNotBlank(loamsEmail)) {
                        replyToAddress = loamsEmail;
                        fromAddress = loamsEmail.subStringBefore('@');
                        fromAddress = fromAddress + '@sg.' + loamsEmail.substringAfter('@');
                    }
                    //System.Debug('===fromAddress===' + fromAddress);
                }

                String accountId = mapRecipientAddressToAccountId.get(toAddress);

                SendGridEmailService.SendGridResponse response;
                if (Test.isRunningTest()) {
                    response = new SendGridEmailService.SendGridResponse();
                    response.ResponseStatus = 'Accepted';
                    response.messageId = '123';
                } else {
                    response = SendGridEmailService.sendEmailService(
                        toAddress, toName, ccAddress, ccName, bccAddress, bccName, subject, substitutions, fromAddress,
                        fromName, replyToAddress, replyToName, contentType, contentValue, templateId,
                        new List<Attachment>()
                    );
                }


                if (response.ResponseStatus == 'Accepted') {
                    EmailMessage mail = new EmailMessage();
                    mail.Subject = subject;
                    mail.MessageDate = System.Today();
                    mail.Status = '3';
                    //mail.RelatedToId = relatedId;//Put FM case id
                    mail.RelatedToId = String.isBlank(accountId) ? NULL : accountId;
                    mail.ToAddress = toAddress;
                    mail.FromAddress = fromAddress;
                    mail.HtmlBody = contentValue;
                    //mail.TextBody = contentValue;
                    mail.CcAddress = ccAddress;
                    mail.BccAddress = bccAddress;
                    mail.Sent_By_Sendgrid__c = true;
                    mail.Sendgrid_Status__c = response.ResponseStatus;
                    mail.SentGrid_MessageId__c = response.messageId;
                    mail.Booking_Unit__c = buId == '' ? NULL : buId;
                    mail.Account__c = String.isBlank(accountId) ? NULL : accountId;
                    lstEmail.add(mail);
                } else {
                    Error_Log__c objError = new Error_Log__c();
                    objError.Account__c = String.isBlank(accountId) ? NULL : accountId;
                    objError.Error_Details__c = 'Request Failed from SendGrid';
                    objError.Process_Name__c = 'Generic Email';
                    lstErrors.add(objError);
                }
            }

        }

        List<Database.SaveResult> lstEmailSaveResult = Database.insert(lstEmail, false);
        Integer counter = 0;
        for (Database.SaveResult emailSaveResult : lstEmailSaveResult) {
            if (!emailSaveResult.isSuccess()) {
                String error = '';
                for (Database.Error dbError : emailSaveResult.getErrors()) {
                    error += dbError.getMessage() + '\n';
                }
                error = error.removeEnd('\n');

                lstErrors.add(new Error_Log__c(
                    Account__c = lstEmail[counter].Account__c,
                    Error_Details__c = 'Error in Saving SendGrid Email to Salesforce \n\n' + error,
                    Process_Name__c = 'Generic Email'
                ));
            }
            counter++;
        }
        Database.insert(lstErrors, false);
    }

    global void finish(Database.BatchableContext BC) {

    }

}