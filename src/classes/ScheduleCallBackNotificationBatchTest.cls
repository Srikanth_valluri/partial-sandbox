/*
* Description - Test class developed for ScheduleUpdateCustomerFlagonCallingList
*
* Version            Date            Author            Description
* 1.0               8/01/2018                             Initial Draft
*/
@isTest
public class ScheduleCallBackNotificationBatchTest {
      static testMethod void testExecute() {
        Test.startTest();
            ScheduleCallBackNotificationBatch objScheduler = new ScheduleCallBackNotificationBatch();
            String schCron = '0 30 8 1/1 * ? *';
            String jobId = System.Schedule('Reccord Updated',schCron,objScheduler);
        Test.stopTest();
        CronTrigger cronTrigger = [
            SELECT Id
                 , CronExpression
                 , TimesTriggered
                 , NextFireTime
            FROM CronTrigger
           WHERE id = :jobId
        ];
        System.assertEquals( schCron,  cronTrigger.CronExpression  , 'Cron expression should match' );
        System.assertEquals( 0, cronTrigger.TimesTriggered  , 'Time to Triggered batch should be 0' );
        List<AsyncapexJob> apexJobs = [SELECT Id, 
                                              ApexClassID ,
                                              JobType , 
                                              Status 
                                         FROM AsyncapexJob
                                        WHERE JobType = 'BatchApex'];
        System.assertEquals( false , apexJobs.isEmpty()  , 'Expected at least one Async apex Job' );
        System.assertEquals( 'BatchApex', apexJobs[0].JobType  , 'Job Type should match ' );
    }
}