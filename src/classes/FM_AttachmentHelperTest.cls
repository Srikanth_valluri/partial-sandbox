/*-------------------------------------------------------------------------------------------------
Description: Test class for FM_AttachmentHelper
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
v1.0     | 24-01-2019       | Lochana Rajput   | 1. Initial draft
=============================================================================================================================
*/
@isTest
public class FM_AttachmentHelperTest {

    public static testmethod void testController(){

        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('NOC For FitOut').getRecordTypeId();

        FM_Case__c obj=new FM_Case__c();
        obj.Status__c='New';
        obj.Origin__c='Call';
        obj.recordtypeId = devRecordTypeId;
        insert obj;

        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.FM_Case__c = obj.Id;
        objAttach.isValid__c = true;
        objAttach.Description__c = 'Abctd';
        objAttach.IsRequired__c = True;
        objAttach.Name = 'Test';
        objAttach.Need_Correction__c =  False;
        //insert objAttach;

        List<Attachment> attachList = new List<Attachment>();
        for(Integer i=0;i<5;i++){
            Attachment attobj=new Attachment();
            attobj.Name='NOC Document';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attobj.body=bodyBlob;
            attobj.ContentType = 'application/pdf';
            attobj.parentId=obj.id;
            attachList.add(attobj);
        }
        insert attachList;

        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        Test.stopTest();
        FM_AttachmentHelper objDocExt = new FM_AttachmentHelper();
        FM_AttachmentHelper.DocWrapper objDocWrapper = new FM_AttachmentHelper.DocWrapper(obj.name,1,string.valueof(system.today()),objAttach);

        FM_AttachmentHelper.createFM_SR_Attachment(attachList);
        /*SR_Attachments__c insertedDoc = new SR_Attachments__c();
        insertedDoc = [SELECT id
                       FROM SR_Attachments__c];
        system.debug('insertedDoc*****'+insertedDoc);*/
    }

    public static testmethod void testControllerForBU(){

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        Case objCase = new Case();
        objCase.Account = acc;
        objCase.Status = 'Submitted';
        objCase.Approval_Status__c = 'Approved';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.NewPaymentTermJSON__c = '[{"strTermID":"145486","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"98822.84","strLineID":"808896","strInvoiceAmount":"98860.8","strDueAmount":"37.96","percentValue":"24","paymentDate":"06-DEC-2015","name":"Date","mileStoneEventArabic":"فورا","mileStoneEvent":"Immediate","isReceiptPresent":true,"installment":"DP","description":"DEPOSIT","blnNewTerm":null},{"strTermID":"145487","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":null,"strLineID":"808897","strInvoiceAmount":null,"strDueAmount":null,"percentValue":"0","paymentDate":"","name":"Date","mileStoneEventArabic":null,"mileStoneEvent":null,"isReceiptPresent":true,"installment":"I001","description":"1ST INSTALMENT","blnNewTerm":null},{"strTermID":"145488","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"0","strLineID":"808898","strInvoiceAmount":"49430.4","strDueAmount":"49430.4","percentValue":"10","paymentDate":"03-JUN-2016","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I002","description":"2ND INSTALMENT","blnNewTerm":null},{"strTermID":"145489","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"0","strLineID":"808899","strInvoiceAmount":"49430.4","strDueAmount":"49430.4","percentValue":"10","paymentDate":"02-AUG-2016","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I003","description":"3RD INSTALMENT","blnNewTerm":null},{"strTermID":"145490","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"0","strLineID":"808900","strInvoiceAmount":"49430.4","strDueAmount":"49430.4","percentValue":"10","paymentDate":"30-NOV-2016","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I004","description":"4TH INSTALMENT","blnNewTerm":null},{"strTermID":"145491","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":null,"strLineID":"808901","strInvoiceAmount":null,"strDueAmount":null,"percentValue":"50","paymentDate":"30/11/2017","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I005","description":"5TH INSTALMENT","blnNewTerm":null}]';
        objCase.Type = 'Question';  
        objCase.Account_Email__c = 'test@gmail.com';
        insert objCase;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

            //create Deal SR record
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR;

        //create Booking record for above created Deal and Account
            bookingList = TestDataFactory_CRM.createBookingForAccount(acc.Id,objDealSR.Id,1);
            insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC';
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
        }
        insert bookingUnitList;

        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Booking_Unit__c = bookingUnitList[0].Id;
        objAttach.isValid__c = true;
        objAttach.Description__c = 'Abctd';
        objAttach.IsRequired__c = True;
        objAttach.Name = 'Test';
        objAttach.Need_Correction__c =  False;
        //insert objAttach;

        List<Attachment> attachList = new List<Attachment>();
        for(Integer i=0;i<5;i++){
            Attachment attobj=new Attachment();
            attobj.Name='NOC Document';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attobj.body=bodyBlob;
            attobj.ContentType = 'application/pdf';
            attobj.parentId= bookingUnitList[0].Id;
            attachList.add(attobj);
        }
        insert attachList;

        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        Test.stopTest();
        FM_AttachmentHelper objDocExt = new FM_AttachmentHelper();
        FM_AttachmentHelper.DocWrapper objDocWrapper = new FM_AttachmentHelper.DocWrapper(bookingUnitList[0].name,1,string.valueof(system.today()),objAttach);

        FM_AttachmentHelper.createFM_SR_Attachment(attachList);
        /*SR_Attachments__c insertedDoc = new SR_Attachments__c();
        insertedDoc = [SELECT id
                       FROM SR_Attachments__c];
        system.debug('insertedDoc*****'+insertedDoc);*/
    }
}