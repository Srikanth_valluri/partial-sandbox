/*
Pratiksha Narvekar

*/

@isTest
public  class AgentPortalIntEmail_Test {
     @isTest static void test_method_1() {
        Test.startTest();
        Inventory__c inv = new Inventory__c();
	    inv.Inventory_ID__c = '1';
	    inv.Building_ID__c = '2';
	    inv.UNIT__C = 'ABCD/2';
	    inv.Marketing_Name__c ='test';
	    inv.Floor__C = 'abcd/2';
	    inv.Floor_ID__c = '3';
	    inv.Address_Id__c = '4' ;
	    inv.Property_ID__c = '5' ;
	    inv.Status__c='Released';
	    inv.Unit_ID__c= '6';
        insert inv;
        
        PageReference pageRef = Page.DAMAC_InventoryDetails;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(inv);
        ApexPages.currentPage().getParameters().put('Id',inv.id);
        AgentPortalIntEmail cls = new AgentPortalIntEmail(sc);
        Blob content = Blob.valueOf('UNIT.TEST');
        cls.SendEmail();
        Test.stopTest();
     }
}