@RestResource(urlMapping='/insertStandInquiry/*')
global class InsertFailureInquiry{
    @HttpPost 
    global static void InsertFailureInquiryRecords(){
        RestResponse res = RestContext.response;
        try{
            string nsuuId = '';
            RestRequest req = RestContext.request;
            system.debug(req.requestBody.tostring());
            system.debug(req.headers);
            string datestring = '';
            map<string,object> m = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
            list<Stand_Inquiry__c> newInsesertList = new list<Stand_Inquiry__c>();
            system.debug('m>>>'+m);
            List <String> response = New List <String> ();
            for (Inquiry_Checks__c rec :[SELECT Email__c FROM Inquiry_Checks__c WHERE Email__c != NULL]) {
                response.add (rec.Email__c);
            }
            List<User> lstuser = [Select Id,Stand_Id__c FROM User WHERE Id=:UserInfo.getUserId() limit 1];    
            for(object obj :(list<object>)m.get('sObject')){
                Stand_Inquiry__c newData = new Stand_Inquiry__c();
                map<string,object> mapOffileds = (map<string,object>)obj;
                for(string str : mapOffileds.keyset()){
                    System.debug (str);
                    if ( str == 'Tour_Date_Time__c' ) {
                        TimeZone tz = UserInfo.getTimeZone();
                        datestring = ((string)(mapOffileds.get(str))).replace('T',' ');
                        dateTime dt = datetime.valueOfGMT(((string)(mapOffileds.get(str))).replace('T',' '))- tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
                        newData.put(str,dt);
                    } else {
                        if (mapOffileds.get(str) == true || mapOffileds.get(str) == false) {
                            newData.put(str, (mapOffileds.get(str)));
                        } else {
                            newData.put(str, (string)(mapOffileds.get(str)));
                        }
                        if (str == 'NSUUID__c') {
                            nsuuId = (string)(mapOffileds.get(str));
                        }
                        if ( str == 'OwnerId' ) {
                            if ( (string)(mapOffileds.get(str)) == 'Logged_User_Id' ) {
                                newData.put(str,UserInfo.getUserId());
                            } 
                        }
                    }
                    if (str.toLowerCase () == 'email__c') {
                        if (mapOffileds.get(str) != NULL) {
                            if (response.contains (String.valueOf (mapOffileds.get(str)))) {
                                newData.Invalid_Email_Inquiry_Checks__c = true;
                            }
                        }
                    }
                }
                if(lstuser.size() >0) {
                    if(lstuser[0].Stand_Id__c != null && lstuser[0].Stand_Id__c != '') {
                        newData.Campaign__c = lstuser[0].Stand_Id__c;
                    }
                }
                
                newData.RecordTypeId = 
                    Schema.SObjectType.Stand_Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
                newInsesertList.add(newData);
            }
            Integer flag = 0;
            if ( nsuuid != null && nsuuid != '' ) {
                list<Stand_Inquiry__c> listOfInquery = [select Name from Stand_Inquiry__c where NSUUID__c =:nsuuid];
                if ( listOfInquery.size() > 0 ) {
                    flag = 1;
                }
            }
            if ( flag == 1 ) {
                wrapper wrap = new wrapper();
                wrap.code = 409;
                wrap.inquiryId = null;
                wrap.message = 'The details provided already exists in the system and hence marked as duplicate' ;
                res.statusCode = 409;
                res.responseBody = blob.valueof(JSON.serialize(wrap));
            } else {
                insert newInsesertList;
                Set <ID> standInqIds = new Set <ID> ();
                for (Stand_Inquiry__c rec: newInsesertList)
                    standInqIds.add (rec.ID);
                ValidateEmailAndNumber.validateStandInquiryData  (standInqIds);  
                //DAMAC_Get_Bitly_URL_Stand_Inquiries.generateShortURL(standInqIds); // Commented on March 11
                
                res.statusCode = 200;
                list<Stand_Inquiry__c> listOfInquery = [select Name from Stand_Inquiry__c where id =:newInsesertList[0].Id];
                //res.responseBody = blob.valueof(' Inquiry ' + listOfInquery[0].Name + ' has been created successfully ');
                wrapper wrap = new wrapper();
                wrap.code = 201;
                wrap.inquiryId = listOfInquery[0].Name;
                wrap.message = 'The inquiry '+listOfInquery[0].Name+' has been created successfully' ;
                res.responseBody = blob.valueof(JSON.serialize(wrap));
            }    
            
        }
        catch(exception e){
            wrapper wrap = new wrapper();
            wrap.code = 406;
            wrap.inquiryId = null;
            wrap.message = e.getMessage()+e.getLineNumber(); //'Unable to Create the inquiry failure record';
            res.statusCode = 406;
            res.responseBody = blob.valueof(JSON.serialize(wrap));
        }      
    }
    public class wrapper {
        public Integer code;
        public string inquiryId;
        public string message;
    }
}