@isTest
public Class CRM_Survey_Ctrl_Test{
    @isTest
    public static void testgetSurveyEmailBody() {
        //create user
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standardusertest@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testtest@testorg.com'); 
        insert u;

        //create account
        Account accObj = new Account(Name = 'Test Account');

        //create Case
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        Case objCase = new Case();            
        objCase.RecordTypeID = devRecordTypeId;
        objCase.AccountId = accObj.id;
        
        System.runAs( u ) {
            
            insert accObj;
            insert objCase;
        }
        
        List<EmailTemplate> listEmailTemplate = new List<EmailTemplate>( [SELECT 
                                            Id,Name, Subject, Body, HtmlValue, TemplateType
                                        FROM 
                                            EmailTemplate 
                                            LIMIT 1] );

        String TemplateName = listEmailTemplate[0].Name;

        //create email message related to account
        EmailMessage mail = new EmailMessage();
        mail.Subject = listEmailTemplate[0].Subject;
        mail.MessageDate = System.Today();
        mail.Status = '3';//'Sent';
        mail.RelatedToId = accObj.id;
        mail.Account__c  = accObj.id;
        mail.Type__c = 'CRM Survey Email';
        mail.ToAddress = 'to@Address.com';
        mail.FromAddress = 'from@Address.com';
        mail.TextBody = 'contentBody';
        mail.Sent_By_Sendgrid__c = true;
        mail.SentGrid_MessageId__c = '3434';
        insert mail;
        
        CRE_Survey_Feedback__c objSurveyRes = new CRE_Survey_Feedback__c();
        objSurveyRes.Case__c = objCase.Id;
        objSurveyRes.CRM_Survey_Email__c = true;
        objSurveyRes.CRM_Survey_Type__c = 'Email Case Survey';
        insert objSurveyRes;

        PageReference pageRef = page.CRM_Survey;
        Test.setCurrentPage( pageRef );


        ApexPages.currentPage().getParameters().put('id', objCase.Id);
        ApexPages.currentPage().getParameters().put('creid', u.id);
        ApexPages.currentPage().getParameters().put('AccountId', accObj.Id );
        ApexPages.currentPage().getParameters().put('Template', TemplateName);
        ApexPages.currentPage().getParameters().put('SurveyType', 'Email Case Survey');

        CRM_Survey_Ctrl ctrl = new CRM_Survey_Ctrl();
        Test.startTest();
        ctrl.submitCrmSurvey();
        Test.stopTest();
    }
}