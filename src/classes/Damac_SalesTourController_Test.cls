@isTest
private class Damac_SalesTourController_Test {
    
    @isTest static void test_singleAttendee() {
        Test.startTest();
        Activity_Manager__c aManager = salesTourController (UserInfo.getUserID());

        Damac_SalesTourController obj = new Damac_SalesTourController(null);
        obj.createAttendees();

        System.assertEquals([SELECT COUNT() FROM Meeting_Attendees__c WHERE Activity_Manager__c =: aManager.ID], 2);
        Test.stopTest();
    }

    @isTest static void test_multipleAttendees() {
        Test.startTest();
        Activity_Manager__c aManager = salesTourController (UserInfo.getUserID()+','+UserInfo.getUserID());

        Damac_SalesTourController obj = new Damac_SalesTourController(null);
        obj.createAttendees();

        System.assertEquals([SELECT COUNT() FROM Meeting_Attendees__c WHERE Activity_Manager__c =: aManager.ID], 2);
        Test.stopTest();
    }

    @isTest static void test_existingAttendees() {
        Test.startTest();
        
        Activity_Manager__c aManager = salesTourController (UserInfo.getUserID());
        
        Meeting_Attendees__c attendees = new Meeting_Attendees__c();
        attendees.Activity_Manager__c = aManager.Id;
        attendees.Others__c = 'test@gmail.com';
        insert attendees;
        
        Damac_SalesTourController obj = new Damac_SalesTourController(null);
        obj.createAttendees();

        System.assertEquals([SELECT COUNT() FROM Meeting_Attendees__c WHERE Activity_Manager__c =: aManager.ID], 3);
        Test.stopTest();
    }
    
    public static Activity_Manager__c salesTourController(String attendees){
        Inquiry__c  inqObj = createInquiries();
        Task objTask = createTask(inqObj.Id);
        Sales_Tours__c objSalesTour = createSalesTour(inqObj.Id);
        Activity_Manager__c aManager = createActivityManager(objSalesTour.ID);

        Apexpages.currentpage().getparameters().put('id', objSalesTour.Id);
        Apexpages.currentpage().getparameters().put('taskID', objSalesTour.Id);
        Apexpages.currentpage().getparameters().put('attendeesId', attendees);
        Apexpages.currentpage().getparameters().put('otherAttendee', 'testing@gmail.com');
        return aManager;

    }
    public static Activity_Manager__c createActivityManager(id salesTourId) {
        Activity_Manager__c aManager = new Activity_Manager__c();
        aManager.Related_to_Sales_Tours__c = salesTourId;
        aManager.Activity_or_Task_Id__c = salesTourId;
        insert aManager;
        return aManager;
    }

    public static Inquiry__c createInquiries() {
        Inquiry__c inquiryInst = new Inquiry__c(
            First_Name__c = 'Test',
            Last_Name__c = 'Last Test',
            Inquiry_Source__c = 'Call Center',
            Inquiry_Status__c = 'Active',
            Email__c = 'test@gmail.com',
            OwnerId = UserInfo.getuserid()
        );
        insert inquiryInst;
        return inquiryInst;
    }
    
    /**
     * Method to insert records for Task
     */
    public static Task createTask(Id inqId) {
        Task objTask = new Task(
            WhatId = inqId,
            Subject = 'Last Test',
            Description = 'Last Test',
            OwnerId = UserInfo.getuserid()
        );
        insert objTask;
        return objTask;
    }

     /**
     * Method to insert records for Sales Tour
     */
    public static Sales_Tours__c createSalesTour(Id inqId) {
        Sales_Tours__c objSalesTour = new Sales_Tours__c(
            Inquiry__c = inqId,
            OwnerId = UserInfo.getuserid(),
            Tour_Outcome__c = 'No Show - Follow up needed',
            Pickup_Location__c = 'Park Towers',
            Check_In_Date__c = System.today(),
            Check_Out_Date__c = System.today(),
            Comments__c = 'Test'
        );
        insert objSalesTour;
        return objSalesTour;
    }
    
}