/*
Created Date: 15/06/2020
Batch Class: Damac_CardExpiryNotificationBatch_Con
Test Class: Damac_CardExpiryNotificationBatchTest
Description: In this while creating object for batch class parameters are given, depending on this execution is done
*/
global class Damac_CardExpiryNotificationSch_Con implements Schedulable {
    global void execute(SchedulableContext sc) {
        Damac_CardExpiryNotificationBatch_Con cen1 = new Damac_CardExpiryNotificationBatch_Con();
        Database.executeBatch(cen1, 1);
    }
}