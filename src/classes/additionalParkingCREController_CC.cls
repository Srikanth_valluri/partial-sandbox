public without sharing class additionalParkingCREController_CC extends additionalParkingCREController{
    // Constructor
    public additionalParkingCREController_CC(){
        super(false);
        if (CustomerCommunityUtils.isCurrentView('additionalparking')) {
            super();
        }
    }

    public override SR_Attachments__c getCaseAttachment(String strDocType,String name)
    {
      system.debug('objCase.Id '+objCase.Id);
      SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
      //objCaseAttachment.Case__c = objCase.Id;
      objCaseAttachment.Name = name;
      objCaseAttachment.Type__c = strDocType;
      if(strDocType.equalsIgnoreCase('PDC'))
      {
        objCaseAttachment.IsValid__c = false;
      }
      return objCaseAttachment;
    }

    public override Pagereference createSR()
    {
        system.debug('totalPrice=='+totalPrice);
        String strStatus;
        Boolean blnSRSubmittedSuccess = true;
        List<SR_Attachments__c> lstCaseAttachment = new List<SR_Attachments__c>();
        UploadMultipleDocController.data objResponseData;
        List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocReq = new List<UploadMultipleDocController.MultipleDocRequest>();

        try
        {
          Decimal decBuildingID = Decimal.valueOf(objUnit.Building_ID__c);
          String strIsParkingReserved = AdditionalParkingUtility.validateParkingSelection(
                                                                                          decBuildingID
                                                                                         ,JSON.serialize(lstParkingInformation)
                                                                                         );
          system.debug('strIsParkingReserved '+strIsParkingReserved);
          if(String.isNotBlank(strIsParkingReserved))
          {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error - Selected Parking/s are reserved: '+strIsParkingReserved));
            return null;
          }

          if( String.isNotBlank(strPaymentType) && !strPaymentType.equalsIgnoreCase('None') &&
              strPaymentType.equalsIgnoreCase('Installment Based Payment')
              && intTotalPaymentTermsInstallment != null && intTotalPaymentTermsInstallment <= 0
            )
          {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error - No installments provided for Installment Based Payment.'));
            return null;
          }

          if(blnIsPOA != null && blnIsPOA)
          {
              if(String.isNotBlank(strPOAAttachmentBody) && String.isNotBlank(strPOAAttachmentName))
              {
                  String strExtenstion = extractType(strPOAAttachmentName);
                  UploadMultipleDocController.MultipleDocRequest objMultipleDocRequestPOA = new UploadMultipleDocController.MultipleDocRequest();
                  objMultipleDocRequestPOA.category = 'Document';
                  objMultipleDocRequestPOA.entityName = 'Damac Service Requests';
                  objMultipleDocRequestPOA.base64Binary = EncodingUtil.base64Encode(extractBody(String.valueOf(strPOAAttachmentBody)));
                  objMultipleDocRequestPOA.fileDescription = 'Power Of Attorney';
                  //objMultipleDocRequestPOA.fileId = 'IPMS-'+objAccount.party_ID__C+'-'+strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+extractName( strPOAAttachmentName );
                  objMultipleDocRequestPOA.fileId = strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+'Power Of Attorney'+ strExtenstion;
                  //objMultipleDocRequestPOA.fileName = 'IPMS-'+objAccount.party_ID__C+'-'+strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+extractName( strPOAAttachmentName );
                  objMultipleDocRequestPOA.fileName = strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+'Power Of Attorney'+ strExtenstion;
                  //objMultipleDocRequestPOA.registrationId = objBURegId.Registration_ID__c != null ? objBURegId.Registration_ID__c : '';
                  objMultipleDocRequestPOA.registrationId = strCaseNumber != null ? strCaseNumber : '';
                  objMultipleDocRequestPOA.sourceFileName = 'IPMS-'+objAccount.party_ID__C+'-'+'Power Of Attorney';//extractName( strPOAAttachmentName );
                  objMultipleDocRequestPOA.sourceId = 'IPMS-'+objAccount.party_ID__C+'-'+'Power Of Attorney'+//extractName( strPOAAttachmentName );

                  //lstAttachment.add(getAttachment(extractName(strPOAAttachmentName), extractBody(strPOAAttachmentBody)));
                  //lstCaseAttachment.add(getCaseAttachment('Power Of Attorney',extractName( strPOAAttachmentName )));
                  lstCaseAttachment.add(getCaseAttachment('Power Of Attorney','Power Of Attorney'));

                  lstMultipleDocReq.add(objMultipleDocRequestPOA);
              }
          }

          if(String.isNotBlank(strPRFAttachmentBody) && String.isNotBlank(strPRFAttachmentName))
          {
              String strExtenstion = extractType(strPRFAttachmentName);

              UploadMultipleDocController.MultipleDocRequest objMultipleDocRequestPRF = new UploadMultipleDocController.MultipleDocRequest();
              objMultipleDocRequestPRF.category = 'Document';
              objMultipleDocRequestPRF.entityName = 'Damac Service Requests';
              objMultipleDocRequestPRF.base64Binary = EncodingUtil.base64Encode(extractBody(String.valueOf(strPRFAttachmentBody)));
              objMultipleDocRequestPRF.fileDescription = 'Parking Request Form';
              //objMultipleDocRequestPRF.fileId = 'IPMS-'+objAccount.party_ID__C+'-'+strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+extractName( strPRFAttachmentName );
              objMultipleDocRequestPRF.fileId = strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+'Parking Request Form'+ strExtenstion;
              //objMultipleDocRequestPRF.fileName = 'IPMS-'+objAccount.party_ID__C+'-'+strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+extractName( strPRFAttachmentName );
              objMultipleDocRequestPRF.fileName = strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+'Parking Request Form'+ strExtenstion;
              //objMultipleDocRequestPRF.registrationId = objBURegId.Registration_ID__c != null ? objBURegId.Registration_ID__c : '';
              objMultipleDocRequestPRF.registrationId = strCaseNumber != null ? strCaseNumber : '';
              objMultipleDocRequestPRF.sourceFileName = 'IPMS-'+objAccount.party_ID__C+'-'+'Parking Request Form';//extractName( strPRFAttachmentName );
              objMultipleDocRequestPRF.sourceId = 'IPMS-'+objAccount.party_ID__C+'-'+'Parking Request Form';//extractName( strPRFAttachmentName );

              //lstAttachment.add(getAttachment(extractName(strPRFAttachmentName), extractBody(strPRFAttachmentBody)));
              //lstCaseAttachment.add(getCaseAttachment('Parking Request Form',extractName( strPRFAttachmentName )));
              lstCaseAttachment.add(getCaseAttachment('Parking Request Form','Parking Request Form'));

              lstMultipleDocReq.add(objMultipleDocRequestPRF);
          }

          if(String.isNotBlank(strPaymentType) && !strPaymentType.equalsIgnoreCase('None'))
          {
            if(strPaymentType.equalsIgnoreCase('Full Payment'))
            {
                if(String.isNotBlank(strPOPAttachmentBody) && String.isNotBlank(strPOPAttachmentName))
                {
                    String strExtenstion = extractType(strPOPAttachmentName);
                    UploadMultipleDocController.MultipleDocRequest objMultipleDocRequestPOA = new UploadMultipleDocController.MultipleDocRequest();
                    objMultipleDocRequestPOA.category = 'Document';
                    objMultipleDocRequestPOA.entityName = 'Damac Service Requests';
                    objMultipleDocRequestPOA.base64Binary = EncodingUtil.base64Encode(extractBody(String.valueOf(strPOPAttachmentBody)));
                    objMultipleDocRequestPOA.fileDescription = 'Proof Of Payment';
                    //objMultipleDocRequestPOA.fileId = 'IPMS-'+objAccount.party_ID__C+'-'+strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+extractName( strPOPAttachmentName );
                    objMultipleDocRequestPOA.fileId = strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+'Proof Of Payment'+ strExtenstion;
                    //objMultipleDocRequestPOA.fileName = 'IPMS-'+objAccount.party_ID__C+'-'+strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+extractName( strPOPAttachmentName );
                    objMultipleDocRequestPOA.fileName = strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+'Proof Of Payment'+ strExtenstion;
                    //objMultipleDocRequestPOA.registrationId = objBURegId.Registration_ID__c != null ? objBURegId.Registration_ID__c : '';
                    objMultipleDocRequestPOA.registrationId = strCaseNumber != null ? strCaseNumber : '';
                    objMultipleDocRequestPOA.sourceFileName = 'IPMS-'+objAccount.party_ID__C+'-'+'Proof Of Payment';//extractName( strPOPAttachmentName );
                    objMultipleDocRequestPOA.sourceId = 'IPMS-'+objAccount.party_ID__C+'-'+'Proof Of Payment';//extractName( strPOPAttachmentName );

                    //lstAttachment.add(getAttachment(extractName(strPOAAttachmentName), extractBody(strPOAAttachmentBody)));
                    //lstCaseAttachment.add(getCaseAttachment('Proof Of Payment',extractName( strPOPAttachmentName )));
                    lstCaseAttachment.add(getCaseAttachment('Proof Of Payment','Proof Of Payment'));

                    lstMultipleDocReq.add(objMultipleDocRequestPOA);
                }
            }
            else if(strPaymentType.equalsIgnoreCase('Installment Based Payment'))
            {
                if(String.isNotBlank(strPDCAttachmentBody) && String.isNotBlank(strPDCAttachmentName))
                {
                    String strExtenstion = extractType(strPDCAttachmentName);
                    UploadMultipleDocController.MultipleDocRequest objMultipleDocRequestPOA = new UploadMultipleDocController.MultipleDocRequest();
                    objMultipleDocRequestPOA.category = 'Document';
                    objMultipleDocRequestPOA.entityName = 'Damac Service Requests';
                    objMultipleDocRequestPOA.base64Binary = EncodingUtil.base64Encode(extractBody(String.valueOf(strPDCAttachmentBody)));
                    objMultipleDocRequestPOA.fileDescription = 'Post Dated Cheque';
                    //objMultipleDocRequestPOA.fileId = 'IPMS-'+objAccount.party_ID__C+'-'+strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+extractName( strPDCAttachmentName );
                    objMultipleDocRequestPOA.fileId = strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+'Post Dated Cheque'+ strExtenstion;
                    //objMultipleDocRequestPOA.fileName = 'IPMS-'+objAccount.party_ID__C+'-'+strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+extractName( strPDCAttachmentName );
                    objMultipleDocRequestPOA.fileName = strCaseNumber+'-'+String.valueOf(System.currentTimeMillis())+'-'+'Post Dated Cheque'+ strExtenstion;
                    //objMultipleDocRequestPOA.registrationId = objBURegId.Registration_ID__c != null ? objBURegId.Registration_ID__c : '';
                    objMultipleDocRequestPOA.registrationId = strCaseNumber != null ? strCaseNumber : '';
                    objMultipleDocRequestPOA.sourceFileName = 'IPMS-'+objAccount.party_ID__C+'-'+'Post Dated Cheque';//extractName( strPDCAttachmentName );
                    objMultipleDocRequestPOA.sourceId = 'IPMS-'+objAccount.party_ID__C+'-'+'Post Dated Cheque';//extractName( strPDCAttachmentName );

                    //lstAttachment.add(getAttachment(extractName(strPOAAttachmentName), extractBody(strPOAAttachmentBody)));
                    //lstCaseAttachment.add(getCaseAttachment('Post Dated Cheque',extractName( strPDCAttachmentName )));
                    lstCaseAttachment.add(getCaseAttachment('Post Dated Cheque','Post Dated Cheque'));

                    lstMultipleDocReq.add(objMultipleDocRequestPOA);
                }
            }
          }
          else
          {
            system.debug('strPaymentType None '+strPaymentType);
          }


          if( lstMultipleDocReq != null && lstMultipleDocReq.size() > 0 )
          {
            system.debug('lstMultipleDocReq '+lstMultipleDocReq.size());
            objResponseData = new UploadMultipleDocController.data();
            objResponseData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);
          }

          //updateParking();
          objCase.Booking_Unit__c = objUnit.Id;
          objCase.Pending_Amount__c = totalPrice;
          objCase.Total_Amount__c = totalPrice;

          Id parkingRecordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Parking').getRecordTypeId();
          if(String.isNotBlank(strCaseStatusSubmitted) && strCaseStatusSubmitted.equalsIgnoreCase('Submitted'))
          {
            objCase.Status = 'Submitted';
          }
          else
          {
            objCase.Status = 'New';
          }
          objCase.RecordTypeID = parkingRecordTypeID;
          objCase.Subject = 'Additional Parking SR';
          objCase.Origin = 'Portal';
          objCase.Subject = 'Additional Parking SR';
          objCase.SR_Type__c = 'Request for Additional Parking SR';
          if(String.isNotBlank(dtDateOfExpiry))
          {
            objCase.POA_Expiry_Date__c = Date.parse(dtDateOfExpiry);
          }
          objCase.Booking_Unit__c = objUnit.Id;
          objCase.AccountId = strAccParamId;
          // is POA checkbox is selected
          if( blnIsPOA )
          {
            objCase.IsPOA__c = true;
          }
          else
          {
            objCase.IsPOA__c = false;
            objCase.Purpose_of_POA__c = '';
            objCase.POA_Relationship_with_Buyer__c = '';
            objCase.POA_Issued_By__c = '';
            //objCase.Buyer_POA_Phone__c = '';
            //objCase.Buyer_POA_Country_Code__c = 'None';
            objCase.POA_Expiry_Date__c = null;
          }
          system.debug('intTotalParkingSelected createSR '+intTotalParkingSelected);
          objCase.Of_Parking__c = intTotalParkingSelected;
          //store the parking details in the JSON field so that if SR is saved as draft we can again show the selected parking to CRE
          objCase.Parking_Details_JSON__c = JSON.serialize(lstParkingInformation);

          system.debug('strPaymentType '+strPaymentType);
          system.debug('strPaymentModeType '+strPaymentModeType);
          system.debug('JSON.serialize(lstPaymentTerms) '+JSON.serialize(lstPaymentTerms));
          system.debug('totalPrice '+totalPrice);
          if(totalPrice != null && totalPrice > 0)
          {
            if(String.isNotBlank(strPaymentType) && !strPaymentType.equalsIgnoreCase('None'))
            {
              if(strPaymentType.equalsIgnoreCase('Full Payment'))
              {
                objCase.Parking_Type_Of_Payment__c = strPaymentType;
                objCase.Parking_Payment_Mode__c = strPaymentModeType;
                objCase.Parking_Total_Payment_Terms__c = null;
                objCase.Parking_Payment_Terms__c = '';

                // if SR is saved as draft and then payment is changed from Installment based to Full Payment
                // then delete the Attachement of PDC for Installment Payment
                List<SR_Attachments__c> lstPDC = [Select Id FROM SR_Attachments__c WHERE Type__c = 'Post Dated Cheque' AND Case__c =: objCase.Id];
                if(lstPDC != null && lstPDC.size() > 0)
                {
                  delete lstPDC;
                }
                else
                {
                  system.debug('lstPDC list zero');
                }
              }
              else if(strPaymentType.equalsIgnoreCase('Installment Based Payment'))
              {
                objCase.Parking_Type_Of_Payment__c = strPaymentType;
                objCase.Parking_Payment_Terms__c = JSON.serialize(lstPaymentTerms);
                objCase.Parking_Total_Payment_Terms__c = intTotalPaymentTermsInstallment;
                objCase.Parking_Payment_Mode__c = 'None';
                // if SR is saved as draft and then payment is changed from Full Payment to Installment based
                // then delete the Attachement of POP for full payment
                List<SR_Attachments__c> lstPOP = [Select Id FROM SR_Attachments__c WHERE Type__c = 'Proof Of Payment' AND Case__c =: objCase.Id];
                if(lstPOP != null && lstPOP.size() > 0)
                {
                  delete lstPOP;
                }
                else
                {
                  system.debug('lstPOP list zero');
                }
              }
            }
          }

          upsert objCase;

          if(objCase.Id != null)
          {
              String strFileUploadResponseError;
              system.debug('objResponseData '+objResponseData);
              if(objResponseData != null)
              {
                if(objResponseData.status == 'Exception')
                {
                    blnSRSubmittedSuccess = false;
                    ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Error,'Error - Document Upload '+objResponseData.message));
                    return null;
                }
                else
                {
                  strFileUploadResponseError = insertAttachments(objResponseData,lstCaseAttachment);
                }

                system.debug('strFileUploadResponseError '+strFileUploadResponseError);
                if(String.isBlank(strFileUploadResponseError) || !strFileUploadResponseError.equalsIgnoreCase('success'))
                {
                  blnSRSubmittedSuccess = false;
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'ERROR : '+strFileUploadResponseError));
                  return null;
                }
              }

              strCaseID = objCase.Id;
              Case objInsertedCase = [SELECT Id,CaseNumber FROM Case WHERE id =: strCaseID LIMIT 1];

              strStatus = 'SR record created successfully.'+' '+'SR No:'+objInsertedCase.CaseNumber;
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Success : '+strStatus));

              system.debug('blnSRSubmittedSuccess '+blnSRSubmittedSuccess );
              if(blnSRSubmittedSuccess)
              {
                if(String.isNotBlank(strCaseStatusSubmitted) && strCaseStatusSubmitted.equalsIgnoreCase('Submitted'))
                {
                  system.debug('strCaseStatusSubmitted '+strCaseStatusSubmitted);
                  AdditionalParkingUtility.updateParkingStatusInIPMS(objInsertedCase.Id,JSON.serialize(lstParkingInformation),'Submitted');
                  AdditionalParkingUtility.postParkingFeeToIPMS(objInsertedCase.Id);
                  if(String.isNotBlank(strPaymentType) && strPaymentType != 'None' && strPaymentType == 'Installment Based Payment')
                  {
                    if(lstPaymentTerms != null && lstPaymentTerms.size() > 0)
                    {
                      AdditionalParkingUtility.createParkingPaymentTerms(objCase.Id);
                    }
                    else
                    {
                      system.debug('lstPaymentTerms is empty');
                      ApexPages.addmessage(new ApexPages.message(
                                            ApexPages.severity.Error,'Error - Please Provide New Payment Terms'));
                    }
                  }
                }
              }

              if(String.isNotBlank(strCaseStatusSubmitted) && strCaseStatusSubmitted.equalsIgnoreCase('Submitted'))
              {
                //return new Pagereference('/' + objCase.Id);
                //return new PageReference('/Customer');
                return null;
              }
              else
              {
                return null;
                //ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, 'Your Service Request has been successfully saved'));
                //return new PageReference('/Customer');
                //system.debug('save as draft clicked');
                //system.debug('strAccParamId '+strAccParamId);

                //Pagereference pg = Page.DummyTitleDeedCREPortal;
                //Pagereference pg = Page.Additional_ParkingCRE;
                //Pagereference pg = Page.ParkingRequestProcessPage;
                //pg.getParameters().put('AccountId',strAccParamId);
                //pg.getParameters().put('caseID',objCase.Id);
                //pg.getParameters().put('SRType','AdditionalParking');
                //pg.setRedirect(true);
                //return pg;
              }
          }
          else
          {
            strStatus += 'SR record not created. <br/>';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error : '+strStatus));
            return null;
          }
        }
        catch(Exception exp)
        {
          system.debug('exception '+exp);
          system.debug('exception '+exp.getMessage());
          system.debug('exception String'+exp.getStackTraceString());
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error - Create SR '+exp.getMessage()));
          return null;
        }
    }
}