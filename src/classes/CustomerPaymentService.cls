public with sharing class CustomerPaymentService {

     public static CustomerPaymentService.Payment getExistingPT(String strRegID)
    {
        CustomerPayment.CustomerPaymentHttpSoap11Endpoint objHttpsoap = new CustomerPayment.CustomerPaymentHttpSoap11Endpoint();
        objHttpsoap.timeout_x = 120000;
        CustomerPaymentService.Payment objMileStonePaymentDetails = new CustomerPaymentService.Payment();
        try
        {
            CustomerPayment2.APPSXXDC_PROCESS_SERX1794747X2X4 regTerms = new CustomerPayment2.APPSXXDC_PROCESS_SERX1794747X2X4();
            regTerms.PARAM_ID = strRegID;
            String strResponse = objHttpsoap.customerPaymentMade('2-123456', 'GET_REG_PAYMENT_TERMS', 'SFDC', regTerms);
            system.debug('strResponse '+strResponse);
            objMileStonePaymentDetails = CustomerPaymentService.parse(strResponse);
            system.debug('objMileStonePaymentDetails '+objMileStonePaymentDetails);
            if(String.isNotBlank(objMileStonePaymentDetails.status) && objMileStonePaymentDetails.status.equalsIgnoreCase('e'))
            {
                errorLogger('Error getMilestonePaymentDetails response '+objMileStonePaymentDetails.message);
                
            }
        }
        catch(Exception exp)
        {
            errorLogger('Error getMilestonePaymentDetails '+exp.getMessage()+' - '+exp.getStackTraceString());
            objMileStonePaymentDetails.customErrorMsg = 'Error fetching Milestone Payment Details from IPMS.';
        }
        return objMileStonePaymentDetails;
    }
    
    private static void errorLogger(string strErrorMessage)
    {
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        objError.Process_Name__c = 'Early Handover';
        insert objError;
    }
    
    public class Payment
    {
        public List<data> data;
        public String message;
        public String status;
        public String customErrorMsg;
    }
  
    public class data{
        public String ATTRIBUTE1;
        public String ATTRIBUTE2;
        public String ATTRIBUTE3;
        public String ATTRIBUTE4;
        public String ATTRIBUTE5;
        public String ATTRIBUTE6;
        public String ATTRIBUTE7;
        public String ATTRIBUTE8;
        public String ATTRIBUTE9;
        public String ATTRIBUTE10;
        public String ATTRIBUTE11; 
        public String ATTRIBUTE12; 
        public String ATTRIBUTE13;      
    }
    
    public static Payment parse(String json) 
    {
        return (Payment) System.JSON.deserialize(json, Payment.class);
    }
}