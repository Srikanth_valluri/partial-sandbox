public without sharing class CreatePOPSRController {
    public Id caseId;
    public String errorMessage {get;set;}
    public Id caseRecordTypeId {get;set;}
    public List<Case> lstPOPSR {get;set;}
    
    public CreatePOPSRController(ApexPages.standardController controller) {
        PageReference currentPage = ApexPages.currentPage();
        if (currentPage != NULL) {
            caseId = currentPage.getParameters().get('id');
        }
    }

    public pagereference init(){
        lstPOPSR = new List<Case>();
        List<Attachment> newFiles = new List<Attachment>();
        caseRecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('POP').RecordTypeId;
        if(Approval.isLocked(caseId)) {
            System.debug('======Case is locked: ' + caseId);
            errorMessage = 'You cannot convert a case that is submitted for approval. Please ask approver to either approve or reject the case.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,errorMessage));
            return null;
        } else {
          system.debug('>>>>>>>>>>>>>>>>>>>CaseId'+ caseId);
          Case currentCase = [Select Id, Status,OwnerId,Origin,Subject
                                       , Account.Party_ID__c,Description
                                       , POA_Name__c
                                       , POA_Expiry_Date__c
                                       , POA_Relation_With_Owner__c
                                       , CaseNumber
                                       , POA_Issued_By__c
                                       , Purpose_of_POA__c
                                       , OQOOD_Fee_Applicable__c
                                       , OQOOD_Fee_Payment_Mode__c
                                       , OQOOD_Fee__c
                                       , POA_File_URL__c,SuppliedEmail
                      From Case
                     Where Id = :caseId LIMIT 1];

          System.debug('======currentCase : ' + currentCase);
          
      
          if(String.isNotBlank(currentCase.Status) &&
                ( currentCase.Status.equalsIgnoreCase('Cancelled')
                || currentCase.Status.equalsIgnoreCase('Closed')
                || currentCase.Status.equalsIgnoreCase('Rejected') )
          ) {
                System.Debug('======currentCase currentCase Status : ' + currentCase.Status);
                errorMessage = 'You cannot convert a case that is ' + currentCase.Status + '.';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,errorMessage));
                return null;
            } else if(String.isNotBlank(currentCase.Status))  {
                List<Account> lstAccount  = [SELECT Id FROM Account WHERE ( Email__pc =: currentCase.SuppliedEmail OR PersonEmail =: currentCase.SuppliedEmail ) LIMIT 1];
                System.Debug('===lstAccount : ' + lstAccount);
                System.Debug('===the case : ' + currentCase);
                
                Case objPopCase = new Case();
                objPopCase.Status = 'New';
                objPopCase.ParentId = caseId;
                objPopCase.Priority = 'Medium';
                objPopCase.RecordTypeId = caseRecordTypeId;
                objPopCase.OwnerId = currentCase.OwnerId;
                objPopCase.Origin = currentCase.Origin;
                objPopCase.SR_Type__c = 'Proof of Payment SR';
                objPopCase.SuppliedEmail = currentCase.SuppliedEmail;
                objPopCase.Case_Summary__c = currentCase.Subject;
                
                if( lstAccount.size() > 0 ) {
                    objPopCase.AccountId = lstAccount[0].Id;
                }else {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'You cannot convert this SR to POP as email is not registered'));
                          return null;
                }
                
                System.Debug('===the objPopCase : ' + objPopCase);
                
                lstPOPSR.add(objPopCase);
                
                currentCase.Status = 'Closed';
                currentCase.Reason_to_Close_Email_Case__c = 'POP';
                currentCase.Description = 'POP Created';
                currentCase.CRE_Comments__c = 'POP Created';
                
                try{
                    
                    if( lstPOPSR != null && lstPOPSR.Size() > 0 ) {
                        insert lstPOPSR;
                    }

                    /*System.Debug('===newFiles : ' + newFiles + 'newFiles.size() ' +newFiles.size());
                    if( newFiles != null && newFiles.Size() > 0 ) {
                        Insert newFiles;
                    }*/
                    update currentCase;
                  
                    createSRAttachments(  currentCase.Id , lstPOPSR[0].Id );
                  System.Debug('=update the case : ' + currentCase);
                } catch(Exception e) {
                    System.Debug('=Error in update the case : ' + e);
                    errorMessage = 'Please contact your system adminstrator ' + e.getMessage();
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,errorMessage));
                          return null;
                }
              }
            }
            pagereference newpg = new PageReference ('/'+lstPOPSR[0].id);
            newpg.setRedirect(true);
            return newpg;
    }
    
    @future(callout=true)
    public static void createSRAttachments( Id currentSRId , Id popSRId ) {
        
        Case objCase = new Case();
        List<Attachment> lstAttachment = new List<Attachment>();
        
        if( popSRId != null ) { 
            objCase = [SELECT Id,CaseNumber,Account.Party_ID__c FROM Case WHERE Id =: popSRId ];
        }
        if( currentSRId != null ) {
            lstAttachment = [ SELECT Id,Body,Name,ParentId FROM Attachment WHERE ParentId IN ( SELECT Id FROM EmailMessage WHERE ParentId =: currentSRId )];
        } 

        System.Debug('===lstAttachment : ' + lstAttachment);
        if( lstAttachment != null && lstAttachment.size() > 0) {
        
            List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest = new  List<UploadMultipleDocController.MultipleDocRequest>();
            List<SR_Attachments__c> lstSRAttachment = new List<SR_Attachments__c>();
            integer intIncrementor = 0;
            
            
            for( Attachment objAttachments : lstAttachment ) {
                if( objAttachments.Body != null && String.isNotBlank( objAttachments.Name ) ) {
                    UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
                    objDocRequest.category =  'Document';
                    objDocRequest.entityName = 'Damac Service Requests'; 
                    objDocRequest.fileDescription  =  objAttachments.Name;                        
                    
                    intIncrementor++;
                    objDocRequest.fileId = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+objAttachments.Name;
                    objDocRequest.fileName = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+objAttachments.Name;
                    
                    System.debug('---System.currentTimeMillis()--'+System.currentTimeMillis());
                    System.debug('---intIncrementor--'+intIncrementor);
                    System.debug('---objDocRequest.fileName--'+objDocRequest.fileName);
                    System.debug('---extractName objAttachments.Name .--'+objAttachments.Name);
                    
                    if(objCase != null){
                        objDocRequest.registrationId =  objCase.CaseNumber;
                    }
                    objDocRequest.sourceFileName  = 'IPMS-'+objCase.Account.party_ID__C+'-'+objAttachments.Name;
                    objDocRequest.sourceId  =  'IPMS-'+objCase.Account.party_ID__C+'-'+objAttachments.Name;                        
                    if( objAttachments.Body != null ){
                        blob objBlob = objAttachments.Body;
                        if(objBlob != null){
                            objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
                        }
                    }
                    //objDocRequest.base64Binary  =  crfAttachmentBody;
                    lstMultipleDocRequest.add(objDocRequest);
                    SR_Attachments__c objAttach = new SR_Attachments__c();
                    objAttach.Case__c  = objCase.Id;
                    objAttach.isValid__c = true;
                    objAttach.IsRequired__c = true;
                    objAttach.Name = objAttachments.Name;
                    //objAttach.Attachment_URL__c = 'www.salesforce.com';
                    objAttach.Need_Correction__c =  False;
                    lstSRAttachment.add(objAttach);
        
                }
            }
            if( lstMultipleDocRequest.Size() > 0 ) {
                UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();
                if( !Test.isRunningTest() ) { 
                    MultipleDocData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocRequest);
                }else {
                    
                    List<UploadMultipleDocController.MultipleDocResponse> data = new List<UploadMultipleDocController.MultipleDocResponse>();
                    UploadMultipleDocController.MultipleDocResponse objMultipleDocResponse = new UploadMultipleDocController.MultipleDocResponse();
                    objMultipleDocResponse.PROC_STATUS = 'S';
                    objMultipleDocResponse.url = 'www.google.com';
                    data.add(objMultipleDocResponse);
                    
                    MultipleDocData.Data = data;
                }
                System.debug('--MultipleDocData--'+MultipleDocData);
                if(MultipleDocData != null && MultipleDocData.status == 'Exception'){
                    errorLogger(MultipleDocData.message,objCase.Id,'');
                }                                   
                if(MultipleDocData != null && (MultipleDocData.Data == null || MultipleDocData.Data.Size() == 0)){
                    errorLogger(MultipleDocData.message,objCase.Id,'');                                        
                }

                if( MultipleDocData != null ) {
                    for( Integer objInt = 0; objInt < lstSRAttachment.Size(); objInt++ ) {
                        UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[objInt];
                        if( objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url) ) {
                            lstSRAttachment[objInt].Attachment_URL__c = objData.url;
                        }
                    }
                    if( lstSRAttachment.Size() > 0 ){
                        insert lstSRAttachment;
                    }  
                }
            }
            /*Attachment newFile = objAttachments.clone();
            newFile.ParentId = lstPOPSR[0].Id;
            newFiles.add(newFile);*/
        }
    }
    
    private static void errorLogger(string strErrorMessage, string strCaseID,string strBookingUnitID){
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        if(String.isNotBlank(strCaseID) && strCaseID.startsWith('500')){
            objError.Case__c = strCaseID;
        }
        if(String.isNotBlank(strBookingUnitID)){
            objError.Booking_unit__c = strBookingUnitID;
        }
        insert objError;
    }
        
    
}