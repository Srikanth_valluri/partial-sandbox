/****************************************************************************************************
* Name          : SendGridEmailService                                                              *
* Description   : Class to send emails to Users                                                     *
* Created Date  : 12-06-2018                                                                        *
* Created By    : ESPL                                                                              *
* Documentation : https://sendgrid.com/docs/API_Reference/api_v3.html                               *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE        COMMENTS                                                    *
* 1.0                                 Initial Draft.                                              *
****************************************************************************************************/
global class SendGridEmailService {

    /**
     * toAddress        - Send Email To User's Address
     * toName           - Send Email To User's Name
     * ccAddress        - Send Email CC User's Address
     * ccName           - Send Email CC User's Name
     * bccAddress       - Send Email BCC User's Address
     * bccName          - Send Email BCC User's Name
     * subject          - Email Subject
     * substitutions    - Variables values used in the Email Template
     * fromAddress      - Email From User's Address
     * fromName         - Email From User's Name
     * replyToAddress   - Reply To User's Address
     * replyToName      - Reply To User's Name
     * contentType      - text/plain or text/html
     * contentValue     - If mail body is Text/HTML
     * templateId       - id of the Template to be used
     */

    public static String responseStatus;


    public static SendGridResponse sendEmailService(
        String toAddress, String toName,
        String ccAddress, String ccName,
        String bccAddress, String bccName,
        String subject, String substitutions,
        String fromAddress, String fromName,
        String replyToAddress, String replyToName,
        String contentType, String contentValue,
        String templateId,
        list<Attachment> lstAttach
    ) {

        string toAddressReq = '';
        if(toAddress!=null && toAddress.contains(',')){
            toAddress = toAddress.remove(' ');
            List<String> lsttoAddress = toAddress.split(',');
            System.debug(lsttoAddress);
            for (string singleToAdd: lsttoAddress){
                toAddressReq += '{ "email" : "' + singleToAdd +  '"},';
            }
            toAddressReq = toAddressReq.removeEnd(',');
        } else {
            toAddressReq = '{ "email" : "' + toAddress +  '"}';
        }
        system.debug('toAddressReq --'+toAddressReq);

        string ccAddressReq = '';
        if(ccAddress.contains(',')){
            ccAddress = ccAddress.remove(' ');
            List<String> lstccAddress = ccAddress.split(',');
            System.debug(lstccAddress);
            for (string singleccAdd: lstccAddress){
                ccAddressReq += '{ "email" : "' + singleccAdd+  '"},';
            }
            ccAddressReq = ccAddressReq .removeEnd(',');
        } else {
            ccAddressReq = '{ "email" : "' + ccAddress +  '"}';
        }
        system.debug('ccAddressReq --'+ccAddressReq);

        string bccAddressReq = '';
        if(String.isNotBlank(bccAddress) && bccAddress.contains(',')){
            bccAddress = bccAddress.remove(' ');
            List<String> lstbccAddress = bccAddress.split(',');
            System.debug(lstbccAddress);
            for (string singlebccAdd: lstbccAddress){
                bccAddressReq += '{ "email" : "' + singlebccAdd+  '"},';
            }
            bccAddressReq = bccAddressReq .removeEnd(',');
        } else {
            bccAddressReq = '{ "email" : "' + bccAddress +  '"}';
        }
        system.debug('bccAddressReq --'+bccAddressReq);

        String request  = '{'
                        + '"personalizations": [ { '
                        //+ '    "to" : [{ "email" : "' + toAddressReq +  '"}], '
                        + '    "to" : [' + toAddressReq +  '], '
                        + '    "subject" : "' + subject + '"';
        // CC Address Check
        if (String.isNotBlank(ccAddress)) {
            request += ',"cc":[ ' +ccAddressReq+ ']';
            request += ', "name"   : { ' + ccName + ' } ';
            //request += ', "cc": [ { "email": "' + ccAddressReq + '", "name": "' + ccName + '" } ] ';
            //request += ',"cc":[ ' +ccAddress+ ']';
        }

        // BCC Address Check
        if (String.isNotBlank(bccAddress)) {
            request += ',"bcc":[ ' +bccAddressReq+ ']';
            request += ', "name"   : { ' + bccName + ' } ';
            //request += ', "bcc": [ { "email": "' + bccAddress + '", "name": "' + bccName + '" } ]';
            //request += ',"bcc":[' +bccAddress + ']';

        }

        // Substitutions Check
        if (String.isNotBlank(substitutions) ) {
            request += ', "substitutions"   : { ' + substitutions+ ' } ';


        }

        request += ' } ], '
                 + ' "from" : { "email" : "' + fromAddress + '", "name" : "' + fromName + '" },'
                 + ' "tracking_settings" :  { '
                 + '   "open_tracking" :  { "enable": true , "substitution_tag" : "" } , '
                 + '   "ganalytics" : { "enable": true , "utm_source" : "" , "utm_medium" : "" , "utm_term" : "" , "utm_content" : "" , "utm_campaign" : "" } '
                 + ' } ';

        if(String.isNotBlank(replyToAddress)){
            request += ',"reply_to" : {"email" : "'+ replyToAddress +'","name" :"' +replyToName +'"}';

        }
        System.debug('---contentValue ---'+contentValue);
        System.debug('---json contentValue ---'+json.serialize(contentValue) );
        if (String.isNotBlank(templateId)) {
             //request += ', "template_id" : "' + templateId + '" ';
            request += ', "template_id" : "7cd1aa6e-a8c2-4a8d-97e3-34b653335b6b"';
        } else {
            request += ', "content" : [ { "type": "' + contentType + '", "value": ' + json.serialize(contentValue)+ '} ] ';
        }

        String mailbody;
        if(!lstAttach.isEmpty()) {
            request += ', "attachments" : [';
            for (Attachment objAttach : lstAttach) {
                if( objAttach.Body != null ) {
                    mailbody = EncodingUtil.base64Encode(objAttach.Body);
                    if (String.isNotBlank(mailbody)) {
                        request += '{ "content" : "' + mailbody + '" , "filename" : "' + objAttach.Name + '" }  ';
                    }
                }
            }
            request += ']';
        }

        request += ' } ';

            System.debug('req: '+request);
            HttpRequest req = new HttpRequest();
            String endPoint = 'https://api.sendgrid.com/v3/mail/send';
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setBody(request);
            req.setheader ('Authorization', 'Bearer '+ Label.SendGrid_Username);
            req.setheader ('Content-Type', 'application/json');
            System.debug('req: '+req);
            Http http = new Http();
            SendGridResponse response = new SendGridResponse();
            if (!Test.isRunningTest ()) {
                HttpResponse res = http.send(req);
                response.ResponseStatus = res.getStatus();
                response.messageId = res.getHeader('X-Message-Id');
                system.debug('!!!!!response'+res.getHeader('X-Message-Id'));
                system.debug('!!!!!response'+res.getStatuscode());
                system.debug('!!!!!response'+res);
            }
        return response;
    }


    public class SendGridResponse {
        public String ResponseStatus;
        public String messageId;
    }

}