/***************************************************************************************************
 * @Name              : DAMAC_SubmitBuyerDetails_API
 * @Test Class Name   : DAMAC_SubmitBuyerDetails_API_Test
 * @Description       : 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         13/07/2020       Created
****************************************************************************************************/
@RestResource(urlMapping='/submitBuyerDetails')
global without sharing class DAMAC_SubmitBuyerDetails_API{
    
  
    @HttpPost
    global static SubmitBuyerResponse doPost() {
        
        User usr = [SELECT Id, Profile.Name FROM User WHERE Id =: UserInfo.getUserId()];
        RestRequest request = RestContext.request;
        system.debug('request: ' + request);
        SubmitBuyerResponse resp = new SubmitBuyerResponse();
        String statusMessage = 'Success';
        try{  
            String jsonBody = request.requestBody.toString();
            system.debug('jsonBody: ' + jsonBody);
            SubmitBuyerRequest reqst = (SubmitBuyerRequest) JSON.deserialize(jsonBody, SubmitBuyerRequest.class);               
            system.debug('reqst: ' + reqst);                                 
            if(usr.Profile.Name == 'Customer Community - Super User' || usr.Profile.Name == 'Customer Community - Agent' || usr.Profile.Name == 'Customer Community - Agent + Admin'){
                //updates primary buyer with new details
                update reqst.primaryBuyer;
               
                List<Buyer__c> primaryBuyerList = [SELECT Id, First_Name__c 
                                                   FROM Buyer__c 
                                                   WHERE Id =: reqst.primaryBuyer.Id AND 
                                                         Primary_Buyer__c = true LIMIT 1];
                List<JointBuyerWrapper> saveJointBuyerList = reqst.jointBuyerList;
                List<Unit_Documents__c> primaryBuyerUnitDocumentsList = new List<Unit_Documents__c>();
                List<Unit_Documents__c > primaryBuyerUnitDocumentsUpdateList = new List<Unit_Documents__c >();
                List<Buyer__c> jBuyerList = new List<Buyer__c>();
                List<Id> existingIds = new List<Id>();
                String jointBuyerId;                
                for(JointBuyerWrapper wrapper: reqst.jointBuyerList){
                    jBuyerList.add(wrapper.jointBuyer);
                    system.debug('Update this Id>>'+wrapper.jointBuyer.Id);
                    jointBuyerId = wrapper.jointBuyer.Id; //Stored into a String as (wrapper.jointBuyer.Id) != NULL cannot be used
                    //if(wrapper.jointBuyer.Id != null && wrapper.jointBuyer.Id != ''){ 
                    if(jointBuyerId != null && jointBuyerId != ''){ 
                        system.debug('Update this Id>>'+wrapper.jointBuyer.Id);                      
                        existingIds.add(wrapper.jointBuyer.Id);
                        system.debug('existingIds>>'+wrapper.jointBuyer.Id);
                        
                    }
                }
                if(!test.isRunningTest())
                upsert jBuyerList;
                Integer i = 0;
                
                List<Unit_Documents__c> unitDocuments = [SELECT Id 
                                                  FROM Unit_Documents__c 
                                                  WHERE  Buyer__c IN : jBuyerList
                                                  AND Buyer__c NOT IN : existingIds];                                                  
                
                delete unitDocuments;  
                
                
                List<Unit_Documents__c> docList = new List<Unit_Documents__c>();
                String jbId;
                String jbFirstName;
                for(Unit_Documents__c primeDOc: reqst.PrimaryBuyerUnitDoc){
                    primeDOc.Document_Name__c = primaryBuyerList[i].First_Name__c + '-' + primeDOc.Document_Name__c; 
                    docList.add(primeDOc);    
                }
                for(JointBuyerWrapper wrapper: reqst.jointBuyerList){
                    jbId = jBuyerList[i].Id;
                    jbFirstName = jBuyerList[i].First_Name__c;
                    for(Unit_Documents__c doc: wrapper.unitDoc){
                        doc.buyer__c = jbId;
                        if(jbFirstName != '' && jbFirstName != NULL && String.isBlank(jbFirstName) == false){
                            doc.Document_Name__c = jbFirstName + '-' + doc.Document_Name__c; 
                        }
                        docList.add(doc);    
                    }
                    i++;
                }
                upsert docList;                                  
                resp.statusCode = '200';
                resp.statusMessage = 'Success';
                resp.primaryBuyer = reqst.primaryBuyer;
                resp.jointBuyerList = reqst.jointBuyerList; 
                //resp.PrimaryBuyerUnitDoc = reqst.PrimaryBuyerUnitDoc;                                
                primaryBuyerUnitDocumentsList = [SELECT Id,Document_Name__c,Buyer__c,Service_Request__c,Booking__c,Status__c
                                                 FROM Unit_Documents__c 
                                                 WHERE Buyer__c IN: primaryBuyerList];                                
                if(primaryBuyerList.size() > 0 && primaryBuyerUnitDocumentsList.size() > 0){                    
                    for(Unit_Documents__c units : primaryBuyerUnitDocumentsList){
                        if(!units.Document_Name__c.Contains('-')){
                            units.Document_Name__c = primaryBuyerList[0].First_Name__c + '-' + units.Document_Name__c;                             
                        } 
                        if(units.Document_Name__c.Contains('-')){
                            units.Document_Name__c = units.Document_Name__c.removeStart(units.Document_Name__c.substringBefore('-'));
                            units.Document_Name__c = units.Document_Name__c.removeStart('-');                            
                            units.Document_Name__c = primaryBuyerList[0].First_Name__c + '-' + units.Document_Name__c;                                                         
                        }  
                        primaryBuyerUnitDocumentsUpdateList.add(units);             
                    }
                }
                upsert primaryBuyerUnitDocumentsUpdateList; 
                resp.PrimaryBuyerUnitDoc = primaryBuyerUnitDocumentsUpdateList;                               
            }
            else{
                resp.statusCode = '400';
                resp.statusMessage = 'You are not authorized to update buyer details';
            }
           
        } catch( Exception e ){
            resp.statusCode = '400';
            system.debug('In catch');
            resp.statusMessage = 'Exception : ' + e.getMessage();           
        }    
    
      return resp;
    }
    global class SubmitBuyerRequest {
        
        public Buyer__c primaryBuyer {get; set;} 
        public Unit_Documents__c[] PrimaryBuyerUnitDoc {get; set;}
        public JointBuyerWrapper[] jointBuyerList {get; set;}
       
    }
    
    global class JointBuyerWrapper{
        public Buyer__c jointBuyer {get; set;}
        public Unit_Documents__c[] unitDoc {get; set;}
    }
    
    global class SubmitBuyerResponse {
        
        public Buyer__c primaryBuyer {get; set;}
        public JointBuyerWrapper[] jointBuyerList {get; set;}
        public Unit_Documents__c[] PrimaryBuyerUnitDoc {get; set;}
        public String StatusCode{get; set;}
        public String StatusMessage{get; set;}
    }
    
    

}