/**************************************************************************************************
* Name               : AgentPortalPopularSearchesTest
* Description        :                                               
* Created Date       : Naresh(Accely)                                                                       
* Created By         : 6/09/2017                                                                 
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.1         Naresh kaneriya      6/09/2017                                                             
**************************************************************************************************/

@isTest 
public class AgentPortalPopularSearchesTest{
  private  static Inventory__c inventory ; 
  private static Address__c addressDetail ; 
  private  static Property__c propertyDetail ;
  
@testsetup
public static void TestData(){
    
         addressDetail = InitialiseTestData.getAddressDetails(9086);
         insert addressDetail ; 
        
         propertyDetail = InitialiseTestData.getPropertyDetails(7650);
         insert propertyDetail ; 
        
         inventory = InitialiseTestData.getInventoryDetails('3456','1234','2345',9086,7650); 
         inventory.Status__c = 'Released';
         inventory.Unit_Type__c = 'Test';
         inventory.Address__c = addressDetail.Id;    
         insert inventory ; 
        
}


public static testMethod void getStudioStudioTypesTest(){
         
         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Unit_Type__c = 'Studio';
         update inv;
         Test.stopTest(); 
         AgentPortalPopularSearches obj =  new AgentPortalPopularSearches();
         AgentPortalPopularSearches.getStudioStudioTypes();
}

public static testmethod void getAllDubaiApartmentsTest(){
         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Unit_Type__c = 'Apartments';
         inv.Property_Status__c = 'Ready';
         update inv;
         Test.stopTest(); 
         AgentPortalPopularSearches.getAllDubaiApartments();
    
}

public static testmethod void getAllDubaiVillasTest(){
         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Unit_Type__c = 'Villas';
         inv.Property_Status__c = 'Ready';
         update inv;
         Test.stopTest(); 
         AgentPortalPopularSearches.getAllDubaiVillas();
         AgentPortalPopularSearches.getAllOneBHKInventories();
         AgentPortalPopularSearches.getAllTwoBHKInventories();
         AgentPortalPopularSearches.getAllOneMillionVillas();
         AgentPortalPopularSearches.getAllHotelRoomsIndubai();
         AgentPortalPopularSearches.getAllHotelApartmentsdubai();
    
}

}