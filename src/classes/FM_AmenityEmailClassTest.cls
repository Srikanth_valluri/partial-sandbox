/*-------------------------------------------------------------------------------------------------
Description: Test class for FM_AmenityEmailClass
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 21-11-2017       | Lochana Rajput   | 1. Draft
=============================================================================================================================
*/
@isTest
private class FM_AmenityEmailClassTest {

	@isTest static void test_method_one() {
		insert new Contact(LastName='Test');
		Resource__c objRes = new Resource__c();
        objRes.Chargeable_Fee_Slot__c = 10;
        objRes.Non_Operational_Days__c = 'Sunday';
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.No_of_advance_booking_days__c = 2;
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.Deposit__c = 3;
        objRes.No_of_deposit_due_days__c = 1;
        insert objRes;
		ContentVersion contentVersion_1 = new ContentVersion(
            Title='Header_Picture1',
            PathOnClient ='/Header_Picture1.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            origin = 'H'
        );
        insert contentVersion_1;

        ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];

        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = objRes.id;
        contentlink.contentdocumentid = contentVersion_2.contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;
		System.debug('===contentlink====' +contentlink.LinkedEntityId);
		System.debug('===objRes.id====' +objRes.id);
		Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                            get('Amenity Booking').getRecordTypeId();
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Booking_Date__c = Date.Today();
		objFMCase.Tenant_Email__c = 'test@gmail.com';
        objFMCase.Resource_Booking_Type__c = 'FM Amenity';
        objFMCase.RecordTypeId =devRecordTypeId;
        objFMCase.Booking_Start_Time_p__c = '10:00';
        objFMCase.Booking_End_Time_p__c = '10:30';
		objFMCase.Resource__c = objRes.ID;
        objFMCase.Amenity_Booking_Status__c = 'Booking Confirmed';
        insert objFMCase;
		Test.startTest();
		FM_AmenityEmailClass.sendConfirmationEmail(new List<FM_Case__c>{objFMCase});
		Test.stopTest();
	}
}