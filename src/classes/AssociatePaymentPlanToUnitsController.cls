/****************************************************************************************************
* Name               : AssociatePaymentPlanToUnitsController
* Description        : Controller class for Associate Payment PlantoUnits VF Page
* Created Date       : 16/10/2020
* Created By         : QBurst
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         16/10/2020      Initial Draft
****************************************************************************************************/

    global with sharing class AssociatePaymentPlanToUnitsController {
    public List<String> propertyNames {get; set;}
    public string propertyId {get; set;}
    public boolean propertyExists {get; set;}
    public string propertyName {get; set;}
    public Map<String, Property__c> propertyMap;
    public static string buildingDetails {get;set;}
    public Map<String, Id> planNamesMap {get; set;}
    public List<String> planNamesList {get; set;}
    public List<Inventory__c> inventoryList {get;set;}
    public string unitIds{get;set;}
    public String[] unitIdList{get;set;}
    public string planName {get;set;}
    public string isProjectToUpdate {get;set;}

    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public AssociatePaymentPlanToUnitsController() {
        propertyNames = new List<String>();
        propertyName = '';
        planNamesMap = new  Map<String, Id>();
        planNamesList = new List<String>();
        isProjectToUpdate = '';
        propertyMap = new  Map<String, Property__c>();
        for(Property__c property: [SELECT Id, Property_Name__c, Property_ID__c 
                                   FROM Property__c 
                                   WHERE Property_Name__c != ''
                                   ORDER BY Property_Name__c ASC]){
            propertyMap.put(property.Property_Name__c, property);
            propertyNames.add(property.Property_Name__c);
        }
        for(Payment_Plan__c payPlan: [SELECT Id, Name, Building_ID__c, Booking_Unit__c, 
                                        Payment_Term_Description__c, Active__c, 
                                        Effective_To_calculated__c, Effective_from__c,
                                        Effective_To__c
                                 FROM Payment_Plan__c
                                 WHERE Active__c = TRUE
                                 ORDER BY CreatedDate DESC
                                 LIMIT 1000]){
            String planName = payPlan.Name;
            if(payPlan.Payment_Term_Description__c != null  && payPlan.Payment_Term_Description__c != ''){
                planName += ' - ' + payPlan.Payment_Term_Description__c;
            }
            planNamesList.add(planName);
            planNamesMap.put(planName, payPlan.Id);
        }
    }

    /************************************************************************************************
    * @Description : method to fetch the Buildng Details.
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
    public void fetchPropertyDetails(){
        String projName = ApexPages.currentPage().getParameters().get('projectName');
        Property__c property =  propertyMap.get(projName);
        system.debug('property: ' + property);
        propertyId = property.Id;
        fetchPropertyDetails(property.Id);
    }

    /************************************************************************************************
    * @Description : method to fetch the  Property Details.
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
    public void fetchPropertyDetails(String proId) {

        Map<Id, Location__c> locationMap = new Map<Id, Location__c>();
        Map<Id, Boolean > locationPayPlanMap = new Map<Id, Boolean>();
        Map<Id, string> locationFloorMap = new Map<Id, string>();
        Map<Id, Location__c> locationUnitMap = new Map<Id, Location__c>();
        List<UnitDetailsWrapper> wrapperList = new List<UnitDetailsWrapper>();
        Map<Inventory__c, Id> locationInventoryMap = new Map<Inventory__c, Id>();
        inventoryList = new List < Inventory__c > ();
        for (Location__c loc: [SELECT Id, Name, Building_Name__c FROM Location__c
                              WHERE Location_Type__c = 'Building'AND Property_Name__c = :proId]) {
            locationMap.put(loc.Id, loc);
        }
        system.debug('locationMap: ' + locationMap);
        for (Location__c loc: [SELECT Id, Name, Building_Name__c, Building_Number__c
                              FROM Location__c WHERE Location_Type__c = 'Floor'
                               AND Building_Number__c IN: locationMap.keyset()]) {
            locationFloorMap.put(loc.id, loc.Building_Name__c);
        }
        system.debug('locationFloorMap: ' + locationFloorMap);

        for (Location__c loc: [SELECT Id, Name, Unit_Name__c, Location_Code__c FROM Location__c
                              WHERE RecordType.Name = 'Unit'
                              AND Floor_Number__c IN: locationFloorMap.keyset() ORDER BY Unit_Name__c ASC]) {
            locationUnitMap.put(loc.Id, loc);
        }
        system.debug('locationUnitMap: ' + locationUnitMap);
        for (Inventory__c inv: [SELECT Id, Status__c, Unit_Location__c, Bedroom_Type_Code__c, Floor__c,Plot_Area_sft__c,Area_sft__c ,
                                Unit_Name__c, Building_Name__c, Marketing_Name__c, Area__c, Plot_Area__c, Parking__c, Price__c
                                FROM Inventory__c
                                WHERE Unit_Location__c IN: locationUnitMap.keyset()]) {
            inventoryList.add(inv);
            locationInventoryMap.put(inv, inv.Unit_Location__c);
        }
        System.debug('inventoryList'+inventoryList.size());
        for(Payment_Plan_Association__c planAssoc : [SELECT Id, Payment_Plan__c, Location__c
                                                 FROM Payment_Plan_Association__c
                                                 WHERE Location__c IN: locationUnitMap.keyset()]){
            locationPayPlanMap.put(planAssoc.Location__c , true);
        }

        system.debug('payplanMap' + locationPayPlanMap);
        for (Inventory__c inv: locationInventoryMap.keyset()) {
            Boolean payPlan = false;
            string unit;
            string unitId;
            string buildingName;
            string status;
            string plotArea;
            string types;
            string marketingName;
            string parking;
            string price;
            string terms = 'available';
            string floor;
            string area;
            if (locationPayPlanMap.get(locationInventoryMap.get(inv)) != null) {
                payPlan = locationPayPlanMap.get(locationInventoryMap.get(inv));
            } else {
                terms = 'Not available';
            }
            unit = inv.Unit_Name__c;
            unitId = inv.Unit_Location__c;
            buildingName = inv.Building_Name__c;
            if(inv.Price__c != null){
            	price = String.valueOf(decimal.valueOf(inv.Price__c).setScale(2, RoundingMode.HALF_UP).format());
            }
            parking = inv.Parking__c;
            floor = inv.Floor__c;
            if(inv.Plot_Area_sft__c != null ){
            	plotArea = String.valueOf(inv.Plot_Area_sft__c.setScale(2, RoundingMode.HALF_UP).format());
            }else{
                plotArea = String.valueOf(inv.Plot_Area_sft__c);
            }
            
            marketingName = inv.Marketing_Name__c;
            status = inv.Status__c;
            types = inv.Bedroom_Type_Code__c;
            if(inv.Area_sft__c  != null ){
            	area = String.valueOf(inv.Area_sft__c .setScale(2, RoundingMode.HALF_UP).format());
            }else{
                area = String.valueOf(inv.Area__c);
            }
            UnitDetailsWrapper details = new UnitDetailsWrapper();
            details.buildingName = buildingName;
            details.status = status;
            details.plotArea = plotArea;
            details.types = types;
            details.marketingName = marketingName;
            details.parking = parking;
            details.price = price;
            details.terms = terms;
            details.floor = floor;
            details.unitId = unitId;
            details.unit = unit;
            details.area = area;
            wrapperList.add(details);
        }
        system.debug('wrapper: ' + wrapperList);
        buildingDetails = JSON.serialize(wrapperList);
        system.debug('buildingDetails: ' + buildingDetails);
    }

   /************************************************************************************************
    * @Description : method to create the  AssociatePaymentPlan
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
   public void createAssociatePlan() {
      //string planName = ApexPages.currentPage().getParameters().get('planName'); 
     // List<string> unitIds = ApexPages.currentPage().getParameters().get('unitIds');
    system.debug('planNAME: ' + planName);
    system.debug('unitIds: ' + unitIds);
    system.debug('project: ' + isProjectToUpdate + ', property: ' + propertyId);
    List<Payment_Plan_Association__c> plansToInsert = new List<Payment_Plan_Association__c>();
    String[] unitIdList = new List<String>();
    List<id> planIds = new List<id>();
    List<Payment_Terms__c> payTermsToInsert;
    if(String.isNotBlank(unitIds)) {
        for(String eachStr : unitIds.split(',')) {
            unitIdList.add(eachStr);
        }
    }
    Payment_Plan__c explan = [select Id, Building_ID__c, Name, Building_Location__c,
                                  Effective_From__c,Effective_To__c ,
                                  (SELECT Id, Description__c, Milestone_Event__c, Line_ID__c,
                                                 Milestone_Event_Arabic__c, Percent_Value__c, Payment_Date__c,
                                                 Installment__c, Modified_Percent_Value__c, Seq_No__c, Event_Days__c
                                                 FROM Payment_Terms__r ORDER  BY Installment__c),
                                  Effective_To_calculated__c, TERM_ID__c from Payment_Plan__c Where Name=: planName.trim()];
        system.debug('explan: ' + explan.id);
        if(isProjectToUpdate != ''){
            Payment_Plan_Association__c planAssoc = new Payment_Plan_Association__c();
            planAssoc.Payment_Plan__c = explan.Id;
            planAssoc.Location__c = null;
            planAssoc.Effective_From__c = system.today();
            planAssoc.Property__c = propertyId;
            plansToInsert.add(planAssoc);
        } else{
            for(string id : unitIdList){
                Payment_Plan_Association__c planAssoc = new Payment_Plan_Association__c();
                planAssoc.Payment_Plan__c = explan.Id;
                planAssoc.Location__c = id;
                planAssoc.Effective_From__c = system.today();
                plansToInsert.add(planAssoc);
            } 
        }
        system.debug('plansToInsert'+plansToInsert);
        Database.SaveResult[] srList = Database.insert(plansToInsert,false);
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                System.debug('Successfully inserted PaymentPlan. ID: ' + sr.getId());
                planIds.add(sr.getId());
            } else{
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Fields that affected this error: ' + err.getFields());
                }
            }
        }
    }

    /************************************************************************************************
    * @Description : Wrapper Class to wrap the  Unit Details
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
    public class UnitDetailsWrapper{
        public string unit;
        public string unitId;
        public string buildingName;
        public string status;
        public string plotArea;
        public string types;
        public string marketingName;
        public string parking;
        public string price;
        public string terms;
        public string floor;
        public string area;
        public UnitDetailsWrapper(){
        }
    }
}