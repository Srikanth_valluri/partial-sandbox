/*
Class Name : HDAppUpdateAccountAdditionalDetails
Description : To update additional profile details for an account 
Test Class : HDAppUpdateAccountAdditionalDetailsTest   

========================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
--------------------------------------------------------------------------------------------------------
1.0     |   03-11-2020      | Subin Antony        | Added logic to save whatsapp number for a profile
*******************************************************************************************************/
@RestResource(urlMapping='/updateAccountAdditionalDetails/*')
global class HDAppUpdateAccountAdditionalDetails {

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Technical error'
    };

    @httpPost
    global static FinalReturnWrapper ProcessUpdateDetails() {

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();

        List<User> userLst = new List<User>();

        RestRequest r = RestContext.request;
        Blob body = r.requestBody;
        String jsonString = body.toString();
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);
        
        if(String.isBlank(jsonString)) {
            objMeta.message = 'No JSON body found';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        if(String.isNotBlank(jsonString)) {

            System.debug('jsonString:: ' + jsonString);
            AdditionalDetailsWrapper objAddDetailsWrap = (AdditionalDetailsWrapper)JSON.deserialize(jsonString,AdditionalDetailsWrapper.class);
            System.debug('objAddDetailsWrap:: ' + objAddDetailsWrap);

            Account accObj  = new Account();
            List<Account> acc  = new List<Account>();
            String accountId;
            if(String.isNotBlank(objAddDetailsWrap.account_id)) {
                //accObj = GetCustomerDetailsForHDApp_API.GetAccountDetails(objAddDetailsWrap.account_id);
                acc = [SELECT id, Party_Type__c FROM Account WHERE id=:objAddDetailsWrap.account_id];
                System.debug('acc fetched:: ' + acc);
                
                if(acc != null && acc.size() > 0) {
                    accountId = acc[0].id;
                }
                else {
                    objMeta.message = 'No Account record found for given account_id';
                    objMeta.status_code = 3;
                    objMeta.title = mapStatusCode.get(3);
                    objMeta.developer_message = null;

                    returnResponse.meta_data = objMeta;
                    return returnResponse;
                }
            }
            else{
                objMeta.message = 'Missing parameter value : account_id';
                objMeta.status_code = 3;
                objMeta.title = mapStatusCode.get(3);
                objMeta.developer_message = null;

                returnResponse.meta_data = objMeta;
                return returnResponse;

            }
            

            //updating account with new details
            try {
                accObj.id = acc[0].id;

                if(acc[0].Party_Type__c == 'PERSON') {

                    accObj.Date_of_Birth__pc = Date.valueOf(objAddDetailsWrap.date_of_birth);
                    accObj.Primary_Language__c = objAddDetailsWrap.preferred_language;

                    cls_contact_details objCont = objAddDetailsWrap.contact_details;
                    //for Person mobile phone field
                    if(objCont != null && objCont.mobile_details.size() > 0) {
                        for(cls_mobile_details objMD : objCont.mobile_details) {

                            if(objMD.index == 2) {
                                accObj.Mobile_Country_Code_2__pc = objMD.mobile_country_code;
                                accObj.Mobile_Phone_Encrypt_2__pc = objMD.mobile_number;
                            }
                            if(objMD.index == 3) {
                                accObj.Mobile_Country_Code_3__pc = objMD.mobile_country_code;
                                accObj.Mobile_Phone_Encrypt_3__pc = objMD.mobile_number;
                                
                            }
                            if(objMD.index == 4) {
                                accObj.Mobile_Country_Code_4__pc = objMD.mobile_country_code;
                                accObj.Mobile_Phone_Encrypt_4__pc = objMD.mobile_number;
                                
                            }
                            if(objMD.index == 5) {
                                accObj.Mobile_Country_Code_5__pc = objMD.mobile_country_code;
                                accObj.Mobile_Phone_Encrypt_5__pc = objMD.mobile_number;
                            }
                        }//For End
                    }

                    //for Person Email field
                    if(objCont != null && objCont.email_details.size() > 0) {
                        for(cls_email_details objED : objCont.email_details) {

                            if(objED.index == 2) {
                                if(String.isNotBlank(objED.email_address) && !validateEmailPattern(objED.email_address)) {
                                    objMeta.message = 'Please enter a valid email address';
                                    objMeta.status_code = 3;
                                    objMeta.title = mapStatusCode.get(3);
                                    objMeta.developer_message = null;
                            
                                    returnResponse.meta_data = objMeta;
                                    return returnResponse;
                                } 
                                accObj.Email_2__pc = objED.email_address;
                            }
                            if(objED.index == 3) {
                                if(String.isNotBlank(objED.email_address) && !validateEmailPattern(objED.email_address)) {
                                    objMeta.message = 'Please enter a valid email address';
                                    objMeta.status_code = 3;
                                    objMeta.title = mapStatusCode.get(3);
                                    objMeta.developer_message = null;
                            
                                    returnResponse.meta_data = objMeta;
                                    return returnResponse;
                                } 
                                accObj.Email_3__pc = objED.email_address;
                            }
                            if(objED.index == 4) {
                                if(String.isNotBlank(objED.email_address) && !validateEmailPattern(objED.email_address)) {
                                    objMeta.message = 'Please enter a valid email address';
                                    objMeta.status_code = 3;
                                    objMeta.title = mapStatusCode.get(3);
                                    objMeta.developer_message = null;
                            
                                    returnResponse.meta_data = objMeta;
                                    return returnResponse;
                                } 
                                accObj.Email_4__pc = objED.email_address;
                            }
                            if(objED.index == 5) {
                                if(String.isNotBlank(objED.email_address) && !validateEmailPattern(objED.email_address)) {
                                    objMeta.message = 'Please enter a valid email address';
                                    objMeta.status_code = 3;
                                    objMeta.title = mapStatusCode.get(3);
                                    objMeta.developer_message = null;
                            
                                    returnResponse.meta_data = objMeta;
                                    return returnResponse;
                                } 
                                accObj.Email_5__pc = objED.email_address;
                            }
                        }//For End
                    }
                    
                    if(NULL != objCont.whatsapp_number) {  /* Added on 30-10-2020 */
                        accObj.WhatsApp_Number__c = objCont.whatsapp_number;
                    }
                }

                //Business Account
                else {

                    accObj.Date_Of_Birth__c = Date.valueOf(objAddDetailsWrap.date_of_birth);
                    accObj.Primary_Language__c = objAddDetailsWrap.preferred_language;

                    cls_contact_details objCont = objAddDetailsWrap.contact_details;
                    //for Business mobile phone field
                    if(objCont != null && objCont.mobile_details.size() > 0) {
                        for(cls_mobile_details objMD : objCont.mobile_details) {

                            if(objMD.index == 2) {
                                accObj.Mobile_Country_Code_2__c = objMD.mobile_country_code;
                                accObj.Mobile_Phone_2__c = objMD.mobile_number;
                            }
                            if(objMD.index == 3) {
                                accObj.Mobile_Country_Code_3__c = objMD.mobile_country_code;
                                accObj.Mobile_Phone_3__c = objMD.mobile_number;
                                
                            }
                            if(objMD.index == 4) {
                                accObj.Mobile_Country_Code_4__c = objMD.mobile_country_code;
                                accObj.Mobile_Phone_4__c = objMD.mobile_number;
                                
                            }
                            if(objMD.index == 5) {
                                accObj.Mobile_Country_Code_5__c = objMD.mobile_country_code;
                                accObj.Mobile_Phone_5__c = objMD.mobile_number;
                            }
                        }//For End
                    }

                    //for Business Email field
                    if(objCont != null && objCont.email_details.size() > 0) {
                        for(cls_email_details objED : objCont.email_details) {

                            if(objED.index == 2) {
                                if(String.isNotBlank(objED.email_address) && !validateEmailPattern(objED.email_address)) {
                                    objMeta.message = 'Please enter a valid email address';
                                    objMeta.status_code = 3;
                                    objMeta.title = mapStatusCode.get(3);
                                    objMeta.developer_message = null;
                            
                                    returnResponse.meta_data = objMeta;
                                    return returnResponse;
                                }
                                accObj.Email_1__c = objED.email_address;
                            }
                            if(objED.index == 3) {
                                if(String.isNotBlank(objED.email_address) && !validateEmailPattern(objED.email_address)) {
                                    objMeta.message = 'Please enter a valid email address';
                                    objMeta.status_code = 3;
                                    objMeta.title = mapStatusCode.get(3);
                                    objMeta.developer_message = null;
                            
                                    returnResponse.meta_data = objMeta;
                                    return returnResponse;
                                }
                                accObj.Email_2__c = objED.email_address;
                            }
                            if(objED.index == 4) {
                                if(String.isNotBlank(objED.email_address) && !validateEmailPattern(objED.email_address)) {
                                    objMeta.message = 'Please enter a valid email address';
                                    objMeta.status_code = 3;
                                    objMeta.title = mapStatusCode.get(3);
                                    objMeta.developer_message = null;
                            
                                    returnResponse.meta_data = objMeta;
                                    return returnResponse;
                                }
                                accObj.Email_3__c = objED.email_address;
                            }
                            //if(objED.index == 5) {
                            //    accObj.Email_5__pc = objED.email_address;
                            //}
                        }//For End
                    }
                    
                    if(NULL != objCont.whatsapp_number) {  /* Added on 30-10-2020 */
                        accObj.WhatsApp_Number__c = objCont.whatsapp_number;
                    }
                }//END of Business Account

                System.debug('accObj before update::' + accObj);
                update accObj;
                System.debug('accObj after update::' + accObj);

                objMeta.message = 'successful';
                objMeta.status_code = 1;
                objMeta.title = mapStatusCode.get(1);
                objMeta.developer_message = null;

            }//
            catch(Exception e) {
                objMeta.message = e.getMessage();
                objMeta.status_code = 2;
                objMeta.title = mapStatusCode.get(2);
                objMeta.developer_message = null;
            }
            /*Main logic END*/
        }

        returnResponse.meta_data = objMeta;
        System.debug('returnResponse:: '+returnResponse);
        return returnResponse;
       
    }

    public static boolean validateEmailPattern(String email) {

        System.debug('email:: ' + email);
        Boolean isEmailValid = Pattern.matches('[a-zA-Z0-9._-]+@[a-zA-Z]+.[a-zA-Z]{2,4}[.]{0,1}[a-zA-Z]{0,2}', email) ? true : false;

        System.debug('isEmailValid:: ' + isEmailValid);
        return isEmailValid;
    }

    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_meta_data {
        //public Boolean is_success;
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }
    public class cls_data {

    }

    public class AdditionalDetailsWrapper {
        public String account_id;
        public String preferred_language;
        public String date_of_birth;
        public cls_contact_details contact_details;
    }

    public class cls_contact_details {
        public cls_email_details[] email_details;
        public cls_mobile_details[] mobile_details;
        public String whatsapp_number; /* Added on 30-10-2020 */
    }

    public class cls_email_details {
        public String email_address;
        public Integer index;
    }

    public class cls_mobile_details {
        public String mobile_country_code;
        public String mobile_number;
        public Integer index;
    }

}