@isTest
private class CC_NotificationRenderControllerTest {

    @isTest
    static void testGetNotification() {
        Test.startTest();
        List<NotificationService.Notification> lstNotification = CC_NotificationRenderController.getNotifications(
            new NotificationService.Notification()
        );
        Test.stopTest();
        System.assert(lstNotification.isEmpty());
    }

    @isTest
    static void testMarkRead() {
        Test.startTest();
        List<NotificationService.Notification> lstNotification = CC_NotificationRenderController.markRead(
            new List<NotificationService.Notification>()
        );
        Test.stopTest();
        System.assert(lstNotification.isEmpty());
    }

}