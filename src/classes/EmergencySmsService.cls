/**
 * @File Name          : EmergencySmsService.cls
 * @Description        : 
 * @Author             : arjunK@@espl.com
 * @Group              : 
 * @Last Modified By   : arjunK@@espl.com
 * @Last Modified On   : 10/13/2019, 4:06:58 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/13/2019   arjunK@@espl.com     Initial Version
**/
//*****************************AKISHOR 02-Aug-2019: Emergency SMS Service for Visitor Mgmt System***************************//
//*******************VMS will call to pass building names and visitors mobile numbers to send emergency SMS**************//

/*

{

        "strMessageInput": {
            "objBuildingName":
                [
                    {"buildName":"DRZ"},
                    {"buildName":"AH"},
                    {"buildName":"DH"}
                ],
            "objVisitorMibiles":
                [
                    {"visNumber":"009715454564"},
                    {"visNumber":"00971225454"}
                ]
        }
}

*/


@RestResource(urlMapping='/EmergencySMSService/*')
global with sharing class EmergencySmsService {

    global class MessageInput{
        global List<BuildingName> objBuildingName;//{DH,DRZ}
        global List<VisitorMobiles> objVisitorMibiles;
        global MessageInput(){
            this.objBuildingName = new List<BuildingName>();
            this.objVisitorMibiles= new List<VisitorMobiles>();
        }    
    }

    global class BuildingName {
        global String buildName {get;set;} 
        global BuildingName(){
            this.buildName = '';
        }    
    }
    global class VisitorMobiles {
        global String visNumber {get;set;} 
        global VisitorMobiles(){
            this.visNumber = '';
        }    
    }

   @HttpPost
   global static String EmergencySMS( MessageInput strMessageInput ) {
      system.debug('List1 : MessageInput : '+strMessageInput);
      system.debug('List1 : MessaobjBuildingNamegeInput : '+strMessageInput.objBuildingName);
      system.debug('List1 : objVisitorMibiles : '+strMessageInput.objVisitorMibiles);
        if( strMessageInput != null ) { 
            
            set<String> setBuildName = new set<String>();
            set<String> setVisitorNumber = new set<String>();

            if( strMessageInput.objBuildingName != null 
             && strMessageInput.objBuildingName.size() > 0  ) {
                for( BuildingName objBuildingName : strMessageInput.objBuildingName ) {
                    if( String.isNotBlank( objBuildingName.buildName )  ) {
                        setBuildName.add( objBuildingName.buildName );
                    }
                }
            }
            


            system.debug('setBuildName : '+  setBuildName );
            if( setBuildName != null 
             && setBuildName.size() > 0  ) {
                List<Booking_Unit__c> listUpdBU = [ SELECT Id
                                                         , Emergency_SMS__c
                                                         , Resident__r.Mobile_Phone_Encrypt__pc
                                                         , Resident__c 
                                                      FROM Booking_Unit__c 
                                                     WHERE Inventory__r.Property_Code__c IN: setBuildName
                                                       AND Resident__c != Null
                                                       AND Resident__r.Mobile_Phone_Encrypt__pc != Null
                                                       AND Inventory__c != Null
                                                        AND Id = 'a0x0Y000002Tmzp'  ]; 
                System.debug('listUpdBU=' + listUpdBU);
                if( listUpdBU != null 
                 && listUpdBU.size() > 0  ) {
                   Database.executeBatch(new EmergencySmsBatch(listUpdBU),1);
                }
                
                /*if( setVisitorNumber != null 
                && setVisitorNumber.size() > 0  ) {
                    String msgContents = 'test visitors Emergency sms';
                    for(String strPhone : setVisitorNumber) {
                        EmergencySMSUtility.sendEmergencySmsToVisitors(strPhone, msgContents);
                    }
                }*/

            }// end of setBuildName check

            if( strMessageInput.objVisitorMibiles != null 
             && strMessageInput.objVisitorMibiles.size() > 0  ) {
                
                String msgContents = 'Dear Resident,';
                msgContents =msgContents +'\nTHIS IS A TEST MESSAGE OF EMERGNCY EVACUTION MESSAGE , PLEASE IGNORE';
                msgContents =msgContents +'\n Regards\n';
                msgContents =msgContents +'LOAMS';

                /*String msgContents = 'Dear Visitor,';
                msgContents =msgContents +'\nThere is a fire in the building, please vaccate the building immediatly.';
                msgContents =msgContents +'\n Regards\n';
                msgContents =msgContents +'LOAMS';*/
                for( VisitorMobiles objVisitorMobiles : strMessageInput.objVisitorMibiles ) {
                    if( String.isNotBlank( objVisitorMobiles.visNumber )  ) {
                        //setVisitorNumber.add( objVisitorMobiles.visNumber );
                        EmergencySMSUtility.sendEmergencySmsToVisitors(objVisitorMobiles.visNumber, msgContents);
                    }
                }
            }

        }// end of strMessageInput check
        return null;   
             
    }//end of method     
}//end of service