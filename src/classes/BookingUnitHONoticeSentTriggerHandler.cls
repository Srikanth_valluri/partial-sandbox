/* create unit wise calling list for collection after HO notices are sent and assign those as below:
*  Create calling list of type “Collection Calling List” and calling list type as “Handover list”.
*  Assign to collection user who is already handling any unit of same customer else tag with queue
*/
global without sharing class BookingUnitHONoticeSentTriggerHandler implements Schedulable {

    /*public static void onAfterUpdate( List<Booking_Unit__c > newBooking_Unit , Map<Id , Booking_Unit__c> buOldMap ) {
        createCollectionCLOnHONoticeSent(newBooking_Unit,buOldMap);
    }*/

    global List<Booking_Unit__c > newBooking_Unit;
    global Map<Id , Booking_Unit__c> buOldMap;
    private static final Id INFORMATIC_INTEGRATION_USER_ID = [Select id,name, contactId from User WHERE Name = 'DAMAC Integration' AND Profile.Name = 'System Administrator'].Id;

    global static void onAfterUpdate( List<Booking_Unit__c > newBooking_Unit , Map<Id , Booking_Unit__c> buOldMap ) {
        set<String> setRegId = new set<String>();
        system.debug('newBooking_Unit : '+newBooking_Unit);
        system.debug('buOldMap : '+buOldMap);
        If( UserInfo.getUserId() != INFORMATIC_INTEGRATION_USER_ID ) {
            for( Booking_Unit__c objBu : newBooking_Unit ) {
                if ( objBu != null && String.isNotBlank( objBu.Registration_ID__c ) &&
                    objBu.Handover_Notice_Sent__c  != buOldMap.get(objBu.Id).Handover_Notice_Sent__c 
                  && objBu.Handover_Notice_Sent__c == true ) {
                    setRegId.add(objBu.Registration_ID__c);
                }
            }

            if( setRegId != null && setRegId.size() > 0 ) {
        
                BookingUnitHONoticeSentTriggerHandler objBuHONoticeSent = new BookingUnitHONoticeSentTriggerHandler();
                objBuHONoticeSent.newBooking_Unit = newBooking_Unit;
                objBuHONoticeSent.buOldMap = buOldMap;
                DateTime dt = system.now().addMinutes(1);
                String day = string.valueOf(dt.day());
                String month = string.valueOf(dt.month());
                String hour = string.valueOf(dt.hour());
                String minute = string.valueOf(dt.minute());
                String second = string.valueOf(dt.second());
                String year = string.valueOf(dt.year());
                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            
                String jobID = system.schedule('create unit wise calling list for collection after HO notices '+newBooking_Unit[0].Id + system.now(), strSchedule, objBuHONoticeSent);
            }
        }
    }
    
    global void execute(SchedulableContext ctx) {
        createCollectionCLOnHONoticeSent(newBooking_Unit,buOldMap);
    }
    
    global static void createCollectionCLOnHONoticeSent( List<Booking_Unit__c > newBooking_Unit , Map<Id , Booking_Unit__c> buOldMap ) {
        set<String> setRegId = new set<String>();
        
        for( Booking_Unit__c objBu : newBooking_Unit ) {
            if ( objBu != null && String.isNotBlank( objBu.Registration_ID__c ) &&
                objBu.Handover_Notice_Sent__c  != buOldMap.get(objBu.Id).Handover_Notice_Sent__c 
              && objBu.Handover_Notice_Sent__c == true ) {
                setRegId.add(objBu.Registration_ID__c);
            }
        }

        if( setRegId != null && setRegId.size() > 0 ) {
            createUnitWiseCL(setRegId);
        }
    }

    @Future(callout=true)   
    global static void createUnitWiseCL( set<String> setRegId ) {
        List<Calling_List__c> newCallingList = new List<Calling_List__c>();
        Map<String,List<Calling_List__c>> mapRegIdToobjCalling_List  = new Map<String,List<Calling_List__c>>();
        List<Calling_List__c> callingListToUpdate = new List<Calling_List__c>();
    
        if ( setRegId != null && setRegId.size() > 0 ) {
            Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
            for( Calling_List__c objCalling_List : [ SELECT Id,OwnerId,RecordTypeId,Account__c,Calling_List_Type__c
                                                          , Uncollectible_Category__c,Registration_ID__c
                                                       FROM Calling_List__c 
                                                      WHERE Account__c != null 
                                                        AND IsHideFromUI__c = false 
                                                        AND RecordTypeId =: devRecordTypeId
                                                        AND Registration_ID__c IN: setRegId 
                                                        AND IsHideFromUI__c = false
                                                        AND Customer_Flag__c = true] ) {
                if( mapRegIdToobjCalling_List.containsKey(objCalling_List.Registration_ID__c) && mapRegIdToobjCalling_List.get( objCalling_List.Registration_ID__c ) != null ) {
                    List<Calling_List__c> lstCL = mapRegIdToobjCalling_List.get( objCalling_List.Registration_ID__c );
                    lstCL.add(objCalling_List);
                    mapRegIdToobjCalling_List.put( objCalling_List.Registration_ID__c,lstCL );
                } else {
                    mapRegIdToobjCalling_List.put( objCalling_List.Registration_ID__c,new List<Calling_List__c>{objCalling_List});
                }
            }
            
            system.debug( ' mapRegIdToobjCalling_List :'+mapRegIdToobjCalling_List );
            system.debug( ' newBooking_Unit :'+setRegId );
            
            List<Error_Log__c> errorLogList = new List<Error_Log__c>();
            for( Booking_Unit__c objBu : [SELECT Id ,Handover_Notice_Sent__c,Registration_ID__c,Party_Id__c,Account_Id__c 
                                            FROM Booking_Unit__c WHERE Registration_ID__c IN: setRegId 
                                             AND Handover_Notice_Sent__c = true ]) {
                if ( objBu != null && 
                    //objBu.Handover_Notice_Sent__c  != buOldMap.get(objBu.Id).Handover_Notice_Sent__c 
                    //&& objBu.EHO_Notice_Sent__c == true ) {
                    objBu.Handover_Notice_Sent__c == true ) {
                    if( !mapRegIdToobjCalling_List.containsKey( objBu.Registration_ID__c ) ) {
                        system.debug( ' in not contains :'+mapRegIdToobjCalling_List );
                        Calling_List__c callbackCallInst = new Calling_List__c(RecordTypeId = devRecordTypeId,
                                                                                Customer_Flag__c = true,
                                                                                Calling_List_Type__c = 'Handover list',
                                                                                Registration_ID__c = objBu.Registration_ID__c,
                                                                                Party_ID__c = objBu.Party_Id__c,
                                                                                Uncollectible_Category__c = 'Handover',
                                                                                MQ_Unique_Id__c = objBu.Registration_ID__c +'-'+ Date.Today().Month() +'-'+ Date.Today().Year() );
                        newCallingList.add(callbackCallInst);
                    }else {
                        system.debug( ' in else contains :' );
                        if( mapRegIdToobjCalling_List.get(objBu.Registration_ID__c) != null 
                         && mapRegIdToobjCalling_List.get(objBu.Registration_ID__c).size() > 0) {
                            for( Calling_List__c objCL : mapRegIdToobjCalling_List.get(objBu.Registration_ID__c) ) {
                                if( objCL != null ) {
                                    objCL.Uncollectible_Category__c = 'Handover';
                                    callingListToUpdate.add(objCL);
                                }
                            }
                        }
                        
                        Error_Log__c objErr = new Error_Log__c();
                        objErr.Error_Details__c = 'Calling list already present : '+mapRegIdToobjCalling_List.get(objBu.Registration_ID__c);
                        objErr.Account__c = objBu.Account_Id__c;
                        objErr.Booking_Unit__c = objBu.Id;
                        objErr.Process_Name__c = 'Handover';
                        errorLogList.add(objErr);
                        
                        //objCalling_List.OwnerId =  collectionQueueId;
                    }
                }
            }
            
            if ( newCallingList != null && newCallingList.size() > 0 ) {
                insert newCallingList;
            }
            if( callingListToUpdate != null && callingListToUpdate.size() > 0 ) {
                update callingListToUpdate;
            }
            if ( errorLogList != null && errorLogList.size() > 0 ) {
                insert errorLogList;
            }
        }
    }   
}