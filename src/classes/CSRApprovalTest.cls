@isTest
public class CSRApprovalTest{
    @IsTest
    static void positiveTest1(){
        // Create common test accounts
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Approving_Authorities__c=',Committee,HOD';
        objCase.Parking_Details_JSON__c ='[{"Approving_Authorities__c":",Committee,HOD","AggrementStatus":"Agreement executed by DAMAC","Area":"518.00","PaidAmmount":"1200693","PaidPersentage":"","Price":"1295000.00","PropertyName":"","PSF":"2500.00","RegDate":"2013-09-29","UnitNumber":"DTPC/32/3213"}]';
        //objCase.Approving_User_Role__c = '';
        insert objCase;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        objSR.Agency__c = objAcc.Id ;
        insert objSR ;
        
        Id dealId = objSR.Deal_ID__c;
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
        insert lstBookings ; 
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
        lstBookingUnits[0].Registration_ID__c = '1520';
        insert lstBookingUnits;
        
        String str ='{"CRE_Comments__c":"Test","Booking_Unit__c":"a0x25000000BDbiAAG","RequestType__c":"Area Variation Waiver","Per_Area_Variation_to_be_waived__c":"550"}';
        CaseSummaryRequest__c objcsr = (CaseSummaryRequest__c)JSON.deserialize(str, CaseSummaryRequest__c.class);
        system.debug('obj :'+objcsr);
        objcsr.case__c = objCase.Id;
        objcsr.Booking_Unit__c = lstBookingUnits[0].Id;
                
        insert objcsr;
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        PageReference myVfPage = Page.Case_Summary_Client_Relation;
        Test.setCurrentPage(myVfPage);
        
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('caseId',objCase.Id);
        ApexPages.currentPage().getParameters().put('AccountId',objAcc.Id);
        Test.startTest();
        Case_Summary_Client_RelationController obj = new Case_Summary_Client_RelationController();
        //Case_Summary_Client_RelationController.getBU( new List<String>{lstBookingUnits[0].Id});
        obj.getBU();
        obj.JsonBU = '[{"Approving_Authorities__c":",Committee,HOD","AggrementStatus":"Agreement executed by DAMAC","Area":"518.00","PaidAmmount":"1200693","PaidPersentage":"","Price":"1295000.00","PropertyName":"","PSF":"2500.00","RegDate":"2013-09-29","UnitNumber":"DTPC/32/3213"}]';
        obj.getRequestDetails();
        obj.selectedRequestVal = '1-Waiver or Discount Parking Purchase Price';
        obj.getFields();
        obj.submitSRReset();
        CSRApproval.updateCase(objCase);
        Test.stopTest();
        
        
        
    }
    
    @IsTest
    static void positiveTest2(){
        // Create common test accounts
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Approving_Authorities__c=',Committee,HOD';
        objCase.Parking_Details_JSON__c ='[{"Approving_Authorities__c":",Committee,HOD","AggrementStatus":"Agreement executed by DAMAC","Area":"518.00","PaidAmmount":"1200693","PaidPersentage":"","Price":"1295000.00","PropertyName":"","PSF":"2500.00","RegDate":"2013-09-29","UnitNumber":"DTPC/32/3213"}]';
        objCase.Approving_User_Role__c = '';
        insert objCase;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        objSR.Agency__c = objAcc.Id ;
        insert objSR ;
        
        Id dealId = objSR.Deal_ID__c;
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
        insert lstBookings ; 
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
        lstBookingUnits[0].Registration_ID__c = '1520';
        insert lstBookingUnits;
        
        String str ='{"CRE_Comments__c":"Test","Booking_Unit__c":"a0x25000000BDbiAAG","RequestType__c":"Area Variation Waiver","Per_Area_Variation_to_be_waived__c":"550"}';
        CaseSummaryRequest__c objcsr = (CaseSummaryRequest__c)JSON.deserialize(str, CaseSummaryRequest__c.class);
        system.debug('obj :'+objcsr);
        objcsr.case__c = objCase.Id;
        objcsr.Booking_Unit__c = lstBookingUnits[0].Id;
        
        insert objcsr;
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        PageReference myVfPage = Page.Case_Summary_Client_Relation;
        Test.setCurrentPage(myVfPage);
        
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('caseId',objCase.Id);
        ApexPages.currentPage().getParameters().put('AccountId',objAcc.Id);
        Test.startTest();
        Case_Summary_Client_RelationController obj = new Case_Summary_Client_RelationController();
        //Case_Summary_Client_RelationController.getBU( new List<String>{lstBookingUnits[0].Id});
        obj.getBU();
        obj.JsonBU = '[{"Approving_Authorities__c":",Committee,HOD","AggrementStatus":"Agreement executed by DAMAC","Area":"518.00","PaidAmmount":"1200693","PaidPersentage":"","Price":"1295000.00","PropertyName":"","PSF":"2500.00","RegDate":"2013-09-29","UnitNumber":"DTPC/32/3213"}]';
        obj.getRequestDetails();
        obj.selectedRequestVal = '1-Waiver or Discount Parking Purchase Price';
        obj.getFields();
        obj.submitSRReset();
        CSRApproval.updateCase(objCase);
        Test.stopTest();
        
        
        
    }
    
    @IsTest
    static void positiveTest3(){
        // Create common test accounts
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Approving_Authorities__c=',HOD,Committee,';
        objCase.Parking_Details_JSON__c ='[{"Approving_Authorities__c":",HOD,Committee","AggrementStatus":"Agreement executed by DAMAC","Area":"518.00","PaidAmmount":"1200693","PaidPersentage":"","Price":"1295000.00","PropertyName":"","PSF":"2500.00","RegDate":"2013-09-29","UnitNumber":"DTPC/32/3213"}]';
        objCase.Approving_User_Role__c = '';
        insert objCase;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        objSR.Agency__c = objAcc.Id ;
        insert objSR ;
        
        Id dealId = objSR.Deal_ID__c;
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
        insert lstBookings ; 
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
        lstBookingUnits[0].Registration_ID__c = '1520';
        insert lstBookingUnits;
        
        String str ='{"CRE_Comments__c":"Test","Booking_Unit__c":"a0x25000000BDbiAAG","RequestType__c":"Area Variation Waiver","Per_Area_Variation_to_be_waived__c":"550"}';
        CaseSummaryRequest__c objcsr = (CaseSummaryRequest__c)JSON.deserialize(str, CaseSummaryRequest__c.class);
        system.debug('obj :'+objcsr);
        objcsr.case__c = objCase.Id;
        objcsr.Booking_Unit__c = lstBookingUnits[0].Id;
        
        insert objcsr;
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        PageReference myVfPage = Page.Case_Summary_Client_Relation;
        Test.setCurrentPage(myVfPage);
        
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('caseId',objCase.Id);
        ApexPages.currentPage().getParameters().put('AccountId',objAcc.Id);
        Test.startTest();
        Case_Summary_Client_RelationController obj = new Case_Summary_Client_RelationController();
        //Case_Summary_Client_RelationController.getBU( new List<String>{lstBookingUnits[0].Id});
        obj.getBU();
        obj.JsonBU = '[{"Approving_Authorities__c":",HOD,Committee","AggrementStatus":"Agreement executed by DAMAC","Area":"518.00","PaidAmmount":"1200693","PaidPersentage":"","Price":"1295000.00","PropertyName":"","PSF":"2500.00","RegDate":"2013-09-29","UnitNumber":"DTPC/32/3213"}]';
        //List<Id> caseIds = new List<Id>();
        //caseIds.add(objCase.Id);
        obj.getRequestDetails();
        obj.selectedRequestVal = '1-Waiver or Discount Parking Purchase Price';
        obj.getFields();
        obj.submitSRReset();
        CSRApproval.updateCase(objCase);
        //CSRApproval.getCase(caseIds);
        Test.stopTest();
        
    }
    
    @IsTest
    static void positiveTest4(){
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        //ID adminRoleId = [ Select id from userRole where name = 'Director'].id;
        UserRole userRoleObj = new UserRole(Name = 'HOD', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        User u2 = new User(Alias = 'standt2', Email='stand2@testorg.com', userRoleId=userRoleObj.Id, isActive = true,
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p2.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='adm@testorg123.com');
        insert u2;
        User u1 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
                           LocaleSidKey='en_US', ProfileId = p2.Id, ManagerID = u2.id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='sta0@damactest1234.com');
        insert u1;
        System.runAs(u1) {
            // Create common test accounts
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            objAcc.Email__c ='test@test.com';
            objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
            insert objAcc ;
            
            Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
            System.debug('recTypeCOD :'+ recTypeCOD);
            // insert Case
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
            objCase.Approving_Authorities__c=',HOD,Committee,';
            objCase.Parking_Details_JSON__c ='[{"Approving_Authorities__c":",HOD,Committee","AggrementStatus":"Agreement executed by DAMAC","Area":"518.00","PaidAmmount":"1200693","PaidPersentage":"","Price":"1295000.00","PropertyName":"","PSF":"2500.00","RegDate":"2013-09-29","UnitNumber":"DTPC/32/3213"}]';
            //objCase.Approving_User_Role__c = '';
            objCase.Approving_User_Id__c = u1.Id;
            insert objCase;
            
            //Insert Service Request
            NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
            objSR.Agency__c = objAcc.Id ;
            insert objSR ;
            
            Id dealId = objSR.Deal_ID__c;
            //Insert Bookings
            List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
            insert lstBookings ; 
            
            Inventory__c objInventory = new Inventory__c();
            insert objInventory;
            
            //Insert Booking Units
            List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
            lstBookingUnits[0].Registration_ID__c = '1520';
            lstBookingUnits[0].Inventory__c = objInventory.Id;
            insert lstBookingUnits;
            
            String str ='{"CRE_Comments__c":"Test","Booking_Unit__c":"a0x25000000BDbiAAG","RequestType__c":"Area Variation Waiver","Per_Area_Variation_to_be_waived__c":"550"}';
            CaseSummaryRequest__c objcsr = (CaseSummaryRequest__c)JSON.deserialize(str, CaseSummaryRequest__c.class);
            system.debug('obj :'+objcsr);
            objcsr.case__c = objCase.Id;
            objcsr.Booking_Unit__c = lstBookingUnits[0].Id;
            insert objcsr;
            
            PageReference myVfPage = Page.Case_Summary_Client_Relation;
            Test.setCurrentPage(myVfPage);
            
            // Put Id into the current page Parameters
            ApexPages.currentPage().getParameters().put('caseId',objCase.Id);
            ApexPages.currentPage().getParameters().put('AccountId',objAcc.Id);
            Test.startTest();
            Case_Summary_Client_RelationController obj = new Case_Summary_Client_RelationController();
            //Case_Summary_Client_RelationController.getBU( new List<String>{lstBookingUnits[0].Id});
            obj.getBU();
            obj.JsonBU = '[{"Approving_Authorities__c":",HOD,Committee","AggrementStatus":"Agreement executed by DAMAC","Area":"518.00","PaidAmmount":"1200693","PaidPersentage":"","Price":"1295000.00","PropertyName":"","PSF":"2500.00","RegDate":"2013-09-29","UnitNumber":"DTPC/32/3213"}]';
            List<Id> caseIds = new List<Id>();
            caseIds.add(objCase.Id);
            obj.getRequestDetails();
            obj.selectedRequestVal = '1-Waiver or Discount Parking Purchase Price';
            obj.getFields();
            obj.submitSRReset();
            CSRApproval.updateCase(objCase);
            CSRApproval.getCase(caseIds);
            Test.stopTest();
            
            
        } 
    }
    
}