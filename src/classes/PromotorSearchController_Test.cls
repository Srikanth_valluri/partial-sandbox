/****************************************************************************************************
* Description - Test class developed for PromotorSearchController                                   *
*---------------------------------------------------------------------------------------------------*
* Version   Date        Author          Description                                                 *
* 1.0       27/04/18    Sachin Singh          Initial Draft ,Update the code coverage                                               *
****************************************************************************************************/

@isTest
public class PromotorSearchController_Test {

 public static Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(
        'Agent Team').getRecordTypeId();
    //public static Id agenTeamRT = '0120Y000000FNkA';
    
    
     public static testmethod void getInquryrecordTest1(){
        List<Inquiry__c> lstInquiry = new List<Inquiry__c>();
        for (Integer i = 0; i < 25; i++) {
            Inquiry__c inqObj= new Inquiry__c(
                RecordTypeId=agenTeamRT,
                Inquiry_Source__c='Agent Referral',
                Mobile_Phone_Encrypt__c='456123',
                Mobile_CountryCode__c='American Samoa: 001684',
                Mobile_Phone__c='1234',
                Email__c='mk@gmail.com',
                First_Name__c='Test',
                Last_Name__c='Last',
                Meeting_Type__c = 'Scheduled Tour'
            );
            lstInquiry.add(inqObj);
        }
        insert lstInquiry;
        
        PromotorSearchController obj = new PromotorSearchController();
        obj.getMeetingType();
        system.assertEquals(false, obj.isSF1);
       // Boolean abc = PromotorSearchController.getisSF1(true);
        obj.searchString = 'Test';
        obj.meetingValue = 'Select Meeting Type';
        obj.fstbtn();
        obj.nextbtn();
        obj.endbtn();
       // obj.prvbtn();
        obj.inqClear();
        obj.getnxt();
        obj.getprv();
        obj.sortListASC();
        obj.sortListDSC();
        
        //
        obj.inquiryIdToUpdate = lstInquiry[0].Id;
        obj.updateInquiry();
        
        //
        obj.tourDate = '2030-03-17T00:59';
        obj.userId = Userinfo.getUserId();
        obj.inquiryIdToUpdate = lstInquiry[0].Id;
        obj.updateInquiry();
        
        //
        obj.inquiryIdToUpdate = lstInquiry[0].Id;
        obj.objInquiry.Tour_Outcome__c = 'Not Interested';
        obj.updateInquiry();
        
        //
        obj.inquiryIdToUpdate = '';
        obj.objInquiry.Tour_Outcome__c = 'Not Interested';
        obj.updateInquiry();
        
        obj.searchString = '';
        obj.searchFrmDte = '2030-03-17';
        obj.searchToDte = '2030-03-17';
        obj.searchresult();
    }

    public static testmethod void getInquryrecordTest2(){
        List<Inquiry__c> lstInquiry = new List<Inquiry__c>();
        for (Integer i = 0; i < 10; i++) {
            Inquiry__c inqObj= new Inquiry__c(
                RecordTypeId=agenTeamRT,
                Inquiry_Source__c='Agent Referral',
                Mobile_Phone_Encrypt__c='456123',
                Mobile_CountryCode__c='American Samoa: 001684',
                Mobile_Phone__c='1234',
                Email__c='mk@gmail.com',
                First_Name__c='Test',
                Last_Name__c='Last',
                Meeting_Type__c = 'Scheduled Tour'
            );
            lstInquiry.add(inqObj);
        }
        insert lstInquiry;

        PromotorSearchController obj = new PromotorSearchController();
        obj.getMeetingType();
        obj.searchString = '';
        obj.meetingValue = 'Scheduled Tour';
        obj.fstbtn();
        obj.nextbtn();
        obj.endbtn();
        //obj.prvbtn();
        obj.inqClear();
    }

 public static testmethod void getInquryrecordTest3(){
        PromotorSearchController obj = new PromotorSearchController();
        List<Inquiry__c> lstInquiry = new List<Inquiry__c>();
        for (Integer i = 0; i < 25; i++) {
            Inquiry__c inqObj= new Inquiry__c(
                RecordTypeId=agenTeamRT,
                Inquiry_Source__c='Agent Referral',
                Mobile_Phone_Encrypt__c='456123',
                Mobile_CountryCode__c='American Samoa: 001684',
                Mobile_Phone__c='1234',
                Email__c='mk@gmail.com',
                First_Name__c='Test',
                Last_Name__c='Last',
                Meeting_Type__c = 'Scheduled Tour'
            ); 
            lstInquiry.add(inqObj);
        }
        insert lstInquiry;
        obj.getMeetingType();
        obj.searchString = 'Test';
        obj.meetingValue = 'Scheduled Tour';
        obj.fstbtn();
        obj.nextbtn();
        obj.endbtn();
        //obj.prvbtn();
        obj.inqClear();
        obj.cancel();
       obj.getInquryrecord();
       obj.getMeetingType();
       obj.searchresult();
       obj.inqClear();
       obj.cancel();
       obj.cancelbtn();
       obj.nextbtn();
       obj.prvbtn();
       obj.fstbtn();
       obj.getprv();
       obj.getnxt();
       obj.sortListASC();
       obj.sortListDSC();
       obj.setRecCount();
       obj.updateInquiry();
       
     
    }
   
 public static testmethod void getInquryrecordTest4(){
        PromotorSearchController obj = new PromotorSearchController();
        List<Inquiry__c> lstInquiry = new List<Inquiry__c>();
        for (Integer i = 0; i < 25; i++) {
            Inquiry__c inqObj= new Inquiry__c(
                RecordTypeId=agenTeamRT,
                Inquiry_Source__c='Agent Referral',
                Mobile_Phone_Encrypt__c='456123',
                Mobile_CountryCode__c='American Samoa: 001684',
                Mobile_Phone__c='1234',
                Email__c='mk@gmail.com',
                First_Name__c='Test',
                Last_Name__c='Last',
                Assigned_PC__c = UserInfo.getUserId(),
                Meeting_Type__c = 'Scheduled Tour'
            ); 
            lstInquiry.add(inqObj);
        }
        insert lstInquiry;
        obj.tourDate = '2022-10-22T16:54';
        obj.inquiryIdToUpdate = lstInquiry[0].id;
        obj.getMeetingType();
        obj.searchString = 'Test';
        obj.meetingValue = 'Scheduled Tour';
        obj.fstbtn();
        obj.nextbtn();
        obj.endbtn();
        //obj.prvbtn();
        obj.inqClear();
        obj.cancel();
       obj.getInquryrecord();
       obj.getMeetingType();
       obj.searchresult();
       obj.inqClear();
       obj.cancel();
       obj.cancelbtn();
       obj.nextbtn();
       obj.prvbtn();
       obj.fstbtn();
       obj.getprv();
       obj.getnxt();
       obj.sortListASC();
       obj.sortListDSC();
       obj.setRecCount();
       obj.updateInquiry();
       
       obj.inquiryIdToUpdate = lstInquiry[0].Id;
       obj.scheduleTourComments = 'test';
       obj.createScheduleTask ();
       
       obj.inquiryIdToUpdate = lstInquiry[0].Id;
       obj.tourDate = '2022-10-22T16:54';
       obj.objInquiry.Meeting_Type__c = 'Scheduled Tour';
       obj.updateInquiryMeeting ();
       obj.objInquiry.Meeting_Type__c = 'Meeting On Stand';
       
       obj.updateInquiryMeetingByType();
       obj.createSalesTour (lstInquiry[0].id);
       
       obj.getUserDetails ();
       PromotorSearchController.getTaskId (null, null);
     
    }

  }