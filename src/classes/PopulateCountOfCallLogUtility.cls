public class PopulateCountOfCallLogUtility {

    public void stampCountOfCallLogOnCL(List<Call_Log__c> lstCallLog) {
        Map<Id, List<Call_Log__c>> mapCLToLog = new Map<Id, List<Call_Log__c>>();
        List<Calling_List__c> lstCallingListToUpdate = new List<Calling_List__c>();

        if (!lstCallLog.isEmpty()) {
            for (Call_Log__c objCallLog : lstCallLog) {
                if (objCallLog.Calling_List__c != null) {
                    if (mapCLToLog.containsKey(objCallLog.Calling_List__c)) {
                        List<Call_Log__c> lstTempLog = mapCLToLog.get(objCallLog.Calling_List__c);
                        lstTempLog.add(objCallLog);
                        mapCLToLog.put(objCallLog.Calling_List__c, lstTempLog);
                    } else {
                        mapCLToLog.put(objCallLog.Calling_List__c, new List<Call_Log__c> {objCallLog});
                    }
                }
            }
            
            if (!mapCLToLog.isEmpty()) {
                for (Id objCLId : mapCLToLog.keySet()) {
                    List<Call_Log__c> lstLogs = mapCLToLog.get(objCLId);
                    Calling_List__c objCallingList = new Calling_List__c(Id = objCLId);
                    objCallingList.Call_Count__c = lstLogs.size();
                    lstCallingListToUpdate.add(objCallingList);
                }
                
                if (!lstCallingListToUpdate.isEmpty()) {
                    update lstCallingListToUpdate;
                }
            }
        }
    }
}