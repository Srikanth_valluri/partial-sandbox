/*
 * Description - Handler class for EmailMessageTrigger
 *
 * Version        Date            Author            Description
 * 1.0            01/03/18        Vivek Shinde      Initial Draft
 * 1.1            07/03/18        Vivek Shinde      Changes to reassign case ownership to queue if current owner is inactive
 */
public without sharing class EmailMessageTriggerHandler {
    
    // Method called on after insert trigger event
    public void onAfterInsert(List<EmailMessage> lstEmailMsg) {
        Set<Id> setCaseId = new Set<Id>();
        //Map<Id, Id> mapCaseToEmailMsg = new Map<Id, Id>();
        for(EmailMessage objEmailMsg: lstEmailMsg) {
            if(objEmailMsg.ParentId != null && String.valueOf(objEmailMsg.ParentId).startsWith('500') &&
                objEmailMsg.Incoming) {
                setCaseId.add(objEmailMsg.ParentId);
                //mapCaseToEmailMsg.put(objEmailMsg.ParentId, objEmailMsg.Id);
            }
        }
        
        if(!setCaseId.isEmpty()) {
            /*Map<Id, EmailMessage> mapEmailMsg = new Map<Id, EmailMessage>([Select Id, ParentId, FromAddress,
                FromName, ToAddress, Subject, TextBody, HtmlBody, Status, BccAddress, CcAddress, Headers
                From EmailMessage
                Where Id IN: mapCaseToEmailMsg.values()]);*/
            
            List<Case> lstCaseToUpdate = new List<Case>();
            //Map<Id, Case> mapOldToNewCase = new Map<Id, Case>();
            for(Case objCase: [Select Id, RecordTypeId, Status, Origin, AccountId, OwnerId, Subject, 
                               Case_Reopen_Count__c, Owner.IsActive
                               From Case Where Id IN: setCaseId And Status = 'Closed']) {
                /*Case objNewCase = new Case(
                    RecordTypeId = objCase.RecordTypeId,
                    Subject = objCase.Subject,
                    Status = 'New',
                    Origin = objCase.Origin,
                    AccountId = objCase.AccountId,
                    OwnerId = objCase.OwnerId,
                    ParentId = objCase.Id);
                mapOldToNewCase.put(objCase.Id, objNewCase);*/
                
                objCase.Status = 'New';
                objCase.Case_Reopen_Count__c = objCase.Case_Reopen_Count__c != null ? (objCase.Case_Reopen_Count__c + 1) : 1;
                if(String.valueOf(objCase.OwnerId).startsWith('005') && !objCase.Owner.IsActive) {
                    objCase.OwnerId = getGroupIdFromDeveloperName('Contact_Center_Queue', 'Queue');
                }
                lstCaseToUpdate.add(objCase);
            }
            
            update lstCaseToUpdate;
            //insert mapOldToNewCase.values();
            //system.debug('--mapOldToNewCase.values()--'+mapOldToNewCase.values());
            
            /*List<EmailMessage> lstEmailMsgToInsert = new List<EmailMessage>();
            for(Id idCase: mapOldToNewCase.keySet()) {
                EmailMessage objOldMsg = mapEmailMsg.get(mapCaseToEmailMsg.get(idCase));
                //mapEmailMsg.get(mapCaseToEmailMsg.get(idCase)).ParentId = mapOldToNewCase.get(idCase).Id;
                
                EmailMessage objNewMsg = new EmailMessage();
                objNewMsg = objOldMsg.clone();
                objNewMsg.ParentId = mapOldToNewCase.get(idCase).Id;
                lstEmailMsgToInsert.add(objNewMsg);
            }
            system.debug('--lstEmailMsgToInsert--'+lstEmailMsgToInsert);
            insert lstEmailMsgToInsert;
            
            system.debug('--mapEmailMsg--'+mapEmailMsg);
            //update mapEmailMsg.values();*/
        }
    }
    
    // Method which return queue id
    public static Id getGroupIdFromDeveloperName( String strDeveloperName, String strGroupType ) {
        if( String.isNotBlank( strDeveloperName ) ) {
            list<Group> lstGroups =  new list<Group>( [ SELECT Id FROM Group WHERE Type = :strGroupType AND DeveloperName = :strDeveloperName ] );
            if( lstGroups != null && !lstGroups.isEmpty() ) { return lstGroups[0].Id; }
        }
        return null;
    }
}