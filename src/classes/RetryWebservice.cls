/************************************************************************************************
 * @Name              : RetryWebservice
 * @Test Class Name   : IPMS_REST_Generate_Draft_SPA_Test, AsynReceiptWebServiceCloneTest
 * @Description       : Methods for reinvoking the webservices
 * Modification Log
 * 1.0    ------    20/04/2017        Created Class
 * 1.1    QBurst    29/01/2020        Added Method to invoke Generate Draft SPA    
 ************************************************************************************************/
global class RetryWebservice{

    webservice static string retrybooking(Id srId){     
        String res='Success';
        
        integer regcount=[select count() from Booking_Unit__c where Status__c!='Removed' and Registration_ID__c=null and Booking__r.Deal_SR__c=:srId];
        
        if(regcount==0)
        res='Registration already successful. Retry not allowed';
        
        else{
            try{
                //List<id>Bookingids= new List<id>();
                for(Booking__c bk :[select id from Booking__c where Deal_SR__c=:srId]){                    
                    List<id>Bookingids= new List<id>();
                    Bookingids.add(bk.id);
                    //system.enqueueJob(new AsyncReceiptWebservice (Bookingids,'Registration Creation'));
                    AsyncReceiptWebservice.sendRegistrationRequest(BookingIds); 
                }
                //if(BookingIds.size()>0)
                //AsyncReceiptWebservice.sendRegistrationRequest(BookingIds);            
            }
            catch(Exception ex) {
                 res=ex.getMessage();        
            }  
        }
        return res;
        
    }
    
    webservice static string retrySPA(Id srId){     
        String res='Success';   
           
        integer regcount=[select count() from Booking_Unit__c where Status__c!='Removed' and Registration_ID__c=null and Booking__r.Deal_SR__c=:srId];
        
        if(regcount>0)
        res='Registration not created yet. SPA cannot be generated';
        
        else{
            try{
                List<Id> BUIds= new List<id>();                    
                for(Booking__c BU :[select id from Booking__c where Status__c!='Removed' and Deal_SR__c=:srId]){
                    BUIds.add(BU.id);
                    //system.enqueueJob(new AsyncReceiptWebservice (BUIds,'SPA')); // Old Method
                }
                for(id bui:buids){                    
                    IPMS_REST_SPA_getProcessID.DataForRequest(bui);    // New Polling Method
                }      
            }
            catch(Exception ex) {
                 res=ex.getMessage();        
            }  
        }
        return res;
        
    }

    // 1.1 starts
    /************************************************************************************************
    * @Description : fetches Mappings from Custom Metadata and populates the values for PDF 
    * @Params      : 1. string srId - Id of the Service Request for which Generate Draft SPA
    *                                    button is clicked.
    * @Return      : String res - Response from Webservice callout class is returned to the button                                                                   
    *************************************************************************************************/
    webservice static string retryDraftSPA(Id srId){     
        String res = 'Success';   
        system.debug(' srId : ' + srId );
        Integer regcount = [SELECT count() FROM Booking_Unit__c 
                            WHERE Status__c != 'Removed' AND Registration_ID__c = null 
                              AND Booking__r.Deal_SR__c =: srId];
        system.debug(' regcount : ' + regcount);
        if(regcount > 0) {
            res = 'Registration not created yet. SPA cannot be generated';
        } else{
            try{
                List<Booking_Unit__c> bookingUnits = new List<Booking_Unit__c>();                    
                for(Booking_Unit__c bookUnit :[SELECT Id
                                               FROM Booking_Unit__c 
                                               WHERE Booking__r.Status__c != 'Removed' 
                                               AND Booking__r.Deal_SR__c =: srId]){
                    IPMS_REST_Generate_Draft_SPA.DataForRequest(bookUnit.Id);
                }
            } catch(Exception ex) {
                 res = ex.getMessage();        
            }  
        }
        return res;
    }
    // 1.1 ends
        
    webservice static string GetPayPlan(Id BUid){     
        String res='Success';        
        
        try{           
            //List<Id> BUIds= new List<id>();
            //BUIds.add(BUid);
            ///system.enqueueJob(new AsyncReceiptWebservice (BUIds,'Retrieve PayPlan'));
            AsyncReceiptWebservice.RetrievePayPlan(BUid);        
        }
        catch(Exception ex) {
             res=ex.getMessage();        
        }  
        
        return res;
        
    }
}