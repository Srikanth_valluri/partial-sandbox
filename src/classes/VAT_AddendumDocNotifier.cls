/*******************************************************************************************************
Description : Class to send Addedndum document
--------------------------------------------------------------------------------------------------------
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
--------------------------------------------------------------------------------------------------------
1.0     | 04-02-2020        |Aishwarya Todkar    | Initial Draft
*****************************************************************************************************/
Public Class VAT_AddendumDocNotifier {

    public static List<EmailMessage> lstEmails;
    public static List<Booking_Unit__c> lstBuToUpdate;

/********************************************************************************************************
    Method to send Addedndum document
*********************************************************************************************************/
    @InvocableMethod
    public static void sendAddendumDoc( List<Id> ListBuId) {

        Map<id, Attachment> mapOfAttachToCase = new Map<Id, Attachment>();
        
        if( !ListBuId.isEmpty() ) {
            for(Id buId :ListBuId) {
                    getBookingUnit( buId, null, false,'',false);
            }
        }
    }

    //@future(callout=true)
    public static void getBookingUnit( Id buId, Id caseId, Boolean isResendEmail, String  emailAddress, Boolean sendAtPrimaryEmail ) {

        System.debug('buId--' + buId);
        lstEmails = new List<EmailMessage>();
        lstBuToUpdate = new List<Booking_Unit__c>();

        Boolean vatEmailSent = isResendEmail ? true : false;
        System.debug('vatEmailSent--' + vatEmailSent);

        List<Booking_Unit__c> listBu = new List<Booking_Unit__c>( [SELECT
                                                                        Id
                                                                        , Booking__r.Account__r.Person_Business_Email__c
                                                                        , Booking__r.Account__r.Name
                                                                        , Unit_Name__c
                                                                        , Permitted_Use_Type__c
                                                                        , VAT_addendum_Email_Sent_Date__c
                                                                        , Booking__r.Account__c
                                                                        , Requested_Price_AED__c
                                                                        , Property_Name__c
                                                                        , Inventory__r.Seller_Name__c
                                                                        , Handover_Status__c
                                                                        , Permitted_Use__c
                                                                    FROM
                                                                        Booking_Unit__c
                                                                    WHERE
                                                                        Id =: buId
                                                                    AND
                                                                        VAT_Email_Sent__c =: vatEmailSent
                                                                    AND
                                                                        Handover_Status__c != null
                                                                    AND
                                                                        Booking__r.Account__r.Person_Business_Email__c != null
                                                                ] );
        System.debug('listBu---'+ listBu);
        if( listBu != null && listBu.size() > 0 )
            prepareSendGridEmail( listBu[0], caseId, isResendEmail, emailAddress, sendAtPrimaryEmail);
            
    }

/********************************************************************************************************
    Method to prepare email to send VAT Addendum
*********************************************************************************************************/

    public static Boolean prepareSendGridEmail( Booking_Unit__c objBU 
                                            , Id caseId
                                            , Boolean isResendEmail
                                            , String  emailAddress
                                            , Boolean sendAtPrimaryEmail) {
        System.debug('prepareSendGridEmail =====' );
        lstEmails = new List<EmailMessage>();
        System.debug('objBU - ' + objBU);
        Boolean emailSent = false;

        if( objBU != null ) {
            List<SendGrid_Email_Details__mdt> listSendGrid = getSendGridDetails('Serviced VAT HO Addendum');
            Map<String, Attachment> mapOfAttachments = new Map<String, Attachment>();
            List<Attachment> listAttVAt = new List<Attachment>([SELECT
                                            Id
                                            , ParentId
                                            , Body
                                            , Name
                                        FROM
                                            Attachment
                                        WHERE
                                            Name LIKE 'HO VAT Addendum%'
                                        AND
                                            ParentId =: objBU.Id
                                        ORDER BY
                                            CreatedDate DESC
                                        LIMIT 1] );
            /*List<Attachment> listAttCoverLtr = new List<Attachment>([SELECT
                                            Id
                                            , ParentId
                                            , Body
                                            , Name
                                        FROM
                                            Attachment
                                        WHERE
                                            Name LIKE '%Handed Over Cover Letter%'
                                        AND
                                            ParentId =: objBU.Id
                                        ORDER BY
                                            CreatedDate DESC
                                        LIMIT 1] );
            
            if( !listAttCoverLtr.isEmpty() )
                listAtt.add( listAttCoverLtr[0] );*/
            
            
            List<Attachment> listAtt = new List<Attachment>();
            if( !listAttVAt.isEmpty() )
                listAtt.add( listAttVAt[0] );
                
            System.debug('listAtt== ' +listAtt);
            if( String.isNotBlank( objBU.Handover_Status__c ) )  {
                System.debug('Handover_Status__c--' + objBU.Handover_Status__c );
                
                //Get Email Template
                String emailTemplateName = '';
                if( objBu.Handover_Status__c.equalsIgnoreCase( 'Handed Over' ) 
                || objBu.Handover_Status__c.equalsIgnoreCase( 'Early Handed Over' ) 
                || objBu.Handover_Status__c.equalsIgnoreCase( 'Lease Handed Over' ) ) {
                    emailTemplateName = 'HO VAT Addendum';
                }
                else {
                    emailTemplateName = 'Non HO VAT Addendum';
                }
                List<EmailTemplate> listEmailTemplate = new List<EmailTemplate>( 
                                                                                [SELECT 
                                                                                    Id, Name, Subject, Body, HtmlValue, TemplateType
                                                                                FROM 
                                                                                    EmailTemplate 
                                                                                WHERE 
                                                                                    Name =: emailTemplateName
                                                                                Limit 1] );
                System.debug('listEmailTemplate--' + listEmailTemplate);

                if( !listEmailTemplate.isEmpty() && !listSendGrid.isEmpty() ) {

                    SendGrid_Email_Details__mdt objSendGridCs = listSendGrid[0];
                    EmailTemplate emailTemplateObj = listEmailTemplate[0];

                    String toAddress = isResendEmail && !sendAtPrimaryEmail && String.isNotBlank( emailAddress ) ? emailAddress 
                                        : objBU.Booking__r.Account__r.Person_Business_Email__c;

                    String toName = String.isNotBlank( objSendGridCs.To_Name__c ) ? objSendGridCs.To_Name__c : '';

                    String ccAddress = String.isNotBlank( objSendGridCs.CC_Address__c ) ? objSendGridCs.CC_Address__c : '';

                    String ccName = String.isNotBlank( objSendGridCs.CC_Name__c ) ? objSendGridCs.CC_Name__c : '';

                    String bccAddress = String.isNotBlank( objSendGridCs.Bcc_Address__c ) ? objSendGridCs.Bcc_Address__c : '';

                    String bccName = String.isNotBlank( objSendGridCs.Bcc_Name__c ) ? objSendGridCs.Bcc_Name__c : '';

                    String fromAddress = String.isNotBlank( objSendGridCs.From_Address__c ) ? objSendGridCs.From_Address__c : '';

                    String fromName = String.isNotBlank( objSendGridCs.From_Name__c ) ? objSendGridCs.From_Name__c : '';

                    String replyToAddress = String.isNotBlank( objSendGridCs.Reply_To_Address__c ) ? objSendGridCs.Reply_To_Address__c : '';

                    String replyToName = String.isNotBlank( objSendGridCs.Reply_To_Name__c ) ? objSendGridCs.Reply_To_Name__c : '';

                    String contentType = 'text/html';
                    
                    String contentValue = replaceMergeeFields( emailTemplateObj.HtmlValue, objBU );// EncodingUtil.base64Encode( mapOfAttachments.get( 'Cover Letter' ).Body );//
                    
                    String contentBody = replaceMergeeFields( emailTemplateObj.Body, objBU );//EncodingUtil.base64Encode( mapOfAttachments.get( 'Cover Letter' ).Body );//
                    
                    String subject = emailTemplateObj.Subject;
                    toAddress = (Test.isRunningTest())? 'test@test.com':toAddress ; 
                    System.debug('contentValue--'+ contentValue);
                    if( String.isNotBlank( toAddress) ) {
                        SendGridEmailService.SendGridResponse objSendGridResponse = 
                            SendGridEmailService.sendEmailService(
                                                        toAddress, toName
                                                        , ccAddress, ccName
                                                        , bccAddress, bccName
                                                        , subject, ''
                                                        , fromAddress, fromName
                                                        , replyToAddress, replyToName
                                                        , contentType
                                                        , contentValue, ''
                                                        , listAtt);
                        
                        system.debug('objSendGridResponse === ' + objSendGridResponse);
                        String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                        System.debug('responseStatus== ' + responseStatus);

                        //Create reportable activity 
                        if (responseStatus == 'Accepted') {
                            EmailMessage mail = new EmailMessage();
                            mail.Subject = subject;
                            mail.MessageDate = System.Today();
                            mail.Status = '3';//'Sent';
                            mail.relatedToId = objBU.Booking__r.Account__c;
                            //mail.Account__c  = accountId;
                            //mail.Collection_Promotion__c = objColProm.Id;
                            mail.Type__c = 'Handover VAT Addendum';
                            mail.ToAddress = toAddress;
                            mail.FromAddress = fromAddress;
                            mail.TextBody = contentBody;//contentValue.replaceAll('\\<.*?\\>', '');
                            mail.Sent_By_Sendgrid__c = true;
                            mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                            if( !isResendEmail ) {
                                mail.Sub_Type__c = 'Addnm Email';
                                mail.Booking_Unit__c = objBU.Id;
                            }
                                
                            if( caseId != null && isResendEmail ) {
                                mail.Sub_Type__c = sendAtPrimaryEmail ? 'Primary Email' : 'Revised Email';
                                mail.ParentId = caseId;
                            }
                            mail.CcAddress = ccAddress;
                            mail.BccAddress = bccAddress;
                            lstEmails.add(mail);
                            system.debug('Mail obj == ' + mail);

                            //Updating Booking Unit
                            if( !isResendEmail ) {
                                /*lstBuToUpdate.add( new Booking_Unit__c( Id = objBU.Id
                                                                    , VAT_addendum_Email_Sent_Date__c = System.Today()
                                                                    , VAT_Email_Sent__c = true ) );*/
                                emailSent = true;
                            }
                        }
                        System.debug('lstEmails=== ' + lstEmails);
                        System.debug('lstBuToUpdate=== ' + lstBuToUpdate);

                        if(lstEmails.size() > 0) {
                            if(!Test.isRunningTest()) {
                                insert lstEmails;
                            }
                        }
                        // if(lstBuToUpdate.size() > 0) {
                        //     update lstBuToUpdate;
                        // }
                    }//End toAddress if
                }//End listEmailTemplate if
            }// Handover status if
        }// End objBU if
        return emailSent;
    }//End method

/********************************************************************************************************
    Method to get configured SendGrid email details
*********************************************************************************************************/    
    public static List<SendGrid_Email_Details__mdt> getSendGridDetails( String sendGridCsName ) {
        if( String.isNotBlank( sendGridCsName ) ) {
            
            //Get Sendgrid Email details from custom metadat
            List<SendGrid_Email_Details__mdt> listSendGridDetails
                = new List<SendGrid_Email_Details__mdt>( [SELECT
                                                            From_Address__c
                                                            , From_Name__c
                                                            , To_Address__c
                                                            , To_Name__c
                                                            , CC_Address__c
                                                            , CC_Name__c
                                                            , Bcc_Address__c
                                                            , Bcc_Name__c
                                                            , Reply_To_Address__c
                                                            , Reply_To_Name__c
                                                            , Email_Template__c
                                                        FROM
                                                            SendGrid_Email_Details__mdt
                                                        WHERE
                                                            MasterLabel =: sendGridCsName
                                                        LIMIT 1
                                                        ] );
            System.debug( 'listSendGridDetails = ' + listSendGridDetails);
            return listSendGridDetails;
        }
        return null;
    }

    public static String replaceMergeeFields( String strContents, Booking_Unit__c objBU ) {
        if( strContents.contains( '{Account.Name}' ) ) {
            strContents = strContents.replace( '{Account.Name}', objBU.Booking__r.Account__r.Name );
        }

        if( strContents.contains( '{Booking_Unit__c.Unit_Name__c} ') ) {
            strContents = strContents.replace( '{Booking_Unit__c.Unit_Name__c} ', objBU.Unit_Name__c );
        }

        if( strContents.contains( '{project name}') ) {
            if( String.isNotBlank( objBU.Property_Name__c ) ) {
                strContents = strContents.replace( '{project name}', objBU.Property_Name__c );
            }
            else {
                strContents = strContents.replace( '{project name}', '' );
            }
        }

        if( strContents.contains( '{Booking_Unit__c.Seller_Name__c}') ) {
            if( String.isNotBlank( objBU.Inventory__r.Seller_Name__c ) ) {
                strContents = strContents.replace( '{Booking_Unit__c.Seller_Name__c}', objBU.Inventory__r.Seller_Name__c );
            }
            else {
                strContents = strContents.replace( '{Booking_Unit__c.Seller_Name__c}', '' );
            }
        }
        
        if( strContents.contains( '{Booking_Unit__c.Requested_Price__c}') ) {
            if( objBU.Requested_Price_AED__c != null ) {
                strContents = strContents.replace( '{Booking_Unit__c.Requested_Price__c}', String.valueOf( objBU.Requested_Price_AED__c ) );
            }
            else {
                strContents = strContents.replace( '{Booking_Unit__c.Requested_Price__c}', '' );
            }
        }
        if( strContents.contains( '{Booking_Unit__c.Permitted_Use__c}') ) {
            if( objBU.Permitted_Use__c!= null ) {
                strContents = strContents.replace( '{Booking_Unit__c.Permitted_Use__c}', String.valueOf( objBU.Permitted_Use__c) );
            }
            else {
                strContents = strContents.replace( '{Booking_Unit__c.Permitted_Use__c}', '' );
            }
        }
        return strContents;
    }
}