/*
 * Description : This class is used to Schedule UpdateCallingListOutcome batch at every midnight
 * Revision History:
 *
 *  Version          Author              Date           Description
 *  1.0                                 7/11/2017         Initial Draft
 *                                                      
 */
global class ScheduleUpdateCallingListOutcome implements Schedulable {
  global void execute(SchedulableContext sc) {
  UpdateCallingListOutcome batchInstance = new UpdateCallingListOutcome(); 
  database.executebatch(batchInstance);
  }
}