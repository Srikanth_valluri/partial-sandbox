@isTest
public  class GetFileFromOffice365CntrlTest {
    @TestSetup
    static void makeData(){
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
    }
    
    @isTest
    static void methodName1(){
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        
        Account testacc=new Account( Name='test');
        insert testacc;
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.GetFileFromOffice365')); 
        System.currentPageReference().getParameters().put('id', 'idVariable');
        System.currentPageReference().getParameters().put('recId', testacc.id);
        GetFileFromOffice365Cntrl objCntrl = new GetFileFromOffice365Cntrl();
        Test.stopTest();
    }
    @isTest
    static void methodName2(){
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        
        case testcase = new case (status = 'New');
        insert testcase;
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.GetFileFromOffice365')); 
        System.currentPageReference().getParameters().put('id', 'idVariable');
        System.currentPageReference().getParameters().put('recId', testcase.id);
        GetFileFromOffice365Cntrl objCntrl = new GetFileFromOffice365Cntrl();
        Test.stopTest();
    }
    @isTest
    static void methodName3(){
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        Account testacc=new Account( Name='test');
        insert testacc;
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
        Booking__c objBooking = new Booking__c(Account__c=testacc.Id, Deal_SR__c=objSR.Id);
        insert objBooking;
       Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='AB/019/TH500', Bedroom_Type__c = '1BR',
                                    Registration_ID__c = '74712', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100,
                                    Requested_Price__c = 3456899, Anticipated_Completion_Date__c = system.Today() );
        insert BUObj;
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
        settingLst2.add(newSetting1);
        insert settingLst2; 
        Calling_List__c testCalling_List = new Calling_List__c( Registration_ID__c = BUObj.Registration_ID__c , Inv_Due__c = 0, DM_Due_Amount__c = 0 );
        insert testCalling_List;
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.GetFileFromOffice365')); 
        System.currentPageReference().getParameters().put('id', 'idVariable');
        System.currentPageReference().getParameters().put('recId', testCalling_List.id);
        Test.stopTest();
    }
    @isTest
    static void methodName4(){
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        
        Account testacc=new Account( Name='test');
        insert testacc;
         NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
        Booking__c objBooking = new Booking__c(Account__c=testacc.Id, Deal_SR__c=objSR.Id);
        insert objBooking;
        
        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='AB/019/TH500', Bedroom_Type__c = '1BR',
                                    Registration_ID__c = '74712', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100,
                                    Requested_Price__c = 3456899, Anticipated_Completion_Date__c = system.Today() );
        insert BUObj;
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.GetFileFromOffice365')); 
        System.currentPageReference().getParameters().put('id', 'idVariable');
        System.currentPageReference().getParameters().put('recId', BUObj.id);
        GetFileFromOffice365Cntrl objCntrl = new GetFileFromOffice365Cntrl();
        Test.stopTest();
    }
    @isTest
    static void methodName5(){
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockNegative());
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.GetFileFromOffice365'));
        System.currentPageReference().getParameters().put('recId', '');
        System.currentPageReference().getParameters().put('id', 'idVariable');
        GetFileFromOffice365Cntrl objCntrl = new GetFileFromOffice365Cntrl();
        Test.stopTest();
    }
    @isTest
    static void methodName6(){
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockNegative());
        
        Account testacc=new Account( Name='test');
        insert testacc;
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.GetFileFromOffice365')); 
        System.currentPageReference().getParameters().put('id', 'idVariable');
        System.currentPageReference().getParameters().put('recId', testacc.id);
        GetFileFromOffice365Cntrl objCntrl = new GetFileFromOffice365Cntrl();
        Test.stopTest();
    }
    @isTest
    static void methodName7(){
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockNegative());
        
        case testcase = new case (status = 'New');
        insert testcase;
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.GetFileFromOffice365')); 
        System.currentPageReference().getParameters().put('id', 'idVariable');
        System.currentPageReference().getParameters().put('recId', testcase.id);
        GetFileFromOffice365Cntrl objCntrl = new GetFileFromOffice365Cntrl();
        Test.stopTest();
    }
    @isTest
    static void methodName8(){
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockNegative());
        Account testacc=new Account( Name='test');
        insert testacc;
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
        Booking__c objBooking = new Booking__c(Account__c=testacc.Id, Deal_SR__c=objSR.Id);
        insert objBooking;
        
        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='AB/019/TH500', Bedroom_Type__c = '1BR',
                                    Registration_ID__c = '74712', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100,
                                    Requested_Price__c = 3456899, Anticipated_Completion_Date__c = system.Today() );
        insert BUObj;
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
        settingLst2.add(newSetting1);
        insert settingLst2; 
        Calling_List__c testCalling_List = new Calling_List__c( Registration_ID__c = BUObj.Registration_ID__c , Inv_Due__c = 0, DM_Due_Amount__c = 0 );
        insert testCalling_List;
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.GetFileFromOffice365')); 
        System.currentPageReference().getParameters().put('id', 'idVariable');
        System.currentPageReference().getParameters().put('recId', testCalling_List.id);
        Test.stopTest();
    }
    @isTest
    static void methodName9(){
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockNegative());
        
        Account testacc=new Account( Name='test');
        insert testacc;
         NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
        Booking__c objBooking = new Booking__c(Account__c=testacc.Id, Deal_SR__c=objSR.Id);
        insert objBooking;
        
        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='AB/019/TH500', Bedroom_Type__c = '1BR',
                                    Registration_ID__c = '74712', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100,
                                    Requested_Price__c = 3456899, Anticipated_Completion_Date__c = system.Today() );
        insert BUObj;
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.GetFileFromOffice365')); 
        System.currentPageReference().getParameters().put('id', 'idVariable');
        System.currentPageReference().getParameters().put('recId', BUObj.id);
        GetFileFromOffice365Cntrl objCntrl = new GetFileFromOffice365Cntrl();
        Test.stopTest();
    }
    @isTest
    static void methodName10(){
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockNegative());
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.GetFileFromOffice365'));
        System.currentPageReference().getParameters().put('recId', '');
        System.currentPageReference().getParameters().put('id', 'idVariable');
        GetFileFromOffice365Cntrl objCntrl = new GetFileFromOffice365Cntrl();
        Test.stopTest();
    }
    
}