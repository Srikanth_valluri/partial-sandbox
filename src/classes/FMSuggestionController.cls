public with sharing class FMSuggestionController {
    
    public Booking_Unit__c objUnit              {get;set;}
    public string strFMCaseId                   {get;set;}
    public String strSRType                     {get;set;}
    public String strSelectedUnit               {get;set;}
    public String strAccountId                  {get;set;}
    public FM_Case__c sr                        {get; set;}
    private Id suggestionRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName()
                                        .get('Suggestion').getRecordTypeId();
    private static final String GENERIC_SAVE_ERROR = 'There was an error while processing your request. Please try again later.';
    private map<String, Id> mapFMUserId;
    private map<Id, String> UserIdEmail;
    public FMSuggestionController() {
    
        strFMCaseId = ApexPages.currentPage().getParameters().get('Id');
        if(String.isBlank( strFMCaseId )){
            strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
            strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
            strSRType = ApexPages.currentPage().getParameters().get('SRType');
            initializeVariables();
        }
    }
    
    public void initializeVariables() {
        String buildingId;
        objUnit = [Select Id
                       , Inventory__c
                       , Property_Name__c
                       , Name
                       , Unit_Name__c
                       , Inventory__r.Building_Location__c
                       , Inventory__r.Building_Location__r.Loams_Email__c
                   From Booking_Unit__c
                   Where Id =: strSelectedUnit];
        buildingId = objUnit.Inventory__r.Building_Location__c;
        mapFMUserId = new map<String, Id>();
        UserIdEmail = new map<Id, String>();
        system.debug('!!!!buildingId'+buildingId);
        if (!string.isBlank(buildingId) ){
            for (FM_User__c objFMUSer : [Select Id
                                            , FM_User__c
                                            , FM_User__r.Email
                                            , FM_Role__c
                                            , Building__c
                                         From FM_User__c
                                         Where Building__c =: buildingId]){
                mapFMUserId.put(strSelectedUnit +'-'+objFMUSer.FM_Role__c, objFMUSer.FM_User__c);
                UserIdEmail.put(objFMUSer.FM_User__c, objFMUSer.FM_User__r.Email);
            }
        }
        system.debug('!!!!mapFMUserId'+mapFMUserId);
        List<FM_Case__c> draftSr = [
            SELECT      Id,
                        Name,
                        Request_Type__c,
                        Status__c,
                        Booking_Unit__c,
                        Suggestion_Type__c,
                        Suggestion_Sub_Type__c,
                        Description__c,
                        Account__c,
                        OwnerId
            FROM        FM_Case__c
            WHERE       Status__c = 'Draft Request'
            And         Booking_Unit__c =: strSelectedUnit
            And         Account__c =: strAccountId
            ORDER BY    Status__c
            LIMIT       1
        ];
        system.debug('!!!!draftSr'+draftSr);
        //if (draftSr.isEmpty()) {
            sr = new FM_Case__c(
                Origin__c = 'Walk-In',
                RecordTypeId = suggestionRecordTypeId,
                Request_Type__c = 'Suggestion',
                Request_Type_DeveloperName__c = 'Suggestion',
                Account__c = strAccountId,
                Property_Name1__c = objUnit.Property_Name__c
            );
        /*} else {
            sr = draftSr[0];
        }*/
    }
    
    public PageReference saveAsDraft() {
        sr.Status__c = 'Draft Request';
        sr.Origin__c = 'Walk-In';
        sr.RecordTypeId = suggestionRecordTypeId;
        sr.Request_Type__c = 'Suggestion';
        sr.Request_Type_DeveloperName__c = 'Suggestion';
        sr.Account__c = strAccountId;
        try {
            upsert sr;
            PageReference objPage = new PageReference('/' + sr.Id );
            return objPage;
        } catch(Exception excp) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, GENERIC_SAVE_ERROR));
            return null;
        }
    }
    
    public PageReference submitSr() {
        system.debug('!!!!!!sr'+sr);
        Datetime dt = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
        String dayOfWeek=dt.format('EEEE');
        
        Group objGroup = [Select Id
                            , Name
                          From Group
                          Where Name = 'FM Collection Queue' limit 1];
        sr.Submitted__c = true;
        sr.Status__c = 'Submitted';
        sr.Origin__c = 'Walk-In';
        sr.RecordTypeId = suggestionRecordTypeId;
        sr.Request_Type__c = 'Suggestion';
        sr.Request_Type_DeveloperName__c = 'Suggestion';
        sr.Account__c = strAccountId;
        sr.Booking_Unit__c = strSelectedUnit;        
        sr.Property_Manager__c = mapFMUserId.get(strSelectedUnit +'-'+ 'Property Manager');
        sr.Property_Manager_Email__c = UserIdEmail.get(sr.Property_Manager__c);
        sr.Property_Director__c = mapFMUserId.get(strSelectedUnit +'-'+ 'Property Director');
        sr.Property_Director_Email__c = UserIdEmail.get(sr.Property_Director__c);
        sr.Admin__c = Label.FM_Collection_User_Id;
        sr.Email_3__c = Label.FM_Collection_User_Email;
        sr.Email__c = objUnit.Inventory__r.Building_Location__r.Loams_Email__c;
        sr.Email_2__c = Label.Customer_Happiness_Centre_email;
        if (sr.Suggestion_Type__c == 'Service Charges'){
            sr.ownerId = objGroup.Id;
        } else if (mapFMUserId.containsKey(strSelectedUnit +'-'+ 'Property Manager')){
            sr.ownerId = mapFMUserId.get(strSelectedUnit +'-'+ 'Property Manager');
        }
        if (dayOfWeek == 'Sunday' || dayOfWeek == 'Monday'){
            sr.Suggestion_Escalation_Date__c = system.today().addDays(3);
        } else if (dayOfWeek == 'Tuesday' || dayOfWeek == 'Wednesday' || dayOfWeek == 'Thurday' || 
            dayOfWeek == 'Friday'){
            sr.Suggestion_Escalation_Date__c = system.today().addDays(5);
        } else if (dayOfWeek == 'Saturday'){
            sr.Suggestion_Escalation_Date__c = system.today().addDays(4);
        }
        //sr.Suggestion_Escalation_Date__c = system.today();
        try {
            upsert sr;
            system.debug('****sr'+sr.OwnerId);
            if (sr.Suggestion_Type__c != 'Service Charges' &&mapFMUserId != null 
                && mapFMUserId.containsKey(strSelectedUnit +'-'+ 'Property Manager')){
	            Task taskInstance = new Task();
	            taskInstance.Priority = 'High';
	            taskInstance.WhatId = sr.Id;
	            taskInstance.Status = 'Not Started';
	            taskInstance.Process_Name__c = sr.Request_Type__c;
	            taskInstance.Subject =  Label.FM_Suggestion_Task_Subject;
	            taskInstance.ActivityDate = Date.today();
	            taskInstance.OwnerId = mapFMUserId.get(strSelectedUnit +'-'+ 'Property Manager');
	            taskInstance.Assigned_User__c = 'Property Manager';
	            insert taskInstance;
            }
            PageReference objPage = new PageReference('/' + sr.Id );
            return objPage;
        } catch(Exception excp) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, GENERIC_SAVE_ERROR));
            return null;
        }        
    }
}