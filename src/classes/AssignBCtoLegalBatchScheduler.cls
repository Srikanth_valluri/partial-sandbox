global class AssignBCtoLegalBatchScheduler implements Schedulable {
    
    // Execute at regular intervals
    global void execute(SchedulableContext ctx){
      AssignBCtoLegalBatch batch = new AssignBCtoLegalBatch();
      Database.executebatch(batch, 200);
    }
}