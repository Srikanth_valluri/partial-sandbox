@isTest
public without sharing Class CourierTrackingCtrlTest {
    public class FirstFlightServiceMock implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req) {
            req.setMethod('POST');
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String Json = '{"AirwayBillTrackList":[{"AirWayBillNo":"123456","Destination":"RIYADH-SAUDI ARABIA","ForwardingNumber":"6202937311","Origin":"DUBAI-UNITED ARAB EMIRATES","ShipmentProgress":3,"ShipperReference":"","TrackingLogDetails":[{"ActivityDate":"Monday 14  September  2020","ActivityTime":"13:04","DeliveredTo":"","Location":"CALLCENTRE","Remarks":"No Answer","Status":"NR"},{"ActivityDate":"Monday 14  September  2020","ActivityTime":"13:03","DeliveredTo":"","Location":"CALLCENTRE","Remarks":"No Answer","Status":"NR"},{"ActivityDate":"Sunday 30  August  2020","ActivityTime":"11:33","DeliveredTo":"TEST","Location":"DUBAI","Remarks":"","Status":"POD"},{"ActivityDate":"Sunday 30  August  2020","ActivityTime":"11:31","DeliveredTo":"","Location":"DUBAI","Remarks":"With Delivery Courier","Status":"WC"},{"ActivityDate":"Tuesday 25  August  2020","ActivityTime":"08:56","DeliveredTo":"","Location":"DUBAI","Remarks":"With Delivery Courier","Status":"WC"},{"ActivityDate":"Tuesday 25  August  2020","ActivityTime":"08:44","DeliveredTo":"","Location":"DUBAI","Remarks":"With Delivery Courier","Status":"WC"},{"ActivityDate":"Tuesday 25  August  2020","ActivityTime":"08:39","DeliveredTo":"","Location":"DUBAI","Remarks":"With Delivery Courier","Status":"WC"},{"ActivityDate":"Saturday 22  August  2020","ActivityTime":"15:24","DeliveredTo":"","Location":"DUBAI","Remarks":"Arrived Facility","Status":"AF"},{"ActivityDate":"Monday 10  August  2020","ActivityTime":"16:37","DeliveredTo":"","Location":"DUBAI","Remarks":"Location Information Received","Status":"LC"},{"ActivityDate":"Monday 10  August  2020","ActivityTime":"16:30","DeliveredTo":"","Location":"DUBAI","Remarks":"Whatsapp To Customer","Status":"SM"},{"ActivityDate":"Tuesday 07  July  2020","ActivityTime":"17:23","DeliveredTo":"","Location":"DUBAI","Remarks":"Shipment Information Recieved","Status":"INF"}],"Weight":"6.600"}],"Code":1,"Description":"Success"}';
            res.setBody(Json);
            res.setStatusCode(200);
            return res;
        }
    }
    @isTest
    public static void trackingDetailsTest() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.FirstName = 'Test';
        objAcc.LastName = 'Account';
        //objAcc.Master_Address__c = 'Test Address';
        objAcc.Zip_Postal_Code__c = '123456';
        objAcc.City__c = 'Dubai';
        objAcc.Country__c = 'Oman';
        objAcc.Phone_1__c = '123456';
        objAcc.Phone_2__c = '123456';
        objAcc.Fax__c = '123';
        objAcc.Party_Id__c = '12335';
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
        Credentials_Details__c objCred = new Credentials_Details__c();
        objCred.Name = 'First Flight Tracking';
        objCred.Endpoint__c = 'https://ontrack.firstflightme.com/FFCService.svc/Tracking';
        objCred.User_Name__c = '3000';
        objCred.Password__c = 'fftes';
        insert objCred;
        Courier_Delivery__c courierInst = new Courier_Delivery__c();
        courierInst.Airway_Bill__c = '12345';
        courierInst.Request_Number__c = '12345';
        courierInst.Country__c = 'UAE';
        courierInst.Account__c = objAcc.Id;
        courierInst.Courier_Service__c = 'First Flight';
        insert courierInst;
        //PageReference myVfPage = Page.CourierTracking;
        //Test.setCurrentPage(myVfPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(courierInst);
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('Id',courierInst.Id);
        
  
        Test.setMock(HttpCalloutMock.class, new FirstFlightServiceMock());
        
        Test.startTest();
            CourierTrackingCtrl courierTrackingInst = new CourierTrackingCtrl(sc);
            courierTrackingInst.trackCourier();
            courierTrackingInst.populateAccountDetails();
           
        Test.stopTest();
    }
     @isTest
    public static void trackingDetailsTest1() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.FirstName = 'Test';
        objAcc.LastName = 'Account';
        //objAcc.Master_Address__c = 'Test Address';
        objAcc.Zip_Postal_Code__c = '123456';
        objAcc.City__c = 'Dubai';
        objAcc.Country__c = 'Oman';
        objAcc.Phone_1__c = '123456';
        objAcc.Phone_2__c = '123456';
        objAcc.Fax__c = '123';
        objAcc.Party_Id__c = '12335';
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
        Credentials_Details__c objCred = new Credentials_Details__c();
        objCred.Name = 'Aramax Tracking';
        objCred.Endpoint__c = 'https://ws.dev.aramex.net/ShippingAPI.V2/Tracking/Service_1_0.svc/json/TrackShipments';
        objCred.User_Name__c = '3000';
        objCred.Password__c = 'fftes';
        insert objCred;
        Courier_Delivery__c courierInst = new Courier_Delivery__c();
        courierInst.Airway_Bill__c = '12345';
        courierInst.Request_Number__c = '12345';
        courierInst.Country__c = 'UAE';
        courierInst.Account__c = objAcc.Id;
        courierInst.Courier_Service__c = 'Aramex';
        insert courierInst;
        //PageReference myVfPage = Page.CourierTracking;
        //Test.setCurrentPage(myVfPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(courierInst);
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('Id',courierInst.Id);
        
  
        Test.setMock(HttpCalloutMock.class, new FirstFlightServiceMock());
        
        Test.startTest();
            CourierTrackingCtrl courierTrackingInst = new CourierTrackingCtrl(sc);
            courierTrackingInst.trackCourier();
            //courierTrackingInst.populateAccountDetails();
            
        Test.stopTest();
    }
}