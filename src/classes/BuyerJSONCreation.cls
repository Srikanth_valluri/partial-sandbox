public class BuyerJSONCreation {
    public String sourceSystem;
    public String extRequestNumber;
    
    
    public List<registrationLines> registrationLines;
    public class registrationLines {
        public String registrationId; 
        public List<jointCustomerLines> jointCustomerLines;                   
    }
    
    public class jointCustomerLines {
        public String subRequestName;
        public String extCustomerNumber;
        public String partyId;
        public String partyType;
        public String personTitle;
        public String organizationName;
        public String personFirstName;
        public String personMiddleName;
        public String personLastName;
        public String nationality;
        public String passportNumber;
        public String ppIssueDate;
        public String ppIssuePlace;
        public String ppExpDate;
        public String gender;
        public String birthPlace;
        public String birthDate;
        public String eidNumber;
        public String eidExpDate;
        
        public String crNumber;
        public String crRegPlace;
        public String crRegDate;
            
        public List<customerContactLines> customerContactLines;
        public List<customerAddressLines> customerAddressLines;  
        public List<customerArabicLines> customerArabicLines;    
    }
    
    public class customerContactLines {
        public String partyId;
        public String homeCountryCode;
        public String homeAreaCode;
        public String home_number;
        public String workCountryCode;
        public String workAreaCode;
        public String workNumber;
        public String mobileCountryCode;
        public String mobileAreaCode;
        public String mobileNumber;
        public String faxCountryCode;
        public String faxAreaCode;
        public String faxNumber;
        public String emailAddress;
        public String url;
        public String mobileCountryCodeAr;
        public String mobileAreaCodeAr;
        public String mobileNumberAr;
        public String phoneCountryCodeAr;
        public String phoneAreaCodeAr;
        public String phoneNumberAr;    
        public String phoneExtentionAr;
        public String faxCountryCodeAr;
        public String faxAreaCodeAr;
        public String faxNumberAr;
        public String faxExtensionAr;
           
    }
    
    public class customerAddressLines {
        public String partyId;
        public String address1;
        public String address2;
        public String address3; 
        public String address4; 
        public String city;
        public String postalCode;  
        public String state;             
        public String country;                               
    }
    
    public class customerArabicLines {
        public String partyId;
        public String titleAr;
        public String firstNameAr;
        public String middleNameAr;
        public String lastNameAr; 
        public String nationalityAr;   
        public String passportNumberAr;    
        public String crNumberAr;  
        public String crRegDateAr;             
        public String crRegPlaceAr;
        public String emailAddressAr;  
        public String passportIssueDateAr;             
        public String passportIssuePlaceAr;
        public String address1Ar;
        public String address2Ar;
        public String address3Ar; 
        public String address4Ar;  
        public String cityAr;
        public String countryAr; 
        public String postalCodeAr;            
    }
}