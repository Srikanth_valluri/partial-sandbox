/**************************************************************************************************
* Name               : InquiryScoreCalculator                                                     *
* Description        : Batch class to handle calculate inquiry score for each PC.                 *
*                       - Update the inquiry count.                                               *
*                       - Update the events count.                                                *
* Created Date       : 21/05/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         NSI - Vineet      21/05/2017      Initial Draft.                                    *
* 1.1         NSI - Vineet      31/05/2017      Update the date filter to get the inquiry related,* 
*                                               to last 90 days and source not as prospecting,    *
*                                               referral, agent referral and customer referral.   *
**************************************************************************************************/
public class InquiryScoreCalculator implements Database.Batchable<sObject>{ 
     
    public static String PROFILE_NAME = 'Property Consultant';
    public static String COMPLETED = 'Completed';
    public static String INQUIRY_ACTIVE_STATUS = DAMAC_Constants.INQUIRY_ACTIVE_STATUS;
    public static String INQUIRY_NEW_STATUS = DAMAC_Constants.INQUIRY_NEW_STATUS;
    private static final Id INQUIRY_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('Inquiry__c', 'Inquiry');
    
    /********************************************************************************************* 
    * @Description : Implementing the start method of batch interface, contains query.           *
    * @Params      : Database.BatchableContext                                                   *
    * @Return      : Database.QueryLocator                                                       *
    *********************************************************************************************/  
    public Database.QueryLocator start(Database.BatchableContext BC){
        /* Calling method to reset the iteration counter to 1. */
        resetIterationCounter();
        String query = 'SELECT Id, OwnerId, Inquiry_Status__c, Owner.Name, '+
                                '(SELECT Id, OwnerId, Status__c FROM Events '+
                                  'WHERE ActivityDate = LAST_N_DAYS:90) '+
                       'FROM Inquiry__c '+
                       'WHERE CreatedDate = LAST_N_DAYS:90 AND '+
                             'Owner.Profile.Name =: PROFILE_NAME AND '+
                             'RecordTypeId =: INQUIRY_RECORD_TYPE_ID AND '+
                             '(Inquiry_Status__c =: INQUIRY_ACTIVE_STATUS OR '+
                              'Inquiry_Status__c =: INQUIRY_NEW_STATUS) AND '+
                             'Duplicate__c = false AND IsDeleted = false AND ('; 
        for(String thisSource : DAMAC_Constants.inquirySourceToInclude){
            query = query + 'Inquiry_Source__c = \''+thisSource+'\' OR ';
        }
        query = query.removeEnd('OR '); 
        query = query + ')'; 
        system.debug('#### query = '+query);
        return Database.getQueryLocator(query);
    }
   
    /*********************************************************************************************
    * @Description : Implementing the execute method of batch interface, contains the criteria.  *
    * @Params      : Database.BatchableContext, List<sObject>                                    *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void execute(Database.BatchableContext BC, List<SObject> inquiryList){
        system.debug('#### inquiryList = '+inquiryList);
        Map<Id, Integer> pcWithNewLeadCount = new Map<Id, Integer>();
        Map<Id, Integer> pcWithCompletedMeetingCount = new Map<Id, Integer>();
        Map<Id, Integer> pcWithNewMeetingCount = new Map<Id, Integer>();
        Map<Id, List<Inquiry__c>> userAssociatedInquiryListMap = new Map<Id, List<Inquiry__c>>();
        if(inquiryList != null){
            for(Inquiry__c thisInquiry : (List<Inquiry__c>)inquiryList){
                if(userAssociatedInquiryListMap.containsKey(thisInquiry.OwnerId)){
                    userAssociatedInquiryListMap.get(thisInquiry.OwnerId).add(thisInquiry);    
                }else{
                    userAssociatedInquiryListMap.put(thisInquiry.OwnerId, new List<Inquiry__c>{thisInquiry});       
                }   
            }
        }
        if(!userAssociatedInquiryListMap.isEmpty()){
            for(Id thisKey : userAssociatedInquiryListMap.keySet()){
                for(Inquiry__c thisInquiry : userAssociatedInquiryListMap.get(thisKey)){
                    /* Creating map of all new leads count. */ 
                    if(String.isNotBlank(thisInquiry.Inquiry_Status__c) && 
                       thisInquiry.Inquiry_Status__c.equalsIgnoreCase(DAMAC_Constants.INQUIRY_NEW_STATUS)){ 
                        if(pcWithNewLeadCount.containsKey(thisInquiry.OwnerId)){
                            pcWithNewLeadCount.put(thisInquiry.OwnerId, pcWithNewLeadCount.get(thisInquiry.OwnerId)+1);     
                        }else{
                            pcWithNewLeadCount.put(thisInquiry.OwnerId, 1); 
                        }  
                    }
                    for(Event thisEvent : thisInquiry.Events){
                        /* Creating map of completed meeting count. */
                        if(String.isNotBlank(thisEvent.Status__c) && 
                           thisEvent.Status__c.equalsIgnoreCase(DAMAC_Constants.EVENT_COMPLETED_STATUS)){
                            if(pcWithCompletedMeetingCount.containsKey(thisInquiry.OwnerId)){
                                pcWithCompletedMeetingCount.put(thisInquiry.OwnerId, pcWithCompletedMeetingCount.get(thisInquiry.OwnerId) + 1);     
                            }else{
                                pcWithCompletedMeetingCount.put(thisInquiry.OwnerId, 1);    
                            }       
                        }   
                        /* Creating map of planned meeting count. */
                        if(String.isNotBlank(thisEvent.Status__c) && 
                           thisEvent.Status__c.equalsIgnoreCase(DAMAC_Constants.EVENT_PLANNED_STATUS)){
                            if(pcWithNewMeetingCount.containsKey(thisInquiry.OwnerId)){
                                pcWithNewMeetingCount.put(thisInquiry.OwnerId, pcWithNewMeetingCount.get(thisInquiry.OwnerId) + 1);     
                            }else{
                                pcWithNewMeetingCount.put(thisInquiry.OwnerId, 1);  
                            }       
                        }   
                    }
                }
            }
            Map<Id, PCInquiryCounter__c> pcInquiryCountMap = new Map<Id, PCInquiryCounter__c>();
            /* Setting the values in custom setting. */
            for(User thisUser : [SELECT Id, Name FROM User WHERE Profile.Name =: PROFILE_NAME AND isActive= true]){
                PCInquiryCounter__c existingRecord = PCInquiryCounter__c.getInstance(thisUser.Id);
                PCInquiryCounter__c newPCInquiryCounter = new PCInquiryCounter__c(); 
                newPCInquiryCounter.Name = thisUser.Id;
                newPCInquiryCounter.Owner_Name__c = thisUser.Name; 
                
                /* If the all lead data already exists in the table, add to it else insert the new value. */
                Integer existingAllInquiryCount = existingRecord != null && 
                                                  existingRecord.All_Inquiry_Count__c != null && 
                                                  existingRecord.All_Inquiry_Iteration_Count__c > 1 ? 
                                                    Integer.valueOf(existingRecord.All_Inquiry_Count__c) : 0; 
                newPCInquiryCounter.All_Inquiry_Count__c = userAssociatedInquiryListMap.containsKey(thisUser.Id) ? 
                                                                userAssociatedInquiryListMap.get(thisUser.Id).size() + existingAllInquiryCount : 
                                                                existingAllInquiryCount;
                newPCInquiryCounter.All_Inquiry_Iteration_Count__c = existingRecord != null && existingRecord.All_Inquiry_Iteration_Count__c != null ? 
                                                                        existingRecord.All_Inquiry_Iteration_Count__c + 1 : 1;
                    
                /* If the new lead data already exists in the table, add to it else insert the new value. */
                Integer existingNewInquiryCount = existingRecord != null && 
                                                  existingRecord.New_Inquiry_Count__c != null && 
                                                  existingRecord.New_Inquiry_Iteration_Count__c > 1 ? 
                                                    Integer.valueOf(existingRecord.New_Inquiry_Count__c) : 0;
                newPCInquiryCounter.New_Inquiry_Count__c = pcWithNewLeadCount.containsKey(thisUser.Id) ? 
                                                                pcWithNewLeadCount.get(thisUser.Id) + existingNewInquiryCount : 
                                                                existingNewInquiryCount;
                newPCInquiryCounter.New_Inquiry_Iteration_Count__c = existingRecord != null && existingRecord.New_Inquiry_Iteration_Count__c != null ? 
                                                                        existingRecord.New_Inquiry_Iteration_Count__c + 1 : 1;
                
                /* If the completed meeting data already exists in the table, add to it else insert the new value. */
                Integer existingCompletedMeetingCount = existingRecord != null && 
                                                        existingRecord.Completed_Meeting_Count__c != null && 
                                                        existingRecord.Completed_Meeting_Iteration_Count__c > 1 ? 
                                                            Integer.valueOf(existingRecord.Completed_Meeting_Count__c) : 0;
                newPCInquiryCounter.Completed_Meeting_Count__c = pcWithCompletedMeetingCount.containsKey(thisUser.Id) ? 
                                                                    pcWithCompletedMeetingCount.get(thisUser.Id) + existingCompletedMeetingCount : 
                                                                    existingCompletedMeetingCount;
                newPCInquiryCounter.Completed_Meeting_Iteration_Count__c = existingRecord != null && existingRecord.Completed_Meeting_Iteration_Count__c != null ? 
                                                                                existingRecord.Completed_Meeting_Iteration_Count__c + 1 : 1;
                
                /* If the new meeting data already exists in the table, add to it else insert the new value. */
                Integer existingNewMeetingCount = existingRecord != null && 
                                                  existingRecord.New_Meeting_Count__c != null && 
                                                  existingRecord.New_Meeting_Iteration_Count__c > 1 ? 
                                                        Integer.valueOf(existingRecord.New_Meeting_Count__c) : 0;
                newPCInquiryCounter.New_Meeting_Count__c = pcWithNewMeetingCount.containsKey(thisUser.Id) ? 
                                                                pcWithNewMeetingCount.get(thisUser.Id) + existingNewMeetingCount : 
                                                                existingNewMeetingCount;
                newPCInquiryCounter.New_Meeting_Iteration_Count__c = existingRecord != null && existingRecord.New_Meeting_Iteration_Count__c != null ? 
                                                                        existingRecord.New_Meeting_Iteration_Count__c + 1 : 1;
                pcInquiryCountMap.put(thisUser.Id, newPCInquiryCounter);
            }
            if(!pcInquiryCountMap.isEmpty()){
                upsert pcInquiryCountMap.values() Name;
            }
        }
    }
    
    /*********************************************************************************************
    * @Description : Implementing Finish method, to end an email after job completion.           *
    * @Params      : Database.BatchableContext                                                   *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void finish(Database.BatchableContext BC){
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems
                         FROM AsyncApexJob 
                         WHERE Id =: BC.getJobId()];
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       //String[] toAddresses = new String[] {'vineet.kumar@nsigulf.com'};
       String[] toAddresses = new String[] {label.Batch_Notification_Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('Inquiry Score Calculator Batch : ' + a.Status);
       mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
                             ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    /*********************************************************************************************
    * @Description : Method to reset the iteration counter to 1.                                 *
    * @Params      : void                                                                        *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    private void resetIterationCounter(){
        List<PCInquiryCounter__c> updateScoreList = new List<PCInquiryCounter__c>(); 
        for(PCInquiryCounter__c thisScore : [SELECT Id, Name, All_Inquiry_Count__c, All_Inquiry_Iteration_Count__c, 
                                                    New_Inquiry_Count__c, New_Inquiry_Iteration_Count__c, 
                                                    Completed_Meeting_Count__c, Completed_Meeting_Iteration_Count__c, 
                                                    New_Meeting_Count__c, New_Meeting_Iteration_Count__c, 
                                                    Owner_Name__c 
                                             FROM PCInquiryCounter__c]){
            thisScore.All_Inquiry_Iteration_Count__c = 1;
            thisScore.New_Inquiry_Iteration_Count__c = 1;
            thisScore.Completed_Meeting_Iteration_Count__c = 1; 
            thisScore.New_Meeting_Iteration_Count__c = 1;
            updateScoreList.add(thisScore); 
        }   
        if(!updateScoreList.isEmpty()){
            update updateScoreList; 
        }
    }
}// End of class.