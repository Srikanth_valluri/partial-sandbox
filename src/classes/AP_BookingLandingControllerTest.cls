@istest
public class AP_BookingLandingControllerTest {
    @testSetup static void setupData() {
        ID RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        
        Account a = new Account();
        a.recordtypeid=RecordTypeIdAGENT;
        a.Name = 'TestAccount';
        a.Agency_Short_Name__c = 'testShrName';
        insert a;
        
        Contact con = new Contact();
        con.LastName = 'testlnme';
        con.AccountId = a.Id ;
        con.Portal_Administrator__c=true;
        insert con;
        
        Agent_Site__c agency = new Agent_Site__c();
        agency.Name = 'UAE';
        agency.Agency__c = a.id;
        insert agency;  
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        loc.Property_ID__c = '123';
        insert loc;
        
        Payment_Plan__c pp = new Payment_Plan__c();        
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today(); 
        pp.Building_Location__c =  loc.id;  
        pp.term_id__c = '1234';   
        insert pp;   
        
        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c  = 1;
        newProperty.Property_Code__c    = 'VIR' ;
        newProperty.Property_Name__c    = 'VIRIDIS @ AKOYA OXYGEN' ;
        newProperty.District__c = 'AL YUFRAH 2' ;
        newProperty.AR_Transaction_Type__c  = 'INV VIR' ;
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR' ;
        newProperty.Brokerage_Distribution_Set__c   = '11600' ;
        newProperty.Sales_Commission_Dist_Set__c    = '11601' ;
        newProperty.Currency_Of_Sale__c = 'AED' ;
        newProperty.Signature_Col_Customer_Stmt__c  = 'Front Line Investment Management Co. LLC' ;
        newProperty.EOI_Enabled__c = true;
        insert newProperty; 
        
        
        Id salesEventsRt = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Sales Events').getRecordTypeId();
        List<Campaign__c> campaignList = TestDataFactory.createCampaignRecords(new List<Campaign__c>{new Campaign__c(RecordTypeId=salesEventsRt), new Campaign__c(RecordTypeId=salesEventsRt),
            new Campaign__c(RecordTypeId=salesEventsRt), new Campaign__c(RecordTypeId=salesEventsRt), new Campaign__c(RecordTypeId=salesEventsRt)});
        TestDataFactory.createInquiryRecords(new List<Inquiry__c> {new Inquiry__c(Campaign__c=campaignList[0].Id, Inquiry_Source__c='', OwnerId = UserInfo.getUserId())});
        
        Property__c propObjnew = new Property__c();
        propObjnew.Name = System.Label.Default_EOI_Property;
        propObjnew.Property_ID__c   = 455546;
        propObjnew.EOI_Enabled__c = true;
        insert propObjnew;
        
        profile profId = [SELECT Id FROM Profile WHERE Name='Standard User']; // query on Profile to fetch ProfileId
        String orgId = UserInfo.getOrganizationId();
        Profile p = [select Id from Profile where name LIKE 'Customer Community - Admin' LIMIT 1];
        System.debug('Profile----------------->' + p);
        
        //user 
        User portalUser = new User(alias = 'test456', email='testusr1@test.com',
                                   emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                                   localesidkey='en_US', profileid = p.id, country='United Arab Emirates',IsActive =true,
                                   ContactId = con.Id,timezonesidkey='America/Los_Angeles', username='portalUser1Test@test.com');
        System.debug('portalUser----------------->' + portalUser);
        insert portalUser;
        
        List<NSIBPM__Status__c> createStatus = new List<NSIBPM__Status__c>();
        createStatus = InitialiseTestData.createStatusRecords(
            new List<NSIBPM__Status__c>{
                new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_ADDITIONAL_INFO', Name = 'AWAITING_ADDITIONAL_INFO'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'MORE_INFO_UPDATED', Name = 'MORE_INFO_UPDATED'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_FFA_AA', Name = 'AWAITING_FFA_AA'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'FFA_AA_UPLOADED', Name = 'FFA_AA_UPLOADED')
                    }
        );
        
        NSIBPM__SR_Steps__c srStep= new NSIBPM__SR_Steps__c();
        insert srStep;
        
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;
        
        List<NSIBPM__Service_Request__c> SRList  = InitialiseTestData.createTestServiceRequestRecords(
            new List<NSIBPM__Service_Request__c>{
                new NSIBPM__Service_Request__c(
                    recordTypeId = 
                    InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration'),
                    NSIBPM__SR_Template__c = srTemplate.Id
                )
                    }
        );
        System.debug('SRList------------------->' + SRList);
        List<NSIBPM__Step__c> createStepList = InitialiseTestData.createTestStepRecords(
            new List<NSIBPM__Step__c>{
                new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id, 
                                    NSIBPM__Status__c = createStatus[0].Id, 
                                    NSIBPM__SR_Step__c = srStep.id
                                   ),
                    new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id, 
                                        NSIBPM__Status__c = createStatus[1].Id, 
                                        NSIBPM__SR_Step__c = srStep.id
                                       )
                    }
        );
        
        Campaign__c camp = new Campaign__c();
        camp.End_Date__c = system.today().addmonths(10);
        camp.Marketing_End_Date__c = system.today().addmonths(10);
        camp.Marketing_Start_Date__c = system.today().addmonths(-10);
        camp.Start_Date__c =  system.today().addmonths(-10);
        insert camp;
        
        Inquiry__c inq = new Inquiry__c ();
        inq.Activity_Counter__c =101;
        inq.Inquiry_Status__c='Active';
        inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        inq.campaign__c = camp.id;
        //insert inq;
        EOI_Process__c eoObj = new EOI_Process__c();
        eoObj.No_of_Units__c = 'Single Unit – Token AED 40,000';
        eoObj.Agency__c =   a.id;
        eoObj.Mode_of_Token_Payment__c = 'Cash';
        eoObj.Property__c = newProperty.id;
        insert eoObj;
        
        Inventory__c invent = new Inventory__c();
        invent.CM_Price_Per_Sqft__c = 20;
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1' ;
        invent.EOI__C = eoObj.Id;
        invent.Tagged_to_EOI__c = true;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.ID;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Released';
        invent.special_price__c = 1000000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234'; 
        invent.Floor_Package_ID__c = ''; 
        invent.building_location__c = loc.id;
        invent.property_id__c = newProperty.id;
        insert invent;
        
        NSIBPM__Document_Master__c Dm = new NSIBPM__Document_Master__c();
        Dm.Name = 'Test DM';
        Dm.NSIBPM__Code__c = 'Bank Statement';
        Dm.NSIBPM__Document_Type_Code__c = 'Doc Type';
        insert Dm;
        
        //List<NSIBPM__SR_Doc__c> SRDoc_List = new List<NSIBPM__SR_Doc__c>();
        NSIBPM__SR_Doc__c srDoc = new NSIBPM__SR_Doc__c();
        srDoc = new NSIBPM__SR_Doc__c();
        srDoc.Name = 'Test SR DOC';
        srDoc.NSIBPM__Is_Not_Required__c = true;
        srDoc.NSIBPM__Status__c = 'Re-upload';
        srDoc.NSIBPM__Document_Master__c = Dm.Id;
        srDoc.NSIBPM__Service_Request__c = SRList[0].id;
        
        /*EOI_Process__c eoObj = new EOI_Process__c();
eoObj.No_of_Units__c = 'Single Unit – Token AED 40,000';
eoObj.Agency__c = a.id;
eoObj.Mode_of_Token_Payment__c = 'Cash';
eoObj.Property__c = propObjnew.id ;
insert eoObj;*/
        
        Booking__c booking = InitializeSRDataTest.createBooking(SRList[0].id);        
        booking.Deal_SR__c = SRList[0].id;
        booking.Inquiry_Account_Id__c = a.id;       
        insert booking;
        
        List<Booking_Unit__c> BUList = new List<Booking_Unit__c>();
        Booking_Unit__c bu1 = new Booking_Unit__c();
        bu1.Booking__c = booking.id;
        bu1.Payment_Method__c = 'Cash';
        bu1.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu1.Primary_Buyer_s_Name__c = 'testNSI';
        bu1.Primary_Buyer_s_Nationality__c = 'Russia';
        bu1.Inventory__c = invent.id;
        bu1.Registration_ID__c = '1234'; 
        bu1.Payment_Plan_Id__c = pp.id;     
        bu1.Requested_Price_AED__c = 500;     
        BUList.add(bu1);
        insert BUList;
        
        buyer__c b = new buyer__c();
        b.Buyer_Type__c =  'Individual';
        b.Address_Line_1__c =  'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.City__c = 'Dubai' ;       
        b.Account__c = a.id;
        b.inquiry__c = inq.id;
        b.dob__c = system.today().addyears(-30);
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname' ;
        b.Last_Name__c =  'lastname';
        b.Nationality__c = 'Indian' ;
        b.Passport_Expiry__c = system.today().addyears(20) ;
        b.Passport_Number__c = 'J0565556' ;
        b.Phone__c = '569098767' ;
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b.Place_of_Issue__c =  'India';
        b.Title__c = 'Mr';
        b.booking__c = booking.id;
        b.Primary_Buyer__c =true;
        b.Unique_Key__c = booking.id+''+inq.id;
        insert b;
        
        Unit_Documents__c unitDoc = new Unit_Documents__c();
        unitDoc.Booking_Unit__c = BUList[0].Id;
        unitDoc.Service_Request__c = SRList[0].id;
        insert unitDoc;
    }
    
    public static testMethod void Test_1(){
        Booking__c booking = [SELECT Id FROM Booking__c LIMIT 1];
        Contact con = [SELECT Id,Owner__c,Portal_Administrator__c,AccountId FROM Contact LIMIT 1];
        List<NSIBPM__Service_Request__c> SRList = [SELECT Id FROM NSIBPM__Service_Request__c ];
        EOI_Process__c  eoId = [SELECT Id FROM EOI_Process__c ];
        Test.startTest();
        Id adminRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = 'Chairman' LIMIT 1].Id;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'qw56', email='xqw@email.com',
                                  emailencodingkey='UTF-8', lastname='Userqw', languagelocalekey='en_US',
                                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                  ,timezonesidkey='America/Los_Angeles', username='xqw@email.com',UserRoleId = adminRoleId);
        System.RunAs(adminUser){
            Account adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;
            
            Contact adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            insert adminContact;
            Contact adminContactNew = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            adminContactNew.Owner__c = false;
            adminContactNew.Agent_Representative__c= false;
            adminContactNew.Portal_Administrator__c= false;
            insert adminContactNew;
            
            Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            
            User portalUser = InitialiseTestData.getPortalUser('xyz@test.com', adminContact.Id, 'Admin');
            User portalUserNew = InitialiseTestData.getPortalUser('t11t@test.com', adminContactNew.Id, 'Admin');
            insert portalUserNew;
            Test.setMock(HttpCalloutMock.class, new MockHttpDamacOfferServiceResponse ());
            System.runAs(portalUserNew){
                
                NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
                SR.Agency_Type__c = 'Corporate';
                SR.Agency__c = null;
                SR.Trade_License_Number__c = 'test';
                SR.Agent_Name__c = portalUserNew.id;
                insert SR;
                
                PageReference pageRef = Page.AP_BookingLandingPage;
                test.setCurrentPageReference(pageRef);
                pageRef.getParameters().put('objSRId',SRList[0].Id);
                pageRef.getParameters().put('sr',SR.Id);
                AP_BookingLandingController controller = new AP_BookingLandingController();
                controller.currentEOIId = eoId.Id;
                AP_BookingLandingController.SaveSOAResponse('http://test.com',booking.Id);
                AP_BookingLandingController.POST_SOA_REQUEST('Test',booking.Id);
                
                controller.strSelectedStatus = 'All';
                controller.fillMapOfStatus();
                // controller.initQuery();
                //controller.SendPreBookId();
                controller.navigateToUnitSearchPage();
                controller.downloadSOA();
                //controller.fetchServiceRequest();
                //controller.getDateLiterals();
                //controller.getAgentsNameList();
                // controller.getStatusValues();
                // controller.getStatusValuesForEOI();
                //Test.startTest();
                //controller.fetchTheUserDisplayStatus();
                //Test.stopTest();
                //controller.showAll();
                //controller.SendPayementLinkMail();
                //controller.callSOAGeneration();
                
                Map<String, Integer> pTempMap = new Map<String, Integer>();
                pTempMap.put('UNDER_MANAGER_REVIEW',202);
                controller.populateheaderCount(pTempMap);
                controller.DataForRequest(booking.Id);
                controller.agentIdVal = portalUserNew.id;
                controller.statusValue = 'DRAFT';
                //controller.init();
                Set<String> SetSrid =  new Set<String>();
                SetSrid.add('SetSrid');
                List<String> stepStatusesList =  new List<String>();
                List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
                    //controller.fetchTheListAsPerStatusFilter(statuses,con);
                    controller.lstSR = null;
                controller.listToDisplay = null;
                controller.SrStatus = null; 
                controller.SetSrid = SetSrid;
            }
        } 
        Test.stopTest();
        
    }
    
    public static testMethod void Test_2(){
        Test.startTest();
        Id adminRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = 'Chairman' LIMIT 1].Id;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'qw56', email='xqw@email.com',
                                  emailencodingkey='UTF-8', lastname='Userqw', languagelocalekey='en_US',
                                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                  ,timezonesidkey='America/Los_Angeles', username='xqw@email.com',UserRoleId = adminRoleId);
        System.RunAs(adminUser){
            Account adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;
            
            Contact adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            adminContact.Owner__c = false;
            adminContact.Portal_Administrator__c = false;
            insert adminContact;
            
            Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            
            User portalUser = InitialiseTestData.getPortalUser('xyz@test.com', adminContact.Id, 'Admin');
            Test.setMock(HttpCalloutMock.class, new MockHttpDamacOfferServiceResponse ());
            System.runAs(portalUser){
                NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
                SR.Agency_Type__c = 'Corporate';
                SR.Agency__c = null;
                SR.Trade_License_Number__c = 'test';
                insert SR;
                
                PageReference pageRef = Page.AP_BookingLandingPage;
                test.setCurrentPageReference(pageRef);
                pageRef.getParameters().put('sr',SR.Id);
                AP_BookingLandingController controller = new AP_BookingLandingController();
                Set<String> SetSrid =  new Set<String>();
                SetSrid.add('SetSrid');
                controller.lstSR = null;
                controller.listToDisplay = null;
                controller.SrStatus = null; 
                controller.SetSrid = SetSrid;
                controller.SendPayementLinkMail();  
            }
        }
        Test.stopTest();
    }
    
    public static testMethod void Test_3(){
        Test.startTest();
        Id adminRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = 'Chairman' LIMIT 1].Id;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'qw56', email='xqw@email.com',
                                  emailencodingkey='UTF-8', lastname='Userqw', languagelocalekey='en_US',
                                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                  ,timezonesidkey='America/Los_Angeles', username='xqw@email.com',UserRoleId = adminRoleId);
        System.RunAs(adminUser){
            Account adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;
            
            Contact adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            insert adminContact;
            Contact adminContactNew = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            adminContactNew.Owner__c = false;
            adminContactNew.Agent_Representative__c= false;
            adminContactNew.Portal_Administrator__c= false;
            insert adminContactNew;
            
            Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            
            User portalUser = InitialiseTestData.getPortalUser('xyz@test.com', adminContact.Id, 'Admin');
            User portalUserNew = InitialiseTestData.getPortalUser('t11t@test.com', adminContactNew.Id, 'Admin');
            //User portalOnlyAgent = InitialiseTestData.getPortalUser('asba1@test.com', agentContact.Id, 'Agent');
            Test.setMock(HttpCalloutMock.class, new MockHttpDamacOfferServiceResponse ());
            
            System.runAs(portalUser){
                NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
                SR.Agency_Type__c = 'Corporate';
                SR.Agency__c = null;
                SR.Trade_License_Number__c = 'test';
                SR.Agent_Name__c = portalUser.id;
                insert SR;
                
                PageReference pageRef = Page.AP_BookingLandingPage;
                test.setCurrentPageReference(pageRef);
                pageRef.getParameters().put('objSRId',SR.Id);
                pageRef.getParameters().put('sr',SR.Id);
                AP_BookingLandingController controller = new AP_BookingLandingController();
                controller.getDateLiterals();
                controller.getAgentsNameList();
                controller.getStatusValues();
                controller.agentIdVal = portalUserNew.id;
                controller.statusValue = 'DRAFT';
                controller.init();
                Set<String> SetSrid =  new Set<String>();
                SetSrid.add('SetSrid');
                controller.lstSR = null;
                controller.listToDisplay = null;
                controller.SrStatus = null; 
                controller.SetSrid = SetSrid;
                controller.SendPayementLinkMail();
                controller.callSOAGeneration();  
            }
        }
        Test.stopTest();
    }
    
    public static testMethod void Test_4(){
        Id adminRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = 'Chairman' LIMIT 1].Id;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'qw56', email='xqw@email.com',
                                  emailencodingkey='UTF-8', lastname='Userqw', languagelocalekey='en_US',
                                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                  ,timezonesidkey='America/Los_Angeles', username='xqw@email.com',UserRoleId = adminRoleId);
        System.RunAs(adminUser){
            Account adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;
            
            Contact adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            insert adminContact;
            Contact adminContactNew = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            adminContactNew.Owner__c = false;
            adminContactNew.Agent_Representative__c= false;
            adminContactNew.Portal_Administrator__c= false;
            insert adminContactNew;
            
            Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            
            User portalUser = InitialiseTestData.getPortalUser('xyz@test.com', adminContact.Id, 'Admin');
            
            Test.startTest();
            System.runAs(portalUser){
                Account a = [Select Id From Account limit 1];
                
                Property__c newProperty = new Property__c();
                newProperty.Property_ID__c  = 2;
                newProperty.Property_Code__c    = 'VIR-2' ;
                newProperty.Property_Name__c    = 'VIRIDIS @ AKOYA OXYGEN 2' ;
                newProperty.District__c = 'AL YUFRAH 22' ;
                newProperty.AR_Transaction_Type__c  = 'INV VIR 2' ;
                newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR 2' ;
                newProperty.Brokerage_Distribution_Set__c   = '116002' ;
                newProperty.Sales_Commission_Dist_Set__c    = '116012' ;
                newProperty.Currency_Of_Sale__c = 'AED' ;
                newProperty.Signature_Col_Customer_Stmt__c  = 'Front Line Investment Management Co. LLC' ;
                newProperty.EOI_Enabled__c = true;
                insert newProperty; 
                
                Location__c loc = InitializeSRDataTest.createLocation('1234','Building');
                loc.Property_ID__c = '1234';
                insert loc;
                
                EOI_Process__c eoObj = new EOI_Process__c();
                eoObj.No_of_Units__c = 'Single Unit – Token AED 40,000 - 2';
                eoObj.Agency__c =   a.id;
                eoObj.Mode_of_Token_Payment__c = 'Cash';
                eoObj.Property__c = newProperty.id;
                insert eoObj;
                
                Inventory__c invent = new Inventory__c();
                invent.CM_Price_Per_Sqft__c = 202;
                invent.Inventory_ID__c = '2';
                invent.Building_ID__c = '2';
                invent.Floor_ID__c = '2';
                invent.Marketing_Name__c = 'Damac Heights 2';
                invent.Address_Id__c = '2' ;
                invent.EOI__C = eoObj.Id;
                invent.Tagged_to_EOI__c = true;
                invent.Is_Assigned__c = false;
                invent.Property_ID__c = newProperty.ID;
                invent.Property__c = newProperty.Id;
                invent.Status__c = 'Released';
                invent.special_price__c = 1000000;
                invent.CurrencyISOCode = 'AED';
                invent.Property_ID__c = '1234'; 
                invent.Floor_Package_ID__c = ''; 
                invent.building_location__c = loc.id;
                invent.property_id__c = newProperty.id;
                insert invent;
                
                
                NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
                SR.Agency_Type__c = 'Corporate';
                SR.Agency__c = null;
                SR.Trade_License_Number__c = 'test';
                SR.Agent_Name__c = portalUser.id;
                insert SR;
                
                PageReference pageRef = Page.AP_BookingLandingPage;
                test.setCurrentPageReference(pageRef);
                pageRef.getParameters().put('objSRId',SR.Id);
                pageRef.getParameters().put('sr',SR.Id);
                AP_BookingLandingController controller = new AP_BookingLandingController();
                controller.getDateLiterals();
                controller.getAgentsNameList();
                controller.getStatusValues();
                controller.statusValue = 'DRAFT';
                controller.init();
                Set<String> SetSrid =  new Set<String>();
                SetSrid.add('SetSrid');
                controller.lstSR = null;
                controller.listToDisplay = null;
                controller.SrStatus = null; 
                controller.SetSrid = SetSrid;
                controller.SendPayementLinkMail();
                controller.callSOAGeneration();
                Boolean isPortalUser = AP_BookingLandingController.isPortalUser;
                controller.getStatusValuesForEOI();
                controller.showAll();
                
                EOI_Process__c eoi = [Select Id From EOI_Process__c Limit 1];
                controller.currentEOIId = eoi.Id;
                controller.fetchServiceRequest();
                controller.SendPreBookId();
            }
        }
        Test.stopTest();
    }
}