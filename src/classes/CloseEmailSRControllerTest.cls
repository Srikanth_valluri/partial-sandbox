@isTest
private class CloseEmailSRControllerTest {

    static testMethod void testMethod1() {
    
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
    
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.Subject = 'Test email';
        objCase.Description = 'Test email 1111';
        objCase.Origin = 'Email';
        objCase.AccountId = objAcc.id;
        objCase.Status = 'New';
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        insert objCase;
        
        List<Case> lstCase = [SELECT Id, CaseNumber FROM Case WHERE Id =: objCase.Id Limit 1];
        
        Test.startTest();
            PageReference pageCloseEmailSR = Page.CloseEmailSR;
            Test.setCurrentPage(pageCloseEmailSR);
            String strNode = '["'+lstCase[0].CaseNumber+'"]';
            ApexPages.currentPage().getParameters().put('node',strNode);
            
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(new List<Case> {objCase});
            
            CloseEmailSRController controllerObj = new CloseEmailSRController(sc);
            controllerObj.init();
            controllerObj.objCaseInitial.Reason_to_Close_Email_Case__c = 'Spam';
            controllerObj.processSelected();
        Test.stopTest();
    }
    static testMethod void testMethod2() {

        User creUser = createCREUsers('creUser7000@test.com', '7000');
    
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
    
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.Subject = 'Test email';
        objCase.Description = 'Test email 1111';
        objCase.Origin = 'Email';
        objCase.AccountId = objAcc.id;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        insert objCase;
        
        List<Case> lstCase = [SELECT Id, CaseNumber FROM Case WHERE Id =: objCase.Id Limit 1];
        
        Test.startTest();
            PageReference pageCloseEmailSR = Page.CloseEmailSR;
            Test.setCurrentPage(pageCloseEmailSR);
            String strNode = '["'+lstCase[0].CaseNumber+'"]';
            
            ApexPages.currentPage().getParameters().put('node1',strNode);
            
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(new List<Case> {objCase});
            
            CloseEmailSRController controllerObj = new CloseEmailSRController(sc);
            
            controllerObj.objCaseInitial.OwnerId = creUser.Id;
            controllerObj.init();
            controllerObj.processSelectedForOwnerChange();
        Test.stopTest();
    }
    
    public static User createCREUsers(String creUserName, String extension) {
        User creUser;
        System.runAs(new User(Id = UserInfo.getUserID())) {
             creUser = new User(
                Username = creUserName,
                Extension = extension,
                Alias = 'TestCRE',
                Email = creUserName,
                Emailencodingkey = 'UTF-8',
                Lastname = 'Test CRE',
                Languagelocalekey = 'en_US',
                Localesidkey = 'en_US',
                Profileid = [Select Id From Profile Where Name='Contact Center - Manager'].Id,
                Country = 'United Arab Emirates',
                IsActive = true,
                Timezonesidkey='America/Los_Angeles'
            );
            insert creUser;
        }
        return creUser;
    }
}