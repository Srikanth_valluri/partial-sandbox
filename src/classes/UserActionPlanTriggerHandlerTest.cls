/****************************************************************************************************
* Name          : UserActionPlanTriggerHandlerTest                                                  *
* Description   : Test class for UserActionPlanTriggerHandler                                       *
* Created Date  : 02/04/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    02/04/2018      Initial Draft.                                          *
****************************************************************************************************/
@isTest 
public class UserActionPlanTriggerHandlerTest {
    @isTest static void testMethod1 (){
        
        Profile profileObj = [SELECT Id FROM Profile WHERE Name = 'Property Consultant' LIMIT 1]; 
        User userObj = new User(
            Alias = 'stand', 
            Email = 'user@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'Test', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObj.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfd@testorg.com'
        );
        insert userObj;

        Zero_Sales_User__c zsObj = new Zero_Sales_User__c(
            Name = userObj.Name,
            HOS_Descision__c = 'Pending'
        );
        insert zsObj;

        User_Action_Plan__c plan1 = new User_Action_Plan__c(
            Name = userObj.Name,
            PC__c = userObj.Id,
            Action_Plan__c = '1st to 15th',
            Zero_Sales_User__c = zsObj.Id,
            Target_Meetings__c = 5,
            Target_Outbound_Calls__c = 7,
            Target_Prospects_Created__c = 15
        );
        insert plan1;

        Test.startTest();
        plan1.Name = 'testUpdated';
        update plan1;
        plan1.Target_Prospects_Created__c = 25;
        try { update plan1; } catch(Exception e) {}
        delete plan1;
        Test.stopTest();
    }
}