public with sharing class ProfilePhotoUploadController {

    public String getUserPhotoUrl() {
        return CustomerCommunityUtils.getFullPhotoUrl();
    }

    /**
     * Sets current User's Community Profile Photo from the given ContentDocument Id
     * @param  fileId Id of the ContentDocument from where the Photo Binary data is to be used
     * @return        Photo Object
     */
    @RemoteAction
    public static ConnectApi.Photo setPhotoFromVersion(String fileId) {
        ConnectApi.Photo photo;
        List<ContentVersion> lstVersion = [ SELECT  Id
                                                    , ContentDocumentId
                                                    , VersionNumber
                                                    , VersionData
                                                    , FileType
                                                    , Title
                                            FROM    ContentVersion
                                            WHERE   Id = :fileId];
        if (lstVersion.size() > 0) {
            photo = ConnectApi.UserProfiles.setPhoto(
                Network.getNetworkId(),
                UserInfo.getUserId(),
                new ConnectApi.BinaryInput(
                    lstVersion[0].VersionData,
                    'image/' + lstVersion[0].FileType.toLowerCase(),
                    lstVersion[0].Title
                )
            );
            Database.delete(lstVersion[0].ContentDocumentId);
        }
        return photo;
    }

}