@isTest
public with sharing class MileStonePaymentDetailsWrapperTest 
{

  /* * * * * * * * * * * * *
  *  Method Name:  testGeneratedWsdl
  *  Purpose:      This method is used to unit test functionality for MileStonePaymentDetailsWrapper class
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */
  static testMethod void testGeneratedWsdl()
  {
    MileStonePaymentDetailsWrapper.MileStonePaymentDetails objPaymentDetails = new MileStonePaymentDetailsWrapper.MileStonePaymentDetails();
    objPaymentDetails.message='ABC';
    objPaymentDetails.status = 'Active';
    objPaymentDetails.customErrorMsg = 'Error';

    List<MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE> lstRegTable = new List<MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE>();
    MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE objPaymentTerm = new MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE();
    objPaymentTerm.REGISTRATION_ID = '12345';
    objPaymentTerm.DUE_DATE = '10/12/2017';
    objPaymentTerm.MILESTEON_PERCENT_VALUE = '10';
    objPaymentTerm.PAID_AMOUNT = '100';
    objPaymentTerm.PAID_PERCENTAGE = '5';
    objPaymentTerm.DUE_AMOUNT = '50';
    objPaymentTerm.LINE_ID = '123';
    objPaymentTerm.MILESTONE_EVENT_AR = 'test arabic';
    objPaymentTerm.DESCRIPTION = 'test DESCRIPTION';
    objPaymentTerm.MILESTONE_EVENT = 'test milestone';
    objPaymentTerm.INVOICE_AMOUNT = '1000';
    objPaymentTerm.INSTALLMENT = 'I001';
    objPaymentTerm.TERM_ID = '12345';
    lstRegTable.add(objPaymentTerm);

    objPaymentDetails.REG_TERM_PYMNT_TABLE = lstRegTable;

    String json = '{'+
    '  \"REG_TERM_PYMNT_TABLE\": ['+
    '    {'+
    '      \"REGISTRATION_ID\": \"41332\",'+
    '      \"DUE_DATE\": \"21-MAR-2013\",'+
    '      \"MILESTEON_PERCENT_VALUE\": \"10\",'+
    '      \"PAID_AMOUNT\": \"199020\",'+
    '      \"PAID_PERCENTAGE\": \"100\",'+
    '      \"DUE_AMOUNT\": \"0\",'+
    '      \"LINE_ID\": \"285263\",'+
    '      \"MILESTONE_EVENT_AR\": \"فورا\",'+
    '      \"DESCRIPTION\": \"DEPOSIT\",'+
    '      \"MILESTONE_EVENT\": \"Immediate\",'+
    '      \"INVOICE_AMOUNT\": \"199020\",'+
    '      \"INSTALLMENT\": \"DP\",'+
    '      \"TERM_ID\": \"27493\"'+
    '    },'+
    '    {'+
    '      \"REGISTRATION_ID\": \"41332\",'+
    '      \"DUE_DATE\": null,'+
    '      \"MILESTEON_PERCENT_VALUE\": \"0\",'+
    '      \"PAID_AMOUNT\": null,'+
    '      \"PAID_PERCENTAGE\": \"0\",'+
    '      \"DUE_AMOUNT\": null,'+
    '      \"LINE_ID\": \"285264\",'+
    '      \"MILESTONE_EVENT_AR\": null,'+
    '      \"DESCRIPTION\": \"1ST INSTALLMENT\",'+
    '      \"MILESTONE_EVENT\": null,'+
    '      \"INVOICE_AMOUNT\": null,'+
    '      \"INSTALLMENT\": \"I001\",'+
    '      \"TERM_ID\": \"27493\"'+
    '    },'+
    '    {'+
    '      \"REGISTRATION_ID\": \"41332\",'+
    '      \"DUE_DATE\": \"20-MAY-2013\",'+
    '      \"MILESTEON_PERCENT_VALUE\": \"10\",'+
    '      \"PAID_AMOUNT\": \"199020\",'+
    '      \"PAID_PERCENTAGE\": \"100\",'+
    '      \"DUE_AMOUNT\": \"0\",'+
    '      \"LINE_ID\": \"285265\",'+
    '      \"MILESTONE_EVENT_AR\": \"خلال 60 يوما من تاريخ البيع\",'+
    '      \"DESCRIPTION\": \"2ND INSTALLMENT\",'+
    '      \"MILESTONE_EVENT\": \"Within 60 days of Sale Date\",'+
    '      \"INVOICE_AMOUNT\": \"199020\",'+
    '      \"INSTALLMENT\": \"I002\",'+
    '      \"TERM_ID\": \"27493\"'+
    '    },'+
    '    {'+
    '      \"REGISTRATION_ID\": \"41332\",'+
    '      \"DUE_DATE\": \"19-JUL-2013\",'+
    '      \"MILESTEON_PERCENT_VALUE\": \"10\",'+
    '      \"PAID_AMOUNT\": \"199020\",'+
    '      \"PAID_PERCENTAGE\": \"100\",'+
    '      \"DUE_AMOUNT\": \"0\",'+
    '      \"LINE_ID\": \"285266\",'+
    '      \"MILESTONE_EVENT_AR\": \"خلال 120 يوما من تاريخ البيع\",'+
    '      \"DESCRIPTION\": \"3RD INSTALLMENT\",'+
    '      \"MILESTONE_EVENT\": \"Within 120 days of Sale Date\",'+
    '      \"INVOICE_AMOUNT\": \"199020\",'+
    '      \"INSTALLMENT\": \"I003\",'+
    '      \"TERM_ID\": \"27493\"'+
    '    },'+
    '    {'+
    '      \"REGISTRATION_ID\": \"41332\",'+
    '      \"DUE_DATE\": \"03-FEB-2015\",'+
    '      \"MILESTEON_PERCENT_VALUE\": \"10\",'+
    '      \"PAID_AMOUNT\": \"199020\",'+
    '      \"PAID_PERCENTAGE\": \"100\",'+
    '      \"DUE_AMOUNT\": \"0\",'+
    '      \"LINE_ID\": \"285267\",'+
    '      \"MILESTONE_EVENT_AR\": \"عند إستكمال 20 ٪ من المشروع\",'+
    '      \"DESCRIPTION\": \"4TH INSTALLMENT\",'+
    '      \"MILESTONE_EVENT\": \"On 20% Of Project Completion\",'+
    '      \"INVOICE_AMOUNT\": \"199020\",'+
    '      \"INSTALLMENT\": \"I004\",'+
    '      \"TERM_ID\": \"27493\"'+
    '    },'+
    '    {'+
    '      \"REGISTRATION_ID\": \"41332\",'+
    '      \"DUE_DATE\": \"11-APR-2015\",'+
    '      \"MILESTEON_PERCENT_VALUE\": \"10\",'+
    '      \"PAID_AMOUNT\": \"199020\",'+
    '      \"PAID_PERCENTAGE\": \"100\",'+
    '      \"DUE_AMOUNT\": \"0\",'+
    '      \"LINE_ID\": \"285268\",'+
    '      \"MILESTONE_EVENT_AR\": \"عند إستكمال 30 ٪ من المشروع\",'+
    '      \"DESCRIPTION\": \"5TH INSTALLMENT\",'+
    '      \"MILESTONE_EVENT\": \"On 30% Of Project Completion\",'+
    '      \"INVOICE_AMOUNT\": \"199020\",'+
    '      \"INSTALLMENT\": \"I005\",'+
    '      \"TERM_ID\": \"27493\"'+
    '    },'+
    '    {'+
    '      \"REGISTRATION_ID\": \"41332\",'+
    '      \"DUE_DATE\": \"30-JUL-2015\",'+
    '      \"MILESTEON_PERCENT_VALUE\": \"10\",'+
    '      \"PAID_AMOUNT\": \"199020\",'+
    '      \"PAID_PERCENTAGE\": \"100\",'+
    '      \"DUE_AMOUNT\": \"0\",'+
    '      \"LINE_ID\": \"285269\",'+
    '      \"MILESTONE_EVENT_AR\": \"عند إستكمال 40 ٪ من المشروع\",'+
    '      \"DESCRIPTION\": \"6TH INSTALLMENT\",'+
    '      \"MILESTONE_EVENT\": \"On 40% Of Project Completion\",'+
    '      \"INVOICE_AMOUNT\": \"199020\",'+
    '      \"INSTALLMENT\": \"I006\",'+
    '      \"TERM_ID\": \"27493\"'+
    '    },'+
    '    {'+
    '      \"REGISTRATION_ID\": \"41332\",'+
    '      \"DUE_DATE\": \"30-OCT-2015\",'+
    '      \"MILESTEON_PERCENT_VALUE\": \"10\",'+
    '      \"PAID_AMOUNT\": \"199020\",'+
    '      \"PAID_PERCENTAGE\": \"100\",'+
    '      \"DUE_AMOUNT\": \"0\",'+
    '      \"LINE_ID\": \"285270\",'+
    '      \"MILESTONE_EVENT_AR\": \"عند إستكمال 50 ٪ من المشروع\",'+
    '      \"DESCRIPTION\": \"7TH INSTALLMENT\",'+
    '      \"MILESTONE_EVENT\": \"On 50% Of Project Completion\",'+
    '      \"INVOICE_AMOUNT\": \"199020\",'+
    '      \"INSTALLMENT\": \"I007\",'+
    '      \"TERM_ID\": \"27493\"'+
    '    },'+
    '    {'+
    '      \"REGISTRATION_ID\": \"41332\",'+
    '      \"DUE_DATE\": \"23-DEC-2015\",'+
    '      \"MILESTEON_PERCENT_VALUE\": \"10\",'+
    '      \"PAID_AMOUNT\": \"199020\",'+
    '      \"PAID_PERCENTAGE\": \"100\",'+
    '      \"DUE_AMOUNT\": \"0\",'+
    '      \"LINE_ID\": \"285271\",'+
    '      \"MILESTONE_EVENT_AR\": \"عند إستكمال 60 ٪ من المشروع\",'+
    '      \"DESCRIPTION\": \"8TH INSTALLMENT\",'+
    '      \"MILESTONE_EVENT\": \"On 60% Of Project Completion\",'+
    '      \"INVOICE_AMOUNT\": \"199020\",'+
    '      \"INSTALLMENT\": \"I008\",'+
    '      \"TERM_ID\": \"27493\"'+
    '    },'+
    '    {'+
    '      \"REGISTRATION_ID\": \"41332\",'+
    '      \"DUE_DATE\": \"15-AUG-2016\",'+
    '      \"MILESTEON_PERCENT_VALUE\": \"10\",'+
    '      \"PAID_AMOUNT\": \"199020\",'+
    '      \"PAID_PERCENTAGE\": \"100\",'+
    '      \"DUE_AMOUNT\": \"0\",'+
    '      \"LINE_ID\": \"285272\",'+
    '      \"MILESTONE_EVENT_AR\": \"عند إستكمال 80 ٪ من المشروع\",'+
    '      \"DESCRIPTION\": \"9TH INSTALLMENT\",'+
    '      \"MILESTONE_EVENT\": \"On 80% Of Project Completion\",'+
    '      \"INVOICE_AMOUNT\": \"199020\",'+
    '      \"INSTALLMENT\": \"I009\",'+
    '      \"TERM_ID\": \"27493\"'+
    '    },'+
    '    {'+
    '      \"REGISTRATION_ID\": \"41332\",'+
    '      \"DUE_DATE\": null,'+
    '      \"MILESTEON_PERCENT_VALUE\": \"10\",'+
    '      \"PAID_AMOUNT\": null,'+
    '      \"PAID_PERCENTAGE\": \"0\",'+
    '      \"DUE_AMOUNT\": null,'+
    '      \"LINE_ID\": \"285273\",'+
    '      \"MILESTONE_EVENT_AR\": \"عند الاكمال\",'+
    '      \"DESCRIPTION\": \"10TH INSTALLMENT\",'+
    '      \"MILESTONE_EVENT\": \"On Completion\",'+
    '      \"INVOICE_AMOUNT\": null,'+
    '      \"INSTALLMENT\": \"I010\",'+
    '      \"TERM_ID\": \"27493\"'+
    '    }'+
    '  ],'+
    '  \"message\": \"Successfully Fetched Details for RegID41332\",'+
    '  \"status\": \"S\"'+
    '}';

    MileStonePaymentDetailsWrapper.parse(json);

  }
}