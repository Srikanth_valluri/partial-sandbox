/****************************************************************************************************
* Name          : CollateralsController                                                             *
* Description   : Class to fetch the annoucement details related to account filter                  *
* Created Date  : 08/08/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER     AUTHOR            DATE            COMMENTS                                                *
* 1.0     Twinkle Panjabi   08/08/2018      Initial Draft.                                          *
****************************************************************************************************/
public without sharing class CollateralsController {
    
    public List<WrapAnnouncementAndFiles> announcemetIdWrapperList {get; set;}
    public Integer announcemetIdWrapperListSize                    {get; set;}
    private Map<Id,Id> mapOfContentVersionDocument;
    //Constructor
    public CollateralsController() {
        mapOfContentVersionDocument = new Map<Id,Id>();
        fetchAnnoncements();
    }
    
    /**
* Method to fetch the  Annoucements
* 
* @param: NA
* 
* @return: NA
*/
    public void fetchAnnoncements(){
        announcemetIdWrapperList = new List<WrapAnnouncementAndFiles>();
        User portalUser = [SELECT ContactId, AccountId, Profile.Name,
                           Account.Broker_Class__c,
                           Account.RecordType.Name,
                           Account.recordTypeId, 
                           Account.Country__c,
                           Account.Agency_Corporate_Type__c,
                           Account.Agency_Type__c
                           FROM User 
                           WHERE ID =:UserInfo.getUserId() 
                           LIMIT 1];                                                           
        Map<Id,Announcement__c>  mapOfIdAndAnnoucements = new Map<Id,Announcement__c>();
        for (Announcement__c announcement: [ SELECT Account__c, Account_Record_Type__c, Agency_Corporate_Type__c,
                                            Country__c, Agency_Type__c, Broker_Class__c,
                                            Name, Title__c, Description__c, Description_2__c
                                            FROM Announcement__c 
                                            WHERE Start_Date__c <= today 
                                            AND End_Date__c >= today 
                                            AND Active__c = true])
        {
            if (portalUser.AccountId == announcement.Account__c) {
                mapOfIdAndAnnoucements.put (announcement.id, announcement);
            }
            if (announcement.Agency_Type__c == 'All' && announcement.Account__c == NULL) {
                mapOfIdAndAnnoucements.put (announcement.id, announcement);
            }
            boolean addToMap = false;
            if (announcement.Account__c == NULL && announcement.Agency_Type__c != 'All') {
            
                if (announcement.Agency_Corporate_Type__c != NULL){if(announcement.Agency_Corporate_Type__c == portalUser.Account.Agency_Corporate_Type__c){addToMap = true;}} 
                    
                if (announcement.Agency_Type__c != NULL){if(announcement.Agency_Type__c  == portalUser.Account.Agency_Type__c){addToMap = true;}}
                    
                if (announcement.Country__c != NULL){if(announcement.Country__c == portalUser.Account.Country__c){addToMap = true;}}
                     
                if (announcement.Broker_Class__c != NULL){if(announcement.Broker_Class__c == portalUser.Account.Broker_Class__c){addToMap = true;}}
                    
                if (announcement.Account_Record_Type__c != NULL){if(announcement.Account_Record_Type__c == portalUser.Account.RecordType.Name){addToMap = true;}}
                    
                if(addToMap == true){
                    mapOfIdAndAnnoucements.put (announcement.id, announcement);    
                }
            }
        }
        system.debug('>>>>>mapOfIdAndAnnoucements>>>'+mapOfIdAndAnnoucements);                                                                                      
        if(!mapOfIdAndAnnoucements.isEmpty()) {
            List<Id> idList = new List<Id>();
            idList.addAll(mapOfIdAndAnnoucements.keySet());
            
            Map<Id,List<ContentDocument>> announcmentIdContentDocMap = displayListOfAttachment(idList);
            
            for(Id announcementId : mapOfIdAndAnnoucements.keySet()){
                List<ContentDocument> contentDocumentList =  announcmentIdContentDocMap.containsKey(announcementId) ? 
                    announcmentIdContentDocMap.get(announcementId) : 
                new List<ContentDocument> ();
                List<ContentDocumentWithVersionId> contentDocumentWrp = new List<ContentDocumentWithVersionId>();
                for( ContentDocument objContentDoc : contentDocumentList ) {
                    Id converId = mapOfContentVersionDocument.get(objContentDoc.Id);
                    contentDocumentWrp.add(new ContentDocumentWithVersionId(converId, objContentDoc) );
                }
                announcemetIdWrapperList.add(new wrapAnnouncementAndFiles
                                             (mapOfIdAndAnnoucements.get(announcementId),
                                              contentDocumentWrp));                
            }
        }
        announcemetIdWrapperListSize = announcemetIdWrapperList.size();
    }
    
    /**
* Method to fetch the Content Document related to the Annoucements
* 
* @param: parentIds: Accouncement Ids
* 
* @return: Id of Accouncement and List of Content Document
*/
    public Map<Id,List<ContentDocument>> displayListOfAttachment(List<Id> parentIds) {
        Map<Id,List<ContentDocument>> announcmentIdContentDocMap = new Map<Id,List<ContentDocument>>();
        Map<Id, Id> mapOfEntityIdAndContent = new Map<Id,Id>();
        List<ContentDocumentLink> contentDocumentLink = [SELECT ContentDocumentId,
                                                         LinkedEntityId
                                                         FROM ContentDocumentLink
                                                         WHERE LinkedEntityId IN: parentIds ];
        
        Set<Id> contentDocumentLinkIds = new Set<Id>();
        for(ContentDocumentLink objContent : contentDocumentLink) {
            contentDocumentLinkIds.add(objContent.ContentDocumentId);
            mapOfEntityIdAndContent.put(objContent.ContentDocumentId,objContent.LinkedEntityId);
        }
        String queryStr = 'SELECT Id ,Title,Owner.Name,ContentSize,FileType,ParentId FROM ContentDocument  WHERE ID =:contentDocumentLinkIds ';
        queryStr += ' ORDER BY CreatedDate DESC LIMIT 10';
        List<ContentDocument> contentList = Database.query(queryStr);
        system.debug('contentList--------------'+contentList);
        Set<Id> contentDocumentIds = new Set<Id> ();
        for(ContentDocument contentObj :contentList) {
            contentDocumentIds.add(contentObj.Id);
            Id parentId = mapOfEntityIdAndContent.get(contentObj.Id);
            if(announcmentIdContentDocMap.containsKey(parentId)){
                List<ContentDocument> contenetList = announcmentIdContentDocMap.get(parentId);
                contenetList.add(contentObj);
                announcmentIdContentDocMap.put(parentId, contenetList);
                
            }else{
                announcmentIdContentDocMap.put(parentId,new List<ContentDocument>{contentObj});
            }
        }
        
        for(ContentVersion conVerObj : [SELECT Id,ContentDocumentId FROM ContentVersion WHERE ContentDocumentId in: contentDocumentIds ORDER BY CreatedDate ASC]) {
            mapOfContentVersionDocument.put(conVerObj.contentdocumentId,conVerObj.Id );
        }
        system.debug('announcmentIdContentDocMap--------------'+announcmentIdContentDocMap);
        return announcmentIdContentDocMap;
    }
    
    //Wrapper Class   
    public class WrapAnnouncementAndFiles{
        public Announcement__c objAcc {get; set;}
        public List<ContentDocumentWithVersionId> contentDocumentList {get; set;}
        public WrapAnnouncementAndFiles( Announcement__c objAcc,
                                        List<ContentDocumentWithVersionId> contentDocumentList ) 
        {
            this.objAcc = objAcc;
            this.contentDocumentList = contentDocumentList;
        }
    }
    
    //Wrapper Class for Content Version 
    public class ContentDocumentWithVersionId {
        public String contentVersionId {get; set;}
        public ContentDocument contentDoc  {get; set;}
        public ContentDocumentWithVersionId(String contentVersionId,ContentDocument contentDoc ) {
            this.contentVersionId = contentVersionId;
            this.contentDoc = contentDoc;
        }
    }
}