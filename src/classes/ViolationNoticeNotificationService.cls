public without sharing class ViolationNoticeNotificationService implements IsNotificationService {

    public List<NotificationService.Notification> getNotifications() { return null;}

    public List<NotificationService.Notification> getNotifications(NotificationService.Notification request) {
        List<NotificationService.Notification> notificationList = new List<NotificationService.Notification>();
        if (String.isBlank(request.accountId)) {
            return notificationList;
        }
        Id accountId = request.accountId;
        for(SR_Attachments__c violationNotice : [
            SELECT      Id
                        , Booking_Unit__r.Unit_Name__c
            FROM        SR_Attachments__c
            WHERE       Document_Type__c = 'Violation Notice'
                        AND Notifications_Read__c = false
                        AND (FM_Case__r.Account__c = :accountId
                            OR FM_Case__r.Tenant__c = :accountId)
            ORDER BY    LastModifiedDate DESC
        ]) {
            NotificationService.Notification notification = new NotificationService.Notification();
            notification.accountId = accountId;
            //String status = caseRecord.Status == 'New' ? 'Created' : 'Closed';
            notification.title      = 'New Violation Notice raised against the unit '
                                        + violationNotice.Booking_Unit__r.Unit_Name__c;
            notification.recordId   = violationNotice.Id;
            notification.status     = 'New';
            notification.record     = violationNotice;
            notificationList.add(notification);
        }
        return notificationList;
    }

    @RemoteAction
    public static list<NotificationService.Notification> markRead(
        List<NotificationService.Notification> notificationList
    ) {
        List<SR_Attachments__c> lstNotice = new List<SR_Attachments__c>();
        for (NotificationService.Notification notification: notificationList) {
            if (String.isNotBlank(notification.recordId)) {
                lstNotice.add(new SR_Attachments__c(Id=notification.recordId, Notifications_Read__c=true));
            }
        }
        Integer i = 0;
        for (Database.SaveResult saveResult : Database.update(lstNotice, false)) {
            notificationList[i++].isRead = saveResult.isSuccess();
        }
        return notificationList;
    }

    public NotificationService.Notification markUnread(NotificationService.Notification notification) {
        update new SR_Attachments__c(Id = notification.recordId, Notifications_Read__c = false);
        notification.isRead = true;
        return notification;
    }

}