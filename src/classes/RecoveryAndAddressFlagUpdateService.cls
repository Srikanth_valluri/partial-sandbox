public without sharing class RecoveryAndAddressFlagUpdateService {
    public static Id AddressCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Welcome Calling List').RecordTypeId;
    public static Id RecoveryCallCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Recovery Calling List').RecordTypeId;

    @InvocableMethod
    public static void updateFlagInIPMS( List<Calling_List__c> lstCallingList ) {
        system.debug('in  updateFlagInIPMS: '+lstCallingList);
        Set<String> addCallingIds = new Set<String>();
        Set<String> recoveryCallingIds = new Set<String>();
        
        for( Calling_List__c objCallingList : lstCallingList ) {
            system.debug(objCallingList.RecordTypeId + ' AddressCallingRecordTypeId : '+AddressCallingRecordTypeId);
            if( objCallingList.RecordTypeId == AddressCallingRecordTypeId ) {
                 system.debug('In Address If');
                addCallingIds.add( objCallingList.Id );
            }else if( objCallingList.RecordTypeId == RecoveryCallCallingRecordTypeId )  {
                recoveryCallingIds.add( objCallingList.Id );
            }
        }
        if( !addCallingIds.isEmpty() ) {
            getAddressFlagUpdate(addCallingIds);
        }
        if(!recoveryCallingIds.isEmpty()){
            getRecoveryFlagUpdate(recoveryCallingIds);
        }
    }
    
    @Future(callout=true)
    public static void getAddressFlagUpdate( Set<String> callIds ) {
        system.debug('callIds : '+callIds);
        
        List<Calling_List__c> callingList = [SELECT Id, Booking_Unit__c,Account__c,Address_Verified__c, Flag_Updated_In_IPMS__c ,Registration_ID__c
                                               FROM Calling_List__c WHERE Id IN :callIds ];
        
        FlagUpdateService.CallingListFlagHttpSoap11Endpoint calloutObj = new FlagUpdateService.CallingListFlagHttpSoap11Endpoint();
        calloutObj.timeout_x = 120000;
        Data resObj = new Data();

        try{
            system.debug('in try');
            List<FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5> regTerms = new List<FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5>();
            FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5 objTerms = new FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5();
            if( callingList[0].Address_Verified__c == true ) {
                objTerms.ATTRIBUTE1     = 'Y';
            }else{
                objTerms.ATTRIBUTE1     = 'N';
            }
            
            
            objTerms.ATTRIBUTE10 = '';
            objTerms.ATTRIBUTE11    = '';
            objTerms.ATTRIBUTE12 = '';
            objTerms.ATTRIBUTE13 = '';
            objTerms.ATTRIBUTE14 = '';
            objTerms.ATTRIBUTE15 = '';
            objTerms.ATTRIBUTE16 = '';
            objTerms.ATTRIBUTE17 = '';
            objTerms.ATTRIBUTE18 = '';
            objTerms.ATTRIBUTE19 = '';
            objTerms.ATTRIBUTE2     = '';
            objTerms.ATTRIBUTE20 = '';
            objTerms.ATTRIBUTE21 = '';
            objTerms.ATTRIBUTE22 = '';
            objTerms.ATTRIBUTE23 = '';
            objTerms.ATTRIBUTE24 = '';
            objTerms.ATTRIBUTE25 = '';
            objTerms.ATTRIBUTE26 = '';
            objTerms.ATTRIBUTE27 = '';
            objTerms.ATTRIBUTE28 = '';
            objTerms.ATTRIBUTE29 = '';
            objTerms.ATTRIBUTE3     = '';
            objTerms.ATTRIBUTE30 = '';
            objTerms.ATTRIBUTE31 = '';
            objTerms.ATTRIBUTE32 = '';
            objTerms.ATTRIBUTE33 = '';
            objTerms.ATTRIBUTE34 = '';
            objTerms.ATTRIBUTE35 = '';
            objTerms.ATTRIBUTE36 = '';
            objTerms.ATTRIBUTE37 = '';
            objTerms.ATTRIBUTE38 = '';
            objTerms.ATTRIBUTE39 = '';
            objTerms.ATTRIBUTE4     = '';
            objTerms.ATTRIBUTE41 = '';
            objTerms.ATTRIBUTE42 = '';
            objTerms.ATTRIBUTE43 = '';
            objTerms.ATTRIBUTE44 = '';
            objTerms.ATTRIBUTE45 = '';
            objTerms.ATTRIBUTE46 = '';
            objTerms.ATTRIBUTE47 = '';
            objTerms.ATTRIBUTE48 = '';
            objTerms.ATTRIBUTE49 = '';
            objTerms.ATTRIBUTE5     = '';
            objTerms.ATTRIBUTE50 = '';
            objTerms.ATTRIBUTE6     = '';
            objTerms.ATTRIBUTE7     = '';
            objTerms.ATTRIBUTE8     = '';
            objTerms.ATTRIBUTE9     = '';
            objTerms.PARAM_ID   = callingList[0].Registration_ID__c;
            regTerms.add(objTerms);
            system.debug( 'regTerms' + regTerms);
            
            String response = calloutObj.UpdateAddressVerifiedFlag( '2-'+String.valueOf( Datetime.now().getTime())
                                                                  , 'UPDATE_ADDRESS_VERIFIED' ,'SFDC' ,regTerms);
            system.debug('== response  =='+response );
            resObj = (Data)JSON.deserialize(response, RecoveryAndAddressFlagUpdateService.Data.class);
            system.debug('resObj response === '+ resObj);
            if( resObj.data[0].PROC_STATUS.equalsIgnoreCase('S')) {
                callingList[0].Flag_Updated_In_IPMS__c = true;
            }else {
                callingList[0].Flag_Updated_In_IPMS__c = false;
            }
            update callingList;
                
        } catch ( Exception e ){
            Error_Log__c errorInst = new Error_Log__c();
            errorInst.Error_Details__c = 'No Data Found';
            errorInst.Booking_Unit__c = callingList[0].Booking_Unit__c;
            if(callingList[0].Account__c != null){
                errorInst.Account__c = callingList[0].Account__c;
            }
            if(callingList[0].Id != null){
                errorInst.Calling_List__c = callingList[0].Id;
            }
            errorInst.Process_Name__c = 'Calling List';
            insert errorInst;
            
            
            resObj.status = 'Exception';
            resObj.message = String.valueOf( e ) ;
        }
        
    }

    @Future(callout=true)
    public static void getRecoveryFlagUpdate( Set<String> callIds ) {
        system.debug('callIds : '+callIds);
        
        List<Calling_List__c> callingList = [SELECT Id,Booking_Unit__c,Account__c, Recovery__c, Flag_Updated_In_IPMS__c ,Registration_ID__c
                                               FROM Calling_List__c WHERE Id IN :callIds ];
        
        FlagUpdateService.CallingListFlagHttpSoap11Endpoint calloutObj = new FlagUpdateService.CallingListFlagHttpSoap11Endpoint();
        calloutObj.timeout_x = 120000;
        Data resObj = new Data();

        try{
            system.debug('in try');
            List<FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5> regTerms = new List<FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5>();
            FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5 objTerms = new FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5();
            if( callingList[0].Recovery__c == true ) {
                objTerms.ATTRIBUTE1     = 'Y';
            }else{
                objTerms.ATTRIBUTE1     = 'N';
            }
            
            
            objTerms.ATTRIBUTE10 = '';
            objTerms.ATTRIBUTE11    = '';
            objTerms.ATTRIBUTE12 = '';
            objTerms.ATTRIBUTE13 = '';
            objTerms.ATTRIBUTE14 = '';
            objTerms.ATTRIBUTE15 = '';
            objTerms.ATTRIBUTE16 = '';
            objTerms.ATTRIBUTE17 = '';
            objTerms.ATTRIBUTE18 = '';
            objTerms.ATTRIBUTE19 = '';
            objTerms.ATTRIBUTE2     = '';
            objTerms.ATTRIBUTE20 = '';
            objTerms.ATTRIBUTE21 = '';
            objTerms.ATTRIBUTE22 = '';
            objTerms.ATTRIBUTE23 = '';
            objTerms.ATTRIBUTE24 = '';
            objTerms.ATTRIBUTE25 = '';
            objTerms.ATTRIBUTE26 = '';
            objTerms.ATTRIBUTE27 = '';
            objTerms.ATTRIBUTE28 = '';
            objTerms.ATTRIBUTE29 = '';
            objTerms.ATTRIBUTE3     = '';
            objTerms.ATTRIBUTE30 = '';
            objTerms.ATTRIBUTE31 = '';
            objTerms.ATTRIBUTE32 = '';
            objTerms.ATTRIBUTE33 = '';
            objTerms.ATTRIBUTE34 = '';
            objTerms.ATTRIBUTE35 = '';
            objTerms.ATTRIBUTE36 = '';
            objTerms.ATTRIBUTE37 = '';
            objTerms.ATTRIBUTE38 = '';
            objTerms.ATTRIBUTE39 = '';
            objTerms.ATTRIBUTE4     = '';
            objTerms.ATTRIBUTE41 = '';
            objTerms.ATTRIBUTE42 = '';
            objTerms.ATTRIBUTE43 = '';
            objTerms.ATTRIBUTE44 = '';
            objTerms.ATTRIBUTE45 = '';
            objTerms.ATTRIBUTE46 = '';
            objTerms.ATTRIBUTE47 = '';
            objTerms.ATTRIBUTE48 = '';
            objTerms.ATTRIBUTE49 = '';
            objTerms.ATTRIBUTE5     = '';
            objTerms.ATTRIBUTE50 = '';
            objTerms.ATTRIBUTE6     = '';
            objTerms.ATTRIBUTE7     = '';
            objTerms.ATTRIBUTE8     = '';
            objTerms.ATTRIBUTE9     = '';
            objTerms.PARAM_ID   = callingList[0].Registration_ID__c;
            regTerms.add(objTerms);
            system.debug( 'regTerms' + regTerms);
            
            String response = calloutObj.UpdateRecoveryFlag( '2-'+String.valueOf( Datetime.now().getTime())
                                                           , 'UPDATE_RECOVERY_FLAG' ,'SFDC' ,regTerms);
            system.debug('== response  =='+response );
            resObj = (Data)JSON.deserialize(response, RecoveryAndAddressFlagUpdateService.Data.class);
            system.debug('resObj response === '+ resObj);
            if( resObj.data[0].PROC_STATUS.equalsIgnoreCase('S')) {
                callingList[0].Flag_Updated_In_IPMS__c = true;
            }else {
                callingList[0].Flag_Updated_In_IPMS__c = false;
            }
            update callingList;
                
        } catch ( Exception e ){
            Error_Log__c errorInst = new Error_Log__c();
            errorInst.Error_Details__c = 'No Data Found';
            errorInst.Booking_Unit__c = callingList[0].Booking_Unit__c;
            if(callingList[0].Account__c != null){
                errorInst.Account__c = callingList[0].Account__c;
            }
            if(callingList[0].Id != null){
                errorInst.Calling_List__c = callingList[0].Id;
            }
            errorInst.Process_Name__c = 'Calling List';
            insert errorInst;
            
            resObj.status = 'Exception';
            resObj.message = String.valueOf( e ) ;
        }
        
    }

    public class Data {
        public List<CallingListFlagUpdateResponse> data         {get;set;}
        public String message                                   {get;set;}
        public String status                                    {get;set;}
    }
    public class CallingListFlagUpdateResponse {
        public String PROC_STATUS                               {get;set;}
        public String PROC_MESSAGE                              {get;set;}
        public String PARAM_ID                                  {get;set;}
        public String Message_ID                                {get;set;}
    }
}