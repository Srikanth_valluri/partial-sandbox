//test class for abandoned SMS 
@isTest
public class AbandonedSMSTest{
    @testSetup static void setup() {
        EmailTemplate et1 = new EmailTemplate (developerName = 'test_EmailTemplateName', IsActive=true, FolderId = UserInfo.getUserId(),TemplateType= 'text', Name = 'test'); // plus any other fields that you want to set
        insert et1;

    }
    static testMethod void testMethod1() {      
    
    List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2; 
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
          List<Abandoned_Calls__c> lstCallingLists = createCallingList( objAcc.Id, 1 );
        insert lstCallingLists;

        ApexPages.StandardController controller = new ApexPages.standardController(lstCallingLists[0]);
               
        PageReference pageRef = Page.SendAbndSMSPage;
        pageRef.getParameters().put('Id', String.valueOf(lstCallingLists[0].Id));
        Test.setCurrentPage(pageRef);

        AbandonedSMS obj = new AbandonedSMS(controller);
    }

    static testMethod void testMethod2() {
    
    List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
              
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, Name = 'Test Name' ,First_Name__c='Test FirstName', Last_Name__c='Test LastName', Type='Other');
        insert objAcc;
       

         List<Abandoned_Calls__c> lstCallingLists = createCallingList( objAcc.Id, 1 );
        insert lstCallingLists;

        ApexPages.StandardController controller = new ApexPages.standardController(lstCallingLists[0]);
               
        PageReference pageRef = Page.SendAbndSMSPage;
        pageRef.getParameters().put('Id', String.valueOf(lstCallingLists[0].Id));
        Test.setCurrentPage(pageRef);

        AbandonedSMS obj = new AbandonedSMS(controller);

       
        obj.addElements();
        obj.removeElements();
        obj.callSMSService();
        List<SelectOption> SO = obj.getMyPersonalTemplateOptions();

        EmailTemplate et = [Select Id from EmailTemplate Where developerName = 'test_EmailTemplateName' Limit 1];
        obj.callingId = lstCallingLists[0].Id; 
        obj.selectedTemplateId = et.Id;
        obj.showContent();
    }
    
    static testMethod void testMethod3() {
    
    List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
        
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
         List<Abandoned_Calls__c> lstCallingLists = createCallingList( objAcc.Id, 1 );
        insert lstCallingLists;

        ApexPages.StandardController controller = new ApexPages.standardController(lstCallingLists[0]);
               
        PageReference pageRef = Page.SendAbndSMSPage;
        pageRef.getParameters().put('Id', String.valueOf(lstCallingLists[0].Id));
        Test.setCurrentPage(pageRef);

        AbandonedSMS obj = new AbandonedSMS(controller);
        obj.addElements();
        obj.callingId = lstCallingLists[0].Id; 
        obj.templateBody = '';
        obj.callSMSService();

    }    
    
    /*
     @ Description : To create calling list reocord
     @ Return      : list of abandoned record to be created
    */
    public static List<Abandoned_Calls__c> createCallingList( Id AccountId, Integer counter ) {
        List<Abandoned_Calls__c> lstCallingLists = new List<Abandoned_Calls__c>();
        for( Integer i=0; i<counter; i++ ) {
            lstCallingLists.add(new Abandoned_Calls__c( 
                Account__c = AccountId 
              , Mobile_No__c = '009123121323'
              ) );
        }
        return lstCallingLists;
    }    
}