@isTest
public class CollectionEmailAssignmentTest {

    
    static testMethod void testMethod1() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        objAcc.Party_Id__c = '15155';
        insert objAcc;
        System.assert(objAcc != null);
        
        Id collectionQueueId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName( 'Collection_Queue','Queue' );
        
        Id RecordTypeIdCollection = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Collections_Calling_List'
            AND IsActive = TRUE LIMIT 1
        ].Id;
        
        List<Calling_List__c> lstCallingLists = createCallingList( RecordTypeIdCollection , 1  );
        lstCallingLists[0].Party_Id__c = '15155';
        insert lstCallingLists;
        
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.Subject = 'Test email';
        objCase.Description = 'Test email 1111';
        objCase.Origin = 'Email';
        objCase.AccountId = objAcc.id;
        objCase.Status = 'New';
        objCase.OwnerId = collectionQueueId;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        insert objCase;
        
        CollectionEmailAssignment objCollectionEmailAssignment  = new CollectionEmailAssignment();
        
        Test.startTest();
            Database.executeBatch(objCollectionEmailAssignment,1);
        Test.stopTest();
    }
    /*
     @ Description : To create calling list reocord
     @ Return      : list of calling list to be created
    */
    public static List<Calling_List__c> createCallingList( Id RecordTypeIdCollection, Integer counter ) {
        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        for( Integer i=0; i<counter; i++ ) {
            lstCallingLists.add(new Calling_List__c( IsHideFromUI__c  = false 
                                                   , Inv_Due__c = 0
                                                   , DM_Due_Amount__c = 0 
                                                   , RecordTypeId = RecordTypeIdCollection
                                                   ) );
        }
        return lstCallingLists;
    }   
}