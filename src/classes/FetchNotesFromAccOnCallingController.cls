/***********************************************************************
* 
* Version            Date            Author            Description
* 1.0               17/01/18                            Initial Draft
**********************************************************************/
public class FetchNotesFromAccOnCallingController {
    public String callingId                                                         {get;set;}
    public String accId                                                             {get;set;}
    public static List<Account>AccountList                                          {get;set;}
    public static list<ContentDocumentLink>contentDocLinkLstForAccounts             {get;set;}
    public static list<ContentDocumentLink> contentDocLinkLstForCalling             {get;set;}
    public static List<CalllingToNotesWrapper>lstCalllingToNotesWrapperForCalling   {get;set;}
    public static List<CalllingToNotesWrapper>lstCalllingToNotesWrapperForAccounts  {get;set;}
    public static List<Note>noteListFromNotesAndAttachmentForAccount                {get;set;}
    public static List<Note>noteListFromNotesAndAttachmentForCalling                {get;set;}
    public static List<Note>noteListFromNotesAndAttachmentForCases                  {get;set;}
    public static List<CalllingToNotesWrapper>lstOfWrapperForAccounts               {get;set;}
    public static List<CalllingToNotesWrapper>lstOfWrapperForCallings               {get;set;}
   // public static List<CalllingToNotesWrapper>lstOfWrapperForCases                    {get;set;}
    public static Map<Id,ContentNote>IdToNoteMapForAccounts;
    public static Map<Id,ContentNote>IdToNoteMapForCalling;
    public FetchNotesFromAccOnCallingController(ApexPages.StandardController controller) {
        fetchNotes();
    }
    /*This method is used to fetch notes from accounts and Calling List to display on calling list*/
    public void fetchNotes(){
         lstOfWrapperForAccounts = new List<CalllingToNotesWrapper>();
         lstOfWrapperForCallings = new List<CalllingToNotesWrapper>();
        // lstOfWrapperForCases = new List<CalllingToNotesWrapper>();
         noteListFromNotesAndAttachmentForAccount = new List<Note>();
         noteListFromNotesAndAttachmentForCalling = new List<Note>();
         noteListFromNotesAndAttachmentForCases = new List<Note>();
        callingId = ApexPages.currentPage().getParameters().get('id');
        contentDocLinkLstForAccounts = new list<ContentDocumentLink>();
        contentDocLinkLstForCalling = new list<ContentDocumentLink>();
        AccountList = new List<Account>();
        lstCalllingToNotesWrapperForCalling = new List<CalllingToNotesWrapper>();
        lstCalllingToNotesWrapperForAccounts = new List<CalllingToNotesWrapper>();
        List<Id>notesIdLstForAccounts = new List<Id>();
        List<Id>notesIdLstForCalling = new List<Id>();
        Calling_List__c callingObj = [SELECT Id,
                                              Name,
                                              RecordType.Name,
                                              Account__c,
                                              owner.Name,
                                              ownerId
                                         FROM Calling_List__c
                                         WHERE Id =:callingId];
        if(String.isNotBlank(callingObj.Account__c)){
            accId = callingObj.Account__c;
            System.debug('accId::1:'+accId);
            accId = String.valueOf(accId).substring(0, 15); 
            System.debug('accId::2:'+accId);
        }
        if(String.isNotBlank(accId)){
        AccountList = [SELECT Id,
                                name,
                                RecordType.Name,
                                owner.Name,
                                ownerId
                           FROM Account
                            WHERE Id = :accId
                            Limit 1];
        }
        if(AccountList != null && !AccountList.isEmpty()){
            /*fetch notes from notes and attachments from account*/
                noteListFromNotesAndAttachmentForAccount = [SELECT Id,
                                                                    Body,
                                                                    Title,
                                                                    CreatedDate,
                                                                    OwnerId, 
                                                                    Owner.Name, 
                                                                    parentId 
                                                               FROM Note 
                                                               WHERE parentId = : accId];
                for(Note noteForAcc: noteListFromNotesAndAttachmentForAccount){
                    lstOfWrapperForAccounts.add(new CalllingToNotesWrapper('Account',
                                                                           AccountList[0],
                                                                            null,
                                                                            null,
                                                                            null));
                }
        }
        if(String.isNotBlank(accId)){
            contentDocLinkLstForAccounts =[SELECT Id ,
                                                  LinkedEntityId,
                                                  ContentDocumentId,
                                                  ContentDocument.title,
                                                  ContentDocument.Description  
                                             FROM ContentDocumentLink 
                                            WHERE LinkedEntityId 
                                               = : accId];
        }
        if(callingObj != null){
             /*fetch notes from notes and attachments from calling list*/
                noteListFromNotesAndAttachmentForCalling = [SELECT Id,
                                                                Body,
                                                                Title,
                                                                CreatedDate,
                                                                OwnerId, 
                                                                Owner.Name,
                                                                parentId 
                                                           FROM Note 
                                                           WHERE parentId = : callingId];
                for(Note noteForCalling: noteListFromNotesAndAttachmentForCalling){
                    lstOfWrapperForCallings.add(new CalllingToNotesWrapper('Calling List',
                                                                           callingObj,
                                                                            null,
                                                                            null,
                                                                            null));
                }   
        }
        
        
        if(String.isNotBlank(callingId)){
            contentDocLinkLstForCalling = [SELECT Id ,
                                                  LinkedEntityId,
                                                  ContentDocumentId,
                                                  ContentDocument.title,
                                                  ContentDocument.Description  
                                             FROM ContentDocumentLink 
                                            WHERE LinkedEntityId 
                                              = : callingId];
        }
         /*fill wrapper list with notes for accounts*/
         for(Integer index = 0; index < lstOfWrapperForAccounts.size(); index ++) {
            lstOfWrapperForAccounts[index].NoteListToMove = noteListFromNotesAndAttachmentForAccount[index];
         }   
         
         /*fill wrapper list with notes for calling list*/
         for(Integer index = 0; index < lstOfWrapperForCallings.size(); index ++) {
            lstOfWrapperForCallings[index].NoteListToMove = noteListFromNotesAndAttachmentForCalling[index];
         } 
         
        if(contentDocLinkLstForAccounts != null && !contentDocLinkLstForAccounts.isEmpty()){
            for(ContentDocumentLink linkObjForAccounts : contentDocLinkLstForAccounts){
                 notesIdLstForAccounts.add(linkObjForAccounts.contentDocumentId);
                 lstCalllingToNotesWrapperForAccounts.add(new CalllingToNotesWrapper('Account',
                                                                                     AccountList[0],
                                                                                     null,
                                                                                     null,
                                                                                     linkObjForAccounts.contentDocumentId));
            }
        }
        if(contentDocLinkLstForCalling != null && !contentDocLinkLstForCalling.isEmpty()){
            for(ContentDocumentLink linkObjForCalling : contentDocLinkLstForCalling){
                notesIdLstForCalling.add(linkObjForCalling.contentDocumentId);
                lstCalllingToNotesWrapperForCalling.add(new CalllingToNotesWrapper('Calling List',
                                                                                   callingObj,
                                                                                    null,
                                                                                    null,
                                                                                    linkObjForCalling.contentDocumentId));
           }
        }
        if(notesIdLstForAccounts != null && !notesIdLstForAccounts.isEmpty()){
          IdToNoteMapForAccounts = new Map<Id,ContentNote>([SELECT Id,
                                                                 Title,
                                                                 CreatedDate,
                                                                 OwnerId, 
                                                                 Owner.Name,
                                                                 TextPreview 
                                                            FROM ContentNote 
                                                           WHERE Id 
                                                              IN :notesIdLstForAccounts
                                                        ORDER BY CreatedDate 
                                                            desc
                                                           limit 950]);
        }
         if(notesIdLstForCalling != null && !notesIdLstForCalling.isEmpty()){
           IdToNoteMapForCalling = new Map<Id,ContentNote>([SELECT  Id,
                                                                    Title,
                                                                    CreatedDate,
                                                                    TextPreview,
                                                                    OwnerId, 
                                                                    Owner.Name 
                                                               FROM ContentNote 
                                                              WHERE Id 
                                                                 IN :notesIdLstForCalling
                                                           ORDER BY CreatedDate 
                                                                desc
                                                               limit 950]);
         }
         if(!lstCalllingToNotesWrapperForAccounts.isEmpty()){     
            for(Integer index = 0; index < lstCalllingToNotesWrapperForAccounts.size(); index ++) {
                 if(IdToNoteMapForAccounts.containsKey(lstCalllingToNotesWrapperForAccounts[index].NoteId)){
                    lstCalllingToNotesWrapperForAccounts[index].NoteLst = IdToNoteMapForAccounts.get(lstCalllingToNotesWrapperForAccounts[index].NoteId);
                 }
            }
         }
         if(!lstCalllingToNotesWrapperForCalling.isEmpty()){
            for(Integer index = 0; index < lstCalllingToNotesWrapperForCalling.size(); index ++) {
                 if(IdToNoteMapForCalling.containsKey(lstCalllingToNotesWrapperForCalling[index].NoteId)){
                    lstCalllingToNotesWrapperForCalling[index].NoteLst = IdToNoteMapForCalling.get(lstCalllingToNotesWrapperForCalling[index].NoteId);
                 }
            }      
         }                                                                                                                                    
    }
     public class CalllingToNotesWrapper{
      //  public Case caseObj{get;set;}
        public Account accObj{get;set;}
        public Calling_List__c callObj{get;set;}
        public ContentNote NoteLst{get;set;}
        public Id NoteId{get;set;}
         public Note NoteListToMove {get;set;} 
        public String sobjectName {get;set;}
        public CalllingToNotesWrapper(String sobjectNameW, Calling_List__c callObjW,ContentNote NoteLstW,Note NoteListToMoveW ,Id NoteIdW){
            sobjectName = sobjectNameW;
            callObj = callObjW;
            NoteLst = NoteLstW;
            NoteListToMove = NoteListToMoveW;
            NoteId = NoteIdW;
        }
       
        public CalllingToNotesWrapper( String sobjectNameW,Account accObjW,ContentNote NoteLstW,Note NoteListToMoveW,Id NoteIdW){
            sobjectName = sobjectNameW;
            accObj = accObjW;
            NoteLst = NoteLstW;
            NoteListToMove = NoteListToMoveW;
            NoteId = NoteIdW;
        }
        
    }
}