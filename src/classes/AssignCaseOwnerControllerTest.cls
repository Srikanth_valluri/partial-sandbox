@isTest
private class AssignCaseOwnerControllerTest{
   @testSetup 
   static void testData(){
     List<Case> callingLst = new List<Case>();
        Account accObj1 = new Account(Name = 'Miss. Madina Alieva');
        insert accObj1;
        Account accObj2 = new Account(Name = 'Miss. Madina Alieva Test');
        insert accObj2;
     Case callObj1 = new Case(AccountId = accObj1.Id, Origin = 'Email', Priority = 'Medium',
                              RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Email').getRecordTypeId());
         callingLst.add(callObj1);
        Case callObj2 = new Case(AccountId = accObj2.Id, Origin = 'Email', Priority = 'Medium',
                              RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Email').getRecordTypeId());
         callingLst.add(callObj2);
         insert callingLst;
    }
    static testMethod void changeOwnerTest(){
        List<Case> callObjInstLst = [SELECT Id,
                                              AccountId
                                         FROM Case
                                         LIMIT 2];
        System.debug('inside test class:callObjInst::'+callObjInstLst);
       // System.assertNotEquals(null,callObjInstLst.Id);
        Test.startTest();
            PageReference changeOwnerPage = Page.AssignCaseOwner;
            Test.setCurrentPage(changeOwnerPage);
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(callObjInstLst);
            AssignCaseOwnerController controllerObj = new AssignCaseOwnerController(sc);
            controllerObj.saveOwner();
        Test.stopTest();
        //controllerObj.fetchCases();
    }

    static testMethod void transferFromQueueToQueueTest() {
        User creUser = createCREUsers('creUser7000@test.com', '7000');
        Group unregisteredQueue = createQueue('unregisteredQueue');
        Group contactCenterQueue = createQueue('contactCenterQueue');
        Case caseObj = new Case(
            Origin = 'Email',
            Priority = 'Medium',
            RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Email').getRecordTypeId(),
            OwnerId = unregisteredQueue.Id
        );
        insert caseObj;
        caseObj.OwnerId = contactCenterQueue.Id;
        Test.startTest();
            PageReference changeOwnerPage = Page.AssignCaseOwner;
            Test.setCurrentPage(changeOwnerPage);
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(new List<Case> {caseObj});
            AssignCaseOwnerController controllerObj = new AssignCaseOwnerController(sc);
            System.runAs(creUser) {
                controllerObj.saveOwner();
            }
        Test.stopTest();
    }


    public static Group createQueue(String queueName) {
        QueuesObject queue;
        Group groupInstance;
        System.runAs(new User(Id = UserInfo.getUserID())) {
            groupInstance = new Group(
                Name = queueName,
                Type = 'Queue'
            );
            insert groupInstance;
            queue = new QueueSObject(QueueID = groupInstance.Id, SobjectType = 'Case');
            insert queue;
        }
        return groupInstance;

    }

    public static User createCREUsers(String creUserName, String extension) {
        User creUser;
        System.runAs(new User(Id = UserInfo.getUserID())) {
             creUser = new User(
                Username = creUserName,
                Extension = extension,
                Alias = 'TestCRE',
                Email = creUserName,
                Emailencodingkey = 'UTF-8',
                Lastname = 'Test CRE',
                Languagelocalekey = 'en_US',
                Localesidkey = 'en_US',
                Profileid = [Select Id From Profile Where Name='Contact Center - Manager'].Id,
                Country = 'United Arab Emirates',
                IsActive = true,
                Timezonesidkey='America/Los_Angeles'
            );
            insert creUser;
        }
        return creUser;
    }
}