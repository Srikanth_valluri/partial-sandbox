/*
* Description - Class used as a controller for RP Termination component.
*
* Version            Date            Author            Description
* 1.0                                Ashish           Initial Draft
*/

public virtual with sharing class RentalPoolTerminationController {
    
    public Id accountId             {get;set;}
    public Id bookingUnitId                 {get;set;}
    public Id deleteAttRecId {get;set;}
    public WrapperBookingUnit objUnitWrap   {get;set;}
    public list<Buyer__c> lstJointBuyer     {get;set;}
    public list<Case> lstOpenCases { get; set; }
    public list<Case> lstClosedCases { get; set; }
    public SR_Attachments__c objCRFAttach { get; set; }
    public SR_Attachments__c objPOAAttach { get; set; }
    public SR_Attachments__c objRPAttach { get; set; }
    public boolean isAllowed { get; set; }
    public String strSrType { get; set; }
    public String strSelectedSRType         {get;set;}
    public String strFMCharges { get; set; }
    public String strNumberOfUnitsInPool { get; set; }
    public Case objCase { get; set; }
    public Case objAgreementCase { get; set; }
    public transient String strDisplayMessage { get; set; }
    public transient String crfAttachmentBody { get; set; }
    public transient String crfAttachmentName { get; set; }
    public transient String poaAttachmentBody { get; set; }
    public transient String poaAttachmentName { get; set; }
    public transient String rpAgreementName { get; set; }
    public transient String rpAgreementBody { get; set; }
    public transient String strDocUrl { get; set; }
    
    
    @testVisible private map<Id, Booking_Unit__c> mapAllBookingUnits;
    @testVisible private Booking_Unit__c selectedUnit ;
    @testVisible private String strCaseID ;
    @testVisible private Boolean isNewCase ;
    @testVisible private Boolean isSuccessfull ;
    @testVisible private Boolean isRequestFromDetailPage ;
    
    public RentalPoolTerminationController() {
        isSuccessfull = false ;
        isRequestFromDetailPage = false ;
        if( String.isNotBlank( ApexPages.currentPage().getParameters().get('AccountID') ) &&
            String.isNotBlank( ApexPages.currentPage().getParameters().get('caseID') ) &&
            String.isNotBlank( ApexPages.currentPage().getParameters().get('SRType') ) && 
            ApexPages.currentPage().getParameters().get('SRType').equalsIgnoreCase( 'RentalPoolTermination' ) ) {
            isRequestFromDetailPage = true ;
            strCaseID = ApexPages.currentPage().getParameters().get('caseID') ;
            initializeRequest();
        }
    }
    
    //Getter for the list of Booking Units
    public list<SelectOption> getlstBookingUnits() {
        if( accountId != null && String.isNotBlank( strSelectedSRType ) && strSelectedSRType.equalsIgnoreCase('RentalPoolTermination') ) {
            mapAllBookingUnits = RentalPoolTerminationUtility.getAllRelatedBookingUnits( accountId );
            if( mapAllBookingUnits != null && !mapAllBookingUnits.isEmpty() ) {
                list<SelectOption> lstBooking = new list<SelectOption>();
                lstBooking.add( new SelectOption( '' ,'--NONE--' ) );
                for( Booking_Unit__c objUnit : mapAllBookingUnits.values() ) {
                    if( String.isNotBlank( objUnit.Unit_Name__c ) ) {
                        lstBooking.add( new SelectOption( objUnit.Id ,objUnit.Unit_Name__c ) );
                    }
                }
                strNumberOfUnitsInPool = String.valueOf( lstBooking.size() - 1 );
                return lstBooking ;
            }
            else if( String.isNotBlank( strCaseID ) && objCase != null && String.isNotBlank( objCase.Booking_Unit__c ) ) {
              Booking_Unit__c objUnit = RentalPoolTerminationUtility.getUnitDetails( objCase.Booking_Unit__c );
              list<SelectOption> lstBooking = new list<SelectOption>();
                lstBooking.add( new SelectOption( '' ,'--NONE--' ) );
                if( String.isNotBlank( objUnit.Unit_Name__c ) ) {
                    lstBooking.add( new SelectOption( objUnit.Id ,objUnit.Unit_Name__c ) );
                }
                strNumberOfUnitsInPool = String.valueOf( lstBooking.size() - 1 );
                return lstBooking ;
            }
        }
        return null;
    }
    
    //Getting Booking Unit Details
    public void getBookinUnitDetails() {
        selectedUnit = selectedUnit != null ? selectedUnit : mapAllBookingUnits.get( bookingUnitId );
        objUnitWrap = PenaltyWaiverService.getBookingUnitDetails( selectedUnit );
        strFMCharges = RentalPoolTerminationService.fetchFMCharges( selectedUnit );
        PenaltyWaiverHelper.CaseListWrapper objInfoWrap = RentalPoolTerminationHelper.getCaseAndUnitDetailsWrapper( selectedUnit, 
                                                                                                            new set<String>{ 'AOPT', 
                                                                                                                             'Early_Handover', 
                                                                                                                             'Fund_Transfer', 
                                                                                                                             'Handover', 
                                                                                                                             'Rental_Pool_Agreement', 
                                                                                                                             'Rental_Pool_Assignment', 
                                                                                                                             'Rental_Pool_Termination', 
                                                                                                                             'Utility_Registration_SR', 
                                                                                                                             'Plot_Handover' } );
        if( selectedUnit != null && selectedUnit.Booking__c != null ) {
            lstJointBuyer = PenaltyWaiverUtility.getAllJointBuyer( selectedUnit.Booking__c );
        }
        if( objUnitWrap != null && objInfoWrap != null ) {
            if( objUnitWrap.objBookingUnit != null && objUnitWrap.objBookingUnit.Options__r != null && 
                !objUnitWrap.objBookingUnit.Options__r.isEmpty() && objUnitWrap.objIPMSDetailsWrapper != null ) {
                objUnitWrap.objIPMSDetailsWrapper.strPromotion = objUnitWrap.objBookingUnit.Options__r[0].PromotionName__c ;
                objUnitWrap.objIPMSDetailsWrapper.strScheme = objUnitWrap.objBookingUnit.Options__r[0].SchemeName__c ;
                objUnitWrap.objIPMSDetailsWrapper.strOption = objUnitWrap.objBookingUnit.Options__r[0].OptionsName__c ;
                objUnitWrap.objIPMSDetailsWrapper.strCampaign = objUnitWrap.objBookingUnit.Options__r[0].CampaignName__c ;
            }
            if( objUnitWrap.objBookingUnit != null && objUnitWrap.objBookingUnit.Payment_Plans__r != null && !objUnitWrap.objBookingUnit.Payment_Plans__r.isEmpty() ) {
                objUnitWrap.objIPMSDetailsWrapper.objPlan = objUnitWrap.objBookingUnit.Payment_Plans__r[0];
            }
            lstOpenCases = objInfoWrap.lstOpenCase;
            lstClosedCases = objInfoWrap.lstCloseCase ;
            isAllowed = String.isNotBlank( strCaseID ) ? true : objInfoWrap.isAllowedToOpenPenaltyCase ;
            strSrType = objInfoWrap.strOpenSrType ;
            objAgreementCase = objInfoWrap.caseobj ;
        }
        
        if( String.isBlank( strCaseID ) ) {
            objCase = new Case();
            objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId();
            if( !test.isRunningTest() ) {
                insert objCase;
                objCase = RentalPoolTerminationUtility.getCaseDetails( objCase.Id );
            }
            
        }
    }
    
    public void generateCRForm() { 
        strDocUrl = null ;
        if( selectedUnit != null && objUnitWrap.objIPMSDetailsWrapper != null ) {
            try {
                String strResponse = RentalPoolTerminationService.generateCustomerRequestForm( selectedUnit, objUnitWrap.objIPMSDetailsWrapper );
                if( String.isNotBlank( strResponse ) ) {
                    strDocUrl = strResponse ;
                }
                else {
                    Error_Log__c objErr = new Error_Log__c();
                    objErr.Account__c = accountId;
                    objErr.Booking_Unit__c = selectedUnit.Id;
                    objErr.Case__c = objCase.Id;
                    objErr.Process_Name__c = 'Rental Pool Termination';
                    objErr.Error_Details__c = 'No CRF link returned... ';
                    insert objErr;
                    
                    strDisplayMessage = PenaltyWaiverUtility.SPAN_CROSS + ' No CRF link returned...';
                }
            }
            catch( Exception e ) {
                strDisplayMessage = PenaltyWaiverUtility.SPAN_CROSS + e.getMessage();
            } 
        }
        else {
            strDisplayMessage = PenaltyWaiverUtility.SPAN_CROSS + ' The selected unit dosent have a Registration Id associated with it.';
        }
    }
    
    public void insertStatementOfAccount() {
        strDocUrl = null ;
        if( String.isNotBlank( selectedUnit.Registration_ID__c ) ) {
            try {
                GenerateSOAController.soaResponse objSOAResponse = GenerateSOAController.getSOADocument( selectedUnit.Registration_ID__c );
                if( objSOAResponse != null && String.isNotBlank( objSOAResponse.Status ) && objSOAResponse.Status.equalsIgnoreCase('S') &&
                    String.isNotBlank( objSOAResponse.PROC_STATUS ) && objSOAResponse.PROC_STATUS.equalsIgnoreCase('S') && 
                    String.isNotBlank( objSOAResponse.url ) ) {
                    strDocUrl = objSOAResponse.url;
                }
                else if(  objSOAResponse != null && String.isNotBlank( objSOAResponse.Status ) && String.isNotBlank( objSOAResponse.PROC_STATUS ) &&
                          ( !objSOAResponse.Status.equalsIgnoreCase('S') || !objSOAResponse.PROC_STATUS.equalsIgnoreCase('S') ) ) {
                    Error_Log__c objErr = new Error_Log__c();
                    objErr.Account__c = accountId;
                    objErr.Booking_Unit__c = selectedUnit.Id;
                    objErr.Case__c = objCase.Id;
                    objErr.Process_Name__c = 'Rental Pool Termination';
                    objErr.Error_Details__c = objSOAResponse.PROC_MESSAGE;
                    insert objErr ;
                    
                    strDisplayMessage = PenaltyWaiverUtility.SPAN_CROSS + ' ' + objSOAResponse.PROC_MESSAGE ;
                }
            }
            catch( Exception e ) {
                strDisplayMessage = PenaltyWaiverUtility.SPAN_CROSS + e.getMessage();
            }   
        }
        else {
            strDisplayMessage = PenaltyWaiverUtility.SPAN_CROSS + ' The selected unit dosent have a Registration Id associated with it.';
        }
    }
    
    public void fetchRPAgreement() { 
        if( objAgreementCase != null && objAgreementCase.Id != null ) {
            SR_Attachments__c objNewAttach ;
            for( SR_Attachments__c objAttach : RentalPoolTerminationUtility.getCaseAttachments( objAgreementCase.Id ) ) {
                if( String.isNotBlank( objAttach.Name ) && objAttach.Name.containsIgnoreCase( Label.RP_Agreement_Name ) ) {
                    objNewAttach = objAttach.clone( false, true, false, false );
                    objNewAttach.Case__c = objCase.Id ;
                    objNewAttach.Type__c = Label.RP_Agreement;
                    objNewAttach.Account__c = accountId ;
                    break;
                }
            }
            try {
                if( objNewAttach != null ) {
                    insert objNewAttach ;
                    strDisplayMessage = PenaltyWaiverUtility.SPAN_TICK + ' RP Agreement attached successfully.';
                }
                else {
                    strDisplayMessage = PenaltyWaiverUtility.SPAN_CROSS + ' No RP Agreement document found.';
                }
            }
            catch( Exception e ) {
                strDisplayMessage = PenaltyWaiverUtility.SPAN_CROSS + ' ' + e.getMessage();
            }
        }
        else {
            strDisplayMessage = PenaltyWaiverUtility.SPAN_CROSS + ' There is no RP Agreement case under this unit.';
        }
    }
    
    public void initializeRequest() {
        objCase = RentalPoolTerminationUtility.getCaseDetails( ApexPages.currentPage().getParameters().get('caseID') );
        bookingUnitId = objCase.Booking_Unit__c;
        accountId = objCase.AccountId ;
        selectedUnit = RentalPoolTerminationUtility.getUnitDetails( bookingUnitId ) ;
        getBookinUnitDetails();
        getAttachments( objCase.Id );
    }
    
    public void initiateTerminationSR() {
        UploadMultipleDocController.data objResponse = uploadDoc();
        if( objResponse != null ) {
            if( String.isNotBlank( objResponse.status ) && objResponse.status.equalsIgnoreCase('S')
                && objResponse.data != null && objResponse.data.size() > 0  ) {
                insertCustomAttachment( objCase.Id, objResponse );
                isSuccessfull = true ;
            }
            else {
                isSuccessfull = false ;
                strDisplayMessage = PenaltyWaiverUtility.SPAN_CROSS ;
                strDisplayMessage += String.isNotBlank( objResponse.message ) ? ' '+objResponse.message : '';
                strDisplayMessage += objResponse.data != null && objResponse.data.size() > 0 ? ' ' + objResponse.data[0].PROC_MESSAGE : '' ;
                
                //Generate Error Log
                Error_Log__c objErr = new Error_Log__c();
                objErr.Account__c = accountId;
                objErr.Booking_Unit__c = selectedUnit.Id;
                objErr.Case__c = objCase.Id;
                objErr.Process_Name__c = 'Rental Pool Termination';
                objErr.Error_Details__c = objResponse.message + '---' + objResponse.data[0].PROC_MESSAGE ;
                insert objErr ;
            }
        }
        else {
            isSuccessfull = true ;
        }
        
        if( isSuccessfull ) {
            mapCaseData();
            objCase.Status = 'New' ;
            objCase.Rental_Pool_Termination_Status__c = 'Draft';
            try {
                upsert objCase ; 
                strDisplayMessage = PenaltyWaiverUtility.SPAN_TICK + ' SR has been saved successfully..';
            }
            catch( Exception e ) {
                strDisplayMessage = PenaltyWaiverUtility.SPAN_CROSS + ' ' + e.getMessage() ;
            }
        }
    }
    
    public pageReference submitTerminationSR() {
        //Uploading doc on central repo.
        UploadMultipleDocController.data objResponse = uploadDoc();
        
        if( objResponse != null ) {
            if( String.isNotBlank( objResponse.status ) && objResponse.status.equalsIgnoreCase('S')
                && objResponse.data != null && objResponse.data.size() > 0  ) {
                insertCustomAttachment( objCase.Id, objResponse );
                isSuccessfull = true ;
            }
            else {
                isSuccessfull = false ;
                strDisplayMessage = PenaltyWaiverUtility.SPAN_CROSS ;
                strDisplayMessage += String.isNotBlank( objResponse.message ) ? ' '+objResponse.message : '';
                strDisplayMessage += objResponse.data != null && objResponse.data.size() > 0 ? ' ' + objResponse.data[0].PROC_MESSAGE : '' ;
                
                //Generate Error Log
                Error_Log__c objErr = new Error_Log__c();
                objErr.Account__c = accountId;
                objErr.Booking_Unit__c = selectedUnit.Id;
                objErr.Case__c = objCase.Id;
                objErr.Process_Name__c = 'Rental Pool Termination';
                objErr.Error_Details__c = objResponse.message + '---' + objResponse.data[0].PROC_MESSAGE ;
                insert objErr ;
            }
        }
        else {
            isSuccessfull = true ;
        }
        
        if( isSuccessfull ) {
            mapCaseData();
            objCase.Status = 'Submitted' ;
            objCase.Rental_Pool_Termination_Status__c = 'In Notice Period';
            objCase.Approval_Status__c = 'Pending';
            objCase.Termination_Date__c = system.today().addyears(1);
            try {
                upsert objCase ; 
                strDisplayMessage = PenaltyWaiverUtility.SPAN_TICK + ' SR has been saved successfully..';
                PageReference objPage = new PageReference('/'+objCase.Id);
                objPage.setRedirect( true );
                createTaskForCRE();
                //fetchRPAgreement();
                return objPage ; 
            }
            catch( Exception e ) {
                strDisplayMessage = PenaltyWaiverUtility.SPAN_CROSS + ' ' + e.getMessage() ;
            }
        }
        return null ;
    }
    
    @testVisible 
    private UploadMultipleDocController.data uploadDoc() {      
        
        UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
        list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
        if( String.isNotBlank( crfAttachmentBody ) && String.isNotBlank( crfAttachmentName ) && objCRFAttach == null) {
            lstWrapper.add( PenaltyWaiverHelper.makeWrapperObject( EncodingUtil.Base64Encode( RentalPoolTerminationHelper.extractBody( crfAttachmentBody ) ) ,
                                                                   PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF ,
                                                                   RentalPoolTerminationHelper.extractName( crfAttachmentName ) ,
                                                                   selectedUnit.Registration_Id__c, objCase.CaseNumber, '1' ) );
                
        }
        if( String.isNotBlank( poaAttachmentBody ) && String.isNotBlank( poaAttachmentName ) && objPOAAttach == null) {
            lstWrapper.add( PenaltyWaiverHelper.makeWrapperObject( EncodingUtil.Base64Encode( RentalPoolTerminationHelper.extractBody( poaAttachmentBody ) ) ,
                                                                   PenaltyWaiverUtility.ATTACH_TYPE_FULL_POA ,
                                                                   RentalPoolTerminationHelper.extractName( poaAttachmentName ) ,
                                                                   selectedUnit.Registration_Id__c, objCase.CaseNumber, '2' ) ) ;
        }
        if( String.isNotBlank( rpAgreementBody ) && String.isNotBlank( rpAgreementName ) && objRPAttach == null ) {
            lstWrapper.add( PenaltyWaiverHelper.makeWrapperObject( EncodingUtil.Base64Encode( RentalPoolTerminationHelper.extractBody( rpAgreementBody ) ) ,
                                                                   'RP Agreement',
                                                                   RentalPoolTerminationHelper.extractName( rpAgreementName ) ,
                                                                   selectedUnit.Registration_Id__c, objCase.CaseNumber, '3' ) ) ;
        }
        if( !lstWrapper.isEmpty() ) {
            objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper );
        }
        else {
            objResponse = null ;
        }
        return objResponse;
    }
    
    public void deleteAttachment() {
        /*SR_Attachments__c objCustAttach = mapSrAttach != null && mapSrAttach.containsKey( deleteAttRecId ) ?
                                          mapSrAttach.get( deleteAttRecId ) :
                                          null ;
        if( objCustAttach != null ) {
            if( objCustAttach.Type__c.equalsIgnoreCase( PenaltyWaiverUtility.ATTACH_TYPE_FULL_POA ) ) {
                objCase.Is_POA_Uploaded__c = false ;
                objPOAAttach = null ;
            }
            else if( objCustAttach.Type__c.equalsIgnoreCase( PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF ) ) {
                objCase.Is_CRF_Uploaded__c = false;
                objCRFAttach = null ;
            }
            
            try{
                strDisplayMessage = PenaltyWaiverUtility.SPAN_TICK +' Document for '+ objCustAttach.Type__c +' deleted successfully.';
                update objCase ;
                delete objCustAttach ;
            }
            catch( Exception e ) {
                strDisplayMessage = PenaltyWaiverUtility.SPAN_CROSS + ' Exception generated while deleting the attachment '+e.getMessage() ;
                system.debug( 'Exception generated while deleting the attachment '+e.getMessage() );
            }
        }*/
    }
    
    @testVisible 
    private void insertCustomAttachment( Id caseId, UploadMultipleDocController.data objResponse ) {
        list<SR_Attachments__c> lstCustomAttachments = new list<SR_Attachments__c>();
        for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            
            SR_Attachments__c objCustAttach = new SR_Attachments__c();
            objCustAttach.Account__c = accountId ;
            objCustAttach.Attachment_URL__c = objFile.url;
            objCustAttach.Booking_Unit__c = bookingUnitId ;
            objCustAttach.Case__c = caseId ;
            if( String.isNotBlank( objFile.PARAM_ID ) && 
                String.isNotBlank( RentalPoolTerminationHelper.extractName( crfAttachmentName ) ) && 
                objFile.PARAM_ID.contains( RentalPoolTerminationHelper.extractName( crfAttachmentName ) ) ) {
                objCustAttach.Type__c = PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF ;
                objCustAttach.Name = RentalPoolTerminationHelper.extractName( crfAttachmentName ) ;
                objCustAttach.isValid__c = true ;
            }
            else if( String.isNotBlank( objFile.PARAM_ID ) && 
                     String.isNotBlank( RentalPoolTerminationHelper.extractName( poaAttachmentName ) ) && 
                     objFile.PARAM_ID.contains( RentalPoolTerminationHelper.extractName( poaAttachmentName ) ) ) {
                objCustAttach.Type__c = PenaltyWaiverUtility.ATTACH_TYPE_FULL_POA ;
                objCustAttach.Name = RentalPoolTerminationHelper.extractName( poaAttachmentName ) ;
                objCustAttach.isValid__c = objCase.Is_POA_Verified__c ;
            }
            else if( String.isNotBlank( objFile.PARAM_ID ) && 
                     String.isNotBlank( RentalPoolTerminationHelper.extractName( rpAgreementName ) ) &&
                     objFile.PARAM_ID.contains( RentalPoolTerminationHelper.extractName( rpAgreementName ) ) ) {
                objCustAttach.Type__c = Label.RP_Agreement ;
                objCustAttach.Name = RentalPoolTerminationHelper.extractName( rpAgreementName ) ;
                objCustAttach.isValid__c = true ;
            }
            else {
                objCustAttach.Type__c = 'Other';
            }
            lstCustomAttachments.add( objCustAttach );  
        }
        if( !lstCustomAttachments.isEmpty() ) { 
            insert lstCustomAttachments ;
            getAttachments( objCase.Id ) ;
            crfAttachmentName = '';
            crfAttachmentBody = '';
            poaAttachmentName = '';
            poaAttachmentBody = '';
            rpAgreementName = '' ;
            rpAgreementBody = '' ; 
        }
    }
    
    @testVisible 
    private void getAttachments( Id caseId ) {
        for( SR_Attachments__c objAttach : RentalPoolTerminationUtility.getCaseAttachments( caseId ) ) {
            if( String.isNotBlank( objAttach.Type__c ) ) {
                if( objCRFAttach == null && objAttach.Type__c.equalsIgnoreCase( PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF ) ) {
                    objCRFAttach = objAttach ;
                }
                else if( objPOAAttach == null && objAttach.Type__c.equalsIgnoreCase( PenaltyWaiverUtility.ATTACH_TYPE_FULL_POA ) ) {
                    objPOAAttach = objAttach ;
                }
                else if( objRPAttach == null && objAttach.Type__c.equalsIgnoreCase( Label.RP_Agreement ) ) {
                    objRPAttach = objAttach ;
                }
                else if( objCRFAttach != null && objPOAAttach != null && objRPAttach != null ) {
                    break;
                }
            }
        }
    }
    
    @testVisible 
    private void mapCaseData() {
        if( objCase != null ) {
            objCase.AccountId = accountId;
            objCase.Booking_Unit__c = bookingUnitId ;
            objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId();
            objCase.SR_Type__c = 'Rental Pool Termination';
            objCase.Origin = 'Walk-In';
            objCase.Subject = 'Rental Pool Termination';
            objCase.Account_Email__c = selectedUnit.Booking__r.Account__r.IsPersonAccount ? 
                                       selectedUnit.Booking__r.Account__r.Email__pc : 
                                       selectedUnit.Booking__r.Account__r.Email__c ;
        }
    }
    
    @testVisible 
    private void createTaskForCRE() {       
        Task objTask = RentalPoolTerminationHelper.createTaskObject( 'CRE', 'Normal', 'Rental Pool Termination', 'Pending',
                                    'Collect Rental Pool Agreement(RPA) from customer', objCase, selectedUnit.CurrencyIsoCode );
        insert objTask ;
    }
}