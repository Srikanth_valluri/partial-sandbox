public class DealExceptionUnitTriggerHandler {
    
    //public static void beforeHandler(list<Deal_Exception_Unit__c> lstNew, map<id, Deal_Exception_Unit__c> mpOld, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUndelete){
        
    //}
    
    public static void afterHandler(list<Deal_Exception_Unit__c> lstNew, map<id, Deal_Exception_Unit__c> mpOld, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUndelete){
        set<Id> inventoryIds = new set<Id>();
        for(Deal_Exception_Unit__c unit : lstNew){
            if(unit.inventory__c != null && unit.Project_Cost__c == null)
                inventoryIds.add(unit.Id);
        }
        
        if(!inventoryIds.isEmpty() && !System.isFuture() && !System.isBatch())
             getCostsAsync(inventoryIds);
    }
    
    @future(callout=true)
    public static void getCostsAsync(set<Id> inventoryIds){
        getCostsSync(inventoryIds);
    }
    
    public static void getCostsSync(set<Id> inventoryIds){
        DAMAC_Central_Push_Notifications__c credentials = DAMAC_Central_Push_Notifications__c.getInstance(UserInfo.getUserID());
        string accessToken = getAccessToken(credentials);
        if(accessToken != ''){
            list<Deal_Exception_Unit__c> inventories = [Select id, Inventory__r.Conn_Project_Code__c, Area_SFT__c, Inventory__r.Building_Code__c from Deal_Exception_Unit__c where id in: inventoryIds];
            map<id, Deal_Exception_Unit__c> mpDEU = new map<id, Deal_Exception_Unit__c>();
            if(!inventories.isEmpty()){
                set<string> projectCodes = new set<string>();
                for(Deal_Exception_Unit__c inv: inventories){
                    mpDEU.put(inv.id, inv);
                    projectCodes.add(inv.Inventory__r.Conn_Project_Code__c+'_'+inv.Inventory__r.Building_Code__c);
                }
                list<string> projectCodeLst = new list<string>();
                projectCodeLst.addAll(projectCodes);
                string body = string.join(projectCodeLst,',');
                System.debug('>>>>>>>>>>>>'+body);
                HTTPResponse res = getProjectCost(credentials, accessToken, body);
                
                if(res.getStatusCode() == 200){
                    ResponseJSON parsed = parse(res.getBody());
                    System.debug('>>>>>>parsed>>>>>>>>'+parsed);
                    list<Deal_Exception_Unit__c> lstDEUnits = new list<Deal_Exception_Unit__c>();
                   
                    for(Id unitId : inventoryIds){
                        Deal_Exception_Unit__c deu = mpDEU.get(unitId);
                        if(!parsed.data.costDetails.data.isEmpty()){
                            deu.Project_Cost__c = parsed.data.costDetails.data[0].totalCost;
                            deu.Project_Cost_per_SQFT__c = parsed.data.costDetails.data[0].totalCost_Psf;
                            if(deu.Project_Cost_per_SQFT__c != null && deu.Area_SFT__c != null){
                                deu.Base_Cost__c = deu.Project_Cost_per_SQFT__c*deu.Area_SFT__c;
                            }
                            lstDEUnits.add(deu);
                        }
                    }
                    
                    if(!lstDEUnits.isEmpty())
                        update lstDEUnits;
                }
                
            }
        }
    }
    
    public static string getAccessToken(DAMAC_Central_Push_Notifications__c credentials){
        String loginRequestBody = Damac_PushNotifications.loginRequestBody(credentials);
        String accessToken = '';
        HTTPResponse loginResponse = Damac_PushNotifications.login(credentials, loginRequestBody);
        System.debug(loginResponse.getBody());
        return loginResponse.getStatusCode() == 200 ?Damac_PushNotifications.loginResponseBody(loginResponse.getBody()) : '';
    }
    
    public static HTTPResponse getProjectCost(DAMAC_Central_Push_Notifications__c credentials, string accessToken, string requestBody){
        System.debug('>>>>>>credentials>>>>>>>>>'+credentials);
        System.debug('>>>>>>accessToken>>>>>>>>>'+accessToken);
        
        HTTPRequest req = new HTTPRequest();
        ResponseJSON obj = new ResponseJSON();
        req.setEndpoint(credentials.API_URL__c + 'ProjectConnect/GetProjectTotalCost?filterby='+requestBody);
        req.setHeader('Content-Type', 'application/json');
        System.debug(credentials.Project_connect_API_Token__c+'>>>>>>>>>>>>'+accessToken);
        req.setHeader('Api-Token', credentials.Project_connect_API_Token__c);
        req.setHeader('Access-Token', accessToken);
        req.setMethod('GET');
        //req.setBody(requestBody);
        req.setTimeOut(120000);
        HTTP http = new HTTP();
        HTTPResponse res = new HTTPResponse();
        if(!test.isRunningTest())
            res = http.send(req);
        else{
            res.setStatusCode(200);
            res.setbody('{"data":{"costDetails":{"data":[{"projectNumber":"P021.40","cluster_Code":"VET","totalCost":214.53,"totalCost_Psf":563.13}]}},"meta_data":""}');
        }
        return res;
    }
    
    public class ResponseJSON{
        public actualData data;
        public String meta_data;
    }
    public class costDetailsJSON {
        public list<projectdata> data;
    }
    
    public class actualData {
        public costDetailsJSON costDetails;
    }
    public class projectdata {
        public String projectNumber;    //P021.05
        public String cluster_Code; //LR1B
        public Double totalCost;    //75.54
        public Double totalCost_Psf;    //869.66
    }
    public static ResponseJSON parse(String json){
        return (ResponseJSON) System.JSON.deserialize(json, ResponseJSON.class);
    }
}