/* 

/**********************************************************************************************************************
Description: This service is used for getting the available slots for booking an appointment for Unit Inspection.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By   | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   8-08-2020       |   Jyoti Aher   |   1. Initial implementation to return available Appointment Dates on Mobile App

1.1     |   18-08-2020      |   Jyoti Aher   |   2. Implementation to return Appointment slots based on selected Appointment Date

1.2     |   20-08-2020      |   Jyoti Aher   |   3. Added accountId and Process parameter in the request implementation. Also, added processName in the implementation.
***********************************************************************************************************************/

@RestResource(urlMapping='/getBookingSlotForUnitInsp/*')
global class HDApp_getBookingSlotForUnitInsp_API {
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Bad Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };
    
    @HttpGet
    /**********************************************************************************************************************
    Description : Method to get appointment Slots for Booking Unit Inspection
    Parameter(s )  : NA
    Return Type : Wrapper (FinalReturnWrapper) as responnse which contains data and metadata
    **********************************************************************************************************************/
    global static FinalReturnWrapper getBookingSlotForUnit(){
        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_data objData = new cls_data();
        cls_meta_data objMeta = new cls_meta_data();
        List<appointment_slots_Wrapper> lstAppointmentSlots = new List<appointment_slots_Wrapper>();
        
        if(!r.params.containsKey('bookingUnitId')) {
            objMeta = ReturnMetaResponse('Missing parameter : bookingUnitId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;      
        }
        
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : bookingUnitId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        else if(r.params.containsKey('bookingUnitDate') && String.isBlank(r.params.get('bookingUnitDate'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : bookingUnitDate', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        else if(r.params.containsKey('bookingType') && String.isBlank(r.params.get('bookingType'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : bookingType', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        else if(r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : accountId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        else if(r.params.containsKey('process') && String.isBlank(r.params.get('process'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : process', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        if( r.params.containsKey('accountId') && String.isNotBlank(r.params.get('accountId'))
        && r.params.containsKey('bookingUnitId') && String.isNotBlank(r.params.get('bookingUnitId')) 
        && r.params.containsKey('bookingUnitDate') && String.isNotBlank(r.params.get('bookingUnitDate')) 
        && r.params.containsKey('bookingType') && String.isNotBlank(r.params.get('bookingType'))
        && r.params.containsKey('process') && String.isNotBlank(r.params.get('process'))) {
            
            String strSelectedAccount = r.params.get('accountId');
            String bookingUnitId = String.valueOf(r.params.get('bookingUnitId'));
            String bookingType = r.params.get('bookingType');
            String selectedDate = String.valueOf(r.params.get('bookingUnitDate'));
            String processName = r.params.get('process');
            Date fromDate;
            Date toDate;
            System.debug('bookingUnitDate---->'+selectedDate);
            DAMACHolidaySettings__c holidayCustomSettings = HDApp_getAppointmentDetailsHandler.holidayFunction(selectedDate);
            if ( holidayCustomSettings != null ) {
                    fromDate = holidayCustomSettings.From__c;
                    toDate = holidayCustomSettings.To__c;
                    objMeta.message = 'Damac service teams are not working from '+fromDate+
                            ' to '+toDate+ ' .Kindly choose a different date.';
                    objMeta.status_code = 2;
                    objMeta.title = mapStatusCode.get(2);
                    objMeta.developer_message = null;

                    returnResponse.meta_data = objMeta;
                    return returnResponse;
            }else{
               //main logic 
                List<Booking_Unit__c> lstBU = [SELECT id,
                                                    Name  
                                                FROM Booking_Unit__c
                                                WHERE id=: r.params.get('bookingUnitId')];

                System.debug('lstBU:: ' + lstBU);
                if(!lstBU.isEmpty() && lstBU.size() > 0) {
                    list<appointment_slots_Wrapper> returned_SlotsList = new List<appointment_slots_Wrapper>();
                    
                    // retrieving Appointment slots results in Handler wrapper
                    List<HDApp_getAppointmentDetailsHandler.AppointmentWrapper> handlerWrapper = new List<HDApp_getAppointmentDetailsHandler.AppointmentWrapper>();

                    handlerWrapper = HDApp_getAppointmentDetailsHandler.getAvailableAppointments(strSelectedAccount,bookingUnitId, selectedDate, bookingType, processName);
                    
                    for(HDApp_getAppointmentDetailsHandler.AppointmentWrapper wrapperInstance: handlerWrapper){
                        System.debug('Appointment : '+ wrapperInstance.objApp);
                        
                        System.debug('--- obj.ObjApp.Slots__c --- : '+wrapperInstance.ObjApp.Slots__c);
                        Appointment__c objApp = wrapperInstance.objApp;
                        Calling_List__c objCL = wrapperInstance.objCL;
                        String slots = wrapperInstance.ObjApp.Slots__c;
                        
                        appointment_slots_Wrapper objAppSlot = new appointment_slots_Wrapper();
                        
                        if(!String.isBlank(wrapperInstance.ErrorMessage)){
                            //returned response if slots are available
                            objMeta.message = wrapperInstance.ErrorMessage;
                            objMeta.status_code = 2;
                            objMeta.title = mapStatusCode.get(2);
                            objMeta.developer_message = null;
                            
                            returnResponse.meta_data = objMeta;
                            return returnResponse;
                        }

                        /*Logic to split Appointment slot into StartTime and EndTime by Shubham*/
                        objAppSlot.id = wrapperInstance.objApp.id;
                        objAppSlot.start_time = wrapperInstance.ObjApp.Slots__c.split('-')[0].trim();
                        objAppSlot.end_time = wrapperInstance.ObjApp.Slots__c.split('-')[1].trim();
                        lstAppointmentSlots.add(objAppSlot);
                        
                    }
                    System.debug('handlerWrapper'+handlerWrapper);
                    System.debug('lstAppointmentSlots:: ' + lstAppointmentSlots);
                    
                    //returned_SlotsList = (list<appointment_slots_Wrapper>)JSON.deserialize(JSON.serialize(handlerWrapper), List<appointment_slots_Wrapper>.class);
                    //System.debug('returned_SlotsList'+returned_SlotsList);
                    
                    objMeta.message = 'Success';
                    objMeta.status_code = 1;
                    objMeta.title = mapStatusCode.get(1);
                    objMeta.developer_message = null;

                    objData.appointment_slots = lstAppointmentSlots;
                    
                    returnResponse.meta_data = objMeta;
                    returnResponse.data = objData;    //Added by Shubham
                    return returnResponse;
                }
                objMeta.message = 'Missing parameter value : bookingUnitId';
                objMeta.status_code = 3;
                objMeta.title = mapStatusCode.get(2);
                objMeta.developer_message = null;

                returnResponse.meta_data = objMeta;
                return returnResponse;
            }  
        }
        return returnResponse;
    }
    /**********************************************************************************************************************
    Description : Method to process and return the response for the API request
    Parameter(s )  : Message, statusCode
    Return Type : Wrapper (FinalReturnWrapper) as responnse which contains data and metadata
    **********************************************************************************************************************/
    public static cls_meta_data ReturnMetaResponse(String message, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data(); 
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = null;
        
        return retMeta;
            
    }
    /**********************************************************************************************************************
    Description : Wrapper Class to combine data and metadata for API response 
    Parameter(s )  : NA
    Return Type : NA
    **********************************************************************************************************************/
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }
    /**********************************************************************************************************************
    Description : Wrapper Class to combine and return metadata results for API response 
    Parameter(s )  : NA
    Return Type : NA
    **********************************************************************************************************************/
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }
    /**********************************************************************************************************************
    Description : Wrapper Class to combine and return data results for API response
    Parameter(s )  : NA
    Return Type : NA
    **********************************************************************************************************************/
    public class appointment_slots_Wrapper {
        public String id;
        public String start_time;
        public String end_time;
    }
    /**********************************************************************************************************************
    Description : Class contains appointment slots data and return it to FinalReturnWrapper wrapper class for API response
    Parameter(s )  : NA
    Return Type : NA
    **********************************************************************************************************************/
    public class cls_data {
        public appointment_slots_Wrapper[] appointment_slots;
    }
    
}