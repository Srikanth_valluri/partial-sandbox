public class HOPack_WebServiceAdapter extends GenericWebServiceAdapter implements WebServiceAdapter{

    public String url; 
    
    public override WebServiceAdapter call(){
        String regID_BUID = parameters.get('regID_BUID');
        system.debug('!!!!!!!!regID_BUID'+regID_BUID);
        String BUId, regID, StartDate;
        Datetime startTime;
        
        if(String.isNotBlank(regID_BUID)) {
            BUId = regID_BUID.substringBefore('-');
            regID = regID_BUID.substringAfter('-');
        }        
        
        HO_DocGeneration.HO_DocResponse objHOPack =
            HO_DocGeneration.GetHOPack('HO_PACK' , regID );
        System.debug('==objHOPack===' + objHOPack );
        if(objHOPack != NULL && String.isNotBlank(objHOPack.URL)) {
            this.url = objHOPack.URL;
        }
        
        return this;
    }
    
    public override String getResponse(){
        return this.url;
    }    
}