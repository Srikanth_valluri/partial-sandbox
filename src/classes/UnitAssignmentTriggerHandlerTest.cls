@isTest
public class UnitAssignmentTriggerHandlerTest {
    public static testmethod void testMethod1(){
        Inventory__c inventory;
        Unit_Assignment__c uAObj = new Unit_Assignment__c();
        uAObj.Start_Date__c = Date.parse('11/12/17');
        uAObj.End_Date__c =  Date.parse('11/12/18');
        uAObj.Unit_Assignment_Name__c = 'UA-Test';
        uAObj.Expired__c = false;
        uAObj.Reason_For_Unit_Assignment__c = 'Reason of UA';
        uaObj.Status__c = 'PC Submitted';
        insert uAObj;
        

        List<Inventory__c> inventoriesList = new  List<Inventory__c>();
        for(Integer i = 0; i <= 40; i++) {
            inventory = InitialiseTestData.getInventoryDetails('345'+i,'123'+i,'234'+i,908+i,765+i); 
            inventory.Tagged_To_Unit_Assignment__c = TRUE;
            inventory.Is_Assigned__c = false;
            inventory.Status__c = 'Released';
            inventory.Unit_Assignment__c = uAObj.id; 
            inventory.Unit_Type__c = 'HOTEL APARTMENTS';
            inventory.Floor__c = 'Floor';
            inventory.Unit__c = 'Unit';
            inventoriesList.add(inventory);
        }
        insert inventoriesList ;
        System.Test.StartTest();
            uAObj.Active__c = TRUE;
            uaObj.status__c = 'HOD Approved';
            update uaObj;
        System.Test.StopTest();   
    } 
    
    public static testmethod void testMethod2 (){
        Inventory__c inventory;
        Unit_Assignment__c uAObj = new Unit_Assignment__c();
        uAObj.Start_Date__c = Date.parse('11/12/17');
        uAObj.End_Date__c =  Date.parse('11/12/18');
        uAObj.Unit_Assignment_Name__c = 'UA-Test';
        uAObj.Expired__c = false;
        uAObj.Reason_For_Unit_Assignment__c = 'Reason of UA';
        insert uAObj;
        

        List<Inventory__c> inventoriesList = new  List<Inventory__c>();
        for(Integer i = 0; i <= 40; i++) {
            inventory = InitialiseTestData.getInventoryDetails('345'+i,'123'+i,'234'+i,908+i,765+i); 
            inventory.Tagged_To_Unit_Assignment__c = TRUE;
            inventory.Is_Assigned__c = false;
            inventory.Status__c = 'Released';
            inventory.Unit_Assignment__c = uAObj.id; 
            inventory.Unit_Type__c = 'HOTEL APARTMENTS';
            inventory.Floor__c = 'Floor';
            inventory.Unit__c = 'Unit';
            inventoriesList.add(inventory);
        }
        insert inventoriesList ;
        System.Test.StartTest();
            uAObj.Rejected__c = TRUE;
            uaObj.Status__c = 'Rejected';
            update uaObj;
        System.Test.StopTest();   
    } 
    
    public static testmethod void testMethod3 (){
        Inventory__c inventory;
        Unit_Assignment__c uAObj = new Unit_Assignment__c();
        uAObj.Start_Date__c = Date.parse('11/12/17');
        uAObj.End_Date__c =  Date.parse('11/12/18');
        uAObj.Unit_Assignment_Name__c = 'UA-Test';
        uAObj.Expired__c = false;
        uAObj.Reason_For_Unit_Assignment__c = 'Reason of UA';
        uaObj.Status__c = 'HOD Approved';
        uaObj.PC_Approval_Status__c = 'Reassign';
        insert uAObj;
        

        List<Inventory__c> inventoriesList = new  List<Inventory__c>();
        for(Integer i = 0; i <= 40; i++) {
            inventory = InitialiseTestData.getInventoryDetails('345'+i,'123'+i,'234'+i,908+i,765+i); 
            inventory.Tagged_To_Unit_Assignment__c = TRUE;
            inventory.Is_Assigned__c = false;
            inventory.Status__c = 'Released';
            inventory.Unit_Assignment__c = uAObj.id; 
            inventory.Unit_Type__c = 'HOTEL APARTMENTS';
            inventory.Floor__c = 'Floor';
            inventory.Unit__c = 'Unit';
            inventoriesList.add(inventory);
        }
        insert inventoriesList ;
        System.Test.StartTest();
            uAObj.Rejected__c = TRUE;
            
            UAObj.Status__c = 'Vikram Approved';
            UAObj.PC_Approval_Status__c = 'Confirmed';
            update uaObj;
        System.Test.StopTest();   
    }  
    
    public static testmethod void testMethod4 (){
        Inventory__c inventory;
        Unit_Assignment__c uAObj = new Unit_Assignment__c();
        uAObj.Start_Date__c = Date.parse('11/12/17');
        uAObj.End_Date__c =  Date.parse('11/12/18');
        uAObj.Unit_Assignment_Name__c = 'UA-Test';
        uAObj.Expired__c = false;
        uAObj.Reason_For_Unit_Assignment__c = 'Reason of UA';
        uaObj.Status__c = 'HOD Approved';
        uaObj.PC_Approval_Status__c = 'Reassign';
        insert uAObj;
        

        List<Inventory__c> inventoriesList = new  List<Inventory__c>();
        for(Integer i = 0; i <= 40; i++) {
            inventory = InitialiseTestData.getInventoryDetails('345'+i,'123'+i,'234'+i,908+i,765+i); 
            inventory.Tagged_To_Unit_Assignment__c = TRUE;
            inventory.Is_Assigned__c = false;
            inventory.Status__c = 'Released';
            inventory.Unit_Assignment__c = uAObj.id; 
            inventory.Unit_Type__c = 'HOTEL APARTMENTS';
            inventory.Floor__c = 'Floor';
            inventory.Unit__c = 'Unit';
            inventoriesList.add(inventory);
        }
        insert inventoriesList ;
        System.Test.StartTest();
            UAObj.Status__c = 'Vikram Approved';
            UAObj.PC_Approval_Status__c = 'Reassign';
            update uaObj;
        System.Test.StopTest();   
    }    
}