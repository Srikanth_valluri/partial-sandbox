/******************************************************************************
* Description - Test class developed for NOCVisaController
*
* Version            Date            Author                    Description
* 1.0                13/12/17        Naresh Kaneriya (Accely)   Initial Draft
********************************************************************************/

@isTest
public class NOCVisaControllerTest{
    
    public static Case ObjCaseR = new Case();
    @testsetup
    public static void TestData(){  
        
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('NOC For Visa').getRecordTypeId();
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        Acc.party_ID__C='12345';
        insert Acc;
        System.assert(Acc != null);
     
        Case Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Type = 'NOCVisa';
        insert Cas;
        
        System.assert(Cas != null);
         
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
        
        SR_Booking_Unit__c SrbookingUnit = new SR_Booking_Unit__c();
        SrbookingUnit.Agreement_Status__c = 'Test';
        SrbookingUnit.Booking_Unit__c = BU.Id;
        SrbookingUnit.Case__c = Cas.Id;
        SrbookingUnit.Deal_Status__c = 'Test';
        insert SrbookingUnit;
        
        SR_Attachments__c SRattch =  new SR_Attachments__c();
        SRattch.Account__c = Acc.Id;
        SRattch.Booking_Unit__c =BU.Id;
        SRattch.Case__c = Cas.Id;
        SRattch.isValid__c = true;
        //SRattch.Error_Message__c = 'Error';
        insert SRattch;
        
        Buyer__c buyer =  new Buyer__c();
        buyer.Account__c  = Acc.Id;
        buyer.Booking__c   = Booking.Id;
        buyer.Primary_Buyer__c   = false;
        buyer.First_Name__c  = 'Test';      
        buyer.Last_Name__c  = 'Test Last';
        buyer.IPMS_Registration_ID__c ='123456';
        insert buyer;
        
    }  
  
    @istest static void toCoverVariable(){     
        
      NOCVisaController obj =  new NOCVisaController();
        obj.mapURLS = null;
        obj.lstJointBuyer = null;
        obj.strSRType = 'null';    
        obj.accountId = '07M4E000003rPCP';
        obj.bookingUnitId = '07M4E000003rPCP';
        obj.selectedUnit = null;
        obj.deleteAttRecId = '07M4E000003rPCP';
        obj.strDisplayMessage = 'null';
        obj.soaUrl = 'null';
        obj.bookingUnitId = '07M4E000003rPCP';
        obj.selectedUnit = null;
        obj.strCRFAttachmentBody  = 'Test';
        obj.strCRFAttachmentName  = 'Test';
        obj.strPOAAttachmentBody  = 'Test';
        obj.strPOAAttachmentName  = 'Test';
        obj.titleDeedAttachmentBody  = 'Test';
        obj.titleDeedAttachmentName  = 'Test';
        obj.additionalAttachmentBody  = 'Test';
        obj.additionalAttachmentName  = 'Test';
        obj.passportAttachmentBody  = 'Test';
        obj.passportAttachmentName  = 'Test';
        obj.isLightningMode = false;
        obj.strSelectedSection  = 'Test';
        obj.lstOpenCases  = null;
        obj.lstClosedCases  = null;
        obj.strHelpPageUrl  = 'Test';
       // obj.listAttachment  = null;
        obj.listCaseAttachment  = null;
        obj.showNOCVisabutton  =false;
        obj.bookingUnitWrapper = null;  
        obj.caseRecord  = null;
        obj.renderUnitDetails  =false;
        obj.renderOtherDetails  =false;
        obj.poaDateOfExpiry  = 'Test';
        obj.userNationality  = 'Test';
        obj.isSaveAsDraft  = false;
        obj.isPOAPresent  = false;
        obj.numberOfBuyer  = 1;
        obj.newlstBuyer  =null;
        obj.buyerMultiselect = false; 
        obj.buyerNameList  = null;
        obj.poaAttachmentWrapper  = null;
        obj.crfAttachmentWrapper  = null;
        obj.titleDeedAttachmentWrapper  = null;
        obj.passportAttachmentWrapper  =null;
        obj.additionalAttachmentWrapper = null; 
        obj.listAttachmentWrapper  = null;
        obj.mapIdAttachmentWrapper  = null;
        obj.listNOCforVisa  = null;
        obj.strDocUrl  = 'Test';
        obj.blnIsLitigationFlag  =false;
        obj.blnAllowSRInitiation = false; 
        obj.lstPayments  = null;
        obj.SOAString  = 'Test';
        obj.FMSOAString  = 'Test';
    }
    
    
    // TO cover getName , getAllRelatedBookingUnits,  getBookingUnitList
     public static testMethod void GetName_Test(){
         Test.StartTest();
         NOCVisaController obj = new NOCVisaController();
        
         Account Acc = getAcc();
         Acc.Nationality__c = null;
         update Acc;
         
         Buyer__c  Byer = getBuyer();
         Byer.Primary_Buyer__c = true;
         Byer.Account__c = Acc.Id;
         update Byer;
         
         Booking_Unit__c Bu = getBookingUnit();
         Bu.Requested_Price__c  = 2000000;
         Bu.Registration_Status__c  = 'Active';
         Bu.Title_Deed__c   = true; 
         update Bu;
         
         Booking_Unit_Active_Status__c  Bs = new Booking_Unit_Active_Status__c ();
         Bs.Status_Value__c = 'Active';
         Bs.Name = 'SA - Agreement pending disputes'; 
         insert Bs;
        
         obj.accountId = Acc.Id ; 
         obj.strSRType ='NOCVisa'; 
         obj.bookingUnitId  = Bu.Id;
         obj.getName();
         NOCVisaController.getAllRelatedBookingUnits(Acc.Id);
         obj.getBookingUnitList();
         NOCVisaController.getAllJointBuyer(obj.bookingUnitId , Acc.id);  
         obj.getBuyerList();
         
         obj.renderUnitDetails = true;
         obj.next(); 
         
         Acc.Nationality__c = 'Indian';
         update Acc;
         
         NOCVisaController.getAllRelatedBookingUnits(Acc.Id);
         obj.getBookingUnitList();
         
         obj.AccID = null;
         obj.strCaseID = null;
         obj.strSRtypeURL = 'NOCVisa';  
         obj.titleDeedAttachmentBody = null ;
         obj.executews();   
         
         Test.stopTest();
     }
     
   public static testmethod void changePOACheckbox_Test(){
       Test.startTest();
       NOCVisaController obj = new NOCVisaController();
       NOCVisaController.filebodyName objfilebodyName = new NOCVisaController.filebodyName();
       objfilebodyName.filenames = 'filenames';
       objfilebodyName.filebody = 'filebody';
       
       NOCVisaController.AttachmenWrapper objAttacht =  new NOCVisaController.AttachmenWrapper(getSRAttach());
       
       Account Acc = getAcc();
       Acc.Nationality__c = null;
       update Acc;
         
       Booking_Unit__c Bu = getBookingUnit();
       Bu.Requested_Price__c  = 2000000;
       Bu.Registration_Status__c  = 'Active';
       Bu.Title_Deed__c   = true; 
       update Bu;
         
       Buyer__c  Byer = getBuyer();
       Byer.Primary_Buyer__c = true;
       Byer.Account__c = Acc.Id;
       update Byer;
       
       Case c=  getCase();
       c.IsPOA__c = true;
       c.Buyer_For_NOC__c = 'Buyer[';
       c.AccountId = Acc.Id;
       c.IsPOA__c = false;
       update c ;
       
       SR_Attachments__c Sr = getSRAttach() ;
       Sr.Type__c = 'NOC For VISA';
       update Sr;
       
       obj.caseRecord = new Case() ;       
       obj.createnewCase();
       
       obj.caseRecord = c ;
       obj.accountId = Acc.Id;
       obj.bookingUnitId  = Bu.Id;
       obj.poaDateOfExpiry = '2017-01-12';
       obj.isSaveAsDraft = true;
       obj.changePOACheckbox();
       obj.extractName('Test Account');
       obj.verifyBuyer();
       NOCVisaController.getCaseDetails(c.id); 
       NOCVisaController.getCaseDetails(new Case().Id);
       NOCVisaController.getRecordTypeId(null , null);   
         
       List<String> buyerId =  new List<String>();
       buyerId.add(Byer.Id);
       obj.listbuyerstr = buyerId ;
       obj.populateBuyerList();
       obj.populateCaseData();  
       obj.submitCase();   
       obj.saveNewSRBookingUNIT(Bu.Id);
       obj.errorLogger('Error Message',c.Id,getBookingUnit());
       obj.createCaseAttachment('Power Of Attorney7' , 'Test');
       obj.closeCase();
       obj.loadNOC();
       
       Test.stopTest();
         
   }
    
    public static testmethod void extractBody_Test(){
        Test.startTest();
         NOCVisaController obj = new NOCVisaController();
        Blob b=Blob.valueOf('Test Data');
        String BlobStrg =EncodingUtil.base64Encode(b);
        obj.extractBody(BlobStrg); 
        obj.extractBody(''); 
        obj.uploadAttachment('BlobStrg',b);
        
        NOCVisaController.getExistingOpenCase(getBookingUnit().Id,getAcc().Id);
        NOCVisaController.getExistingOpenCase(new Booking_Unit__c().Id,new Account().Id); 
        
        Test.stopTest();
        
    }
    
     @istest static void populateCaseAttachments_Test(){
        test.StartTest();
        NOCVisaController instance = new NOCVisaController();
        SR_Attachments__c Sr = getSRAttach();
        Case Cas = getCase(); 
        Cas.AccountId = getAcc().Id;
        Cas.IsPOA__c = false;
        update Cas;
        
        Sr.Type__c = 'Power Of Attorney';
        update Sr;
        
        instance.caseRecord = Cas; 
        instance.deleteAttRecId = getSRAttach().Id;
        
        instance.accountId = getAcc().Id;
        instance.bookingUnitId = getBookingUnit().Id; 
        
        NOCVisaController.AttachmenWrapper objAttacht =  new NOCVisaController.AttachmenWrapper(getSRAttach());
        Map < Id, NOCVisaController.AttachmenWrapper > mapIdAttachmentWrapper =  new Map < Id, NOCVisaController.AttachmenWrapper>();
        mapIdAttachmentWrapper.put(instance.deleteAttRecId,objAttacht);
        instance.mapIdAttachmentWrapper  = mapIdAttachmentWrapper;
        instance.deleteAttachment();  
        
        instance.populateCaseAttachments(Cas.Id);
        
        
        instance.populateCaseAttachments(Cas.Id);
        instance.poaDateOfExpiry = '2017-09-09';   
        instance.populateCaseData();
        NOCVisaController.getCountryGroupOfCustomer('UAE/KSA');
        
        // instance.setGCCCountries;   
        NOCVisaController.getCountryGroupOfCustomer('Qatar');
        NOCVisaController.getCountryGroupOfCustomer(null);         
        
        Sr.Type__c = 'CRF Form';
        update Sr;
        instance.populateCaseAttachments(Cas.Id);
        
        Sr.Type__c = 'Title Deed';
        update Sr;
        instance.populateCaseAttachments(Cas.Id);
        
        Sr.Type__c = 'Passport';
        update Sr;
        instance.populateCaseAttachments(Cas.Id);
        
        Sr.Type__c = 'Entry Visa Stamp';  
        update Sr;
        instance.populateCaseAttachments(Cas.Id);      
        
        test.StopTest();
         
    }
    
    
     public static testmethod void generateNOC_Test(){
       
        Booking_Unit__c Bu = getBookingUnit();
        Bu.Registration_ID__c  = '1650';
        Bu.Title_Deed__c   = true;
        update Bu; 
        
        NOCVisaController obj = new NOCVisaController();
        // Buyer__c Buye =  getBuyer();  
        list<string> BuyId =  new list<string>();
        BuyId.add(getBuyer().Id);
        
       
        obj.bookingUnitId = Bu.Id;
        obj.listbuyerstr = BuyId ;     
        
        
        Case c = getCase(); 
        c.status = 'New'; 
        update c ;  
        System.assertEquals(c.Type, 'NOCVisa');
        
        obj.caseRecord = c ; 

        SR_Attachments__c Sr = getSRAttach() ;
        Sr.Case__c = c.Id ;
        Sr.isValid__c = true ;  
        update Sr;           
        
        System.assertEquals(c.Id,Sr.Case__c);   
        
        Test.StartTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,documentGeneration.DocGenerationResponse_element>();
        documentGeneration.DocGenerationResponse_element response_x = new documentGeneration.DocGenerationResponse_element();
        response_x.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        obj.generateNOCMethod();
    
        Test.stopTest();
     
    }
    
     public static testmethod void generateCRForm_Test(){
        
        NOCVisaController obj = new NOCVisaController();
        Booking_Unit__c Bu = getBookingUnit();
        Bu.Registration_ID__c  = '1650';
        Bu.Title_Deed__c   = true;
        update Bu; 
        obj.selectedUnit  = Bu; 
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,GenerateCRFService.GetCustomerRequestFormResponse_element>();
          GenerateCRFService.GetCustomerRequestFormResponse_element response_x = new GenerateCRFService.GetCustomerRequestFormResponse_element();
         
          response_x.return_x ='{'+
        '  "CRFInnerWrap": ['+
        '  {'+
        '    "PROC_STATUS" : "S",'+
        '    "PROC_MESSAGE" : "Message",'+
        '    "ATTRIBUTE1": "www.google.com",'+
        '    "ATTRIBUTE2": "ATTRIBUTE1",'+
        '    "ATTRIBUTE3" : "ATTRIBUTE1"'+
        '  }'+
        '  ],'+
        '  "message": "message",'+
        '  "Status": "S"'+
        '}';  
        
          SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
          Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
          obj.generateCRForm();  
          Test.stopTest();
        
    }
    
    
     public static testmethod void insertStatementOfAccount_Test(){
        
        NOCVisaController obj = new NOCVisaController();
        
        obj.insertStatementOfAccount();  
        
        UnitDetailsService.BookinUnitDetailsWrapper  ObNu = new UnitDetailsService.BookinUnitDetailsWrapper();
        ObNu.strRegId  = '1650';    
        
        WrapperBookingUnit objWrapperBookingUnit  = new WrapperBookingUnit();
        objWrapperBookingUnit.objIPMSDetailsWrapper = ObNu ;
        
        obj.bookingUnitWrapper = objWrapperBookingUnit ;
        
        Test.StartTest(); 
        SOAPCalloutServiceMock.returnToMe = new Map<String,GenerateSOAService.GenCustomerStatementResponse_element>();
        GenerateSOAService.GenCustomerStatementResponse_element response_x = new GenerateSOAService.GenCustomerStatementResponse_element();
          response_x.return_x ='{'+
        '         "status" : "S",'+
        '         "url" : "www.google.com",'+
        '         "PROC_MESSAGE" : "Test",'+
        '         "PROC_STATUS" : "S",'+
        '       '+
        '       '+
        '       }'; 
          SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
          Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());      
        obj.insertStatementOfAccount();     
        Test.stopTest();
    }
    
    public static testmethod void getBookinUnitDetails_Test(){
         
         NOCVisaController obj = new NOCVisaController();
         obj.lstOpenCases = new list<Case>();
         obj.blnIsLitigationFlag = false; 
         obj.numberOfBuyer  = 2;    
         
         Account Acc = getAcc();
         Acc.Nationality__c = null;  
         update Acc;
         
         Buyer__c  Byer = getBuyer();
         Byer.Primary_Buyer__c = true;
         Byer.Account__c = Acc.Id;
         update Byer;
         
         Booking_Unit_Active_Status__c  Bs = new Booking_Unit_Active_Status__c ();
         Bs.Status_Value__c = 'Active';
         Bs.Name = 'SA - Agreement pending disputes'; 
         insert Bs;  
         Inventory__c inventory = InitialiseTestData.getInventoryDetails('3456','1234','2345',9086,7650); 
         inventory.Status__c = 'Released';
         inventory.Property_City__c = 'Dubai';
         inventory.Property_Status__c = 'Ready';
         inventory.Unit_Type__c = 'Test';
         insert inventory ;     
         
         Booking_Unit__c Bu = getBookingUnit();
         Bu.Requested_Price__c  = 2000000;
         Bu.Registration_Status__c  = 'Active';
         Bu.Inventory__c = inventory.id;
         Bu.Title_Deed__c   = true; 
         Bu.Handover_Flag__c   = 'Y'; 
         Bu.Early_Handover__c   = true; 
         update Bu; 
         
         Option__c Op = new option__c();
         Op.Booking_Unit__c = Bu.Id;
         Op.CampaignName__c = 'CampaignName';
         Op.PromotionName__c = 'PromotionName';
         Op.SchemeName__c = 'SchemeName';
         insert Op;    
       
         Case Cs = getCase();
         Cs.Status = 'New';
         Cs.Booking_Unit__c  = Bu.Id;   
         Cs.POA_Expiry_Date__c = System.today() + 30;  
         update Cs;
         
         SR_Attachments__c  Sr = getSRAttach();  
         Sr.Case__c = Cs.Id;
         Sr.Type__c ='NOC For VISA';  
         update Sr ;
         
         System.assert(Cs!=null);
         
         Payment_Plan__c pl =  new Payment_Plan__c();
         pl.Status__c = 'Active';
         pl.Booking_Unit__c = obj.bookingUnitId;
         insert pl; 
          
         obj.selectedUnit  = Bu; 
         obj.accountId = Acc.Id ;   
         obj.bookingUnitId  = Bu.Id;
         obj.caseRecord = Cs ;  
        
         obj.getName();
         NOCVisaController.getAllRelatedBookingUnits(getAcc().Id);  
         obj.getBookingUnitList();
         
         
         
         Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,AssignmentProcessWSDL.getPendingDuesResponse_element>();
        AssignmentProcessWSDL.getPendingDuesResponse_element response1 = new AssignmentProcessWSDL.getPendingDuesResponse_element();
        response1.return_x = 'S';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock()); 
          
        obj.getBookinUnitDetails();  
        Test.stopTest();  
        
     }  
    
    
     
    public static testmethod void uploadAttachments_Test(){
        
        Test.startTest();
        Case c = getCase() ;
        SR_Attachments__c  Sr = getSRAttach();  
        Sr.Case__c = c.Id;
        update Sr ;
        
        Blob blob1=Blob.valueOf('strPOAAttachmentName.DOC');  
        
        //  strPOAAttachmentBody
        NOCVisaController obj = new NOCVisaController();
        
        obj.listCaseAttachment = new List < SR_Attachments__c > ();
        obj.userNationality = ''; 
        //// obj.listAttachment = new List < Attachment > ();
        obj.listAttachmentWrapper = new List <NOCVisaController.AttachmenWrapper> ();
        obj.userNationality = '';   
        
        
      obj.caseRecord = c; 
        obj.deleteAttRecId = getSRAttach().Id;
        obj.accountId = getAcc().Id;
        obj.bookingUnitId = getBookingUnit().Id; 
      NOCVisaController.AttachmenWrapper objAttacht =  new NOCVisaController.AttachmenWrapper(getSRAttach());
        Map < Id, NOCVisaController.AttachmenWrapper > mapIdAttachmentWrapper =  new Map < Id, NOCVisaController.AttachmenWrapper>();
        mapIdAttachmentWrapper.put(obj.deleteAttRecId,objAttacht);
        obj.mapIdAttachmentWrapper  = mapIdAttachmentWrapper;
       // obj.strPOAAttachmentBody=EncodingUtil.base64Encode(blob1);
       // obj.strPOAAttachmentName='strPOAAttachmentName';
        obj.strPOAAttachmentBody='ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        obj.strPOAAttachmentName='image005.jpg';
       
        obj.uploadAttachments(); 
      
      Test.stopTest();     
    }
    
    public static testmethod void uploadAttachments_Test2(){
        
        Test.startTest();
        Case c = getCase() ;
        SR_Attachments__c  Sr = getSRAttach();  
        Sr.Case__c = c.Id;
        update Sr ;
        
        Blob blob1=Blob.valueOf('strCRFAttachmentName.DOC');
        
        //  strCRFAttachmentBody
        NOCVisaController obj = new NOCVisaController();
        obj.listCaseAttachment = new List < SR_Attachments__c > ();
        // // obj.listAttachment = new List < Attachment > ();
         obj.listAttachmentWrapper =  new List <NOCVisaController.AttachmenWrapper> ();
         obj.userNationality = ''; 
         
         
      obj.caseRecord = c; 
        obj.deleteAttRecId = getSRAttach().Id;
        obj.accountId = getAcc().Id;
        obj.bookingUnitId = getBookingUnit().Id; 
      NOCVisaController.AttachmenWrapper objAttacht =  new NOCVisaController.AttachmenWrapper(getSRAttach());
        Map < Id, NOCVisaController.AttachmenWrapper > mapIdAttachmentWrapper =  new Map < Id, NOCVisaController.AttachmenWrapper>();
        mapIdAttachmentWrapper.put(obj.deleteAttRecId,objAttacht);
        obj.mapIdAttachmentWrapper  = mapIdAttachmentWrapper;
        //obj.strCRFAttachmentBody=EncodingUtil.base64Encode(blob1);
        //obj.strCRFAttachmentName='strCRFAttachmentName';
        
        obj.strCRFAttachmentBody='ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        obj.strCRFAttachmentName='image005.jpg';
        
        obj.uploadAttachments(); 
      
      Test.stopTest();     
    }
    
    public static testmethod void uploadAttachments_Test3(){
        
        Test.startTest();
        Case c = getCase() ;
        SR_Attachments__c  Sr = getSRAttach();  
        Sr.Case__c = c.Id;
        update Sr ;
        
       Blob blob1=Blob.valueOf('titleDeedAttachmentName.DOC');
        
        //  titleDeedAttachmentBody 
        NOCVisaController obj = new NOCVisaController();
        obj.listCaseAttachment = new List < SR_Attachments__c > ();
        // // obj.listAttachment = new List < Attachment > ();
         obj.listAttachmentWrapper = new List <NOCVisaController.AttachmenWrapper> ();
         obj.userNationality = ''; 
         
         
      obj.caseRecord = c; 
        obj.deleteAttRecId = getSRAttach().Id;
        obj.accountId = getAcc().Id;
        obj.bookingUnitId = getBookingUnit().Id; 
      NOCVisaController.AttachmenWrapper objAttacht =  new NOCVisaController.AttachmenWrapper(getSRAttach());
        Map < Id, NOCVisaController.AttachmenWrapper > mapIdAttachmentWrapper =  new Map < Id, NOCVisaController.AttachmenWrapper>();
        mapIdAttachmentWrapper.put(obj.deleteAttRecId,objAttacht);
        obj.mapIdAttachmentWrapper  = mapIdAttachmentWrapper;
        //obj.titleDeedAttachmentBody=EncodingUtil.base64Encode(blob1);
       // obj.titleDeedAttachmentName='titleDeedAttachmentName';   
       
        obj.titleDeedAttachmentBody='ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        obj.titleDeedAttachmentName='image005.jpg';
        obj.uploadAttachments();   
      
      Test.stopTest();     
    }
    
    
    public static testmethod void uploadAttachments_Test4(){
        
        Test.startTest();
        Case c = getCase() ;
        SR_Attachments__c  Sr = getSRAttach();  
        Sr.Case__c = c.Id;
        update Sr ;
        
        Blob blob1=Blob.valueOf('passportAttachmentName.DOC');
        
        //  passportAttachmentBody
        NOCVisaController obj = new NOCVisaController();
        obj.listCaseAttachment = new List < SR_Attachments__c > ();
         // obj.listAttachment = new List < Attachment > ();
         obj.listAttachmentWrapper = new List <NOCVisaController.AttachmenWrapper> ();
         obj.userNationality = ''; 
         
         
      obj.caseRecord = c; 
        obj.deleteAttRecId = getSRAttach().Id;
        obj.accountId = getAcc().Id;
        obj.bookingUnitId = getBookingUnit().Id; 
      NOCVisaController.AttachmenWrapper objAttacht =  new NOCVisaController.AttachmenWrapper(getSRAttach());
        Map < Id, NOCVisaController.AttachmenWrapper > mapIdAttachmentWrapper =  new Map < Id, NOCVisaController.AttachmenWrapper>();
        mapIdAttachmentWrapper.put(obj.deleteAttRecId,objAttacht);
        obj.mapIdAttachmentWrapper  = mapIdAttachmentWrapper;
       // obj.passportAttachmentBody=EncodingUtil.base64Encode(blob1);
       // obj.passportAttachmentName='passportAttachmentName';
        
        obj.passportAttachmentBody='ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        obj.passportAttachmentName='image005.jpg';
        
        obj.uploadAttachments();   
      
      Test.stopTest();     
    }
    
    
    public static testmethod void uploadAttachments_Test5(){
        
        Test.startTest();
        Case c = getCase() ;
        SR_Attachments__c  Sr = getSRAttach();  
        Sr.Case__c = c.Id;
        update Sr ;
        
        Blob blob1=Blob.valueOf('additionalAttachmentName.DOC');
        
        //  additionalAttachmentBody
        NOCVisaController obj = new NOCVisaController();
       
         obj.listCaseAttachment = new List < SR_Attachments__c > ();
         obj.listCaseAttachment = new List < SR_Attachments__c > ();
         // obj.listAttachment = new List < Attachment > ();
         obj.listAttachmentWrapper = new List <NOCVisaController.AttachmenWrapper> ();
         obj.userNationality = '';    
          
         
      obj.caseRecord = c; 
        obj.deleteAttRecId = getSRAttach().Id;
        obj.accountId = getAcc().Id;
        obj.bookingUnitId = getBookingUnit().Id; 
      NOCVisaController.AttachmenWrapper objAttacht =  new NOCVisaController.AttachmenWrapper(getSRAttach());
        Map < Id, NOCVisaController.AttachmenWrapper > mapIdAttachmentWrapper =  new Map < Id, NOCVisaController.AttachmenWrapper>();
        mapIdAttachmentWrapper.put(obj.deleteAttRecId,objAttacht);
        obj.mapIdAttachmentWrapper  = mapIdAttachmentWrapper;
       // obj.additionalAttachmentBody=EncodingUtil.base64Encode(blob1);
       // obj.additionalAttachmentName='additionalAttachmentName'; 
     
        obj.additionalAttachmentBody='ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        obj.additionalAttachmentName='image005.jpg';    
        obj.uploadAttachments();     
        Test.stopTest();        
    }
    
    
    public static testmethod void executews_Test1(){
         Test.startTest();
         
         NOCVisaController obj = new NOCVisaController();
         obj.userNationality = '';    
         obj.poaAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.crfAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.titleDeedAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.passportAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.additionalAttachmentWrapper =new NOCVisaController.AttachmenWrapper();
        
         Account acc = getAcc(); 
         acc.Party_ID__c = '2548';
         update acc ;
       
       Case c = getCase(); 
       c.Buyer_For_NOC__c = '[Test';  
         update c ;
        
       obj.caseRecord =  getCase();
       
       obj.AccID = acc.Id; 
       obj.strCaseID = c.Id; 
       obj.strSRtypeURL ='NOCVisa';  
       
       obj.caseRecord = getCase();
       obj.verifyBuyer();
       obj.populateBuyerList();
         
         SR_Attachments__c Sr = getSRAttach() ;
         Sr.Type__c = 'CRF Form';
         update Sr;
         obj.executews();
        
         SR_Attachments__c Sr1 = getSRAttach() ;
         Sr1.Type__c = 'Title Deed';
         update Sr1;
         obj.executews();
         
         SR_Attachments__c Sr2 = getSRAttach() ;
         Sr2.Type__c = 'Passport';
         update Sr2;
         obj.executews();
         
         SR_Attachments__c Sr3 = getSRAttach() ;
         Sr3.Type__c = 'Power Of Attorney';
         update Sr3;
         obj.executews();
         
         SR_Attachments__c Sr4 = getSRAttach() ;
         Sr4.Type__c = 'Entry Visa Stamp';
         update Sr4;
         obj.executews(); 
     
         Test.stopTest(); 
     }
     
        
     public static testmethod void executews_Test2(){
         
         
         NOCVisaController obj = new NOCVisaController();
          obj.userNationality = '';    
         obj.poaAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.crfAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.titleDeedAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.passportAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.additionalAttachmentWrapper =new NOCVisaController.AttachmenWrapper();
         
         Account acc = getAcc(); 
       acc.Party_ID__c = '2548';
       update acc ;  
       ObjCaseR = [select id from case Limit 1];
       SR_Attachments__c Sr = getSRAttach();
       Sr.Case__c = ObjCaseR.ID ;
       update Sr;
       
       obj.accountId = acc.Id ;
         obj.AccID = acc.Id ;
         obj.caseRecord = getCase();    
       
       Blob blob1=Blob.valueOf('titleDeedAttachmentBody');
      obj.titleDeedAttachmentBody=EncodingUtil.base64Encode(blob1);
      obj.titleDeedAttachmentName='titleDeedAttachmentName';
        
         obj.verifyBuyer();
         obj.populateBuyerList();
         Test.startTest();
         SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
    MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response_x = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
    response_x.return_x = '{'+
        '  "data" : ['+
        '  {'+
        '    "PROC_STATUS":"S", '+
        '    "PROC_MESSAGE":"Message",'+
        '    "PARAM_ID":"1234",1'+
        '    "url":"www.google.com",'+
        '    "FTP_Response":"s"'+
        '     '+
        '  }'+
        '    '+
        '   ],'+
        '   '+
        '   "message":"message ",'+
        '    "status":"s"'+
        ''+
        '}';
    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
         System.debug('-----Hi1-----');
         
         obj.executews();
         Test.stopTest();
     }
     
     
     public static testmethod void executews_Test3(){ 
         
         
         NOCVisaController obj = new NOCVisaController();
          obj.userNationality = '';    
         obj.poaAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.crfAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.titleDeedAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.passportAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.additionalAttachmentWrapper =new NOCVisaController.AttachmenWrapper();
         
         Account acc = getAcc(); 
       acc.Party_ID__c = '2548';
       update acc ;  
       ObjCaseR =[select id from case Limit 1];
       SR_Attachments__c Sr = getSRAttach();
       Sr.Case__c = ObjCaseR.ID ;
       update Sr;
       
       obj.accountId = acc.Id ;
         obj.AccID = acc.Id ;
         obj.caseRecord = getCase();    
       
       Blob blob1=Blob.valueOf('strCRFAttachmentBody');
      obj.strCRFAttachmentBody=EncodingUtil.base64Encode(blob1);
      obj.strCRFAttachmentName= 'strCRFAttachmentName';
        
         obj.verifyBuyer();
         obj.populateBuyerList();
         Test.startTest();
          SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
    MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response_x = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
    response_x.return_x = '{'+
        '  "data" : ['+
        '  {'+
        '    "PROC_STATUS":"S", '+
        '    "PROC_MESSAGE":"Message",'+
        '    "PARAM_ID":"1234",'+
        '    "url":"www.google.com",'+
        '    "FTP_Response":"s"'+
        '     '+
        '  }'+
        '    '+
        '   ],'+
        '   '+
        '   "message":"message ",'+
        '    "status":"s"'+
        ''+
        '}';
    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
         System.debug('-----Hi2-----');
         
         obj.executews();
         Test.stopTest();
     }
     
     
      public static testmethod void executews_Test4(){
         
         Test.startTest();
         NOCVisaController obj = new NOCVisaController();
          obj.userNationality = '';    
         obj.poaAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.crfAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.titleDeedAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.passportAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.additionalAttachmentWrapper =new NOCVisaController.AttachmenWrapper(); 
         
         
       Account acc = getAcc(); 
       acc.Party_ID__c = '2548';
       update acc ;  
       ObjCaseR =[select id from case Limit 1];
       SR_Attachments__c Sr = getSRAttach();
       Sr.Case__c = ObjCaseR.ID ;
       update Sr;      
       
       obj.accountId = acc.Id ;
         obj.AccID = acc.Id ;
         obj.caseRecord = getCase();    
       
       Blob blob1=Blob.valueOf('strPOAAttachmentBody');
      obj.strPOAAttachmentBody=EncodingUtil.base64Encode(blob1);
       // obj.strPOAAttachmentName='strPOAAttachmentName';
        
         obj.verifyBuyer();
         obj.populateBuyerList();
         
         
         obj.executews();
         Test.stopTest();
     }
     
       public static testmethod void executews_Test5(){
         
         
         NOCVisaController obj = new NOCVisaController();
          obj.userNationality = '';    
         obj.poaAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.crfAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.titleDeedAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.passportAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.additionalAttachmentWrapper =new NOCVisaController.AttachmenWrapper();
         
         Account acc = getAcc(); 
       acc.Party_ID__c = '2548';
       update acc ;  
       ObjCaseR =[select id from case Limit 1];
       SR_Attachments__c Sr = getSRAttach();
       Sr.Case__c = ObjCaseR.ID ;
       update Sr;
       
       obj.accountId = acc.Id ;
         obj.AccID = acc.Id ;
         obj.caseRecord = getCase();    
       
       Blob blob1=Blob.valueOf('additionalAttachmentBody');
       obj.additionalAttachmentBody=EncodingUtil.base64Encode(blob1);
       obj.additionalAttachmentName = 'additionalAttachmentName';
         obj.verifyBuyer();
         obj.populateBuyerList();
         Test.startTest();
           SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
    MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response_x = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
    response_x.return_x = '{'+
        '  "data" : ['+
        '  {'+
        '    "PROC_STATUS":"S", '+
        '    "PROC_MESSAGE":"Message",'+
        '    "PARAM_ID":"1234",'+
        '    "url":"www.google.com",'+
        '    "FTP_Response":"s"'+
        '     '+
        '  }'+
        '    '+
        '   ],'+
        '   '+
        '   "message":"message ",'+
        '    "status":"s"'+
        ''+
        '}';
    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
         System.debug('-----Hi4-----');

         obj.executews();
         Test.stopTest();
     }
     
     
      public static testmethod void executews_Test6(){
      
         
         NOCVisaController obj = new NOCVisaController();
          obj.userNationality = '';    
         obj.poaAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.crfAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.titleDeedAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.passportAttachmentWrapper = new NOCVisaController.AttachmenWrapper();
         obj.additionalAttachmentWrapper =new NOCVisaController.AttachmenWrapper();
         
         Account acc = getAcc(); 
       acc.Party_ID__c = '2548';
       update acc ;  
       ObjCaseR =[select id from case Limit 1];
       SR_Attachments__c Sr = getSRAttach();
       Sr.Case__c = ObjCaseR.ID ;
       update Sr;
       
       obj.accountId = acc.Id ;
         obj.AccID = acc.Id ;
         obj.caseRecord = getCase();    
       
       Blob blob1=Blob.valueOf('passportAttachmentBody');
      obj.passportAttachmentBody=EncodingUtil.base64Encode(blob1);
      obj.passportAttachmentName = 'passportAttachmentName';
        
         obj.verifyBuyer();
         obj.populateBuyerList();
         Test.startTest();
          SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
    MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response_x = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
    response_x.return_x = '{'+
        '  "data" : ['+
        '  {'+
        '    "PROC_STATUS":"S", '+
        '    "PROC_MESSAGE":"Message",'+
        '    "PARAM_ID":"1234",'+
        '    "url":"www.google.com",'+
        '    "FTP_Response":"s"'+
        '     '+
        '  }'+
        '    '+
        '   ],'+
        '   '+
        '   "message":"message ",'+
        '    "status":"s"'+
        ''+
        '}';
    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
         System.debug('-----Hi5-----');
         obj.executews();
         Test.stopTest();
     }
      
    public static testmethod void Init_Test(){
        
        NOCVisaController obj =  new NOCVisaController();
        obj.AccID = ApexPages.currentPage().getParameters().put('AccountId',getAcc().Id);
        obj.strCaseID = ApexPages.currentPage().getParameters().put('CaseID',getCase().Id);
        obj.strSRtypeURL = ApexPages.currentPage().getParameters().put('SRType','NOCVisa');
        obj.init();
    }
    
    // To get Case Record
    public static Case getCase(){
        return [select id ,Buyer_For_NOC__c, CaseNumber ,RecordType.Name,Type  from Case where Credit_Note_Amount__c=5000 limit 1];
    }
    
    //  // To get Account Record
    public static Account getAcc(){
        return [select id ,Party_ID__c  from Account where party_ID__C='12345' limit 1];
    }
    
    public static Booking__c getBooking(){
        
        return [select Id from Booking__c where AWB_Number__c = 'Test AWB' limit 1];
    }
    
    public static Booking_Unit__c getBookingUnit(){
       return [select Id , Booking__c from Booking_Unit__c where Unit_Selling_Price_AED__c = 100 limit 1]; 
    }
      
     public static SR_Booking_Unit__c getSRbooking(){
       return [select Id from SR_Booking_Unit__c where Agreement_Status__c = 'Test' limit 1];
    }

    public static SR_Attachments__c getSRAttach(){
       return [select Id , Case__c from SR_Attachments__c  limit 1];
    }
   
    public static Buyer__c getBuyer(){
       return [select Id from Buyer__c where First_Name__c  = 'Test' limit 1];
    }
    
    
}