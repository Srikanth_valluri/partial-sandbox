@isTest
Public Class OfficeMeetingTriggerHandler_Test {
    public static testMethod void method1 () {
        Id pcProfile = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id;
        User PC = new User(alias = 'te156', email='x123@email.com', emailencodingkey='UTF-8',
            lastname='User 123', languagelocalekey='en_US', localesidkey='en_US', 
            profileid = pcProfile, country='United Arab Emirates', IsActive =true,
            timezonesidkey='America/Los_Angeles', username = 'x123@email.com',
            ManagerId = userInfo.getUserId()
        );
      
      
      User u = [SELECT ID, Extension,Profile.Name FROM User where ID =: USERInfo.getUserID ()  ];
      if (u.Extension != NULL) {
            u.Extension = '0715';
            u.Profile.Name = 'Property Consultant';
            u.Languages_Known__c = 'Hindi';
            u.Sales_Office__c  = 'RIYADH';
            u.New_Team__c = 'Direct Team';
            update u;
        }
    Id inquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        Inquiry__c inq = New Inquiry__c ();
            inq.First_Name__c = 'testInq';
            inq.Preferred_Language__c = 'English';
            inq.Last_Name__c ='inqLast';
            inq.Inquiry_Source__c = 'Prospecting';
            inq.Primary_Contacts__c = 'Mobile Phone';
            inq.Mobile_CountryCode__c = 'India: 0091';
            inq.Mobile_Phone__c = '1236547890';
            inq.Mobile_Phone_Encrypt__c = '1223467886';
            inq.Email__c = 'test@gmail.com';
            inq.recordTypeId = inquiryRecordTypeId;
            inq.Sales_Office__c = 'RIYADH';
            
        insert inq;
        
        Office_Meeting__c  officeMeeting = New Office_Meeting__c ();
        officeMeeting.Inquiry__c = inq.id;
        officeMeeting.Bypass_Validation__c = false;
        officeMeeting.Outcome__c  = 'show';
        officeMeeting.Recommended_RM__c = u.id;
        officeMeeting.Comments__c = 'testData';
        insert officeMeeting;
        officeMeeting.Convert_inquiry__c = true;
        officeMeeting.Recommended_RM__c = PC.id;
        update officeMeeting;

        Office_Meeting__c ofcMeeting = new Office_Meeting__c(); 
        officeMeeting.Status__c = 'Completed';
        ofcMeeting.Check_Out_Date__c = system.now();
        ofcMeeting.Check_In_Date__c = system.now();
        ofcMeeting.Convert_inquiry__c = true;
        ofcMeeting.Outcome__c = 'Show';
        update officeMeeting;
          
        u.Sales_Office__c = 'OCEAN HEIGHTS';
        update u;
        officeMeeting.Recommended_RM__c = u.id;
        //update officeMeeting;

    //OfficeMeetingTriggerHandler obj = New OfficeMeetingTriggerHandler();
    }
    public static testMethod void method2 () {
        User u = [SELECT ID, Extension,Profile.Name FROM User where ID =: USERInfo.getUserID ()  ];
      if (u.Extension != NULL) {
            u.Extension = '0715';
            u.Profile.Name = 'Property Consultant';
            u.Languages_Known__c = 'Hindi';
            u.Sales_Office__c  = 'RIYADH';
            u.New_Team__c = 'Direct Team';
            update u;
        }
    Id inquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        Inquiry__c inq = New Inquiry__c ();
            inq.First_Name__c = 'testInq';
            inq.Preferred_Language__c = 'English';
            inq.Last_Name__c ='inqLast';
            inq.Inquiry_Source__c = 'Prospecting';
            inq.Primary_Contacts__c = 'Mobile Phone';
            inq.Mobile_CountryCode__c = 'India: 0091';
            inq.Mobile_Phone__c = '1236547890';
            inq.Mobile_Phone_Encrypt__c = '1223467886';
            inq.Email__c = 'test@gmail.com';
            inq.recordTypeId = inquiryRecordTypeId;
            inq.Sales_Office__c = 'RIYADH';
            
        insert inq;
        
        Office_Meeting__c  officeMeeting = New Office_Meeting__c ();
            officeMeeting.Inquiry__c = inq.id;
            officeMeeting.Bypass_Validation__c = false;
            officeMeeting.Outcome__c  = 'No show';
            officeMeeting.Recommended_RM__c = u.id;
            officeMeeting.Comments__c = 'testData';
        insert officeMeeting;
       Office_Meeting__c ofcMeeting = new Office_Meeting__c(); 
        officeMeeting.Status__c = 'Completed';
        ofcMeeting.Check_Out_Date__c = system.now();
        ofcMeeting.Check_In_Date__c = system.now();
        ofcMeeting.Outcome__c = 'Show';
        ofcMeeting.Convert_inquiry__c = true;
        update officeMeeting;
         Task tsk = new Task();
        tsk.WhatId=inq.Id;
        tsk.ActivityDate = System.today().addDays(2);
        tsk.Status = 'Not Started';
        tsk.Type = 'Meeting at Office';
        tsk.Activity_Type_3__c = 'Meeting at Office';
        tsk.Activity_Outcome__c = 'Show - Further Meeting Scheduled';
        tsk.Subject = 'Meeting With Customer';
        tsk.is_Owner_changed__c = true;
        insert tsk;    

    //OfficeMeetingTriggerHandler obj = New OfficeMeetingTriggerHandler();
    
    }
    
    public static testMethod void method3 () {
        User u = [SELECT ID, Extension,Profile.Name FROM User where ID =: USERInfo.getUserID ()  ];
      if (u.Extension != NULL) {
            u.Extension = '0715';
            u.Profile.Name = 'Property Consultant';
            u.Languages_Known__c = 'Hindi';
            u.Sales_Office__c  = 'RIYADH';
            u.New_Team__c = 'Direct Team';
            update u;
        }
    Id inquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        Inquiry__c inq = New Inquiry__c ();
            inq.First_Name__c = 'testInq';
            inq.Preferred_Language__c = 'English';
            inq.Last_Name__c ='inqLast';
            inq.Inquiry_Source__c = 'Prospecting';
            inq.Primary_Contacts__c = 'Mobile Phone';
            inq.Mobile_CountryCode__c = 'India: 0091';
            inq.Mobile_Phone__c = '1236547890';
            inq.Mobile_Phone_Encrypt__c = '1223467886';
            inq.Email__c = 'test@gmail.com';
            inq.recordTypeId = inquiryRecordTypeId;
            inq.Sales_Office__c = 'RIYADH';
            
        insert inq;
        
        Office_Meeting__c  officeMeeting = New Office_Meeting__c ();
            officeMeeting.Inquiry__c = inq.id;
            officeMeeting.Bypass_Validation__c = false;
            officeMeeting.Outcome__c  = 'No show';
            officeMeeting.Recommended_RM__c = u.id;
            officeMeeting.Comments__c = 'testData';
        insert officeMeeting;
       Office_Meeting__c ofcMeeting = new Office_Meeting__c(); 
        officeMeeting.Status__c = 'Completed';
        ofcMeeting.Check_Out_Date__c = system.now();
        ofcMeeting.Check_In_Date__c = system.now();
        ofcMeeting.Outcome__c = 'Show';
        ofcMeeting.Convert_inquiry__c = true;
        update officeMeeting;
         Task tsk = new Task();
        tsk.WhatId=inq.Id;
        tsk.ActivityDate = System.today().addDays(2);
        tsk.Status = 'Not Started';
        tsk.Type = 'Meeting at Office';
        tsk.Activity_Type_3__c = 'Meeting at Office';
        tsk.Activity_Outcome__c = 'Show - Further Meeting Scheduled';
        tsk.Subject = 'Meeting With Customer';
        tsk.is_Owner_changed__c = true;
        insert tsk;    

    //OfficeMeetingTriggerHandler obj = New OfficeMeetingTriggerHandler();
    
    }
}