@isTest
public class BulkSOAMock implements HttpCalloutMock {
    // Implement this interface method
    public HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(200);
        
        if(!req.getEndpoint().contains('lines')) {
            String strBody = '{"processAttributes":{"responseId":"760002","responseTime":"Mon Jan 22 16:06:49 GST 2018","responseMessage":null,"nextAction":null,"recordCount":159,"totalPages":8,"currentPage":1,"next":"http://94.200.40.200:8080/DCOFFEE/customer/statement/760002?page=1","previous":"http://94.200.40.200:8080/DCOFFEE/customer/statement/760002?page=1"},"units":[{"id":"76000251463","partyId":760002,"unitNumber":"BD3/34/3401","unitStatus":"Agreement executed by DAMAC","unitPrice":2073600,"propertyName":"THE DISTINCTION","registrationId":"51463","registrationDate":"29-Jan-2014","disputeStatus":null,"debitTotal":2379407.36,"creditTotal":2380375.36,"vatCharged":null,"vatPaid":0,"installmentDue":0,"curency":"AED","buildingId":42467,"statusFlag":"LE","sellerName":"SELLER_NAME","bankDetail":{"id":42467,"beneficiaryName":"Damac Development (L.L.C)","beneficiaryBank":"Abu Dhabi Commercial Bank","bankBranch":"Karama,Dubai,AE","bankAccountName":"DAMAC DEVELOPMENT LLC","beneficiaryNumber":"509978020001","eftSwiftCode":"ADCBAEAA","iban":"AE320030000509978020001"},"lineUrl":"http://94.200.40.200:8080/DCOFFEE/customer/statement/lines/51463"}]}';
            res.setBody(strBody);
        }
        else {
            String strBody = '{"processAttributes":{"responseId":"51469","responseTime":"Sun Jan 21 15:40:48 GST 2018","responseMessage":null,"nextAction":null,"recordCount":38,"totalPages":2,"currentPage":1,"next":"http://94.200.40.200:8080/DCOFFEE/customer/statement/lines/51469?page=1","previous":"http://94.200.40.200:8080/DCOFFEE/customer/statement/lines/51469?page=1"},"custlines":[{"id":"51469Advance25-JUN-15ABIN MIS M F R TRADING","partyId":760002,"registrationId":"51469","registrationDate":"29-Jan-2014","statusFlag":"LE","propertyId":2611,"propertyName":"THE DISTINCTION","buildingId":42467,"building":"THE DISTINCTION","unitId":42625,"unitNumber":"BD3/34/3407","orgId":81,"docType":"Advance","docDate":"24-Jun-2015","dueDate":null,"description":"ABIN MIS M F R TRADING","currency":"AED","debit":0,"credit":1709,"vatCharged":null,"vatPaid":0}]}';
            res.setBody(strBody);
        }
        return res;
    }
}