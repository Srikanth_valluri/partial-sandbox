/*
* Description - Test class developed for 'ApprovalProcessForRentalPoolCases'
*
* Version            Date            Author            Description
* 1.0               28/02/2018       Ashish           Initial Draft
*/

@isTest 
private class ApprovalProcessForRentalPoolCasesTest {

    static testMethod void testSubmitCasesForApproval() {
        // TO DO: implement unit test
        
        UserRole objUserRole = [Select Id from UserRole where name = 'Collection - CRE'];
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        
        User usr1 = createTestUser(objUserRole.Id , pf.Id, 'Test FirstName1', 'Test LastName1',null);
        insert usr1;
        
        User usr2 = createTestUser(objUserRole.Id , pf.Id, 'Test FirstName', 'Test LastName',usr1);
        usr2.ManagerId = usr1.Id;
        insert usr2 ; 
        
        System.runAs(usr2) {
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc;
            
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() );
            objCase.OwnerId = usr2.Id;
            insert objCase ;
            
            Task objTask = TestDataFactory_CRM.createTask( (sObject)objCase , 'Collect Rental Pool Agreement(RPA) from customer', 'CRE', 'Rental Pool Termination', system.today().addDays(1) );
            objTask.Status = 'Closed';
            insert objTask;
            
            test.startTest();
                ApprovalProcessForRentalPoolCases.submitCasesForApproval( new list<Task> { objTask } );
            test.stopTest();
        
        }
    }
    
    /* * * * * * * * * * * * *
    *  Method Name:  createTestUser
    *  Purpose:      This method is used to create test user
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 20-Feb-2018
    * * * * * * * * * * * * */
    public static User createTestUser(Id roleId , Id profID, String fName, String lName, User objUser)
    {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
       
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User tuser = new User(  firstname = fName,
                                lastName = lName,
                                email = uniqueName + '@test' + orgId + '.org',
                                Username = uniqueName + '@test' + orgId + '.org',
                                EmailEncodingKey = 'ISO-8859-1',
                                Alias = uniqueName.substring(18, 23),
                                TimeZoneSidKey = 'America/Los_Angeles',
                                LocaleSidKey = 'en_US',
                                LanguageLocaleKey = 'en_US',
                                ProfileId = profId,
                                UserRoleId = roleId
                             );
        if(objUser != null)
        {
            tuser.ManagerId = objUser.Id;
        }
        return tuser;
    }

}