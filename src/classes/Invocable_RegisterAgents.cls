/**************************************************************************************************
* Name               : Invocable_RegisterAgents                                                   *
* Test Class         : Invocable_RegisterAgents_Test                                              *
* Description        :                                                                            *
* Created Date       :                                                                            *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION   AUTHOR          DATE            COMMENTS                                              *
* 1.0                                       Initial Draft.                                        *
* 1.1       QBurst          20/04/2020      Agent Owner change only for Corporates                *
**************************************************************************************************/
global without sharing class Invocable_RegisterAgents {

    private static RecordType RecordTypePA = [Select Id from RecordType Where developerName = 'Individual_Agency' and SObjectType = 'Account'];

    @InvocableMethod
    public static void registerAgents(List<Id> stepId) {
        for(New_Step__c step : [SELECT Id, Service_Request__c FROM New_Step__c WHERE Id IN :stepId]) {
            EvaluateCustomCode(step);
        }
    }

    public static String EvaluateCustomCode(New_Step__c step) {
        String retStr = 'Success';
        String strCurrSR = '';
        Account newAcc;
        Account exacc;
        List<id> accids = new List<id>();
        Map<String, Amendment__c> amdMap = new Map<String, Amendment__c>();
        Map<String, Contact> conMap = new Map<String, Contact>();

        try {
            Boolean bankchange = false;
            NSIBPM__Service_Request__c currSR = CC_RegisterAgents.getSRDetails(step.Service_Request__c);
            if(currSR.NSIBPM__Customer__c !=null){

                exacc =[Select Id, Name,Vendor_ID__c,Bank_Name__c,Bank_Country__c,
                        Bank_Branch__c,Bank_City__c,Bank_Account_Currency__c,
                        Bank_Account_Number__c,IBAN_Number__c,Swift_Code__c,
                        Sort_Code__c,IFSC_Code__c 
                        FROM Account 
                        WHERE id =:currSR.NSIBPM__Customer__c];
                system.debug('>>>>exacc.Bank_Name__c'+exacc.Bank_Name__c);
                system.debug('>>>>currSR.Bank_Name__c'+currSR.Bank_Name__c);

                if(exacc.Bank_Name__c!= currSR.Bank_Name__c)
                    bankchange=true;
                else if(exacc.Bank_Country__c!= currSR.Account_Details_Country__c)
                    bankchange=true;
                else if(exacc.Bank_Branch__c!= currSR.Bank_Branch__c)
                    bankchange=true;
                else if(exacc.Bank_City__c!= currSR.Bank_City__c)
                    bankchange=true;
                else if(exacc.Bank_Account_Currency__c!= currSR.Bank_Account_Currency__c)
                    bankchange=true;
                else if(exacc.Bank_Account_Number__c!= currSR.Account_Number__c)
                    bankchange=true;
                else if(exacc.IBAN_Number__c!= currSR.IBAN_Number__c)
                    bankchange=true;
                else if(exacc.Swift_Code__c!= currSR.Swift_Code__c)
                    bankchange=true;
                else if(exacc.Sort_Code__c!= currSR.Sort_Code__c)
                    bankchange=true;
                else if(exacc.IFSC_Code__c!= currSR.IFSC_Code__c)
                    bankchange=true;

            }
            List<Amendment__c> amd_List =  CC_RegisterAgents.getAllAmendments(currSR.Id);
            String currentSrId = step.Service_Request__c;
            //AmendmentID -->Amendment
            for (Amendment__c amd : amd_List) {
                amdMap.put(amd.Id, amd);
            }

            //Cretae portal user/contacts for Corporate Agency SRs
            if (currSR.Agency_type__c == 'Corporate') {
                System.debug('==>Corporate Agency');
                //newAcc = createAccount(currSR);
                id corporrateRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
                newAcc = UpsertAccount(currSR,corporrateRecordTypeId,false);
                System.debug('==>newAcc'+ newAcc);
                //Update SR to lookup on account
                if(newAcc != null){
                    currSR.NSIBPM__Customer__c = newAcc.Id;
                    update currSR;
                    // Stamp the Account on the Inquiry related to the SR 
                    if (currSR.Inquiry__c != null) {
                        Inquiry__c relatedInquiry = new Inquiry__c(
                            Id = currSR.Inquiry__c, 
                            Agency_Name__c = newAcc.Id,
                            Bypass_Inquiry_Status__c = true,
                            Inquiry_Status__c = 'Agent Converted'
                        );
                        update relatedInquiry;
                        
                    }
                    if(currSR.Record_Type_Name__c == Label.ManageAgencySiteInsert)
                        SRUtility.CreateChatterGroup(new map<id,account>{newAcc.id => newAcc});
                    //Create Contacts
                    List<Contact> newCon_List = createContacts(currSR, newAcc, amdMap);
                    for (Contact con :  newCon_List) {
                        conMap.put(con.AmendmentID__c, Con);
                    }
                    //Update amendment Acc/Contact lookups
                    amd_List = updateAmendmentLookups(amd_List, newAcc, conMap);
                    System.debug('amd_List==>>' + amd_List);
                    System.enqueueJob(new CC_RegisterAgents(currSR.Id));
                }
            } else {
                System.debug('==>Individual Agency');
                String keyVal = currSR.First_Name__c + currSR.Last_Name__c + currSR.Agency_Mobile__c;
                System.debug('keyVal ==>'+keyVal);
                if(keyVal != NULL && keyVal != '')
                    if (!checkForDuplicatePersonAccounts(keyVal,currSR.NSIBPM__Customer__c)) {
                        System.debug('==>No Duplicate');
                        //newAcc =  createPersonAccount(currSR);
                        newAcc=UpsertAccount(currSR,RecordTypePA.Id,true);
                        if(newAcc != null){
                            currSR.NSIBPM__Customer__c = newAcc.Id;
                            for(user u: [select id,name,profile.name,Profile.UserLicense.Name,contactid from user where id = : currSR.createdbyid]){
                                if(u.profile.name == 'Property Consultant' 
                                    && currSR.Record_Type_Name__c == 'Agent_Registration'){
                                    CC_RegisterAgents.addpctoAccount(currSR.NSIBPM__Customer__c,u.id,u.name);
                                }
                            }
                            update currSR;
                            // Stamp the Account on the Inquiry related to the SR 
                            if (currSR.Inquiry__c != null) {
                                Inquiry__c relatedInquiry = new Inquiry__c(
                                    Id = currSR.Inquiry__c, 
                                    Agency_Name__c = newAcc.Id,
                                    Bypass_Inquiry_Status__c = true,
                                    Inquiry_Status__c = 'Agent Converted'
                                );
                                update relatedInquiry;
                            }
                            System.debug('==>' + newAcc);
                        }
                    } else {
                        retStr = 'Duplicate Person Account.';
                        return retstr;
                    }
            }

            if(newAcc.id!=null){
                accids.add(newAcc.id);

            }
            if(accids.size()>0){
                IF(currSR.Record_Type_Name__c == Label.ManageAgencySiteInsert)
                    system.enqueueJob(new AsyncAgentWebservice (accids,'Agent Creation'));
                if(bankchange)
                    system.enqueueJob(new AsyncAgentWebservice (accids,'Bank Updation'));

            }

            if(currSR.NSIBPM__Customer__c != null){
                if(currSR.Record_Type_Name__c == Label.ManageAgencySiteUpdate && currSR.Country_of_Sale__c != null){
                    ManageAgencySites.UpdateAgencySites(currSR.Country_of_Sale__c,currSR.NSIBPM__Customer__c);
                }
            }

        } catch (Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
        }
        return retStr;
    }

    public static Account UpsertAccount(NSIBPM__Service_Request__c currSR, Id recordTypeId, Boolean isPersonAccount) {
        try{
            Account personAcc  = new Account(id=currSR.NSIBPM__Customer__c);

            // v4.0 Craig 25/02/2018  #232
            // If Account exist assigned previous Record Type of Account while upserting
            if (currSR.NSIBPM__Customer__c != null) {
                List<Account> accList = [ SELECT Id, Name, RecordTypeId 
                                            FROM Account 
                                           WHERE Id = :currSR.NSIBPM__Customer__c
                                           LIMIT 1
                ];
                if(!accList.isEmpty()) {
                    personAcc.RecordTypeId = accList[0].RecordTypeId;
                }
            } else {
                personAcc.RecordTypeId = recordTypeId;
            }
           
            system.debug('>>>>personAcc>>>>'+personAcc);
            system.debug('>>>>personAcc.RecorrdTypeId>>>>'+personAcc.RecordTypeId);
            // v4.0 Craig 25/02/2018 

            // Map SR fields to Account Record
            List<Account_SR_Field_Mapping__c> CS = Account_SR_Field_Mapping__c.getAll().values();
            for (Account_SR_Field_Mapping__c mapping : CS) {
                if((isPersonAccount && (mapping.Is_Person_Account__c || mapping.Is_Common_to_All_RT__c)) ||
                   (!isPersonAccount && mapping.Is_Common_to_All_RT__c) ||
                   (!isPersonAccount && !mapping.Is_Person_Account__c && !mapping.Is_Common_to_All_RT__c)){
                   //if(currSR.get(mapping.SR_Field__c) != null){ //ALOK 25/Oct/2017 Commented For Bug #131
                       //system.debug('>>>>>>>>'+mapping.Account_Field__c);
                       //system.debug('>>>>>>>>'+currSR.get(mapping.SR_Field__c));
                       personAcc.put(mapping.Account_Field__c, currSR.get(mapping.SR_Field__c));
                   //}
               }
            }

            system.debug('personAcc>>>>  = '+personAcc.OwnerId);
            system.debug('personAcc>>>>  = '+personAcc.Agency_Type__c);
            system.debug('isPersonAccount>>>>  = '+isPersonAccount);
            String countryName = '';
            if(!isPersonAccount){
                system.debug('Inside>>>>  = ');
                personAcc.put('Name', currSR.get('Agency_Name__c'));
                countryName = currSR.Country_Of_Incorporation_New__c;
            } else {
                countryName = currSR.Country__c;
            }
            if (personAcc.Agency_Type__c == 'Corporate' && personAcc.Id == null ) { // 1.1
            //if (personAcc.Id == null ) 
                // Craig 03-06-2018  > Map Account Owners
                String recordOwnerId = getAgencyOwnerId(
                    currSR.Country__c, 
                    currSR.Agency_Corporate_Type__c,
                    countryName
                );
                if (String.isNotBlank(recordOwnerId)) {
                    personAcc.put('OwnerId', recordOwnerId);
                }
            }

           /*Id corporrateTerminatedRecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency Terminated').getRecordTypeId();
            
            system.debug('personAcc.RecordTypeId>>>>  = '+personAcc.RecordTypeId);
            system.debug('corporrateTerminatedRecordTypeId>>>>  = '+corporrateTerminatedRecordTypeId);
            // v4.0 Mohit  21/02/2018 
            // Restrict the Update of recordtype Corporate Agency
            // Terminated to Corporate Agency
            if(personAcc.RecordTypeId != corporrateTerminatedRecordTypeId) {
                system.debug('INDSIDE IF >>>>  = ');
                personAcc.RecordTypeId = recordTypeId;
            } else { 
                system.debug('INDSIDE ELSE >>>>  = ');
            }
             // v4.0 Mohit  21/02/2018 
            // Restrict the Update of recordtype Corporate Agency
            // Terminated to Corporate Agency
            */
            // personAcc.RecordTypeId = recordTypeId;
            system.debug('personAcc CCRegister >>> '+personAcc.ownerId);
            upsert personAcc ;
            return personAcc;
        }
        catch(exception ex){
            system.debug('Exception at line number = '+ex.getLineNumber()+', Exception message = '+ex.getMessage());
            return null;
        }
    }

    public static List<Contact> createContacts(NSIBPM__Service_Request__c SR, Account newAcc, Map<String, Amendment__c> amdMap) {
        String strCurrSR = '';
        Id agencyContactRTId =
            Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Agency Contact').getRecordTypeId();
        Id SRId = SR.Id;
        Id srOwnerId = SR.OwnerId;
        System.debug('srOwnerId==>' + srOwnerId);
        List<Contact> conList = new List<Contact>();
        Contact newCon;
        System.debug('SR==>' + SRId);
        List<User> srOwnerList = [SELECT Id, Name, IsPortalEnabled, User_License_type__c 
                                    FROM User 
                                   WHERE Id = :srOwnerId LIMIT 1];
        
        for (Amendment__c amd : amdMap.values()) {
            newCon = new Contact(id = amd.Contact__c);
            List<User> userList = new List<User>();
            //Added By Twinkle to Avoid Owner Update If the Owner is a Queue
            if(srOwnerList != null && !srOwnerList.isEmpty()) {
                System.debug('srOwnerList==> ' + srOwnerList);
                System.debug('srOwnerList[0].IsPortalEnabled ==> ' + srOwnerList[0].User_License_type__c );
                System.debug('srOwnerList[0].IsPortalEnabled ==> ' + srOwnerList[0].IsPortalEnabled );
                if (srOwnerList[0].IsPortalEnabled == true 
                    || srOwnerList[0].User_License_type__c == 'Guest User License') {
                    if (amd.Contact__c == null) {
                        // Craig 03-06-2018  > Map Account Owners
                        system.debug('Country_Of_Incorporation_New__c>>>>  = ' + SR.Country_Of_Incorporation_New__c); 
                        String recordOwnerId = getAgencyOwnerId(
                            SR.Country__c, 
                            SR.Agency_Corporate_Type__c,
                            SR.Country_Of_Incorporation_New__c
                        );
                        System.debug('recordOwnerId ==> ' + recordOwnerId );
                        if (String.isNotBlank(recordOwnerId)) {
                            newCon.OwnerId = recordOwnerId;
                        }
                        /*if (SR.Country__c == 'United Arab Emirates') {
                            // v3.0 Craig Lobo 04/02/2018 
                            // Moved the static text of Agency Owner names to Labels
                            userList = [SELECT Id, Name FROM User WHERE Name = :agencyOwnerNameUAE LIMIT 1];
                        } else {
                            // v3.0 Craig Lobo 04/02/2018 
                            // Moved the static text of Agency Owner names to Labels
                            userList = [SELECT Id, Name FROM User WHERE Name = :agencyOwnerNameNonUAE LIMIT 1];
                        }
                        if (userList != null && !userList.isEmpty()) {
                            newCon.OwnerId = userList[0].Id;
                        }*/
                    }
                } else if (srOwnerList[0].IsPortalEnabled == false) {
                    if (amd.Contact__c == null) {
                        newCon.OwnerId = srOwnerId;
                    }
                }
           }
            newCon.FirstName =  amd.First_Name__c;
            newCon.LastName = amd.Last_Name__c;
            newCon.AccountId = newAcc.Id;
            newCon.Email = amd.Email__c;
            newCon.ID_Issue_Date__c = amd.ID_Issue_Date__c;
            newCon.MobilePhone = amd.Mobile__c;
            newCon.Shareholding__c = amd.Shareholding__c;
            newCon.Designation__c = amd.Designation__c;
            newCon.Country_Of_Origin__c = amd.Nationality__c;
            newCon.Salutation = amd.Title__c;
            newCon.Date_of_Birth__c = amd.BirthDate__c;
            newCon.Birthdate = amd.BirthDate__c;
            newCon.ID_Number__c = amd.ID_Number__c;
            newCon.AmendmentID__c = amd.Id;
            newCon.ID_Type__c = amd.ID_Type__c;
            newCon.Broker_Card_Expiry_Date__c = amd.Broker_Card_Expiry_Date__c;
            newCon.Broker_Card_Number__c = amd.Broker_Card_Number__c;
            newCon.Is_Eligible_To_Sell__c = amd.Is_Eligible_To_Sell__c;
            newCon.Agent_Representative__c = amd.Agent_Representative__c;
            newCon.Owner__c = amd.Owner__c;
            newCon.Mobile_Country_Code__c = amd.Mobile_Country_Code__c;
            newCon.Id_Expiry_date__c = amd.ID_Expiry_Date__c;
            newCon.Authorised_Signatory__c = amd.Authorised_Signatory__c;
            newCon.Portal_Administrator__c = amd.Portal_Administrator__c;
            if(amd.Mark_for_Deletion__c)
                newCon.Status__c = 'Cancelled';
            newCon.RecordTypeId  = agencyContactRTId;
            conList.add(newCon);
        }
        System.debug('conList==>' + conList);
        upsert conList;
        System.debug('AFTER conList==>' + conList);
        return conList;
    }

    public static List<Amendment__c> updateAmendmentLookups(List<Amendment__c> amd_List, Account acc, Map<String, Contact> conMap) {
        List<Amendment__c> amdUpdateList = new List<Amendment__c>();
        for (Amendment__c amd :  amd_List) {
            amd.Account__c = acc.Id;
            amd.Contact__c = conMap.get(amd.Id).Id;
            amdUpdateList.add(amd);
        }
        update amdUpdateList;
        return amdUpdateList;
    }

    public static boolean checkForDuplicatePersonAccounts(String keyVal,id accID) {
        Boolean flag ;
        String strCurrSR = '';
        List<Account> acc_List = new List<Account>();
        if(accID != null){
            for(Account a : [SELECT Id, RecordType.developerName FROM Account
                             WHERE RecordType.developerName = 'Individual_Agency' AND
                                   PersonAccountKey__c =: keyVal AND
                                   PersonAccountKey__c != NULL AND
                                   Id != : accID LIMIT 1] ){
                acc_List.add(a);
            }
        }else{
            for(Account a : [SELECT Id, RecordType.developerName FROM Account
                             WHERE RecordType.developerName = 'Individual_Agency' AND
                                   PersonAccountKey__c =: keyVal AND
                                   PersonAccountKey__c != NULL LIMIT 1] ){
                acc_List.add(a);
            }
        }
        flag = (acc_List.Size() > 0) ? true : false;
        return flag;
    }

    public static String getAgencyOwnerId(String pAgencyCountry, String pAgencyCorporateType, String pCountryOfIncorp) {
        String agencyOwnerId;
        String agencyOwnerName;

        Agency_Owner__c agencyOwnerRec;
        System.debug('pCountryOfIncorp==>' + pCountryOfIncorp);
        if (String.isNotBlank(pCountryOfIncorp)) {
            agencyOwnerRec = Agency_Owner__c.getInstance(pCountryOfIncorp);
            //agencyOwnerRec = Corporate_Agency_Owner__c.getInstance('Incorp_India');
            //agencyOwnerName = agencyOwnerRec.User_Name__c;
        }  
        System.debug('agencyOwnerRec==>' + agencyOwnerRec);
        if (agencyOwnerRec == null) {   
        System.debug('agencyOwnerRecCalled=='); 
            if (pAgencyCorporateType == 'Real Estate') {
                System.debug('Real Estate1==');
                // Real Estate
                if (pAgencyCountry == 'United Arab Emirates') {
                    System.debug('United Arab Emirates1==');
                    agencyOwnerRec = Agency_Owner__c.getInstance('Real_Estate_UAE');
                    System.debug('agencyOwnerRec635==>' + agencyOwnerRec);
                    //agencyOwnerName = agencyOwnerRec.User_Name__c;
                    //agencyOwnerName = Label.Owner_Real_Estate_UAE;
                } else {
                    agencyOwnerRec = Agency_Owner__c.getInstance('Real_Estate_Non_UAE');
                    System.debug('agencyOwnerRec640==>' + agencyOwnerRec);
                    //agencyOwnerName = agencyOwnerRec.User_Name__c;
                    //agencyOwnerName = Label.Owner_Real_Estate_Non_UAE;
                }
            } else if (pAgencyCorporateType != 'Real Estate') {
                System.debug('pAgencyCorporateType==>');
                // Non Real Estate 
                if (pAgencyCountry == 'United Arab Emirates') {
                    agencyOwnerRec = Agency_Owner__c.getInstance('Non_Real_Estate_UAE');
                    System.debug('agencyOwnerRec648==>' + agencyOwnerRec);
                    //agencyOwnerName = agencyOwnerRec.User_Name__c;
                    //agencyOwnerName = Label.Owner_Non_Real_Estate_UAE;
                } else {
                    agencyOwnerRec = Agency_Owner__c.getInstance('Non_Real_Estate_Non_UAE');
                    System.debug('agencyOwnerRec653==>' + agencyOwnerRec);
                    //agencyOwnerName = agencyOwnerRec.User_Name__c;
                    //agencyOwnerName = Label.Owner_Non_Real_Estate_Non_UAE;
                }
            }
        }


        system.debug('agencyOwnerName>>>>  = '+agencyOwnerName);
        if (agencyOwnerRec != null 
            && String.isNotBlank(agencyOwnerRec.User_Name__c)
        ) {
            List<User> userList = [ SELECT Id, Name 
                                      FROM User 
                                     WHERE Name = :agencyOwnerRec.User_Name__c 
                                     LIMIT 1];
            system.debug('agencyOwnerName>>>>  = ' + userList);
            if (userList != null && !userList.isEmpty()) {
                agencyOwnerId = userList[0].Id;
                system.debug('agencyOwnerId>>>>  = '+agencyOwnerId);
            }
        }
        return agencyOwnerId;
    }
}