/*--------------------------------------------------------------------------------------------------*
* Name               : AgentPortalAnnounsmentControllerTest                                         *
* Description        : Test class for AgentPortalAnnounsmentController                              *
* Created Date       : 2/09/2017                                                                    *
* Created By         : Naresh Kaneriya                                                              *
* Last Modified Date :                                                                              *
* Last Modified By   :                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR                    DATE                                                        *
* 1.0         Naresh                 02-09-2017                                                     *
* 2.0         Craig                  28-03-2018                                                     *
----------------------------------------------------------------------------------------------------*/

@isTest(SeeAllData=false)
public class AgentPortalAnnounsmentControllerTest{
    
  

  public static testMethod void AgentPortalAnnounsmentTest(){
      
       Account adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        insert adminAccount;
        
       Contact adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        insert adminContact;
        
      Contact  agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
        insert agentContact;
        
      User  portalUser = InitialiseTestData.getPortalUser('test21@test.com', adminContact.Id, 'Admin');
      User  portalOnlyAgent = InitialiseTestData.getPortalUser('test1@test.com', agentContact.Id, 'Agent');
      
      AgentPortalController Controller ;
      
     // AgentPortalAnnounsmentController  objController = new AgentPortalAnnounsmentController(Controller);
      AgentPortalAnnounsmentController  obj = new AgentPortalAnnounsmentController();
      AgentPortalAnnounsmentController.AnnounsmentCount = 1;
      AgentPortalAnnounsmentController.NotificationCount = 1.0;
      AgentPortalAnnounsmentController.EventsCount = 1.0;
      AgentPortalAnnounsmentController.UserTier = 'test';
      AgentPortalAnnounsmentController.totalnotification =2.0;
      obj.UserProfile = 'UserProfile'; 
      obj.UserCreatedDate = System.now(); 
      obj.agentCommissionList = null; 
      obj.totalUnitPrice = 3.0; 
      obj.totalCommissionAmount = 3.4; 
      obj.fieldSetSize = 3; 
      obj.uRec = portalUser; 
      obj.acc = adminAccount;
      obj.con = agentContact; 
      obj.showheader = true; 
      obj.notification= null;
      obj.accountId = adminAccount.id; 
      obj.contactInfo  = agentContact; 
  
      obj.getNotification();
      obj.getAnnouncementRequestDetail();
      obj.beginning();
      obj.previous();
      obj.next();
      obj.end();
      obj.getprev();
      obj.redirect();
      obj.getnxt();
      
      ApexPages.currentPage().getParameters().put('nid', '01p0Y000009neJwQAI');
      obj.viewNotificationDetail();
      ApexPages.currentPage().getParameters().put('notificationId', '01p0Y000009neJwQAI');
      obj.viewCurrentNotificationDetail();
      obj.readNotification('1234567');
      
  
  } 
  
  
  public static testMethod void AgentPortalAnnounsmentTest2(){
              UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
       Account adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        insert adminAccount;
        
       Contact adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
       adminContact.Authorised_Signatory__c = true;
        insert adminContact;
        
      Contact  agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
        insert agentContact;
        
      User  portalUser = InitialiseTestData.getPortalUser('testUser111@test.com', adminContact.Id, 'Admin');
      User  portalOnlyAgent = InitialiseTestData.getPortalUser('test1@test.com', agentContact.Id, 'Agent');
      
    
     System.runAs(portalUser){  
     
      AgentPortalAnnounsmentController  obj = new AgentPortalAnnounsmentController();
     
     }
        }
  
  }
  
  
  
  
}