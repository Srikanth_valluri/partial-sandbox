@isTest
public class AOPT30thDayNotificationTest {
    
    public static testmethod void testAOPT30thDayNotification() {
        Case objCase = new Case();
        objCase.Account_Email__c = 'test@test.com';
        objCase.Is_21st_Day__c = true;
        insert objCase;
        
        AOPT30thDayNotification.checkIfAddendumUploaded(new List<Case>{objCase});
    }
}