/*
* Class Name    :Damac_SalesTourController
* Functionality : To get the attendees based on sales tour and to create the attendees for sales tour
*/

public class Damac_SalesTourController{

    public List<SelectOption> attendeeRecords { get; set; }
    public List<String> selectedAttendees { get; set; }
    public Meeting_Attendees__c otherAttendee { get; set; }
    public List<Meeting_Attendees__c> taskAttendees { get; set; }

    public Damac_SalesTourController(ApexPages.standardController stdController) {
        attendeeRecords = new List<SelectOption>();
        selectedAttendees = new List<String>();
        otherAttendee = new Meeting_Attendees__c();
        taskAttendees = new List<Meeting_Attendees__c>();
        
        ID salesTourId = Apexpages.currentpage().getparameters().get ('id');
        Apexpages.currentpage().getparameters().put('taskID', salesTourId);
        // To get the existing attendees
        Damac_ActivityManager obj = new Damac_ActivityManager ();
        obj.currentTaskAttendees();
        obj.attendees();
        attendeeRecords = obj.attendeeRecords;
        taskAttendees = obj.taskAttendees;
    
    }
    
    // Method to create the attendees for the sales tour which is editing
    public void createAttendees() {
        ID taskID = Apexpages.currentpage().getparameters().get('taskID');
        String attendeeIds = apexpages.currentpage().getparameters().get('attendeesId');
        String otherAttendeeVal = apexpages.currentpage().getparameters().get('otherAttendee');
        
        Activity_Manager__c manager = new Activity_Manager__c();
        manager = [SELECT ID FROM Activity_Manager__c WHERE Activity_or_Task_Id__c =: taskID];
        List<ID> existingAttendees = new List<ID>();
        for (Meeting_Attendees__c obj :[SELECT Attendee__c FROM Meeting_Attendees__c WHERE Activity_Manager__c =: manager.ID]) {
            existingAttendees.add(obj.Attendee__c);
        }

        attendeeIds = attendeeIds.removeEnd(',');
        SET <ID> attendeeIdsList = new SET<ID>();
        System.Debug(attendeeIds);
        if (attendeeIds != '' && attendeeIds != NULL) {
            if (attendeeIds.contains(',')) {
                for (String key :attendeeIds.split (','))
                    attendeeIdsList.add(key);
            }
            else
                attendeeIdsList.add(attendeeIds);
        }
        List<Meeting_Attendees__c> attendeesList = new List<Meeting_Attendees__c>();
        for (ID key :attendeeIdsList) {
            if (key != NULL) {
                if (!existingAttendees.contains(key)) {
                    Meeting_Attendees__c obj = new Meeting_Attendees__c();
                    obj.Activity_Manager__c = manager.ID;
                    obj.Attendee__c = key;
                    attendeesList.add(obj);
                }
            }
        }

        if (otherAttendeeVal != NULL && otherAttendeeVal != '') {
            Meeting_Attendees__c obj = new Meeting_Attendees__c();
            obj.Activity_Manager__c = manager.ID;
            obj.Others__c = otherAttendeeVal;
            attendeesList.add(obj);
        }
        if (attendeesList.size() > 0) {
            insert attendeesList;
            otherAttendee = new Meeting_Attendees__c();
        }
    }
}