@isTest
public without sharing Class FinancePagePicklistCtrlTest {
     @isTest
    static void testAccountDetail(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Booking__c = objBooking.Id;
        insert objBU;
        
        Case objCase = new Case();
        objCase.AccountId = objCase.Id;
        insert objCase;
        
        SR_Booking_Unit__c objSRBooking = new SR_Booking_Unit__c();
        objSRBooking.Case__c = objCase.Id;
        insert objSRBooking;
        
        POP_Bank_Details__c objPOP = new POP_Bank_Details__c();
        objPOP.Case__c = objCase.Id;
        insert objPOP;
        List<POP_Bank_Details__c>  csList = new List<POP_Bank_Details__c>();
        PageReference financePicklistPop = Page.FinancePicklistPop;
        Test.setCurrentPage(financePicklistPop);
        ApexPages.currentPage().getParameters().put('id', objCase.Id);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(csList);
        FinancePagePicklistCtrl lstController = new FinancePagePicklistCtrl(stdSetController);
        lstController.selectedOption = '12345'+'/'+'Test Bank'+'/'+'Test Benificary';
        lstController.saveDetails();
    }
}