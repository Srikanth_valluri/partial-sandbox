/*
* This is the Test calss of DPCallingHandler.
*
*  Revision History: 
*  Version     Author          Date             Description 
*  1.0                        18/01/2018      Initial Draft          
*  1.1         Arjun Khatri     30/01/2018    1.Added test method to test SecureSmsGenericCRM
*/
@isTest
private class DPCallingHandlerTest {
    //static Id DPcallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('DP Calling List').RecordTypeId;
    static Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
    
       @testSetup()
      private static  void testData(){
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        List<Calling_List__c>callingLst = new List<Calling_List__c>();
        Payment_Plan__c payment = new Payment_Plan__c(Status__c = 'Active');
        insert payment;
        Payment_Terms__c paymentTerm = new Payment_Terms__c(Percent_Value__c = '10',
                                  Payment_Plan__c = payment.id,
                                  Installment__c = 'DP');
        insert paymentTerm;
        Calling_List__c dpCallInst = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                       Calling_List_Type__c  = 'DP Calling',
                                                       Customer_Flag__c = true,
                                                       Registration_ID__c = '86466',
                                                       Party_ID__c = '1177911',
                                                       Payment_Plan__c =payment.id );
         callingLst.add(dpCallInst);
         Calling_List__c dpCallInst2 = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                       Calling_List_Type__c  = 'DP Calling',
                                                       Customer_Flag__c = true,
                                                       Registration_ID__c = '8646',
                                                       Party_ID__c = '117791',
                                                       Payment_Plan__c =payment.id );
         callingLst.add(dpCallInst2);
         insert callingLst;
      }
      private static testMethod void  unitTest(){
        List<Calling_List__c>CallingLstToPass = new List<Calling_List__c>();
        CallingLstToPass = [SELECT Id,
                       RecordTypeId,
                       Customer_Flag__c,
                       Registration_ID__c,
                       Party_ID__c,
                       Payment_Plan__c,Calling_List_Type__c  
                     FROM Calling_List__c];
        DPCallingHandler.UpdateDpPercentOnCallingList(CallingLstToPass);
      }
    @isTest
    public static void testSecureSmsGenericCRM() {
        
        //Create account
        Account objAccount = createAccount();
        System.debug('test account = ' + objAccount);
        
        //Create Service Request
        NSIBPM__Service_Request__c objDealSr = createDealSR();

        //Create Booking
        Booking__c objBooking = createBooking(objAccount.Id, objDealSr.Id);

        //Create Booking Unit
        Booking_Unit__c objBU = createBookingUnit(objBooking.Id,'test Hod');

        //create calling list
        Calling_List__c objCL = createCallingList(objAccount.Id, objBU.Id);
        Test.setMock(HttpCalloutMock.class, new MockHttpSecureSMSGenericCRM());
        Test.startTest();
        insert objCL;
        Test.stopTest();
    }
    @isTest
    public static void testSendEMail() {
        
        //Create account
        Account objAccount = createAccount();
        System.debug('test account = ' + objAccount);
        
        //Create Service Request
        NSIBPM__Service_Request__c objDealSr = createDealSR();

        //Create Booking
        Booking__c objBooking = createBooking(objAccount.Id, objDealSr.Id);

        //Create Booking Unit
        Booking_Unit__c objBU = createBookingUnit(objBooking.Id,'test Hod');

        //create calling list
        Calling_List__c objCL = createCallingList(objAccount.Id, objBU.Id);
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        Test.startTest();
        insert objCL;
        Test.stopTest();
    }
    
    static Account createAccount() {
         Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        //Create Customer record
        Account objAcc = new Account(FirstName='Test FirstName'
                                    , LastName='Test LastName'
                                    , Email__pc = 'test123@tmail.com'
                                    , recordTypeId = personAccRTId);
        insert objAcc;
        return objAcc;
    }

    Static NSIBPM__Service_Request__c createDealSR() {
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
        return objSR;
    }

    Static Booking__c createBooking(Id accId, Id SRId) {
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = accId;
        objBooking.Deal_SR__c = SRId;
        insert objBooking;
        return objBooking;

    }

    static Booking_Unit__c createBookingUnit(Id bookingId, String HodName) {
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/001';
        objBookingUnit.Booking__c  = bookingId;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Hod_name__c = HodName;
        insert objBookingUnit;
        System.debug('test objBookingUnit = ' + objBookingUnit);
        return objBookingUnit;
    }

    static Calling_List__c createCallingList(Id accId, Id BUId) {

        Id collectioncallingRecordTypeId =
            Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get(
            'Collections Calling List').RecordTypeId;
        System.debug('test FMCollectionRecordTypeId = ' + collectioncallingRecordTypeId);
        Calling_List__c callListObj = new Calling_List__c(Account__c = accId,
                                                        Booking_Unit__c = BUId,
                                                        recordTypeId = collectioncallingRecordTypeId,
                                                        Calling_List_Type__c = 'DP Calling',
                                                        Calling_List_Status__c = 'New',
                                                        Mobile_Phone__c = '8877665542',
                                                        Email__c = '1test1@gmail.com',
                                                        Customer_Name__c = 'test customer',
                                                        Unit_Name__c = 'JNU/ABC/001');
        //insert callListObj;
        return callListObj;
    }
}