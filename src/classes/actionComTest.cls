/************************************************************************************
* Description - Test class developed for 'actionCom'
*
* Version            Date            Author            Description
* 1.0                16/11/17        Naresh (Accely)           Initial Draft
***********************************************************************************/

@isTest(SeeAllData=false)
public class actionComTest{
    
  public static testMethod void WebServiceTest (){
      
             actionCom.COCDHttpSoap11Endpoint obj =  new actionCom.COCDHttpSoap11Endpoint();
             String resp ;   
        
            Test.startTest();
            SOAPCalloutServiceMock.returnToMe = new Map<String, actionCom.COCDFinancialsResponse_element>();
            actionCom.COCDFinancialsResponse_element response = new actionCom.COCDFinancialsResponse_element();
            response.return_x = 'S';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        
            resp = obj.COCDFinancials('PartyId','SourceofRequest','ProcessName','SubProcesName','ProjectCity','Project',
                                'BuildingCode','PermittedUse','BedroomType','ReadyOffPlan',
                                'CustomerType','ApplicableNationality','POA');   
            System.assertEquals('S',resp);                    
            Test.stopTest();
   }

  public static testMethod void COCDFinancialsTest(){
            actionCom.COCDHttpSoap11Endpoint obj =  new actionCom.COCDHttpSoap11Endpoint();
            String resp ;   
        
            Test.startTest();
            SOAPCalloutServiceMock.returnToMe = new Map<String, actionCom.COCDDocumentationResponse_element  >();
            actionCom.COCDDocumentationResponse_element  response = new actionCom.COCDDocumentationResponse_element ();
            response.return_x = 'S';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response);
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock() );
        
            resp = obj.COCDDocumentation('RegistrationId', 'SourceofRequest', 
                                          'ProcessName', 'SubProcesName', 'ProjectCity', 'Project', 'BuildingCode', 'PermittedUse', 
                                           'BedroomType', 'ReadyOffPlan', 'CustomerType', 'ApplicableNationalityNew', 'UnderAssignment', 'POA');
          
             System.assertEquals('S',resp); 
             Test.stopTest();
   }
   
     public static testMethod void UpdateCOCDTest(){
             actionCom.COCDHttpSoap11Endpoint obj =  new actionCom.COCDHttpSoap11Endpoint();
             String resp ;   
        
            Test.startTest();
            SOAPCalloutServiceMock.returnToMe = new Map<String, actionCom.UpdateCOCDResponse_element   >();
            actionCom.UpdateCOCDResponse_element   response = new actionCom.UpdateCOCDResponse_element  ();
            response.return_x = 'S';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response);
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock() );
            beanComXsd.COCDRequestMessage objCOCDRequestMessage  =  new  beanComXsd.COCDRequestMessage();
            resp = obj.UpdateCOCD(objCOCDRequestMessage);
            System.assertEquals('S',resp);
            Test.stopTest();
   }
  
  
  
     
     public static testMethod void generateCOCDTest(){
             actionCom.COCDHttpSoap11Endpoint obj =  new actionCom.COCDHttpSoap11Endpoint();
             String resp ;   
        
            Test.startTest();
            SOAPCalloutServiceMock.returnToMe = new Map<String, actionCom.generateCOCDResponse_element    >();
            actionCom.generateCOCDResponse_element    response = new actionCom.generateCOCDResponse_element   ();
            response.return_x = 'S';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response);
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock() );
            
            resp = obj.generateCOCD('P_REQUEST_NUMBER', 'P_REQUEST_NAME', 'P_SOURCE_SYSTEM', 'IPMSPartyID', 'Address1', 
                                          'Address2', 'Address3', 'Address4', 'city', 'state', 'postalCode', 'country', 'MobileCountryCode', 
                                          'MobileAreaCode', 'MobileNumber', 'PhoneCountryCode', 'PhoneAreaCode', 'PhoneNumber', 'FaxCountryCode', 
                                          'FaxAreaCode', 'FaxNumber', 'EmailAddress', 'PassportIssueDate', 'PassportNumnber');
        
            System.assertEquals('S',resp);
            Test.stopTest();
   }
     
     public static testMethod void COCDApprovalsRequiredTest(){
             actionCom.COCDHttpSoap11Endpoint obj =  new actionCom.COCDHttpSoap11Endpoint();
             String resp ;   
        
            Test.startTest();
            SOAPCalloutServiceMock.returnToMe = new Map<String, actionCom.COCDApprovalsRequiredResponse_element>();
            actionCom.COCDApprovalsRequiredResponse_element response = new actionCom.COCDApprovalsRequiredResponse_element    ();
            response.return_x = 'S';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response);
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock() );
            
            resp = obj.COCDApprovalsRequired('RegistrationId', 'SourceofRequest', 'ProcessName', 'SubProcesName', 'ProjectCity', 
                                               'Project', 'BuildingCode', 'PermittedUse', 'BedroomType', 'ReadyOffPlan', 'CustomerType', 
                                               'ApplicableNationality', 'UnderAssignment', 'POA');
        
            System.assertEquals('S',resp);
            Test.stopTest();
   }
   
   public static testmethod void CreateDebitCreditMemoTest(){
       
           actionCom.COCDHttpSoap11Endpoint obj = new actionCom.COCDHttpSoap11Endpoint();
           
           Test.startTest();
           SOAPCalloutServiceMock.returnToMe = new Map<String,actionCom.CreateDebitCreditMemoResponse_element>();
           actionCom.CreateDebitCreditMemoResponse_element response_x = new actionCom.CreateDebitCreditMemoResponse_element();
           response_x.return_x = 'S';
           SOAPCalloutServiceMock.returnToMe.put('response_x', response_x);
           Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock() );
           
           obj.CreateDebitCreditMemo('P_REQUEST_NUMBER', 'P_REQUEST_NAME', 
                                     'P_SOURCE_SYSTEM', 'RegistrationID', 'Amount', 
                                     'TRANS_TYPEClass', 'CALL_TYPE', 
                                     'UniqueTransactionNumber', 'DESCRIPTION');
          Test.stopTest();                             
   }
}