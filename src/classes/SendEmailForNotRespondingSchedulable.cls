public with sharing class SendEmailForNotRespondingSchedulable implements Schedulable  {
 public Set<Id> callingListIds;
 public static String emailSubject;
 public static String emailBody;
 public static List<Attachment> lstAttach;
 public void execute(SchedulableContext ctx) {
       System.debug('callingListIds'+callingListIds);
        if( callingListIds != null && callingListIds.size() > 0 ) {
            SendEmailForNotResponding( callingListIds );
        }
    }
    
    
    @future (callout=true)
    public static void SendEmailForNotResponding( Set<Id> setCallLstId ) {

        list<EmailMessage> lstEmailMsg = new List<EmailMessage>();
        List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
        List<SMS_History__c> lstSMSHist = new List<SMS_History__c>();
        lstAttach = new List<Attachment>();
        String callingListId;

        List<Calling_List__c> lstCallList = [SELECT id,
                                                    Name,
                                                    Booking_Unit__c,
                                                    Booking_Unit__r.Registration_ID__c,
                                                    Booking_Unit__r.Booking__r.Account__c,
                                                    Booking_Unit__r.Booking__r.Account__r.IsPersonAccount,
                                                    Booking_Unit__r.Booking__r.Account__r.Email__pc,
                                                    Booking_Unit__r.Booking__r.Account__r.Email__c,
                                                    Booking_Unit__r.Booking__r.Account__r.Name,
                                                    Booking_Unit__r.Booking__r.Account__r.Mobile_Phone_Encrypt__pc,
                                                    Call_Outcome__c
                                            FROM Calling_List__c
                                            WHERE id IN : setCallLstId
                                            AND Booking_Unit__c != Null];


        System.debug('lstCallList ::: ' + lstCallList);
        System.debug('lstCallList Size ::: ' + lstCallList.size() );

        List<OrgWideEmailAddress> orgWideAddressHelloDamac = [SELECT  Id
                                                                    ,Address
                                                                    ,DisplayName
                                                               FROM  OrgWideEmailAddress
                                                               WHERE DisplayName = :Label.DAMAC_no_replysf_damacgroup_com ];

        Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;
        System.debug('IsSandbox is: '+isSandbox);

        if(lstCallList.size() > 0) {
            for(Calling_List__c objCallLst : lstCallList) {

                System.debug('Phone Number is: ' + objCallLst.Booking_Unit__r.Booking__r.Account__r.Mobile_Phone_Encrypt__pc);
                //List<Attachment> attach = new List<Attachment>();

                callingListId = objCallLst.Id;

                String emailId = objCallLst.Booking_Unit__r.Booking__r.Account__r.IsPersonAccount ?
                                 objCallLst.Booking_Unit__r.Booking__r.Account__r.Email__pc :
                                 objCallLst.Booking_Unit__r.Booking__r.Account__r.Email__c;

                String mobileNumber = objCallLst.Booking_Unit__r.Booking__r.Account__r.Mobile_Phone_Encrypt__pc;


                //calling new method to set emailBody, Subject & attachment as per Call Outcome value
                //FmIpmsRestCoffeeServices.updateCustomSetting = false;

                EmailContentSelector(objCallLst.Call_Outcome__c, objCallLst);


                /*String subject = 'Test Email for Call outcome - Not Responding Calling List';

                String emailBody = 'Hello '+objCallLst.Booking_Unit__r.Booking__r.Account__r.Name;
                emailBody += '\n This is the test body for Call outcome - Not Responding Calling List';
                emailBody += '\nThank you';*/
                System.debug('Email Id : ' + emailId);


                System.debug('@@@FROM Email Address: ' +  orgWideAddressHelloDamac[0].Address);

                if(String.isNotBlank(emailId) && emailId != '') {



                   if ((isSandbox && (emailId.containsIgnoreCase('@eternussolutions.com')) ) || !isSandbox) {

                        //SendGrid
                        SendGridEmailService.SendGridResponse sendGridResponse = SendGridEmailService.sendEmailService(
                           emailId, objCallLst.Booking_Unit__r.Booking__r.Account__r.Name,
                           '','',
                           '','',
                           emailSubject,'',
                           orgWideAddressHelloDamac[0].Address, '',
                           '','',
                           'text/plain', emailBody,
                           '', lstAttach);

                        if(sendGridResponse.ResponseStatus == 'Accepted') {
                           System.debug('SendGrid response is accepted');
                           EmailMessage mail = new EmailMessage();
                           mail.Subject = emailSubject;
                           mail.MessageDate = System.Today();
                           mail.Status = '3';
                           mail.RelatedtoId = objCallLst.Booking_Unit__r.Booking__r.Account__c; //here it is Account
                           mail.ToAddress = emailId;
                           mail.FromAddress = orgWideAddressHelloDamac[0].Address;
                           mail.TextBody = emailBody;
                           //mail.CcAddress = '';
                           //mail.BccAddress = bccMailAddress[0].Address;
                           mail.Sent_By_Sendgrid__c = true;
                           mail.SentGrid_MessageId__c = sendGridResponse.messageId;
                           mail.Account__c = objCallLst.Booking_Unit__r.Booking__r.Account__c;
                           mail.SendGrid_Status__c = sendGridResponse.ResponseStatus;
                           //insert mail;
                           lstEmailMsg.add(mail);

                        }
                        else {
                            Error_Log__c objError = new Error_Log__c();
                            objError.Account__c = objCallLst.Booking_Unit__r.Booking__r.Account__c;
                            objError.Error_Details__c = 'Unable to send Email to customer when updated Calling List - Call Outcome : Not Resopnding, for : ' + objCallLst.Name;
                            objError.Process_Name__c = 'Calling List';
                            lstErrorLog.add(objError);
                        }

                        FmIpmsRestCoffeeServices.updateCustomSetting = true;
                        FmIpmsRestCoffeeServices.updateBearerToken();//calling updateMethod of Custom Setting after all callouts execution

                    }

                    //SMS History
                    if ((isSandbox && (mobileNumber.containsIgnoreCase('0088888')) ) || !isSandbox) {

                        if(String.isNotBlank(mobileNumber)) {

                            EmailTemplate emtSms = [SELECT Id
                                                          ,DeveloperName
                                                          ,Body
                                                          ,Subject
                                                FROM EmailTemplate
                                                WHERE DeveloperName= 'Calling_List_Not_Responding_SMS'];
                            System.debug('Email TemplateSMS: ' + emtSms);



                            SMS_History__c objSMS = new SMS_History__c();
                            objSMS.Booking_Unit__c = objCallLst.Booking_Unit__c;
                            objSMS.Calling_List__c = objCallLst.Id;
                            objSMS.Customer__c = objCallLst.Booking_Unit__r.Booking__r.Account__c;
                            objSMS.Message__c = emtSms.Body.replace('{!Calling_List__c.Account__c}',objCallLst.Booking_Unit__r.Booking__r.Account__r.Name);
                            objSMS.Phone_Number__c = objCallLst.Booking_Unit__r.Booking__r.Account__r.Mobile_Phone_Encrypt__pc;
                            lstSMSHist.add(objSMS);
                        }
                    }

                }    //End of String.isNotBlank(emailId)
            }    //End of for loop

            System.debug('lstEmailMsg: ' + lstEmailMsg);
            if(lstEmailMsg.size() > 0) {
                insert lstEmailMsg;
            }

            System.debug('lstErrorLog: ' + lstErrorLog);
            if(lstErrorLog.size() > 0) {
                insert lstErrorLog;
            }

            System.debug('lstSMSHist : ' + lstSMSHist);
            if(lstSMSHist.size() > 0) {
                insert lstSMSHist;

                System.debug('lstSMSHist : ' + lstSMSHist);

                for(SMS_History__c objSMStoSend : lstSMSHist) {
                    System.debug('sending SMS');
                    SendSMSQueueable objSMSQue = new SendSMSQueueable(objSMStoSend.Id);
                    Id jobId = System.enqueueJob(objSMSQue);
                    system.debug('jobId' + jobId);

                }

            }
        }

    }
    
    public static void EmailContentSelector(String callOutcome, Calling_List__c objCallingList) {

        if(String.isNotBlank(callOutcome)) {

            if(callOutcome == 'Not Responding') {

                EmailTemplate emt = [SELECT Id
                                           ,DeveloperName
                                           ,Body
                                           ,Subject
                                    FROM EmailTemplate
                                    WHERE DeveloperName= 'CallingList_Not_responding'];
                System.debug('Email Template1: ' + emt);

                emailSubject = emt.Subject.replace('{!Calling_List__c.Account__c}', objCallingList.Booking_Unit__r.Booking__r.Account__r.Name);
                System.debug('Email Subject : ' + emailSubject);
                emailBody = emt.Body.replace('{!Calling_List__c.Account__c}', objCallingList.Booking_Unit__r.Booking__r.Account__r.Name);
                System.debug('Email Body: ' + emailBody);


                List<Attachment> lstAttachTemp = new List<Attachment>();
                lstAttach = lstAttachTemp;

            }
            else if(callOutcome == 'Not Responding - SOA Sent') {

                FmIpmsRestCoffeeServices.updateCustomSetting = false;

                EmailTemplate emt = [SELECT Id
                                           ,DeveloperName
                                           ,Body
                                           ,Subject
                                    FROM EmailTemplate
                                    WHERE DeveloperName= 'CallingList_Not_Responding_with_SOA'];
                System.debug('Email Template1: ' + emt);

                emailSubject = emt.Subject.replace('{!Calling_List__c.Account__c}', objCallingList.Booking_Unit__r.Booking__r.Account__r.Name);
                System.debug('Email Subject : ' + emailSubject);
                emailBody = emt.Body.replace('{!Calling_List__c.Account__c}', objCallingList.Booking_Unit__r.Booking__r.Account__r.Name);
                System.debug('Email Body: ' + emailBody);

                //For SOA attachment
                String pdfFile;
                //FmIpmsRestCoffeeServices.updateCustomSetting = false;

                if(String.isNotBlank(objCallingList.Booking_Unit__r.Registration_ID__c) && objCallingList.Booking_Unit__r.Registration_ID__c != '') {

                    String responseUrl = FmIpmsRestCoffeeServices.getUnitSoa(objCallingList.Booking_Unit__r.Registration_ID__c);
                    System.debug('responseUrl is: '+responseUrl);

                    String accessToken = FmIpmsRestCoffeeServices.accessToken;
                    System.debug('AccessToken is' + accessToken);
                if(String.isNotBlank(responseUrl) && String.isNotBlank(accessToken)) {
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint(responseUrl);
                    req.setHeader('Accept', 'application/json');
                    req.setMethod('GET');
                    req.setHeader('Authorization','Bearer' + accessToken);
                    req.setTimeout(120000);
                    HttpResponse response = new Http().send(req);
                    System.debug('Status Code = ' + response.getStatusCode());
                    System.debug('Pdf Body = ' + response.getBodyAsBlob());

                    if(response.getStatusCode() == 200) {
                        pdfFile = EncodingUtil.base64Encode(response.getBodyAsBlob());

                        Attachment attach = new Attachment();
                        attach.contentType = 'application/pdf';
                        attach.name = 'Unit SOA '+objCallingList.Booking_Unit__r.Registration_ID__c+'.pdf';
                        //temporary
                        /*String myString = 'StringToBlob';
                        Blob myBlob = Blob.valueof(myString);
                        attach.body = myBlob;*/
                        attach.body = response.getBodyAsBlob();

                        List<Attachment> lstAttachTemp = new List<Attachment>();
                        lstAttachTemp.add(attach);
                        lstAttach = lstAttachTemp;


                    }
                }
                }

            }

        }
    }

}