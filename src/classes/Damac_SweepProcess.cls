global class Damac_SweepProcess implements Database.Batchable<sObject> {
    
    global Integer processedInquiries = 0;
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        Date currentDate = Date.Today ();
        String query = 'SELECT Old_Sales_Office__c, Not_Eligible_for_Reassignment__c, CreatedDate, RecordTypeId, Old_Pre_Inquiry_Id__c, Is_Owner_Queue__c, Inquiry_Source__c,'
                        +'Is_Meeting_Confirmed__c, Is_Meeting_scheduled__c, Is_Meeting_Completed__c, Sales_Office__c,Meeting_Due_Date__c, '
                        +'Meeting_Due_Date_Age__c , Meeting_Type__c, Campaign__c, Assigned_PC__c, Pre_InquiryId__c, Inquiry_Status__c, Original_PreInq_Status__c, '
                        +'CreatedDate_Age__c, Sweep_Inquiry__c, Pre_Inquiry_Number__c, Telesales_Executive__c, OwnerId FROM Inquiry__c'
                    +' WHERE Sweep_Criteria_Met__c = TRUE ';
         if (Test.isRunningTest())
            query = 'SELECT CreatedDate_Age__c, Meeting_Due_Date_Age__c, Meeting_Type__c, Sales_Office__c, Pre_InquiryId__c FROM Inquiry__c ';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List <Inquiry__c> scope){
        Set <ID> preInqToChange = new Set <ID> ();
        Set <ID> inqIdsForShare = new Set <ID> ();
        List <Inquiry__c> inquiryList = new List <Inquiry__c> ();
        Id preInqRecId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        Map <String, Sweep_Inquiry_Sales_office__c> salesOffices = new Map <String, Sweep_Inquiry_Sales_office__c > ();
        
        ID defaultOwnerId = [SELECT Id from Group where id =: Label.sweep_Queue_id and Type = 'Queue'].ID;
        for (Inquiry__c inq : Scope)
        {
                 
            inqIdsForShare.add (inq.ID);
            if (inq.Pre_InquiryId__c != NULL) {
                preInqToChange.add (inq.Pre_InquiryId__c);
                inqIdsForShare.add (inq.Pre_InquiryId__c);
            }
            if (salesOffices.containsKey (inq.Sales_Office__c)) {
                inq.OwnerId = salesOffices.get (inq.Sales_Office__c).Owner_id__c;
                inq.Damac_office__c = salesOffices.get (inq.Sales_Office__c).Damac_office__c ;
            } else {
                inq.OwnerId = defaultOwnerId;
            }
            if ( inq.Meeting_Type__c == 'Direct Tour' || ( inq.Meeting_Type__c == 'Scheduled Tour' &&  inq.Assigned_PC__c != null )) {
                inq.Meeting_Type__c = NULL;
                inq.Is_Assigned_Tour__c = true;
            }
            inq.Old_Pre_Inquiry_Id__c = inq.Pre_InquiryId__c;
            inq.Old_Sales_Office__c = inq.Sales_Office__c;
            inq.RecordTypeId = preInqRecId;
            inq.Inquiry_Status__c = 'New';
            inq.Sweep_Inquiry__c = TRUE;
            inq.Telesales_Executive__c = NULL;
            inq.Pre_InquiryId__c = NULL;
            inq.Not_Eligible_for_Reassignment__c = true;
            inq.Pre_Inquiry_Number__c = NULL;
            inq.Assigned_PC__c = NULL;
            inq.Original_PreInq_Status__c = NULL;
            inq.Is_Meeting_scheduled__c = false;
            inq.Is_Meeting_Confirmed__c = false;
            inq.Is_Meeting_Completed__c = false;
            inq.Sales_Office__c = null;
            inq.Meeting_Due_Date__c = null;
            
            inquiryList.add (inq);
            processedInquiries += 1;
        
        }
        //To Change the owner of Preinquiry related to the Inquiry
        if (preInqToChange.size () > 0) {
            List <Inquiry__c> preInquiryList = new List <Inquiry__c> ();
            for (Inquiry__c preInq: [SELECT ownerId FROM Inquiry__c WHERE ID IN: preInqToChange]) {
                preInq.OwnerId = [SELECT Id FROM Group where Name = 'Nurturing for Sweep Queue' and Type = 'Queue'].ID;
                preInquiryList.add (preInq);
            }
            if (preInquiryList.size () > 0) {
                Update preInquiryList;
            }
        }
        if (inquiryList.size () > 0) {
            
            Update inquiryList;
        }
        System.Debug (inqIdsForShare);
        // Removing the telesales sharing for the inquiry and preinquiry
        if (inqIdsForShare.size () > 0) {
            List<Inquiry__Share> sharesToDelete = new List <Inquiry__Share> ();
            Map <ID, Inquiry__Share> userIds = new Map <ID, Inquiry__Share> ();
            
            for (Inquiry__Share rec: [SELECT UserOrGroupId
                                                    FROM Inquiry__Share
                                                    WHERE ParentId IN :inqIdsForShare
                                                    ]) {
                if (rec.UserOrGroupId != NULL) {
                    if (String.valueOf (rec.UserOrGroupId).startsWith ('005'))
                        userIds.put (rec.UserOrGroupId, rec);    
                }
            }
            if (userIds.size () > 0) {
                for (User u: [select Profile.Name from user where id IN: userIds.keySet ()]) {
                    if (u.Profile.Name.contains ('Telesales'))
                        sharesToDelete.add (userIds.get (u.id));
                }
            }
            if(!sharesToDelete.isEmpty()){
                Database.Delete(sharesToDelete, false);
            }
        }
        
        Integer sweepLimit = Label.Sweep_Limit != NULL ? Integer.valueOf (Label.Sweep_Limit) : 0;
        if (sweepLIMIT != 0) {
            if (processedInquiries == sweepLimit) {
                System.AbortJob(BC.getJobId());
            }
        }
        
    }
    global void finish(Database.BatchableContext BC){
    }

}