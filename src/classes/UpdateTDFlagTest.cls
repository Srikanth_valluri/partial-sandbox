@isTest
public class UpdateTDFlagTest{
    @isTest
    public static void testUpdateTDService(){
        Location__c loc = new Location__c();
        loc.New_Ho_Doc_Type__c = 'Villa';
        loc.Location_ID__c = '123';
        insert loc;
        Inventory__c inv = new Inventory__c();
        inv.Building_Location__c = loc.id;
        insert inv;
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        insert sr;
        Booking__c booking = new Booking__c();
        booking.Deal_SR__c= sr.Id;
        insert booking ;
        Booking_Unit__c  bookingUnit = new Booking_Unit__c();
        //bookingUnit.Account_Id__c =  acc.Id;
        bookingUnit.Registration_ID__c = '80512';
        bookingUnit.Property_Name__c = 'Damac Hills';
        bookingUnit.Inventory__c = inv.Id;
        bookingUnit.Booking__c = booking.Id;
        bookingUnit.Registration_Id__c = '89899';
        insert bookingUnit ;
        insert new Credentials_Details__c(User_Name__c = 'oracle_user', Password__c = 'crp1user' , Endpoint__c = 'http://151.253.15.117:8050/webservices/rest/XXDC_PROCESS_SERVICE_WS/process/'
                                    ,Name = 'Title Deed'
        );
        Case caseInstance = new Case();
        caseInstance.Booking_Unit__c = bookingUnit.Id;
        caseInstance.Subvention_Amount__c = 1200;
        insert caseInstance;
        Test.startTest();
            UpdateTDFlag.updateTD('909090');
        Test.stopTest();
      
    }
}