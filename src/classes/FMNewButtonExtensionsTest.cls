/*-------------------------------------------------------------------------------------------------
Description: Test class
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 11-10-2018       | Lochana Rajput   | 1. Draft
=============================================================================================================================
*/
@isTest
private class FMNewButtonExtensionsTest {

	@isTest static void test_message() {
		FM_Case__c obj = new FM_Case__c();
		insert obj;
		Test.startTest();
        PageReference pageRef = Page.FMNewButtonPage;
        // pageRef.getParameters().put('id', String.valueOf(objobj.Id));
		pageRef.getParameters().put('retURL', String.valueOf(obj.Id));
        Test.setCurrentPage(pageRef);
		ApexPages.StandardController sc = new ApexPages.StandardController(obj);
        FMNewButtonExtensions extension = new FMNewButtonExtensions(sc);
		Test.stopTest();
		System.assertEquals(true, String.isNotBlank(extension.retURL));
	}

}