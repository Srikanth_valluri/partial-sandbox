@isTest
private class Send_Escalation_For_Walk_In_CL_Test {
  @isTest static void testcallFutureToSendEscalation() {
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accountobj[0].Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;

        List<Calling_List__c> listCallingList = TestDataFactory_CRM.createCallingList( 'Walk_In_Calling_List', 1 , bookingUnitsObj[0] );
        listCallingList[0].Address_Verified__c = true;
        insert listCallingList;
        system.debug(listCallingList);
        
        Test.startTest();
           Send_Escalation_For_Walk_In_Calling_List.callFutureToSendEscalation(listCallingList);
        Test.stopTest();
   }
}