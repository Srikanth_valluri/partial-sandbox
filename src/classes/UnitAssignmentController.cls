/*********************************************************************************************************
* Name               : UnitAssignmentController
* Test Class         : 
* Description        : Controller class for UnitAssignment VF Page
* Created Date       : 29/10/2020
* ----------------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         29/10/2020      Initial Draft.
**********************************************************************************************************/
public With Sharing Class UnitAssignmentController{
    public List<String> propertyNames {get; set;}
    Map<String, Property__c> propertyMap;
    public List<String> unitAssignmentNames {get; set;}
    Map<String, Unit_Assignment__c> unitAssignmentMap;
    public List<String> marketingCampaignNames {get; set;}
    Map<String, Campaign__c> marketingCampaignMap;
    public List<String> eoiNames {get; set;}
    Map<String, EOI_Process__c> eoiMap;
    public List<String> userNames {get; set;}
    public static string unitDetails {get;set;} 
    public List<Inventory__c > inventoryList {get;set;} 

    public String selectedAssignment {get; set;}
    public String selectedCampaign {get; set;}
    public String selectedEOI {get; set;}
    public String allocType {get; set;}
    public Map<String, User> userNameMap;
    public Map<String, Inventory__c> invNameMap;
    public Map <String, String> selectedUserMap {get; set;}
    public String selectedUserMapJSON  {get; set;}
    public Map <String, String> selectedCommentMap {get; set;}
    public String selectedCommentMapJSON  {get; set;}
    public Map <String, String> selectedStartDateMap {get; set;}
    public String selectedStartDateMapJSON  {get; set;}
    public Map <String, String> selectedEndDateMap {get; set;}
    public String selectedEndDateMapJSON  {get; set;}

    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public UnitAssignmentController() {
        propertyNames = new List<String>();
        propertyMap = new  Map<String, Property__c>();
        unitAssignmentNames = new List<String>();
        unitAssignmentMap = new  Map<String, Unit_Assignment__c>();
        marketingCampaignNames = new List<String>();
        marketingCampaignMap = new  Map<String, Campaign__c>();
        eoiNames = new List<String>();
        eoiMap = new  Map<String, EOI_Process__c>();
        userNames = new List<String>();
        userNameMap = new  Map<String, User>();
        invNameMap = new  Map<String, Inventory__c>();
        selectedAssignment = '';
        selectedCampaign = '';
        selectedEOI = '';
        allocType = 'Unit Assignment';
        selectedUserMap = new Map <String, String> ();
        selectedUserMapJSON = JSON.serialize(selectedUserMap);
        selectedCommentMap = new Map <String, String> ();
        selectedCommentMapJSON = JSON.serialize(selectedCommentMap);
        selectedStartDateMap = new Map <String, String> ();
        selectedStartDateMapJSON = JSON.serialize(selectedStartDateMap);
        selectedEndDateMap = new Map <String, String> ();
        selectedEndDateMapJSON = JSON.serialize(selectedEndDateMap);
        for(Property__c property: [SELECT Id, Property_Name__c, Property_ID__c 
                                       FROM Property__c 
                                       WHERE Property_Name__c != ''
                                       ORDER BY Property_Name__c ASC
                                       LIMIT 1000]){
            propertyMap.put(property.Property_Name__c, property);
            propertyNames.add(property.Property_Name__c);
        }
        for(Unit_Assignment__c assignment: [SELECT Id, Name, Unit_Assignment_Name__c 
                                       FROM Unit_Assignment__c 
                                       WHERE End_Date__c = NULL OR End_Date__c > TODAY
                                       ORDER BY CreatedDate DESC
                                       LIMIT 1000]){
            String name = assignment.Name;
            if(assignment.Unit_Assignment_Name__c != null && assignment.Unit_Assignment_Name__c != ''){
                name += ' - ' + assignment.Unit_Assignment_Name__c;
            }
            if(selectedAssignment == ''){
                selectedAssignment = name;
            }
            unitAssignmentMap.put(name, assignment);
            unitAssignmentNames.add(name);
        }
        for(Campaign__c campaign: [SELECT Id, Name, Campaign_Name__c 
                                       FROM Campaign__c 
                                       WHERE (End_Date__c = NULL OR End_Date__c > TODAY)
                                       AND Active__c = TRUE
                                       ORDER BY CreatedDate DESC
                                       LIMIT 1000]){
            String name = campaign.Name;
            if(campaign.Campaign_Name__c != null && campaign.Campaign_Name__c != ''){
                name += ' - ' + campaign.Campaign_Name__c;
            }
            if(selectedCampaign == ''){
                selectedCampaign = name;
            }
            marketingCampaignMap.put(name, campaign);
            marketingCampaignNames.add(name);
        }
        for(EOI_Process__c eoi: [SELECT Id, Name, EOI_for_Project__c 
                                       FROM EOI_Process__c 
                                       WHERE Status__c = 'Approved' OR Status__c = 'Allocated'
                                       ORDER BY CreatedDate DESC
                                       LIMIT 1000]){
            String name = eoi.Name;
            if(eoi.EOI_for_Project__c != null && eoi.EOI_for_Project__c != ''){
                name += ' - ' + eoi.EOI_for_Project__c;
            }
            if(selectedEOI == ''){
                selectedEOI = name;
            }
            eoiMap.put(name, eoi);
            eoiNames.add(name);
        }
        for(User usr: [SELECT Id, Name FROM User 
                                       WHERE IsActive = TRUE
                                       AND Profile.Name = 'Property Consultant'
                                       ORDER BY CreatedDate DESC
                                       LIMIT 1000]){
            userNameMap.put(usr.name, usr);
            userNames.add(usr.name);
        }
        List<Inventory__c> unitList = new List<Inventory__c>();
        unitDetails = JSON.serialize(unitList);
    }

   /************************************************************************************************
    * @Description : method to fetch the Unit Details.
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
   public void fetchUnitDetails(){
        String projName = ApexPages.currentPage().getParameters().get('projectName');
        Property__c property =  propertyMap.get(projName);
        system.debug('property: ' + property);
        fetchPropertyDetails(projName);
    }

   /************************************************************************************************
    * @Description : method to
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
   public Pagereference updateUser(){
        String userName = ApexPages.currentPage().getParameters().get('userName');
        String invName = ApexPages.currentPage().getParameters().get('invName');
        String comments = ApexPages.currentPage().getParameters().get('comments');
        String startDate = ApexPages.currentPage().getParameters().get('startDate');
        String endDate = ApexPages.currentPage().getParameters().get('endDate');
        system.debug('userName: ' + userName);
        system.debug('invName: ' + invName);
        system.debug('comments: ' + comments);
        system.debug('startDate: ' + startDate);
        system.debug('endDate: ' + endDate);
        if(userName != null && userName != ''){
            selectedUserMap.put(invName, userName);
            selectedCommentMap.put(invName, comments);
            selectedStartDateMap.put(invName, startDate);
            selectedEndDateMap.put(invName, endDate);
        }
        selectedUserMapJSON = JSON.serialize(selectedUserMap);
        selectedCommentMapJSON = JSON.serialize(selectedCommentMap);
        selectedStartDateMapJSON = JSON.serialize(selectedStartDateMap);
        selectedEndDateMapJSON = JSON.serialize(selectedEndDateMap);
        system.debug('selectedUserMapJSON: ' + selectedUserMapJSON);
        system.debug('selectedCommentMapJSON: ' + selectedCommentMapJSON);
        system.debug('selectedStartDateMapJSON: ' + selectedStartDateMapJSON);
        system.debug('selectedEndDateMapJSON: ' + selectedEndDateMapJSON);
        return null;
    }

    /************************************************************************************************
    * @Description : method to
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
   public void updateAllocation(){
        allocType = ApexPages.currentPage().getParameters().get('allocType');
        String allocName = ApexPages.currentPage().getParameters().get('allocName');
        system.debug('allocType: ' + allocType);
        system.debug('allocName: ' + allocName);
        if(allocType == 'Unit Assignment'){
            selectedAssignment = allocName;
        } else if(allocType == 'Marketing Campaign'){
            selectedCampaign = allocName;
        } else if(allocType == 'EOI'){
            selectedEOI = allocName;
        }
        system.debug('selectedCampaign: ' + selectedCampaign);
        system.debug('selectedAssignment: ' + selectedAssignment);
        system.debug('selectedEOI: ' + selectedEOI);
    }

       /************************************************************************************************
    * @Description : method to
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
   public Pagereference createInventoryUsers(){
       String invNames = ApexPages.currentPage().getParameters().get('inventoryNames');
       List<String> invNamesList = new List<String>();
       for(String invName: invNames.split(',')){
           if(invName != null && invName != 'undefined' && invName != ''){
               invNamesList.add(invName);
           }
       }
       system.debug('allocType: ' + allocType);
       system.debug('selectedAssignment: ' + selectedAssignment);
       system.debug('selectedCampaign: ' + selectedCampaign);
       Map<String, Unit_Allocation__c> invUnitAllocMap = new Map<String, Unit_Allocation__c>();
       for(String invName: invNamesList){
           Unit_Allocation__c unitAlloc = new Unit_Allocation__c();
           unitAlloc.Comments__c = selectedCommentMap.get(invName);
           unitAlloc.Start_Date__c = Date.parse(selectedStartDateMap.get(invName));
           unitAlloc.End_Date__c = Date.parse(selectedEndDateMap.get(invName));
           invUnitAllocMap.put(invName, unitAlloc);
       }
       if(invUnitAllocMap.keyset().size() > 0){
           insert invUnitAllocMap.values();
       }
       List<Inventory_User__c> invUserList = new List<Inventory_User__c>();
       for(String invName: invNamesList){
            Inventory_User__c invUser = new Inventory_User__c();
            if(allocType == 'Unit Assignment'){
                invUser.Unit_Assignment__c = unitAssignmentMap.get(selectedAssignment).Id;
            } else if(allocType == 'Marketing Campaign'){
                invUser.Campaign__c = marketingCampaignMap.get(selectedCampaign).Id;
            } else if(allocType == 'EOI'){
                invUser.EOI__c = eoiMap.get(selectedEOI).Id;
            }
            invUser.Unit_Allocation__c = invUnitAllocMap.get(invName).Id;
            String selUserName = selectedUserMap.get(invName);
            invUser.User__c = userNameMap.get(selUserName).Id;
            invUser.Inventory__c = invNameMap.get(invName).Id;
            invUser.Comments__c = selectedCommentMap.get(invName);
            invUser.Start_Date__c = Date.parse(selectedStartDateMap.get(invName));
            invUser.End_Date__c = Date.parse(selectedEndDateMap.get(invName));
            invUserList.add(invUser);
        }
        try{
            insert invUserList;
            String reqURL = System.URL.getSalesforceBaseUrl().toExternalForm() + '/apex/UnitAssignment' ;
             PageReference unitAssignPage = new PageReference(reqURL);
            unitAssignPage.setRedirect(true);
            return unitAssignPage;
        } catch (exception e){
            system.debug('Error while Inserting Inventory Users, Please try Again.');
            return null;
        }
    }

  /************************************************************************************************
    * @Description : method to fetch the  Property Details.
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
  public void fetchPropertyDetails(String projName) {
      List <UnitDetailsWrapper > wrapperList = new List < UnitDetailsWrapper > ();
      inventoryList = new List < Inventory__c > ();
      for (Inventory__c inv: [SELECT Id, Status__c, Unit_Location__c, Bedroom_Type_Code__c, ACD_Date__c,
                                Unit_Name__c, Building_Name__c, Property_Name_2__c, Price__c, Selling_Price__c
                  FROM Inventory__c
                  WHERE Unit_Name__c != null AND Property_Name_2__c =: projName]) {
        inventoryList.add(inv);
      }
      for (Inventory__c inv: inventoryList) {
          invNameMap.put(inv.Unit_Name__c, inv);
        UnitDetailsWrapper details = new UnitDetailsWrapper();
        details.project = inv.Property_Name_2__c;
        if(inv.Bedroom_Type_Code__c != null && inv.Bedroom_Type_Code__c != ''){
            details.bedroom = inv.Bedroom_Type_Code__c;
        } else {
            details.bedroom = '-';
        }
        if(inv.ACD_Date__c != null){
            details.acd = inv.ACD_Date__c;
        } else {
            details.acd = '-';
        }
        if(inv.Selling_Price__c != null){
            details.price = String.valueOf(inv.Selling_Price__c);
        } else {
            details.price = '-';
        }
        details.unitId = inv.Id;
        details.unit = inv.Unit_Name__c;
        details.status = inv.Status__c;
        wrapperList.add(details);
      }
      system.debug('wrapper: ' + wrapperList);
      unitDetails = JSON.serialize(wrapperList);
      system.debug('unitDetails: ' + unitDetails);
    }

    /************************************************************************************************
    * @Description : Wrapper Class to wrap the  Unit Details
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
   public class UnitDetailsWrapper{
        public string project;
        public string bedroom;
        public string acd;
        public string price;
        public string unitId;
        public string unit;
        public string status;
      //  public List<UnitAssignmentWrapper> assignment;
        public UnitDetailsWrapper(){}
    }

    /************************************************************************************************
    * @Description : Wrapper Class to wrap the  Unit Details
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
   public class UnitAssignmentWrapper{
        public string unit_allocation;
        public string marketing_campaign;
        public string start_date;
        public string end_date;
        public UnitAssignmentWrapper(){}
    }
}