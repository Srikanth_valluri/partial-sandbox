@isTest
private class TaskTriggerHandlerAssignOwnerTest{
    private static testMethod void normalUserTask(){
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Portal';
        insert objCase;
        
        Task objTask = new Task();
        objTask.OwnerId = UserInfo.getUserId();
        objTask.Subject = 'Test';
        objTask.Status = 'Not Started';
        objTask.WhatId = objCase.Id;
        insert objTask;
    }
    
    private static testMethod void portalUserTask(){
        Account objAccount = TestDataFactory_CRM.createBusinessAccount();
        insert objAccount;
        
        Contact objC = new Contact();
        objC.LastName = 'Charlie';
        objC.AccountId = objAccount.Id;
        insert objC;
        
        Profile objProfile = [SELECT Id FROM Profile where UserLicense.Name LIKE 'Customer Community%' limit 1];
        User objU = TestDataFactory_CRM.createUser('Charlie', string.valueOf(objProfile.Id),null);
        objU.ContactId = objC.Id;
        insert objU;
        
        System.runAs(objU) {
            Case objCase = new Case();
            objCase.Status = 'New';
            objCase.Origin = 'Portal';
            insert objCase;
        
            Task objTask = new Task();
            objTask.OwnerId = UserInfo.getUserId();
            objTask.Subject = 'Test';
            objTask.Status = 'Not Started';
            objTask.WhatId = objCase.Id;
            insert objTask;
        }
    }
}