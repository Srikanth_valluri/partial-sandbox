@istest
public with sharing class CorporateAgencyContactsController_Test {


    static testmethod void testAgencyContacts() {

        // Test Data
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABC@test.com');
        insert pcUser;

        Account acc = InitialiseTestData.getCorporateAccount('TestAgency');
        insert acc ;

        List<Contact> conList = new List<Contact>();
        for (Integer i = 0; i < 5; i++) {
            Contact con = new Contact();
            con.LastName = 'Corporate Agency ' + i;
            con.AccountId = acc.Id;
            con.OwnerId = pcUser.Id;
            conList.add(con);
        }
        insert conList ;

        CorporateAgencyContactsController corporateContacts;

        // Test the functionality in user context
        System.runAs(pcUser) {

            System.Test.startTest();
            ApexPages.StandardController controller = new ApexPages.StandardController(acc);
            corporateContacts = new CorporateAgencyContactsController(controller);
            System.Test.stopTest();

        }

        // Assert to verify if Contacts of logged in user are returned
        System.assertNotEquals(true, corporateContacts.contactList.isEmpty());

    }
}