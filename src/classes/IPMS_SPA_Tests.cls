/**************************************************************************************************
* Name               : IPMS_SPA_Tests
* Test Class         : Invocable_UpdateBuyer_Test
* Description        : Test Class for SPA classes - Async_IPMS_Rest_SPA   
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0                         09/11/2017      Created
**************************************************************************************************/
@IsTest
public without sharing class IPMS_SPA_Tests{
    
    static testMethod void testParse() {
        String json = '{'+
        '   \"sourceSystem\":\"SFDC\",'+
        '   \"extRequestId\":\"TOJOSUBASH\",'+
        '   \"requestName\":\"SPA\",'+
        '   \"extSystemObject\":\"\",'+
        '   \"extSystemKey\":\"\",'+
        '   \"requestLines\":['+
        '      {'+
        '         \"subRequestName\":\"SPA\",'+
        '         \"extSystemObject\":\"\",'+
        '         \"extSystemKey\":\"\",'+
        '         \"attribute1\":\"92694\",'+
        '         \"attribute2\":\"ATT-2\",'+
        '         \"attribute3\":\"ATT-3\",'+
        '         \"attribute4\":\"\",'+
        '         \"attribute5\":\"\",'+
        '         \"attribute6\":\"\",'+
        '         \"attribute7\":\"\",'+
        '         \"attribute8\":\"\",'+
        '         \"attribute9\":\"\",'+
        '         \"attribute10\":\"ATT-10\"'+
        '      }'+
        '   ]'+
        '}';
        IPMS_JSON_SPA_Response obj = IPMS_JSON_SPA_Response.parse(json);
        IPMS_JSON_SPA_Request obj1 = IPMS_JSON_SPA_Request.parse(json); 
        System.assert(obj != null);
        System.assert(obj1 != null);
    }

    static testmethod void IPMS_Rest_SPA_ProcessID(){
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;

        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal', false, null);
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c = null;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        insert sr;

        NSIBPM__Step_Template__c stpTemplate = InitializeSRDataTest.createStepTemplate('Deal', 'MANAGER_APPROVAL');
        insert stptemplate;

        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
        Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);

        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id,stepStatuses.values()[0].id, stptemplate.id);
        insert stp;

        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        insert lstbk;

        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        insert loc;

        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;

        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'test@test.com';
        bu.Primary_Buyer_s_Name__c = 'test';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        bu.registration_id__c = '1234';
        insert bu;

        Test.setMock(HttpCalloutMock.class, new IPMS_SPA_Mock_Submission());
        test.starttest();
            IPMS_REST_SPA_getProcessID.DataForRequest(string.valueof(lstbk[0].id)); 
        test.stoptest();
    }

    static testmethod void IPMS_Rest_SPA_Async(){
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;

        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal', false, null);
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c=null;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        sr.ownerid = userinfo.getuserid();
        insert sr;

        NSIBPM__Step_Template__c stpTemplate =  InitializeSRDataTest.createStepTemplate('Deal', 'MANAGER_APPROVAL');
        insert stptemplate;

        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
        Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);

        List<string> statuses1 = new list<string>{'AGREEMENT_GENERATED'};
        Map<string,NSIBPM__Status__c> stepStatuses1 = InitializeSRDataTest.createStepStatus(statuses1);
        stepStatuses1.values()[0].ownerid=userinfo.getuserid();
        update stepStatuses1.values()[0];

        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id, stepStatuses.values()[0].id, stptemplate.id);
        insert stp;

        NSIBPM__Step__c stp1 = InitializeSRDataTest.createStep(sr.id, stepStatuses1.values()[0].id, stptemplate.id);
        insert stp1;

        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        insert lstbk;

        booking__c book = [SELECT Id, Booking_Channel__c FROM booking__c WHERE Id =: lstbk[0].Id];
        book.Booking_Channel__c = 'Office';
        update book;

        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        insert loc;

        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;

        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'test@test.com';
        bu.Primary_Buyer_s_Name__c = 'test';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        bu.registration_id__c = '1234';
        insert bu;

        user u = [SELECT Id FROM User WHERE Id =: sr.ownerid LIMIT 1];
        IPMS_REST_SPA_getProcessID req = new IPMS_REST_SPA_getProcessID();
        Test.setMock(HttpCalloutMock.class, new IPMS_SPA_Mock_AsyncSPA());
        test.starttest();
            system.runas(u){
                Async_IPMS_Rest_SPA cls = new Async_IPMS_Rest_SPA('', lstbk[0].id);
                cls.srstatus = stepStatuses.values()[0].id;
                cls.POLLForSPAAsync('', lstbk[0].id);
            }
        test.stoptest();
    }

    static testmethod void IPMS_Rest_SOA_ProcessID(){

        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;

        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal', false, null);
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c=null;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        insert sr;

        NSIBPM__Step_Template__c stpTemplate =  InitializeSRDataTest.createStepTemplate('Deal', 'MANAGER_APPROVAL');
        insert stptemplate;

        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
        Map<string, NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);

        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id, stepStatuses.values()[0].id, stptemplate.id);
        insert stp;

        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        insert lstbk;

        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        insert loc;

        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;

        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'test@test.com';
        bu.Primary_Buyer_s_Name__c = 'test';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        bu.registration_id__c = '1234';
        insert bu;

        Test.setMock(HttpCalloutMock.class, new IPMS_SPA_Mock_Submission());
        test.starttest();
            IPMS_REST_SOA_getProcessID.DataForRequest(string.valueof(lstbk[0].id));
        test.stoptest();
    }

    static testmethod void IPMS_Rest_SOA_Async(){
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;

        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal', false, null);
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c=null;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        sr.ownerid = userinfo.getuserid();
        insert sr;

        NSIBPM__Step_Template__c stpTemplate = InitializeSRDataTest.createStepTemplate('Deal', 'MANAGER_APPROVAL');
        insert stptemplate;

        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
        Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);

        List<string> statuses1 = new list<string>{'AGREEMENT_GENERATED'};
        Map<string,NSIBPM__Status__c> stepStatuses1 = InitializeSRDataTest.createStepStatus(statuses1);
        stepStatuses1.values()[0].ownerid = userinfo.getuserid();
        update stepStatuses1.values()[0];

        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id, stepStatuses.values()[0].id, stptemplate.id);
        insert stp;
        NSIBPM__Step__c stp1 = InitializeSRDataTest.createStep(sr.id, stepStatuses1.values()[0].id, stptemplate.id);
        insert stp1;

        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        insert lstbk;

        booking__c book = [SELECT Id, Booking_Channel__c FROM booking__c WHERE Id =: lstbk[0].Id];
        book.Booking_Channel__c = 'Office';
        update book;

        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        insert loc;

        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;

        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'test@test.com';
        bu.Primary_Buyer_s_Name__c = 'test';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        bu.registration_id__c = '1234';
        insert bu;

        User u = [SELECT Id FROM User WHERE Id =: sr.ownerid LIMIT 1];
        IPMS_REST_SOA_getProcessID req = new IPMS_REST_SOA_getProcessID();
        Test.setMock(HttpCalloutMock.class, new IPMS_SPA_Mock_AsyncSPA());
        test.starttest();
            system.runas(u){
                Async_IPMS_Rest_SOA cls = new Async_IPMS_Rest_SOA('', lstbk[0].id);
                cls.POLLForSOAAsync('', lstbk[0].id);
            }
        test.stoptest();
    }

    static testmethod void IPMS_Rest_SOA_Async_ReGen(){
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;

        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal', false, null);
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c=null;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        sr.ownerid = userinfo.getuserid();
        insert sr;

        NSIBPM__Step_Template__c stpTemplate = InitializeSRDataTest.createStepTemplate('Deal', 'MANAGER_APPROVAL');
        insert stptemplate;

        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
        Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);

        List<string> statuses1 = new list<string>{'AGREEMENT_GENERATED'};
        Map<string,NSIBPM__Status__c> stepStatuses1 = InitializeSRDataTest.createStepStatus(statuses1);
        stepStatuses1.values()[0].ownerid = userinfo.getuserid();
        update stepStatuses1.values()[0];

        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id, stepStatuses.values()[0].id, stptemplate.id);
        insert stp;
        NSIBPM__Step__c stp1 = InitializeSRDataTest.createStep(sr.id, stepStatuses1.values()[0].id, stptemplate.id);
        insert stp1;

        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        insert lstbk;

        booking__c book = [SELECT Id, Booking_Channel__c FROM booking__c WHERE Id =: lstbk[0].Id];
        book.Booking_Channel__c = 'Office';
        update book;

        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        insert loc;

        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;

        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'test@test.com';
        bu.Primary_Buyer_s_Name__c = 'test';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        bu.registration_id__c = '1234';
        insert bu;

        User u = [SELECT Id FROM User WHERE Id =: sr.ownerid LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new IPMS_SPA_Mock_AsyncSPA());
        test.starttest();
            system.runas(u){
                DAMAC_SOA_Generation.callSOAGeneration(string.valueof(sr.id));
            }
        test.stoptest();
    }
}