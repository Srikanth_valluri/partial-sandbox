/*
* Description - Test class developed for 'AOPTMQService'
*
* Version            Date            Author            Description
* 1.0                21/11/17        Hardik            Initial Draft
*/
@isTest
public class AOPTUtilityTest {
    static testMethod void formatDateTestMethod() {
        test.StartTest();
        AOPTUtility obj = new AOPTUtility();
        String todayDate = system.today().format();
        String dt = AOPTUtility.formatDate(todayDate);
        test.StopTest();
    }

    static testMethod void calculateACDExtentsionTestMethod() {
        test.StartTest();
        String date1 = system.today().format();
        String date2 = (system.today().addDays(+1)).format();
        Boolean flag = AOPTUtility.calculateACDExtentsion('11-jan-2017','11-jan-2018');
        //Boolean flag1 = AOPTUtility.calculateACDExtentsion(null,null);
        //Boolean flag2 = AOPTUtility.calculateACDExtentsion('11-jan-2018',null);
        test.StopTest();
    }

    static testMethod void convertDateFormatTestMethod() {
        test.StartTest();
        Date janDate = AOPTUtility.convertDateFormat('11-jan-2017');
        Date febDate = AOPTUtility.convertDateFormat('11-feb-2017');
        Date marDate = AOPTUtility.convertDateFormat('11-mar-2017');
        Date aprDate = AOPTUtility.convertDateFormat('11-apr-2017');
        Date mayDate = AOPTUtility.convertDateFormat('11-may-2017');
        Date junDate = AOPTUtility.convertDateFormat('11-jun-2017');
        Date julDate = AOPTUtility.convertDateFormat('11-jul-2017');
        Date augDate = AOPTUtility.convertDateFormat('11-aug-2017');
        Date sepDate = AOPTUtility.convertDateFormat('11-sep-2017');
        Date octDate = AOPTUtility.convertDateFormat('11-oct-2017');
        Date novDate = AOPTUtility.convertDateFormat('11-nov-2017');
        Date decDate = AOPTUtility.convertDateFormat('11-dec-2017');
        test.StopTest();
    }

    static testMethod void calculateDateDiffTestMethod() {
        test.StartTest();
        String date1 = system.today().format();
        String date2 = (system.today().addDays(+1)).format();
        String dt1 = AOPTUtility.calculateDateDiff(date1,date2);
        String dt2 = AOPTUtility.calculateDateDiff('11-jan-2017',date2);
        String dt3 = AOPTUtility.calculateDateDiff('',date2);
        test.StopTest();
    }

    /*static testMethod void formatCurrencyTestMethod() {
        test.StartTest();
        String currency = AOPTUtility.formatCurrency('11');
        test.StopTest();
    }*/

    static testMethod void checkDateFormateTestMethod() {
        test.StartTest();
        AOPTUtility obj = new AOPTUtility();
        String todayDate = system.today().format();
        Boolean falg = AOPTUtility.checkDateFormate(todayDate);
        test.StopTest();
    }

    static testMethod void errorLoggerTestMethod() {
        String strErrorMessage = 'Error';
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Type = 'Change of Contact Details';
        objCase.IsPOA__c = false;
        objCase.OCR_verified__c = true;
        objCase.OQOOD_Fee_Applicable__c = false;
        objCase.Approval_Status__c = 'Approved' ;
        objCase.Status = 'Working';
        insert objCase;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        objSR.Agency__c = objAcc.Id ;
        insert objSR ;

        Id dealId = objSR.Deal_ID__c;
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
        insert lstBookings ; 

        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
        insert lstBookingUnits;

        test.StartTest();
        AOPTUtility.errorLogger(strErrorMessage,objCase.Id,lstBookingUnits[0].Id);
        test.StopTest();
    }

    static testMethod void validateSRInitiatonTestMethod() {
        String strErrorMessage = 'Error';
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Type = 'Change of Contact Details';
        objCase.IsPOA__c = false;
        objCase.OCR_verified__c = true;
        objCase.OQOOD_Fee_Applicable__c = false;
        objCase.Approval_Status__c = 'Approved' ;
        objCase.Status = 'Working';
        insert objCase;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        objSR.Agency__c = objAcc.Id ;
        insert objSR ;

        Id dealId = objSR.Deal_ID__c;
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
        insert lstBookings ; 

        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
        insert lstBookingUnits;

        Set<Id> bookingUnitIDSet = new Set<Id>();
        for(Booking_Unit__c bUnit : lstBookingUnits) {
            bookingUnitIDSet.add(bUnit.Id);
        }
        test.StartTest();
        Map<String,Object> srMap = AOPTUtility.validateSRInitiaton(bookingUnitIDSet, objCase.RecordType.DeveloperName);
        test.StopTest();
    }

}