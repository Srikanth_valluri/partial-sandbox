@RestResource(urlMapping='/DAMACSiteFactory/*')
global class GlobalRESTServices {
    
    @HttpPost
    global static void doPost(){
        
        RestContextHandler handler = new RestContextHandler(true);
        RestRequest req = handler.restRequest;
        System.debug('>>>>>requestURI>>>>>>>'+req.requestURI);
        String action = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        System.debug('>>>>>action>>>>>>>'+action);

        try{
            Map<String, string> params = handler.getRequestParams();
            System.debug('>>>>>params>>>>>>>'+params); 
            if(action.toLowerCase() == 'alacarte')
                handler = getProducts();
            
            handler.finalize();
        }
        catch (Exception exc) {
            system.debug('>>>>>>>>>>ERROR>>>>>>>>>>>' +exc.getmessage()+'>>>>>>LineNO>>>>>'+exc.getlinenumber()+'>>>>>>Action>>>>>>>'+action);
            handler.response.success = false;
            handler.response.statusCode = WebServiceResponse.STATUS_CODE_SYSTEM_ERROR;
            handler.response.message = exc.getMessage()+'-'+exc.getLineNumber();
            handler.finalize(exc);
        }
    }
    
    public static RestContextHandler getProducts(){
        RestContextHandler handler = new RestContextHandler(true);
        
        list<Inventory__c> units = [SELECT 
                                Unit_Plan_SF_Link__c,Floor_Plan_SF_Link__c, Plot_Plan_SF_Link__c, promotion_unit__c,
                                Marketing_Name_Doc__c, Marketing_Name_Doc__r.Name, Property_Name__c, Unit_Id__c,
                                Unit_Name__c, Bedroom_Type__c, Bedrooms__c,IPMS_Bedrooms__c, Area_sft__c,
                                Unit_Type__c, View_Type__c, Property_Status__c, Price_Per_Sqft__c,App_Property_Status__c,
                                ACD_Date__c, Plot_Area__c, Floor_Plan__c, Unit_Plan__c, Plot_Plan__c,
                                Sales_Offer_Additional_Charges__c, Unit_Catagory__c, convertCurrency(Total_Price_inc_VAT__c),
                                Rera_Percentage__c, Special_Price__c, Special_Price_Tax_Amount__c, Marketing_Plan__c,
                                Property_country__c,Property_City__c, Property_Type__c, Space_type_lookup_code__c
                                from inventory__c 
                                WHERE Tagged_To_Unit_Assignment__c = FALSE AND Marketing_name__c != NULL
                                AND Status__c = 'Released' AND Is_Assigned__c = false and Marketing_name__c like '%A LA%'];
                                
        list<Inventory> data = new list<Inventory>();
        for(Inventory__c inv: units){
            Inventory record = new Inventory();
            
            data.add(record);
        }
        handler.response.success = true;
        handler.response.message = 'A LA CARTE INVENTORY';                 
        handler.response.data = units;
        return handler;
        
    }
    
    global class Inventory{
        global string name;
        global boolean available;
        global integer bedrooms;
        global integer facade;
        global datetime startDate;
        global datetime endDate;
        global integer terrace;
    }

}