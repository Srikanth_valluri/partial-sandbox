public without sharing class FM_TaskHandlerPOP {

 	//private static map<Id,list<Error_Log__c>> mapCaseIdlstErrorsGenerated = new map<Id,list<Error_Log__c>>();
	private static set<Id> setFailedCasesId = new set<Id>();
	private static list<Error_Log__c> lstErrorLogs = new list<Error_Log__c>();
 	private static list<SR_Attachments__c> lstCustomAttachment = new list<SR_Attachments__c>();

    @InvocableMethod
    public static void sendPOPApprovalMail( list<Task> lstApprovedTasks ) {

    	system.debug( '==lstApprovedTasks==' +lstApprovedTasks );

    	set<Id> setPOPApprovedTasksId = new set<Id>();
    	set<Id> setFmCaseIds = new set<Id>();

    	for( Task objTask : lstApprovedTasks ) {
    		if( isRelatedToFMCase( objTask ) ) {
    			setPOPApprovedTasksId.add( objTask.Id );
    			setFmCaseIds.add( objTask.WhatId );
    		}
    	}
    	system.debug( '==setPOPApprovedTasksId==' +setPOPApprovedTasksId );

    	if( !setPOPApprovedTasksId.isEmpty() ) {
    		sendPOPApprovalMail( setPOPApprovedTasksId, setFmCaseIds );
    	}
    }


    @future(callout=true)
    public static void sendPOPApprovalMail( set<Id> setPOPApprovedTasksId, set<Id> setFmCaseIds ) {

    	list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
    	list<FM_Case__c> lstCasesToBeUpdated = new list<FM_Case__c>();
    	list<Task> lstPOPApprovedTasks = getTaskDetails( setPOPApprovedTasksId );
		system.debug( '==  lstPOPApprovedTasks==' +lstPOPApprovedTasks );
    	map<Id, FM_Case__c> mapFMCase = new map<Id, FM_Case__c>( getCaseDetails( setFmCaseIds ) );
		list<SMS_History__c> lstVerificationSms = new list<SMS_History__c>();

    	for( Task objTask : lstPOPApprovedTasks ) {
			system.debug( '==  in for=='  );
			system.debug( '==  objTask=='+objTask);
			system.debug( '==  mapFMCase=='+mapFMCase);
    		if( mapFMCase.containsKey( objTask.WhatId ) ) {
				system.debug( '==  in if=='  );
    			FM_Case__c objFMCase = mapFMCase.get( objTask.WhatId );

    			FM_POP_Field_Reference__c mc = FM_POP_Field_Reference__c.getOrgDefaults();

    			String strReceiptId = mc != NULL && mc.Use_Cash_Receipt_Id__c ?
    								  objTask.Cash_Receipt_Id__c :
    								  mc != NULL && mc.Use_Document_Number__c ? objTask.Document_Number__c : '' ;
    			system.debug( '==strReceiptId==' +strReceiptId );
    			lstWrapper = addReceiptToWrapperList( strReceiptId ,objFMCase ,lstWrapper );

    			if( !lstWrapper.isEmpty() ) {
		    		UploadMultipleDocController.data objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper, 'FM' );
		    		system.debug( '==objResponse==' +objResponse );
					if( objResponse != NULL && objResponse.data != NULL ) {
						system.debug( '==in if==' );
		    			insertCustomAttachment( objFMCase, objResponse, 'Receipt : ' + strReceiptId );
		    			lstWrapper.clear();
		    		}
		    	}

		    	/*if( objFMCase.Booking_Units__r != NULL && !objFMCase.Booking_Units__r.isEmpty() ) {
		    		for( SR_Booking_Unit__c objJunc : objFMCase.Booking_Units__r ) {
		    			lstWrapper = addSOAToWrapperList( objJunc.Booking_Unit__r.Registration_ID__c, objJunc, lstWrapper );
		    			if( !lstWrapper.isEmpty() ) {
		    				UploadMultipleDocController.data objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper, 'FM' );
		    				if( objResponse != NULL && objResponse.data != NULL ) {
				    			insertCustomAttachment( objFMCase, objResponse, objJunc.Booking_Unit__r.Unit_Name__c + ' Statement Of Account'   );
				    			lstWrapper.clear();
				    		}
		    			}
		    		}
		    	}*/
                if( String.isNotBlank( objFMCase.Account__r.Party_Id__c ) ) {
		    		lstWrapper = addSOAToWrapperList( objFMCase.Account__r.Party_Id__c, objFMCase, lstWrapper );
                    if( !lstWrapper.isEmpty() ) {
                        UploadMultipleDocController.data objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper, 'FM' );
                        if( objResponse != NULL && objResponse.data != NULL ) {
                            insertCustomAttachment( objFMCase, objResponse, objFMCase.Account__r.Party_Id__c + ' Statement Of Account'   );
                            lstWrapper.clear();
                        }
                    }
		    	}
				system.debug( '==  setFailedCasesId=='+setFailedCasesId);
				system.debug( '==  objFMCase.Id=='+objFMCase.Id);
	    		if( !setFailedCasesId.contains( objFMCase.Id ) ) {
					system.debug( '==  in if=='  );
					lstCasesToBeUpdated.add( new FM_Case__c( Id=objFMCase.Id, Status__c='Closed', Approval_Status__c='Approved' ) );
	            	//added logic to send sms on closure
					SMS_History__c verificationSms = new SMS_History__c();
					verificationSms.FM_Case__c = objFMCase.Id;					
					string messageToSend = 'Dear ' + [SELECT Id, Name, Account__c, Account__r.name FROM FM_Case__c WHERE Id = :objFMCase.Id].Account__r.name +',\n\n';
					messageToSend = messageToSend + 'Thank you for making the service charge payment towards your property. You will receive an email with the Receipt and Statement shortly.\n\n';
					messageToSend = messageToSend + 'Regards,\n';
					messageToSend = messageToSend + 'LOAMS';
					verificationSms.Message__c = messageToSend;
					verificationSms.Phone_Number__c =
							(objFMCase.Mobile_Country_Code__c == NULL
								? ''
								: objFMCase.Mobile_Country_Code__c.substringAfterLast(':').trim())
							+ objFMCase.Mobile_no__c;

					verificationSms.Name = 'Proof of Payment closure for '
							+ [SELECT Id, Name FROM FM_Case__c WHERE Id = :objFMCase.Id].Name;		
					lstVerificationSms.add(verificationSms);
					system.debug( '==lstVerificationSms==' +lstVerificationSms);
				}
    		}
    	}

    	if( !lstCustomAttachment.isEmpty() ) {
    		system.debug( '==lstCustomAttachment==' +lstCustomAttachment );
    		insert lstCustomAttachment ;
    	}

    	if( !lstErrorLogs.isEmpty() ) {
    		/*system.debug( '==mapCaseIdlstErrorsGenerated==' +mapCaseIdlstErrorsGenerated );
    		insert mapCaseIdlstErrorsGenerated.values();
    		list<Error_Log__c> lstErrorLogs = new list<Error_Log__c>();
    		for( list<Error_Log__c> lstErrors : mapCaseIdlstErrorsGenerated.values() ) {
    			lstErrorLogs.addAll( lstErrors );
    		}*/
    		insert lstErrorLogs ;
    	}

    	if( !lstCasesToBeUpdated.isEmpty() ) {
    		update lstCasesToBeUpdated;
    	}

		if(Test.isRunningTest()) {

		}
		else {
			if(!lstVerificationSms.isEmpty()) {
				insert lstVerificationSms;
				system.debug( '==insert zala lstVerificationSms==' +lstVerificationSms);
			}			
		}
		
    }

    @testVisible
    private static void addErrorLogs( String strAccountId,
    								  String strCaseId,
    								  String strMessage,
									  Boolean hasReceipt ) {
    	Error_Log__c objLog = FM_Utility.createErrorLog( strAccountId,
        												 NULL,
        												 strCaseId,
        												 'Proof Of Payment',
        												 strMessage );
		system.debug('objLog====='+objLog);
		system.debug('hasReceipt====='+hasReceipt);
		system.debug('strMessage====='+strMessage);
    	/*if( mapCaseIdlstErrorsGenerated.containsKey( strCaseId ) ) {
    		mapCaseIdlstErrorsGenerated.get( strCaseId ).add( objLog );
    	}
    	else {
    		mapCaseIdlstErrorsGenerated.put( strCaseId , new list<Error_Log__c> { objLog } );
    	}*/
		if( hasReceipt ) {
			setFailedCasesId.add( strCaseId );
		}
		lstErrorLogs.add( objLog );
		
    }

     //Check if the task is related to the FM Case object.
    @testVisible
    private static boolean isRelatedToFMCase( Task objTask ) {
    	if( objTask != NULL && String.isNotBlank( objTask.WhatId ) && 
        		String.valueOf( objTask.WhatId ).startsWith( FM_Utility.getObjectKeyPrefix('FM_Case__c') ) ) {
    		return true ;
    	}
    	else {
    		return false ;
    	}
    }

    @testVisible
    private static list<UploadMultipleDocController.MultipleDocRequest> addReceiptToWrapperList( String strReceiptId ,
    																							 FM_Case__c objFMCase ,
    																							 list<UploadMultipleDocController.MultipleDocRequest> lstWrapper ) {
    	if( String.isNotBlank( strReceiptId ) ) {
    		try {
				String strReceiptURL = FmIpmsRestServices.generateDuplicateReceipt( strReceiptId );
				system.debug( '==strReceiptURL==' +strReceiptURL );
				if( String.isNotBlank( strReceiptURL ) ) {
					lstWrapper.add( FM_Utility.makeWrapperObject( getDocumentBodyAsString( strReceiptURL ) ,
	                                                              'Receipt : ' + strReceiptId + '.pdf',
	                                                              'Receipt : ' + strReceiptId + '.pdf',
	                                                              objFMCase.Name, String.valueOf( lstWrapper.size() + 1 ) ) );
				}
			}
			catch( Exception e ) {
				system.debug('in Exception');
				addErrorLogs( NULL, objFMCase.Id, 'Exception while generating the receipts : ' + e.getMessage(), true );
			}
		}
		else {
			system.debug('in else');
			addErrorLogs( NULL, objFMCase.Id, 'No receipt Id found on the task', true );
		}
		return lstWrapper ;
    }

    @testVisible
    private static list<UploadMultipleDocController.MultipleDocRequest> addSOAToWrapperList( String strPartyId,
                                                                                             FM_Case__c objFMCase,
																							 list<UploadMultipleDocController.MultipleDocRequest> lstWrapper ) {
    	if( String.isNotBlank( strPartyId ) ) {
    		try {
				String strURL = FmIpmsRestServices.getBulkSoaUrlForPartyId( strPartyId ) ;
				system.debug( '==strURL==' +strURL );
				if( String.isNotBlank( strURL ) ) {
					lstWrapper.add( FM_Utility.makeWrapperObject( getDocumentBodyAsString( strURL ) ,
	                                                              'SOA : ' + strPartyId + '.pdf',
	                                                              'SOA : ' + strPartyId + '.pdf',
	                                                              objFMCase.Name, String.valueOf( lstWrapper.size() + 1 ) ) );
				}
				else {
					system.debug('in else');
					addErrorLogs( NULL, objFMCase.Id, 'No SOA returned from the service for : '+strPartyId, false );
				}
			}
			catch( Exception e ) {
				system.debug('in Exception');
				addErrorLogs( NULL, objFMCase.Id, 'Exception while generating the SOA : ' + e.getMessage(), false );
			}
		}
		else {
			system.debug('in outer else');
			addErrorLogs( NULL, objFMCase.Id, 'No Registration Id on the unit.', false );
		}
		return lstWrapper ;
    }

    @testVisible
    private static list<UploadMultipleDocController.MultipleDocRequest> addSOAToWrapperList( String strRegId ,
																							 SR_Booking_Unit__c objJunc ,
																							 list<UploadMultipleDocController.MultipleDocRequest> lstWrapper ) {
    	if( String.isNotBlank( strRegId ) ) {
    		try {
				String strURL = FmIpmsRestServices.getUnitSoaByRegistrationId( strRegId ) ;
				system.debug( '==strURL==' +strURL );
				if( String.isNotBlank( strURL ) ) {
					lstWrapper.add( FM_Utility.makeWrapperObject( getDocumentBodyAsString( strURL ) ,
	                                                              'SOA : ' + objJunc.Booking_Unit__r.Unit_Name__c + '.pdf',
	                                                              'SOA : ' + objJunc.Booking_Unit__r.Unit_Name__c + '.pdf',
	                                                              objJunc.FM_Case__r.Name, String.valueOf( lstWrapper.size() + 1 ) ) );
				}
				else {
					addErrorLogs( NULL, objJunc.FM_Case__c, 'No SOA returned from the service for : '+objJunc.Booking_Unit__r.Unit_Name__c, false );
				}
			}
			catch( Exception e ) {
				addErrorLogs( NULL, objJunc.FM_Case__c, 'Exception while generating the SOA : ' + e.getMessage(), false );
			}
		}
		else {
			addErrorLogs( NULL, objJunc.FM_Case__c, 'No Registration Id on the unit.', false );
		}
		return lstWrapper ;
    }

    @testVisible
    private static void insertCustomAttachment( FM_Case__c objFMCase, UploadMultipleDocController.data objResponse, String strDocumentName ) {
        for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            SR_Attachments__c objCustAttach = new SR_Attachments__c();
            objCustAttach.Account__c = objFMCase.Account__c ;
            objCustAttach.Attachment_URL__c = objFile.url;
            //objCustAttach.Booking_Unit__c = objFMCase.Booking_Unit__c ;
            objCustAttach.FM_Case__c = objFMCase.Id ;
            objCustAttach.Name = strDocumentName ;
            objCustAttach.isValid__c = true ;
            objCustAttach.Is_sent_in_email__c = true ;
            lstCustomAttachment.add( objCustAttach );
        }
    }

    @testVisible
    private static String getDocumentBodyAsString( String strURL ) {
    	if( String.isNotBlank( strURL ) ) {
	    	HttpRequest req = new HttpRequest();
			req.setEndpoint( strURL );
			req.setMethod('GET');
			req.setHeader('Content-Type', 'application/pdf');
			req.setTimeout(60000);
			HTTPResponse res = new Http().send( req );
			//return res.getBody();
			String strBody = EncodingUtil.base64Encode( res.getBodyAsBlob() );
			return strBody ;
    	}
    	return '';
    }

    private static list<Task> getTaskDetails( set<Id> setTasksId ) {
    	return [ SELECT Id
    				  , OwnerId
    				  , WhatId
    				  , Cash_Receipt_Id__c
    				  , Document_Number__c
    			   FROM Task
    			  WHERE Id IN :setTasksId ] ;
    }

    private static list<FM_Case__c> getCaseDetails( set<Id> setFMCaseId ) {
    	return [ SELECT Id
    				  , Account__c
    				  , Account__r.Email__pc
    				  , Account__r.Email__c
    				  , Booking_Unit__c
    				  , Name
					  , Mobile_Country_Code__c
					  , Mobile_no__c
                	  , Account__r.Party_ID__c
    				  , ( SELECT Id
    				  		   , Booking_Unit__c
    				  		   , Booking_Unit__r.Registration_ID__c
    				  		   , Booking_Unit__r.Unit_Name__c
    				  		   , FM_Case__r.Name
    				  		   , FM_Case__c
    				  		FROM Booking_Units__r )
    			   FROM FM_Case__c
    			  WHERE Id = :setFMCaseId ];
    }

}