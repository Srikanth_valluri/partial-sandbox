/*
* Description - Test class developed for RefreshFMInvDueController
*/
@isTest
public class RefreshFMInvDueControllerTest  {
    
    static Id fmCLRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
    
    static testMethod void testFetchData() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';           
        }
        insert lstBookingUnits;

        List<Calling_List__c> lstCallingLists = createCallingList( fmCLRecordTypeId , 1 ,lstBookingUnits );
        insert lstCallingLists;
        system.debug(' Test lstCallingLists : ' + lstCallingLists);

            insert new IpmsRestServices__c(
                SetupOwnerId = UserInfo.getOrganizationId(),
                BaseUrl__c = 'http://0.0.0.0:8080/webservices/rest',
                Username__c = 'username',
                Password__c = 'password',
                Timeout__c = 120000
            );

            FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
            invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
            invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
            invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
            invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
            invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
            invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
                new Map<String, String> {
                    'ATTRIBUTE1' => 'registrationId',
                    'ATTRIBUTE2' => 'unitName',
                    'ATTRIBUTE3' => 'projectName',
                    'ATTRIBUTE4' => 'customerId',
                    'ATTRIBUTE5' => 'orgId',
                    'ATTRIBUTE6' => 'partyId',
                    'ATTRIBUTE7' => 'partyName',
                    'ATTRIBUTE8' => 'trxNumber',
                    'ATTRIBUTE9' => 'creationDate',
                    'ATTRIBUTE10' => 'callType',
                    'ATTRIBUTE11' => '50',
                    'ATTRIBUTE12' => '100',
                    'ATTRIBUTE13' => '50',
                    'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                    'ATTRIBUTE15' => 'trxType',
                    'ATTRIBUTE16' => '0'
                }
            );
                    
        
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));
            PageReference popPage = Page.RefreshFMInvDue;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstCallingLists[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
            RefreshFMInvDueController objController = new RefreshFMInvDueController(sc);
            

            
            objController.fetchData();
            objController.returnBack();
        Test.stopTest();
    }

    /*
     @ Description : To create calling list reocord
     @ Return      : list of calling list to be created
    */
    public static List<Calling_List__c> createCallingList( Id RecordTypeIdCollection, Integer counter , List<Booking_Unit__c> lstBookingUnits ) {
        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        for( Integer i=0; i<counter; i++ ) {
            lstCallingLists.add(new Calling_List__c( Registration_ID__c = '1234' ,NOT_DUE__c = 545,Invoice_Original_Due__c  =582
                                                   , Inv_Due__c = 0 ,DUE_0_30_DAYS__c = 120 , DUE_30_60_DAYS__c = 14 , DUE_60_90_DAYS__c = 4455
                                                   , DM_Due_Amount__c = 0  , DUE_90_180_DAYS__c = 522,DUE_180_360_DAYS__c = 545 , DUE_MORE_THAN_360_DAYS__c= 45
                                                   , RecordTypeId = RecordTypeIdCollection ) );
        }
        return lstCallingLists;
    }
}