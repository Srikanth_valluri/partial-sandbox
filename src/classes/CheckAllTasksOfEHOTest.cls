/**********************************************************************************************
* Description - Test class developed for CheckAllTasksOfEHO
*
* Version            Date            Author                    Description
* 1.0                18/12/17        Naresh Kaneriya (Accely)   Initial Draft
**********************************************************************************************/

@isTest
public class CheckAllTasksOfEHOTest{

       @isTest static void getTest(){
   
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;    
        System.assert(Sr != null);
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
     
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Type = 'NOCVisa';
        insert Cas;
        System.assert(Cas != null);
         
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
        
        Task tsk = new Task();
        tsk.WhatId = Cas.Id;
        tsk.ActivityDate = System.today()+2;
        tsk.Subject = 'EHO - Raise FM invoices, collect, and update receipt in IPMS';  
        tsk.Status = 'Not Started';    
        tsk.Assigned_User__c = 'Finance';   
        tsk.Status = 'Completed';
        tsk.Process_Name__c = 'Early Handover';
        insert tsk ;  
        
        Task tsk1 = new Task();
        tsk1.WhatId = Cas.Id;
        tsk1.ActivityDate = System.today()+2;
        tsk1.Subject = 'EHO - Reverse calls, Raise new invoices, collect, and update receipt in IPMS';
        tsk1.Status = 'Not Started';      
        tsk1.Assigned_User__c = 'Finance';   
        tsk1.Status = 'Completed';
        tsk1.Process_Name__c = 'Early Handover';
        insert tsk1 ;  
        
        list<Id> lstTask =  new list<Id>();
        lstTask.add(tsk.Id);
        lstTask.add(tsk1.Id);     
        
        set<String> TskWhatId =  new set<String>();
        TskWhatId.add(tsk.WhatId);
        map<Id, set<String>> mapCaseIdTasks = new map<Id, set<String>>();
        mapCaseIdTasks.put(tsk.Id,TskWhatId);

        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();
        TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response1 = new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
        response1.return_x = '{'+
            ''+
            '"message":"message",'+
            '"status":"S"'+
            '}';
       SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
       Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
       CheckAllTasksOfEHO.updateTask(lstTask);  
       Test.stopTest();
          
   }

}