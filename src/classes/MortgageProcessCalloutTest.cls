@isTest
private class MortgageProcessCalloutTest {
     @isTest static void testUpdateMortgageFlag() {
   
    Id recodTypeIdCase = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType=:'Case'
            AND DeveloperName='Mortgage'
            AND IsActive = TRUE LIMIT 1
        ].Id;
Account accObj = TestDataFactory_CRM.createPersonAccount();
    insert accObj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accObj.Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;
        
        List<Case> lstCase = new List<Case>();
        for( Integer i=0; i<2; i++ ) {
        case objCase = new Case();
            objCase.Mortgage_Flag__c = Label.Mortgage_Intitiation_Flag.trim();
            objCase.NOC_Received_Date__c =System.today();
            objCase.NOC_Request_Date__c = System.today();
            objCase.Submit_Date__c = System.today();
            objCase.Loan_Amount__c = 200;
            objCase.Bank_Code__c = '2532523';
            objCase.Loan_Offer_Date__c = System.today();
            objCase.Bank_Name__c = 'test';
            objCase.Mortgage_Bank_Name__c ='test bank';
            objCase.Bank_Name_Picklist__c  ='DOHA BANK';
            objCase.Mortgage_Amount__c = 10;
            objCase.AccountId = accObj.id;
            objCase.status = 'new';
            objCase.Type = 'Mortgage';
            objCase.Credit_Note_Amount__c=5000;
            objCase.RecordTypeId = recodTypeIdCase;
                
            lstCase.add(objCase);
        }
        insert lstCase;
         Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMortgage());
            MortgageProcessCallout.sendbankDetails(lstCase[0],string.valueof(lstCase[0].CaseNumber),bookingUnitsObj[0]);
        Test.stopTest();
  }
  
  @isTest static void testUpdateMortgageFlag1() {
   Id recodTypeIdCase = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType=:'Case'
            AND DeveloperName='Mortgage'
            AND IsActive = TRUE LIMIT 1
        ].Id;
    
Account accObj = TestDataFactory_CRM.createPersonAccount();
    insert accObj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accObj.Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;
        
        List<Case> lstCase = new List<Case>();
        for( Integer i=0; i<2; i++ ) {
        case objCase = new Case();
            objCase.Mortgage_Flag__c = Label.Mortgage_Approval_Flag.trim();
            objCase.NOC_Received_Date__c =System.today();
            objCase.NOC_Request_Date__c = System.today();
            objCase.Submit_Date__c = System.today();
            objCase.Loan_Amount__c = 200;
            objCase.Bank_Code__c = '2532523';
            objCase.Loan_Offer_Date__c = System.today();
            objCase.Bank_Name__c = 'test';
            objCase.Mortgage_Bank_Name__c ='test bank';
            objCase.Bank_Name_Picklist__c  ='DOHA BANK';
            objCase.Mortgage_Amount__c = 10;
            objCase.AccountId = accObj.id;
            objCase.status = 'new';
            objCase.Type = 'Mortgage';
            objCase.Credit_Note_Amount__c=5000;
            objCase.RecordTypeId = recodTypeIdCase;
                
            lstCase.add(objCase);
        }
        insert lstCase;
         Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMortgage());
            MortgageProcessCallout.sendbankDetails(lstCase[0],string.valueof(lstCase[0].CaseNumber),bookingUnitsObj[0]);
        Test.stopTest();
  }
  
  @isTest static void testUpdateMortgageFlag2() {
   Id recodTypeIdCase = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType=:'Case'
            AND DeveloperName='Mortgage'
            AND IsActive = TRUE LIMIT 1
        ].Id;
    
Account accObj = TestDataFactory_CRM.createPersonAccount();
    insert accObj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accObj.Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;
        
        List<Case> lstCase = new List<Case>();
        for( Integer i=0; i<2; i++ ) {
        case objCase = new Case();
            objCase.Mortgage_Flag__c = 'C';
            objCase.NOC_Received_Date__c =System.today();
            objCase.NOC_Request_Date__c = System.today();
            objCase.Submit_Date__c = System.today();
            objCase.Loan_Amount__c = 200;
            objCase.Bank_Code__c = '2532523';
            objCase.Loan_Offer_Date__c = System.today();
            objCase.Bank_Name__c = 'test';
            objCase.Mortgage_Bank_Name__c ='test bank';
            objCase.Bank_Name_Picklist__c  ='DOHA BANK';
            objCase.Mortgage_Amount__c = 10;
            objCase.AccountId = accObj.id;
            objCase.status = 'new';
            objCase.Type = 'Mortgage';
            objCase.Credit_Note_Amount__c=5000;
            objCase.RecordTypeId = recodTypeIdCase;
                
            lstCase.add(objCase);
        }
        insert lstCase;
         Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMortgage());
            MortgageProcessCallout.sendbankDetails(lstCase[0],string.valueof(lstCase[0].CaseNumber),bookingUnitsObj[0]);
        Test.stopTest();
  }
  


}