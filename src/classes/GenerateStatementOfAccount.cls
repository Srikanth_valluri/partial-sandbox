public without sharing class GenerateStatementOfAccount {
    private Case caseobj;
    
    public GenerateStatementOfAccount( ApexPages.StandardController controller ) {
        caseobj= (Case)controller.getrecord();
        caseobj = [ SELECT Id
                         , Booking_Unit__c
                         , Booking_Unit__r.Registration_ID__c
                         , AccountId
                         , IPMS_Updated__c
                         , Status
                      FROM Case 
                     WHERE Id = :caseobj.Id 
                     LIMIT 1 ];
    }
    
    public PageReference generatestataccount(){ 
        if( caseobj != null && String.isNotBlank( caseobj.Booking_Unit__r.Registration_ID__c ) ) {
            try {
                GenerateSOAController.soaResponse objSOAResponse = GenerateSOAController.getSOADocument( caseobj.Booking_Unit__r.Registration_ID__c );
                if( objSOAResponse != null && String.isNotBlank( objSOAResponse.Status ) && objSOAResponse.Status.equalsIgnoreCase('S') &&
                    String.isNotBlank( objSOAResponse.PROC_STATUS ) && objSOAResponse.PROC_STATUS.equalsIgnoreCase('S') && 
                    String.isNotBlank( objSOAResponse.url ) ) {
                    
                    Pagereference pg = new PageReference( objSOAResponse.url );
                    pg.setRedirect(true);
                    return pg;
                }
                else if(  objSOAResponse != null && String.isNotBlank( objSOAResponse.Status ) && String.isNotBlank( objSOAResponse.PROC_STATUS ) &&
                          ( !objSOAResponse.Status.equalsIgnoreCase('S') || !objSOAResponse.PROC_STATUS.equalsIgnoreCase('S') ) ) {
                    Error_Log__c objErr = new Error_Log__c();
                    objErr.Account__c = caseobj.AccountId ;
                    objErr.Booking_Unit__c = caseobj.Booking_Unit__c ;
                    objErr.Case__c = caseobj.Id ;
                    objErr.Process_Name__c = 'Penalty Waiver';
                    objErr.Error_Details__c = objSOAResponse.PROC_MESSAGE;
                    insert objErr ;
                    ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, ' ' + objSOAResponse.PROC_MESSAGE) );
                }
            }
            catch( Exception e ) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, e.getMessage() ) );
            }
        }
        else {
            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, 'The selected unit dosent have a Registration Id associated with it.') );
        }
        return null;
    }
    
    public PageReference generatestatofpenalty(){
        if( caseobj != null && String.isNotBlank( caseobj.Booking_Unit__r.Registration_ID__c ) ) {
            try {
                PenaltyWaiverService.SopResponseObject objSOPResponse = PenaltyWaiverService.getSOPDocument( caseobj.Booking_Unit__r.Registration_ID__c );
                if( objSOPResponse != null && String.isNotBlank( objSOPResponse.Status ) && objSOPResponse.Status.equalsIgnoreCase('S') &&
                    String.isNotBlank( objSOPResponse.PROC_STATUS ) && objSOPResponse.PROC_STATUS.equalsIgnoreCase('S') &&
                    String.isNotBlank( objSOPResponse.url ) ) {
                    
                    Pagereference pg = new PageReference( objSOPResponse.url );
                    pg.setRedirect(true);
                    return pg;
                }
                else if(  objSOPResponse != null && String.isNotBlank( objSOPResponse.Status ) && String.isNotBlank( objSOPResponse.PROC_STATUS ) &&
                          ( !objSOPResponse.Status.equalsIgnoreCase('S') || !objSOPResponse.PROC_STATUS.equalsIgnoreCase('S') )  ) {
                    Error_Log__c objErr = new Error_Log__c();
                    objErr.Account__c = caseobj.AccountId ;
                    objErr.Booking_Unit__c = caseobj.Booking_Unit__c ;
                    objErr.Case__c = caseobj.Id ;
                    objErr.Process_Name__c = 'Penalty Waiver';
                    objErr.Error_Details__c = objSOPResponse.PROC_MESSAGE;
                    insert objErr ;
                    
                    ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, ' ' + objSOPResponse.PROC_MESSAGE) );
                }
            }
            catch( Exception e ) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, e.getMessage() ) );
            }   
        }
        else {
            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, 'The selected unit dosent have a Registration Id associated with it.') );
        }
        return null;
    }
    
    public Pagereference cancelrequest(){
        if( caseobj != null && caseobj.id != null ) {
            if( caseobj.IPMS_Updated__c == false ) {
                caseobj.Status = 'Cancelled';    
                update caseobj ;
                Pagereference pg = new PageReference('/'+caseobj.id);
                pg.setRedirect(true);
                return pg;
            }
            else {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.FATAL, 'Request could not be cancelled.' ) );
            }
        }
        else {
            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.FATAL, 'Invalid Case.' ) );
        }
        return null ;
    }
}