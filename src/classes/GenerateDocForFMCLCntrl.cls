public without sharing class GenerateDocForFMCLCntrl {

    public String callingListId                         {get;set;}
    public Id callingListRecordTypeId                   {get;set;}
    public Id fmCLRecordTypeId                          {get;set;}
    public List<Calling_List__c> callingList            {get;set;}
    
    public GenerateDocForFMCLCntrl(ApexPages.StandardController controller) {
        callingList = new List<Calling_List__c>();
        
        callingListRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
        fmCLRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collection-Unipic').RecordTypeId;
        System.debug('fmCLRecordTypeId :::::::'+fmCLRecordTypeId );
        callingListId = ApexPages.currentPage().getParameters().get('id');
        
        System.debug('callingListId:::::::'+callingListId);
        callingList = [SELECT Id,Unit_Name__c,
                            RecordTypeId,Party_ID__c,Property__r.Property_Code__c,
                            Registration_ID__c
                       FROM Calling_List__c
                       WHERE Id = :callingListId
                         LIMIT 1];
    }
    
    public PageReference generateFMSOA(){
        System.debug('callingList:::::::'+callingList);
        if( String.isNotBlank( callingList[0].Registration_ID__c ) ) {
            return fetchUnitFMSOA( callingList[0].Registration_ID__c , callingList[0].Id , null,null);
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter registration id for Generating SOA'));  return null;
        }
    }// End of generateFMSOA
    
    public Static PageReference fetchUnitFMSOA( String strRegId , Id clId , Id buId,Id accId ) {
        if( String.isNotBlank( strRegId ) ) {
            String soaUrl;
            try {
                if(Label.UseCoffeeForFMSoa.equalsIgnoreCase('Y')){
                    soaUrl = FmIpmsRestCoffeeServices.getFMSoa( strRegId );
                }else{
                    FmIpmsRestServices.getUnitSoaByRegistrationId( strRegId );
                }
                
                
            } catch(Exception excp) {errorLogger( excp.getMessage()+' at ' +String.valueOf( excp.getLineNumber() ) , clId ,buId,accId  );
            }           
        
            if (String.isBlank(soaUrl)) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error : Unit SOA generation failed. Please try again'));return null;                
            }else {
                Pagereference pg = new PageReference(soaUrl); pg.setRedirect(true); return pg;
            }
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter registration id for Generating SOA')); return null;
        }
    }// End of fetchUnitFMSOA

    public PageReference generateFMPartySOA(){
        
        if( String.isNotBlank( callingList[0].Party_ID__c ) ) {
                return fetchPartyWiseFMSOA( callingList[0].Party_ID__c , callingList[0].Id , null, null);
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please provide party id for Generating SOA'));
            return null;
        }

    }// End of generateFMPartySOA    

    public Static PageReference fetchPartyWiseFMSOA( String strPartyId , Id clId ,Id buId , Id accId) {
        
        if( String.isNotBlank( strPartyId ) ) {
            String soaUrl;
            try {
                soaUrl = FmIpmsRestServices.getBulkSoaUrlForPartyId( strPartyId );
            } catch(Exception excp) {
                errorLogger( excp.getMessage()+' at ' +String.valueOf( excp.getLineNumber() ) ,  clId ,buId,accId );
            }           
        
            if (String.isBlank(soaUrl)) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error : SOA generation failed. Please try again'));
                return null;                
            }else {
                Pagereference pg = new PageReference(soaUrl);
                pg.setRedirect(true);
                return pg;
            }
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please provide party id for Generating SOA'));
            return null;
        }


    }// End of fetchPartyWiseFMSOA
    
    public PageReference generateFMPropertyWiseSOA(){
        
        if( String.isNotBlank( callingList[0].Unit_Name__c ) ) {
            
            String soaUrl;
            try {
                soaUrl = FmIpmsRestServices.getBulkSoaUrlForPartyIdAndProjectCode(callingList[0].Party_ID__c,callingList[0].Unit_Name__c.substringBefore('/'));
                system.debug( ' soaUrl '+ soaUrl);
            } catch(Exception excp) { errorLogger( excp.getMessage()+' at ' +String.valueOf( excp.getLineNumber() ) , callingList[0].Id , null , null );
            }           
        
            if (String.isBlank(soaUrl)) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error : SOA generation failed. Please try again')); return null;                
            }else {
                Pagereference pg = new PageReference(soaUrl); pg.setRedirect(true);return pg;
            }
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please provide property code for Generating SOA')); return null;
        }


    }// End of generateFMPropertyWiseSOA    
    
    
    private static void errorLogger(string strErrorMessage,Id clId, Id buId , Id accId ){
        Error_Log__c objError = new Error_Log__c(); objError.Error_Details__c = strErrorMessage;
        objError.Calling_List__c = clId;objError.Booking_Unit__c = buId;objError.Account__c = accId;insert objError;
    }           
    
}