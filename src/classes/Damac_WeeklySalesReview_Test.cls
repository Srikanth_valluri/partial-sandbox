/************************************************************************************************
 * @Name              : Damac_WeeklySalesReview_Test
 * @Description       : Test Class for Damac_WeeklySalesReview
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         QBurst         20/05/2020       Created
***********************************************************************************************/
@isTest
public class Damac_WeeklySalesReview_Test {

    @isTest
    static void test_method_1() {
        User dosUser;
        User hosUser;
        String rmId = '';
        for(User usr: [SELECT Id, Name, Manager.Id FROM User WHERE Profile.Name = 'Director of Sales' AND IsActive = TRUE LIMIT 5]){
            List<User> rmList = new List<User>();
            for(User rm: [SELECT Id FROM User WHERE IsActive = TRUE AND Manager.Id =: usr.Id]){
                rmList.add(rm);
                rmId = rm.Id;
            }
            if(rmList.size() > 0){
                dosUser = usr;
                break;
            }
        }
        hosUser = [SELECT Id, Manager.Id FROM User WHERE Id =: dosUser.Manager.Id];
        System.runAs(dosUser) {
            Test.startTest();
            PageReference pageRef = Page.Damac_WeeklySalesReview;
            Test.setCurrentPageReference(pageRef);
            Damac_WeeklySalesReview ctlr = new Damac_WeeklySalesReview();
            ctlr.fetchWSR();
            ctlr.fetchWSRDetails();
            ctlr.createWSR();
            ctlr.fetchWSR();
            ctlr.fetchWSRDetails();
            ctlr.saveWSR();
            ctlr.submitWSR();
            ctlr.saveHOSWSR();
            ctlr.updateWSRToPendingAck();
            ctlr.closeWSR();
            ctlr.closeWSRByDOS();
            Test.stopTest();
        }
        Weekly_Sales_Review__c wsr = [SELECT Id FROM Weekly_Sales_Review__c ORDER BY CreatedDate DESC LIMIT 1]; 
        System.runAs(hosUser) {
            PageReference pageRef = Page.Damac_WeeklySalesReview;
            Test.setCurrentPageReference(pageRef);
            System.currentPageReference().getParameters().put('absentList', rmId);
            
            Damac_WeeklySalesReview ctlr = new Damac_WeeklySalesReview();
            ctlr.fetchWSR();
            ctlr.fetchWSRDetails();
            ctlr.saveWSR();
            ctlr.fetchWSR();
            ctlr.submitWSR();
            ctlr.saveHOSWSR();
            ctlr.updateWSRToPendingAck();
            ctlr.closeWSR();
            ctlr.closeWSRByDOS();
            System.currentPageReference().getParameters().put('id', '');
            Damac_WeeklySalesReview ctlr2 = new Damac_WeeklySalesReview();
            ctlr2.createWSR();
            ctlr2.fetchWSR();
            ctlr2.fetchWSRDetails();
            ctlr2.submitWSR();
            ctlr2.sendClosedNotificationEmail();
            
        }
        wsr = [SELECT Id FROM Weekly_Sales_Review__c ORDER BY CreatedDate DESC LIMIT 1]; 
        User hodUser = [SELECT Id FROM User WHERE Id =: hosUser.Manager.Id];
        System.runAs(hodUser) {
            PageReference pageRef = Page.Damac_WeeklySalesReview;
            Test.setCurrentPageReference(pageRef);
            System.currentPageReference().getParameters().put('id', wsr.Id);
            Damac_WeeklySalesReview ctlr = new Damac_WeeklySalesReview();
            ctlr.fetchWSR();
            ctlr.fetchWSRDetails();
        }
        PageReference pageRef = Page.Damac_WeeklySalesReview;
        Test.setCurrentPageReference(pageRef);
        System.currentPageReference().getParameters().put('id', wsr.Id);
        Damac_WeeklySalesReview ctlr = new Damac_WeeklySalesReview();
    }
}