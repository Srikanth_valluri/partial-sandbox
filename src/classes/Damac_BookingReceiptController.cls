public class Damac_BookingReceiptController {
    public NSIBPM__Service_Request__c srRecord { get; set; }
    public Buyer__c primaryBuyer { get; set; }
    public String tokenAmount { get; set; }
    public String unitNames { get; set; }
    public String attachmentLink { get; set; }
    public Damac_BookingReceiptController () {
        String srRecordId = apexpages.currentpage().getparameters().get('id');
        srRecord = new NSIBPM__Service_Request__c ();
        primaryBuyer = new Buyer__c ();
        tokenAmount = '';
        unitNames = '';
        attachmentLink = '';
        srRecord = Database.query ('SELECT createdBy.Name, '+ getAllFields ('NSIBPM__Service_Request__c') +' FROM NSIBPM__Service_Request__c WHERE id=:srRecordId');
        if (srrecord.Token_Attachment_Name__c != NULL)
        	attachmentLink = [SELECT ID FROM Attachment WHERE ParentId =: srRecord.ID AND Name =: srrecord.Token_Attachment_Name__c Order By createdDate Desc LIMIT 1].ID;
        
        primaryBuyer = Database.query ('SELECT '+ getAllFields ('Buyer__c') +' FROM Buyer__c WHERE  Booking__r.Deal_SR__c =:srRecordId');
        for (Booking_Unit__c unit :[SELECT Unit_Location__c, Unit_Name__c from Booking_Unit__c WHERE Booking__r.Deal_SR__c =:srRecordId]) {
            unitNames += unit.Unit_Location__c+', ';
        }
        unitNames = unitNames.removeEND (', ');
        if (unitNames == NULL || unitNames == 'null')
            unitNames = '';
        tokenAmount = NumberToWord.english_number(integer.valueof(srRecord.Token_Amount_AED__c ));
    }
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null)
            return fields;
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            fields += sfield.getDescribe().getName ()+ ', ';
        }
        return fields.removeEnd(', ');
    }

}