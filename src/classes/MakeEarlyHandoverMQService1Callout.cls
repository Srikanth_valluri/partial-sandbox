public class MakeEarlyHandoverMQService1Callout {

  @InvocableMethod (label='' description='')
   public static void invokeapexcallout(list<Id> lstCaseId) {
       system.debug('inside main');
       list<string> lstRegId = new list<String>();
       list<string> lstLeaseRegId = new list<String>();
       list<case> lstCase = new list<case>();
       lstCase = [select id
       				, Booking_Unit__r.Registration_ID__c 
       				, RecordTypeId
       				, RecordType.DeveloperName
       				, Call_Outcome__c
       			 from Case 
       			 where id in: lstCaseId]; 
       for(case objCase: lstCase) {
           if (objCase.RecordType.DeveloperName == 'Early_Handover'){
           		lstRegId.add(objCase.Booking_Unit__r.Registration_ID__c);
           } else if (objCase.RecordType.DeveloperName == 'Lease_Handover'){
           		if (objCase.Call_Outcome__c != null && 
           			(objCase.Call_Outcome__c.equalsIgnoreCase('Lease Handover without Rental Guarantee')
           			|| objCase.Call_Outcome__c.equalsIgnoreCase('Lease Handover with Rental Guarantee'))) {
           			lstLeaseRegId.add(objCase.Booking_Unit__r.Registration_ID__c);
           		}
           }
       }
       if (lstRegId != null && lstRegId.size() > 0){
       		CallEarlyHandoverMQService1.CallEarlyHandoverMQService1Name('E', 'N', lstRegId);
       } else if (lstLeaseRegId != null && lstLeaseRegId.size() > 0){
       		CallEarlyHandoverMQService1.CallEarlyHandoverMQService1Name('L', 'N', lstRegId);
       }
   }
}