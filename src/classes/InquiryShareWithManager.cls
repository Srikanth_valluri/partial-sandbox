/**************************************************************************************
* Description - The class developed to share community User's record with their manager
*
* Version            Date            Author            Description
* 1.0                01/03/18        Monali            Initial Draft
* 1.1                01/03/18        Craig             Modified the logic
***************************************************************************************/

public without sharing class InquiryShareWithManager {

    public Set<Id> inqIds;
    public Set<Id> creatorOwnerIdSet;
    public Map<Id, String> mapInq_Creator;
    public Map<Id, String> mapInq_Owner;
    public Map<Id, List<String>> mapInq_Manager;
    public List<Inquiry__Share> existingShare ;
    public List<String>mangerLst;

    public InquiryShareWithManager() {
        inqIds = new Set<Id>();
        creatorOwnerIdSet = new Set<Id>();
        mapInq_Creator = new Map<Id, String>();
        mapInq_Owner = new Map<Id, String>();
        mapInq_Manager = new Map<Id, List<String>>();
        existingShare = new List<Inquiry__Share>();
    }
    
    public void InquiryShare(List<Inquiry__c> listInquiry){

        // Get Inquiry Owner Ids and Creator Ids 
        for(Inquiry__c inq : listInquiry) {
            inqIds.add(inq.Id);
            creatorOwnerIdSet.add(inq.CreatedById);
            creatorOwnerIdSet.add(inq.OwnerId);
            System.debug('OwnerId::'+inq.OwnerId);
            System.debug('CreatedById::'+inq.CreatedById);
            mapInq_Creator.put(inq.Id, inq.CreatedById);
            mapInq_Owner.put(inq.Id, inq.OwnerId);
        }

        // Get existing Inquiry Share Records
        for(Inquiry__Share inqShare : [ SELECT Id
                                             , RowCause
                                             , UserOrGroupId
                                             , ParentId
                                          FROM Inquiry__Share 
                                         WHERE ParentId IN :inqIds
        ]) {
            System.debug('inqShare.RowCause::'+inqShare.RowCause);
            if (inqShare.RowCause != 'Owner' 
                && inqShare.RowCause != Schema.Inquiry__Share.RowCause.Promoter__c
            ) {
                existingShare.add(inqShare);
            }
            
        }

        System.debug('==existingShare==' + existingShare);
        System.debug('==mapInq_Creator==' + mapInq_Creator);
        System.debug('==mapInq_Owner==' + mapInq_Owner);
        System.debug('==creatorOwnerIdSet==' + creatorOwnerIdSet);

        //Query user to get Managers
        for(User userObj : [SELECT Id
                                 , Name
                                 , ContactId
                                 , ManagerId
                                 , Manager.ManagerId
                                 , Manager.Manager.ManagerId
                                 , Manager.Manager.Manager.ManagerId
                                 , Manager.Manager.Manager.Manager.ManagerId
                              FROM User 
                             WHERE Id IN :creatorOwnerIdSet
        ]) {
            for(Id inqId : mapInq_Creator.keySet()) {

                if(!mapInq_Manager.containsKey(inqId)){
                     mapInq_Manager.put(inqId, new List<String>{});
                }

                // Share the record with the Creator if Creator is not Community User
                if (String.isBlank(userObj.ContactId)
                    && userObj.Id == mapInq_Creator.get(inqId)) {
                    mapInq_Manager.get(inqId).add(userObj.Id);
                }

                System.debug('==userObj.Name==' + userObj.Name);
                if(userObj.Id == mapInq_Creator.get(inqId)
                    /*|| userObj.Id == mapInq_Owner.get(inqId)*/
                ) {
                    System.debug('=IF==' + userObj.Name);
                    if(String.isNotBlank(userObj.ManagerId)) {
                        mapInq_Manager.get(inqId).add(userObj.ManagerId);
                        if(String.isNotBlank(userObj.Manager.ManagerId)) {
                            mapInq_Manager.get(inqId).add(userObj.Manager.ManagerId);
                            if(String.isNotBlank(userObj.Manager.Manager.ManagerId)) {
                                mapInq_Manager.get(inqId).add(userObj.Manager.Manager.ManagerId);
                                if(String.isNotBlank(userObj.Manager.Manager.Manager.ManagerId)) {
                                    mapInq_Manager.get(inqId).add(userObj.Manager.Manager.Manager.ManagerId);
                                    if(String.isNotBlank(userObj.Manager.Manager.Manager.Manager.ManagerId)) {
                                        mapInq_Manager.get(inqId).add(userObj.Manager.Manager.Manager.Manager.ManagerId);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        System.debug('==UserInfo==' + UserInfo.getUserId());
        System.debug('==mapInq_Manager==' + mapInq_Manager);
        List<Inquiry__Share> lstShare = new List<Inquiry__Share>();

        for(Id inqId : mapInq_Manager.keySet()) {
            System.debug('==inqId==' + inqId);
            mangerLst = new List<String>();
            mangerLst.addAll(mapInq_Manager.get(inqId));
            for(String managerId : mangerLst) {
                System.debug('==managerId==' + managerId);
                if(String.isNotBlank(managerId)) {
                    Inquiry__Share shareObj = new Inquiry__Share();
                    shareObj.AccessLevel = 'Read';
                    shareObj.ParentId = inqId;
                    shareObj.UserOrGroupId = managerId;
                    shareObj.RowCause = Schema.Inquiry__Share.RowCause.Promoter__c;
                    lstShare.add(shareObj);
                }
            }
        }
        try{
            System.debug('=Try block=lstShare==' + lstShare);
            // DML
            insert lstShare;
            delete existingShare;
        } catch(Exception e) {
            System.debug('==InquiryShareBatch==Exception=' + e);
        }
    }
}