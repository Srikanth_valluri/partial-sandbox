@isTest
private class FMAmenityBookingCancelPageControllerTest {

	@isTest static void test_method_one() {
        Resource__c objRes = new Resource__c();
        objRes.Chargeable_Fee_Slot__c = 10;
        objRes.Non_Operational_Days__c = 'Sunday';
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.No_of_advance_booking_days__c = 2;
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.Deposit__c = 3;
        objRes.No_of_deposit_due_days__c = 1;
        insert objRes;
        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                            get('Amenity Booking').getRecordTypeId();
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Booking_Date__c = Date.Today();
        objFMCase.Resource_Booking_Type__c = 'FM Amenity';
        objFMCase.RecordTypeId =devRecordTypeId;
        // objFMCase.Resource_Booking_Deposit__c = 20;
        objFMCase.Amenity_Booking_Status__c = 'Booking Confirmed';
        objFMCase.Resource__c = objRes.Id;
        objFMCase.Admin__c = UserInfo.getUserId();
        insert objFMCase;
        Task objRefundTask = new Task();
        objRefundTask.whatId = objFMCase.Id;
        objRefundTask.Subject = Label.FM_CollectFeeTaskLabel;
        objRefundTask.Status = 'Closed';
        objRefundTask.Priority = 'Normal';
        objRefundTask.Process_Name__c = 'Amenity Booking';
        objRefundTask.ActivityDate = Date.today().addDays(2);
        objRefundTask.Assigned_User__c = 'FM Admin';
        insert objRefundTask;
		Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(objFMCase);
        FMAmenityBookingCancelPageController controller = new FMAmenityBookingCancelPageController(sc);
        PageReference pageRef = Page.FMAmenityBookingCancelPage;
        pageRef.getParameters().put('id', String.valueOf(objFMCase.Id));
        Test.setCurrentPage(pageRef);
        controller.cancelAmenityBooking();
        Test.stopTest();
	}
    @isTest static void test_method_2() {
        Resource__c objRes = new Resource__c();
        objRes.Chargeable_Fee_Slot__c = 10;
        objRes.Non_Operational_Days__c = 'Sunday';
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.No_of_advance_booking_days__c = 2;
        objRes.Resource_Type__c = 'FM Amenity';
        insert objRes;
        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                            get('Amenity Booking').getRecordTypeId();
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Booking_Date__c = Date.Today();
        objFMCase.Resource_Booking_Type__c = 'FM Amenity';
        objFMCase.RecordTypeId =devRecordTypeId;
        // objFMCase.Resource_Booking_Deposit__c = 20;
        objFMCase.Amenity_Booking_Status__c = 'Booking Confirmed';
        objFMCase.Resource__c = objRes.Id;
        objFMCase.Admin__c = UserInfo.getUserId();
        insert objFMCase;
		Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(objFMCase);
        FMAmenityBookingCancelPageController controller = new FMAmenityBookingCancelPageController(sc);
        PageReference pageRef = Page.FMAmenityBookingCancelPage;
        pageRef.getParameters().put('id', String.valueOf(objFMCase.Id));
        Test.setCurrentPage(pageRef);
        controller.cancelAmenityBooking();
        Test.stopTest();
	}

}