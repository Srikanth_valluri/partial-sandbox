/************************************************************************************
* Description - Rest service to update DLP Expiry Date                              *
*                                                                                   *
* Version   Date            Author              Description                         *
* 1.0       28/11/2019      Aishwarya Todkar    Initial Draft.                      *   
*************************************************************************************/
public class DLP_ExpiryDateUpdateService {
   
    @InvocableMethod
    public static void getRecord( List<String> listUnitNames ) {
        System.debug( 'listUnitNames==' + listUnitNames);

        if( listUnitNames != null && listUnitNames.size() > 0 ) {

            List<Booking_Unit__c> listBU = 
                new List<Booking_Unit__c>( [SELECT
                    Id
                    , DLP_End_Date__c
                FROM
                    Booking_Unit__c
                WHERE
                    Unit_Name__c =: listUnitNames[0]
                AND 
                    DLP_End_Date__c != null
                LIMIT 1
            ]);

            List<CAFM_Location__c> listCafmLocation =  
                new List<CAFM_Location__c>( [ SELECT
                                                Id
                                                , Location_Id__c
                                                , Unit_Code__c
                                            FROM
                                                CAFM_Location__c
                                            WHERE
                                                Unit_Code__c != null
                                            AND
                                                Unit_Code__c =: listUnitNames[0]
                                            AND
                                                Location_Id__c != null
                                            LIMIT 1
                                        ] );
            System.debug( 'listBU == ' + listBU );
            System.debug( 'listCafmLocation == ' + listCafmLocation );

            if( listCafmLocation != null && listCafmLocation.size() > 0 
                && listBU != null && listBU.size() > 0 ) {
                    String dlpDate = listBU[0].DLP_End_Date__c.day() + '/' 
                                    + listBU[0].DLP_End_Date__c.month() + '/' 
                                    + listBU[0].DLP_End_Date__c.year();

                System.enqueueJob( new DLP_ExpiryDateUpdateServiceQueueable ( 
                                        listCafmLocation[0].Location_Id__c
                                        , dlpDate
                                        , listBU[0].Id )
                                );
                //updateDLPExpiryDate( listCafmLocation[0].Location_Id__c, dlpDate, listBU[0].Id );
            }
       }
    }

    /*@future( callout=true )
    public static void updateDLPExpiryDate( String locationId, String dlpDate, Id buId ) {

       if( String.isNotBlank( locationId ) && String.isNotBlank( dlpDate ) ) {
           String accessToken = Test.isRunningTest() ? 'testToken' : FmIpmsRestCoffeeServices.getNewBearerToken();
            if( String.isNotBlank( accessToken ) ) {
               
                Credentials_Details__c creds = RebateOnAdvanceService.getCredentials('DLP Expiry Date Update Service');
                System.debug( 'creds = ' + creds);
                if( creds != null && String.isNotBlank( creds.Endpoint__c ) ) {
                    
                    String requestBody = '{' +
                                            '"cafmUnitCode":"' + locationId + '"' +
                                            ',"dlpDate":"' + dlpDate + 
                                        '"}';
                    String userName = creds.User_Name__c; //crp1user
                    String password = creds.Password__c; //oracle_user
                    String endPoint = creds.Endpoint__c;
                    String headerValue = 'Basic ' + EncodingUtil.base64Encode( Blob.valueOf( userName + ':' + password ) );
                    HttpRequest request = new HttpRequest();
                    request.setMethod( 'POST' );
                    request.setHeader( 'Accept','application/json');
                    request.setHeader( 'Content-Type','application/json' );
                    request.setBody( requestBody );
                    request.setEndPoint( endPoint );
                    request.setHeader('Authorization' ,'Bearer' + accessToken);//headerValue );
                    System.debug('request = ' + request);
                    System.debug('requestBody = ' + request.getBody());

                    HttpResponse response = new HttpResponse();
                    
                    try {
                        response = new Http().send( request );

                        System.debug('response body ==' + response.getBody());
                        System.debug('response status code ==' + response.getStatusCode());
                        System.debug('response status ==' + response.getStatus());

                        if( response != null && response.getBody() != null) {
                            DlpResponseWrapper dlpWrapObj = new DlpResponseWrapper();
                            dlpWrapObj = ( DlpResponseWrapper ) JSON.deserialize( response.getBody(), DlpResponseWrapper.class );
                            System.debug( 'dlpWrapObj = ' + dlpWrapObj );
                            String resMsg = dlpWrapObj .Status + ': ' + dlpWrapObj.responseMessage;
                            insert GenericUtility.createErrorLog(resMsg  , null, buId, null, null);
                        }
                    }
                    catch( Exception e ) {
                        System.debug('exception--' + e.getMessage()) ;
                        insert GenericUtility.createErrorLog( e.getMessage(), null, buId, null, null);
                    }
               }//End Credentials settings if
           }// End accessToken if
        }//End locationId & dlpDate if
    }// updateDLPExpiryDate method
    
    class DlpResponseWrapper {
        String responseId;
        String responseTime;
        String requestName;
        String status;
        String responseMessage;
        Integer elapsedTimeMs;
        Boolean complete;
    }*/
}