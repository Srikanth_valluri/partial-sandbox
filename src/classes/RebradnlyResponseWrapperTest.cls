@isTest
public with sharing class RebradnlyResponseWrapperTest {
    
    @isTest
    public static void testParse () {
        String strJson = '{' +
            '"id": "3bff867afbb2497294e74f0e0e1144df",'+
            '"title": "Link x2y18ql",'+
            '"slashtag": "x2y18ql",'+
            '"destination": "https://www.youtube.com/channel/UCHK4HD0ltu1-I212icLPt3g",'+
            '"createdAt": "2020-02-24T10:03:48.000Z",'+
            '"updatedAt": "2020-02-24T10:03:48.000Z",'+
           '"status": "active",'+
           '"tags": [],'+
            '"clicks": 0,'+
            '"isPublic": false,'+
            '"shortUrl": "rebrand.ly/x2y18ql",'+
            '"domainId": "8f104cc5b6ee4a4ba7897b06ac2ddcfb",'+
            '"domainName": "rebrand.ly",'+
            '"domain": {'+
                '"id": "8f104cc5b6ee4a4ba7897b06ac2ddcfb",'+
                '"ref": "/domains/8f104cc5b6ee4a4ba7897b06ac2ddcfb",'+
                '"fullName": "rebrand.ly",'+
                '"active": true'+
            '},'+
            '"https": true,'+
            '"favourite": false,'+
            '"creator": {'+
                '"id": "010f4f33e9004e8c9840f37eb21a7d71",'+
                '"fullName": "Aishwarya Todkar",'+
                '"avatarUrl": "https://s.gravatar.com/avatar/5053d3f94b164eea6bbf974a9c94615c?size=80&d=retro&rating=g"'+
            '},'+
           ' "integrated": false'+
        '}';
        Test.startTest();
        RebradnlyResponseWrapper.parse( strJson );
        Test.stopTest();
    }
}