/****************************************************************************************************
* Name               : AP_SupportPageController                                                     *
* Description        : AP_SupportPage Page controller (Agents Portal)                               *
* Created Date       : 02-10-2018                                                                   *
* Created By         : Craig Lobo                                                                   *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE        Description                                                 *
* 1.0   Craig Lobo          02-10-2018  Initial Draft                                               *
****************************************************************************************************/

public with sharing class AP_SupportPageController {

    public String WHERE_CLAUSE = ' RecordType.Name = \'Support\' ';

    public Id SUPPORT_RT = Schema.SObjectType.Case__c.getRecordTypeInfosByName().get('Support').getRecordTypeId();
    public String errorJSON                                                             {get; set;}
    public String detailId                                                              {get; set;}
    public String casePrefix                                                            {get; set;}
    public String errorDisplay                                                          {get; set;}
    public String showError                                                             {get; set;}
    public String caseSubject                                                           {get; set;}
    public String supportDesc                                                           {get; set;}
    public String requestedDateTime                                                     {get; set;}
    public Integer numberOfNights                                                       {get; set;}
    public Boolean isNewTicket                                                          {get; set;}
    public Boolean isValid                                                              {get; set;}
    public boolean isAdditionalData                                                     {get; set;}
    public Boolean isTierBenefits;
    public Case__c newCase                                                              {get; set;}
    public List<Case__c> caseList                                                       {get; set;}
    public List<SelectOption> caseSubjectOptionsList                                    {get; set;}
    public List<Schema.FieldSetMember> fieldList                                        {get; set;}
    public Map<String, Schema.SObjectType> globalDescribeMap;
    public Map<String, String> fieldAPIErrorMsgMap                                      {get; set;}

    private transient Schema.DescribeSObjectResult describeSObjectResultObj;
    private transient Schema.FieldSet fieldSetObj;


    public AP_SupportPageController() {
        loadCasesList();
    }

    public void init() {
        casePrefix = '';
        errorJSON = '';
        detailId = '';
        supportDesc = '';
        caseSubject = '';
        errorDisplay = 'none';
        requestedDateTime = '';
        isValid = true;
        isNewTicket = false;
        isTierBenefits = false;
        isAdditionalData = false;
        newCase = new Case__c();
        caseList = new List<Case__c>();
        caseSubjectOptionsList = new List<SelectOption>();
        globalDescribeMap = new Map<String, Schema.SObjectType>();
        fieldAPIErrorMsgMap = new Map<String, String>();
    }


    public void loadCasesList(){
        init();
        Contact loginContact = UtilityQueryManager.getContactInformation();
        String condition = WHERE_CLAUSE;
        if(null != loginContact
            && (loginContact.Portal_Administrator__c
                || loginContact.Authorised_Signatory__c
                || loginContact.Owner__c
        )){
            Set<Id> userIds = UtilityQueryManager.getAllUsers(loginContact.AccountID);
            System.debug('userIds >>>> ' + userIds);
            condition = condition
                      + ' AND CreatedById IN :userIds ';//OwnerId IN :userIds OR
            System.debug('condition >>>> ' + condition);
            caseList = UtilityQueryManager.getCases(condition, userIds);
        } else {
            condition = ' CreatedById = \''
                      + UserInfo.getUserId()
                      + '\'';
            caseList = UtilityQueryManager.getCases(condition, null);
        }
        System.debug('caseList >>>> ' + caseList);
        casePrefix = DamacUtility.getObjectPrefix('Case__c');
    }

    /**
     * Method to navigate to the selected Agent Case Support Detail Page
     */
    public PageReference navigateToSupportDetail() {
        System.debug('detailId>>>> : ' + detailId);
        if (String.isNotBlank(detailId)) {
            PageReference supportDetailPage = Page.AP_SupportDetail;
            supportDetailPage.getParameters().put('Id', detailId);
            supportDetailPage.setRedirect(true);
            return supportDetailPage;
        }
        return null;
    }


    public PageReference addNewTicket(){
        System.debug('Save Case ');
        errorDisplay = 'none';
        fieldAPIErrorMsgMap = new Map<String, String>();
        System.debug('requestedDateTime>>>>>>> ' + requestedDateTime);
        System.debug('newCase>>>>>>> ' + newCase);
        isValid = validateCaseFields();
        System.debug('isValid>>>>>>> ' + isValid);
        if(isValid) {
            newCase.RecordTypeId = SUPPORT_RT;
            newCase.Case_Subject__c = newCase.Case_Subject__c.trim();
            newCase.Case_Subject__c = caseSubject;
            System.debug('INSERT newCase>>>>>>> ' + newCase);
            insert newCase;
            System.debug('relaodPage>>>> : ');
            /* 19-12-2018 */
           // PageReference supportPage = new PageReference('/AP_SupportPage');//Page.AP_SupportPage;
            PageReference supportPage = Page.AP_SupportPage;
            supportPage.setRedirect(true);
            return supportPage;
        }
        return null;
    }

    public Boolean validateCaseFields(){
        System.debug('Entered Validation>>  ');
        showError = '';
        errorJSON = '';
        newCase.Case_Subject__c = caseSubject;
        String subVal = newCase.Case_Subject__c;
        newCase.Case_Description__c = supportDesc;
        System.debug('Support_Category__c >>> ' + newCase.Support_Category__c);
        System.debug('Case_Subject__c>>>>  ' + newCase.Case_Subject__c);
        System.debug('Case_Description__c >>>' + newCase.Case_Description__c);

        if(String.isBlank(newCase.Support_Category__c)){
            errorDisplay = 'block';
            showError +='Error: Please Select Support Category. <br/>';
            fieldAPIErrorMsgMap.put('Support_Category__c', 'Please Select Support Category');
            errorJSON = JSON.serialize(fieldAPIErrorMsgMap);
            system.debug('Support_Category__c >>> ' + showError);
            return false;
        } else if(String.isBlank(newCase.Case_Subject__c)){
            errorDisplay = 'block';
            showError += 'Error: Please Select Support Subject. <br/>';
            fieldAPIErrorMsgMap.put('Case_Subject__c', 'Please Select Support Subject');
            errorJSON = JSON.serialize(fieldAPIErrorMsgMap);
            system.debug(showError);
            return false;
        } else if(String.isBlank(newCase.Case_Description__c)){
            errorDisplay = 'block';
            showError += 'Error: Please fill Support Description. <br/>';
            fieldAPIErrorMsgMap.put('Case_Description__c', 'Please fill Support Description');
            errorJSON = JSON.serialize(fieldAPIErrorMsgMap);
            system.debug('Case_Description__c>>>>>' + showError);
            return false;
        } else if(subVal.trim().equalsIgnoreCase('Request for Marketing Support')){
            boolean retVal = true;
            newCase.Date_Time__c = convertDateString(requestedDateTime);
            retVal = validateRequestedDateTime();

            if (newCase.Date_Time__c == null) {
                errorDisplay='block';
                showError = 'Error: Please fill Date and Time. <br/>';
                fieldAPIErrorMsgMap.put('Date_Time__c', 'Please fill Date and Time');
                errorJSON = JSON.serialize(fieldAPIErrorMsgMap);
                system.debug('Date_Time__c>>>>>' + showError);
                retVal = retVal && false;
            } else {
                retVal = validateRequestedDateTime();
            }
            system.debug('retVal - ' + retVal);
            return retVal;
        } else if(subVal.trim().equalsIgnoreCase('request for site viewing')){
            boolean retVal = true;
            newCase.Date_Time__c = convertDateString(requestedDateTime);
            retVal = validateRequestedDateTime();
            system.debug(' validateRequestedDateTime retVal - ' + retVal);
            if(null == newCase.Project_Name__c){
                errorDisplay = 'block';
                showError += 'Error: Please fill Project Name. <br/>';
                fieldAPIErrorMsgMap.put('Project_Name__c', 'Please fill Project Name');
                errorJSON = JSON.serialize(fieldAPIErrorMsgMap);
                system.debug('Project_Name__c>>>>>' + showError);
                retVal = retVal && false;
            }
            system.debug('retVal - ' + retVal);
            return retVal;
        } else if(subVal.trim().equalsIgnoreCase('book a hotel')){
            boolean retVal = true;
            System.debug('numberOfNights>>>>>' + numberOfNights);
            newCase.Date_Time__c = convertDateString(requestedDateTime);
            retVal = validateRequestedDateTime();
            if (numberOfNights !=null && numberOfNights > 0) {
                System.debug('INNN numberOfNights>>>>>');
                newCase.No_of_Nights__c = numberOfNights;
            } else {

                errorDisplay = 'block';
                showError = 'Error: Please fill No. of Nights. Value should be Numeric and Greater than 0. <br/>';
                fieldAPIErrorMsgMap.put('No_of_Nights__c', 'Please fill No. of Nights. Value should be Numeric and Greater than 0');
                errorJSON = JSON.serialize(fieldAPIErrorMsgMap);
                System.debug('ERROR numberOfNights>>>>>' +  showError);
                return retVal = retVal && false;
            }
            return retVal;
        }  else if(subVal.trim().equalsIgnoreCase('book a luxury car')){
            boolean retVal = true;
            newCase.Date_Time__c = convertDateString(requestedDateTime);
            retVal = validateRequestedDateTime();
            if(String.isBlank(newCase.Pick_Up_Location__c)){
                errorDisplay = 'block';
                showError +='Error: Please fill Pick Up Location. <br/>';
                fieldAPIErrorMsgMap.put('Pick_Up_Location__c', 'Please fill Pick Up Location');
                errorJSON = JSON.serialize(fieldAPIErrorMsgMap);
                retVal = retVal && false;
            }
            if(String.isBlank(newCase.Drop_Off_Location__c)){
                errorDisplay = 'block';
                showError +='Error: Please fill Drop Off Location. <br/>';
                fieldAPIErrorMsgMap.put('Drop_Off_Location__c', 'Please fill Drop Off Location');
                errorJSON = JSON.serialize(fieldAPIErrorMsgMap);
                retVal = retVal && false;
            }
            System.debug('book a luxury car>>>>' +  showError);
            return retVal;
        } else if(subVal.trim().equalsIgnoreCase('book a meeting room')){
            boolean retVal = true;
            newCase.Date_Time__c = convertDateString(requestedDateTime);
            retVal = validateRequestedDateTime();

            if(String.isBlank(newCase.Sales_Office__c)){
                errorDisplay = 'block';
                showError += 'Error: Please fill Sales Office. <br/>';
                fieldAPIErrorMsgMap.put('Sales_Office__c', 'Please fill Sales Office');
                errorJSON = JSON.serialize(fieldAPIErrorMsgMap);
                retVal = retVal && false;
            }
             System.debug('book a meeting room' +  showError);
            return retVal;
        } else if(subVal.trim().equalsIgnoreCase('Commission Payment')){
            boolean retVal = true;
            if(String.isBlank(newCase.Unit_Number__c)){
                errorDisplay = 'block';
                showError += 'Error: Please fill Unit Number. <br/>';
                fieldAPIErrorMsgMap.put('Unit_Number__c', 'Please fill Unit Number');
                errorJSON = JSON.serialize(fieldAPIErrorMsgMap);
                retVal = retVal && false;
            }
             System.debug('Commission Payment' +  showError);
            return retVal;
        } else if(subVal.equalsIgnoreCase('request for brochures')){ // subVal.trim().equalsIgnoreCase('request for training') ||
            Boolean retVal = true;
            System.debug('*newCase.Project_Name__ct>>> ' + newCase.Project_Name__c);
            newCase.Date_Time__c = convertDateString(requestedDateTime);
            retVal = validateRequestedDateTime();
            if(String.isBlank(newCase.Project_Name__c)){
                errorDisplay = 'block';
                showError += 'Error: Please fill Project Name.<br/>';
                fieldAPIErrorMsgMap.put('Project_Name__c', 'Please fill Project Name');
                errorJSON = JSON.serialize(fieldAPIErrorMsgMap);
                System.debug('retVal>>>t>>> ' + retVal);
                retVal = retVal && false;
                System.debug('rAFTERetVal> ' +retVal);
            }
            system.debug('FINALretVal>>>>>' + retVal);
            System.debug('request for brochures' +  showError);
            return retVal;
        } else if(subVal.trim().equalsIgnoreCase('query on unit commission')){
            boolean retVal = true;
            System.debug('**Query on Unit');
            if(String.isBlank(newCase.Unit_Number__c)){
                errorDisplay = 'block';
                showError += 'Error: Please fill Unit Number.<br/>';
                fieldAPIErrorMsgMap.put('Unit_Number__c', 'Please fill Unit Number');
                errorJSON = JSON.serialize(fieldAPIErrorMsgMap);
                retVal = retVal && false;
            }
            System.debug('**Query on Unit retVal>>>>  ' + retVal );
            System.debug('query on unit commission' +  showError);
            return retVal;
        }
        return true;
    }


/*
    public boolean validateCaseFields(){
        System.debug('Entered Validation>>  ');
        showError = '';

        String subVal = newCase.Case_Subject__c;
        newCase.Case_Description__c = supportDesc;
        System.debug('Support_Category__c >>> ' + newCase.Support_Category__c);
        System.debug('Case_Subject__c>>>>  ' + newCase.Case_Subject__c);
        System.debug('Case_Description__c >>>' + newCase.Case_Description__c);

        if(String.isBlank(newCase.Support_Category__c)){
            errorDisplay = 'block';
            showError +='Error: Please Select Support Category. <br/>';
            system.debug(showError);
            return false;
        } else if(String.isBlank(newCase.Case_Subject__c)){
            errorDisplay = 'block';
            showError += 'Error: Please Select Support Subject. <br/>';
            system.debug(showError);
            return false;
        } else if(String.isBlank(newCase.Case_Description__c)){
            errorDisplay = 'block';
            showError += 'Error: Please Fill Support Description. <br/>';
            system.debug(showError);
            return false;
        } else if(subVal.trim().equalsIgnoreCase('Request for Marketing Support')){
            boolean retVal = true;
            newCase.Date_Time__c = convertDateString(requestedDateTime);
            retVal = validateRequestedDateTime();

            if (newCase.Date_Time__c == null) {
                errorDisplay='block';
                showError = 'Error: Please Fill Date and Time. <br/>';
                retVal = retVal && false;
            } else {
                retVal = validateRequestedDateTime();
            }

            return retVal;
        } else if(subVal.trim().equalsIgnoreCase('request for site viewing')){
            boolean retVal = true;
            newCase.Date_Time__c = convertDateString(requestedDateTime);
            retVal = validateRequestedDateTime();

            if(null == newCase.Project_Name__c){
                errorDisplay = 'block';
                showError += 'Error: Please Fill Project Name. <br/>';
                retVal = retVal && false;
            }

            return retVal;
        } else if(subVal.trim().equalsIgnoreCase('book a hotel')){
            boolean retVal = true;
            System.debug('numberOfNights>>>>>' + numberOfNights);
            newCase.Date_Time__c = convertDateString(requestedDateTime);
            retVal = validateRequestedDateTime();
            if (numberOfNights !=null && numberOfNights > 0) {
                System.debug('INNN numberOfNights>>>>>');
                newCase.No_of_Nights__c = numberOfNights;
            } else {
                System.debug('ERROR numberOfNights>>>>>');
                errorDisplay = 'block';
                showError = 'Error: Please Fill No. of Nights. Value should be Numeric and Greater than 0. <br/>';
                return retVal = retVal && false;
            }
            return retVal;
        }  else if(subVal.trim().equalsIgnoreCase('book a luxury car')){
            boolean retVal = true;
            newCase.Date_Time__c = convertDateString(requestedDateTime);
            retVal = validateRequestedDateTime();
            if(String.isBlank(newCase.Pick_Up_Location__c)){
                errorDisplay = 'block';
                showError +='Error: Please Fill Pick Up Location. <br/>';
                retVal = retVal && false;
            }
            if(String.isBlank(newCase.Drop_Off_Location__c)){
                errorDisplay = 'block';
                showError +='Error: Please Fill Drop Off Location. <br/>';
                retVal = retVal && false;
            }
            return retVal;
        } else if(subVal.trim().equalsIgnoreCase('book a meeting room')){
            boolean retVal = true;
            newCase.Date_Time__c = convertDateString(requestedDateTime);
            retVal = validateRequestedDateTime();

            if(String.isBlank(newCase.Sales_Office__c)){
                errorDisplay = 'block';
                showError += 'Error: Please Fill Sales Office. <br/>';
                retVal = retVal && false;
            }
            return retVal;
        } else if(subVal.trim().equalsIgnoreCase('Commission Payment')){
            boolean retVal = true;
            if(String.isBlank(newCase.Unit_Number__c)){
                errorDisplay = 'block';
                showError += 'Error: Please Fill Unit Number. <br/>';
                retVal = retVal && false;
            }
            return retVal;
        } else if(subVal.equalsIgnoreCase('request for brochures')){ // subVal.trim().equalsIgnoreCase('request for training') ||
            boolean retVal = true;
            newCase.Date_Time__c = convertDateString(requestedDateTime);
            retVal = validateRequestedDateTime();
            if(String.isBlank(newCase.Project_Name__c)){
                errorDisplay = 'block';
                showError += 'Error: Please Fill Project Name.<br/>';
                retVal = retVal && false;
            }
            return retVal;
        } else if(subVal.trim().equalsIgnoreCase('query on unit commission')){
            boolean retVal = true;
            System.debug('**Query on Unit');
            if(String.isBlank(newCase.Unit_Number__c)){
                errorDisplay = 'block';
                showError += 'Error: Please fill Unit Number.<br/>';
                retVal = retVal && false;
            }
            System.debug('**Query on Unit retVal>>>>  ' + retVal );
            return retVal;
        }
        return true;
    }
*/


    public boolean validateRequestedDateTime() {
        System.debug('newCase.Date_Time__c = ' + newCase.Date_Time__c);
        System.debug('System.now() = ' + System.now());
        if(null == newCase.Date_Time__c){
            System.debug('Date Null******************');
            errorDisplay = 'block';
            showError += 'Error: Please Fill Request Date & Time.<br/>';
            fieldAPIErrorMsgMap.put('Date_Time__c', 'Please fill Request Date & Time');
            errorJSON = JSON.serialize(fieldAPIErrorMsgMap);
            return false;
        } else if(newCase.Date_Time__c <= System.now()) {
            System.debug('Date Not Null******************');
            errorDisplay = 'block';
            showError += 'Error: Request Date & Time field must be a Future Date. <br/>';
            fieldAPIErrorMsgMap.put('Date_Time__c', 'Request Date & Time field must be a Future Date');
            errorJSON = JSON.serialize(fieldAPIErrorMsgMap);
            return false;
        }
        return true;
    }

    public void toggleCaseSubjects() {
        System.debug('caseSubjectOptionsList>>>> ' + caseSubjectOptionsList);
        //System.debug('Support_Category__c>>>> ' + newCase.Support_Category__c);
        List<String> picklistValueList = new List<String>();
        if (String.isNotBlank(newCase.Support_Category__c)) {
            if (newCase.Support_Category__c == 'My Customer Queries') {
                picklistValueList.addAll(DamacUtility.splitMutliSelect(LABEL.Support_Customer_Queries));
            } else if(newCase.Support_Category__c == 'My Sales Support') {
                picklistValueList.addAll(DamacUtility.splitMutliSelect(LABEL.Support_Sales_Support));
            } else if(newCase.Support_Category__c == 'My Support Needs') {
                picklistValueList.addAll(DamacUtility.splitMutliSelect(LABEL.Support_Support_Needs));
            }
        }

        System.debug('picklistValueList>>>> ' + picklistValueList);
        caseSubjectOptionsList = new List<SelectOption>();
        caseSubjectOptionsList.add(new SelectOption('', '--None--'));
        for(String subVal : picklistValueList){
            if (subVal.trim() == 'Book a Luxury Car' || subVal.trim() == 'Book a Hotel') {
                if (isTierBenefits == true) {
                    caseSubjectOptionsList.add(new SelectOption(subVal.trim(), subVal.trim()));
                }
            } else {
                caseSubjectOptionsList.add(new SelectOption(subVal.trim(), subVal.trim()));
            }
        }
        System.debug('caseSubjectOptionsList>>>> ' + caseSubjectOptionsList);
    }


    public void fieldSetValue(){
        System.debug('--caseSubject--'+ caseSubject);
        newCase.Case_Subject__c = caseSubject;
        System.debug('--fieldSetValue--'+newCase.Case_Subject__c);
        if(String.IsNotBlank(newCase.Case_Subject__c)){
            isAdditionalData = false;
            if(newCase.Case_Subject__c == 'SPA'){
                readFieldSet('SPA');
            } else if(newCase.Case_Subject__c == 'Request for site Viewing'){
                readFieldSet('Request_for_site_Viewing');
            } else if(newCase.Case_Subject__c == 'Book a Hotel'){
                readFieldSet('book_a_hotel');
            } else if(newCase.Case_Subject__c == 'Query on Unit Commission'){
                readFieldSet('Query_on_Unit_Commission');
            }  else if(newCase.Case_Subject__c == 'Request for Brochures'){
                readFieldSet('Request_for_Brochures');
            } else if(newCase.Case_Subject__c == 'Request for Marketing Support'){
                readFieldSet('Request_for_Marketing_Support');
            } else if(newCase.Case_Subject__c == 'Booking Related Queries'){
                readFieldSet('Booking_related_Queries');
            } else if(newCase.Case_Subject__c == 'Commission Payment'){
                readFieldSet('Commission_Payment');
            } else if(newCase.Case_Subject__c == 'Reinstatements'){
                readFieldSet('Reinstatements');
            } else if(newCase.Case_Subject__c == 'Reassignment'){
                readFieldSet('Reassignment');
            } else if(newCase.Case_Subject__c == 'Book a Luxury Car'){
                readFieldSet('Book_a_Luxury_Car');
            } else if(newCase.Case_Subject__c == 'Book a Meeting Room'){
                readFieldSet('Book_a_Meeting_Room');
            }
        }
    }

    public void readFieldSet(String fieldSetName) {
        try{
            System.debug('--fieldSetName--'+fieldSetName);
            globalDescribeMap = Schema.getGlobalDescribe();
            Schema.SObjectType SObjectTypeObj = globalDescribeMap.get('Case__c');
            describeSObjectResultObj = SObjectTypeObj.getDescribe();
            fieldSetObj = describeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
            System.debug('--fieldSetObj --'+fieldSetObj);
            fieldList = new List<Schema.FieldSetMember>();
            fieldList = fieldSetObj.getFields(); // all fields from field set
            System.debug('fieldList>>>> : ' + fieldList);
            isAdditionalData = true;
        } catch(exception ex){
              System.debug('Error Message'+ex.getMessage());
              System.debug('@Line Number'+ex.getLineNumber());
        }
    }

    public DateTime convertDateString(String pDateTime) {
        DateTime reqDateTimeVal;
        List<String> dateTimeList = pDateTime.split(' ');
        if (dateTimeList != null && dateTimeList.size() == 2) {
            String [] dateStr = dateTimeList[0].split('-');
            String [] timeStr = dateTimeList[1].split(':');
            reqDateTimeVal = DateTime.newInstance(
                Integer.valueof(dateStr[2]),
                Integer.valueof(dateStr[1]),
                Integer.valueof(dateStr[0]),
                Integer.valueof(timeStr[0]),
                Integer.valueof(timeStr[1]),
                00
            );
            System.debug('reqDateTimeVal====  ' + reqDateTimeVal);
            return reqDateTimeVal;

        } else {
            return null;
        }
    }

}