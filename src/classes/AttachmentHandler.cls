/**********************************************************************************************
Description: Handler for Attachment Trigger
----------------------------------------------------------------------------------------------
Version Date(DD-MM-YYYY)    Author              Version History
-----------------------------------------------------------------------------------------------
1.0     10-11-2019          Aishwarya Todkar    Initial Draft
2.0     20-08-2020          Neha Dave           Added functionality to clone the inserted
                                                attachments.
***********************************************************************************************/
public class AttachmentHandler {

    public static final String HANDOVER = 'Handover';
    public static final String EARLY_HANDOVER = 'Early Handover';
    
    @future( callout = true )
    public static void sendVATLetter( Id attachementId ) {

        if( attachementId != null ) {
            Attachment objAtt = [SELECT
                                    Id, ParentId,Body, Name
                                FROM
                                    Attachment
                                WHERE
                                    Id =: attachementId];
        
            if( objAtt != null && objAtt.ParentId != null 
            && String.ValueOf( objAtt.ParentId ).startsWith( AttachmentHandlerUploadDocument.keyPrefix( 'Booking_Unit__c' ))) {
                List<Booking_Unit__c> lstBU = [ SELECT
                                                    Id
                                                    , Account_Id__c
                                                    , Booking__r.Account__r.Person_Business_Email__c
                                                    , Booking__r.Account__r.Name
                                                    , Inventory__r.Property_Name__c
                                                    , Inventory__c
                                                FROM
                                                    Booking_Unit__c
                                                WHERE
                                                    Id =: objAtt.ParentId
                                                AND
                                                    Booking__r.Account__c != null
                                                AND
                                                    Booking__r.Account__r.Person_Business_Email__c != null ];

                System.debug( ' lstBU == ' + lstBU);
                if( lstBU != null && lstBU.size() > 0 ) {
                    //Get Sendgrid Email details from custom metadat
                    List<SendGrid_Email_Details__mdt> listVATCs 
                        = new List<SendGrid_Email_Details__mdt>( [SELECT
                                                                    From_Address__c
                                                                    , From_Name__c
                                                                    , To_Address__c
                                                                    , To_Name__c
                                                                    , CC_Address__c
                                                                    , CC_Name__c
                                                                    , Bcc_Address__c
                                                                    , Bcc_Name__c
                                                                    , Reply_To_Address__c
                                                                    , Reply_To_Name__c
                                                                    , Email_Template__c
                                                                FROM
                                                                    SendGrid_Email_Details__mdt
                                                                WHERE
                                                                    MasterLabel = 'VAT Letter Email Notification'
                                                                LIMIT 1
                                                                ] );
                    System.debug( ' listVATCs == ' + listVATCs);                                            
                    if( listVATCs != null && listVATCs.size() > 0) {
                        SendGrid_Email_Details__mdt objVATcs = listVATCs[0];
                        List<EmailTemplate> listVATEmailTemplate;
                        
                        if( String.isNotBlank(objVATcs.Email_Template__c) ) {
                            
                            //Get Email Template
                            listVATEmailTemplate = new List<EmailTemplate>( [SELECT 
                                                                                ID, Subject, Body, HtmlValue, TemplateType
                                                                            FROM 
                                                                                EmailTemplate 
                                                                            WHERE 
                                                                                Name =: objVATcs.Email_Template__c limit 1]
                                                                            );
                            system.debug('listVATEmailTemplate  === ' + listVATEmailTemplate );
                            //Prepare SendGrid Email Details
                            if( listVATEmailTemplate != null && listVATEmailTemplate.size() > 0 ) {
                                EmailTemplate objVATEmailTemplate = listVATEmailTemplate[0];

                                String toAddress = lstBU[0].Booking__r.Account__r.Person_Business_Email__c;
                                String toName = String.isNotBlank( objVATcs.To_Name__c ) ? objVATcs.To_Name__c : '';
                                String ccAddress = String.isNotBlank( objVATcs.CC_Address__c ) ? objVATcs.CC_Address__c : '';
                                String ccName = String.isNotBlank( objVATcs.CC_Name__c ) ? objVATcs.CC_Name__c : '';
                                String bccAddress = String.isNotBlank( objVATcs.Bcc_Address__c ) ? objVATcs.Bcc_Address__c : '';
                                String bccName = String.isNotBlank( objVATcs.Bcc_Name__c ) ? objVATcs.Bcc_Name__c : '';
                                String fromAddress = String.isNotBlank( objVATcs.From_Address__c ) ? objVATcs.From_Address__c : '';
                                String fromName = String.isNotBlank( objVATcs.From_Name__c ) ? objVATcs.From_Name__c : '';
                                String replyToAddress = String.isNotBlank( objVATcs.Reply_To_Address__c ) ? objVATcs.Reply_To_Address__c : '';
                                String replyToName = String.isNotBlank( objVATcs.Reply_To_Name__c ) ? objVATcs.Reply_To_Name__c : '';
                                String contentType = 'text/html';
                                String contentValue = replaceMergeFields( objVATEmailTemplate.HtmlValue, lstBU[0] );
                                String contentBody = replaceMergeFields( objVATEmailTemplate.Body, lstBU[0] );
                                String subject = replaceMergeFields( objVATEmailTemplate.Subject, lstBU[0] );
                                
                                //ccAddress = ccAddress + ',' + objCase.Owner.Email;
                                
                                system.debug('toAddress === ' + toAddress);
                                system.debug('fromAddress === ' + fromAddress);
                                system.debug('contentValue === ' + contentValue);
                                system.debug('ccAddress === ' + ccAddress);

                                //Call method to send an email via SendGrid
                                List<EmailMessage> lstEmails = new List<EmailMessage>(); 
                                if(String.isNotBlank( toAddress )) {
                                            
                                    SendGridEmailService.SendGridResponse objSendGridResponse = 
                                        SendGridEmailService.sendEmailService(
                                                                    toAddress, ''
                                                                    , ccAddress, ''
                                                                    , bccAddress, ''
                                                                    , subject, ''
                                                                    , fromAddress, ''
                                                                    , replyToAddress, ''
                                                                    , contentType
                                                                    , contentValue, ''
                                                                    , new List<Attachment> { objAtt } );
                                    
                                    system.debug('objSendGridResponse === ' + objSendGridResponse);
                                    
                                    String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                                    System.debug('responseStatus== ' + responseStatus);

                                    //Create reportable activity 
                                    if (responseStatus == 'Accepted') {
                                        
                                        EmailMessage mail = new EmailMessage();
                                        mail.Subject = subject;
                                        mail.MessageDate = System.Today();
                                        mail.Status = '3';//'Sent';
                                        mail.RelatedToId = lstBU[0].Booking__r.Account__c;
                                        mail.Account__c  = lstBU[0].Booking__r.Account__c;
                                        mail.Type__c = 'VAT Letter';
                                        mail.ToAddress = toAddress;
                                        mail.FromAddress = fromAddress;
                                        mail.TextBody = contentBody;//contentValue.replaceAll('\\<.*?\\>', '');
                                        mail.Sent_By_Sendgrid__c = true;
                                        mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                                        mail.Booking_Unit__c = lstBU[0].Id;
                                        mail.CcAddress = ccAddress;
                                        mail.BccAddress = bccAddress;
                                        lstEmails.add(mail);
                                        system.debug('Mail obj == ' + mail);
                                    }
                                }
                                System.debug('lstEmails=== ' + lstEmails);
                                if(lstEmails.size() > 0) {
                                    if(!Test.isRunningTest()) {
                                        insert lstEmails;
                                    }
                                }
                            } // END listVATEmailTemplate if
                        } // END Email_Template__c if
                    } // End listVATCs if
                }// End booking unit if
            }// End attachement if
        }// End attachement id if
    }

/*********************************************************************************
 * Method Name : replaceMergeFields
 * Description : Replace merge fields from email contents
 * Return Type : String
 * Parameter(s): Contents, Case
**********************************************************************************/
    public static string replaceMergeFields( String strContents, Booking_Unit__c objBU) {
        if( String.isNotBlank( strContents ) ) {
            if( strContents.contains( '{Account Name}' ) ) {
                if( String.isNotBlank( objBU.Booking__r.Account__r.Name )) {
                    strContents = strContents.replace( '{Account Name}', objBU.Booking__r.Account__r.Name );
                }
                else {
                    strContents = strContents.replace( '{Account Name}', '' );
                }
            }

            if( strContents.contains( '{Active Units}' ) ) {
                if( String.isNotBlank( objBU.Account_Id__c )) {
                    String strActiveUnits = getActiveUnits( objBU.Account_Id__c );
                    if( String.isNotBlank( strActiveUnits ) ) {
                        strContents = strContents.replace( '{Active Units}', strActiveUnits);
                    }
                    else {
                        strContents = strContents.replace( '{Active Units}', '' );    
                    }
                }
                else {
                    strContents = strContents.replace( '{Active Units}', '' );
                }
            }

            if( strContents.contains( '{Property name' ) ) {
                if(objBU.Inventory__c != null && String.isNotBlank( objBU.Inventory__r.Property_Name__c )) {
                    strContents = strContents.replace( '{Property name}', objBU.Inventory__r.Property_Name__c );
                }
                else {
                    strContents = strContents.replace( '{Property name', '' );
                }
            }
        }
        return strContents;
    }

/*********************************************************************************
 * Method Name : getActiveUnits
 * Description : Gets all active units of customer
 * Return Type : String
 * Parameter(s): Account Id
**********************************************************************************/
    public static String getActiveUnits( String accountId ) {
        String activeUnits = '';
        for( Booking_Unit__c objUnit : [ SELECT
                                            Unit_Name__c
                                        FROM
                                            Booking_Unit__c
                                        WHERE
                                            Account_Id__c =: accountId
                                        AND
                                            Unit_Active__c = 'Active']) {
            activeUnits += objUnit.Unit_Name__c + ' - ';
        }

        activeUnits = activeUnits.removeEnd(' - ');
        System.debug( ' activeUnits == '  + activeUnits);
        return activeUnits;
    }

    /************************************************************************
    * @Description : Method to clone the inserted attachment related to booking
    *                unit on the related case of booking unit.
	* @Params      : List<Attachment>
	* @Return      : None
	*************************************************************************/
    public static void attachAttachmenttoCase(Map<Id,Attachment> attachObjMap){
        Set<Id> buIdSet = new Set<Id>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        List<Attachment> attachmentList = new List<Attachment>();
        System.debug( 'attachObjMap-' + attachObjMap);
        if(!attachObjMap.keySet().isEmpty()){
            bookingUnitList = [SELECT Id
                                    , HO_EHO_PHO_CaseId__c
                                    , ( SELECT Id
                                             , Booking_Unit__c
                                          FROM Cases__r
                                         WHERE RecordType.Name =: HANDOVER
                                      ORDER BY CreatedDate DESC
                                         LIMIT 1 )
                                FROM Booking_Unit__c
                               WHERE Id IN :attachObjMap.keySet()];
        
            System.debug('bookingUnitList='+bookingUnitList);
            for(Booking_Unit__c buObjItr :bookingUnitList) {
                if(/*!buObjItr.Cases__r.isEmpty() && */attachObjMap.containsKey(buObjItr.Id)){
                    if(!String.isBlank(buObjItr.HO_EHO_PHO_CaseId__c)){
                        Attachment newAttachmentObj = attachObjMap.get(buObjItr.Id).clone();
                        newAttachmentObj.ParentId = buObjItr.HO_EHO_PHO_CaseId__c;
                        attachmentList.add(newAttachmentObj);
                    }else if(!buObjItr.Cases__r.isEmpty()){
                        Attachment newAttachmentObj =
                            attachObjMap.get(buObjItr.Id).clone();
                        newAttachmentObj.ParentId = buObjItr.Cases__r[0].Id;
                        attachmentList.add(newAttachmentObj);
                    }else{
                        buIdSet.add(buObjItr.Id);
                    }
                }
            }
            System.debug( 'attachmentList 1==' + attachmentList);
            if(!buIdSet.isEmpty()){
                bookingUnitList = new List<Booking_Unit__c>();//bookingUnitList.clear();
                bookingUnitList = [SELECT Id
                                        , ( SELECT Id
                                                , Booking_Unit__c
                                            FROM Cases__r
                                            WHERE RecordType.Name =: EARLY_HANDOVER
                                            ORDER BY CreatedDate DESC
                                            LIMIT 1 )
                                    FROM Booking_Unit__c
                                    WHERE Id IN :buIdSet
                                ];

                for(Booking_Unit__c buObj :bookingUnitList){
                    if(!buObj.Cases__r.isEmpty() && attachObjMap.containsKey(buObj.Id)){
                        Attachment newAttachmentObj =
                            attachObjMap.get(buObj.Id).clone();
                        newAttachmentObj.ParentId = buObj.Cases__r[0].Id;
                        attachmentList.add(newAttachmentObj);
                    }
                }
            }
            System.debug( 'attachmentList 2==' + attachmentList);
            if(!attachmentList.isEmpty()){
                insert attachmentList;
            }
        }
    }

}