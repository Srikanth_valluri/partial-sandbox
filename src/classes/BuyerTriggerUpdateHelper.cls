/*                                                        *
* Description        : This is a trigger helper to update Account of related Booking.                                       *
* Created Date       : 30/11/2017                                                                 *
* Created By         : Swapnil Gholap                                                                      *
*/

public without sharing class BuyerTriggerUpdateHelper {
    
    public void helperMethod(Map<ID,Buyer__c> OldMapBuyer, List<Buyer__c> lstNewBuyer){
       System.debug('--helperMethod BuyerTriggerUpdateHelper ---'+OldMapBuyer);        
       List<ID> lstBookingIDs = new List<ID>(); 
       Map<ID,ID> MapAccountBooking = new Map<ID,ID>(); 
            
           for(Buyer__c objBuyer : lstNewBuyer){               
               if(OldMapBuyer != null && OldMapBuyer.ContainsKey(objBuyer.id)){                   
                   if(OldMapBuyer.get(objBuyer.id).Account__c == null 
                     && objBuyer.Account__c != null
                     && objBuyer.Booking__c != null
                     && objBuyer.Primary_Buyer__c){
                       
                       lstBookingIDs.add(objBuyer.Booking__c);
                       MapAccountBooking.put(objBuyer.Booking__c, objBuyer.Account__c);
                       
                   } 
               }
           } 
           if(lstBookingIDs.Size()>0){
               List<Booking__c> lstBooking = new List<Booking__c>(); 
               lstBooking = [Select id,name,Account__c from Booking__c where id IN: lstBookingIDs];
               for(Booking__c objBooking : lstBooking){
                   if(MapAccountBooking.ContainsKey(objBooking.id)){
                       objBooking.Account__c = MapAccountBooking.get(objBooking.id);
                   }
               }               
               update lstBooking;
           }      
    }
    
}