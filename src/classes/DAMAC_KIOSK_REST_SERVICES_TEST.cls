/*
Developed By: DAMAC IT Team
Test Class for :DAMAC_KIOSK_REST_SERVICES
*/
@istest
public class DAMAC_KIOSK_REST_SERVICES_TEST{

    static testmethod void test_GETMethods(){
        //test data
        
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);
        
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        //sr.Is_From_New_Wizard__c = true;
       
        sr.Booking_Wizard_Level__c = 'Level 5';
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE';
        sr.Mode_of_Payment__c = 'Kiosk';
        insert sr;
        system.debug('TESTSR'+sr.Name);
        
        NSIBPM__Step_Template__c stpTemplate =  InitializeSRDataTest.createStepTemplate('Deal','MANAGER_APPROVAL');
        insert stptemplate;
        
        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
        Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);
        
        
        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id,stepStatuses.values()[0].id,stptemplate.id);
        insert stp;
        
        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        insert lstbk;
        
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        insert loc;       
        
        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        bu.Registration_ID__c = '123';
        insert bu;
        
        /*
        buyer__c b = new buyer__c();
        b.Buyer_Type__c =  'Individual';
        b.Address_Line_1__c =  'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.City__c = 'Dubai' ;
        
        b.Date_of_Birth__c = string.valueof(system.today().addyears(-30)) ;
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname' ;
        b.Last_Name__c =  'lastname';
        b.Nationality__c = 'Indian' ;
        b.Passport_Expiry_Date__c = string.valueof(system.today().addyears(20)) ;
        b.Passport_Number__c = 'J0565556' ;
        b.Phone__c = '569098767' ;
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b.Place_of_Issue__c =  'India';
        b.Title__c = 'Mr';
        b.booking__c = lstbk[0].id;
        b.Primary_Buyer__c =true;
        insert b;
        */
        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Booking_Unit__c = bu.id;
        pp.Effective_From__c = system.today().adddays(-4);
        pp.Effective_To__c = system.today().adddays(7); 
        pp.Building_Location__c =  loc.id;      
        insert pp;
        
        Payment_Terms__c pt = new Payment_Terms__c();
        pt.Payment_Plan__c = pp.id;
        pt.Booking_Unit__c = bu.id;
        pt.Percent_Value__c = '5';
        insert pt;
    
    
        // PTerms
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        system.debug('>>**'+SR.NAME);
        // pass the req and resp objects to the method     
        req.requestURI = 'https://damacholding--fullcopy.cs80.my.salesforce.com/services/apexrest/payservices';  
        req.httpMethod = 'GET';
        req.addparameter('bref',SR.NAME);
        RestContext.request = req;
        DAMAC_KIOSK_REST_SERVICES.bookingDetails(SR.NAME);
        
        try{
            DAMAC_KIOSK_REST_SERVICES.doGet();
        }
        catch(exception e){
        }    
    }
     
    
    static testmethod void test_ObjCls(){   
        
        string regstr = '{"X_RETURN_STATUS":"S","X_ERROR_MSG":null,"UNIT_NAME":"VET/SD114/N259B","STATUS":"SA - Agreement Generated","SIZE_OF_APARTMENT":"2696.00","SFSRSTATUS":"Docs Verified","SFSRNUMBER":"SR-66335","SELLER_NAME":null,"REGISTRATION_ID":"92707","REGISTRATION_DATE":"2017-10-26 13:15:49","PROJECT_NAME":null,"PRIMARY_PHONE_NUM":"56664308456","PRESENT_CM_PRICE":"","POSTAL_CODE":"","PHONE_NUMBER":"56664308456","PERMITTED_USE":"Residential","paymentTerms":null,"PASSPORT_NO":"h789021","PARKING_ADDENDUM":"","NATIONALITY":"Indian","FINANCE":"N","FAX_NUMBER":"","EMAIL_ADDRESS":"varun@constancecapital.com","EFF_DATE_OF_CURR_CM_PRICE":"","DOCUMENTS_OK":"true","DISPUTE":null,"DEPOSIT_RECEIVED":"false","CUSTOMER_NAME":"Test Velma","COUNTRY":"India","CM_PRICE":null,"CITY":"Hyderabad","CDCSTATUS":null,"APARTMENT_PRICE":"0.00","ADDRESS4":null,"ADDRESS3":null,"ADDRESS2":null,"ADDRESS1":"AL1"}';
        DAMAC_REST_RESP_UNIT.parse(regstr);
    }


}