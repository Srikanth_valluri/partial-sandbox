/**************************************************************************************************
 * @Name              : Damac_UKRegistration
 * @Test Class Name   : DAMAC_IPMS_PARTY_CREATION_TEST
 * @Description       : Websevice Call to Register Booking Units
 * Modification Log
* VERSION     AUTHOR          DATE            Update Log
* 1.0                        14/08/2019       Created
* 1.1         QBurst         03/02/2020       Modifications for DP Deals [SOAP to REST]
**************************************************************************************************/
Public class Damac_UKRegistration {
    public static void processRequest (ID srRecordId, String partyId) {
        system.debug('Inside Process Request');
        NSIBPM__Service_Request__c sr = [SELECT Name, Agency__r.Vendor_ID__c, Agency__c, Deal_Type__c,
                                                UK_Booking_Agent__c, UK_Booking_Agent__r.Vendor_Id__c,
                                                Solicitor__c, Solicitor__r.Vendor_Id__c, is_uk_deal__c
                                        FROM NSIBPM__Service_Request__c WHERE Id =: srRecordId ];
        List <Booking_Unit__c> bus = new List <Booking_unit__c> ();
        List <Deal_Team__c> dealTeams = new List <Deal_Team__c> ();
        Map <ID, List <Option__c>> options = new Map <ID, List <Option__c>> ();
        Map <ID, Payment_plan__c> paymentPlans = new Map <ID, Payment_plan__c> ();
        SET<ID> bookingIds = new SET <ID>();
        SET<ID> bookingUnitIds = new SET <ID>(); 
        Set <ID> buildingLocations = new Set <ID> ();
        set<Id> paymentPlanIds = new set<Id>();
        List<Buyer__c> primaryBuyer = new List<Buyer__c>();
        primaryBuyer = [SELECT Party_Id__c, Buyer_Solicitor_Name__c, Buyer_Solicitor_Firm__c,
                             Buyer_Solicitor_Address_Line_1__c, Buyer_Solicitor_Address_Line_2__c,
                             Buyer_Solicitor_DX_Number__c, Buyer_Solicitor_Email__c, Inquiry__r.Promoter_Name__c,
                             Buyer_Solicitor_Mobile_Number__c, Telesales_Executive__c, Telesales_Manager__c
                        FROM Buyer__c WHERE Primary_BUyer__c = TRUE AND Booking__r.Deal_Sr__c =: sr.Id ];
        if (partyId == '') {
            partyId = primaryBuyer[0].Party_Id__c;
        }
        bus = Database.query('SELECT Inventory__r.Building_Location__c, Inventory__r.Unit_Name__c, Inventory__r.Property__r.Alacarte_Enabled__c, '
                           + 'Booking__r.Deal_SR__r.Delivery_mode__c, '
                           + getAllFields('Booking_unit__c')
                           + ' FROM Booking_Unit__c WHERE Booking__r.Deal_Sr__c =: srRecordId');
        
        dealTeams = [SELECT Associated_DOS__c, Associated_DOS__r.IPMS_Employee_ID__c, 
                            Associated_HOD__c, Associated_HOD__r.IPMS_Employee_ID__c,
                            Associated_HOS__c, Associated_HOS__r.IPMS_Employee_ID__c,
                            Associated_PC__c, Associated_PC__r.IPMS_Employee_ID__c, PC_Team__c, User_Sales_Office__c
                        FROM Deal_Team__c WHERE Associated_Deal__c =: srRecordId  AND PC_Team__c = 'PC Team'];
        String userSalesOffice = '';
        for (Deal_Team__c team : dealTeams){
            if (team.PC_Team__c == 'PC Team') {
                userSalesOffice = team.User_Sales_Office__c;
            }
        }

        for (Booking_Unit__c bu :bus) {
            bookingunitIds.add (bu.Id);
            bookingIds.add (bu.Booking__c);
            if (bu.Inventory__r.Building_Location__c != NULL) {
                buildingLocations.add (bu.Inventory__r.Building_Location__c);
            }
        }

        for(Payment_Plan__c plan : [SELECT TERM_ID__c, Booking_Unit__c, Parent_Payment_Plan__r.Term_ID__c  
                                    FROM Payment_plan__c 
                                    WHERE Booking_Unit__c in: bus]){
            paymentPlans.put (plan.Booking_Unit__c, plan);
        }

        if (buildingLocations.size () > 0) {
            for (Payment_Plan__c plan : [SELECT TERM_ID__c, Building_Location__c 
                                        FROM Payment_plan__c 
                                        WHERE Building_Location__c IN : buildingLocations 
                                        AND Selected_Units__c = NULL
                                        AND Effective_From__c <= TODAY
                                        AND Effective_To_calculated__c >= TODAY
                                        AND booking_unit__c=null
                                        AND parent_payment_plan__c = null]){
                paymentPlans.put(plan.Building_Location__c, plan);
            }
        }
        List <Buyer__c> jointBuyersList = new List<Buyer__c> ();
        jointBuyersList = [SELECT Party_Id__c, Buyer_Solicitor_Name__c, Buyer_Solicitor_Firm__c, Buyer_Solicitor_Address_Line_1__c, 
                                Buyer_Solicitor_Address_Line_2__c,
                                Buyer_Solicitor_DX_Number__c, Buyer_Solicitor_Email__c, Buyer_Solicitor_Mobile_Number__c 
                            FROM Buyer__c WHERE Booking__r.Deal_Sr__c =: sr.Id AND Primary_Buyer__c = FALSE];
        
        for (option__c option : Database.Query('SELECT ' + getAllFields('Option__c') + ' FROM Option__c WHERE Booking_Unit__c IN :bookingunitIds')) {
            if (options.containsKey (option.Booking_Unit__c)) {
                options.get (option.Booking_Unit__c).add(option);
            } else {
                options.put (option.Booking_Unit__c, new List <Option__c> {option});
            }
        }
        DAMAC_IPMS_BOOKING_JSON obj = new DAMAC_IPMS_BOOKING_JSON();
        obj.extRequestNumber = sr.Name;
        List <Damac_IPMS_Booking_JSON.cls_registrationLines> regLines = new List <Damac_IPMS_Booking_JSON.cls_registrationLines> ();
        boolean hasAlacarteUnits = false;
        for (Booking_Unit__c bu : bus) {
            /****************1.1 starts*****************************/
            //Getting the Communication mode
            string communicationMode = '';
            if(bu.Booking__r.Deal_SR__r.Delivery_mode__c == 'Email') {
                communicationMode = 'E';
            }
            if(bu.Booking__r.Deal_SR__r.Delivery_mode__c == 'Dispatch') {
                communicationMode = 'D';
            }
            if(bu.Booking__r.Deal_SR__r.Delivery_mode__c =='Print at Sales Office') {
                communicationMode = 'P'; 
            } 
            String spaType = bu.SPA_Type_Unit__c;
            if (spaType == 'Standard SPA') {
                spaType = '';
            }
            String freeParking = '0';
            if(bu.No_of_parking__c != null && bu.No_of_parking__c > 0){
                freeParking = String.valueOf(bu.No_of_parking__c);
            }
            /****************1.1 ends*****************************/
            Damac_IPMS_Booking_JSON.cls_registrationLines regLine = new Damac_IPMS_Booking_JSON.cls_registrationLines ();
            regLine.bookingReference = bu.Name;
            regLine.subRequestName = 'NEW_BOOKING';
            regLine.bookingLocation = 'UK';
            if(!sr.is_uk_deal__c){ // 1.1
                regLine.bookingLocation = 'DP';
            }
            regLine.bookingType = 'NW'; // Deal Type on SR Recovery - RN , Others - NW
            if(sr.Deal_Type__c == 'Recovery'){
                regLine.bookingType = 'RN';
            }
            if(sr.Deal_Type__c == 'Swap'){
                regLine.bookingType = 'CN';
            }
            regLine.registrationDate = getDatetext(Date.Today());
            regLine.partyId = partyId;
            regLine.unitCode = bu.Inventory__r.Unit_Name__c;
            if(paymentPlans.containsKey (bu.iD)){
                regLine.paymentHeaderId = paymentPlans.get(bu.Id).Parent_Payment_Plan__r.Term_ID__c;
            }
            else if (paymentPlans.containsKey (bu.Inventory__r.Building_Location__c)) {
                regLine.paymentHeaderId = paymentPlans.get (bu.Inventory__r.Building_Location__c).Term_Id__c;
            }
            
            if(bu.Inventory__r.Property__r.Alacarte_Enabled__c)
                hasAlacarteUnits = true;
            regLine.heardOfDamac = bu.Related_Campaign_Name__c; // 1.1
            regLine.bookingSource = 'SFDC';
            regLine.salesOffice = userSalesOffice;
            regLine.communicationMode = communicationMode; // 1.1
            regLine.discountPercent = '';
            regLine.conditionalDiscountPercent = '';
            regLine.freeParking = freeParking; // 1.1
            regLine.paidParking = '0';
            regLine.externalParking = '0';
            regLine.storeRoom = 0;
            regLine.regularOrBulk = 'R';
            regLine.specialPriceSale = 'N';
            regLine.bookingRemarks = '';
            regLine.spaceType = bu.Permitted_Usage__c != null ? bu.Permitted_Usage__c.toUpperCase():''; 
            regLine.spaTemplate = spaType; // 1.1
            List <Damac_IPMS_Booking_JSON.cls_salesTeam> salesTeams = new List <Damac_IPMS_Booking_JSON.cls_salesTeam>();
            for (Deal_Team__c deal :dealTeams) {
                if (deal.Associated_PC__c != NULL) {
                    Damac_IPMS_Booking_JSON.cls_salesTeam saleTeam = new Damac_IPMS_Booking_JSON.cls_salesTeam();
                    saleTeam.roleCode = 'PC';
                    saleTeam.roleOwnerId = deal.Associated_PC__r.IPMS_Employee_ID__c;
                    salesTeams.add(saleTeam);
                }
                if (deal.Associated_DOS__c != NULL) {
                    Damac_IPMS_Booking_JSON.cls_salesTeam saleTeam = new Damac_IPMS_Booking_JSON.cls_salesTeam();
                    saleTeam.roleCode = 'DOS';
                    saleTeam.roleOwnerId = deal.Associated_DOS__r.IPMS_Employee_ID__c;
                    salesTeams.add(saleTeam);
                }
                if (deal.Associated_HOS__c != NULL) {
                    Damac_IPMS_Booking_JSON.cls_salesTeam saleTeam = new Damac_IPMS_Booking_JSON.cls_salesTeam();
                    saleTeam.roleCode = 'HOS';
                    saleTeam.roleOwnerId = deal.Associated_HOS__r.IPMS_Employee_ID__c;
                    salesTeams.add(saleTeam);
                }
                if (deal.Associated_HOD__c != NULL) {
                    Damac_IPMS_Booking_JSON.cls_salesTeam saleTeam = new Damac_IPMS_Booking_JSON.cls_salesTeam();
                    saleTeam.roleCode = 'HOD';
                    saleTeam.roleOwnerId = deal.Associated_HOD__r.IPMS_Employee_ID__c;
                    salesTeams.add(saleTeam);
                }
                if (sr.Agency__c != NULL) {
                    if (sr.Agency__r.Vendor_ID__c != NULL) {
                        Damac_IPMS_Booking_JSON.cls_salesTeam saleTeam = new Damac_IPMS_Booking_JSON.cls_salesTeam();
                        saleTeam.roleCode = 'VEN';
                        saleTeam.roleOwnerId = sr.Agency__r.Vendor_ID__c;
                        salesTeams.add(saleTeam);
                    }
                }
                if (sr.UK_Booking_Agent__c != NULL) {
                    if (sr.UK_Booking_Agent__r.Vendor_ID__c != NULL) {
                        Damac_IPMS_Booking_JSON.cls_salesTeam saleTeam = new Damac_IPMS_Booking_JSON.cls_salesTeam();
                        saleTeam.roleCode = 'BA';
                        saleTeam.roleOwnerId = String.valueOf(sr.UK_Booking_Agent__r.Vendor_ID__c);
                        salesTeams.add(saleTeam);
                    }
                }
            }
            List <Damac_IPMS_Booking_JSON.cls_jointBuyers> jointBuyers = new List <Damac_IPMS_Booking_JSON.cls_jointBuyers>();
            for (Buyer__c b : jointBuyersList ) {
                Damac_IPMS_Booking_JSON.cls_jointBuyers buyer = new Damac_IPMS_Booking_JSON.cls_jointBuyers();
                buyer.jbPartyId = b.Party_id__c;
                buyer.sharePercentage = '';
                jointBuyers.add(buyer);
            }
            List <Damac_IPMS_Booking_JSON.cls_solicitors> solicitors = new List <Damac_IPMS_Booking_JSON.cls_solicitors>();
            List <Damac_IPMS_Booking_JSON.cls_offers> offers = new List <Damac_IPMS_Booking_JSON.cls_offers> ();
            if (options.containsKey (bu.Id)) {
                for (Option__c o: options.get (bu.Id)) {
                    if (o.PromotionName__c != NULL) {
                        Damac_IPMS_Booking_JSON.cls_offers offer = new Damac_IPMS_Booking_JSON.cls_offers();
                        offer.offerCode = 'P';    
                        offer.offerId = o.PromotionName__c;
                        offers.add(offer);
                    }
                    if (o.CampaignName__c != NULL) {
                        Damac_IPMS_Booking_JSON.cls_offers offer = new Damac_IPMS_Booking_JSON.cls_offers();
                        offer.offerCode = 'C';
                        offer.offerId = o.CampaignName__c;
                        offers.add(offer);
                    }
                    if (o.OptionsName__c != NULL) {
                        Damac_IPMS_Booking_JSON.cls_offers offer = new Damac_IPMS_Booking_JSON.cls_offers();
                        offer.offerCode = 'O';
                        offer.offerId = o.OptionsName__c;
                        offers.add(offer);
                    }
                    if (o.SchemeName__c != NULL) {
                        Damac_IPMS_Booking_JSON.cls_offers offer = new Damac_IPMS_Booking_JSON.cls_offers();
                        offer.offerCode = 'S';
                        offer.offerId = o.SchemeName__c ;
                        offers.add(offer);
                    }
                }
            }
            for (Buyer__c buyer : primaryBuyer ) {
                if (buyer.Telesales_Executive__c != NULL && buyer.Telesales_Executive__c != '') {
                    Damac_IPMS_Booking_JSON.cls_salesTeam saleTeam = new Damac_IPMS_Booking_JSON.cls_salesTeam();
                    saleTeam.roleCode = 'TSE';
                    saleTeam.roleOwnerId = String.valueOf(buyer.Telesales_Executive__c);
                    salesTeams.add(saleTeam);
                }
                if (buyer.Telesales_Manager__c != NULL && buyer.Telesales_Manager__c != '')  {
                    Damac_IPMS_Booking_JSON.cls_salesTeam saleTeam = new Damac_IPMS_Booking_JSON.cls_salesTeam();
                    saleTeam.roleCode = 'TSM';
                    saleTeam.roleOwnerId = String.valueOf(buyer.Telesales_Manager__c);
                    salesTeams.add(saleTeam);
                }
                if (buyer.Inquiry__r.Promoter_Name__c != NULL && buyer.Inquiry__r.Promoter_Name__c != '')  {
                    Damac_IPMS_Booking_JSON.cls_salesTeam saleTeam = new Damac_IPMS_Booking_JSON.cls_salesTeam();
                    saleTeam.roleCode = 'PMT';
                    saleTeam.roleOwnerName = buyer.Inquiry__r.Promoter_Name__c != null ? String.valueOf(buyer.Inquiry__r.Promoter_Name__c) : '';
                    salesTeams.add(saleTeam);
                }
                if(sr.is_uk_deal__c){ 
                    Damac_IPMS_Booking_JSON.cls_solicitors solicitor = new Damac_IPMS_Booking_JSON.cls_solicitors ();
                    if (sr.Solicitor__c != NULL) {
                        solicitor.sellerSolicitorId = String.valueOf(sr.Solicitor__r.Vendor_id__c);
                    }
                    solicitor.buyerSolicitorName = buyer.Buyer_Solicitor_Name__c;
                    solicitor.buyerSolicitorFirm = buyer.Buyer_Solicitor_Firm__c;
                    solicitor.buyerSolicitorAddress = buyer.Buyer_Solicitor_Address_Line_1__c + ' ' + buyer.Buyer_Solicitor_Address_Line_2__c;
                    solicitor.buyerSolicitorDxNo = buyer.Buyer_Solicitor_DX_Number__c;
                    solicitor.buyerSolicitorEmail = buyer.Buyer_Solicitor_Email__c;
                    solicitor.buyerSolicitorPhone = buyer.Buyer_Solicitor_Mobile_Number__c;
                    solicitors.add(solicitor);
                }
            }
            regLine.salesTeam = salesTeams;
            regLine.jointBuyers = jointBuyers;
            regLine.offers = offers;
            if(sr.is_uk_deal__c){ 
                regLine.solicitors = solicitors;
            }
            regLines.add(regLine);
        }
        obj.registrationLines = regLines;
        String jsonBody = JSON.serialize (obj);
        System.Debug('JSON Body: ' + jsonBody);
        callService (jsonBody, srRecordId);
        if(hasAlacarteUnits)
            System.enqueueJob(new ALACARTE_Queueable(bookingIds));
    }

    public static string getDatetext(Date d){
        String dd, mm, mon, yy, dtext = '';
        dd = d.day() > 9 ? string.valueof(d.day()) : '0'+string.valueof(d.day());
        
        mm = string.valueof(d.month());
        if(mm == '1') {
            mon = 'JAN';
        } else if(mm == '2') {
            mon = 'FEB';
        } else if(mm == '3') {
            mon = 'MAR';
        } else if(mm == '4') {
            mon = 'APR';
        } else if(mm == '5') {
            mon = 'MAY';
        } else if(mm == '6') {
            mon = 'JUN';
        } else if(mm == '7') {
            mon = 'JUL';
        } else if(mm == '8') {
            mon = 'AUG';
        } else if(mm == '9') {
            mon = 'SEP';
        } else if(mm == '10') {
            mon = 'OCT';
        } else if(mm == '11') {
            mon = 'NOV';
        } else if(mm == '12') {
            mon = 'DEC';
        }
        yy = string.valueof(d.year());
        dtext = dd + '-' + mon + '-' + yy;
        return dtext;
    }

    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            fields += sfield.getDescribe().getName () + ', ';
        }
        return fields.removeEnd(', '); 
    }

    public static void callService (String reqBody, String srRecordId) {
        IPMS_Integration_Settings__mdt settings = [SELECT Client_ID__c, Endpoint_URL__c 
                                                   FROM IPMS_Integration_Settings__mdt WHERE DeveloperName = 'UK_Party' LIMIT 1];
        HttpRequest req = new HttpRequest ();
        string token = DAMAC_IPMS_PARTY_CREATION.getIPMSAccessToken();
        req.setEndpoint(settings.Endpoint_URL__c + '/booking');
        req.setMethod('POST');
        req.setHeader('Authorization', 'Bearer ' + token);
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        req.setBody (reqBody);
        Http http = new HTTP ();
        HTTPResponse res = new HTTPResponse ();
        if (!Test.isRunningTest()) {
            res = http.send (req);
        } else {
            res.setBody ('{"status":"S","responseMessage":"Process Completed",'
                        + '"responseLines":[{"registrationId":"00099988777","bookingLocation":"DP","status":"S"}],"complete":true}');
        }
        System.Debug ('Response body: ' + res.getBody());
        if (res.getBody () != NULL) {
            parseResponse (res.getBody(), srRecordId);
            if(Label.IPMS_Party_Reg_Log_Creation == 'Yes' ){ // 1.1
                createLog(srRecordId, req.getBody(), 'IPMS Registration Request');
                createLog(srRecordId, res.getBody(), 'IPMS Registration Response');
            }
        }
        else {
            createLog(srRecordId, res.getBody(), 'UK Booking');
        }
        
    }

    public static void parseResponse (String responseBody, String srRecordId) {
        DAMAC_UKRegistrationResponse obj = DAMAC_UKRegistrationResponse.parse(responseBody);
        Map <String, String> regIds = new Map <String, String> ();
        Boolean errorFlag = false;
        if (obj.responseLines != NULL) {
            for (DAMAC_UKRegistrationResponse.cls_responseLines res : obj.responseLines) {
                regIds.put (res.bookingReference, res.registrationId);
                System.Debug(res.Status);
                if (res.Status == 'E' && errorFlag == false) {
                    errorFlag = true;
                }
            }
            System.Debug(errorFlag + '===');
            if (errorFlag) {
                createLog(srRecordId, responseBody, 'UK Booking');
            } else {
                List <Booking_Unit__c> unitsToUpdate = new List <Booking_Unit__c> ();
                for (Booking_unit__c unit : [SELECT Name, Registration_ID__c FROM Booking_unit__c WHERE Name IN : regIds.keySet()]) {
                    unit.Registration_ID__c = regIds.get(unit.Name);
                    unit.Registration_DateTime__c = system.now();
                    unitsToUpdate.add (unit);
                }
                Map<String,Integer> paymentMethodDurationMap = new Map<String,Integer>();
                for(Payment_Method_Mapping__mdt thisMapping : [SELECT Payment_Method_Type__c, Time_Out_Duration__c FROM Payment_Method_Mapping__mdt]) {
                    paymentMethodDurationMap.put(thisMapping.Payment_Method_Type__c, Integer.valueOf(thisMapping.Time_Out_Duration__c));
                }
                system.debug('srRecordId: ' + srRecordId);
                // Iterate over the booking units
                integer duration = 0;
                for(Booking_Unit__c bookingUnit : [SELECT Id, Registration_ID__c, Payment_Method__c, Booking__c, Booking__r.Deal_SR__c, Token_Paid__c,
                                                        SR_Id__c, Status__c 
                                                   FROM Booking_Unit__c WHERE Status__c != 'Removed' AND Booking__r.Deal_SR__c =: srRecordId]){
                    system.debug('#### payment method = ' + bookingUnit.Payment_Method__c);
                    if(String.isNotBlank(bookingUnit.Payment_Method__c)){
                        if(paymentMethodDurationMap.containsKey(bookingUnit.Payment_Method__c)){
                            duration = duration < paymentMethodDurationMap.get(bookingUnit.Payment_Method__c) ? 
                                                paymentMethodDurationMap.get(bookingUnit.Payment_Method__c) : duration;
                        } else{
                            duration = Integer.valueOf(system.label.Inventory_Block_Duration);   
                        }
                    }
                }
                Id srStatus = [SELECT Id, NSIBPM__Code__c FROM NSIBPM__SR_Status__c WHERE NSIBPM__Code__c = 'SUBMITTED'].Id; //'a0H0Y000002im4W'
                NSIBPM__Service_Request__c srRecord = [SELECT Id, Token_Deposit_Due_Date_Time__c, EOI_Process__r.Approval_Status__c, is_uk_deal__c,
                                                                NSIBPM__Submitted_Date__c, NSIBPM__Submitted_DateTime__c, Registration_Date__c,
                                                                Registration_DateTime__c, NSIBPM__Internal_SR_Status__c, NSIBPM__External_SR_Status__c 
                                                           FROM NSIBPM__Service_Request__c WHERE Id =: srRecordId LIMIT 1];

                srRecord.Id = srRecordId;
                srRecord.NSIBPM__Internal_SR_Status__c = srStatus;
                srRecord.NSIBPM__External_SR_Status__c = srStatus;
                srRecord.NSIBPM__Submitted_DateTime__c = System.Now();
                srRecord.NSIBPM__Submitted_Date__c = Date.Today();
                srRecord.Registration_Date__c = Date.Today();
                srRecord.Registration_DateTime__c = System.Now();
                if(srRecord.Token_Deposit_Due_Date_Time__c == null) {
                    if (srRecord.EOI_Process__r.Approval_Status__c != 'Approved') {
                        srRecord.Token_Deposit_Due_Date_Time__c = system.now().addHours(duration); 
                    }   
                }
                if(srRecord.is_uk_deal__c){
                    srRecord.currencyISOCode = 'GBP';
                }
                ServiceRequestAgentSharing.BYPASS_UPDATE_SR = true;
                update srRecord;
                if (unitsToUpdate.size () > 0) {
                    Update unitsToUpdate;
                }
            }
        } else {
            createLog(srRecordId, responseBody, 'UK Booking');
        }
    }

    @future(callout=true)
    public static void processJointBuyerFuture(String reqName, String partyId, String srId, String buyerName, String srName, String regId){
        String response = processJointBuyer(reqName, partyId, srId, buyerName, srName, regId);
        system.debug('Response: ' + response);
    }    

    /************************ 1.1 Starts  *************************/
    /************************************************************************************** 
     * @Description          : Calls IPMS webservice to Add/Delete Joint Buyer in the Back End
     * @Parameters           : 
     * @Return               : None
    ***************************************************************************************/
    public static String processJointBuyer(String reqName, String partyId, String srId, String buyerName, String srName, String regId){
        System.debug('Add Joint Buyer');
        String responseStatus = '';
        DAMAC_IPMS_BOOKING_JSON obj = new DAMAC_IPMS_BOOKING_JSON();
        obj.extRequestNumber = srName;
        List<Damac_IPMS_Booking_JSON.cls_registrationLines> regLines = new List<Damac_IPMS_Booking_JSON.cls_registrationLines> ();
        Damac_IPMS_Booking_JSON.cls_registrationLines regLine = new Damac_IPMS_Booking_JSON.cls_registrationLines ();
        regLine.subRequestName = reqName;
        regLine.registrationId = regId;
        List <Damac_IPMS_Booking_JSON.cls_jointBuyers> jointBuyers = new List <Damac_IPMS_Booking_JSON.cls_jointBuyers>();
        Damac_IPMS_Booking_JSON.cls_jointBuyers buyerJSON = new Damac_IPMS_Booking_JSON.cls_jointBuyers();
        buyerJSON.jbPartyId = partyId;
        buyerJSON.sharePercentage = '';
        jointBuyers.add(buyerJSON);
        regLine.jointBuyers = jointBuyers;
        regLine.bookingReference = srName;
        regLines.add(regLine);
        obj.registrationLines = regLines;
        String jsonBody = JSON.serialize (obj);
        System.Debug('JSON Body: ' + jsonBody);
        IPMS_Integration_Settings__mdt settings = [SELECT Client_ID__c, Endpoint_URL__c 
                                                        FROM IPMS_Integration_Settings__mdt 
                                                        WHERE DeveloperName = 'UK_Party' LIMIT 1];
        HttpRequest req = new HttpRequest ();
        string token = DAMAC_IPMS_PARTY_CREATION.getIPMSAccessToken();
        req.setEndpoint(settings.Endpoint_URL__c + '/booking');
        req.setMethod('POST');
        req.setHeader('Authorization', 'Bearer ' + token);
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        req.setBody (jsonBody);
        Http http = new HTTP ();
        HTTPResponse res = new HTTPResponse ();
        if (!Test.isRunningTest()) {
            res = http.send (req);
        } else {
            res.setBody ('{"status": "S","responseLines": [{"bookingReference": "BU-223596", '
                                + '"partyId": "34602", "status": "E" }], "complete": true}');
        }
        IPMS_PartyId_Creation_Response respCls = new IPMS_PartyId_Creation_Response();
        respCls = IPMS_PartyId_Creation_Response.parse(res.getBody());                    
        if(respCls.status == 'S'){
            responseStatus = 'Success';
        } else{
            responseStatus = 'Error';
        }
        createLog(srId, req.getBody(), 'Joint Buyer - ' + buyerName + ' ' + reqName + ' Request');
        createLog(srId, res.getBody(), 'Joint Buyer - ' + buyerName + ' ' + reqName + ' Response');
        System.Debug ('Response body: ' + res.getBody());
        return responseStatus;
    }
    
    //Create Log for Error
    public static void createLog(string parentId, String errorMessage, String type){
        log__c log = new log__c();
        log.Service_Request__c = parentId;
        log.Description__c = errorMessage;
        log.Type__c = type;
        insert log;
    }
    
    //Create Log for Error
    public static log__c createLogNoInsert(string parentId, String errorMessage, String type){
        log__c log = new log__c();
        log.Service_Request__c = parentId;
        log.Description__c = errorMessage;
        log.Type__c = type;
        return log;
    }
    
    @future(callout=true)
    public static void processJointBuyerFutureNew(String reqName, String partyId, String srId, String buyerName, String srName, String regId){
        List<log__c> log = processJointBuyerNew(reqName, partyId, srId, buyerName, srName, regId);
    }
     
    
    /************************************************************************************** 
     * @Description          : Calls IPMS webservice to Add/Delete Joint Buyer in the Back End
     * @Parameters           : 
     * @Return               : None
    ***************************************************************************************/
    
    public static List<log__c> processJointBuyerNew(String reqName, String partyId, String srId, String buyerName, String srName, String regId){
        System.debug('Add Joint Buyer'); 
        String responseStatus = '';
        DAMAC_IPMS_BOOKING_JSON obj = new DAMAC_IPMS_BOOKING_JSON();
        obj.extRequestNumber = srName;
        List<Damac_IPMS_Booking_JSON.cls_registrationLines> regLines = new List<Damac_IPMS_Booking_JSON.cls_registrationLines> ();
        Damac_IPMS_Booking_JSON.cls_registrationLines regLine = new Damac_IPMS_Booking_JSON.cls_registrationLines ();
        regLine.subRequestName = reqName;
        regLine.registrationId = regId;
        List <Damac_IPMS_Booking_JSON.cls_jointBuyers> jointBuyers = new List <Damac_IPMS_Booking_JSON.cls_jointBuyers>();
        Damac_IPMS_Booking_JSON.cls_jointBuyers buyerJSON = new Damac_IPMS_Booking_JSON.cls_jointBuyers();
        buyerJSON.jbPartyId = partyId;
        buyerJSON.sharePercentage = '';
        jointBuyers.add(buyerJSON);
        regLine.jointBuyers = jointBuyers;
        regLine.bookingReference = srName;
        regLines.add(regLine);
        obj.registrationLines = regLines;
        String jsonBody = JSON.serialize (obj);
        System.Debug('JSON Body: ' + jsonBody);
        IPMS_Integration_Settings__mdt settings = [SELECT Client_ID__c, Endpoint_URL__c 
                                                        FROM IPMS_Integration_Settings__mdt 
                                                        WHERE DeveloperName = 'UK_Party' LIMIT 1];
        HttpRequest req = new HttpRequest ();
        string token = DAMAC_IPMS_PARTY_CREATION.getIPMSAccessToken();
        req.setEndpoint(settings.Endpoint_URL__c + '/booking');
        req.setMethod('POST');
        req.setHeader('Authorization', 'Bearer ' + token);
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        req.setBody (jsonBody);
        Http http = new HTTP ();
        HTTPResponse res = new HTTPResponse ();
        if (!Test.isRunningTest()) {
            res = http.send (req);
        } else {
            res.setBody ('{"status": "S","responseLines": [{"bookingReference": "BU-223596", '
                                + '"partyId": "34602", "status": "E" }], "complete": true}');
        }
        IPMS_PartyId_Creation_Response respCls = new IPMS_PartyId_Creation_Response();
        respCls = IPMS_PartyId_Creation_Response.parse(res.getBody());                    
        if(respCls.status == 'S'){
            responseStatus = 'Success';
        } else{
            responseStatus = 'Error';
        }
        List<log__c> logList = new List<log__c>();
        logList.add(createLogNoInsert(srId, req.getBody(), 'Joint Buyer - ' + buyerName + ' ' + reqName + ' Request'));
        logList.add(createLogNoInsert(srId, res.getBody(), 'Joint Buyer - ' + buyerName + ' ' + reqName + ' Response'));
        System.Debug ('Response body: ' + res.getBody());
        return logList;
    }
    /************************ 1.1 Ends *************************/
}