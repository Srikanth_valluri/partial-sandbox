@isTest
private class MoveOutCancelSRControllerTest {

	static testMethod void testMethod_cancelSR(){
        // SetUp();
		Booking_Unit__c bu = new Booking_Unit__c();
		Booking__c  bk = new  Booking__c();
		Account a = new Account();
		NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
		a.Name = 'Test Account';
	    a.party_ID__C = '1039032';
	    insert a;

	    Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
	    sr.NSIBPM__Customer__c = a.id;
	    sr.RecordTypeId = RecType;
	    insert sr;

	    bk.Account__c = a.id;
	    bk.Deal_SR__c = sr.id;
	    insert bk;

	    bu.Booking__c = bk.id;
	    bu.Unit_Name__c = 'LSB/10/B1001';
	    bu.Owner__c = a.id;
	    bu.Property_City__c = 'Dubai';
	    insert bu;
        FM_Case__c fmObj = new FM_Case__c();
        Id RecordTypeIdFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move Out').getRecordTypeId();
        fmObj.Booking_Unit__c = bu.id;
        fmObj.Expected_move_out_date__c = Date.Today();
        fmObj.Move_in_date__c = Date.Today();
        fmObj.Tenant__c = a.id;
        fmObj.Admin__c = UserInfo.getUserId();
        fmObj.Actual_move_out_date__c = system.today();
        fmObj.Account__c = a.id;
        fmObj.Tenant_Email__c = 'test@gmail.com';
        fmObj.Access_card_required__c = 'Yes';
        fmObj.recordtypeid = RecordTypeIdFMCase;
        insert fmObj;
		PageReference myVfPage = Page.MoveOutModifySRPage;
		Test.setCurrentPage(myVfPage);
		ApexPages.currentPage().getParameters().put('id',fmObj.Id);
		ApexPages.StandardController sc = new ApexPages.StandardController(fmObj);
        Test.startTest();
		MoveOutCancelSRController controller = new MoveOutCancelSRController(sc);
        controller.cancelSR();
        Test.stopTest();
    }

}