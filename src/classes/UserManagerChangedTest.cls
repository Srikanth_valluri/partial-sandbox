/**********************************************************************************************************
* Description - The class developed to cover code ocoverage of UserManagerChanged
*
* Version            Date            Author            Description
* 1.0                31/05/18        Monali            Initial Draft
***********************************************************************************************************/
@isTest
private class UserManagerChangedTest {
    static testmethod Map<String, Profile> getProfileDetails(){
        Set<String> profileNamesSet = new Set<String>{ 'Director of Sales', 'Head of Sales', 'Chairman'};
        Map<String, Profile> nameProfileMap = new MaP<String, Profile>();
        for(Profile thisProfile : [SELECT Id, Name FROM Profile WHERE Name =: profileNamesSet]){
            nameProfileMap.put(thisProfile.Name, thisProfile);  
        }
        return nameProfileMap;
    }

    static testmethod Map<String, UserRole> getUserRoles(){
        Map<String, UserRole> nameRoleMap = new MaP<String, UserRole>();
        for(UserRole role : [SELECT Id, DeveloperName FROM UserRole WHERE DeveloperName LIKE 'DOS2'
                             OR DeveloperName LIKE 'HOS2'
                             OR DeveloperName LIKE 'HOD1'
                             OR DeveloperName LIKE 'General_Manager']
                             ){
            nameRoleMap.put(role.DeveloperName, role);  
        }
        return nameRoleMap;
    }


    static testMethod void testMethod1() {
    	Map<String, Profile> profileInfo = getProfileDetails();
        Map<String, UserRole> roleInfo = getUserRoles();

        User user = [SELECT Id FROM User 
                      WHERE ManagerId <> null 
                        AND Manager.ManagerId <> null 
                        AND Manager.Manager.ManagerId <> null 
                        AND Manager.Manager.Manager.ManagerId <> null 
                        AND IsActive = true
                      LIMIT 1 ];

        User GM1 = new User();
        GM1.FirstName = 'test';
        GM1.LastName = 'testGM1';
        GM1.ProfileId = profileInfo.get('Chairman').Id;
        GM1.email = 'testGM1@gmail.com';
        GM1.alias = 'testGM1';
        GM1.emailencodingkey='UTF-8';
        GM1.languagelocalekey='en_US';
        GM1.localesidkey='en_US';
        GM1.country='United Arab Emirates';
        GM1.IsActive =true;
        GM1.timezonesidkey='America/Los_Angeles';
        GM1.username='testGM1@account.com';
        GM1.Languages_Known__c='English;Arabic;Hindi;French';
        GM1.UserRoleId = roleInfo.get('General_Manager').Id;
        insert GM1;

        User GM2 = new User();
        GM2.FirstName = 'test';
        GM2.LastName = 'testGM2';
        GM2.ProfileId = profileInfo.get('Chairman').Id;
        GM2.email = 'testGM2@gmail.com';
        GM2.alias = 'testGM2';
        GM2.emailencodingkey='UTF-8';
        GM2.languagelocalekey='en_US';
        GM2.localesidkey='en_US';
        GM2.country='United Arab Emirates';
        GM2.IsActive =true;
        GM2.timezonesidkey='America/Los_Angeles';
        GM2.username='testGM2@account.com';
        GM2.Languages_Known__c='English;Arabic;Hindi;French';
        GM2.UserRoleId = roleInfo.get('General_Manager').Id;
        insert GM2;

        User HOD1 = new User();
        HOD1.FirstName = 'test';
        HOD1.LastName = 'testHOD1';
        HOD1.ProfileId = profileInfo.get('Head of Sales').Id;
        HOD1.email = 'testHOD1@gmail.com';
        HOD1.alias = 'testHOD1';
        HOD1.emailencodingkey='UTF-8';
        HOD1.languagelocalekey='en_US';
        HOD1.localesidkey='en_US';
        HOD1.country='United Arab Emirates';
        HOD1.IsActive =true;
        HOD1.timezonesidkey='America/Los_Angeles';
        HOD1.username='testHOD1@account.com';
        HOD1.Languages_Known__c='English;Arabic;Hindi;French';
        HOD1.UserRoleId = roleInfo.get('HOD1').Id;
        HOD1.ManagerId = GM1.Id;
        insert HOD1;

        User HOD2 = new User();
        HOD2.FirstName = 'test';
        HOD2.LastName = 'testHOD2';
        HOD2.ProfileId = profileInfo.get('Head of Sales').Id;
        HOD2.email = 'testHOD2@gmail.com';
        HOD2.alias = 'testHOD2';
        HOD2.emailencodingkey='UTF-8';
        HOD2.languagelocalekey='en_US';
        HOD2.localesidkey='en_US';
        HOD2.country='United Arab Emirates';
        HOD2.IsActive =true;
        HOD2.timezonesidkey='America/Los_Angeles';
        HOD2.username='testHOD2@account.com';
        HOD2.Languages_Known__c='English;Arabic;Hindi;French';
        HOD2.UserRoleId = roleInfo.get('HOD1').Id;
        HOD2.ManagerId = GM1.Id;
        insert HOD2;

        User HOS1 = new User();
        HOS1.FirstName = 'test';
        HOS1.LastName = 'testHOS1';
        HOS1.ProfileId = profileInfo.get('Head of Sales').Id;
        HOS1.email = 'testHOS1@gmail.com';
        HOS1.alias = 'testHOS1';
        HOS1.emailencodingkey='UTF-8';
        HOS1.languagelocalekey='en_US';
        HOS1.localesidkey='en_US';
        HOS1.country='United Arab Emirates';
        HOS1.IsActive =true;
        HOS1.timezonesidkey='America/Los_Angeles';
        HOS1.username='testHOS1@account.com';
        HOS1.Languages_Known__c='English;Arabic;Hindi;French';
        HOS1.UserRoleId = roleInfo.get('HOS2').Id;
        HOS1.ManagerId = HOD1.Id;
        insert HOS1;


        User HOS2 = new User();
        HOS2.FirstName = 'test';
        HOS2.LastName = 'testHOS2';
        HOS2.ProfileId = profileInfo.get('Head of Sales').Id;
        HOS2.email = 'testHOS2@gmail.com';
        HOS2.alias = 'testHOS2';
        HOS2.emailencodingkey='UTF-8';
        HOS2.languagelocalekey='en_US';
        HOS2.localesidkey='en_US';
        HOS2.country='United Arab Emirates';
        HOS2.IsActive =true;
        HOS2.timezonesidkey='America/Los_Angeles';
        HOS2.username='testHOS2@account.com';
        HOS2.Languages_Known__c='English;Arabic;Hindi;French';
        HOS2.UserRoleId = roleInfo.get('HOS2').Id;
        HOS2.ManagerId = HOD2.Id;
        insert HOS2;

        User DOS1 = new User();
        DOS1.FirstName = 'test';
        DOS1.LastName = 'testDOS1';
        DOS1.ProfileId = profileInfo.get('Director of Sales').Id;
        DOS1.email = 'testDOS1@gmail.com';
        DOS1.alias = 'testDOS1';
        DOS1.emailencodingkey='UTF-8';
        DOS1.languagelocalekey='en_US';
        DOS1.localesidkey='en_US';
        DOS1.country='United Arab Emirates';
        DOS1.IsActive =true;
        DOS1.timezonesidkey='America/Los_Angeles';
        DOS1.username='testDOS1@account.com';
        DOS1.Languages_Known__c='English;Arabic;Hindi;French';
        DOS1.UserRoleId = roleInfo.get('DOS2').Id;
        DOS1.ManagerId = HOS1.Id;
        insert DOS1;

        User DOS2 = new User();
        DOS2.FirstName = 'test';
        DOS2.LastName = 'testDOS2';
        DOS2.ProfileId = profileInfo.get('Director of Sales').Id;
        DOS2.email = 'testDOS2@gmail.com';
        DOS2.alias = 'testDOS2';
        DOS2.emailencodingkey='UTF-8';
        DOS2.languagelocalekey='en_US';
        DOS2.localesidkey='en_US';
        DOS2.country='United Arab Emirates';
        DOS2.IsActive =true;
        DOS2.timezonesidkey='America/Los_Angeles';
        DOS2.username='testDOS2@account.com';
        DOS2.Languages_Known__c='English;Arabic;Hindi;French';
        DOS2.UserRoleId = roleInfo.get('DOS2').Id;
        DOS2.ManagerId = HOS2.Id;
        insert DOS2;
                    
        Id adminRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = 'Chairman' LIMIT 1].Id;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;

        User adminUser = new User(alias = 'qw56', email='xqw@email.com',
                emailencodingkey='UTF-8', lastname='Userqw', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xqw@email.com',UserRoleId = adminRoleId, ManagerId=user.Id);

        System.RunAs(adminUser){

        Test.startTest();
	        Account acc = InitialiseTestData.getCorporateAccount('Test Agency11');
	        insert acc ;

	        acc.Secondary_Owner__c = DOS1.Id;
	        update acc;

	        DOS1.ManagerId = HOS2.Id;
	        update DOS1;

	        HOS2.ManagerId = HOD1.Id;
	        update HOS2;

	        HOD1.ManagerId = GM2.Id;
	        update HOD1;

	        GM2.ManagerId = GM1.Id;
	        update GM2;

        Test.stopTest();

        }
    }
}