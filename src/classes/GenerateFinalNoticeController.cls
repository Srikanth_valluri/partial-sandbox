/**
 * @File Name          : GenerateFinalNoticeController.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10/21/2019, 4:39:45 AM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    10/14/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public with sharing class GenerateFinalNoticeController {
   public String fmCaseId;  
    public fm_case__c objFMCase;
    public List<Attachment> attachmentList; 
    public GenerateFinalNoticeController(ApexPages.StandardController stdCon) {
        fmCaseId = stdCon.getId();
        attachmentList = new List<Attachment>();
    }
    public pageReference returnToCase(){
        PageReference Pg = new PageReference('/'+fmCaseId);
        return pg;
    }
    public void callNoticePDFGenerator(){
        objFMCase = [
            SELECT 
                 Name
                , Booking_Unit__c
                , Account__c
                , First_Notice_Generation_Date__c
                , Account__r.Name
                , Notice_Type__c
                , Date_of_Noticing_Violation__c
                , Booking_Unit__r.Resident__r.Name
                , Unit_Name__c
                , Booking_Unit__r.Property_Name__c
                , Violation_rule_name__c
                , Remedial_Period__c
                , Payble_Fine__c
                , Rectification_Cost_Text__c
            FROM fm_case__c 
            WHERE Id =: fmCaseId];
        callNoticeHtmlDocument(objFMCase);
    } 
    public void callNoticeHtmlDocument(fm_case__c objFMCase){
        PageReference noticeHTMLDocument ;
        String docVersion = '';
        String todaysDate = String.valueOf(System.Today()); 
        todaysDate = todaysDate.substring(0, 10); 
        if(objFMCase.Notice_Type__c != NULL){
            
           // if(objFMCase.Notice_Type__c == 'First Notice'){
                
                // For First Notice HTML Page
                noticeHTMLDocument = page.FinalNoticeHTMLDocument;
                
                // Convert it into json
                String JSONString = JSON.serialize(objFMCase);
                System.debug('JSONString::::'+JSONString);

                // Call page to send json 
                noticeHTMLDocument.getparameters().put('srJSON', JSONString);
                 //System.debug('doc body::::'+noticeHTMLDocument.getContentAsPDF());
                
                Blob FinalNotice_DocBody;  
                try {
                    if(Test.isRunningTest()) { 
                        FinalNotice_DocBody = Blob.valueOf('test');
                    }
                    else{
                        // Get content from pdf
                        FinalNotice_DocBody = noticeHTMLDocument.getContentAsPDF();
                        System.debug('body should be fine');
                    } 
                   
                } catch (Exception e) {
                    System.debug('in the catch block');
                    FinalNotice_DocBody = Blob.valueOf('Some Text');
                }
                 
                Attachment finalNotice_Attachment = new Attachment();
                finalNotice_Attachment.Body = FinalNotice_DocBody;
                finalNotice_Attachment.Name = 'Final Notice Document' + '.pdf';
                finalNotice_Attachment.IsPrivate = false;
                finalNotice_Attachment.ContentType='application/pdf';
                finalNotice_Attachment.ParentId = fmCaseId;
                attachmentList.add(finalNotice_Attachment);
            }
        
        // insert attachmentList
        try{
            ApexPages.Message myMsg = new ApexPages.Message(
                ApexPages.Severity.CONFIRM,
                 'Your request for Notice document was successfully submitted.'+
                 ' Please check the documents section for the document in a while.');
              ApexPages.addMessage(myMsg);
            insert attachmentList;
        }
        catch(Exception e){
            System.debug('exception:::::::'+e);
        }

    }
}