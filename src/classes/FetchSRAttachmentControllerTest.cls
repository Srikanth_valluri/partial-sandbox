// This class is test class for FetchSRAttachmentController
@isTest
public class FetchSRAttachmentControllerTest{
    
    public static Id handoverParentRecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Handover Parent').RecordTypeId;
    public static Id handoverChildRecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Handover').RecordTypeId;
    
    public static testmethod void testController1(){
        
        Account objA = TestDataFactory_CRM.createPersonAccount();
        insert objA;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        list<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount(objA.Id, objSR.Id, 2);
        insert lstBookings;     
        
        list<Booking_Unit__c> lstBU = TestDataFactory_CRM.createBookingUnits(lstBookings,2);
        for(Booking_Unit__c objB : lstBU){
            objB.Unit_Name__c = 'DFA/12/1204';
        }
        insert lstBU;       
        
        Case objC = TestDataFactory_CRM.createCase(objA.Id, handoverChildRecordTypeId);
        objC.Booking_Unit__c = lstBU[0].Id;
        insert objC;
        
        list<Case> lstCase = new list<Case>();
        Case objC1 = new Case();
        objC1.AccountId = objA.Id;
        objC1.Booking_Unit__c = lstBU[0].Id;
        objC1.ParentId = objC.Id;
        objC1.RecordTypeId = handoverChildRecordTypeId;
        lstCase.add(objC1);
        
        Case objC2 = new Case();
        objC2.AccountId = objA.Id;
        objC2.Booking_Unit__c = lstBU[1].Id;
        objC2.ParentId = objC.Id;
        objC2.RecordTypeId = handoverChildRecordTypeId;
        lstCase.add(objC2);
        insert lstCase;
        
        lstCase[0].PRC_Document__c = true;
        update lstCase;
        
        
        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Case__c  = objC.Id;
        objAttach.isValid__c = true;
        objAttach.Description__c = 'Abctd';
        objAttach.IsRequired__c = True;
        objAttach.Name = 'Test';
        objAttach.Need_Correction__c =  False;
        insert objAttach;

        test.StartTest();
            Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockAssignment());
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            PageReference PageInst = Page.FetchSRAttachment;
            Test.setCurrentPage(PageInst);
            String childSRId = PageInst.getParameters().put('Id',String.valueOf(objC1.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(objC1);
            FetchSRAttachmentController controller = new FetchSRAttachmentController(sc);
            controller.init();    
            
        test.StopTest();
    }
    
    public static testmethod void testController2(){
        
        Account objA = TestDataFactory_CRM.createPersonAccount();
        insert objA;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        list<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount(objA.Id, objSR.Id, 2);
        insert lstBookings;     
        
        list<Booking_Unit__c> lstBU = TestDataFactory_CRM.createBookingUnits(lstBookings,2);
        for(Booking_Unit__c objB : lstBU){
            objB.Unit_Name__c = 'DFA/12/1204';
        }
        insert lstBU;       
        
        Case objC = TestDataFactory_CRM.createCase(objA.Id, handoverChildRecordTypeId);
        objC.Booking_Unit__c = lstBU[0].Id;
        insert objC;
        
        list<Case> lstCase = new list<Case>();
        Case objC1 = new Case();
        objC1.AccountId = objA.Id;
        objC1.Booking_Unit__c = lstBU[0].Id;
        objC1.ParentId = objC.Id;
        objC1.RecordTypeId = handoverChildRecordTypeId;
        lstCase.add(objC1);
        
        Case objC2 = new Case();
        objC2.AccountId = objA.Id;
        objC2.Booking_Unit__c = lstBU[1].Id;
        objC2.ParentId = objC.Id;
        objC2.RecordTypeId = handoverChildRecordTypeId;
        lstCase.add(objC2);
        insert lstCase;
        
        lstCase[0].PRC_Document__c = true;
        update lstCase;

        test.StartTest();
            PageReference PageInst = Page.FetchSRAttachment;
            Test.setCurrentPage(PageInst);
            String childSRId = PageInst.getParameters().put('Id',String.valueOf(objC1.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(objC1);
            FetchSRAttachmentController controller = new FetchSRAttachmentController(sc);
            controller.init();    
            
        test.StopTest();
    }
    
    public static testmethod void testController3(){
        
        Account objA = TestDataFactory_CRM.createPersonAccount();
        insert objA;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        list<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount(objA.Id, objSR.Id, 2);
        insert lstBookings;     
        
        list<Booking_Unit__c> lstBU = TestDataFactory_CRM.createBookingUnits(lstBookings,2);
        for(Booking_Unit__c objB : lstBU){
            objB.Unit_Name__c = 'DFA/12/1204';
        }
        insert lstBU;       
        
        Case objC = TestDataFactory_CRM.createCase(objA.Id, handoverChildRecordTypeId);
        objC.Booking_Unit__c = lstBU[0].Id;
        insert objC;
        
        list<Case> lstCase = new list<Case>();
        Case objC1 = new Case();
        objC1.AccountId = objA.Id;
        objC1.Booking_Unit__c = lstBU[0].Id;
        objC1.RecordTypeId = handoverChildRecordTypeId;
        lstCase.add(objC1);
        
        Case objC2 = new Case();
        objC2.AccountId = objA.Id;
        objC2.Booking_Unit__c = lstBU[1].Id;
        objC2.RecordTypeId = handoverChildRecordTypeId;
        lstCase.add(objC2);
        insert lstCase;
        
        lstCase[0].PRC_Document__c = true;
        update lstCase;

        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Case__c  = objC.Id;
        objAttach.isValid__c = true;
        objAttach.Description__c = 'Abctd';
        objAttach.IsRequired__c = True;
        objAttach.Name = 'Test';
        objAttach.Need_Correction__c =  False;
        insert objAttach;
        
        test.StartTest();
            Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockAssignment());
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            PageReference PageInst = Page.FetchSRAttachment;
            Test.setCurrentPage(PageInst);
            String childSRId = PageInst.getParameters().put('Id',String.valueOf(objC1.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(objC1);
            FetchSRAttachmentController controller = new FetchSRAttachmentController(sc);
            controller.init();    
            
        test.StopTest();
    }
}