/*************************************************************************************************************************
* Name               : PreInquiryGetEligiblePCS                                                                             *
* Description        : This is a InquiryTriggerHandler helper class. It will assign inquiry owner based on inquiry assignment rules 
                       for pre inquiry record type                            
* Test class Name    : PreInquiryGetEligiblePCSTest                                                                                                    
* Created Date       : 22/05/2018                                                                                        *
* Created By         : CH V Gopinadh                                                                                     *
* --------------------------------------------------------------------------------------------------------------------   */
public class PreInquiryGetEligiblePCS {
    /*********** This method assigns the owner of the inquiry object ****************/
    public List<Inquiry__c> getEligiblsPCS(List<Inquiry__c> newInquiryList,string executeOn) {
        List<Inquiry__c> finalList = new List<Inquiry__c>();
        Map<string,List<Inquiry__c>> inquiryAssignmentMap = userAssignment(newInquiryList,executeOn);
        System.debug('inquiryAssignmentMapinquiryAssignmentMap'+inquiryAssignmentMap);
        if ( inquiryAssignmentMap.get('assigned') != null ) {
            finalList.addAll(inquiryAssignmentMap.get('assigned'));
        } 
        
        if ( inquiryAssignmentMap.get('notassigned') != null ) {
        
            newInquiryList = new List<Inquiry__c>();
            newInquiryList.addAll(inquiryAssignmentMap.get('notassigned'));
            
        
            if (newInquiryList.size() > 0) {
                inquiryAssignmentMap = queueAssignment(newInquiryList,executeOn);
                System.debug('inquiryAssignmentMapinquiryAssignmentMap'+inquiryAssignmentMap);
                if ( inquiryAssignmentMap.get('assigned') != null ) {
                    finalList.addAll(inquiryAssignmentMap.get('assigned'));
                } 
            }
        }
        if ( inquiryAssignmentMap.get('notassigned') != null ) {
            newInquiryList = new List<Inquiry__c>();
            newInquiryList.addAll(inquiryAssignmentMap.get('notassigned'));
            
            if ( newInquiryList.size() > 0 ) {
                Map<Inquiry__c,List<User>> eligibleUsers = getEligiblePCSfromQueue(newInquiryList,executeOn,null);
                Map<Inquiry__c,Id> ownerIds = InquiryOwnerAssignment.assignOwner(eligibleUsers);
                System.debug('oooooooooooo'+ownerIds);
                for (Inquiry__c inq : ownerIds.keyset() ) {
                    if ( ownerIds.get(inq) != null && executeOn != 'Update') {
                        inq.OwnerId = ownerIds.get(inq);
                    }
                    finalList.add(inq);
                }
            }
        }
        System.debug('finalListfinalList'+finalList); 
        return finalList;
    }
    
    /*************** This method directly assigns the queue of inquiry assignment rule ***********/
    public Map<string,List<Inquiry__c>> queueAssignment(List<Inquiry__c> newInquiryList,string executeOn) {
        Map<string,List<Inquiry__c>> inquiryAssignmentMap = new Map<string,List<Inquiry__c>>();
        Id preInquiryRecId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        Id inquiryRecId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        
        string fields = getAllfields('Inquiry_Assignment_Rules__c');
        string queryCondition = 'select '+fields+' from Inquiry_Assignment_Rules__c where Fields_to_Apply_Rule__c != null and Active__c = True and Queue_Assignment__c = True and Execute_on__c = '+'\''+executeOn+'\''+' order by Priority__c Asc';
        List<Inquiry_Assignment_Rules__c> assignmentRules = database.query(queryCondition);
        if ( assignmentRules.size() > 0 ) {
            for (Inquiry__c inq : newInquiryList) {
                Integer tempFlag = 0;
                for (Inquiry_Assignment_Rules__c inqAssignment : assignmentRules ) {
                    set<String> rulesList = DamacUtility.splitMutliSelect(inqAssignment.Fields_to_Apply_Rule__c);
                    Integer flag = 0;
                    for (string field : rulesList) {
                        field = field.trim();
                        string field1 = field;
                        if ( field == 'RecordTypeId' ) {
                            field1 = 'Inquiry_Record_Type__c';
                            if (inqAssignment.get(field1) == 'Pre Inquiry') {
                                inqAssignment.Inquiry_Record_Type__c = preInquiryRecId;
                                
                            }
                            if (inqAssignment.get(field1) == 'Inquiry') {
                                inqAssignment.Inquiry_Record_Type__c = InquiryRecId;
                                
                            }
                        }
                        try {
                            if ( inq.get(field) != null && inqAssignment.get(field1) != null) { 
                                if ( inq.get(field) == inqAssignment.get(field1)){
                                    flag = 1;
                                } else {
                                    flag = 0;
                                    break;
                                }
                            } else {
                                flag = 0;
                                break;
                            }
                        } catch ( Exception e ) {
                            break;
                        }
                    }
                    tempFlag = flag;
                    if (flag == 1) {
                        if (executeOn != 'Update')
                            inq.ownerId = inqAssignment.On_Create_Queue_Id__c;
                        inq.Not_Eligible_for_Reassignment__c = inqAssignment.Exclude_from_Re_assignment__c; // Added on July 8 2018 
                        if (inq.OwnerID == NULL && executeOn != 'Update') 
                            inq.OwnerId = inqAssignment.OwnerID;   
                        
                        inq.Inquiry_Assignment_Rules__c = inqAssignment.id;
                        if (executeOn == 'Update') {
                            inq.Allocation_Model__c = inqAssignment.Allocation_Model__c;
                        }
                        System.Debug ('Rule ID '+inqAssignment.id);
                        if ( inquiryAssignmentMap.containsKey('assigned')) {
                            inquiryAssignmentMap.get('assigned').add(inq);
                        } else {
                            inquiryAssignmentMap.put('assigned',new List<Inquiry__c>{inq});
                        }
                        break;
                    } 
                }
                if ( tempFlag == 0 ) {
                    if ( inquiryAssignmentMap.containsKey('notassigned')) {
                        inquiryAssignmentMap.get('notassigned').add(inq);
                    } else {
                        inquiryAssignmentMap.put('notassigned',new List<Inquiry__c>{inq});
                    }  
                }
            }   
        } else {
            for (Inquiry__c inq : newInquiryList) {
                if ( inquiryAssignmentMap.containsKey('notassigned')) {
                    inquiryAssignmentMap.get('notassigned').add(inq);
                } else {
                    inquiryAssignmentMap.put('notassigned',new List<Inquiry__c>{inq});
                }
            }
        }
        return inquiryAssignmentMap;
    }
    
    /*************** This method directly assigns the queue of inquiry assignment rule ***********/
    public Map<string,List<Inquiry__c>> userAssignment(List<Inquiry__c> newInquiryList,string executeOn) {
        Map<string,List<Inquiry__c>> inquiryAssignmentMap = new Map<string,List<Inquiry__c>>();
        Id preInquiryRecId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        Id inquiryRecId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        
        string fields = getAllfields('Inquiry_Assignment_Rules__c');
        string queryCondition = 'select '+fields+' from Inquiry_Assignment_Rules__c where Fields_to_Apply_Rule__c != null and Active__c = True and Assign_to_Created_by__c = True and Execute_on__c = '+'\''+executeOn+'\''+' order by Priority__c Asc';
        List<Inquiry_Assignment_Rules__c> assignmentRules = database.query(queryCondition);
        if ( assignmentRules.size() > 0 ) {
            for (Inquiry__c inq : newInquiryList) {
                Integer tempFlag = 0;
                for (Inquiry_Assignment_Rules__c inqAssignment : assignmentRules ) {
                    set<String> rulesList = DamacUtility.splitMutliSelect(inqAssignment.Fields_to_Apply_Rule__c);
                    Integer flag = 0;
                    for (string field : rulesList) {
                        field = field.trim();
                        string field1 = field;
                        if ( field == 'RecordTypeId' ) {
                            field1 = 'Inquiry_Record_Type__c';
                            if (inqAssignment.get(field1) == 'Pre Inquiry') {
                                inqAssignment.Inquiry_Record_Type__c = preInquiryRecId;
                                
                            }
                            if (inqAssignment.get(field1) == 'Inquiry') {
                                inqAssignment.Inquiry_Record_Type__c = InquiryRecId;
                                
                            }
                        }
                        try {
                            if ( inq.get(field) != null && inqAssignment.get(field1) != null) { 
                                if ( inq.get(field) == inqAssignment.get(field1)){
                                    flag = 1;
                                } else {
                                    flag = 0;
                                    break;
                                }
                            } else {
                                flag = 0;
                                break;
                            }
                        } catch ( Exception e ) {
                            break;
                        }
                    }
                    tempFlag = flag;
                    if (flag == 1) {
                           
                        inq.Inquiry_Assignment_Rules__c = inqAssignment.id;
                        if (executeOn == 'Update') {
                            inq.Allocation_Model__c = inqAssignment.Allocation_Model__c;
                        }
                        System.Debug ('Rule ID '+inqAssignment.id);
                        if ( inquiryAssignmentMap.containsKey('assigned')) {
                            inquiryAssignmentMap.get('assigned').add(inq);
                        } else {
                            inquiryAssignmentMap.put('assigned',new List<Inquiry__c>{inq});
                        }
                        break;
                    } 
                }
                if ( tempFlag == 0 ) {
                    if ( inquiryAssignmentMap.containsKey('notassigned')) {
                        inquiryAssignmentMap.get('notassigned').add(inq);
                    } else {
                        inquiryAssignmentMap.put('notassigned',new List<Inquiry__c>{inq});
                    }  
                }
            }   
        } else {
            for (Inquiry__c inq : newInquiryList) {
                if ( inquiryAssignmentMap.containsKey('notassigned')) {
                    inquiryAssignmentMap.get('notassigned').add(inq);
                } else {
                    inquiryAssignmentMap.put('notassigned',new List<Inquiry__c>{inq});
                }
            }
        }
        return inquiryAssignmentMap;
    }
   
    /*************** This method filters the queue users based on tenure and languaage of the user ************/
    public Map<Inquiry__c,List<User>> getEligiblePCSfromQueue(List<Inquiry__c> newInquiryList,string executeOn,List<Inquiry_Assignment_Rules__c> existingRules) {
        set<Id> assignerIds = new set<Id>();
        set<Id> queueIds = new Set<Id>();
        Id preInquiryRecId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        Id inquiryRecId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        
        Map<Inquiry__c,set<Id>> assignersMap = new Map<Inquiry__c,set<Id>>();
        Map<String,string> codeCategoryMap = new Map<String,string>();
        Map<String,string> codeCategoryMapForUpdate = new Map<String,string>();
        List<Inquiry_Country_Code_Categories__c> categories = [select name,Category__c,Category_Update__c from Inquiry_Country_Code_Categories__c];
        for ( Inquiry_Country_Code_Categories__c code : categories) {
            codeCategoryMap.put(code.Name,code.Category__C);
            codeCategoryMapForUpdate.put(code.Name,code.Category_Update__c);
        }
        string fields = getAllfields('Inquiry_Assignment_Rules__c');
        string queryCondition = 'select '+fields+' from Inquiry_Assignment_Rules__c where Fields_to_Apply_Rule__c != null and Active__c = True and Execute_on__c = '+'\''+executeOn+'\''+' order by Priority__c Asc';
        
        List<Inquiry_Assignment_Rules__c> assignmentRules = new  List<Inquiry_Assignment_Rules__c>();
        if ( existingRules != null) {
            assignmentRules.addAll(existingRules);
        } else {
            assignmentRules = database.query(queryCondition);
        }
        Map<Inquiry__c,Id> inquiryOwnerMap = new Map<Inquiry__c,Id>();
        Map<Id,string> ruleTenureMap = new Map<Id,string>();
        Map<Id,Boolean> ruleLanguageMap = new Map<Id,Boolean>();
        for (Inquiry__c inq : newInquiryList) {
            for (Inquiry_Assignment_Rules__c inqAssignment : assignmentRules  ) {
                
                Integer flag = 0;
                
                if ( executeOn == 'Reassign' ) {
                    flag = 1;
                } else {
                    set<String> rulesList = DamacUtility.splitMutliSelect(inqAssignment.Fields_to_Apply_Rule__c);
                    System.debug('rulesListrulesList'+rulesList);
                    for (string field : rulesList) {
                        field = field.trim();
                        string field1 = field;
                        if ( field == 'RecordTypeId' ) {
                            field1 = 'Inquiry_Record_Type__c';
                            if (inqAssignment.get(field1) == 'Pre Inquiry') {
                                inqAssignment.Inquiry_Record_Type__c = preInquiryRecId;
                                
                            }
                            if (inqAssignment.get(field1) == 'Inquiry') {
                                inqAssignment.Inquiry_Record_Type__c = InquiryRecId;
                                
                            }
                        }
                        
                        try {
                            if ( field == 'Mobile_CountryCode__c' ) {
                                if ( inq.get(field) != null ) {
                                    if(executeon == 'Create'){
                                        string code = codeCategoryMap.get(String.valueOf(inq.get(field)));
                                        if ( inqAssignment.get('Category__c')  == code ) {
                                            flag = 1;
                                        } else {
                                            flag = 0;
                                            break;
                                        }
                                    }else if(executeon == 'Update'){
                                        string code = codeCategoryMapForUpdate.get(String.valueOf(inq.get(field)));
                                        if ( inqAssignment.get('Category_update__c')  == code ) {
                                            flag = 1;
                                        } else {
                                            flag = 0;
                                            break;
                                        }
                                    }
                                } else {
                                    flag = 0;
                                    break;
                                }
                                
                            } else if ( inq.get(field) != null && inqAssignment.get(field1) != null) { 
                                if ( inq.get(field) == inqAssignment.get(field1)){
                                    
                                    flag = 1;
                                    System.Debug ('flag==='+flag);
                                } else {
                                    flag = 0;
                                    break;
                                }
                            } else {
                                flag = 0;
                                break;
                            }
                        } catch ( Exception e ) {
                            break;
                        }
                    }
                }
                System.Debug ('flag==='+flag);
                if (flag == 1 ) {
                    
                    Id assignmentOwnerId = inqAssignment.ownerId;
                    inq.Inquiry_Assignment_Rules__c = inqAssignment.id;
                    if (executeOn == 'Update') {
                        inq.Allocation_Model__c = inqAssignment.Allocation_Model__c;
                    }
                    if (executeOn != 'Update')
                        inq.OwnerId = inqAssignment.On_Create_Queue_Id__c;
                    inq.Not_Eligible_for_Reassignment__c = inqAssignment.Exclude_from_Re_assignment__c; // Added on July 8 2018 
                    if (inq.OwnerID == NULL && executeOn != 'Update') 
                        inq.OwnerId = inqAssignment.OwnerID;    
                    ruleTenureMap.put(assignmentOwnerId,inqAssignment.Tenure__c);
                    ruleLanguageMap.put(assignmentOwnerId, inqAssignment.Skip_language_Check__c); 
                    if(string.valueOf(assignmentOwnerId).startsWith('005')) {
                        assignerIds.add(assignmentOwnerId);
                        assignersMap.put(inq,assignerIds);
                        
                    } else if ( string.valueOf(assignmentOwnerId).startsWith('00G') ) {
                        queueIds.add(assignmentOwnerId);  
                        assignersMap.put(inq,queueIds);
                    }
                    
                    break;
                }
                
            } 
            
        }
        system.debug('assignersMapassignersMap'+assignersMap);
        Map<Id,set<Id>> queueUsersMap = new Map<Id,set<Id>>();
        Map<Id,Id> userQueueMap = new Map<Id,Id>();
        if ( queueIds.size() > 0 || test.isRunningTest()) {
            List<groupmember> groupMemberList = [select userorgroupid,groupid from groupmember where groupid=:queueIds];
            for ( groupmember member : groupMemberList) {
                assignerIds.add(member.userorgroupid);
                userQueueMap.put(member.userorgroupid,member.groupid);
                if ( queueUsersMap.containskey(member.groupid)) {
                    queueUsersMap.get(member.groupid).add(member.userorgroupid);
                } else {
                    queueUsersMap.put(member.groupid,new Set<Id>{member.userorgroupid});
                }
            }
            
        }
        System.debug('queueUsersMapqueueUsersMap'+queueUsersMap);
        Map<Inquiry__c,List<User>> eligibleUsers = new Map<Inquiry__c,List<User>>();
        Map<Id,User> assigners = new Map<Id,User>([select Languages_Known__c,Date_of_Joining__c from user where id in : assignerIds
                                                   and IsActive = True
                                                   and Is_Blacklisted__c = False and isUserOnLeave__c = False]);
        for (Inquiry__c thisInquiry : newInquiryList) {
            List<user> eligibleUserList = new List<User>();
            if ( assignersMap.get(thisInquiry) != null ) {
                for(Id queueOruserId : assignersMap.get(thisInquiry)){
                    set<Id> userIds = new set<Id>();
                    thisInquiry.Assignment_Queue_Id__c = 'temp_'+queueOruserId;
                    if(string.valueOf(queueOruserId).startsWith('005')) {
                        userIds.add(queueOruserId);
                    } else if ( string.valueOf(queueOruserId).startsWith('00G') ) {
                        userIds.addAll(queueUsersMap.get(queueOruserId));
                    }
                    for ( Id thisUserId : userIds ) {
                        if (assigners.containsKey (thisUserID)) {
                            User  thisUser = assigners.get(thisUserId);
                            Date userdateOfJoin = thisUser.Date_of_Joining__c;
                            if(test.isRunningTest()) {
                                userQueueMap.put(thisUserId,thisUserId);
                                userdateOfJoin = system.today().adddays(-1);
                            }
                            if ( userQueueMap.get(thisUserId) != null ) {
                                Id queueId =  userQueueMap.get(thisUserId);
                                string tenure = ruleTenureMap.get(queueId);
                                Boolean language = ruleLanguageMap.get (queueId);
                                System.Debug ('Language'+language);
                                if ( tenure != null ) {
                                    Integer flag = 0;
                                    if ( userdateOfJoin != null ) {
                                        string weekOrMonth = tenure.substringAfter(' ');
                                        string symbol= tenure.substringBefore(' ').substring(0,1);
                                        Integer no = Integer.valueOf(tenure.substringBefore(' ').substring(1));
                                        Integer days = 0;
                                        if ( weekOrMonth.contains('week')) {
                                            days = no * 7;
                                        } else if (weekOrMonth.contains('month')) {
                                            days = no * 30;
                                        }
                                        Integer userTenure = userdateOfJoin.daysBetween(Date.today());
                                        if ( symbol == '>' ) {
                                            if ( userTenure >= days ) {
                                                flag = 1;
                                            }
                                            
                                        } else if (symbol == '<' ) {
                                            if ( userTenure <= days ) {
                                                flag = 1;
                                            }
                                        }
                                    } 
                                    if ( flag == 1 ) {
                                        if (language == FALSE) {
                                            if((String.isNotBlank(thisUser.Languages_Known__c) &&
                                                String.isNotBlank(thisInquiry.Preferred_Language__c) &&
                                                DamacUtility.splitMutliSelect(thisUser.Languages_Known__c).contains(thisInquiry.Preferred_Language__c)) ||
                                               (String.isNotBlank(thisUser.Languages_Known__c) &&
                                                String.isBlank(thisInquiry.Preferred_Language__c) &&
                                                thisUser.Languages_Known__c.contains(DAMAC_Constants.DEFAULT_LANGUAGE))) {
                                                eligibleUserList.add(thisUser);
                                            }
                                        }
                                        else {
                                            eligibleUserList.add(thisUser);
                                        }
                                    }
                                    
                                } else {
                                    if (language == FALSE) {
                                        
                                        if((String.isNotBlank(thisUser.Languages_Known__c) &&
                                        String.isNotBlank(thisInquiry.Preferred_Language__c) &&
                                        DamacUtility.splitMutliSelect(thisUser.Languages_Known__c).contains(thisInquiry.Preferred_Language__c)) ||
                                       (String.isNotBlank(thisUser.Languages_Known__c) &&
                                        String.isBlank(thisInquiry.Preferred_Language__c) &&
                                        thisUser.Languages_Known__c.contains(DAMAC_Constants.DEFAULT_LANGUAGE))) {
                                            eligibleUserList.add(thisUser);
                                        } 
                                    }  else {
                                        eligibleUserList.add(thisUser);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            System.debug('eligibleUserListeligibleUserList'+eligibleUserList);
            eligibleUsers.put(thisInquiry,eligibleUserList);
        }
        return eligibleUsers;
    }
    
    /**** This method returns all fields of Inquiry Object **********/
    public static string getAllfields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ())
            fields += f+ ', ';
        return fields.removeEnd(', '); 
        
    }
}