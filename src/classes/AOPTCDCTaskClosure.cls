/*
 * Description - Called from process builder when AOPT CDC task is closed
 *
 * Version        Date            Author            Description
 * 1.0            21/05/18        Vivek Shinde      Initial Draft
 */
public class AOPTCDCTaskClosure {
    
    @InvocableMethod
    public static void ValidateDocument(List<Task> lstTask) {
        Set<Id> setCaseId = new Set<Id>();
        for(Task objTask: lstTask) {
            if(String.valueOf( objTask.whatId ).startsWith('500'))
                setCaseId.add(objTask.WhatId);
        }
        
        if(!setCaseId.isEmpty()) {
            Set<Id> setCaseIdForUpdate = new Set<Id>();
            for(Id idCase: setCaseId) {
                try {
                    AOPTServiceRequestHandler.makeCallout(idCase);
                    setCaseIdForUpdate.add(idCase);
                } catch(exception ex){
                    Error_Log__c objError = new Error_Log__c(Error_Details__c='Error: '+ex.getMessage()+'-'+ex.getstackTraceString(), Case__c=idCase, Process_Name__c='AOPT');
                    insert objError; 
                }
            }
            List<Case> lstCase = [Select Id, Apply_New_Payment_Terms__c From Case Where Id IN: setCaseIdForUpdate];
            for(Case objCase: lstCase) {
                objCase.Apply_New_Payment_Terms__c  = true;
            }
            update lstCase;
        }
    }
}