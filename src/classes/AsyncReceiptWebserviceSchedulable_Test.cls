@isTest
public class AsyncReceiptWebserviceSchedulable_Test {
    
    @testSetup 
    static void setupData() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;
        
        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.id;
        nstp1.Step_No__c = 2;
        nstp1.Step_Status__c = 'Awaiting Token Deposit';
        nstp1.Step_Type__c = 'Token Payment';
        insert nstp1;
        
        Booking__c booking = new Booking__c(Account__c = acc.Id, Deal_SR__c = sr.Id);
        insert booking;
        
        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id, Unit_Name__c = 'Test name 1',
                                                          Registration_ID__c = '1600', Registration_Status__c = 'Active Status',
                                                          Unit_Selling_Price_AED__c = 100);
        insert bookingUnit;
    }
    
    
    @isTest
    static void testScheduler() {
        Test.startTest();
        
        map<Id, Booking__c> BookingMap = new map<Id, Booking__c>([Select Id From Booking__c]);
        
        AsyncReceiptWebserviceSchedulable scheduledJob = new AsyncReceiptWebserviceSchedulable(new List<Id>(BookingMap.keySet()));
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Account Sales Calculator Check', sch, scheduledJob); 
        Test.stopTest();
    }
}