/************************************************************************************************
 * @File Name          : PaymentReminderBefore60Batch.cls                                           *
 * @Description        :             *
 * @Author             : Mosib Khan                                                         *
 * @Group              :                                                                        *
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 05/02/2019, 2:26:21 PM
 * @Modification Log   :                                                                        * 
 *===============================================================================================
 * Ver      Date            Author                  Modification                                *
 *===============================================================================================
 * 1.0                      Mosib Khan           Initial Version                             *
 
*************************************************************************************************/
public without sharing class PaymentReminderBefore60Batch implements Database.Batchable<sObject>
                                                                , Database.AllowsCallouts
                                                                , Database.Stateful { 
    private String query;
    public String recordId;
    List<DP_Invoices__c> lstInvoicesTUpdate;
    List<EmailMessage> lstEmails;
    public String regId;
    public List<Attachment> attachmentUrlsForEmailMessage; 
    public Database.Querylocator start( Database.BatchableContext bc ) {
        recordId =  Label.dailyDpBatchForSingleUser.split('-',2)[1];
        query = ' SELECT Id,'+ 
                ' Name,'+
                ' Unit_Name__c,'+
                ' Property_Name__c,'+ 
                ' FinalDate__c,' +
                ' Milestone_Type_for_Dunning__c,'+
                ' When2Trigger__c,' +
                ' Current_Level__c,' +
                ' Future_Level__c,' +
                ' sixtyninty__c,' +
                ' PropertyCategory__c,'+
                ' ReraPersentage__c ,'+
                ' Current_percentage__c,'+
                ' Expected_percentage__c,'+
                ' Expected_Date__c,'+
                ' Accounts__c, '+
                ' Accounts__r.recordTypeId, '+
                ' Accounts__r.email__c, '+
                ' Accounts__r.email__pc, '+
                ' Accounts__r.Name,Dunning_60_Email_Sent__c '+
                ' FROM DP_Invoices__c '+
                ' Where ';
                if(Label.dailyDpBatchForSingleUser.split('-',2)[0] == 'ON'){
                    query +=' ID = :recordId';
                }
                else{
                     query += ' sixtyninty__c = TRUE AND PropertyCategory__c != NULL  '+
                              ' AND  ( When2Trigger__c >= 45 AND When2Trigger__c <= 60 )';
                }
        System.debug('Dp  ::: '+query);       
        System.debug('Dp  ::: '+Database.getQueryLocator(query));
        return Database.getQueryLocator(query);
    }
    public void execute( Database.BatchableContext bc, list<DP_Invoices__c> dpInvoiceRecords ) {
       
        lstEmails = new List<EmailMessage>();
        lstInvoicesTUpdate = new List<DP_Invoices__c>();
        attachmentUrlsForEmailMessage = new List<Attachment>();
        System.debug('Payment Reminder Before 60  execute dpInvoiceRecords  == ' + dpInvoiceRecords );
        String TemplateName;
        /**
         *      akoya_payment_reminder_60_days_date_base
         *      akoya_payment_reminder_60_days_percentage_base
                akoya_payment_reminder_60_days_structure_base
                
                damac_hills_payment_reminder_60_days_date_base
                damac_hills_payment_reminder_60_days_percentage_base
                damac_hills_payment_reminder_60_days_structure_base
                
                payment_reminder_60_days_date_base
                payment_reminder_60_days_percentage_base
                payment_reminder_60_days_structure_base
                
         * 
         */
        for(DP_Invoices__c objInvoice: dpInvoiceRecords){ 
            if(objInvoice.PropertyCategory__c.contains('AKOYA') && objInvoice.Milestone_Type_for_Dunning__c.contains('Date')){
                TemplateName='akoya_payment_reminder_60_days_date_base'; 
            }else if(objInvoice.PropertyCategory__c.contains('AKOYA') && objInvoice.Milestone_Type_for_Dunning__c.contains('Percentage ')){
                TemplateName='akoya_payment_reminder_60_days_structure_base'; 
            }else if(objInvoice.PropertyCategory__c.contains('AKOYA') && objInvoice.Milestone_Type_for_Dunning__c.contains('Structure ')){
                TemplateName='akoya_payment_reminder_60_days_structure_base'; 
            }else if(objInvoice.PropertyCategory__c.contains('DH') && objInvoice.Milestone_Type_for_Dunning__c.contains('Date')){
                TemplateName='damac_hills_payment_reminder_60_days_date_base'; 
            }else if(objInvoice.PropertyCategory__c.contains('DH') && objInvoice.Milestone_Type_for_Dunning__c.contains('Percentage')){
                TemplateName='damac_hills_payment_reminder_60_days_percentage_base'; 
            }else if(objInvoice.PropertyCategory__c.contains('DH') && objInvoice.Milestone_Type_for_Dunning__c.contains('Structure')){
                TemplateName='damac_hills_payment_reminder_60_days_structure_base'; 
            }else if(objInvoice.PropertyCategory__c.contains('Others') && objInvoice.Milestone_Type_for_Dunning__c.contains('Date')){
                TemplateName='payment_reminder_60_days_date_base'; 
            }else if(objInvoice.PropertyCategory__c.contains('Others') && objInvoice.Milestone_Type_for_Dunning__c.contains('Percentage')){
                TemplateName='payment_reminder_60_days_percentage_base'; 
            }else if(objInvoice.PropertyCategory__c.contains('Others') && objInvoice.Milestone_Type_for_Dunning__c.contains('Structure')){
                TemplateName='payment_reminder_60_days_structure_base'; 
            }
            sendEmail(objInvoice,TemplateName); 
        }

        System.debug('lstEmails=== ' + lstEmails);
        if(lstEmails.size() > 0) {
            if(!Test.isRunningTest()) {
                insert lstEmails;
            }
            System.debug('lstEmails==after insert= ' + lstEmails);
        }
        

        if(lstInvoicesTUpdate.size() > 0){ update lstInvoicesTUpdate;}
    }
    public void finish( Database.BatchableContext bc ) {
    }

    public void sendEmail(DP_Invoices__c objDPInvoice,String TemplateName) {
        List<Attachment>attachmentUrls = new List<Attachment>();
        
        Id personRecTypeId, businessRecTypeId;
        personRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        
        
        if(objDPInvoice.Accounts__c != NULL && String.isNotBlank(TemplateName)){
            System.debug('objDPInvoice.Accounts__c !!==' + objDPInvoice.Accounts__c);
            
            String emailTemplateName;Process_Email_Template__mdt emt;EmailTemplate emailTemplateObj;
            String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';String subject, strAccountId, strDPIId, bccAddress ='';
            
            

            emailTemplateObj = [SELECT 
                                ID, Subject, Body, HtmlValue, TemplateType
                            FROM 
                                EmailTemplate 
                            WHERE 
                                DeveloperName =: TemplateName limit 1];
            
            
            strAccountId = objDPInvoice.Accounts__c != null ? String.valueOf(objDPInvoice.Accounts__c ) : objDPInvoice.BookingUnits__r.Booking__r.Account__c;
            
            strDPIId = String.valueOf(objDPInvoice.id);
            bccAddress = Label.Sf_Copy_Bcc_Mail_Id; //SF_Copy_EMail_Address;
            //System.debug('bccAddress !!==' + bccAddress);

            // If Person Acount 
            if(objDPInvoice.Accounts__r.recordTypeId == personRecTypeId && objDPInvoice.Accounts__r.Email__pc != null) {
                 toAddress = objDPInvoice.Accounts__r.Email__pc;
            }

            //If business account
            else if(objDPInvoice.Accounts__r.recordTypeId == businessRecTypeId && objDPInvoice.Accounts__r.Email__c != null){  
                toAddress = objDPInvoice.Accounts__r.Email__c;
            }
            System.debug('toAddress = ' + toAddress);
            fromAddress = Label.AT_Your_Service;
            contentType = 'text/html';
            replyToAddress = Label.AT_Your_Service;
            
            String strCCAddress = '';
           // System.debug('emailTemplateObj = ' + emailTemplateObj);       
            if(toAddress != '' && emailTemplateObj != null) {

                Subject = emailTemplateObj.subject != null ? emailTemplateObj.subject : 'Payment Reminder by Damac ';
                System.debug('SubjectL:::'+Subject);
                
                
                System.debug('after replacement SubjectL:::'+Subject);
                if(emailTemplateObj.body != NULL){
                    contentBody = emailTemplateObj.body;
                }
                if(emailTemplateObj.htmlValue != NULL){
                    contentValue = emailTemplateObj.htmlValue;
                }    
                if(string.isblank(contentValue)){
                    contentValue='No HTML Body';
                }   
                

                 // TODO  add code fetch Attachment related to email template 
                
                
                
                contentValue = (String.isNotBlank(objDPInvoice.Accounts__r.Name)) ?contentValue.replace('{Customer Name}', GenericUtility.getCamelCase(objDPInvoice.Accounts__r.Name)): '';
                contentBody = (String.isNotBlank(objDPInvoice.Accounts__r.Name)) ?contentBody.replace('{Customer Name}', GenericUtility.getCamelCase(objDPInvoice.Accounts__r.Name)): '';
                /**
                 * {Expected percentage}
                {Current percentage}
                {Property}
                {Customer Name}
                {Due Date}
                
                {Current Level}
                {Expected Level}
                 * Property_Name__c
                 * Current_Level__c
                    Future_Level__c
                    Current_percentage__c
                    Expected_percentage__c
                
                 */
                
                contentValue =(String.isNotBlank(objDPInvoice.Unit_Name__c)) ? contentValue.replace('{Property}',String.valueOf(objDPInvoice.Unit_Name__c) +' In '+String.valueOf(objDPInvoice.Property_Name__c)) :contentValue.replace('{Property}', '');
                contentBody = (String.isNotBlank(objDPInvoice.Unit_Name__c)) ?contentBody.replace('{Property}', String.valueOf(objDPInvoice.Unit_Name__c) +' In '+String.valueOf(objDPInvoice.Property_Name__c)) : contentBody.replace('{Property}','');
                
                contentValue = (String.isNotBlank(String.valueOf(objDPInvoice.FinalDate__c)))?contentValue.replace('{Due Date}',String.valueOf(objDPInvoice.FinalDate__c)):contentValue.replace('{Due Date}', '');
                contentBody = (String.isNotBlank(String.valueOf(objDPInvoice.FinalDate__c)))?contentBody.replace('{Due Date}', String.valueOf(objDPInvoice.FinalDate__c)): contentBody.replace('{Due Date}','');
            
                contentValue = (String.isNotBlank(String.valueOf(objDPInvoice.ReraPersentage__c )))?contentValue.replace('{Current percentage}',String.valueOf(objDPInvoice.ReraPersentage__c ) +'%'): contentValue.replace('{Current percentage}','');
                contentBody = (String.isNotBlank(String.valueOf(objDPInvoice.ReraPersentage__c )))?contentBody.replace('{Current percentage}', String.valueOf(objDPInvoice.ReraPersentage__c ) +'%'):contentBody.replace('{Current percentage}', '');
                
                contentValue = (String.isNotBlank(String.valueOf(objDPInvoice.Current_percentage__c)))?contentValue.replace('{Current percentage}',String.valueOf(objDPInvoice.Current_percentage__c) +'%'): contentValue.replace('{Current percentage}', '');
                contentBody = (String.isNotBlank(String.valueOf(objDPInvoice.Current_percentage__c)))?contentBody.replace('{Current percentage}', String.valueOf(objDPInvoice.Current_percentage__c) +'%'): contentBody.replace('{Current percentage}','');
            
                contentValue = (String.isNotBlank(String.valueOf(objDPInvoice.Expected_percentage__c)))?contentValue.replace('{Expected percentage}',String.valueOf(objDPInvoice.Expected_percentage__c) +'%'):contentValue.replace('{Expected percentage}', '');
                contentBody = (String.isNotBlank(String.valueOf(objDPInvoice.Expected_percentage__c)))?contentBody.replace('{Expected percentage}', String.valueOf(objDPInvoice.Expected_percentage__c) +'%'): contentBody.replace('{Expected percentage}','');
                
                contentValue = (String.isNotBlank(String.valueOf(objDPInvoice.Current_Level__c)))?contentValue.replace('{Current Level}',String.valueOf(objDPInvoice.Current_Level__c)):contentValue.replace('{Current Level}', '');
                contentBody = (String.isNotBlank(String.valueOf(objDPInvoice.Current_Level__c)))?contentBody.replace('{Current Level}', String.valueOf(objDPInvoice.Current_Level__c)):contentBody.replace('{Current Level}','');
            
                contentValue = (String.isNotBlank(String.valueOf(objDPInvoice.Future_Level__c)))?contentValue.replace('{Expected Level}',String.valueOf(objDPInvoice.Future_Level__c)): contentValue.replace('{Expected Level}','');
                contentBody = (String.isNotBlank(String.valueOf(objDPInvoice.Future_Level__c)))?contentBody.replace('{Expected Level}', String.valueOf(objDPInvoice.Future_Level__c)): contentBody.replace('{Expected Level}','');
                
               
                //System.debug('contentValue = ' + contentValue);
                // Callout to sendgrid to send an email
                SendGridEmailService.SendGridResponse objSendGridResponse =  
                    SendGridEmailService.sendEmailService(toAddress,
                    '', 
                    strCCAddress, 
                    '', 
                    bccAddress, 
                    '', 
                    subject,
                    '',
                    fromAddress,
                    '',
                    replyToAddress,
                    '',
                    contentType,
                    contentValue,
                    '',
                    attachmentUrls
                );
                system.debug('toAddress === ' + toAddress);
                system.debug('fromAddress === ' + fromAddress);
                system.debug('subject === ' + subject);
                //system.debug('contentBody === ' + contentBody);
                //system.debug('contentType === ' + contentType);
                system.debug('objSendGridResponse === ' + objSendGridResponse);
                String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                if( responseStatus == 'Accepted' ) {
                    EmailMessage mail = new EmailMessage();
                    mail.Subject = subject;
                    mail.MessageDate = System.Today();
                    mail.Status = '3';//'Sent';
                    mail.RelatedToId = strAccountId;
                    mail.Account__c  = strAccountId;
                    mail.Type__c = 'FM Invoice';
                    mail.ToAddress = toAddress;
                    mail.FromAddress = fromAddress;
                    mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                    mail.Sent_By_Sendgrid__c = true;
                    mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                    mail.DP_Invoices__c = strDPIId;
                    mail.CcAddress = strCCAddress;
                    mail.BccAddress = bccAddress;
                     
                    lstEmails.add(mail);

                   
                 
                    system.debug('Mail obj == ' + mail);

                    objDPInvoice.Dunning_60_Email_Sent__c = true;
                    lstInvoicesTUpdate.add(objDPInvoice);
                }
            }
        }
    }
    
   
    
    
  
}