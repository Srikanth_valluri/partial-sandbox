@isTest
public class UpdateSignNowTemplateSettingsTest {
	@isTest
    public static void updateRecipientTest(){
        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        objAcc.Mobile__c ='1234567890';
        insert objAcc;
                
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'DEA/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        insert objBookingUnit;
    
        Case objCase = new Case();
        objCase.Status='New';
        objCase.Origin='Call';
        objCase.Priority='Medium';
        objCase.RecordTypeId = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        objCase.Booking_Unit__c = objBookingUnit.Id;
        insert objCase;
        
        List<Buyer__c> lstBuyer = TestDataFactory_CRM.createBuyer(objBooking.Id, 1, objAcc.Id);
        lstBuyer[0].Case__c = objCase.Id;
        lstBuyer[0].Booking__c = objBooking.Id;
        insert lstBuyer;
        
        UpdateSignNowTemplateSettings objSignNowT = new UpdateSignNowTemplateSettings(new ApexPages.StandardController(objCase));
    	System.Test.startTest();
        objSignNowT.updateRecipients();
        System.Test.stopTest();
        System.assertEquals(1, [Select count() from cuda_signnow__CN_TemplateSetting__c]);
    }
}