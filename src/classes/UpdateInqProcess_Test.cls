@isTest
private class UpdateInqProcess_Test {

    static testMethod void myUnitTest() {
        List<NSIBPM__SR_Template__c> createSrTemplateList = InitialiseTestData.createTestTemplateRecords(
            new List<NSIBPM__SR_Template__c>{
                new NSIBPM__SR_Template__c()});
        List<NSIBPM__SR_Status__c> createSrStatusList = InitialiseTestData.createSrStatusRecords(
            new List<NSIBPM__SR_Status__c>{
                new NSIBPM__SR_Status__c(Name = 'Draft', NSIBPM__Code__c = 'Draft'), 
                new NSIBPM__SR_Status__c(Name = 'Submitted', NSIBPM__Code__c = 'Submitted')});
        List<NSIBPM__Service_Request__c> createServiceRequestList = 
            InitialiseTestData.createtestServiceRequestRecords(
                new List<NSIBPM__Service_Request__c>{
                    new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Deal'),
                                                   NSIBPM__SR_Template__c = createSrTemplateList[0].Id)});
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/UpdateInquiry'; //Request URL
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof('{"srId" : "'+createServiceRequestList[0].id+'"}');

        RestContext.request = req;
        RestContext.response= res;
        UpdateInqProcess.doPost ();
        
    }
}