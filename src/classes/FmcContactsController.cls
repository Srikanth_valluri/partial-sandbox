public without sharing class FmcContactsController {

    public List<LocationContacts> locationContacts {get; set;}

    public String photoUrl {get; set;}

    public FmcContactsController() {
        if (FmcUtils.isCurrentView('Contacts')) {
            locationContacts = fetchContacts(CustomerCommunityUtils.customerAccountId);
            photoUrl = [SELECT Id, SmallPhotoUrl FROM User WHERE Id = :UserInfo.getUserId()].SmallPhotoUrl;
        }
    }

    public static List<LocationContacts> fetchContacts(Id accountId) {
        Set<Id> locationIds = new Set<Id>();
        List<LocationContacts> lstLocationContacts = new List<LocationContacts>();

        for (Booking_Unit__c unit : FmcUtils.queryUnitsForAccount(
                accountId,
                'Id, Name, Unit_Name__c, Inventory__c, Inventory__r.Building_Location__c, '
                + 'Inventory__r.Building_Location__r.Id'
        )) {
            if (unit.Inventory__c != NULL && unit.Inventory__r.Building_Location__c != NULL) {
                locationIds.add(unit.Inventory__r.Building_Location__c);
            }
        }

        for (Location__c location : [
            SELECT      Id
                        , Name
                        , Loams_Email__c
                        , Building_Name__c
                        , GeoLocation__latitude__s
                        , GeoLocation__longitude__s
                        , ( SELECT  Id
                                    , Building__c
                                    , FM_Role__c
                                    , Is_Emergency_Contact__c
                                    , Day_Shift_Contact_No__c
                                    , Night_Shift_Contact_No__c
                                    , FM_User__c
                                    , FM_User__r.Id
                                    , FM_User__r.FirstName
                                    , FM_User__r.LastName
                                    , FM_User__r.Name
                                    , FM_User__r.Title
                                    , FM_User__r.SmallPhotoUrl
                                    , FM_User__r.FullPhotoUrl
                            FROM    FM_Users__r
                            WHERE   FM_Role__c IN ('FM Admin', 'Property Manager'))
            FROM        Location__c
            WHERE       Id IN :locationIds
            ORDER BY    Name, Building_Name__c
        ]) {
            LocationContacts locationContacts = new LocationContacts();
            locationContacts.location = location;
            locationContacts.buildingContacts = new List<FM_User__c>();
            locationContacts.emergencyContacts = new List<FM_User__c>();
            for (FM_User__c contact : location.FM_Users__r) {
                if (contact.Is_Emergency_Contact__c) {
                    locationContacts.emergencyContacts.add(contact);
                } else {
                    locationContacts.buildingContacts.add(contact);
                }
            }
            //if ((location.GeoLocation__latitude__s != NULL && location.GeoLocation__longitude__s != NULL)
            //    || !location.FM_Users__r.isEmpty()
            //) {
                lstLocationContacts.add(locationContacts);
            //}
        }
        return lstLocationContacts;
    }

    @RemoteAction
    public static GoogleNearbyPlaces.Response getNearbyPlaces(Id locationId) {
        if (locationId == NULL) {
            return NULL;
        }

        List<Location__c> lstLocation = [
            SELECT  Id
                    , GeoLocation__latitude__s
                    , GeoLocation__longitude__s
            FROM    Location__c
            WHERE   Id = :locationId
        ];
        if (lstLocation.isEmpty()
            || lstLocation[0].GeoLocation__latitude__s == NULL || lstLocation[0].GeoLocation__longitude__s == NULL
        ) {
            return NULL;
        }

        return GoogleNearbyPlaces.getNearbyPlaces(new GoogleNearbyPlaces.Request(
            lstLocation[0].GeoLocation__latitude__s, lstLocation[0].GeoLocation__longitude__s
        ));
    }

    @RemoteAction
    public static GoogleNearbyPlaces.Place getPlaceDetails(String placeId) {
        if (String.isBlank(placeId)) {
            return NULL;
        }

        return GoogleNearbyPlaces.getPlaceDetails(placeId);
    }

    public class LocationContacts {
        public Location__c location {get; set;}
        //public List<Booking_Unit__c> units {get; set;}
        public List<FM_User__c> buildingContacts {get; set;}
        public List<FM_User__c> emergencyContacts {get; set;}
    }

}