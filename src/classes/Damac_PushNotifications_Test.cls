@isTest
private class Damac_PushNotifications_Test{
    static testMethod void createNotification(){
        
        DAMAC_Central_Push_Notifications__c pn = new DAMAC_Central_Push_Notifications__c();
        pn.API_URL__c = 'test';
        pn.API_Token__c = 'test';
        pn.SetupOwnerId=UserInfo.getOrganizationId();
        pn.Email__c = 'c@k.com';
        pn.password__c = 'c@k.com';
        pn.Device_Source__c = 'etst';
        pn.Device_os_version__c = 'test';
        pn.App_Version__c = 'test';
        pn.App_ID__c = 2;
        pn.Device_ModeL__c = 'test';
        pn.is_authorization_required__c = false;
        pn.Project_connect_API_Token__c = 'test';
        insert pn;

        Account acc = new Account ();
        acc.Agency_Type__c = 'Corporate';
        acc.LastName = 'Test Account';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Agency').getRecordTypeId();
        acc.Blacklisted__c = false;
        acc.Terminated__c = false;
        acc.Vendor_ID__c = '999988887777';
        insert acc;

        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        loc.property_id__c = '123';
        insert loc;

        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c = 1;
        newProperty.Property_Code__c = 'VIR';
        newProperty.Property_Name__c = 'VIRIDIS @ AKOYA OXYGEN';
        newProperty.District__c = 'AL YUFRAH 2';
        newProperty.AR_Transaction_Type__c = 'INV VIR';
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR';
        newProperty.Brokerage_Distribution_Set__c = '11600';
        newProperty.Sales_Commission_Dist_Set__c = '11601';
        newProperty.Currency_Of_Sale__c = 'AED';
        newProperty.Signature_Col_Customer_Stmt__c = 'Front Line Investment Management Co. LLC';
        newProperty.EOI_Enabled__c = true;
        insert newProperty;

        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.Id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.Id;
        lstInv[0].building_location__c = loc.Id;
        lstInv[0].property_id__c = newProperty.Id;
        insert lstInv;
        
        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today(); 
        pp.Building_Location__c =  loc.Id;
        pp.term_id__c = '1234';
        insert pp;
        Payment_Terms__c pt = new Payment_Terms__c();
        pt.Payment_Plan__c = pp.Id;
        pt.Percent_Value__c = '5';
        insert pt;
        
        Agency_PC__c agencyPC = new Agency_PC__c();
        agencyPC.user__c = userinfo.getUserId();
        agencyPC.Agency__c = acc.id;
        insert agencyPC;
       
        
        DealExceptionRequest_Remote.getAgencyDetails ('test');        
        DealExceptionRequest_Remote.getCorporateAgents (acc.id);
        DealExceptionRequest_Remote.getInquiryDetails ('test');
        DealExceptionRequest_Remote.getAccountDetails (acc.LastName);
        String derId = DealExceptionRequest_Remote.createDealExpRequest(lstInv[0].Id);




        Push_Notifications__c notification = new Push_Notifications__c();
        notification.Email__c = 'damac@testdamac.com';
        notification.Template_Id__c = 0;
        notification.Message__c = 'test';
        notification.Title__c = 'test';
        notification.detail_type__c = 'test';
        notification.Source_Application_Id__c = 10;
        notification.Dest_Application_Id__c = 6;
        notification.Deal_Exception_Request__c = derId;
        insert notification;
    }
}