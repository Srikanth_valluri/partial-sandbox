/*-------------------------------------------------------------------------------------------------
Description: Test Class for DocumentComparisonRestService
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 09-04-2018       | Lochan Karle     | 1. Added logic to test DocumentComparisonRestService functionality
=============================================================================================================================
*/
@isTest
private class DocumentComparisonRestServiceTest{
	@isTest
	static void itShould(){
		Case objCase = new Case();
        objCase.Status = 'NotSubmitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';  
        insert objCase;

		list<DocumentComparisonRestService.pages> lstPages = new list<DocumentComparisonRestService.pages>();
		DocumentComparisonRestService.pages objPage = new DocumentComparisonRestService.pages();
		objPage.accuracy = 2;
		objPage.addChar = 2;
		objPage.delChar = 2;
		objPage.originalPageNumber = 5;
		objPage.scannedPageNumber = 5;
		objPage.totalChar = 20;
		lstPages.add(objPage);
		
		DocumentComparisonRestService.DocComparison objDocCom = new DocumentComparisonRestService.DocComparison();
		objDocCom.accuracy = 2;
		objDocCom.addChar = 2;
		objDocCom.deleteChar = 2;
		objDocCom.diffReport = 'test';
		objDocCom.pageAccuracy = '5';
		objDocCom.totalChar = 20;
		objDocCom.pages = lstPages;

		Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"accuracy":0.0,"addChar":0,"deleteChar":0,"diffReport":null,"pageAccuracy":null,"pages":[],"totalChar":0}',
                                                 null);
		Test.setMock(HttpCalloutMock.class, fakeResponse);
			DocumentComparisonRestService objClass = new DocumentComparisonRestService();
			DocumentComparisonRestService.compareDocs('https://sftest.deeprootsurface.com/docs/t/IPMS-1031899-POAFile.pdf','https://sftest.deeprootsurface.com/docs/t/IPMS-79042-39839721_75740_CRF.pdf',objCase.id);
		Test.stopTest();
	}
}