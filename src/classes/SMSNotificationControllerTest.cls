@isTest
public with sharing class SMSNotificationControllerTest{
    @isTest static void searchInquiriesTest0(){
    
        SMSNotificationController smsNotificationObject=new SMSNotificationController();
        ReportFinderUtil obj = new ReportFinderUtil ();
        tsLeadsReassignment obj1 = new tsLeadsReassignment ();
        tsLeadsReassignment1 obj2 = new tsLeadsReassignment1 ();
        String sch = '0 0 0 * * ?'; 
        system.schedule('Test Inquiry Score Calculator Check 1', sch, obj1); 
        system.schedule('Test Inquiry Score Calculator Check 2', sch, obj2); 
        DatePickerController obj3 = new DatePickerController();        
        
    }
    
     @isTest static void searchInquiriesTest(){
        Id accRecordTypeId   = Schema.SObjectType.Account.getRecordTypeInfosByName().get(
                                'Corporate Agency').getRecordTypeId();
        List<String> statusVal = new List<String>();
        List<String> ownerVal = new List<String>();
        List<String> accRecType = new List<String>();
        ownerVal.add(UserInfo.getFirstName() + ' ' + UserInfo.getLastName());
        accRecType.add(String.valueOf(accRecordTypeId));
        statusVal.add('New');
        String digitalRecordType=DamacUtility.getRecordTypeId('Campaign__c',
                                                               'Digital');
        Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(
                        'Agent Team').getRecordTypeId();
        Campaign__c campaignObject = new Campaign__c(RecordTypeId = digitalRecordType,
                                                     Campaign_Type_New__c = 'Social',
                                                     Campaign_Category_New__c = 'Facebook',
                                                     Campaign_Sub_Category_1__c ='Organic',
                                                     End_Date__c = date.parse('11/10/18'),
                                                     Marketing_End_Date__c = date.parse('11/10/18'),
                                                     Marketing_Start_Date__c = date.parse('11/10/16'),
                                                     Start_Date__c = date.parse('11/10/16'),
                                                     Lead_Prioritization__c = 'General Digital'
                                                     );
                                                     
        insert campaignObject;
        Inquiry__c inqObj= new Inquiry__c(RecordTypeId = agenTeamRT,
                                          Validity__c ='Valid',
                                          Reachable__c = 'Reachable',
                                          Inquiry_Source__c ='Agent Referral',
                                          Inquiry_Status__c = 'New',
                                          Mobile_Phone_Encrypt__c ='456123',
                                          Mobile_CountryCode__c ='American Samoa: 001684',
                                          Mobile_Phone__c = '1234',
                                          Email__c ='mk@gmail.com',
                                          First_Name__c ='Test',
                                          Last_Name__c ='Last',
                                          CR_Number__c ='0987',
                                          ORN_Number__c ='7842',
                                          Agency_Type__c ='Corporate',
                                          Organisation_Name__c = 'Oliver',
                                          isDuplicate__c = false);
        insert inqObj;  
        Account acc = InitialiseTestData.getCorporateAccount('Test Agency11');
        acc.Country__c = 'United Arab Emirates';
        acc.City__c = 'Abu Dhabi';   
        insert acc ;

        Campaign_SMS__c campSMS = new Campaign_SMS__c();
        campSMS.SMS_Message__c = 'test Text';
        campSMS.Scheduled_Date_Time__c = DateTime.Now();
        campSMS.User__c = UserInfo.getUserId();
        insert campSMS;

        SMSNotificationController smsNotificationObject=new SMSNotificationController();
        SMSNotificationController.agentWrappper  wrp = new SMSNotificationController.agentWrappper();
        Test.startTest();
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new MockSMSResponseGenerator());
        Id camId = campaignObject.Id;
         wrp.recordId = acc.id;
         wrp.recordName = acc.Name;
        smsNotificationObject.selTier = 'Yes';
        smsNotificationObject.accountObj.Country__c = 'United Arab Emirates';
        smsNotificationObject.accountObj.City__c = 'Abu Dhabi';
        smsNotificationObject.selCampaign = String.valueOf(camId);
        smsNotificationObject.ownerVal = ownerVal;
        smsNotificationObject.mobileCountryVal = 'India: 0091';
        smsNotificationObject.selAccountRecordType =accRecType;
        smsNotificationObject.selMobileNo ='NONE';
        smsNotificationObject.startDate ='10/1/17';
        smsNotificationObject.endDate = '31/12/18';
        smsNotificationObject.inquiryStatusVal = statusVal;
        smsNotificationObject.searchInquiries();
        smsNotificationObject.searchAgentAccount();
        smsNotificationObject.resetInquiries();
        smsNotificationObject.resetAccount();
        DatePickerController dateObj = new DatePickerController();
        dateObj.setTargetDate(null);

        Async_Rest_SMS asynJob = new Async_Rest_SMS(campSMS.id,
                                                    new Set<String>{'00971561422390'},
                                                    'Test text',
                                                    'Account');
          ID jobID = System.enqueueJob(asynJob);
        Test.stopTest();
    }
    @isTest static void searchInquiriesTest1(){
        Id accRecordTypeId   = Schema.SObjectType.Account.getRecordTypeInfosByName().get(
                                'Corporate Agency').getRecordTypeId();
        List<String> statusVal = new List<String>();
         List<String> accRecType = new List<String>();
        List<String> contactRole = new List<String>();
        contactRole.add('Owner');
        contactRole.add('Authorised Signatory');
        contactRole.add('Portal Administrator');
        contactRole.add('Agent Representative');
        accRecType.add(String.valueOf(accRecordTypeId));
        statusVal.add('New');
        String digitalRecordType=DamacUtility.getRecordTypeId('Campaign__c',
                                                               'Digital');
        Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(
                        'Agent Team').getRecordTypeId();
        Campaign__c campaignObject = new Campaign__c(RecordTypeId = digitalRecordType,
                                                     Campaign_Type_New__c = 'Social',
                                                     Campaign_Category_New__c = 'Facebook',
                                                     Campaign_Sub_Category_1__c = 'Organic',
                                                     End_Date__c = date.parse('11/10/18'),
                                                     Marketing_End_Date__c = date.parse('11/10/18'),
                                                     Marketing_Start_Date__c = date.parse('11/10/16'),
                                                     Start_Date__c = date.parse('11/10/16'),
                                                     Lead_Prioritization__c = 'General Digital');
        insert campaignObject;
        Inquiry__c inqObj= new Inquiry__c(RecordTypeId = agenTeamRT,
                                          Inquiry_Status__c = 'New',
                                          Validity__c = 'Valid',
                                          Reachable__c = 'Reachable',
                                          Inquiry_Source__c = 'Agent Referral',
                                          Mobile_Phone_Encrypt__c = '456123',
                                          Mobile_CountryCode__c = 'American Samoa: 001684',
                                          Mobile_Phone__c = '1234',
                                          Email__c = 'mk@gmail.com',
                                          First_Name__c = 'Test',
                                          Last_Name__c = 'Last',
                                          CR_Number__c = '0987',
                                          ORN_Number__c = '7842',
                                          Agency_Type__c = 'Corporate',
                                          Organisation_Name__c = 'Oliver',
                                          isDuplicate__c = false);
        insert inqObj; 
        Inquiry__c inqObjNew= new Inquiry__c(Campaign__c=campaignObject.Id,
                                             RecordTypeId=agenTeamRT,
                                             Inquiry_Source__c='Agent Referral',
                                             Validity__c='Valid',
                                             Reachable__c = 'Reachable',
                                             Mobile_Phone_Encrypt__c='456126553',
                                             Mobile_CountryCode__c='American Samoa: 001684',
                                             Mobile_Phone__c='1234',
                                             Email__c='mk12@gmail.com',
                                             First_Name__c='Test12',
                                             Last_Name__c='Last12',
                                             CR_Number__c='0987',
                                             ORN_Number__c='7842',
                                             Agency_Type__c='Corporate',
                                             Organisation_Name__c = 'Oliver',
                                             isDuplicate__c=false);
        insert inqObjNew;
        Account acc = InitialiseTestData.getCorporateAccount('Test Agency11');
        insert acc ;
        SMSNotificationController smsNotificationObject=new SMSNotificationController();
        Test.startTest();
        smsNotificationObject.selAccountRecordType =accRecType;
        smsNotificationObject.selTier = 'No';
        smsNotificationObject.newSMSRequest.sms_Message__c = 'ABC XYZ';
        smsNotificationObject.contactRole =contactRole;
        smsNotificationObject.accountSearchCount = 2;
        smsNotificationObject.scheduleDate = '1/1/19';
        smsNotificationObject.selTime = '01:00 AM';
        smsNotificationObject.selMobileNo ='Contacts';
        smsNotificationObject.startDate ='10/1/17';
        smsNotificationObject.endDate = '31/12/18';
        smsNotificationObject.inquiryStatusVal = statusVal;
        smsNotificationObject.searchInquiries();
        smsNotificationObject.searchAgentAccount();
        smsNotificationObject.resetInquiries();
        smsNotificationObject.resetAccount();
        smsNotificationObject.accountSearchCount = 2;
        smsNotificationObject.submitSMSForApproval();
        Test.stopTest();
    }
    @isTest static void searchInquiriesTest2(){
        Id accRecordTypeId   = Schema.SObjectType.Account.getRecordTypeInfosByName().get(
                                'Corporate Agency').getRecordTypeId();
        List<String> statusVal = new List<String>();
         List<String> accRecType = new List<String>();
        List<String> contactRole = new List<String>();
        contactRole.add('Owner');
        contactRole.add('Authorised Signatory');
        contactRole.add('Portal Administrator');
        contactRole.add('Agent Representative');
        accRecType.add(String.valueOf(accRecordTypeId));
        statusVal.add('New');
        String digitalRecordType=DamacUtility.getRecordTypeId('Campaign__c',
                                                               'Digital');
        Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(
                        'Agent Team').getRecordTypeId();
        Campaign__c campaignObject = new Campaign__c(RecordTypeId=digitalRecordType,
                                                     Campaign_Type_New__c='Social',
                                                     Campaign_Category_New__c='Facebook',
                                                     Campaign_Sub_Category_1__c='Organic',
                                                     End_Date__c=date.parse('11/10/18'),
                                                     Marketing_End_Date__c=date.parse('11/10/18'),
                                                     Marketing_Start_Date__c=date.parse('11/10/16'),
                                                     Start_Date__c=date.parse('11/10/16'),
                                                     Lead_Prioritization__c = 'General Digital');
        insert campaignObject;
        Inquiry__c inqObj= new Inquiry__c(RecordTypeId=agenTeamRT,
                                          Inquiry_Status__c='New',
                                          Validity__c='Valid',
                                          Reachable__c = 'Reachable',
                                          Inquiry_Source__c='Agent Referral',
                                          Mobile_Phone_Encrypt__c='456123',
                                          Mobile_CountryCode__c='American Samoa: 001684',
                                          Mobile_Phone__c='1234',
                                          Email__c='mk@gmail.com',
                                          First_Name__c='Test',
                                          Last_Name__c='Last',
                                          CR_Number__c='0987',
                                          ORN_Number__c='7842',
                                          Agency_Type__c='Corporate',
                                          Organisation_Name__c = 'Oliver',
                                          isDuplicate__c=false);
        insert inqObj; 
        Inquiry__c inqObjNew= new Inquiry__c(Campaign__c=campaignObject.Id,
                                             RecordTypeId=agenTeamRT,
                                             Inquiry_Source__c='Agent Referral',
                                             Validity__c='Valid',
                                             Reachable__c = 'Reachable',
                                             Mobile_Phone_Encrypt__c='456126553',
                                             Mobile_CountryCode__c='American Samoa: 001684',
                                             Mobile_Phone__c='1234',
                                             Email__c='mk12@gmail.com',
                                             First_Name__c='Test12',
                                             Last_Name__c='Last12',
                                             CR_Number__c='0987',
                                             ORN_Number__c='7842',
                                             Agency_Type__c='Corporate',
                                             Organisation_Name__c = 'Oliver',
                                             isDuplicate__c=false);
        insert inqObjNew;
        Account acc = InitialiseTestData.getCorporateAccount('Test Agency11');
        insert acc ;
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt',
                          Email='stanfdarduser@testorg.com',
                          EmailEncodingKey='UTF-8',
                          LastName='Testing',
                          LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US',
                          ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName='stafgfdarduser@testorg.com');
        insert u;

        SMSNotificationController smsNotificationObject=new SMSNotificationController();
        Test.startTest();
        smsNotificationObject.selAccountRecordType =accRecType;
         Campaign_sms__c smsObj = new Campaign_sms__c(Approval_Status__c = 'Approved',
                                                      SMS_Message__c= 'dsaf',
                                                      User__c = u.Id,
                                                     Total_Inquiries_in_the_Date_Range__c=1,
                                                     Sobject_Query__c = 'SELECT Id,Name,'+
                                                     'Agency_Mobile_combined__c FROM Account'+
                                                     'WHERE RecordTypeId != null AND'+
                                                     'Agency_Mobile__c != null  AND RecordTypeId IN ' +
                                                      accRecType + ' AND Owner.Name IN ( ' +
                                                      UserInfo.getFirstName() + ' ' +
                                                      UserInfo.getLastName() +
                                                      ' ) AND Agency_Mobile_combined__c != null');
        insert smsObj;
        smsNotificationObject.CampsmsId = smsObj.Id; 
        smsNotificationObject.selTier = 'NONE';
        smsNotificationObject.newSMSRequest.sms_Message__c = 'ABC XYZ';
        smsNotificationObject.contactRole =contactRole;
        smsNotificationObject.accountSearchCount = 2;
        smsNotificationObject.scheduleDate = '1/1/19';
        smsNotificationObject.selTime = '12:00 AM';
        smsNotificationObject.selMobileNo ='Contacts';
        smsNotificationObject.startDate ='10/1/17';
        smsNotificationObject.endDate = '31/12/18';
        smsNotificationObject.inquiryStatusVal = statusVal;
       // smsNotificationObject.callSMSJob();
        smsNotificationObject.searchInquiries();
        smsNotificationObject.searchAgentAccount();
        smsNotificationObject.resetInquiries();
        smsNotificationObject.resetAccount();
        smsNotificationObject.accountSearchCount = 2;
        smsNotificationObject.getMobileCountryList();
        smsNotificationObject.getInquiryStatusList();
        smsNotificationObject.submitSMSForApproval();
        smsNotificationObject.getAccountRecordType();
        smsNotificationObject.getOwnerList();
        Test.stopTest();
    }
     @isTest static void searchInquiriesTest3(){
        Id accRecordTypeId   = Schema.SObjectType.Account.getRecordTypeInfosByName().get(
                                'Corporate Agency').getRecordTypeId();
        List<String> statusVal = new List<String>();
         List<String> accRecType = new List<String>();
        accRecType.add(String.valueOf(accRecordTypeId));
        statusVal.add('New');
        String digitalRecordType=DamacUtility.getRecordTypeId('Campaign__c',
                                                               'Digital');
        Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(
                        'Agent Team').getRecordTypeId();
        Campaign__c campaignObject = new Campaign__c(RecordTypeId=digitalRecordType,
                                                     Campaign_Type_New__c='Social',
                                                     Campaign_Category_New__c='Facebook',
                                                     Campaign_Sub_Category_1__c='Organic',
                                                     End_Date__c=date.parse('11/10/18'),
                                                     Marketing_End_Date__c=date.parse('11/10/18'),
                                                     Marketing_Start_Date__c=date.parse('11/10/16'),
                                                     Start_Date__c=date.parse('11/10/16'),
                                                     Lead_Prioritization__c = 'General Digital');
        insert campaignObject;
        Inquiry__c inqObj= new Inquiry__c(RecordTypeId=agenTeamRT,
                                          Validity__c='Valid',
                                          Reachable__c = 'Reachable',
                                          Inquiry_Source__c='Agent Referral',
                                          Inquiry_Status__c = 'New',
                                          Mobile_Phone_Encrypt__c='456123',
                                          Mobile_CountryCode__c='American Samoa: 001684',
                                          Mobile_Phone__c='1234',
                                          Email__c='mk@gmail.com',
                                          First_Name__c='Test',
                                          Last_Name__c='Last',
                                          CR_Number__c='0987',
                                          ORN_Number__c='7842',
                                          Agency_Type__c='Corporate',
                                          Organisation_Name__c = 'Oliver',
                                          isDuplicate__c=false);
        insert inqObj;
        Account acc = InitialiseTestData.getCorporateAccount('Test Agency11');
        insert acc ;
        SMSNotificationController smsNotificationObject=new SMSNotificationController();
        Test.startTest();
        smsNotificationObject.selAccountRecordType =accRecType;
        smsNotificationObject.selMobileNo ='NONE';
        smsNotificationObject.startDate ='10/1/17';
        smsNotificationObject.endDate = '31/12/18';
        smsNotificationObject.inquiryStatusVal = statusVal;
        smsNotificationObject.searchInquiries();
        smsNotificationObject.searchAgentAccount();
        smsNotificationObject.resetInquiries();
        smsNotificationObject.resetAccount();
        smsNotificationObject.selMobileNo ='Contacts';
        smsNotificationObject.searchAgentAccount();
        Test.stopTest();
    }
    @isTest static void searchInquiriesTest4(){
        Id accRecordTypeId   = Schema.SObjectType.Account.getRecordTypeInfosByName().get(
                                'Corporate Agency').getRecordTypeId();
        List<String> statusVal = new List<String>();
        List<String> ownerVal = new List<String>();
        List<String> accRecType = new List<String>();
        ownerVal.add(UserInfo.getFirstName() + ' ' + UserInfo.getLastName());
        accRecType.add(String.valueOf(accRecordTypeId));
        statusVal.add('New');
        String digitalRecordType=DamacUtility.getRecordTypeId('Campaign__c',
                                                               'Digital');
        Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(
                        'Agent Team').getRecordTypeId();
        Campaign__c campaignObject = new Campaign__c(RecordTypeId=digitalRecordType,
                                                     Campaign_Type_New__c='Social',
                                                     Campaign_Category_New__c='Facebook',
                                                     Campaign_Sub_Category_1__c='Organic',
                                                     End_Date__c=date.parse('11/10/18'),
                                                     Marketing_End_Date__c=date.parse('11/10/18'),
                                                     Marketing_Start_Date__c=date.parse('11/10/16'),
                                                     Start_Date__c=date.parse('11/10/16'),
                                                     Lead_Prioritization__c = 'General Digital');
        insert campaignObject;
        Inquiry__c inqObj= new Inquiry__c(RecordTypeId=agenTeamRT,
                                          Validity__c='Valid',
                                          Reachable__c = 'Reachable',
                                          Inquiry_Source__c='Agent Referral',
                                          Inquiry_Status__c = 'New',
                                          Mobile_Phone_Encrypt__c='456123',
                                          Mobile_CountryCode__c='American Samoa: 001684',
                                          Mobile_Phone__c='1234',
                                          Email__c='mk@gmail.com',
                                          First_Name__c='Test',
                                          Last_Name__c='Last',
                                          CR_Number__c='0987',
                                          ORN_Number__c='7842',
                                          Agency_Type__c='Corporate',
                                          Organisation_Name__c = 'Oliver',
                                          isDuplicate__c=false);
        insert inqObj;
        Account acc = InitialiseTestData.getCorporateAccount('Test Agency11');
        acc.Country__c = 'United Arab Emirates';
        acc.City__c = 'Abu Dhabi';
        insert acc ;
        SMSNotificationController smsNotificationObject=new SMSNotificationController();
        SMSNotificationController.agentWrappper  wrp = new SMSNotificationController.agentWrappper();
        Test.startTest();
        Id camId = campaignObject.Id;
         wrp.recordId = acc.id;
         wrp.recordName = acc.Name;
        smsNotificationObject.selTier = 'No';
        smsNotificationObject.accountObj.Country__c = 'United Arab Emirates';
        smsNotificationObject.accountObj.City__c = 'Abu Dhabi';
        smsNotificationObject.selCampaign = String.valueOf(camId);
        smsNotificationObject.ownerVal = ownerVal;
        smsNotificationObject.mobileCountryVal = 'India: 0091';
        smsNotificationObject.selAccountRecordType =accRecType;
        smsNotificationObject.selMobileNo ='NONE';
        smsNotificationObject.startDate ='10/1/17';
        smsNotificationObject.endDate = '31/12/18';
        smsNotificationObject.inquiryStatusVal = statusVal;
        smsNotificationObject.searchInquiries();
        smsNotificationObject.searchAgentAccount();
        smsNotificationObject.resetInquiries();
        smsNotificationObject.resetAccount();
        Test.stopTest();
    }
    @isTest static void searchInquiriesTest5(){
        Id accRecordTypeId   = Schema.SObjectType.Account.getRecordTypeInfosByName().get(
                                'Corporate Agency').getRecordTypeId();
        List<String> statusVal = new List<String>();
        List<String> ownerVal = new List<String>();
        List<String> accRecType = new List<String>();
        ownerVal.add(UserInfo.getFirstName() + ' ' + UserInfo.getLastName());
        accRecType.add(String.valueOf(accRecordTypeId));
        statusVal.add('New');
        String digitalRecordType=DamacUtility.getRecordTypeId('Campaign__c',
                                                               'Digital');
        Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(
                        'Agent Team').getRecordTypeId();
        Campaign__c campaignObject = new Campaign__c(RecordTypeId=digitalRecordType,
                                                     Campaign_Type_New__c='Social',
                                                     Campaign_Category_New__c='Facebook',
                                                     Campaign_Sub_Category_1__c='Organic',
                                                     End_Date__c=date.parse('11/10/18'),
                                                     Marketing_End_Date__c=date.parse('11/10/18'),
                                                     Marketing_Start_Date__c=date.parse('11/10/16'),
                                                     Start_Date__c=date.parse('11/10/16'),
                                                     Lead_Prioritization__c = 'General Digital');
        insert campaignObject;
        Inquiry__c inqObj= new Inquiry__c(RecordTypeId=agenTeamRT,
                                          Validity__c='Valid',
                                          Reachable__c = 'Reachable',
                                          Inquiry_Source__c='Agent Referral',
                                          Inquiry_Status__c = 'New',
                                          Mobile_Phone_Encrypt__c='456123',
                                          Mobile_CountryCode__c='American Samoa: 001684',
                                          Mobile_Phone__c='1234',
                                          Email__c='mk@gmail.com',
                                          First_Name__c='Test',
                                          Last_Name__c='Last',
                                          CR_Number__c='0987',
                                          ORN_Number__c='7842',
                                          Agency_Type__c='Corporate',
                                          Organisation_Name__c = 'Oliver',
                                          isDuplicate__c=false);
        insert inqObj;
        Account acc = InitialiseTestData.getCorporateAccount('Test Agency11');
        acc.Country__c = 'United Arab Emirates';
        acc.City__c = 'Abu Dhabi';
        insert acc;
        SMSNotificationController smsNotificationObject=new SMSNotificationController();
        SMSNotificationController.agentWrappper  wrp = new SMSNotificationController.agentWrappper();
        Test.startTest();
        Id camId = campaignObject.Id;
         wrp.recordId = acc.id;
         wrp.recordName = acc.Name;
        smsNotificationObject.accountObj.Country__c = 'United Arab Emirates';
        smsNotificationObject.accountObj.City__c = 'Abu Dhabi';
        smsNotificationObject.selCampaign = String.valueOf(camId);
        smsNotificationObject.ownerVal = ownerVal;
        smsNotificationObject.mobileCountryVal = 'India: 0091';
        smsNotificationObject.selAccountRecordType =accRecType;
        smsNotificationObject.selMobileNo ='Agency Mobile';
        smsNotificationObject.startDate ='10/1/17';
        smsNotificationObject.endDate = '31/12/18';
        smsNotificationObject.inquiryStatusVal = statusVal;
        smsNotificationObject.searchInquiries();
        smsNotificationObject.searchAgentAccount();
        smsNotificationObject.resetInquiries();
        smsNotificationObject.resetAccount();
        Test.stopTest();
    }
}