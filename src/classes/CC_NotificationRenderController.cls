public class CC_NotificationRenderController {

    @RemoteAction   
    public static List<NotificationService.Notification> getNotifications(NotificationService.Notification request){
        return NotificationService.getNotifications(request);
    }

    @RemoteAction   
    public static list<NotificationService.Notification> markRead(List<NotificationService.Notification> requestList){
        return CaseNotificationService.markRead(requestList);
    }    
    
  
}