public without sharing class FetchAOPTCasesForCallingListController {
    public String callingListId {get;set;}
    public Id callingListRecordTypeId {get;set;}
    public Id caseRecordTypeId {get;set;}
    public List<Calling_List__c>callingList {get;set;}
    public List<Id> accList {get;set;}
    public List<Case>caseList {get;set;}
    public FetchAOPTCasesForCallingListController(ApexPages.StandardController controller) {
        fetchCases();
    }
     /*This Method is used to fetch all the cases of AOPT record type*/
    public void fetchCases(){
        callingList = new List<Calling_List__c>();
        Id bouncedChequeRTId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Bounced Cheque').RecordTypeId;
        accList = new List<Id>();
        caseList = new List<Case>();
        caseRecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('AOPT').RecordTypeId;
        callingListRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
        callingListId = ApexPages.currentPage().getParameters().get('id');
       // callingListId = String.valueOf(callingListId).substring(0, 15);
        System.debug('callingListId:::::::'+callingListId);
        callingList = [SELECT Id,
                            RecordTypeId,
                            Case__c,
                            Name,
                            Account__r.Name,
                            Account__c
                       FROM Calling_List__c
                       WHERE Id = :callingListId
                         ];//AND RecordTypeId = :callingListRecordTypeId
        //Method to display Cases for Bounced Cheque record type
        if( !callingList.isEmpty() && callingList[0].RecordTypeId == bouncedChequeRTId && callingList[0].Case__c != NULL) {
            displayBouncedChequeCases(callingList[0].Case__c);
        }
        else {
            system.debug('******callingList******'+callingList);
            for(Calling_List__c callingInst : callingList){
            	if( callingInst.Account__c != null )
                accList.add(callingInst.Account__c);
            }
            System.debug('accList::::'+accList);
            if(accList != null && accList.isEmpty() == false ){
            	System.debug('accList::inside if '+accList );
    	        caseList = [SELECT Id,
    	                           caseNumber,
    	                           Status,
    	                           OwnerId,
    	                           Owner.FirstName,
    	                           CreatedDate,
    	                           Approval_Status__c,
    	                           RecordType.Name,
    	                           AccountId
    	                      FROM Case
    	                     WHERE AccountId IN :accList
    	                       AND RecordTypeId = :caseRecordTypeId
    	                  ORDER BY CreatedDate 
                             desc
                             limit 950];
    	        System.debug('caseList::************::'+caseList);
            }
            else{
            	system.debug('inside else part');
            }
        }


    }//fetchCases

    /*
     @ Description : Method to get Cases for Bounced Cheque Calling List
	*/
    public void displayBouncedChequeCases(Id caseId) {
        //Get SR_Booking_Unit__c for the caseId
        Map<ID,List<Case>> mapBUId_Cases = new Map<ID,List<Case>>();
        List<String> lstBUIds = new List<String>();
        for(SR_Booking_Unit__c srBU : [SELECT
                                        Booking_Unit__c
                                        FROM SR_Booking_Unit__c
                                        WHERE Case__c =: caseId]) {
            lstBUIds.add(srBU.Booking_Unit__c);
        }
        mapBUId_Cases = openSRUtility.getOpenSR(lstBUIds, String.valueOf(caseId));
        caseList = new List<Case>();
        List<Case> temp_caseList = new List<Case>();
        for(Id BUId : mapBUId_Cases.keySet()) {
            temp_caseList.addAll(mapBUId_Cases.get(BUId));
        }

        //Adding Cases of AOPT record type
        for(Case objCase : temp_caseList) {
            if(objCase.RecordTypeId == caseRecordTypeId) {
                caseList.add(objCase);
            }

        }

    }
}