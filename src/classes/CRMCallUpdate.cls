@RestResource(urlMapping='/CRMAmeyoCallUpdate/*')
global with sharing class CRMCallUpdate{
    
//*******************AKISHOR: Created On: 25 May 2020 for Ameyo to SF integration******************//
//*******************Service will create calllog + CL update (Outcome/Remarks)********************//  
 @HttpPatch
    global static void CRMAmeyoCallUpdate(String CrtObjectId, String ObjectType, String RecordId, 
                                            String Remarks, String CallType
                                            ,String AccountName, String AccountId, String CallURL,
                                            String StartDate, String EndDate, String ACW,String Outcome,
                                            String AHT, String CallingExt, String CallingCRE,String CallingNum, 
                                            String CalledNumber,String IVRResponse, String CustomerFeedback, 
                                            String PreviewTime, String EncriptedNum, String system_disposition, 
                                            String attempt_time, String talk_time) {
                                                
   
        
    // ResponseWrapper objResponseWrapper = new ResponseWrapper();
        System.debug('-->>RecordId: '+ RecordId+ObjectType);         
     List<Call_Log__c> CallLog= new List<Call_Log__c>();  
     List <User> Exn = [Select Id,Extension from User where UserName=: CallingCRE AND IsActive= TRUE];
     
     
     List<Calling_List__c> clLst = new List<Calling_List__c>();
     for( Calling_List__c CLobj :[SELECT Id,Call_Outcome__c
                                    FROM Calling_List__c
                                    WHERE Id=: RecordId AND IsHideFromUI__c = false
                                    ]){//AND OwnerId =: Exn[0].Id AND IsHideFromUI__c = false
                                       // CLobj.Call_Outcome__c = Outcome;
                                        //CLobj.Decrypted_Calling_Number__c = String.isNotBlank(EncriptedNum)?EncrytNumber.decryptNo(EncriptedNum):'';
                                      //  clLst.add(CLobj);
                                    }
                                    
     
        if(RecordId!='' && CallingCRE!='') {
             //User cre = getCreForExtension(CallingExt); 
            Call_Log__c callLogc = new Call_Log__c();
            callLogc.CRE__c = Exn[0].Id;
            callLogc.Calling_Number__c = CallingNum;
            //callLogc.Calling_Number__c = UtilityHelperCls.decryptMobile(EncriptedNum);
            callLogc.Called_Number__c = CalledNumber;
            callLogc.Call_Type__c = CallType;
            callLogc.Start_Time__c = makeDate(StartDate);
            callLogc.End_Time__c = makeDate(EndDate);
            callLogc.AHT_Ameyo__c= AHT;
            callLogc.ACW_Time__c= ACW;
            callLogc.Calling_Number_Encrypted__c= EncriptedNum;
            callLogc.PreviewTime__c= PreviewTime;
            callLogc.Description__c='Ameyo call log';
            callLogc.Call_Recording_URL__c = CallURL;
            callLogc.system_disposition__c=system_disposition;
            callLogc.talk_time__c=talk_time;
            callLogc.CrtObjectId__c=CrtObjectId;
            callLogc.attempt_time__c=attempt_time;
            //
               if( RecordId != '') {
                Id objId = RecordId;
                String ObjName = objId.getsobjecttype().getDescribe().getName();
                if ( String.isNotBlank(ObjName) && ObjName.contains('Account')) {
                    callLogc.Account__c =  objId;
                }else if( String.isNotBlank(ObjName) && ObjName.contains('Calling') ){
                    callLogc.Calling_List__c = objId;
                    Calling_List__c CLoBj = [SELECT Account__c from Calling_List__c WHERE id =: objId LIMIT 1];
                      callLogc.Account__c = CLoBj.Account__c;
                         
                    }
                }
                
                CallLog.add(callLogc);
            }
            //end create call log; 
            // if(clLst != NULL && clLst.size() > 0){
            //    update clLst;
            //}
            
          
            if(CallLog.size() > 0 ) {
                ResponseWrapper objResponseWrapper = new ResponseWrapper();
                try{
                    Database.insert(CallLog);
                    objResponseWrapper.status = 'Call Log Created Sucessfully';
                    objResponseWrapper.statusCode = '200';
                    RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                    Restcontext.response.statusCode = 200;                 
                } catch( Exception ex ) {
                    system.debug( ex.getMessage() );
                    objResponseWrapper.errorMessage = ex.getMessage();
                    objResponseWrapper.statusCode = '400';
                    System.debug('============= response : ' + objResponseWrapper);
                    System.debug('============= response : ' + JSON.serialize(objResponseWrapper));
                    RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                    Restcontext.response.statusCode = 400;
                    System.debug('============= response : ' + JSON.serialize(objResponseWrapper));
                    return;                 
                }   
            }

}   

    public static Datetime makeDate(string vdate){
        
        //string vdate = '2020-07-19T14:57:48+0400';
        
        system.debug(vdate);
        system.debug(vdate.substringBefore('T').substringBefore('+'));
        system.debug(vdate.substringAfter('T').substringBefore('+'));
        String  strDate = vdate.substringBefore('T').substringBefore('+');
        String strTime = vdate.substringAfter('T').substringBefore('+');
        Datetime myDate = datetime.valueOf(strDate +' '+ strTime);
        system.debug(myDate);
        return myDate;
    }

    public class ResponseWrapper {
        public String status;
        public String statusCode;
        public String errorMessage;
       
        public ResponseWrapper() {
            this.status = '';
            this.statusCode = '';
            this.errorMessage = '';
        }
       }
}