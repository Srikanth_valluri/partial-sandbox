@isTest
public class DealExceptionRequest_Remote_TEST {
    static testMethod void DealExceptionRequest_Remote_TEST_M1 () {
        Account acc = new Account ();
        acc.Agency_Type__c = 'Corporate';
        acc.LastName = 'Test Account';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Agency').getRecordTypeId();
        acc.Blacklisted__c = false;
        acc.Terminated__c = false;
        acc.Vendor_ID__c = '999988887777';
        insert acc;

        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        loc.property_id__c = '123';
        insert loc;
        
        DAMAC_Central_Push_Notifications__c obj = new DAMAC_Central_Push_Notifications__c();
        obj.Email__c = 'test@test.com';
        obj.Password__c = '1232112';
        obj.device_source__c = 'test';
        obj.device_os_version__c = 'test';
        obj.app_version__c = '1.2';
        obj.device_model__c = 'test';
        obj.Api_Token__c = 'test';
        obj.Project_connect_API_Token__c = 'test';
        obj.app_id__c = 1;
        obj.is_authorization_required__c = false;
        insert obj;

        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c = 1;
        newProperty.Property_Code__c = 'VIR';
        newProperty.Property_Name__c = 'VIRIDIS @ AKOYA OXYGEN';
        newProperty.District__c = 'AL YUFRAH 2';
        newProperty.AR_Transaction_Type__c = 'INV VIR';
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR';
        newProperty.Brokerage_Distribution_Set__c = '11600';
        newProperty.Sales_Commission_Dist_Set__c = '11601';
        newProperty.Currency_Of_Sale__c = 'AED';
        newProperty.Signature_Col_Customer_Stmt__c = 'Front Line Investment Management Co. LLC';
        newProperty.EOI_Enabled__c = true;
        insert newProperty;

        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.Id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.Id;
        lstInv[0].building_location__c = loc.Id;
        lstInv[0].property_id__c = newProperty.Id;
        insert lstInv;
        
        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today(); 
        pp.Building_Location__c =  loc.Id;
        pp.term_id__c = '1234';
        insert pp;
        Payment_Terms__c pt = new Payment_Terms__c();
        pt.Payment_Plan__c = pp.Id;
        pt.Percent_Value__c = '5';
        insert pt;
        
        Agency_PC__c agencyPC = new Agency_PC__c();
        agencyPC.user__c = userinfo.getUserId();
        agencyPC.Agency__c = acc.id;
        insert agencyPC;
       
        
        DealExceptionRequest_Remote.getAgencyDetails ('test');        
        DealExceptionRequest_Remote.getCorporateAgents (acc.id);
        DealExceptionRequest_Remote.getInquiryDetails ('test');
        DealExceptionRequest_Remote.getAccountDetails (acc.LastName);
        String derId = DealExceptionRequest_Remote.createDealExpRequest(lstInv[0].Id);
      //  String frId = DealExceptionRequest_Remote.createFurnitureRequest(lstInv[0].Id);
        Deal_Exception_Unit__c deUnit = [SELECT Id FROM Deal_Exception_Unit__c where Deal_Exception_Request__c =: derId LIMIT 1];
        DealExceptionRequest_Remote.getPaymentPlanRemote(pp.Id);
        DealExceptionRequest_Remote.createProposedPlan(pp.Id, deUnit.Id);
        String result = DealExceptionRequest_Remote.createpaymentPlanHeader(lstInv[0].Id);
        DealExceptionRequest_Remote.getBookingUnits('BU-');
        DealExceptionRequest_Remote.createFurnitureRequest (lstInv[0].Id);
    }
}