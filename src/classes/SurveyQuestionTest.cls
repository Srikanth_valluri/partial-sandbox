/***********************************************************************************
* Description - Test class developed for SurveyQuestion
*
* Version            Date            Author                    Description
* 1.0                13/12/17        Naresh Kaneriya (Accely)   Initial Draft
*************************************************************************************/

@isTest
public class SurveyQuestionTest{

  public static testmethod void getTest(){
      
      Survey_CRM__c Sy = new Survey_CRM__c();
      Sy.Description__c = 'Descrtpyion';
      Sy.End_Date__c = System.today() + 20;
      Sy.Hide_Survey_Name__c = true;
      Sy.Is_Active__c = true;
      Sy.Start_Date__c = System.today();
      Sy.Type__c = 'Portal';
      insert Sy ;
      
      
      Survey_Question_CRM__c SQ =  new Survey_Question_CRM__c();
      SQ.Survey__c= Sy.Id;
      SQ.OrderNumber__c = 25;
      SQ.Order_Number_Displayed__c = 'Order Test';
      SQ.Question__c = 'What is your Name?';
      SQ.Required__c = True;
      SQ.Choices__c = 'Test \n Naresh Write This \n Test class';
      SQ.Type__c = 'Single Select--Vertical';
      SQ.Choice_for_Additional_Textbox__c = 'Single Select Vertical; Test;';  
      
      insert SQ ;
      System.assert(SQ != null);
      
       List<SurveyQuestion> subQuestionList =  new  List<SurveyQuestion>();
       SurveyQuestion obj =  new SurveyQuestion(SQ ,subQuestionList);
       
       
       SQ.Type__c = 'Multi-Select--Vertical';
       update SQ;
       System.assertEquals('Multi-Select--Vertical', SQ.Type__c);
       SurveyQuestion obj1 =  new SurveyQuestion(SQ ,subQuestionList);
       
       SQ.Type__c = 'Single Select--Horizontal';
       update SQ;
       SurveyQuestion obj2 =  new SurveyQuestion(SQ ,subQuestionList);
       
       SQ.Type__c = 'Picklist';
       update SQ;
       SurveyQuestion obj3 =  new SurveyQuestion(SQ ,subQuestionList);
       
       SQ.Type__c = 'Rating';
       update SQ;
       SurveyQuestion obj4 =  new SurveyQuestion(SQ ,subQuestionList);
       
       SQ.Type__c = 'Free Text';
       update SQ;
       SurveyQuestion obj5 =  new SurveyQuestion(SQ ,subQuestionList);
       
      
      
  }

}