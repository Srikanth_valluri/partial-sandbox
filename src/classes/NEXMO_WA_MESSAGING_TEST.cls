@istest

public class NEXMO_WA_MESSAGING_TEST{


    static testmethod void NEXMO_WA_MESSAGING_M(){
    
    /*
    folder f = [select id from folder where id='0051t000001qJ7v' limit 1];
    */
    
    document d = new document();
    d.body = blob.valueof('MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDEwXuI6iH+hJD8i4SCagyxJRXocjypxO8MzL6Rqx7SR6h8QMIMnGbN9L0+QTaIqJCyUFQmm9VBa+XDI6tORRFLiOP1+pSBaz9yaLpCJtAVLc3pnBt5g+VTQX1tRu+AbPCL4YF2ao/yIYcsLanRKRgjJfJAsTcalMEb8pqad8xNAK+l9mdokhybhNtheUCjsVy6aInLyYGtqzoZSwyNMsZodaJPe96CmStWoSQZ5xguCztvgk+1RybL2LX/sW0A2rv0kS7Zdz3kjirPpmcMAiw5TNtdraulOhbxHrai9PShYxQ+lnIGJ5oAqR5u4ubdzruZ4tE9/vYnVlgyUF1/d8jdAgMBAAECggEABTSxVhAL0jYjt+rhGKpBp7Y5fCr5m+xP/uCz8hq6+AlZVeBIwhipR63ADBOho8ACBBXIBDV/UPnP3HfnIm0FZiA7F9kHcN0rkpVeyD1KqCTMG/A7cd2hhbBn5ypHT/iR7MGMsVtkb52fe/XBVn1l3Bg27qtBr/xbl6e1m3yr9SWRn2LTpfa0r09+yW3/XSXpC5bdO7x1fZkWfzlfYpSNuCc5kJstsrkpg/PAUP8Nk7kXJtrpKsKzEjxBzoWgzQ+r5S5GNpqaBQPGvgPGRZD6kgFfEPe/ifRSofCjtFcPr4eBh/FXzHh3wjgKrj5tPLMlrktnWWthsK4hDHeEudS2gQKBgQDkS3mvVOi1VRAUN9/HP+mrOjgnvp/mmhe9aawTAuCOk+fxKptlNZRtBFlc/XHX7vizOLF1p6W0Nes4vjwHo7JXF8pnPQohks9tLEr/L+NFdRoJR+39KqUf6ua0m57+XHO5P+fYlig8v5tPPXfKgE/i3lwescL+2OsidhjcavGbzQKBgQDcoiv1FMpgxt9bQIPNX3VIwN3hhRaG/VhrOA4ztzYfg4KDZuhmtpk6wuMM+uPkmQgRhfBxR9GaA5KKIfKw2fzZEsk7OkaiIjyP5uXYdJdaEnQ2bVLyb2HL4Dx9Tq2nWD83pMpknItJ9coPOYXDBuyhmWjtewAoT+v89IekUEBxUQKBgQCbkiNWK9J434KZhInmSdN+b6ocy3ZDuvY6iWxNeER2ZKxGV/aVPbHrqG8NQ1j1AVBRMW0c9CWjbKrifCCERDUfzIKNX7crsdPQXcKtq4CAsJARiwbO4uSrn3Of1/y88Vua/cWMfqbk3t2DXivnvxCxVT4JXLG44TE6xqeibO5AcQKBgQCGrqByvTxFE2FpTNqoVas8vC816Y403RZKbS+ONjrdxeO33uvjN3gWPL1pQQY4l4orWD3MiPDUOXYMyGhjuM32nNcmRo6zjn61wVdxaMqzeyxhxR6rS37Be6KxQ4QR8Xa1uuypuj306gtse8rBoMp2YzWAMmOIODFwer0Kesq74QKBgAZCm3+SeG/gWSxUXq8c7CSnVzoTmUkpITvvtISl4HeArfUaA15iYVl7bKsSiWwaogvgkXZ4GyOhMJlEXmeH5i5rXBxB7o7oHCiKjUgnZ5PYEyEJ0CYV7gg4ih2m9BY0dzF58EAkfW2IgiBERuhE7Yr/ZFZRPzaxuQcL87m+jD09');
    d.name = 'JWT_Private_Key';
    d.FolderId = [select id from folder where name = 'NEXMO'].id;
    insert d;
    
    
    Account a = new account();
    a.Name = 'Test';
    a.phone = '00123';
    insert a;
    system.debug(a.id);
    
    
    
    Nexmo_Whats_App_Request__c parent = new Nexmo_Whats_App_Request__c();
    
    parent.Last_Message__c= 'TEST';
    parent.To_number__c = 00123;
    parent.Unique_Key__c= a.id+userinfo.getuserid();
    insert parent;
    
    Nexmo_Whats_App_Message__c child = new Nexmo_Whats_App_Message__c();
    child.Nexmo_Whats_App_Request__c = parent.id;
    child.Nexmo_Message_Body__c= 'Test';
    child.Direction__c='o';
    
     
    
    
    PageReference pageRef = Page.nexmo_wamesssage;    
    Test.setCurrentPage(pageRef); 
    ApexPages.currentPage().getParameters().put('id', a.id);
     
    
    NEXMO_WA_MESSAGING controller = new NEXMO_WA_MESSAGING();
    
    controller.sfrecid = a.id;
    controller.wamessage = 'Test';
    controller.selectedTemplate = 'calls';
    controller.selectedWhatsAppNumber = 'Phone';
    controller.messageHistory();
    //Map<String,String> temp = NEXMO_WA_MESSAGING.getcredentials('Calls');
    controller.sendMessageWithMTM();
    controller.sendMessageWithoutMTM();
    controller.populatetemplates();
    controller.bodyTosend(true,'calls');
    NEXMO_WA_MESSAGING.prepareStringForBase64Decoding ('test');
    controller.bodyTosend(false,'');
    sobject s = a;
    
    
    
    }

}