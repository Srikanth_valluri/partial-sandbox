/**************************************************************************************************
* Name               : AgentPortalSearchByInventoryController
* Description        :  Class for AgentPortalSearchByInventoryController
* Created Date       : 3/09/2017                                                                     
* Created By         : Naresh Kaneriya                                                             
* Last Modified Date :                                                                            
* Last Modified By   :                                                                             
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         Naresh Kaneriya      03/09/2017                                                               
**************************************************************************************************/
 
@isTest(SeeAllData=false)
public class AgentPortalSearchByInventoryTest{

 public static Contact adminContact;
    public static User portalUser;
    public static Account adminAccount;
    public static User portalOnlyAgent;
    
    private  static Inventory__c inventory ; 
    private static Address__c addressDetail ; 
    private  static Property__c propertyDetail ;
  
    @testsetup
    public static void TestData(){
    

         addressDetail = InitialiseTestData.getAddressDetails(9086);
         insert addressDetail ; 
        
         propertyDetail = InitialiseTestData.getPropertyDetails(7650);
         insert propertyDetail ; 
        
         inventory = InitialiseTestData.getInventoryDetails('3456','1234','2345',9086,7650); 
         inventory.Status__c = 'Released';
         inventory.Unit_Type__c = 'Test';
         inventory.Address__c = addressDetail.Id;    
         insert inventory ; 
         
         Account acc =  new Account();
         acc.Name = 'Name';
         acc.Country_of_Sale__c = 'UAE';
         insert acc ;
        
        
}

    
    static void init(){


        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole00');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'te156', email='xy11z1@email.com',
                emailencodingkey='UTF-8', lastname='User 41156', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xy11z1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
        adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
         adminAccount.Country_of_Sale__c = 'UAE';
        insert adminAccount;
        
        adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        insert adminContact;
        
        Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
        insert agentContact;
        
        portalUser = InitialiseTestData.getPortalUser('t12345@test.com', adminContact.Id, 'Admin');
        portalOnlyAgent = InitialiseTestData.getPortalUser('test221@test.com', agentContact.Id, 'Agent');
        
        System.runAs(portalUser){
        Property__c property = InitialiseTestData.insertProperties();
        InitialiseTestData.createInventoryUser(property);
        }
        
        }
        
        
        
    }
    
    static void ApexPagePut(){

        ApexPages.currentPage().getParameters().put('sfdc.tabName','aX982364');
        ApexPages.currentPage().getParameters().put('Id', 'IdTest');
        ApexPages.currentPage().getParameters().put('Bedrooms','Bedrooms');
        ApexPages.currentPage().getParameters().put('Type','Type');
        ApexPages.currentPage().getParameters().put('Location','Location');   
        
        List<string> listSTR = new List<string>();
        listSTR.add('test');
        
        ApexPages.currentPage().getParameters().put('BedroomsSelected','["1 Br","2 Br"]');  
        ApexPages.currentPage().getParameters().put('TypeSelected','["Hotel","Retail"]');  
        ApexPages.currentPage().getParameters().put('floorPackageTypeSelected','["1 Br","2 Br"]');  
        ApexPages.currentPage().getParameters().put('villaTypeSelected','["1 Br","2 Br"]'); 
        ApexPages.currentPage().getParameters().put('CitySelected','["Test","Test"]');  
        ApexPages.currentPage().getParameters().put('ACDSelected','["1 Br","2 Br"]');  
         ApexPages.currentPage().getParameters().put('SinglebedroomsSelected','["1 Br","2 Br"]'); 
        ApexPages.currentPage().getParameters().put('propertyStatusSelected','["1 Br","2 Br"]');  
        ApexPages.currentPage().getParameters().put('MinimumPriceSelected','["1 Br","2 Br"]');  
        ApexPages.currentPage().getParameters().put('MaximumPriceSelected','["1 Br","2 Br"]');
       
        ApexPages.currentPage().getParameters().put('urlMinString','500');
        ApexPages.currentPage().getParameters().put('urlMaxString','5000');
        
    }


public static TestMethod void AgentPortalSearch(){
            UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole89');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'te16', email='x11yz1@email.com',
                emailencodingkey='UTF-8', lastname='Us11er 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='x11yz1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
    
    
    init();    
    ApexPagePut();
    
    
    
    AgentPortalSearchByInventoryController obj = new AgentPortalSearchByInventoryController();
   
   
   
    // Variable 
    obj.Notification = 'Notification';
    obj.NotificationCount = 'NotificationCount';
    obj.strbedrooms = 'strbedrooms';
    obj.strprojectType = 'strprojectType';
    obj.strfloorPackageType = 'strfloorPackageType';
    obj.strvillaType = 'strvillaType';
    obj.strpropertyStatus = 'strpropertyStatus';
    obj.selectedId = 'selectedId';
    obj.priceMin = '20';
    obj.priceMax = '20';
    obj.selectedBedrooms = 'NotificationCount';
    obj.tabName = 'sfdc.tabName';
    obj.bedroomsSelectedFromURL = 'bedroomsSelectedFromURL';
    obj.locationSelectedFromURL = 'locationSelectedFromURL';
    obj.typeSelectedFromURL = 'typeSelectedFromURL';
    obj.render = true;
    obj.renderaspdf = 'renderaspdf';
    obj.pTermKeyStr = 'pTermKeyStr';
    obj.widthPx = 'widthPx';
    obj.ptWidthPx = 'ptWidthPx';
    obj.strSinglebedrooms = 'strSinglebedrooms';
    obj.strCity = 'strCity';
    obj.strACD = 'strACD';
    obj.strSingleBedroom = 'strSingleBedroom';
    obj.minValueFromUrl = 'minValueFromUrl';
    obj.maxValueFromUrl = 'maxValueFromUrl';
    obj.DubaiReadyStudio = 'DubaiReadyStudio';
    obj.DubaiReadyAprt = 'DubaiReadyAprt';
   
   
   
    //Method
    
    obj.dosubmit();  
    obj.onLoad();
    obj.getVillaType();
    obj.getPropertyStatus();
    obj.getFloorPackageName();
    obj.filterAllInventories();
    //obj.prepareData();
    
    // for convertListToSet
    List<String> convertListToSet =  new List<String>{'Test' ,'Test2','Test3','Test'};
    obj.convertListToSet(convertListToSet);
    
    
        Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Unit_Type__c = 'Studio';
         update inv;
         Test.stopTest(); 
  
   obj.getAllUnitsOnPageLoad(inv.IPMS_Bedrooms__c, inv.Unit_Location__c,inv.Unit_Type__c,'200','500');
        }
  }
  
  
    public static testMethod void getVillaTypeTest(){

         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Unit_Type__c = 'Studio';
         inv.Project_Category__c  = 'Project';
         inv.Status__c   = 'Released';
         update inv;
         Test.stopTest(); 
         AgentPortalSearchByInventoryController  obj =  new AgentPortalSearchByInventoryController ();
        
         obj.getVillaType();
        
   }
   
   
   public static testMethod void getPropertyStatus(){
               
         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Unit_Type__c = 'Studio';
         inv.Property_Status__c   = 'Project';
         inv.Status__c   = 'Released';
         update inv;
         Test.stopTest(); 
         AgentPortalSearchByInventoryController  obj =  new AgentPortalSearchByInventoryController ();
         obj.getPropertyStatus();
        
   }
  
  
  
     public static testMethod void getMarketingNameTest(){
                 
         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Unit_Type__c = 'Studio';
         inv.Marketing_Name__c    = 'Market';
         inv.Status__c   = 'Released';
         update inv;
         Test.stopTest(); 
         AgentPortalSearchByInventoryController  obj =  new AgentPortalSearchByInventoryController ();
         obj.getMarketingName();
        
   }
   
   
   
       public static testMethod void getPropertyName(){

         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Unit_Type__c = 'Studio';
         inv.property_name__c     = 'Name';
         inv.Status__c   = 'Released';
         update inv;
         Test.stopTest(); 
         AgentPortalSearchByInventoryController  obj =  new AgentPortalSearchByInventoryController ();
         obj.getPropertyName();
        
   }


    
       public static testMethod void getCityName(){
                 
         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Unit_Type__c = 'Studio';
         inv.property_city__c = 'Dubai';
         inv.Status__c   = 'Released';
         update inv;
         Test.stopTest(); 
         AgentPortalSearchByInventoryController  obj =  new AgentPortalSearchByInventoryController ();
         obj.getCityName();
        
   }
   
   
   
        public static testMethod void getACDTest(){
                
         Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Unit_Type__c = 'Studio';
         inv.property_city__c = 'Dubai';
         inv.Status__c   = 'Released';
         inv.ACD_Date__c    = '2017-11-15';
         update inv;  
         Test.stopTest(); 
         AgentPortalSearchByInventoryController  obj =  new AgentPortalSearchByInventoryController ();
         obj.getACD();
        
         
   }
   
   
      // Inner Test
        public static testMethod void ProjectWrapperTest(){
         
         AgentPortalSearchByInventoryController.ProjectWrapper   obj =  new AgentPortalSearchByInventoryController.ProjectWrapper('MarketingName' ,
                                                                                                                                  'ProjectName' ,
                                                                                                                                  'unit',
                                                                                                                                  'unitType',
                                                                                                                                   'bedrooms',
                                                                                                                                  'bedroomType',
                                                                                                                                  'area',
                                                                                                                                  'ploatArea',
                                                                                                                                  Double.valueOf('22.55'),
                                                                                                                                  Double.valueOf('1500.2'),
                                                                                                                                  Double.valueOf('125.55'),
                                                                                                                                  'propertyStatus',
                                                                                                                                  'Id',
                                                                                                                                  'ACD');
         
   }
   
   
   
     public static testMethod void getAllUnitsTest(){
         
         
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Unit_Type__c = 'Studio';
         inv.property_city__c = 'Dubai';
         inv.Status__c   = 'Released';
         inv.ACD_Date__c    = '2017-11-15';
         
         inv.Bedroom_Type__c = 'bedroom1';
         
         update inv;
         
         Set<String> bedroomsSelectedList = new Set<String>{'bedroom1','bedroom1','bedroom1' ,'bedroom1'};
         Set<String> typeSelectedList = new Set<String>{'typeSelected1','typeSelected2','typeSelected3' ,'typeSelected4'};
         Set<String> floorPackageTypeSelectedList = new Set<String>{'floorPackage1','floorPackage2','floorPackage3' ,'floorPackage4'};
         Set<String> villaTypeSelectedList = new Set<String>{'villaTyp1','villaTyp2','villaTyp3' ,'villaTyp4'};
         Set<String> propertyStatusSelectedList = new Set<String>{'Release','Release2','Release3' ,'Release4'};
         
         Set<String> CitySelectedList =  new Set<String>{'bedroom1','bedroom2','bedroom3' ,'bedroom4'};
         Set<String> ACDSelectedList =  new Set<String>{'bedroom1','bedroom2','bedroom3' ,'bedroom4'};
         Set<String> SinglebedroomsSelectedList=  new Set<String>{'bedroom1','bedroom2','bedroom3' ,'bedroom4'};
   
         AgentPortalSearchByInventoryController  obj =  new AgentPortalSearchByInventoryController();
         obj.getAllUnits(bedroomsSelectedList,
                         typeSelectedList,
                         floorPackageTypeSelectedList,
                         villaTypeSelectedList,
                         propertyStatusSelectedList,
                         CitySelectedList,
                         ACDSelectedList,
                         SinglebedroomsSelectedList ,
                         '500' ,
                         '5000'); 
        
   } 
   
   
   
         public static testMethod void getAllUnitsOnPageLoadTest(){
            
          Test.startTest();
         Inventory__c inv = [select id ,Unit_Type__c , Address__r.City__c from Inventory__c where Unit_Type__c = 'Test' limit 1];
         inv.Unit_Type__c = 'Studio';
         inv.property_city__c = 'Dubai';
         inv.Status__c   = 'Released';
         inv.Property_Status__c = 'ready';
         inv.IPMS_Bedrooms__c = '2';
         inv.Plot_Area__c = '1000';
         inv.Special_Price__c = Decimal.valueOf('1000');
        
         inv.Property_Status__c = 'ready';
         inv.Bedroom_Type__c = '2 BHk';
         update inv;
         Test.stopTest();   
         
          ApexPages.currentPage().getParameters().put('DubaiReadyStudio','Studio');
          ApexPages.currentPage().getParameters().put('ReadyVillasinDubai','Villa');
          ApexPages.currentPage().getParameters().put('ONEBHKApartments','1 BHK');
          ApexPages.currentPage().getParameters().put('TWOBHKApartments','2 BHK');
          
         AgentPortalSearchByInventoryController  obj =  new AgentPortalSearchByInventoryController ();
         obj.getAllUnitsOnPageLoad('bedroomsSelected','locationSelected','typeSelected','500','5000');
        
   }
   
  
   
}