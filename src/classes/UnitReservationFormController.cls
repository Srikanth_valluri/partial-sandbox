/************************************************************************************************
 * @Name              : UnitReservationFormController
 * @Test Class Name   : UnitReservationFormController_Test
 * @Description       : Controller for Unit Reservation Form PDF
 * Modification Log
 * 1.0    QBurst    29/01/2020        Created Class
 ************************************************************************************************/

public without sharing class UnitReservationFormController {
    public string invId{set;get;}
    public Inventory__c selInventory {get;set;}
    public boolean isHighRise {get; set;}
    public boolean isHorizontal {get; set;}
    public string city {get; set;}
    public string unitNo {get; set;}
    public string unitArea {get; set;}
    public string unitType {get; set;}
    public String currentUser {get;set;}
    public String currentUserEmail {get; set;}
    public String userCurrentTime { get; set; }
    public Integer dd { get; set; }
    public Integer mon { get; set; }
    public Integer year { get; set; }
    public UnitReservationFormController(ApexPages.StandardController controller) {
        try {
            isHighRise = false;
            isHorizontal = false;
            unitType = '';
            invId = Apexpages.currentPage().getParameters().get('recId');
            if(invId != null && invId != ''){
                fetchMappings(invId);
            }
        } catch (Exception e) {
            system.debug('Exception: ' + e);
        }
    }
    
    /****************************************************************************************
    * @Description : fetches Mappings from Custom Metadata and populates the values for PDF 
    * @Params      : 1. string invId - Id of the Inventory related to the Unt Reservation Form
    * @Return      : void                                                                   
    ****************************************************************************************/
    public void fetchMappings(String invId) {
        String query = 'SELECT Id';
        Set<string> inventoryFields = new Set<String>();
        Map<String, List<Unit_Reservation_Form_Mapping__mdt>> fieldMap 
                = new Map<String, List<Unit_Reservation_Form_Mapping__mdt>>();
        for(Unit_Reservation_Form_Mapping__mdt mapping: [SELECT Id, MasterLabel, Source_Field_API_Name__c,
                                                            Priority__c, Source_Values__c, Target_Value__c
                                                         FROM Unit_Reservation_Form_Mapping__mdt]){
            if(mapping.Source_Field_API_Name__c != null && mapping.Source_Field_API_Name__c != ''){
                inventoryFields.add(mapping.Source_Field_API_Name__c);
            }
            
            List<Unit_Reservation_Form_Mapping__mdt> mappingList 
                    = new List<Unit_Reservation_Form_Mapping__mdt>();            
            if(mapping.Source_Values__c != null && mapping.Source_Values__c != ''){
                if(fieldMap.containsKey(mapping.MasterLabel)){
                    mappingList = fieldMap.get(mapping.MasterLabel);
                }
                mappingList.add(mapping);
                fieldMap.put(mapping.MasterLabel, mappingList);
            }
        }
        for(String field: inventoryFields){
            query += ', ' + field;
        }
        query += ' FROM Inventory__c WHERE Id = \'' + invId + '\'';
        system.debug('query: ' + query);
        selInventory = Database.query(query);
        system.debug('selInventory: ' + selInventory);
        for(String key: fieldMap.keyset()){
            String targetValue = '';
            if(fieldMap.get(key).size() > 1){
                fieldMap.put(key, sortMappings(fieldMap.get(key)));    
            }
            for(Unit_Reservation_Form_Mapping__mdt mapping: fieldMap.get(key)){
                List<String> valueList = new List<String>();
                for(String value: mapping.Source_Values__c.split(',')){
                    valueList.add(value.normalizeSpace());
                }
                //valueList.addAll(mapping.Source_Values__c.split(','));
                String fieldValue = String.valueOf(selInventory.get(mapping.Source_Field_API_Name__c));
                system.debug('valueList: ' + valueList + ', fieldValue: ' + fieldValue);
                if(valueList.contains(fieldValue)){
                    targetValue = mapping.Target_Value__c;
                    break;
                }
            }
            system.debug('Key: ' + key + ', Target Value: ' + targetValue);
            if(key == 'Project Type' && targetValue == 'HighRise'){
                isHighRise = true;
            } else  if(key == 'Project Type' && targetValue == 'Horizontal'){
                isHorizontal = true;
            } else if(key == 'Unit Type'){
                unitType = targetValue;
            }
        }
        system.debug('isHighRise: ' + isHighRise);
        system.debug('isHorizontal: ' + isHorizontal);
        system.debug('unitType: ' + unitType);
        userCurrentTime = System.Now().format()+':'+System.Now().Second();
        currentUser = UserInfo.getName();
        currentUserEmail = UserInfo.getUserEmail();
        dd = System.today().day();
        mon = System.today().month();
        year = System.today().year();
        
    }
    
    /*******************************************************************************************************
    * @Description : Sorts the mapping List based on the Priority value provided in custom metadata 
    * @Params      : 1. List<Unit_Reservation_Form_Mapping__mdt> mappingList - List to be sorted  
    * @Return      : List<Unit_Reservation_Form_Mapping__mdt> sortedList - List sorted based on Priority
    ********************************************************************************************************/
    public List<Unit_Reservation_Form_Mapping__mdt> sortMappings
                                    (List<Unit_Reservation_Form_Mapping__mdt> mappingList ){
        system.debug('mappingList: ' + mappingList);
        Map<Decimal, Unit_Reservation_Form_Mapping__mdt> sortMap 
            =  new Map<Decimal, Unit_Reservation_Form_Mapping__mdt>();
        List<Unit_Reservation_Form_Mapping__mdt> sortedList = new List<Unit_Reservation_Form_Mapping__mdt>();
        List<Unit_Reservation_Form_Mapping__mdt> nonPriorityList = new List<Unit_Reservation_Form_Mapping__mdt>();
        List<Decimal> keySort = new List<Decimal>();
        for(Unit_Reservation_Form_Mapping__mdt mapping: mappingList){
            if(mapping.Priority__c != null){
                sortMap.put(mapping.Priority__c, mapping);
                keySort.add(mapping.Priority__c);
            } else{
                nonPriorityList.add(mapping);
            }
        }
        keySort.sort();
        for(Decimal key: keySort){
            sortedList.add(sortMap.get(key));
        }
        if(nonPriorityList.size() > 0){
            sortedList.addAll(nonPriorityList);    
        }
        system.debug('sortedList: ' + sortedList);
        return sortedList;
    }
}