/*
 * Description: Test Class for UpdatePaymentPlanPageExtension
 */

@isTest
public class UpdatePaymentPlanPageExtensionTest {

    /*
     * Description: Test method to check if payment plan is generated or not.
     */
    @isTest
    public static void checkPaymentPlanAndTerms() {
        
        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
            
        Id aoptRecordTypeID = getRecordTypeIdForAOPT();

        // create Case record
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , aoptRecordTypeID);

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;
    
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;
        
        // create Payment Plan record
        Payment_Plan__c objPaymentPlan = new Payment_Plan__c();
        objPaymentPlan.Status__c = 'InActive';
        objPaymentPlan.Booking_Unit__c = bookingUnitList[0].Id;
        insert objPaymentPlan;
        
        Test.startTest();
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
            PageReference pageRef = Page.UpdatePaymentPlanPage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Id', bookingUnitList[0].Id);
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(bookingUnitList[0]);
            UpdatePaymentPlanPageExtension objController = new UpdatePaymentPlanPageExtension(standardControllerInstance);
            objController.objBookingUnit =  bookingUnitList[0];
            objController.generatePaymentPlanAndTerms();
        Test.stopTest();
        
        // Check whether the payment plan is generated or not
        List<Payment_Plan__c> lstPaymentPlans = [SELECT Id,Status__c FROM Payment_Plan__c WHERE Booking_Unit__c =: bookingUnitList[0].Id];
        System.assert(lstPaymentPlans != null);
        System.assert(lstPaymentPlans.size() > 0);
        System.assert(lstPaymentPlans.size() == 2);
    }

    /*
     * Description: Test method to check if page error message is generated or not
     */
    @isTest
    public static void checkErrorGenerationForPaymentPlan() {
        
        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
            
        Id aoptRecordTypeID = getRecordTypeIdForAOPT();
    
        // Create Case record
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , aoptRecordTypeID);

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;
    
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;

        // Create Payment Plan record
        Payment_Plan__c objPaymentPlan = new Payment_Plan__c();
        objPaymentPlan.Status__c = 'InActive';
        objPaymentPlan.Booking_Unit__c = bookingUnitList[0].Id;
        insert objPaymentPlan;
        
        Test.startTest();
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
            PageReference pageRef = Page.UpdatePaymentPlanPage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Id', bookingUnitList[0].Id);
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(bookingUnitList[0]);
            UpdatePaymentPlanPageExtension objController = new UpdatePaymentPlanPageExtension(standardControllerInstance);
            objController.objBookingUnit =  bookingUnitList[0];
            objController.generatePaymentPlanAndTerms();
        Test.stopTest();
        
        List<Payment_Plan__c> lstPaymentPlans = [SELECT Id,Status__c FROM Payment_Plan__c WHERE Booking_Unit__c =: bookingUnitList[0].Id];
        System.assert(lstPaymentPlans.size() > 0);
        System.assert(lstPaymentPlans.size() == 1);

        // Check whether error message is generated or not
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assert(pageMessages.size() > 0);
    }

    /*
     * Description: Method to fetch the record type Id of "AOPT" Case record type
     */
    
    private static Id getRecordTypeIdForAOPT() {
        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id aoptRecordTypeID = caseRecordTypes.get('AOPT').getRecordTypeId();
        return aoptRecordTypeID;
    }
    
}