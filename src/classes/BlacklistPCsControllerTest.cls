@isTest (seeAllData=false)
public class BlacklistPCsControllerTest {
    @isTest static void testMethod1(){
         List<User> userList = new List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name='Property Consultant']; 
        for(integer i = 1;i<15;i++){
          User u = new User(Alias = 'standt', Email='sxyz'+i+'@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing'+i+'abc', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='abc'+i+'@testorg.com');
          userList.add(u);
        }
        
        insert userList;
        BlacklistPCsController blist = new BlacklistPCsController();
        
       blist.selDOS = 'Testing1abc';
       blist.startDate = '02/02/2018';
       blist.reason = 'Test';
       blist.endDate = '02/02/2040';
       PageReference blackListPCs = blist.blackListPCs();
        blist.Nxtbtn();
       blist.prvbtn();
        blist.LastBtn();
        blist.fetchPCs();
    }
     @isTest static void testMethod2(){
        List<User> userList = new List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name='Property Consultant']; 
        for(integer i = 1;i<15;i++){
          User u = new User(Alias = 'standt', Email='sxyz'+i+'@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing'+i+'abc', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, BlackList_Start_Date__c = System.today()-1,
            BlackList_End_Date__c = System.today()+1,
            TimeZoneSidKey='America/Los_Angeles', UserName='abc'+i+'@testorg.com');
          userList.add(u);
        }
        
        insert userList;
        BlacklistPCsController blist = new BlacklistPCsController();
        
       blist.selBlacklistedUserName = 'Testing1abc';
       PageReference blackListPCs = blist.unBlackListPCs();
       
        blist.NxtbtnBlacklIsted();
        blist.prvbtnBlacklisted();
        blist.LastBtnBlacklisted();
        blist.fetchPCsBlackListed();
    }
    @isTest static void testMethod3(){
         List<User> userList = new List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name='Property Consultant']; 
        for(integer i = 1;i<15;i++){
          User u = new User(Alias = 'standt', Email='sxyz'+i+'@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing'+i+'abc', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='abc'+i+'@testorg.com');
          userList.add(u);
        }
        
        insert userList;
        BlacklistPCsController blist = new BlacklistPCsController();
        
       blist.selDOS = 'Testing1abc';
       //blist.startDate = '02/02/2018';
       blist.reason = 'Test';
       //blist.endDate = '02/02/2040';
       PageReference blackListPCs = blist.blackListPCs();
        blist.Nxtbtn();
       blist.prvbtn();
        blist.LastBtn();
        blist.fetchPCs();
    }
    @isTest static void testMethod4(){
         List<User> userList = new List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name='Property Consultant']; 
        for(integer i = 1;i<15;i++){
          User u = new User(Alias = 'standt', Email='sxyz'+i+'@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing'+i+'abc', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='abc'+i+'@testorg.com');
          userList.add(u);
        }
        
        insert userList;
        BlacklistPCsController blist = new BlacklistPCsController();
        
       blist.selDOS = 'Testing1abc';
       blist.startDate = '02/02/2018';
       //blist.reason = 'Test';
       blist.endDate = '02/02/2040';
       PageReference blackListPCs = blist.blackListPCs();
        blist.Nxtbtn();
       blist.prvbtn();
        blist.LastBtn();
        blist.fetchPCs();
    }
    @isTest static void testMethod5(){
         List<User> userList = new List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name='Property Consultant']; 
        for(integer i = 1;i<15;i++){
          User u = new User(Alias = 'standt', Email='sxyz'+i+'@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing'+i+'abc', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='abc'+i+'@testorg.com');
          userList.add(u);
        }
        
        insert userList;
        BlacklistPCsController blist = new BlacklistPCsController();
        
       blist.selDOS = 'Testing1abc';
       blist.startDate = '02/02/2018';
       blist.reason = 'Test';
       blist.endDate = '02/02/2016';
       PageReference blackListPCs = blist.blackListPCs();
        blist.Nxtbtn();
       blist.prvbtn();
        blist.LastBtn();
        blist.fetchPCs();
    }

    @isTest static void testMethod6(){

      DatePickerController dc = new DatePickerController();
      Integer i = dc.getRandomNumber(5);
      dc.setTargetDate('01-01-2000');
    }

}