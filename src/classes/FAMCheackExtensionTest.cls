/*
* Description - Test class developed for 'FAMCheackExtension'
*
* Version            Date            Author            Description
* 1.0                20/04/20        Mosib           Initial Draft
*/
@isTest
public class FAMCheackExtensionTest {

    
    static testMethod void testMethod1() {
        List<User> userList = [SELECT Id from User where Id != :UserInfo.getUserId() AND isActive = True LIMIT 1];
        
        // Insert Account
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_2__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_3__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_4__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_5__pc = '123123123';
        objAcc.Telephone__c = '123123123';
        objAcc.FAM_CAUTION__c = Label.FAMFieldValue;
        objAcc.Primary_CRE__c = userList[0].Id;
        insert objAcc ;


        ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
               
        PageReference pageRef = Page.FAMCheack;
        pageRef.getParameters().put('Id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        FAMCheackExtension obj = new FAMCheackExtension(controller);
        obj.checkFam();
        obj.checkFamApn();
    }
    
     static testMethod void testMethod2() {
        List<User> userList = [SELECT Id from User where Id != :UserInfo.getUserId() AND isActive = True LIMIT 1];
        
        // Insert Account
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_2__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_3__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_4__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_5__pc = '123123123';
        objAcc.Telephone__c = '123123123';
        objAcc.FAM_CAUTION__c = Label.FAMFieldValue;
        objAcc.Primary_CRE__c = userList[0].Id;
        insert objAcc ;
        
         //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        objSR.Agency__c = objAcc.Id ;
        insert objSR ;

        Id dealId = objSR.Deal_ID__c;
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
        insert lstBookings ; 

        Property__c objProperty = TestDataFactory_CRM.createProperty();
        insert objProperty;

        Inventory__c objInventory = TestDataFactory_CRM.createInventory(objProperty.id);
        objInventory.Area_Sqft__c = '1520';
        insert objInventory;

        //Insert Booking Units
        DateTime today = datetime.now();
        DateTime prevDate = today.addMonths(-2);

        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
        lstBookingUnits[0].Registration_ID__c = '1520';
        lstBookingUnits[0].Requested_Price__c = 1000;
        lstBookingUnits[0].Area__c = 100;
        lstBookingUnits[0].Registration_Status_Code__c = 'AA';
        lstBookingUnits[0].Registration_DateTime__c = prevDate;
        lstBookingUnits[0].Inventory__c = objInventory.id;
        lstBookingUnits[0].Unit_Name__c = 'Test'; 
        
        insert lstBookingUnits;


        ApexPages.StandardController controller = new ApexPages.standardController(lstBookingUnits[0]);
               
        PageReference pageRef = Page.FAMCheack;
        pageRef.getParameters().put('Id', String.valueOf(lstBookingUnits[0].Id));
        Test.setCurrentPage(pageRef);

        FAMCheackExtension obj = new FAMCheackExtension(controller);
        //obj.checkFam();
        obj.checkFamApnBU();
    }

    
}