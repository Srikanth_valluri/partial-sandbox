public class NEXMO_RESPONSE_PARSER{

    public String message_uuid {get;set;} 
    public To to {get;set;} 
    public To from_Z {get;set;} // in json: from
    public String timestamp {get;set;} 
    public String status {get;set;} 
    public Error error {get;set;} 

    public NEXMO_RESPONSE_PARSER(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'message_uuid') {
                        message_uuid = parser.getText();
                    } else if (text == 'to') {
                        to = new To(parser);
                    } else if (text == 'from') {
                        from_Z = new To(parser);
                    } else if (text == 'timestamp') {
                        timestamp = parser.getText();
                    } else if (text == 'status') {
                        status = parser.getText();
                    } else if (text == 'error') {
                        error = new Error(parser);
                    } else {
                        System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    public class Error {
        public Integer code {get;set;} 
        public String reason {get;set;} 

        public Error(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'code') {
                            code = parser.getIntegerValue();
                        } else if (text == 'reason') {
                            reason = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Error consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class To {
        public String number_Z {get;set;} // in json: number
        public String type_Z {get;set;} // in json: type

        public To(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'number') {
                            number_Z = parser.getText();
                        } else if (text == 'type') {
                            type_Z = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'To consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    
    public static NEXMO_RESPONSE_PARSER parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new NEXMO_RESPONSE_PARSER(parser);
    }
    
    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
    





}