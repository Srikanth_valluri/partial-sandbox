/*****************************************************************************************************
* Description - Test class developed for 'GenerateCRFService'
*
* Version            Date            Author               Description
* 1.0                21/11/17        Naresh(Accely)               Initial Draft
 *******************************************************************************************************/


@isTest(SeeAllData=false)
public class GenerateCRFServiceTest{


public static testmethod void GetCustomerRequestForm(){
    
    Test.startTest();
    GenerateCRFService.CRFHttpSoap11Endpoint obj = new GenerateCRFService.CRFHttpSoap11Endpoint();
    List<processXxdcProcessServiceWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5> regTerms =  new List<processXxdcProcessServiceWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5>();
    
    SOAPCalloutServiceMock.returnToMe =  new Map<String, GenerateCRFService.GetCustomerRequestFormResponse_element>();
    GenerateCRFService.GetCustomerRequestFormResponse_element response_x = new GenerateCRFService.GetCustomerRequestFormResponse_element();
    response_x.return_x = 'S';
    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    
    test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
    
    String respose = obj.GetCustomerRequestForm('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM',regTerms);
    
    System.assertEquals(respose,'S');
    Test.stopTest();
    
  } 

}