/**
 * @File Name          : FmIpmsRestServices.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10/13/2019, 4:29:17 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    10/10/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public with sharing class FmIpmsRestServices {

    @testVisible private static final String SF_REST_BASEURL = Label.FM_REST_IPMS_BaseUrl;

    @testVisible private static final String FM_RECEIPTS_FOR_THE_PARTY_ID =
            '/receipt/party/{0}/FM';

    @testVisible private static final String GENERATE_DUPLICATE_RECEIPT =
            '/report/RECEIPT/SFDC/INTERNAL_KEY/RECEIPT_ID/{0}';

    @testVisible private static final String UNIT_SOA_BY_REGISTRATIONID =
            '/report/FMSOA/SFDC/INTERNAL_KEY/REGISTRATION_ID/{0}';

    @testVisible private static final String BULK_SOA_FOR_PARTY_ID =
            '/report/FMBULKSOA/SFDC/EXT-12345/MULTIPLE/PARTY_ID={0}';

    @testVisible private static final String BULK_SOA_FOR_PARTY_ID_LOCATION_CODE =
            '/report/FMBULKSOA/SFDC/EXT-12345/MULTIPLE/PARTY_ID={0}~PCODE={1}';

    @testVisible private static final String INVOICE_BY_REGISTRATIONID =
            '/report/FMINV/SFDC/EXT-12345/REGISTRATION_ID/{0}';

    @testVisible private static final String INVOICE_BY_REGISTRATIONID_AND_DATE =
            '/report/FMINV/SFDC/EXT-12345/MULTIPLE/REGISTRATION_ID={0}~INVOICE_FROM={1}';

    @testVisible private static final String INVOICE_BY_REGISTRATIONID_AND_DATERANGE =
            '/report/FMINV/SFDC/EXT-12345/MULTIPLE/REGISTRATION_ID={0}~INVOICE_FROM={1}~INVOICE_TO={2}';

    @testVisible private static final String BULK_INVOICE_BY_PARTYID =
            '/report/FMBULKINV/PUBLIC/SFDC/PARTY_ID/{0}';

    @testVisible private static final String BULK_INVOICE_BY_PARTYID_AND_LOCATIONCODE =
            '/report/FMBULKINV/PUBLIC/SFDC/MULTIPLE/PARTY_ID={0}~PCODE={1}';

    @testVisible private static final String BULK_INVOICE_BY_PARTYID_AND_DATE =
            '/report/FMBULKINV/SFDC/EXT-12345/MULTIPLE/PARTY_ID={0}~INVOICE_FROM={1}';

    @testVisible private static final String BULK_INVOICE_BY_PARTYID_LOCATIONCODE_AND_DATERANGE =
            '/report/FMBULKINV/SFDC/EXT-12345/MULTIPLE/PARTY_ID={0}~INVOICE_FROM={1}~INVOICE_TO={2}~PCODE={3}';

    @testVisible private static final String GET_INV_DUE_LIST   = 'GET_INV_DUE_LIST';

    @testVisible private static final String GET_INV_INFO_LIST  = 'GET_INV_INFO_LIST';

    @testVisible private static final String CREATE_RECEIPT     = '/createreceipt';

    @testVisible private static  Decimal sumAmount;

    @testVisible private static List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
    
    public static Boolean logErrors = true;

    public static boolean byPassDueDateCheck = false;

    public static String getReceiptsForPartyId(String partyId) {
        if (String.isBlank(partyId)) {
            return NULL;
        }

        HttpResponse res = callout(String.format(FM_RECEIPTS_FOR_THE_PARTY_ID, new List<String>{ partyId }));
        return res.getBody();
    }

    public static String generateDuplicateReceipt(String receiptId) {
        if (String.isBlank(receiptId)) {
            return receiptId;
        }

        HttpResponse res = callout(String.format(GENERATE_DUPLICATE_RECEIPT, new List<String>{ receiptId }));

        return fetchResponseUrl(res.getBody());
    }

    public static String getUnitSoaByRegistrationId(String registrationId) {
        if (String.isBlank(registrationId)) {
            return registrationId;
        }

        HttpResponse res = callout(String.format(UNIT_SOA_BY_REGISTRATIONID, new List<String>{ registrationId }));

        String unitSoaUrl = fetchResponseUrl(res.getBody());
        system.debug('!!!!!.avoidErrorLogCreation'+DisplayRelatedUnitsController.avoidErrorLogCreation);
        if (String.isBlank(unitSoaUrl) && DisplayRelatedUnitsController.avoidErrorLogCreation == false) {
            //unitSoaUrl = 'https://sftest.deeprootsurface.com/docs/e/46589457_68115_SOA.pdf';
            insert new Error_Log__c(
                Process_Name__c = 'Unit SOA Generation',
                Account__c = CustomerCommunityUtils.customerAccountId,
                Error_Details__c = 'res:\n' + res.getBody()
            );
        }
        return unitSoaUrl;
    }

    public static Response getBulkSoaForPartyId(String partyId) {
        if (String.isBlank(partyId)) {
            return NULL;
        }

        HttpResponse res = callout(String.format(BULK_SOA_FOR_PARTY_ID, new List<String>{ partyId }));

        return (Response) Json.deserialize(res.getBody(), Response.class);
    }

    public static String getBulkSoaUrlForPartyId(String partyId) {
        if (String.isBlank(partyId)) {
            return partyId;
        }

        HttpResponse res = callout(String.format(BULK_SOA_FOR_PARTY_ID, new List<String>{ partyId }));

        return fetchResponseUrl(res.getBody());
    }

    public static Response getBulkSoaForPartyIdAndProjectCode(String partyId, String projectCode) {
        if (String.isBlank(partyId) || String.isBlank(projectCode)) {
            return NULL;
        }

        HttpResponse res = callout(
            String.format(BULK_SOA_FOR_PARTY_ID_LOCATION_CODE, new List<String>{ partyId, projectCode })
        );

        return (Response) Json.deserialize(res.getBody(), Response.class);
    }

    public static String getBulkSoaUrlForPartyIdAndProjectCode(String partyId, String projectCode) {
        if (String.isBlank(partyId) || String.isBlank(projectCode)) {
            return NULL;
        }

        HttpResponse res = callout(
            String.format(BULK_SOA_FOR_PARTY_ID_LOCATION_CODE, new List<String>{ partyId, projectCode })
        );

        return fetchResponseUrl(res.getBody());
    }

    public static String getInvoiceByRegistrationId(String registrationId) {
        if (String.isBlank(registrationId)) {
            return registrationId;
        }

        HttpResponse res = callout(
            String.format(INVOICE_BY_REGISTRATIONID, new List<String>{ registrationId })
        );

        return fetchResponseUrl(res.getBody());
    }

    public static String getInvoiceByRegistrationIdAndDate(String registrationId, Date invoiceDate) {
        if (String.isBlank(registrationId) || invoiceDate == NULL) {
            return NULL;
        }

        HttpResponse res = callout(String.format(
            INVOICE_BY_REGISTRATIONID_AND_DATE,
            new List<String>{ registrationId, FmcUtils.formatAsIpmsDate(invoiceDate) }
        ));

        return fetchResponseUrl(res.getBody());
    }

    public static String getInvoiceByRegistrationIdAndDateRange(String registrationId, Date startDate, Date endDate) {
        if (String.isBlank(registrationId) || startDate == NULL || endDate == NULL) {
            return NULL;
        }

        HttpResponse res = callout(String.format(
            INVOICE_BY_REGISTRATIONID_AND_DATERANGE,
            new List<String>{registrationId, FmcUtils.formatAsIpmsDate(startDate), FmcUtils.formatAsIpmsDate(endDate)}
        ));

        return fetchResponseUrl(res.getBody());
    }

    public static String getBulkInvoiceByPartyId(String partyId) {
        if (String.isBlank(partyId)) {
            return partyId;
        }

        HttpResponse res = callout(
            String.format(BULK_INVOICE_BY_PARTYID, new List<String>{ partyId })
        );

        return fetchResponseUrl(res.getBody());
    }

    public static String getBulkInvoiceByPartyIdAndProjectCode(String partyId, String projectCode) {
        if (String.isBlank(partyId) || String.isBlank(projectCode)) {
            return NULL;
        }

        HttpResponse res = callout(
            String.format(BULK_INVOICE_BY_PARTYID_AND_LOCATIONCODE, new List<String>{ partyId, projectCode })
        );

        return fetchResponseUrl(res.getBody());
    }

    public static String getBulkInvoiceByPartyIdAndDate(String partyId, Date invoiceDate) {
        if (String.isBlank(partyId) || invoiceDate == NULL) {
            return NULL;
        }

        HttpResponse res = callout(String.format(
            BULK_INVOICE_BY_PARTYID_AND_DATE, new List<String>{ partyId, FmcUtils.formatAsIpmsDate(invoiceDate) }
        ));

        return fetchResponseUrl(res.getBody());
    }

    public static String getBulkInvoiceByPartyIdDateRangeAndProjectCode(
        String partyId, Date startDate, Date endDate, String projectCode
    ) {
        if (String.isBlank(partyId) || startDate == NULL || endDate == NULL || String.isBlank(projectCode)) {
            return NULL;
        }

        HttpResponse res = callout(String.format(
            BULK_INVOICE_BY_PARTYID_LOCATIONCODE_AND_DATERANGE,
            new List<String>{
                partyId, FmcUtils.formatAsIpmsDate(startDate), FmcUtils.formatAsIpmsDate(startDate), projectCode
            }
        ));

        return fetchResponseUrl(res.getBody());
    }

    //Added by Shubham - To byPass dueDate for current month IF check (11/05/2020)
    public static DueInvoicesResult getDueInvoices(String registrationId, String partyId, String propertyName, Boolean byPassIfCheck) {
        if (String.isBlank(registrationId) && String.isBlank(partyId) && String.isBlank(propertyName)) {
            return NULL;
        }
        System.debug('byPassIfCheck: ' + byPassIfCheck);
        byPassDueDateCheck = byPassIfCheck;

        return getDueInvoices(registrationId, partyId, propertyName);
    }

    public static DueInvoicesResult getDueInvoices(String registrationId, String partyId, String propertyName) {

        if (String.isBlank(registrationId) && String.isBlank(partyId) && String.isBlank(propertyName)) {
            return NULL;
        }
        sumAmount = 0.0;
        IpmsRestServices__c settings = IpmsRestServices__c.getInstance();
        if (settings == NULL
            || String.isBlank(settings.BaseUrl__c) || String.isBlank(settings.Username__c) || String.isBlank(settings.Password__c)
        ) {
            return NULL;
        }
        
        HttpRequest req = new HttpRequest();


        req.setEndpoint(settings.BaseUrl__c + '/XXDC_PROCESS_SERVICE_WS/retrieve/');
        System.debug('req.getEndpoint = ' + req.getEndpoint());

        RetrieveRequest retrieveRequest = new RetrieveRequest();
        retrieveRequest.RETRIEVE_Input.InputParameters.P_REQUEST_NUMBER             =   String.valueOf(
                                                                                            Datetime.now().getTime()
                                                                                        );
        retrieveRequest.RETRIEVE_Input.InputParameters.P_REQUEST_NAME               =   'GET_INV_DUE_LIST';
        //retrieveRequest.RETRIEVE_Input.InputParameters.P_REQUEST_MESSAGE =
            //'{' +
            //    '"PARAM_ID":"' + (String.isBlank(registrationId) ? '' : registrationId) + '",' +
            //    '"ATTRIBUTE1":"' + (String.isBlank(partyId) ? '' : partyId) + '",' +
            //    '"ATTRIBUTE2":"' + (String.isBlank(propertyName) ? '' : propertyName) + '"' +
            //'}';
        retrieveRequest.RETRIEVE_Input.InputParameters.P_REQUEST_MESSAGE.PARAM_ID   =   registrationId;
        retrieveRequest.RETRIEVE_Input.InputParameters.P_REQUEST_MESSAGE.ATTRIBUTE1 =   partyId;
        retrieveRequest.RETRIEVE_Input.InputParameters.P_REQUEST_MESSAGE.ATTRIBUTE2 =   propertyName;


        String requestBody =
            '{' +
              '"RETRIEVE_Input": {' +
                '"RESTHeader": {' +
                  '"Responsibility": "ONT_ICP_SUPER_USER",' +
                  '"RespApplication": "ONT",' +
                  '"SecurityGroup": "STANDARD",' +
                  '"NLSLanguage": "AMERICAN"' +
                '},' +
                '"InputParameters": {' +
                  '"P_REQUEST_NUMBER": '
        ;

        requestBody += ('"' + String.valueOf(Datetime.now().getTime()) + '"');

        requestBody +=
        ',"P_SOURCE_SYSTEM": "SFDC",' +
              '"P_REQUEST_NAME": ';

        requestBody += ('"' + GET_INV_DUE_LIST + '"');

        requestBody += ',"P_REQUEST_MESSAGE": ';

        requestBody +=
            '{' +
                '"PARAM_ID":"' + (String.isBlank(registrationId) ? '' : registrationId) + '",' +
                '"ATTRIBUTE1":"' + (String.isBlank(partyId) ? '' : partyId) + '",' +
                '"ATTRIBUTE2":"' + (String.isBlank(propertyName) ? '' : propertyName) + '"' +
            '}';

        requestBody += '}}}';

        System.debug('requestBody = ' + requestBody);

        req.setBody(requestBody);

        System.debug('req.getBody = ' + req.getBody());

        req.setHeader(
            'Authorization' ,
            'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(settings.Username__c + ':' + settings.Password__c))
        );

        System.debug('Authorization: ' + EncodingUtil.base64Encode(Blob.valueOf(settings.Username__c + ':' + settings.Password__c)) );
        req.setTimeout(Integer.valueOf(settings.Timeout__c == NULL ? 120000 : settings.Timeout__c));


        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Content-Length', '0');
        req.setHeader('ACCEPT','application/json');

        System.debug('getHeader : ' + req.getHeader('Authorization'));

        req.setMethod('POST');


        HttpResponse res = new Http().send(req);
        System.debug('Status Code = ' + res.getStatusCode());
        System.debug('body = ' + res.getBody());

        DueInvoicesResponse dueResponse = (DueInvoicesResponse) JSON.deserialize(
            res.getBody(), DueInvoicesResponse.class
        );

        System.debug('dueResponse: ' + dueResponse);

        DueInvoicesResult dueResult;

        if (dueResponse == NULL) {
            return dueResult;
        }

        dueResult = new DueInvoicesResult(dueResponse);

        if (dueResponse.OutputParameters.X_RESPONSE_MESSAGE == NULL
            || dueResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM == NULL
        ) {
            return dueResult;
        }

        dueResult.lstDueInvoice = new List<DueInvoice>();

        Date nextDueDate;
        Decimal totalDueAmount = 0.00;

        Date today = Date.today();
        Integer currentMonth = today.month();
        Integer currentYear = today.year();

        for(Map<String, String> attributeMap : dueResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM
        ) {
            DueInvoice dueInvoice;
            System.debug('attributeMap: ' + attributeMap);
            if (attributeMap != NULL) {
                dueInvoice = new DueInvoice();
                dueInvoice.registrationId = attributeMap.get('ATTRIBUTE1');
                dueInvoice.unitName = attributeMap.get('ATTRIBUTE2');
                dueInvoice.projectName = attributeMap.get('ATTRIBUTE3');
                dueInvoice.customerId = attributeMap.get('ATTRIBUTE4');
                dueInvoice.orgId = attributeMap.get('ATTRIBUTE5');
                dueInvoice.partyId = attributeMap.get('ATTRIBUTE6');
                dueInvoice.partyName = attributeMap.get('ATTRIBUTE7');
                dueInvoice.trxNumber = attributeMap.get('ATTRIBUTE8');
                dueInvoice.creationDate = attributeMap.get('ATTRIBUTE9');
                dueInvoice.callType = attributeMap.get('ATTRIBUTE10');
                dueInvoice.dueRemaining = Decimal.valueOf(attributeMap.get('ATTRIBUTE11'));
                dueInvoice.chargeAmount = Decimal.valueOf(attributeMap.get('ATTRIBUTE12'));
                dueInvoice.paidAmount = Decimal.valueOf(attributeMap.get('ATTRIBUTE13'));
                dueInvoice.dueDate = attributeMap.get('ATTRIBUTE14');
                dueInvoice.trxType = attributeMap.get('ATTRIBUTE15');
                dueInvoice.dueDays = Integer.valueOf(attributeMap.get('ATTRIBUTE16'));
                dueInvoice.invoiceNumber = attributeMap.get('ATTRIBUTE17');
                dueInvoice.trxId = attributeMap.get('ATTRIBUTE18');

                Date dueDate = CustomerCommunityUtils.parseIpmsDateString(dueInvoice.dueDate);
                dueInvoice.dateDueDate = dueDate;
                dueInvoice.isNextQ1Invoice = isNextQuarter1Date(dueInvoice.dateDueDate);
                dueInvoice.quarter = (dueDate.month()/3) + 1;

                System.debug('dueDate: ' + dueDate);
                
                dueResult.dueAmountExceptCurrentQuarter = sumOfDueAmountExceptCurrentQuarter(dueDate, dueInvoice.dueRemaining);    

                if (dueDate != NULL) {
                    Integer dueMonth = dueDate.month();
                    Date nextMonthDate = today.addMonths(1);

                    System.debug('currentMonth: ' +currentMonth);
                    System.debug('currentYear: '+currentYear);
                    System.debug('dueMonth: ' + dueMonth);
                    System.debug('nextMonthDate: ' + nextMonthDate);
                    System.debug('byPassDueDateCheck : ' + byPassDueDateCheck);

                    //PN : (dueInvoice.dueRemaining != null && dueInvoice.dueRemaining > 0) - this check in the below IF condition is added to ByPass this IF check for current month due as per earlier logic - 11-05-2020
                    if (dueDate < today
                        || (dueDate.month() == currentMonth && dueDate.year() == currentYear)
                        || (byPassDueDateCheck && dueInvoice.dueRemaining != null && dueInvoice.dueRemaining > 0)
                        || (Math.Mod(currentMonth, 3) == 0
                            && dueMonth == nextMonthDate.month() && dueDate.year() == nextMonthDate.year()
                        )
                    ) {
                        totalDueAmount += dueInvoice.dueRemaining;
                        if (nextDueDate == NULL || dueDate < nextDueDate) {
                            nextDueDate = dueDate;
                        }
                        System.debug('dueInvoice before adding to list: ' + dueInvoice);
                        dueResult.lstDueInvoice.add(dueInvoice);
                    }
                }
            }
        }
        if (nextDueDate != NULL) {
            dueResult.nextDueDate = FmcUtils.formatAsIpmsDate(nextDueDate);
        }
        dueResult.totalDueAmount =  String.valueOf(totalDueAmount.setScale(2));


        System.debug('dueResult returning: ' + dueResult);
        return dueResult;
    }

    public static DueInvoicesResult getAllInvoices(String registrationId, String partyId, String propertyName) {

        if (String.isBlank(registrationId) && String.isBlank(partyId) && String.isBlank(propertyName)) {
            return NULL;
        }
        sumAmount = 0.0;
        IpmsRestServices__c settings = IpmsRestServices__c.getInstance();
        if (settings == NULL
            || String.isBlank(settings.BaseUrl__c) || String.isBlank(settings.Username__c) || String.isBlank(settings.Password__c)
        ) {
            return NULL;
        }

        HttpRequest req = new HttpRequest();


        req.setEndpoint(settings.BaseUrl__c + '/XXDC_PROCESS_SERVICE_WS/retrieve/');
        System.debug('req.getEndpoint = ' + req.getEndpoint());


        String requestBody =
            '{' +
              '"RETRIEVE_Input": {' +
                '"RESTHeader": {' +
                  '"Responsibility": "ONT_ICP_SUPER_USER",' +
                  '"RespApplication": "ONT",' +
                  '"SecurityGroup": "STANDARD",' +
                  '"NLSLanguage": "AMERICAN"' +
                '},' +
                '"InputParameters": {' +
                  '"P_REQUEST_NUMBER": '
        ;

        requestBody += ('"' + String.valueOf(Datetime.now().getTime()) + '"');

        requestBody +=
        ',"P_SOURCE_SYSTEM": "SFDC",' +
              '"P_REQUEST_NAME": ';

        requestBody += ('"' + GET_INV_INFO_LIST + '"');

        requestBody += ',"P_REQUEST_MESSAGE": ';

        requestBody +=
            '{' +
                '"PARAM_ID":"' + (String.isBlank(registrationId) ? '' : registrationId) + '",' +
                '"ATTRIBUTE1":"' + (String.isBlank(partyId) ? '' : partyId) + '",' +
                '"ATTRIBUTE2":"' + (String.isBlank(propertyName) ? '' : propertyName) + '"' +
            '}';

        requestBody += '}}}';

        System.debug('requestBody = ' + requestBody);

        req.setBody(requestBody);

        System.debug('req.getBody = ' + req.getBody());

        req.setHeader(
            'Authorization' ,
            'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(settings.Username__c + ':' + settings.Password__c))
        );
        req.setTimeout(Integer.valueOf(settings.Timeout__c == NULL ? 120000 : settings.Timeout__c));


        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Content-Length', '0');
        req.setHeader('ACCEPT','application/json');

        req.setMethod('POST');


        HttpResponse res = new Http().send(req);
        System.debug('Status Code = ' + res.getStatusCode());
        System.debug('body = ' + res.getBody());

        DueInvoicesResponse dueResponse = (DueInvoicesResponse) JSON.deserialize(
            res.getBody(), DueInvoicesResponse.class
        );

        DueInvoicesResult dueResult;

        if (dueResponse == NULL) {
            return dueResult;
        }

        dueResult = new DueInvoicesResult(dueResponse);

        if (dueResponse.OutputParameters.X_RESPONSE_MESSAGE == NULL
            || dueResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM == NULL
        ) {
            return dueResult;
        }

        dueResult.lstDueInvoice = new List<DueInvoice>();

        Date nextDueDate;
        Decimal totalDueAmount = 0.00;

        Date today = Date.today();
        Integer currentMonth = today.month();
        Integer currentYear = today.year();

        dueResult.totalPenaltyAmount = 0;
        dueResult.paidPenaltyAmount = 0;
        dueResult.pendingPenaltyAmount = 0;

        for(Map<String, String> attributeMap : dueResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM
        ) {
            DueInvoice dueInvoice;
            if (attributeMap != NULL) {
                dueInvoice = new DueInvoice();
                dueInvoice.registrationId = attributeMap.get('ATTRIBUTE1');
                dueInvoice.unitName = attributeMap.get('ATTRIBUTE2');
                dueInvoice.projectName = attributeMap.get('ATTRIBUTE3');
                dueInvoice.customerId = attributeMap.get('ATTRIBUTE4');
                dueInvoice.orgId = attributeMap.get('ATTRIBUTE5');
                dueInvoice.partyId = attributeMap.get('ATTRIBUTE6');
                dueInvoice.partyName = attributeMap.get('ATTRIBUTE7');
                dueInvoice.trxNumber = attributeMap.get('ATTRIBUTE8');
                dueInvoice.creationDate = attributeMap.get('ATTRIBUTE9');
                dueInvoice.callType = attributeMap.get('ATTRIBUTE10');
                dueInvoice.dueRemaining = Decimal.valueOf(attributeMap.get('ATTRIBUTE11'));
                dueInvoice.chargeAmount = Decimal.valueOf(attributeMap.get('ATTRIBUTE12'));
                dueInvoice.paidAmount = Decimal.valueOf(attributeMap.get('ATTRIBUTE13'));
                dueInvoice.dueDate = attributeMap.get('ATTRIBUTE14');
                dueInvoice.trxType = attributeMap.get('ATTRIBUTE15');
                dueInvoice.dueDays = Integer.valueOf(attributeMap.get('ATTRIBUTE16'));
                dueInvoice.invoiceNumber = attributeMap.get('ATTRIBUTE17');
                dueInvoice.trxId = attributeMap.get('ATTRIBUTE18');

                Date dueDate = CustomerCommunityUtils.parseIpmsDateString(dueInvoice.dueDate);
                dueInvoice.dateDueDate = dueDate;
                dueInvoice.isNextQ1Invoice = isNextQuarter1Date(dueInvoice.dateDueDate);
                dueInvoice.quarter = (dueDate.month()/3) + 1;

                dueResult.dueAmountExceptCurrentQuarter = sumOfDueAmountExceptCurrentQuarter(dueDate, dueInvoice.dueRemaining);    

                if ('Interest'.equalsIgnoreCase(dueInvoice.callType)) {
                    dueResult.totalPenaltyAmount += dueInvoice.chargeAmount;
                    dueResult.paidPenaltyAmount += dueInvoice.paidAmount;
                    dueResult.pendingPenaltyAmount += dueInvoice.dueRemaining;
                }

                if (dueDate != NULL) {
                    Integer dueMonth = dueDate.month();
                    Date nextMonthDate = today.addMonths(1);
                    if (dueDate < today
                        || (dueDate.month() == currentMonth && dueDate.year() == currentYear)
                        || (Math.Mod(currentMonth, 3) == 0
                            && dueMonth == nextMonthDate.month() && dueDate.year() == nextMonthDate.year()
                        )
                    ) {
                        totalDueAmount += dueInvoice.dueRemaining;
                        if (nextDueDate == NULL || dueDate < nextDueDate) {
                            nextDueDate = dueDate;
                        }
                        dueResult.lstDueInvoice.add(dueInvoice);
                    }
                }
            }
        }
        if (nextDueDate != NULL) {
            dueResult.nextDueDate = FmcUtils.formatAsIpmsDate(nextDueDate);
        }
        dueResult.totalDueAmount =  String.valueOf(totalDueAmount.setScale(2));

        return dueResult;
    }

    public static Decimal sumOfDueAmountExceptCurrentQuarter(Date duedate, Decimal dueAmount) {
        System.debug('-->> duedate: '+ duedate);
        System.debug('-->> dueAmount: '+ dueAmount);
       
        Date todayDateValue = System.today();
        System.debug('-->> todayDateValue: '+ todayDateValue);
        Integer thisQuarterNo = (todayDateValue.month()/3);
        Date startDateThisQuarter;
        startDateThisQuarter = Date.newInstance(todayDateValue.year(), 3 * thisQuarterNo - 2, 1);
        System.debug('-->> startDateThisQuarter: '+ startDateThisQuarter);
        if(dueDate < startDateThisQuarter){
            System.debug('-->> Inside If: ');
            sumAmount += dueAmount;
            System.debug('-->> sumAmount: '+ sumAmount);
        }
        return sumAmount;
    }

    public static String createReceipt(CreateReceiptRequest request) {
        if (request == NULL) {
            return NULL;
        }

        List<FM_Receipt__c> lstFmReceipt = [
            SELECT  Id
            FROM    FM_Receipt__c
            WHERE   Name = :request.receiptNumber
        ];

        HttpRequest req = new HttpRequest();
        req.setEndpoint(SF_REST_BASEURL + CREATE_RECEIPT);
        System.debug(req.getEndpoint());

        req.setHeader('Content-Type', 'application/json');
        req.setBody(JSON.serialize(request));
        //req.setBody('{"sourceSystem": "' + request.sourceSystem +'","extRequestNumber": "' + request.extRequestNumber +'","businessGroup": "FM","partyId":' + request.partyId + ',"receiptNumber":"' + request.receiptNumber + '","paidAmount":' + request.paidAmount + ',"currencyCode":"' + request.currencyCode + '","applyReceipt":"' + request.applyReceipt + '","comments":"' + request.comments + '","applicationLines": ' + JSON.serialize(request.applicationLines) + '}');
        System.debug('req.body = ' + req.getBody());

        req.setMethod('POST');
        req.setTimeout(120000);

        System.debug('req : '+req);

        HttpResponse res = new Http().send(req);
        System.debug('Status Code = ' + res.getStatusCode());
        System.debug('res.body = ' + res.getBody());

        String createReceiptLog = 'Create Receipt Request:\n' + req.getEndpoint() + '\n' + req.getBody()
                + '\n\nResponse:' + res.getBody();
        //insert new Error_Log__c(
        //    Process_Name__c = 'Create Receipt Log',
        //    FM_Receipt__c = lstFmReceipt.isEmpty() ? NULL : lstFmReceipt[0].Id,
        //    Account__c = CustomerCommunityUtils.customerAccountId,
        //    Error_Details__c = 'Log (NOT ERROR)\n\nReq: ' + req.getEndpoint() + ' ' + req.getBody() + '\n\nres:' + res.getBody()
        //);

        String createReceiptUrl = fetchResponseUrl(res.getBody());
        System.debug('createReceiptUrl: '+createReceiptUrl);
        if (String.isBlank(createReceiptUrl)) {
            Error_Log__c errorLog = new Error_Log__c(
                Process_Name__c = 'Create Receipt',
                FM_Receipt__c = lstFmReceipt.isEmpty() ? NULL : lstFmReceipt[0].Id,
                Account__c = CustomerCommunityUtils.customerAccountId,
                Error_Details__c = 'Req: ' + req.getEndpoint() + ' ' + req.getBody() + '\n\nres:' + res.getBody()
            );
            if (logErrors) {
                insert errorLog;
            } else {
                lstErrorLog.add(errorLog);
            }
            
            return createReceiptUrl;
        }

        req = new HttpRequest();
        req.setEndpoint(createReceiptUrl);
        req.setMethod('GET');
        res = new Http().send(req);
        System.debug('res.body = ' + res.getBody());

        createReceiptLog += '\n\nReceipt PDF Request: ' + req.getEndpoint() + ' ' + req.getBody()
                + '\n\nResponse:' + res.getBody();

        Error_Log__c errorLog = new Error_Log__c(
            Process_Name__c = 'Create Receipt Log',
            FM_Receipt__c = lstFmReceipt.isEmpty() ? NULL : lstFmReceipt[0].Id,
            Account__c = CustomerCommunityUtils.customerAccountId,
            Error_Details__c = 'Log (Not ERROR)\n\n' + createReceiptLog
        );
        if (logErrors) {
            insert errorLog;
        } else {
            lstErrorLog.add(errorLog);
        }

        String receiptUrl = fetchResponseUrl(res.getBody());
        if (String.isNotBlank(receiptUrl)) {
            return receiptUrl;
        } else {
            return createReceiptUrl;
        }
    }

    public static void insertErrorLogs() {
        if (logErrors) {
            insert lstErrorLog;
        }
    }

    public static HttpResponse callout(String relativeRequestUrl) {
        HttpRequest req = new HttpRequest();
        req.setEndpoint(SF_REST_BASEURL + relativeRequestUrl);
        System.debug(req.getEndpoint());
        req.setMethod('GET');
        req.setTimeout(120000);
        System.debug('req : '+req);
        HttpResponse response = new Http().send(req);
        System.debug('Status Code = ' + response.getStatusCode());
        System.debug('body = ' + response.getBody());
        return new Http().send(req);
    }

    public static Boolean isNextQuarter1Date(Date dueDate) {
        Date today = Date.today();
        if (dueDate.month() <= 3) {
            if (today.month() <= 3) {
                if (dueDate.year() == today.year()) {
                    return true;
                }
            } else if (dueDate.year() > today.year()) {
                return true;
            }
        }
        return false;
    }

    @testVisible
    private static String fetchResponseUrl(String result) {
        Response response = (Response) Json.deserialize(result, Response.class);

        if (response == NULL) {
            return NULL;
        }
        if (response.complete == NULL
            || !response.complete
            || response.actions == NULL
            || response.actions.isEmpty()
        ) {
            return NULL;
        }

        return response.actions[0].url;
    }


    public class Response {
        public String           responseId;   //142215
        public String           responseTime; //Tue Jul 17 16:19:15 GST 2018
        public String           requestName;
        public String           extRequestName;
        public String           responseMessage;
        public Receipt[]        responseLines;
        public Action[]         actions;
        public ErrorMessage[]   errorMessage;
        public Boolean          complete;
    }

    public class Receipt {
        public Integer  cashReceiptId;   //647371
        public String   receiptType;  //CASH
        public Integer  partyId; //142215
        public String   customerName; //Ahlam Mohamed Ebrahim AlHosani
        public String   currencyCode; //AED
        public Integer  enteredAmount;   //20887
        public Integer  unAppliedAmount; //0
        public Integer  appliedAmount;   //20887
        public Integer  functionalAmount;    //20887
        public String   receiptNumber;    //1101138972
        public String   receiptReference; //AL HILAL CHQ NO 000174
        public String   receiptDate;  //19-Jan-2015
        public Integer  orgId;   //112
        public String   businessGroup;    //FM
        public String   paymentMethod;    //Cheque/Cash to be Remitted
        public String   comment;  //MB/15/1511
        public Action[] actions;
    }

    public class Action {
        public String action;   //GENERATE_RECEIPT
        public String method;   //GET
        public String url;  //https://ptctest.damacgroup.com/DCOFFEE/report/RECEIPT/PUBLIC_APP/NO_KEY/RECEIPT_ID/647371
    }

    class ErrorMessage {
        public String code; //E
        public String message;  //[17SEP-11:14]Deriving the Property Id for Project = LSA[ORA-01403: no data found]
    }



    public class RetrieveRequest {
        public RETRIEVE_Input RETRIEVE_Input;

        public RetrieveRequest() {
            RETRIEVE_Input = new RETRIEVE_Input();
        }
    }

    class RETRIEVE_Input {
        public RESTHeader RESTHeader;
        //public String RESTHeader;
        public InputParameters InputParameters;

        public RETRIEVE_Input() {
            RESTHeader = new RESTHeader();
            //RESTHeader = '{"Responsibility": "ONT_ICP_SUPER_USER","RespApplication": "ONT","SecurityGroup": "STANDARD","NLSLanguage": "AMERICAN"}';
            InputParameters = new InputParameters();
        }
    }
    class RESTHeader {
        public String Responsibility;   //ONT_ICP_SUPER_USER
        public String RespApplication;  //ONT
        public String SecurityGroup;    //STANDARD
        public String NLSLanguage;  //AMERICAN

        public RESTHeader() {
            Responsibility = 'ONT_ICP_SUPER_USER';
            RespApplication = 'ONT';
            SecurityGroup = 'STANDARD';
            NLSLanguage = 'AMERICAN';
        }
    }
    class InputParameters {
        public String P_REQUEST_NUMBER; //A2232
        public String P_SOURCE_SYSTEM;  //SFDC
        public String P_REQUEST_NAME;   //GET_INV_DUE_LIST
        public P_Request_Message P_REQUEST_MESSAGE;
        //public String P_REQUEST_MESSAGE;

        public InputParameters() {
            P_SOURCE_SYSTEM = 'SFDC';
            P_REQUEST_MESSAGE = new P_Request_Message();
            //P_REQUEST_MESSAGE = '';
        }
    }
    class P_REQUEST_MESSAGE {
        public String PARAM_ID; //
        public String ATTRIBUTE1;   //
        public String ATTRIBUTE2;   //WAVES TOWER
    }


    public class DueInvoicesResponse {
        public OutputParameters OutputParameters;
    }

    @testVisible
    public class OutputParameters {
        public ResponseMessage X_RESPONSE_MESSAGE;
        public String X_RETURN_STATUS;  //S
        public String X_RETURN_MESSAGE; //Total Records Fetched...15
    }
    @testVisible
    public class ResponseMessage {
        //public ResponseMessageItem[] X_RESPONSE_MESSAGE_ITEM;
        public List<Map<String, String>> X_RESPONSE_MESSAGE_ITEM;
    }
    @testVisible
    public class ResponseMessageItem {
        public String ATTRIBUTE1;   //39213
        public String ATTRIBUTE2;   //BD4/13/1318
        public String ATTRIBUTE3;   //THE COSMOPOLITAN
        public String ATTRIBUTE4;   //3797
        public String ATTRIBUTE5;   //112
        public String ATTRIBUTE6;   //3246
        public String ATTRIBUTE7;   //Faisal Ali Habib Sajwani
        public String ATTRIBUTE8;   //BD4/13/1318Q4-18
        public String ATTRIBUTE9;   //12-MAR-2018
        public String ATTRIBUTE10;  //Maintenance Charges
        public String ATTRIBUTE11;  //.04
        public String ATTRIBUTE12;  //9972.91
        public String ATTRIBUTE13;  //9972.87
        public String ATTRIBUTE14;  //21-FEB-2018
        public String ATTRIBUTE15;  //INV
        public String ATTRIBUTE16;  //167
        public String ATTRIBUTE17;  //167
    }


    public class DueInvoicesResult {
        public String           status                          {get; set;}
        public String           message                         {get; set;}
        public String           nextDueDate                     {get; set;}
        public String           totalDueAmount                  {get; set;} // total outstanding FM Due Amount/ Service Charges
        public Decimal          totalPenaltyAmount              {get; set;}
        public Decimal          paidPenaltyAmount               {get; set;}
        public Decimal          pendingPenaltyAmount            {get; set;}
        public List<DueInvoice> lstDueInvoice                   {get; set;}
        public Decimal          dueAmountExceptCurrentQuarter   {get; set;}

        public DueInvoicesResult(DueInvoicesResponse response) {
            this.status = response.OutputParameters.X_RETURN_STATUS;
            this.message = response.OutputParameters.X_RETURN_MESSAGE;
        }
    }

    public class DueInvoice {
        public String   registrationId  {get; set;}
        public String   unitName        {get; set;}
        public String   projectName     {get; set;}
        public String   customerId      {get; set;}
        public String   orgId           {get; set;}
        public String   partyId         {get; set;}
        public String   partyName       {get; set;}
        public String   trxNumber       {get; set;}
        public String   creationDate    {get; set;}
        public String   callType        {get; set;}
        public Decimal  dueRemaining    {get; set;}
        public Decimal  chargeAmount    {get; set;}
        public Decimal  paidAmount      {get; set;}
        public String   dueDate         {get; set;}
        public Date     dateDueDate     {get; set;}
        public String   trxType         {get; set;}
        public Integer  dueDays         {get; set;}
        public String   invoiceNumber   {get; set;}
        public String   trxId           {get; set;}
        public Boolean  isNextQ1Invoice {get; set;}
        public Integer  quarter         {get; set;}
    }

    public class CreateReceiptRequest {
        public String               sourceSystem        {get; set;} //SFDC
        public String               extRequestNumber    {get; set;} //RECEIPT-12345
        public String               businessGroup       {get; set;} //FM
        public String               partyId             {get; set;} //3893
        public String               receiptNumber       {get; set;} //Test-0910181
        public Decimal              paidAmount          {get; set;} //4000
        public String               currencyCode        {get; set;} //AED
        public String               applyReceipt        {get; set;} //Yes
        public String               comments            {get; set;} //This is a general Payment done
        public String               customerTrxId       {get; set;} //4237227
        public String               discountAttribute   {get; set;} //10% Discount
        public ApplicationLine[]    applicationLines    {get; set;}
    }

    public class ApplicationLine {
        public String   registrationId               {get; set;}  //25
        public Decimal amountToApply                {get; set;}   //2000
    }

}