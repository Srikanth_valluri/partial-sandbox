@isTest
private class FmNotificationServiceTest {

    @isTest
    static void notificationsTest() {
        insert new FMNotificationServices__c(
            Name = 'Violation Notice Notifications',
            NotificationServiceClass__c = 'ViolationNoticeNotificationService'
        );

        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        FM_Case__c violationCase = new FM_Case__c(
            Account__c = portalAccountId,
            Origin__c = 'Walk-In',
            Status__c = 'Submitted',
            RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Violation Notice').getRecordTypeId()
        );
        insert violationCase;
        SR_Attachments__c violationNotice = new SR_Attachments__c(
            FM_Case__c =violationCase.Id,
            Document_Type__c = 'Violation Notice',
            Notifications_Read__c = false
        );
        insert violationNotice;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        Test.startTest();
            NotificationService.Notification notificationRequest = new NotificationService.Notification();
            notificationRequest.accountId = portalAccountId;
            List<NotificationService.Notification> lstNotification = FmNotificationService.getNotifications(
                notificationRequest
            );
            lstNotification = FmNotificationService.markRead(lstNotification);
            lstNotification = FmNotificationService.markRead(NULL);
        Test.stopTest();
    }

}