@isTest 
global class MockHttpCalloutClass1 implements HttpCalloutMock{

    global HTTPResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type','application/json');
        res.setBody('{"OutputParameters": {"@xmlns": "http://xmlns.oracle.com/apps/ont/rest/XXDC_PROCESS_SERVICE_WS/retrieve/","@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance","X_RESPONSE_MESSAGE": {"X_RESPONSE_MESSAGE_ITEM": [{"ATTRIBUTE1": "On Completion"}, {"ATTRIBUTE1": "On or Before"}]}}}');
     return res;
     }
     }