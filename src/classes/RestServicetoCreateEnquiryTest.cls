/**********************************************************************************************************************
* Name               : RestServicetoCreateEnquiryTest                                                                 *
* Description        : This is a test class for RestServicetoCreateEnquiry.                                           *
* Created Date       : 01/31/2017                                                                                     *
* Created By         : PWC                                                                                            *
* --------------------------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                                  *
* 1.0         Charan        01/31/2017      Initial Draft.                                                            *
**********************************************************************************************************************/
@isTest 
private class RestServicetoCreateEnquiryTest{

    @isTest static void testInquiryCreation() {
        String campaignObjectPrefix = Campaign__c.sobjecttype.getDescribe().getKeyPrefix();
        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';      
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        Campaign__c sCamp = new Campaign__c();
        sCamp.Campaign_Name__c= 'TestDTPCamp';
        sCamp.RecordTypeId='0120Y000000E19A';
        sCamp.Start_Date__c=date.Today();
        sCamp.End_Date__c = date.Today();
        sCamp.Marketing_End_Date__c =date.Today();
        sCamp.Marketing_Start_Date__c=date.Today();
        insert sCamp;
            RestServiceToCreateEnquiry.ResponseHandler results;
              results= RestServiceToCreateEnquiry.createEnquiry('firstname', 'lastname', 'email', 'title', 'telephone', 'Source', 
                                                         'UTMSource', 'UTMMedium', 'UTMCampaign', 'HeardOfDAMAC', 'message',
                                                         'mobileCode', 'city', 'nationality', 'country',sCamp.Id,'phonenumbertype','validity','Reachable', '12345');
        Test.stopTest();  
    }
}// End of class.