/**************************************************************************************************
* Name               : AsyncSalesAuditUpdate 
* Description        : This is the queuable webservice class for Sales Audit Approval
                                   
* Created Date       : 09/July/2017                                                                 
* Created By         : Alok Chauhan-DAMAC                                                      
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE                                                              
* 1.0         Alok-DAMAC      09/July/2017                                                        
**************************************************************************************************/

global class AsyncSalesAuditUpd implements Queueable, Database.AllowsCallouts {
    public List<id> recordids = new List<id>();
    public string servicetype;
    public string sAuditApprovalFlag;
    
    //Defining the constructor
    public AsyncSalesAuditUpd(List<Id> rids, string svctype,string sAuditApproval){
        recordids=rids;
        servicetype=svctype; 
        sAuditApprovalFlag=sAuditApproval;
    
    }
    
    public void execute(QueueableContext context) {
    if(servicetype=='SalesAuditUpdate')
    {sendRegnUpdate(recordids,'UPDATE_REGISTRATION',sAuditApprovalFlag);}
    }
    
    global static string endurl;
    global static string usrname;
    global static string pwd; 
    /**********************Getting the IPMS integration setting*********************************/ 
    
    public static void getIPMSsetting(String servicename){
        system.debug('***SSS***'+servicename);
        IPMS_Integration_Settings__mdt ipms= [select id,Endpoint_URL__c,Password__c,  Username__c from IPMS_Integration_Settings__mdt where DeveloperName = :servicename];
        endurl=ipms.Endpoint_URL__c;
        usrname=ipms.Username__c;      
        pwd=PasswordCryptoGraphy.DecryptPassword(ipms.Password__c);
        system.debug('***WS_URL***'+endurl);  
    }
    
    /**********************Method to Prepare the Request For WS*********************************/  
    public static string sendSalesAuditUpdate(List<id> regids,String reqType,String sAudit){
    
       string bookUnitQuery = IPMS_WSHelper.getCreatableFieldsSOQL('Booking_Unit__c');
       bookUnitQuery+=' where Booking__c in :regids';
       List<Booking_Unit__c> sBUlist= database.query(bookUnitQuery); 
       
       String reqno='';
       if(sBUlist.size()>0)
       {reqno ='UpdReg-'+sBUlist[0].name+'-'+IPMS_WSHelper.GetFormattedDateTime(system.now());}
       system.debug('REQ NO==='+reqno);
       system.debug('AuditFlag==='+sAudit);
       
       List<Id> srIds = new List<Id>();
       for(Booking_Unit__c sBU : sBUlist)
       {
           srIds.add(sBU.SR_Id__c);
       }
        string body = '';
        body+= IPMS_WSHelper.sSoaHeader('IPMS_webservice',usrname,pwd);         
            body+='<soapenv:Body>';
                body+='<proc:InputParameters>';
                    body+='<proc:P_REQUEST_NUMBER>'+reqno+'</proc:P_REQUEST_NUMBER>';
                    body+='<proc:P_SOURCE_SYSTEM>SFDC</proc:P_SOURCE_SYSTEM>';
                    body+='<proc:P_REQUEST_NAME>UPDATE_REGISTRATION</proc:P_REQUEST_NAME>';
                    body+='<proc:P_REQUEST_MESSAGE>';
                    for(Booking_Unit__c sBU :sBUlist){
                        body+='<proc:P_REQUEST_MESSAGE_ITEM>';
                            body+='<proc:PARAM_ID>'+sBU.id+'</proc:PARAM_ID>';                            
                            body+='<proc:ATTRIBUTE1>'+sBU.Registration_ID__c+'</proc:ATTRIBUTE1>';
                            body+='<proc:ATTRIBUTE2>UPDATE_AGENT_AUDIT_FLAG</proc:ATTRIBUTE2>';
                            body+='<proc:ATTRIBUTE17>'+sAudit+'</proc:ATTRIBUTE17>';
                        body+='</proc:P_REQUEST_MESSAGE_ITEM>';
                    }
                    body+='</proc:P_REQUEST_MESSAGE>';
                body+='</proc:InputParameters>';
            body+='</soapenv:Body>';
        body+='</soapenv:Envelope>';
        
        return body.trim();
        
    }
/**********************Callout for Sales Audit Update*********************************/
     webservice static void sendRegnUpdate(List<id> regids,String sUpdType,string sAudit){
        
        getIPMSsetting('IPMS_webservice');
        HTTPRequest req = new HTTPRequest();
        req.setMethod('POST');
        
        String reqXML = sendSalesAuditUpdate(regids,sUpdType,sAudit);
        reqXML = reqXML.replaceAll('null', '');
        reqXML = reqXML.trim();
        req.setbody(reqXML);
        req.setEndpoint(endurl);
        System.debug('>>>>>>>>reqXML>>>>>>>>'+reqXML);
        req.setHeader('Content-Type','text/xml');
        req.setTimeout(120000);
        HTTP http = new HTTP();
        if(!Test.isrunningTest()){
            try{
                HTTPResponse res = http.send(req);
                
                System.debug('>>>>>Response>>>>>>'+res.getbody());
                
                parseRegnUpdateResponse(res.getBody());
            }
            catch(Exception ex) {
                System.debug('Callout error: '+ ex);
                Log__c objLog = new Log__c();
                objLog.Description__c =sUpdType+'--Ids=='+regids+'-Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage();
                objLog.Type__c = 'Webservice Callout For Sales Audit Updation';
                insert objLog;             
            }    
        }
    }
    
     /**********************Parsing the response received for Sales Audit Update request*********************************/

    public static void parseRegnUpdateResponse(string body){
        
        String reqId,sBookUnits,status,stageId,doclist = '';
        
        List<NSIBPM__SR_Doc__c> sRDocList = new List<NSIBPM__SR_Doc__c>();
        
        List<String> sBUids = new List<String>();
        Map<string,String> statusMap = new Map<string,string>();
        
        DOM.Document xmlDOC = new DOM.Document();
        xmlDOC.load(body);
        DOM.XMLNode rootElement = xmlDOC.getRootElement();
        
        
        for(Dom.XMLNode child1: rootElement.getChildElements()){
            for(Dom.XMLNode child2: child1.getChildElements()){
                for(Dom.XMLNode child3: child2.getChildElements()){
                    for(Dom.XMLNode child4: child3.getChildElements()){
                       for(Dom.XMLNode child5: child4.getChildElements()){
                            System.debug('>>>>>>>>child5>>>>>>'+child5.getName());
                            if(child5.getName()=='PARAM_ID') 
                            {
                            sBookUnits=child5.getText();
                            sBUids.add(sBookUnits);   
                            }
                            if(child5.getName()=='PROC_MESSAGE')
                            {status=child5.getText();  }                           
                            
                       }                          
                       Statusmap.put(sBookUnits,status);                        
                       
                    }
                }
            }
        }
        
 /**********************Callout for Sales Audit Updation*********************************/      
        List<Booking_Unit__c> bookingUnitLst= [select id,Booking__c,SR_Status_Code__c,Booking__r.Deal_SR__c,Registration_ID__c,IPMS_Status__c,SOA_Request_ID__c,SOA_Stage_Id__c,Unit_Location_ID__c from Booking_Unit__c where Status__c!='Removed' and id in:Statusmap.keyset()];
        if(bookingUnitLst.size()>0){
            for(Booking_Unit__c sBU :bookingUnitLst){     
                sBU.IPMS_Status__c='Status Update -'+sBU.SR_Status_Code__c+'-'+statusMap.get(sBU.id);                
            }
            update bookingUnitLst;
        }
        
    }
    
}