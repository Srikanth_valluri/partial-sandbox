@RestResource(urlMapping='/SaveScheduleAppointment/*')
 Global class SaveScheduleAppointment
 {
     @HtTPPost
    Global static Calling_List__c ScheduleAppointment(AppointmentWrapper codCaseWrapper)
    {
        String hoTeamEmail;
         String strProjectName = codCaseWrapper.BookingUnit.substringBefore('/');
            if( String.isNotBlank( strProjectName ) ) {
                Location__c objLoc = [ SELECT Id
                                            , Name
                                            , Handover_Team_notification_Email__c 
                                         FROM Location__c 
                                        WHERE Name = :strProjectName ];
                if( objLoc != null ) {
                   // buildingId = objLoc.Id ;
                    hoTeamEmail = objLoc.Handover_Team_notification_Email__c;
                  }
             }
    
        Group objGroup, objGroupArab, objGroupnNonArab;
        map<Id, list<GroupMember>> mapOwnerIdGroupMember = new map<Id, list<GroupMember>>();
        map<String, Calling_List__c > mapProcessCL = new map<String, Calling_List__c >();
        List<GroupMember> lstGroupMember = new List<GroupMember>();
        list<Calling_List__c> lstAppointmentCL = new list<Calling_List__c>();
        list<Event> lstEvent = new list<Event>();
        List<Group> lstQueues = [select Id,Name
                                     from Group
                                     where Type = 'Queue'
                                     AND (name= 'Non Elite Arabs Queue'
                                     OR Name = 'Non Elite Non Arabs Queue'
                                     OR Name = 'Handover Queue')
                                     LIMIT 3];

        for(Group grp : lstQueues) {
            if(grp.Name == 'Non Elite Arabs Queue') {
                objGroupArab = grp;
            }
            else if(grp.Name == 'Non Elite Non Arabs Queue') {
                objGroupnNonArab = grp;
            } else if (grp.Name == 'Handover Queue') {
                objGroup = grp;
            }
        }
         system.debug('!!!!!!!lstQueues'+lstQueues);
           for (GroupMember objGM : [Select id,GroupId, Group.name, UserOrGroupId from GroupMember where GroupId IN: lstQueues]) {
           system.debug('!!!!!!!objGM'+objGM);
            if (mapOwnerIdGroupMember.containsKey(objGM.GroupId)) {
                list<GroupMember> lstGM = mapOwnerIdGroupMember.get(objGM.GroupId);
                lstGM.add(objGM);
                mapOwnerIdGroupMember.put(objGM.GroupId, lstGM);
            } else {
                mapOwnerIdGroupMember.put(objGM.GroupId, new list<GroupMember> {objGM});
            }
        }
        String acId=codCaseWrapper.AccountID.substring(0, codCaseWrapper.AccountID.length() - 3);
        
         user u=[select id from user where profile.Name='System Administrator' limit 1];
         list<Booking_Unit__c> bu=[select id,Registration_ID__c,name,Unit_Name__c from Booking_Unit__c where Unit_Name__c=:codCaseWrapper.BookingUnit and Account_Id__c=:acId];
         
         Account ac=[select id,name,Customer_Category__c,Customer_Type__c,Primary_CRE__c,Secondary_CRE__c,Tertiary_CRE__c,Primary_Language__c,Email__pc,Email__c,isPersonAccount from Account where id=:codCaseWrapper.AccountID];
       
              Calling_List__c objCallList = new Calling_List__c();
                    objCallList.Account__c = ac.id;
                    if(bu.size()>0)
                    objCallList.Booking_Unit__c = bu[0].id;
                    objCallList.Appointment_Status__c = 'Confirmed';
                    objCallList.RecordTypeId =Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get(codCaseWrapper.RecordType).getRecordTypeId(); 
             if((codCaseWrapper.ServiceType!='Handover' || codCaseWrapper.ServiceType!='01-Handover') && ac.Customer_Category__c=='Ellite' || ac.Customer_Category__c=='Top Broker'|| ac.Customer_Category__c=='Documentation'){
                    Group g=[select Id,name from Group where Type = 'Queue' and name='Request for Elite'];
                      // objCallList.Appointment_Status__c = 'Confirmed';
                      objCallList.ownerId = g.id;  
                 } else if (ac.Primary_Language__c == 'Arabic') {
                            objCallList.ownerId = objGroupArab.Id;
                        } else if (ac.Primary_Language__c != 'Arabic') {
                            objCallList.ownerId = objGroupnNonArab.Id;
                        }
               
                   
                    if ((codCaseWrapper.ServiceType=='Handover' || codCaseWrapper.ServiceType=='01-Handover')&&  ( codCaseWrapper.SubProcessName == 'Unit Viewing' || codCaseWrapper.SubProcessName == 'Key Handover' || ac.Customer_Category__c=='Documentation')) {
                        objCallList.Appointment_Status__c = 'Requested';
                        objCallList.ownerId = objGroup.Id;
                    } else if (codCaseWrapper.SubProcessName == 'Documentation' ) {
                        if (ac.Primary_CRE__c != null) {
                            objCallList.ownerId = ac.Primary_CRE__c;
                        } else if (ac.Secondary_CRE__c != null) {
                            objCallList.ownerId = ac.Secondary_CRE__c;
                        } else if (ac.Tertiary_CRE__c != null) {
                            objCallList.ownerId = ac.Tertiary_CRE__c;
                        }
                    }
                  
                    objCallList.Service_Type__c = codCaseWrapper.ServiceType;
                    objCallList.Sub_Purpose__c =codCaseWrapper.SubProcessName;
                    objCallList.Appointment_Date__c = codCaseWrapper.AppointmentDate;
                    objCallList.Appointment_Origin__c= codCaseWrapper.origin;
                   // objCallList.Appointment_Status__c = 'Requested';
                    objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList.Appointment_Origin__c='Mobile';
                    if(bu.size()>0)
                    objCallList.Registration_ID__c = bu[0].Registration_ID__c;
                    String Slot = codCaseWrapper.TimeSlot.substringBefore('-');
                    String EndTime = codCaseWrapper.TimeSlot.substringAfter('-');
                    objCallList.Appointment_Slot__c = Slot;
                    if (ac.isPersonAccount == true) {
                        objCallList.Account_Email__c = ac.Email__pc;
                    } else {
                        objCallList.Account_Email__c = ac.Email__c;
                    }
                   /* objCallList.Appointment__c = objWrap.objApp.Id;
                    String ownerId = objCalling.ownerId;
                    if (ownerId.startsWith('005')) {
                        objCallList.HO_Calling_list_Owner__c = objCalling.owner.Email;
                    }
                    lstAppointmentCL.add(objCallList);*/
                    lstAppointmentCL.add(objCallList);
                    try{
                    upsert objCallList;
                    }catch(Exception e)
                    {
                      objCallList.Unblock_Reason__c=e.getmessage();
                       return objCallList;
                    }
                    
                       if (mapOwnerIdGroupMember != null && mapOwnerIdGroupMember.containsKey(objCallList.ownerId)) {
                        lstGroupMember = mapOwnerIdGroupMember.get(objCallList.ownerId);
                    }
                      system.debug('!!!!!!!lstGroupMember'+lstGroupMember);
                     Datetime StartDT = datetime.newInstance(codCaseWrapper.AppointmentDate.year(), codCaseWrapper.AppointmentDate.month(),
                        codCaseWrapper.AppointmentDate.day(), integer.Valueof(Slot.substringBefore(':')), 
                        Slot.substringAfter(':') == '00' ? integer.Valueof(Slot.substringAfter(':')) : 00, 00);
                    Datetime EndDT = datetime.newInstance(codCaseWrapper.AppointmentDate.year(), codCaseWrapper.AppointmentDate.month(),
                        codCaseWrapper.AppointmentDate.day(), integer.Valueof((EndTime.substringBefore(':')).deleteWhitespace()), 
                        EndTime.substringAfter(':') != '00' ? integer.Valueof(EndTime.substringAfter(':')) : 00, 00);
                    Event objEvent = new Event();
                   if(codCaseWrapper.ServiceType=='Handover' || codCaseWrapper.ServiceType=='01-Handover')
                   {
                    objEvent.status__c = 'Requested';
                    }else
                    {
                      objEvent.status__c = 'Confirmed';
                    }
                      
                        // objEvent.OwnerId = u.id;
                       if(lstGroupMember.Size()>0){
                        system.debug('!!!!!!!Owner2'+lstGroupMember[0].UserOrGroupId);
                         objEvent.OwnerId = lstGroupMember[0].UserOrGroupId ;
                     }else
                     {
                     objEvent.OwnerId = u.id;
                     }
                    objEvent.Type = 'Meeting';
                    objEvent.IsVisibleInSelfService = true;
                    objEvent.StartDateTime = StartDT;
                    objEvent.EndDateTime = EndDT;
                    objEvent.Subject = 'Appointment Scheduled with Customer:'+ac.Name;
                    objEvent.Description= 'Appointment Scheduled with Customer:'+ac.Name;
                    lstEvent.add(objEvent);
                    
                     if (lstAppointmentCL != null && lstAppointmentCL.size() > 0) {
                          // insert lstAppointmentCL;
                        }
                     for(Integer i=0; i<lstAppointmentCL.size(); i++) {
                              lstEvent[i].WhatId = objCallList.Id;
                        }
                        system.debug('!!!!!!!lstEvent'+lstEvent);
                           insert lstEvent;
                     if (lstAppointmentCL != null) {
                        sendEmail(lstAppointmentCL);
                      }
                    
                    
                    
           
        return objCallList;
        
    }
    
    
   public static void sendEmail(List<Calling_List__c> lstAppointment) {
    List<String> sendTo;
        List<Messaging.SingleEmailMessage> messages =   new List<Messaging.SingleEmailMessage>();
        EmailTemplate reqEmailTemplate =  [SELECT ID, Subject, Body, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Appointment_Notifications_to_Customer' limit 1];
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
        for (Calling_List__c objCL : [Select Id, Name, Customer_Name__c, Service_Type__c, Appointment_Date__c, Appointment_Slot__c,
                                             Handover_Team_Email__c, HO_Calling_list_Owner__c, CRE_Email__c, Account_Email__c,
                                             Appointment_Status__c, Sub_Purpose__c, Booking_Unit_Name__c
                                      From Calling_List__c  Where Id IN: lstAppointment]) {
            Messaging.SingleEmailMessage mail = 
                            new Messaging.SingleEmailMessage();
            mail.setWhatId(objCL.Id);
            mail.setSaveAsActivity(true); 
            list<String> bccAddress = new list<String>();
           sendTo = new List<String>();
            String body, sub;
            if (reqEmailTemplate != null) {
                body = reqEmailTemplate.HtmlValue;
                sub = reqEmailTemplate.Subject;
                if (objCL.Customer_Name__c!= null) {
                    body = body.replace('{!Calling_List__c.Customer_Name__c}', objCL.Customer_Name__c);
                } else {
                    body = body.replace('{!Calling_List__c.Customer_Name__c}', '');
                }
                if (objCL.Sub_Purpose__c!= null) {
                    body = body.replace('{!Calling_List__c.Sub_Purpose__c}', objCL.Sub_Purpose__c);
                } else {
                    body = body.replace('{!Calling_List__c.Sub_Purpose__c}', '');
                }
                if (objCL.Booking_Unit_Name__c!= null) {
                    body = body.replace('{!Calling_List__c.Booking_Unit_Name__c}', objCL.Booking_Unit_Name__c);
                } else {
                    body = body.replace('{!Calling_List__c.Booking_Unit_Name__c}', '');
                }
                if (objCL.Appointment_Status__c != null) {
                    body = body.replace('{!Calling_List__c.Appointment_Status__c}', objCL.Appointment_Status__c);
                } else {
                    body = body.replace('{!Calling_List__c.Appointment_Status__c}', '');
                }
                if (objCL.Service_Type__c != null) {
                    body = body.replace('{!Calling_List__c.Service_Type__c}', objCL.Service_Type__c);
                } else {
                    body = body.replace('{!Calling_List__c.Service_Type__c}', '');
                }
                if (objCL.Appointment_Date__c != null) {
                    body = body.replace('{!Calling_List__c.Appointment_Date__c}', String.valueOf(objCL.Appointment_Date__c));
                } else {
                    body = body.replace('{!Calling_List__c.Appointment_Date__c}', null);
                }
                if (objCL.Appointment_Slot__c != null) {
                    body = body.replace('{!Calling_List__c.Appointment_Slot__c}', objCL.Appointment_Slot__c);
                } else {
                    body = body.replace('{!Calling_List__c.Appointment_Slot__c}', '');
                }
                body = body.replace(']]>', '');
                if (objCL.Service_Type__c != null) {
                    sub = sub.replace('{!Calling_List__c.Service_Type__c}', objCL.Service_Type__c);
                } else {
                    sub = sub.replace('{!Calling_List__c.Service_Type__c}', '');
                }
                if (objCL.Appointment_Status__c != null) {
                    sub = sub.replace('{!Calling_List__c.Appointment_Status__c}', objCL.Appointment_Status__c);
                } else {
                    sub = sub.replace('{!Calling_List__c.Appointment_Status__c}', '');
                }
                mail.setHtmlBody(body);
                mail.setSubject(sub);
            }
            if ((objCL.Sub_Purpose__c == 'Key Handover' || objCL.Sub_Purpose__c == 'Unit Viewing') &&
                objCL.Handover_Team_Email__c != null) {
                bccAddress.add(objCL.Handover_Team_Email__c);
            }
            if (objCL.HO_Calling_list_Owner__c != null) {
                bccAddress.add(objCL.HO_Calling_list_Owner__c);
            }
            if (objCL.CRE_Email__c != null) {
                bccAddress.add(objCL.CRE_Email__c);
            }
            if (bccAddress != null) {
                mail.setBccAddresses(bccAddress);
            }
            if(objCL.Account_Email__c!=null)
            sendTo.add(objCL.Account_Email__c);
            mail.setToAddresses(sendTo);            
            messages.add(mail);
            
            if ( owea.size() > 0 ) {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
            }
        }
        if(sendTo.size()>0)
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
    } 
    
    
   Global class AppointmentWrapper{
        public String RecordType;
        public String AccountID;
        public String UserName;  
        public Date AppointmentDate;  
        public String BookingUnit;
        public String SubProcessName;
        public String ServiceType;
      
        public String origin;
       
        public String AppointmentStatus;
        public String TimeSlot;
      
    }
 }