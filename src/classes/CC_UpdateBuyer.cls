/**************************************************************************************************
* Name               : CC_UpdateBuyer
* Test Class         : DealCustomCodesTest
* Description        : This is the custom code class for invoking Primary Buyer and Joint Buyer Update webservice
* Created Date       : 17/04/2017
* Created By         : NSI - Kaavya Raghuram
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0         NSI - Kaavya    17/04/2017      Created
* 1.1         QBurst          08/04/2020      SOAP to REST changes
* 1.2         QBurst          17/04/2020      Added Buyer Type check
**************************************************************************************************/
global without sharing class CC_UpdateBuyer implements NSIBPM.CustomCodeExecutable {
    public String EvaluateCustomCode(NSIBPM__Service_Request__c sr, NSIBPM__Step__c step) {
        String retStr = 'Success';
        List<Id> bookingIds = new List<Id>();
        try{
            for(Booking__c bookings : [SELECT Id FROM Booking__c WHERE Deal_SR__c =: step.NSIBPM__SR__c]){
                bookingIds.add(bookings.id);
            }
            if(bookingIds.size() > 0){
               List<Buyer__c> buyerlist = [SELECT Id, Primary_Buyer__c, Status__c, Change_Count__c,buyer_type__c
                                           FROM Buyer__c WHERE Is_3rd_Party__c = false AND Booking__c IN: bookingIds];
               for(Buyer__c buyer : buyerlist){
                   Boolean primary = false;
                   Boolean jointBuyer = false;
                   //1.2 starts
                   Boolean isPerson = true;
                   if(buyer.buyer_type__c == 'Corporate'){
                       isPerson = false;
                   }
                   //1.2 ends
                   if(buyer.Primary_Buyer__c){
                       if(buyer.Change_Count__c > 0){
                           primary = true;
                       }
                   } else if(buyer.Status__c == 'New' || buyer.Status__c == 'Updated' || buyer.Status__c == 'Removed'){
                       jointBuyer = true;
                   }
                   if (primary) {
                       // system.enqueueJob(new AsyncReceiptWebservice(BookingIds, 'Party Update'));
                        DAMAC_IPMS_PARTY_CREATION.CreateParty('UPDATE_CUSTOMER', isPerson, true, buyer.id, true); // 1.1
                    }
                    if (jointBuyer) {
                        //system.enqueueJob(new AsyncReceiptWebservice(BookingIds, 'Joint Buyer'));
                        DAMAC_IPMS_PARTY_CREATION.CreateParty('UPDATE_CUSTOMER', isPerson, false, buyer.id, false);  // 1.1
                    }
                }
            }
        } catch (Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
            Log__c objLog = new Log__c();
            objLog.Description__c = 'Ids==' + BookingIds + '-Line No===>' + e.getLineNumber() + '---Message==>' + e.getMessage();
            objLog.Type__c = 'Error for Update Buyer custom code';
            insert objLog;    
        }
        return retStr;
    }
}