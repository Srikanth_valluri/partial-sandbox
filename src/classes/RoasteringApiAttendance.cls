public class RoasteringApiAttendance {

/***************************************************************************************************************
Description: Method to return Attendance daily response
Return Type: HTTPResponse 
Parameters : IPMS Employee Id Of User, fromDate, toDate
****************************************************************************************************************/    
    public static HTTPResponse getAttendanceDetails(String ipmsUserId, Date fromDate, Date toDate) {
        System.debug('fromDate'+fromDate);
        System.debug('toDate'+toDate);
        if( String.isNotBlank(ipmsUserId)) {
            Credentials_details__c creds = Credentials_details__c.getInstance('Roastering Api');
            if(creds != null && String.isNotBlank(creds.Endpoint__c) && String.isNotBlank(creds.User_Name__c) && String.isNotBlank(creds.Password__c)) { 
                String dateRange = String.valueOf(fromDate.format()).replaceAll('/','')+'-'+String.valueOf(toDate.format()).replaceAll('/','');
                String endpoint = creds.Endpoint__c +ipmsUserId+';date-range='+dateRange;
                HTTPResponse resp = GenericHttpResponseFromService.returnResponseFromService(endpoint,'GET',creds.User_Name__c,creds.Password__c,'Basic');
                return resp;               
            }
        }
        return null;    
    }
}