@isTest
public class EncrytNumberTest{
  
    public static testMethod void EncrytNumbertest() {
   
   
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__pc = '123123123';
        objAcc.Mobile__c = '123123123';
        objAcc.Telephone__c = '123123123';
        objAcc.POA_Phone__c = '123123123';
        
        insert objAcc ;
        
        
        EncrytNumber.EncrytNumber(new List<Id>{objAcc.Id});
    }
    
    public static testMethod void encryptNoTest(){
       System.Test.startTest();
       EncrytNumber.encryptNo('00919405883787');
       System.Test.stopTest();
    }
    
    public static testMethod void decryptNoTest(){
       System.Test.startTest();
       String encryptedMob = EncrytNumber.encryptNo('00919405883787');
       String decryptedMob = EncrytNumber.decryptNo(encryptedMob);
       System.Test.stopTest();
    }
}