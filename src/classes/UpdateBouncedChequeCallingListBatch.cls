/*-------------------------------------------------------------------------------------------------
Description: Batch to get get Dues and Overdues from IPMS and update Calling List

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 04-12-2017       | Lochana Rajput   | Initial draft
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 05-12-2017       | Lochana Rajput   | Get Overdues and Dues Consuming endpoints
   =============================================================================================================================
*/
global without sharing class UpdateBouncedChequeCallingListBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {

    String query;
    Id bouncedChequeCLRecordTypeId;

    global UpdateBouncedChequeCallingListBatch() {
        bouncedChequeCLRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Bounced Cheque').getRecordTypeId();
        query = 'SELECT Case__r.Booking_Unit__r.Registration_ID__c FROM Calling_List__c WHERE RecordtypeId =: bouncedChequeCLRecordTypeId AND Calling_List_Status__c != \'Closed\'';
        System.debug('===query=' + query);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Calling_List__c> scope) {
        BouncedChequeUtility.updateCallingList(scope);
    }//execute end

    global void finish(Database.BatchableContext BC) {

    }

}