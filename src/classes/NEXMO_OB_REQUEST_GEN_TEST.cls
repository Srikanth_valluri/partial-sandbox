@IsTest
public class NEXMO_OB_REQUEST_GEN_TEST {
    static testMethod void testParse() {
        String json=        '{"to":{"number":"919652300133","type":"whatsapp"},"from":{"number":"447418342136","type":"whatsapp"},"message":{"content":{"type":"text","text":"Hi "}}}';
        NEXMO_OB_REQUEST_GEN obj = NEXMO_OB_REQUEST_GEN.parse(json);
        NEXMO_OB_REQUEST_GEN_NO_MTM obj1 = NEXMO_OB_REQUEST_GEN_NO_MTM.parse(json); 
        System.assert(obj != null);
    }
}