public class Undo_SR{

public string cid {get;set;}
    public Undo_SR(ApexPages.StandardController controller) { cId = apexpages.currentpage().getparameters().get('id');}
    
      
    public pagereference updateCasestatus(){        
     
        List<case> lstCase = [SELECT ID,RecordType.Name,status FROM case WHERE ID =:cId];
        system.debug('\n--lstCase--'+lstCase);
        if(!lstCase.isEmpty()){           
                 if(lstCase[0].status=='New'||lstCase[0].status=='Draft Request')
                 {
                 lstCase[0].status= 'Cancelled';               
                 system.debug('\n--lstCase--'+lstCase);
                   update lstCase;
                 pagereference page = new Pagereference('/'+cId);
                  page.setRedirect(true);
                 return page;                 
                 } 
                       
        }
        return null;             
    }
}