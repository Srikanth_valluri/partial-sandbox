@isTest
public class FmcViewInvoicesControllerTest {
    static final String TEST_URL = 'https://test.damacgroup.com/test.pdf';
	@isTest
    static void testController() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Building_Name__c = 'Test Building',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_Name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference currentPage = Page.CommunityPortal;
        Test.setCurrentPage(currentPage);

        Test.startTest();
            System.runAs(portalUser) {
                FmcViewInvoicesController controller = new FmcViewInvoicesController();
                controller.initiateVariables();
                FmcViewInvoicesController.strSelectedUnit = '';
                FmcViewInvoicesController.strSelectedlocation = '';
            }
        Test.stopTest();
    }

    @isTest
    static void getInvoiceByRegistrationIdTest() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Building_Name__c = 'Test Building',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_Name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

		PageReference currentPage = Page.CommunityPortal;
        Test.setCurrentPage(currentPage);

        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
			System.runAs(portalUser) {
                FmcViewInvoicesController controller = new FmcViewInvoicesController();
                controller.initiateVariables();
				FmcViewInvoicesController.viewInvoice(NULL,NULL,'1234','');
				//System.assertEquals(TEST_URL, FmIpmsRestServices.getInvoiceByRegistrationId('1234'));
            }
        Test.stopTest();
    }

    @isTest
    static void getInvoiceByRegistrationIdAndDateTest() {
		User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Building_Name__c = 'Test Building',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_Name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

		PageReference currentPage = Page.CommunityPortal;
        Test.setCurrentPage(currentPage);

        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
			System.runAs(portalUser) {
                FmcViewInvoicesController controller = new FmcViewInvoicesController();
                controller.initiateVariables();
				FmcViewInvoicesController.viewInvoice(Date.today(),NULL,'1234','');
            }
            //System.assertEquals(TEST_URL, FmIpmsRestServices.getInvoiceByRegistrationIdAndDate('1234', Date.today()));
        Test.stopTest();
    }

    @isTest
    static void getBulkInvoiceByPartyIdTest() {
		User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Building_Name__c = 'Test Building',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_Name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

		PageReference currentPage = Page.CommunityPortal;
        Test.setCurrentPage(currentPage);

        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
			System.runAs(portalUser) {
                FmcViewInvoicesController controller = new FmcViewInvoicesController();
                controller.initiateVariables();
				FmcViewInvoicesController.viewInvoice(NULL,NULL,'','');
            }
            //System.assertEquals(TEST_URL, FmIpmsRestServices.getBulkInvoiceByPartyId('1234'));
        Test.stopTest();
    }

    @isTest
    static void getBulkInvoiceByPartyIdAndProjectCodeTest() {
		User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Building_Name__c = 'Test Building',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_Name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

		PageReference currentPage = Page.CommunityPortal;
        Test.setCurrentPage(currentPage);

        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
			System.runAs(portalUser) {
                FmcViewInvoicesController controller = new FmcViewInvoicesController();
                controller.initiateVariables();
				FmcViewInvoicesController.viewInvoice(NULL,NULL,'','TEST');
            }
            //System.assertEquals(TEST_URL, FmIpmsRestServices.getBulkInvoiceByPartyIdAndProjectCode('1234', 'TEST'));
        Test.stopTest();
    }

    @isTest
    static void getBulkInvoiceByPartyIdAndDateTest() {
		User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Building_Name__c = 'Test Building',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_Name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

		PageReference currentPage = Page.CommunityPortal;
        Test.setCurrentPage(currentPage);

        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
			System.runAs(portalUser) {
                FmcViewInvoicesController controller = new FmcViewInvoicesController();
                controller.initiateVariables();
				FmcViewInvoicesController.viewInvoice(Date.today(),NULL,'','');
            }
            //System.assertEquals(TEST_URL, FmIpmsRestServices.getBulkInvoiceByPartyIdAndDate('1234', Date.today()));
        Test.stopTest();
    }

    @isTest
    static void getBulkInvoiceByPartyIdDateRangeAndProjectCodeTest() {
		User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Building_Name__c = 'Test Building',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_Name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

		PageReference currentPage = Page.CommunityPortal;
        Test.setCurrentPage(currentPage);

        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        String responseBody;

        Test.startTest();
			System.runAs(portalUser) {
                FmcViewInvoicesController controller = new FmcViewInvoicesController();
                controller.initiateVariables();
				FmcViewInvoicesController.viewInvoice(Date.today(), Date.today(),'1234','TEST');
            }
            /*System.assertEquals(
                TEST_URL,
                FmIpmsRestServices.getBulkInvoiceByPartyIdDateRangeAndProjectCode(
                    '1234', Date.today(), Date.today(), 'TEST'
                )
            );*/
        Test.stopTest();
    }
}