/**************************************************************************************************
* Name               : AgentportalSessionControllerTest
* Description        :                                               
* Created Date       : Naresh(Accely)                                                                       
* Created By         : 6/09/2017                                                                 
* Last Modified Date :                                                                             
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.1         Naresh kaneriya      6/09/2017                                                             
**************************************************************************************************/

@isTest
public class AgentportalSessionControllerTest{
 private static Contact adminContact;
    private static User portalUser;
    private static Account adminAccount;

    static void init(){
       adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        insert adminAccount;
        
        system.debug('--adminAccount--'+adminAccount.Agency_Tier__c);
        
        adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        insert adminContact;
        
        Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
        insert agentContact;
        
        portalUser = InitialiseTestData.getPortalUser('test@test.com', adminContact.Id, 'Admin');
    }


public static testMethod void portalSessionTest(){
  init();
   System.runAs(portalUser){
  AgentportalSessionController obj = new AgentportalSessionController();
  }
}



}