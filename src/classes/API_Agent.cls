@RestResource(urlMapping='/api_contact/*')
global with sharing class API_Agent {
    
    @HttpGet
    global static void doGet() {
        RestRequest request = RestContext.request;
        
        String recordType = request.requestURI.substring(request.requestURI.lastIndexOf('/' ) + 1);
        String queryOffsetString = RestContext.request.params.get('offset');
        String queryLimitString = RestContext.request.params.get('limit');
        String sortBy = RestContext.request.params.get('sortBy');
        String sortType = RestContext.request.params.get('sortType');
        
        String whereClause = RestContext.request.params.get('whereClause');
        
        Integer queryOffset = 0;
        Integer queryLimit = 15;
        
        if (!String.isBlank(queryOffsetString)) {
            queryOffset = Integer.valueOf(queryOffsetString);
        }
        
        if (!String.isBlank(queryLimitString)) {
            queryLimit = Integer.valueOf(queryLimitString);
        }
        List<Contact> contactList = getContactList(queryOffset, queryLimit, sortBy, sortType, whereClause, recordType);
        
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(contactList));        
    }
    
    global static List<Contact> getContactList(Integer queryOffset, Integer queryLimit, String sortBy, String sortType, String whereClause, String recordType) {
        String currentUserId = UserInfo.getUserId();
        
        if (String.isNotBlank(whereClause)) {
            whereClause = ' AND ' + whereClause;
        } else {
            whereClause = '';
        }
        
        if (String.isBlank(recordType) || recordType.equalsIgnoreCase('api_contact')) {
           recordType = ''; 
        } else if (recordType.equalsIgnoreCase('agent_profile')) {
            whereClause += ' AND Salesforce_User__c = :currentUserId';
        } else {
            recordType = ' AND RecordType.DeveloperName = \'' + recordType + '\'';  
        }
        
        if (String.isBlank(sortBy)) {
            sortBy = 'Name';
        }
        
        if (String.isBlank(sortType)) {
            sortType = 'ASC';
        }
        String contactQuery = SOQLHelper.getObjectQuery('Contact');
        contactQuery += ' WHERE Id != null ';
        contactQuery += whereClause;
        contactQuery += ' ORDER BY ' + sortBy + ' ' + sortType;
        contactQuery += ' LIMIT :queryLimit ';
        contactQuery += ' OFFSET :queryOffset';
        system.debug(contactQuery);
        List<contact> contactList = Database.query(contactQuery);
        return contactList;
    }
}