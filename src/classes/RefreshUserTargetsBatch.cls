/****************************************************************************************************
* Name          : RefreshUserTargetsBatch                                                           *
* Description   : Batch class to populate the Users Sale on the Target record respectively          *
* Created Date  : 12/04/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE            COMMENTS                                                *
* 1.0   Craig Lobo          12/04/2018      Initial Draft.                                          *
****************************************************************************************************/
global class RefreshUserTargetsBatch implements Database.Batchable<sObject>, Database.Stateful {

    global Datetime CURRENT_TIME        = System.now();
    global Datetime CURRENT_MONTH       = Date.newInstance(CURRENT_TIME.year(), CURRENT_TIME.month(), 1);
    global Datetime LAST_MONTH          = CURRENT_MONTH.addMonths(-1);

    global String LAST_MONTH_NAME       = CURRENT_TIME.addMonths(-1).format('MMMMM');
    global String CURRENT_MONTH_NAME    = CURRENT_MONTH.format('MMMMM');
    global String CURRENT_YEAR          = String.valueOf(CURRENT_TIME.year());

    global String BU_QUERY              = ' SELECT Id, Name, Registration_Status__c, '
                                        + ' Booking__r.Deal_SR__r.OwnerId, Requested_Price__c, '
                                        + ' Booking__r.Deal_SR__c, Registration_DateTime__c, '
                                        + ' Booking__r.Deal_SR__r.Total_Booking_Amount__c '
                                        + ' FROM Booking_Unit__c '
                                        + ' WHERE Requested_Price__c != null '
                                        + ' AND Registration_DateTime__c != null '
                                        + ' AND Registration_DateTime__c >= :LAST_MONTH '
                                        + ' AND Registration_DateTime__c <= :CURRENT_TIME '
                                        + ' AND Registration_Status__c IN :buStatusList '
                                        + ' AND Booking__r.Deal_SR__r.OwnerId IN :userIdList ';

    List<String> buStatusList           = new List<String>();

    /**
     * Constructor 
     */
    global RefreshUserTargetsBatch() {
        // Get the all the Booking Unit Active Status value for the Custom setting
        for (Booking_Unit_Active_Status__c buStatus : Booking_Unit_Active_Status__c.getall().values()) {
            buStatusList.add(buStatus.Status_Value__c);
        }
    }

    /** 
     * Start Block
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String targetQuery  = ' SELECT Id, User__c, Month__c, Actual_Sales__c,'
                            + ' Target__c, Year__c '
                            + ' FROM Target__c ' 
                            + ' WHERE Year__c = :CURRENT_YEAR '
                            + ' AND '
                            + ' (  Month__c = :CURRENT_MONTH_NAME '
                            + ' OR  Month__c = :LAST_MONTH_NAME '
                            + ' ) ';
        System.debug('targetQuery>>>>>>> ' + targetQuery);
        return Database.getQueryLocator(targetQuery);
    }

    /** 
     * Excute Block
     */
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
System.debug('scope>>>>>>> ' + scope);
        List<String> userIdList = new List<String>();
        
        Map<String, Target__c> userMonthTargetsMap = new Map<String, Target__c>();

        // Get all target records according to User and Month
        for (Target__c targetObj : (List<Target__c>) scope) {
            targetObj.Actual_Sales__c = 0;
            userIdList.add(targetObj.User__c);
            userMonthTargetsMap.put(
                targetObj.User__c + '-' + targetObj.Month__c,
                targetObj
            );
        }

        // Query String to fetch Booking Unit records
        String buQueryString = BU_QUERY;
                             //+ ' AND Registration_Status__c IN :buStatusList '
                             //+ ' AND Booking__r.Deal_SR__r.OwnerId IN :userIdList ';
        System.debug('buQueryString>>>>>>> ' + buQueryString);

        // Sum up the booking Units Price and populate it on the Target record Actual Sales field
        for (Booking_Unit__c bookingUnitObj : Database.query(buQueryString)) {
            String mapKey   = bookingUnitObj.Booking__r.Deal_SR__r.OwnerId 
                            + '-' 
                            + bookingUnitObj.Registration_DateTime__c.format('MMMMM');
            if (userMonthTargetsMap.containsKey(mapKey)) {
                System.debug('Actual_Sales__c>>>>>> ' + userMonthTargetsMap.get(mapKey).Actual_Sales__c);
                System.debug('Actual_Sales__c>>>>>> ' + bookingUnitObj.Requested_Price__c);
                userMonthTargetsMap.get(mapKey).Actual_Sales__c += bookingUnitObj.Requested_Price__c;
            }
        }
        System.debug('userMonthTargetsMap>>>>>> ' + userMonthTargetsMap);

        // DML
        update userMonthTargetsMap.values();

    }

    /** 
     * Finish Block
     */
    global void finish(Database.BatchableContext BC) {

    }
    
}