/******************************************************************
Methods for Retriggering IPMS webservice
Version   Author        Comments
1.0     Alok Chauhan   Added Method to Retrigger Vendor Creation WS

*****************************************************************/
global class ReTriggerIPMS_WS{

    webservice static string retriggerVendorCreation(Id accnId){     
        String resMsg='Success';
   
            try{
                for(Account agentAccn :[select id from Account where Id=:accnId]){                    
                    List<id> agentIds= new List<id>();
                    agentIds.add(agentAccn.id);
                    AsyncAgentWebservice.sendAgentCreate(agentIds); 
                }      
            }
            catch(Exception ex) {
                 resMsg=ex.getMessage();        
            }  
        return resMsg;
        }    
 }