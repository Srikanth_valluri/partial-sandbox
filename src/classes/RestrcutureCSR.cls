public class RestrcutureCSR {
  	
	public static void createNewCase(Set<Id> setOfCaseId){ 
         List<Case> lstCaseToInsert = new List<Case>();
    	 map<id,List<SR_Attachments__c>> mapofCaseAndAttachments = new map<id,List<SR_Attachments__c>>(); 
         List<SR_Attachments__c> lstAttachmentsToUpdate = new List<SR_Attachments__c>();
         List<SR_Attachments__c> lstAttachmentsToInsert = new List<SR_Attachments__c>();
      
          
        if(setOfCaseId != null && setOfCaseId.size() > 0){
            case caseObjectToInsert;
            List<Case> lstCase = [Select id ,Booking_Unit__c,Waiver_Amount__c,Description,AccountId,OwnerId,(Select id,Name,isValid__c,Attachment_URL__c,CurrencyIsoCode from SR_Attachments__r Limit 1), (Select id,RequestType__c,Booking_Unit__c,Per_penalty_to_be_waived__c,Amount__c,WaiverAmount__c,Per_Area_Variation_to_be_waived__c,Percent__c,CRE_Comments__c from CaseSummaryRequests__r where RequestType__c In ('Penalty Waiver','Service Charge Waiver','Area Variation Waiver')) from case where id in :setOfCaseId];
            for(Case objCase : lstCase){
                if( objCase.SR_Attachments__r!=null){
                     mapofCaseAndAttachments.put(objCase.Id, objCase.SR_Attachments__r);
                }                     
                for(CaseSummaryRequest__c objCaseSummaryRequest : objCase.CaseSummaryRequests__r){

                    String recordType;
                    caseObjectToInsert = new case();
                    if(objCaseSummaryRequest.RequestType__c == 'Area Variation Waiver'){ 
                        recordType = 'Waiver';
                        caseObjectToInsert.Waiver_Type__c = 'Area Variation Charge Waiver';
                        caseObjectToInsert.Waiver_Amount__c = String.valueof(objCaseSummaryRequest.Amount__c);    
                        caseObjectToInsert.Fee_charge_VAT_Balance__c = Decimal.valueOf(objCaseSummaryRequest.Per_Area_Variation_to_be_waived__c);
                    }else if (objCaseSummaryRequest.RequestType__c == 'Service Charge Waiver'){
                        recordType = 'Waiver';
                        caseObjectToInsert.Waiver_Type__c ='Service Charge';
                        if(objCaseSummaryRequest.Amount__c!=null){
                           caseObjectToInsert.Waiver_Amount__c =  String.valueof(objCaseSummaryRequest.Amount__c);
                         }
                         if(objCaseSummaryRequest.Percent__c!=null){
                             caseObjectToInsert.Percent_to_be_waived__c = objCaseSummaryRequest.Percent__c;
                         }
                    }else{
                       recordType =  objCaseSummaryRequest.RequestType__c;
                       caseObjectToInsert.Percent_to_be_waived__c = Decimal.valueof(objCaseSummaryRequest.Per_penalty_to_be_waived__c);
                       caseObjectToInsert.Amount_to_be_waived__c = Decimal.valueof(objCaseSummaryRequest.Amount__c);
                    }
                    caseObjectToInsert.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
                    caseObjectToInsert.Booking_Unit__c  = objCaseSummaryRequest.Booking_Unit__c;
                    caseObjectToInsert.ParentId = objCase.Id;
                    caseObjectToInsert.AccountId = objCase.AccountId;
                    caseObjectToInsert.Description=objCaseSummaryRequest.CRE_Comments__c;
                    caseObjectToInsert.status = 'Submitted';
                    caseObjectToInsert.OwnerId = objCase.OwnerId;	
                    lstCaseToInsert.add(caseObjectToInsert);
                }
            }
            system.debug('lstCaseToInsert==='+lstCaseToInsert);
                if(lstCaseToInsert!=null && lstCaseToInsert.size()>0){
                    insert lstCaseToInsert;
                }
        }
          
         
        List<Case> lstCasesToUpdate = new list<Case>();
        for(Case objCase : lstCaseToInsert) {
            if(mapofCaseAndAttachments.containskey(objCase.ParentId) && (objCase.Waiver_Type__c =='Service Charge' || objCase.Waiver_Type__c =='Area Variation Charge Waiver' )){
               lstAttachmentsToInsert = mapofCaseAndAttachments.get(objCase.parentId);
                lstAttachmentsToInsert = lstAttachmentsToInsert.deepClone(false,false,false);
                 for(SR_Attachments__c objAttachments:lstAttachmentsToInsert){
                    objAttachments.case__c = objCase.id;
                    lstAttachmentsToUpdate.add(objAttachments);
                }
            }
         String approver = '';
          String strApprovalAuth = SfRuleEngine.filterSfRuleEngine('Case Summary Child', objCase.Id);
            System.debug('strApprovalAuth>>>'+strApprovalAuth);
            if (strApprovalAuth != null && !String.isBlank(strApprovalAuth) ){
                    approver = strApprovalAuth;
                    system.debug('approver'+approver);
         
            }
             if (!string.isBlank(approver)) {
                objCase.Roles_from_Rule_Engine__c = approver;
                objCase.Approving_Authorities__c = approver;
                objCase.Submit_for_Approval__c = true;
             }
             lstCasesToUpdate.add(objCase);
        }
        if(lstCasesToUpdate != null && lstCasesToUpdate.size() > 0) {
            update lstCasesToUpdate;
        }
                
        if(lstAttachmentsToUpdate!=null && lstAttachmentsToUpdate.size()>0){
            System.debug('lstAttachmentsToUpdate==='+lstAttachmentsToUpdate);
          insert lstAttachmentsToUpdate;
        }
    }
}