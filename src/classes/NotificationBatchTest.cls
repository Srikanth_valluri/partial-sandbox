@isTest
private Class NotificationBatchTest{

    @isTest static void test_method_one() {
        Account acc = InitialiseTestData.getCorporateAccount('Acc-21');
        acc.Trade_License_Number__c='123';
        insert acc ;
        
        
              
        NSIBPM__SR_Status__c statusObject = new NSIBPM__SR_Status__c ();
        statusObject.NSIBPM__Code__c = 'UNDER_AE_REVIEW';
        statusObject.name ='Under Agent Executive Review';
        insert statusObject;      
        
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.NSIBPM__Customer__c = acc.Id;
        SR.NSIBPM__Send_SMS_to_Mobile__c = '009715262000';
        SR.NSIBPM__Email__c = 'pavithra@nsigulf.com';
        SR.country_of_sale__c = 'UAE';
        SR.Trade_License_Number__c='123';
        SR.Agency_Type__c = 'Individual';
        SR.ID_Type__c = 'National ID';
        SR.Eligible_to_Sell_in_Dubai__c = true;
        SR.recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration');
        SR.Account_Number__c = '34543';
        SR.Account_Details_Country__c = 'Spain';
        SR.Account_Number_2__c = '454564';
        SR.Account_Details_Country_2__c = 'Spain';
        SR.Account_Number_3__c = '343546576';
        SR.Account_Details_Country_3__c = 'Spain';
        SR.NSIBPM__Internal_SR_Status__c =statusObject.id;
        SR.Under_Agent_Review__c = Date.newInstance(2018, 06, 4);
        insert SR;
        
        SR = [Select Under_Agent_Review__c,Under_Agent_Review_1day_formula__c,Under_Agent_Review_2dayformula__c 
              from  NSIBPM__Service_Request__c  where id = :SR.id];
        SR.Under_Agent_Review__c = Date.newInstance(2018, 06, 3);
        update SR;
        SR = [Select Under_Agent_Review__c,Under_Agent_Review_1day_formula__c,Under_Agent_Review_2dayformula__c 
              from  NSIBPM__Service_Request__c  where id = :SR.id];
        System.debug('date valueUnder_Agent_Review__c   @@' + sr.Under_Agent_Review__c);
        System.debug('date valueUnder_Agent_Review_1day_formula__c   @@' + sr.Under_Agent_Review_1day_formula__c);
        System.debug('date valueUnder_Agent_Review_2dayformula__c   @@' + sr.Under_Agent_Review_2dayformula__c);
        Test.startTest();
            NotificationBatch  notifyobj = new NotificationBatch(); 
            database.executebatch(notifyobj);
            NotificationBatchHandler obj = new NotificationBatchHandler();
            NotificationBatchHandler.sendEmail(SR,new List<String>{'test@test.com'},'mailbody');
        Test.stopTest();      

    }
}