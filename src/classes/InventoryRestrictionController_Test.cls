@isTest
public class InventoryRestrictionController_Test {
    
    @isTest
    public static void unitTestMethod1()
    {
      //User usr=[select id from user where email='test-user1@fakeemail.com'];
      //Id AgencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        
        ID ProfileID = [ Select id,UserType from Profile where name = 'Customer Community - Admin'].id;
        ID consultantId = [ Select id,UserType from Profile where name = 'Property Consultant'].id;
        ID adminProfileID = [ Select id,UserType from Profile where name = 'System Administrator'].id;
        ID adminRoleId = [ Select id from userRole where name = 'Chairman'].id;
        
         User u = new User( email='test-user1@fakeemail.com', profileid = adminProfileID, UserRoleId = adminRoleId,
                          UserName='test-user@fakeemail.com', alias='tuser2', CommunityNickName='tuser1', isActive = true,
                          TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
                          LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User');
        insert u;
        Group grp = new Group();
        grp.name = 'Sales Admin Queue';
        grp.Type = 'Queue'; 
        Insert grp; 
        GroupMember grpMember= new GroupMember();
        grpMember.GroupId=grp.id; 
      System.runAs(u)
          {
        Inventory_Restriction_Functional_Matrix__c matrix= new Inventory_Restriction_Functional_Matrix__c();
        matrix.Defined_Approval_User__c=u.Id;
        matrix.Defined_Approval_User_2__c=u.Id;
        matrix.Defined_Approval_User_3__c=u.Id;
        matrix.Functional_User__c=u.Id;
        matrix.Function__c='Strategy';
        matrix.Restrict_Reason__c='Leasing';
        insert matrix;
         Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        //loc.recordTypeId = bildingLocRecTypeId;
        loc.Location_Name__c = 'test';
        loc.Address__c = 'test';
        loc.Contact__c = 'test';
        loc.Timings__c = 'test';
        loc.Map_URL__c = 'https://test';
        insert loc;
        Property__c objProperty = TestDataFactory_CRM.createProperty();
        insert objProperty;
        Inventory__c objInventory = TestDataFactory_CRM.createInventory(objProperty.id);
        objInventory.Area_Sqft__c = '1520';
        objInventory.Bedroom_Type__c = 'Test';
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit_Location__c=loc.id;
        objInventory.Floor_Package_Name__c='Floor1';
        objInventory.View_Type__c='Type1';
        objInventory.Property_City__c='Dubai';
        objInventory.Property_Status__c='Approve';
        objInventory.Space_Type_Lookup_Code__c='123';
        insert objInventory;
        Inventory_Restriction_Request__c inventoryReq= new Inventory_Restriction_Request__c();
        inventoryReq.Status__c='Draft';
        inventoryReq.Function__c='Strategy';
        inventoryReq.Request_Processed_Datetime__c=DateTime.now();
        inventoryReq.Functional_User__c=u.id;
        inventoryReq.Request_Date__c=DateTime.now();
        inventoryReq.Restrict_Reason__c='Leasing';
        inventoryReq.Inventory_Request_Filters__c='BedRoom:1;';
        inventoryReq.Approval_User_2__c=u.Id;
        inventoryReq.Approval_User_3__c=u.Id;
        inventoryReq.Request_Type__c='Inventory Restriction';
        inventoryReq.Approval_User__c=u.Id;
        inventoryReq.Approved_by_Approver_User_1__c=False;
        inventoryReq.Approved_by_Approver_User_2__c=True;
        inventoryReq.Approved_by_Approver_User_3__c=True;
        inventoryReq.Inventory_Restriction_Functional_Matrix__c=matrix.Id;
        inventoryReq.Approver_User_1_Comments__c='Test Comments1';
        inventoryReq.Approver_User_2_Comments__c='Test Comments2';
        inventoryReq.Approver_User_3_Comments__c='Test Comments3';
        inventoryReq.Sales_Admin_Comments__c='Test Sales comment';
        inventoryReq.Restriction_End_Date__c=System.today()+2;
        inventoryReq.Restriction_Extended__c=false;
        insert inventoryReq;
        Inventory_Restriction_Unit__c inventoryUnit= new Inventory_Restriction_Unit__c();
        inventoryUnit.Inventory_Restriction_Request__c=inventoryReq.Id;
        inventoryUnit.Inventory_Initial_Status__c='Draft';
        inventoryUnit.Inventory__c=objInventory.id;
        insert inventoryUnit; 
        Unit_Documents__c objUnitDocument = new Unit_Documents__c();
        objUnitDocument.Inventory_Restriction_Request__c = inventoryReq.Id; 
        insert objUnitDocument;
        Inventory_Restriction_Picklists__c pckList= new Inventory_Restriction_Picklists__c();
        pckList.SetupOwnerId=adminProfileID;
        pckList.Restrict_Reasons__c='Bulk Sale OnlyDescoped unitsDesign ChangesFuture SalesHeld for Management';
        pckList.Restrict_Reason2__c='Non Residential UnitsHandover officesales officeStatus ErrorFor Credit controlSatycae';
        pckList.Restrict_Functions__c='StrategyTechnical teamProjects teamLeasingLegal';
        insert pckList;
        test.startTest();
        PageReference pageRefrence=Page.InventoryRestrictionRequest;
       
         Test.setCurrentPage(pageRefrence);
      
        apexpages.currentpage().getparameters().put('id', inventoryReq.Id);
        //apexpages.currentpage().getparameters().put('selectedInventIds', objInventory.Id);

              
        ApexPages.StandardController sc = new ApexPages.StandardController(inventoryReq);
        InventoryRestrictionController cntr = new InventoryRestrictionController(sc);
         cntr.fetchFilters();
         cntr.refreshUnitDocumentList();
         cntr.getItems();
         cntr.getPropStatus();
         cntr.getCities();
         cntr.getBedRooms();
         cntr.getLocation();
         cntr.getPropertyNames();
         cntr.getStatuses();
         cntr.getViewTypes();
         cntr.getUnitTypes();
         cntr.getUnits();
         cntr.getUnitDocuments();
         cntr.getPkgNames();
         cntr.getProjctFtrs();
         cntr.getAcd();
         
         cntr.createExtenstionRequest();
         cntr.applyInventoryRestriction();
         cntr.rejectBySalesAdmin();
         cntr.approveRejectInvRequest();
         cntr.addInventoriesToRequest();
         cntr.addInventoriesToRequest2();
         cntr.modifyRequestFilter();
         cntr.updateUnitDocumentList();
         cntr.approveRejectRequest('Submitt', 'comments');
         cntr.rejectBySalesAdmin();
         cntr.fetchInventories();
         InventoryRestrictionController.insertUnitDocument(inventoryReq.Id,'TestDoc');
         cntr.rejectBySalesAdmin();   
         cntr.approveRejectInvRequest();
         cntr.getRestrictFunctionOptions();
         cntr.getRestrictReasonOptions();
       
         
         cntr.fetchSelectedInventories();
         InventoryRestrictionController.doUploadAttachment(objUnitDocument.id, ' ', 'Test Doc is inserted');
         
         inventoryReq.Status__c='Submitted';
         update inventoryReq;
         inventoryReq.Status__c='Approved';
         update inventoryReq;
         inventoryReq.Status__c='Completed';
         inventoryReq.Restrict_Reason__c = 'Future Sales';
         update inventoryReq;
         /*
         List<Inventory_Restriction_Request__c> reqList = new List<Inventory_Restriction_Request__c>();
         reqList.add(inventoryReq);
         InvRestrictionReqTriggerHandler.updateApprovalMatrix(reqList); */
         test.stopTest();
          }
     }

}