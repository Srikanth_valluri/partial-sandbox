/****************************************************************************************************
* Name          : RefreshUserActionPlanTest                                                         *
* Description   : Test class for RefreshUserActionPlan                                              *
* Created Date  : 01/04/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    01/04/2018      Initial Draft.                                          *
****************************************************************************************************/
@isTest 
public class RefreshUserActionPlanTest {


    public static testmethod void test1() { 

        Profile profileObj = [SELECT Id FROM Profile WHERE Name = 'Property Consultant' LIMIT 1]; 
        User userObj = new User(
            Alias = 'stand', 
            Email = 'user@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'Test', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObj.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfd@testorg.com'
        );
        insert userObj;

        DateTime todaysDateTime = datetime.now();
        String lastMonthName = todaysDateTime.addmonths(-1).format('MMMMM');
        String secondLastMonthName = todaysDateTime.addmonths(-2).format('MMMMM');

        Zero_Sales_User__c zsObj = new Zero_Sales_User__c(
            Name = userObj.Name,
            HOS_Descision__c = 'Pending'
        );
        insert zsObj;

        User_Action_Plan__c plan1 = new User_Action_Plan__c(
            Name = userObj.Name,
            PC__c = userObj.Id,
            Action_Plan__c = '1st to 15th',
            Zero_Sales_User__c = zsObj.Id
        );
        insert plan1;

        User_Action_Plan__c plan2 = new User_Action_Plan__c(
            Name = userObj.Name,
            PC__c = userObj.Id,
            Action_Plan__c = '16th to 25th',
            Zero_Sales_User__c = zsObj.Id
        );
        insert plan2;

        User_Action_Plan__c plan3 = new User_Action_Plan__c(
            Name = userObj.Name
        );

        Test.startTest();
        PageReference refreshUserActionPlanPage = Page.RefreshUserActionPlan;
        Test.setCurrentPage(refreshUserActionPlanPage);
        ApexPages.StandardController controller = new ApexPages.StandardController(plan1);
        RefreshUserActionPlan refreshActionPlan = new RefreshUserActionPlan(controller);
        refreshActionPlan.refreshZeroSalesUserPlans();
        Test.stopTest();

    }

        public static testmethod void test2() { 

        Profile profileObj = [SELECT Id FROM Profile WHERE Name = 'Property Consultant' LIMIT 1]; 
        User userObj = new User(
            Alias = 'stand', 
            Email = 'user@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'Test', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObj.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfd@testorg.com'
        );
        insert userObj;

        DateTime todaysDateTime = datetime.now();
        String lastMonthName = todaysDateTime.addmonths(-1).format('MMMMM');
        String secondLastMonthName = todaysDateTime.addmonths(-2).format('MMMMM');

        Zero_Sales_User__c zsObj = new Zero_Sales_User__c(
            Name = userObj.Name,
            HOS_Descision__c = 'Pending'
        );
        insert zsObj;

        User_Action_Plan__c plan3 = new User_Action_Plan__c(
            Name = userObj.Name
        );

        Test.startTest();
        PageReference refreshUserActionPlanPage = Page.RefreshUserActionPlan;
        Test.setCurrentPage(refreshUserActionPlanPage);
        ApexPages.StandardController controller = new ApexPages.StandardController(plan3);
        Apexpages.currentPage().getParameters().put('zsRecId', zsObj.Id);
        RefreshUserActionPlan refreshActionPlan = new RefreshUserActionPlan(controller);
        refreshActionPlan.refreshZeroSalesUserPlans();
        Test.stopTest();

    }

        public static testmethod void test3() { 

        Profile profileObj = [SELECT Id FROM Profile WHERE Name = 'Property Consultant' LIMIT 1]; 
        User userObj = new User(
            Alias = 'stand', 
            Email = 'user@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'Test', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObj.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfd@testorg.com'
        );
        insert userObj;

        DateTime todaysDateTime = datetime.now();
        String lastMonthName = todaysDateTime.addmonths(-1).format('MMMMM');
        String secondLastMonthName = todaysDateTime.addmonths(-2).format('MMMMM');

        Zero_Sales_User__c zsObj = new Zero_Sales_User__c(
            Name = userObj.Name,
            HOS_Descision__c = 'Pending'
        );
        insert zsObj;

        User_Action_Plan__c plan3 = new User_Action_Plan__c(
            Name = userObj.Name
        );

        Test.startTest();
        PageReference refreshUserActionPlanPage = Page.RefreshUserActionPlan;
        Test.setCurrentPage(refreshUserActionPlanPage);
        ApexPages.StandardController controller = new ApexPages.StandardController(plan3);
        RefreshUserActionPlan refreshActionPlan = new RefreshUserActionPlan(controller);
        refreshActionPlan.refreshZeroSalesUserPlans();
        Test.stopTest();

    }
}