public with sharing class GenerateLMAAgreement {
    public String caseId;
    public Case objC;
    public GenerateLMAAgreement(ApexPages.StandardController stdCon){
        caseId = stdCon.getId();
        objC = [Select Id
                     , Booking_Unit__c
                     , Booking_Unit__r.RGS_Percent__c                 
                     , Registration_ID__c
                     , Early_Handover_Status__c
                     , Handover__c
                     , Call_Outcome__c
                     , RGS_Option__c
                from Case 
                where Id =: caseId];
    }
    
    public void callDrawloop(){
        executeBatch();
    }
    
    public pageReference returnToCase(){
        PageReference Pg = new PageReference('/'+caseId);
        return pg;
    }
    
    public void executeBatch(){
        if(String.isNotBlank(objC.Registration_ID__c)){
            GenerateDrawloopDocumentBatch objInstance;
            Id batchId;
            if (objC.Handover__c == true || 
                (objC.Call_Outcome__c == 'Normal Handed over but without Rental Guarantee'
                || objC.Call_Outcome__c == 'Normal Handed over but with Rental Guarantee'
                || objC.RGS_Option__c == 'Option 1 – Opted out of RGS')){
                if (objC.Booking_Unit__c != null && objC.Booking_Unit__r.RGS_Percent__c == null) {
                    objInstance = new GenerateDrawloopDocumentBatch(caseId
                        , System.Label.LMA_existing_non_resident_DDP
                        , System.Label.LMA_existing_non_resident_Template);
                } else if (objC.Booking_Unit__c != null && objC.Booking_Unit__r.RGS_Percent__c != null){
                    if (objC.RGS_Option__c == 'Option 1 – Opted out of RGS') {
                        objInstance = new GenerateDrawloopDocumentBatch(caseId
                            , System.Label.LMA_with_RGS_DDP
                            , System.Label.LMA_with_RGS_Template);
                    } else if (objC.RGS_Option__c == 'Option 2 – In RGS') {
                        objInstance = new GenerateDrawloopDocumentBatch(caseId
                            , System.Label.LMA_RGS_Option_2_DDP
                            , System.Label.LMA_RGS_Option_2_Template_Id);
                    }
                }
                if (objInstance != null) {
                    batchId = Database.ExecuteBatch(objInstance);
                }
                if(!string.isBlank(batchId) && String.valueOf(batchId) != '000000000000000'){
                    if (objC.Booking_Unit__c != null && objC.Booking_Unit__r.RGS_Percent__c == null){
                         SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                         objCaseAttachment.Case__c = caseId ;
                         objCaseAttachment.Name = 'Signed Agreement - Non resident';
                         objCaseAttachment.Booking_Unit__c = objC.Booking_Unit__c;
                         insert objCaseAttachment;
                    } else if (objC.Booking_Unit__c != null 
                        && objC.Booking_Unit__r.RGS_Percent__c != null) {
                         SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                         objCaseAttachment.Case__c = caseId ;
                         objCaseAttachment.Name = 'Signed Agreement with RGS';
                         objCaseAttachment.Booking_Unit__c = objC.Booking_Unit__c;
                         insert objCaseAttachment;
                    }
                    ApexPages.Message myMsg = 
                        new ApexPages.Message(
                        ApexPages.Severity.CONFIRM, 
                        'Your request for Agreement was successfully submitted. Please check the documents section for the document in a while.');
                    ApexPages.addMessage(myMsg);
                }else{
                    ApexPages.Message myMsg = 
                        new ApexPages.Message(
                        ApexPages.Severity.ERROR,
                        'Your request for Agreement could not be completed. Please try again later.');
                    ApexPages.addMessage(myMsg);
                }
            } else {
                ApexPages.Message myMsg = 
                    new ApexPages.Message(
                        ApexPages.Severity.ERROR,
                        'Finance Task needs to be comleted before generating Agreement.');
                ApexPages.addMessage(myMsg);
            }
        }else{
            ApexPages.Message myMsg = 
                new ApexPages.Message(
                ApexPages.Severity.ERROR,'Regid for the new unit has not been generated yet. Please try again later.');
            ApexPages.addMessage(myMsg);
        }
    }
}