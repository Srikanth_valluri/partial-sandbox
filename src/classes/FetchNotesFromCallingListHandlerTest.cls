/***********************************************************************
* Description - Test class developed for FetchNotesFromCallingListHandler
*
* Version            Date            Author            Description
* 1.0                12/12/17        Naresh (accely)    Initial Draft
* 1.1                18/12/17                           Initial Draft
**********************************************************************/

@isTest(seeAllData=false)
public class FetchNotesFromCallingListHandlerTest{
    
    
    public static testmethod void getTestedForCallingList(){
        Test.startTest(); 
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Account acc = TestDataFactory_CRM.createPersonAccount();
        insert acc;
        TriggerOnOffCustomSetting__c obj1 = new TriggerOnOffCustomSetting__c();
        obj1.Name = 'CallingListTrigger';
        obj1.OnOffCheck__c = true;
        insert obj1;
        
        Calling_List__c CallObj = new Calling_List__c();
        CallObj.RecordTypeId  = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        CallObj.Account__c = acc.Id;  
        CallObj.Mobile_Phone__c   = '9856214598';  
        CallObj.Email__c   = 'abc@gmail.com'; 
        insert CallObj;
        
        
        ApexPages.currentPage().getParameters().put('id',acc.id);
        
        ContentVersion contentVersion = new ContentVersion(
        Title = 'Test',
        PathOnClient = 'test.jpg',
        VersionData = Blob.valueOf('Test Content'),
        IsMajorVersion = true);
        
        insert contentVersion; 

        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
 
        ContentDocumentLink cdl = New ContentDocumentLink(
            LinkedEntityId = CallObj.id, ContentDocumentId = documents[0].Id, shareType = 'I');
            insert cdl ;
                
       
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        FetchNotesFromCallingListHandler obj = new FetchNotesFromCallingListHandler(controller);
        
        obj.fetchNotes(); 
        Test.stopTest(); 
    }
     public static testmethod void getTestedForAccounts(){
        Test.startTest(); 
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Account acc = TestDataFactory_CRM.createPersonAccount();
        insert acc;
        
        
        
        ApexPages.currentPage().getParameters().put('id',acc.id);
        
        ContentVersion contentVersion = new ContentVersion(
        Title = 'Test',
        PathOnClient = 'test.jpg',
        VersionData = Blob.valueOf('Test Content'),
        IsMajorVersion = true);
        
        insert contentVersion; 

        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
 
        ContentDocumentLink cdl = New ContentDocumentLink(
            LinkedEntityId = acc.id, ContentDocumentId = documents[0].Id, shareType = 'I');
            insert cdl ;
                
       
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        FetchNotesFromCallingListHandler obj = new FetchNotesFromCallingListHandler(controller);
        
        obj.fetchNotes(); 
        Test.stopTest(); 
    }
     public static testmethod void getTestedForCases(){
        Test.startTest(); 
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Account acc = TestDataFactory_CRM.createPersonAccount();
        insert acc;
        
        Case newCase = TestDataFactory_CRM.createCase(acc.Id ,devRecordTypeId);
        insert newCase; 
        
        ApexPages.currentPage().getParameters().put('id',acc.id);
        
        ContentVersion contentVersion = new ContentVersion(
        Title = 'Test',
        PathOnClient = 'test.jpg',
        VersionData = Blob.valueOf('Test Content'),
        IsMajorVersion = true);
        
        insert contentVersion; 

        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
 
        ContentDocumentLink cdl = New ContentDocumentLink(
            LinkedEntityId = newCase.id, ContentDocumentId = documents[0].Id, shareType = 'I');
            insert cdl ;
                
       
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        FetchNotesFromCallingListHandler obj = new FetchNotesFromCallingListHandler(controller);
        
        obj.fetchNotes(); 
        Test.stopTest(); 
    }
     public static testmethod void getTestedInnerClass(){
        Test.startTest();
        Calling_List__c callObj;
        Case caseObj;
        Account accObj;
        ContentNote NoteLst;
        Note NoteListToMove;
        Id NoteId;
        FetchNotesFromCallingListHandler.CalllingToNotesWrapper obj = new FetchNotesFromCallingListHandler.CalllingToNotesWrapper('Calling List',callObj,NoteLst,NoteListToMove,NoteId);
        FetchNotesFromCallingListHandler.CalllingToNotesWrapper obj2 = new FetchNotesFromCallingListHandler.CalllingToNotesWrapper('Cases',caseObj,NoteLst,NoteListToMove,NoteId);
        FetchNotesFromCallingListHandler.CalllingToNotesWrapper obj3 = new FetchNotesFromCallingListHandler.CalllingToNotesWrapper('Account',accObj,NoteLst,NoteListToMove,NoteId);
       
        Test.stopTest();
    }
}