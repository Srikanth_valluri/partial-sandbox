/*
* Description - Test class developed for AdditionalParkingWSDLService
*
* Version            Date            Author            Description
* 1.0              18/12/2017      Ashish Agarwal      Initial Draft
*/

@isTest
private class AdditionalParkingWSDLServiceTest {

    static testMethod void testGetParkingInformation() {
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, AdditionalParkingWSDL.getParkingResponse_element>();
            AdditionalParkingWSDL.getParkingResponse_element response1 = new AdditionalParkingWSDL.getParkingResponse_element();
            response1.return_x = '"status":"e"';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            
            AdditionalParkingWSDLService.getParkingInformation( 12.12 );
            
        test.stopTest();
    }
    
    static testMethod void testCreateParkingPaymentTerms() {
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, AdditionalParkingWSDL.getParkingResponse_element>();
            AdditionalParkingWSDL.getParkingResponse_element response1 = new AdditionalParkingWSDL.getParkingResponse_element();
            response1.return_x = '"status":"e"';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            
            AdditionalParkingWSDLService.createParkingPaymentTerms( new list<parkingPaymentTermsXxdcParkingInvW.APPSXXDC_PARKING_INVX1844909X3X2>() );
            
        test.stopTest();
    }

    static testMethod void testUpdateParkingStatusInIPMS() {
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, AdditionalParkingWSDL.getParkingResponse_element>();
            AdditionalParkingWSDL.getParkingResponse_element response1 = new AdditionalParkingWSDL.getParkingResponse_element();
            response1.return_x = '"status":"e"';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );
            
            AdditionalParkingWSDLService.updateParkingStatusInIPMS(64547.00, 13245.00,'131',100.00,'123', 'sold');

          processXxdcProcessServiceWsPlsqlSoParkin.APPSXXDC_PROCESS_SERX1794747X1X5[] regTerms = new processXxdcProcessServiceWsPlsqlSoParkin.APPSXXDC_PROCESS_SERX1794747X1X5[1];
          processXxdcProcessServiceWsPlsqlSoParkin.APPSXXDC_PROCESS_SERX1794747X1X5 objRegTerm = new processXxdcProcessServiceWsPlsqlSoParkin.APPSXXDC_PROCESS_SERX1794747X1X5();
           objRegTerm.PARAM_ID = '2-12345';
           objRegTerm.ATTRIBUTE1 = '100';
           objRegTerm.ATTRIBUTE10 = '';
           objRegTerm.ATTRIBUTE11 = '';
           objRegTerm.ATTRIBUTE12 = '';
           objRegTerm.ATTRIBUTE13 = '';
           objRegTerm.ATTRIBUTE14 = '';
           objRegTerm.ATTRIBUTE15 = '';
           objRegTerm.ATTRIBUTE16 = '';
           objRegTerm.ATTRIBUTE17 = '';
           objRegTerm.ATTRIBUTE18 = '';
           objRegTerm.ATTRIBUTE19 = '';
           objRegTerm.ATTRIBUTE2 = '';
           objRegTerm.ATTRIBUTE20 = '';
           objRegTerm.ATTRIBUTE21 = '';
           objRegTerm.ATTRIBUTE22 = '';
           objRegTerm.ATTRIBUTE23 = '';
           objRegTerm.ATTRIBUTE24 = '';
           objRegTerm.ATTRIBUTE25 = '';
           objRegTerm.ATTRIBUTE26 = '';
           objRegTerm.ATTRIBUTE27 = '';
           objRegTerm.ATTRIBUTE28 = '';
           objRegTerm.ATTRIBUTE29 = '';
           objRegTerm.ATTRIBUTE3 = '';
           objRegTerm.ATTRIBUTE30 = '';
           objRegTerm.ATTRIBUTE31 = '';
           objRegTerm.ATTRIBUTE32 = '';
           objRegTerm.ATTRIBUTE33 = '';
           objRegTerm.ATTRIBUTE34 = '';
           objRegTerm.ATTRIBUTE35 = '';
           objRegTerm.ATTRIBUTE36 = '';
           objRegTerm.ATTRIBUTE37 = '';
           objRegTerm.ATTRIBUTE38 = '';
           objRegTerm.ATTRIBUTE39 = '';
           objRegTerm.ATTRIBUTE4 = '';
           objRegTerm.ATTRIBUTE41 = '';
           objRegTerm.ATTRIBUTE42 = '';
           objRegTerm.ATTRIBUTE43 = '';
           objRegTerm.ATTRIBUTE44 = '';
           objRegTerm.ATTRIBUTE45 = '';
           objRegTerm.ATTRIBUTE46 = '';
           objRegTerm.ATTRIBUTE47 = '';
           objRegTerm.ATTRIBUTE48 = '';
           objRegTerm.ATTRIBUTE49 = '';
           objRegTerm.ATTRIBUTE5 = '';
           objRegTerm.ATTRIBUTE50 = '';
           objRegTerm.ATTRIBUTE6 = '';
           objRegTerm.ATTRIBUTE7 = '';
           objRegTerm.ATTRIBUTE8 = '';
           objRegTerm.ATTRIBUTE9 = '';
           regTerms[0] = objRegTerm;
           AdditionalParkingWSDLService.postParkingFeeToIPMS('2-12345',regTerms);
            
        test.stopTest();
    }
    
    static testMethod void testCreateTasksForParking() {
        // Insert Account
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            objCase.Pending_Amount__c = 10 ;
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        test.startTest();
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
            AdditionalParkingWSDLService.createTasksForParking( new list<Case>() );
            AdditionalParkingWSDLService.createTasksForParking( lstCases );
            
        test.stopTest();
    }
}