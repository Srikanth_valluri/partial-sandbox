public class fmcViewAmenityBookingController {
    private static final Map<String, String> RECEIPT_ATTRIBUTE_MAP = new Map<String, String> {
        'Name' => 'SRNumber',
        'Task_Id__c' => 'TaskId',
        'CAFM_Location__r.Description__c' => 'Location',
        'Short_Description__c' => 'ShortDescription',
        'Raised_Date__c' => 'RaisedDate',
        'CAFM_Task_State__c' => 'TaskState',
        'RefreshStatus' => 'RefreshStatus'
    };
    public List<SelectOption> lstUnit                           {get; set;}
    public transient List<Booking_Unit__c> bookingUnitList      {get;set;}
    public String strUnitId                                     {get;set;}

    public fmcViewAmenityBookingController() {
        if (FmcUtils.isCurrentView('ViewBookAmenity')) {
          lstUnit = getUnitOptions();
        }
    }

    private static List<SelectOption> getUnitOptions() {
        List<SelectOption> lstOptions = new List<SelectOption>{new SelectOption('', 'Select', false)};
        for (Booking_Unit__c unit : FmcUtils.queryOwnedUnitsForAccount(
            CustomerCommunityUtils.customerAccountId, 'Id, Name, Unit_Name__c'
        )) {
            lstOptions.add(new SelectOption(unit.Id, unit.Unit_Name__c));
        }
        return lstOptions;
    }

    @RemoteAction
    public static list<FM_Case__c> fetchFMCases(String strBoookingUnitId){
        List<FM_Case__c> lstAmenityFMCases = new List<FM_Case__c>();
        ID bookAmenityRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Amenity Booking').getRecordTypeId();
        System.debug('bookAmenityRecordTypeId---------: '+bookAmenityRecordTypeId);

        System.debug('>>>strBoookingUnitId : '+strBoookingUnitId);
        if(strBoookingUnitId != NULL && String.isNotBlank(strBoookingUnitId)){
            lstAmenityFMCases = [Select Amenity_Name__c
                                       , Amenity_Booking_Status__c
                                       , Booking_Date__c
                                       , Resource_Booking_Due_Date__c
                                       , Booking_Start_Time_p__c
                                       , Booking_End_Time_p__c
                                       , Resource_Booking_Chargeable_Fee__c
                                       , Resource_Booking_Deposit__c
                                       , Booking_Unit__c
                                       , Unit_Name__c
                                       , Id
                                  FROM FM_Case__c
                                  Where Booking_Unit__c =: strBoookingUnitId
                                  //AND Task_Id__c != NULL
                                  AND RecordTypeId =: bookAmenityRecordTypeId];
            System.debug('>>>>lstAmenityFMCases : '+lstAmenityFMCases);
        }
        return lstAmenityFMCases;
    }
}