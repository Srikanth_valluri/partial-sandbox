/*
* Description - Test class developed for 'ReceiptDetails1'
*
* Version            Date            Author            Description
* 1.0                28/11/17        Monali            Initial Draft
*/
@isTest
private class ReceiptDetails1Test {
    static testMethod void testMethod1() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        Test.startTest();

        SOAPCalloutServiceMock.returnToMe = new Map<String, ReceiptDetails1.GenerateReceipt1Response_element>();
        ReceiptDetails1.GenerateReceipt1Response_element response1 = new ReceiptDetails1.GenerateReceipt1Response_element();
        response1.return_x = '{"data":[{"ATTRIBUTE3":"1040788","ATTRIBUTE10":"118265456","ATTRIBUTE2":"CASH","ATTRIBUTE1":"943590","ATTRIBUTE14":"75017","ATTRIBUTE13":"Y","ATTRIBUTE12":"13-DEC-2015","ATTRIBUTE11":"CASH","ATTRIBUTE9":"5000","ATTRIBUTE8":"5000","ATTRIBUTE7":"0","ATTRIBUTE6":"5000","ATTRIBUTE5":"AED","ATTRIBUTE4":"RAGINI RAJAN PATIL","ATTRIBUTE18":null,"ATTRIBUTE17":"DH/32/3204 - Token amount paid by cash","ATTRIBUTE16":"Cheque/Cash to be Remitted","ATTRIBUTE15":null,"ATTRIBUTE19":null}],"message":"[16 Receipts found For Given PartyID]","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );

        ReceiptDetails1.ReceiptIPMSHttpSoap11Endpoint obj = new ReceiptDetails1.ReceiptIPMSHttpSoap11Endpoint();
        String response = obj.GenerateReceipt1('GET_RECEIPT_LIST','SFDC',objAcc.party_ID__C,'','','');
        ReceiptDetails1.GenerateReceipt_element obj1 = new ReceiptDetails1.GenerateReceipt_element();
        Test.stopTest();

    }

    static testMethod void testMethod2() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;
        
        Test.startTest();

        SOAPCalloutServiceMock.returnToMe = new Map<String, ReceiptDetails1.GetReceiptDetailsResponse_element>();
        ReceiptDetails1.GetReceiptDetailsResponse_element response1 = new ReceiptDetails1.GetReceiptDetailsResponse_element();
        response1.return_x = '{"data":[{"ATTRIBUTE3":"1040788","ATTRIBUTE10":"118265456","ATTRIBUTE2":"CASH","ATTRIBUTE1":"943590","ATTRIBUTE14":"75017","ATTRIBUTE13":"Y","ATTRIBUTE12":"13-DEC-2015","ATTRIBUTE11":"CASH","ATTRIBUTE9":"5000","ATTRIBUTE8":"5000","ATTRIBUTE7":"0","ATTRIBUTE6":"5000","ATTRIBUTE5":"AED","ATTRIBUTE4":"RAGINI RAJAN PATIL","ATTRIBUTE18":null,"ATTRIBUTE17":"DH/32/3204 - Token amount paid by cash","ATTRIBUTE16":"Cheque/Cash to be Remitted","ATTRIBUTE15":null,"ATTRIBUTE19":null}],"message":"[16 Receipts found For Given PartyID]","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );

        ReceiptDetails1.ReceiptIPMSHttpSoap11Endpoint obj = new ReceiptDetails1.ReceiptIPMSHttpSoap11Endpoint();
        ReceiptDetails3.APPSXXDC_PROCESS_SERX1794747X2X4 regTerms = new ReceiptDetails3.APPSXXDC_PROCESS_SERX1794747X2X4();
			regTerms.ATTRIBUTE1 = '';
			regTerms.ATTRIBUTE10 = '';
			regTerms.ATTRIBUTE11 = '';
			regTerms.ATTRIBUTE12 = '';
			regTerms.ATTRIBUTE13 = '';
			regTerms.ATTRIBUTE14 = '';
			regTerms.ATTRIBUTE15 = '';
			regTerms.ATTRIBUTE16 = '';
			regTerms.ATTRIBUTE17 = '';
			regTerms.ATTRIBUTE18 = '';
			regTerms.ATTRIBUTE19 = '';
			regTerms.ATTRIBUTE2 = '';
			regTerms.ATTRIBUTE20 = '';
			regTerms.ATTRIBUTE21 = '';
			regTerms.ATTRIBUTE22 = '';
			regTerms.ATTRIBUTE23 = '';
			regTerms.ATTRIBUTE24 = '';
			regTerms.ATTRIBUTE25 = '';
			regTerms.ATTRIBUTE26 = '';
			regTerms.ATTRIBUTE27 = '';
			regTerms.ATTRIBUTE28 = '';
			regTerms.ATTRIBUTE29 = '';
			regTerms.ATTRIBUTE3 = '';
			regTerms.ATTRIBUTE30 = '';
			regTerms.ATTRIBUTE31 = '';
			regTerms.ATTRIBUTE32 = '';
			regTerms.ATTRIBUTE33 = '';
			regTerms.ATTRIBUTE34 = '';
			regTerms.ATTRIBUTE35 = '';
			regTerms.ATTRIBUTE36 = '';
			regTerms.ATTRIBUTE37 = '';
			regTerms.ATTRIBUTE38 = '';
			regTerms.ATTRIBUTE39 = '';
			regTerms.ATTRIBUTE4 = '';
			regTerms.ATTRIBUTE41 = '';
			regTerms.ATTRIBUTE42 = '';
			regTerms.ATTRIBUTE43 = '';
			regTerms.ATTRIBUTE44 = '';
			regTerms.ATTRIBUTE45 = '';
			regTerms.ATTRIBUTE46 = '';
			regTerms.ATTRIBUTE47 = '';
			regTerms.ATTRIBUTE48 = '';
			regTerms.ATTRIBUTE49 = '';
			regTerms.ATTRIBUTE5 = '';
			regTerms.ATTRIBUTE50 = '';
			regTerms.ATTRIBUTE6 = '';
			regTerms.ATTRIBUTE7 = '';
			regTerms.ATTRIBUTE8 = '';
			regTerms.ATTRIBUTE9 = '';
			regTerms.PARAM_ID = objAcc.party_ID__C;
			
        String response = obj.GetReceiptDetails('2-'+String.valueOf( Datetime.now().getTime()),'GET_RECEIPT_LIST','SFDC', regTerms);
        Test.stopTest();

    }

    static testMethod void testMethod3() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        List<ReceiptDetails2.APPSXXDC_PROCESS_SERX1794747X1X5> lstRegTerms = new List<ReceiptDetails2.APPSXXDC_PROCESS_SERX1794747X1X5>();
        Test.startTest();

        SOAPCalloutServiceMock.returnToMe = new Map<String, ReceiptDetails1.GenerateReceiptResponse_element>();
        ReceiptDetails1.GenerateReceiptResponse_element response1 = new ReceiptDetails1.GenerateReceiptResponse_element();
        response1.return_x = '{"data":[{"ATTRIBUTE3":"1040788","ATTRIBUTE10":"118265456","ATTRIBUTE2":"CASH","ATTRIBUTE1":"943590","ATTRIBUTE14":"75017","ATTRIBUTE13":"Y","ATTRIBUTE12":"13-DEC-2015","ATTRIBUTE11":"CASH","ATTRIBUTE9":"5000","ATTRIBUTE8":"5000","ATTRIBUTE7":"0","ATTRIBUTE6":"5000","ATTRIBUTE5":"AED","ATTRIBUTE4":"RAGINI RAJAN PATIL","ATTRIBUTE18":null,"ATTRIBUTE17":"DH/32/3204 - Token amount paid by cash","ATTRIBUTE16":"Cheque/Cash to be Remitted","ATTRIBUTE15":null,"ATTRIBUTE19":null}],"message":"[16 Receipts found For Given PartyID]","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );

        ReceiptDetails1.ReceiptIPMSHttpSoap11Endpoint obj = new ReceiptDetails1.ReceiptIPMSHttpSoap11Endpoint();
        
        String res1 = obj.GenerateReceipt('2-'+String.valueOf( Datetime.now().getTime()),'GET_RECEIPT_LIST','SFDC',lstRegTerms);
        Test.stopTest();

    }

    
}