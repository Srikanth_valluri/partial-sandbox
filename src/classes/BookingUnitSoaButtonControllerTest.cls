@isTest
private class BookingUnitSoaButtonControllerTest {

    @TestSetup
    static void customSettingData() {

        insert new IpmsRestServices__c(
                   SetupOwnerId = UserInfo.getOrganizationId(),
                   BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                   Username__c = 'oracle_user',
                   Password__c = 'crp1user',
                   Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                   BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                   Timeout__c = 120000
                   );

        insert new TriggerOnOffCustomSetting__c(
        SetupOwnerId = UserInfo.getOrganizationId(),
        Name = 'CallingListTrigger',
        OnOffCheck__c = true);
    }

    @isTest
    static void getUnitSoaUrlNullTest() {

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id);
        insert booking;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Registration_ID__c     = '3901');
        insert bookingUnit;

        String blobStr = 'Test Blob Response';

        FmHttpCalloutMock.Response unitSoaMockResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": " "  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": " "  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ');

        FmHttpCalloutMock.Response blobResponse = new FmHttpCalloutMock.Response(200, 'Success', blobStr);

        String UNIT_SOA_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.UNIT_SOA, new List<String> {'3901'});

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            UNIT_SOA_str => unitSoaMockResponse,
            ' ' => blobResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.startTest();
            PageReference pgRef = Page.BookingUnitSoaButton;
            Test.setCurrentPage(pgRef);
            pgRef.getParameters().put('id', bookingUnit.Id);
            ApexPages.StandardController sc = new ApexPages.standardController(bookingUnit);

            BookingUnitSoaButtonController obj = new BookingUnitSoaButtonController(sc);
            //obj.getUnitSoaUrl();
            obj.getUnitSoaUrlBookingUnit();
        Test.stopTest();
    }

    @isTest
    static void getUnitSoaUrlTest() {

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                        Agency_ID__c = '1234');
        insert sr;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id);
        insert booking;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Registration_ID__c     = '3901');
        insert bookingUnit;

        String blobStr = 'Test Blob Response';

        FmHttpCalloutMock.Response unitSoaMockResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ');

        FmHttpCalloutMock.Response blobResponse = new FmHttpCalloutMock.Response(200, 'Success', blobStr);

        String UNIT_SOA_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.UNIT_SOA, new List<String> {'3901'});

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            UNIT_SOA_str => unitSoaMockResponse,
            'https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a' => blobResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.startTest();
            PageReference pgRef = Page.BookingUnitSoaButton;
            Test.setCurrentPage(pgRef);
            pgRef.getParameters().put('id', bookingUnit.Id);
            ApexPages.StandardController sc = new ApexPages.standardController(bookingUnit);

            BookingUnitSoaButtonController obj = new BookingUnitSoaButtonController(sc);
            //obj.getUnitSoaUrl();
            obj.getUnitSoaUrlBookingUnit();
        Test.stopTest();
    }

    @isTest
    static void getUnitSoaUrlCallingListTest() {

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                        Agency_ID__c = '1234');
        insert sr;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id);
        insert booking;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Registration_ID__c     = '3901');
        insert bookingUnit;

        Calling_List__c callLst = new Calling_List__c(
                                                Booking_Unit__c = bookingUnit.Id
                                                );
        insert callLst;

        String blobStr = 'Test Blob Response';

        FmHttpCalloutMock.Response unitSoaMockResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ');

        FmHttpCalloutMock.Response blobResponse = new FmHttpCalloutMock.Response(200, 'Success', blobStr);

        String UNIT_SOA_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.UNIT_SOA, new List<String> {'3901'});

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            UNIT_SOA_str => unitSoaMockResponse,
            'https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a' => blobResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.startTest();
            PageReference pgRef = Page.CallingListSoaButton;
            Test.setCurrentPage(pgRef);
            pgRef.getParameters().put('id', callLst.Id);
            ApexPages.StandardController sc = new ApexPages.standardController(callLst);

            BookingUnitSoaButtonController obj = new BookingUnitSoaButtonController(sc);
            obj.getUnitSoaUrlCallingList();
        Test.stopTest();

    }
}