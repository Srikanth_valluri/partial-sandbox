/****************************************************************************************************
* Name          : FlyinDetailsController                                                            *
* Description   : FlyinDetails page Controler                                                       *
* Created Date  : 25-07-2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE        COMMENTS                                                    *
* 1.0   Craig Lobo          25-07-2018  Initial Draft.  
* 2.0   Bhanu Gupta         12-12-2018  Updated the code to add new Travel Details record           *
****************************************************************************************************/

global with sharing class FlyinDetailsController { 
    public blob file                                                                    {get; set;}
    public Travel_Details__c travelDetailsRec                                           {get; set;}
    public Passenger_Details__c passengerList                                           {get; set;}
    public String isDisplay                                                             {get; set;}
    public String msgType                                                               {get; set;}
    public String message                                                               {get; set;}
    public String passenegrId                                                           {get; set;}
    public String departureLocation                                                     {get; set;}
    public String arrivalLocation                                                       {get; set;}
    public String travelDetailsRecId                                                    {get; set;}
    public String inqId;
    public List<SelectOption> departureOptions;
    public List<SelectOption> departureCountryOptions;
    public List<SelectOption> departureCityOptions;
    public List<SelectOption> arrivalOptions;
    public String departureDate                                                         {get; set;}
    public String returnDate                                                            {get; set;}
    public String passId                                                                {get; set;}
    public String filename                                                              {get; set;}
    public String body                                                                  {get; set;}
    public String uploadform                                                            {get; set;}
    public String paramValue                                                            {get; set;}
    public String contentType                                                           {get; set;}
    public Map<ID,List<Attachment>> passengerID_attachmentlst                           {get; set;}
    public String fname                                                                 {get; set;}
    public List<SelectOption> classofTraveloptions;
    public List<SelectOption> transportoptions;
    public List<SelectOption> serviceTypeOptions;
    public List<SelectOption> validVisaOptions;
    public Boolean checkBoxval                                                          {get; set;}
    public Boolean isTermsAndConditions                                                 {get; set;}
    public Boolean displayPopup                                                         {get; set;}
    public PageReference pg                                                             {get; set;}
    public Passenger_Details__c passengerDetail                                         {get; set;}
    public String passengerDob                                                          {get; set;}
    public String passengerIdExpiry                                                     {get; set;}
    public List<SelectOption> passengerIdTypeoptions;
    
    //Collect paggenger details
    public String passengerName                                                         {get; set;}
    
    //Store inquiry if inqid is avalaible
    Inquiry__c inq                                                                      {get; set;}
    
    //Link Expired
    public Boolean detailsconfirmedorexpired                                            {get; set;}
    public Boolean renderButton                                                         {get; set;}
    public String alertmessage                                                          {get; set;}
    public String travelInqEmail                                                        {get; set;}
    public String travelInqTSAEmail                                                     {get; set;}
    public String travelInqid                                                     {get; set;}
    
    public Integer passengerCount {get; set;}
    public boolean proofOfPaymentAttached { get; set; }
    public Boolean showUploadBtn {get; set;}
    public String objectName {get; set;}
    public boolean filesUploaded { get; set; }
    
    public FlyinDetailsController(ApexPages.StandardController controller) {
        renderButton=false;
        passengerCount = 0;
        proofOfPaymentAttached = false; filesUploaded = false;
        showUploadBtn = true;
        pg = Page.UploadFlyinDocument;
        displayPopup = false;
        detailsconfirmedorexpired= false;
        passId = '';
        //arrivalLocation = 'Dubai';
        isDisplay = '';
        if (String.isBlank(message)) {
            isDisplay = 'hide';
        }
        msgType = '';
        message = '';
        //travelDetailsRecId = controller.getRecord().Id;
        if(Apexpages.currentpage().getparameters().get('inqid') == null ){
            travelDetailsRecId = Apexpages.currentpage().getparameters().get('recid');
        } else {
            inqId= Apexpages.currentpage().getparameters().get('inqid'); 
        }
        if ((String.isNotBlank(inqId) || String.isNotBlank(travelDetailsRecId))) {
            passengerDetail = new Passenger_Details__c();
            passengerDob = '';
            passengerIdExpiry = '';
            initQuery();
            populatePassengerAttachmentmap();
        }
        
    }

    /************************************************************************************************
    * @Description : Method to Display the success/Error messages                                   *
    *                depending on their visiblity, Value and Type                                   *
    * @Params      : pDisplay > show/hide, pType > success/error, pMessage > custom message         *
    * @Return      : void                                                                           *
    ************************************************************************************************/
    /*public void messageBlock(String pDisplay, String pType, String pMessage) {
        isDisplay = pDisplay;
        msgType = pType;
        message = pMessage ;
    } */

    public List<SelectOption> getValidVisaOptions(){
        Schema.DescribeFieldResult fieldResult = Passenger_Details__c.Valid_Visa__c.getDescribe();
        return getPicklistOptions(fieldResult);
        /*List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'None'));
        Schema.DescribeFieldResult fieldResult = Passenger_Details__c.Valid_Visa__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;*/
    
    }
    
    public List<SelectOption> getPassengerIdTypeoptions(){
        Schema.DescribeFieldResult fieldResult = Passenger_Details__c.Id_Type__c.getDescribe();
        return getPicklistOptions(fieldResult);
        /*List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'None'));
        Schema.DescribeFieldResult fieldResult = Passenger_Details__c.Id_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;*/
    }
    
    public List<SelectOption> getClassofTraveloptions(){
        Schema.DescribeFieldResult fieldResult = Travel_Details__c.Class_of_Travel__c.getDescribe();
        return getPicklistOptions(fieldResult);
        /*List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'None'));
        Schema.DescribeFieldResult fieldResult = Travel_Details__c.Class_of_Travel__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;*/
    }
    
    public List<SelectOption> getTransportoptions(){
        Schema.DescribeFieldResult fieldResult = Travel_Details__c.Transport__c.getDescribe();
        return getPicklistOptions(fieldResult);
        /*List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'None'));
        Schema.DescribeFieldResult fieldResult = Travel_Details__c.Transport__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;*/
    }
    
    public List<SelectOption> getDepartureCountryOptions() {
        Schema.DescribeFieldResult fieldResult = Travel_Details__c.Departure_Country__c.getDescribe();
        return getPicklistOptions(fieldResult);
        /*List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'None'));
        Schema.DescribeFieldResult fieldResult = Travel_Details__c.Departure_Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        //ple.sort();
        for( Schema.PicklistEntry f : ple) {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;*/
    }
    @RemoteAction 
    public static List<String> getDepatureCities (String pSearchKey) {
        String searchString = '%'
            + pSearchKey 
            + '%';
        List<String> citiesList = new List<String>();
        for (FlyIn_Departure_Cities__mdt city :[ SELECT MasterLabel FROM FlyIn_Departure_Cities__mdt WHERE MasterLabel LIKE :searchString LIMIT 10 ]) {
            citiesList.add(city.masterLabel);
        }
        return citiesList;
    }
    public List<SelectOption> getDepartureCityOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'None'));
       /* for (FlyIn_Departure_Cities__mdt cities : [SELECT MasterLabel, City_Name__c FROM FlyIn_Departure_Cities__mdt WHERE masterlabel != NULL]) {
            options.add(new SelectOption(cities.masterlabel , cities.masterlabel ));
        }*/
        return options;
    }

    public List<SelectOption> getArrivalOptions(){
        Schema.DescribeFieldResult fieldResult = Travel_Details__c.Arrival_Location__c.getDescribe();
        return getPicklistOptions(fieldResult);
        /*List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'None'));
        
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        //ple.sort();
        for( Schema.PicklistEntry f : ple)
        {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
        }  
        return options;*/
    }
    
    public List<SelectOption> getServiceTypeOptions(){
        Schema.DescribeFieldResult fieldResult = Travel_Details__c.Service_Type__c.getDescribe();
        return getPicklistOptions(fieldResult);
        /*List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'None'));
        
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        //ple.sort();
        for( Schema.PicklistEntry f : ple)
        {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
        }   
        
        return options;*/
    }

    public List<SelectOption> getPicklistOptions(Schema.DescribeFieldResult fieldResult) {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'None'));
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple) {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }

    /**
     * 
     */
    public void initQuery() {
        String travelDetailsQuery = getQuery();
        System.debug('@@@inqid @@@' + inqid);
        if(inqid == null){
           travelDetailsQuery += ' WHERE Id = \''
                               + travelDetailsRecId
                               + '\' LIMIT 1';
        } else {
            travelDetailsQuery += ' WHERE Inquiry__c = \''
                               + inqid
                               + '\' AND Status__c != \'Closed\' '
                               + ' Order by createddate desc LIMIT 1 ';
            inq = [Select Email__c,Owner_s_Manager_Email__c from Inquiry__c where id=:inqid limit 1];
        }                    
        System.debug('travelDetailsQuery>>>> ' + travelDetailsQuery); 
        List<Travel_Details__c> traveldetailslst = Database.query(travelDetailsQuery);
        String createNew = Apexpages.currentpage().getparameters().get('new');
        if (createNew == 'true') {
            traveldetailslst = new List <Travel_Details__c> ();
        }
        if(traveldetailslst.size() > 0){
            
            renderButton=true;
            travelInqid = traveldetailslst[0].Inquiry__c;
            travelInqEmail = traveldetailslst[0].Inquiry__r.email__c;
            travelInqTSAEmail = traveldetailslst[0].Inquiry__r.Owner_s_Manager_Email__c;
           travelDetailsRec = traveldetailslst[0];
           serviceTypeOptions = new List<SelectOption>();
           for (Attachment att :[SELECT Name FROM Attachment WHERE ParentId =: travelInqId]) {
               proofOfPaymentAttached = true;
           }
           if(String.isnotBlank(travelDetailsRec.Service_Type__c)){
              //serviceTypeOptions.add(travelDetailsRec.Service_Type__c); 
              /*String[] optionsarr = travelDetailsRec.Service_Type__c.split(',');
              for(String str: optionsarr){
                  System.debug('@@@ str ### '+ str);
                  serviceTypeOptions.add(new SelectOption(str,str));
              }*/ 
           }
        }

        if(travelDetailsRec  == null){
            travelDetailsRec = new Travel_Details__c();
            travelDetailsRec.Inquiry_Email__c = inq.email__c;
            travelDetailsRec.TSA_Manager_Email__c = inq.Owner_s_Manager_Email__c;
            //travelDetailsRec.Arrival_Location__c = 'Dubai';
            departureLocation = '';
            departureDate = '';
            returnDate = '';
        }else{
            travelDetailsRecId = travelDetailsRec.id;
            //departureLocation  = travelDetailsRec.Departure_Location__c;
            //departureDate = travelDetailsRec.From_Date__c +'';

            if(travelDetailsRec.From_Date__c != null){
                departureDate = travelDetailsRec.From_Date__c.month()+'/'+travelDetailsRec.From_Date__c.day()+'/'+travelDetailsRec.From_Date__c.year();                                                      
            }

            if(travelDetailsRec.To_Date__c != null){
                returnDate = travelDetailsRec.To_Date__c.month()+'/'+travelDetailsRec.To_Date__c.day()+'/'+travelDetailsRec.To_Date__c.year(); 
            }

            /*if(travelDetailsRec.Email_Sent_Time__c != null){
                //Datetime ins = travelDetailsRec.Email_Sent_Time__c - System.now();
                Long emailtime = travelDetailsRec.Email_Sent_Time__c.getTime();
                Long nowtime = DateTime.now().getTime();
                Long minutes = (nowtime - emailtime)/60000;
                System.debug('@@@ minutes for email @@@@  ' + minutes);
                /*if(minutes > 30 && Userinfo.getProfileId() == '00e0Y000000Nl7WQAS'){
                    isExpired= true;        
                } */
                //System.debug('@@ Time span @@ ' + travelDetailsRec.Email_Sent_Time__c.getTime() - Datetime.now().getTime());
            /*} */
        }
    }

    // 2.0  Added by Bhanu Gupta
    public PageReference createNewTravelDetailRecord(){
            travelDetailsRec = new Travel_Details__c();
            travelDetailsRec.Inquiry_Email__c = travelInqEmail;
            travelDetailsRec.TSA_Manager_Email__c = travelInqTSAEmail;
            //travelDetailsRec.Arrival_Location__c = 'Dubai';
            travelDetailsRec.Inquiry__c = travelInqid; 
            updateInquiryFltinTour(travelDetailsRec.Inquiry__c);
            upsert travelDetailsRec;
            travelDetailsRecId = travelDetailsRec.Id;
            PageReference pageRef = new PageReference('/apex/FlyinDetails?recid='+ travelDetailsRec.Id);
            pageRef.setRedirect(true);

            return pageRef;
    }
    
    public String getQuery(){
        // Get the fields for the Travel_Details__c object field Set
        String travelDetailsQuery = ' SELECT ';
        List<Schema.FieldSetMember> fsTravelMemberList 
            = SObjectType.Travel_Details__c.FieldSets.FlyinDetails_Field_Set.getFields();
        for (Schema.FieldSetMember fsMember : fsTravelMemberList) {
            travelDetailsQuery += fsMember.getFieldPath() + ', ';
        }

        // Get the fields for the Passenger_Details__c object field Set
        String passengerDetailsQuery = ' ( SELECT '; 
        List<Schema.FieldSetMember> fsPassengerMemberList 
            = SObjectType.Passenger_Details__c.FieldSets.Flyin_Passengers_Field_Set.getFields();
        for (Schema.FieldSetMember fsMember : fsPassengerMemberList) {
            passengerDetailsQuery += fsMember.getFieldPath() + ', ';
        }
        passengerDetailsQuery = passengerDetailsQuery.removeEnd(', '); 
        passengerDetailsQuery += ' FROM Passenger_Details__r order by createddate desc) ';
        travelDetailsQuery += passengerDetailsQuery + ',Inquiry__r.email__c,Inquiry__r.Owner_s_Manager_Email__c FROM Travel_Details__c';
        System.debug('===travelDetailsQuery=='+travelDetailsQuery);
        return travelDetailsQuery;
    }
    
    public PageReference saveTravelDetails(){
        String loadAtt = apexpages.currentpage().getparameters().get('loadAttachment');
        
        if(travelDetailsRec.Id == null){
            travelDetailsRec.Inquiry__c = inqid; 
            updateInquiryFltinTour(travelDetailsRec.Inquiry__c);
        }

        if(String.isnotBlank(departureDate)){
            String[] formatdate = departureDate.split('/');
            Date formatdeparturedate = Date.newInstance(Integer.valueof(formatdate[2].trim()),Integer.valueof(formatdate[0].trim()),Integer.valueof(formatdate[1].trim()));
            travelDetailsRec.From_Date__c = formatdeparturedate;
        }
        
        if(String.isnotBlank(returnDate)){
            System.debug('returnDate>>>>>  '+ returnDate);
            String[] formatretdate = returnDate.split('/');
            Date formatreturndate = Date.newInstance(Integer.valueof(formatretdate[2].trim()),Integer.valueof(formatretdate[0].trim()),Integer.valueof(formatretdate[1].trim())); 
            travelDetailsRec.To_Date__c = formatreturndate;
        }
        //travelDetailsRec.Arrival_Location__c ='Dubai';//Hardcoded Dubai for flyin use case 
        
        upsert travelDetailsRec;

        travelDetailsRecId = travelDetailsRec.Id;
        PageReference pageRef = new PageReference('/apex/FlyinDetails?recid='+ travelDetailsRec.Id+'&att='+loadAtt);
        pageRef.setRedirect(true);

        return pageRef;
        
    }

    @future
    public static void updateInquiryFltinTour(Id pInqId) {
        if (String.isNotBlank(pInqId)) {
            Inquiry__c inq = new Inquiry__c(Id = pInqId, Flyin_Tour__c = true);
            update inq;
        }
    }

    public Pagereference sendTravelDetails(){
        if(travelDetailsRec != null  && travelDetailsRec.Status__c != 'Tickets Confirmed'){
            System.debug('travelDetailsRec.Email_Type__c @@@@@ '+ travelDetailsRec.Email_Type__c);
            travelDetailsRec.Email_Type__c ='Details Link';
            update travelDetailsRec;
            System.debug('@@@ updateddetails link @@@ ');
            PageReference pageRef = new PageReference('/apex/FlyinDetails?recid='+travelDetailsRecId);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }
    
    public void populatePassengerAttachmentmap(){
        passengerID_attachmentlst  = new Map<ID,List<Attachment>>();
        passengerCount = travelDetailsRec.Passenger_Details__r.size();
        for(Passenger_Details__c passenger :travelDetailsRec.Passenger_Details__r){
            passengerID_attachmentlst.put(passenger.Id,new List<Attachment>());
        }

        if(passengerID_attachmentlst != null){
            
            Map <ID, List <Attachment>> passengerAttachments = new Map <ID, List <Attachment>> ();
            
            for(Attachment att : [Select id,parentId,name from Attachment where parentID in :passengerID_attachmentlst.keyset() ]){
                if (passengerAttachments.containsKey (att.parentId))
                    passengerAttachments.get (att.parentId).add(att);            
                else
                    passengerAttachments.put (att.parentId, new List <Attachment> {att});
                
                //passengerID_attachmentId.get(att.parentID).put(String.valueOf(att.id));
                //passengerID_attachmentId.put(att.parentID,String.valueOf(att.id));
                if(passengerID_attachmentlst.containskey(att.parentID)){
                   List<Attachment> attlst = passengerID_attachmentlst.get(att.parentID);
                   attlst.add(att); 
                }
            }//end of for
            
            for(Passenger_Details__c passenger :travelDetailsRec.Passenger_Details__r){
                if (passengerAttachments.containskey (passenger.id) && filesUploaded == false) {
                     filesUploaded = false;    
                } else {
                    filesUploaded = true;
                }
            }
        
        }//end of if
        
    }

    public PageReference savepassengerdetail(){
        Passenger_Details__c passengerDetailremote = new Passenger_Details__c();
        if(System.currentPageReference().getParameters().get('passengerfisrtname') != null){
           passengerDetailremote.Passenger_First_Name__c = System.currentPageReference().getParameters().get('passengerfisrtname'); 
        }

        if(System.currentPageReference().getParameters().get('passengerlastname') != null){
           passengerDetailremote.Passenger_Last_Name__c =System.currentPageReference().getParameters().get('passengerlastname');  
        }

        if(System.currentPageReference().getParameters().get('passengerdob') != null){
           String passengerdob = System.currentPageReference().getParameters().get('passengerdob');
           if(String.isnotBlank(passengerdob)){
                String[] formatdate = passengerdob.split('/');
                if(formatdate.size() == 3){
                    Date passengerdobt = Date.newInstance(Integer.valueof(formatdate[2].trim()),Integer.valueof(formatdate[0].trim()),Integer.valueof(formatdate[1].trim()));
                    passengerDetailremote.DOB__c = passengerdobt;
                }
            }
        }

        if(System.currentPageReference().getParameters().get('passengervalidvisa') != null){
            passengerDetailremote.Valid_Visa__c = System.currentPageReference().getParameters().get('passengervalidvisa');
        }

        if(System.currentPageReference().getParameters().get('passengeridtype') != null){
            passengerDetailremote.Id_Type__c = System.currentPageReference().getParameters().get('passengeridtype');
        }

        if(System.currentPageReference().getParameters().get('passengeridnum') != null){
            passengerDetailremote.Id_Number__c = System.currentPageReference().getParameters().get('passengeridnum');
        }

        String passengerexpiddt;
        if(System.currentPageReference().getParameters().get('passengerexpiddate') != null){
            passengerexpiddt= System.currentPageReference().getParameters().get('passengerexpiddate');
            if(String.isnotBlank(passengerexpiddt)){
                String[] formatdate = passengerexpiddt.split('/');
                if(formatdate.size() == 3){
                    Date passengerespdat = Date.newInstance(Integer.valueof(formatdate[2].trim()),Integer.valueof(formatdate[0].trim()),Integer.valueof(formatdate[1].trim()));
                    passengerDetailremote.Id_Expiry__c = passengerespdat ;
                }
            }
        }

        String travelrecid= System.currentPageReference().getParameters().get('travelrecid');
        if(String.isNotBlank(travelrecid)){
          passengerDetailremote.Travel_Details__c = travelrecid;  
        }
        insert passengerDetailremote ;
        initQuery();
        populatePassengerAttachmentmap();

        //reload page
        PageReference pageRef = new PageReference('/apex/FlyinDetails?recid='+travelDetailsRecId);
        pageRef.setRedirect(true);
        return pageRef;

    }
    
    public void updateDetailsconfirmed(){
        Id profileId = [ SELECT Id FROM Profile WHERE Name = 'DAMAC Profile'].Id;
        if(UserInfo.getProfileId() == profileid && travelDetailsRec != null ) {
            if(Apexpages.currentpage().getparameters().get('tc') != null ) {
                if(Apexpages.currentpage().getparameters().get('tc') == 'true') {
                    isTermsAndConditions = true;
                }
            } else {
                isTermsAndConditions = false; 
                if(travelDetailsRec.Details_Confirmed__c == true){
                    alertmessage='The Details are already Confirmed';
                    detailsconfirmedorexpired = true; 
                }
                else if(Apexpages.currentpage().getparameters().get('val') != null ) {
                    if(Apexpages.currentpage().getparameters().get('val') == 'true') {
                        detailsconfirmedorexpired = true;
                        alertmessage='Thank You for Confirming your Booking Details';
                        travelDetailsRec.Details_Confirmed__c = true;
                        update travelDetailsRec;
                    }
                } else if(travelDetailsRec != null 
                    && travelDetailsRec.Email_Sent_Time__c != null
                ) {
                    Long emailtime = travelDetailsRec.Email_Sent_Time__c.getTime();
                    Long nowtime = DateTime.now().getTime();
                    Long minutes = (nowtime - emailtime)/60000;
                    System.debug('@@@ minutes for email @@@@  ' + minutes);
                    if(minutes > 30) {
                        detailsconfirmedorexpired = true;
                        alertmessage='Sorry !! The Link has Expired'; 
                    }
                }//end of else if
            }
        }
    }//end of details confirmed


    public void showuploadpage(){
        displayPopup= true;
        String travelidparam = System.currentPageReference().getParameters().get('travelidparam');
        String passengeridparam = System.currentPageReference().getParameters().get('passengeridparam');
        System.debug('@@ Travelid param @@@ ' + travelidparam);
        System.debug('@@ passengeridparam param @@@ ' + passengeridparam );
        
        pg = Page.UploadFlyinDocument;
        //Apexpages.currentpage().getparameters().put('travelid','a549E0000008dAYQAY');
        Apexpages.currentpage().getparameters().put('travelid',travelidparam);
        //Apexpages.currentpage().getparameters().put('passengerid','a5A9E0000009QXk');
        Apexpages.currentpage().getparameters().put('passengerid',passengeridparam ); 
    }
     public void showuploadpage1(){
        displayPopup= true;
    }
    
    public PageReference deleteattachment(){
        Id attachid = apexpages.currentpage().getparameters().get('deleteattid');
        Attachment att = new Attachment();
        att.id = attachid;
        delete att;
        PageReference pageRef = new PageReference('/apex/FlyinDetails?recid='+travelDetailsRecId);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public pageReference updateTravelDetailsStatus() {
            String travelId = Apexpages.currentpage().getparameters().get('recid');
            Travel_Details__c  travelRec = [SELECT Id, Status__c
                       FROM Travel_Details__c WHERE Id = :travelId];
            travelRec.Status__c = 'Submitted';
            update travelRec;
            PageReference pageRef = new PageReference('/apex/FlyinDetails?recid='+travelId );
            pageRef.setRedirect(true);
            return pageRef;
            
        
    }
    public pageReference confirmTicketStatus() {
            String travelId = Apexpages.currentpage().getparameters().get('recid');
            Travel_Details__c  travelRec = [SELECT Id, Status__c
                       FROM Travel_Details__c WHERE Id = :travelId];
            travelRec.Status__c = 'Tickets Confirmed';
            update travelRec;
            PageReference pageRef = new PageReference('/apex/FlyinDetails?recid='+travelId );
            pageRef.setRedirect(true);
            return pageRef;
            
        
    }
    
}