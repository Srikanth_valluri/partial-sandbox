@isTest
public class Manage_Inventory_PCs_Controller_Test {

    public static testmethod void UnitTest()
    {
        Inventory__c inv=new Inventory__c();
        inv.Start_Date__c=system.today()-10;
        inv.End_Date__c=system.today()+3;
        insert inv;
        
        Inventory_User__c invUser=new Inventory_User__c();
        invUser.Inventory__c=inv.id;
        invUser.User__c=userInfo.getUserId();
        insert invUser;
        
        pagereference PageRef=Page.Manage_Inventory_PCs;
       	pageRef.getParameters().put('id',inv.id);
       	Test.setCurrentPageReference(pageRef);
        
		ApexPages.StandardController stdLead = new ApexPages.StandardController(inv);
		Manage_Inventory_PCs_Controller controller=new Manage_Inventory_PCs_Controller();
        controller=new Manage_Inventory_PCs_Controller(stdLead);
 		
        object o= controller.controller;
        
        Manage_Inventory_PCs_Controller.autoCompleteForUsers('test',inv.id,'Property');
        
        controller.searchUser=new Search_User__c();
        
        
        controller.prepareQueryString();
        
        controller.searchUser.Team__c='team1';
        controller.prepareQueryString();
        controller.selectedUserIds=userinfo.getUserId();
        controller.createInventoryUsers();
        controller.serachforUsersWithFilters();
    }
}