@isTest
public class AgencyPCTriggerHandlerTest {
	
	private static List<User> lstUsers = new List<User>();
    @testSetup static void setupData() {
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
        Map<string,id> mpProfile = new Map<string,id>();
        for(profile p : [select id,name from profile where name in ('Agent Admin Team','Property Consultant','Agent Admin Manager','Customer Community Login User')]){
            mpProfile.put(p.name,p.id); 
        }
        
        Account portalAccount1 = new Account(Name = 'TestAccount',Agency_Short_Name__c='testAGN');
        Database.insert(portalAccount1);
        
        //Create contact
        Contact contact1 = new Contact(
            FirstName = 'Test',
            Lastname = 'McTesty',
            AccountId = portalAccount1.Id,
            Email = System.now().millisecond() + 'test@test.com'
        );
        Database.insert(contact1);
        
        
        lstUsers.add(new User(
            Username = System.now().millisecond() + 'test12345@test.com',
            ContactId = contact1.Id,
            ProfileId = mpProfile.get('Customer Community Login User'),
            Alias = 'test123',
            Email = 'test12345@test.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'McTesty',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'));
        
        lstUsers.add(new user(ProfileId = mpProfile.get('Agent Admin Manager'),
                              LastName = 'aamla2st',
                              Email = 'aampuser000@ama2mama.com',
                              Username = 'aampuser000@am2amama.com' + System.currentTimeMillis(),
                              CompanyName = 'TESTaam',
                              Title = 'titleaam',
                              Alias = 'a2iasaam',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              EmailEncodingKey = 'UTF-8',
                              LanguageLocaleKey = 'en_US',
                              LocaleSidKey = 'en_US'));
        
        lstUsers.add(new user(ProfileId = mpProfile.get('Property Consultant'),
                              LastName = 'la2st',
                              Email = 'puser000@ama2mama.com',
                              Username = 'puser000@am2amama.com' + System.currentTimeMillis(),
                              CompanyName = 'TEST3',
                              Title = 'title',
                              Alias = 'a2ias',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              EmailEncodingKey = 'UTF-8',
                              LanguageLocaleKey = 'en_US',
                              LocaleSidKey = 'en_US'));
        lstUsers.add(new user(ProfileId = mpProfile.get('Property Consultant'),
                              LastName = 'la3st',
                              Email = 'puser000@ama3mama.com',
                              Username = 'puser000@am3amama.com' + System.currentTimeMillis(),
                              CompanyName = 'TEST3',
                              Title = 'title',
                              Alias = 'a3ias',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              EmailEncodingKey = 'UTF-8',
                              LanguageLocaleKey = 'en_US',
                              LocaleSidKey = 'en_US'));
        lstUsers.add(new user(ProfileId = mpProfile.get('Property Consultant'),
                              LastName = 'la4st',
                              Email = 'puser000@ama4mama.com',
                              Username = 'puser000@am4amama.com' + System.currentTimeMillis(),
                              CompanyName = 'TEST3',
                              Title = 'title',
                              Alias = 'a4ias',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              EmailEncodingKey = 'UTF-8',
                              LanguageLocaleKey = 'en_US',
                              LocaleSidKey = 'en_US'));
        insert lstUsers;
      }
    }
    
	@isTest static void test_method_3() {
        Test.starttest();
        {
            List<User> lstUsers = [select id,name,profile.name from user where CompanyName = 'TEST3'];
            
            Account a = new Account();
            a.Name = 'Test Account';
            a.Agency_Short_Name__c = 'testASN';
            a.OwnerId = lstUsers[1].Id;
            insert a;
            
            
            
            a = [select id,name,Agency_Short_Name__c,UniqueNo__c,OwnerId from account where id =: a.id];
            List<CollaborationGroup> lstCG = new List<CollaborationGroup>();
            lstCG.add(new CollaborationGroup(name = a.Agency_Short_Name__c+a.UniqueNo__c+'-Sales', CollaborationType='Public'));
            insert lstCG;
            
            List<Agency_PC__c> lstagencies = new List<Agency_PC__c>();
            lstagencies.add(new Agency_PC__c(User__c = lstUsers[1].id,Agency__c =a.id));
            insert lstagencies;
            
            delete lstagencies;
            
            AgencyPCTriggerHandler apthObject = new AgencyPCTriggerHandler();
            apthObject.executeBeforeInsertTrigger(new List<sObject>());
            apthObject.executeBeforeUpdateTrigger(new Map<Id, sObject>(), new Map<Id, sObject>());
            apthObject.executeBeforeInsertUpdateTrigger(new list<sObject>(), new Map<Id, sObject>());
            apthObject.executeAfterInsertUpdateTrigger(new Map<Id, sObject>(), new Map<Id, sObject>());
            Set<Id> UserIdSet = new Set<Id>();
            UserIdSet.add(lstUsers[0].Id);
            Map <Id, Set<Id>> accIdUserMap = new Map <Id, Set<Id>>{a.Id => UserIdSet};
            Map <Id, Id> accIdOwnerMap = new Map <Id, Id>{a.Id => a.OwnerId};   
            AgencyPCTriggerHandler.shareAccount(accIdUserMap,accIdOwnerMap);
        }
        test.stopTest();
    }
}