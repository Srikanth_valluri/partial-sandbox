/**************************************************************************************************************************************************
* Description : Class to Get Customer LOAMS Outstanding Balance against Registration ID for the Visualforce page "GetCustomerLoamsOutstandingBal"
*==================================================================================================================================================
* Ver      Date-DD/MM/YYYY     Author              Modification
*===================================================================================================================================================
* 1.0      29/09/2020       Raveena Jain          Intial Draft
****************************************************************************************************************************************************/
public class GetCustomerLoamsOutstandingBalCntrl {
    
    public static String showResponse {get;set;}
    public static Id caseId {get;set;}
    
    public GetCustomerLoamsOutstandingBalCntrl(ApexPages.StandardController stdController){
        caseId = (Id)stdController.getRecord().Id;    
    }
    
    public static void init(){
        
        List<Case> lstCaseToUpdate = new List<Case>();
        /*Credentials_Details__c creds = Credentials_Details__c.getInstance( 'CustomerLOAMSOutstandingBalance' );
        Boolean isValidCreds = (creds != null && String.isNotBlank(creds.Endpoint__c) && String.isNotBlank(creds.User_Name__c) && String.isNotBlank(creds.Password__c)) ? True : False;
        
        String body = '';        */
        String rId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Service_Charge_Reset').getRecordTypeId();    
        
        List<SR_Booking_Unit__c> lstSRBu = [  SELECT Registration_ID__c  
                                            , Reset_End_Date__c
                                            , Reset_Start_Date__c
                                            FROM SR_Booking_Unit__c 
                                            WHERE Case__c =: caseId ];
        
        if( !lstSRBu.isEmpty()) {
            String requestBody = '';
            
            for(SR_Booking_Unit__c objSRBu : lstSRBu ) {
                
                JSON2ApexWrapper responseObj = getServiceChargeAmt( String.isNOtBlank(objSRBu.Registration_ID__c) ? objSRBu.Registration_ID__c : ''
                                                                  , String.isNotBlank(String.valueOf(objSRBu.Reset_Start_Date__c)) ? String.valueOf(objSRBu.Reset_Start_Date__c) : ''
                                                                  , String.isNotBlank(String.valueOf(objSRBu.Reset_End_Date__c)) ? String.valueOf(objSRBu.Reset_End_Date__c) : '');
                
                System.debug('responseObj: '+responseObj);
                if( responseObj.OutputParameters.X_RETURN_STATUS == 'S'
                   && responseObj.OutputParameters.X_ERROR_MSG != Null){
                       
                       lstCaseToUpdate.add(new Case(Id =caseId, Amount_to_be_waived__c = Decimal.valueOf(responseObj.OutputParameters.X_BALANCE_AMOUNT)));
                       if(!lstCaseToUpdate.isEmpty()){
                        Update lstCaseToUpdate;
                       }
                       
                    showResponse = responseObj.OutputParameters.X_ERROR_MSG;
                    ApexPages.addmessage( new ApexPages.message( ApexPages.severity.CONFIRM
                                            , showResponse
                                           )
                    );
                }
                
                if( responseObj.OutputParameters.X_RETURN_STATUS != 'S' ) {
                    
                    ApexPages.addmessage( new ApexPages.message( ApexPages.severity.INFO 
                                                                ,responseObj.OutputParameters.X_ERROR_MSG
                                                               )
                                        );
                }
                
                
                /*LOAMS_Bal objLoamsBal = new LOAMS_Bal();
                objLoamsBal.InputParameters = new InputParameters();
                
                objLoamsBal.InputParameters.P_REGISTRATION_ID = String.isNOtBlank(objSRBu.Registration_ID__c) ? objSRBu.Registration_ID__c : '';
                objLoamsBal.InputParameters.P_FROM_DATE = String.isNotBlank(String.valueOf(objSRBu.Reset_End_Date__c)) ? formatDate(String.valueOf(objSRBu.Reset_End_Date__c)) : '';               
                objLoamsBal.InputParameters.P_TO_DATE = String.isNotBlank(String.valueOf(objSRBu.Reset_Start_Date__c)) ? formatDate(String.valueOf(objSRBu.Reset_Start_Date__c)) : '';
                
                
                String JSONData = JSON.serialize(objLoamsBal);
                body = '{"LOAMS_Bal": '+JSONData+'}';
                System.debug('JSONData: '+JSONData);
                System.debug('body: '+body);
                if(String.isNotBlank(body)) {
                    HTTPResponse resp = returnResponseFromService(creds.Endpoint__c, 'POST', creds.User_Name__c, creds.Password__c, body); //h.send(request);
                    
                    JSON2ApexWrapper responseObj = ( JSON2ApexWrapper )System.JSON.deserialize(resp.getBody(), JSON2ApexWrapper.class);
                    System.debug('responseObj: '+responseObj);
                    if(resp.getStatusCode() == 200 && responseObj.OutputParameters.X_RETURN_STATUS == 'S'
                       && responseObj.OutputParameters.X_ERROR_MSG != Null){
                           
                           lstCaseToUpdate.add(new Case(Id =caseId, Amount_to_be_waived__c = Decimal.valueOf(responseObj.OutputParameters.X_BALANCE_AMOUNT)));
                           if(!lstCaseToUpdate.isEmpty()){
                           Update lstCaseToUpdate;
                           }
                           
                        showResponse = responseObj.OutputParameters.X_ERROR_MSG;
                        ApexPages.addmessage( new ApexPages.message( ApexPages.severity.CONFIRM
                                                , showResponse
                                               )
                        );
                    }
                    
                    if( responseObj.OutputParameters.X_RETURN_STATUS != 'S' ) {
                        
                        ApexPages.addmessage( new ApexPages.message( ApexPages.severity.INFO 
                                                                    , 'Error while processing your request' 
                                                                   )
                                            );
                    }
                    
                    
                    System.debug('Response+++'+resp); // GenericUtility.createErrorLog(); create error log in both condition failure and success
                    System.debug('Response Body+++'+resp.getBody());
                    
                }*/
            }
            
        }
    }
    
    public static JSON2ApexWrapper getServiceChargeAmt( String strRegId
                                            , String strFromDate 
                                            , String strToDate ){
          System.debug('strRegId--->'+strRegId); 
          System.debug('strFromDate--->'+strFromDate);
           System.debug('strToDate--->'+strToDate);
        JSON2ApexWrapper responseObj = new JSON2ApexWrapper();
        List<Case> lstCaseToUpdate = new List<Case>();
        Credentials_Details__c creds = Credentials_Details__c.getInstance( 'CustomerLOAMSOutstandingBalance' );
        Boolean isValidCreds = (creds != null && String.isNotBlank(creds.Endpoint__c) && String.isNotBlank(creds.User_Name__c) && String.isNotBlank(creds.Password__c)) ? True : False;
        if(isValidCreds) {
            String body = '';   
            LOAMS_Bal objLoamsBal = new LOAMS_Bal();
            objLoamsBal.InputParameters = new InputParameters();
            
            objLoamsBal.InputParameters.P_REGISTRATION_ID = String.isNOtBlank(strRegId) ? strRegId : '';
            System.debug(' formatDate(strFromDate)--->'+formatDate(strFromDate));
            
            objLoamsBal.InputParameters.P_FROM_DATE = String.isNotBlank(strFromDate) ? formatDate(strFromDate) : ''; 
            System.debug('objLoamsBal.InputParameters.P_FROM_DATE--->'+objLoamsBal.InputParameters.P_FROM_DATE);
           // objLoamsBal.InputParameters.P_FROM_DATE = String.isNotBlank(strFromDate) ?  formatDate(String.valueOf(strFromDate))  : ''; 
           
            objLoamsBal.InputParameters.P_TO_DATE = String.isNotBlank(strToDate) ? formatDate(strToDate) : '';
           
            
            String JSONData = JSON.serialize(objLoamsBal);
            body = '{"LOAMS_Bal": '+JSONData+'}';
            System.debug('JSONData: '+JSONData);
            System.debug('body: '+body);
            if(String.isNotBlank(body)) {
                HTTPResponse resp = returnResponseFromService(creds.Endpoint__c, 'POST', creds.User_Name__c, creds.Password__c, body); //h.send(request);
                if(resp.getStatusCode() == 200) {
                    responseObj = ( JSON2ApexWrapper )System.JSON.deserialize(resp.getBody(), JSON2ApexWrapper.class);
                } else {
                    responseObj.OutputParameters.X_ERROR_MSG = 'Error while processing your request';
                }
                System.debug('responseObj: '+responseObj);
                
                
            }
        }
        return responseObj;
    }
    
    //Method used to convert date format from yyyy-mm-dd to dd-mmm-yyyy to SF format.
    public static  String formatDate( String strDate ){
        System.debug('strDate---->'+strDate);
        Map<String,String> myMap = new Map<String,String>{
            '01' => 'JAN',
                '02' => 'FEB', 
                '03' => 'MAR',
                '04' => 'APR',
                '05' => 'MAY', 
                '06' => 'JUN',
                '07' => 'JUL',
                '08' => 'AUG', 
                '09' => 'SEP',
                '10' => 'OCT',
                '11'=> 'NOV', 
                '12' => 'DEC'
                };
                    
                    List<String > lstStr = strDate.Split('-');
                 System.debug('lstStr---->'+lstStr);
        return lstStr[2]+'-'+myMap.get(lstStr[1])+'-'+lstStr[0];
        
    }
    
    public static HTTPResponse returnResponseFromService(String endpoint,String method,String userName,String password, String body) {
        String authorization  = 'Basic';
        
        HTTP h = new HTTP();
        HTTPRequest request = new HTTPRequest();
        request.setEndpoint(endpoint);
        request.setMethod(method);
        request.setHeader('Accept', 'application/json');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Accept-Language', 'en-US');
        Blob headerValue = Blob.valueOf(userName + ':' +password);
        String authorizationHeader = authorization + EncodingUtil.base64Encode(headerValue);
        request.setHeader('Authorization', authorizationHeader);
        request.setBody(body);
        request.setTimeout(120000);
        System.debug('request: '+request);
        HTTPResponse resp = h.send(request);
        return resp;
    }
    public pageReference back(){
        caseId = ApexPages.currentPage().getParameters().get('id');
        pageReference pgr = new pageReference('/'+caseId);
        pgr.setRedirect(true);
        return pgr;
    }
    
    public class LOAMS_Bal {
        public InputParameters InputParameters;
    }
    
    public class InputParameters {
        public String P_REGISTRATION_ID;
        public String P_FROM_DATE;
        public String P_TO_DATE;
    }
    
    public class JSON2ApexWrapper {
        public OutputParameters OutputParameters;
    }
    
    public class OutputParameters {
        public String xmlns;
        public String xmlnsxsi;
        public String X_REGISTRATION_ID;
        public String X_BALANCE_AMOUNT;
        public String X_RETURN_STATUS;
        public String X_ERROR_MSG;
    }
}