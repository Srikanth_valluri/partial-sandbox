/******************************************************************************
* Description - Test class developed for GetJOPDAreaonBUScheduler
*
* Version            Date            Author                    Description
* 1.0                04/02/2018           						Initial Draft
********************************************************************************/
@isTest
private class GetJOPDAreaonBUSchedulerTest {

    static testMethod void testScheduler() {
    	Test.startTest();
	    	GetJOPDAreaonBUScheduler ascsObject = new GetJOPDAreaonBUScheduler();
			String sch = '0 0 23 * * ?'; 
		//	system.schedule('demo ', sch, ascsObject); 
		Test.stopTest();
    }
     static testMethod void testScheduler2() {
    	Test.startTest();
    		String jobName = 'demo job';
	    	GetJOPDAreaonBUScheduler ascsObject = new GetJOPDAreaonBUScheduler(jobName);
			String sch = '0 0 23 * * ?'; 
			system.schedule(jobName+System.Now(), sch, ascsObject); 
		Test.stopTest();
    }
}// End of class.