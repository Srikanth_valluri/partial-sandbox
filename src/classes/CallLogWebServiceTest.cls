@isTest
private class CallLogWebServiceTest {

    @isTest static void callLogDetailsCheck(){

        User userObj = new User(
                             ProfileId = [SELECT Id FROM Profile WHERE Name = 'Contact Center - Manager'].Id,
                             LastName = 'test',
                             Email = 'puser000@test.com',
                             Username = 'puser000@test.com' + System.currentTimeMillis(),
                             CompanyName = 'TEST',
                             Title = 'title',
                             isActive = true,
                             Alias = 'alias',
                             Extension = '7001',
                             TimeZoneSidKey = 'Asia/Dubai',
                             EmailEncodingKey = 'UTF-8',
                             LanguageLocaleKey = 'en_US',
                             LocaleSidKey = 'en_IN'
        );
        insert userObj ;


        CallLogLogic.CallLogRequestBody reqst = new CallLogLogic.CallLogRequestBody();
        reqst.callingNumber = '8787878787';
        reqst.calledNumber = '999999999';
        reqst.agentExtension = '7001';
        reqst.callType = 'Inbound';
        reqst.startTime = '10/15/2017 23:30:20';
        reqst.endTime = '10/15/2017 1:30:20';
        reqst.terminatingParty = 'Customer';
        reqst.terminationReason = '';
        reqst.callRecordingUrl = 'https://callRecording.com';

        String JsonMsg=JSON.serialize(reqst);

        Test.startTest();

        //As Per Best Practice it is important to instantiate the Rest Context

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/callWrapup';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;

//        Test.setMock(HttpCalloutMock.class, new CallLogWebServiceMock());

        CallLogLogic.CallLogRequestBody resp = new CallLogLogic.CallLogRequestBody();
        CallLogWebService.doPost();
        Test.stopTest();
        Call_Log__c callLog = [Select Id From Call_Log__c];
        System.assertNotEquals(null,callLog );
    }

     @isTest static void callLogDetailsUerInvalidCheck(){

        /*User userObj = new User(
                             ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
                             LastName = 'test',
                             Email = 'puser000@test.com',
                             Username = 'puser000@test.com' + System.currentTimeMillis(),
                             CompanyName = 'TEST',
                             Title = 'title',
                             isActive = true,
                             Alias = 'alias',
                             Extension = '7901',
                             TimeZoneSidKey = 'Asia/Dubai',
                             EmailEncodingKey = 'UTF-8',
                             LanguageLocaleKey = 'en_US',
                             LocaleSidKey = 'en_IN'
        );
        insert userObj ; */


        CallLogLogic.CallLogRequestBody reqst = new CallLogLogic.CallLogRequestBody();
        reqst.callingNumber = '8787878787';
        reqst.calledNumber = '999999999';
        reqst.agentExtension = '79001';
        reqst.callType = 'Inbound';
        reqst.startTime = '10/15/2017 23:30:20';
        reqst.endTime = '10/15/2017 1:30:20';
        reqst.terminatingParty = 'Customer';
        reqst.terminationReason = '';
        reqst.callRecordingUrl = 'https://callRecording.com';

        String JsonMsg=JSON.serialize(reqst);

        Test.startTest();

        //As Per Best Practice it is important to instantiate the Rest Context

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/callWrapup';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;

//        Test.setMock(HttpCalloutMock.class, new CallLogWebServiceMock());

        CallLogLogic.CallLogRequestBody resp = new CallLogLogic.CallLogRequestBody();
        CallLogWebService.doPost();
        Test.stopTest();


        String callWrapResponse = RestContext.response.responseBody.toString();
        System.assert(callWrapResponse != null);
        CallLogLogic.CallLogResponseBody  response=
            (CallLogLogic.CallLogResponseBody) JSON.deserializeStrict(
                callWrapResponse,
                CallLogLogic.CallLogResponseBody.Class
            );
        //System.assertEquals(response.errorCode, '400');
        System.assertEquals(response.errorMessage, 'No CRE User found for the given extension');
    }

    @isTest static void callLogDetailsDateTimeInvalidCheck(){

        User userObj = new User(
                             ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
                             LastName = 'test',
                             Email = 'puser000@test.com',
                             Username = 'puser000@test.com' + System.currentTimeMillis(),
                             CompanyName = 'TEST',
                             Title = 'title',
                             isActive = true,
                             Alias = 'alias',
                             Extension = '7001',
                             TimeZoneSidKey = 'Asia/Dubai',
                             EmailEncodingKey = 'UTF-8',
                             LanguageLocaleKey = 'en_US',
                             LocaleSidKey = 'en_IN'
        );
        insert userObj ;


        CallLogLogic.CallLogRequestBody reqst = new CallLogLogic.CallLogRequestBody();
        reqst.callingNumber = '8787878787';
        reqst.calledNumber = '999999999';
        reqst.agentExtension = '7001';
        reqst.callType = 'Inbound';
        reqst.startTime = '10/15/2017 23:30 AM';
        reqst.endTime = '10/15/2017 1:30:20';
        reqst.terminatingParty = 'Customer';
        reqst.terminationReason = '';
        reqst.callRecordingUrl = 'https://callRecording.com';

        String JsonMsg=JSON.serialize(reqst);

        Test.startTest();

        //As Per Best Practice it is important to instantiate the Rest Context

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/callWrapup';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;

//        Test.setMock(HttpCalloutMock.class, new CallLogWebServiceMock());

        CallLogLogic.CallLogRequestBody resp = new CallLogLogic.CallLogRequestBody();
        CallLogWebService.doPost();
        Test.stopTest();


        String callWrapResponse = RestContext.response.responseBody.toString();
        System.assert(callWrapResponse != null);
        CallLogLogic.CallLogResponseBody  response=
            (CallLogLogic.CallLogResponseBody) JSON.deserializeStrict(
                callWrapResponse,
                CallLogLogic.CallLogResponseBody.Class
            );
        //System.assertEquals(response.errorCode, '400');
        System.assertEquals(response.errorMessage, 'Please enter valid date format (DD/MM/YYYY HH:MM:SS)');
    }


}