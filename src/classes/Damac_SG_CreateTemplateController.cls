/*****************************************************************************************************************
* Name              : Damac_SG_CreateTemplateController
* Test Class        : Damac_SG_createTemplateTest
* Description       : Class to create Email Request Template/Version in SendGrid  
* -------------------------------------------------------------------------------------------------------------
* VERSION       AUTHOR          DATE            COMMENTS
* 1.0                       07/08/2018          Created
* 1.1           QBurst      10/03/2020          Added new Custom Settings to fetch SendGrid Credentials
*****************************************************************************************************************/
public without sharing class Damac_SG_CreateTemplateController {

    public Email_Request__c emailReq { get; set; }
    public String emailBody { get; set; }
    public Boolean htmlEmailType { get; set; }
    public String message { get; set; }
    public String messageType { get; set; }
    public List <String> imageURLs { get; set; }
    public String endPoint; // 1.1
    public string apiKey; // 1.1

    public Damac_SG_CreateTemplateController (Apexpages.StandardController stdController) {
        init ();
    }

    public Damac_SG_CreateTemplateController () {
        init ();    
    }

    public void init () {
        emailReq = new Email_Request__c ();
        emailBody = ''; 
        message = ''; 
        messageType = '';
        endPoint = ''; // 1.1
        apiKey = ''; // 1.1
        htmlEmailType = true;
        imageURLs = new List <String> ();
        string emailReqId = apexpages.currentpage().getparameters().get ('id');
        try {
            /************1.1 starts **********************/
            if(emailReqId != null && emailReqId != ''){
                id reqId = id.valueOf(emailReqId);
                if(string.valueOf(reqId.getSobjectType()) == 'Email_Request__c')
                    emailReq = [SELECT Name, subject__c, Sender_Account__c, Template_name__c, Template_Version_ID__c 
                        FROM Email_Request__c WHERE ID =: emailReqId];
                if(string.valueOf(reqId.getSobjectType()) == 'Email_Template_Versions__c'){
                    Email_Template_Versions__c etv = [Select id, Email_Request__c from Email_Template_Versions__c where id =: emailReqId];
                    emailReq = [SELECT Name, subject__c, Sender_Account__c, Template_name__c, Template_Version_ID__c 
                        FROM Email_Request__c WHERE ID =: etv.Email_Request__c];
                }
            }
            if(emailReq.Sender_Account__c != NULL && emailReq.Sender_Account__c != ''){
                SendGrid_Credentials_Sender_based__c credentials = 
                    SendGrid_Credentials_Sender_based__c.getValues(emailReq.Sender_Account__c);
                if(credentials != null){
                    endPoint = credentials.End_Point_URL__c.removeEND ('/mail/send')+'/templates';
                    apiKey = credentials.API_Key__c;
                }
            }
            if(endPoint == ''){
                Sendgrid_Credentials__c credentials = Sendgrid_Credentials__c.getInstance (UserInfo.getUserID ());
                endPoint = credentials.Endpoint_URL__c.removeEND ('/mail/send')+'/templates';
                apiKey = credentials.API_Key__c;
            }
            system.debug('endPoint: ' + endPoint);
            system.debug('apiKey: ' + apiKey);
            /***************1.1 ends*************************/
            if (emailReq.Template_Version_ID__c != NULL) {
                Integer num = 0;
                for (Email_Template_Versions__c versions :[SELECT Name FROM Email_Template_Versions__c 
                                                            WHERE Email_Request__c =: emailReq.ID]){
                    num += 1;
                }
                emailReq.Template_name__c = emailReq.Template_name__c+' V'+(num+1);
            }
        } catch (Exception e) {}
    }

    public void getContentDocURL () {
        imageURLs = new List <String> ();        
        List <String> fileTypes = new List <String> ();
        fileTypes.add ('jpg');
        fileTypes.add ('jpeg');
        fileTypes.add ('png');
        List <ID> contentDocumentIds = new List <ID> ();
        for (ContentDocument doc :[SELECT ID FROM ContentDocument 
                                   WHERE FileExtension IN :fileTypes 
                                   Order By ContentModifiedDate DESC LIMIT 100]) 
            contentDocumentIds.add (doc.ID);
        System.Debug (contentDocumentIds.size ());
        Map <ID, ID> contentDocumentIdToNewestVersion = new Map <ID, ID> ();
        for (ContentVersion version :[SELECT ContentDocumentId FROM ContentVersion 
                                      WHERE ContentDocumentId IN :contentDocumentIds
                                     AND IsLatest = TRUE]) {
            contentDocumentIdToNewestVersion.put (version.ContentDocumentId, version.ID) ;
        }
        List <ContentDistribution> distributionsToInsert = new List <ContentDistribution> ();
        Map <ID, String> contentDocumentUrls = new Map <ID, String> ();
        for (ContentDistribution url : [SELECT ContentDownloadUrl, ContentDocumentId 
                                        FROM ContentDistribution
                                       WHERE ContentDocumentId IN :contentDocumentIds 
                                       order By LastModifiedDate Desc]) {
            contentDocumentUrls.put (url.ContentDocumentId, url.ContentDownloadUrl);
        }
        System.Debug (contentDocumentUrls.keySet ().size ());
        for(Id thisContentDocumentId : contentDocumentIds) {
            if (!contentDocumentUrls.keySet ().contains(thisContentDocumentId)) {
                ContentDistribution newDistribution = new ContentDistribution(
                    ContentVersionId = contentDocumentIdToNewestVersion.get(thisContentDocumentId), 
                    Name = 'External Link',
                    PreferencesNotifyOnVisit = false);
                distributionsToInsert.add(newDistribution);
            }
        }
        
        if(!distributionsToInsert.isEmpty()) {
            insert distributionsToInsert;
            
            for (ContentDistribution url :[SELECT Id, DistributionPublicUrl, ContentDownloadUrl, ContentDocumentId
                                           FROM ContentDistribution 
                                           WHERE Id IN :
                                            (new Map<Id, ContentDistribution>(distributionsToInsert)).keySet() 
                                           order BY LastModifiedDate DESC]) {
                imageUrls.add (url.ContentDownloadUrl);   
            }
        }
        for (ID key :contentDocumentUrls.keySet()) {
            imageUrls.add (contentDocumentUrls.get (key));
        }
    }

    public void getEmailTemplate () {
        emailBody = '';
        String emailReqId = apexpages.currentpage().getparameters().get ('id');
        Email_request__c emailRec = new Email_request__c ();
        emailRec = [SELECT Email_template__c, Template_version_id__c 
                    FROM Email_request__c WHERE ID =:emailReqId];
        if (emailRec.Email_template__c != NULL && emailrec.Template_version_id__c != NULL) {
            HttpRequest req = new HttpRequest ();
            HTTP http = new HTTP ();
            HTTPResponse res = new HTTPresponse ();

            req.setMethod ('GET');
            req.setEndpoint (endPoint + '/' + emailRec.Email_template__c
                                + '/versions/' + emailRec.Template_version_id__c);
            req.setHeader('Authorization', 'Bearer ' + apiKey);
            req.setHeader('content-type', 'application/json');
            if (!Test.isRunningTest ()) {
                res = http.send (req);
            }
            if (Test.isRunningTest ()) {
                res.setStatusCode (200);
                res.setBody ('{"html_content" : "\u003c%body%\u003e", "plain_content" : "Testing"}');
            }
            if (res.getStatusCode() == 200) {
                Map <String, Object> responseMap = (Map <String, Object>) JSON.deserializeUntyped (res.getBody ());
                emailBody = (String) responseMap.get ('html_content');
                if (emailBody == '\u003c%body%\u003e') {
                    emailBody = (String) responseMap.get ('plain_content');
                }
            }
        }
    }

    public void getVersionTemplate () {
        emailBody = '';
        String emailReqId = apexpages.currentpage().getparameters().get ('id');
        Email_Template_Versions__c emailRec = new Email_Template_Versions__c ();
        emailRec = [SELECT Email_Request__r.Email_template__c, Template_version_id__c 
                    FROM Email_Template_Versions__c WHERE ID =:emailReqId];
        if (emailRec.Email_Request__r.Email_template__c != NULL && emailrec.Template_version_id__c != NULL) {
            HttpRequest req = new HttpRequest ();
            HTTP http = new HTTP ();
            HTTPResponse res = new HTTPresponse ();
            
            req.setMethod ('GET');
            req.setEndpoint (endPoint + '/' + emailRec.Email_Request__r.Email_template__c
                                    + '/versions/' + emailRec.Template_version_id__c);
            req.setHeader('Authorization', 'Bearer ' + apiKey);
            req.setHeader('content-type', 'application/json');
            if (!Test.isRunningTest ())
                res = http.send (req);
            if (Test.isRunningTest ()) {
                res.setStatusCode (200);
                res.setBody ('{"html_content" : "\u003c%body%\u003e", "plain_content" : "Testing"}');
            }
            System.Debug (res.getBody ());
            if (res.getStatusCode() == 200) {
                Map <String, Object> responseMap = (Map <String, Object>) JSON.deserializeUntyped (res.getBody ());
                emailBody = (String) responseMap.get ('html_content');
                if (emailBody == '\u003c%body%\u003e') {
                    emailBody = (String) responseMap.get ('plain_content');
                }
            }
        }
    }

    public void createTemplate () {
        String emailReqId = apexpages.currentpage().getparameters().get ('id');
        if (emailReqId != NULL) {
            deleteExistingVersions(emailReqId);
        }
        Email_request__c emailRec = new Email_request__c ();
        Boolean createTemplate = true;
        
        HTTP http = new HTTP ();
        HTTPResponse res = new HTTPresponse ();
        HTTPRequest req = new HTTPRequest ();
        
        String templateID = '';
        system.debug('endPoint: ' + endPoint);
        system.debug('apiKey: ' + apiKey);
        
        try {
            emailRec = [SELECT Email_template__c 
                        FROM Email_request__c WHERE Id =: emailReqId];
            if (emailrec.Email_template__c != NULL) {
                createTemplate = false;
                res.setStatusCode (201);
                res.setBody ('{"id":"' + emailRec.Email_template__c + '"}');
            }
        } catch (Exception e) {}
        
        req.setEndPoint (endPoint);
        req.setMethod ('POST');
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
            gen.writeStringField('name', emailreq.Template_Name__c);
        gen.writeEndObject();

        req.setBody (gen.getAsString());
        
        req.setHeader ('Authorization', 'Bearer ' + apiKey);
        req.setHeader ('Content-Type', 'application/json');

        System.debug (gen.getAsString());
        
        if (!Test.isRunningTest () && createTemplate) {
            res = http.send (req);
        }
        system.debug('Create Template Response body: ' + res.getBody());
        if (Test.isRunningTest ()) {
            res.setStatusCode (201);
            res.setBody ('{"id" : "' + emailreq.Email_template__c + '"}');
        }
        if (res.getStatusCode () == 201) {
            Map <String, Object> responseMap = (Map <String, Object>) JSON.deserializeUntyped (res.getBody ());
            if (responseMap.containsKey ('id')) {
                templateID = (String) responseMap.get ('id');
                gen = JSON.createGenerator(true);
                gen.writeStartObject();
                    gen.writeStringField('name', emailreq.Template_Name__c);
                    gen.writeStringField('subject', ''+emailreq.Subject__c);
                    gen.writeNumberField('active', 1);
                    if (htmlEmailType) {
                        gen.writeStringField('html_content', ''+emailBody);
                        gen.writeStringField('plain_content', '<%body%>');       
                        gen.writeStringField('editor', 'code');                 
                    } else {
                        gen.writeStringField('editor', 'design');
                        gen.writeStringField('html_content', '<%body%>');
                        gen.writeStringField('plain_content', ''+emailBody);
                    }
                gen.writeEndObject();

                req = new HttpRequest ();
                req.setEndPoint (endPoint + '/' + templateID + '/versions');
                req.setMethod ('POST');
                System.Debug (gen.getAsString ());
                req.setBody (gen.getAsString());
                req.setHeader('Authorization', 'Bearer ' + apiKey);
                req.setHeader('content-type', 'application/json');
                http = new HTTP ();
                res = new HTTPResponse ();
                if (!Test.isRunningTest ()){
                    res = http.send (req);
                    system.debug('Create Versions Response body: ' + res.getBody());
                }
                if (Test.isRunningTest ()) {
                    res.setStatusCode (201);
                    res.setBody ('{"id" : "'+emailreq.Email_template__c+'"}');
                }
                responseMap = (Map <String, Object>) JSON.deserializeUntyped (res.getBody ());
                if (responseMap.containsKey ('id')) {
                    if (emailReqId != NULL) {
                        emailRec.id = emailReqID;
                    }
                    emailRec.Template_name__c = emailReq.Template_Name__c;
                    emailRec.Subject__c = emailReq.Subject__c;
                    emailRec.Email_template__c = templateId;
                    emailRec.Template_Version_ID__c = (String) responseMap.get ('id');
                    if (emailRec.Id != NULL) {
                        update emailRec;
                    } else {
                        insert emailRec;
                    }
                    messageType = 'success';
                    Email_Template_Versions__c version = new Email_Template_Versions__c ();
                    version.Email_Request__c = emailrec.Id;
                    version.Template_Name__c = emailReq.Template_Name__c;
                    version.Subject__c = emailReq.Subject__c;
                    version.Template_Body__c = emailBody;
                    version.Template_Version_ID__c = (String) responseMap.get ('id');
                    insert version;

                    emailRec = [SELECT Name FROM Email_request__c where id =: emailRec.ID];
                    message = 'Email Template created successfully. <a href="/'
                                +emailRec.ID+'">'+emailRec.Name+'</a>';
                } else {
                    message = 'Failed to create Sendgrid Email template. '+res.getBody ();
                    messageType = 'error';
                }
            } else { 
                message = 'Failed to create Sendgrid Email template. '+res.getBody ();
                messageType = 'error';
            }
        } else {
            message = 'Failed to create Sendgrid Email template. '+res.getBody ();
            messageType = 'error';
        }
    }

    public void deleteExistingVersions(String emailReqId) {
        for (Email_Template_Versions__c version: [SELECT Email_Request__r.Email_template__c, 
                                                        Template_Version_Id__c 
                                                  FROM Email_Template_Versions__c 
                                                  WHERE Email_Request__c =: emailReqId]) {
            if (version.Template_Version_Id__c != NULL) {
                String delEndPoint = endPoint + '/' + version.Email_Request__r.Email_template__c 
                                        + '/versions/' + version.Template_Version_Id__c ;
                HTTPRequest req = new HTTPRequest();
                req.setEndpoint(delEndPoint);
                req.SetMethod('DELETE');
                req.setHeader('Authorization', 'Bearer ' + apiKey);
                req.setHeader('content-type', 'application/json');

                HTTP http = new HTTP();
                HttpResponse res = new HttpResponse();
                if(!Test.isRunningTest()) {
                    res = http.send(req);
                }
                system.debug('Delete Versions Response body: ' + res.getBody());
            }
        }
    }
}