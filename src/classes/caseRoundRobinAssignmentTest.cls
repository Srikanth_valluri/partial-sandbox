@isTest
public class caseRoundRobinAssignmentTest {
    
    @isTest
    public static void test1() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;
        Id mortgageCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Mortgage Calling List').RecordTypeId;
        Calling_List__c objCL = new Calling_List__c();
        objCL.Calling_List_Type__c = 'Mortgage- Referred by CRM';
        objCL.RecordtypeId = mortgageCallingRecordTypeId;
        insert objCL;
        
        system.debug('Test objCL =' + objCL);
        Test.startTest();
            caseRoundRobinAssignment.assignTicketsRoundRobin(new Set<Id>{objCL.Id});
        Test.stopTest();
    }
}