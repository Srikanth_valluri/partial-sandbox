public without sharing class HandoverReminderEmailBatch implements Database.Batchable<sObject>, Database.AllowsCallouts,Database.Stateful {
    
    String query;
    List<String> lstBookingIds;
    public list<SMS_History__c> lstSMSHistory ;
    public HandoverReminderEmailBatch () {}
    
    public HandoverReminderEmailBatch (List<String> plstBookingIds) {
        lstBookingIds = new List<String>();
        lstSMSHistory = new list<SMS_History__c>();
        lstBookingIds.addAll(plstBookingIds);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('==lstBookingIds=='+lstBookingIds);
        query = 'SELECT Id,Booking__r.Account__r.Email__c,Registration_ID__c, Booking__r.Account__c, Unit_Name__c, Unit_Type__c, Approval_Status__c,AWB_No__c,';
        query += 'Booking__r.Account__r.Email__pc, Booking__r.Account__r.isPersonAccount, Property_Name__c, Booking__r.Account__r.Name, Seller_Name__c, Reminder_Pack_URL__c,';
        query += 'Booking__r.Account__r.Primary_CRE__c, Booking__r.Account__r.Tertiary_CRE__c,Booking__r.Account__r.Secondary_CRE__c, Handover_Notice_URLs__c, ';
        query += 'Booking__r.Account__r.Primary_Language__c,Booking__r.Account__r.Mobile_Phone_Encrypt__pc, Booking__r.Account__r.Mobile__c, Image_of_Unit__c,';
        query += 'Registration_DateTime__c, Reminder_1_Sent__c , Reminder_1_Sent_Date__c, Reminder_2_Sent__c, Reminder_2_Sent_Date__c, Handover_Flag__c FROM Booking_Unit__c ';
        query += 'WHERE ID IN: lstBookingIds and Handover_Flag__c != \''+ String.escapeSingleQuotes('Y')+ '\'';
        return Database.getQueryLocator(query);
    }
     
    public void execute(Database.BatchableContext BC, List<Booking_Unit__c> scope) {
        map<String, String> mapemail_phoneNo = new map<String, String>();
        map<String, String> mapemail_AccID = new map<String, String>();
        Map<String, String> mapAttachmentName_Email = new Map<String, String>();
        Map<String, list<Booking_Unit__c>> mapEmail_lstBUs = new Map<String, list<Booking_Unit__c>>();
        Map<String, list<String>> mapEmail_lstAttachmentNames = new Map<String, list<String>>();
        Map<String, list<ContentVersion>> mapEmail_lstAttachments = new Map<String, list<ContentVersion>>();
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        Map<String, Boolean> mapunitReminderSent = new Map<String, Boolean>();
        Set<ID> BUIds = new Set<ID>();
        String strBccEmailAddress ;
        String unitName, Reminder1, Reminder2, EHOReminder1, EHOReminder2, projName;
        EmailTemplate Reminder1Template, Reminder2Template, EHOReminder1Template, EHOReminder2Template;
        EmailTemplate EHOSMSTemplateId = [SELECT ID, Subject, Body FROM EmailTemplate WHERE DeveloperName = 'EHO_SMS' limit 1];
        EmailTemplate NHOSMSTemplateId = [SELECT ID, Subject, Body FROM EmailTemplate WHERE DeveloperName = 'NHO_SMS' limit 1];        
        
        for (Booking_Unit__c BUObj : scope) {
            //accIds.add(BUObj.Booking__r.Account__c);
            if (String.isBlank(unitName)) {
                unitName = BUObj.Unit_Name__c.substringBefore('/');
            }
        }
        
        for (Location__c objLocation : [Select Id, 
                                               Notification_1_Email_Template__c, 
                                               Notification_2_Email_Template__c, 
                                               Name, 
                                               BCC_Email_Address__c,
                                               EHO_Notification_1_Email_Template__c,
                                               EHO_Notification_2_Email_Template__c,
                                               Project_Name_for_HO_notice__c
                                        From Location__c
                                        Where Name =: unitName]) {
             if (!String.isBlank(objLocation.Notification_1_Email_Template__c)) {
                 Reminder1 = objLocation.Notification_1_Email_Template__c;
             } 
             if (!String.isBlank(objLocation.Notification_2_Email_Template__c)) {
                 Reminder2 = objLocation.Notification_2_Email_Template__c;
             }
             if (!String.isBlank(objLocation.EHO_Notification_1_Email_Template__c)) {
                 EHOReminder1 = objLocation.EHO_Notification_1_Email_Template__c;
             } 
             if (!String.isBlank(objLocation.EHO_Notification_2_Email_Template__c)) {
                 EHOReminder2 = objLocation.EHO_Notification_2_Email_Template__c;
             }
             if( String.isNotBlank( objLocation.BCC_Email_Address__c ) ) {
                 strBccEmailAddress = objLocation.BCC_Email_Address__c;
             }
             if (!String.isBlank(objLocation.Project_Name_for_HO_notice__c)) {
                 projName = objLocation.Project_Name_for_HO_notice__c;
             }           
        }
        
        if (!String.isBlank(Reminder1)) {
            Reminder1Template = [SELECT ID, Subject, Body, HtmlValue FROM EmailTemplate WHERE DeveloperName =:Reminder1 limit 1];
        }
        if (!String.isBlank(Reminder2)) {
            Reminder2Template = [SELECT ID, Subject, Body, HtmlValue FROM EmailTemplate WHERE DeveloperName =:Reminder2 limit 1];
        }
        if (!String.isBlank(EHOReminder1)) {
            EHOReminder1Template = [SELECT ID, Subject, Body, HtmlValue FROM EmailTemplate WHERE DeveloperName =:EHOReminder1 limit 1];
        }
        if (!String.isBlank(EHOReminder2)) {
            EHOReminder2Template = [SELECT ID, Subject, Body, HtmlValue FROM EmailTemplate WHERE DeveloperName =:EHOReminder2 limit 1];
        }
        
        
        for(Booking_Unit__c BUObj : scope) {
            //Get email address
            String email = '';

            if(BUObj.Booking__r.Account__r.isPersonAccount && String.isNotBlank(BUObj.Booking__r.Account__r.Email__pc)) {
                email = BUObj.Booking__r.Account__r.Email__pc;
            }
            else if(!(BUObj.Booking__r.Account__r.isPersonAccount) && String.isNotBlank(BUObj.Booking__r.Account__r.Email__c)) {
                email = BUObj.Booking__r.Account__r.Email__c;
            }
            
            if(String.isNotBlank(email)) {
                system.debug('!!!!!!email'+email);
                mapemail_phoneNo.put(email,
                            BUObj.Booking__r.Account__r.Mobile_Phone_Encrypt__pc);
                mapemail_AccID.put(email,
                            BUObj.Booking__r.Account__c);
                system.debug('!!!!!!BUObj.Approval_Status__c'+BUObj.Approval_Status__c);
                system.debug('!!!!!!BUObj.Reminder_1_Sent__c '+BUObj.Reminder_1_Sent__c );
                system.debug('!!!!!!BUObj.Reminder_2_Sent__c '+BUObj.Reminder_2_Sent__c );
                if(BUObj.Approval_Status__c == 'EHO' || BUObj.Approval_Status__c == 'EHO_IF_PAY_MORE_RES') {
                    if (BUObj.Reminder_1_Sent__c == false || BUObj.Reminder_2_Sent__c == false) {
                        BUIds.add(BUObj.Id);
                        mapAttachmentName_Email.put(BUObj.Unit_Name__c + ' EHO Offer.zip', email + '-EHO');
                        if(mapEmail_lstBUs.containsKey(email + '-EHO')) {
                            mapEmail_lstBUs.get(email + '-EHO').add(BUObj);
                            mapEmail_lstAttachmentNames.get(email + '-EHO').add(BUObj.Unit_Name__c + ' EHO Offer.zip');
                        }
                        else {
                            mapEmail_lstBUs.put(email + '-EHO', new Booking_Unit__c[]{BUObj});
                            mapEmail_lstAttachmentNames.put(email + '-EHO', new String[]{BUObj.Unit_Name__c + ' EHO Offer.zip'});
                        }                        
                    }
                }
                else if(BUObj.Approval_Status__c == 'HO' || BUObj.Approval_Status__c == 'Approved') {
                    if (BUObj.Reminder_1_Sent__c == false || BUObj.Reminder_2_Sent__c == false) {
                        BUIds.add(BUObj.Id);
                        mapAttachmentName_Email.put(BUObj.Unit_Name__c + ' Handover Pack.zip', email + '-HO');
                        if(mapEmail_lstBUs.containsKey(email + '-HO')) {
                            mapEmail_lstBUs.get(email + '-HO').add(BUObj);
                            mapEmail_lstAttachmentNames.get(email + '-HO').add(BUObj.Unit_Name__c + ' Handover Pack.zip');
                        }
                        else {
                            mapEmail_lstBUs.put(email + '-HO', new Booking_Unit__c[]{BUObj});
                            mapEmail_lstAttachmentNames.put(email + '-HO', new String[]{BUObj.Unit_Name__c + ' Handover Pack.zip'});
                        }                        
                    }
                }//else if
            }
        }// for
        system.debug('!!!!!!!!!!mapAttachmentName_Email'+mapAttachmentName_Email);
        Set<Id> setContendDocIds = new Set<Id>();
        Map<Id, String> mapAccId_TaskSubject = new Map<Id, String>();
        map<Id, SR_Attachments__c> mapAttachmentId = new map <Id, SR_Attachments__c>();
        for (SR_Attachments__c objDoc : [Select Id, Name, Attachment_URL__c, Booking_Unit__c From SR_Attachments__c Where Booking_Unit__c IN: BUIds]) {
            if (objDoc.Name.contains('Handover')) {
                mapAttachmentId.put(objDoc.Booking_Unit__c, objDoc);
            }
        }
        Map<Id, List<Messaging.EmailFileAttachment>> mapAccId_lstEmailAttachments = new Map<Id, List<Messaging.EmailFileAttachment>>();
        if (mapAttachmentName_Email != null && mapAttachmentName_Email.keyset().size() > 0) {
            for(ContentDocumentLink CDL : [Select id, ContentDocumentid,
                                        ContentDocument.Title
                                    FROM ContentDocumentLink
                                    WHERE LinkedEntityId IN: BUIds
                                    AND ContentDocument.Title IN :mapAttachmentName_Email.keySet()]) {
    
                setContendDocIds.add(CDL.ContentDocumentid);
            }
        }
        //Get Filebody(VersionData) from Content version
        for(ContentVersion CV : [Select ID,
                                        VersionData,
                                        Title
                                FROM Contentversion
                                WHERE ContentDocumentid IN : setContendDocIds]) {
            if(mapEmail_lstAttachments.containsKey(mapAttachmentName_Email.get(CV.Title))) {
                mapEmail_lstAttachments.get(mapAttachmentName_Email.get(CV.Title)).add(CV);
            }
            else {
                mapEmail_lstAttachments.put(mapAttachmentName_Email.get(CV.Title),new ContentVersion[] {CV});
            }
        }
        
        for(String str: mapEmail_lstAttachments.keySet()) {
            List<Messaging.EmailFileAttachment> lstEmailAtta = new List<Messaging.EmailFileAttachment>();
            for(Contentversion CV : mapEmail_lstAttachments.get(str)) {
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName(CV.Title);
                efa.setBody(CV.VersionData);
                lstEmailAtta.add(efa);
            }
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            system.debug('!!!!!message'+message);
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'atyourservice@damacproperties.com'];
            if ( owea.size() > 0 ) {
                message.setOrgWideEmailAddressId(owea.get(0).Id);
            }
            String unitNames='', propertyNames='', strAWB='', strCustomerName='', strApprovalStatus = '', mailMessage= '', smsMessage='';
            String projectName = '', sellerName = '', zipURL = '', HOURLs = '', UnitURL = '';
            Boolean is1Reminder, is2Reminder;
            if(mapEmail_lstBUs.containsKey(str)) {
                for(Booking_Unit__c BU : mapEmail_lstBUs.get(str)) {
                    if(BU.Unit_Name__c != NULL) {
                        unitNames += BU.Unit_Name__c + ',';
                    }
                    if(BU.Unit_Type__c!= NULL) {
                        propertyNames += BU.Unit_Type__c+ ',';
                    }
                    if(BU.AWB_No__c!= NULL) {
                        strAWB += BU.AWB_No__c+ ',';
                    }
                    if(BU.Booking__r.Account__r.Name!= NULL) {
                        strCustomerName = BU.Booking__r.Account__r.Name;
                    }
                    if (BU.Approval_Status__c != NULL) {
                        strApprovalStatus = BU.Approval_Status__c;
                    }
                    if (BU.Property_Name__c != NULL) {
                        projectName = BU.Property_Name__c;
                    }
                    if (BU.Seller_Name__c != NULL) {
                        sellerName = BU.Seller_Name__c;
                    }
                    if (BU.Image_of_Unit__c != NULL) {
                        UnitURL = BU.Image_of_Unit__c;
                    }
                    if (mapAttachmentId != null && mapAttachmentId.containsKey(BU.Id) ) {
                        zipURL = mapAttachmentId.get(BU.Id).Attachment_URL__c;
                    }
                    if (BU.Reminder_Pack_URL__c!= null) {
                        if(string.isBlank(HOURLs)) {
                            HOURLs = 'For Unit ' + BU.Unit_Name__c + '</br>' + BU.Reminder_Pack_URL__c;
                        } else {
                            HOURLs = HOURLs + '</br> For Unit ' + BU.Unit_Name__c + '</br>' + BU.Reminder_Pack_URL__c;
                        }
                    }
                    if (BU.Reminder_1_Sent__c == false) {
                        is1Reminder = true;
                    } else if (BU.Reminder_1_Sent__c == true && BU.Reminder_2_Sent__c == false ) {
                        is2Reminder = true;
                    }
                }//for
                unitNames = unitNames.removeEnd(',');
                propertyNames = propertyNames.removeEnd(',');
                strAWB = strAWB.removeEnd(',');
            }
            system.debug('!!!!!!!!HOURLs'+HOURLs);
            if (strApprovalStatus == 'EHO' || strApprovalStatus == 'EHO_IF_PAY_MORE_RES') {
                if (is1Reminder == true) {
                    if (EHOReminder1Template!= null) {
                        mailMessage = EHOReminder1Template.HtmlValue;
                        smsMessage = EHOReminder1Template.Body;
                    }
                } else if (is2Reminder == true){
                    if (EHOReminder2Template!= null) {
                        mailMessage = EHOReminder2Template.HtmlValue;
                        smsMessage = EHOReminder2Template.Body;
                    }
                }
                //system.debug('!!!!!!!!HOURLs'+HOURLs);
                if (!string.isBlank(mailMessage)) {
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.Unit_Type__c}', propertyNames);
                    if (!string.isBlank(projName)) {
                        mailMessage = mailMessage.replace('{!Booking_Unit__c.Property_Name__c}', projName );
                    } else {
                        mailMessage = mailMessage.replace('{!Booking_Unit__c.Property_Name__c}', '');
                    }
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.Unit_Name__c}', unitNames);
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.AWB_No__c}', strAWB );
                    mailMessage = mailMessage.replace('{!Account.Name}', strCustomerName );
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.Reminder_Pack_URL__c}', HOURLs);
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.Image_of_Unit__c}', UnitURL );
                    mailMessage = mailMessage.replace(']]>', '');
                    message.subject = Reminder1Template.Subject.replace('{!Booking_Unit__c.Unit_Name__c}', unitNames);
                    if (unitNames.contains(',') ){
                        List <String> lstNames = unitNames.split(',');                        
                        if (lstNames != null && lstNames.size() > 0) {
                            for (String unit : lstNames) {
                                mapunitReminderSent.put(unit, true);
                            }
                        } 
                    } else {
                        mapunitReminderSent.put(unitNames, true);
                    }                                        
               }
               if (!String.isBlank(smsMessage)) {
                    smsMessage = smsMessage.replace('{!Account.Name}', strCustomerName );
                    smsMessage = smsMessage.replace('{!Booking_Unit__c.Unit_Type__c}', propertyNames );
                    smsMessage = smsMessage.replace('{!Booking_Unit__c.Unit_Name__c}', unitNames);
                    smsMessage = smsMessage.replace('!Booking_Unit__c.AWB_No__c}', strAWB);
               }
                    
                    //message.setHtmlBody(EHOTemplateId.Body.replace('{!Booking_Unit__c.Property_Name__c}', propertyNames));                
               // }
            } else if (strApprovalStatus == 'HO' || strApprovalStatus == 'Approved') {
                if (is1Reminder == true) {
                    if (Reminder1Template != null) {
                        mailMessage = Reminder1Template.HtmlValue;
                        smsMessage = Reminder1Template.Body;
                    }
                } else if (is2Reminder == true){
                    if (Reminder2Template != null) {
                        mailMessage = Reminder2Template.HtmlValue;
                        smsMessage = Reminder2Template.Body;
                    }
                }
                if (!string.isBlank(mailMessage)) {
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.Unit_Type__c}', propertyNames);
                    if (!string.isBlank(projName)) {
                        mailMessage = mailMessage.replace('{!Booking_Unit__c.Property_Name__c}', projName );
                    } else {
                        mailMessage = mailMessage.replace('{!Booking_Unit__c.Property_Name__c}', '');
                    }
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.Unit_Name__c}', unitNames);
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.AWB_No__c}', strAWB );
                    mailMessage = mailMessage.replace('{!Account.Name}', strCustomerName );
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.Reminder_Pack_URL__c}', HOURLs);
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.Image_of_Unit__c}', UnitURL );
                    mailMessage = mailMessage.replace(']]>', '');
                    message.subject = Reminder1Template.Subject.replace('{!Booking_Unit__c.Unit_Name__c}', unitNames);
                    if (unitNames.contains(',') ){
                        List <String> lstNames = unitNames.split(',');                        
                        if (lstNames != null && lstNames.size() > 0) {
                            for (String unit : lstNames) {
                                mapunitReminderSent.put(unit, true);
                            }
                        } 
                    } else {
                        mapunitReminderSent.put(unitNames, true);
                    }  
               }
               if (!string.isBlank(smsMessage)) {                    
                    smsMessage = smsMessage.replace('{!Account.Name}', strCustomerName );
                    smsMessage = smsMessage.replace('{!Booking_Unit__c.Unit_Type__c}', propertyNames );
                    smsMessage = smsMessage.replace('{!Booking_Unit__c.Unit_Name__c}', unitNames);
                    smsMessage = smsMessage.replace('!Booking_Unit__c.AWB_No__c}', strAWB);
               }
            }
            /*else if (is2Reminder == true){
                if (Reminder2Template != null) {          
                    mailMessage = Reminder2Template.HtmlValue;
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.Property_Name__c}', projectName);
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.Seller_Name__c}', sellerName);
                    mailMessage = mailMessage.replace('{!Account.Name}', strCustomerName );
                    mailMessage = mailMessage.replace( 'http://www.google.com', zipURL );
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.Unit_Type__c}', propertyNames);
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.Unit_Name__c}', unitNames);
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.Handover_Notice_URLs__c}', HOURLs);
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.Image_of_Unit__c}', UnitURL );
                    mailMessage = mailMessage.replace(']]>', '');
                    message.subject = Reminder2Template.Subject.replace('{!Booking_Unit__c.Unit_Name__c}', unitNames);
                    
                    smsMessage = Reminder2Template.Body;
                    smsMessage = smsMessage.replace('{!Account.Name}', strCustomerName );
                    smsMessage = smsMessage.replace('{!Booking_Unit__c.Unit_Type__c}', propertyNames );
                    smsMessage = smsMessage.replace('{!Booking_Unit__c.Unit_Name__c}', unitNames);
                    smsMessage = smsMessage.replace('!Booking_Unit__c.AWB_No__c}', strAWB);
                }                
            }*/
            
            message.setHtmlBody(mailMessage);
            message.setSaveAsActivity(true);
            //message.setFileAttachments(lstEmailAtta);
            String accountId = '',email = '';
            if(str.contains('-HO')) {
                accountId = mapemail_AccID.get(str.substringBefore('-HO'));
                email = str.substringBefore('-HO');
            }
            else if(str.contains('-EHO')) {
                accountId = mapemail_AccID.get(str.substringBefore('-EHO'));
                email = str.substringBefore('-EHO');
            }
            message.setWhatId(accountId);
            message.toAddresses = new String[] {email};
            if( String.isNotBlank( strBccEmailAddress ) ) {
              message.bccAddresses = new list<String>{ strBccEmailAddress };
            }
            lstSMSHistory.add(new SMS_History__c(Customer__c = accountId, Is_SMS_Sent__c = false,
                Phone_Number__c = mapemail_phoneNo.get(email), Message__c = smsMessage));
            if (!string.isBlank(mailMessage)) {
                messages.add(message);
            }
            mapAccId_TaskSubject.put(accountId, 'Email: ' + message.subject);
            mapAccId_lstEmailAttachments.put(accountId, lstEmailAtta);            
        }// end of for
        
        for (Booking_Unit__c BUObj : scope) {
            //Updating fields on booking unit
            if (mapunitReminderSent != null && mapunitReminderSent.containsKey(BUObj.Unit_Name__c) 
                && mapunitReminderSent.get(BUObj.Unit_Name__c) == true ) {
                if (BUObj.Approval_Status__c == 'HO' || BUObj.Approval_Status__c == 'Approved' 
                    || BUObj.Approval_Status__c == 'EHO' || BUObj.Approval_Status__c == 'EHO_IF_PAY_MORE_RES') {
                    if (BUObj.Reminder_1_Sent__c == false) {
                        BUObj.Reminder_1_Sent_Date__c = Date.today();
                        BUObj.Reminder_1_Sent__c = true;
                    } else if (BUObj.Reminder_1_Sent__c == true && BUObj.Reminder_2_Sent__c == false) {
                        BUObj.Reminder_2_Sent_Date__c = Date.today();
                        BUObj.Reminder_2_Sent__c = true;
                    }
                }
            }
        }
        system.debug('!!!!!messages'+messages);
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);//Sending emails
        system.debug('!!!!!results'+results);
         //Get tasks created after emails sent
        Set<String> taskIds = new Set<String>();
        Map<ID, List<String>> mapTaskId_lstTitle = new Map<ID, List<String>>();
        List<ContentVersion> lstContentVersion = new List<ContentVersion>();
        for(Task taskobj : [SELECT Id, WhatId,Subject
                            FROM Task
                            WHERE WhatId IN : mapAccId_TaskSubject.keySet()
                            AND Subject IN : mapAccId_TaskSubject.values()
                            ORDER BY LastModifiedDate DESC]) {
            if(!taskIds.contains(taskobj.Subject)) {//To ensure we get the most recent task
                for(Messaging.EmailFileAttachment efa : mapAccId_lstEmailAttachments.get(taskobj.WhatId)) {
                    ContentVersion cv = new ContentVersion();
                    cv.ContentLocation = 'S';
                    cv.PathOnClient = efa.filename;
                    // cv.Origin = 'H';
                    // cv.OwnerId = UserInfo.getUserId();
                    cv.Title = efa.filename;
                    cv.VersionData = efa.Body;
                    lstContentVersion.add(cv);
                    if(mapTaskId_lstTitle.containsKey(taskobj.Id)) {
                        mapTaskId_lstTitle.get(taskobj.Id).add(efa.filename);
                    }
                    else {
                        mapTaskId_lstTitle.put(taskobj.Id, new String[] {efa.filename});
                    }
                }
            }//if
            taskIds.add(taskobj.Subject);//To ensure we get the most recent task
        }
        insert lstContentVersion;
        System.debug('==lstContentVersion==' + lstContentVersion.SIZE());
        System.debug('==taskIds==' + taskIds);
        Map<String, Id> mapTitle_CDId = new Map<String, Id>();
        for(ContentVersion cv : [SELECT ID, Title, ContentDocumentId
                                FROM ContentVersion
                                WHERE ID IN : lstContentVersion]) {
            mapTitle_CDId.put(cv.Title, cv.ContentDocumentId);
            System.debug('==ContentDocumentId==' + cv.ContentDocumentId);
        }
        update scope;
        //Inserting ContentDocumentLink
        List<ContentDocumentLink> lstContentDocumentLink = new List<ContentDocumentLink>();
        for(Id taskId : mapTaskId_lstTitle.keySet()) {
            for(String title : mapTaskId_lstTitle.get(taskId)) {
                System.debug('==title==' + title);
                lstContentDocumentLink.add(new ContentDocumentLink(LinkedEntityId = taskId, ContentDocumentId = mapTitle_CDId.get(title), ShareType = 'I'));
                // break;
            }
        }
        insert lstContentDocumentLink;
    }
    
    public void finish(Database.BatchableContext BC) {
        insert lstSMSHistory;
        System.debug('===lstSMSHistory===' + lstSMSHistory);
        Database.executeBatch(new CustomerNotificationSendSMSBatch(lstSMSHistory),90);
    }
}