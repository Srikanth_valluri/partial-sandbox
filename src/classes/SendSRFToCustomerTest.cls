@isTest
public without sharing class SendSRFToCustomerTest {

    private class Mock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse res = new HTTPResponse();
            System.debug('Inside Mock class');
            if(req.getEndpoint().startsWith('https://damac.onsiteapp.co')){
                System.debug('Inside onsiteapp');
                res.setBody('Test PDF');
                res.setStatusCode(200);
                res.setHeader('Content-Type', 'application/pdf');
            }else if(req.getEndpoint() == 'https://api.signnow.com/oauth2/token'){
                res.setBody('{ "access_token": "6cd862b817ca919e78d165c4615d1e1031cacd8bde1e6701c748e0abc14708f0" }');
                res.setStatusCode(200);    
            }else if(req.getEndpoint() == 'https://api.signnow.com/document/fieldextract'){
                res.setBody('{ "id": "b5f53127b7174b8e84661785edf8ec7d54013119"}');
                res.setStatusCode(200);
            }else if(req.getEndpoint().startsWith('https://api.signnow.com/document/') &&
                     req.getEndpoint().endsWith('/invite')){
                res.setBody('{ "status": "success"}');
                res.setStatusCode(200);
            }else if(req.getEndpoint().startsWith('https://api.signnow.com/document/')){
                res.setBody('{"id":"938efe81e8fd422eab00c72d6a6c1839949cdf45","signatures":[],"texts":[],"checks":[],"views":[],"attachments":[],"radiobuttons":[],"hyperlinks":[]}');
                 res.setStatusCode(200);
            }else if(req.getEndpoint() == 'https://api.signnow.com/api/v2/events'){
                res.setBody('{ "status": "success" }');
                res.setStatusCode(200);
            }else if(req.getEndpoint().endsWith('token')) {
                res.setBody('{'+
                            '"token_type": "Bearer",'+  
                            '"expires_in": "3600",'+
                            '"ext_expires_in": "3600",'+
                            '"expires_on": "1570628201",'+
                            '"not_before": "1570624301",'+
                            '"resource": "https://graph.microsoft.com",'+
                            '"access_token": "eyJ0eXAiOiJKV1QiLCJub25jZSI6IkN3MVpSbEFnNTZKaUt4Mkd2Tl"'+
                            '}');
                res.setStatusCode(200);
             }else if(req.getEndpoint().endsWith('content')) {
                 res.setBody('{'+
                             '"@microsoft.graph.downloadUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/download",'+
                             '"id": "123456789A",'+
                             '"webUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/Documents/test.pdf"'+
                             '}');
                 res.setStatusCode(200);
             }
            return res;
        }
    }

    @isTest
    static void fetchSRFDocfromOnsiteAPI(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        
        //Create Customer record
        Account objAcc = new Account(FirstName='Test FirstName'
                                    , LastName='Test LastName'
                                    , Email__pc = 'test123@tmail.com'
                                    , recordTypeId = personAccRTId);
        insert objAcc;
        System.debug('objAcc:::' +objAcc);
        //Create Property
        //Property__c  objProp = TestDataFactory_CRM.createProperty();
        Property__c objProp = new Property__c();
        objProp.Name = 'Damac Hills';
        objProp.DIFC__c = false;
        objProp.Property_ID__c = System.currentTimeMillis();
        objProp.CurrencyISoCode = 'AED';
        objProp.SnagValue__c = 'vardon';
        insert objProp;
        
        //Create Location
        Location__c objLocation = createLocation();
        insert objLocation;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        objInventory.Building_Location__c = objLocation.id;
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        
        insert listBookings;
        System.debug('listBookings:::' +listBookings);
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].SNAGs_Completed__c = false;
        listBUs[0].Unit_Name__c = 'VRD/SD649/XR1149B';
        listBUs[0].Registration_Status_Code__c = 'ABC';
        //listBUs[0].SnagValue__c = 'asd';
        insert listBUs;
        System.debug('listBUs:::' +listBUs);
        System.debug('Booking__c:::' +listBUs[0].Booking__c);
        System.debug('Unit_Name__c:::' +listBUs[0].Unit_Name__c);
        System.debug('Registration_Status_Code__c:::' +listBUs[0].Registration_Status_Code__c);
        System.debug('Unit Active :::' +listBUs[0].Unit_Active__c);
        
        TriggerOnOffCustomSetting__c objCustomSetting = new TriggerOnOffCustomSetting__c();
        objCustomSetting.Name = 'CallingListTrigger';
        objCustomSetting.OnOffCheck__c = false;
        insert objCustomSetting;

        Id callingListRecTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();

        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Account__c = objAcc.Id;
        objCallingList.Appointment_Start_DateTime__c = System.now();
        objCallingList.Appointment_End_DateTime__c = System.now()+1;
        objCallingList.Booking_Unit__c = listBUs[0].Id;
        objCallingList.Virtual_Viewing__c = true;
        objCallingList.RecordTypeId = callingListRecTypeId;
        insert objCallingList;
        
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        
        TriggerOnOffCustomSetting__c triggerSetting = new TriggerOnOffCustomSetting__c(Name= 'CaseTrigger',
                                                                                     OnOffCheck__c = false);
        insert triggerSetting;                                                                             
                                                                                             
        //Create Case
        Case  objCase = TestDataFactory_CRM.createCase(objAcc.Id, caseRecTypeId);
        objCase.Booking_Unit__c = listBUs[0].Id;
        objCase.SNAGs_Completed__c = true;
        insert objCase;
        
        SNAGs__c objSnag = new SNAGs__c();
        objSnag.Booking_Unit__c = listBUs[0].Id ;
        insert objSnag;
        
        SNAG_URLs__c objSNAGURL = new SNAG_URLs__c();
        objSNAGURL.Name = 'dd';// listBUs[0].Unit_Name__c.substringBefore('/');
        objSNAGURL.URL__c = 'avgttn';
        insert objSNAGURL;
        
        
        System.assert(objCase != null);
    
        set<Id> CaseId = new set<Id>();
        CaseId.add(objCase.Id);
        
        List<Booking_Unit__c> SngBUList= [Select Id, Booking__r.Account__c,Booking__r.Account__r.Mobile_Phone_Encrypt__pc
                                  , Booking__r.Account__r.Name
                                  , Booking__r.Account__r.Email__pc,Unit_Name__c,SRF_Date__c,Revised_SRF_Date__c,
        Registration_Id__c,Onsite_Handover_Date__c,Snags_Reported__c,Snags_Completed__c,Snag_Status_Onsite__c,
                                          Viewing_Status_onsite__c,Total_Snags_Open__c,FirstViewingDate__c,SecondViewingDate__c,
                                          ThirdViewingDate__c,Snag_Completion_Target_Date__c,Snag_Completion_Revised_Target_Date__c,
                                          Total_Snags_Closed__c,Total_Snags_Fixed__c, Calling_List__r.Virtual_Viewing__c, SnagValue__c
                                          from Booking_Unit__c where Unit_Active__c='Active' 
                                            AND Unit_Name__c=:listBUs[0].Unit_Name__c limit 1];
        System.debug('SngBUList::' +SngBUList);
        List<Booking_Unit__c> buList = [SELECT Unit_Active__c, Booking__r.Account__r.Email__pc 
                                        FROM Booking_Unit__c
                                        WHERE Id =: listBUs[0].Id ];
        System.debug('buList::' +buList[0].Booking__r.Account__r.Email__pc);
        Test.setMock(HttpCalloutMock.class, new Mock());
        Test.startTest();       
        SendSRFToCustomer.fetchSRFDocfromOnsiteAPI(SngBUList, 'Open');
        Test.stopTest(); 
    }
    
     @isTest
    static void invokeSnagInfo(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        
        //Create Customer record
        Account objAcc = new Account(FirstName='Test FirstName'
                                    , LastName='Test LastName'
                                    , Email__pc = 'test123@tmail.com'
                                    , recordTypeId = personAccRTId);
        insert objAcc;
        System.debug('objAcc:::' +objAcc);
        //Create Property
        //Property__c  objProp = TestDataFactory_CRM.createProperty();
        Property__c objProp = new Property__c();
        objProp.Name = 'Damac Hills';
        objProp.DIFC__c = false;
        objProp.Property_ID__c = System.currentTimeMillis();
        objProp.CurrencyISoCode = 'AED';
        objProp.SnagValue__c = 'vardon';
        insert objProp;
        
        //Create Location
        Location__c objLocation = createLocation();
        insert objLocation;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        objInventory.Building_Location__c = objLocation.id;
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        
        insert listBookings;
        System.debug('listBookings:::' +listBookings);
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].SNAGs_Completed__c = false;
        listBUs[0].Unit_Name__c = 'VRD/SD649/XR1149B';
        listBUs[0].Registration_Status_Code__c = 'ABC';
        //listBUs[0].SnagValue__c = 'asd';
        insert listBUs;
    System.debug('listBUs:::' +listBUs);
        System.debug('Booking__c:::' +listBUs[0].Booking__c);
        System.debug('Unit_Name__c:::' +listBUs[0].Unit_Name__c);
        System.debug('Registration_Status_Code__c:::' +listBUs[0].Registration_Status_Code__c);
        System.debug('Unit Active :::' +listBUs[0].Unit_Active__c);
        
        TriggerOnOffCustomSetting__c objCustomSetting = new TriggerOnOffCustomSetting__c();
        objCustomSetting.Name = 'CallingListTrigger';
        objCustomSetting.OnOffCheck__c = false;
        insert objCustomSetting;

        Id callingListRecTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();

        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Account__c = objAcc.Id;
        objCallingList.Appointment_Start_DateTime__c = System.now();
        objCallingList.Appointment_End_DateTime__c = System.now()+1;
        objCallingList.Booking_Unit__c = listBUs[0].Id;
        objCallingList.Virtual_Viewing__c = true;
        objCallingList.RecordTypeId = callingListRecTypeId;
        insert objCallingList;
        
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        
        TriggerOnOffCustomSetting__c triggerSetting = new TriggerOnOffCustomSetting__c(Name= 'CaseTrigger',
                                                                                     OnOffCheck__c = false);
        insert triggerSetting;                                                                             
                                                                                             
        //Create Case
        Case  objCase = TestDataFactory_CRM.createCase(objAcc.Id, caseRecTypeId);
        objCase.Booking_Unit__c = listBUs[0].Id;
        objCase.SNAGs_Completed__c = true;
        insert objCase;
        
        SNAGs__c objSnag = new SNAGs__c();
        objSnag.Booking_Unit__c = listBUs[0].Id ;
        insert objSnag;
        
        SNAG_URLs__c objSNAGURL = new SNAG_URLs__c();
        objSNAGURL.Name = 'dd';// listBUs[0].Unit_Name__c.substringBefore('/');
        objSNAGURL.URL__c = 'avgttn';
        insert objSNAGURL;
        
        
        System.assert(objCase != null);
    
        set<Id> CaseId = new set<Id>();
        CaseId.add(objCase.Id);
        
        List<Booking_Unit__c> SngBUList= [Select Id, Booking__r.Account__c,Booking__r.Account__r.Mobile_Phone_Encrypt__pc
                                  , Booking__r.Account__r.Name
                                  , Booking__r.Account__r.Email__pc,Unit_Name__c,SRF_Date__c,Revised_SRF_Date__c,
        Registration_Id__c,Onsite_Handover_Date__c,Snags_Reported__c,Snags_Completed__c,Snag_Status_Onsite__c,
                                          Viewing_Status_onsite__c,Total_Snags_Open__c,FirstViewingDate__c,SecondViewingDate__c,
                                          ThirdViewingDate__c,Snag_Completion_Target_Date__c,Snag_Completion_Revised_Target_Date__c,
                                          Total_Snags_Closed__c,Total_Snags_Fixed__c, Calling_List__r.Virtual_Viewing__c, SnagValue__c
                                          from Booking_Unit__c where Unit_Active__c='Active' 
                                            AND Unit_Name__c=:listBUs[0].Unit_Name__c limit 1];
        System.debug('SngBUList::' +SngBUList);
        List<Id> bookingUnitIds = new List<Id>();
        bookingUnitIds.add(SngBUList[0].Id);
        List<Booking_Unit__c> buList = [SELECT Unit_Active__c, Booking__r.Account__r.Email__pc 
                                        FROM Booking_Unit__c
                                        WHERE Id =: listBUs[0].Id ];
        System.debug('buList::' +buList[0].Booking__r.Account__r.Email__pc);
        Test.setMock(HttpCalloutMock.class, new Mock());
        Test.startTest();       
        SendSRFToCustomer.invokeSnagInfo(bookingUnitIds);
        Test.stopTest(); 
    }

    static Location__c createLocation() {
        return new Location__c(Name = '', Location_ID__c = 'uu', Is_New_Snagger__c=true);
    }
}