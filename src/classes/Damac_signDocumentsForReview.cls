public class Damac_signDocumentsForReview implements Queueable, Database.allowsCallouts {

    Set <Id> srId = new Set<ID> ();
    public Damac_signDocumentsForReview (Set <ID> srRecIds) {
        srId = srRecIds;
    }
    public void execute(QueueableContext context) {
        Signnow_Credentials__c credentials = Signnow_Credentials__c.getInstance ();
        String accessToken = Damac_SignNowAPI.authenticate(credentials);
       
        for (ESign_Details__c uDocs :[SELECT Unit_Documents__c , Unit_Documents__r.Document_name__c, Unit_Documents__r.Service_Request__c,
                                      Unit_Documents__r.CV_Id__c FROM ESign_Details__c 
                                        WHERE Unit_Documents__r.Service_Request__c IN: srId AND Status__c = 'Signed'
                                        AND Signer_Type__c = NULL
                                        AND Unit_Documents__r.CV_Id__c != null])
        {
            agentReviewDocuments (accessToken, uDocs.Unit_Documents__c, uDocs.Unit_Documents__r.CV_Id__c, 
                                  uDocs.Unit_Documents__r.Document_name__c, uDocs.Unit_Documents__r.Service_Request__c);
            
        }
        
    }
    @Future (Callout = true)
    public static void agentReviewDocuments (String accessToken, ID uDocId, ID contentDocId, String documentName, Id srId) {
        Signnow_Credentials__c credentials = Signnow_Credentials__c.getInstance ();
        
        String docuName = documentName.split (' ')[0]+'%';
        String docName = documentName.split (' ')[0];
        List<Agent_Sign_Positions__mdt> agentSignPositions = null;  
        
        String signer2Email = credentials.Second_Signer_Agent_Registration__c;
        for (Country_based_signing_Authorities__c authorities : [SELECT Signer_1__c, Signer_2__c FROM Country_based_signing_Authorities__c
                                                                    WHERE Name =: docName])
        {
            signer2Email = authorities.Signer_1__c;
        }
        signer2Email = 'srikanth.v@bigworks.co';
        Unit_Documents__c doc = new Unit_Documents__c ();
        
        try{
            doc = [SELECT Document_Name__c, Service_Request__c, Service_Request__r.name FROM Unit_Documents__c WHERE Service_Request__c =: srId 
                                       AND Status__c = 'Generated' AND Document_Name__c LIKE :docuName LIMIT 1];
            
                                      
            agentSignPositions = [SELECT MasterLabel, Document_Name__c, Page_Number__c,Signer__c,
                                              Type__c, X_Position__c, Y_Position__c, Width__c, Height__c,Required__c
                                   FROM Agent_Sign_Positions__mdt
                                   WHERE Document_Name__c =: doc.Document_Name__c  
                                   AND Signer__c = '2' ORDER BY Page_Number__c];
        }Catch(Exception e) {}
                                                       
        List <ESign_Details__c> eSignRecords = new List <ESign_Details__c> ();
        
        for (ContentVersion version : [SELECT VersionData, Title FROM ContentVersion WHERE Id =: contentDocId])
        {
            String documentId = Damac_SignNowAPI.uploadDocument(credentials, version, accessToken, null, agentSignPositions);
                
            List <String> documentIds = new List <String> ();
            documentIds.add (documentId);
            
            Map <String, HttpResponse> res = new Map <String, HttpResponse> ();
            res = Damac_SignNowAPI.sendInvitieToSign (credentials, documentIds, accessToken, signer2Email , '', '', doc.Service_Request__r.Name+'-'+version.Title);

            System.debug (res.get (documentId).getBody());
            
            if (res.get(documentId).getStatusCode () == 200) {
                ESign_Details__c sDoc = new ESign_Details__c ();
                sDoc.Document_Id__c = documentId;
                sDoc.Unit_Documents__c = uDocId;
                sDoc.Status__c = 'Email Sent';
                sDoc.Signer_Type__c = 'Signer 2';
                sDoc.To_email__c = signer2Email;
                eSignRecords.add (sDoc);
            
            }
        }
        insert eSignRecords;
    
    }
}