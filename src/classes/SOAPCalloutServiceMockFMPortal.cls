@isTest
global with sharing class SOAPCalloutServiceMockFMPortal implements WebServiceMock {
    
     global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
           
        if ( request instanceof GenerateSOAService.GenCustomerStatement_element ) {
          GenerateSOAService.GenCustomerStatementResponse_element responseNew = new GenerateSOAService.GenCustomerStatementResponse_element();
          responseNew.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338397 and Request Id :40187735 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"92061","REQUEST_ID":"40187735","STAGE_ID":"338397","URL":"https://sftest.deeprootsurface.com/docs/e/40187735_92061_SOA.pdf"}';
          response.put('response_x', responseNew);
        }
        else if( request instanceof GenerateReceipt.GenerateReceipt_element ) {
          GenerateReceipt.GenerateReceiptResponse_element responseNew = new GenerateReceipt.GenerateReceiptResponse_element();
          responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=340219 and Request Id :40238955 ...","ATTRIBUTE3":"340219","ATTRIBUTE2":"40238955","ATTRIBUTE1":"https://sftest.deeprootsurface.com/docs/e/40238955_274522_RECEIPT.pdf","PARAM_ID":"2-274522","Message_ID":"2-274522"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
          response.put('response_x', responseNew);
        }
           
    }
    
}