/***************************************************************************************************
 * @Description : Test class for EmailToCaseTriggerHandler                                         *
 *                                                                                                 *
 *                                                                                                 *
 *  Version     Author           Date         Description                                          *
 *  1.0                          17/12/2018   Initial Development                                  *
***************************************************************************************************/
@isTest
public class EmailToCaseTriggerHandlerTest {
    @isTest
    static void testpopulateAccountOnCase1() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        objAcc.Email__c = 'test@test.com';
        insert objAcc;
        
        list<Id> lstCaseId = new list<Id>();
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.Subject = 'Test email';
        objCase.SuppliedEmail = 'test@test.com';
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        Test.startTest();
        insert objCase;        
        Test.stopTest(); 
        lstCaseId.add(objCase.Id);
        EmailToCaseTriggerHandler.populateAccountOnCase(lstCaseId);       
        System.debug('test case = ' + [Select AccountId from case]);
        System.assertEquals(objAcc.Id, [Select AccountId from case][0].AccountId);
    }

    @isTest
    public static void testpopulateAccountOnCase2() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        objAcc.Email__pc = 'test@test.com';
        insert objAcc;
        
        list<Id> lstCaseId = new list<Id>();
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.Subject = 'Test email';
        objCase.SuppliedEmail = 'test@test.com';
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        Test.startTest();
        insert objCase;
        Test.stopTest();
        
        lstCaseId.add(objCase.Id);
        EmailToCaseTriggerHandler.populateAccountOnCase(lstCaseId); 
        System.debug('test case = ' + [Select AccountId from case]);
        System.assertEquals(objAcc.Id, [Select AccountId from case][0].AccountId);
    }
}