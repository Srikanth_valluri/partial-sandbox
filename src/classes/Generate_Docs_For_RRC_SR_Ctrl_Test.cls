@isTest
public class Generate_Docs_For_RRC_SR_Ctrl_Test {

    @isTest
    public static void test1() {


        Account objAcc = new Account(Name = 'test');
        insert objAcc;
                
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'DEA/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        //objBookingUnit.Riyadh_Building__c = 'Fendi';
        objBookingUnit.Signed_SPA_AGS__c = 'SPA';
        insert objBookingUnit;

        Case caseObj = new Case(AccountId = objAcc.id,
                                Booking_Unit__c = objBookingUnit.Id,
                                Owner_Interested_In_Hotel__c= 'Interested' );
        insert caseObj;

        insert new Riyadh_Rotana_Document_Criteria_Mapping__c( 
                    Name = 'I-F-S'
                    , Required_Document_Names__c = 'RRC1,RCC2');

        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(
                    Name = 'RRC1'
                    , Drawloop_Document_Package_Id__c = 'test'
                    , Delivery_Option_Id__c = 'test');
                    
        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(
            Name = 'RRC2'
                    , Drawloop_Document_Package_Id__c = 'test'
                    , Delivery_Option_Id__c = 'test');

        ApexPages.StandardController sc = new ApexPages.StandardController(caseObj);
        Generate_Docs_For_RRC_SR_Ctrl ctrl = new Generate_Docs_For_RRC_SR_Ctrl(sc);
        
        PageReference pageRef = Page.Generate_Docs_For_RRC_SR;
        pageRef.getParameters().put('id', String.valueOf(caseObj.Id));
        ctrl.caseId = caseObj.Id;
        ctrl.generateDrawloopDocuments();
        Test.setCurrentPage(pageRef);
    }
}