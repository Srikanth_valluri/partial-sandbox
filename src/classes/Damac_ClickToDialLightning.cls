global without sharing class Damac_ClickToDialLightning {
    
    @AuraEnabled
    public static String encryptMobilePhoneAura(String mobileNumber){
        system.debug('Mobile Number: ' + mobileNumber);
        return Damac_ClickToDial.getMobileEncrypted(mobileNumber);
    }
    
    @AuraEnabled
    public static Inquiry__c fetchInquiryDetails(String inquiryId){
        List<Inquiry__c> inqList = new List<Inquiry__c>();
        if(inquiryId != null) {   
            inqList = [SELECT Id, Name, Mobile_Phone_Encrypt_2__c, Mobile_Phone_Encrypt__c
                       FROM Inquiry__c WHERE Id = : inquiryId LIMIT 1];
            if(! inqList.isEmpty()){
                return inqList.get(0);
            }
        }
        return null;
    }
    
    @AuraEnabled 
    public static void makeCallWaybeo(String ext, String customer){
        
        String url = 'https://3cxtelephone.damacgroup.com:19200/Waybeo/OutboundCall';
        String jsonBody = '{"agent": ' + ext + ' , "customer": ' + customer + '}';
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        req.setBody(jsonBody);
        system.debug('jsonBody: ' + jsonBody);
        system.debug('Request: ' + req);
        if(!Test.isRunningTest()){
            HttpResponse res = h.send(req);
            System.debug('response:--> ' + res.getBody());
        }
    }
    
    @AuraEnabled 
    public static String fetchUserExtension(){
      User currentUser = [SELECT Id, Extension FROM User WHERE Id =: userInfo.getUserId() LIMIT 1];
      system.debug('currentUser: ' + currentUser);
        return currentUser.Extension;
    }
    
    @AuraEnabled
    public static void updateInquiryWithMobileEncrypted(String encryptNumber, String inquiryId){
        Inquiry__c inq = new Inquiry__c();
       // String encryptNumber = '4daacdaf1227c9d1218ca636cda363c0';
        system.debug('Mobile Number Encrypt: ' + encryptNumber);
        system.debug('inquiryId: ' + inquiryId);
        inq.Id = inquiryId;
        inq.Encrypted_Mobile__c = encryptNumber;
        try{
        update inq;
        } catch (exception e){
            system.debug('Exception while updating Inquiry: ' + e);
        }
    }
    
}