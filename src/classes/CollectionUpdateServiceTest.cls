/***********************************************************************
* Description - Test class developed for CollectionUpdateService
*
* Version            Date            Author        Description
* 1.0                26/11/17        Snehil		   Initial Draft
**********************************************************************/
@isTest
private class CollectionUpdateServiceTest
{
	@isTest static void getNUpdateCollectionCallingListMonthly() {

		Test.startTest();
		
		CollectionUpdateService.CallingListHttpSoap11Endpoint calloutObj = new CollectionUpdateService.CallingListHttpSoap11Endpoint();
		
		Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockCallingList());
		
		String response = calloutObj.getNUpdateCollectionCallingListMonthly();

		Test.stopTest();
	}


	@isTest static void getNUpdateCollectionCallingListHourlySF() {

		Test.startTest();
		
		CollectionUpdateService.CallingListHttpSoap11Endpoint calloutObj = new CollectionUpdateService.CallingListHttpSoap11Endpoint();
		
		Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockCallingList());
		
		String response = calloutObj.getNUpdateCollectionCallingListHourlySF();

		Test.stopTest();
	}

	@isTest static void getCollectionCallingListIPMS() {

		Test.startTest();
		
		CollectionUpdateService.CallingListHttpSoap11Endpoint calloutObj = new CollectionUpdateService.CallingListHttpSoap11Endpoint();
		
		Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockCallingList());
		
		String response = calloutObj.getCollectionCallingListIPMS( '234687435186','123135', '', '', '', '', '', '', '' );

		Test.stopTest();
	}
}