/***************************************************************************************************************
Description: Mock class for RoasteringSerivce.
________________________________________________________________________________________________________________
Version Date(DD-MM-YYYY)    Author              Description
----------------------------------------------------------------------------------------------------------------
1.0     16-07-2020          Komal Shitole       Initial Draft
****************************************************************************************************************/
@isTest
global class RoasteringServiceMockHttpNegative implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req){
        String temp = '';
        HttpResponse res = new HttpResponse();
        System.debug(req.getEndpoint());
        res.setStatusCode( 200 );
        
        if(req.getEndpoint().containsIgnoreCase('attendance-daily')) {
            temp = 'failed: 0010101004 : Invalid command : date-range=1707020-30082020';
        }
        else if(req.getEndpoint().containsIgnoreCase('holiday')) {
            
            temp = 'failed: 0350102003 : Insufficient User rights or Inactive User(s) or ID(s) does not exist.';
        }
        else {
            temp = 'failed: 0440101004 : Invalid command : Shift ID is blank or invalid.';
        }
        res.setBody( temp );
        return res;
        
    }
    
}