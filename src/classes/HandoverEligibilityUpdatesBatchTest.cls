@isTest 
public with sharing class HandoverEligibilityUpdatesBatchTest {
    
    static testMethod void testMethodOne() {
    	Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
       
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;
        
        Task objTask = new Task();
        objTask.Subject = 'Handover Eligibility status of Unit ALR/13/1310 has changed. Please check and send Handover/EHO notice as required.';
        objTask.WhatId = buIns.Id ;
        insert objTask ;
        
        Test.startTest();
        	Database.executeBatch( new HandoverEligibilityUpdatesBatch() );
        Test.stopTest();
    }
    
}