// CreatedBy :  Naresh Kaneriya
        
        public class AgentServiceRequestDetailsController{
        
        
        public String SrId ;

        public String SrNumber {get;set ;} 
        public transient List<Buyer__c> BuyerList {get;set;}
        public transient List<Unit_Documents__c> unitDocumentList {get;set;}
        public transient List<Proof_of_Payment__c> proofOfPaymentList {get;set;}
        public Transient  List<Booking_Unit__c> Bulist  {get;set;}
        public Transient  List<Payment_Terms__c> Paymentterms {get;set;}
        public List<BookingData>  BUdata {get ; set ;}
             
        //Constructor
        public AgentServiceRequestDetailsController(){
        
        String SrId  = ApexPages.Currentpage().getparameters().get('SRId');
        SrNumber = SrId ;
        BUdata = new List<BookingData> ();
        Paymentterms =  new List<Payment_Terms__c>(); 
        Bulist = new List<Booking_Unit__c>();
        BuyerList =  new List<Buyer__c>();
        unitDocumentList = new List<Unit_Documents__c> ();
        proofOfPaymentList = new List<Proof_of_Payment__c>();
        System.debug('Service Request Name-----  '+SrId);
        
        BuyerList =   [ Select Id ,
                           Title__c,
                           First_Name__c ,  
                           Last_Name__c,
                           Passport_Number__c,
                           Place_of_Issue__c,
                           Passport_Expiry_Date__c,
                           Date_of_Birth__c,
                           Email__c,
                           Nationality__c,
                           Primary_Buyer__c,
                           Buyer_Type__c from Buyer__c  where Booking__r.Deal_SR__r.Name =: SrId ORDER BY CreatedDate DESC ] ;
        system.debug('>>>>BuyerList>>>>>'+BuyerList);                     
        unitDocumentList = [ SELECT Id, 
                            Name,
                            Created_Date_Time__c,
                            SPA__c,
                            SPA_F__c,
                            DP_INVOICE_F__c,
                            DP_INVOICE__c,
                            OPTION__c,
                            PROMOTION__c,
                            Scheme__c,
                            SOA__c,
                            SOA_F__c,
                            Document_type__c,
                            Service_Request__r.Name,
                            Campaign_SPA_F__c,
                            Campaign_SPA__c,
                            SPA_ABN__c,
                            SPA_ABN_F__c,
                            SPA_AGS__c,
                            SPA_AGS_F__c,
                            SPA_AGSX__c,
                            SPA_AGSX_F__c,
                            SPA_CRT__c,
                            SPA_CRT_F__c,
                            SPA_LAGX__c,
                            SPA_LAGX_F__c
                            FROM Unit_Documents__c
                            WHERE Service_Request__r.Name =: SrId ORDER BY CreatedDate DESC];
                            system.debug('>>>>unitDocumentList>>>>>'+unitDocumentList);  
       proofOfPaymentList = [ SELECT id,
                                     Name,
                                    Payment_Amount__c,
                                    IPMS_Status__c,
                                    Finance_Status__c,
                                    CreatedDate,(SELECT Id, Name FROM Attachments  ORDER BY CreatedDate DESC LIMIT 1)
                               FROM Proof_of_Payment__c
                               WHERE Deal_SR__r.Name=: SrId ORDER BY CreatedDate DESC];  
         system.debug('>>>>proofOfPaymentList>>>>>'+proofOfPaymentList);                                        
         Bulist =       [Select id ,
                            Name ,
                            Registration_DateTime__c,               
                            Requested_Price__c,
                            Area_sft__c,
                            Requested_Token_Amount__c,
                            Token_Paid__c,  
                            Token_Paid_Time__c,
                            DP_Due_Date__c,
                            DP_Overdue__c,
                            DP_OK__c,
                            Doc_OK__c,
                            NI_Payment_URL__c,
                            Unit_Location__c,
                            Delivery_Mode__c,
                            (select id , Name from Payment_Plans__r)  
                            from Booking_Unit__c  where Booking__r.Deal_SR__r.Name =: SrId ORDER BY CreatedDate DESC ] ;   
         system.debug('>>>>Bulist>>>>>'+Bulist);                    
               
               for(Booking_Unit__c B : Bulist ){
                   
                   BookingData BU = new BookingData();  
                   BU.Units = B.Unit_Location__c;
                   BU.RegistrationDate = String.valueOf(B.Registration_DateTime__c);
                   BU.Price = String.valueOf(B.Requested_Price__c);
                   BU.Area = String.valueOf(B.Area_sft__c);
                   BU.RequestedTokenamount = String.valueOf(B.Requested_Token_Amount__c);
                   BU.TokenPaid = String.valueOf(B.Token_Paid__c);
                   BU.TokenPaidTime = String.valueOf(B.Token_Paid_Time__c);
                   BU.DPDueDate = String.valueOf(B.DP_Due_Date__c);
                   BU.DPOverdue = B.DP_Overdue__c;
                   BU.DPOK = B.DP_OK__c;
                   BU.DOCOK = B.Doc_OK__c;
                   
                   for(Payment_Plan__c s : B.Payment_Plans__r){ 
                       BU.PaymentPlan = s.Name ;
                       BU.PaymentPlanID = s.Id ;
                   } 
                  
                   BU.SPA = B.Delivery_Mode__c;
                   
                   BUdata.add(BU);
               }             
               
               
        }
        
        public class BookingData{
            
            public String Units{get; set;}
            public String RegistrationDate {get; set;}
            public String Price{get; set;}
            public String Area{get; set;}
            public String RequestedTokenamount{get; set;}
            public String TokenPaid{get; set;}
            public String TokenPaidTime{get; set;}
            public String TokenDueDate{get; set;}
            public String DPDueDate{get; set;}
            public Boolean DPOverdue{get; set;}
            public Boolean DPOK{get; set;}
            public Boolean DOCOK{get; set;}
            public String PaymentPlan{get; set;}
            public String PaymentPlanID{get; set;}
            public String SPA {get; set;}
            
            
           }
           
           
           public pagereference Back(){
            Pagereference pg =  new Pagereference('/apex/AgentOnGoingBookings'); 
            pg.setRedirect(true);
            return pg;
           
           }
        }