/*
 * Description: Class used to fetch Case details for FetchCaseDetailsComponent
 */

public class FetchCaseDetailsController {
    public String whatId {get; set;}
    public String newLine { get; set; }
    public Case objCase {get; set;}
    
    /*public String newLine {
        get {return '\r\n';} set;
    }*/
    
    /*public FetchCaseDetailsController() {
        newLine = '\r\n'; 
    }*/

    /*
     * Method to fetch Case from related Task's What Id
     */
    public String getCaseDetails() {
        init();
        return '';
    }
    
    private void init() {
        newLine = '\r\n';
        List<Case> lstCase = [SELECT CaseNumber, Account.Name, RecordType.Name FROM Case WHERE Id =: whatId];
        objCase = lstCase[0];
    }
}