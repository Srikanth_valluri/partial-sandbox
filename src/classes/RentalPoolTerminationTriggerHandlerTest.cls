/*
* Description - Test class developed for 'RentalPoolTerminationTriggerHandler'
*
* Version            Date            Author            Description
* 1.0              28/02/2018        Ashish           Initial Draft
*/

@isTest 
private class RentalPoolTerminationTriggerHandlerTest {

    static testMethod void testAfterCaseUpdate() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        Case objCase1 = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() );
        objCase1.OwnerId = UserInfo.getUserId();
        objCase1.Submit_for_Approval__c = true ;
        objCase1.Approval_Status__c = 'Approved';
        objCase1.Rental_Pool_Termination_Status__c = 'Approved By Manager';
        objCase1.Status = 'Manager Approved';
        insert objCase1 ;
        
        Case objCase2 = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() );
        objCase2.OwnerId = UserInfo.getUserId();
        objCase2.Submit_for_Approval__c = true ;
        objCase2.Approval_Status__c = '';
        objCase2.Rental_Pool_Termination_Status__c = '';
        objCase2.Status = '';
        
        Case objCase3 = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() );
        objCase3.OwnerId = UserInfo.getUserId();
        objCase3.Submit_for_Approval__c = true ;
        objCase3.Approval_Status__c = 'Approved';
        objCase3.Rental_Pool_Termination_Status__c = 'Approved from Hospitality Executive';
        objCase3.Status = 'HE Approved';
        objCase3.Final_Settlement_Amount__c = 200;
        objCase3.Final_Termination_Date__c = system.today();
        insert objCase3 ;
        
        Case objCase4 = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() );
        objCase4.OwnerId = UserInfo.getUserId();
        objCase4.Submit_for_Approval__c = true ;
        objCase4.Approval_Status__c = '';
        objCase4.Rental_Pool_Termination_Status__c = '';
        objCase4.Status = '';
        objCase4.Final_Settlement_Amount__c = null;
        objCase4.Final_Termination_Date__c = null;
        
        SR_Attachments__c objAttach = TestDataFactory_CRM.createCaseDocument( objCase1.Id, 'CRF Form');
        insert objAttach ;
        
        SR_Attachments__c objAttach1 = TestDataFactory_CRM.createCaseDocument( objCase1.Id, 'RP Agreement');
        insert objAttach1 ;
        
        RentalPoolTerminationTriggerHandler.afterCaseUpdate( new map<Id, Case>{ objCase1.Id => objCase1, objCase3.Id => objCase3 }, 
        														 new map<Id, Case>{ objCase1.Id => objCase2, objCase3.Id => objCase4 } );
        test.startTest();
			Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAssignment() );	
        	
			
			
		test.stopTest();
		
    }
}