//AKISHOR
@isTest
private class Undo_SRTest{
    static testMethod void test_UndoTest() {          
             
    // Insert Accont
    Id accntrecid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
    Account objAcc = new Account();
    objAcc.FirstName='AK';
    objAcc.LastName='TEST';
    objAcc.Type='Person';
    objAcc.Party_Id__c='79658';
    objAcc.RecordTypeId=accntrecid;
    insert objAcc;

    //Insert & Upd Case
    Id caseRecId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Fund Transfer Active Units').getRecordTypeId();
    Case caseObj =new Case(AccountId=objAcc.id,Status='New',RecordTypeId=caseRecId,Description=objAcc.FirstName);
    insert caseObj;
    system.debug('caseObj : '+caseObj);
         test.startTest();
         PageReference pageRef = Page.Undo_SR;
         Test.setCurrentPage(pageRef);
         ApexPages.StandardController sc = new ApexPages.standardController(caseObj);
         Undo_SR controller = new Undo_SR(sc);
         controller.cid = caseObj.Id;
         Pagereference newPg = controller.updateCasestatus();
         //System.assertEquals(null,controller.cancelCase());
         test.stopTest();
     } 
}