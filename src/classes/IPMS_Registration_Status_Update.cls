/**************************************************************************************************
* Name               : IPMS_Registration_Status_Update
* Description        : This is the queuable webservice class for the below webservices
                        
                        -Rehistration Status update with Specific code                                  
* Created Date       : Dec/2017                                                                 
* Created By         : DAMAC IT                                                     
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR              DATE                                                             
* 1.0         VK                  12/06/2017                                                        
**************************************************************************************************/

global class IPMS_Registration_Status_Update implements Queueable, Database.AllowsCallouts {
    
    public List<id> recordids = new List<id>();
    public string servicetype;
    public string statusCode;
    
    global static string endurl;
    global static string usrname;
    global static string pwd; 
    
    //Defining the constructor
    public IPMS_Registration_Status_Update(List<Id> rids, string svctype,string ToupdateCode){
        recordids=rids;
        servicetype=svctype; 
        statusCode = ToupdateCode;
    }
     //The execute method for the queuable class which contains the callout
    public void execute(QueueableContext context) {        
        //if(servicetype == 'STATUS_UPDATE'){   
            sendRegnUpdate(recordids,servicetype,statusCode);
        //}      
    }
    
     
    /**********************Getting the IPMS integration setting*********************************/ 
    
    public static void getIPMSsetting(String servicename){
      system.debug('***SSS***'+servicename);
      IPMS_Integration_Settings__mdt ipms= [select id,Endpoint_URL__c,Password__c,  Username__c from IPMS_Integration_Settings__mdt where DeveloperName = :servicename];
      endurl=ipms.Endpoint_URL__c;
      usrname=ipms.Username__c;      
      pwd=PasswordCryptoGraphy.DecryptPassword(ipms.Password__c);
      system.debug('***IPMS***'+ipms);
      
      system.debug('***PWD***'+pwd);
      
    }
    
     /**********************Preparing the general header for the http request*********************************/
     
    public static string preparesoapHeader(string body){
   
     body+='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xxdc="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/" xmlns:proc="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/process/">';
       body+='<soapenv:Header>';
          body+='<xxdc:SOAHeader>';
             //body+='<!--Optional:-->';
             body+='<xxdc:Responsibility>ONT_ICP_SUPER_USER</xxdc:Responsibility>';
             //body+='<!--Optional:-->';
             body+='<xxdc:RespApplication>ONT</xxdc:RespApplication>';
             //body+='<!--Optional:-->';
             body+='<xxdc:SecurityGroup>standard</xxdc:SecurityGroup>';
             //body+='<!--Optional:-->';
             body+='<xxdc:NLSLanguage>american</xxdc:NLSLanguage>';
             //body+='<!--Optional:-->';
             body+='<xxdc:Org_Id/>';
          body+='</xxdc:SOAHeader>';
          body+='<wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">';
             body+='<wsse:UsernameToken>';
                body+='<wsse:Username>'+usrname+'</wsse:Username>';
                body+='<wsse:Password>'+pwd+'</wsse:Password>';
             body+='</wsse:UsernameToken>';
          body+='</wsse:Security>';
       body+='</soapenv:Header>';
       return body;
    }
    

    
    
    
    webservice static void sendRegnUpdate(List<id> regids, String UpdType, string code){
        
        getIPMSsetting('IPMS_webservice');
        HTTPRequest req = new HTTPRequest();
        req.setMethod('POST');
        
        String reqXML = prepareRegnUpdate(regids,UpdType,code);
        reqXML = reqXML.replaceAll('null', '');
        reqXML = reqXML.trim();
        req.setbody(reqXML);
        req.setEndpoint(endurl);
        System.debug('>>>>>>>>reqXML>>>>>>>>'+reqXML);
        req.setHeader('Content-Type','text/xml');
        req.setTimeout(120000);
        HTTP http = new HTTP();
        if(!Test.isrunningTest()){
            try{
                HTTPResponse res = http.send(req);                
                System.debug('>>>>>Response>>>>>>'+res.getbody());                
                parseRegnUpdateResponse(res.getBody(),code);
            }
            catch(Exception ex){
                System.debug('Callout error: '+ ex);
                Log__c objLog = new Log__c();
                objLog.Description__c =UpdType+'--Ids=='+regids+'-Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage();
                objLog.Type__c = 'Webservice Callout For Registration Updation';
                insert objLog;             
            }    
        }
    }
    
     public static string prepareRegnUpdate(List<id> regids,String UpdType,string code){
        
        string BUQuery = getCreatableFieldsSOQL('Booking_Unit__c');
        BUQuery+=' where Booking__c in :regids';
        List<Booking_Unit__c> BUlist= database.query(BUQuery); 
        
        String reqno='123456';
        if(BUlist.size()>0)
            reqno ='UpdReg-'+BUlist[0].name+'-'+GetFormattedDateTime(system.now());
        system.debug('REQ NO==='+reqno);
        
        List<Id>SRIds = new List<Id>();
        for(Booking_Unit__c BU : BUlist){
            SRIds.add(BU.SR_Id__c);
        }
        
        string body = '';
        body+= preparesoapHeader(body);            
        body+='<soapenv:Body>';
        body+='<proc:InputParameters>';
        body+='<proc:P_REQUEST_NUMBER>'+reqno+'</proc:P_REQUEST_NUMBER>';
        body+='<proc:P_SOURCE_SYSTEM>SFDC</proc:P_SOURCE_SYSTEM>';
        body+='<proc:P_REQUEST_NAME>UPDATE_REGISTRATION</proc:P_REQUEST_NAME>';
        body+='<proc:P_REQUEST_MESSAGE>';
        for(Booking_Unit__c BU :BUlist){
            String vendorid,docok,delmode,callok,status='';
            
            if(UpdType=='AGENT_UPDATE'){
                vendorid=BU.Vendor_ID__c;
            }
            if (UpdType=='DOC_OK'){
                if(BU.Doc_OK__c){
                    docok='Y';
                }
                else{
                    docok='N';
                }
            }
            if(UpdType=='DISPATCH_MODE'){
                delmode=BU.Delivery_Mode__c;
            }
            if (UpdType=='CALL_GENERATION'){
                callok='Y';  
            }
            
            if(UpdType=='BOOKING_CANCELLATION' ||  UpdType=='STATUS_UPDATE'){
                status = code;
            }              
            
            
            body+='<proc:P_REQUEST_MESSAGE_ITEM>';
            body+='<proc:PARAM_ID>'+BU.id+'</proc:PARAM_ID>';                            
            body+='<proc:ATTRIBUTE1>'+BU.Registration_ID__c+'</proc:ATTRIBUTE1>';
            body+='<proc:ATTRIBUTE2>'+UpdType+'</proc:ATTRIBUTE2>';
            body+='<proc:ATTRIBUTE3>'+docok+'</proc:ATTRIBUTE3>';
            body+='<proc:ATTRIBUTE4>'+vendorid+'</proc:ATTRIBUTE4>';
            body+='<proc:ATTRIBUTE5>'+delmode+'</proc:ATTRIBUTE5>';
            body+='<proc:ATTRIBUTE6>'+status+'</proc:ATTRIBUTE6>';            
            body+='<proc:ATTRIBUTE11>'+callok+'</proc:ATTRIBUTE11>';
            body+='</proc:P_REQUEST_MESSAGE_ITEM>';
        }
        body+='</proc:P_REQUEST_MESSAGE>';
        body+='</proc:InputParameters>';
        body+='</soapenv:Body>';
        body+='</soapenv:Envelope>';
        
        return body.trim();
        
    }
    
     public static void parseRegnUpdateResponse(string body,string code){
        
        string reqId,BUid,status,stageId,doclist = '';
        
        List<NSIBPM__SR_Doc__c> SRDocList = new List<NSIBPM__SR_Doc__c>();
        
        List<String> BUids = new List<String>();
        Map<string,String> StatusMap = new Map<string,string>();
        
        DOM.Document xmlDOC = new DOM.Document();
        xmlDOC.load(body);
        DOM.XMLNode rootElement = xmlDOC.getRootElement();
        
        
        for(Dom.XMLNode child1: rootElement.getChildElements()){
            for(Dom.XMLNode child2: child1.getChildElements()){
                for(Dom.XMLNode child3: child2.getChildElements()){
                    for(Dom.XMLNode child4: child3.getChildElements()){
                        for(Dom.XMLNode child5: child4.getChildElements()){
                            System.debug('>>>>>>>>child5>>>>>>'+child5.getName());
                            if(child5.getName()=='PARAM_ID') 
                                BUid=child5.getText();
                            BUids.add(BUid);   
                            if(child5.getName()=='PROC_MESSAGE')
                                status=child5.getText();
                        }                          
                        Statusmap.put(BUid,status);     
                    }
                }
            }
        }
        
        
        List<Booking_Unit__c> BUlist= [select id,Booking__c,SR_Status_Code__c,Booking__r.Deal_SR__c,Registration_ID__c,IPMS_Status__c,SOA_Request_ID__c,SOA_Stage_Id__c,Unit_Location_ID__c from Booking_Unit__c where Status__c!='Removed' and id in:Statusmap.keyset()];
        if(BUList.size()>0){
            for(Booking_Unit__c BU :BUlist){     
                BU.IPMS_Status__c='Status Update -'+code+'-'+StatusMap.get(BU.id);                
            }
            update BUlist;
        }
        
    }
    
    /*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/


    
    /*********************************************************************************************
    * @Description : Reusable methods                                                              *
    *                                                                                              *
    *********************************************************************************************/
    
    
    //Generic method for fields for SOQL query
    public static string getCreatableFieldsSOQL(string objectName){
        String selects = '';
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
               // if (fd.isCreateable()){ // field is creatable
                    selectFields.add(fd.getName());
              //  }
            }
        }
        // contruction of SOQL
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
             
        }
        // return constrcucted query
        return 'SELECT ' + selects + ' FROM ' + objectName ;
         
    }
    
     //Method to Get formatted date
    public static string GetDatetext(Date d){
        String dd,mm,mon,yy,dtext='';
        dd=string.valueof(d.day());
        mm=string.valueof(d.month());
        if(mm=='1')
        mon='JAN';
        else if(mm=='2')
        mon='FEB';
        else if(mm=='3')
        mon='MAR';
        else if(mm=='4')
        mon='APR';
        else if(mm=='5')
        mon='MAY';
        else if(mm=='6')
        mon='JUN';
        else if(mm=='7')
        mon='JUL';
        else if(mm=='8')
        mon='AUG';
        else if(mm=='9')
        mon='SEP';
        else if(mm=='10')
        mon='OCT';
        else if(mm=='11')
        mon='NOV';
        else if(mm=='12')
        mon='DEC';
        
        yy=string.valueof(d.year());
        dtext= dd+'-'+mon+'-'+yy;
        return dtext;
    
    }
    
    //Method to Get current time in a string
    public static string GetFormattedDateTime(DateTime dt){
      String yyyy=string.valueof(dt.year());
      String mm=string.valueof(dt.month());
      String dd=string.valueof(dt.day());
      String hh=string.valueof(dt.hour());
      String mi=string.valueof(dt.minute());
      String ss=string.valueof(dt.second());
      String ms=string.valueof(dt.millisecond());
      
      String formatdate=yyyy+mm+dd+hh+mi+ss+ms;
      return formatdate;
    }
    
    } // End of class