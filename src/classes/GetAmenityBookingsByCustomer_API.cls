@RestResource(urlMapping='/getAmenityBookingsByCustomer/*')
global class GetAmenityBookingsByCustomer_API {

    public static final String NO_FMCASES = 'No FmCases found for given accountId & bookingUnitId';
    public static final String SUCCESS = 'Successful';
    public static String responseMessage;
    public static Integer statusCode;

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    @HttpPost
    global static FinalReturnWrapper getAmenityBookings() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        AmenityBookingsFinalWrapper objFinalWrapper = new AmenityBookingsFinalWrapper();
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        List<FmCaseWrapper> FmCases = new  List<FmCaseWrapper>();

        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();

        if(!r.params.containsKey('accountId')) {

            objMeta.message = 'Missing parameter : accountId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;

        }
        else if(r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId'))) {
            objMeta.message = 'Missing parameter value: accountId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;

        }
        if(!r.params.containsKey('bookingUnitId')) {

            objMeta.message = 'Missing parameter : bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;

        }
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) {
            objMeta.message = 'Missing parameter value: bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;

        }


        if(String.isNotBlank(r.params.get('accountId')) && String.isNotBlank(r.params.get('bookingUnitId'))) {

            System.debug('accountId: ' + r.params.get('accountId'));
            System.debug('bookingUnitId: ' + r.params.get('bookingUnitId'));


            List<Account> acc = [SELECT id FROM Account WHERE id =: r.params.get('accountId')];
            List<Booking_Unit__c> BU = [SELECT id FROM Booking_Unit__c WHERE id =: r.params.get('bookingUnitId')];

            if(acc == null || acc.isEmpty()) {
                objMeta.message = 'No Account found for given accountId';
                objMeta.status_code = 3;
                objMeta.title = mapStatusCode.get(3);
                objMeta.developer_message = null;
               //objMeta.is_success = false;

                returnResponse.meta_data = objMeta;
                return returnResponse;
            }
            if(BU == null || BU.isEmpty()) {
                objMeta.message = 'No Account found for given bookingUnitId';
                objMeta.status_code = 3;
                objMeta.title = mapStatusCode.get(3);
                objMeta.developer_message = null;
               //objMeta.is_success = false;

                returnResponse.meta_data = objMeta;
                return returnResponse;
            }

            try {
                FmCases = fetchAmenityBookingFmCases(r.params.get('accountId'), r.params.get('bookingUnitId'));
                //statusCode = FmCases.size() > 0 ? 1 : 2;
                statusCode = 1;
                responseMessage = FmCases.size() > 0 ? SUCCESS : NO_FMCASES;
            }
            catch (Exception e) {
                objMeta.status_code = 6;
                objMeta.message = e.getMessage();
                objMeta.title = mapStatusCode.get(6);
                objMeta.developer_message = null;
                
                returnResponse.meta_data = objMeta;
                return returnResponse; 
            }
           

            //segregate upcoming & past cases :
            List<FmCaseWrapper> pastBookings = new List<FmCaseWrapper> ();
            List<FmCaseWrapper> upcomingBookings = new List<FmCaseWrapper>();

            if(FmCases.size() > 0) {

                objData.account_id = r.params.get('accountId');
                objData.booking_unit_id =  r.params.get('bookingUnitId');


                for(FmCaseWrapper objFM : FmCases) {

                    Date bookingDate = Date.valueOf(objFM.booking_date);
                    System.debug('bookingDate: ' + bookingDate);

                    /*logic for getting current time*/
                    DateTime now = DateTime.now();
                    Integer hours = now.hour(), minutes = now.minute();
                    System.debug('==hours==' + hours);
                    System.debug('==minutes==' + minutes);
                    String currentTime = String.valueOf(hours) + ':' + String.valueOf(minutes);
                    Time tmCurrentTime = getTimeInstanceFromText(currentTime);
                    System.debug('tmCurrentTime'+tmCurrentTime);

                    Time startT = getTimeInstanceFromText(objFM.start_time);
                    System.debug('startT::'+startT);
                    /*End*/

                    if((bookingDate > Date.today()) || (bookingDate == Date.today() && startT > tmCurrentTime)) {
                        upcomingBookings.add(objFM);
                        //objData.upcoming_bookings.add(objFM); 
                    }
                    else {
                        pastBookings.add(objFM);
                        //objData.past_bookings.add(objFM);

                    }
                }

                objData.upcoming_bookings = upcomingBookings;
                objData.past_bookings = pastBookings;
            }

            System.debug('objData.upcoming_bookings: ' + objData.upcoming_bookings);
            System.debug('objData.past_bookings: ' + objData.past_bookings);
        }

        //System.debug('responseMessage: ' + responseMessage);
        //objFinalWrapper.status = responseMessage == SUCCESS || responseMessage == NO_FMCASES ? 'Success' : 'failed';
        //objFinalWrapper.message = responseMessage;

        //Wrapper filling for final response
        //cls_meta_data objMeta = new cls_meta_data();
        objMeta.status_code = statusCode;
        objMeta.message = responseMessage;
        objMeta.title = mapStatusCode.get(statusCode);
        objMeta.developer_message = null;

        //cls_data objData = new cls_data();

        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;

        //System.debug('objFinalWrapper: ' + objFinalWrapper);
        System.debug('returnResponse: ' + returnResponse);

        //return objFinalWrapper;
        return returnResponse;

    }

    public static List<FmCaseWrapper> fetchAmenityBookingFmCases(String accountId, String bookingUnitId) {

        List<FmCaseWrapper> lstFmCaseWrapper = new List<FmCaseWrapper>();

        List<FM_Case__c> lstFmCase = [SELECT id
                                           , Name
                                           , Resource__c
                                           , Resource__r.Name
                                           , Resource__r.Amenity_Icon__c
                                           , Resource_Booking_Chargeable_Fee__c
                                           , Resource_Booking_Deposit__c
                                           , Resource_Booking_Due_Date__c
                                           , Resource_Booking_Type__c
                                           , Initiated_by_tenant_owner__c
                                           , Amenity_Name__c    
                                           , Booking_Date__c
                                           , Email__c
                                           , Mobile_Country_Code__c
                                           , Mobile_no__c
                                           , Submission_Datetime__c
                                           , Tenant__c
                                           , Account__c
                                           , Booking_Unit__c
                                           , Unit_Name__c
                                           , Origin__c
                                           , Booking_Start_Time_p__c
                                           , Booking_End_Time_p__c
                                           , Amenity_Booking_Status__c
                                           , Status__c
                                           , No_of_BBQ_pit__c
                                           , No_of_hosts__c
                                           , No_of_guests__c
                                           , Need_swimming_pool__c
                                           , First_Name__c
                                           , Last_Name__c
                                           , Resource_SlotID__c
                                           , CreatedDate
                                           , Description__c
                                           , Amenity_location_Latitude__c
                                           , Amenity_location_Longitude__c
                                      FROM FM_Case__c
                                      WHERE Tenant__c =:accountId      //since same field is updated for both Tenant/Account
                                      AND Booking_Unit__c =: bookingUnitId
                                      AND recordType.Name = 'Amenity Booking'
                                      AND (Amenity_Booking_Status__c != 'Cancelled'
                                           AND Amenity_Booking_Status__c != 'Rejected') /*As per bug : 2025*/
                                      AND Origin__c = 'Portal'
                                      ORDER BY Booking_Date__c DESC];

        System.debug('lstFmCase: '+lstFmCase);
        System.debug('lstFmCase.size: '+lstFmCase.size());

        for(FM_Case__c objFmCase : lstFmCase) {

            //String jsonStr = JSON.serialize(objFmCase);
   //         jsonStr = jsonStr.replaceAll('__c','');
   //         jsonStr = jsonStr.replaceAll('_p','');
   //         System.debug('jsonStr: ' + jsonStr);

   //         FmCaseWrapper objWrap = (FmCaseWrapper)JSON.deserialize(jsonStr, FmCaseWrapper.class);
            System.debug('objFmCase:: ' + objFmCase);

            FmCaseWrapper objWrap = new FmCaseWrapper();

            objWrap.fm_case_id = objFmCase.Id;
            //objWrap.account_id = objFmCase.Tenant__c;
            //objWrap.booking_unit_id = objFmCase.Booking_Unit__c;
            objWrap.amenity_id = objFmCase.Resource__c;
            objWrap.amenity_slot_id = objFmCase.Resource_SlotID__c;
            objWrap.amenity_name = objFmCase.Resource__r.Name;
            objWrap.amenity_icon = objFmCase.Resource__r.Amenity_Icon__c; /* Added on 2020 Sep 08 to add amenity icon : Azure ticket 1113 */
            objWrap.amenity_booking_status = objFmCase.Amenity_Booking_Status__c;


            objWrap.booking_date = objFmCase.Booking_Date__c != null ? Datetime.newInstance(objFmCase.Booking_Date__c.year(), objFmCase.Booking_Date__c.month(), objFmCase.Booking_Date__c.day()).format('yyyy-MM-dd') : null;
            objWrap.start_time = objFmCase.Booking_Start_Time_p__c;
            objWrap.end_time = objFmCase.Booking_End_Time_p__c;
            objWrap.fm_case_number = objFmCase.Name;


            objWrap.fm_case_creation_date = objFmCase.CreatedDate != null ? Datetime.newInstance(objFmCase.CreatedDate.year(), objFmCase.CreatedDate.month(), objFmCase.CreatedDate.day()).format('yyyy-MM-dd') : null;
            objWrap.location_latitude = String.isNotBlank(objFmCase.Amenity_location_Latitude__c) ? Decimal.valueOf(objFmCase.Amenity_location_Latitude__c) : null;
            objWrap.location_longitude = String.isNotBlank(objFmCase.Amenity_location_Longitude__c) ? Decimal.valueOf(objFmCase.Amenity_location_Longitude__c) : null; 
            objWrap.no_of_guests = objFmCase.No_of_guests__c;
            objWrap.comments = objFmCase.Description__c;
            
            lstFmCaseWrapper.add(objWrap);
        }
        System.debug('lstFmCaseWrapper:: ' + lstFmCaseWrapper);
        return lstFmCaseWrapper;
    }

    public static Time getTimeInstanceFromText(String timeInText) {
        String [] timeTxt = timeInText.split(':');
        Time timeInstance = Time.newInstance(Integer.valueOf(timeTxt[0]),
                            Integer.valueOf(timeTxt[1]), 0, 0);
        return timeInstance;
    }

    //Wrapper class
    global class AmenityBookingsFinalWrapper {
        public String status;
        public String message;
        public FmCaseWrapper[] FMCases;
    }

    public class FmCaseWrapper {
        //public String Id; //a4025000000Z4b5AAC
        //public String Name;   //FMN-05826
        //public String Resource;   //a4b250000013QUvAAM
        //public String Resource_Booking_Chargeable_Fee;    //23
        //public String Resource_Booking_Deposit;   //2
        //public String Resource_Booking_Due_Date;  //2020-05-24
        //public String Resource_Booking_Type;  //FM Amenity
        //public String Initiated_by_tenant_owner;  //Owner
        //public String Amenity_Name;   //Tennis Court - 1
        //public String Mobile_Country_Code;    //United Arab Emirates: 00971
        //public String Submission_Datetime;    //2020-05-21T13:29:02.000+0000
        //public String Tenant; //0010Y00000MaKkwQAF
        //public String Account;    //0010Y00000MaKkwQAF
        //public String Booking_Unit;   //a0x25000000OnEwAAK
        //public String Unit_Name;  //DFA/20/A2004
        //public String Origin; //Portal
        //public String Booking_Start_Time; //04:00
        //public String Booking_End_Time;   //06:00
        //public String Amenity_Booking_Status; //On Hold
        //public String Status; //Submitted
        //public String No_of_hosts;    //10
        //public String No_of_guests;   //10
        //public String RecordTypeId;   //012250000009e5QAAQ

        /*New*/
        public String fm_case_id;
        //public String account_id;
        //public String booking_unit_id;
        public String amenity_id;
        public String amenity_slot_id;
        public String amenity_name;
        public String amenity_icon; /* Added on 2020 Sep 08 to add amenity icon : Azure ticket 1113 */
        public String amenity_booking_status;
        public String booking_date;
        public String start_time;
        public String end_time;
        public String fm_case_number;
        public String fm_case_creation_date;
        public Decimal location_latitude;
        public Decimal location_longitude;
        public Decimal no_of_guests;
        public String comments;
    }

    //Wrapper classes for returning reponse
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        //public FmCaseWrapper[] fm_cases;
        public String account_id;
        public String booking_unit_id;
        public FmCaseWrapper[] upcoming_bookings;
        public FmCaseWrapper[] past_bookings;
    }

    public class cls_meta_data {
        //public String status;
        //public String message;   

        public Integer status_code;
        public String message;
        public String title;
        public String developer_message; 
    }


}