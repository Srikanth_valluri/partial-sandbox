/*
Class Name : HD_HomePageRefresh_API
Description : To get basic data in login refreshed on request 
Test Class : HD_HomePageRefresh_APITest   

========================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
--------------------------------------------------------------------------------------------------------
1.0     |   03-11-2020      | Subin Antony        | Added profile completion data to response
*******************************************************************************************************/
@RestResource(urlMapping='/hd/home')
global class HD_HomePageRefresh_API {
    public static Map<Integer, String> statusCodeMap;
    static {
        statusCodeMap = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'};
    }
    
    @HttpGet
    global static void doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg, exceptionMsg, successMsg;
        
        String ownerAccountId = req.params.containskey('account_id') ? req.params.get('account_id') : '';
        if(String.isBlank(ownerAccountId)){
            errorMsg = 'Please provide account_id.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
            return;
        }
        
        Account accountRecord;
        try{
            accountRecord = [SELECT id, Name, Name_Arabic__c, IsPersonAccount, Party_ID__c, Party_Type__c, 
                 Primary_Language__c, Email__pc, Mobile_Country_Code__pc, Mobile_Phone_Encrypt__pc 
                 FROM Account WHERE Id = :ownerAccountId];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
        }
        if(NULL == accountRecord){
            errorMsg = 'The account_id provided does not match any records.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
            return;
        }
        
        System.debug('....................... ownerAcc_inAPI: ' + ownerAccountId);
        
        User currentUser;
        try{
            currentUser = [SELECT Id, Name,  UserName,  Profile.Name,  AccountId,  ContactId,  Account.Name,  Contact.Name, Contact.AccountId, 
                           Contact.Account.Party_ID__c,  Contact.Account.Nationality__c ,  Contact.Account.Nationality__pc,  
                           Contact.Account.FirstName,  Contact.Account.LastName,  Contact.Account.Salutation,  Contact.Account.Email__pc,  
                           Contact.Account.Email__c,  Contact.Account.IsPersonAccount,  Contact.Account.Party_Type__c,  FullPhotoUrl 
                           FROM User WHERE Profile.Name LIKE '%COMMUNITY%' 
                           AND IsActive = TRUE AND Contact.AccountId = :ownerAccountId LIMIT 1];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
        }
        if(NULL == currentUser){
            errorMsg = 'Failed to fetch corresponding `Portal User` record for the account_id provided.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
            return;
        }
        
        cls_data usrData = new cls_data();
        List<API_CommunityPortalLogin.UnitDetailsWrapper> units;
        API_CommunityPortalLogin.UnitDetailsDTO returnDTO;
        
        try{
            /* usrData.session_id = req.headers.get('Authorization').subStringAfter('Bearer '); */
            usrData.user_id = currentUser.Id;
            usrData.account_id = currentUser.AccountId;
            usrData.contact_id = currentUser.ContactId;
            usrData.profile_name = currentUser.Profile.Name;
            usrData.account_name = currentUser.Account.Name;
            usrData.party_id = currentUser.Contact.Account.Party_ID__c;
            usrData.user_name = currentUser.Name;
            usrData.nationality = currentUser.Contact.Account.IsPersonAccount ? 
                currentUser.Contact.Account.Nationality__pc : currentUser.Contact.Account.Nationality__c;
            usrData.profile_pic_uri = Label.PortalDomainForLoginApi.replace('/Customer','') + String.valueOf(currentUser.FullPhotoUrl);
            usrData.person_pre_name_adjunct = currentUser.Contact.Account.Salutation;
            usrData.person_first_name = currentUser.Contact.Account.FirstName;
            usrData.person_last_name = currentUser.Contact.Account.LastName;
            usrData.email = currentUser.Contact.Account.IsPersonAccount ? 
                currentUser.Contact.Account.Email__pc : currentUser.Contact.Account.Email__c;
            
            returnDTO = API_CommunityPortalLogin.GetUnitsDetailsFromAccountId(currentUser.AccountId);
            units = returnDTO.properties_inUAE;
            usrData.property_details = units;
            
        	usrData.profile_completion = getProfileCompletionData(currentUser.Contact.AccountId, currentUser);  /* Added on 03 Nov 2020 : Azure ticket 2766 */
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            system.debug(ex.getStackTraceString());
        	exceptionMsg = ex.getCause() + ' : ' + ex.getMessage() + ' : ' + ex.getStackTraceString();
            units = NULL;
        }
        
        if(NULL == units) {
            errorMsg = 'Error while fetching portal user home page data.';
            getErrorResponse(2, statusCodeMap.get(2), errorMsg, exceptionMsg);
            return;
        }
        
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = 1;
        responseMetaData.title = statusCodeMap.get(1);
        responseMetaData.message = 'Success in fetching refreshed home page data for user';
        responseMetaData.developer_message = null;
        
        ResponseWrapper responseWrapper = new ResponseWrapper();
        responseWrapper.meta_data = responseMetaData;
        responseWrapper.data = usrData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    private static void getErrorResponse(Integer statusCode, String title, 
    String responseMessage, String devMessage) {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = statusCode;
        responseMetaData.title = title;
        responseMetaData.message = responseMessage;
        responseMetaData.developer_message = devMessage;
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    /*
     * To get profile completion data.
     * Added on 03 Nov 2020 : Azure ticket 2766 */
    public static GetCustomerDetailsForHDApp_API.cls_profile_completion getProfileCompletionData(String accountId, User usr) {
        GetCustomerDetailsForHDApp_API.cls_profile_completion profileCompletionData = new GetCustomerDetailsForHDApp_API.cls_profile_completion();
        
        try{
            Account userAccount = GetCustomerDetailsForHDApp_API.GetAccountDetails(accountId);
            GetCustomerDetailsForHDApp_API.cls_data customerProfileData = GetCustomerDetailsForHDApp_API.CreateCustomerDetailsWrap(userAccount, usr);
            profileCompletionData = customerProfileData.profile_completion;
        }
        catch(Exception ex) {
            System.debug(ex.getStackTraceString());
        }
         
        return profileCompletionData;
    }  /* Added on 03 Nov 2020 : Azure ticket 2766 */
    
    /* Wrapper classes for returning reponse */
    public class ResponseWrapper {
        public cls_meta_data meta_data;
        public cls_data data;
    }

    public class cls_meta_data {
        public Integer status_code;
        public String message;
        public String title;
        public String developer_message;   
    }

    public class cls_data {
        public ID user_id;
        public String user_name;
        public ID account_id;
        public ID contact_id;
        public String profile_name;
        public String account_name;
        public String party_id;
        public String nationality;
        public String profile_pic_uri;
        public List<API_CommunityPortalLogin.UnitDetailsWrapper> property_details;
        public String person_pre_name_adjunct;
        public String person_first_name;
        public String person_last_name;
        public String email;
        public GetCustomerDetailsForHDApp_API.cls_profile_completion profile_completion; /* Added on 03 Nov 2020 : Azure ticket 2766 */
    }
}