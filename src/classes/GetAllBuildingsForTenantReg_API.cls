/**********************************************************************************************************************
    Description: This service is used for getting all the buildings for Tenant Registration (Guest)
    =======================================================================================================================
    Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
    -----------------------------------------------------------------------------------------------------------------------
    1.0     |   29-09-2020      | Shubham Suryawanshi | Added the logic for excluding buildings not having units
    ***********************************************************************************************************************/

    @RestResource(urlMapping='/getAllBuildings/*')
    global class GetAllBuildingsForTenantReg_API {

        public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{ 
            1 => 'Success',
            2 => 'Failure',
            3 => 'Wrong Request',
            4 => 'Username is not correct',
            5 => 'Password is not correct',
            6 => 'Something went wrong'
        };

        @HttpPost 
        global static FinalReturnWrapper getProperty() {

            RestRequest r = RestContext.request;
            //System.debug('Request Headers:' + r.headers);
            //System.debug('Request params:' + r.params);

             cls_meta_data objMeta = new cls_meta_data();
            

            List<BuildingWrapper> lstBuildWrapper = new List<BuildingWrapper>();
            List<Location__c> lstLocations = new List<Location__c>();
            Map<Id, List<Booking_Unit__c> > mapBuildingIdToBU = new Map<Id, List<Booking_Unit__c> >();

            for(Booking_Unit__c objBU : [SELECT    Id
                                                  , Unit_Name__c
                                                  , Unit_Active__c
                                                  , Inventory__r.Building_Location__c
                                         FROM Booking_Unit__c
                                         WHERE Inventory__r.Building_Location__c != null
                                             AND ((Unit_Active__c = 'Active'
                                                    AND (Property_Status__c = 'Ready' OR Inventory__r.Property_Status__c = 'Ready'))
                                              OR Dummy_Booking_Unit__c = TRUE)]) {

                if(!mapBuildingIdToBU.containsKey(objBU.Inventory__r.Building_Location__c)) {

                    mapBuildingIdToBU.put(objBU.Inventory__r.Building_Location__c, new List<Booking_Unit__c>{objBU});
                }
                else {
                    mapBuildingIdToBU.get(objBU.Inventory__r.Building_Location__c).add(objBU);
                }

            }
                
                //if(lstLocations.size() > 0) {
            for(Location__c objLoc : [SELECT  Id
                                            , Name
                                            , Building_Name__c
                                            , Location_ID__c
                                            , Country__c
                                            , UAEProp__c
                                      FROM  Location__c
                                      WHERE(NOT Building_Name__c  LIKE '%STORAGE%')
                                      AND Building_Name__c != null
                                      AND UAEProp__c = true   //for fetching only UAE building - 17 Sept'20
                                      AND (Location_Type__c = 'Building' OR Location_Type__c = NULL) ]) {

                if(mapBuildingIdToBU.containsKey(objLoc.id) && mapBuildingIdToBU.get(objLoc.id).size() > 0) {

                    BuildingWrapper objBuildWrap = new BuildingWrapper();
                    objBuildWrap.id = objLoc.id;
                    objBuildWrap.building_name = objLoc.Building_Name__c;
                    objBuildWrap.building_id = Integer.valueOf(objLoc.Location_ID__c);
                    lstBuildWrapper.add(objBuildWrap);  
                }
                                    
            }

            objMeta.message = lstBuildWrapper.size() > 0 ? 'Successful' : 'No building records fetched';
            objMeta.status_code = 1;
            
            //objMeta.is_success = lstBuildWrapper.size() > 0 ? true : false;
            objMeta.title = mapStatusCode.get(1);
            objMeta.developer_message = null;

            cls_data objData = new cls_data();
            objData.buildings = lstBuildWrapper;

            FinalReturnWrapper objReturn = new FinalReturnWrapper();
            objReturn.data = objData;
            objReturn.meta_data = objMeta;
            
            System.debug('objReturn: ' + objReturn);
            
            return objReturn;

        }
        
        global class ReturnWrapper {
            public String status;
            public String message;
            public BuildingWrapper[] buildings;
        }
         
        public class BuildingWrapper {
             public String id;
             //public String name;
             public String building_name;
             public Integer building_id;
             //public String Location_Code; Ruled Out
             //public String Location_Type;
             //public String Country;
        }

        global class FinalReturnWrapper {
            public cls_data data;
            public cls_meta_data meta_data;
        }

        public class cls_data {
            public BuildingWrapper[] buildings;
        }

        public class cls_meta_data {
            //public Boolean is_success {get; set;} Ruled Out
            public String message;
            public Integer status_code;
            public String title;
            public String developer_message;
        }
            

    }