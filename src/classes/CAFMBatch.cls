/*----------------------------------------------------------------------------
Description: Batch to fetch status of CAFM tickets

========================================================================================
    Version | Date(DD-MM-YYYY) | Last Modified By | Comments
-------------------------------------------------------------------------------------------
    1.0     | 25-07-2018       | Lochana Rajput   | Initial draft
----------------------------------------------------------------------------------------------
    2.0     | 26-07-2018       | Lochana Rajput   | Added logic for callout and update FM case
==============================================================================================
*/

public class CAFMBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {
 String query;
 public CAFMBatch() {
  Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Helpdesk').getRecordTypeId();
  query = 'SELECT Id, Task_Id__c from FM_Case__c ';
  query += 'WHERE RecordTypeId =\''+ devRecordTypeId+'\'';
  query += ' AND Task_Id__c != NULL AND Status__c !=\'Closed\'';
  System.debug('===query==='+query);
 }

 public Database.QueryLocator start(Database.BatchableContext BC) {
  return Database.getQueryLocator(query);
 }

public void execute(Database.BatchableContext BC, List<sObject> scope) {
    System.debug('===scope==='+scope);
    try {
        List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
        for(FM_Case__c obj : (List<FM_Case__c>)scope) {
            FM_Case__c objFMCase = makeCallout((Integer)obj.Task_Id__c,obj.Id);
            System.debug('--objFMCase--~~~'+objFMCase);
            if(objFMCase != NULL) {
                lstFMCases.add(objFMCase);
            }
        }
        System.debug('--lstFMCases--~~~'+lstFMCases);
        update lstFMCases;
    }
    catch(Exception excp) {
        System.debug('==CAFMBatch===excp==='+excp);
    }
 }

 public FM_Case__c makeCallout(Integer taskId, String FMCaseId) { 
  String sessionId;
          try {
              System.debug('===taskId==='+taskId);
              System.debug('===taskId==='+FMCaseId);
              //Get session id

             sessionId = getSessionIDfromCAFM(); 
              System.debug('-->>sessionId : '+sessionId);

              schemasDatacontractOrg200407FsiConc_t32.TaskDtoV3 CAFMTaskObj =
                  new schemasDatacontractOrg200407FsiConc_t32.TaskDtoV3();
                wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3  obj = new wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3 ();
              CAFMTaskObj = obj.GetTaskById(sessionId,taskId);
              System.debug('===CAFMTaskObj.LongDescription==='+CAFMTaskObj.LongDescription);
                System.debug('===CAFMTaskObj==='+CAFMTaskObj);
                System.debug('===CAFMTaskObj==='+CAFMTaskObj.Code);
                System.debug('===CAFMTaskObj==='+CAFMTaskObj.State);
                System.debug('===FMCaseId==='+FMCaseId);
              FM_Case__c objFMCase = new FM_Case__c();
              System.debug('-->>0 : ');
              objFMCase.Id = FMCaseId;
              System.debug('-->>1 : ');
              objFMCase.CAFM_Task_Code__c = CAFMTaskObj.Code;
              System.debug('-->>2 : ');
              // objFMCase.Long_Description__c = CAFMTaskObj.LongDescription;
              // objFMCase.Task_Priority__c = CAFMTaskObj.Priority;
              // objFMCase.Short_Description__c = CAFMTaskObj.ShortDescription;
              objFMCase.CAFM_Task_State__c = CAFMTaskObj.State;
              System.debug('-->>3 : ');
              System.debug('--objFMCase--'+objFMCase);
              return objFMCase;
          }
          catch(Exception excp) {
              System.debug('===excp==='+excp);
          }
    return NULL;
 }

@TestVisible
private static String getSessionIDfromCAFM() {
      try{
          wwwFsiCoUkServicesEvolution0409.SecurityService1 obj = new wwwFsiCoUkServicesEvolution0409.SecurityService1();
          obj.timeout_x = 120000;
          System.debug('-->> obj: '+ obj);
          schemasDatacontractOrg200407FsiPlat.AuthenticationDto result = obj.Authenticate(Label.CAFM_Username,Label.CAFM_Password);
          System.debug('-->> result: '+ result);
          if(result != NULL) {
              if(result.OperationResult == 'Succeeded') {
                  System.debug('===result.SessionId=='+result.SessionId);
                  return result.SessionId;
              }
          }

      }
      catch(System.Exception excp) {
          System.debug('=CAFMBatch=excp==SESSION ID='+excp);
          // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,excp.getMessage() + excp.getLineNumber()));
      }
     return NULL;
}
public void finish(Database.BatchableContext BC) {

}

}