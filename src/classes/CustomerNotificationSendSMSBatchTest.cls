/******************************************************************************
* Description - Test class developed for CustomerNotificationBatch
*
* Version            Date            Author                    Description
* 1.0                11/12/17        Ashish Agarwal            Initial Draft
********************************************************************************/

@isTest
private class CustomerNotificationSendSMSBatchTest {

    static testMethod void myUnitTestOne() {

        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Tenant_Registration';
        fmCaseObj.Booking_Unit__c=buIns.id;
        fmCaseObj.Account__c=acctIns.id;
        //fmCaseObj.Approving_Authorities__c='FM Manager';
        fmCaseObj.Nationality__c ='Pakistani';
        fmCaseObj.Mobile_no__c = '123456789';
        fmCaseObj.Outstanding_service_charges__c = '0';
        fmCaseObj.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId();
        insert fmCaseObj;

        SMS_History__c objSMS = new SMS_History__c();
        objSMS.Message__c = 'Test';
        objSMS.Is_SMS_Sent__c = false ;
        objSMS.FM_Case__c = fmCaseObj.Id;
        insert objSMS ;

        test.startTest();
            Database.executeBatch( new CustomerNotificationSendSMSBatch() );
        test.stopTest();

    }

    static testMethod void myUnitTestTwo() {
        SMS_History__c objSMS = new SMS_History__c();
        objSMS.Message__c = 'Test';
        objSMS.Is_SMS_Sent__c = false ;
        insert objSMS ;
        List<SMS_History__c> lstSMSHistory = new List<SMS_History__c>();
        lstSMSHistory.add(objSMS);

        test.startTest();
            Database.executeBatch( new CustomerNotificationSendSMSBatch(lstSMSHistory) );
        test.stopTest();

    }
}