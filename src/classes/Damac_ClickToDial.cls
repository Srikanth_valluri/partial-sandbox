global class Damac_ClickToDial
{
    webservice static String getMobileEncrypted (String mobileNumber) 
    {    
         system.debug('Mobile Number2: ' + mobileNumber);
         String decryptedNumber = UtilityHelperCls.decryptMobile(mobileNumber);
         system.debug('decryptedNumber: ' + decryptedNumber);
         String retValue = Damac_PhoneEncrypt.encryptPhoneNumber (decryptedNumber);
         system.debug('retValue: ' + retValue);
         return retValue;
    }
    
    public String mobileNumber { get; set; }
    public String userExtension { get; set; }
    public void encryptMobilePhone () {
        ID inqId = apexpages.currentpage().getparameters().get('id');
        Inquiry__c inq = new Inquiry__c ();
        inq = [SELECT Mobile_Phone_Encrypt__c FROM Inquiry__c WHERE ID =: inqId LIMIT 1];
        String decryptedNumber = UtilityHelperCls.decryptMobile(inq.Mobile_Phone_Encrypt__c);
        mobileNumber = Damac_PhoneEncrypt.encryptPhoneNumber (decryptedNumber);
        userExtension = [SELECT Extension FROM User WHERE ID =: UserInfo.getUserID ()].Extension;        
    }
    
    @AuraEnabled
    public static String encryptMobilePhoneAura(String mobileNumber){
        system.debug('Mobile Number: ' + mobileNumber);
        return getMobileEncrypted(mobileNumber);
    }
    
}