@isTest
public class DL_ProfileCompletionNotifyBatchTest {
    @isTest (seeAllData = true)
    static void  testProfileIncompleteNotifyBatch(){
        List<User> owners = [SELECT id, name, profileId, profile.name, accountId 
                       FROM User WHERE profile.name = 'Customer Community Login User(Use this)' 
                       AND IsActive = TRUE AND AccountId != NULL LIMIT 2];
        
        List<String> ownerAccIds = new List<String>();
        for(User owner : owners) {
            String ownerAccountId = String.valueOf(owner.accountId);
            ownerAccIds.add(ownerAccountId);                                  
        }
        
        Test.StartTest();
        DL_ProfileCompletionNotificationBatch objBatch = new DL_ProfileCompletionNotificationBatch(ownerAccIds);
        database.executebatch(objBatch, 50);
        Test.StopTest();
    }
    
    @isTest
    static void testBatchScheduler() {
        Test.StartTest();
        DL_ProfileCompletionNotificationSched sch = new DL_ProfileCompletionNotificationSched();
        String schCron = '0 0 */3 * * ?'; // schedule interval time
        System.schedule('DL profile incomplete push notification scheduler', schCron, sch);
        Test.stopTest();
    }
    
    @isTest
    static void testProfileCompletionNotifyUtil() {
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            Test.startTest();
            List<Account> lstAccount = new List<Account>();
            Id RecTypepersonal = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            
            Account person_1 = new Account(FirstName = 'p1',LastName ='test',recordtypeid = RecTypepersonal);
            lstAccount.add(person_1);
            Account person_2 = new Account(FirstName = 'p2',LastName ='test',recordtypeid = RecTypepersonal);
            person_2.Is_DAMAC_Living_App_user__c = true;
            person_2.Passport_Number__pc = 'PN1234567890';
            person_2.Passport_Expiry_Date__pc = Date.today().addMonths(3);
            // person_2.passportDocURL = 'https://testcdn.com/u/3456?docid=98765';
            person_2.Address_Line_1__pc = 'TEST Address';
            person_2.City__pc = 'Dubai';
            person_2.Country__pc = 'United Arab Emirates';
            person_2.Mobile_Phone_Encrypt__pc = '9876543210';
            person_2.Mobile_Phone_Encrypt_2__pc = '8967452301';
            person_2.Email__pc = 'test@test.com';
            person_2.Email_2__pc = 'test.alt@test.com';
            // person_2.WhatsApp_Number__c = '09718967452301';
            lstAccount.add(person_2);
            insert lstAccount;
            
            SR_Attachments__c passportDoc = new SR_Attachments__c();
            passportDoc.name = 'Customer Passport 2020-11-04 00:00:00';
            passportDoc.Attachment_URL__c = 'https://testcdn.com/u/3456?docid=98765';
            passportDoc.Account__c = person_2.id;
            insert passportDoc;
            
            person_1.WhatsApp_Number__c = '09718967452301';
            person_2.WhatsApp_Number__c = '09718967452301';
            update lstAccount;
            
            Test.stopTest();
        }
    }
}