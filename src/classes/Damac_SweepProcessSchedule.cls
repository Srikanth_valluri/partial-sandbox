global class Damac_SweepProcessSchedule implements Schedulable{
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new Damac_SweepProcess (), 1);
    }

}