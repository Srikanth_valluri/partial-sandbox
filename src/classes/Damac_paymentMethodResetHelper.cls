public class Damac_paymentMethodResetHelper {
    
    public static void resetTimeOut () {
        
        List <String> dayOfWeeks = new List <String> {'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday'};
        
        Datetime currentDateTime = System.now();
        String dayOfWeek = currentDateTime.format('EEEE');
        Integer currentHour = currentDateTime.Hour ();
        System.debug (dayOfWeek+'---'+currentHour );
                
        
        Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
        Boolean dataChanged = false;
        for (Payment_Method_Mapping__mdt obj : [SELECT DeveloperName, MasterLabel, Payment_Method_Type__c, Time_Out_Duration__c 
                                                    FROM Payment_Method_Mapping__mdt 
                                                WHERE Payment_Method_Type__c != null]) {
        
            if (Test.isRunningTest ()) {
                dayOfWeek = 'Monday';
                currentHour = 10;
            }
            if (dayOfWeeks.contains (dayOfWeek) && currentHour == 10) {
                if (obj.Payment_Method_Type__c == 'Cash' 
                    || obj.Payment_Method_Type__c == 'Cheque' 
                    || obj.Payment_method_Type__c == 'Credit_Card_Machine'
                    || obj.Payment_method_Type__c == 'Damac_Website'
                    || obj.Payment_method_Type__c == 'Online_Payment') {
                    
                    dataChanged = true;
                    mdContainer.addMetadata(createUpdateMetadata(obj, 2));
                }
                
                if (obj.Payment_Method_Type__c == 'Wire_Transfer') {
                    mdContainer.addMetadata(createUpdateMetadata(obj, 24));
                    dataChanged = true;
                }
                    
            }  
            if (Test.isRunningTest ()) {
                dayOfWeek = 'Monday';
                currentHour = 20;
            }      
            if (dayOfWeeks.contains (dayOfWeek) && currentHour == 20 && dayOfWeek != 'Thursday') {
                if (obj.Payment_Method_Type__c == 'Cash' 
                    || obj.Payment_Method_Type__c == 'Cheque' 
                    || obj.Payment_method_Type__c == 'Credit_Card_Machine'
                    || obj.Payment_method_Type__c == 'Damac_Website') {
                    
                    dataChanged = true;
                    mdContainer.addMetadata(createUpdateMetadata(obj, 15));
                }
                
            }
            if (Test.isRunningTest ()) {
                dayOfWeek = 'Thursday';
                currentHour = 20;
            }
            if (currentHour == 20 && dayOfWeek == 'Thursday') {
                if (obj.Payment_Method_Type__c == 'Cash' 
                    || obj.Payment_Method_Type__c == 'Cheque' 
                    || obj.Payment_method_Type__c == 'Credit_Card_Machine'
                    || obj.Payment_method_Type__c == 'Damac_Website'
                    || obj.Payment_method_Type__c == 'Wire_Transfer') {
                    
                    dataChanged = true;
                    mdContainer.addMetadata(createUpdateMetadata(obj, 65));
                }
            }
        }
        
        ID jobId = null;
        if (dataChanged && !Test.isRunningTest ()) {
            jobId = Metadata.Operations.enqueueDeployment(mdContainer, null);
            system.debug('jobId***'+jobId); 
            
        }        
    }
    
    public static Metadata.CustomMetadata createUpdateMetadata(Payment_Method_Mapping__mdt objMetadata, Integer value){
        Metadata.CustomMetadata metadataRec =  new Metadata.CustomMetadata();
        metadataRec.fullName = 'Payment_Method_Mapping__mdt.'+objMetadata.DeveloperName;
        metadataRec.label = objMetadata.MasterLabel;
        
        Metadata.CustomMetadataValue customFieldtoUpdate = new Metadata.CustomMetadataValue();
        customFieldtoUpdate.field = 'Time_out_Duration__c';
        customFieldtoUpdate.value = value;
        metadataRec.values.add(customFieldtoUpdate);
        return metadataRec;
    }
}