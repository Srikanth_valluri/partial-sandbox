@isTest
private class FmcGatePassRequestControllerTest {

    public static testmethod void createFMCase() {
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account customerAccount = TestDataFactoryFM.createAccount();
        insert customerAccount;
        CustomerCommunityUtils.customerAccountId = customerAccount.Id;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(customerAccount,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(customerAccount,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        PageReference myVfPage = Page.FMGatePassProcessPage;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Account__c = customerAccount.id;
        Test.setCurrentPage(myVfPage);
        //Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        ApexPages.currentPage().getParameters().put('AccountId',customerAccount.Id);
        ApexPages.currentPage().getParameters().put('unitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Gate_Pass_Request');
        ApexPages.currentPage().getParameters().put('view','GatePassRequest');
        //ApexPages.currentPage().getParameters().put('Id',Opp.Id);
        FmcGatePassRequestController obj = new FmcGatePassRequestController();
        obj.strSelectedUnit = buIns.Id;
        obj.selectUnit();
        obj.objFMCase=fmCaseObj;
        obj.saveAsDraft();
        obj = new FmcGatePassRequestController();
    }

    public static testmethod void submitFMCase(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account customerAccount = TestDataFactoryFM.createAccount();
        insert customerAccount;
        CustomerCommunityUtils.customerAccountId = customerAccount.Id;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(customerAccount,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(customerAccount,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.fm_role__c ='FM Manager';
        insert fmUser;

        PageReference myVfPage = Page.FMGatePassProcessPage;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Email__c = 'test@test.com';
        fmCaseObj.Request_Type_DeveloperName__c='Gate_Pass_Request';
        fmCaseObj.Booking_Unit__c = buIns.Id;
        insert fmCaseObj;
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',customerAccount.Id);
        ApexPages.currentPage().getParameters().put('id', fmCaseObj.Id);
        ApexPages.currentPage().getParameters().put('unitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Gate_Pass_Request');
        ApexPages.currentPage().getParameters().put('view','GatePassRequest');
        FmcGatePassRequestController obj = new FmcGatePassRequestController();
        obj.strSelectedUnit = buIns.Id;
        obj.selectUnit();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';

        Test.startTest();
            obj.createCaseShowUploadDoc();
            //obj.uploadDocument();
            obj.strFMCaseId = fmCaseObj.id;
            obj.submitRequest();
            //obj.returnBackToCasePage();
        Test.stoptest();
    }

    public static testmethod void testInsertAttachment() {
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account customerAccount = TestDataFactoryFM.createAccount();
        insert customerAccount;
        CustomerCommunityUtils.customerAccountId = customerAccount.Id;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(customerAccount,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(customerAccount,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        PageReference myVfPage = Page.FMGatePassProcessPage;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Gate_Pass_Request';
        fmCaseObj.Booking_Unit__c = buIns.Id;
        insert fmCaseObj;
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',customerAccount.Id);
        ApexPages.currentPage().getParameters().put('id', fmCaseObj.Id);
        ApexPages.currentPage().getParameters().put('unitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Gate_Pass_Request');
        ApexPages.currentPage().getParameters().put('view','GatePassRequest');
        FmcGatePassRequestController obj = new FmcGatePassRequestController();
        obj.strSelectedUnit = buIns.Id;
        obj.selectUnit();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';

        Test.startTest();
            obj.strFMCaseId = fmCaseObj.id;
            obj.objFMCase = fmCaseObj;
            obj.uploadDocument();
            obj.submitRequest();
            //obj.returnBackToCasePage();
        Test.stoptest();
    }

    public static testmethod void createFMCaseExistingCase() {
        Id RecTypeFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Work Permit').getRecordTypeId();
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account customerAccount = TestDataFactoryFM.createAccount();
        insert customerAccount;
        CustomerCommunityUtils.customerAccountId = customerAccount.Id;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(customerAccount,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(customerAccount,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        PageReference myVfPage = Page.FMGatePassProcessPage;
        FM_Case__c fmCaseIns=new Fm_Case__c();
        fmCaseIns.recordtypeid=RecTypeFMCase;
        fmCaseIns.Account__c=CustomerCommunityUtils.customerAccountId;
        fmCaseIns.Booking_Unit__c=buIns.id;
        fmCaseIns.status__c='Closed';
        fmCaseIns.Approval_Status__c='Approved';
        //fmCaseIns.Request_Type__c='Work Permit';
        fmCaseIns.Permit_To_Work_For__c='Hot Works;Confined Spaces';
        insert fmCaseIns;
        //Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',customerAccount.Id);
        ApexPages.currentPage().getParameters().put('unitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Gate_Pass_Request');
        ApexPages.currentPage().getParameters().put('view','GatePassRequest');
        //ApexPages.currentPage().getParameters().put('Id',Opp.Id);
        FmcGatePassRequestController obj = new FmcGatePassRequestController();
        obj.strSelectedUnit = buIns.Id;
        obj.selectUnit();
        obj.strDocumentBody = 'ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName = 'Test_Document_Work_Permit.htm';
        //obj.objFMCase=fmCaseObj;
        Test.startTest();
            obj.strFMCaseId = fmCaseObj.id;
            //obj.uploadDocument();
            obj.notifyContractorConsultant();
            obj.saveAsDraft();
            obj = new FmcGatePassRequestController();
        Test.stoptest();
    }
}