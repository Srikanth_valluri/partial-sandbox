@isTest
public class AOPT21stDayNotificationTest {
    
    public static testmethod void testAOPT21stDayNotification() {
        Case objCase = new Case();
        objCase.Account_Email__c = 'test@test.com';
        insert objCase;
        
        AOPT21stDayNotification.checkIfAddendumUploaded(new List<Case>{objCase});
    }
}