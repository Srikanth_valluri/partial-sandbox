@isTest
private class AP_AddNewActivityControllerTest
{
  @isTest
  static void itShould()
  {
    Account acc = new Account ();
        acc.Name = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        acc.Vendor_ID__c = '969696';
        insert acc;        
        
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName= 'Test';
        con.Email = 'test@test.com';
        con.AccountId = acc.id;
        con.Agent_Representative__c = true;
        insert con;
        
        Agent_Site__c agency = new Agent_Site__c();
        agency.Name = 'UAE';
        agency.Agency__c = acc.id;
        insert agency;       
        
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);        
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        sr.Token_Amount_AED__c = 40000;
        sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Change Agent').getRecordTypeId();
        sr.Booking_Wizard_Level__c = null;
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE';
        sr.Mode_of_Payment__c = 'Cash';
        sr.agency__c = acc.id;
        //sr.Booking_from_New_Wizard__c = True;
        insert sr;
        system.debug('>>'+sr.id);
        
        NSIBPM__Step_Template__c stpTemplate =  InitializeSRDataTest.createStepTemplate('Deal','MANAGER_APPROVAL');
        insert stptemplate;
        
        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
        Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);
        
        
        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id,stepStatuses.values()[0].id,stptemplate.id);
        //insert stp;
        
        Campaign__c camp = new Campaign__c();
        camp.End_Date__c = system.today().addmonths(10);
        camp.Marketing_End_Date__c = system.today().addmonths(10);
        camp.Marketing_Start_Date__c = system.today().addmonths(-10);
        camp.Start_Date__c =  system.today().addmonths(-10);
        insert camp;
        
        Inquiry__c inq = new Inquiry__c ();
        inq.Activity_Counter__c =101;
        inq.Inquiry_Status__c='Active';
        inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        inq.campaign__c = camp.id;
        insert inq;
        
        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk[0].Deal_SR__c = sr.id;
        lstbk[0].Inquiry_Account_Id__c = acc.id;       
        insert lstbk;
        
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        loc.Property_ID__c = '123';
        insert loc;    
        
        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c  = 1;
        newProperty.Property_Code__c    = 'VIR' ;
        newProperty.Property_Name__c    = 'VIRIDIS @ AKOYA OXYGEN' ;
        newProperty.District__c = 'AL YUFRAH 2' ;
        newProperty.AR_Transaction_Type__c  = 'INV VIR' ;
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR' ;
        newProperty.Brokerage_Distribution_Set__c   = '11600' ;
        newProperty.Sales_Commission_Dist_Set__c    = '11601' ;
        newProperty.Currency_Of_Sale__c = 'AED' ;
        newProperty.Signature_Col_Customer_Stmt__c  = 'Front Line Investment Management Co. LLC' ;
        newProperty.EOI_Enabled__c = true;
        insert newProperty;   
                
        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        lstInv[0].building_location__c = loc.id;
        lstInv[0].property_id__c = newProperty.id;
        
        insert lstInv;
        
        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account a = new Account();
        a.LastName= 'Test Account';
        a.RecordTypeId = accountRTId;        
        insert a;
        
        
        buyer__c b = new buyer__c();
        b.Buyer_Type__c =  'Individual';
        b.Address_Line_1__c =  'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.City__c = 'Dubai' ;       
        
        b.Account__c = acc.id;
        b.dob__c = system.today().addyears(-30);
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname' ;
        b.Last_Name__c =  'lastname';
        b.Nationality__c = 'Indian' ;
        b.Passport_Expiry__c = system.today().addyears(20) ;
        b.Passport_Number__c = 'J0565556' ;
        b.Phone__c = '569098767' ;
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b.Place_of_Issue__c =  'India';
        b.Title__c = 'Mr';
        b.booking__c = lstbk[0].id;
        b.Primary_Buyer__c =true;
        b.Unique_Key__c = lstbk[0].id+''+acc.id;
              
        insert b;
                               
        
        
        Currency_Rate__c rate = new Currency_Rate__c();
        rate.To_Currency__c = 'AED';
        rate.Conversion_Date__c = system.today();
        rate.conversion_rate__c = 1;
        rate.From_Currency__c = 'AED';        
        insert rate;
        
      
        
        Inventory__c invent = new Inventory__c();
        invent.CM_Price_Per_Sqft__c = 20;
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1' ;
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.ID;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Released';
        invent.special_price__c = 1000000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234'; 
        invent.Floor_Package_ID__c = ''; 
        invent.building_location__c = loc.id;
        invent.property_id__c = newProperty.id;
        insert invent;
        
        
        Unit_Assignment__c uAObj = new Unit_Assignment__c();
        uAObj.Start_Date__c = Date.parse('11/12/17');
        uAObj.End_Date__c =  Date.parse('11/12/18');
        uAObj.Unit_Assignment_Name__c = 'UA-Test';
        uAObj.Expired__c = false;
        uAObj.Reason_For_Unit_Assignment__c = 'Reason of UA';
        uaObj.Status__c = 'PC Submitted';
        //insert uAObj;
        
        Inventory__c invvent1 = new Inventory__c();
        invvent1.CM_Price_Per_Sqft__c = 20;
        invvent1.Unit_Assignment__c = uAobj.id;
        invvent1.Inventory_ID__c = '2';
        invvent1.Building_ID__c = '2';
        invvent1.Floor_ID__c = '2';
        invvent1.Marketing_Name__c = 'Damac Heights2';
        invvent1.Address_Id__c = '2' ;
        invvent1.EOI__C = NULL;
        invvent1.Tagged_to_EOI__c = false;
        invvent1.Is_Assigned__c = false;
        invvent1.Property_ID__c = newProperty.ID;
        invvent1.Property__c = newProperty.Id;
        invvent1.Status__c = 'Released';
        invvent1.special_price__c = 1000000;
        invvent1.CurrencyISOCode = 'AED';
        invvent1.Property_ID__c = '12534'; 
        invvent1.Floor_Package_ID__c = ''; 
        invvent1.building_location__c = loc.id;
        invvent1.property_id__c = newProperty.id;
        invvent1.Tagged_To_Unit_Assignment__c = true;
        insert invvent1;        
        
        Campaign__c c = new Campaign__c();
        c.Start_Date__c = system.today().addDays(-1);
        c.End_Date__c = system.today().addDays(1);
        c.Marketing_Start_Date__c = system.today().addDays(-1);
        c.Marketing_End_Date__c = system.today().addDays(1);     
        c.RecordTypeID = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId();   
        insert c; 
        
        Available_options__c opt = new Available_options__c();
        opt.type_of_option__c = 'Option';
        opt.Promotion_Option__c = 'test';
        opt.discount_type__c = 'psf';
        opt.Price_Value__c = 1;
        opt.Percent_Value__c = 5;
        insert opt;
        
        
        
        Payment_Plan__c pp = new Payment_Plan__c();        
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today(); 
        pp.Building_Location__c =  loc.id;  
        pp.term_id__c = '1234';   
        insert pp;        
        
                                        
        Payment_Terms__c pt = new Payment_Terms__c();
        pt.Payment_Plan__c = pp.id;        
        pt.Percent_Value__c = '5';       
        insert pt;
        
        selected_units__c sel = new selected_units__c();
        sel.service_request__c = sr.id;
        sel.Payment_Plan__c = pp.ID;
        sel.inventory__c = invent.id;
        sel.Requested_Price_AED__c = 1000000;
        sel.Selected_Campaign__c = opt.id;
        insert sel;
        
        selected_units__c sel1 = new selected_units__c();
        sel1.service_request__c = sr.id;
        sel1.inventory__c = invvent1.id;
        sel1.Payment_Plan__c = pp.ID;
        sel1.Requested_Price_AED__c = 1000000;
        sel1.Selected_Campaign__c = opt.id;
        insert sel1;
        
        Available_options__c opt1 = new Available_options__c();
        opt1.type_of_option__c = 'Scheme';
        opt1.Promotion_Option__c = 'test';
        opt1.discount_type__c = 'psf';
        opt1.Price_Value__c = 1;
        opt1.Selected_Units__c = sel.id;
        insert opt1;
        
        DAMAC_Constants.skip_BookingUnitTrigger = true;
        List<Booking_Unit__c> BUList = new List<Booking_Unit__c>();
        Booking_Unit__c bu1 = new Booking_Unit__c();
        bu1.Booking__c = lstbk[0].id;
        bu1.Payment_Method__c = 'Cash';
        bu1.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu1.Primary_Buyer_s_Name__c = 'testNSI';
        bu1.Primary_Buyer_s_Nationality__c = 'Russia';
        bu1.Inventory__c = invent.id;
        bu1.Registration_ID__c = '1234'; 
        bu1.Payment_Plan_Id__c = pp.id;     
    bu1.Requested_Price_AED__c = 500;     
        BUList.add(bu1);
        //insert bu1;
        
        
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = invent.id;
        bu.Registration_ID__c = 'test123'; 
        bu.Payment_Plan_Id__c = pp.id; 
    bu.Requested_Price_AED__c = 500;
        BUList.add(bu);     
        //insert bu;
        
        insert BUList;
    system.debug('BUList'+BUList);
        
                           
        Assigned_PC__c pc = new Assigned_PC__c();
        pc.Start_Date__c = system.today().addDays(-1);
        pc.End_Date__c = system.today().addDays(1);
        pc.Campaign__c = c.id;
        pc.User__c = UserInfo.getUserId();
        insert pc;

    apexpages.currentpage().getparameters().put('id' , sr.id);
    apexpages.currentpage().getparameters().put('inquiryId' , inq.id);
    apexpages.currentpage().getparameters().put('pageName' , 'AP_CILDetailPage');

    AP_AddNewActivityController objController = new AP_AddNewActivityController();
    //objController.getOutcomeLst();
    objController.activityType3 = 'Call - Outbound';
    //List<SelectOption> lstOutcome = objController.getOutcomeLst();
    objController.activityOutcome();
    objController.activityType3 = 'Call-Inbound';
    objController.activityOutcome();
    objController.activityType3 = 'SMS-Inbound';
    objController.activityOutcome();
    objController.activityType3 = 'SMS-Outbound';
    objController.activityOutcome();
    objController.activityType3 = 'Meeting at Office';
    objController.activityOutcome();
    objController.activityType3 = 'Meeting Outside Office';
    objController.activityOutcome();
    objController.activityType3 = 'Meeting-Site Visit';
    objController.activityOutcome();
    objController.activityType3 = 'Email Delivered';
    objController.activityOutcome();
    objController.activityType3 = 'Email-Outbound';
    objController.activityOutcome();
    //objController.OutcomeLst = lstOutcome;
    //objController.getactivityTypeLst();
    List<SelectOption> lstActType = objController.getactivityTypeLst();
    objController.activityTypeLst = lstActType;

    objController.addNew();
    objController.closeTask();

    list<task> lstTask = new list<task>();
    lstTask = [SELECT id, WhatId,Priority
           FROM task
           WHERE WhatId =: inq.id];
    system.debug('lstTask'+lstTask);
    //system.assertEquals(lstTask[0].Priority,'Normal');

  }
}