public without sharing class GenericRecordDetailsDataProvider implements DataProvider {

    public DataDisplayConfig getData(DataDisplayConfig config) {
        List<Sobject> lstRecord = new List<Sobject>();
        if (String.isNotBlank(config.recordId)) {
            String fields = '';

            Schema.DescribeSObjectResult describeObject =
                    Schema.getGlobalDescribe().get(config.objectName.toLowerCase()).getDescribe();

            if (config.mapRecordTypeToFieldList != NULL) {
                Sobject record = Database.query(
                    'SELECT RecordTypeId FROM ' + config.objectName
                    + ' WHERE Id = \'' + config.recordId + '\''
                );
                List<Map<String, String>> lstField = config.mapRecordTypeToFieldList.get(
                    describeObject.getRecordTypeInfosById().get((Id)record.get('RecordTypeId')).getName()
                );
                if(lstField == NULL){
                    lstField = config.mapRecordTypeToFieldList.get('Master'); 
                }
                system.debug('>>>>>fields'+lstField);
                config.fieldList = lstField;
                config.detailFieldList = lstField;
            }

            if (config.fieldList != NULL) {
                for (Map<String,String> fieldMap : config.fieldList) {
                    String fieldName = fieldMap.get(DataRendererUtils.FIELD_NAME);
                    if (String.isNotBlank(fieldName) && !fields.containsIgnoreCase(fieldName + ',')) {
                        fields += fieldName + ',';
                    }
                }
            }

            if (config.detailFieldList != NULL) {
                for (Map<String,String> fieldMap : config.detailFieldList) {
                    String fieldName = fieldMap.get(DataRendererUtils.FIELD_NAME);
                    if (String.isNotBlank(fieldName) && !fields.containsIgnoreCase(fieldName + ',')) {
                        fields += fieldName + ',';
                    }
                }
            }

            fields = fields.removeEnd(',');
            system.debug('>>>>>>>>>Query'+' SELECT ' + fields + ' FROM ' + config.objectName
                + ' WHERE Id = \'' + config.recordId + '\'');
            lstRecord = Database.query(
                ' SELECT ' + fields + ' FROM ' + config.objectName
                + ' WHERE Id = \'' + config.recordId + '\''
            );
        }

        config.dataList = DataRendererUtils.wrapData(config, lstRecord);
        return config;
    }

}