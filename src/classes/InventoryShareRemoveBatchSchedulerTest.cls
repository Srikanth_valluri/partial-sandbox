/**************************************************************************************************
* Name               : InventoryShareRemoveBatchSchedulerTest
* Description        : Test class for InventoryShareRemoveBatchScheduler class.
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst      11/08/2020      Initial Draft.
**************************************************************************************************/
@isTest
private class InventoryShareRemoveBatchSchedulerTest {

    static testMethod void testScheduler() {
         Account acc = new Account ();
        acc.LastName = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '767676';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        insert acc;

                
        DAMAC_Central_Push_Notifications__c obj = new DAMAC_Central_Push_Notifications__c();
        obj.Email__c = 'test@test.com';
        obj.Password__c = '1232112';
        obj.device_source__c = 'test';
        obj.device_os_version__c = 'test';
        obj.app_version__c = '1.2';
        obj.device_model__c = 'test';
        obj.Api_Token__c = 'test';
        obj.Project_connect_API_Token__c = 'test';
        obj.app_id__c = 1;
        obj.is_authorization_required__c = false;
        insert obj;
        
        Unit_Assignment__c uAObj = new Unit_Assignment__c();
         uAObj.Start_Date__c = Date.parse('11/12/17');
         uAObj.End_Date__c =  Date.parse('11/12/18');
         uAObj.Unit_Assignment_Name__c = 'Test US1';
         uAObj.Reason_For_Unit_Assignment__c = 'Test reason';
         uAObj.Active__c = false;
         insert uAObj;

        Agent_Site__c agency = new Agent_Site__c();
        agency.Name = 'UAE';
        agency.Agency__c = acc.id;
        insert agency;


        Deal_Exception_Request__c der = new Deal_Exception_Request__c();
        der.RM__c = UserInfo.getUserId();
        der.Inventory_Request_Filters__c = 'Product:90210 Boutique Villas;UnitType:FURNISHED APARTMENT,HOTEL,HOTEL APARTMENTS';
        insert der;
        List<Id> derIdList = new List<Id>();
        derIdList.add(der.Id);

        Inquiry__c inq = new Inquiry__c ();
        inq.Activity_Counter__c =101;
        inq.Inquiry_Status__c='Active';
        inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        inq.ownerId = userinfo.getUserId();
        inq.Promoter_Name__c = 'Test';
        inq.Telesales_Executive__c = UserInfo.getUserId();
        insert inq;
        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        loc.property_id__c = '123';
        insert loc;

        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c = 1;
        newProperty.Property_Code__c = 'VIR';
        newProperty.Property_Name__c = 'VIRIDIS @ AKOYA OXYGEN';
        newProperty.District__c = 'AL YUFRAH 2';
        newProperty.AR_Transaction_Type__c = 'INV VIR';
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR';
        newProperty.Brokerage_Distribution_Set__c = '11600';
        newProperty.Sales_Commission_Dist_Set__c = '11601';
        newProperty.Currency_Of_Sale__c = 'AED';
        newProperty.Signature_Col_Customer_Stmt__c = 'Front Line Investment Management Co. LLC';
        newProperty.EOI_Enabled__c = true;
        insert newProperty;

        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.Id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.Id;
        lstInv[0].building_location__c = loc.Id;
        lstInv[0].property_id__c = newProperty.Id;
        lstInv[0].status__c = 'Released';
        lstInv[0].Unit_Assignment__c = uAObj.Id;
        lstInv[0].Tagged_To_Unit_Assignment__c = true;
        insert lstInv;
        
        Inventory_User__c invUser = new Inventory_User__c();
        invUser.Inventory__c = lstInv[0].Id;
        invUser.User__c = userInfo.getUserId();
        insert invUser;
        
        Deal_Exception_Unit__c deUnit = new Deal_Exception_Unit__c();
        deUnit.Deal_Exception_Request__c = der.Id;
        deUnit.Inventory__c = lstInv[0].Id;
        deUnit.Inventory_Shared_to_RM__c = true;
        deUnit.Inventory_Initial_Status__c = 'Restricted';
        deUnit.Inventory_Share_Time__c = system.now().addDays(-30);
        insert deUnit;
        
        
        Test.startTest();
            InventoryShareRemoveBatchScheduler ascsObject = new InventoryShareRemoveBatchScheduler();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test Scheduler', sch, ascsObject); 
            
            PageReference pageRefrence=Page.Inventory_Request_Cancellation;
            Test.setCurrentPage(pageRefrence);
            apexpages.currentpage().getparameters().put('Id', der.Id);
            InventoryShareRemovalController obj2 = new InventoryShareRemovalController();
            obj2.cancelInventoryRequest();
            //obj2.sendCancelNotificationToRM(der.Id);
        Test.stopTest();
    }
}// End of class.