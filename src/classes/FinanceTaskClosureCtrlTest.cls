@isTest
public class FinanceTaskClosureCtrlTest {
    
    @isTest
    public static void testInit() {
        
        // create Case
        Account objAcc = new Account(Name = 'test');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        Id caseRecTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Overdue Rebate/Discount').getRecordTypeId();
        Case objCase = new Case(Status = 'In Progress'
                                , AccountId = objAcc.Id
                                , recordTypeid = caseRecTypeId);
        insert objCase;                     
        
        Task objTask = new Task(Subject = 'Apply Rebate'
                                , whatId = objCase.Id
                                , Assigned_User__c =    'Finance'
                                , Status = 'In Progress'
                                , Process_name__c = 'Overdue Rebate/Discount'
                                , CurrencyIsoCode = 'AED'
                                , ActivityDate = system.today().addDays(3)
                                , Priority = 'Normal');
        insert objTask;
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        FinanceTaskClosureCtrl ctrl = new FinanceTaskClosureCtrl(sc);
        PageReference pageRef = Page.FinanceTaskClosurePage;
        Test.setCurrentPage(pageRef);
        Test.startTest();
        ctrl.init();
        Test.stopTest();
    }
    
    @isTest
    public static void testCloseFinanceTask() {
        
        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        objAcc.Mobile__c ='1234567890';
        insert objAcc;
                
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Id  bildingLocRecTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.recordTypeId = bildingLocRecTypeId;
        loc.Location_Name__c = 'test';
        loc.Address__c = 'test';
        loc.Contact__c = 'test';
        loc.Timings__c = 'test';
        loc.Map_URL__c = 'https://test';
        insert loc;
        
        Property__c objProperty = TestDataFactory_CRM.createProperty();
        insert objProperty;
        
        Inventory__c objInventory = TestDataFactory_CRM.createInventory(objProperty.Id);
        objInventory.Building_Location__c = loc.id;
        objInventory.property_Name__c = 'akoya oxygen';
        objInventory.Master_Community_EN__c = 'akoya oxygen';
        insert objInventory;
        
        Booking_Unit_Active_Status__c buStatusObj = new Booking_Unit_Active_Status__c();
        buStatusObj.Name = 'Agreement Rejected By Sales Admin';
        buStatusObj.Status_Value__c  = 'Agreement Rejected By Sales Admin';
        insert buStatusObj;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Inventory__c = objInventory.Id;
        objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Allow_Overdue__c  = true;
        objBookingUnit.Registration_DateTime__c = system.today().addDays(-200);
        objBookingUnit.Requested_Price_AED__c = 90000;
        objBookingUnit.Anticipated_Completion_Date__c = Date.newInstance(2020, 12, 31 );
        insert objBookingUnit;
        
        Id caseRecTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Overdue Rebate/Discount').getRecordTypeId();
        Case objCase = new Case(Status = 'In Progress'
                                , AccountId = objAcc.Id
                                , recordTypeid = caseRecTypeId
                                , Is_Overdue_Rebate_Letter_Genrated__c= true
                                , Paid_Amount__c = 100
                                , Overdue_Rebate_Letter_Date__c = system.today()
                                , Account_Email__c = 'test@t.com'
                                , Phone_Number__c = '234234234'
                                , Booking_Unit__c = objBookingUnit.iD);
        insert objCase;                     
        
        Task objTask = new Task(Subject = 'Apply Rebate'
                                , whatId = objCase.Id
                                , Assigned_User__c =    'Finance'
                                , Status = 'In Progress'
                                , Process_name__c = 'Overdue Rebate/Discount'
                                , CurrencyIsoCode = 'AED'
                                , ActivityDate = system.today().addDays(3)
                                , Priority = 'Normal');
        insert objTask;
        
       
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        FinanceTaskClosureCtrl ctrl = new FinanceTaskClosureCtrl(sc);
        PageReference pageRef = Page.FinanceTaskClosurePage;
        Test.setCurrentPage(pageRef);
        Test.startTest();
        Test.setMock( HttpCalloutMock.class, new OfferCalloutHttpMock());
        ctrl.init();
        ctrl.closeFinanceTask();
        Test.stopTest();
        
        system.debug('exception in test = ' + apexPages.getMessages());
        //system.assert(apexPages.getMessages().size() == 0);
    }
}