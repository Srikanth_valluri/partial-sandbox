/****************************************************************************************************
* Name          : SendEmailToResourceContact                                                        *
* Description   : Logic to send email to Resource Contact Person on case Submission or Closure
                  getting called from process builder 'FM Amenity Booking'                          *
* Created Date  : 15-05-2019                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE        COMMENTS                                                    *
* 1.0                                   Initial Draft.                                              *
****************************************************************************************************/

public with sharing class SendEmailToResourceContact {

    @InvocableMethod
    public static void sendEmail(list<Id> lstFMCaseId) {
        List<FM_Case__c> lstFMCase = new list<FM_Case__c>();

        List<Messaging.SingleEmailMessage> messages =   new List<Messaging.SingleEmailMessage>();
        OrgWideEmailAddress[] owea;
        EmailTemplate templateId;
         try {
             templateId = [select id, Body, Subject, name from EmailTemplate where developername = 'Booking_Info'];
             owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
         }
         catch (Exception e) {
             system.debug('!!!e'+e);
         }
         Boolean send;
        for (FM_Case__c objFMCase : [Select Id
                                        , Name
                                        , Resource__c
                                        , Resource__r.Name
                                        , Resource__r.Contact_Person_Email__c
                                        , Tenant__c
                                        , Tenant__r.Name
                                        , Account__c
                                        , Account__r.Name
                                        , Booking_Date__c
                                        , Booking_Start_Time_p__c
                                        , Booking_End_Time_p__c
                                        , Amenity_Booking_Status__c
                                        , Initiated_by_tenant_owner__c
                                        , Unit_Name__c
                                        , CreatedById
                                        , CreatedBy.UserType
                                      From FM_Case__c
                                      Where Id IN: lstFMCaseId]) {
            lstFMCase.add(objFMCase);

            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            String mailMessage = '';
            system.debug('objFMCase-----'+objFMCase);
            if (templateId != null) {
                mailMessage = templateId.Body;
                mailMessage = mailMessage.replace('{!FM_Case__c.Name}', objFMCase.Name);
                if (objFMCase.Resource__r.Name != null) {
                    mailMessage = mailMessage.replace('{!FM_Case__c.Resource__c}', objFMCase.Resource__r.Name);
                } else {
                    mailMessage = mailMessage.replace('{!FM_Case__c.Resource__c}', '');
                }
                if (objFMCase.Unit_Name__c != null) {
                    system.debug('ala if madhe-----');
                    mailMessage = mailMessage.replace('{!FM_Case__c.Unit_Name__c}', objFMCase.Unit_Name__c);
                } else {
                    system.debug('ala else madhe-----');
                    string unitNameStart = '\\('+'Unit';
                    unitNameStart = unitNameStart.replace('\\','');          
                    system.debug('ala unitNameStart-----'+unitNameStart);
                    mailMessage = mailMessage.replace(unitNameStart, '');
                    string unitNameEnd = '\\)';
                    unitNameEnd = unitNameEnd.replace('\\','');
                    system.debug('ala unitNameEnd-----'+unitNameEnd);
                    mailMessage = mailMessage.replace(unitNameEnd, '');
                    mailMessage = mailMessage.replace('{!FM_Case__c.Unit_Name__c}', '');
                }
                if (objFMCase.Booking_Date__c != null) {
                    mailMessage = mailMessage.replace('{!FM_Case__c.Booking_Date__c}', string.valueOf(objFMCase.Booking_Date__c));
                } else {
                    mailMessage = mailMessage.replace('{!FM_Case__c.Booking_Date__c}', '');
                }
                if (objFMCase.Booking_Start_Time_p__c != null) {
                    mailMessage = mailMessage.replace('{!FM_Case__c.Booking_Start_Time_p__c}', objFMCase.Booking_Start_Time_p__c);
                } else {
                    mailMessage = mailMessage.replace('{!FM_Case__c.Booking_Start_Time_p__c}', '');
                }
                if (objFMCase.Booking_End_Time_p__c != null) {
                    mailMessage = mailMessage.replace('{!FM_Case__c.Booking_End_Time_p__c}', objFMCase.Booking_End_Time_p__c);
                } else {
                    mailMessage = mailMessage.replace('{!FM_Case__c.Booking_End_Time_p__c}', '');
                }
                if (objFMCase.Amenity_Booking_Status__c != null) {
                    mailMessage = mailMessage.replace('{!FM_Case__c.Amenity_Booking_Status__c}', objFMCase.Amenity_Booking_Status__c);
                } else {
                    mailMessage = mailMessage.replace('{!FM_Case__c.Amenity_Booking_Status__c}', '');
                }
                if (objFMCase.Initiated_by_tenant_owner__c != null  ){
                    if ('tenant'.equalsIgnoreCase(objFMCase.Initiated_by_tenant_owner__c)){
                        if (objFMCase.Tenant__c != null){
                            system.debug('ala T if madhe-----');
                            mailMessage = mailMessage.replace('{!FM_Case__c.Tenant__c}', objFMCase.Tenant__r.Name);
                            email.setWhatId(objFMCase.Tenant__c);
                        } else {
                            system.debug('ala T else madhe-----');
                            mailMessage = mailMessage.replace('{!FM_Case__c.Tenant__c}', '');
                        }
                    } else if ('owner'.equalsIgnoreCase(objFMCase.Initiated_by_tenant_owner__c)){
                        if (objFMCase.Account__c != null){
                            mailMessage = mailMessage.replace('{!FM_Case__c.Tenant__c}', objFMCase.Account__r.Name);
                            email.setWhatId(objFMCase.Account__c);
                        } else {
                            mailMessage = mailMessage.replace('{!FM_Case__c.Tenant__c}', '');
                        }
                    }
                } else {
                    system.debug('ala mothya T else madhe-----');
                    mailMessage = mailMessage.replace('{!FM_Case__c.Tenant__c}', '');
                }
                system.debug('mailMessage-----'+mailMessage);
                email.subject = templateId.Subject;
                email.setplainTextBody(mailMessage);
                system.debug('objFMCase.Resource__r.Contact_Person_Email__c=='+objFMCase.Resource__r.Contact_Person_Email__c);
                email.toAddresses = new String[] {objFMCase.Resource__r.Contact_Person_Email__c};
                email.setSaveAsActivity(true);
                send = String.isNOtBlank(objFMCase.Resource__r.Contact_Person_Email__c)? true: false;
                if ( owea.size() > 0 ) {
                    email.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                messages.add(email);
            }// template Id check
        }// end of For

        if (messages != null && !messages.isEmpty() && send  ){
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        }
    }
}