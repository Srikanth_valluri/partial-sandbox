public without sharing class fmcAccessCardComponentController extends FMAccessCardComponentController {

    public List<Booking_Unit__c> objUnitList            {get; set;}
    public map<Id,Booking_Unit__c> mapUnitsOwned        {get; set;}
    public map<Id,Booking_Unit__c> mapMyUnits           {get; set;}
    public List<Id> objUnitOwner                        {get; set;}
    public List<Id> objUnitRes                          {get; set;}
    public List<SelectOption> lstUnits                  {get; set;}
    public Booking_Unit__c objUnit;
    public static final String requestType = 'Request For Access Card';

    public fmcAccessCardComponentController() {
        super(false);
        if (FmcUtils.isCurrentView('AccessCards')) {
            //super();
            uploadedDocLst = new Set<String>();
            uploadedDocuments = new List<SR_Attachments__c>();
            objFMCase = new FM_Case__c();
            selectedValuesOfAccessCard = new List<String>();
            objAcc = new Account();
            objLocation = new Location__c();
            showDocPanel = false;
            //strAccountId = CustomerCommunityUtils.customerAccountId;
            strFMCaseId = ApexPages.currentPage().getParameters().get('Id');
            if(String.isBlank(strFMCaseId)) {
                strAccountId = CustomerCommunityUtils.customerAccountId;
                strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
            }else {
                objFMCase = FM_Utility.getCaseDetails( strFMCaseId );
                strAccountId = objFMCase.Account__c ;
                strSelectedUnit = objFMCase.Booking_Unit__c;
                objFMCase = FM_Utility.getCaseDetails( strFMCaseId );
                fetchUnits();
                init();
            }
            //system.debug('FmcUtils.isOwner() : '+FmcUtils.isOwner());
            //if(FmcUtils.isOwner()){
                //strSRType = 'Request_for_Access_cards';
            //}else{
              //  strSRType = 'Request_for_Access_cards_T';
            //}
            objFMCase.Request_Type_DeveloperName__c = strSRType;
            objFMCase.Request_Type__c = requestType;
            objFMCase.Origin__c='Portal';
            objUnit = new Booking_Unit__c();
            System.debug('>>>---strSRType--- : '+strSRType);
            system.debug('>>>---strAccountId--- : '+strAccountId );
            System.debug('>>>---objFMCase.Request_Type__c--- : '+objFMCase.Request_Type__c);
            fetchUnits();
        }
    }

    public void fetchUnits() {
        Set<String> activeStatusSet= Booking_Unit_Active_Status__c.getall().keyset();
        System.debug('>>>---activeStatusSet--- : '+activeStatusSet);
        mapUnitsOwned = new map<Id,Booking_Unit__c>();
        objUnitOwner = new List<Id>();
        objUnitRes = new List<Id>();
        mapMyUnits = new map<Id,Booking_Unit__c>();
        lstUnits = new list<SelectOption>();
        lstUnits.add(new selectOption('None','--None--'));
        objUnitList = new List<Booking_Unit__c>();
        objUnitList = [SELECT Id
                           , Name
                           , Owner__c
                           , Booking__r.Account__c
                           , Booking__r.Account__r.Name
                           , Owner__r.IsPersonAccount
                           , Tenant__r.IsPersonAccount
                           , Owner__r.Name
                           , Tenant__c
                           , Tenant__r.Name
                           , Resident__c
                           , Resident__r.Name
                           , Unit_Name__c
                           , Registration_Id__c
                           , Property_Name__c
                           , Registration_Status__c
                           , Handover_Flag__c
                           , Early_Handover__c
                       FROM Booking_Unit__c
                       WHERE ( Resident__c = :strAccountId
                              OR Booking__r.Account__c = :strAccountId
                              OR Tenant__c = :strAccountId )
                       AND ( Handover_Flag__c = 'Y'
                            OR Early_Handover__c = true )
                       AND Registration_Status__c IN :activeStatusSet ];
        System.debug('>>>---objUnitList---- : ' +objUnitList);
        System.debug('>>>---objUnitList---- : ' +objUnitList.size());
        if(!objUnitList.isEmpty() && objUnitList != NULL) {
            for( Booking_Unit__c objBU : objUnitList ) {
                if( objBU.Owner__c == strAccountId ) {
                    objUnitOwner.add(objBU.Id);
                    mapUnitsOwned.put(objBU.Id,objBU);
                }

                if( ( objBU.Tenant__c == strAccountId || objBU.Resident__c == strAccountId )
                            && objBU.Owner__c != strAccountId ) {
                    objUnitRes.add(objBU.Id);
                    mapMyUnits.put(objBU.Id,objBU);
                }
                lstUnits.add(new selectOption(objBU.Id,objBU.Unit_Name__c));
                System.debug('>>>---lstUnits--- : '+lstUnits);
            }
        }
        else {
            ApexPages.addmessage(new ApexPages.message(
                            ApexPages.severity.Error,'No Booking Units available'));
        }
        System.debug('>>>---lstUnits--- : '+lstUnits);
    }

    public pageReference getUnitDetailsNew(){
        objFMCase.Type_of_Access_Card__c = '';
        valueListOfAccessCard = new List<SelectOption>();
        selectedValuesOfAccessCard = new List<String>();
        //lstDocuments = new List<FM_Documents__mdt>();
        system.debug('strSelectedUnit : '+strSelectedUnit);
        if (String.isBlank(strSelectedUnit)) {
            return NULL;
        }
        init();
        return NULL;
    }

    public void init() {
        noOfRequDocs=0;
        Decimal minimumOutstandingCharge=0;
        isEditable = allowInitiation=true;
        strFMCaseId = ApexPages.currentPage().getParameters().get('Id');
        System.debug('>>>---strFMCaseId--- : '+strFMCaseId);
        if(String.isBlank(strFMCaseId)){
            //strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
            //strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
            strSRType = ApexPages.currentPage().getParameters().get('SRType');
            system.debug('>>>>objUnitList : '+objUnitList.size());
            for(Booking_Unit__c obj : objUnitList) {
                if(obj.Tenant__c != NULL && obj.Tenant__c == strAccountId) {
                    objUnit = obj;
                }
            }
            system.debug('>>>---objUnit.Name--- : '+objUnit.Name);
            system.debug('>>>---objUnit.Tenant__c--- : '+objUnit.Tenant__c);
            if(objUnit.Tenant__c != NULL && objUnit.Tenant__c == strAccountId) {
                strSRType = 'Request_for_Access_cards_T';
            }else {
                strSRType = 'Request_for_Access_cards';
            }
            //strSRType = 'Request_for_Access_cards';
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                                    get('Request For Access Card').getRecordTypeId();
        }
        else {//Get existing case details
            for(Booking_Unit__c obj : objUnitList) {
                if(obj.Tenant__c != NULL && obj.Tenant__c == strAccountId) {
                    objUnit = obj;
                }
            }
            // system.debug('>>>---objUnit.Name--- : '+objUnit.Name);
            // system.debug('>>>---objUnit.Tenant__c--- : '+objUnit.Tenant__c);
            if(objUnit != NULL && objUnit.Tenant__c != NULL && objUnit.Tenant__c == strAccountId) {
                strSRType = 'Request_for_Access_cards_T';
            }else {
                strSRType = 'Request_for_Access_cards';
            }
            getCaseDetails();
        }
        //get account details
        System.debug('>>>---strAccountId--- : '+strAccountId);
        if(String.isNotBlank(strAccountId)) {
            List<Account> lstAcc = new List<Account>();
            lstAcc = [ SELECT Id
                            , Party_ID__c
                            , Name
                            , Mobile_Phone__pc
                            , Email__pc
                            , IsPersonAccount
                      FROM Account
                      WHERE Id =: strAccountId
                      AND IsPersonAccount = TRUE ];
            if(lstAcc.size() >0) {
                objAcc = lstAcc[0];
                objFMCase.Tenant_email__c = objAcc.Email__pc;
                objFMCase.Tenant__c = objAcc.Id;
            }
        }
        //Case initiation
        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                            get('Request For Access Card').getRecordTypeId();
        System.debug('>>>---devRecordTypeId--- : '+devRecordTypeId);
        // system.debug('>>>>objUnit.Tenant__c : '+objUnit.Tenant__c);

        if(objUnit != NULL && objUnit.Tenant__c != NULL && objUnit.Tenant__c == strAccountId) {
            strSRType = 'Request_for_Access_cards_T';
        }else {
            strSRType = 'Request_for_Access_cards';
        }
        System.debug('>>>---strSRType--- : '+strSRType);
        objFMCase.Request_Type_DeveloperName__c = strSRType;
        System.debug('>>>---objFMCase.Request_Type_DeveloperName__c--- : '
                            +objFMCase.Request_Type_DeveloperName__c);
        objFMCase.RecordTypeId = devRecordTypeId;
        objFMCase.Origin__c='Portal';
        System.debug('>>>---strAccountId--- : '+strAccountId);
        objFMCase.Tenant__c = strAccountId ;
        System.debug('>>>---strSelectedUnit--- : '+strSelectedUnit);
        objFMCase.Booking_Unit__c = strSelectedUnit ;
        //objFMCase.Request_Type__c = 'Request For Access Card';
        objFMCase.Request_Type__c = requestType;
        FM_process__mdt fmProcessMetadata=[SELECT Minimum_Outstanding_Charge__c
                                           FROM FM_process__mdt
                                           WHERE DeveloperName = :objFMCase.Request_Type_DeveloperName__c];
        System.debug('>>>---fmProcessMetadata--- : '+fmProcessMetadata);
        if(fmProcessMetadata != NULL) {
            minimumOutstandingCharge = fmProcessMetadata.Minimum_Outstanding_Charge__c != NULL
                                              ? fmProcessMetadata.Minimum_Outstanding_Charge__c : 0;
        }

        if(String.isNotBlank(strSelectedUnit)) {
            List<Booking_Unit__c> lstBU = new List<Booking_Unit__c>();
            lstBU = [SELECT Id
                            , Registration_ID__c
                            , Inventory__r.Property_Code__c
                            , Booking__r.Account__c
                            , Property_City__c
                            , Booking__r.Account__r.Name
                            , Unit_Name__c
                            , Property_Name__c
                    FROM Booking_Unit__c
                    WHERE ID =: strSelectedUnit ];

            if(lstBU.size() > 0) {
                objBU = lstBU[0];
                // objFMCase.Account__c = objBU.Booking__r.Account__c;
                if(String.isNotBlank(lstBU[0].Unit_Name__c)) {
                    String buildingName = lstBU[0].Unit_Name__c.split('/')[0];
                    List<Location__c> lstBuildings = new List<Location__c>();
                    lstBuildings = [SELECT Id
                                          , Name
                                          , Property_Name__c
                                          , Community_Name__c
                                          , Access_Card_Replacement_Fee__c
                                          , New_Access_Card_Fee__c
                                          , (SELECT FM_User__c
                                                    , FM_User__r.Email
                                                    , FM_Role__c
                                                    , FM_User__r.Name
                                             FROM FM_Users__r)
                                    FROM Location__c
                                    WHERE Name = :buildingName
                                    AND RecordType.DeveloperName = 'Building'
                                    LIMIT 1 ];
                    if(lstBuildings.size() > 0) {
                        objLocation = lstBuildings[0];
                        if(objLocation.FM_Users__r.size() > 0) {
                            for(FM_User__c obj :objLocation.FM_Users__r) {
                                if(obj.FM_Role__c == 'FM Admin'
                                            || obj.FM_Role__c == 'Master Community Admin') {
                                    objFMCase.Admin__c = obj.FM_User__c;
                                }
                            }//for
                        }
                    }
                }
            }
            if( objBU != NULL ) {
                try {
                    objFMCase.Account__c = objBU.Booking__r.Account__c;
                    FmIpmsRestServices.DueInvoicesResult objResponse =
                                    FmIpmsRestServices.getDueInvoices( objBU.Registration_Id__c,'',
                                    objBU.Inventory__r.Property_Code__c);
                    System.debug('>>>---objResponse--- : '+objResponse);
                    if(objResponse != NULL) {
                        objFMCase.Outstanding_service_charges__c =
                                !String.isBlank( objResponse.totalDueAmount )
                                ? objResponse.totalDueAmount : '0';
                        allowInitiation = Decimal.valueof(objFMCase.Outstanding_service_charges__c)
                                            <= minimumOutstandingCharge ? true : false;
                    }
                }
                catch(Exception e) {
                    system.debug( '--Exception while fetching oustanding balance--'+e.getMessage());
                }
            }
        }
        getfieldValues();
        System.debug('--- strSRType --- : '+strSRType);
        System.debug('objBU.Property_City__c : '+objBU.Property_City__c);
        getDocuments();//Get documents to upload from metadata
        if(String.isNotBlank(strFMCaseId)){
            System.debug('Show Documents');
            System.debug('--- lstDocuments --- : '+lstDocuments);
            showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
        }
    }

    public override PageReference saveAsDraft() {
        draftSR();
        objFMCase.Status__c='Draft Request';
        System.debug('>>>---objFMCase.Request_Type__c--- : '+objFMCase.Request_Type__c);
        upsert objFMCase;
        PageReference objPage = new PageReference('/' + objFMCase.Id );
        if (objPage != NULL) {
            String unitId = objPage.getUrl().substringAfterLast('/');
            objPage = new PageReference(ApexPages.currentPage().getUrl());
            objPage.getParameters().clear();
            objPage.getParameters().put('view', 'CaseDetails');
            objPage.getParameters().put('id', unitId);
            objPage.setRedirect(true);
        }
        return objPage ;
    }

    public override PageReference submitSR() {
        draftSR();
        objFMCase.Approval_Status__c='Pending';
        objFMCase.Submit_for_Approval__c = false;
        objFMCase.Submitted__c = true;
        objFMCase.Status__c='Submitted';

        list<FM_Approver__mdt> lstApprovers = new list<FM_Approver__mdt>();
        System.debug('--- objBU --- : '+objBU);
        System.debug('--- objFMCase.Request_Type_DeveloperName__c --- : '
                                        +objFMCase.Request_Type_DeveloperName__c);
        if(objBU != NULL && objFMCase.Request_Type_DeveloperName__c != NULL) {
            lstApprovers = FM_Utility.fetchApprovers(objFMCase.Request_Type_DeveloperName__c,
                                                    objBU.Property_City__c);
        }
        System.debug('>>>---lstApprovers--- : '+lstApprovers);
        String approvingRoles = '';
        for(FM_Approver__mdt mdt : lstApprovers) {
            approvingRoles += mdt.Role__c + ',';
        }
        approvingRoles = approvingRoles.removeEnd(',');
        objFMCase.Approving_Authorities__c = approvingRoles;
        System.debug('>>>---objFMCase.Request_Type__c--- : '+objFMCase.Request_Type__c);
        upsert objFMCase;
        PageReference objPage = new PageReference('/' + objFMCase.Id );
        if (objPage != NULL) {
            String unitId = objPage.getUrl().substringAfterLast('/');
            objPage = new PageReference(ApexPages.currentPage().getUrl());
            objPage.getParameters().clear();
            objPage.getParameters().put('view', 'CaseDetails');
            objPage.getParameters().put('id', unitId);
            objPage.setRedirect(true);
        }
        return objPage ;
    }
}