@isTest
public class notifyCREforCalBackTestClone {
     public static Id collectioncallingRecordTypeId = 
            Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Call Back Request').RecordTypeId;
    @isTest
    static void testBatch(){
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       Email__pc = 'test@mailinator.com',
                                       Active_Customer__c = 'Active');
        insert account;
        
         List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
    
         List<Calling_List__c> listCallingList = new List<Calling_List__c> ();
        Calling_List__c collectionCallInstManagement5 = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                                         Calling_List_Type__c = 'Case Summary',
                                                                         DP_CL_OutCome_Update_Date__c = System.Today().addDays(-5),
                                                                         Account__c = account.id,
                                                                         Customer_Flag__c = true,
                                                                         Unit_Name__c = 'demo unit name1',
                                                                         Inv_Due__c = 87,
                                                                         Call_Back_Date__c = System.Now(),
                                                                         DM_Due_Amount__c = 17,
                                                                         Amount_Pending__c =1720,
                                                                         IsHideFromUI__c = false,
                                                                         Registration_ID__c = '10300',
                                                                         Party_ID__c = '11723'  );
                                                                         
        listCallingList.add(collectionCallInstManagement5);
        
        
        
        insert listCallingList;
    
        
        CallLog_Count__c objCallLog = new CallLog_Count__c();
        objCallLog.Name ='CBR';
        objCallLog.NoOfCallLog__c = 4;
        insert objCallLog;
        
        List<Call_Log__c> lstCL = new List<Call_Log__c>();
        Call_Log__c objCall_Log = new Call_Log__c();
        objCall_Log.Calling_List__c = listCallingList[0].Id;
        objCall_Log.Calling_Number__c = '123456789';
        objCall_Log.CRE__c = UserInfo.getUserId();
        objCall_Log.Account__c = account.Id;
        objCall_Log.Call_Type__c = 'Outbound';
        objCall_Log.CreatedDate = Datetime.newInstance(2020, 3, 2, 12, 30, 2);
        lstCL.add(objCall_Log);
        Call_Log__c objCall_Log1 = new Call_Log__c();
        objCall_Log.Calling_List__c = listCallingList[0].Id;
        objCall_Log1.Calling_Number__c = '123456789';
        objCall_Log1.CRE__c = UserInfo.getUserId();
        objCall_Log1.Account__c = account.Id;
        objCall_Log1.Call_Type__c = 'Outbound';
        objCall_Log1.CreatedDate = Datetime.newInstance(2020, 3, 2, 12, 30, 2);
        lstCL.add(objCall_Log1);
        Call_Log__c objCall_Log2 = new Call_Log__c();
        objCall_Log.Calling_List__c = listCallingList[0].Id;
        objCall_Log2.Calling_Number__c = '123456789';
        objCall_Log2.CRE__c = UserInfo.getUserId();
        objCall_Log2.Account__c = account.Id;
        objCall_Log2.Call_Type__c = 'Outbound';
        objCall_Log2.CreatedDate = Datetime.newInstance(2020, 3, 2, 9, 30, 2);
        lstCL.add(objCall_Log2);
        insert lstCL;
        
        
        Test.startTest();
            notifyCREforCalBack objNotify = new notifyCREforCalBack();
            Database.executeBatch(objNotify);
        Test.stopTest();
        
        
    }
}