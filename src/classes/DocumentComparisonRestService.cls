/*-------------------------------------------------------------------------------------------------
Description: Rest Service for Comparison of Documents

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 01-11-2018       | Lochan Karle   | Initial draft
    ----------------------------------------------------------------------------------------------------------------------------
   =============================================================================================================================
*/
public with sharing class DocumentComparisonRestService {
    
    public static DocComparison compareDocs (String originalDoc, String scannedDoc, String caseId) {
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(label.Doc_Compare_Endpoint_URL);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody('{"original":"'+originalDoc+'", "scanned":"'+scannedDoc+'", "referenceId":"'+caseId+'", "pdfReport":"true"}');
        request.setTimeout(120000);
        system.debug('mazi request===='+request);
        HttpResponse response = http.send(request);
        system.debug('Response '+response.getBody());
        system.debug('Response '+response.getStatusCode());
        
        DocComparison objComparison = new DocComparison();
        if (response.getStatusCode() == 200){
            objComparison = DocComparison.parseDocResponse(response.getBody());
        }   
        system.debug('!!!!!!objComparison'+objComparison);  
        return objComparison;
    }
    
    public class DocComparison {
        public Decimal accuracy;
        public Decimal addChar;
        public Decimal deleteChar;
        public String diffReport;
        public String pageAccuracy;
        public Decimal totalChar;
        public List<pages> pages;
    }
    
    public class pages {
        public Decimal accuracy;
        public Decimal addChar;
        public Decimal delChar;
        public Decimal originalPageNumber;
        public Decimal scannedPageNumber;
        public Decimal totalChar;
    }
    
    public static DocComparison parseDocResponse (String json) {
        return (DocComparison) System.JSON.deserialize(json, DocComparison.class);
    }
}