@isTest
public class FirstFlightServiceTest {
    
    public class FirstFlightServiceMock implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req) {
            req.setMethod('POST');
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String Json = '{ "AirwayBillTrackList": [{ "AirWayBillNo": "12345", "Destination": "ABU DHABI-UNITED ARAB EMIRATES", "ForwardingNumber": "", "Origin": "DUBAI-UNITED ARAB EMIRATES", "ShipmentProgress": 3, "ShipperReference": "AE5248222", "TrackingLogDetails": [{ "ActivityDate": "Saturday 02 November 2019", "ActivityTime": "12:21", "DeliveredTo": "", "Location": "DUBAI", "Remarks": "Location Information Received", "Status": "LC" }, { "ActivityDate": "Saturday 02 November 2019", "ActivityTime": "11:43", "DeliveredTo": "", "Location": "DUBAI", "Remarks": "Whatsapp To Customer", "Status": "SM" } ], "Weight": "1.000" }], "Code": 1, "Description": "Success" }';
            res.setBody(Json);
            res.setStatusCode(200);
            return res;
        }
    }
    
    public class FirstFlightAirwayBillPDFMock implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req) {
            req.setMethod('POST');
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String Json = '{ "Code": 1, "Description": "Success", "ReportDoc": "JVBERi0xLjIgCiXi48/TIAoxIDAgb2JqIAo8PCAKL1R5cGUgL0NhdGFsb2cgCi9QYWdlcyAyIDAgUiAKL1BhZ2VNb2RlIC9Vc2VOb25lIAovVmlld2VyUHJlZmVyZW5jZXMgPDwgCi9GaXRXaW5kb3cgdHJ1ZSAKL1BhZ2VMYXlvdXQgL1NpbmdsZVBhZ2UgCi9Ob25GdWxsUzIAo1MDAgCjI3OCAKNTU2IAo1MDAgCjcyMiAKMCAKNTAwIApdIAplbmRvYmogCjE1IDAgb2JqIAovQUFBQUFBK0FyaWFsIAplbmRvYmogCjE3IDAgb2JqIAo8PCAKL1R5cGUgL0ZvbnREZXNjcmlwdG9yIAovQXNjZW50IDkwNSAKL0NhcEhlaWdodCA1MDAgCi9EZXNjZW50IC0yMTIgCi9GbGFnDAwMDAwIG4gCjAwMDAwNDUyOTggMDAwMDAgbiAKdHJhaWxlciAKPDwgCi9TaXplIDI5IAovUm9vdCAxIDAgUiAKL0luZm8gMjggMCBSIAo+PiAKc3RhcnR4cmVmIAo0NTM4NiAKJSVFT0YgCg==" }';
            res.setBody(Json);
            res.setStatusCode(200);
            return res;
        }
    }
    
    @isTest
    public static void firstFlightTrackingTest(){
        String strTrackingAWB = '12345';
        String accountNo = '3000';
        String country = 'AE';
        
        Credentials_Details__c objCred = new Credentials_Details__c();
        objCred.Name = 'First Flight Tracking';
        objCred.Endpoint__c = 'https://ontrack.firstflightme.com/FFCService.svc/Tracking';
        objCred.User_Name__c = '3000';
        objCred.Password__c = 'fftes';
        insert objCred;
        //FirstFlightService.getFirstFlightTracking(strTrackingAWB, accountNo, country);
        Test.setMock(HttpCalloutMock.class, new FirstFlightServiceMock ());
        Test.startTest();
        FirstFlightService.getFirstFlightTracking('123', '300', 'AE');
        Test.stopTest();
    }
    
    @isTest
    public static void generateAirwayBillTest(){
        Credentials_Details__c objCred = new Credentials_Details__c();
        objCred.Name = 'First Flight Create AirwayBill';
        objCred.Endpoint__c = 'https://ontrack.firstflightme.com/FFCService.svc/CreateAirwayBill';
        objCred.User_Name__c = '3000';
        objCred.Password__c = 'fftes';
        insert objCred;
        
        Account objAccount = new Account();
        objAccount.Name = 'test';
        objAccount.Party_ID__c = '12345';
        insert objAccount;
        
        Case objCase = new Case();
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        objCase.AccountId = objAccount.Id;
        insert objCase;
        
        List<Courier_Delivery__c> lstCourier = new List<Courier_Delivery__c>();
        lstCourier.add(new Courier_Delivery__c(Country__c= 'AE', Account__c = objAccount.Id, Case__c = objCase.id, Phone_1__c = '1234679585', Postal_Code__c = '425001'));
        lstCourier.add(new Courier_Delivery__c(Country__c= 'India', Account__c = objAccount.Id, Case__c = objCase.id, Phone_1__c = '1238957959', Postal_Code__c = '425101'));
        insert lstCourier;
        List<Courier_Delivery__c> lstUpdatedCourier = new List<Courier_Delivery__c>();
        Test.setMock(HttpCalloutMock.class, new FirstFlightServiceMock ());
        Test.startTest();
        lstUpdatedCourier = FirstFlightService.generateAirwayBill(lstCourier);
        System.assertNotEquals(Null, lstUpdatedCourier);
        System.assertEquals(2, lstUpdatedCourier.size());
        System.debug('lstUpdatedCourier: '+lstUpdatedCourier);
        Test.stopTest();
    }
    
    @isTest
    public static void getAirwayBillInPDFTest(){
        String strTrackingAWB = '12345';
        String accountNo = '3000';
        String country = 'AE';
        
        Credentials_Details__c objCred = new Credentials_Details__c();
        objCred.Name = 'First Flight AWB PDF';
        objCred.Endpoint__c = 'https://ontrack.firstflightme.com/FFCService.svc/Tracking';
        objCred.User_Name__c = '3000';
        objCred.Password__c = 'fftes';
        objCred.Resource__c = '3000';
        insert objCred;
        
        Test.setMock(HttpCalloutMock.class, new FirstFlightAirwayBillPDFMock ());
        Test.startTest();
        Blob objBlob = FirstFlightService.getAirwayBillInPDF(strTrackingAWB, country);
        System.debug('objBlob: '+objBlob);
        System.assertNotEquals(Null, objBlob);
        Test.stopTest();
    }
}