/****************************************************************************************************
* Name          : CreateInquiryController
* Description   : CreateInquiry Page Controller
* Created Date  : 19-08-2018
* Created By    : ESPL
* --------------------------------------------------------------------------------------------------
* VER   AUTHOR              DATE            COMMENTS
* 1.0   Craig Lobo          19-08-2018      Initial Draft.
* 2.0   Bhanu Gupta         18-11-2018      For Property Consultant UK Profile
* 3.0   QBurst              02-03-2020      Added new fields for copying from Parent Inquiry
****************************************************************************************************/

public without sharing class CreateInquiryController {

    public Inquiry__c inqObj                                                            {get; set;}
    public Boolean isTSA                                                                {get; set;}
    public Boolean isConsultantUK                                                       {get; set;}
    public String descText                                                              {get; set;}
    public String currentIQId;

    /**
     * Constructor: Variable Initialization and default values
     */
    public CreateInquiryController(ApexPages.StandardController controller) {
        currentIQId = controller.getRecord().Id;
        isTSA = false;
        isConsultantUK = false;
        List<User> currUserList = new List<User>();
        currUserList = [ SELECT Id, Name, ProfileId, Profile.Name 
                           FROM User 
                          WHERE Id = :UserInfo.getUserId() 
                          LIMIT 1];

        if (!currUserList.isEmpty()) {
            System.debug('>>>> currUserList[0].Profile.Name >> ' + currUserList[0].Profile.Name);
            if (currUserList[0].Profile.Name.contains('Telesales')) {
                isTSA = true;
            } 
            else if (currUserList[0].Profile.Name.contains('Property Consultant UK')) {
                isConsultantUK = true;
            } 
        }
        System.debug('>>>> isTSA >> ' + isTSA);
        System.debug('>>>> isConsultantUK >> ' + isConsultantUK);
        init(); 
    }

    public void init() {
        System.debug('>>>> isTSA >> ' + isTSA);
        Inquiry__c iq = [ SELECT Id, Campaign__c, Fly_in_TSA_Manager__c, Flyin_RM__c, Fly_in_TSA__c,
                                 Sales_Office__c, Promoter_Name__c, Telesales_Executive__c, Meeting_Type__c,
                                 Tour_Date_Time__c, Assigned_PC__c, 
                                 Ameyo_ID_Dataload__c, // 3.0
                                 Inquiry_Assignment_Rules__c, // 3.0
                                 (SELECT Id, Name FROM Sales_Tours__r)
                            FROM Inquiry__c 
                           WHERE Id = :currentIQId LIMIT 1
        ];
        if (isTSA) {
            inqObj = new Inquiry__c(
                Inquiry_Source__c = 'Customer Referral', 
                Referred_By__c = currentIQId,
                Primary_Contacts__c = 'Mobile Phone',
                Campaign__c = iq.Campaign__c,
                Promoter_Name__c = iq.Promoter_Name__c,
                Sales_Office__c = iq.Sales_Office__c,
                Telesales_Executive__c = iq.Telesales_Executive__c,
                Customer_Referral__c = true
            ); 
            descText = '';
        }else if (isConsultantUK) {
            inqObj = new Inquiry__c(
                Inquiry_Source__c = 'Prospecting' 
            ); 
        } else {
            inqObj = new Inquiry__c(
                Inquiry_Source__c = 'Customer Referral', 
                Referred_By__c = iq.Id, 
                Primary_Contacts__c = 'Mobile Phone',
                Campaign__c = iq.Campaign__c,
                Fly_in_TSA_Manager__c = iq.Fly_in_TSA_Manager__c,
                Fly_in_TSA__c = iq.Fly_in_TSA__c,
                Promoter_Name__c = iq.Promoter_Name__c,
                Sales_Office__c = iq.Sales_Office__c,
                Telesales_Executive__c = iq.Telesales_Executive__c,
                Customer_Referral__c = true
            );
        }
        descText = '';
        inqObj.Primary_Contacts__c = 'Mobile Phone';
        inqObj.Meeting_Type__c = iq.Meeting_Type__c;
        inqObj.Inquiry_Assignment_Rules__c = iq.Inquiry_Assignment_Rules__c; // 3.0
        inqObj.Ameyo_ID_Dataload__c = iq.Ameyo_ID_Dataload__c; // 3.0
        inqObj.Tour_Date_Time__c = iq.Tour_Date_Time__c;
        inqObj.Assigned_PC__c = iq.Assigned_PC__c;
        if (iq.Sales_Tours__r != null && !iq.Sales_Tours__r.isEmpty()) {
            inqObj.Sales_Tour__c = iq.Sales_Tours__r[0].Name;
        }
    }

    //2.0 Added by Bhanu Gupta
    /**
     * Method to get Inquiry Source List for Property Consultant Profile
     **/

    public static List<SelectOption> getInquirySource() {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Inquiry__c.Inquiry_Source__c.getDescribe();
        options.add( new SelectOption('Choose','Choose'));
        //options.add( new SelectOption('Other Country Code','Other Country Code'));
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            String valueOfPicklist = f.getValue();
            if(f.getLabel() == 'Agent Referral' 
                || f.getLabel() == 'Prospecting'
            ){
                options.add( new SelectOption(f.getLabel(),f.getLabel()));  
            }
        }       
        return options;
    }


    /**
     * Method insert the new Inquiry or update the current Inquiry record in context
     */
    public PageReference saveReferralInquiry() {
        try{
            inqObj.Description__c = descText;
            system.debug('>>insert '+inqObj);
            system.debug('>>insert>>>>  '+inqObj.Meeting_Type__c);
            insert inqObj;
            system.debug('>>insert2 '+inqObj);
            if(isConsultantUK){
                PageReference nextPage = new PageReference('/' + inqObj.Id);
                return nextPage;
            }
        }
        catch(exception e){
            system.debug('>>Exception '+e+ e.getLineNumber());
        }
        return null;
    } 
}