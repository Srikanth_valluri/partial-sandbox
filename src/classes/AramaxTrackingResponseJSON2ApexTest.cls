@isTest
public class AramaxTrackingResponseJSON2ApexTest {
	@isTest
    public static void jsonParseTest(){
        String SAMPLE_RESPONSE=  '{'+
                             '"Transaction": {'+
                             '"Reference1": null,'+
                             '"Reference2": null,'+
                             '"Reference3": null,'+
                             '"Reference4": null,'+
                             '"Reference5": null'+
                             '},'+
                             '"Notifications": [],'+
                             '"HasErrors": false,'+
            				'"Notifications": ['+
                            '{'+
                                    '"Code": "REQ11",'+
                                    '"Message": "Shipper.PartyAddress.CountryCode - Country Code is empty"'+
                                '}'+
                            '],'+
                             '"TrackingResults": ['+
                             '{'+
                             '"Key": "44097879825",'+
                             '"Value": ['+
                             '{'+
                             '"WaybillNumber": "44097879825",'+
                             '"UpdateCode": "SH203",'+
                             '"UpdateDescription": "Record Created",'+
                             '"UpdateDateTime": "/Date(1599121080000+0300)/",'+
                             '"UpdateLocation": "Dubai, United Arab Emirates",'+
                             '"Comments": "",'+
                             '"ProblemCode": "",'+
                             '"GrossWeight": "0.5",'+
                             '"ChargeableWeight": "0.5",'+
                             '"WeightUnit": "KG"'+
                             '},'+
                             '{'+
                             '"WaybillNumber": "44097879825",'+
                             '"UpdateCode": "SH278",'+
                             '"UpdateDescription": "Data received.",'+
                             '"UpdateDateTime": "/Date(1599121080000+0300)/",'+
                             '"UpdateLocation": "Dubai, United Arab Emirates",'+
                             '"Comments": " ",'+
                             '"ProblemCode": "",'+
                             '"GrossWeight": "0.5",'+
                             '"ChargeableWeight": "0.5",'+
                             '"WeightUnit": "KG"'+
                             '}'+
                             ']'+
                             '}'+
                             '],'+
                             '"NonExistingWaybills": []'+
                            '}';
        
        	AramaxTrackingResponseJSON2Apex  objRes = (AramaxTrackingResponseJSON2Apex)AramaxTrackingResponseJSON2Apex.parse(SAMPLE_RESPONSE);
    }
}