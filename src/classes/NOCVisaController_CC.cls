public without sharing class NOCVisaController_CC extends NOCVisaController{
    public NOCVisaController_CC() {
        super(false);
        if (CustomerCommunityUtils.isCurrentView('nocvisa')) {
            super();
        }
    }
    public override PageReference executews() {
        strCaseID = caseRecord.id;
        try {
            soaUrl = '';
            strDocUrl = '';
            List < Case > lstCase = [Select Id, CaseNumber From Case Where Id =: caseRecord.Id Limit 1];
            String CaseNumberStr = lstCase[0].CaseNumber;
            poaAttachmentWrapper = new AttachmenWrapper();
            crfAttachmentWrapper = new AttachmenWrapper();
            titleDeedAttachmentWrapper = new AttachmenWrapper();
            passportAttachmentWrapper = new AttachmenWrapper();
            additionalAttachmentWrapper = new AttachmenWrapper();
            verifyBuyer();
            populateBuyerList();
            String attachmentType = '';
            if (userNationality.equals('UAE/KSA')) {
                attachmentType = 'Emirates ID';
            } else if (userNationality.equals('GCC')) {
                attachmentType = 'GCC ID';
            } else {
                attachmentType = 'Entry Visa Stamp';
            }
            listAttachmentWrapper = new List < AttachmenWrapper > ();
            System.debug('...caseRecord....in executews.....' + caseRecord);
            System.debug('...soaUrl....value in executews method...for sr creation....' + soaUrl);
            list < SR_Attachments__c > listCaseAttachment = new list < SR_Attachments__c > ();
            map < String, SR_Attachments__c > mapCaseAttachment = new map < String, SR_Attachments__c > ();

            list < SR_Attachments__c > listCaseAttachmentnew = new list < SR_Attachments__c > ([select ID, Attachment_URL__c, case__c, Type__c, Name, isValid__c
                from SR_Attachments__c
                where case__c =: caseRecord.ID
                AND(Type__c = 'Title Deed'
                    OR Type__c = 'CRF Form'
                    OR Type__c = 'Power Of Attorney'
                    OR Type__c = 'Passport'
                    OR Type__c = 'Emirates ID'
                    OR Type__c = 'GCC ID'
                    OR Type__c = 'Entry Visa Stamp'
                    OR Type__c = 'NOC For VISA'
                )
            ]);
            if (!listCaseAttachmentnew.isEmpty()) {
                for (SR_Attachments__c objSR: listCaseAttachmentnew) {
                    mapCaseAttachment.put(objSR.Type__c, objSR);
                }
            }
            system.debug('...listCaseAttachmentnew...' + listCaseAttachmentnew);
            system.debug('>>isSaveAsDraft Portal : '+isSaveAsDraft);
            if (!listCaseAttachmentnew.isEmpty() ) {
                System.debug('....mapCaseAttachment...' + mapCaseAttachment);
                if (mapCaseAttachment.containsKey('CRF Form')) {
                    System.debug('...PRATIKSHA,,,');
                    SR_Attachments__c srAttachCRF = new SR_Attachments__c();
                    System.debug('...PRATIKSHA2,,,');
                    srAttachCRF = mapCaseAttachment.get('CRF Form');
                    System.debug('...PRATIKSHA,3,,' + srAttachCRF);
                    crfAttachmentWrapper.srAttachment = srAttachCRF;
                    System.debug('...PRATIKSHA,,4,');
                    crfAttachmentWrapper.isPresent = true;
                    System.debug('...PRATIKSHA,,5,');
                    caseRecord.Is_CRF_Uploaded__c = true;
                    System.debug('...PRATIKSHA,6,,');
                    listCaseAttachment.add(srAttachCRF);
                    System.debug('...PRATIKSHA,,7,');
                    System.debug('...listCaseAttachment...1,,,' + listCaseAttachment);
                }
                if (mapCaseAttachment.containsKey('Title Deed')) {
                    SR_Attachments__c srAttachTitle = new SR_Attachments__c();
                    srAttachTitle = mapCaseAttachment.get('Title Deed');
                    titleDeedAttachmentWrapper.srAttachment = srAttachTitle;
                    titleDeedAttachmentWrapper.isPresent = true;
                    caseRecord.Is_CRF_Uploaded__c = true;
                    listCaseAttachment.add(srAttachTitle);
                    titleDeedAttachmentWrapper.isPresent = true;
                    listAttachmentWrapper.add(titleDeedAttachmentWrapper);
                    System.debug('...listCaseAttachment...2,,' + listCaseAttachment);
                }
                if (mapCaseAttachment.containsKey('Passport')) {
                    SR_Attachments__c srAttachPassport = new SR_Attachments__c();
                    srAttachPassport = mapCaseAttachment.get('Passport');
                    passportAttachmentWrapper.srAttachment = srAttachPassport;
                    passportAttachmentWrapper.isPresent = true;
                    caseRecord.Is_CRF_Uploaded__c = true;
                    listCaseAttachment.add(srAttachPassport);
                    listAttachmentWrapper.add(passportAttachmentWrapper);
                    System.debug('...listCaseAttachment...3,,,' + listCaseAttachment);
                }
                if (mapCaseAttachment.containsKey(attachmentType)) {
                    SR_Attachments__c srAttachattachmentType = new SR_Attachments__c();
                    srAttachattachmentType = mapCaseAttachment.get(attachmentType);
                    additionalAttachmentWrapper.srAttachment = srAttachattachmentType;
                    additionalAttachmentWrapper.isPresent = true;
                    caseRecord.Is_CRF_Uploaded__c = true;
                    listCaseAttachment.add(srAttachattachmentType);
                    listAttachmentWrapper.add(additionalAttachmentWrapper);
                    System.debug('...listCaseAttachment...4,,,' + listCaseAttachment);
                }
                if (mapCaseAttachment.containsKey('Power Of Attorney')) {
                    SR_Attachments__c srAttachPOA = new SR_Attachments__c();
                    srAttachPOA = mapCaseAttachment.get('Power Of Attorney');
                    poaAttachmentWrapper.srAttachment = srAttachPOA;
                    poaAttachmentWrapper.isPresent = true;
                    caseRecord.Is_CRF_Uploaded__c = true;
                    listCaseAttachment.add(srAttachPOA);
                    listAttachmentWrapper.add(poaAttachmentWrapper);
                    poaAttachmentWrapper.isPresent = true;
                    System.debug('...listCaseAttachment...5,,' + listCaseAttachment);
                }
                system.debug('>>isSaveAsDraft : '+isSaveAsDraft);
                if (!isSaveAsDraft) {
                    System.debug('..abcd...');
                    submitCase();
                }
            } else {

                showNOCVisabutton = false;
                SR_Attachments__c srAttach1 = new SR_Attachments__c();
                List < Account > AccountList = new List < Account > ([SELECT Id,
                    Name,
                    Party_ID__c
                    FROM Account
                    WHERE id =: accountId
                ]);
                system.debug('...AccountList...' + AccountList);
                UploadMultipleDocController.MultipleDocRequest objWrapper = new UploadMultipleDocController.MultipleDocRequest();
                list < UploadMultipleDocController.MultipleDocRequest > listobjWrapper = new list < UploadMultipleDocController.MultipleDocRequest > ();
                UploadMultipleDocController.data MultipleData = new UploadMultipleDocController.data();

                if (AccountList[0].party_ID__C != '') {
                    system.debug('...AccountList[0].party_ID__C...' + AccountList[0].party_ID__C);
                    objWrapper.category = 'Document';
                    objWrapper.entityName = 'Damac Service Requests';
                    //for(filebodyName objFile : listfilebodyName){
                    System.debug('...titleDeedAttachmentBody...' + titleDeedAttachmentBody);
                    system.debug('...caseRecord...' + caseRecord + 'caseRecord.CaseNumber' + caseRecord.CaseNumber);
                    if (String.isNotBlank(titleDeedAttachmentBody)) {
                        String titledeedurl = '';
                        blob objBlob = extractBody(titleDeedAttachmentBody);
                        if (objBlob != null) {
                            objWrapper.base64Binary = EncodingUtil.base64Encode(objBlob);
                        }
                        objWrapper.fileDescription = 'Title Deed';
                        objWrapper.fileId = CaseNumberStr + '-' + String.valueOf(System.currentTimeMillis()) + '.' + titleDeedAttachmentName.substringAfterLast('.');
                        objWrapper.fileName = CaseNumberStr + '-' + String.valueOf(System.currentTimeMillis()) + '.' + titleDeedAttachmentName.substringAfterLast('.');
                        objWrapper.registrationId = CaseNumberStr;
                        objWrapper.sourceFileName = extractName(titleDeedAttachmentName);
                        objWrapper.sourceId = extractName(titleDeedAttachmentName);
                        listobjWrapper.add(objWrapper);
                        if (!listobjWrapper.isEmpty()) {
                            try {
                                MultipleData = UploadMultipleDocController.getMultipleDocUrl(listobjWrapper);
                                if (MultipleData != null && MultipleData.status == 'Exception') {
                                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, MultipleData.message));
                                    errorLogger(MultipleData.message, caseRecord.ID, selectedUnit);
                                }
                                if (MultipleData != null && (MultipleData.Data == null || MultipleData.Data.Size() == 0)) {
                                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, MultipleData.message));
                                }
                                if (MultipleData.data != null) {
                                    for (UploadMultipleDocController.MultipleDocResponse objData: MultipleData.Data) {
                                        if (objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)) {
                                            titledeedurl = objData.url;
                                            system.debug('...titledeedurl....' + titledeedurl);
                                            //mapURLS.put(pdfurl.substring( pdfurl.lastIndexOf('/')+1 ),pdfurl);
                                        } else {
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'error... ' + objData.PARAM_ID));
                                        }
                                    }
                                }
                                system.debug('...caseRecord.Id....' + caseRecord.Id + '...titledeedurl...' + titledeedurl);
                                if (!mapCaseAttachment.containsKey('Title Deed') && titledeedurl != '' && titledeedurl != null) {
                                    srAttach1.Attachment_URL__c = titledeedurl;
                                    srAttach1.Case__c = caseRecord.Id;
                                    srAttach1.Type__c = 'Title Deed';
                                    showNOCVisabutton = false;
                                    srAttach1.isValid__c = false;
                                    srAttach1.Name = titledeedurl != '' ? titledeedurl.substring(titledeedurl.lastIndexOf('/') + 1) : '';
                                    listCaseAttachment.add(srAttach1);
                                    titleDeedAttachmentWrapper.srAttachment = srAttach1;
                                    titleDeedAttachmentWrapper.isPresent = true;
                                    listAttachmentWrapper.add(titleDeedAttachmentWrapper);
                                }

                            } catch (Exception e) {
                                System.debug('...exception is...' + e + '..line number is ...' + e.getLineNumber());
                                srAttach1.Name = titledeedurl.substring(titledeedurl.lastIndexOf('/') + 1);
                            }
                        }
                    }
                    System.debug('...strCRFAttachmentBody...' + strCRFAttachmentBody);
                    if (String.isNotBlank(strCRFAttachmentBody)) {
                        blob objBlob = extractBody(strCRFAttachmentBody);
                        String CRRUrl;
                        if (objBlob != null) {
                            objWrapper.base64Binary = EncodingUtil.base64Encode(objBlob);
                        }
                        objWrapper.fileDescription = 'CRF';
                        objWrapper.fileId = CaseNumberStr + '-' + String.valueOf(System.currentTimeMillis()) + '.' + strCRFAttachmentName.substringAfterLast('.');
                        objWrapper.fileName = CaseNumberStr + '-' + String.valueOf(System.currentTimeMillis()) + '.' + strCRFAttachmentName.substringAfterLast('.');
                        objWrapper.registrationId = CaseNumberStr;
                        objWrapper.sourceFileName = extractName(strCRFAttachmentName);
                        objWrapper.sourceId = extractName(strCRFAttachmentName);
                        listobjWrapper.add(objWrapper);
                        System.debug('...listobjWrapper...' + listobjWrapper);
                        if (!listobjWrapper.isEmpty()) {
                            SR_Attachments__c srAttach2 = new SR_Attachments__c();
                            try {
                                MultipleData = UploadMultipleDocController.getMultipleDocUrl(listobjWrapper);
                                if (MultipleData != null && MultipleData.status == 'Exception') {
                                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, MultipleData.message));
                                    errorLogger(MultipleData.message, caseRecord.ID, selectedUnit);
                                }
                                if (MultipleData != null && (MultipleData.Data == null || MultipleData.Data.Size() == 0)) {
                                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, MultipleData.message));
                                }
                                for (UploadMultipleDocController.MultipleDocResponse objData: MultipleData.Data) {
                                    if (objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)) {
                                        CRRUrl = objData.url;
                                        //mapURLS.put(pdfurl.substring( pdfurl.lastIndexOf('/')+1 ),pdfurl);
                                    } else {
                                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'error... ' + objData.PARAM_ID));
                                    }
                                }
                                if (!mapCaseAttachment.containsKey('CRF Form') && CRRUrl != '' && CRRUrl != null) {
                                    srAttach2.Attachment_URL__c = CRRUrl;
                                    srAttach2.Case__c = caseRecord.Id;
                                    srAttach2.Type__c = 'CRF Form';
                                    srAttach2.isValid__c = false;
                                    srAttach2.Name = CRRUrl != '' ? CRRUrl.substring(CRRUrl.lastIndexOf('/') + 1) : '';
                                    listCaseAttachment.add(srAttach2);
                                    crfAttachmentWrapper.srAttachment = srAttach2;
                                    crfAttachmentWrapper.isPresent = true;
                                    caseRecord.Is_CRF_Uploaded__c = true;
                                    System.debug('...crfAttachmentWrapper.isPresent ......1....' + crfAttachmentWrapper.isPresent);

                                }



                            } catch (Exception e) {
                                system.debug('...exception is...' + e + '..line number is ...' + e.getLineNumber());
                            }
                        }
                    }
                    system.debug('...strPOAAttachmentBody...' + strPOAAttachmentBody);
                    if (String.isNotBlank(strPOAAttachmentBody)) {
                        String POAUrl;
                        blob objBlob = extractBody(strPOAAttachmentBody);
                        if (objBlob != null) {
                            objWrapper.base64Binary = EncodingUtil.base64Encode(objBlob);
                        }
                        objWrapper.fileDescription = 'POA';
                        objWrapper.fileId = CaseNumberStr + '-' + String.valueOf(System.currentTimeMillis()) + '.' + strPOAAttachmentName.substringAfterLast('.');
                        objWrapper.fileName = CaseNumberStr + '-' + String.valueOf(System.currentTimeMillis()) + '.' + strPOAAttachmentName.substringAfterLast('.');
                        objWrapper.registrationId = CaseNumberStr;
                        objWrapper.sourceFileName = extractName(strPOAAttachmentName);
                        objWrapper.sourceId = extractName(strPOAAttachmentName);
                        listobjWrapper.add(objWrapper);
                        if (!listobjWrapper.isEmpty()) {
                            SR_Attachments__c srAttach3 = new SR_Attachments__c();
                            try {
                                MultipleData = UploadMultipleDocController.getMultipleDocUrl(listobjWrapper);
                                if (MultipleData != null && MultipleData.status == 'Exception') {
                                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, MultipleData.message));
                                    errorLogger(MultipleData.message, caseRecord.ID, selectedUnit);
                                }
                                if (MultipleData != null && (MultipleData.Data == null || MultipleData.Data.Size() == 0)) {
                                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, MultipleData.message));
                                }
                                for (UploadMultipleDocController.MultipleDocResponse objData: MultipleData.Data) {
                                    if (objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)) {
                                        POAUrl = objData.url;
                                        //mapURLS.put(pdfurl.substring( pdfurl.lastIndexOf('/')+1 ),pdfurl);
                                    } else {
                                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'error... ' + objData.PARAM_ID));
                                    }
                                }
                                if (!mapCaseAttachment.containsKey('Power Of Attorney') && POAUrl != '' && POAUrl != null) {
                                    srAttach3.Attachment_URL__c = POAUrl;
                                    srAttach3.Case__c = caseRecord.Id;
                                    srAttach3.Type__c = 'Power Of Attorney';
                                    srAttach3.Name = POAUrl != '' ? POAUrl.substring(POAUrl.lastIndexOf('/') + 1) : '';
                                    listCaseAttachment.add(srAttach3);
                                    poaAttachmentWrapper.srAttachment = srAttach3;
                                    poaAttachmentWrapper.isPresent = true;
                                    caseRecord.Is_POA_Uploaded__c = true;
                                }
                            } catch (Exception e) {
                                system.debug('...exception is...' + e + '..line number is ...' + e.getLineNumber());
                            }
                        }
                    }
                    system.debug('...additionalAttachmentBody...' + additionalAttachmentBody);
                    if (String.isNotBlank(additionalAttachmentBody)) {
                        String AdditionalAttachmentURL;

                        blob objBlob = extractBody(additionalAttachmentBody);
                        if (objBlob != null) {
                            objWrapper.base64Binary = EncodingUtil.base64Encode(objBlob);
                        }

                        //objWrapper.base64Binary = fileBody;
                        objWrapper.fileDescription = 'Additional Attachment';
                        objWrapper.fileId = CaseNumberStr + '-' + String.valueOf(System.currentTimeMillis()) + '.' + additionalAttachmentName.substringAfterLast('.');
                        objWrapper.fileName = CaseNumberStr + '-' + String.valueOf(System.currentTimeMillis()) + '.' + additionalAttachmentName.substringAfterLast('.');
                        objWrapper.registrationId = CaseNumberStr;
                        objWrapper.sourceFileName = extractName(additionalAttachmentName);
                        objWrapper.sourceId = extractName(additionalAttachmentName);
                        listobjWrapper.add(objWrapper);
                        if (!listobjWrapper.isEmpty()) {
                            SR_Attachments__c srAttach4 = new SR_Attachments__c();
                            try {
                                MultipleData = UploadMultipleDocController.getMultipleDocUrl(listobjWrapper);
                                if (MultipleData != null && MultipleData.status == 'Exception') {
                                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, MultipleData.message));
                                    errorLogger(MultipleData.message, caseRecord.ID, selectedUnit);
                                }
                                if (MultipleData != null && (MultipleData.Data == null || MultipleData.Data.Size() == 0)) {
                                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, MultipleData.message));
                                }
                                for (UploadMultipleDocController.MultipleDocResponse objData: MultipleData.Data) {
                                    if (objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)) {
                                        AdditionalAttachmentURL = objData.url;
                                        //mapURLS.put(pdfurl.substring( pdfurl.lastIndexOf('/')+1 ),pdfurl);
                                    } else {
                                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'error... ' + objData.PARAM_ID));
                                    }
                                }
                                if (!mapCaseAttachment.containsKey(attachmentType) && AdditionalAttachmentURL != '' && AdditionalAttachmentURL != null) {
                                    srAttach4.Attachment_URL__c = AdditionalAttachmentURL;
                                    srAttach4.Case__c = caseRecord.Id;
                                    srAttach4.Type__c = attachmentType;
                                    srAttach4.isValid__c = false;
                                    srAttach4.Name = AdditionalAttachmentURL != '' ? AdditionalAttachmentURL.substring(AdditionalAttachmentURL.lastIndexOf('/') + 1) : '';
                                    listCaseAttachment.add(srAttach4);
                                    additionalAttachmentWrapper.srAttachment = srAttach4;
                                    additionalAttachmentWrapper.isPresent = true;
                                    listAttachmentWrapper.add(additionalAttachmentWrapper);
                                }
                            } catch (Exception e) {
                                system.debug('...exception is...' + e + '..line number is ...' + e.getLineNumber());
                            }
                        }
                    }
                    system.debug('...passportAttachmentBody...' + passportAttachmentBody);
                    if (String.isNotBlank(passportAttachmentBody)) {
                        String PassportAttachURL;
                        blob objBlob = extractBody(passportAttachmentBody);
                        if (objBlob != null) {
                            objWrapper.base64Binary = EncodingUtil.base64Encode(objBlob);
                        }

                        //objWrapper.base64Binary = fileBody;
                        objWrapper.fileDescription = 'Passport';
                        objWrapper.fileId = CaseNumberStr + '-' + String.valueOf(System.currentTimeMillis()) + '.' + passportAttachmentName.substringAfterLast('.');
                        objWrapper.fileName = CaseNumberStr + '-' + String.valueOf(System.currentTimeMillis()) + '.' + passportAttachmentName.substringAfterLast('.');
                        objWrapper.registrationId = CaseNumberStr;
                        objWrapper.sourceFileName = extractName(passportAttachmentName);
                        objWrapper.sourceId = extractName(passportAttachmentName);
                        listobjWrapper.add(objWrapper);
                        if (!listobjWrapper.isEmpty()) {
                            SR_Attachments__c srAttach5 = new SR_Attachments__c();
                            try {
                                MultipleData = UploadMultipleDocController.getMultipleDocUrl(listobjWrapper);
                                if (MultipleData != null && MultipleData.status == 'Exception') {
                                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, MultipleData.message));
                                    errorLogger(MultipleData.message, caseRecord.ID, selectedUnit);
                                }
                                if (MultipleData != null && (MultipleData.Data == null || MultipleData.Data.Size() == 0)) {
                                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, MultipleData.message));
                                }
                                for (UploadMultipleDocController.MultipleDocResponse objData: MultipleData.Data) {
                                    if (objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)) {
                                        PassportAttachURL = objData.url;
                                        //mapURLS.put(pdfurl.substring( pdfurl.lastIndexOf('/')+1 ),pdfurl);
                                    } else {
                                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'error... ' + objData.PARAM_ID));
                                    }
                                }
                                if (!mapCaseAttachment.containsKey('Passport') && PassportAttachURL != '' && PassportAttachURL != null) {
                                    srAttach5.Attachment_URL__c = PassportAttachURL;
                                    srAttach5.Case__c = caseRecord.Id;
                                    srAttach5.Type__c = 'Passport';
                                    srAttach5.isValid__c = false;
                                    srAttach5.Name = PassportAttachURL != '' ? PassportAttachURL.substring(PassportAttachURL.lastIndexOf('/') + 1) : '';
                                    listCaseAttachment.add(srAttach5);
                                    passportAttachmentWrapper.srAttachment = srAttach5;
                                    passportAttachmentWrapper.isPresent = true;
                                    listAttachmentWrapper.add(passportAttachmentWrapper);
                                }
                            } catch (Exception e) {
                                system.debug('...exception is...' + e + '..line number is ...' + e.getLineNumber());
                                strDisplayMessage = e.getMessage();
                            }
                        }
                    }
                    system.debug('...listobjWrapper...' + listobjWrapper);

                    if (listCaseAttachment.size() > 0 && string.isNotBlank(caseRecord.id)) {
                        submitCase();
                        upsert listCaseAttachment;
                        System.debug('...listCaseAttachment...' + listCaseAttachment);
                    }


                    // uploadAttachments();


                }

            } //getBookinUnitDetails();

            //getBookinUnitDetails();
        } catch (Exception e) {
            strDisplayMessage = PenaltyWaiverUtility.SPAN_CROSS + ' Exception generated ' + e.getMessage();
        }

        return null;


    }
    public override list < SelectOption > getBuyerList() {
        // system.debug('....selectedUnit.Booking__c....'+selectedUnit.Booking__c);
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit = [select ID, Booking__c from Booking_Unit__c where id =: bookingUnitId];
        List < Buyer__c > listBuyer = getAllJointBuyerPortal(objBookingUnit.Booking__c, accountId);
        // [Select Id, Account__r.Name from Buyer__c where Booking__c =: selectedUnit.ID];
        if (listBuyer != null && !listBuyer.isEmpty()) {
            list < SelectOption > lstBuyer = new list < SelectOption > ();
            for (Buyer__c buyer: listBuyer) {
                //lstBuyer.add(new SelectOption(buyer.Account__r.Name, buyer.Account__r.Name));
                lstBuyer.add(new SelectOption(buyer.ID, buyer.First_Name__c + ' ' + buyer.Last_Name__c));
                mapBuyer.put(buyer.ID, buyer.First_Name__c + ' ' + buyer.Last_Name__c);
            }
            system.debug('>>>>lstBuyer'+lstBuyer);
            return lstBuyer;
        }

        return null;
    }

    public static list < Buyer__c > getAllJointBuyerPortal(Id bookingId, ID strAccountID) {
        System.debug('...bookingId...' + bookingId + '...strAccountID...' + strAccountID);
        set < String > bookingIds = new set < String > ();
        list < Buyer__c > buyersList = new list < Buyer__c > ();
        /* return new list<Buyer__c>( [ SELECT Id
                                           , Name
                                           , First_Name__c
                                           , Last_Name__c
                                           , Account__c
                                           , Buyer_ID__c
                                           ,Account__r.Name
                                           , Account__r.Party_Id__c
                                        FROM Buyer__c

                                       WHERE (Account__c =: strAccountID
                                         AND Primary_Buyer__c = true)
                                         OR
                                         (Account__c != null
                                         AND Booking__c = :bookingId
                                         AND Primary_Buyer__c = false )] );
                                         */
        for (Buyer__c buyerObj: [SELECT Booking__c FROM Buyer__c WHERE Account__c =: strAccountID AND Primary_Buyer__c = true]) {
            bookingIds.add(buyerObj.Booking__c);
        }
        system.debug('...bookingIds...' + bookingIds);
        if (!bookingIds.isEmpty()) {
            //buyersList = [SELECT Account__c, Buyer_ID__c, Account__r.Party_Id__c, First_Name__c, Last_Name__c, Account__r.Name, Id, Account__r.IsPersonAccount, Primary_Buyer__c FROM Buyer__c WHERE Booking__c IN: bookingIds];
            buyersList = [SELECT Account__c, Buyer_ID__c, Account__r.Party_Id__c, First_Name__c, Last_Name__c, Account__r.Name, Id, Account__r.IsPersonAccount, Primary_Buyer__c FROM Buyer__c WHERE Booking__c =: bookingId];
        }
        system.debug('...buyersList...' + buyersList);
        return buyersList;
    }

    public override void submitCase() {
        try {
            verifyBuyer();
            populateBuyerList();
            system.debug('...isSaveAsDraft....' + isSaveAsDraft);
            populateCaseData();
            Boolean isNewCase = caseRecord.Id == null ? true : false;
            caseRecord.Type = 'NOC For VISA';
            system.debug('1 caseRecord.AccountId : ' + caseRecord.AccountId);
            //if(String.isBlank(strCaseID) && strSRtypeURL == 'NOCVisa'){
            AccID = ApexPages.currentPage().getParameters().get('AccountId');
            strCaseID = ApexPages.currentPage().getParameters().get('CaseID');
            strSRtypeURL = ApexPages.currentPage().getParameters().get('SRType');

            system.debug('1 AccID  : ' + AccID);
            system.debug('1 strCaseID : ' + strCaseID);
            system.debug('1 strSRtypeURL : ' + strSRtypeURL);
            system.debug('>>isSaveAsDraft Submit : '+isSaveAsDraft);
            //if (!string.isBlank(AccID) && !string.isBlank(strCaseID) && !string.isBlank(strSRtypeURL)) {
                system.debug('in submit if');
                system.debug('>>isSaveAsDraft in : '+isSaveAsDraft);
                if (!isSaveAsDraft) {
                    caseRecord.Status = 'Submitted';
                    caseRecord.Origin = 'Portal';
                    upsert caseRecord;
                    strCaseID = caseRecord.id;
                    system.debug('>>>>>strCASEID '+strCaseID);
                    caseRecord = [SELECT Id, OwnerId, Amount_to_be_waived__c, Percent_to_be_waived__c, Status,
                                        Penalty_Category__c, IsPOA__c, Mode_of_Collection__c,
                                        POA_Name__c, Additional_Details__c, POA_Relationship_with_Buyer__c,
                                        Purpose_of_POA__c, POA_Expiry_Date__c, POA_Issued_By__c,
                                        Is_CRF_Uploaded__c, Origin, Buyer_For_NOC__c, Is_NOC_For_Visa_Generated__c FROM Case
                                WHERE Id =:caseRecord.id];
                    Task objTask = TaskUtility.getTask((SObject)caseRecord,
                    'Verify Case Details', 'CRE', 'NOC For Visa',
                    system.today().addDays(1));
                    objTask.OwnerId = Label.DefaultCaseOwnerId;
                    //objTask.Status = 'Completed';
                    insert objTask;
                    list < Database.SaveResult > listSRBookingUnit = saveNewSRBookingUNIT(bookingUnitId);
                } else {
                    caseRecord.Status = 'new';
                    caseRecord.Origin = 'Portal';
                    upsert caseRecord;
                    strCaseID = caseRecord.id;
                    system.debug('>>>>>strCASEID '+strCaseID);
                    list < Database.SaveResult > listSRBookingUnit = saveNewSRBookingUNIT(bookingUnitId);
                }

            //}

            //strDisplayMessage = PenaltyWaiverUtility.SPAN_TICK + 'NOC Visa request has been raised.';

            // }
            System.debug('...value of showNOCVisabutton...' + showNOCVisabutton);
        } catch (Exception e) {
            strDisplayMessage = e.getMessage();
        }
    }
}