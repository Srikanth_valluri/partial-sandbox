/**************************************************************************************************
* Name               : CC_ConvertInquiry
* Description        : This is the custom code class for converting the inquiries associated with buyers          
* Created Date       : 30/04/2017                                                                 
* Created By         : NSI - Kaavya Raghuram                                                       
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE                                                              
* 1.0         NSI - Kaavya    30/04/2017                                                        
**************************************************************************************************/
global without sharing class CC_ConvertInquiry implements NSIBPM.CustomCodeExecutable {

    
    public String EvaluateCustomCode(NSIBPM__Service_Request__c SR, NSIBPM__Step__c step) {
        system.debug('CC==== convertinquiry');
        String retStr = 'Success';
        Set<Id> inquiryIdsSet= new Set<Id>();
        Set<Id> agentinquiryIdsSet= new Set<Id>();
        try{ 
          for(Buyer__c b:[select id,Inquiry__c, Booking__r.Deal_SR__r.Deal_Type__c from Buyer__c where Inquiry__c!=null AND Booking__r.Deal_SR__c=:step.NSIBPM__SR__c]){
            if (b.Booking__r.Deal_SR__r.Deal_Type__c != 'Agent Portal Deal')
                inquiryIdsSet.add(b.Inquiry__c);
            else {
                agentinquiryIdsSet.add(b.Inquiry__c);
            }
          } 
          
          
          system.debug('II==== '+inquiryIdsSet);
          if(!inquiryIdsSet.isEmpty()){
               if (!Test.isRunningTest ())
                   InquiryConversionProcess.convertInquiry(inquiryIdsSet);
            } 
            
            if(!agentinquiryIdsSet.isEmpty()){
                CC_ConvertInquiry.convertInquiry (step.NSIBPM__SR__c);
                
            }         
        
         }catch (Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
            Log__c objLog = new Log__c(Type__c = 'Error converting Inquiry',Description__c ='Ids=='+step.NSIBPM__SR__c+'-Line No===>'+e.getLineNumber()+'---Message==>'+e.getMessage() );
            insert objLog;
         }
        
        return retStr;
    }
    @Future (callout = TRUE)
    public static void convertInquiry (id SRID) {
        IPMS_Integration_Settings__mdt settings = new IPMS_Integration_Settings__mdt ();
        settings = [SELECT Endpoint_URL__c FROM IPMS_Integration_Settings__mdt WHERE Label = 'Convert_Inquiry' AND Endpoint_URL__c != NULL LIMIT 1];
        HttpRequest req = new HTTPRequest ();
        req.setEndpoint (settings.Endpoint_URL__c +'/services/apexrest/UpdateInquiry');
        req.setMethod ('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setBody ('{"srId" : "'+SRID+'"}');
        
        Http h = new HTTP ();
        System.Debug (req);
        HTTPresponse res = new HTTPResponse ();
        if (!Test.isRunningTest ())
            res = h.send (req);
        
        System.debug (res.getbody ());
    }

}