/**********************************************************************************************
 * @Name              : InventoryRequestApproveRejectTest
 * @Description       : Test Class for InventoryRequestApproveRejectController
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         QBurst         19/08/2020        Created
***********************************************************************************************/
@istest
public with sharing class InventoryRequestApproveRejectTest{


    static testmethod void InventoryRequestApproveRejectTestMethod(){
        
        Profile prof = [SELECT Id FROM Profile WHERE Name LIKE '%Property Consultant%' LIMIT 1];
        User user = new User();
        user.FirstName = 'Jithesh';
        user.LastName = 'Vasudevan';
        user.profileId = prof.id;
        user.username = 'testuseduser@test.com';
        user.email = 'testuseduser@test.com';
        user.Alias = 'jit';
        user.TimeZoneSidKey = 'Asia/Dubai';
        user.LocaleSidKey = 'en_GB';
        user.EmailEncodingKey = 'UTF-8';
        user.LanguageLocaleKey = 'en_US';
        insert user;           
        Account acc = new Account ();
        acc.LastName = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '767676';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        insert acc;
        
        DAMAC_Central_Push_Notifications__c obj = new DAMAC_Central_Push_Notifications__c();
        obj.Email__c = 'test@test.com';
        obj.Password__c = '1232112';
        obj.device_source__c = 'test';
        obj.device_os_version__c = 'test';
        obj.app_version__c = '1.2';
        obj.device_model__c = 'test';
        obj.Api_Token__c = 'test';
        obj.Project_connect_API_Token__c = 'test';
        obj.app_id__c = 1;
        obj.is_authorization_required__c = false;
        insert obj;
        
        Deal_Exception_Request__c der = new Deal_Exception_Request__c();
        der.Token_Amount_AED__c = 40000;        
        der.agency__c = acc.Id;
        der.Token_Transfer__c = 'Yes';
        der.RecordTypeId = Schema.SObjectType.Deal_Exception_Request__c.getRecordTypeInfosByName().get('Request Inventory').getRecordTypeId();
        der.Booking_Units__c = 'BU-000117,BU-000118';
        der.RM_Comments__c = 'RM Comments'; 
        der.Sales_Admin_Comments__c = 'Sales Admin Comments';
        der.Sales_Admin_Manager_Comments__c = 'Sales Admin Manager Comments';
        der.RM__c = user.Id;
        der.Contains_Restricted_Inventories__c = true;
        der.Status__c = 'Awaiting MIS HOD Approval';
        der.Inventory_Request_Filters__c ='UnitType:FURNISHED APARTMENT,HOTEL;MinPrice:1000';
        insert der;
        
        Location__c loc = new Location__c();
        loc.Name = 'VCT';
        loc.Location_ID__c = '270791';
        insert loc;

        Inventory__c inv = new Inventory__c();
        inv.Marketing_Name__c = 'BAIT AL ASEEL';        
        inv.Unit_Location__c = loc.Id;
        inv.Status__c = 'Released';                       
        insert inv;
        
        Deal_Exception_Unit__c deu = new Deal_Exception_Unit__c();
        deu.Deal_Exception_Request__c = der.Id;
        deu.Inventory__c = inv.Id;
        insert deu;

        update der;
        InventoryRequestApproveRejectController requestApproveReject = new InventoryRequestApproveRejectController();       
        requestApproveReject.reqId = der.Id; 
        requestApproveReject.isApproval = true;
        requestApproveReject.recipientType = 'recipient';
        Deal_Exception_Request__c derNew = new Deal_Exception_Request__c();
        derNew = requestApproveReject.req;
        requestApproveReject.isApproval = false;
        derNew = requestApproveReject.req;
    }
}