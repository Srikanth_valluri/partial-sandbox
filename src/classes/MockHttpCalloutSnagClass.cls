/********************************************************************************
* Description -         *
*                                                                               *
* Version            Date            Author                    Description      *
* 1.0                17/01/2019      Arjun Khatri              Initial Draft    *
********************************************************************************/

public class MockHttpCalloutSnagClass implements HttpCalloutMock{
    
    Static integer index;
    public MockHttpCalloutSnagClass(integer i) {
        index = i;
    }
    public HTTPResponse respond(HTTPRequest req){
        //System.assertEquals('https://www.google.com/recaptcha/api/siteverify', req.getEndpoint());
        System.debug('**** MockHttpCalloutSnagClass ***** : ' + index);
        HttpResponse res;
        if(index == 1) {
            System.assertEquals('GET', req.getMethod());
            res = new HttpResponse();
            res.setStatus('Success');
            res.setStatusCode(200);
            res.setBody('1590560');
        }
        else if(index == 2) {
            System.assertEquals('GET', req.getMethod());
            res = new HttpResponse();
            //res.setStatus('Success');
            res.setStatusCode(400);
            res.setBody('1590560');
        }
        return res;
    }
}