public class StaffTargetsController {
    public class Target{
        public integer index{get; set;}
        public Target__c tar{get; set;}
        public string searchText{get; set;}
        public Target(integer i, Target__c t, string s){
            tar = t;
            index = i;
            searchText = s;
        }
    }
    
    public list<Target__c> existing = new list<Target__c>();
    public Target__c filterTarget{get; set;}
    public integer selIndex{get; set;}
    public string searchText{get; set;}
    public string rowIndex{get; set;}
    public list<Target> lstTargets{get; set;}
    public string usersJSON{get; set;}
    
    public map<string, User> mpUsers{get; set;}
    public map<Id, User> mpRMUsers{get; set;}
    public string StaffRecordTyeId = Schema.SObjectType.Target__c.getRecordTypeInfosByName().get('Staff').getRecordTypeId();
    public blob fileBody{get; set;}
    
    public map<string, User> mpAllUsers = new Map<string, User>();
    
    public StaffTargetsController(){
        filterTarget = new Target__c();
        prepareData();
    }

    public void prepareData(){
        lstTargets = new list<Target>();
        string monthName = System.now().format('MMMM');
        
        if(filterTarget.Year__c == null)
            filterTarget.Year__c = string.valueOf(System.today().year());
        if(filterTarget.Month__c == null)
            filterTarget.month__c = monthName;
        
        for(User u : [Select id, name, IPMS_Employee_ID__c, HR_Employee_ID__c 
                        from User 
                        where Profile.Name != 'Property Consultant' and accountId = null
                        order by name asc]){
            mpAllUsers.put(u.IPMS_Employee_ID__c, u);                
        }
        
        mpRMUsers = new Map<Id, User>();
        mpUsers = new Map<string, User>();
        for(User u : [Select id, name, HR_Employee_ID__c, HOD_IPMS_Id__c, DOS_IPMS_Id__c, HOS_IPMS_Id__c
                        from User 
                        where isActive = true and Profile.Name = 'Property Consultant'
                        order by name asc]){
            string searchText = u.name+' - '+(u.HR_Employee_ID__c == null ? 'NA': u.HR_Employee_ID__c);
            mpUsers.put(searchText, u);
            lstTargets.add(new Target(lstTargets.size(), new Target__c(RecordTypeId = StaffRecordTyeId, 
                                                                        User__c = u.Id,
                                                                        DOS_User__c = mpAllUsers.containsKey(u.DOS_IPMS_Id__c) ? mpAllUsers.get(u.DOS_IPMS_Id__c).Id : null,
                                                                        HOS_USer__c = mpAllUsers.containsKey(u.HOS_IPMS_Id__c) ? mpAllUsers.get(u.HOS_IPMS_Id__c).Id : null, 
                                                                        HOD_User__c = mpAllUsers.containsKey(u.HOD_IPMS_Id__c) ? mpAllUsers.get(u.HOD_IPMS_Id__c).Id : null), searchText));
            mpRMUsers.put(u.Id, u);
        }
        usersJSON = JSON.serialize(mpUsers.keyset());
        
        
                            
        filterData();
    }
    
    public void removeRow(){
        system.debug('>>>>>>>selIndex>>>>>>>>>'+selIndex);
        system.debug('>>>>>>>lstTargets>>>>>>>>>'+lstTargets.size());
        if(selIndex != null){
            list<Target__c> lst2Delete = new list<Target__c>();
            Target targ2Delete = lstTargets[selIndex];
            if(targ2Delete.tar.Id != null)
                lst2Delete.add(targ2Delete.tar);
                
            lstTargets.remove(selIndex);
            list<Target> targetsCopy = lstTargets.clone();
            lstTargets = new list<Target>();
            if(!targetsCopy.isEmpty()){
                for(target targ : targetsCopy){
                    lstTargets.add(new Target(lstTargets.size(), targ.tar, targ.searchText));
                }
            }
            else 
                lstTargets.add(new Target(lstTargets.size(), new Target__c(RecordTypeId = StaffRecordTyeId),''));        
            
            if(!lst2Delete.isEmpty())
                delete lst2Delete;
        }
    }
    
    public void saveTargets(){
        if(filterTarget.Month__c == null || filterTarget.Year__c == null)
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.Fatal, 'Month and Year are mandatory for saving targets.'));
        else{
            list<Target__c> lst2Insert = new list<Target__c>();
            for(Target targ: lstTargets){
                if(targ.tar.Target__c > 0){
                    targ.tar.Month__c = filterTarget.Month__c;
                    targ.tar.Year__c = filterTarget.Year__c;
                    targ.tar.Uniq__c = string.valueOf(targ.tar.User__c).substring(0,15)+'-'+targ.tar.Month__c+'-'+targ.tar.Year__c;
                    lst2Insert.add(targ.tar);
                }
            }
            list<Database.UpsertResult> results = database.upsert(lst2Insert, Target__c.Uniq__c);
            for(Database.UpsertResult res : results){
                System.debug('>>>>>>Errors>>>>>>>>'+res.getErrors());
                System.debug('>>>>>>Success>>>>>>>>'+res.isSuccess());
            }   
            
            prepareData();
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.info, 'Targets saved successfully.'));
        }
    }
    
    public void readCSVFile(){
        if(fileBody == null || filebody.size() == 0){
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.Fatal, 'Please upload file to process.'));
        }
        else{
            string DataAsString = fileBody.toString();
            list<string> csvLines = DataAsString.split('\n');
            lstTargets = new List<Target>();
            integer rowCount = 0;
            for(integer i=1; i< csvLines.size(); i++){
                if(csvLines[i] != null){
                    list<string> vals = csvLines[i].split(',');
                    System.debug('>>>>>vals>>>>>>>'+vals);
                    Target__c tar = new Target__c(RecordTypeId = StaffRecordTyeId);
                    tar.User__c = vals[0].trim();
                    //tar.DOS_User__c = vals[4] != '' && vals[4] != null && mpAllUsers.containsKey(vals[4].trim()) ? mpAllUsers.get(vals[4].trim()).Id : null;
                    //tar.HOS_User__c = vals[7] != '' && vals[7] != null && mpAllUsers.containsKey(vals[7].trim()) ? mpAllUsers.get(vals[7].trim()).Id : null;
                    //tar.HOD_User__c = vals[10] != '' && vals[10] != null && mpAllUsers.containsKey(vals[10].trim()) ? mpAllUsers.get(vals[10].trim()).Id : null;
                    tar.DOS_User__c = (mpAllUsers.containsKey(mpRMUsers.get(tar.User__c).DOS_IPMS_Id__c) ? mpAllUsers.get(mpRMUsers.get(tar.User__c).DOS_IPMS_Id__c).Id : null);
                    tar.HOS_User__c = (mpAllUsers.containsKey(mpRMUsers.get(tar.User__c).HOS_IPMS_Id__c) ? mpAllUsers.get(mpRMUsers.get(tar.User__c).HOS_IPMS_Id__c).Id : null);
                    tar.HOD_User__c = (mpAllUsers.containsKey(mpRMUsers.get(tar.User__c).HOD_IPMS_Id__c) ? mpAllUsers.get(mpRMUsers.get(tar.User__c).HOD_IPMS_Id__c).Id : null);
                    tar.Target__c = vals[13] != '' ? decimal.valueOf(vals[13].trim()) : 0;
                    tar.Month__c = filterTarget.Month__c;
                    tar.Year__c = filterTarget.Year__c;
                    
                    User RMUser = mpRMUsers.get(vals[0].trim());
                    string searchKey = RMUser.name+' - '+(RMUser.HR_Employee_ID__c == null ? 'NA': RMUser.HR_Employee_ID__c);
                    lstTargets.add(new Target(lstTargets.size(), tar, searchKey));
                }
            }
        } 
    }
    
    public void filterData(){
        System.debug('>>>>>>>>>>>>>>>'+filterTarget.Month__c);
        System.debug('>>>>>>>>>>>>>>>'+filterTarget.Year__c);
        
        if(filterTarget.Month__c != null && filterTarget.Year__c != null){
            list<Target__c> existing = [Select id, User__c, DOS_User__c, HOS_User__c, HOD_User__c, Target__c from Target__c 
                            where Month__c =: filterTarget.Month__c and Year__c =: filterTarget.Year__c 
                            and RecordTypeId =: StaffRecordTyeId];
            if(!existing.isEmpty()){
                map<string, Target__c> mpExisting = new map<string, Target__c>();
                for(Target__c tar: existing){
                    mpExisting.put(tar.User__c, tar);
                }

                lstTargets = new list<Target>();
                mpUsers = new Map<string, User>();

                for(User u : [Select id, name, HR_Employee_ID__c, ManagerId, HOD_IPMS_Id__c, DOS_IPMS_Id__c, HOS_IPMS_Id__c
                                from User 
                                where isActive = true and Profile.Name = 'Property Consultant'
                                order by name asc]){
                    string searchText = u.name+' - '+(u.HR_Employee_ID__c == null ? 'NA': u.HR_Employee_ID__c);
                    mpUsers.put(searchText, u);
                    if(!mpExisting.containsKey(u.Id))
                        lstTargets.add(new Target(lstTargets.size(), new Target__c(RecordTypeId = StaffRecordTyeId, User__c = u.Id,
                                                                        DOS_User__c = mpAllUsers.containsKey(u.DOS_IPMS_Id__c) ? mpAllUsers.get(u.DOS_IPMS_Id__c).Id : null,
                                                                        HOS_USer__c = mpAllUsers.containsKey(u.HOS_IPMS_Id__c) ? mpAllUsers.get(u.HOS_IPMS_Id__c).Id : null, 
                                                                        HOD_User__c = mpAllUsers.containsKey(u.HOD_IPMS_Id__c) ? mpAllUsers.get(u.HOD_IPMS_Id__c).Id : null), searchText));
                    else
                        lstTargets.add(new Target(lstTargets.size(), mpExisting.get(u.Id), searchText));             
                }

                /*
                for(Target targ: lstTargets){
                    if(targ.tar.User__c != null && mpExisting.containsKey(targ.tar.User__c)){
                        targ.tar = mpExisting.get(targ.tar.User__c);
                    }
                }
                */
            }
            else{
                lstTargets = new list<Target>();
                mpUsers = new Map<string, User>();
                for(User u : [Select id, name, HR_Employee_ID__c, ManagerId, HOD_IPMS_Id__c, DOS_IPMS_Id__c, HOS_IPMS_Id__c
                                from User 
                                where isActive = true and Profile.Name = 'Property Consultant'
                                order by name asc]){
                    string searchText = u.name+' - '+(u.HR_Employee_ID__c == null ? 'NA': u.HR_Employee_ID__c);
                    mpUsers.put(searchText, u);
                    lstTargets.add(new Target(lstTargets.size(), new Target__c(RecordTypeId = StaffRecordTyeId, User__c = u.Id,
                                                                        DOS_User__c = mpAllUsers.containsKey(u.DOS_IPMS_Id__c) ? mpAllUsers.get(u.DOS_IPMS_Id__c).Id : null,
                                                                        HOS_USer__c = mpAllUsers.containsKey(u.HOS_IPMS_Id__c) ? mpAllUsers.get(u.HOS_IPMS_Id__c).Id : null, 
                                                                        HOD_User__c = mpAllUsers.containsKey(u.HOD_IPMS_Id__c) ? mpAllUsers.get(u.HOD_IPMS_Id__c).Id : null), searchText));
                }
            }
        }
        
    }
    
    public void userSelected(){
        System.debug('>>>>>>>>>>>>'+rowIndex);
        System.debug('>>>>>>>>>>>>'+searchText);
        
        User userRecord = mpUsers.get(searchText);
        integer index = integer.valueOf(rowIndex);
        System.debug('>>>>>>userRecord>>>>>>>'+userRecord);
        lstTargets[index].tar.User__c = userRecord.Id;
        lstTargets[index].searchText = searchText;
    }
}