@isTest
public class InvoiceDetailsTest {
    
    @isTest
    public static void test_PostInvoiceDetails1() {
        
         // create Account
        Account objAcc = new Account(Name = 'test', Party_Id__c = '43434');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        DP_Invoices__c obj=new DP_Invoices__c();
        obj.Accounts__c = objAcc.id;
        obj.BookingUnits__c = objBookingUnit.id;
        obj.Cover_Letter__c = 'test';
        obj.SOA__c= 'test';
        obj.Trxn_Number__c= 'testTrxnNumber';
        obj.TAX_Invoice__c= 'test';
        obj.COCD_Letter__c= 'test';
        obj.Other_Language_TAX_Invoice__c = 'test';
        obj.EmailSent__c = false;
        obj.Invoice_URLs__c = 'test';
        obj.Registration_ID__c = '12345';
        obj.Call_Type__c = 'testCallType';
        insert obj;
        
        Test.startTest();
        
        InvoiceDetails.PostInvoiceDetails(system.today(), '12345', 'JNU/ABC/234', '43434'
                                        , 'testCallType', '1000', system.today().addDays(3)
                                        , 'testCxName', 'testDescription', 'NO'
                                        , 'testTrxnNumber1', 'testBankName', 'testAccountName'
                                        , 'testAccountNo', 'testIBAN', 'testBranch', 'testSwiftcode'
                                        , 'test', 'Date Based');
        Test.stopTest();
    }
    
    @isTest
    public static void test_PostInvoiceDetails2() {
        
         // create Account
        Account objAcc = new Account(Name = 'test', Party_Id__c = '43434');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        DP_Invoices__c obj=new DP_Invoices__c();
        obj.Accounts__c = objAcc.id;
        obj.BookingUnits__c = objBookingUnit.id;
        obj.Cover_Letter__c = 'test';
        obj.SOA__c= 'test';
        obj.Trxn_Number__c= 'testTrxnNumber';
        obj.TAX_Invoice__c= 'test';
        obj.COCD_Letter__c= 'test';
        obj.Other_Language_TAX_Invoice__c = 'test';
        obj.EmailSent__c = false;
        obj.Invoice_URLs__c = 'test';
        obj.Registration_ID__c = '12345';
        //obj.Call_Type__c = 'testCallType';
        insert obj;
        
        Test.startTest();
        
        InvoiceDetails.PostInvoiceDetails(system.today(), '12345', 'JNU/ABC/234', '43434'
                                        , 'testCallType', '1000', system.today().addDays(3)
                                        , 'testCxName', 'testDescription', 'Yes'
                                        , 'testTrxnNumber', 'testBankName', 'testAccountName'
                                        , 'testAccountNo', 'testIBAN', 'testBranch', 'testSwiftcode'
                                        , 'test', 'Date Based');
        Test.stopTest();
    }
}