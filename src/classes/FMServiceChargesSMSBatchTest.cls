/************************************************************************************
* Description - Test Class for class FMServiceChargesSMSBatch                       *
*                                                                                   *
* Version   Date            Author              Description                         *
* 1.0       05/03/2019      Arjun Khatri        Initial Draft.                      * 
*************************************************************************************/
@isTest
public class FMServiceChargesSMSBatchTest {

    @isTest
    public static void testAms_UAE1() {
         //Create Customer record
        Id businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(Name = 'Test Account',
                                    recordtypeid = businessRecTypeId,
                                    email__c = 'tt@tfdst.com',
                                    Mobile__c = '2323232',
                                    ZBEmailStatus__c = 'Valid');
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].FM_Outstanding_Amount__c = '2151';
        listBUs[0].Property_Country__c = 'United Arab Emirates';
        listBUs[0].Email__c = '321test@test.com';
        listBUs[0].Handover_Flag__c = 'Y';
        listBUs[0].Registration_Status_Code__c= 'MM';
        //listBUs[0].Registration_Status__c= 'executed';
        listBUs[0].SC_From_RentalIncome__c = false;
        insert listBUs;
        system.debug('listBUs== ' + listBUs);
        Test.setMock(HttpCalloutMock.class, new FMServiceChargesHttpMock(2));
        Test.StartTest();
        DataBase.executeBatch(new FMServiceChargesSMSBatch(), 1);
        Test.StopTest();
    }
    @isTest
    public static void testSms_UAE2() {
         //Create Customer record
         Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        //Create Customer record
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__pc = '123123123';
        
        /*Account objAcc = new Account(FirstName='Test FirstName'
                                    , LastName='Test LastName'
                                    , Email__pc = 'test123@tmail.com'
                                    , recordTypeId = personAccRTId
                                    //, Mobile_Phone_Encrypt__pc  = '4342342'
                                    ,  = 'Valid');*/
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].FM_Outstanding_Amount__c = '1';
        listBUs[0].Email__c = '321test@test.com';
        listBUs[0].Property_Country__c = 'United Arab Emirates';
        listBUs[0].Handover_Flag__c = 'Y';
        listBUs[0].Registration_Status_Code__c= 'MM';
        //listBUs[0].Registration_Status__c= 'executed';
        listBUs[0].SC_From_RentalIncome__c = false;
        insert listBUs;
        system.debug('listBUs== ' + listBUs);
        Test.setMock(HttpCalloutMock.class, new FMServiceChargesHttpMock(2));
        Test.StartTest();
        DataBase.executeBatch(new FMServiceChargesSMSBatch(), 1);
        Test.StopTest();
    }

    @isTest
    public static void testQatar() {
         //Create Customer record
        Id businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(Name = 'Test Account',
                                    recordtypeid = businessRecTypeId,
                                    email__c = 'tt@tfdst.com',
                                    Mobile__c = '4234234',
                                    //Mobile_Phone_Encrypt__pc = '4234234',
                                    ZBEmailStatus__c = 'Valid');
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].FM_Outstanding_Amount__c = '42542';
        listBUs[0].Property_Country__c = 'Qatar';
        listBUs[0].Email__c = '321test@test.com';
        listBUs[0].Handover_Flag__c = 'Y';
        listBUs[0].Registration_Status_Code__c= 'MM';
        //listBUs[0].Registration_Status__c= 'executed';
        listBUs[0].SC_From_RentalIncome__c = false;
        insert listBUs;
        system.debug('listBUs== ' + listBUs);
        Test.setMock(HttpCalloutMock.class, new FMServiceChargesHttpMock(2));
        Test.StartTest();
        DataBase.executeBatch(new FMServiceChargesSMSBatch(), 1);
        Test.StopTest();
    }
    @isTest
    public static void testEmail_Lebanon() {
         //Create Customer record
        Id businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(Name = 'Test Account',
                                    recordtypeid = businessRecTypeId,
                                    email__c = 'tt@tfdst.com',
                                    Mobile__c = '4234234',
                                    //Mobile_Phone_Encrypt__pc = '4234234',
                                    ZBEmailStatus__c = 'Valid');
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].FM_Outstanding_Amount__c = '454542';
        listBUs[0].Property_Country__c = 'Lebanon';
        listBUs[0].Email__c = '321test@test.com';
        listBUs[0].Handover_Flag__c = 'Y';
        listBUs[0].Registration_Status_Code__c= 'MM';
        //listBUs[0].Registration_Status__c= 'executed';
        listBUs[0].SC_From_RentalIncome__c = false;
        insert listBUs;
        system.debug('listBUs== ' + listBUs);
        Test.setMock(HttpCalloutMock.class, new FMServiceChargesHttpMock(2));
        Test.StartTest();
        DataBase.executeBatch(new FMServiceChargesSMSBatch(), 1);
        Test.StopTest();
    }
    
    
}