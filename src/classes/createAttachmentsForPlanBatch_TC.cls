@isTest
public class createAttachmentsForPlanBatch_TC {
    static testmethod void myUnitTest() {
        ID recordTypeId = Schema.SObjectType.inventory__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        inventory__c inv1 = new inventory__c ();
        inv1.RecordtypeId = recordTypeId;
        inv1.Status__c = 'Released';
        inv1.unit_plan__c = 'https://test.com';
        inv1.floor_plan__c = 'https://test.com';
        inv1.plot_plan__c = 'https://test.com';
        
        inv1.CurrencyIsoCode = 'EUR';
        insert inv1;
        Update inv1;
        
        Test.setMock(HttpCalloutMock.class, new createAttachmentsForPlansMock ());

        createAttachmentsForPlanBatch obj = new createAttachmentsForPlanBatch (null);
        DataBase.executeBatch(obj);

    }
    static testmethod void myUnitTest1() {
        ID recordTypeId = Schema.SObjectType.inventory__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        inventory__c inv1 = new inventory__c ();
        inv1.RecordtypeId = recordTypeId;
        inv1.Status__c = 'Released';
        inv1.unit_plan__c = 'https://test.com';
        inv1.floor_plan__c = 'https://test.com';
        inv1.plot_plan__c = 'https://test.com';        
        inv1.CurrencyIsoCode = 'EUR';
        insert inv1;
        Test.setMock(HttpCalloutMock.class, new createAttachmentsForPlansMock ());

        createAttachmentsForPlanBatch obj = new createAttachmentsForPlanBatch (null);
        DataBase.executeBatch(obj);

    }
    
    static testmethod void myUnitTest2 () {
        ID recordTypeId = Schema.SObjectType.inventory__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        inventory__c inv1 = new inventory__c ();
        inv1.RecordtypeId = recordTypeId;
        inv1.Status__c = 'Released';
        inv1.unit_plan__c = 'https://test.com';
        inv1.floor_plan__c = 'https://test.com';
        inv1.plot_plan__c = 'https://test.com';        
        inv1.CurrencyIsoCode = 'EUR';
        insert inv1;
        Test.setMock(HttpCalloutMock.class, new createAttachmentsForPlansMock ());

        createAttachmentsForPlanBatch obj = new createAttachmentsForPlanBatch (null);
        DataBase.executeBatch(obj);

    }
}