/*
 * Description : This class is used to Schedule FMCallingListCreationBatch batch at every 3 
 *                                                      
 */
global class ScheduleFMCallingListCreationBatch implements Schedulable {
  global void execute(SchedulableContext sc) {
      FMCallingListCreationBatch batchInstance = new FMCallingListCreationBatch(); 
      database.executebatch(batchInstance,5);
  }
}