public class ThirdPartyController {
    Static Id caseId;
    public ThirdPartyController(ApexPages.StandardController controller) {
        Case objCase = ( Case )controller.getRecord();
        caseId = objCase.Id;
    }
    
    public static void generate3rdParty() {
        if( caseId != null ) {
            String csName = '3rd party';
            Riyadh_Rotana_Drawloop_Doc_Mapping__c cs = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance(csName);
            System.debug('cs--'+cs);   
                    try {
                        DrawloopDocGen.generateDoc( cs.Drawloop_Document_Package_Id__c
                                                   , cs.Delivery_Option_Id__c
                                                   , caseId);
                        ApexPages.addmessage(
                            new ApexPages.message( ApexPages.severity.INFO, 
                                                  'Third Party letter generated!')
                        );                                
                    } 
                    catch (Exception ex ) {
                        ApexPages.addmessage(
                            new ApexPages.message( ApexPages.severity.ERROR, 
                                                  ex.getMessage() )
                        );
                    }
                    
          
        }//End CaseId if
        else {
            ApexPages.addmessage(
                new ApexPages.message( ApexPages.severity.ERROR, 
                                      'Case Id not found!')
            );
        }
    }
}