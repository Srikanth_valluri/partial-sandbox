/* * * * * * * * * * * * * *
*  Class Name:   MileStoneEventsWrapperTest
*  Purpose:      Unit test class for MileStoneEventsWrapper class
*  Author:       Hardik Mehta - ESPL
*  Company:      ESPL
*  Created Date: 21-Nov-2017
*  Type:         Test Class
* * * * * * * * * * * * */
@isTest
public Class MileStoneEventsWrapperTest{
  /* * * * * * * * * * * * *
  *  Method Name:  wrapperTest
  *  Purpose:      This method is used to check the Wrapper class functionality
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */      
    public static testMethod void wrapperTest(){
                
        MileStoneEventsWrapper.MileStoneEvents objMileStoneEvents = 
            new MileStoneEventsWrapper.MileStoneEvents();
            
            objMileStoneEvents.message='ABC';
            objMileStoneEvents.Status = 'Active';
            objMileStoneEvents.customErrorMsg = 'Error';
        
        MileStoneEventsWrapper.MILESTONE_TAB_TYPE objMILESTONE_TAB_TYPE = 
            new MileStoneEventsWrapper.MILESTONE_TAB_TYPE();
            
            objMILESTONE_TAB_TYPE.MILESTONE_EVENT = 'Event1';
            objMILESTONE_TAB_TYPE.ARABIC_MILESTONE_EVENT = 'Event2';
            
            //MileStoneEventsWrapper obj = new MileStoneEventsWrapper ();
            String s = '{"strTermID":"145486","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"98822.84","strLineID":"808896","strInvoiceAmount":"98860.8","strDueAmount":"37.96","percentValue":"24","paymentDate":"06-DEC-2015","name":"Date","mileStoneEventArabic":"فورا","mileStoneEvent":"Immediate","isReceiptPresent":true,"installment":"DP","description":"DEPOSIT","blnNewTerm":null},{"strTermID":"145487","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":null,"strLineID":"808897","strInvoiceAmount":null,"strDueAmount":null,"percentValue":"0","paymentDate":"","name":"Date","mileStoneEventArabic":null,"mileStoneEvent":null,"isReceiptPresent":true,"installment":"I001","description":"1ST INSTALMENT","blnNewTerm":null},{"strTermID":"145488","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"0","strLineID":"808898","strInvoiceAmount":"49430.4","strDueAmount":"49430.4","percentValue":"10","paymentDate":"03-JUN-2016","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I002","description":"2ND INSTALMENT","blnNewTerm":null},{"strTermID":"145489","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"0","strLineID":"808899","strInvoiceAmount":"49430.4","strDueAmount":"49430.4","percentValue":"10","paymentDate":"02-AUG-2016","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I003","description":"3RD INSTALMENT","blnNewTerm":null},{"strTermID":"145490","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"0","strLineID":"808900","strInvoiceAmount":"49430.4","strDueAmount":"49430.4","percentValue":"10","paymentDate":"30-NOV-2016","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I004","description":"4TH INSTALMENT","blnNewTerm":null},{"strTermID":"145491","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":null,"strLineID":"808901","strInvoiceAmount":null,"strDueAmount":null,"percentValue":"50","paymentDate":"30/11/2017","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I005","description":"5TH INSTALMENT","blnNewTerm":null}';
            MileStoneEventsWrapper.parse(s);        
    }
}