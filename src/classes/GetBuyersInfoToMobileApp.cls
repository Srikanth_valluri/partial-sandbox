@RestResource(urlMapping='/GetBuyersInfoToMobileApp/*')
 Global class GetBuyersInfoToMobileApp
 {
     @HtTPPost
    Global static list<Buyer__c> GetBuyersInfoToMobileAp(String bookingId)
    {
       list<Buyer__c> buyerList;
       if(!String.isBlank(bookingId))
       {
         buyerList=[SELECT Id, 
                                         Name, 
                                         First_Name__c, 
                                         Last_Name__c, 
                                         Account__c, 
                                         Buyer_ID__c,
                                         Booking__c, 
                                         Primary_Buyer__c,
                                         Account__r.Party_Id__c,
                                         Account__r.Passport_Expiry_Date__c,
                                         Account__r.Passport_Issue_Date__c,
                                         Account__r.Passport_Issue_Place__c,
                                         Account__r.Passport_Number__c,
                                         Account__r.Passport_Expiry_Date__pc,
                                          Account__r.Passport_Issue_Date__pc,
                                           Account__r.Passport_Issue_Place__pc,
                                            Account__r.Passport_Number__pc
                                            
                                         
                                    FROM Buyer__c
                                   WHERE Account__c != null 
                                     AND Booking__c = :bookingId
                                     AND Primary_Buyer__c = false];
       }
       return buyerList;
    }
 }