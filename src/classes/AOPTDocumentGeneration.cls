//Generated by wsdl2apex

public class AOPTDocumentGeneration {
    public class DocGenerationResponse_element {
        public String return_x;
        private String[] return_x_type_info = new String[]{'return','http://generation.doc.com',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://generation.doc.com','true','true'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class DocGeneration_element {
        public String TemplateName;
        public AOPTDocumentGenerationXsd.DocGenDTO objDocGenDTO;
        private String[] TemplateName_type_info = new String[]{'TemplateName','http://generation.doc.com',null,'0','1','true'};
        private String[] objDocGenDTO_type_info = new String[]{'objDocGenDTO','http://generation.doc.com',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://generation.doc.com','true','true'};
        private String[] field_order_type_info = new String[]{'TemplateName','objDocGenDTO'};
    }
    public class SFDCDocumentGenerationHttpSoap11Endpoint {
        public String endpoint_x = 'http://34.227.23.86:8080/DocGeneration/services/SFDCDocumentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint/';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://generation.doc.com', 'AOPTDocumentGeneration', 'http://docgeneration.dto.com/xsd', 'AOPTDocumentGenerationXsd', 'http://util.com/xsd', 'AOPTDocumentGenerationUtilComXsd'};
        public String DocGeneration(String TemplateName,AOPTDocumentGenerationXsd.DocGenDTO objDocGenDTO) {
            AOPTDocumentGeneration.DocGeneration_element request_x = new AOPTDocumentGeneration.DocGeneration_element();
            request_x.TemplateName = TemplateName;
            request_x.objDocGenDTO = objDocGenDTO;
            AOPTDocumentGeneration.DocGenerationResponse_element response_x;
            Map<String, AOPTDocumentGeneration.DocGenerationResponse_element> response_map_x = new Map<String, AOPTDocumentGeneration.DocGenerationResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:DocGeneration',
              'http://generation.doc.com',
              'DocGeneration',
              'http://generation.doc.com',
              'DocGenerationResponse',
              'AOPTDocumentGeneration.DocGenerationResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
    }
}