/***********************************************************************
* Description - Test class developed for RefreshTokenService
*
* Version            Date            Author        Description
* 1.0                26/11/17        Snehil		   Initial Draft
**********************************************************************/
@isTest
private class RefreshTokenServiceTest
{
	@isTest static void NoException() {

		RefreshTokenService.AgingBucketResponse agingBucket = new RefreshTokenService.AgingBucketResponse();
		agingBucket.ATTRIBUTE2 = '';
		agingBucket.ATTRIBUTE5 = '';
		agingBucket.ATTRIBUTE6 = '';
		agingBucket.ATTRIBUTE13 = '';
		agingBucket.ATTRIBUTE14 = '';
		agingBucket.ATTRIBUTE15 = '';
		agingBucket.ATTRIBUTE16 = '';
		agingBucket.ATTRIBUTE17 = '';
		agingBucket.ATTRIBUTE18 = '';
		agingBucket.ATTRIBUTE19 = '';
		agingBucket.ATTRIBUTE20 = '';
		agingBucket.ATTRIBUTE21 = '';	
		agingBucket.ATTRIBUTE22 = '';
		agingBucket.ATTRIBUTE23 = '';		

		Test.startTest();
		
		Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockCallingList());
		
		agingBucket = RefreshTokenService.getAgingbucket( '123456' );

		Test.stopTest();
	}
}