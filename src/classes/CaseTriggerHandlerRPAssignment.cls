public without sharing class CaseTriggerHandlerRPAssignment{
    public void afterUpdate(map<Id,Case> newMap, map<Id,Case> oldMap){
        Id RPAssignmentRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Assignment').getRecordTypeId();
        set<Id> setCaseIds = new set<Id>();
        for(Case objCase : newMap.values()){
            if(objCase.Status.equalsIgnoreCase('Closed')
            && oldMap.get(objCase.Id).Status != objCase.Status
            && objCase.RecordTypeId == RPAssignmentRecTypeId){
                setCaseIds.add(objCase.Id);
            }
        }
        if(!setCaseIds.isEmpty()){
            processCase(setCaseIds);
        }
    }
    
    public static void processCase(set<Id> setCaseIds){
        map<Id,Id> mapOldBU_Case = new map<Id,Id>();
        map<Id,Id> mapNewBU_Case = new map<Id,Id>();
        Integer intOldCount = 1;
        Integer intNewCount = 1;
        for(Case objC : [Select Booking_Unit__c
                              , New_Booking_Unit__c
                              , Commencement_Date__c 
                        from Case where Id IN : setCaseIds]){
            if(objC.Booking_Unit__c != null){
                if(intOldCount >= 49){
                    removeOldUnitFromRentalPool(mapOldBU_Case);
                    mapOldBU_Case = new map<Id,Id>();
                    intOldCount = 1;
                    mapOldBU_Case.put(objC.Booking_Unit__c,objC.Id);
                }else{
                    mapOldBU_Case.put(objC.Booking_Unit__c,objC.Id);
                }
                intOldCount++;
            }
            if(objC.New_Booking_Unit__c != null){
                if(intNewCount >= 49){
                    addNewUnitToRentalPool(mapNewBU_Case);
                    mapOldBU_Case = new map<Id,Id>();
                    intNewCount = 1;
                    mapNewBU_Case.put(objC.New_Booking_Unit__c,objC.Id);
                }else{
                    mapNewBU_Case.put(objC.New_Booking_Unit__c,objC.Id);
                }
                intNewCount++;
            }
        }
        if(!mapOldBU_Case.isEmpty()){
            removeOldUnitFromRentalPool(mapOldBU_Case);
        }
        if(!mapNewBU_Case.isEmpty()){
            addNewUnitToRentalPool(mapNewBU_Case);
        }
    } // end of processCase
    
    @future(callout=true)
    public static void removeOldUnitFromRentalPool(map<Id,Id> mapOldUnit_Case){
        system.debug('mapOldUnit_Case*****'+mapOldUnit_Case);
        map<Id,Case> mapId_Case = new map<Id,Case>([Select Id, Seller__c, CaseNumber from Case where Id IN : mapOldUnit_Case.values()]);
        for(Booking_Unit__c objBU : [Select Id, Rental_Pool__c, Registration_ID__c
                                    from Booking_Unit__c 
                                    where Id IN : mapOldUnit_Case.keySet()]){
            String strDate = system.now().format('dd-MMM-YY');
            String rentalDocResponse = 
            RentalPoolUpdateInIPMS.rentalPoolUpdateCallout('TERMINATE_RP_RECORD'
                                                            ,'Y'
                                                            ,strDate
                                                            ,mapId_Case.get(mapOldUnit_Case.get(objBU.Id)).CaseNumber
                                                            ,objBU.Registration_ID__c);
            parseResponse(rentalDocResponse
                         ,mapId_Case.get(mapOldUnit_Case.get(objBU.Id)).Seller__c
                         ,mapId_Case.get(mapOldUnit_Case.get(objBU.Id))
                         ,objBU.Id
                         ,'OldUnit');
        }
    }
    
    @future(callout=true)
    public static void addNewUnitToRentalPool(map<Id,Id> mapNewUnit_Case){
        map<Id,Case> mapId_Case = new map<Id,Case>([Select Id, CaseNumber,AccountId, Commencement_Date__c from Case where Id IN : mapNewUnit_Case.values()]);
        for(Booking_Unit__c objBU : [Select Id, Rental_Pool__c, Registration_ID__c
                                    from Booking_Unit__c 
                                    where Id IN : mapNewUnit_Case.keySet()]){
            Date commDate = mapId_Case.get(mapNewUnit_Case.get(objBU.Id)).Commencement_Date__c;
            String strDate;
            if(commDate != null){
                DateTime commDateTime = datetime.newInstance(commDate.year(), commDate.month(), commDate.day());
                strDate = commDateTime.format('dd-MMM-YY');
            }else{
                strDate = system.now().format('dd-MMM-YY');
            }
            String rentalDocResponse = 
            RentalPoolUpdateInIPMS.rentalPoolUpdateCallout('CREATE_RP_RECORD'
                                                           ,'Y'
                                                           ,strDate
                                                           ,mapId_Case.get(mapNewUnit_Case.get(objBU.Id)).CaseNumber
                                                           ,objBU.Registration_ID__c);
            parseResponse(rentalDocResponse
                         ,mapId_Case.get(mapNewUnit_Case.get(objBU.Id)).AccountId
                         ,mapId_Case.get(mapNewUnit_Case.get(objBU.Id))
                         ,objBU.Id
                         ,'NewUnit');
        }
    }
    
    public static void parseResponse(String rentalDocResponse, String accountId, Case objC, String unitId, String strUnit){
        List<RPDocDetailsWrapper> lstRPDocDetails = new List<RPDocDetailsWrapper>();
        List<Booking_Unit__c> listUpdateBookingUnit = new List<Booking_Unit__c>();
        List<Error_Log__c> listErrorLog = new List<Error_Log__c>();
        if(String.isNotBlank(rentalDocResponse)) {
            map<String, Object> mapDeserializeRPDoc = (map<String,Object>)JSON.deserializeUntyped(rentalDocResponse);
            system.debug('mapDeserializeRPDoc==='+mapDeserializeRPDoc);
            if(mapDeserializeRPDoc.get('status') == 'S') {
                String StringRPData = JSON.serialize(mapDeserializeRPDoc.get('data'));
                system.debug('StringRPData==='+StringRPData);
                lstRPDocDetails = 
                    (List<RPDocDetailsWrapper>)JSON.deserialize(StringRPData, List<RPDocDetailsWrapper>.class);
                system.debug('lstRPDocDetails==='+lstRPDocDetails);
                for(RPDocDetailsWrapper rpaInst: lstRPDocDetails) {
                    if(rpaInst.PROC_STATUS == 'S') {
                        Booking_Unit__c bookingUnitObj = new Booking_Unit__c();
                        bookingUnitObj.Id = unitId;
                        if(strUnit.equalsIgnoreCase('NewUnit')){
                            bookingUnitObj.Rental_Pool__c = true;
                            String strDate;
                            if(objC.Commencement_Date__c != null){
                                DateTime commDateTime = datetime.newInstance(objC.Commencement_Date__c.year()
                                                                            ,objC.Commencement_Date__c.month()
                                                                            ,objC.Commencement_Date__c.day());
                                strDate = commDateTime.format('dd-MMM-YY');
                            }else{
                                strDate = system.now().format('dd-MMM-YY');
                            }
                            bookingUnitObj.Rental_Pool_Start_Date__c = strDate;
                        }else{
                            bookingUnitObj.Rental_Pool__c = false;
                            bookingUnitObj.Rental_Pool_End_Date__c = system.now().format('dd-MMM-YY');
                        }
                        listUpdateBookingUnit.add(bookingUnitObj);
                    }else {
                        Error_Log__c objErr = createErrorLogRecord(accountId, unitId, objC.Id);
                        objErr.Error_Details__c = 'Error : '+rpaInst.PROC_MESSAGE; 
                        listErrorLog.add(objErr);
                    }
                }
            }
            else {
                Error_Log__c objErr = createErrorLogRecord(accountId, unitId, objC.Id);
                objErr.Error_Details__c = 'Error : '+(String)mapDeserializeRPDoc.get('message'); 
                listErrorLog.add(objErr);
            }
        }
        else {
            Error_Log__c objErr = createErrorLogRecord(accountId, unitId, objC.Id);
            objErr.Error_Details__c = 'Error : No response from IPMS'; 
            listErrorLog.add(objErr);
        }
        if(!listUpdateBookingUnit.isEmpty()) {
            update listUpdateBookingUnit;
        }
        
        if(!listErrorLog.isEmpty()) {
            insertErrorLog(listErrorLog);
        }
    }
    
    public static Error_Log__c createErrorLogRecord(Id accId, Id bookingUnitId, Id caseId){
        Error_Log__c objErr = new Error_Log__c();
        objErr.Account__c = accId;
        objErr.Booking_Unit__c = bookingUnitId;
        objErr.Case__c = caseId;
        objErr.Process_Name__c = 'Rental Pool Assignment';
        return objErr;
    }
    
    public static void insertErrorLog(list<Error_Log__c> listObjErr){
        try{
            insert listObjErr;
        }catch(Exception ex){
            //errorMessage = 'Error : '+ ex.getMessage();
            system.debug('Error Log ex*****'+ex);
        }
    }
    
    public class RPDocDetailsWrapper {
        public String PROC_STATUS {get;set;}
        public String PROC_MESSAGE {get;set;}
        public String ATTRIBUTE1 {get;set;}
        public String ATTRIBUTE2 {get;set;}
        public String ATTRIBUTE3 {get;set;}
        public String ATTRIBUTE4 {get;set;}
        public String PARAM_ID {get;set;}

        public RPDocDetailsWrapper(){

        }
    }
}