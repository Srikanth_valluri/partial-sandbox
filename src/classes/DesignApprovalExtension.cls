public class DesignApprovalExtension {

    public String strCaseId;
    public String isPMTUser;
    public Case objCase;
    public List<SR_Attachments__c> lstSRAttachments {get;set;}

    public DesignApprovalExtension(ApexPages.StandardController stdController) {
        strCaseId = ApexPages.currentPage().getParameters().get('Id');
        system.debug('!!!!!strCaseId'+strCaseId);
        isPMTUser = ApexPages.currentPage().getParameters().get('pmtUser');
        objCase = [SELECT Design_Approval_Status__c, Plot_Handover_Status__c FROM Case WHERE Id =: strCaseId];
    }
    
    public void loadSRAttachments() {
        if (strCaseId != null) {
            lstSRAttachments = [SELECT Id
                                    , Name
                                    , Attachment_URL__c
                                    , Type__c 
                                FROM SR_Attachments__c 
                                WHERE Case__c =: strCaseId
                                AND Name LIKE 'Building Design Plan%'];
            system.debug('!!!!lstSRAttachments'+lstSRAttachments);
        }
    }

    public PageReference submitApproval() {
       if (strCaseId != null) {
            if (isPMTUser.equalsIgnoreCase('yes')) {
                objCase.PMT_Approval_Status__c = 'Approved';
                objCase.Plot_Handover_Status__c = 'Building Designs Approved by PMT';
                update objCase;
                ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Info,'Documents are approved'));         
            } else if (isPMTUser.equalsIgnoreCase('no')) {
                objCase.Design_Approval_Status__c = 'Approved';
                objCase.Plot_Handover_Status__c = 'Building Designs Approved by Design Team';
                update objCase;
                ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Info,'Documents are approved')); 
            }
        }
        return null;
    }

    public PageReference rejectApproval() {
       if (strCaseId != null) {
            if (isPMTUser.equalsIgnoreCase('yes')) {
                objCase.PMT_Approval_Status__c = 'Rejected';
                update objCase;
                ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Info,'Documents are rejected'));         
            } else if (isPMTUser.equalsIgnoreCase('no')) {
                objCase.Design_Approval_Status__c = 'Rejected';
                update objCase;
                ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Info,'Documents are rejected')); 
            }
        }
        return null;
    }
}