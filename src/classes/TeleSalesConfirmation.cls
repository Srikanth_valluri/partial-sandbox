/****************************************************************************************************
* Name          : TeleSalesConfirmation                                                             *
* Description   :                                                                                   *
* Created Date  :                                                                                   *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE            COMMENTS                                                *
* 1.1   Craig Lobo          22-05-2018      Added code to share the Inquiry record with TSA Manager *
* 1.2   Craig Lobo          23-05-2018      Assign TSA's Task(Meeting at Office) to Inquiry Owner   *
                                            Commented the Creation of Task for the RM               *
* 1.3   Craig Lobo          27-05-2018      Populated the Task Due Date with the Inquiry Meeting    *
                                            Due Date.                                               *
                                            Resolved the issue of GMT time in SMS                   *
* 1.4   Craig Lobo          27-05-2018      Removed Sharing if meeting Due Date and Sales Office    *
                                            is blank on Inquiry                                     *
* 1.5   Craig Lobo          29-05-2018      Moved the Send Email Notification to Workflow           *
                                            Records updated via Informatica to fire Workflow        *
* 1.6   Craig Lobo          19-08-2018      Change in Meeting Confirmation Module                   *
* 1.7                       07-07-2019      Added Telesales Team Lightning Profile on 7 July 2019   *
* 1.8    QBurst             31-03-2020      Added online meetings in Activity Query
****************************************************************************************************/

public without sharing class TeleSalesConfirmation {

    public static Boolean iscalled ; 
    public static String INQUIRY_QUERY  = ' SELECT Id, Inquiry_Status__c, Inquiry_Id__c, '
                                        + ' TSA_TL_of_Owner__c, First_Name__c, Last_Name__c, '
                                        + ' Send_Email__c, Send_SMS__c, Office_meeting_created__c, '
                                        + ' Email__c, Meeting_Due_Date__c, Sales_Office__c, '
                                        + ' Mobile_Phone__c, Sales_Office_Location_Url__c, Name, '
                                        + ' Customer_Notification__c, Pre_InquiryId__c, Dos_User_Id__c, '
                                        + ' Is_Meeting_Confirmed__c, Is_Meeting_scheduled__c, '
                                        + ' Is_Meeting_Completed__c, RecordType.Name, '
                                        + ' OwnerId, Owner.Name, Owner.Profile.Name, Owner.Email, '
                                        + ' Telesales_Executive__c, Telesales_Executive__r.Email, '
                                        + ' Telesales_Executive__r.Profile.Name, '
                                        + ' Schedule_Conditions__c, Confirmation_Conditions__c, '
                                        + ' ( SELECT OwnerId, Id, Owner.Name,  Subject, ActivityDate, '
                                        + ' Type, Activity_Type_3__c, Activity_Outcome__c, WhatId, '
                                        + ' Status, Is_meeting_scheduled__c,  is_Owner_changed__c, '
                                        + ' Is_meeting_confirmed_reminder_sent__c '
                                        + ' FROM Tasks '
                                        + ' WHERE Activity_Type_3__c = \'Meeting at Office\' '
                                        + ' OR  Activity_Type_3__c = \'Online Meeting\' '  // 1.8
                                        + ' ORDER By CreatedDate DESC) '
                                        + ' FROM Inquiry__c ';

    /**
     * Constructor
     */
    public TeleSalesConfirmation(){
        system.debug('=InquiryTrigger==TeleSalesConfirmation===');
    }

    /**
     * Method to update the location URL on the Inquiry and pass it forward for processing
     */
    public void UpdateLocation(List<Inquiry__c> inquiryList){
        Map<String, Sales_Office_Locations__c> officeLocationMap = Sales_Office_Locations__c.getAll();
        System.debug('officeLocationMap>>>>>>   ' + officeLocationMap);
        for(Inquiry__c inq : inquiryList){
            if (String.isNotBlank(inq.Sales_Office__c)
                && officeLocationMap.containsKey(inq.Sales_Office__c)
            ) {
                inq.Sales_Office_Location_Url__c = officeLocationMap.get(inq.Sales_Office__c).Location_URL__c;
                inq.Meeting_Template_Name__c = officeLocationMap.get(inq.Sales_Office__c).Email_Template_Name__c;
            }
        }
    }

    /**
     * 
     */
    public static void SendTeleSalesConfirmationEmail(Set<Id> inqIdSet){
        System.debug('SendTeleSalesConfirmationEmail iscalled>>>>>>   ' + iscalled);
        System.debug('SendTeleSalesConfirmationEmail inqIdSet >>>>>>' + inqIdSet);

        if(inqIdSet != null && !inqIdSet.isEmpty()) {
            Map<String, String> mapBodyPhoneNumber = new Map<String, String>();
            //List<Office_Meeting__c> officeMeetingList  = new List<Office_Meeting__c>();
            List<Inquiry__Share> inqShareList = new List<Inquiry__Share>();
            List<Inquiry__c> inquiryUpdateList = new List<Inquiry__c>();
            List<String> inqIdDeleteShareList = new List<String>();
            List<Task> taskList = new List<Task>();

            /*
            List<String> inqSalesOffice = new List<String>();
            Schema.DescribeFieldResult fieldResult = Inquiry__c.Sales_Office__c.getDescribe();
            List<Schema.PicklistEntry> pickListEntryList = fieldResult.getPicklistValues();

            for( Schema.PicklistEntry field : pickListEntryList){
                inqSalesOffice.add(field.getValue());
            }

            List<QueueSObject> queueList = [ SELECT Queue.Id, Queue.Name, Queue.Type  
                                               FROM QueueSObject 
                                              WHERE Queue.Type ='Queue' 
                                                AND Queue.Name IN :inqSalesOffice];
            System.debug('queueList>>>>>  ' + queueList);
            */

            Inquiry__c inquiryToUpdate;
            Task updateTask = new Task();
            String inqQueryString   = INQUIRY_QUERY
                                    + ' WHERE Id IN :inqIdSet '
                                    + ' AND RecordType.Name = \'Pre Inquiry\' '
                                    + ' AND (Inquiry_Status__c = \'New\' OR Inquiry_Status__c = \'Active\') '
                                    + ' AND Sales_Office_Location_Url__c != \'\' ';
            System.debug('inqQueryString>>>>  ' + inqQueryString);

            for(Inquiry__c inqLoopObj : DataBase.Query(inqQueryString)) {
                System.debug('inqLoopObj>>>>  ' + inqLoopObj);
                List<Task> inqTask = inqLoopObj.Tasks;
                System.debug('inqTask>>>>  ' + inqTask);

                // Inquiry Task Size Check
                if(inqTask != null && !inqTask.isEmpty()) {
                    updateTask = inqTask[0];

                    // Record Type, Status Check
                    if( inqLoopObj.RecordType.Name == 'Pre Inquiry' 
                        && (inqLoopObj.Inquiry_Status__c == 'New' 
                            || inqLoopObj.Inquiry_Status__c == 'Active'
                        )
                    ) {

                        // Meeting Date, Sales Office Check
                        if (inqLoopObj.Meeting_Due_Date__c != null 
                            && String.isNotBlank(inqLoopObj.Sales_Office__c)
                        ) {

                            // Owner Profile Check 
                            if((inqLoopObj.Owner.Profile.Name == 'Telesales Team TL'|| inqLoopObj.Owner.Profile.Name == Label.Telesales_Team_Lightning) || ( (inqLoopObj.Owner.Profile.Name == 'Telesales Team' 
                                || inqLoopObj.Owner.Profile.Name == 'Telesales Team Lightning') && inqLoopObj.TSA_TL_of_Owner__c != null ) ) {

                                System.debug('PRE INQUIRY>>>>  ');
                                updateTask = new Task();
                                updateTask = inqTask[0];

                                // If Owner is not TSA TL then update the Task Owner 
                                // and Share the Inquiry with TSA TL
                                if(inqLoopObj.Owner.Profile.Name != 'Telesales Team TL' && inqLoopObj.Owner.Profile.Name != Label.Telesales_Team_Lightning) { 
                                    updateTask.OwnerId = inqLoopObj.TSA_TL_of_Owner__c;
                                    // Share inquiry with TSA TL
                                    Inquiry__Share shareTL = new Inquiry__Share();
                                    shareTL.AccessLevel = 'Edit';
                                    shareTL.ParentId = inqLoopObj.Id;
                                    shareTL.UserOrGroupId = inqLoopObj.TSA_TL_of_Owner__c;
                                    shareTL.RowCause = Schema.Inquiry__Share.RowCause.TSA_TL__c;
                                    inqShareList.add(shareTL);
                                } else {
                                    updateTask.OwnerId = inqLoopObj.OwnerId;
                                }

                                // Craig 22-05-2018 - Share inquiry with TSA Manager
                                if (String.isNotBlank(inqLoopObj.Dos_User_Id__c) 
                                    && inqLoopObj.Dos_User_Id__c != inqLoopObj.TSA_TL_of_Owner__c
                                ) {
                                    // Share inquiry with TSA Manager
                                    Inquiry__Share shareManager = new Inquiry__Share();
                                    shareManager.AccessLevel = 'Edit';
                                    shareManager.ParentId = inqLoopObj.Id;
                                    shareManager.UserOrGroupId = inqLoopObj.Dos_User_Id__c;
                                    shareManager.RowCause = Schema.Inquiry__Share.RowCause.TSA_TL__c;
                                    inqShareList.add(shareManager);
                                }
                                // Craig 22-05-2018

                                DateTime meetingDateTime = inqLoopObj.Meeting_Due_Date__c;
                                Date taskDueDate = Date.newinstance(
                                    meetingDateTime.year(), meetingDateTime.month(), meetingDateTime.day()
                                );

                                inquiryToUpdate = new Inquiry__c(Id = inqLoopObj.Id);
                                //Change Owner of Task
                                if (updateTask.is_Owner_changed__c != true) {
                                    updateTask.OwnerId = inqLoopObj.TSA_TL_of_Owner__c;
                                    updateTask.ActivityDate = taskDueDate;
                                    updateTask.Task_Due_Date__c = meetingDateTime;
                                    updateTask.is_Owner_changed__c = true;
                                    updateTask.Is_meeting_scheduled__c = true;
                                    System.debug('updateTask>>>>  ' + updateTask);
                                }

                                if(updateTask.is_Owner_changed__c
                                    && updateTask.Status != 'Completed'
                                    && updateTask.Is_meeting_scheduled__c
                                    && !updateTask.Is_meeting_confirmed_reminder_sent__c
                                    && !inqLoopObj.Schedule_Conditions__c
                                ){
                                    System.debug('Schedule_Conditions__c>>>>  ');
                                    inquiryToUpdate.Schedule_Conditions__c = true;
                                    inquiryToUpdate.Is_Meeting_scheduled__c = true;

                                    if(inqLoopObj.Send_SMS__c) {
                                        String smsBody  = 'Hi ' + inqLoopObj.First_Name__c + ' ' + inqLoopObj.Last_Name__c 
                                                        + ', \n Thank you for scheduling a meeting with DAMAC Properties. '
                                                        + ' We will be contacting you shortly to provide further details.';
                                        mapBodyPhoneNumber.put(inqLoopObj.Mobile_Phone__c, smsBody);
                                        updateTask.Is_meeting_scheduled__c = true;
                                        System.debug('Schedule_Send_SMS__c__c>>>>  ');
                                    }
                                }  else if(updateTask.is_Owner_changed__c
                                    && updateTask.Status != 'Completed'
                                    && updateTask.Is_meeting_scheduled__c 
                                    && inqLoopObj.Schedule_Conditions__c
                                    && inqLoopObj.Confirmation_Conditions__c
                                    && !updateTask.Is_meeting_confirmed_reminder_sent__c
                                ) {
                                    if(inqLoopObj.Send_SMS__c) {
                                        String meetingTime = inqLoopObj.Meeting_Due_Date__c.Format();
                                        String smsBody  = 'Hi ' + inqLoopObj.First_Name__c + ' '+  inqLoopObj.Last_Name__c 
                                                        //+ ', your meeting with ' + inqLoopObj.Owner.Name + ' on ' 
                                                        + ', your meeting is confirmed for ' 
                                                        + meetingTime + ', ' + inqLoopObj.Sales_Office__c 
                                                        + ' (' + inqLoopObj.Sales_Office_Location_Url__c 
                                                        + '). Ref No: ' 
                                                        + inqLoopObj.Name + '. For assistance, call @04 3019999.';
                                        mapBodyPhoneNumber.put(inqLoopObj.Mobile_Phone__c, smsBody);
                                        updateTask.Is_meeting_confirmed_reminder_sent__c = true;
                                        System.debug('Confirmation_Send_SMS__c__c>>>>  ');
                                    }
                                }
                                taskList.add(updateTask);
                                inquiryUpdateList.add(inquiryToUpdate);
                                System.debug('taskList>>>>  ' + taskList);
                                System.debug('inquiryUpdateList>>>>  ' + inquiryToUpdate);
                            }// Owner Profile and TSA TL Check

                        } else {
                            inqIdDeleteShareList.add(inqLoopObj.Id);
                            System.debug('inqIdDeleteShareList>>>>> ' + inqIdDeleteShareList);
                        } // Meeting Date, Sales Office Check
                    } // RecordType Check
                } // Task List Size Check
            } // Inquiry Query END

            List<String> pMobileNumberList;
            for(String phoneNo : mapBodyPhoneNumber.keySet()){
                pMobileNumberList = new List<String>();
                pMobileNumberList.add(phoneNo); 
                String msgBody = mapBodyPhoneNumber.get(phoneNo);
                System.debug('pMobileNumberList>>>  ' + pMobileNumberList);
                TelesalesSMS objTelesalesSMS = new TelesalesSMS(msgBody, pMobileNumberList);
                ID jobID = System.enqueueJob(objTelesalesSMS);
            }

            System.debug('inqIdDeleteShareList>>>  '+inqIdDeleteShareList);
            List<Inquiry__Share> inqDeleteShareList = new List<Inquiry__Share>();
            inqDeleteShareList = [ SELECT Id, RowCause, UserOrGroupId, ParentId
                                     FROM Inquiry__Share 
                                    WHERE ParentId IN :inqIdDeleteShareList
                                      AND RowCause = 'TSA_TL__c'
            ];
            System.debug('inqDeleteShareList>>>>>  '+inqDeleteShareList);

            // ******DML's******
            try {

                // Update the Inquiries with the Email Status
                update inquiryUpdateList;

                // Update the Tasks with the Email Status
                upsert taskList;

                // Delete existing share records
                delete inqDeleteShareList;

                // Share Inquiry with TSA TL and TSA Manager
                insert inqShareList;

                // Create office Meeting Records for New Inquiries
                //insert officeMeetingList;

            } catch(Exception ex) {
                System.debug('*Exception*************************' + ex);
                System.debug('*getStackTraceString***************' + ex.getStackTraceString());
                System.debug('*getCause*Exception****************' + ex.getCause());
                System.debug('*getLineNumber*Exception***********' + ex.getLineNumber());
            }
        }

    }



/* 

*******  OLD MODULE FOR REFERENCE  *******

            // RecordType Check
            if(inq.RecordType.Name == 'Inquiry' && inq.Inquiry_Status__c == 'Meeting Scheduled') {
                if (inq.Meeting_Due_Date__c != null && String.isNotBlank(inq.Sales_Office__c)) {
                    System.debug('inq>>>>  ' + inq);
                    inqInsatnce = new Inquiry__c(Id = inq.Id);
                    for(Task taskRec : inqTask) {
                        
                        if( !taskRec.Is_meeting_confirmed_reminder_sent__c
                            && taskRec.is_Owner_changed__c
                            && taskRec.Status != 'Completed'
                        ){
                            System.debug('Is_meeting_scheduled__c>>>>  ' + taskRec.Is_meeting_scheduled__c );
                            if (taskRec.Is_meeting_scheduled__c
                                || (inq.Telesales_Executive__c != null 
                                    && inq.Telesales_Executive__r.Profile.Name == 'Telesales Team TL')
                            ) {
                                System.debug('Confirmation_Conditions__c>>>>  ');
                                inqInsatnce.Confirmation_Conditions__c = true;


                                if(inq.Send_SMS__c) {
                                    String meetingTime = inq.Meeting_Due_Date__c.Format();
                                    String smsBody  = 'Hi ' + inq.First_Name__c + ' '+  inq.Last_Name__c 
                                                    + ', your meeting with ' + inq.Owner.Name + ' on ' 
                                                    + meetingTime+ ', '+ inq.Sales_Office__c 
                                                    + ' ('+ inq.Sales_Office_Location_Url__c 
                                                    + ') is confirmed. Ref No : ' 
                                                    + inq.Name + '. For assistance, call @04 3019999.';
                                    mapBodyPhoneNumber.put(inq.Mobile_Phone__c, smsBody);
                                }

                                DateTime meetingDateTime = inq.Meeting_Due_Date__c;
                                Date taskDueDate = Date.newinstance(
                                    meetingDateTime.year(), meetingDateTime.month(), meetingDateTime.day()
                                );

                                // Update existing task
                                Task existngTask = new Task();
                                existngTask = taskRec;
                                existngTask.ActivityDate = taskDueDate;
                                existngTask.Task_Due_Date__c = meetingDateTime;
                                existngTask.Is_meeting_confirmed_reminder_sent__c = true;
                                existngTask.OwnerId = inq.OwnerId;
                                taskList.add(existngTask);
                                System.debug('taskList>>>>  ' + taskList);

                            } // TSA TL check and Is_meeting_scheduled__c check
                        } // Task fields check
                    } // Task Loop

                    if(!inq.Office_meeting_created__c){
                        Office_Meeting__c  officeMeetingRec = new Office_Meeting__c();
                        officeMeetingRec.Inquiry__c = inq.Id;
                        officeMeetingRec.Outcome__c = '';
                        officeMeetingRec.Status__c = 'Not Started';
                        for(QueueSObject que : queueList){
                            String queueName = que.Queue.Name;
                            if(queueName.equalsIgnoreCase(inq.Sales_Office__c)) {
                                officeMeetingRec.OwnerId = que.Queue.Id;
                            }
                        }
                        officeMeetingList.add(officeMeetingRec);
                        System.debug('officeMeetingList>>>>  ' + officeMeetingList);

                        inqInsatnce.Office_meeting_created__c = true;
                        inqInsatnce.Is_Meeting_Confirmed__c = true;
                        //if (inq.Owner.Profile.Name == 'Telesales Team TL' ) {
                        //    inqInsatnce.Is_Meeting_scheduled__c = true;
                        //    if (inq.Pre_InquiryId__c != '') {
                        //        Inquiry__c inqTL = new Inquiry__c(
                        //            Id = inq.Pre_InquiryId__c,
                        //            Is_Meeting_scheduled__c = true
                        //        );
                        //        inquiryUpdateList.add(inqTL);
                        //    }
                        //}
                        inquiryUpdateList.add(inqInsatnce);
                        System.debug('inquiryUpdateList>>>>  ' + inquiryUpdateList);
                    }
                } // Date and Office Check

            // PRE INQUIRY > NEW OR ACTIVE
            } else if( inq.RecordType.Name == 'Pre Inquiry' 
                && (inq.Inquiry_Status__c == 'New' || inq.Inquiry_Status__c == 'Active')
            ) {

                
                if (inq.Meeting_Due_Date__c != null && String.isNotBlank(inq.Sales_Office__c)) {
                    if(inq.Owner.Profile.Name == 'Telesales Team TL' 
                        || (inq.Owner.Profile.Name == 'Telesales Team' && inq.TSA_TL_of_Owner__c != null) 
                    ) {

                        System.debug('PRE INQUIRY>>>>  ');
                        inqInsatnce = new Inquiry__c(
                            Id = inq.Id, 
                            Is_Meeting_scheduled__c = true
                        );

                        DateTime meetingDateTime = inq.Meeting_Due_Date__c;
                        Date taskDueDate = Date.newinstance(
                            meetingDateTime.year(), meetingDateTime.month(), meetingDateTime.day()
                        );

                        //Change Owner of Task
                        Task updateTask = new Task();
                        updateTask = inqTask[0];
                        updateTask.OwnerId = inq.TSA_TL_of_Owner__c;
                        updateTask.ActivityDate = taskDueDate;
                        updateTask.Task_Due_Date__c = meetingDateTime;
                        updateTask.is_Owner_changed__c = true;

                        // If Owner is not TSA TL then update the Task Owner 
                        // and Share the Inquiry with TSA TL
                        if(inq.Owner.Profile.Name != 'Telesales Team TL') { 
                            updateTask.OwnerId = inq.TSA_TL_of_Owner__c;

                            // Share inquiry with TSA TL
                            Inquiry__Share shareTL = new Inquiry__Share();
                            shareTL.AccessLevel = 'Edit';
                            shareTL.ParentId = inq.Id;
                            shareTL.UserOrGroupId = inq.TSA_TL_of_Owner__c;
                            shareTL.RowCause = Schema.Inquiry__Share.RowCause.TSA_TL__c;
                            inqShareList.add(shareTL);
                        } else {
                            updateTask.OwnerId = inq.OwnerId;
                        }

                        // Craig 22-05-2018 - Share inquiry with TSA Manager
                        if (String.isNotBlank(inq.Dos_User_Id__c) 
                            && inq.Dos_User_Id__c != inq.TSA_TL_of_Owner__c
                        ) {
                            // Share inquiry with TSA Manager
                            Inquiry__Share shareManager = new Inquiry__Share();
                            shareManager.AccessLevel = 'Edit';
                            shareManager.ParentId = inq.Id;
                            shareManager.UserOrGroupId = inq.Dos_User_Id__c;
                            shareManager.RowCause = Schema.Inquiry__Share.RowCause.TSA_TL__c;
                            inqShareList.add(shareManager);
                        }
                        // Craig 22-05-2018

                        System.debug('.is_Owner_changed__c>>>>  ' + updateTask.is_Owner_changed__c);
                        System.debug('!.Is_meeting_scheduled__c >>>>  ' + updateTask.Is_meeting_scheduled__c );
                        System.debug('!.Is_meeting_confirmed_reminder_sent__c>>>>  ' + updateTask.Is_meeting_confirmed_reminder_sent__c);
                        System.debug('!.Is_Meeting_scheduled__c>>>>  ' + inq.Is_Meeting_scheduled__c);
                        if(updateTask.is_Owner_changed__c
                            && updateTask.Status != 'Completed'
                            && !updateTask.Is_meeting_scheduled__c 
                            && !updateTask.Is_meeting_confirmed_reminder_sent__c
                            && !inq.Is_Meeting_scheduled__c
                        ){
                            System.debug('Schedule_Conditions__c>>>>  ');
                            inqInsatnce.Schedule_Conditions__c = true;

                            if(inq.Send_SMS__c) {
                                String smsBody  = 'Hi ' + inq.First_Name__c + ' ' + inq.Last_Name__c + ', \n'
                                                + ' Thank you for scheduling a meeting with DAMAC Properties. '
                                                + ' We will be contacting you shortly to provide further details.';
                                mapBodyPhoneNumber.put(inq.Mobile_Phone__c, smsBody);
                            }
                            updateTask.Is_meeting_scheduled__c = true;
                        } // if
                        taskList.add(updateTask);
                        inquiryUpdateList.add(inqInsatnce);
                        System.debug('taskList>>>>  ');
                        System.debug('inquiryUpdateList>>>>  ');
                    }// Owner Profile and TSA TL Check

                } else {
                    inqIdDeleteShareList.add(inq.Id);
                    System.debug('inqIdDeleteShareList>>>>> ' + inqIdDeleteShareList);
                }
            } // Record Type Check
*/
}