/* * * * * * * * * * * * * *
*  Class Name:   SendSMSAccountServiceTest
*  Purpose:      Unit test class for SendSMSAccountService Controller
*  Author:       Hardik Mehta - ESPL
*  Company:      ESPL
*  Created Date: 26-Nov-2017
*  Updated Date: 26-Nov-2017
*  Type:         Test Class
* * * * * * * * * * * * */
@isTest
private class SendSMSAccountServiceTest 
{
  
  /* * * * * * * * * * * * *
  *  Method Name:  Sendtextmessage_test
  *  Purpose:      This method is used to unit test functionality in Sendtextmessage method
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 26-Nov-2017
  * * * * * * * * * * * * */
  static testMethod void Sendtextmessage_test()
  {
    //insert sample data for Account
    Account objAccount = TestDataFactory_CRM.createPersonAccount();
    insert objAccount;

    Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
    Id aoptRecordTypeID = caseRecordTypes.get('AOPT').getRecordTypeId();

    Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , aoptRecordTypeID);
    objCase.Status = 'Submitted';
    insert objCase;

    List<String> phoneNoList = new List<String>();
    phoneNoList.add(objCase.Id+':9898989898');
    String strAbndId='';
    Test.startTest();
      Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList());
      SendSMSAccountService.Sendtextmessage(phoneNoList,'test message content',objAccount.Id,false,strAbndId);
    Test.stopTest();
  }

  /* * * * * * * * * * * * *
  *  Method Name:  Sendtextmessage_test2
  *  Purpose:      This method is used to unit test functionality in Sendtextmessage method
  *                if response is returned null from endpoint
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 26-Nov-2017
  * * * * * * * * * * * * */
  static testMethod void Sendtextmessage_test2()
  {
    //insert sample data for Account
    Account objAccount = TestDataFactory_CRM.createPersonAccount();
    insert objAccount;

    Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
    Id aoptRecordTypeID = caseRecordTypes.get('AOPT').getRecordTypeId();

    Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , aoptRecordTypeID);
    objCase.Status = 'Submitted';
    insert objCase;

    List<String> phoneNoList = new List<String>();
    phoneNoList.add(objCase.Id+':9898989898');
    String strAbndId='';
    Test.startTest();
      Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
      SendSMSAccountService.Sendtextmessage(phoneNoList,'test message content',objAccount.Id,false,strAbndId);
    Test.stopTest();
  }

  /* * * * * * * * * * * * *
  *  Method Name:  Sendtextmessage_test3
  *  Purpose:      This method is used to unit test functionality in Sendtextmessage method
  *                if error is returned in response from endpoint
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 26-Nov-2017
  * * * * * * * * * * * * */
  static testMethod void Sendtextmessage_test3()
  {
    //insert sample data for Account
    Account objAccount = TestDataFactory_CRM.createPersonAccount();
    insert objAccount;

    Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
    Id aoptRecordTypeID = caseRecordTypes.get('AOPT').getRecordTypeId();

    Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , aoptRecordTypeID);
    objCase.Status = 'Submitted';
    insert objCase;

    List<String> phoneNoList = new List<String>();
    phoneNoList.add(objCase.Id+':9898989898');
    String strAbndId='';
    Test.startTest();
      Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(2));
      SendSMSAccountService.Sendtextmessage(phoneNoList,'test message content',objAccount.Id,false,strAbndId);
    Test.stopTest();
  }
}