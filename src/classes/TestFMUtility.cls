@isTest
public class TestFMUtility {
    public static testmethod void testUtility(){
        Id profileId = [select id from profile where name = 'System Administrator'].id;
        User u= TestDataFactoryFM.createUser(profileId);
        insert u;

        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        FM_User__c fmUser=TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;

        Tenant_History__c tenantHistoryInstance=new Tenant_History__c();
        tenantHistoryInstance.Booking_Unit__c=buIns.id;
        insert tenantHistoryInstance;

        Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Change of Contact Details').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Change_of_Contact_Details';
        fmCaseObj.Booking_Unit__c=buIns.id;
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
        fmCaseObj.Actual_move_out_date__c=Date.today();
        insert fmCaseObj;

        Id recId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Penalty Waiver').getRecordTypeId();

        FM_Additional_Detail__c insAddDet = new FM_Additional_Detail__c();
        insAddDet.Penalty_Case__c = fmCaseObj.id;
        insAddDet.RecordTypeId = recId;
        insert insAddDet;

        FM_Utility.updateTenantHistory(new List<FM_Case__c>{fmCaseObj});

        String authorities=FM_Utility.populateApprovingAuthorities(true, 'Change of Contact Details');

        String name=FM_Utility.extractName('TestDoc.htm');

        Blob body=FM_utility.extractBody('ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk');

        Blob b=FM_utility.extractBody('k');

        list<FM_Approver__mdt> approvers=FM_utility.fetchApprovers('Change_of_Name_Nat_Details','All');

        Booking_Unit__c buObj=FM_Utility.getUnitDetails(String.valueOf(buIns.id));

        list<FM_Documents__mdt> docList=FM_Utility.getDocumentsList('Change_of_Name_Nat_Details','All');

        FM_Case__c getfmCaseObj=FM_Utility.getCaseDetails(String.valueOf(fmCaseObj.id));

        List<FM_Case__c> listFMCase=FM_Utility.getCaseDetails(new List<Id>{fmCaseObj.id });

        List<Location__c> listlocObj=FM_Utility.getLocationDetails(new Set<String>{'JNU'});

        list<FM_User__c> listFMUser=FM_Utility.getApprovingUsers(new Set<String>{'JNU'});

        FM_Utility.getRelatedObjectDetails(String.valueOf(fmCaseObj.id),'Penalty_Waiver');

        String keyPrefix=FM_Utility.getObjectKeyPrefix('FM_Case__c');

        Error_Log__c recErrorlog=FM_Utility.createErrorLog(String.valueOf(acctIns.id),String.valueOf(buIns.id), String.valueOf(fmCaseObj.id),'Change_of_Name_Nat_Details', 'Error Msg');

        Map<String,FM_User__c> maplocationCodeFMUser=Fm_utility.getFMUserAdmin(new Set<String>{'JNU'});

        list<SR_Attachments__c> listDoc=fm_utility.getDocumentsList(String.valueOf(fmCaseObj.id));

        UploadMultipleDocController.MultipleDocRequest obj=FM_Utility.makeWrapperObject( 'ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk' ,
                                                                       'Test_Document_Work_Permit.htm' ,
                                                                       'Test_Document_Work_Permit.htm' ,
                                                                       fmCaseObj.Name, '1' ) ;
    }

    @isTest static void test_fetchFmCaseDetails() {
        Set<ID> sObjectSet = new Set<ID>();
        FM_Utility.fetchFmCaseDetails(sObjectSet);
    }
    @isTest static void test_fetchTaskDetails() {
        Set<Id> sObjectSet = new Set<Id>();
        FM_Utility.fetchTaskDetails(sObjectSet);
    }
    @isTest static void test_fetchSrBookings() {
        Set<Id> sObjectSet = new Set<Id>();
        FM_Utility.fetchSrBookings(sObjectSet);
    }
    @isTest static void test_getFMUserCollection() {
        Id profileId = [select id from profile where name = 'System Administrator'].id;
        User u= TestDataFactoryFM.createUser(profileId);
        insert u;
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.name='JNU';
        insert locObj;
        FM_User__c fmUser=TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.FM_Role__c = 'FM Collection';
        insert fmUser;
        Set<String> sObjectSet = new Set<String>();
        sObjectSet.add('JNU');
        FM_Utility.getFMUserCollection(sObjectSet);
    }
}