/**************************************************************************************************
* Name               : AgentPaymentDetailsController                                                   *
* Description        : Controller class for AvailableUnits component, has below functions.        *
*                       - Get available campaigns list .                                          *
*                       - Associated units to campaign.                                           * 
* Created Date       : 05/02/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         NSI - Vineet      05/02/2017      Initial Draft.                                    *
**************************************************************************************************/
public without sharing class AgentPaymentDetailsController {    
    public String sessionId {get; set;}
    public String selectedInventory {get; set;}
    public String errorMessage {get; set;} 
    public String phonenumber {get; set;}
    public String OnlinePayment{get;set;}
    public String CountryCode{get;set;}
    public Boolean generate_OTP {get;set;} 
    public Boolean isVerify{get;set;} 
    public Boolean initial{get; set;}
    public string OTP{get; set;}
    public Boolean ShowSceen5 {get; set;}
    public UtilityWrapperManager uwmObject {get; set;}  
    public string validation {get; set;} 
    public Map<Id, UtilityWrapperManager.InventoryBuyerWrapper> inventoryBuyerWrapperMap {
        get{
            return inventoryBuyerWrapperMap != null  ? inventoryBuyerWrapperMap : getInventories(uwmObject.ibwList);
        } set{
            inventoryBuyerWrapperMap = inventoryBuyerWrapperMap != null  ? inventoryBuyerWrapperMap : getInventories(uwmObject.ibwList);    
        }
    }
    public Set<Id> inventoryIdsList {
        get{
            return inventoryIdsList != null ? inventoryIdsList : getInventories(uwmObject.ibwList).keySet();
        }set{
            inventoryIdsList = inventoryIdsList != null ? inventoryIdsList : getInventories(uwmObject.ibwList).keySet();    
        }
    }
    public Buyer__c thirdParty {get; set;}
    
    /*********************************************************************************************
    * @Description : Controller class.                                                           *
    * @Params      : void                                                                        *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public AgentPaymentDetailsController(){
      system.debug('#### Payment Details Controller');
        try{
          OnlinePayment='Online Payment';
          generate_OTP = false;
          isVerify = false; 
          ShowSceen5 = false;
          initial =true;
          sessionId = UserInfo.getSessionId();
          thirdParty = new Buyer__c(Is_3rd_Party__c = true);  
        }catch(Exception ex){
            errorMessage = 'Exception at line number = '+ex.getLineNumber()+', Exception message = '+ex.getMessage();     
        }
    }  
    
    /*********************************************************************************************
    * @Description : Getter method for payment methods.                                          *
    * @Params      : void                                                                        *
    * @Return      : List<SelectOption>                                                          *
    *********************************************************************************************/
    public List<SelectOption> getPaymentParty() {
        List<SelectOption> options = new List<SelectOption>();
        if(Booking_Unit__c.getSObjectType() != null && Booking_Unit__c.getSObjectType().getDescribe() != null){
            Map<String, Schema.SObjectField> field_map = Booking_Unit__c.getSObjectType().getDescribe().fields.getMap(); 
            if(field_map.containsKey('Online_Payment_Party__c')){
                List<Schema.PicklistEntry> pick_list_values = field_map.get('Online_Payment_Party__c').getDescribe().getPickListValues();
                for (Schema.PicklistEntry thisValue : pick_list_values) { 
                    options.add(new selectOption(thisValue.getValue(), thisValue.getLabel()));
                }   
            }
        }
        return options;
    }
   /*************************************************************************
    * @Description :  Method to Generate OTP
    * @Param       : void
    * @return      : void
    * Createdby    : Naresh Kaneriya
    * Createddae   : 21/08/2017
    * ***********************************************************************/
    public void btngenerateOTP(){
      try{
        initial =false;
        generate_OTP = true;        
        System.debug('Lovel Genertae OTP----------------------');
        phonenumber = getMobileNumber();
        System.debug('Lovel Phone Number----------------------  '+phonenumber);
        AgentOTPController.GenrateOTP(phonenumber); 
        }
        catch(Exception ex){
            System.debug('Lovel Exception----------------------  '+ex.getMessage());

        }
    }
    
    /*************************************************************************
    * @Description :  Method to Verify OTP
    * @Param       : void 
    * @return      : void
    * Createdby    : Naresh Kaneriya
    * Createddae   : 21/08/2017
    * ***********************************************************************/ 
   
    public void verifyOTP(){
        try{
            ShowSceen5 =  true;     
            getShowScreen5();
          
            System.debug('Lovel Phone Number----------------------  '+phonenumber);
             isVerify =  AgentOTPController.verifyotp(phonenumber,OTP); 
            if(isVerify){
               // validation = 'OTP Sucessfully validated.';  
                errorMessage = 'OTP Sucessfully validated.';                           
                generate_OTP =  false;
                initial = false;
                
            }
            else
            { 
                //isVerify = false;
                 generate_OTP =  true; 
                 initial = false;
                 //validation = 'OTP not validated. Please enter correct OTP';
                 errorMessage = 'OTP not validated. Please enter correct OTP';

            }
        }
         catch(Exception ex){
            System.debug('Lovel Exception----------------------  '+ex.getMessage());
        }
    }
    
    public Boolean getShowScreen5(){
        
        System.debug('ShowSceen5ShowSceen5ShowSceen5====================== '+ShowSceen5);
        return ShowSceen5 ; 
    }
  
    /*********************************************************************************************
    * @Description : Getter method for payment methods.                                          *
    * @Params      : void                                                                        *
    * @Return      : List<SelectOption>                                                          *
    *********************************************************************************************/
    public List<SelectOption> getPaymentMethods() {
          Profile objProfile =[Select ID,
                                    Name
                             From Profile
                             WHERE ID=:userInfo.getProfileId()];     
                              String pname = objProfile.name;
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Select a option TET-- '));
        if(Booking_Unit__c.getSObjectType() != null && Booking_Unit__c.getSObjectType().getDescribe() != null){
            Map<String, Schema.SObjectField> field_map = Booking_Unit__c.getSObjectType().getDescribe().fields.getMap(); 
            if(field_map.containsKey('Payment_Method__c')){
                List<Schema.PicklistEntry> pick_list_values = field_map.get('Payment_Method__c').getDescribe().getPickListValues();
                for (Schema.PicklistEntry thisValue : pick_list_values) { 
                     if(!(pname.contains('Community') && (thisValue.getLabel().contains('cash') || thisValue.getLabel().contains('Damac website')))){
                              options.add(new selectOption(thisValue.getValue(), thisValue.getLabel()));
                        }
                  
                }   
            }
        }
        return options;
    }
    
    /*********************************************************************************************
    * @Description : Method to save third party details.                                         *
    * @Params      : void                                                                        *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void saveThirdPartyDetails(){
        String uniqueKey = '';
        try{    
            for(UtilityWrapperManager.InventoryBuyerWrapper thisInventory : uwmObject.ibwList){
                if(thisInventory.isSelected && thisInventory.selectedInventory.Id == selectedInventory){
                    if(thisInventory.jointBuyerList != null){
                        uniqueKey = uwmObject.dealRecord.Id;
                        for(Buyer__c thisBuyer : thisInventory.jointBuyerList){
                            uniqueKey = thisBuyer.Passport_Number__c +' - '+ uniqueKey;
                        }
                    }
                }
            }
            if(String.isNotBlank(uniqueKey)){
                for(Booking__c thisBooking : [SELECT Id FROM Booking__c WHERE Unique_Key__c =: uniqueKey]){
                    thirdParty.Booking__c = thisBooking.Id;
                    break; 
                }   
                if(thirdParty.Booking__c != null){
                    insert thirdParty;
                    /* Updating primary buyer's detail on the booking unit, in case 3rd party is selected. */
                    List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
                    for(Booking_Unit__c thisBookingUnit : [SELECT Id, Primary_Buyer_s_Email__c, Primary_Buyer_s_Name__c, 
                                                                  Primary_Buyer_Country__c, Primary_Buyer_s_Nationality__c, 
                                                                  Unique_Key__c 
                                                           FROM Booking_Unit__c 
                                                           WHERE Booking__c =: thirdParty.Booking__c]){
                        thisBookingUnit.Primary_Buyer_s_Email__c = thirdParty.Email__c; 
                        thisBookingUnit.Primary_Buyer_s_Name__c = thirdParty.First_Name__c != null ? thirdParty.First_Name__c : thirdParty.Last_Name__c;
                        thisBookingUnit.Primary_Buyer_Country__c = thirdParty.Country__c;    
                        thisBookingUnit.Primary_Buyer_s_Nationality__c = thirdParty.Nationality__c;
                        bookingUnitList.add(thisBookingUnit);
                    }
                    if(!bookingUnitList.isEmpty()){
                        update bookingUnitList;
                    }
                } 
            }
        }catch(Exception ex){
            errorMessage = 'Exception at line number = '+ex.getLineNumber()+', Exception message = '+ex.getMessage();       
        }   
    }
    
    /*********************************************************************************************
    * @Description : Method to get countries.                                                    *
    * @Params      : void                                                                        *
    * @Return      : List<SelectOption>                                                          *
    *********************************************************************************************/
    public List<SelectOption> getCountries(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Select a Country -- '));
        if(Buyer__c.getSObjectType() != null && Buyer__c.getSObjectType().getDescribe() != null){
            Map<String, Schema.SObjectField> field_map = Buyer__c.getSObjectType().getDescribe().fields.getMap(); 
            if(field_map.containsKey('Country__c')){
                List<Schema.PicklistEntry> pick_list_values = field_map.get('Country__c').getDescribe().getPickListValues();
                for (Schema.PicklistEntry thisValue : pick_list_values) { 
                    options.add(new selectOption(thisValue.getValue(), thisValue.getLabel()));
                }   
            }
        }
        return options; 
    }
    
    /*********************************************************************************************
    * @Description : Method to get cities/                                                       *
    * @Params      : void                                                                        *
    * @Return      : List<SelectOption>                                                          *
    *********************************************************************************************/
    public List<SelectOption> getCities(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Select a City -- '));
        if(Buyer__c.getSObjectType() != null && Buyer__c.getSObjectType().getDescribe() != null){
            Map<String, Schema.SObjectField> field_map = Buyer__c.getSObjectType().getDescribe().fields.getMap(); 
            if(field_map.containsKey('City__c')){
                List<Schema.PicklistEntry> pick_list_values = field_map.get('City__c').getDescribe().getPickListValues();
                for (Schema.PicklistEntry thisValue : pick_list_values) { 
                    options.add(new selectOption(thisValue.getValue(), thisValue.getLabel()));
                }   
            }
        }
        return options; 
    }
    
    /*********************************************************************************************
    * @Description : Method to upload proof of payment.                                          *
    * @Params      : String, String, String                                                      *
    * @Return      : String                                                                      *
    *********************************************************************************************/
    @RemoteAction 
    public static String uploadProof(String attachmentBody, String srId, String bookingUnitId, String fileName, String fileType){
        String result = '';
        system.debug('#### attachmentBody = '+attachmentBody);
        try{
            if(String.isNotBlank(srId) && String.isNotBlank(attachmentBody) && 
               attachmentBody.containsIgnoreCase(';base64')){
                Attachment attachmentRecord = new Attachment();
                attachmentRecord.body = EncodingUtil.base64Decode(attachmentBody.subStringAfter(';base64,'));
                attachmentRecord.name = 'Proof of payment - '+fileName;
                attachmentRecord.ContentType = fileType;
                attachmentRecord.ParentId = srId;
                insert attachmentRecord;
                result = 'success';
                system.debug('#### attachmentRecord = '+attachmentRecord);  
            }   
            if(String.isNotBlank(bookingUnitId)){
                Booking_Unit__c updateBookingUnit = new Booking_Unit__c(Id = bookingUnitId, Proof_of_Payment_Submitted__c = true);
                update updateBookingUnit;
            }
        }catch(Exception ex){
            result =  'Unable to upload file : Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage();   
        }
        return result;
    }
    
    /*********************************************************************************************
    * @Description : Method to parse only selected inventories.                                  *
    * @Params      : List<UtilityWrapperManager.InventoryBuyerWrapper>                           *
    * @Return      : Map<Id, UtilityWrapperManager.InventoryBuyerWrapper>                        *
    *********************************************************************************************/
    private Map<Id, UtilityWrapperManager.InventoryBuyerWrapper> getInventories(List<UtilityWrapperManager.InventoryBuyerWrapper> inventoryList){
        Map<Id, UtilityWrapperManager.InventoryBuyerWrapper> valueMap = new Map<Id, UtilityWrapperManager.InventoryBuyerWrapper>();
        if(inventoryList != null && !inventoryList.isEmpty()){
            for(UtilityWrapperManager.InventoryBuyerWrapper thisRecord : inventoryList){
                if(thisRecord.isSelected){ 
                    valueMap.put(thisRecord.selectedInventory.Id, thisRecord);
                }
            }
        }   
        return valueMap;
    }

     
     /*********************************************************************************************
    * @Description : Method to get Primary buyer Phone Number and CountryCode for OTP.                                        *
    * @Params      : String                                                                        *
    * @Return      : String  
    * @CreatedBy   : Naresh Kaneriya
    * @CreatedDate : 28/08/2017
    *********************************************************************************************/
    public String  getMobileNumber(){
      List<Buyer__c> BuyerList =  new List<Buyer__c>();
        try{    
            for(UtilityWrapperManager.InventoryBuyerWrapper thisInventory : uwmObject.ibwList){
                    System.debug('In jointBuyerList.jointBuyerList---------- '+thisInventory.jointBuyerList);
                   if(thisInventory.isSelected){ 
                        BuyerList = thisInventory.jointBuyerList ;
                 }
             }
            System.debug('In BuyerList---BuyerListBuyerListBuyerList Id-------' +BuyerList);
            if(!BuyerList.isEmpty()){
                for(Buyer__c buyer : BuyerList ){
                    if(buyer.Primary_Buyer__c == true){
                       PhoneNumber = buyer.Phone__c;
                       CountryCode =  (buyer.Phone_Country_Code__c.replaceAll('[^0-9]', ''));
                       CountryCode = CountryCode.substring(2,CountryCode.length());
                    }
                }
               
                System.debug('PhoneNumber OTP------' +PhoneNumber);
                System.debug('CountryCode OTP------' +CountryCode);
            }
            else{
              errorMessage = 'We did not get Primary Buyer Phone Number.';   
            }
         }
         catch(Exception ex){
            errorMessage = 'Exception at line number = '+ex.getLineNumber()+', Exception message = '+ex.getMessage();       
        } 
        return CountryCode+PhoneNumber ;
    }
   
}//End of class.