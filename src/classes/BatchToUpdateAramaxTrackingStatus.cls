Global class BatchToUpdateAramaxTrackingStatus implements Database.Batchable<sObject>, schedulable, Database.AllowsCallouts{
    public AirwayBillTrackListResponse trackingRes{get;set;}
    public AramaxTrackingResponseJSON2Apex trackingResAremax{get;set;}
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('SELECT Id, Country__c, Account__r.Party_ID__c ,Courier_Status__c, Courier_Service__c, Airway_Bill__c, City__c, Postal_Code__c FROM Courier_Delivery__c');
    }

    global void execute(Database.BatchableContext BC, List<Courier_Delivery__c> lstCourier) {
        List<Courier_Delivery__c> lstCourierToUpdate = new List<Courier_Delivery__c>();
        for(Courier_Delivery__c objCourier: lstCourier){
            System.debug('objCourier: '+objCourier);
            if(objCourier.Courier_Service__c != Null && (objCourier.Courier_Service__c == 'Aramex' || objCourier.Courier_Service__c == 'Aramex CDS')){
                AramaxTrackingResponseJSON2Apex objTrackingResponse = AramexService.getAramexTracking(objCourier.Airway_Bill__c, objCourier.Country__c, 'DXB', objCourier.Account__r.Party_ID__c);
                System.debug('objTrackingResponse: '+objTrackingResponse);
                System.debug('objTrackingResponse.TrackingResults: '+objTrackingResponse.TrackingResults);
                Map<String, List<AramaxTrackingResponseJSON2Apex.Value>> mapTrackingKeyValue = new Map<String, List<AramaxTrackingResponseJSON2Apex.Value>>();
                for(AramaxTrackingResponseJSON2Apex.TrackingResults objTrack: objTrackingResponse.TrackingResults){
                    System.debug('objTrack: '+objTrack);
                    mapTrackingKeyValue.put(objTrack.key, objTrack.value);
                }
                if(!mapTrackingKeyValue.isEmpty()){
                    Datetime dt;
                    String courierStatus;
                    List<AramaxTrackingResponseJSON2Apex.value> lstValues = mapTrackingKeyValue.get(objCourier.Airway_Bill__c);
                    for(AramaxTrackingResponseJSON2Apex.value objAramexValue: lstValues){
                        String strTime = objAramexValue.UpdateDateTime;
                        String str = strTime.substringBetween('(', '+');
                        Long objL = Long.valueOf(str);
                        Datetime tempdt = DateTime.newInstance(objL);
                        if(dt == Null || dt != Null && tempdt > dt){
                            dt = tempdt;
                            courierStatus = objAramexValue.UpdateDescription;
                        }
                    }
                    System.debug('courierStatus: '+courierStatus);
                    if(courierStatus != Null){
                        lstCourierToUpdate.add(new Courier_Delivery__c(Id = objCourier.Id, Courier_Status__c = courierStatus));
                    }
                }
            }else if(objCourier.Courier_Service__c != Null && objCourier.Courier_Service__c == 'First Flight'){
                AirwayBillTrackListResponse objFirstFlightRes = FirstFlightService.getFirstFlightTracking(objCourier.Airway_Bill__c, objCourier.Account__r.Party_ID__c, objCourier.Country__c);
                //AirwayBillTrackListResponse objFirstFlightRes = FirstFlightService.getFirstFlightTracking('123456', '3000', 'AE');
                System.debug('objFirstFlightRes: '+objFirstFlightRes);
                Map<String, List<AirwayBillTrackListResponse.TrackingLogDetails>> mapBillNoToTrackingLog = new Map<String, List<AirwayBillTrackListResponse.TrackingLogDetails>>();
                for(AirwayBillTrackListResponse.AirwayBillTrackList objTracklist: objFirstFlightRes.AirwayBillTrackList){
                    System.debug('objTracklist: '+objTracklist);
                    mapBillNoToTrackingLog.put(objTracklist.AirWayBillNo, objTracklist.TrackingLogDetails);
                }
                System.debug('mapBillNoToTrackingLog: '+mapBillNoToTrackingLog);
                List<AirwayBillTrackListResponse.TrackingLogDetails> lstLogDetails = mapBillNoToTrackingLog.get(objCourier.Airway_Bill__c);
                String courierStatus;
                Date recentDate;
                Decimal recentTime;
                if(lstLogDetails != Null && !lstLogDetails.isEmpty()){
                    for(AirwayBillTrackListResponse.TrackingLogDetails objLogDetails: lstLogDetails){
                        System.debug('objLogDetails: '+objLogDetails);
                        Date tempDate = getDateFromString(objLogDetails.ActivityDate);
                        System.debug('tempDate: '+tempDate);
                        Decimal activityTime = getTimeFromString(objLogDetails.ActivityTime);
                        System.debug('activityTime: '+activityTime);
                        if(recentDate == Null || tempDate > recentDate || (tempDate == recentDate && activityTime > recentTime)){
                            recentDate = tempDate;
                            recentTime = activityTime;
                            courierStatus = objLogDetails.Status;
                        }
                        System.debug('recentDate: '+recentDate);
                    }
                }
                
                if(courierStatus != Null){
                    lstCourierToUpdate.add(new Courier_Delivery__c(Id = objCourier.Id, Courier_Status__c = courierStatus));
                }
            }
        }
        System.debug('lstCourierToUpdate: '+lstCourierToUpdate);
        if(!lstCourierToUpdate.isEmpty()){
            Update lstCourierToUpdate;
        }
    }
    
    public static Date getDateFromString(String strDate){
        Map<String, String> mapMonthNumber= new Map<String, String>{'January' => '01', 'February' => '02', 'March' => '03', 'April' => '04', 'May' => '05', 'June' => '06', 'July' => '07', 'August' => '08', 'September' => '09', 'Octomber' => '10', 'November' => '11', 'December' => '12'};
        List<String> lstSplittedDate = strDate.normalizeSpace().split(' ');
        System.debug('lstSplittedDate: '+lstSplittedDate);
        String strParseDate = lstSplittedDate[1] + '/' + mapMonthNumber.get(lstSplittedDate[2]) + '/' + lstSplittedDate[3];
        System.debug('strParseDate: '+strParseDate);
        Date tempDate = date.parse(strParseDate);
        return tempDate;
    }
    
    public static Decimal getTimeFromString(String strTime){
        List<String> strSplit = strTime.split(':');
        String strFormattedTime = strSplit[0] + '.' +strSplit[1];
        return Decimal.valueOf(strFormattedTime);
    }

    global void finish(Database.BatchableContext BC) {
    // finish code
    }
    
    global void execute(SchedulableContext SC) {
        BatchToUpdateAramaxTrackingStatus b = new BatchToUpdateAramaxTrackingStatus ();
        database.executebatch(b,200);
    }
}