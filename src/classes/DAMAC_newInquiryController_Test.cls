@isTest
private with sharing class DAMAC_newInquiryController_Test {
    @isTest
    public static void createInquiry() {
        Inquiry__c inq = new Inquiry__c ();
        inq.first_name__c = 'test';
        inq.last_name__c = 'test';
        inq.email__c = 'testemailinq@test.com';
        inq.Nationality__c = 'Indian';
        inq.Preferred_Language__c = 'English';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.Mobile_Phone_Encrypt__c = '12321321';    
    
        DAMAC_newInquiryController obj = new DAMAC_newInquiryController ();
        obj.selectedCountryCode = 'India: 0091';
        obj.selectedNationality = 'Indian';
        obj.selectedLanguage = 'English';
        
        obj.inq = inq;
        obj.submitInquiry ();
        system.assertEquals (obj.statusMessage, 'Success');
    }
    @isTest
    public static void createInquiryFail () {
        Inquiry__c inq = new Inquiry__c ();
        inq.first_name__c = 'test';
        inq.last_name__c = 'test';
        inq.email__c = 'testemailinq@test.com';
        inq.Nationality__c = 'Indian';
        inq.Preferred_Language__c = 'English';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.Mobile_Phone_Encrypt__c = '12321321';    
    
        DAMAC_newInquiryController obj = new DAMAC_newInquiryController ();
        obj.selectedNationality = 'Indian';
        obj.selectedLanguage = 'English';
        
        obj.inq = inq;
        obj.submitInquiry ();
        system.assertNOTEquals (obj.statusMessage, '');
    }
}