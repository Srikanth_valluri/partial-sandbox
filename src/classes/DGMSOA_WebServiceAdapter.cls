public class DGMSOA_WebServiceAdapter extends GenericWebServiceAdapter implements WebServiceAdapter{

    public String url; 
    
    public override WebServiceAdapter call(){
        super.call();
        String regID_BUID = parameters.get('regID_BUID');
        system.debug('!!!!!!!!regID_BUID'+regID_BUID);
        String BUId, regID;
        
        if(String.isNotBlank(regID_BUID)) {
            BUId = regID_BUID.substringBefore('-');
            regID = regID_BUID.substringAfter('-');
        }
        GenerateDGMSOAController.soaResponse objSOA = GenerateDGMSOAController.getSOADocument(regID);
        if(objSOA != NULL && String.isNotBlank(objSOA.url)) {
            this.url = objSOA.url;
        }
        return this;
    }
    
    public override String getResponse(){
        super.getResponse();
        return this.url;
    }
    
}