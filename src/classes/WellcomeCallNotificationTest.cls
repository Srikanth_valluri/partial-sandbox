@isTest
public with sharing class WellcomeCallNotificationTest {

    static Id welcomeCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Welcome Calling List').RecordTypeId;
    
    
    @IsTest
    static void methodName(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        // Insert Accont
        Account objAcc =  new Account(RecordTypeId = personAccRTId, 
                       FirstName='Test FirstName1', 
                       LastName='Test LastName2', 
                       Email__pc = 'test@test.com',
                       Email__c  = 'test@test.com',
                        Mobile_Phone_Encrypt__pc = '00919405883798',
                        Mobile__c = '00919405883798',
                       Type='Person'
                      );
        insert objAcc ;

    

/*Id FolderId = [Select Id from Folder limit 1].id;

EmailTemplate e = new EmailTemplate (developerName = 'Welcome calling list Email', FolderId = folderId, 
TemplateType= 'Text', Name = 'Welcome calling list Email'); 

insert e;
*/

Calling_List__c welcomeCallInst = new Calling_List__c(RecordTypeId = welcomeCallingRecordTypeId,
                                                 Call_Outcome__c = 'Recovery list',
                                                 Customer_Flag__c = true,
                                                 Registration_ID__c = '1111',
                                                 Welcome_Call_Outcome__c = 'In Progress',
                                                 Account__c = objAcc.Id ,
                                                  Party_ID__c = '1177922');

        insert welcomeCallInst;
        
        Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
        
        Test.startTest();
        
        list <Id> listId = new list <Id>();
        listId.add(welcomeCallInst.Id);
        WellcomeCallNotification.getCallingLst(listId);
        
        Test.stopTest();
        
    }
}