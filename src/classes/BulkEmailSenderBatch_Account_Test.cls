@isTest
public class BulkEmailSenderBatch_Account_Test {

    @isTest
    static void testBusinessAccount() {
        insert new Account(Name = 'Test Account'
                        , Email__c = 'test321@ttt.com'
                        , Send_Bulk_Email__c = true
                        , Mobile__c = '429842039'
                        , SurveyCx__c = 'www.tt332203.com'
                        , Nationality__c = 'African');

        insert new Account(Name = 'Test Account'
                        , Email__c = 'test3232431@ttt.com'
                        , Send_Bulk_Email__c = true);

        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        objAccount.Email__pc = 'test322323@ttt.com';
        objAccount.Nationality__pc = 'African';
        objAccount.Mobile_Phone_Encrypt__pc = '243234';
        objAccount.First_Name__c = 'Test';
        objAccount.Send_Bulk_Email__c = true;
        objAccount.Last_Name__c = 'test';
        insert objAccount;

        // Account objAccount2 = TestDataFactory_CRM.createPersonAccount();
        // objAccount2.Email__pc = 'test325551@ttt.com';
        // objAccount2.Send_Bulk_Email__c = true;
        // insert objAccount2;

        System.debug('Test Account = ' + [Select Person_Business_Email__c, Send_Bulk_Email__c from Account]);
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        Test.startTest();
        Database.executeBatch( new BulkEmailSenderBatch_Account() );
        Test.stopTest();
    }
    @isTest
    public static void testMethod2 () {
        insert new Account(Name = 'Test Account'
                        , Email__c = 'test321@ttt.com'
                        , Send_Bulk_Email__c = true
                        , Mobile__c = '429842039'
                        , SurveyCx__c = 'www.tt332203.com'
                        , Nationality__c = 'African');

        insert new Account(Name = 'Test Account'
                        , Email__c = 'test3232431@ttt.com'
                        , Send_Bulk_Email__c = true);

        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        objAccount.Email__pc = 'test322323@ttt.com';
        objAccount.Nationality__pc = 'African';
        objAccount.Mobile_Phone_Encrypt__pc = '243234';
        objAccount.First_Name__c = 'Test';
        objAccount.Send_Bulk_Email__c = true;
        objAccount.Last_Name__c = 'test';
        insert objAccount;

        // Account objAccount2 = TestDataFactory_CRM.createPersonAccount();
        // objAccount2.Email__pc = 'test325551@ttt.com';
        // objAccount2.Send_Bulk_Email__c = true;
        // insert objAccount2;

        System.debug('Test Account = ' + [Select Person_Business_Email__c, Send_Bulk_Email__c from Account]);
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        Test.startTest();
        String strContents = '{!Name} {!Nationality} {!Email} {!Mobile} {!SurveyCx} {CRM Survey Link}';
        new BulkEmailSenderBatch_Account().replaceMergeFields( objAccount, strContents );
        //Database.executeBatch( new BulkEmailSenderBatch_Account() );
        Test.stopTest();
    }
}