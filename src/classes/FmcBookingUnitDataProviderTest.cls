@isTest
private class FmcBookingUnitDataProviderTest {

    @isTest
    static void testProvidedData() {
        Account account = new Account(Name = 'Test Account');
        insert account;

        CustomerCommunityUtils.customerAccountId = account.Id;

        DataDisplayConfig config = new DataDisplayConfig();
        config.strategy = 'class';
        config.recordLimit = '5000';
        config.objectName = 'Booking_Unit__c';
        config.fieldset = '';
        config.fieldList = new List<Map<String,String>> {
            new Map<String, String> {
                'title' => 'Unit Name',
                'field' => 'Unit_Name__c',
                'type' => 'link',
                'referencedFields' => 'Id',
                'href' => '?view=UnitDetails&id={!Id}',
                'target' => '_self',
                'class' => 'unit-name'
            },
            new Map<String, String> {
                'title' => 'Project',
                'field' => 'Property_Name__c'
            },
            new Map<String, String> {
                'title' => 'Resident',
                'field' => 'Resident__r.Name'
            },
            new Map<String, String> {
                'title' => 'DLP Till',
                'field' => 'DLP_End_Date__c',
                'type' => 'date'
            }
        };

        config.displayAs = 'table';
        config.dataProviderClass = 'FmcBookingUnitDataProvider';
        config.dataList = new List<Map<String, Object>>();
        config.configJSON = 'FmcBookingUnitDisplayConfig';
        new FmcBookingUnitDataProvider().getData(config);
    }

    @isTest
    static void testProvidedOwnedData() {
        Account account = new Account(Name = 'Test Account');
        insert account;

        CustomerCommunityUtils.customerAccountId = account.Id;

        DataDisplayConfig config = new DataDisplayConfig();
        config.strategy = 'class';
        config.recordLimit = '5000';
        config.objectName = 'Booking_Unit__c';
        config.fieldset = '';
        config.fieldList = new List<Map<String,String>> {
            new Map<String, String> {
                'title' => 'Unit Name',
                'field' => 'Unit_Name__c',
                'type' => 'link',
                'referencedFields' => 'Id',
                'href' => '?view=UnitDetails&id={!Id}',
                'target' => '_self',
                'class' => 'unit-name'
            },
            new Map<String, String> {
                'title' => 'Project',
                'field' => 'Property_Name__c'
            },
            new Map<String, String> {
                'title' => 'Resident',
                'field' => 'Resident__r.Name'
            },
            new Map<String, String> {
                'title' => 'DLP Till',
                'field' => 'DLP_End_Date__c',
                'type' => 'date'
            }
        };

        config.displayAs = 'table';
        config.dataProviderClass = 'FmcBookingUnitDataProvider';
        config.dataList = new List<Map<String, Object>>();
        config.configJSON = 'FmcBookingUnitDisplayConfig';
        config.filter = ' WHERE Booking__r.Account__c = \'' + account.Id + '\' ';
        new FmcBookingUnitDataProvider().getData(config);
    }

    @isTest
    static void testProvidedLeasedData() {
        Account account = new Account(Name = 'Test Account');
        insert account;

        CustomerCommunityUtils.customerAccountId = account.Id;

        DataDisplayConfig config = new DataDisplayConfig();
        config.strategy = 'class';
        config.recordLimit = '5000';
        config.objectName = 'Booking_Unit__c';
        config.fieldset = '';
        config.fieldList = new List<Map<String,String>> {
            new Map<String, String> {
                'title' => 'Unit Name',
                'field' => 'Unit_Name__c',
                'type' => 'link',
                'referencedFields' => 'Id',
                'href' => '?view=UnitDetails&id={!Id}',
                'target' => '_self',
                'class' => 'unit-name'
            },
            new Map<String, String> {
                'title' => 'Project',
                'field' => 'Property_Name__c'
            },
            new Map<String, String> {
                'title' => 'Resident',
                'field' => 'Resident__r.Name'
            },
            new Map<String, String> {
                'title' => 'DLP Till',
                'field' => 'DLP_End_Date__c',
                'type' => 'date'
            }
        };

        config.displayAs = 'table';
        config.dataProviderClass = 'FmcBookingUnitDataProvider';
        config.dataList = new List<Map<String, Object>>();
        config.configJSON = 'FmcBookingUnitDisplayConfig';
        config.filter = ' WHERE Tenant__c = \'' + account.Id + '\' ';
        new FmcBookingUnitDataProvider().getData(config);
    }

}