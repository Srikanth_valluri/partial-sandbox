/*-------------------------------------------------------------------------------------------------
Description: Test class for UploadMultipleDocController

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 21-11-2017       | Lochana Rajput    | 1. To test upload document functionality for IPMS
   =============================================================================================================================
*/
@isTest
private class UploadMultipleDocControllerTest {

	@testSetup
	static void createSetupDate() {
		Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
		Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;
	}
	@isTest static void test_DocumrntUploadSuccess() {
		Account objAcc = [SELECT Id,party_ID__c FROM Account LIMIT 1];
		String crfAttachmentName = 'Test1.pdf';
        String str1 = 'TestStringToBlob';
		Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
		Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
		insert objCase;
		List<UploadMultipleDocController.MultipleDocRequest> lstReq = new List<UploadMultipleDocController.MultipleDocRequest>();
		UploadMultipleDocController.MultipleDocRequest reqObj1 = new UploadMultipleDocController.MultipleDocRequest();
        reqObj1.category = 'Document';
        reqObj1.entityName = 'Damac Service Requests';
		Blob objBlob = Blob.valueof(str1);
        reqObj1.base64Binary =  EncodingUtil.base64Encode(objBlob);
        reqObj1.fileDescription = 'CRF Form';
        reqObj1.fileId = 'IPMS-'+objAcc.party_ID__C+'-'+crfAttachmentName;
        reqObj1.fileName = 'IPMS-'+objAcc.party_ID__C+'-'+crfAttachmentName;
        reqObj1.registrationId = objCase.CaseNumber;
        reqObj1.sourceFileName = 'IPMS-'+objAcc.party_ID__C+'-'+crfAttachmentName;
        reqObj1.sourceId = 'IPMS-'+objAcc.party_ID__C+'-'+crfAttachmentName;
        lstReq.add(reqObj1);
		//Set mock response
		Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
		test.startTest();
		UploadMultipleDocController.strLabelValue='N';
        UploadMultipleDocController.getMultipleDocUrl(lstReq);
        test.stopTest();
	}

	@isTest static void test_DocumrntUploadFailure() {
			Account objAcc = [SELECT Id,party_ID__c FROM Account LIMIT 1];
			String crfAttachmentName = 'Test1.pdf';
	        String str1 = 'TestStringToBlob';
			Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
			Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
			insert objCase;
			List<UploadMultipleDocController.MultipleDocRequest> lstReq = new List<UploadMultipleDocController.MultipleDocRequest>();
			UploadMultipleDocController.MultipleDocRequest reqObj1 = new UploadMultipleDocController.MultipleDocRequest();
	        reqObj1.category = 'Document';
	        reqObj1.entityName = 'Damac Service Requests';
			Blob objBlob = Blob.valueof(str1);
	        reqObj1.base64Binary =  EncodingUtil.base64Encode(objBlob);
	        reqObj1.fileDescription = 'CRF Form';
	        reqObj1.fileId = 'IPMS-'+objAcc.party_ID__C+'-'+crfAttachmentName;
	        reqObj1.fileName = 'IPMS-'+objAcc.party_ID__C+'-'+crfAttachmentName;
	        reqObj1.registrationId = objCase.CaseNumber;
	        reqObj1.sourceFileName = 'IPMS-'+objAcc.party_ID__C+'-'+crfAttachmentName;
	        reqObj1.sourceId = 'IPMS-'+objAcc.party_ID__C+'-'+crfAttachmentName;
	        lstReq.add(reqObj1);
			//Set mock response
			Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(2));
			test.startTest();
			UploadMultipleDocController.strLabelValue='N';
	        UploadMultipleDocController.getMultipleDocUrl(lstReq);
	        test.stopTest();
		}

    @isTest static void test_DocumrntUploadOfficeSuccess() {
		Account objAcc = [SELECT Id,party_ID__c FROM Account LIMIT 1];
		String crfAttachmentName = 'Test1.pdf';
        String str1 = 'TestStringToBlob';
		Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
		Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
		insert objCase;
		List<UploadMultipleDocController.MultipleDocRequest> lstReq = new List<UploadMultipleDocController.MultipleDocRequest>();
		UploadMultipleDocController.MultipleDocRequest reqObj1 = new UploadMultipleDocController.MultipleDocRequest();
        reqObj1.category = 'Document';
        reqObj1.entityName = 'Damac Service Requests';
		Blob objBlob = Blob.valueof(str1);
        reqObj1.base64Binary =  EncodingUtil.base64Encode(objBlob);
        reqObj1.fileDescription = 'CRF Form';
        reqObj1.fileId = 'IPMS-'+objAcc.party_ID__C+'-'+crfAttachmentName;
        reqObj1.fileName = 'IPMS-'+objAcc.party_ID__C+'-'+crfAttachmentName;
        reqObj1.registrationId = objCase.CaseNumber;
        reqObj1.sourceFileName = 'IPMS-'+objAcc.party_ID__C+'-'+crfAttachmentName;
        reqObj1.sourceId = 'IPMS-'+objAcc.party_ID__C+'-'+crfAttachmentName;
        lstReq.add(reqObj1);
		Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
		//Set mock response
		Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
		test.startTest();
        UploadMultipleDocController.strLabelValue='Y';
        UploadMultipleDocController.getMultipleDocUrl(lstReq);
        test.stopTest();
	}
    



}