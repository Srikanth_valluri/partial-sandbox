@istest
public class NumberToWord_Test{
    static testmethod void Test_Positive(){
        NumberToWord.english_number(100000);
        NumberToWord.convert_nn(99);
        NumberToWord.convert_nnn(999);
    }
    static testmethod void Test_Negative(){
        NumberToWord.english_number(100000);
        NumberToWord.convert_nn(9449);
        NumberToWord.convert_nnn(99);
    }
}