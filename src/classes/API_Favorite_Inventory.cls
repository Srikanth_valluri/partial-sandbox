/************************************************************************************************
 * @Name              : API_Favorite_Inventory
 * @Test Class Name   : API_Favorite_Inventory_Test
 * @Description       : RestReource Class for Inquiries  
 *    GET Method
 *    Return the list of Favorite Inventories related to the looged in Agent
 * 
 *    POST Method
 *    Adds/Removes Favorite Inventories related to the looged in Agent
 * 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         MZaklama       01/04/2020       Created
***********************************************************************************************/
@RestResource(urlMapping='/api_FavoriteInventory')
global class API_Favorite_Inventory {

    @HttpGet
    global static List<Inventory__c> doGet() {
        String contactId = getCommunityContactId();
        
        Set<Id> inventoryIdSet = new Set<Id>();
        for (Agent_Favorite_Inventory__c favUnit : [SELECT Inventory__c FROM Agent_Favorite_Inventory__c WHERE Contact__c = :contactId]) {
            inventoryIdSet.add(favUnit.Inventory__c);
        }
        
        Map<String, String> parentObjectNameMap = new Map<String, String>();
        
        parentObjectNameMap.put('Marketing_Name_Doc__c', 'Marketing_Documents__c');
         
        String query = SOQLHelper.getObjectQueryWithParent('Inventory__c', parentObjectNameMap);
        query += ' WHERE Id IN :inventoryIdSet';
        
        List<Inventory__c> inventoryList = Database.query(query);
        //Map <String, List<Inventory__c>> responseMap = new Map <String, List <Inventory__c>> ();
        //responseMap.put ('Success', inventoryList);
        //return responseMap;
        return inventoryList;
    }
    
    @HttpPost
    global static void doPost(List<FavoriteInventoryRequestDTO> favoriteInventoryList) {
        Integer statusCode = 200;
        
        String returnMessage = toggleFavoriteInventory(favoriteInventoryList);
        String responseVal = '';
        if (returnMessage == 'success') {
            statusCode = 200;
            responseVal = '{"status" : "success"}';            
        } else {
            statusCode = 500;
            responseVal = '{"Error" : "'+returnMessage+'"}';
        }
        
        RestResponse response = RestContext.response;
        response.statusCode = statusCode;
        response.addHeader('Content-Type', 'application/json');

        response.responseBody = Blob.valueOf(responseVal );
    }
    
    global static String toggleFavoriteInventory(List<FavoriteInventoryRequestDTO> favoriteInventoryList) {
        try {
            String contactId = getCommunityContactId();
            
            Set<String> deleteInventoryIdSet = new Set<String>();
            Set<String> insertInventoryIdSet = new Set<String>();
            
            for (FavoriteInventoryRequestDTO favInvDTO : favoriteInventoryList) {
                if (favInvDTO.isFavorite) {
                    insertInventoryIdSet.add(favInvDTO.inventoryId);
                } else {
                    deleteInventoryIdSet.add(favInvDTO.inventoryId);
                }
            }
            
            List<Agent_Favorite_Inventory__c> deleteAgentFavoriteInventoryList = new List<Agent_Favorite_Inventory__c>([SELECT Id FROM Agent_Favorite_Inventory__c WHERE Contact__c = :contactId AND Inventory__c IN :deleteInventoryIdSet]);
            
            if (deleteAgentFavoriteInventoryList.size() > 0) {
                delete deleteAgentFavoriteInventoryList;
            }
            
            List<Agent_Favorite_Inventory__c> newAgentFavoriteInventoryList = new List<Agent_Favorite_Inventory__c>();
            for (String inventoryId : insertInventoryIdSet) {
                Agent_Favorite_Inventory__c favoriteInventory = new Agent_Favorite_Inventory__c();
                favoriteInventory.Contact__c = contactId;
                favoriteInventory.Inventory__c = inventoryId;
                favoriteInventory.External_Id__c = contactId + inventoryId;
                newAgentFavoriteInventoryList.add(favoriteInventory);
            }
            
            upsert newAgentFavoriteInventoryList External_Id__c;
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            return ex.getMessage();
        }
        
        return 'success';
    }
    
    private static String getCommunityContactId() {
        String contactId = null;
        for (User currentUser : [SELECT Id, Name, ContactId FROM User WHERE Id = :UserInfo.getUserId() AND ContactId != null]) {
            contactId = currentUser.ContactId;
        }
        
        return contactId;
    }
    
    global class FavoriteInventoryRequestDTO {
        public String inventoryId {get; set;}
        public Boolean isFavorite {get; set;}
    }
    
}