@isTest
private class ReservationFormSigningController_Test {
    @testSetup static void setup() {
        Signnow_credentials__c credentials = new Signnow_credentials__c ();
        credentials.User_name__c = 'test';
        credentials.password__c = 'test';
        credentials.API_Key__c = 'test';
        credentials.Endpoint_url__c = 'https://api.signnow.com';
        credentials.Callback_URL__c = 'test.com';
        credentials.From_Email__c = 'test@test.com';
        credentials.Subject__c = 'test';
        credentials.Email_Body__c = 'test';
        insert credentials;
    }
    static testMethod void reservationFormSignTest() {
        
        Account acc = insertAccount();
        Inquiry__c inquiry = insertInquiry();
        Form_Request__c formRequest = insertFormRequest(acc.Id, inquiry.Id);
        
        Test.startTest();
        PageReference pageRef = Page.ReservationFormPDFClone;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('recId', String.valueOf(formRequest.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(formRequest);
        ReservationFormSigningController rfsc = new ReservationFormSigningController(sc);
        
        ReservationFormSigningController.GeneratePDF (String.valueOf(formRequest.Id));
        Test.stopTest();
    }
    
    private static Form_Request__c insertFormRequest(Id accId, Id inquiryId) {
        Form_Request__c formRequest = new Form_Request__c();
        formRequest.Corporate__c = accId;
        formRequest.Inquiry__c = inquiryId;
        formRequest.Status__c = 'Sign Now';
        insert formRequest;
        return formRequest;
    }
    
    private static Account insertAccount() {
        Account acc = new Account();
        acc.Name = 'Testing Account';
        acc.Agency_Email__c = 'acc@email.com';
        insert acc;
        return acc;
    }
    
    private static Inquiry__c insertInquiry() {
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c = 'First';
        inquiry.Last_Name__c = 'Last';
        inquiry.Email__c = 'test@email.com';
        insert inquiry;
        return inquiry;
    }
}