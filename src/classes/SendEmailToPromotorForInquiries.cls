global class SendEmailToPromotorForInquiries implements Database.batchable<sObject>,Database.Stateful{

    public Datetime dttime ;
    public Datetime timeThreeHoursEarlier;    
    Map<String,Map<String,Integer>> mapManagerUserCount=new Map<String,Map<String,Integer>>();
    
    global SendEmailToPromotorForInquiries(Datetime dt){
        dttime = dt;      
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
      //DateTime timeThreeHoursEarlier=System.now().addHours(-3);
      Id Profileid = [Select id from profile where name ='Promoter Community Profile'].id;
      timeThreeHoursEarlier= dttime.addHours(-3);
      //System.debug('timeThreeHoursEarlier====='+timeThreeHoursEarlier);
      //List<String> listStringDateTime=String.valueOfGMT(timeThreeHoursEarlier).split(' ');
      //datetimeqrstr=listStringDateTime[0]+'T'+listStringDateTime[1]+'Z'; 
      //String query='Select id , Name, createdby.name,Created_By_Current_Manager__c,Created_By_Manager__r.Email from Promoter_Inquiry__c where createddate >=' +dateTimeStr;
      //String query ='Select id,Manager.name,Managerid,Manager.email,name,(Select id from Promoter_Inquiries__r where createddate >= '+dateTimeStr+' ) from User where manager.email != null and profileid =\''+Profileid +'\''   ;
      String query;
      if(Test.isrunningtest()){
          query ='Select id,Manager.name,Managerid,Manager.email,name from User where manager.email != null and isactive = true and profileid =\''+Profileid +'\' limit 10'   ;
       }else{
          query ='Select id,Manager.name,Managerid,Manager.email,name from User where manager.email != null and isactive = true and profileid =\''+Profileid +'\''   ;
       }   
      System.debug('Querry @@@' + query);
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<User> scope){
       Map<String,Integer> promotername_countmap = new Map<String,Integer>();
       AggregateResult[] groupedResults = [select count(Id),createdBy.name aliasname from Promoter_Inquiry__c where createddate >= :timeThreeHoursEarlier group by createdBy.name];
       for (AggregateResult ar : groupedResults)  {
            //Promoter_Inquiry__c obj = (Promoter_Inquiry__c) ar;   
            promotername_countmap.put((String)ar.get('aliasname'),(Integer)ar.get('expr0')); 
       }
       System.debug('map @@@@' + promotername_countmap);
       
       for(User u : scope){
           String key = u.manager.name+','+ u.manager.email+','+u.managerid;
           if(mapManagerUserCount.containsKey(key)){
                Map<String,Integer> promotermap = mapManagerUserCount.get(key);
                if(promotername_countmap.containskey(u.name) && promotername_countmap.get(u.name) < 5){
                    promotermap.put(u.name,promotername_countmap.get(u.name));
                }else if(!promotername_countmap.containskey(u.name)){
                    promotermap.put(u.name,0);
                }
                //for(promotermap.key)   
           }else{
               if(promotername_countmap.containskey(u.name) && promotername_countmap.get(u.name) < 5){
                   mapManagerUserCount.put(key,new Map<String,Integer>{u.name=>promotername_countmap.get(u.name)});
               }else if(!promotername_countmap.containskey(u.name)){
                   mapManagerUserCount.put(key,new Map<String,Integer>{u.name=>0});
               }
           }           
       }
       
       System.debug('Map @@@ '+ mapManagerUserCount);
       /*for(Promoter_Inquiry__c inqIns:scope){
           if(inqIns.Created_By_Manager__c != null){    
            String key=inqIns.Created_By_Current_Manager__c+','+inqIns.Created_By_Manager__r.Email+','+inqIns.Created_By_Manager__c;
               if(mapManagerUserCount.containsKey(key)){
                   if(mapManagerUserCount.get(key).containsKey(inqIns.CreatedBy.Name)){
                       Integer count=mapManagerUserCount.get(key).get(inqIns.CreatedBy.Name);
                       count=count+1;
                       mapManagerUserCount.get(key).put(inqIns.CreatedBy.Name,count);
                   }else{
                       mapManagerUserCount.get(key).put(inqIns.CreatedBy.Name,1);
                   }
               }else{
                   mapManagerUserCount.put(key,new Map<String,Integer>{inqIns.CreatedBy.Name=>1});
               }
           }
        }
        Id Profileid = [Select id from profile where name ='Promoter Community Profile'].id;
        List<User> promoterusers = [ Select id,Manager.name,Managerid,Manager.email,name from User where profileid = :Profileid];
        for(User obj : promoterusers){
            String key = obj.manager.name +','+obj.manager.email +','+obj.managerid;
            if(mapManagerUserCount.containskey(key)){
                if(!(mapManagerUserCount.get(key).containskey(obj.name))){
                     mapManagerUserCount.get(key).put(obj.name,0);   
                }
            }
        } */       
    }

   global void finish(Database.BatchableContext BC){
       List<Messaging.SingleEmailMessage> mails=new List<Messaging.SingleEmailMessage>();
       OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
       for(String str:mapManagerUserCount.keyset()){
           String mailBody='Hi '+str.split(',')[0]+',<br/>'+'Following are the details for the Promoter Inquiries created:<br/><br/>'+'<html><body>'+
                            '<table style="border:1px solid black;border-collapse:collapse;width:50%;">'+
                            '<tr><th style="border:1px solid black;border-collapse:collapse;">Sr.No</th>'+
                            '<th style="border:1px solid black;border-collapse:collapse;">Promoter</th><th style="border:1px solid black;border-collapse:collapse; text-align:center;">Count</th></tr>';
           Integer serialNumber=1;
           String internalBody='';
           for(String inqIns:mapManagerUserCount.get(str).keyset()){
               
                   internalBody=internalBody+'<tr>'+'<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+serialNumber+'</td>'+'<td style="border:1px solid black;border-collapse:collapse;text-align:center">'+inqIns+'</td>'
                                            +'<td style="border:1px solid black;text-align:center;border-collapse:collapse;">'+mapManagerUserCount.get(str).get(inqIns)+'</td>'+'</tr>';
                   serialNumber++;
               
           }
           if(String.isnotBlank(internalBody)){
               mailBody=mailBody+internalBody+'</table></body></html>';
               System.debug('@@ str.split  @@@' + str.split(',')[1]);
               if(str.split(',')[1] != null){
               List<String> toAddress=new List<String>{str.split(',')[1]};
               //List<String> toAddress=new List<String>{'tejasnaik13@gmail.com'};
               if(toaddress != null && toaddress.size()>0 && !toaddress.isEmpty()){
                   System.debug('@@ toaddress @@' + toaddress);
                   Messaging.SingleEmailMessage emailContent=SendEmailUtility.createEmail(mailBody,toAddress,'Promoters With Leads Less than 5',owea);
                   mails.add(emailContent);
               }
               }
           }
       }
        if(mails != null){   
            Messaging.sendEmail(mails);
        }    
   }
}