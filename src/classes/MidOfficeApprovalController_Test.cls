/************************************************************************************************
 * @Name              : MidOfficeApprovalController_Test
 * @Description       : Test Class for MidOfficeApprovalController
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         QBurst         16/04/2020       Created
***********************************************************************************************/
@isTest
public class MidOfficeApprovalController_Test {

    @testSetup
    static void setupData() {
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('pcUser@pc.com');
        insert pcUser;

        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Agent Registration',false,null);
        sr.New_Country_of_Sale__c = 'UAE';
        insert sr;
        
        Unit_Documents__c srDoc = new Unit_Documents__c ();
        srDoc.Service_Request__c = sr.id;
        srDoc.Document_Type__c = 'SPA Docs';
        srDoc.Status__c = 'Pending Upload';
        srDoc.Is_Required__c = true;
        insert srDoc;
        
        List<New_Step__c> stpList = new List<New_Step__c> ();
        New_Step__c stp = new New_Step__c(Service_Request__c = sr.Id, Step_No__c  = 2.1, Step_Type__c = 'Mid Office Approval', 
                            Step_Status__c = 'Awaiting Mid Office Approval');
        stpList.add(stp);
        New_Step__c stp2 = new New_Step__c(Service_Request__c = sr.Id, Step_No__c  = 2.1, Step_Type__c = 'Mid Office Approval', 
                            Step_Status__c = 'Mid Office Approved');
        stpList.add(stp2);
        New_Step__c stp3 = new New_Step__c(Service_Request__c = sr.Id, Step_No__c  = 2.1, Step_Type__c = 'Mid Office Approval', 
                            Step_Status__c = 'Mid Office Rejected');
        stpList.add(stp3);
        New_Step__c stp4 = new New_Step__c(Service_Request__c = sr.Id, Step_No__c  = 2.2, Step_Type__c = 'Revised URRF Upload', 
                            Step_Status__c = 'Awaiting Revised URRF');
        stpList.add(stp4);
        New_Step__c stp5 = new New_Step__c(Service_Request__c = sr.Id, Step_No__c  = 2.2, Step_Type__c = 'Revised URRF Upload', 
                            Step_Status__c = 'Revised URRF Uploaded');
        stpList.add(stp5);
        insert stpList;

        List<string> statuses = new list<string>{'Mid Office Rejected','Mid Office Approved'};
        Map<string,NSIBPM__SR_Status__c> mpsrStatus =  InitializeSRDataTest.createSRStatus(statuses);

        List<Step_Status_Transition__c> transitions = new List<Step_Status_Transition__c>();
        Step_Status_Transition__c stepTransition = new Step_Status_Transition__c();
        stepTransition.From_Step_Status__c = 'Awaiting Mid Office Approval';
        stepTransition.To_Step_Status__c = 'Mid Office Approved';
        stepTransition.Step_Type__c = 'Mid Office Approval';
        stepTransition.SR_External_Status__c = 'Mid Office Approved';
        stepTransition.SR_Internal_Status__c = 'Mid Office Approved';
        stepTransition.Is_Closed__c = false;
        stepTransition.Step_No__c = 2.1;
        stepTransition.Service_Request_Type__c = 'Deal';
        stepTransition.UK_Deal__c = 'All';
        transitions.add(stepTransition);
        Step_Status_Transition__c stepTransition2 = new Step_Status_Transition__c();
        stepTransition2.From_Step_Status__c = 'Awaiting Mid Office Approval';
        stepTransition2.To_Step_Status__c = 'Mid Office Rejected';
        stepTransition2.Step_Type__c = 'Mid Office Approval';
        stepTransition2.SR_External_Status__c = 'Mid Office Rejected';
        stepTransition2.SR_Internal_Status__c = 'Mid Office Rejected';
        stepTransition2.Is_Closed__c = false;
        stepTransition2.Step_No__c = 2.1;
        stepTransition2.Service_Request_Type__c = 'Deal';
        stepTransition2.UK_Deal__c = 'All';
        transitions.add(stepTransition2);
        insert transitions;
    }

    @isTest
    static void test_method_1() {
        New_Step__c newStep = new New_Step__c();
        for (New_Step__c step : [SELECT Id, Change_Status__c, Comments__c, Is_Closed__c, Is_UK_Deal__c,
                                 Service_Request__c, Step_No__c, Step_Status__c, Step_Type__c, Service_Request__r.Is_VAT_SR__c,
                                 Service_Request__r.RecordType.DeveloperName, Service_Request__r.NSIBPM__Required_Docs_not_Uploaded__c, 
                                 Service_Request__r.Is_UK_Deal__c
                                 FROM New_Step__c]) {
            newStep = step;
        }
        Unit_Documents__c doc = [SELECT ID FROM Unit_Documents__c WHERE CreatedDate = TODAY LIMIT 1];
        
        Test.startTest();
        ApexPages.StandardController std = new ApexPages.StandardController(newStep);
        MidOfficeApprovalController ctlr = new MidOfficeApprovalController(std);
        ctlr.updateStatus();
        ctlr.cancelUpdate();
        
        apexpages.currentpage().getparameters().put('unitIds', doc.Id);
        ctlr.makeUnitsInvalid ();
        
        Test.stopTest();
    }
}