public without sharing class HandoverController{
    
    string caseId;
    public Case objCases                                 {get;set;}
    public Boolean currentExcessAmtValidation            {get;set;}
    public Boolean fundRefundValidation                  {get;set;}
    public Decimal currentExcessAmount                   {get;set;}
    public String crfAttachmentName { get; set; }
    public String crfAttachmentBody { get; set; }
    private List<SR_attachments__c> lstSRattachmentAll;
    
    public HandoverController(ApexPages.StandardController stdCon){
        caseId = stdCon.getId();
        objCases = new Case();
        currentExcessAmount = 0.00;
    }
    
    public void init() {
        objCases = [Select Id,Status,AccountId ,CaseNumber,CurrencyIsoCode,DP_SOA_Balance__c,Additional_Details__c, Excess_Amount__c,Pending_Amount__c, OwnerId from Case where Id =: caseId];
        if(objCases.Status != 'Closed') {
            if(objCases.DP_SOA_Balance__c != null ) {
                if( objCases.DP_SOA_Balance__c > 0 ){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'This unit does not have any excess funds to be refunded.');
                    ApexPages.addMessage(myMsg);
                    currentExcessAmtValidation = true;
                }else {
                    currentExcessAmount = objCases.DP_SOA_Balance__c * -1;
                }
            }else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'This unit does not have any excess funds to be refunded.');
                    ApexPages.addMessage(myMsg);
                    currentExcessAmtValidation = true;            
            }
        }else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'You can not transfer Excess amount to service charge as this SR is Closed.');
            ApexPages.addMessage(myMsg);
            currentExcessAmtValidation = true;        
        }
    }
    
    public pageReference createExcessAmountTask(){
        pageReference pgRef = new PageReference('/'+caseId);
        //list<Case> lstCase = [Select Id, CurrencyIsoCode, Pending_Amount__c, OwnerId from Case where Id =: caseId];
        if(objCases != null ) { //&& !lstCase.isEmpty()){
            

        
            if(objCases.DP_SOA_Balance__c != null && objCases.DP_SOA_Balance__c < 0){
                if( objCases.Excess_Amount__c != null && objCases.Excess_Amount__c <= currentExcessAmount ) {
                    //update objCases;
                    
                   try {
                        UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();            
                        List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest = formDocumentUploadRequest();
                        
                        if(lstMultipleDocRequest.Size()>0) {
                            MultipleDocData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocRequest);
                            System.debug('--MultipleDocData--'+MultipleDocData);
                            if(MultipleDocData != null && MultipleDocData.status == 'Exception') {
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,MultipleDocData.message));                                         
                                errorLogger(MultipleDocData.message, objCases.Id, '');
                                return null;
                            }                                   
                            if(MultipleDocData != null && (MultipleDocData.Data == null || MultipleDocData.Data.Size() == 0)) {
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,MultipleDocData.message)); 
                                errorLogger(MultipleDocData.message, objCases.Id, '');                                        
                                return null; 
                            }
                        }
                        
                        lstSRattachmentAll = new List<SR_Attachments__c>();
                        if( String.isNotBlank( crfAttachmentBody ) && String.isNotBlank( crfAttachmentName ) ) {
                            formAttachment( extractName( crfAttachmentName ), objCases.Id , 'CRF Document'  );
                        }
                        crfAttachmentBody = '';
                        crfAttachmentName = '';
                        
                        list<SR_attachments__c> lstSRattachmentValid = new list<SR_attachments__c>();
                        if(lstSRattachmentAll.Size()>0 ){ //&& lstAttachment.Size()>0
                            if(lstMultipleDocRequest.Size()>0 && MultipleDocData != null){
                                for(integer objInt=0; objInt<lstSRattachmentAll.Size(); objInt++ ) {
                                    UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[objInt];
                                    if(objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)) {
                                       lstSRattachmentAll[objInt].Attachment_URL__c = objData.url;
                                       lstSRattachmentValid.add(lstSRattachmentAll[objInt]);
                                    }
                                    else {
                                       errorLogger('An exception has occurred while processing the request, document-'+objData.PARAM_ID+', PROC_STATUS-'+objData.PROC_STATUS+' URL-'+objData.url,objCases.Id,'');
                                       ApexPages.addmessage(new ApexPages.message(
                                       ApexPages.severity.Error,'An exception has occurred while processing the request, document-'+objData.PARAM_ID+', PROC_STATUS-'+objData.PROC_STATUS+' URL-'+objData.url));     
                                    }
                                }                                   
                                if(lstSRattachmentValid.Size() > 0) {
                                    insert lstSRattachmentValid;
                                }
                            }
                            else if(lstMultipleDocRequest.Size() > 0 && MultipleDocData == null){
                               ApexPages.addmessage(new ApexPages.message(
                               ApexPages.severity.Error,'An exception has occurred while processing the request of document upload, got null value, please contact the support team.'));                                        
                               return null;       
                            }
                        }
                        
                        upsert objCases;
                        
                    } catch(Exception excGen) {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getMessage()));
                        errorLogger(excGen.getMessage(),objCases.Id,'');
                        //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getStackTraceString()));
                    }   
                    
                    
                    Task objTask = new Task();
                    objTask.Assigned_User__c = 'Finance';
                    objTask.CurrencyIsoCode = objCases.CurrencyIsoCode;
                    objTask.OwnerId = string.valueOf(objCases.OwnerId).startsWith('00G') ? Label.DefaultCaseOwnerId : objCases.OwnerId;
                    objTask.Priority = 'High';
                    objTask.Process_Name__c = 'Handover';
                    objTask.Status = 'Not Started';
                    objTask.WhatId = objCases.Id;
                    objTask.Subject = Label.TransferExcessAmountSubject;
                    objTask.ActivityDate = date.today();
                    insert objTask;
                    return pgRef;
                    
                    
                } else {
                    fundRefundValidation = true;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Excess funds to be refunded can not be null or greater then current excess amount.');
                    ApexPages.addMessage(myMsg);
                    return null;
                }
            }else{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'This unit does not have any excess funds to be refunded.');
                ApexPages.addMessage(myMsg);
                return null;
            }
        }
        return null;
    }
    // Method to form request for multiple document upload
    private List<UploadMultipleDocController.MultipleDocRequest> formDocumentUploadRequest() {
        system.debug( ' crfAttachmentBody  : ' + crfAttachmentBody );
                system.debug( ' crfAttachmentName : ' + crfAttachmentName );
        List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest = new  List<UploadMultipleDocController.MultipleDocRequest>();
        Integer intIncrementor = 1;
        if( String.isNotBlank( crfAttachmentBody ) && String.isNotBlank( crfAttachmentName ) ) {
            
            UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
            objDocRequest.category =  'Document';
            objDocRequest.entityName = 'Damac Service Requests'; 
            objDocRequest.fileDescription  =  'CRF Document';                        
            
            intIncrementor++;
            objDocRequest.fileId = objCases.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(crfAttachmentName).substringAfterLast('.');
            objDocRequest.fileName = objCases.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(crfAttachmentName).substringAfterLast('.');
            
            if(objCases != null){
                objDocRequest.registrationId =  objCases.CaseNumber;
            }
            objDocRequest.sourceFileName  = 'IPMS-'+objCases.CaseNumber+'-'+extractName( crfAttachmentName );
            objDocRequest.sourceId  =  'IPMS-'+objCases.CaseNumber+'-'+extractName( crfAttachmentName );                        
            if(String.isNotBlank(crfAttachmentBody)){
                blob objBlob = extractBody(crfAttachmentBody);
                if(objBlob != null){
                    objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
                }
            }
            lstMultipleDocRequest.add(objDocRequest);                          
        }
        
        return lstMultipleDocRequest;
    }    
    // Method to form attachment object
    private void formAttachment( String strfileName, Id parentId, String strDocType ) {
        
        SR_Attachments__c objAttach = new SR_Attachments__c ();
        objAttach.name = strfileName;
        objAttach.Case__c = objCases.Id;         
        objAttach.type__c = strDocType ;
        if(objCases.AccountId != null ) {
            objAttach.Account__c = objCases.AccountId ;
        }
              
        lstSRattachmentAll.add(objAttach);
    }
    private String extractName( String strName ) {
        return strName.substring( strName.lastIndexOf('\\')+1 ) ;
    }
    private Blob extractBody( String strBody ) {
        //System.debug('--strBody ---'+strBody );
        strBody = EncodingUtil.base64Decode( strBody ).toString();
        return EncodingUtil.base64Decode( strBody.substring( strBody.lastIndexOf(',')+1 ) );
    }
    
    // Method for logging error
    private void errorLogger(string strErrorMessage, string strCaseID, string strBookingUnitID) {
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        if(String.isNotBlank(strCaseID) && strCaseID.startsWith('500')){
            objError.Case__c = strCaseID;
        }
        if(String.isNotBlank(strBookingUnitID)){
            objError.Booking_unit__c = strBookingUnitID;
        }
        insert objError;
    }    
}