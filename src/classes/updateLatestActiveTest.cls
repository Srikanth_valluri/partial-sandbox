/************************************************************************************************
 * @Name              : updateLatestActiveTest
 * @Description       : Test Class for updateLatestActive Trigger
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         QBurst         04/05/2020       Created
***********************************************************************************************/

@isTest
public class updateLatestActiveTest{
@isTest
    static void test_method_1() {
        Account acc = new Account(name = 'QBurst');
        insert acc;
        Partnership_Program__c  partner = new Partnership_Program__c (
        
        Active__c = true,
        Account__c = acc.Id,
        Damac_Unity__c = 'Executive Club',
        Start_date__c = Date.newInstance(2020, 05, 09),
        End_Date__c = Date.newInstance(2020, 05, 29));
        insert partner; 
        
    }


}