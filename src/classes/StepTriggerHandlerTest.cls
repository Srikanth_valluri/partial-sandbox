@isTest
public class StepTriggerHandlerTest {
    public static List<NSIBPM__Service_Request__c> createServiceRequestList = new List<NSIBPM__Service_Request__c>();
    private static List<NSIBPM__SR_Template__c> createSrTemplateList = new List<NSIBPM__SR_Template__c>();  
    private static List<NSIBPM__SR_Status__c> createSrStatusList = new List<NSIBPM__SR_Status__c>(); 
    private static List<NSIBPM__Step__c> createStepList = new List<NSIBPM__Step__c>();
    private static List<NSIBPM__Status__c> createStatus = new List<NSIBPM__Status__c>();
    
    static testMethod void myUnitTest1(){
        createSrTemplateList = InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
        createSrStatusList = InitialiseTestData.createSrStatusRecords(new List<NSIBPM__SR_Status__c>{new NSIBPM__SR_Status__c(Name = 'Rejected', NSIBPM__Code__c = 'REJECTED', NSIBPM__Type__c = 'Rejected', NSIBPM__DEV_Id__c='REJECTED'),
            new NSIBPM__SR_Status__c(Name = 'Submitted', NSIBPM__Code__c = 'SUBMITTED', NSIBPM__Type__c = '', NSIBPM__DEV_Id__c='SUBMITTED')});
        
        createServiceRequestList = 
            InitialiseTestData.createtestServiceRequestRecords(
                new List<NSIBPM__Service_Request__c>{
                    new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration'),
                                                   NSIBPM__SR_Template__c = createSrTemplateList[0].Id)});
        
        createStatus = InitialiseTestData.createStatusRecords(
            new List<NSIBPM__Status__c>{
                new NSIBPM__Status__c(NSIBPM__Code__c = 'UNDER_REVIEW', Name = 'Under Review'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'REJECTED', Name = 'Rejected')});
        
        createStatus[0].NSIBPM__Rejection__c = false;
        createStatus[0].NSIBPM__Type__c = 'Start';
        createStatus[1].NSIBPM__Rejection__c = true;
        createStatus[1].NSIBPM__Type__c = 'End';
        update createStatus;
        
        NSIBPM__SR_Steps__c srStep1 = new NSIBPM__SR_Steps__c(Approver_Role__c='DOS', NSIBPM__Step_No__c = 1.0, NSIBPM__SR_Template__c = createSrTemplateList[0].Id, NSIBPM__Start_Status__c = createStatus[0].Id,
                                                              NSIBPM__Execute_on_Submit__c = true, NSIBPM__Step_RecordType_API_Name__c = 'General' );
        insert srStep1;
        
        createStepList = InitialiseTestData.createTestStepRecords(
            new List<NSIBPM__Step__c>{
                new NSIBPM__Step__c(NSIBPM__SR__c = createServiceRequestList[0].id, NSIBPM__Status__c = createStatus[0].Id, NSIBPM__SR_Step__c = srStep1.id)});
    }
}