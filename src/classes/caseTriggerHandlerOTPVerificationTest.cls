/*******************************************************************************************
* Description - Test class developed for caseTriggerHandlerOTPVerification
*
* Version            Date            Author                      Description
* 1.0                22/11/17        Naresh Kaneriya (Accely)    Initial Draft
*******************************************************************************************/
@isTest(SeeAllData=false)
private class caseTriggerHandlerOTPVerificationTest
{

    // Test Method: afterUpdate
    public static testmethod void Test_afterUpdate(){
        test.StartTest();
        
         map<Id, Case> newmap = new map<Id,Case>();
         map<Id, Case> oldmap = new map<Id,Case>();
        
         Id RecordTypeIdCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
         Account AccData=TestDataFactory_CRM.createPersonAccount();
         Insert AccData;
         System.assert(AccData!=null);
         
         NSIBPM__Service_Request__c  sr  = TestDataFactory_CRM.createServiceRequest();
         insert sr;
         System.assert(sr!=null);
         
         List<Booking__c>  booking = TestDataFactory_CRM.createBookingForAccount(AccData.Id ,sr.Id,1);
         insert booking;
         System.assert(booking!=null);
         
         list<Buyer__c> buyer = TestDataFactory_CRM.createBuyer(booking[0].Id ,1, AccData.Id);
         buyer[0].Phone_Country_Code__c = 'India: 0091';
         buyer[0].Phone__c = '9865215789';
         insert buyer;
         System.assert(buyer!=null);
         
         Case CaseData=TestDataFactory_CRM.createCase(AccData.Id,RecordTypeIdCase);
         CaseData.Buyer__c = buyer[0].Id;
         CaseData.Buyer_POA_Country_Code__c = 'India: 0091';
         CaseData.Seller_POA_Country_Code__c = 'India: 0091';
         CaseData.Seller_POA_Phone__c = '9865215789';
         CaseData.Buyer_POA_Phone__c = '9865215789';
         CaseData.Status = 'New';
         Insert CaseData;
         System.assert(CaseData!=null);
         System.assertEquals('9865215789', CaseData.Buyer_POA_Phone__c);
         
         // Store Old Values
         oldmap.put(CaseData.id,CaseData);
         
         CaseData.Status = 'Buyer OTP Verification';
         Update CaseData;
         System.assertEquals(CaseData.Status, 'Buyer OTP Verification');
         
         // New Values
         newmap.put(CaseData.Id,CaseData);
        
        caseTriggerHandlerOTPVerification.afterUpdate(newmap, oldmap);
        test.StopTest();
    }
}