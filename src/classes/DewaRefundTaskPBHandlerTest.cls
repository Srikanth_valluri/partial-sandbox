@isTest
public class DewaRefundTaskPBHandlerTest {
	@isTest
    public static void testMethod1(){
		Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        Case objCase = new Case();
        objCase.Account = acc;
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Type = 'Offer & Acceptance Letter';  
        objCase.Account_Email__c = 'test@gmail.com';
		objCase.Refund_Amount__c = 14000;
        insert objCase;
		
		Task objTask = new Task();
		objTask.Subject = 'Validate Requests and Calculations';
		objTask.WhatId = objCase.Id;
		objTask.OwnerId = userinfo.getUserId();
		objTask.Status = 'Completed';
        objTask.Priority = 'Normal';
		insert objTask;
		List<Task> lstTask = new List<Task>();
		lstTask.add(objTask);
		DewaRefundTaskPBHandler.updateApprovaingAuthorities(lstTask);
	}
}