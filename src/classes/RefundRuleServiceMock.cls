@isTest
/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  21/11/2017  Initial Draft
*/
global class RefundRuleServiceMock implements WebServiceMock {
    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
               
       //docSample.EchoStringResponse_element respElement = 
       //    new docSample.EchoStringResponse_element();
       //respElement.EchoStringResult = 'Mock response';
       //response.put('response_x', respElement);        
       RefundsRule.TokenRefundsTransfersResponse_element respElement = new RefundsRule.TokenRefundsTransfersResponse_element();
       respElement.return_x = 'Manager';
       response.put('response_x', respElement);
       
   }
}