@isTest
public class AnnouncementAttachmentExtensionTest {
    
    @isTest
    public static void testAnnouncementAttachment(){
        test.startTest();
        Announcement__c announcement = new Announcement__c();
        announcement.Title__c = 'Title';
        announcement.Start_Date__c = system.today();
        announcement.End_Date__c = system.today();
        announcement.Description__c = 'Description';
        INSERT announcement;
        
        Announcement_Section__c section = new Announcement_Section__c();
        section.Announcement__c = announcement.Id;
        section.Type__c = 'Image';
        INSERT section;
        
        AnnouncementAttachementExtension extension = new AnnouncementAttachementExtension(new ApexPages.StandardController(section));
        extension.getnewAttachment();
        extension.AttachFile();
        test.stopTest();
    }
    
}