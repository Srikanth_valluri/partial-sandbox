/*--------------------------------------------------------------------------------------
* Name               : AgentPortalNewCIL
* Description        : Controller for AgentPortalNewCIL
* Created Date       : Pratiksha Narvekar                                                                       
* Created By         : 20-08-2017                                                               
* Last Modified Date :                                                                            
* Last Modified By   :                                                                             
* ------------------------------------------------------------------------------------ 
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         Pratiksha Narvekar        20-08-2017 
----------------------------------------------------------------------------------------*/

public class AgentPortalNewCILController {
    public Inquiry__c insertInquery{get;set;}
    public String UserName{get;set;}
    public AgentPortalNewCILController() {
        insertInquery = new Inquiry__c();
        UserName = userInfo.getName(); 
        
        /*User objUser =[Select Name
                        From User 
                        WHERE ID =: currentUser
                        Limit 1];
        UserName= objUser.Name; */
    }
    public PageReference Save(){
        //database.insert(insertInquery);
        ID uId = UserInfo.getUserID(); 
		List<Inquiry__c> listInquiry =new List<Inquiry__c>(); 
		List<User> listUser = new List<User>();
		listUser = [select ContactID from User where Id =:uId];
        insertInquery.Agent_Name__c = listUser[0].ContactID;
        Contact loginContact = AgentPortalUtilityQueryManager.getContactInformation();
        insertInquery.Agency_Name__c = loginContact.AccountId;
        insertInquery.Inquiry_Source__c = 'Agent Referral';
		listInquiry.add(insertInquery);       
        Database.SaveResult[] srList = Database.insert(listInquiry, false);
        System.debug('...srList....'+srList);
        PageReference pageRef;
        //PageReference pageRef;
		for (Database.SaveResult sr : srList) {
		    if (sr.isSuccess()) {
		        System.debug('Successfully inserted cil. cil ID: ' + sr.getId());
		        pageRef = new PageReference('/AgentPortalLeads');
		        pageRef.setRedirect(true);
		        
		    }
		    else {
		        for(Database.Error err : sr.getErrors()) {
		            System.debug('The following error has occurred.');                    
		            System.debug(err.getStatusCode() + ': ' + err.getMessage());
		            System.debug('cil fields that affected this error: ' + err.getFields());
		            //pageRef = new PageReference('/AgentPortal#NewCILID');
		        }
		    }
		}

        return pageRef;
    }
}