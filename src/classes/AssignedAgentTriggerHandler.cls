/**************************************************************************************************
* Name               : AssignedAgentTriggerHandler                                                *
* Test Class         : AssignedAgentTriggerHandlerTest                                            *
* Description        : This is a handler class to manage Assigned Agent object.                   *
*                      - Used to share records with PC and PC's Manager.                          *
* Created Date       : 17/01/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE        COMMENTS                                              *
* 1.0         NSI - Vineet      17/01/2017  Initial draft.                                        *
* 2.0         NSI - Sivasankar  13/02/2017  Added logic to create record on inventory user,       *
*                                           in case an agent is added to the campaign.            *
**************************************************************************************************/
public class AssignedAgentTriggerHandler extends UtilitySharingManager implements TriggerFactoryInterface{ 
    
    /********************************************************************************************* 
    * @Description : Method to contain logic to be executed after insert.                        *
    * @Params      : Map<Id, sObject>                                                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterInsertTrigger(Map<Id, sObject> newRecordsMap){ 
        Map<Id, List<Assigned_Agent__c>> userAssignedUserMap = new Map<Id, List<Assigned_Agent__c>>();  
        Map<Id, List<Assigned_Agent__c>> campaignUserAssignedMap = new Map<Id, List<Assigned_Agent__c>>();
        try{            
            /* Create a map of user and list of assigned agent records. */
            for(Assigned_Agent__c thisAssignedAgent : (List<Assigned_Agent__c>) newRecordsMap.values()){ 
                if(thisAssignedAgent.User__c != null){
                    if(userAssignedUserMap.containsKey(thisAssignedAgent.User__c)){
                        userAssignedUserMap.get(thisAssignedAgent.User__c).add(thisAssignedAgent);      
                    }else{
                        userAssignedUserMap.put(thisAssignedAgent.User__c, new List<Assigned_Agent__c>{thisAssignedAgent});     
                    }
                }
            }   
            /* Create a map of campaign and list of assigned agent records. */
            for(Assigned_Agent__c thisAssignedAgent : (List<Assigned_Agent__c>) newRecordsMap.values()){ 
                if(thisAssignedAgent.Campaign__c != null){
                    if(campaignUserAssignedMap.containsKey(thisAssignedAgent.Campaign__c)){
                        campaignUserAssignedMap.get(thisAssignedAgent.Campaign__c).add(thisAssignedAgent);      
                    }else{
                        campaignUserAssignedMap.put(thisAssignedAgent.Campaign__c, new List<Assigned_Agent__c>{thisAssignedAgent});     
                    }
                }
            }  
            if(!userAssignedUserMap.isEmpty()){
                /* Calling method to update inquiry status when a activity is created. */
                provideAccess(userAssignedUserMap); 
            }
            if(!campaignUserAssignedMap.isEmpty()){
                /* Calling method to create inventory user for agents. */
                createInventoryUsersforAgents(campaignUserAssignedMap);
            }
        //******************************Update the Count of Assigned Agents to Campaign**************************************
        countAssignedAgents(newRecordsMap);   
        }catch(exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
    }    
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after delete.                        *
    * @Params      : Map<Id, sObject>                                                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterDeleteTrigger(Map<Id, sObject> oldRecordsMap){ 
        try{
            /*Calling method to update inquiry status when a activity is created. */
            revokeAccess((Map<Id, Assigned_Agent__c>) oldRecordsMap);
      
         //******************************Update the Count of Assigned Agents to Campaign On Deletion**************************************
        countAssignedAgents(oldRecordsMap);
        
        }catch(exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
    }  
    
    /*********************************************************************************************
    * @Description : Method to provide access to the PC and PC's Manager.                        *
    * @Params      : Map<Id, List<Assigned_Agent__c>>                                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/ 
    @TestVisible  private void provideAccess(Map<Id, List<Assigned_Agent__c>> userAssignedUserMap){
        List<SObject> shareRecordsList = new List<SObject>();
        if(!userAssignedUserMap.isEmpty()){
            /* Calling query utility class for getting the manager details. */
            Map<Id, User> userManagerMap = UtilityQueryManager.getUserManagerDetails(userAssignedUserMap.keySet());
            for(Id thisKey : userAssignedUserMap.keySet()){
                for(Assigned_Agent__c thisAssignedAgent : userAssignedUserMap.get(thisKey)){
                    User userAgentRecord = userManagerMap.get(thisAssignedAgent.User__c);
                    if(userAgentRecord.Id != null){ 
                        /* Calling sharing utility class to grant access to user hierarchy. */
                        shareRecordsList.addAll(invokeAccess(userAgentRecord, thisAssignedAgent.Campaign__c.getSobjectType().getDescribe().getName(), thisAssignedAgent.Campaign__c, ''));
                    }
                }   
            }
            
            if(!shareRecordsList.isEmpty()){
                /* Calling queable method to insert sharing records in the sharing table. */
                system.enqueueJob(new UtilitySharingManager(shareRecordsList, new List<sObject>()));
            }   
        }
    }    
    
    /*********************************************************************************************
    * @Description : Method to revoke access after the PC is removed from the campaign.          *
    * @Params      : Map<Id, Assigned_Agent__c>                                                  *
    * @Return      : void                                                                        *
    *********************************************************************************************/ 
    @TestVisible private void revokeAccess(Map<Id, Assigned_Agent__c> deletedRecordsMap){
        Set<Id> campaignIdsSet = new Set<Id>();
        Set<Id> AssignedAgentIdsSet = new Set<Id>();
        List<Campaign__Share> deleteSharingRecordList = new List<Campaign__Share>();
        List<Inventory_User__c> deleteInventoryUsers;//Version: 2.0
        for(Assigned_Agent__c thisDeletedPc : deletedRecordsMap.values()){
            campaignIdsSet.add(thisDeletedPc.Campaign__c);  
            AssignedAgentIdsSet.add(thisDeletedPc.User__c);
        }

        if(!campaignIdsSet.isEmpty()){
            /* Calling method to get records from campaign sharing table. */
            Map<Id, List<Campaign__Share>> campaignShareRecordsMap = getSharingRecords(campaignIdsSet); 
            /* Calling query utility class for getting the manager details. */
            Map<Id, User> userManagerMap = UtilityQueryManager.getUserManagerDetails(AssignedAgentIdsSet);  
            if(!campaignShareRecordsMap.isEmpty() && !userManagerMap.isEmpty()){
                for(Assigned_Agent__c thisDeletedPc : deletedRecordsMap.values()){
                    if(campaignShareRecordsMap.containsKey(thisDeletedPc.Campaign__c)){
                        User userAgentRecord = userManagerMap.get(thisDeletedPc.User__c);
                        if(userAgentRecord != null){
                            for(Campaign__Share thisShareRecord : campaignShareRecordsMap.get(thisDeletedPc.Campaign__c)){
                                if((userAgentRecord.Id == thisShareRecord.UserOrGroupId) ||
                                   (userAgentRecord.ManagerId != null && userAgentRecord.ManagerId == thisShareRecord.UserOrGroupId) ||
                                   (userAgentRecord.Manager.ManagerId != null && userAgentRecord.Manager.ManagerId == thisShareRecord.UserOrGroupId) ||
                                   (userAgentRecord.Manager.Manager.ManagerId != null && userAgentRecord.Manager.Manager.ManagerId == thisShareRecord.UserOrGroupId) ||
                                   (userAgentRecord.Manager.Manager.Manager.ManagerId != null && userAgentRecord.Manager.Manager.Manager.ManagerId == thisShareRecord.UserOrGroupId)){
                                    deleteSharingRecordList.add(thisShareRecord);   
                                }
                            }
                        }
                    }
                }
                if(!deleteSharingRecordList.isEmpty()){
                    database.deleteResult[] result = database.delete(deleteSharingRecordList, false);
                }
            }
        }

        if(!campaignIdsSet.isEmpty() && !AssignedAgentIdsSet.isEmpty()){
            deleteInventoryUsers = new List<Inventory_User__c>([SELECT Id FROM Inventory_User__c WHERE User__c IN: AssignedAgentIdsSet AND Campaign_ID__c IN: campaignIdsSet]);
            if(!deleteInventoryUsers.isEmpty()){
                database.deleteResult[] result = database.delete(deleteInventoryUsers, false);
            }
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to get sharing records from the campaign sharing table.              *
    * @Params      : Set<Id>                                                                     *
    * @Return      : Map<Id, List<Campaign__Share>>                                              *
    *********************************************************************************************/  
    @TestVisible private Map<Id, List<Campaign__Share>> getSharingRecords(Set<Id> campaignIds){
        Map<Id, List<Campaign__Share>> campaignShareRecordsMap = new Map<Id, List<Campaign__Share>>();
        for(Campaign__Share thisShareRecord : [SELECT Id, ParentId, AccessLevel, UserOrGroupId, RowCause 
                                               FROM Campaign__Share 
                                               WHERE ParentId IN: campaignIds]){
            if(campaignShareRecordsMap.containsKey(thisShareRecord.ParentId)){
                campaignShareRecordsMap.get(thisShareRecord.ParentId).add(thisShareRecord);     
            }else{
                campaignShareRecordsMap.put(thisShareRecord.ParentId, new List<Campaign__Share>{thisShareRecord});
            }   
        }   
        return campaignShareRecordsMap; 
    }
    /*********************************************************************************************
    * @Description : Method to create the Inventory User reocrds for assigned agents.            *
    * @Params      : void                                                                        *
    * @Return      : Map<Id, List<Assigned_Agent__c>>                                            *
    *********************************************************************************************/  
    @TestVisible private void createInventoryUsersforAgents(Map<Id, List<Assigned_Agent__c>> campaignUserAssignedMap){
        List<Inventory_User__c> upsertInventory = new List<Inventory_User__c>();
        Set<Id> campaignIdsSet = new Set<Id>();
        for(Id thisKey : campaignUserAssignedMap.keySet()){
            for(Assigned_Agent__c thisAssignedAgent : campaignUserAssignedMap.get(thisKey)){
                if(thisAssignedAgent.Campaign__c != null){
                    campaignIdsSet.add(thisAssignedAgent.Campaign__c);  
                }   
            }   
        }
        system.debug('#### campaign User Assigned Map = '+campaignUserAssignedMap);
        if(!campaignIdsSet.isEmpty()){
            for(Campaign_Inventory__c campInve : [SELECT Campaign__c, Inventory__c 
                                                  FROM Campaign_Inventory__c 
                                                  WHERE Campaign__c IN: campaignIdsSet]){
                if(campInve.Campaign__c != null && campaignUserAssignedMap.containsKey(campInve.Campaign__c)){
                    for(Assigned_Agent__c assAgent: campaignUserAssignedMap.get(campInve.Campaign__c)){
                        upsertInventory.add(new Inventory_User__c(Inventory__c = campInve.Inventory__c,
                                                                  User__c = assAgent.User__c,
                                                                  Campaign__c = campInve.Campaign__c,
                                                                  Campaign_ID__c = campInve.Campaign__c,
                                                                  Unique_Key__c = assAgent.User__c+'###'+campInve.Inventory__c+'###'+campInve.Campaign__c,
                                                                  Start_Date__c = assAgent.Start_Date__c,
                                                                  End_Date__c = assAgent.End_Date__c));
                    }   
                }
            }
            if(!upsertInventory.isEmpty()){
                Database.UpsertResult[] upsertResults = Database.upsert(upsertInventory, Inventory_User__c.unique_key__c.getDescribe().getSObjectField(), false);
            }   
        }
    }
    
 // Update Assigned Agent Count on Campaign    
    public void countAssignedAgents(Map<Id, sObject> sRecordsMap){
        set<string> campaignIds = new set<string>();
        for(Assigned_Agent__c thisAssignedAgent : (List<Assigned_Agent__c>) sRecordsMap.values()){
            // add parents
            campaignIds.add(thisAssignedAgent.campaign__c);     
        }
        
        list<aggregateresult> groupedResults = [select count(id) agCount, campaign__c camp from Assigned_Agent__c where campaign__c in:campaignIds Group by campaign__c];
        
        list<campaign__c> campsToUpdate = new list<campaign__c>();
            for(AggregateResult ar:groupedResults) {
                system.debug( (INTEGER)ar.get('agCount'));
                Id campid = (ID)ar.get('camp');
                Integer count = (INTEGER)ar.get('agCount');
                campaign__c tempCamp = new campaign__c(Id=campid);
                tempCamp.No_of_assigned_agents__c = count;
                campsToUpdate.add(tempCamp);
            }
            update campsToUpdate;
    
    }
    
    // TOBE Implemented
    public void executeBeforeInsertTrigger(List<sObject> newRecordsList){}
    public void executeBeforeUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){}
    public void executeBeforeInsertUpdateTrigger(List<sObject> newRecordsList, map<Id,sObject> oldRecordsMap){}
    public void executeAfterUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){}
    public void executeBeforeDeleteTrigger(Map<Id,sObject> oldRecordsMap){}
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){}
}// End of class