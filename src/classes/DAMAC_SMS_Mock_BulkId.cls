@isTest
global class DAMAC_SMS_Mock_BulkId implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        
        res.setBody('{"Status":"OK","Data":{"BulkId":3037903,"BulkStatus":"Finished","DateAdded":"2017-11-06T17:49:55.57","DateToStart":"2017-11-06T17:50:00","IndividualResponses":[{"countryId":203,"creditsUsed":"0.055000","dlrStatus":"Delivered","messageLength":1,"mobileNo":"971561985082","msgId":528111565}],"Message":"Test","ReferenceName":"--","SenderName":"DAMAC","TotalNumbers":1,"TotalProcessed":1,"TotalValid":1}}');
        
        res.setStatusCode(200);
        return res;
    }
}