//Test class for crmCollectionLeadTrigger, crmCollectionLeadTrigger
@isTest
private class crmCollectionLeadTriggerHandlerTest {
    static testMethod void testMethod1() {
        Account acc = createAccount();
        Collection__c  coll = createCollection(acc.Id);  
        Collection__c   colll = [Select Mobile_Phone__c from Collection__c   where Id =: coll.Id];
        System.assertEquals('9876543210', colll.Mobile_Phone__c);
    }
    
    private static Account createAccount() {
        Id accRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Agency_Type__c = 'Corporate';
        acc.Agency_Email__c = 'testmail@tmail.com';
        acc.RecordTypeId = accRecTypeId;
        acc.Trade_License_Expiry_Date__c = Date.today().addDays(20);
        acc.RERA_Expiry_Date__c = Date.today().addDays(20);
        acc.Mobile__c = '00919876543210';
        insert acc;
        return acc;
    }
    
    private static Collection__c  createCollection(Id accountId) {
        Collection__c coll = new Collection__c();
        coll.Account__c = accountId;
        insert coll;
        return coll;
    }
}