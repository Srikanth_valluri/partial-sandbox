/**************************************************************************************************
 * @Name              : DAMAC_IPMS_PARTY_CREATION_JSON
 * @Test Class Name   : DAMAC_IPMS_PARTY_CREATION_TEST
 * @Description       : JSON Class for DAMAC_IPMS_PARTY_CREATION
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0                        14/08/2019       Created
 * 1.1         QBurst         03/02/2020       Modifications for DP Deals  [SOAP to REST]
**************************************************************************************************/
public class DAMAC_IPMS_PARTY_CREATION_JSON{
    public String extRequestNumber; //123456
    public cls_customerLine customerLine;
    public class cls_customerLine {
        public String subRequestName;
        public String propertyLocation; //DP
        public String extCustomerNumber;    //CUST-420
        public String partyType;    //PERSON
        public String partyTitle;   //MR.
        public String firstName;    //PNVS
        public String middleName;   //Murthy
        public String lastName; //Last Name
        public String nationality;  //Indian
        public String gender;   //Male
        public String birthDate;    //01-JAN-2019
        public String passportNumber;   //S99009909
        public String passportIssueDate;    //01-JAN-2009
        public String passportExpiryDate;   //01-JAN-2020
        public String passportIssuePlace;   //Hyderabad
        public String organizationName; //The Boring Company
        public String crNumber; //CR-123456789
        public String crRegPlace;   //Dubai
        public String crRegDate;    //15-JAN-2018
        public String address1; //Address-1
        public String address2; //Address-2
        public String address3; //Address-3
        public String address4; //Phoolan Circle
        public String city; //Hyderabad
        public String postalCode;   //560076
        public String state;    //Telengana State
        public String countryCode;  //IN
        public string partyId; // 090909 1.1
        public cls_contactLines[] contactLines;
        public cls_translationLines[] translationLines;
    }

    public class cls_contactLines {
        public String primaryFlag;  //Y
        public String contactType;  //PHONE_MOBILE
        public String countryCode;  //91
        public String areaCode; //80
        public String phoneNumber;  //556656
        
        public String primary;  //556656
        public String email;  //556656
    }

    public class cls_translationLines {
        public String languageCode; //AR
        public String title;    //arabic title
        public String firstName;    //ar first name
        public String middleName;   //ar middle name
        public String lastName; //ar last name
        public String nationality;  //indian ar
        public String address1; //add1 arabic
        public String address2; //add2 arabic
        public String address3; //add3 arabic
        public String address4; //add4 arabic
        public String postalCode;   //postal Code
        public String city; //ara city
        public String country;  //Arabic country
    }

    public static DAMAC_IPMS_PARTY_CREATION_JSON parse(String json){
        return (DAMAC_IPMS_PARTY_CREATION_JSON) System.JSON.deserialize(json, DAMAC_IPMS_PARTY_CREATION_JSON.class);
    }
}