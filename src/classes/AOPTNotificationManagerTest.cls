/*
 * Description - Test class for AOPTNotificationManager class
 *
 * Version        Date            Author            Description
 * 1.0            12/04/18        Vivek Shinde      Initial Draft
 */
@isTest
private class AOPTNotificationManagerTest {
    
    static final Id idAOPTRT = Schema.SobjectType.Case.RecordTypeInfosByName.get('AOPT').RecordTypeId;

    static testMethod void AOPTAddnmGenerationNotificationTest() {
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        Case objCase = new Case();
        objCase.RecordTypeId = idAOPTRT;
        objCase.AccountId = objAccount.Id;
        objCase.Account_Email__c = 'test@test.com';
        objCase.Status = 'Submitted';
        objCase.Offer_Acceptance_Letter_Generated__c = true;
        insert objCase;
        AOPTNotificationManager.createEmailMessages(new List<Case>{objCase});
    }
    
    static testMethod void AOPT21stDayNotificationTest() {
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        Case objCase = new Case();
        objCase.RecordTypeId = idAOPTRT;
        objCase.AccountId = objAccount.Id;
        objCase.Account_Email__c = 'test@test.com';
        objCase.Status = 'Submitted';
        objCase.Is_21st_Day__c = true;
        insert objCase;
        AOPTNotificationManager.createEmailMessages(new List<Case>{objCase});
    }
    
    static testMethod void AOPT30stDayNotificationTest() {
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        Case objCase = new Case();
        objCase.RecordTypeId = idAOPTRT;
        objCase.AccountId = objAccount.Id;
        objCase.Account_Email__c = 'test@test.com';
        objCase.Status = 'Submitted';
        objCase.Is_30th_Day__c = true;
        insert objCase;
        AOPTNotificationManager.createEmailMessages(new List<Case>{objCase});
    }
}