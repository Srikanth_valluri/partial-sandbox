public without sharing class EHOCancellationController {
    
    String caseId;
    boolean isPaymentPlanReverse;
    public EHOCancellationController(ApexPages.StandardController sc){
        caseId = sc.getId();
    }
    
    public pagereference cancelEHO() {
        system.debug('cancel case called');
    Savepoint sp;
    try
    {
      if(String.isNotBlank(caseId))
      {
        system.debug('Case ID '+caseId);
        Case objCase = [ SELECT
                          Id
                        , Status
                        , Approval_Status__c
                        , Offer_Acceptance_Letter_Generated__c
                        , O_A_Signed_Copy_Uploaded__c
                        , Is_New_Payment_Terms_Applied__c
                        , CaseNumber
                        , AccountId
                        , RecordTypeId
                        , RecordType.DeveloperName
                        , Booking_Unit__c
                        , Booking_Unit__r.Registration_ID__c
                         FROM Case 
                         WHERE Id =:caseId
                       ];

        system.debug('objCase======== '+objCase);

        if( String.isNotBlank(objCase.Status) && objCase.Status.equalsIgnoreCase('submitted'))
        {
            //sp = Database.setSavepoint();
            if(objCase.Is_New_Payment_Terms_Applied__c)
            {
                
                List<Task> objTaskList = [ SELECT 
                                           Id,Subject,Status, WhatId
                                           FROM Task
                                           WHERE Status != 'Completed' and WhatId =:objCase.Id
                                         ];
                system.debug('objTaskList======='+objTaskList);
                if(objTaskList == null || objTaskList.size() <= 0)
                {
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error :No Open Task Found with status Update AOPT Details in IPMS.'));
                  return null;
                }

                  // logic to revert the new payment terms and make the previous one active again
                
                  Set<Id> parentPaymentPlanId = new Set<Id>();
                  List<Payment_Plan__c> newPaymentPlanList = new List<Payment_Plan__c>();
                  List<Payment_Plan__c> oldPaymentPlanList = new List<Payment_Plan__c>();

                  /*if(Test.isRunningTest()){
                      isPaymentPlanReverse = true;
                  } else { */
                      isPaymentPlanReverse = PaymentPlanReversalMultipleService.updatePaymentPlanInIPMS( string.valueOf(objCase.Booking_Unit__r.Registration_ID__c), '2-' + string.valueOf(objCase.CaseNumber), objCase );
                  
                  //}
                  system.debug('isPaymentPlanReverse======='+isPaymentPlanReverse);
                  
                  if(isPaymentPlanReverse) {
                      //query for payment plan for above booking unit IDs fetched in Set.
                      for(Payment_Plan__c objPaymentPlan : [SELECT Id,Parent_Payment_Plan__c,Status__c FROM Payment_Plan__c WHERE Booking_Unit__c =:objCase.Booking_Unit__c  AND Status__c = 'Active'])
                      {
                        Payment_Plan__c objNewPaymentPlan =  new Payment_Plan__c( Id = objPaymentPlan.Id);
                        objNewPaymentPlan.Status__c = 'InActive';
                        newPaymentPlanList.add(objNewPaymentPlan);
                        parentPaymentPlanId.add(objPaymentPlan.Parent_Payment_Plan__c);
                      }
    
                      for(Payment_Plan__c objPaymentPlan : [SELECT Id,Parent_Payment_Plan__c,Status__c FROM Payment_Plan__c WHERE Booking_Unit__c =:objCase.Booking_Unit__c AND Status__c = 'InActive' AND Id IN:parentPaymentPlanId])
                      {
                        Payment_Plan__c objOldPaymentPlan = new Payment_Plan__c( Id = objPaymentPlan.Id);
                        objOldPaymentPlan.Status__c = 'Active';
                        oldPaymentPlanList.add(objOldPaymentPlan);
                      }
    
                      system.debug('newPaymentPlanList '+newPaymentPlanList);
                      system.debug('oldPaymentPlanList '+oldPaymentPlanList);
                      
                      if( newPaymentPlanList != null && newPaymentPlanList.size() > 0 )
                      {
                        update newPaymentPlanList;
                        system.debug('newPaymentPlanList '+newPaymentPlanList.size());
                      }
    
                      if( oldPaymentPlanList != null && oldPaymentPlanList.size() > 0 )
                      {
                        update oldPaymentPlanList;
                        system.debug('oldPaymentPlanList '+oldPaymentPlanList.size());
                      }
                    //}
    
                    //update task with Status = closed 'Update AOPT details in IPMS'
                    if(objTaskList != null && objTaskList.size() > 0)
                    {
                      Task objTask = new Task(Id = objTaskList[0].Id);
                      objTask.Status = 'Closed';
                      objTask.Pushed_to_IPMS__c = false;
                      objTask.Description = 'Cancellation initiated by CRE';
                      update objTask;
                    }                
                } else {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error: Payment plan reversal failed, please try again later.'));
                    return null;
                }
            }
            objCase.Status = 'Cancelled';
            update objCase;
            
            PageReference  pg = new PageReference ('/'+caseId);
            pg.setRedirect(true);
            return pg;
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error :You cannot cancel this EHO SR as it is not Submitted.'));
            return null;
        }
      }
      else
      {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error :Case Record is not present.'));
        return null; 
      }
    }
    catch(Exception exp)
    { 
      //Database.rollback(sp);
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please contact your system adminstrator '+exp.getMessage()));
      return null; 
    }
  }
  }