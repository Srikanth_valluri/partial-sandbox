@RestResource(urlMapping='/SaveChangeOfDetailsCase/*')
 Global class SaveChangeOfDetailsCaseToMobileApp
 {
     @HtTPPost
    Global static case SaveChangeOfDetailsCase(CODCaseWrapper codCaseWrapper)
    {
      
        
        Case cocdSR=new Case();
        cocdSR.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(codCaseWrapper.RecordType).getRecordTypeId();
        Group g=[select Id,name from Group where Type = 'Queue' and name='Contact Center Queue'];
        cocdSR.Type = 'Change of Contact Details';
        cocdSR.SR_Type__c = 'Change of Contact Details';
        cocdSR.AccountId = codCaseWrapper.AccountID;
        cocdSR.Address__c =codCaseWrapper.AddressLine1; 
        cocdSR.Address_2__c =codCaseWrapper.AddressLine2;
        cocdSR.Address_3__c =codCaseWrapper.AddressLine3;
        cocdSR.Address_4__c =codCaseWrapper.AddressLine4;
        cocdSR.City__c =codCaseWrapper.City;
        cocdSR.State__c =codCaseWrapper.State; 
        cocdSR.Country__c =codCaseWrapper.Country; 
        cocdSR.Postal_Code__c = codCaseWrapper.PostalCode;
        cocdSR.Address_Arabic_1__c=codCaseWrapper.AddressLine1Arabic; 
        cocdSR.Address_Arabic_2__c=codCaseWrapper.AddressLine2Arabic; 
        cocdSR.Address_Arabic_3__c=codCaseWrapper.AddressLine3Arabic; 
        cocdSR.Address_Arabic_4__c=codCaseWrapper.AddressLine4Arabic; 
        cocdSR.City_Arabic__c=codCaseWrapper.CityArabic; 
        cocdSR.State_Arabic__c=codCaseWrapper.StateArabic; 
        cocdSR.Country_Arabic__c= codCaseWrapper.CountryArabic;
        cocdSR.Postal_Code_Arabic__c=codCaseWrapper.PostalCodeArabic; 
         cocdSR.Status=codCaseWrapper.status;
        cocdSR.Origin=codCaseWrapper.origin;
        cocdSR.Contact_Email__c =codCaseWrapper.Email;
       // cocdSR.Mobile_Country_Code__c = codCaseWrapper.
        //cocdSR.Draft__c=codCaseWrapper.draft;
        cocdSR.Contact_Mobile__c =codCaseWrapper.MObile;
        cocdSR.Additional_Doc_File_URL__c=codCaseWrapper.additionalDocUrl;
        cocdSR.Passport_File_URL__c=codCaseWrapper.passportFileUrl;
        cocdSR.CRF_File_URL__c=codCaseWrapper.crfFormUrl;
        cocdSR.OwnerId=g.id;
        cocdSR.id=codCaseWrapper.salesforceId;
         
        
        try{
        upsert cocdSR;
        }catch(Exception e)
        {
           system.debug('*****case Error '+e.getMessage());
          cocdSR.Payment_Terms_Error__c=e.getMessage();
          return cocdSR; 
        }
        
         Case caseList = [Select id,OwnerId, Status, Origin, Additional_Doc_File_URL__c from Case Where id = :cocdSR.id];
         if(codCaseWrapper.status=='Submitted')
         {
                if(String.isNotBlank(codCaseWrapper.crfFormUrl))
                {
                SR_Attachments__c srAttchmentObj = new SR_Attachments__c(Case__c = cocdSR.id,Account__c = cocdSR.AccountId,Type__c = extractType(cocdSR.CRF_File_URL__c),Name= 'Customer Signed CRF Form '+ System.today(), Attachment_URL__c = cocdSR.CRF_File_URL__c);
                insert srAttchmentObj;
                }
                if(String.isNotBlank( cocdSR.Passport_File_URL__c))
                {
                SR_Attachments__c srAttchmentObj = new SR_Attachments__c(Case__c = cocdSR.id,Account__c = cocdSR.AccountId,Type__c = extractType(cocdSR.Passport_File_URL__c),Name= 'Customer Passport '+ System.today(), Attachment_URL__c =  cocdSR.Passport_File_URL__c);
                insert srAttchmentObj;
                }
                if(String.isNotBlank(cocdSR.Additional_Doc_File_URL__c))
                {
                SR_Attachments__c srAttchmentObj = new SR_Attachments__c(Case__c = cocdSR.id,Account__c = cocdSR.AccountId,Type__c = extractType(cocdSR.Additional_Doc_File_URL__c),Name= ' Additional Document  '+ System.today(), Attachment_URL__c = cocdSR.Additional_Doc_File_URL__c);
                insert srAttchmentObj;
                }
         }
         
         
         
         
        /* {
         Task objTask = new Task();
                try{
                objTask = TaskUtility.getTask((SObject)caseList, 'Verify COD Documents', 'CRE', 
                               'Complaint', system.today().addDays(1));
              //  objTask.Parent_Task_Id__c=complaintCaseObj.id;
                objTask.OwnerId = Label.DefaultCaseOwnerId;
                objTask.Priority = 'High';
                objTask.Status = 'In Progress';
               // objTask.currencyISOcode = caseList.currencyISOcode ;
                system.debug('--objTask--Promotions'+objTask);
                
                  insert objTask;
                  system.debug('>>>>>>>objTask'+objTask);
                }  
                catch(exception e){
                  System.debug('task exception '+ e);
                }                        
        }*/
        Case c=[select id,CaseNumber from case where id=:cocdSR.id];
       // PushNotificationToMobileApp.PushNotificationSend(c.CaseNumber,codCaseWrapper.fcm);
        return cocdSR;
    }
    
    @TestVisible
    private static String setCaseOwner(String AccId) {
    String cocdSR;
        List<Account> lstAccount = [
            SELECT  Id, Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c, Primary_Language__c
            FROM    Account
            WHERE   Id = :AccId
        ];
        if (lstAccount.isEmpty()) {
           
        }
        if (lstAccount[0].Primary_CRE__c != NULL) {
            cocdSR = lstAccount[0].Primary_CRE__c;
        } else if (lstAccount[0].Secondary_CRE__c != NULL) {
            cocdSR = lstAccount[0].Secondary_CRE__c;
        } else if (lstAccount[0].Tertiary_CRE__c != NULL) {
            cocdSR= lstAccount[0].Tertiary_CRE__c;
        } else {
            List<QueueSobject> lstQueue = [
                SELECT      Id, Queue.Name, QueueId FROM QueueSobject
                WHERE       SobjectType = 'Case'
                        AND Queue.Name IN ('Non Elite Arabs Queue', 'Non Elite Non Arabs Queue')
                ORDER BY    Queue.Name
                LIMIT   2
            ];
            if (lstQueue.size() != 2) {
               
            }
            if ('Arabic'.equalsIgnoreCase(lstAccount[0].Primary_Language__c)) {
                cocdSR = lstQueue[0].QueueId;
            } else {
                cocdSR= lstQueue[1].QueueId;
            }
        }
        
        
        return cocdSR;
    }
      
    
    public static String extractType( String strName ) {
        strName = strName.substring( strName.lastIndexOf('\\')+1 );
        return strName.substring( strName.lastIndexOf('.')+1 ) ;
    }
    
       Global class CODCaseWrapper{
        public String RecordType;
        public String AccountID;
        public String UserName;  
        public String AddressLine1;  
        public String AddressLine2;
        public String AddressLine3;
        public String AddressLine4;
        public String City;
        public String State;
        public String PostalCode;
        public String Country;
        public String AddressLine1Arabic;  
        public String AddressLine2Arabic;
        public String AddressLine3Arabic;
        public String AddressLine4Arabic;
        public String CityArabic;
        public String StateArabic;
        public String PostalCodeArabic;
        public String CountryArabic;
        public String Mobile;
        public String Email;
        public String status;
        public String origin;
        public Boolean draft;
        public String fcm;
        public String salesforceId;
        public String additionalDocUrl;
        public String passportFileUrl;
        public String crfFormUrl;
      
    }
 }