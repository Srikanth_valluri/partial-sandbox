@isTest
private class PlotHandoverControllerTest {

    @testSetup
    static void createSetupDate() {
        insert TestDataFactory_CRM.createPersonAccount();
    }

    @isTest static void testInit() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,IsPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        lstBookingUnits[0].Registration_ID__c = '74712';
        lstBookingUnits[0].Unit_Name__c = 'BUTest';
        lstBookingUnits[0].Unit_type__c = 'PLOT';
        lstBookingUnits[0].Handover_Flag__c  = 'N';     
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Plot NOC').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.POA_Expiry_Date__c = System.Today();
        insert objCase;

        SR_Attachments__c pccDoc = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'Undertaking Document');
        insert pccDoc;

        objCase = [SELECT ID, OwnerId FROM Case where Id =: objCase.ID];
        Test.startTest();
            PlotHandoverController controller = new PlotHandoverController();
            controller.accountId = objAcc.Id;
            controller.strSRType = 'PlotHandover';
            controller.objcaseId = String.valueOf(objCase.Id);
            controller.getName();
            controller.gotoInitiation();
            controller.getlstUnits();
        Test.stopTest();
        
        System.assertEquals(controller.strCaseID, String.valueOf(objCase.Id));
        System.assertEquals(controller.strSelectedBookingUnit, lstBookingUnits[0].Id);
    }
    
    @isTest static void testSaveDraft() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,party_ID__c,IsPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        lstBookingUnits[0].Registration_ID__c = '74712';
        lstBookingUnits[0].Unit_Name__c = 'BUTest';
        lstBookingUnits[0].Unit_type__c = 'PLOT';
        lstBookingUnits[0].Handover_Flag__c  = 'N';   
        insert lstBookingUnits;

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Plot NOC').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;

        objCase = [SELECT ID, OwnerId FROM Case where Id =: objCase.ID];

        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
            PageReference pageRef = Page.PlotHandoverProcessPage;
            pageRef.getParameters().put('caseId', String.valueOf(objCase.Id));
            pageRef.getParameters().put('account', String.valueOf(objAcc.Id));
            pageRef.getParameters().put('srType', 'PlotHandover');
            Test.setCurrentPage(pageRef);
            PlotHandoverController controller = new PlotHandoverController();
            controller.strSelectedBookingUnit = lstBookingUnits[0].Id;
            controller.accountId = String.valueOf(objAcc.Id);
            controller.strSRType  = 'PlotHandover';
            controller.strCaseID = String.valueOf(objCase.Id);
            controller.objBookingUnit = lstBookingUnits[0];
            controller.crfAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
            controller.crfAttachmentName  = 'Test1.pdf';
            controller.POAAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
            controller.POAAttachmentName  = 'Test1.pdf';
            controller.UnderTakingDocAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
            controller.UnderTakingDocAttachmentName  = 'Test1.pdf';
            controller.passportBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
            controller.passportName  = 'Test1.pdf';
            controller.saveDraft();

        Test.stopTest();
        PageReference objReference = controller.submitSR();
        List<SR_Attachments__c> lstSRAttachments = [SELECT Id FROM SR_Attachments__c];
        System.assert(lstSRAttachments.size() > 0);
        System.assert(lstSRAttachments.size() == 4);
        System.assert(objCase.Id != null);
        objCase = [SELECT Status FROM Case WHERE Id =: objCase.Id];
        System.assert(objCase != null);
        System.assert(objCase.Status == 'Submitted');
        
    }

    @isTest static void testGetBookingUnitDetails() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,party_ID__c,IsPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        lstBookingUnits[0].Registration_ID__c = '74712';
        lstBookingUnits[0].Unit_Name__c = 'BUTest';
        lstBookingUnits[0].Unit_type__c = 'PLOT';
        lstBookingUnits[0].Handover_Flag__c  = 'N';     
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Plot NOC').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;

        objCase = [SELECT ID, OwnerId FROM Case where Id =: objCase.ID];

        Test.startTest();
            PageReference pageRef = Page.PlotHandoverProcessPage;
            pageRef.getParameters().put('caseId', String.valueOf(objCase.Id));
            pageRef.getParameters().put('account', String.valueOf(objAcc.Id));
            pageRef.getParameters().put('srType', 'PlotHandover');
            Test.setCurrentPage(pageRef);
            PlotHandoverController controller = new PlotHandoverController();
            controller.strSelectedBookingUnit = lstBookingUnits[0].Id;
            controller.accountId = String.valueOf(objAcc.Id);
            controller.strSRType  = 'PlotHandover';
            controller.getlstUnits();
            controller.bookingUnitDetails();
        Test.stopTest();
    }

    @isTest static void testRemoveAttachmentPOA() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,party_ID__c,isPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        lstBookingUnits[0].Registration_ID__c = '74712';
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Title Deed').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Status = 'Submitted';
        insert objCase;
        SR_Attachments__c pccDoc = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'Power Of Attorney');
        insert pccDoc;
        Test.startTest();
            PageReference pageRef = Page.DummyTitleDeedCREPortal;
            pageRef.getParameters().put('CaseId', String.valueOf(objCase.Id));
            pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
            pageRef.getParameters().put('SRType', 'TitleDeed');
            Test.setCurrentPage(pageRef);
            PlotHandoverController controller = new PlotHandoverController();
            controller.RemoveSelected = [SELECT Id FROM SR_Attachments__c LIMIT 1].ID;
            controller.removeAttachment();
        Test.stopTest();
        List<SR_Attachments__c> lstSRAttachments = [SELECT Id FROM SR_Attachments__c];
        System.assert(lstSRAttachments.isEmpty());
    }

    @isTest static void testRemoveAttachmentCRFForm() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,party_ID__c,isPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        lstBookingUnits[0].Registration_ID__c = '74712';
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Title Deed').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Status = 'Submitted';
        insert objCase;
        SR_Attachments__c pccDoc = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'CRF Form');
        insert pccDoc;
        Test.startTest();
            PageReference pageRef = Page.DummyTitleDeedCREPortal;
            pageRef.getParameters().put('CaseId', String.valueOf(objCase.Id));
            pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
            pageRef.getParameters().put('SRType', 'TitleDeed');
            Test.setCurrentPage(pageRef);
            PlotHandoverController controller = new PlotHandoverController();
            controller.RemoveSelected = [SELECT Id FROM SR_Attachments__c LIMIT 1].ID;
            controller.removeAttachment();
        Test.stopTest();
        List<SR_Attachments__c> lstSRAttachments = [SELECT Id FROM SR_Attachments__c];
        System.assert(lstSRAttachments.isEmpty());
    }

    @isTest static void testRemoveAttachmentUndertakingDoc() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,party_ID__c,isPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        lstBookingUnits[0].Registration_ID__c = '74712';
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Title Deed').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Status = 'Submitted';
        insert objCase;
        SR_Attachments__c pccDoc = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'Undertaking Document');
        insert pccDoc;
        Test.startTest();
            PageReference pageRef = Page.DummyTitleDeedCREPortal;
            pageRef.getParameters().put('CaseId', String.valueOf(objCase.Id));
            pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
            pageRef.getParameters().put('SRType', 'TitleDeed');
            Test.setCurrentPage(pageRef);
            PlotHandoverController controller = new PlotHandoverController();
            controller.RemoveSelected = [SELECT Id FROM SR_Attachments__c LIMIT 1].ID;
            controller.removeAttachment();
        Test.stopTest();
        List<SR_Attachments__c> lstSRAttachments = [SELECT Id FROM SR_Attachments__c];
        System.assert(lstSRAttachments.isEmpty());
    }

    @isTest static void testGenerateUndertakingDocument() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,party_ID__c,IsPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        lstBookingUnits[0].Registration_ID__c = '74712';
        lstBookingUnits[0].Unit_Name__c = 'BUTest';
        lstBookingUnits[0].Unit_type__c = 'PLOT';
        lstBookingUnits[0].Handover_Flag__c  = 'N';     
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Plot NOC').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;

        objCase = [SELECT ID, OwnerId FROM Case where Id =: objCase.ID];

        Test.startTest();
            PageReference pageRef = Page.PlotHandoverProcessPage;
            pageRef.getParameters().put('caseId', String.valueOf(objCase.Id));
            pageRef.getParameters().put('account', String.valueOf(objAcc.Id));
            pageRef.getParameters().put('srType', 'PlotHandover');
            Test.setCurrentPage(pageRef);
            PlotHandoverController controller = new PlotHandoverController();
            controller.strSelectedBookingUnit = lstBookingUnits[0].Id;
            controller.objBookingUnit = lstBookingUnits[0];
            controller.accountId = String.valueOf(objAcc.Id);
            controller.strSRType  = 'PlotHandover';
            controller.strCaseID = String.valueOf(objCase.Id);
            controller.blnExceptionalCase = true;
            controller.generateUndertakingDoc();
        Test.stopTest();
        List<SR_Attachments__c> lstSRAttachments = [SELECT Id FROM SR_Attachments__c];
        System.assert(lstSRAttachments.isEmpty());
    }

    @isTest static void testGenerateCRF() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,party_ID__c,IsPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        lstBookingUnits[0].Registration_ID__c = '74712';
        lstBookingUnits[0].Unit_Name__c = 'BUTest';
        lstBookingUnits[0].Unit_type__c = 'PLOT';
        lstBookingUnits[0].Handover_Flag__c  = 'N';     
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Plot NOC').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;

        objCase = [SELECT ID, OwnerId FROM Case where Id =: objCase.ID];

        Test.startTest();
            PageReference pageRef = Page.PlotHandoverProcessPage;
            pageRef.getParameters().put('caseId', String.valueOf(objCase.Id));
            pageRef.getParameters().put('account', String.valueOf(objAcc.Id));
            pageRef.getParameters().put('srType', 'PlotHandover');
            Test.setCurrentPage(pageRef);
            PlotHandoverController controller = new PlotHandoverController();
            controller.strSelectedBookingUnit = lstBookingUnits[0].Id;
            controller.objBookingUnit = lstBookingUnits[0];
            controller.accountId = String.valueOf(objAcc.Id);
            controller.strSRType  = 'PlotHandover';
            controller.strCaseID = String.valueOf(objCase.Id);
            controller.getlstUnits();
            controller.bookingUnitDetails();
            controller.generateCRF();
        Test.stopTest();
    }

    @isTest static void testDeletePOADoc() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,party_ID__c,IsPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        lstBookingUnits[0].Registration_ID__c = '74712';
        lstBookingUnits[0].Unit_Name__c = 'BUTest';
        lstBookingUnits[0].Unit_type__c = 'PLOT';
        lstBookingUnits[0].Handover_Flag__c  = 'N';     
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Plot NOC').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;

        objCase = [SELECT ID, OwnerId FROM Case where Id =: objCase.ID];
        SR_Attachments__c pccDoc = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'Power Of Attorney');
        insert pccDoc;
        Test.startTest();
            PageReference pageRef = Page.PlotHandoverProcessPage;
            pageRef.getParameters().put('caseId', String.valueOf(objCase.Id));
            pageRef.getParameters().put('account', String.valueOf(objAcc.Id));
            pageRef.getParameters().put('srType', 'PlotHandover');
            Test.setCurrentPage(pageRef);
            PlotHandoverController controller = new PlotHandoverController();
            controller.strSelectedBookingUnit = lstBookingUnits[0].Id;
            controller.objBookingUnit = lstBookingUnits[0];
            controller.accountId = String.valueOf(objAcc.Id);
            controller.strSRType  = 'PlotHandover';
            controller.strCaseID = String.valueOf(objCase.Id);
            controller.deletePowerAttornyDoc();
        Test.stopTest();
        List<SR_Attachments__c> lstSRAttachments = [SELECT Id FROM SR_Attachments__c];
        System.assert(lstSRAttachments.isEmpty());
    }

    @isTest static void testPOADoc() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,party_ID__c,IsPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        lstBookingUnits[0].Registration_ID__c = '74712';
        lstBookingUnits[0].Unit_Name__c = 'BUTest';
        lstBookingUnits[0].Unit_type__c = 'PLOT';
        lstBookingUnits[0].Handover_Flag__c  = 'N';     
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Plot NOC').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.isPOA__c = true;
        insert objCase;

        objCase = [SELECT ID, OwnerId, isPOA__c FROM Case where Id =: objCase.ID];
        SR_Attachments__c pccDoc = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'Power Of Attorney');
        insert pccDoc;
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new RestServiceMock());
            PageReference pageRef = Page.PlotHandoverProcessPage;
            pageRef.getParameters().put('caseId', String.valueOf(objCase.Id));
            pageRef.getParameters().put('account', String.valueOf(objAcc.Id));
            pageRef.getParameters().put('srType', 'PlotHandover');
            Test.setCurrentPage(pageRef);
            PlotHandoverController controller = new PlotHandoverController();
            controller.strSelectedBookingUnit = lstBookingUnits[0].Id;
            controller.objBookingUnit = lstBookingUnits[0];
            controller.accountId = String.valueOf(objAcc.Id);
            controller.strSRType  = 'PlotHandover';
            controller.objCase = objCase;
            controller.strCaseID = objCase.Id;
            controller.deletePowerAttornyDoc();
            controller.generateNOCDoc();
            controller.NOCURL();
            controller.closePopup();
        Test.stopTest();
    }

    @isTest static void testSaveDraftAndSubmittionErrorMessages() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,party_ID__c,IsPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        lstBookingUnits[0].Registration_ID__c = '74712';
        lstBookingUnits[0].Unit_Name__c = 'BUTest';
        lstBookingUnits[0].Unit_type__c = 'PLOT';
        lstBookingUnits[0].Handover_Flag__c  = 'N';     
        insert lstBookingUnits;
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));

        Id caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Plot NOC').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRTId);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;

        objCase = [SELECT ID, OwnerId, isPOA__c FROM Case where Id =: objCase.ID];        
            
       Test.startTest();             
        
            PageReference pageRef = Page.PlotHandoverProcessPage;
            pageRef.getParameters().put('caseId', String.valueOf(objCase.Id));
            pageRef.getParameters().put('account', String.valueOf(objAcc.Id));
            pageRef.getParameters().put('srType', 'PlotHandover');
            Test.setCurrentPage(pageRef);
            PlotHandoverController controller = new PlotHandoverController();
            controller.objBookingUnit = lstBookingUnits[0];
            controller.accountId = String.valueOf(objAcc.Id);
            controller.strCaseID = objCase.Id;
            controller.strSRType  = 'PlotHandover';
            controller.showDocuments();            
            controller.saveDraft();
            controller.submitSR();
        Test.stopTest();
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertEquals(2, pageMessages.size());
    }
}