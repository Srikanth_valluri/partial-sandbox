public class DAMAC_AMEYO_DELETE_CALLBACK{

    //Delete Callback
    @InvocableMethod()
    Public static void deleteAmeyoCallback (list<id> inquiryIds){ 
        DAMAC_AMEYO_CUSTOMER.DeleteCallback (inquiryIds);
    }
}