/*-------------------------------------------------------------------------------------------------
Description: Test Class for FMGenerateWorkPermitDocument
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 09-04-2018       | Lochan Karle     | 1. Added logic to test FMGenerateNoticeBRDocument functionality
=============================================================================================================================
*/
@isTest
private class FMGenerateNoticeBRDocumentTest {

	static testMethod void itShould() {
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;

        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;

        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;

        Test.startTest();
			Test.setMock(HttpCalloutMock.class, new RestServiceMock());    
			FM_GenerateDrawloopDocumentBatch objClass = new FM_GenerateDrawloopDocumentBatch(lstBookingUnit[0].Id
																					, System.Label.Notice_BR_DDPId
																					, System.Label.Notice_BR_TemplateId);
			Database.Executebatch(objClass);
        Test.stopTest();
        
		ApexPages.StandardController sc = new ApexPages.StandardController(lstBookingUnit[0]);
        FMGenerateNoticeBRDocument  obj=new FMGenerateNoticeBRDocument(sc);
        obj.callDrawloop();
		obj.returnToBU();
   }
}