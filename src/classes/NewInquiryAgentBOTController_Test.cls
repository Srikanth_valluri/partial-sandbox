@isTest
public class NewInquiryAgentBOTController_Test {

    static testMethod void myUnitTest() {

        Id RSRecordTypeId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId();
        Profile profilePC = [SELECT Id FROM Profile WHERE Name='Property Consultant'];

        Campaign__c campaignInstance = new Campaign__c();
        campaignInstance.RecordTypeId = RSRecordTypeId;
        campaignInstance.Campaign_Name__c='Test Campaign';
        campaignInstance.start_date__c = System.today();
        campaignInstance.end_date__c = System.Today().addDays(30);
        campaignInstance.Marketing_start_date__c = System.today();
        campaignInstance.Marketing_end_date__c = System.Today().addDays(30);
        campaignInstance.Countries__c = 'India';
        campaignInstance.Marketing_Active__c = true;
        campaignInstance.Credit_Control_Active__c = true;
        campaignInstance.Sales_Admin_Active__c = true;
        insert campaignInstance;
        System.assertNotEquals(null, campaignInstance.Id);

        User userObj = new User(
            Alias = 'agntbot',
            Email='agentBot1@bot.com',
            EmailEncodingKey='UTF-8',
            LastName='Testing',
            LanguageLocaleKey='en_US',
            isActive = true,
            LocaleSidKey='en_US',
            ProfileId = profilePC.Id,
            TimeZoneSidKey='America/Los_Angeles',
            UserName='agentBot1@bot.com'
        );
        insert userObj;
        System.assertNotEquals(null, userObj.Id);

        Campaign__c campaignObj = [SELECT Id, Name FROM Campaign__c WHERE Id = :campaignInstance.Id LIMIT 1];

        Test.startTest();

        PageReference newInquiryBOTPage = Page.NewAgentInquiryBOT;
        Test.setCurrentPage(newInquiryBOTPage);

        NewAgentInquiryController newInquiry = new NewAgentInquiryController();
        newInquiry.inquiryObj.Organisation_Name__c = 'NEW AGENT BOT TEST';
        newInquiry.inquiryObj.First_Name__c = 'agent';
        newInquiry.inquiryObj.Last_Name__c = 'botva';
        newInquiry.inquiryObj.Authorized_Signatory__c = 'Test Test';
        newInquiry.inquiryObj.Mobile_CountryCode__c = 'India: 0091';
        newInquiry.inquiryObj.Mobile_Phone_Encrypt__c = '2983743827364';
        newInquiry.inquiryObj.Email__c = 'botkumar@bot.com';
        newInquiry.inquiryObj.Preferred_Language__c = 'English';
        newInquiry.inquiryOwner = userObj.Id;
        newInquiry.inquiryCampaign = campaignObj.Name;
        newInquiry.saveInquiry();

        Test.stopTest();

    }
}