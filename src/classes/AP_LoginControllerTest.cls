@isTest
public class AP_LoginControllerTest{

    public static Contact portalUserContact;
    public static User portalUser;

    static void init(){
        cookie Name = new Cookie('username', 'test', null, 365, false);
        //cookie PasswordCokies = new Cookie('password', 'test', null, 365, false);
        ApexPages.currentPage().setCookies(new Cookie[] {Name});    
    
        
    
    }
    
    @isTest static void loginGuestUser() {
        portalUserContact = InitialiseTestData.getCorporateAgencyContact('testAgent1');
        insert portalUserContact;
        
        portalUser = InitialiseTestData.getPortalUser('test@damac.com', portalUserContact.Id);
        portalUser.isActive = true;
        insert portalUser;
        Test.StartTest();
        init();
        AP_LoginController portalLogin = new AP_LoginController();
        portalLogin.rememberMe = true;
        portalLogin.username = 'test@damac.com';
        portalLogin.password = 'salesforce1';
        portalLogin.login();
        AP_LoginController.loginCheck('test@damac.com','salesforce1');
        PageReference pg = new PageReference('/AP_Dashboard');
        portalLogin.redirectToHome();
        Test.stopTest();
        
    }
    
    @isTest static void testForgotPasswordValidUserName(){
        Test.startTest();
        portalUserContact = InitialiseTestData.getCorporateAgencyContact('testAgent1');
        insert portalUserContact;
        
        portalUser = InitialiseTestData.getPortalUser('testAgent1@damac.com', portalUserContact.Id);
        portalUser.isActive = true;
        insert portalUser;
        
        System.runAs(portalUser){
            AP_LoginController ap_ForgotPassword = new AP_LoginController();
            ap_ForgotPassword.username = 'testAgent1@damac.com';
            ap_ForgotPassword.EmailAddress = 'testAgent1@damac.com';
            ap_ForgotPassword.forgotPassword();
            ap_ForgotPassword.EmailAddress = null;
            ap_ForgotPassword.forgotPassword();
        }
        Test.stopTest();
    }

}