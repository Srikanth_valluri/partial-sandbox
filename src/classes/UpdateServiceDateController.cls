public  class UpdateServiceDateController {
    public List<Calling_List__c> selectedRecords;
    public String filterId;
    public List<Calling_List__c> updateselectedRecords;
    public UpdateServiceDateController(ApexPages.StandardSetController controller) {
           selectedRecords = controller.getSelected();
           updateselectedRecords = new List<Calling_List__c>();
           filterId = controller.getFilterId();
    }
    public pagereference updateServiceDateForSelectedCallings(){
          for(Calling_List__c callInst : selectedRecords){
                System.debug('selectedRecords:::'+selectedRecords);
                callInst.Service_start__c = System.Now();
                callInst.Calling_List_Status__c = 'In Progress';
                updateselectedRecords.add(callInst);
          }
          if(updateselectedRecords != null && !updateselectedRecords.isEmpty()){
            update updateselectedRecords;
          }
          Schema.DescribeSObjectResult result = Calling_List__c.SObjectType.getDescribe();
        PageReference ReturnPage = new PageReference('/'+result.getKeyPrefix()); 
        ReturnPage.setRedirect(true); 
        return ReturnPage;
          //return new Pagereference(String.format(Label.Lightning_List_view_URL, new List<String>{filterId}));
    }
}