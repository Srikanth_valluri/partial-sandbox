@isTest
public class AramexJasonResponceTest {
	@isTest
    public static void jsonParseTest(){
        String SAMPLE_RESPONSE=  '{'+
                '"Transaction": {'+
                    '"Reference1": "00011",'+
                    '"Reference2": "00011",'+
                    '"Reference3": "00011",'+
                    '"Reference4": "00011",'+
                    '"Reference5": "00011"'+
                '},'+
                '"Notifications": [],'+
                '"HasErrors": false,'+
                '"Shipments": ['+
                 '{'+
                        '"ID": "44097896802",'+
                        '"Reference1": "null",'+
                        '"Reference2": null,'+
                        '"Reference3": null,'+
                        '"ForeignHAWB": null,'+
                        '"HasErrors": false,'+
                        '"Notifications": ['+
                            '{'+
                                '"Code": "REQ11",'+
                                '"Message": "Shipper.PartyAddress.CountryCode - Country Code is empty"'+
                            '}'+
                        '],'+
                        '"ShipmentLabel": {'+
                         '   "LabelURL": "http://www.sandbox.aramex.com/content/rpt_cache/50cf77bc973b476db72b44cff76e99bc.pdf",'+
                          '  "LabelFileContents": []'+
                        '},'+
                        '"ShipmentDetails": {'+
                         '   "Origin": "AUH",'+
                          '  "Destination": "AUH",'+
                           ' "ChargeableWeight": {'+
                            '    "Unit": "KG",'+
                            '    "Value": 0.5'+
                            '},'+
                            '"DescriptionOfGoods": "Documents",'+
                            '"GoodsOriginCountry": "AE",'+
                            '"NumberOfPieces": 1,'+
                            '"ProductGroup": "DOM",'+
                            '"ProductType": "ONP",'+
                            '"PaymentType": "P",'+
                            '"PaymentOptions": null,'+
                            '"CustomsValueAmount": null,'+
                            '"CashOnDeliveryAmount": null,'+
                            '"InsuranceAmount": null,'+
                            '"CashAdditionalAmount": null,'+
                            '"CollectAmount": null,'+
                            '"Services": null'+
                        '},'+
                        '"ShipmentAttachments": []'+
                    '}'+
                ']'+
                '}';
        
        	AramexJasonResponce  objRes = (AramexJasonResponce)AramexJasonResponce.parse(SAMPLE_RESPONSE);
    }
}