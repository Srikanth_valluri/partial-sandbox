/****************************************************************************************
Description: Send request to IPMS for Updating Revised Space type
----------------------------------------------------------------------------------------
    Version         Date             Author                     Description                                 
    1.0       18/8/2020      Raveena Jain            Initial Draft
    2.0       19/8/2020      Ruchika Choudhary       Updated class for callling methof from 
                                                     Process Builder
*****************************************************************************************/

global with sharing class UpdateRevisedSpaceType {
    
    /* Method Description : Send request to IPMS for Updating Revised Space type
    * Input Parameters : RegId - Id of Booking Unit record
                      VATPermittedUseType  -   VAT_Permitted_Use_Type__c value 
                                                of record.
    * Return Type : void
    */
    
    global static void sendVATPermittedUseTypeDetails(String RegId, String VATPermittedUseType ){
        
        String attr1;
        if (VATPermittedUseType == 'Serviced') {
            attr1 = System.Label.VAT_Use_Type_Service;
        }else if (VATPermittedUseType == 'Residential') {
            attr1 = System.Label.VAT_Use_Type_Residential;
        } else if (VATPermittedUseType == 'VAT Addnm signed in presence of DLD' ) {
            attr1 = System.Label.VAT_Use_Type_Residential;
        } else if (VATPermittedUseType == 'Unit already residential as per SPA in DLD' ) {
            attr1 = System.Label.VAT_Use_Type_Residential;
        } else if ( VATPermittedUseType == 'Customer paid VAT') {
            attr1 = 'Customer paid VAT';
        }
        
        System.debug('attr1 ' + attr1);
        if (RegId != null && attr1 != null ) {
            system.debug('True if');
            sendHttpRequest(RegId, attr1);
        }
    }
    
    /* Method Description : Method called from Process Builder to send request to IPMS
    * Input Parameters : units - List of Booking Unit from Process Builder
    * Return Type : void
    */
    
    @InvocableMethod
    public static void sendVATPermittedUseTypeDetailsFromPB(List<Id> caseID){
        List<Case> parentCase= new List<Case>();
        if(caseID.size() > 0) {
            parentCase = [ SELECT ID,Registration_Id__c,VAT_Permitted_Use_Type__c
                                        FROM Case
                                       WHERE ID IN :caseID
                                    ];
           
        }
        if (parentCase[0].Registration_Id__c != null && parentCase[0].VAT_Permitted_Use_Type__c != null) {
            sendVATPermittedUseTypeDetails(parentCase[0].Registration_Id__c, parentCase[0].VAT_Permitted_Use_Type__c);
        }
        
      
    }
    
    /* Method Description : Method to send Htp Request to IPMS
    * Input Parameters : units - List of Booking Unit from Process Builder
    * Return Type : void
    */
     @future (Callout = true)
    global static void sendHttpRequest(String paramId, String attribute1 ){
        Credentials_Details__c creds = getCredentials();
        String userName = creds.User_Name__c; //oracle_user
        String password = creds.Password__c; //crp1user
        String headerValue = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(userName + ':' + password));

 

        try {
            
            Http http = new Http();
            HttpResponse response = new HttpResponse();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(creds.Endpoint__c);
            
            request.setMethod('POST');
        
            request.setHeader('Accept', 'application/json');
        
            request.setHeader('Content-Type', 'application/json');
        
            request.setHeader('Accept-Language', 'en-US');
        
         
        
            request.setHeader('Authorization', headerValue);
            request.setBody('{"PROCESS_Input":{ "RESTHeader":{ "Responsibility":"ONT_ICP_SUPER_USER", "RespApplication":"ONT", "SecurityGroup":"STANDARD", "NLSLanguage":"AMERICAN" }, "InputParameters":{ "P_REQUEST_NUMBER":"4546468dfsd6f465465", "P_SOURCE_SYSTEM":"SFDC", "P_REQUEST_NAME":"UPDATE_REVISED_SPACE_TYPE", "P_REQUEST_MESSAGE": { "P_REQUEST_MESSAGE_ITEM": [ {"PARAM_ID":"'+ paramId +'", "ATTRIBUTE1":"'+attribute1+'" } ] } } }}');
            response = http.send(request);
            System.debug('Req Body -----' + request);
            System.debug('Response -----' + response.getBody());
            System.debug('Response -----' + response.getStatusCode());
        } catch (Exception e) {
            System.debug('Exception --- ' + e);
        }
    }
    
    Public Static Credentials_Details__c getCredentials() {
        

            List<Credentials_Details__c> lstCreds = [ SELECT
                                                            Id
                                                            , Name
                                                            , User_Name__c
                                                            , Password__c
                                                            , Endpoint__c
                                                        FROM
                                                            Credentials_Details__c
                                                        WHERE
                                                            Name =  'Update Revised Space Type' ];
            System.debug('Test :' + lstCreds);
            if( lstCreds != null && lstCreds.size() > 0 ) {
                return lstCreds[0];
            } else {
                return null;
            }
        
        
    }

}