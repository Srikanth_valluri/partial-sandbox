@isTest
public class DroppedCallsCallBackCallingHandlerTest{
    public static testmethod void testMethod1() {
    
        User userObj = new User(
                             ProfileId = [SELECT Id FROM Profile WHERE Name = 'Collection - CRE'].Id,
                             LastName = 'test',
                             Email = 'puser000@test.com',
                             Username = 'puser000@test.com' + System.currentTimeMillis(),
                             CompanyName = 'TEST',
                             Title = 'title',
                             isActive = true,
                             Alias = 'alias',
                             Extension = '734535001',
                             TimeZoneSidKey = 'Asia/Dubai',
                             EmailEncodingKey = 'UTF-8',
                             LanguageLocaleKey = 'en_US',
                             LocaleSidKey = 'en_IN'
        );
        insert userObj ;
        
        
        system.runAs(userObj){
            Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Dropped Calls - Call Back').getRecordTypeId();

           
            List<TriggerOnOffCustomSetting__c> settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                             OnOffCheck__c = true);
              
            settingLst2.add(newSetting1);
            insert settingLst2;
            

            Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            Account objAcc = new Account(RecordTypeId = personAccRTId, 
                       FirstName='Test FirstName1', 
                       LastName='Test LastName2', 
                       Type='Person', 
                       party_ID__C='12345'
                       );
            insert objAcc ;
            
            List<Calling_List__c>updateCallingList = new List<Calling_List__c>();
            Calling_List__c callbackCallInst = new Calling_List__c(RecordTypeId = devRecordTypeId,
                                                Customer_Flag__c = true,
                                                Registration_ID__c = '1000',
                                                Party_ID__c = '12345',
                                                CRE_Extension__c = 7001);


            Test.startTest();
                insert callbackCallInst;
            Test.stopTest();
        }
    }
    
    public static testmethod void testMethod2() {
    
        Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Call Back Request').getRecordTypeId();

        List<TriggerOnOffCustomSetting__c> settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        

        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, 
                   FirstName='Test FirstName1', 
                   LastName='Test LastName2', 
                   Type='Person', 
                   party_ID__C='12345'
                   );
        insert objAcc ;
        
        CRE_Pool_Detail__c objCNL = new CRE_Pool_Detail__c();
        objCNL.Name = '6122254';
        objCNL.Language__c = 'Arabic';
        objCNL.Extension__c = '6122254';
        objCNL.Pool_Name__c = 'CRE Pool (Arabic)';
        insert objCNL;
        
        List<Calling_List__c>updateCallingList = new List<Calling_List__c>();
            Calling_List__c callbackCallInst = new Calling_List__c(RecordTypeId = devRecordTypeId,
                                                Customer_Flag__c = true,
                                                Registration_ID__c = '1000',
                                                Party_ID__c = '12345',
                                                CRE_Extension__c = 6122254);


        Test.startTest();
            insert callbackCallInst;
        Test.stopTest();

    }
    
    public static testmethod void testMethod3() {
   
        Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Call Back Request').getRecordTypeId();

        List<TriggerOnOffCustomSetting__c> settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        

        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, 
                   FirstName='Test FirstName1', 
                   LastName='Test LastName2', 
                   Type='Person', 
                   party_ID__C='12345'
                   );
        insert objAcc ;
        
        Collection_Payment_Pool__c objCNL = new Collection_Payment_Pool__c();
        objCNL.Name = '23435532';
        objCNL.Language__c = 'Arabic';
        objCNL.Extension__c = '23435532';
        objCNL.Pool_Name__c = 'Collection / Payments (Arabic)';
        insert objCNL;
        
        List<Calling_List__c>updateCallingList = new List<Calling_List__c>();
            Calling_List__c callbackCallInst = new Calling_List__c(RecordTypeId = devRecordTypeId,
                                                Customer_Flag__c = true,
                                                Registration_ID__c = '1000',
                                                Party_ID__c = '12345',
                                                CRE_Extension__c = 23435532);


        Test.startTest();
            insert callbackCallInst;
        Test.stopTest();
    }
}