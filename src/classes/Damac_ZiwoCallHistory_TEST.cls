@IsTest
public class Damac_ZiwoCallHistory_TEST {

    @testSetup static void setup() {
        Ziwo_settings__c settings = new Ziwo_settings__c ();
        settings.Endpoint__c = 'WWW.DamacGroup.com';
        settings.user_name__c = 'DAMAC';
        settings.password__c = 'DAMAC';
        insert settings;
        
        Waybeo_Logs__c log = new Waybeo_Logs__c ();
        log.Ziwo_callID__c = 'tetst-2321das12-dsa23as23-fa2';
        insert log;


    }
    static testMethod void method1 () {
        Waybeo_Logs__c log = [SELECT ID FROM Waybeo_Logs__c WHERE Ziwo_callID__c != NULL LIMIT 1];
        List <ID> ids = new List <ID> ();
        ids.add (log.id);
        Damac_ZiwoCallHistory.updateCallHistory (ids);
    }
}