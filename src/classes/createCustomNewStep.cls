public class createCustomNewStep {
    
    @InvocableMethod
    public static void createNewStep(List<ID> serviceReqIds) {
        Id serviceReqId;
        Boolean insertNewStep = True;
        
        if (!serviceReqIds.isEmpty()) {
            List<NSIBPM__Service_Request__c> serviceRequests = [SELECT Id FROM NSIBPM__Service_Request__c WHERE Id IN :serviceReqIds];
            if (serviceRequests.Size() > 0) {
                serviceReqId = serviceRequests[0].Id; 
            }
        }
        
        List<New_Step__c> srStep = [SELECT Id FROM New_Step__c WHERE Service_Request__c = :serviceReqId AND Step_Status__c = 'Awaiting More Docs' AND Step_Type__c = 'Awaiting More Docs'];
        if(srStep.Size() > 0) {
            insertNewStep = False;
        }
        
        List<Group> queueList = new List<Group>([SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperName = 'Property_Consultant_Queue']);
        
        if(serviceReqId != Null && insertNewStep) {
            New_Step__c newStep = new New_Step__c();
            newStep.Service_Request__c = serviceReqId;
            newStep.Step_Status__c =  'Awaiting More Docs';
            newStep.Step_Type__c =  'Awaiting More Docs';
            newStep.Step_No__c = 30;
            
            if (queueList.size() > 0) {
                newStep.OwnerId = queueList[0].Id;
            }
            
            insert newStep;
        }
        
    }
}