/*
* Description - Test class developed for UpdateCallingListOutcome
*
* Version            Date            Author            Description
* 1.0              26/11/2017        Arjun Khatri      Initial Draft
*/
@isTest
private class UpdateCallingListOutcomeTest {
    static testMethod void test_UpdateCallingListOutcome() {
        UpdateCallingListOutcome batchCls = new UpdateCallingListOutcome();
        
        Id RecordTypeIdCollection = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Collections_Calling_List'
            AND IsActive = TRUE LIMIT 1
        ].Id;

        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
                
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;

        List<Calling_List__c> lstCallingLists = createCallingList( RecordTypeIdCollection , 5 ,lstBookingUnits );
        insert lstCallingLists;
        
        Test.startTest();
            Database.executeBatch(batchCls);
        Test.stopTest();
    }

    /*
     @ Description : To create calling list reocord
     @ Return      : list of calling list to be created
    */
    public static List<Calling_List__c> createCallingList( Id RecordTypeIdCollection, Integer counter , List<Booking_Unit__c> lstBookingUnits ) {
        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        for( Integer i=0; i<counter; i++ ) {
            lstCallingLists.add(new Calling_List__c( Registration_ID__c = lstBookingUnits[i].Registration_ID__c , Inv_Due__c = 0, DM_Due_Amount__c = 0 , RecordTypeId = RecordTypeIdCollection ) );
        }
        return lstCallingLists;
    }
}