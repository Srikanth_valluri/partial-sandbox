/**************************************************************************************************
* Name               : InventoryUserTriggerHandler                                                *
* Description        : This is a handler class to manage Inventory User object.                   *
*                      - Used to update the Is Assigned flag on the inventory.                    *
* Created Date       : 30/01/2017                                                                 *
* Created By         : NSI - Vineet                                                               *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE          Description                                         *
* 1.0         NSI - Vineet      30/01/2017    Initial Draft                                       *
**************************************************************************************************/
public class InventoryUserTriggerHandler extends UtilitySharingManager implements TriggerFactoryInterface{ 
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after insert.                        *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterInsertTrigger(Map<Id, sObject> newRecordsMap){ 
        try{
            updateInVentoriesWithIsAssigned((Map<Id, Inventory_User__c>) newRecordsMap);
            ProvideAccess((Map<Id, Inventory_User__c>)newRecordsMap);
        }catch(exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
    }    
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after delete.                        *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterDeleteTrigger(Map<Id, sObject> oldRecordsMap){ 
        try{
            updateInVentoriesWithIsAssigned((Map<Id, Inventory_User__c>) oldRecordsMap);
            
            
         revokeAccess((Map<Id, Inventory_User__c>)oldRecordsMap);
        system.debug('executeAfterDeleteTrigger ====');
            
        }catch(exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
    }  
    
    /*********************************************************************************************
    * @Description : Method to update related inventory as Is Assigned = true                     *
    * @Params      : Map<Id, Inventory_User__c>                                                  *
    * @Return      : void                                                                        *
    *********************************************************************************************/ 
    @TestVisible  private void updateInVentoriesWithIsAssigned(Map<Id, Inventory_User__c> inventoruUsersMap){
        List<Inventory__c> updateInventory = new List<Inventory__c>();
        Set<ID> updateInventoryIDs = new Set<ID>();
        System.debug('Trigger execution '+Trigger.isInsert);
        for(Inventory_User__c invUser : inventoruUsersMap.values()){
            if(!updateInventoryIDs.contains(invUser.Inventory__c)){
                updateInventory.add(new Inventory__c(id = invUser.Inventory__c, Is_Assigned__c = (Trigger.isDelete) ? false : true));
                updateInventoryIDs.add(invUser.Inventory__c);
            }
        }
        if(!updateInventory.isEmpty() & updateInventory.size() > 0)
            update updateInventory;
    }
    
    // TOBE Implemented
    public void executeBeforeInsertTrigger(List<sObject> newRecordsList){}
    public void executeBeforeUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){}
    public void executeBeforeInsertUpdateTrigger(List<sObject> newRecordsList, map<Id,sObject> oldRecordsMap){}
    public void executeAfterUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap)
    {
       
    }
    public void executeBeforeDeleteTrigger(Map<Id,sObject> oldRecordsMap)
    {
        List <Inventory_Users_History__c> invHistory = new List <Inventory_Users_History__c> ();
        Set <ID> invIds = new Set <ID> ();
        Set <ID> invUserIds = new Set <ID> ();
        for (Inventory_User__c userVal : (LIST <Inventory_User__c>) oldRecordsMap.values ()) {
            if (userVal.Unit_Allocation__c != NULL && userval.Inventory__c != NULL) {
                invIds.add (userVal.Inventory__c );
                invUserIds.add (userVal.ID);
            }
        }
        
        
        if (invIds.size () > 0) {
            Map <ID, List <Inventory_User__c>> invUserMap = new Map <ID, List <Inventory_User__c>> ();
            for (Inventory_User__c invUser :[SELECT Inventory__c FROM Inventory_User__c 
                                                    WHERE Inventory__c != NULL 
                                                        AND Unit_Allocation__c != NULL
                                                        AND ID NOT IN : invUserIds
                                                        AND Inventory__c IN: invIds]) {
                                                        
                if (!invUserMap.containsKey (invUser.Inventory__c))
                    invUserMap.put (invUser.Inventory__c, new List <Inventory_User__c> {invUser});
                else
                    invUserMap.get (invUser.Inventory__c).add (invUser);
            }
                                                                                                    
            List <Inventory__c> invToUpdate = new List <Inventory__c> ();
            for (Inventory__c inv :[SELECT Unit_allocation__c FROM Inventory__c WHERE ID IN: invIds]) {
                if (invUserMap.containsKey (inv.Id)) {
                    if (invUserMap.get (inv.id).size () == 0) {
                        inv.Unit_allocation__c  = NULL;
                        invTOUpdate.add (inv);
                    }
                } else {
                    inv.Unit_allocation__c  = NULL;
                    invTOUpdate.add (inv);
                }
            }
            if (invToUpdate.size () > 0) {
                Damac_Constants.skip_InventoryTrigger = true;
                update invToUpdate;
            }
        }
        
        for (Inventory_User__c userVal : (LIST <Inventory_User__c>) oldRecordsMap.values ()) {
            if (userVal.Unit_Allocation__c != NULL) {
                Inventory_Users_History__c invUserHistory = new Inventory_Users_History__c ();
                invUserHistory.User__c = userVal.User__c;
                invUserHistory.Inventory__c = userVal.Inventory__c;
                invUserHistory.Unit_Allocation__c = userVal.Unit_Allocation__c;
                invHistory.add (invUserHistory);
            }
        }
        if (invHistory.size () > 0)
            insert invHistory;
    }
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){}
    
    
    /*********************************************************************************************
    * @Description : Method to provide access to the PC and PC's Manager will have record access automatically becuase he is above in roles.                       *
    * @Params      : Map<Id, Inventory_User__c>                                         *
    * @Return      : void                                                                        *
    *********************************************************************************************/ 
    
   public void ProvideAccess(Map<Id, Inventory_User__c> oldRecordsMap)
    {
        List<SObject> shareRecordsList = new List<SObject>();
        map<id,user> userMap=new map<id,user>();
        for(Inventory_User__c obj:oldRecordsMap.values())
        {
           if(obj.Inventory__c!=null&&obj.User__c!=null)
            {
                shareRecordsList.addAll(invokeAccessWithoutHierarchy(obj.User__c, Inventory__c.getSobjectType().getDescribe().getName(), obj.Inventory__c, ''));
            }
        }
         if(!shareRecordsList.isEmpty()){
                // Calling queable method to insert sharing records in the sharing table. 
                //Commented to prevent the "too many queuable jobs error"
                //system.enqueueJob(new UtilitySharingManager(shareRecordsList, new List<sObject>()));
                
             database.saveResult[] result = database.insert(shareRecordsList, false);
            // Iterate through each returned result : TOBE REMOVED
            /*for (Database.SaveResult sr : result) {
                if (sr.isSuccess()) {
                    System.debug('#### Successfully inserted record. ' + sr.getId());
                }else {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('#### The following error has occurred.');                   
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('#### Fields that affected this error: ' + err.getFields());
                    }
                }
            }*/
            }   
    }
    
    /*********************************************************************************************
    * @Description : Method to revoke access after the PC is removed from the Inventory_share .          *
    * @Params      : Map<Id, Inventory_user__c>                                         *
    * @Return      : void                                                                        *
    *********************************************************************************************/ 
    void revokeAccess(Map<Id, Inventory_user__c> DeleteRecordsMap)
    {
        set<id> inventoryIdsSet=new set<id>();
        Map<Id, Map<id,Inventory__share>> InventoryShareRecordsMap;
        List<Inventory__share> ToBeDeleteInventoryShareList=new List<Inventory__share>();
    
        for(Inventory_user__c invUser:DeleteRecordsMap.values())
        {   
            if(invUser.Inventory__c!=null)
                inventoryIdsSet.add(invUser.Inventory__c);
        }
        system.debug('inventoryIdsSet.size() ='+inventoryIdsSet.size());
        if(inventoryIdsSet.size()>0)
            InventoryShareRecordsMap=getSharingRecords(inventoryIdsSet);
        
        for(Inventory_user__c invUser:DeleteRecordsMap.values())
        {
            system.debug('InventoryShareRecordsMap.containsKey(invUser.Inventory__c) ='+InventoryShareRecordsMap.containsKey(invUser.Inventory__c));
            
            system.debug('InventoryShareRecordsMap.get(invUser.Inventory__c).containsKey(invUser.user__c)) ='+InventoryShareRecordsMap.get(invUser.Inventory__c).containsKey(invUser.user__c));
            if(InventoryShareRecordsMap.containsKey(invUser.Inventory__c)&&InventoryShareRecordsMap.get(invUser.Inventory__c).containsKey(invUser.user__c))
            {
                Inventory__share share=InventoryShareRecordsMap.get(invUser.Inventory__c).get(invUser.user__c);
                ToBeDeleteInventoryShareList.add(share);
            }
        }
        
        system.debug('ToBeDeleteInventoryShareList  ='+ToBeDeleteInventoryShareList.size());
         if(!ToBeDeleteInventoryShareList.isEmpty()){
             system.debug('ToBeDeleteInventoryShareList[0]=='+ToBeDeleteInventoryShareList[0]);
            database.deleteResult[] result = database.delete(ToBeDeleteInventoryShareList, false);
            // Iterate through each returned result : TOBE REMOVED
            for (Database.deleteResult sr : result) {
                if (sr.isSuccess()) {
                    System.debug('#### Successfully delete record. ' + sr.getId());
                }else {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('#### The following error has occurred.');                   
                        System.debug(err.getStatusCode() + ': ' + err.getMessage() );
                        System.debug('#### Fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }   

    }

    
/*********************************************************************************************
* @Description : Method to get sharing records from the inventory sharing table.              *
* @Params      : Set<Id>                                                                     *
* @Return      : Map<Id, List<Inventory__share>>                               *
*********************************************************************************************/ 
    private Map<Id, Map<id,Inventory__share>> getSharingRecords(Set<Id> recordIdsSet){ 
        Map<Id, Map<id,Inventory__share>> InventoryShareRecordsMap = new Map<Id, Map<id,Inventory__share>>();
        for(Inventory__share thisShareRecord : [SELECT Id, ParentId, AccessLevel, UserOrGroupId, RowCause 
                                                FROM Inventory__share 
                                                WHERE ParentId IN: recordIdsSet]){
                                                    if(InventoryShareRecordsMap.containsKey(thisShareRecord.ParentId)){
                                                        InventoryShareRecordsMap.get(thisShareRecord.ParentId).put(thisShareRecord.UserOrGroupId,thisShareRecord);      
                                                    }else{
                                                        map<id,Inventory__share> temp= new map<id,Inventory__share>();
                                                        temp.put(thisShareRecord.id,thisShareRecord);
                                                        InventoryShareRecordsMap.put(thisShareRecord.ParentId,temp);
                                                    }   
                                                }   
        system.debug('#### InventoryShareRecordsMap = '+InventoryShareRecordsMap);
        return InventoryShareRecordsMap; 
    }
    
    
}// End of class