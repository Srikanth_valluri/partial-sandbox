@isTest
public class UpdateVehicleDetailsController_cloneTest {

    @isTest
    static void testMethod1() {

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;
        CustomerCommunityUtils.customerAccountId = account.Id;


        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(account);
        insert sr;

        Booking__c booking = new Booking__c(
                                    Deal_SR__c = sr.Id,
                                    Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                             Owner__c = account.Id,
                                             Resident__c =  account.Id,
                                             Registration_Status_Code__c = 'LE',
                                             Handover_Flag__c = 'Y',
                                             Early_Handover__c = true,
                                             Booking__c = booking.Id,
                                             Unit_Name__c = 'Test Unit Name',
                                             Registration_ID__c = '3901');
         insert bookingUnit;

        FM_Additional_Detail__c fmAddDetails = new FM_Additional_Detail__c( IsNewInfo__c = true,
                                                                              Vehicle_Account__c = account.Id,
                                                                              Booking_Unit__c = bookingUnit.Id,
                                                                              Vehicle_Number__c = '1324');
        insert fmAddDetails;


        PageReference myVfPage = Page.CommunityPortal;

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('strAccountId',account.Id);
        //ApexPages.currentPage().getParameters().put('strSelectedUnit',bookingUnit.Id);

        Test.startTest();
        UpdateVehicleDetailsController_clone obj = new UpdateVehicleDetailsController_clone();


        obj.selectUnit();

        //obj.initializeAddDetailsMap();


        //Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        //obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        //obj.strDocumentName='Test_Document_Vehicle_Detaills.htm';
        //obj.uploadDocument();
        Test.stopTest();
    }

    @isTest
    static void testUploaddoc() {

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;
        CustomerCommunityUtils.customerAccountId = account.Id;


        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(account);
        insert sr;

        Booking__c booking = new Booking__c(
                                    Deal_SR__c = sr.Id,
                                    Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                     Owner__c = account.Id,
                                     Resident__c =  account.Id,
                                     Registration_Status_Code__c = 'LE',
                                     Handover_Flag__c = 'Y',
                                     Early_Handover__c = true,
                                     Booking__c = booking.Id,
                                     Unit_Name__c = 'Test Unit Name',
                                     Registration_ID__c = '3901');
        insert bookingUnit;

        FM_Additional_Detail__c fmAddDetails = new FM_Additional_Detail__c( IsNewInfo__c = true,
                                                                              Vehicle_Account__c = account.Id,
                                                                              Booking_Unit__c = bookingUnit.Id,
                                                                              Vehicle_Number__c = '1324');
        insert fmAddDetails;


        //PageReference myVfPage = Page.CommunityPortal;

        //Test.setCurrentPage(myVfPage);
        //ApexPages.currentPage().getParameters().put('strAccountId',account.Id);
        //ApexPages.currentPage().getParameters().put('strSelectedUnit',bookingUnit.Id);
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );


        Test.starttest();
        UpdateVehicleDetailsController_clone obj = new UpdateVehicleDetailsController_clone();

        //PageReference myVfPage = Page.CommunityPortal;

        //Test.setCurrentPage(myVfPage);
        //ApexPages.currentPage().getParameters().put('strAccountId',account.Id);

        //obj.intAddNumOfVehicles = 1;
        //obj.initializeAddDetailsMap();
        //obj.initializeNewAddDetails();
        //obj.strSelectedUnit = bookingUnit.Id;
        //obj.strAccountId = account.Id;

        obj.intAddNumOfVehicles = 1;
        obj.initializeNewAddDetails();

        String strDocumentBody = 'ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        String strDocumentName = 'Test_Document_Vehicle_Detaills.htm';

        obj.strDocumentBody=strDocumentBody;
        obj.strDocumentName=strDocumentName;
        UpdateVehicleDetailsController_clone.uploadDocument(account.id, bookingUnit.id, strDocumentName, strDocumentBody);

        //obj.intAddNumOfVehicles = 1;
        //obj.initializeNewAddDetails();

        //obj.getshowSavebtn();

        obj.saveVehicleDetails();
        //obj.deleteAttachment();
        Test.StopTest();
    }
}