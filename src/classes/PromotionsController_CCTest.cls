/* * * * * * * * * * * * * *
*  Class Name:   PromotionsController_CCTest
*  Purpose:      Unit test class for PromotionsController Controller
*  Author:       Niranjani Mishra - ESPL
*  Company:      ESPL
* * * * * * * * * * * * */
@isTest
public with sharing class PromotionsController_CCTest
{
    public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';

    /* * * * * * * * * * * * *
    *  Method Name:  insertBookingUnitActiveStatus
    *  Purpose:      This method is used to insert custom setting record for Booking Unit Active Status.
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-Feb-2018
    * * * * * * * * * * * * */
    private static void insertBookingUnitActiveStatus()
    {
        List<Booking_Unit_Active_Status__c> buActiveStatusList = new List<Booking_Unit_Active_Status__c>();

        Booking_Unit_Active_Status__c objBUActiveStatus = new Booking_Unit_Active_Status__c();
        objBUActiveStatus.Name = strBookingUnitActiveStatus;
        objBUActiveStatus.Status_Value__c = strBookingUnitActiveStatus;
        buActiveStatusList.add(objBUActiveStatus);

        insert buActiveStatusList;
    }

    /* * * * * * * * * * * * *
    *  Method Name:  getRecordTypeIdForPromotion
    *  Purpose:      This method is used to get record type ID for Promotion
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-Feb-2018
    * * * * * * * * * * * * */
    private static Id getRecordTypeIdForPromotion()
    {
        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id promotionsRecordTypeID = caseRecordTypes.get('Promotions').getRecordTypeId();
        return promotionsRecordTypeID;
    }

    /* * * * * * * * * * * * *
    *  Method Name:  init_test_scenario1
    *  Purpose:      This method is used to unit test functionality in init method with empty case ID as url parameter
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-Feb-2018
    * * * * * * * * * * * * */
    static testMethod void init_test_scenario1()
    {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();

        //insert Booking Unit Active Status custom setting record
        insertBookingUnitActiveStatus();

        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id promotionRecordTypeID = getRecordTypeIdForPromotion();

        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , promotionRecordTypeID);
        objCase.Status = 'Submitted';
        objCase.POA_Expiry_Date__c = System.today();
        objCase.Approval_Status__c = 'Approved';
        objCase.Status = 'Submitted';
        objCase.IsPOA__c = true;
        objCase.Pending_Amount__c = 8000;
        objCase.Total_Amount__c = 8000;
        insert objCase;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property data
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
          objBookingUnit.Re_Assigned__c = false;
        }
        insert bookingUnitList;

        Test.startTest();
          PageReference pageRef = Page.PromotionsProcessRequestPage;
          Test.setCurrentPage(pageRef);

          ApexPages.currentPage().getParameters().put('AccountId', objAccount.Id);
          ApexPages.currentPage().getParameters().put('SRType', 'Promotions');
          PromotionsController objParkingController = new PromotionsController();

          ApexPages.currentPage().getParameters().put('caseID', objCase.Id);
          PromotionsController objParkingControllerNew = new PromotionsController();
        Test.stopTest();
    }

    /* * * * * * * * * * * * *
    *  Method Name:  getName_test
    *  Purpose:      This method is used to unit test functionality in getName getter method
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-Feb-2018
    * * * * * * * * * * * * */
    static testMethod void getName_test()
    {
        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id promotionRecordTypeID = getRecordTypeIdForPromotion();

        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , promotionRecordTypeID);
        objCase.Status = 'Submitted';
        insert objCase;

        Test.startTest();
          PageReference pageRef = Page.PromotionsProcessRequestPage;
          Test.setCurrentPage(pageRef);
          ApexPages.currentPage().getParameters().put('view', 'promotions');
          PromotionsController_CC objPromotionController = new PromotionsController_CC();
          objPromotionController.strAccoundId = objAccount.Id;
          objPromotionController.strSRType = 'Promotions';

          objPromotionController.getName();

        Test.stopTest();
    }

    /* * * * * * * * * * * * *
    *  Method Name:  fetchUnitDetails_test_scenario1
    *  Purpose:      This method is used to unit test functionality in fetchUnitDetails method
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-Feb-2018
    * * * * * * * * * * * * */
    static testMethod void fetchUnitDetails_test_scenario1()
    {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();

        //insert Booking Unit Active Status custom setting record
        insertBookingUnitActiveStatus();

        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id promotionRecordTypeID = getRecordTypeIdForPromotion();

        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , promotionRecordTypeID);
        objCase.Status = 'Submitted';
        objCase.Approval_Status__c = 'Approved';
        objCase.Status = 'Submitted';
        objCase.Pending_Amount__c = 500;
        objCase.Total_Amount__c = 500;
        insert objCase;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property data
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
          objBookingUnit.Re_Assigned__c = false;
        }
        insert bookingUnitList;

        Test.startTest();
          PageReference pageRef = Page.PromotionsProcessRequestPage;
          Test.setCurrentPage(pageRef);

          ApexPages.currentPage().getParameters().put('AccountId', objAccount.Id);
          ApexPages.currentPage().getParameters().put('SRType', 'Promotions');
          ApexPages.currentPage().getParameters().put('view', 'promotions');
          Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );

          PromotionsController_CC objPromotionController = new PromotionsController_CC();
          objPromotionController.strSelectedUnit = bookingUnitList[0].Id;
          objPromotionController.strCaseID = '';
          objPromotionController.fetchUnitDetails();

        Test.stopTest();
    }

    /* * * * * * * * * * * * *
    *  Method Name:  getCustomerPortfolio_test
    *  Purpose:      This method is used to unit test functionality for getCustomerPortfolio method
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-feb-2018
    * * * * * * * * * * * * */
    static testMethod void getCustomerPortfolio_test()
    {

        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Test.startTest();
          PageReference pageRef = Page.PromotionsProcessRequestPage;
          Test.setCurrentPage(pageRef);

          ApexPages.currentPage().getParameters().put('AccountId', objAccount.Id);
          ApexPages.currentPage().getParameters().put('SRType', 'Promotions');

          PromotionsController objPromotionsController = new PromotionsController();
          objPromotionsController.getCustomerPortfolio();
        Test.stopTest();
    }

    /* * * * * * * * * * * * *
    *  Method Name:  GenarateSOA_test
    *  Purpose:      This method is used to unit test functionality for GenarateSOA method
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-Feb-2018
    * * * * * * * * * * * * */
    static testMethod void GenarateSOA_test()
    {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();

        //insert Booking Unit Active Status custom setting record
        insertBookingUnitActiveStatus();

        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id promotionRecordTypeID = getRecordTypeIdForPromotion();

        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , promotionRecordTypeID);
        objCase.Status = 'New';
        insert objCase;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property data
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
          objBookingUnit.Re_Assigned__c = false;
        }
        insert bookingUnitList;

        Test.startTest();
          PageReference pageRef = Page.PromotionsProcessRequestPage;
          Test.setCurrentPage(pageRef);
          ApexPages.currentPage().getParameters().put('AccountId', objAccount.Id);
          ApexPages.currentPage().getParameters().put('SRType', 'Promotions');
          ApexPages.currentPage().getParameters().put('caseID', objCase.Id);



          PromotionsController objPromotionsControllerNew = new PromotionsController();
          objPromotionsControllerNew.strSelectedUnit = bookingUnitList[0].Id;
          Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );

          objPromotionsControllerNew.GenarateSOA();

          objPromotionsControllerNew.strSelectedUnit = 'a0x4E000000sXapAAA';
          objPromotionsControllerNew.GenarateSOA();
        Test.stopTest();
    }


    /* * * * * * * * * * * * *
    *  Method Name:  getPromotions_test
    *  Purpose:      This method is used to unit test functionality for getPromotions method
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-Feb-2018
    * * * * * * * * * * * * */
    static testMethod void getPromotions_test()
    {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();

        //insert Booking Unit Active Status custom setting record
        insertBookingUnitActiveStatus();

        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property data
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,2);
        for(Integer i=0 ; i < bookingUnitList.size() ; i++)
        {
          bookingUnitList[i].Registration_Status__c = strBookingUnitActiveStatus;
          bookingUnitList[i].Inventory__c = objInventory.Id;
          bookingUnitList[i].Registration_ID__c = '74712'+i;
          bookingUnitList[i].Re_Assigned__c = false;
        }
        insert bookingUnitList;

        Promotion_Package__c objPromotionPackage = new Promotion_Package__c();
        objPromotionPackage.Name = 'TestPromotion';
        objPromotionPackage.Price__c = 1000;
        objPromotionPackage.Promotion_Name__c = 'TestPromotion';
        objPromotionPackage.Type__c = 'Armani';
        insert objPromotionPackage;

        Promotion_Package_Offer__c objPromotionPackaOffer = new Promotion_Package_Offer__c();
        objPromotionPackaOffer.Booking_Unit__c = bookingUnitList[0].Id;
        objPromotionPackaOffer.Promotion_Package__c = objPromotionPackage.Id;
        insert objPromotionPackaOffer;

        Test.startTest();
          PageReference pageRef = Page.PromotionsProcessRequestPage;
          Test.setCurrentPage(pageRef);
          ApexPages.currentPage().getParameters().put('AccountId', objAccount.Id);
          ApexPages.currentPage().getParameters().put('SRType', 'Promotions');

          PromotionsController objPromotionsControllerNew = new PromotionsController();
          objPromotionsControllerNew.objUnit = bookingUnitList[0];
          objPromotionsControllerNew.getPromotions();

          // to cover code when no booking unit is selected by CRE
          objPromotionsControllerNew.objUnit = null;
          objPromotionsControllerNew.getPromotions();

          // to cover code when no promotion are found for selected unit
          objPromotionsControllerNew.objUnit = bookingUnitList[1];
          objPromotionsControllerNew.getPromotions();

          PromotionsController.PromotionsWrapper objPromotionWrap = new PromotionsController.PromotionsWrapper();
          objPromotionWrap.blnIsSelected = true;
          objPromotionWrap.objPromotionOffer = objPromotionPackaOffer;
          objPromotionsControllerNew.lstPromotions = new List<PromotionsController.PromotionsWrapper>{objPromotionWrap};

        Test.stopTest();
    }


    /* * * * * * * * * * * * *
    *  Method Name:  calculateTotalPrice_test
    *  Purpose:      This method is used to unit test functionality for calculateTotalPrice method
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-Feb-2018
    * * * * * * * * * * * * */
    static testMethod void calculateTotalPrice_test()
    {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();

        //insert Booking Unit Active Status custom setting record
        insertBookingUnitActiveStatus();

        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property data
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,2);
        for(Integer i=0 ; i < bookingUnitList.size() ; i++)
        {
          bookingUnitList[i].Registration_Status__c = strBookingUnitActiveStatus;
          bookingUnitList[i].Inventory__c = objInventory.Id;
          bookingUnitList[i].Registration_ID__c = '74712'+i;
          bookingUnitList[i].Re_Assigned__c = false;
        }
        insert bookingUnitList;

        Promotion_Package__c objPromotionPackage = new Promotion_Package__c();
        objPromotionPackage.Name = 'TestPromotion';
        objPromotionPackage.Price__c = 1000;
        objPromotionPackage.Promotion_Name__c = 'TestPromotion';
        objPromotionPackage.Type__c = 'Armani';
        insert objPromotionPackage;

        Promotion_Package_Offer__c objPromotionPackaOffer = new Promotion_Package_Offer__c();
        objPromotionPackaOffer.Booking_Unit__c = bookingUnitList[0].Id;
        objPromotionPackaOffer.Promotion_Package__c = objPromotionPackage.Id;
        insert objPromotionPackaOffer;

        Promotion_Package_Offer__c objPromotionPackaOffer2 = new Promotion_Package_Offer__c();
        objPromotionPackaOffer2.Booking_Unit__c = bookingUnitList[0].Id;
        objPromotionPackaOffer2.Promotion_Package__c = objPromotionPackage.Id;
        insert objPromotionPackaOffer2;

        Test.startTest();
          PageReference pageRef = Page.PromotionsProcessRequestPage;
          Test.setCurrentPage(pageRef);
          ApexPages.currentPage().getParameters().put('AccountId', objAccount.Id);
          ApexPages.currentPage().getParameters().put('SRType', 'Promotions');

          PromotionsController objPromotionsControllerNew = new PromotionsController();
          objPromotionsControllerNew.objUnit = bookingUnitList[0];

          List<PromotionsController.PromotionsWrapper> lstPromotionTest = new List<PromotionsController.PromotionsWrapper>();

          PromotionsController.PromotionsWrapper objPromotionWrap = new PromotionsController.PromotionsWrapper();
          objPromotionWrap.blnIsSelected = true;
          objPromotionPackaOffer = [ Select Id,Booking_Unit__c
                                            ,Booking_Unit_Name__c,Promotion_Package__c
                                            ,Promotion_Package__r.Name,Promotion_Package__r.Type__c
                                            ,Promotion_Package__r.Price__c,Promotion_Package__r.Promotion_Name__c
                                     FROM Promotion_Package_Offer__c
                                     WHERE Booking_Unit__c =: bookingUnitList[0].Id
                                     AND ID =:objPromotionPackaOffer.Id
                                   ];
          objPromotionWrap.objPromotionOffer = objPromotionPackaOffer;
          lstPromotionTest.add(objPromotionWrap);
          objPromotionsControllerNew.lstPromotions = lstPromotionTest;
          objPromotionsControllerNew.rowToFetchPrice = 0;
          objPromotionsControllerNew.calculateTotalPrice();

          PromotionsController.PromotionsWrapper objPromotionWrap1 = new PromotionsController.PromotionsWrapper();
          objPromotionWrap1.blnIsSelected = true;
          objPromotionWrap1.objPromotionOffer = objPromotionPackaOffer2;
          lstPromotionTest.add(objPromotionWrap1);
          objPromotionsControllerNew.lstPromotions = lstPromotionTest;
          objPromotionsControllerNew.rowToFetchPrice = 0;
          objPromotionsControllerNew.calculateTotalPrice();


        Test.stopTest();
    }


    /* * * * * * * * * * * * *
    *  Method Name:  createSR_test
    *  Purpose:      This method is used to unit test functionality for createSR method
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-Feb-2018
    * * * * * * * * * * * * */
    static testMethod void createSR_test()
    {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();

        //insert Booking Unit Active Status custom setting record
        insertBookingUnitActiveStatus();

        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id promotionRecordTypeID = getRecordTypeIdForPromotion();

        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , promotionRecordTypeID);
        objCase.Status = 'New';
        insert objCase;


        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property data
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Integer i=0 ; i < bookingUnitList.size() ; i++)
        {
          bookingUnitList[i].Registration_Status__c = strBookingUnitActiveStatus;
          bookingUnitList[i].Inventory__c = objInventory.Id;
          bookingUnitList[i].Registration_ID__c = '74712'+i;
          bookingUnitList[i].Re_Assigned__c = false;
        }
        insert bookingUnitList;

        Promotion_Package__c objPromotionPackage = new Promotion_Package__c();
        objPromotionPackage.Name = 'TestPromotion';
        objPromotionPackage.Price__c = 1000;
        objPromotionPackage.Promotion_Name__c = 'TestPromotion';
        objPromotionPackage.Type__c = 'Armani';
        insert objPromotionPackage;

        Promotion_Package_Offer__c objPromotionPackaOffer = new Promotion_Package_Offer__c();
        objPromotionPackaOffer.Booking_Unit__c = bookingUnitList[0].Id;
        objPromotionPackaOffer.Promotion_Package__c = objPromotionPackage.Id;
        insert objPromotionPackaOffer;

        Promotion_Package_Offer__c objPromotionPackaOffer2 = new Promotion_Package_Offer__c();
        objPromotionPackaOffer2.Booking_Unit__c = bookingUnitList[0].Id;
        objPromotionPackaOffer2.Promotion_Package__c = objPromotionPackage.Id;
        insert objPromotionPackaOffer2;

        Promotion_Package_Allocation__c objPromotionAllocate = new Promotion_Package_Allocation__c();
        objPromotionAllocate.Booking_Unit__c = bookingUnitList[0].Id;
        objPromotionAllocate.Promotion_Package__c = objPromotionPackage.Id;
        objPromotionAllocate.Case__c = objCase.Id;
        insert objPromotionAllocate;

        Test.startTest();
          PageReference pageRef = Page.PromotionsProcessRequestPage;
          Test.setCurrentPage(pageRef);
          ApexPages.currentPage().getParameters().put('AccountId', objAccount.Id);
          ApexPages.currentPage().getParameters().put('SRType', 'Promotions');
          ApexPages.currentPage().getParameters().put('view', 'promotions');

          PromotionsController_CC objPromotionsControllerNew = new PromotionsController_CC();
          objPromotionsControllerNew.objUnit = bookingUnitList[0];
          objPromotionsControllerNew.strCaseStatusSubmitted = 'Submitted';

          List<PromotionsController.PromotionsWrapper> lstPromotionTest = new List<PromotionsController.PromotionsWrapper>();

          PromotionsController.PromotionsWrapper objPromotionWrap = new PromotionsController.PromotionsWrapper();
          objPromotionWrap.blnIsSelected = true;
          objPromotionPackaOffer = [ Select Id,Booking_Unit__c
                                            ,Booking_Unit_Name__c,Promotion_Package__c
                                            ,Promotion_Package__r.Name,Promotion_Package__r.Type__c
                                            ,Promotion_Package__r.Price__c,Promotion_Package__r.Promotion_Name__c
                                     FROM Promotion_Package_Offer__c
                                     WHERE Booking_Unit__c =: bookingUnitList[0].Id
                                     AND ID =:objPromotionPackaOffer.Id
                                   ];
          objPromotionWrap.objPromotionOffer = objPromotionPackaOffer;
          lstPromotionTest.add(objPromotionWrap);

          PromotionsController.PromotionsWrapper objPromotionWrap1 = new PromotionsController.PromotionsWrapper();
          objPromotionWrap1.blnIsSelected = false;
          objPromotionPackaOffer2 = [ Select Id,Booking_Unit__c
                                            ,Booking_Unit_Name__c,Promotion_Package__c
                                            ,Promotion_Package__r.Name,Promotion_Package__r.Type__c
                                            ,Promotion_Package__r.Price__c,Promotion_Package__r.Promotion_Name__c
                                     FROM Promotion_Package_Offer__c
                                     WHERE Booking_Unit__c =: bookingUnitList[0].Id
                                     AND ID =:objPromotionPackaOffer2.Id
                                   ];
          objPromotionWrap1.objPromotionOffer = objPromotionPackaOffer2;
          lstPromotionTest.add(objPromotionWrap1);

          objPromotionsControllerNew.lstPromotions = lstPromotionTest;

          objPromotionsControllerNew.createSR();

          objPromotionsControllerNew.strCaseStatusSubmitted = 'Draft';
          objPromotionsControllerNew.createSR();



        Test.stopTest();
    }

    /* * * * * * * * * * * * *
    *  Method Name:  init_test_scenario2
    *  Purpose:      This method is used to unit test functionality for init method
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-Feb-2018
    * * * * * * * * * * * * */
    static testMethod void init_test_scenario2()
    {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();

        //insert Booking Unit Active Status custom setting record
        insertBookingUnitActiveStatus();

        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property data
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Integer i=0 ; i < bookingUnitList.size() ; i++)
        {
          bookingUnitList[i].Registration_Status__c = strBookingUnitActiveStatus;
          bookingUnitList[i].Inventory__c = objInventory.Id;
          bookingUnitList[i].Registration_ID__c = '74712'+i;
          bookingUnitList[i].Re_Assigned__c = false;
        }
        insert bookingUnitList;

        Id promotionRecordTypeID = getRecordTypeIdForPromotion();

        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , promotionRecordTypeID);
        objCase.Status = 'New';
        objCase.Booking_Unit__c = bookingUnitList[0].Id;
        objCase.Total_Amount__c = 5000;
        insert objCase;

        Promotion_Package__c objPromotionPackage = new Promotion_Package__c();
        objPromotionPackage.Name = 'TestPromotion';
        objPromotionPackage.Price__c = 1000;
        objPromotionPackage.Promotion_Name__c = 'TestPromotion';
        objPromotionPackage.Type__c = 'Armani';
        insert objPromotionPackage;

        Promotion_Package_Offer__c objPromotionPackaOffer = new Promotion_Package_Offer__c();
        objPromotionPackaOffer.Booking_Unit__c = bookingUnitList[0].Id;
        objPromotionPackaOffer.Promotion_Package__c = objPromotionPackage.Id;
        insert objPromotionPackaOffer;

        Promotion_Package_Offer__c objPromotionPackaOffer2 = new Promotion_Package_Offer__c();
        objPromotionPackaOffer2.Booking_Unit__c = bookingUnitList[0].Id;
        objPromotionPackaOffer2.Promotion_Package__c = objPromotionPackage.Id;
        insert objPromotionPackaOffer2;

        Promotion_Package_Allocation__c objPromotionAllocate = new Promotion_Package_Allocation__c();
        objPromotionAllocate.Booking_Unit__c = bookingUnitList[0].Id;
        objPromotionAllocate.Promotion_Package__c = objPromotionPackage.Id;
        objPromotionAllocate.Case__c = objCase.Id;
        insert objPromotionAllocate;

        Test.startTest();
          PageReference pageRef = Page.PromotionsProcessRequestPage;
          Test.setCurrentPage(pageRef);
          ApexPages.currentPage().getParameters().put('AccountId', objAccount.Id);
          ApexPages.currentPage().getParameters().put('caseID', objCase.Id);
          ApexPages.currentPage().getParameters().put('SRType', 'Promotions');

          PromotionsController objPromotionsControllerNew = new PromotionsController();
        Test.stopTest();
    }

    /* * * * * * * * * * * * *
    *  Method Name:  errorLogger_test
    *  Purpose:      This method is used to unit test functionality for errorLogger method
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-Feb-2018
    * * * * * * * * * * * * */
    static testMethod void errorLogger_test()
    {
        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id promotionRecordTypeID = getRecordTypeIdForPromotion();

        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , promotionRecordTypeID);
        objCase.Status = 'New';
        insert objCase;

        Test.startTest();
          PageReference pageRef = Page.PromotionsProcessRequestPage;
          Test.setCurrentPage(pageRef);
          ApexPages.currentPage().getParameters().put('AccountId', objAccount.Id);
          ApexPages.currentPage().getParameters().put('SRType', 'Promotions');

          PromotionsController.errorLogger('Test Error', objCase.Id, '');
        Test.stopTest();
    }
}