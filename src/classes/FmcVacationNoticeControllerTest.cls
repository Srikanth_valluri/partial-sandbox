@isTest
private class FmcVacationNoticeControllerTest {

    @isTest
    public static void testCreateFMCase() {
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;

        CustomerCommunityUtils.customerAccountId = acctIns.Id;

        PageReference myVfPage = Page.CommunityPortal;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view', 'VacationNotice');
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        //ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Vacation_Notice');

        FmcVacationNoticeController obj=new FmcVacationNoticeController();
        obj.strSelectedUnit = buIns.Id;
        obj.selectUnit();
        obj.objFMCase=fmCaseObj;

        obj.saveAsDraft();

        obj.objFMCase.Account__c = acctIns.Id;
        upsert obj.objFMCase;

        ApexPages.currentPage().getParameters().put('id', obj.objFMCase.Id);
        obj = new FmcVacationNoticeController();

        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        obj = new FmcVacationNoticeController();
        ApexPages.currentPage().getParameters().put('UnitId','');
        obj = new FmcVacationNoticeController();
        ApexPages.currentPage().getParameters().put('id',buIns.Id);
        obj = new FmcVacationNoticeController();

    }

    @isTest
    public static void submitFMCase(){
        Id profileId = [select id from profile where name = 'System Administrator'].id;
        User u=TestDataFactoryFM.createUser(profileId);
        insert u;

        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;

        FM_User__c fmUser=TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        PageReference myVfPage = Page.CommunityPortal;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Vacation_Notice';
        insert fmCaseObj;

        CustomerCommunityUtils.customerAccountId = acctIns.Id;

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view', 'VacationNotice');
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        //ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Vacation_Notice');

        FmcVacationNoticeController obj=new FmcVacationNoticeController();
        obj.strSelectedUnit = buIns.Id;
        obj.selectUnit();
        obj.objFMCase.id=fmCaseObj.id;

        test.startTest();
            obj.strDetailType='Emergency Contact';
            obj.addDetails();
            obj.indexOfNewChildToRemove=0;
            obj.removeDetails();
            obj.submitSr();
            obj.returnBackToCasePage();
        test.stopTest();
    }

    @isTest
    public static void testUploadDocument(){
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;

        PageReference myVfPage = Page.CommunityPortal;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Vacation_Notice';
        insert fmCaseObj;

        CustomerCommunityUtils.customerAccountId = acctIns.Id;

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view', 'VacationNotice');
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Vacation_Notice');

        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );

        FmcVacationNoticeController obj=new FmcVacationNoticeController();
        obj.strSelectedUnit = buIns.Id;
        obj.selectUnit();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';
        obj.createCaseShowUploadDoc();

        test.startTest();
        obj.uploadDocument();
        test.stopTest();
    }

}