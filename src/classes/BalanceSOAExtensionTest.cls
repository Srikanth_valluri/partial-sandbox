@isTest
public class BalanceSOAExtensionTest {
    public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';
    private static Id getRecordTypeId(){
        return Schema.SObjectType.Case.getRecordTypeInfosByName().get('Mortgage').getRecordTypeId();
    }
    @isTest public static void myAssignmentDueUnitTest() {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
        
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;
        
        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;
        
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;
        
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
            objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
            objBookingUnit.Inventory__c = objInventory.Id;
            objBookingUnit.Registration_ID__c = '74712';
        }
        insert bookingUnitList;
        
        Case objCase = new Case();
        objCase.Mortgage_Status__c = 'Mortgage Final Offer Letter';
        objCase.Type =   'Mortgage';	
        objCase.Status = 'Submitted';
        objCase.Customer_Eligible_For_Mortgage__c =   true;
        objCase.Approved_by_Bank__c =   true;
        objCase.Seller__c = objAccount.Id;
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';
        objCase.Booking_Unit__c = bookingUnitList[0].Id;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Mortgage').getRecordTypeId();
        insert objCase;
        
        Task taskObj1 = new Task();
        taskObj1.WhatId = objCase.Id;
        taskObj1.Priority = 'High';
        taskObj1.ActivityDate = System.Today();
        taskObj1.Status = 'Closed';
        taskObj1.Subject = System.Label.Mortgage_27;
        taskObj1.Assigned_User__c = 'CRE';
        taskObj1.Process_Name__c = 'Mortgage';
        taskObj1.Task_Due_Date__c = System.Today();
        taskObj1.Completed_DateTime__c = System.Now();
        insert taskObj1;
        
        
        
        
        test.StartTest();
        Test.setMock(WebServiceMock.class, new MortgageWebServiceCalloutMock());
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        BalanceSOAExtension   objDocExt = new BalanceSOAExtension (stdController);
        objDocExt.caseId = objCase.Id;
        objDocExt.getBalanceSOA();
        test.StopTest();
    }
    public static List<Case> createCase( String recordTypeDevName, Integer counter ,Id accId  ) {
        
        Id recodTypeIdCase = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType=:'Case'
            AND DeveloperName=:'Mortgage'
            AND IsActive = TRUE LIMIT 1
        ].Id;
        
        List<Case> lstCase = new List<Case>();
        for( Integer i=0; i<counter; i++ ) {
            lstCase.add(new Case( AccountId = accId, Credit_Note_Amount__c=5000, RecordTypeId = recodTypeIdCase ) );
        }
        return lstCase;
    }
    @isTest public static void errorTest() {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
        
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;
        
        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;
        
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;
        
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
            objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
            objBookingUnit.Inventory__c = objInventory.Id;
            objBookingUnit.Registration_ID__c = '74712';
        }
        insert bookingUnitList;
        
        Case objCase = new Case();
        objCase.Mortgage_Status__c = 'Mortgage Final Offer Letter';
        objCase.Type =   'Mortgage';	
        objCase.Status = 'Submitted';
        objCase.Customer_Eligible_For_Mortgage__c =   true;
        objCase.Approved_by_Bank__c =   true;
        objCase.Seller__c = objAccount.Id;
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';
        objCase.Booking_Unit__c = bookingUnitList[0].Id;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Mortgage').getRecordTypeId();
        insert objCase;
        
        Task taskObj1 = new Task();
        taskObj1.WhatId = objCase.Id;
        taskObj1.Priority = 'High';
        taskObj1.ActivityDate = System.Today();
        taskObj1.Status = 'Closed';
        taskObj1.Subject = System.Label.Mortgage_27;
        taskObj1.Assigned_User__c = 'CRE';
        taskObj1.Process_Name__c = 'Mortgage';
        taskObj1.Task_Due_Date__c = System.Today();
        taskObj1.Completed_DateTime__c = System.Now();
        insert taskObj1;
        
        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1,1));
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        BalanceSOAExtension   objDocExt = new BalanceSOAExtension (stdController);
        objDocExt.caseId = objCase.Id;
        objDocExt.getBalanceSOA();
        test.StopTest();
    }
    @isTest public static void due0Test() {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
        
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;
        
        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;
        
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;
        
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
            objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
            objBookingUnit.Inventory__c = objInventory.Id;
            objBookingUnit.Registration_ID__c = '74712';
        }
        insert bookingUnitList;
        
        Case objCase = new Case();
        objCase.Mortgage_Status__c = 'Mortgage Final Offer Letter';
        objCase.Type =   'Mortgage';	
        objCase.Status = 'Submitted';
        objCase.Customer_Eligible_For_Mortgage__c =   true;
        objCase.Approved_by_Bank__c =   true;
        objCase.Seller__c = objAccount.Id;
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';
        objCase.Booking_Unit__c = bookingUnitList[0].Id;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Mortgage').getRecordTypeId();
        insert objCase;
        
        Task taskObj1 = new Task();
        taskObj1.WhatId = objCase.Id;
        taskObj1.Priority = 'High';
        taskObj1.ActivityDate = System.Today();
        taskObj1.Status = 'Closed';
        taskObj1.Subject = System.Label.Mortgage_27;
        taskObj1.Assigned_User__c = 'CRE';
        taskObj1.Process_Name__c = 'Mortgage';
        taskObj1.Task_Due_Date__c = System.Today();
        taskObj1.Completed_DateTime__c = System.Now();
        insert taskObj1;
        
        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(2));
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        BalanceSOAExtension   objDocExt = new BalanceSOAExtension (stdController);
        objDocExt.caseId = objCase.Id;
        objDocExt.getBalanceSOA();
        test.StopTest();
    }
}