public with sharing class CustomerNotificationBatchUtility {
    /*public static list<Calling_List__c> getAllCallingListRecords() {
    	return new list<Calling_List__c>( [ SELECT Id
    										  FROM Calling_List__c 
    										 WHERE RecordType.Name = 'Collections'] );
    }*/
    
    public static list<Collection_Customer_Notification_Setting__mdt> getListOfNotificationSettings() {
    	return new list<Collection_Customer_Notification_Setting__mdt>( [ SELECT Id
    																		   , MasterLabel
    																		   , Email_Template_API_Name__c
    																		   , Number_of_Days__c
    																		   , DeveloperName
    																		   , Notification_Type__c
    																		FROM Collection_Customer_Notification_Setting__mdt ] );
    }
    
    public static map<Id, EmailTemplate> getEmailTemplates( set<String> lstEmailTemplateAPINames ) {
    	return new map<Id, EmailTemplate>( [ SELECT Id
    											  , DeveloperName
    										   FROM EmailTemplate 
    										  WHERE DeveloperName IN :lstEmailTemplateAPINames ] );
    }
}