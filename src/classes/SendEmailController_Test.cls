@istest
public class SendEmailController_Test {

    public testmethod static void UnitTestCase()
    { 
        Inquiry__c inq=new Inquiry__c();
        insert inq;
        
        Email_message__c msg=new Email_message__c();
        msg.Inquiry__c=inq.id;
        msg.CC_Address__c='test@test.com';
        msg.BCC_Address__c='Test@test2.com';
        insert msg;
        
        PageReference PageRef=Page.SendEmail;
        pageRef.getParameters().put('id',inq.id);
        
        pageRef.getParameters().put('msgId',msg.id);
        pageRef.getParameters().put('Outgoing',msg.id);
        
        Test.setCurrentPageReference(pageRef);
        
        ApexPages.StandardController std = new ApexPages.StandardController(new Inquiry__c());
		SendEmailController controller=new SendEmailController(std);
        
        controller.sendEmailTocustomer();
        controller.eMsg=msg.clone();
        controller.sendEmailTocustomer();
    }
}