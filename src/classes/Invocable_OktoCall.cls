/**************************************************************************************************
* Name               : Invocable_OktoCall
* Test Class         : Invocable_OktoCall_Test
* Description        : This is the custom code class for sending Ok to Call details to IPMS         
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0                         16/06/2019      Created
* 1.1         QBurst          08/04/2020      SOAP to REST Changes
**************************************************************************************************/
public class Invocable_OktoCall {
    @InvocableMethod
    public static void UpdateBuyer(List<Id> stepId) {
        for (New_Step__c step :[SELECT Id, Service_Request__c FROM New_Step__c WHERE Id IN :stepId]) {
            evaluateCustomCode(step);
        }
    }

    public static String evaluateCustomCode(New_Step__c step) {
        String retStr = 'Success';
        List<Id> bookingIds = new List<id> ();
        try {
            for (Booking__c bookings :[SELECT Id FROM Booking__c WHERE Deal_SR__c = : step.Service_Request__c]) {
                bookingIds.add(bookings.id);
            }
            if (bookingIds.size() > 0) {
                system.debug('#### invoking CC_OktoCall ');
               // system.enqueueJob(new AsyncReceiptWebservice(bookingIds, 'OktoCall')); // commented out as part of 1.1
               DAMAC_IPMS_PARTY_CREATION.updateBookingUnitToIPMS(bookingIds, 'GENERATE_CALL', 'Y'); // 1.1
            }
        } catch(Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
        }
        return retStr;
    }
}