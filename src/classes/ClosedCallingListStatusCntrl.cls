public with sharing class ClosedCallingListStatusCntrl{
    public String callingListId {get;set;}
    public List<Calling_List__c>callingList {get;set;}
    //public static Id walkInRecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Walk In Calling List').RecordTypeId;
        
    public List<Calling_List__c>updateCallingList {get;set;}
    public ClosedCallingListStatusCntrl(ApexPages.StandardController controller) {
    //  updateServiceDate();
    }
    public Pagereference updateStatus(){
		system.debug('in updateStatus : ' );
     callingList = new List<Calling_List__c>();
     updateCallingList = new List<Calling_List__c>();
     callingListId = ApexPages.currentPage().getParameters().get('id');
	 system.debug('in callingListId : ' +callingListId);
     callingList = [SELECT Id,
                        RecordTypeId,Closed_By_Reception__c,Closure_by_Reception_Date__c,
                        Case__c,Calling_List_Status__c ,
                        Name,
                        Service_start__c,CreatedDate,
                        Account__r.Name,
                        Account__c
                   FROM Calling_List__c
                  WHERE Id = :callingListId
                   // AND RecordTypeId = :walkInRecordTypeId
                     ];
	   system.debug('in callingList : ' +callingList);
       for(Calling_List__c callObj : callingList){
            //callObj.Calling_List_Status__c = 'Closed';
            callObj.Closed_By_Reception__c = true;
            callObj.Closure_by_Reception_Date__c = system.now();
            updateCallingList.add(callObj);
       }
	   system.debug('in updateCallingList : ' +updateCallingList);
       update updateCallingList;
       PageReference ReturnPage = new PageReference('/' +callingListId); 
       ReturnPage.setRedirect(true); 
       return ReturnPage;
      // return new Pagereference(String.format(Label.Lightning_Calling_Detail_Page_Url, new List<String>{callingListId}));             
    }
}