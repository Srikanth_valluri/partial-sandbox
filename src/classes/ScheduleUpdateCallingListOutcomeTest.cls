/*
* Description - Test class developed for ScheduleUpdateCallingListOutcome
*
* Version            Date            Author            Description
* 1.0              26/11/2017        Arjun Khatri      Initial Draft
*/
@isTest
private class ScheduleUpdateCallingListOutcomeTest  {
    static testMethod void testExecute() {
        Test.startTest();
            ScheduleUpdateCallingListOutcome objScheduler = new ScheduleUpdateCallingListOutcome();
            String schCron = '0 0 0 * * ? *';
            String jobId = System.Schedule('Reccord Updated',schCron,objScheduler);
        Test.stopTest();
        CronTrigger cronTrigger = [
            SELECT Id
                 , CronExpression
                 , TimesTriggered
                 , NextFireTime
            FROM CronTrigger
           WHERE id = :jobId
        ];
        System.assertEquals( schCron,  cronTrigger.CronExpression  , 'Cron expression should match' );
        System.assertEquals( 0, cronTrigger.TimesTriggered  , 'Time to Triggered batch should be 0' );
        List<AsyncapexJob> apexJobs = [
            SELECT Id, ApexClassID ,JobType , Status 
            FROM AsyncapexJob
            WHERE JobType = 'BatchApex'];
        System.assertEquals( false , apexJobs.isEmpty()  , 'Expected at least one Async apex Job' );
        System.assertEquals( 'BatchApex', apexJobs[0].JobType  , 'Job Type should match ' );
    }
}