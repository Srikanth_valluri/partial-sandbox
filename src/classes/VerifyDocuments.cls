public without sharing class VerifyDocuments{
    @InvocableMethod
    public static void closeTask(List<ID> ids){
        set<String> setSubjects = new set<String>();
        set<Id> setCaseIds = new set<Id>();
        for(Task objT : [Select Id
                              , WhatId
                              , Subject 
                         from Task 
                         where Id IN : ids]){
            if(objT.Subject.contains('Verification Pending')){
                String docName = objT.Subject.replace('Verification Pending','');
                setSubjects.add(docName.trim());
                setCaseIds.add(objT.WhatId);
            }
        }
        if(!setSubjects.isEmpty() && !setCaseIds.isEmpty()){
            list<SR_Attachments__c> lstDocs = new list<SR_Attachments__c>();
            for(SR_Attachments__c objDoc : [Select Id
                                                    , Name
                                                    , isValid__c
                                                    , Case__c 
                                               from SR_Attachments__c
                                               where Case__c IN : setCaseIds
                                               and Name IN : setSubjects]){
                objDoc.isValid__c = true;
                lstDocs.add(objDoc);
            }
            if(!lstDocs.isEmpty()){
                update lstDocs;
            }
        }
    }
}