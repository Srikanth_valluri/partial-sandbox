global class Damac_RetrySMSBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.stateFul {
    String queryString;
    String objName;
    String message = '';
    global Damac_RetrySMSBatch (String query, String objectName, String messageReturn) {
        queryString = query;
        objName = objectName;
        
        if (messageReturn != '' && messageReturn != null)
            message = messageReturn;
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        if (queryString != '' && queryString != NULL) {} 
        else
            queryString = Label.Retry_SMS_Query;
        if (Test.isRunningTest ()) {
            queryString = 'Select id from Stand_Inquiry__c LIMIT 1';
        }
        
        return Database.getQueryLocator(queryString);
    }
    global void execute(Database.BatchableContext BC, List <SObject> scope){
        Set <ID> stdInquiries = new Set <ID> ();
        for (SObject inq :scope){
            stdInquiries.add (inq.id);
        }

        Damac_sendSMS.sendSecureMessageSync (stdInquiries, true, objName, message);
    }
    global void finish(Database.BatchableContext BC){
    }

}