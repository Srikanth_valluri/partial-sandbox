/**************************************************************************************************
* Name               : CustomTaskList                                                             *
* Description        : Controller class for CustomTask page.                                      *
* Created Date       : 05/02/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         Rahul Rajeev      05/02/2017      Initial Draft.                                    *
* 1.1         Vineet            16/07/2017      Added wrapper field for field labels.             *
**************************************************************************************************/
public with sharing class CustomTaskList{

    public Wrapper wrapperObj       {get; set;}
    public String inquiryRTId       = '';
    public Boolean isAgentTeamRT    {get; set;}

    /********************************************************************************************* 
    * @Description : Constructor.                                                                *
    * @Params      : void                                                                        *
    * @Return      : void                                                                        *
    *********************************************************************************************/ 
    public CustomTaskList(ApexPages.standardController controller){
        Inquiry__c inq = (Inquiry__c) controller.getRecord();
        inquiryRTId = inq.RecordTypeId;
        getTaskList(controller.getRecord().Id);
    }
    
    /********************************************************************************************* 
    * @Description : Method to get task list based on the logged in user.                        *
    * @Params      : Id                                                                          *
    * @Return      : Wrapper                                                                     *
    *********************************************************************************************/
    public Wrapper getTaskList(Id inquiryId){
        String userId = UserInfo.getUserId();
        String agentTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_Agent_Team_RT).getRecordTypeId();
        System.debug('agentTeamRT >>>>>>  ' + agentTeamRT);
        System.debug('inquiryRTId >>>>>>  ' + inquiryRTId);
        if (inquiryRTId == agentTeamRT) {
            isAgentTeamRT = true;
        } else {
            isAgentTeamRT = false;
        }
        String profileName = '';
        String tsProfileName='Telesales Team';
        String CTI = 'CTI User';
        wrapperObj = new Wrapper();
        String query = 'Select ';
        for(Schema.FieldSetMember f : SObjectType.Task.FieldSets.CustomTaskList.getFields()){
            wrapperObj.fieldList.add(f.getFieldPath());
            wrapperObj.fieldLabelList.add(f.getLabel());
            query += f.getFieldPath() + ', ';
        }
        //User[] eu =[SELECT Extension FROM User WHERE UserProfileName__c =:tsProfileName];
        for(Profile p : [SELECT Id, Name FROM Profile WHERE Id=:UserInfo.getProfileId() LIMIT 1]){
            profileName = p.Name;
        }
        
        List<String> agentProfileNames = Label.Agent_Profiles_For_SR.split(',');
        System.debug('agentProfileNames >>>>>>  ' + agentProfileNames);
        List<String> viewAllActivities = Label.ViewAllActivities.split(',');

        if (viewAllActivities != null ) {
            Set<String> userNamesSet = new Set<String>();
            userNamesSet.addAll(viewAllActivities);
            if (userNamesSet.contains(UserInfo.getName())) {
                query += ' Id FROM Task WHERE WhatId =: inquiryId Order By CreatedDate Desc';
            }else if(profileName.equalsIgnoreCase('property consultant')){
                query += 'Id from Task where WhatId =: inquiryId AND (OwnerId =: userId OR Owner_Profile__c=:tsProfileName OR Owner_Profile__c=:CTI OR Created_By_Profile__c IN :agentProfileNames) Order By CreatedDate Desc';
             } 
            else {
                query += ' Id FROM Task WHERE WhatId =: inquiryId Order By CreatedDate Desc';
            }
        } 
        /*else {
            query += ' Id FROM Task WHERE WhatId =: inquiryId AND OwnerId = :userId ';
        }*/
        /*if(profileName.equalsIgnoreCase('property consultant')){
            query += 'Id from Task where WhatId =: inquiryId AND (OwnerId =: userId OR Owner_Profile__c=:tsProfileName OR Owner_Profile__c=:CTI OR Created_By_Profile__c IN :agentProfileNames )';
        }else{
            query += 'Id from Task where WhatId =: inquiryId';
        }*/
        wrapperObj.taskList = Database.query(query);
        return wrapperObj;
    }
    
    /********************************************************************************************* 
    * @Description : Wrapper class.                                                              *
    *********************************************************************************************/ 
    public class Wrapper{
        public List<Task> taskList {get; set;}
        public List<String> fieldLabelList {get; set;}
        public List<String> fieldList {get; set;}
        public Wrapper(){
            taskList = new List<Task>();
            fieldLabelList = new List<String>();
            fieldList = new List<String>();
        }
    }
}// End of class.