public class StepTriggerHandler implements TriggerFactoryInterface{
    
    //Before Trigger Methods
    public void executeBeforeInsertTrigger(list<sObject> lstNewRecords){
        Set<Id> srIds = new Set<Id>();
        Map<Id, NSIBPM__Service_Request__c> srMap = new Map<Id, NSIBPM__Service_Request__c>();
        List<NSIBPM__Step__c> stepList = lstNewRecords;
        
        if(!stepList.isEmpty()){
            for(NSIBPM__Step__c stp : stepList){
                if(stp.NSIBPM__SR__c != null){
                    srIds.add(stp.NSIBPM__SR__c);
                }
            }
        }
        
        if(!srIds.isEmpty()){
            for(NSIBPM__Service_Request__c sr : [SELECT Id, Manager_Email__c, 
                                                        DOS_Email__c, HOS_Email__c, 
                                                        HOD_Email__c, Name,
                                                        SR_Owner_Email__c, SR_Owner_Profile__c
                                                   FROM NSIBPM__Service_Request__c 
                                                  WHERE Id IN :srIds
            ]) {
                srMap.put(sr.Id, sr);
            }
        }
        
        if(!srMap.isEmpty() && !stepList.isEmpty()){
            for(NSIBPM__Step__c stp : stepList){
                stp.Manager_Email__c = srMap.get(stp.NSIBPM__SR__c).Manager_Email__c;
                stp.DOS_Email__c = srMap.get(stp.NSIBPM__SR__c).DOS_Email__c ;
                stp.HOS_Email__c = srMap.get(stp.NSIBPM__SR__c).HOS_Email__c ;
                stp.HOD_Email__c = srMap.get(stp.NSIBPM__SR__c).HOD_Email__c ;
                // Craig Lobo 28/01/2018 Update the Step Owner fields to SR Owner and not SR creator
                stp.SR_Owner_Email__c = srMap.get(stp.NSIBPM__SR__c).SR_Owner_Email__c;
                stp.SR_Owner_Profile__c = srMap.get(stp.NSIBPM__SR__c).SR_Owner_Profile__c;
                // Craig Lobo 28/01/2018 
            }
        }
        system.debug(stepList);
    }
    
    public void executeBeforeUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
    public void executeBeforeDeleteTrigger(Map<Id, sObject> mapOldRecords){}
    public void executeBeforeInsertUpdateTrigger(list<sObject> lstNewRecords, Map<Id,sObject> mapOldRecords){}
    
    //After Trigger Methods
    public void executeAfterInsertTrigger(Map<Id, sObject> mapNewRecords){}
    public void executeAfterUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
    public void executeAfterDeleteTrigger(Map<Id, sObject> mapOldRecords){}
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
}