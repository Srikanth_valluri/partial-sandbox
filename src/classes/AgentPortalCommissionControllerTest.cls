// Created By : naresh Kaneriya

@isTest(SeeAllData=false)
public class AgentPortalCommissionControllerTest{

    public static Contact adminContact;
    public static User portalUser;
    public static Account adminAccount;
    //public static User portalOnlyAgent;
    public static Contact agentContact;


    public static testMethod void TestData(){
        
        //UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'Chairman');
        //insert userRoleObj;
        Id adminRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = 'Chairman' LIMIT 1].Id;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't156', email='x1234@email.com',
                emailencodingkey='UTF-8', lastname='User 56', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='x1234@email.com',UserRoleId = adminRoleId);
        
        System.RunAs(adminUser) {
            adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;
            
            adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            insert adminContact;
            
            agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            
            portalUser = InitialiseTestData.getPortalUser('test45@test.com', adminContact.Id, 'Admin');
           // portalOnlyAgent = InitialiseTestData.getPortalUser('test54@test.com', agentContact.Id, 'Agent');
            
            NSIBPM__Service_Request__c srRecord = 
                InitialiseTestData.createBookingServiceRequest(true, true, adminContact.AccountId , System.today());

            Booking__c bookingRecord = 
                InitialiseTestData.createBookingRecords(adminContact.AccountId, srRecord, 30000000,'Pending');

            Booking_Unit__c bookingUnitObject = new Booking_Unit__c();
            bookingUnitObject.Booking__c = bookingRecord.Id;
            //bookingUnitObject.Inventory__c = bookingRecord.Inventory__c;
            bookingUnitObject.Unit_Selling_Price__c = 15000000.00;
            bookingUnitObject.Requested_Price__c = 10000000.00;
            bookingUnitObject.Requested_Token_Amount__c = 40000.00;
            bookingUnitObject.Payment_Method__c = 'Online_Payment';
            bookingUnitObject.Online_Payment_Party__c = 'Third Party';
            bookingUnitObject.No_of_parking__c = 1.00;
            bookingUnitObject.Primary_Buyer_s_Email__c = 'test@test.com';
            bookingUnitObject.Primary_Buyer_s_Name__c = 'Damac Test';
            bookingUnitObject.Primary_Buyer_Country__c = 'United Arab Emirates';
            bookingUnitObject.Primary_Buyer_s_Nationality__c ='Emirati';
            bookingUnitObject.Unique_Key__c = 'BOOKING_UNIT ' + String.valueOf(System.now().millisecond()); 
            insert bookingUnitObject;

            List<Id> fixedSearchResults = new  List<Id>();
            fixedSearchResults.add(bookingUnitObject.Id);

            Test.setFixedSearchResults(fixedSearchResults);


            System.RunAs(portalUser) {

                AgentPortalCommissionController obj =  new AgentPortalCommissionController();
                obj.AgentBUList = null ;
                obj.totalUnitPrice= 22.25;
                obj.totalCommissionAmount= 256.22;
                obj.filterValue = 'test';
                obj.filterRecords();
            }
        }
    
    }

}