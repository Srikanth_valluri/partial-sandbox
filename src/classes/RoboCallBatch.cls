/*********************************************************************************************
 *==========================================================================================
 * Ver         Date                     Author                  Modification
 *==========================================================================================
 * 1.0      8/21/2019, 2:47:22 PM       Shashank Maind          Initial Version
 * 1.1      09/29/2019                  Aishwarya Todkar        Added check for Rental Pool    
********************************************************************************************/
global class RoboCallBatch implements Database.Batchable<sObject>
                                                    , Database.Stateful
                                                    , Database.AllowsCallouts {

    private String orderId;
    private Id campaignObjId;

    public RoboCallBatch(String strParam, Id str_campaignObjId) {
        orderId = strParam;
        campaignObjId = str_campaignObjId;
        System.debug('-->> orderId == ' + orderId );
        System.debug('-->> campaignObjId : ' + campaignObjId);
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('-->> Inside start');
        System.debug('label Amount: ' + Label.Robo_Call_FM_Outstanding_Amount);
        String FmOutstandingAmount_str = Label.Robo_Call_FM_Outstanding_Amount;
        if(!FmOutstandingAmount_str.endsWithIgnoreCase('.00')) {
            FmOutstandingAmount_str += '.00';
        }
        System.debug('FmOutstandingAmount_str :' + FmOutstandingAmount_str);

        Decimal FmOutstandingAmount =Decimal.valueOf(FmOutstandingAmount_str);
        System.debug('FmOutstandingAmount :' + FmOutstandingAmount);

        String strQuery = 'SELECT Id, FM_Outstanding_Amount__c, FM_Outstanding_Amount_Formula__c ,Booking__r.Account__c,'
                        + ' Booking__r.Account__r.Mobile__c, Booking__r.Account__r.Mobile_Phone_Encrypt__pc, '
                        + ' Booking__r.Account__r.IsPersonAccount, FM_Outstanding_Date__c ,Unit_Name__c, Owner__r.Phone, '
                        +' Property_Name__c, SC_From_RentalIncome__c,Unit_Active__c FROM Booking_Unit__c ' 
                        + ' WHERE FM_Outstanding_Amount_Formula__c >= '
                        + FmOutstandingAmount 
                        + ' AND Unit_Active__c = \'Active\' AND SC_From_RentalIncome__c = false AND '
                        + ' (CM_Units__c = NULL OR CM_Units__c = \' \') AND ByPassFMReminder__c = False AND (Handover_Flag__c != \'N\' '
                        + ' OR Early_Handover__c = TRUE)'
                        + ' AND ((Booking__r.Account__r.Mobile__c != null AND Booking__r.Account__r.Mobile__c != \'0\') '
                        + ' OR ( Booking__r.Account__r.Mobile_Phone_Encrypt__pc != null AND  Booking__r.Account__r.Mobile_Phone_Encrypt__pc != \'0\'))'
                        + ' AND Rental_Pool__c = false ';

        System.debug('-->> strQuery == ' + strQuery);
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<Booking_Unit__c > listBU){
        System.debug('-->> listBU: ' + listBU);
        System.debug('-->> listBU.size(): ' + listBU.size());
        String addCallString = '';
        List<Added_Call__c> listAdded_Call = new List<Added_Call__c>();
        String strMobileNumber = '';

        for(Booking_Unit__c objBooking_Unit: listBU){

            Added_Call__c objAdded_Call = new Added_Call__c();
            String inputDataStr = '';
            System.debug('-->> objBooking_Unit:'+ objBooking_Unit);

            System.debug('FM outstanding date1 : ' + objBooking_Unit.FM_Outstanding_Date__c);

            if(objBooking_Unit.FM_Outstanding_Date__c == null) {
                objBooking_Unit.FM_Outstanding_Date__c = Date.today();
            }

            System.debug('FM outstanding date2 : ' + objBooking_Unit.FM_Outstanding_Date__c);

            if((objBooking_Unit.Booking__r.Account__r.Mobile_Phone_Encrypt__pc != NULL || objBooking_Unit.Booking__r.Account__r.Mobile__c != NULL) &&  objBooking_Unit.FM_Outstanding_Date__c != NULL){
                System.debug('-->> objBooking_Unit.FM_Outstanding_Date__c:'+ objBooking_Unit.FM_Outstanding_Date__c);
                String dayString = objBooking_Unit.FM_Outstanding_Date__c.format();
                System.debug('-->> dayString:'+ dayString);

                //Check If person account
                if(objBooking_Unit.Booking__r.Account__r.IsPersonAccount == TRUE){
                    System.debug('-->> objBooking_Unit.Booking__r.Account__r.Mobile_Phone_Encrypt__pc: ' + objBooking_Unit.Booking__r.Account__r.Mobile_Phone_Encrypt__pc);
                    inputDataStr = objBooking_Unit.Booking__r.Account__r.Mobile_Phone_Encrypt__pc;
                    objAdded_Call.Phone__c = objBooking_Unit.Booking__r.Account__r.Mobile_Phone_Encrypt__pc;
                    strMobileNumber = objBooking_Unit.Booking__r.Account__r.Mobile_Phone_Encrypt__pc;
                } else {
                    inputDataStr = objBooking_Unit.Booking__r.Account__r.Mobile__c;
                    objAdded_Call.Phone__c =  objBooking_Unit.Booking__r.Account__r.Mobile__c;
                    strMobileNumber = objBooking_Unit.Booking__r.Account__r.Mobile__c;
                }

                inputDataStr += ',' +  objBooking_Unit.FM_Outstanding_Amount_Formula__c + ',' + objBooking_Unit.Property_Name__c+ ' ' + objBooking_Unit.Unit_Name__c + ',' +  dayString + ';';
                System.debug('-->> inputDataStr: ' + inputDataStr);
                addCallString += inputDataStr;

                //Create Added Call Object Record
                objAdded_Call.Account__c = objBooking_Unit.Booking__r.Account__c;
                objAdded_Call.Booking_Unit__c = objBooking_Unit.Id;
                objAdded_Call.Campaign_Robo__c = campaignObjId;
                objAdded_Call.Order_Id__c = orderId;
                objAdded_Call.Property_Name__c = objBooking_Unit.Property_Name__c;
                objAdded_Call.Unit_Name__c = objBooking_Unit.Unit_Name__c;
                objAdded_Call.FM_Outstanding_Amount__c = objBooking_Unit.FM_Outstanding_Amount_Formula__c;
                objAdded_Call.FM_Outstanding_Date__c = objBooking_Unit.FM_Outstanding_Date__c;
                objAdded_Call.Input_String_For_Add_Call__c = inputDataStr;
                listAdded_Call.add(objAdded_Call);
            }
        }   //END of for(Booking_Unit__c objBooking_Unit

        //Adding Kanu's number
        addCallString += '971505124682,1000.0, TestProject PRJ/1/1010,'+Date.today().format()+';';

        System.debug('-->> addCallString: ' + addCallString);
        String finaladdCallString = addCallString.removeEnd(';');
        System.debug('-->> finaladdCallString: ' + finaladdCallString);
        //if(orderId != NULL && finaladdCallString != ''){
        //    RoboCallAPIs.addCalls(orderId, finaladdCallString);
        //}
        //if(!listAdded_Call.isEmpty()){
        //    insert listAdded_Call;
        //}

        Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;
        System.debug('IsSandbox is: '+isSandbox);
        System.debug('-->> strMobileNumber: ' + strMobileNumber);

        if((isSandbox
                && (strMobileNumber.containsignorecase('8149071353')
                    || strMobileNumber.containsIgnoreCase('505124682')
                    || strMobileNumber.containsIgnoreCase('559270964')
                )
            ) || !isSandbox){

                if(orderId != NULL && finaladdCallString != ''){
                    //RoboCallAPIs.addCalls(orderId, finaladdCallString);
                }
                if(!listAdded_Call.isEmpty()){
                    System.debug('Inserting Added Calls');
                    insert listAdded_Call;
                }
            }

    }

    global void finish(Database.BatchableContext BC) {
        System.debug('-->> Finish ' );
    }
}