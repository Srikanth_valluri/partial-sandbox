public class ValidateEmailAndNumber {
    @Future (callout=TRUE)
    public static void validateData (Set <Id> pInqIdSet) {
        Map <String, Object> carrierMap = new Map <String, Object>();
        Map <String, Object> roamingMap = new Map <String, Object>();
        Map <String, Object> responseMap = new Map <String, Object>();
        Map <String, Object> emailResponseMap = new Map <String, Object>();
        
        Boolean isUpdate = false;
        List <Nexmo_ZeroBounce_Validity__c > inqToUpdate = new List <Nexmo_ZeroBounce_Validity__c> ();
        for (Nexmo_ZeroBounce_Validity__c inq :[SELECT Disposable__c, Inquiry__c, Mobile_Country_Code__c,
                                                Mobile_Phone__c, Ported__c, Reachable__c,
                                                Roaming__c, Toxic__c, Validity__c 
                                                FROM Nexmo_ZeroBounce_Validity__c
                                                WHERE Id IN :pInqIdSet
                                               ])
        {
            Nexmo_ZeroBounce_Validity__c inqRecord = new Nexmo_ZeroBounce_Validity__c ();
            inqRecord = inq;
            String mobileNumber = inq.Mobile_Phone__c;
            if (inq.Mobile_Country_Code__c != NULL)
                mobileNumber = inq.Mobile_Country_Code__c.split (': ')[1]+inq.Mobile_Phone__c;
            
            responseMap = phoneValidation.sendNEXMOCallout(mobileNumber);
            if (Test.isRunningTest()) {
                responseMap = new Map <String, Object> ();
                responseMap.put ('original_carrier', 'IN');
                roamingMap.put ('status', 'No');
            }
            if (responseMap != null && !responseMap.isEmpty()) {
                
                if (!Test.isRunningTest()) {
                    carrierMap = (Map<String, Object>) responseMap.get('original_carrier');
                    roamingMap = (Map<String, Object>) responseMap.get('roaming');
                }
                
                if (roamingMap != null
                    && roamingMap.containsKey('status') 
                    && roamingMap.get('status') != null) {
                    inqRecord.put('Roaming__c', roamingMap.get('status'));
                    isUpdate = true;
                }
                inqRecord.put('Validity__c', responseMap.get('valid_number'));
                inqRecord.put('Reachable__c', responseMap.get('reachable'));
                inqRecord.put('Ported__c', responseMap.get('ported'));
                isUpdate = true;
            }
            
            if (isUpdate == TRUE) {
                inqToUpdate.add (inqRecord);
            }
        }
        if (inqToUpdate.size () > 0)
            update inqToUpdate;
    
    }
    
    @Future (Callout=TRUE)
    public static void validateStandInquiryData (Set <Id> pInqIdSet) {
        Map <String, Object> carrierMap = new Map <String, Object>();
        Map <String, Object> roamingMap = new Map <String, Object>();
        Map <String, Object> responseMap = new Map <String, Object>();
        Map <String, Object> emailResponseMap = new Map <String, Object>();
        
        Boolean isUpdate = false;
        List <Stand_Inquiry__c> inqToUpdate = new List <Stand_Inquiry__c> ();
        for (Stand_Inquiry__c inq :[SELECT Mobile_CountryCode__c, Mobile_Country_Code_2__c, Mobile_Phone_Encrypt__c,
                                    Mobile_Phone_Encrypt_2__c, Email__c, ZB_Status__c, Nexmo_Status__c FROM Stand_Inquiry__c
                                    WHERE Id IN :pInqIdSet
                                   ])
        {
            Stand_Inquiry__c inqRecord = new Stand_Inquiry__c ();
            inqRecord = inq;
            String mobileNumber = inq.Mobile_Phone_Encrypt__c;
            if (inq.Mobile_CountryCode__c != NULL)
                mobileNumber = inq.Mobile_CountryCode__c.split (': ')[1]+inq.Mobile_Phone_Encrypt__c;
            
            responseMap = phoneValidation.sendNEXMOCallout(mobileNumber);
            System.Debug (responseMap);
            if (Test.isRunningTest()) {
                responseMap = new Map <String, Object> ();
                responseMap.put ('original_carrier', 'IN');
                roamingMap.put ('status', 'No');
            }
            if (responseMap != null && !responseMap.isEmpty()) {
                
                if (!Test.isRunningTest()) {
                    carrierMap = (Map<String, Object>) responseMap.get('original_carrier');
                    roamingMap = (Map<String, Object>) responseMap.get('roaming');
                }
                if ( carrierMap != null
                    && carrierMap.containsKey('network_type') 
                    && carrierMap.get('network_type') != null)
                {
                    inqRecord.put('Phone_Number_Type__c', carrierMap.get('network_type'));
                    isUpdate = true;
                }
                if ( carrierMap != null
                    && carrierMap.containsKey('country') 
                    && carrierMap.get('country') != null)
                {
                    inqRecord.put('Mobile_Country__c', carrierMap.get('country'));
                    isUpdate = true;
                }
                if (roamingMap != null
                    && roamingMap.containsKey('status') 
                    && roamingMap.get('status') != null) {
                    inqRecord.put('Roaming__c', roamingMap.get('status'));
                    isUpdate = true;
                }
                inqRecord.put('Validity__c', responseMap.get('valid_number'));
                inqRecord.put('Reachable__c', responseMap.get('reachable'));
                inqRecord.put('Ported__c', responseMap.get('ported'));

                inqRecord.put ('Nexmo_Status__c' , responseMap.get ('status_message'));
                isUpdate = true;
            } else {
                inqRecord.put ('Nexmo_Status__c' , 'Error');
            }
            emailResponseMap = emailValidation.sendZBCallout(inq.Email__c);
            System.Debug (emailResponseMap );
            if (emailResponseMap != null && !emailResponseMap.isEmpty()) {
                inqRecord.put ('BVEmailStatus__c', emailResponseMap.get('status'));
                inqRecord.put ('BVSecondaryStatus__c', emailResponseMap.get('sub_status'));
                inqRecord.put ('ZB_Status__c' ,'Success');
                isUpdate = true; 
                               
            } else {
                inqRecord.put ('ZB_Status__c' , 'Error');
            }
            if (isUpdate == TRUE) {
                inqToUpdate.add (inqRecord);
            }
        }
        if (inqToUpdate.size () > 0)
            update inqToUpdate;
    }
}