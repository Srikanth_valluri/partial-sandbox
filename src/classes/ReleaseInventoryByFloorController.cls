/****************************************************************************************************
* Name               : ReleaseInventoryByFloorController
* Description        : Controller class for ReleaseInventoryByFloor VF Page
* Created Date       : 28/10/2020
* Created By         : QBurst
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         28/10/2020      Initial Draft
****************************************************************************************************/
    
  global with sharing class ReleaseInventoryByFloorController {
    public List<String> propertyNames {get; set;}
    Map<String, Property__c> propertyMap;
    public static string floorDetails {get;set;}
    public static string invDetails {get;set;}
    public string fName {get; set;}
    public string selectedStatus {get; set;}
    public List<String> restrictReasons {get; set;}
    public string resReasons {get;set;}
    public string invId {get; set;}
    public map<Id, String> invCommentMap {get; set;}    
    Map<Id, Location__c> locationMap;
    public List<string> loc {get;set;}
    public Map<String, floorDetailsWrapper> floorWrapperMap{get; set;}
    public String invCommentMapJSON  {get; set;}
    //public String invCommentMapJSON {get{return JSON.serialize(invCommentMap);}}

    
    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public ReleaseInventoryByFloorController() {
        propertyNames = new List<String>();
        propertyMap = new  Map<String, Property__c>();
        invCommentMap = new map<Id, String>(); 
        List<Inventory__c> invList = new List<Inventory__c>();
        invDetails = JSON.serialize(invList);
        invCommentMapJSON = JSON.serialize(invCommentMap);
        loc = new List<string>();
        for(Property__c property: [SELECT Id, Property_Name__c, Property_ID__c, District__c 
                                   FROM Property__c 
                                   WHERE Property_Name__c != ''
                                   ORDER BY Property_Name__c ASC]){
            
            propertyNames.add(property.Property_Name__c);
            propertyMap.put(property.Property_Name__c, property);
            restrictReasons = new List<String> ();
            List<Inventory_Restrict_Codes__c> restrictReasonCS = Inventory_Restrict_Codes__c.getall().values();
            for(Inventory_Restrict_Codes__c reason: restrictReasonCS){
                restrictReasons.add( reason.Name);
            }
            resReasons = JSON.serialize(restrictReasons);
            }
    }
      
    /************************************************************************************************
    * @Description : method to fetch the Buildng Details.                                           *
    * @Params      : void                                                                           *
    * @Return      : void                                                                           *
    ************************************************************************************************/
    public void fetchBuildingDetails(){
        String projectName = ApexPages.currentPage().getParameters().get('projectName');
        Property__c propertyObj =  propertyMap.get(projectName);
        floorWrapperMap = new Map<String, floorDetailsWrapper>();
        locationMap = new Map<Id, Location__c>();
        floorDetails = ''; 
       // List<string> propertyName = new List<string>();
        system.debug('property: ' + propertyObj);
        
        for(Location__c loc: [SELECT Id, Name, Building_Name__c , Property_Name__c, Property_Name__r.Property_Name__c
                               FROM Location__c 
                               WHERE Location_Type__c = 'Building'
                               AND Property_Name__r.Property_Name__c =:propertyObj.Property_Name__c ]){
                locationMap.put(loc.id,loc);
            
         }
        system.debug('locationMap'+locationMap);
       
        fetchFloorDetails();
    }
    
    public void fetchfloorDetails(){
        // Map<String, Location__c> locationMap = new Map<String, Location__c>();
         Map<String, Integer> locationFloorCountMap = new Map<String, Integer>();
         Map<String, Integer> locationBuildingCountMap = new Map<String, Integer>();
         Map<String, List<Inventory__c>> locationInventoryMap = new Map<String, List<Inventory__c>>();
         List<floorDetailsWrapper> wrapperList = new List<floorDetailsWrapper>();
        
         for(Location__c loc: [SELECT Id, Name, Building_Name__c , Building_Number__c                                     
                               FROM Location__c 
                               WHERE Location_Type__c = 'Floor'
                               AND Building_Number__c IN: locationMap.keyset()]){
            system.debug(loc);
            floorDetailsWrapper wrapper = new floorDetailsWrapper();
            wrapper.floorName = loc.Name;
            wrapper.floorId = loc.Id;
            wrapper.type = 'Floor';
            floorWrapperMap.put(loc.id, wrapper);
         }
         system.debug('floorWrapperMap: '+ floorWrapperMap);
         for(Inventory__c inv: [SELECT Id,Floor__c,Floor_Location__c,Status__c, Building_Location__c,
                                Building_Location__r.Property_Name__r.Property_Name__c
                                FROM Inventory__c 
                                WHERE Floor_Location__c IN: floorWrapperMap.keyset()
                                ORDER BY Floor__c DESC]){
             List<Inventory__c> invList = new List<Inventory__c>();
             if(locationInventoryMap.containsKey(inv.Floor_Location__c)){
                 invList = locationInventoryMap.get(inv.Floor_Location__c);
             }
             invList.add(inv);
             locationInventoryMap.put(inv.Floor_Location__c, invList);    
         }
         system.debug('locationInventoryMap: '+ locationInventoryMap);
         for(String floorName: floorWrapperMap.keyset()){
             floorDetailsWrapper wrapper = floorWrapperMap.get(floorName);
             Integer units = 0;
             Integer inventory = 0;
             Integer available = 0;
             Integer sold = 0;
             Integer restricted = 0;
             Integer floors = 0;
             Integer buildings = 0;
             Boolean payPlan = false;
             if(locationInventoryMap.containsKey(floorName)){
                 for(Inventory__c inv: locationInventoryMap.get(floorName)){
                     units++;
                     if(inv.Status__c == 'AVAILABLE' || inv.Status__c == 'Released'){
                         available++;
                     } else if(inv.Status__c == 'Inventory'){
                         inventory++;
                     } else if(inv.Status__c == 'Sold'){
                         sold++;
                     } else if(inv.Status__c == 'Restricted'){
                         restricted++;
                     }
                 }
             }
            // wrapper.buildings = buildings;
             //wrapper.floors = floors;
             wrapper.units = units;
             wrapper.available = available;
             wrapper.inventory = inventory;
             wrapper.sold = sold;
             wrapper.restricted = restricted;
             wrapperList.add(wrapper);
         }  
         invCommentMapJSON = JSON.serialize(invCommentMap); 
         floorDetails = JSON.serialize(wrapperList);
         system.debug('floorDetails: ' + floorDetails);
    }
    
    
    
       
    public List<SelectOption> getRestrictReasonOptions() {
        List<Inventory_Restrict_Codes__c> restrictReasons = Inventory_Restrict_Codes__c.getall().values();
        List<SelectOption> restrictReasonOptions = new List<SelectOption>();
        for(Inventory_Restrict_Codes__c reason: restrictReasons){
            restrictReasonOptions.add(new SelectOption(reason.Name, reason.Name));
        }
        system.debug('restrictReasonOptions: ' + restrictReasonOptions);
        return restrictReasonOptions;
    }
    
    public void updateComments(){
        String invId = ApexPages.currentPage().getParameters().get('unitInvId');
        String comments = ApexPages.currentPage().getParameters().get('invComments');
        system.debug('comments'+comments);
        system.debug('invId'+invId);
        List<string> invIdList = new List<string>();
        invIdList= invId.split(',');
        for(string id : invIdList){
            invCommentMap.put(id, comments);            
        }
        
        system.debug('invCommentMap: ' + invCommentMap);        
        invCommentMapJSON = JSON.serialize(invCommentMap); 
    }
    
    
    public void fetchInventoryDetails(){
        String flrName = ApexPages.currentPage().getParameters().get('flName');
        String slStatus = ApexPages.currentPage().getParameters().get('status');
        system.debug('floorName: ' + flrName);
        system.debug('slStatus: ' + slStatus);
       
        string buildingId='';
        List<Inventory__c> invList = new List<Inventory__c>();
        invCommentMap = new map<Id, String>();
         if(flrName != null && flrName != ''){
            fName = flrName;
           for(Location__c location: [SELECT Id FROM Location__c 
                                           WHERE  Location_Code__c =: fName.trim()
                                           AND Location_Type__c  =: 'Floor']){
                    loc.add(location.id);
                                               
            }
         }
        system.debug('loc'+loc);
        if(slStatus != null && slStatus != ''){             
            selectedStatus = slStatus;
            system.debug('selectedStatus'+selectedStatus);
            if(selectedStatus.trim() == 'Available'){
                for(Inventory__c inv: [SELECT Id, Status__c, Building_Location__c,Floor_Location__c,
                                     Bedrooms__c,  ACD_Date__c, Selling_Price__c,
                                     Restriction_Remarks__c, Restriction_Category__c,
                                     Comments__c, Unit_Name__c,IPMS_Bedrooms__c,
                                     Building_Location__r.Property_Name__r.Property_Name__c
                                FROM Inventory__c 
                                WHERE Unit_Name__c != null AND Floor_Location__c IN: loc
                                AND (Status__c =: selectedStatus OR Status__c ='Released')]){
                    invList.add(inv);
                    invCommentMap.put(inv.Id, '');
                 }
                                 
              } else if(selectedStatus.trim() == 'None'){
                    system.debug('floorWrapperMap: ' + floorWrapperMap);
                    system.debug('in else');
                     for(Inventory__c inv: [SELECT Id, Status__c, Building_Location__c,
                                         Bedrooms__c,  ACD_Date__c, Selling_Price__c,
                                         Restriction_Remarks__c, Restriction_Category__c,
                                         Comments__c, Unit_Name__c,IPMS_Bedrooms__c,
                                   Building_Location__r.Property_Name__r.Property_Name__c,Floor_Location__c
                                   FROM Inventory__c 
                                   WHERE Unit_Name__c != null AND Floor_Location__c IN:loc  
                                   AND (Status__c = 'Inventory' OR Status__c = 'Restricted' OR Status__c = 'Released')
                                   ORDER BY Floor_Location__c DESC]){
                 
                                     
                    invList.add(inv);
                    invCommentMap.put(inv.Id, '');  
                        
                }
            } else{
                for(Inventory__c inv: [SELECT Id, Status__c, Building_Location__c,
                                         Bedrooms__c,  ACD_Date__c, Selling_Price__c,Floor_Location__c,
                                         Restriction_Remarks__c, Restriction_Category__c,
                                         Comments__c, Unit_Name__c,IPMS_Bedrooms__c,
                                         Building_Location__r.Property_Name__r.Property_Name__c
                                    FROM Inventory__c 
                                    WHERE Unit_Name__c != null AND Floor_Location__c IN: loc
                                    AND Status__c =: selectedStatus]){
                    invList.add(inv);
                    invCommentMap.put(inv.Id, ''); 
                }
            }
        }else{
           for(Inventory__c inv: [SELECT Id, Status__c, Building_Location__c,
                                         Bedrooms__c,  ACD_Date__c, Selling_Price__c,
                                         Restriction_Remarks__c, Restriction_Category__c,
                                         Comments__c, Unit_Name__c,IPMS_Bedrooms__c,
                                   Building_Location__r.Property_Name__r.Property_Name__c,Floor_Location__c
                                   FROM Inventory__c 
                                   WHERE Unit_Name__c != null AND Floor_Location__c IN:loc  
                                   AND (Status__c = 'Inventory' OR Status__c = 'Restricted' OR Status__c = 'Released')
                                   ORDER BY Floor_Location__c DESC]){
                 
                                     
                    invList.add(inv);
                                       invCommentMap.put(inv.Id, '');  } 
        }
        invDetails = JSON.serialize(invList);
        system.debug('invDetails: ' + invDetails);
    }
    
    
    public void updateInventory(){
        String updDetails = ApexPages.currentPage().getParameters().get('updateDetails');
        system.debug('updateDetails: ' + updDetails);
        List<InvUpdateWrapper> deserializeResults = (List<InvUpdateWrapper>)JSON.deserialize(updDetails, List<InvUpdateWrapper>.class);
        system.debug('deserializeResults: ' + deserializeResults);
        List<Inventory__c> invList = new List<Inventory__c>();
        Map<Id, Inventory__c> invMap = new Map<Id, Inventory__c>();
        for(InvUpdateWrapper wrapper: deserializeResults){
            invMap.put(wrapper.unitId, null);
        }
        for(Inventory__c inv: [SELECT Id, Status__c, Restriction_Remarks__c, Restriction_Category__c,Comments__c
                                FROM Inventory__c WHERE Id IN: invMap.keyset()]){  
            invMap.put(inv.Id, inv);  
        }
        system.debug('invMap: ' + invMap);
        for(InvUpdateWrapper wrapper: deserializeResults){
            Inventory__c inv = invMap.get(wrapper.unitId);
            String status = wrapper.status;
            if(status == 'released'){
                status = 'Released';
            } else if(status == 'restricted'){
                status = 'Restricted';
                if(wrapper.restricted_reason != 'None'){
                    inv.Restriction_Category__c = wrapper.restricted_reason;
                    inv.Restriction_Remarks__c = invCommentMap.get(inv.Id);
                }
            } else if(status == 'inventory'){
                status = 'Inventory';
            }
            inv.Status__c = status;
            system.debug('invCommentMap: ' + invCommentMap);
            String comment = inv.Comments__c;
            if(comment != null && comment != ''){
                comment += '\n';
            }
            comment += invCommentMap.get(inv.Id);
            inv.Comments__c = comment; 
            invList.add(inv);
        }
        system.debug('invList: ' + invList);
        if(invList.size() > 0){
            update invList;
        }
        fetchInventoryDetails();
        fetchfloorDetails();
    }
    
    public class InvUpdateWrapper{
        public String unitId;
        public string status;
        public string restricted_reason;
        public invUpdateWrapper(){}
    }
    
     public class floorDetailsWrapper{
        public string floorName;
        public string type;
        public string floorId;
        public string city;
        public Integer buildings;
        public Integer floors;
        public Integer units;
        public Integer inventory;
        public Integer available;
        public Integer sold;
        public Attachment attach;
        public Integer restricted;
        public floorDetailsWrapper(){
        }
        public floorDetailsWrapper(String flName, String flId, String cityName,
                                Integer buildingCount, Integer floorsCount, Integer unitsCount, 
                                Integer inventoryCount, Integer availableCount, 
                                Integer soldCount, Integer restrictedCount){
            floorName = flName;
            floorId = flId;
            city = cityName;
            buildings = buildingCount;
            floors = floorsCount;
            units = unitsCount;
            inventory = inventoryCount;
            available = availableCount;
            sold = soldCount;
            restricted = restrictedCount;
            
      
        }
        
    
}

}