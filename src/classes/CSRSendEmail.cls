public class CSRSendEmail  {

    public CSRSendEmail(ApexPages.StandardController controller) {

    }



    @InvocableMethod public static void SendMailCSR(List<Id> lstCaseId){
        if( lstCaseId != null && lstCaseId.size() > 0 ) {
            for (Id CaseId : lstCaseId) {
                sendEmail(CaseId);
            }

        }
    }

    public pagereference  init(){
        string strPageID = ApexPages.currentPage().getParameters().get('id');
        string strEmail ='';
        if( strPageID != NULL && strPageID != '' ){
            Case objCase = [SELECT Id,AccountId,Approving_User_Id__c,
                                        Case_Summary__c,CaseNumber,
                                        Approving_User_Role__c,
                                        Approving_User_Name__c
                                        From Case 
                        WHERE Id =: strPageID LIMIT 1];
            if(objCase != NULL && objCase.Approving_User_Id__c != NULL ){
                user objuser = [SELECT  Email
                            FROM User 
                            WHERE Id =: objCase.Approving_User_Id__c 
                            LIMIT 1];
                            strEmail = objuser.Email;
            }
            

            sendEmail( (Id) strPageID);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm, 'Email Send Sucessfully to ' + strEmail ));
            
        }
        return null;
    }
    

    
    
    @Future(callout=true)
    public static void sendEmail(Id CaseId) {

        List<CSRApprovalController.PropertyDetails> lstPropertyDetails = new List<CSRApprovalController.PropertyDetails>();

        Map<String,List<String>> mapheaderValue = new Map<String,List<String>>();
        List<String> lstMapHeadervaluesKeySet = new List<String>();
        
        List<CSRApprovalController.Request> lstRequest  = new List<CSRApprovalController.Request>();


        List<Attachment> lstAttach = new List<Attachment>();
        List<EmailMessage> lstEmails = new List<EmailMessage>();
        CSRApprovalController.CustomerDetails objCustomerDetails = new CSRApprovalController.CustomerDetails();
        

                   
        CSRApprovalController.caseIdStatic = caseId;
        CSRApprovalController.blnSkip = true;
        CSRApprovalController objCSRApprovalController = new CSRApprovalController();
        
        // System.debug(json.serializepretty(objCSRApprovalController.lstPropertyDetails));
        // System.debug(json.serializepretty(objCSRApprovalController.lstRequest));
        
        objCustomerDetails = objCSRApprovalController.objCustomerDetails;
        //lstPropertyDetails = objCSRApprovalController.lstPropertyDetails;
        mapheaderValue = objCSRApprovalController.mapheaderValue;
        lstMapHeadervaluesKeySet = objCSRApprovalController.lstMapHeadervaluesKeySet;
        lstRequest = objCSRApprovalController.lstRequest;
        
        System.debug('lstRequest==='+lstRequest);
        System.debug('lstRequest==='+objCustomerDetails);
        System.debug('lstMapHeadervaluesKeySet==='+lstMapHeadervaluesKeySet);

                   
       
        Case objCase = [SELECT Id,AccountId,Approving_User_Id__c,Approving_User_Role__c,Case_Summary__c,Approving_User_Name__c,CaseNumber,Client_s_Issue__c
                                 From Case WHERE Id =: caseId LIMIT 1];
                                    system.debug('Approving_User_Role:::'+objCase.Approving_User_Role__c);
        if(!objCase.Approving_User_Role__c.containsIgnoreCase('CRE') &&
           !objCase.Approving_User_Role__c.containsIgnoreCase('Review Team') ){                        
        EmailTemplate emailTemplateObj = [SELECT Id
                                                , Subject
                                                , Body
                                                , Name
                                                , HtmlValue
                                                , TemplateType
                                                , BrandTemplateId
                                                FROM EmailTemplate 
                                                WHERE Name = 'Case Summary Email' LIMIT 1 ];
                                                
        // TODO Added to have attachment related to email template
        Attachment[] attachmentLst = [ SELECT Id,ContentType,Name, Body 
                     From Attachment where parentId = : emailTemplateObj.Id];
        lstAttach.addAll(  attachmentLst );

        String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';
        String subject, strAccountId, strDPIId, bccAddress ='', strCCAddress;
        
        strAccountId = objCase.AccountId;
        user objuser = [SELECT  Email
                        FROM User 
                        WHERE Id =: objCase.Approving_User_Id__c 
                        LIMIT 1];

        

        toAddress =Test.isRunningTest()? 'arjun.khatri@eternussolutions.com' : objuser.Email;//'arjun.khatri00231@gmail.com';//kanujulka@gmail.com'ankitashinde732@gmail.com';//objuser.Email;
        System.debug('toAddress = ' + toAddress);

        fromAddress = 'caseapprovals@damacgroup.com'; //'no-replysf@damacgroup.com';
        bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
        contentType = 'text/html';
        strCCAddress = '';
        system.debug(' emailTemplateObj : '+ emailTemplateObj );
        if( toAddress != '' && emailTemplateObj != null ) {
            
        if(emailTemplateObj.body != NULL){
            contentBody =  emailTemplateObj.body;
        }
        if(emailTemplateObj.htmlValue != NULL){
            contentValue = emailTemplateObj.htmlValue;
        }
        if(string.isblank(contentValue)){
            contentValue='No HTML Body';
        }


        // replace {!ClientIssue}
        contentValue = String.isNotBlank(objCase.Client_s_Issue__c)?contentValue.replace('{!ClientIssue}',
                                     objCase.Client_s_Issue__c ):contentValue.replace('{!ClientIssue}',''); 
        contentBody = String.isNotBlank(objCase.Client_s_Issue__c)?contentBody.replace('{!ClientIssue}', 
                                     objCase.Client_s_Issue__c ):contentBody.replace('{!ClientIssue}',''); 
        
        
        
        // replace {!CaseDetails}
        contentValue = String.isNotBlank(objCase.Case_Summary__c)?contentValue.replace('{!CaseDetails}',
                                     objCase.Case_Summary__c ):contentValue.replace('{!CaseDetails}',''); 
        contentBody = String.isNotBlank(objCase.Case_Summary__c)?contentBody.replace('{!CaseDetails}', 
                                     objCase.Case_Summary__c ):contentBody.replace('{!CaseDetails}',''); 
                                     
                                     

                                     
        // replace {!Customer}
        contentValue = String.isNotBlank( objCustomerDetails.CustomerName )?contentValue.replace('{!Customer}',
                                objCustomerDetails.CustomerName ):contentValue.replace('{!Customer}',''); 
        
        contentBody = String.isNotBlank( objCustomerDetails.CustomerName )?contentBody.replace('{!Customer}', 
                                        objCustomerDetails.CustomerName ):contentBody.replace('{!Customer}',''); 

        // replace {!CaseNumber}
        contentValue = String.isNotBlank( objCustomerDetails.strCaseNumber )?contentValue.replace('{!CaseNumber}',
                                objCustomerDetails.strCaseNumber  ):contentValue.replace('{!CaseNumber}',''); 
        
        contentBody = String.isNotBlank( objCustomerDetails.strCaseNumber )?contentBody.replace('{!CaseNumber}', 
                                        objCustomerDetails.strCaseNumber ):contentBody.replace('{!CaseNumber}','');
        // replace {!Executive}
        contentValue = String.isNotBlank( objCustomerDetails.strExecutive )?contentValue.replace('{!Executive}',
                                objCustomerDetails.strExecutive  ):contentValue.replace('{!Executive}',''); 
        
        contentBody = String.isNotBlank( objCustomerDetails.strExecutive )?contentBody.replace('{!Executive}', 
                                        objCustomerDetails.strExecutive ):contentBody.replace('{!Executive}','');                                       
                                        
        // replace {!Team}
        contentValue = String.isNotBlank( objCustomerDetails.team )?contentValue.replace('{!Team}',
                                objCustomerDetails.team  ):contentValue.replace('{!Team}',''); 
        
        contentBody = String.isNotBlank( objCustomerDetails.team )?contentBody.replace('{!Team}', 
                                        objCustomerDetails.team ):contentBody.replace('{!Team}',''); 

        // replace {!TeamLdr}
        contentValue = String.isNotBlank( objCustomerDetails.teamLeader )?contentValue.replace('{!TeamLdr}',
                                objCustomerDetails.teamLeader  ):contentValue.replace('{!TeamLdr}',''); 
        
        contentBody = String.isNotBlank( objCustomerDetails.teamLeader )?contentBody.replace('{!TeamLdr}', 
                                        objCustomerDetails.teamLeader ):contentBody.replace('{!TeamLdr}','');                                       
        
        // replace {!noOfActiveUnit}
        contentValue = String.isNotBlank( objCustomerDetails.noOfAcitveUnit )?contentValue.replace('{!noOfActiveUnit}',
                                objCustomerDetails.noOfAcitveUnit  ):contentValue.replace('{!noOfActiveUnit}',''); 
        
        contentBody = String.isNotBlank( objCustomerDetails.noOfAcitveUnit )?contentBody.replace('{!noOfActiveUnit}', 
                                        objCustomerDetails.noOfAcitveUnit ):contentBody.replace('{!noOfActiveUnit}','');                                       
        
        // replace {!portfolioValue}
        contentValue = String.isNotBlank( objCustomerDetails.portfolioValue )?contentValue.replace('{!portfolioValue}',
                                objCustomerDetails.portfolioValue  ):contentValue.replace('{!portfolioValue}',''); 
        
        contentBody = String.isNotBlank( objCustomerDetails.portfolioValue )?contentBody.replace('{!TeamLdr}', 
                                        objCustomerDetails.portfolioValue ):contentBody.replace('{!portfolioValue}','');                                       
        
        
        // replace {!AllApprpve}
        //contentValue = String.isNotBlank( objCase.Approving_User_Role__c ) && String.isNotBlank( objCase.Approving_User_Name__c )  ?contentValue.replace('{!AllApprpve}', 'https://partial-damacproperties.cs80.force.com/Documents/CSRApproveAll?caseId=5002500000Bg6xMAAR&Approver='+objCase.Approving_User_Role__c+'&Name='+objCase.Approving_User_Name__c ):contentValue.replace('{!AllApprpve}',''); 
        
        //contentBody = String.isNotBlank( objCase.Approving_User_Role__c ) && String.isNotBlank( objCase.Approving_User_Name__c )  ?contentValue.replace('{!AllApprpve}', 'https://partial-damacproperties.cs80.force.com/Documents/CSRApproveAll?caseId=5002500000Bg6xMAAR&Approver='+objCase.Approving_User_Role__c+'&Name='+objCase.Approving_User_Name__c ):contentBody.replace('{!AllApprpve}',''); 

        contentValue = String.isNotBlank( objCase.Approving_User_Role__c ) && String.isNotBlank( objCase.Approving_User_Name__c )  ?contentValue.replace('{!AllApprpve}', System.Label.CSRBaseURL+'/CSRApprovalPage?caseId='+objCase.Id+'&retURL=/'+objCase.Id ):contentValue.replace('{!ApproveWithChange}',''); 
        
        contentBody = String.isNotBlank( objCase.Approving_User_Role__c ) && String.isNotBlank( objCase.Approving_User_Name__c )  ?contentValue.replace('{!AllApprpve}', System.Label.CSRBaseURL+'/CSRApprovalPage?caseId='+objCase.Id+'&retURL=/'+objCase.Id):contentBody.replace('{!ApproveWithChange}',''); 



        // replace {!AllReject}
        //contentValue = String.isNotBlank( objCase.Approving_User_Role__c ) && String.isNotBlank( objCase.Approving_User_Name__c )  ?contentValue.replace('{!AllReject}', 'https://partial-damacproperties.cs80.force.com/Documents/CSRRejectAll?caseId=5002500000Bg6xMAAR&Approver='+objCase.Approving_User_Role__c+'&Name='+objCase.Approving_User_Name__c ):contentValue.replace('{!AllApprpve}',''); 
        
        //contentBody = String.isNotBlank( objCase.Approving_User_Role__c ) && String.isNotBlank( objCase.Approving_User_Name__c )  ?contentValue.replace('{!AllReject}', 'https://partial-damacproperties.cs80.force.com/Documents/CSRRejectAll?caseId=5002500000Bg6xMAAR&Approver='+objCase.Approving_User_Role__c+'&Name='+objCase.Approving_User_Name__c  ):contentBody.replace('{!AllApprpve}',''); 
        
        contentValue = String.isNotBlank( objCase.Approving_User_Role__c ) && String.isNotBlank( objCase.Approving_User_Name__c )  ?contentValue.replace('{!AllReject}', System.Label.CSRBaseURL+'/CSRApprovalPage?caseId='+objCase.Id+'&retURL=/'+objCase.Id):contentValue.replace('{!ApproveWithChange}',''); 
        
        contentBody = String.isNotBlank( objCase.Approving_User_Role__c ) && String.isNotBlank( objCase.Approving_User_Name__c )  ?contentValue.replace('{!AllReject}', System.Label.CSRBaseURL+'/CSRApprovalPage?caseId='+objCase.Id+'&retURL=/'+objCase.Id):contentBody.replace('{!ApproveWithChange}',''); 
        
        // replace {!ApproveWithChange}
        contentValue = String.isNotBlank( objCase.Approving_User_Role__c ) && String.isNotBlank( objCase.Approving_User_Name__c )  ?contentValue.replace('{!ApproveWithChange}', System.Label.CSRBaseURL+'/CSRApprovalPage?caseId='+objCase.Id+'&retURL=/'+objCase.Id):contentValue.replace('{!ApproveWithChange}',''); 
        
        contentBody = String.isNotBlank( objCase.Approving_User_Role__c ) && String.isNotBlank( objCase.Approving_User_Name__c )  ?contentValue.replace('{!ApproveWithChange}', System.Label.CSRBaseURL+'/CSRApprovalPage?caseId='+objCase.Id+'&retURL=/'+objCase.Id):contentBody.replace('{!ApproveWithChange}',''); 
        
        // replace {!NeedMoreInfo}
        contentValue = String.isNotBlank( objCase.Approving_User_Role__c ) && String.isNotBlank( objCase.Approving_User_Name__c )  ?contentValue.replace('{!NeedMoreInfo}', System.Label.CSRBaseURL+'/CSRApprovalPage?caseId='+objCase.Id+'&retURL=/'+objCase.Id):contentValue.replace('{!NeedMoreInfo}',''); 
        
        contentBody = String.isNotBlank( objCase.Approving_User_Role__c ) && String.isNotBlank( objCase.Approving_User_Name__c )  ?contentValue.replace('{!NeedMoreInfo}', System.Label.CSRBaseURL+'/CSRApprovalPage?caseId='+objCase.Id+'&retURL=/'+objCase.Id):contentBody.replace('{!NeedMoreInfo}',''); 
        
        
        //REplace  {!unitDetails}
        String unitDetailsHtml = ''; 
        for (String unit : objCustomerDetails.UnitNumbers) {
            unitDetailsHtml += '<Span>'+unit+'</Span><br />';
        }
        contentValue = String.isNotBlank( unitDetailsHtml )?contentValue.replace('{!unitDetails}',
                                        unitDetailsHtml ):contentValue.replace('{!unitDetails}',''); 
        
        contentBody = String.isNotBlank( unitDetailsHtml )?contentBody.replace('{!unitDetails}', 
                                            unitDetailsHtml ):contentBody.replace('{!unitDetails}','');

        //Repalce {!TypeofRequests}
        String RequestsHtml = ''; 
        for (String req : objCustomerDetails.TypeofRequests) {
            RequestsHtml += '<Span>'+req+'</Span><br />';
        }

        contentValue = String.isNotBlank( RequestsHtml )?contentValue.replace('{!TypeofRequests}',
                                        RequestsHtml ):contentValue.replace('{!TypeofRequests}',''); 
        
        contentBody = String.isNotBlank( RequestsHtml )?contentBody.replace('{!TypeofRequests}', 
                                            RequestsHtml ):contentBody.replace('{!TypeofRequests}','');

        //Replace {!DateofRequest}
        contentValue = String.isNotBlank( objCustomerDetails.DateofRequest )?contentValue.replace('{!DateofRequest}',
                                objCustomerDetails.DateofRequest ):contentValue.replace('{!DateofRequest}',''); 
        
        contentBody = String.isNotBlank( objCustomerDetails.DateofRequest )?contentBody.replace('{!DateofRequest}', 
                                        objCustomerDetails.DateofRequest ):contentBody.replace('{!DateofRequest}','');

        //Repalce {!lstPropertyDetails}
        String lstPropertyDetailsHTML = ''; 
        
        for (String objPropertyDetails : lstMapHeadervaluesKeySet) {
            if( String.isNotBlank(objPropertyDetails) 
                       && !objPropertyDetails.equalsIgnoreCase('Statement of Account')
                       && !objPropertyDetails.equalsIgnoreCase('Payment Plan') 
                       ) {            
                lstPropertyDetailsHTML += '<tr>' +
                                            '<th>' + objPropertyDetails + '</th>';

                if( String.isNotBlank(objPropertyDetails) && mapheaderValue.containskey(objPropertyDetails) ) {
                    //system.debug( ' mapKeyListString.get()  : ' + mapKeyListString.get('Unit Number'));
                    for(String objResultBU : mapheaderValue.get(objPropertyDetails)) {
                          lstPropertyDetailsHTML += '<td>';
                          if( String.isNotBlank(objResultBU) 
                           && !objPropertyDetails.equalsIgnoreCase('Statement of Account')
                           && !objPropertyDetails.equalsIgnoreCase('Payment Plan')
                           ) {
                              lstPropertyDetailsHTML += objResultBU;
                          }
                          lstPropertyDetailsHTML += '</td>';
                    }                       
                }
                lstPropertyDetailsHTML += '</tr>';
            }
            
        
        }

        contentValue = String.isNotBlank( lstPropertyDetailsHTML )?contentValue.replace('{!lstPropertyDetails}',
             lstPropertyDetailsHTML ):contentValue.replace('{!lstPropertyDetails}',''); 
        
        contentBody = String.isNotBlank( lstPropertyDetailsHTML )?contentBody.replace('{!lstPropertyDetails}', 
            lstPropertyDetailsHTML ):contentBody.replace('{!lstPropertyDetails}','');


        // Replace Request Deails 
        String strRequestDeailsHtml = '';
        for (CSRApprovalController.Request lstRequestVar : lstRequest) {
            String strMasterHtml = '';
            strMasterHtml += '<tr style="height: 10px; background-color: #666666;"></tr><tr>';

            String strRequestType = '<tr><td colspan = "3"><b>'+lstRequestVar.objCSR.RequestType__c +' - '+ lstRequestVar.objCSR.Booking_Unit__r.Unit_Name__c+'</b></td></tr>';
            strMasterHtml += strRequestType;
            String strCommiteeComments = '';
            String strHODComments = '' ;
            String strDirectorComments = ''; 
            String strManagerComments  = ''; 
            String strCREComments  = '';
            String masterComments = '' ;
            if(String.isNotBlank(lstRequestVar.objCSR.Committee_Comments__c)){
                String strComments ='';
                for (String strCom : lstRequestVar.objCSR.Committee_Comments__c.split(',')) {
                    strComments +=  '<li>'+strCom+'</li>';
                }
                strCommiteeComments +='<h5>'+ lstRequestVar.objCSR.Committee_Name__c +'  Comments - </h5>'+
                                    '<ul>'+
                                        strComments+
                                    '</ul>'+
                                '<br />' ;
            }
            System.debug('strCommiteeComments'+strCommiteeComments);
            if(String.isNotBlank(lstRequestVar.objCSR.HOD_Comments__c)){
                String strComments ='';
                for (String strCom : lstRequestVar.objCSR.HOD_Comments__c.split(',')) {
                    strComments +=  '<li>'+strCom+'</li>';
                }
                strHODComments += '<h5>'+ lstRequestVar.objCSR.HOD_Name__c +'  Comments - </h5>'+
                                    '<ul>'+
                                        strComments+
                                    '</ul>'+
                                '<br />';

            }
            System.debug('strHODComments'+strHODComments);
            if(String.isNotBlank(lstRequestVar.objCSR.Director_Comments__c)){
                String strComments ='';
                for (String strCom : lstRequestVar.objCSR.Director_Comments__c.split(',')) {
                    strComments +=  '<li>'+strCom+'</li>';
                }
                strDirectorComments += '<h5>'+ lstRequestVar.objCSR.Director_Name__c +'  Comments - </h5>'+
                                        '<ul>'+
                                        strComments+
                                        '</ul>'+
                                    '<br />' ;
            }
            System.debug('strDirectorComments'+strDirectorComments);
            if(String.isNotBlank(lstRequestVar.objCSR.Manager_Comments__c)){
                String strComments ='';
                for (String strCom : lstRequestVar.objCSR.Manager_Comments__c.split(',')) {
                    strComments +=  '<li>'+strCom+'</li>';
                }
                strManagerComments +='<h5>'+ lstRequestVar.objCSR.Manager_Name__c +'  Comments - </h5>'+
                            '<ul>'+
                            strComments+
                            '</ul>'+
                        '<br />' ;   
            }
            System.debug('strManagerComments'+strManagerComments);

            
            if(String.isNotBlank(lstRequestVar.objCSR.CRE_Comments__c)){
                String strComments ='';
                for (String strCom : lstRequestVar.objCSR.CRE_Comments__c.split(',')) {
                    strComments +=  '<li>'+strCom+'</li>';
                }
                strCREComments = '<h5>'+ objCSRApprovalController.CREName +'  Comments - </h5>'+
                                    '<ul>'+
                                    strComments+
                                    '</ul>' ;
            }
            System.debug('strManagerComments'+strCREComments);
           
            

            masterComments = '<td>'+strCommiteeComments+strHODComments+strDirectorComments+strManagerComments+strCREComments+'</td>';
            strMasterHtml += masterComments;

            String strCommiteeReqDetails,strHODReqDetails,strDirectorReqDetails,strManagerReqDetails,strCREReqDetails = '';
            String masterReqDetails = '';
                String strHODReq = '';
                If(lstRequestVar.lstHODRequestDetails.size() > 0){
                    
                    for (CSRApprovalController.RequestDetails lstHODRequest: lstRequestVar.lstHODRequestDetails) {
                        /*strHODReq += '<div style="margin: 5px 0; display: inline-block; width: 46%;">'+
                         '<p>'+lstHODRequest.fieldName+'</p>'+
                         '<div class="comment p-3"> '+lstHODRequest.fieldValue+'</div>'+
                        '</div>';*/
                        strHODReq += '<p class="para-history"><b>'+lstHODRequest.fieldName+'</b> changed to '+
                         '<span class="blue-text"><b>'+lstHODRequest.fieldValue+'</b></span> by <span class="blue-text">'+lstRequestVar.objCSR.HOD_Name__c+
                        '</span></p>';
                    }
                    /*strHODReqDetails =  '<td>' +
                                            '<div class="comments-view">' +
                                                '<h4>'+lstRequestVar.objCSR.HOD_Name__c+ 'Modification history -</h4>'+
                                                strHODReq+
                                            '</div>'+
                                        '</td><br />';*/
                    strHODReqDetails =  strHODReq;
                }else{
                    strHODReqDetails = '';
                }


                String strDirectorReq = '';
                If(lstRequestVar.lstDirectorRequestDetails.size() > 0){
                    for (CSRApprovalController.RequestDetails lstDirecRequest: lstRequestVar.lstDirectorRequestDetails) {
                        /*strDirectorReq += '<div style="margin: 5px 0; display: inline-block; width: 46%;">'+
                         '<p>'+lstDirecRequest.fieldName+'</p>'+
                         '<div class="comment p-3">'+lstDirecRequest.fieldValue+'</div>'+
                        '</div>';*/
                        strDirectorReq += '<p class="para-history"><b>'+lstDirecRequest.fieldName+'</b> changed to '+
                         '<span class="blue-text"><b>'+lstDirecRequest.fieldValue+'</b></span> by <span class="blue-text">'+lstRequestVar.objCSR.Director_Name__c+
                        '</span></p>';  
                    }
                    /*strDirectorReqDetails =  '<td>' +
                                            '<div class="comments-view">' +
                                                '<h4>'+lstRequestVar.objCSR.Director_Name__c+' Modification history -</h4>'+
                                                strDirectorReq+
                                            '</div>'+
                                        '</td><br />';*/
                    strDirectorReqDetails = strDirectorReq;                                        

                }else{
                    strDirectorReqDetails = '';
                }

                String strManagerReq = '';
                If(lstRequestVar.lstManagerRequestDetails.size() > 0){
                    for (CSRApprovalController.RequestDetails lstManagerRequest: lstRequestVar.lstManagerRequestDetails) {
                        /*strManagerReq += '<div style="margin: 5px 0; display: inline-block; width: 46%;">'+
                         '<p>'+lstManagerRequest.fieldName+'</p>'+
                         '<div class="comment p-3">'+lstManagerRequest.fieldValue+'</div>'+
                        '</div>';*/
                        strManagerReq += '<p class="para-history"><b>'+lstManagerRequest.fieldName+'</b> changed to '+
                         '<span class="blue-text"><b>'+lstManagerRequest.fieldValue+'</b></span> by <span class="blue-text">'+lstRequestVar.objCSR.Manager_Name__c+
                        '</span></p>';                      
                        
                    }
                    /*strManagerReqDetails =  '<td>' +
                                            '<div class="comments-view">' +
                                                '<h4>'+lstRequestVar.objCSR.Manager_Name__c +' Modification history -</h4>'+
                                                strManagerReq+
                                            '</div>'+
                                        '</td><br />';
                    }*/
                    strManagerReqDetails =  strManagerReq;

                }else{
                    strManagerReqDetails = '';
                }

                
                String strCREReq = '';
                If(lstRequestVar.lstCRERequestDetails.size() > 0){
                    for (CSRApprovalController.RequestDetails lstCRERequest: lstRequestVar.lstCRERequestDetails) {
                        strCREReq += '<p class="para-history"><b>'+lstCRERequest.fieldName+'</b> updated to '+
                         '<span class="blue-text"><b>'+lstCRERequest.fieldValue+'</b></span> by <span class="blue-text">'+objCSRApprovalController.CREName+
                        '</span></p>';
                        
                    }
                    strCREReqDetails =  strCREReq;

                }else{
                    strCREReqDetails = '';
                }
                

                
            
            masterReqDetails = '<td class="request-detailsBlock">' +strHODReqDetails+strDirectorReqDetails+strManagerReqDetails+strCREReqDetails+'</td>';
            strMasterHtml += masterReqDetails;

            String strApprovals =  '<td>';
            
            strApprovals += (lstRequestVar.objCSR.HOD_Approval_Status__c != NULL )? '<p><i>' +lstRequestVar.objCSR.HOD_Approval_Status__c+' by -</i>'+lstRequestVar.objCSR.HOD_Name__c+'</p>':'';
            strApprovals += (lstRequestVar.objCSR.Director_Approval_Status__c != NULL)? '<p><i>' +lstRequestVar.objCSR.Director_Approval_Status__c+' by -</i>'+lstRequestVar.objCSR.Director_Name__c+'</p>':'';
            strApprovals += (lstRequestVar.objCSR.Manager_Approval_Status__c != NULL )? '<p><i>' +lstRequestVar.objCSR.Manager_Approval_Status__c+' by -</i>'+lstRequestVar.objCSR.Manager_Name__c+'</p>':'';
            
            strApprovals += '</td>';
            System.debug('strApprovals=='+strApprovals);
            strMasterHtml += strApprovals;

            strMasterHtml += '</tr>';

            strRequestDeailsHtml += strMasterHtml;

        }
            System.debug('strRequestDeailsHtml=='+strRequestDeailsHtml);
        contentValue = String.isNotBlank( strRequestDeailsHtml )?contentValue.replace('{!lstRequestVar}',
            strRequestDeailsHtml ):contentValue.replace('{!lstRequestVar}',''); 
        
        contentBody = String.isNotBlank( strRequestDeailsHtml )?contentBody.replace('{!lstRequestVar}', 
            strRequestDeailsHtml ):contentBody.replace('{!lstRequestVar}','');



        subject = emailTemplateObj.Subject != NULL ? emailTemplateObj.Subject : 'Case_Summary_Client';
        // replace {!CaseNumber}
        Subject = String.isNotBlank( objCase.CaseNumber)?Subject.replace('{!CaseNumber}',objCase.CaseNumber):
        Subject.replace('{!CaseNumber}','');
        
                
            // Callout to sendgrid to send an email
            SendGridEmailService.SendGridResponse objSendGridResponse =
                SendGridEmailService.sendEmailService(
                    toAddress
                    , ''
                    , strCCAddress
                    , ''
                    , bccAddress
                    , ''
                    , subject
                    , ''
                    , fromAddress
                    , ''
                    , replyToAddress
                    , ''
                    , contentType
                    , contentValue
                    , ''
                    , lstAttach
                );

            System.debug('objSendGridResponse == ' + objSendGridResponse);
            //Create Email Activity
            String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
            if (responseStatus == 'Accepted') {
                EmailMessage mail = new EmailMessage();
                mail.Subject = subject;
                mail.MessageDate = System.Today();
                mail.Status = '3';//'Sent';
                mail.RelatedToId = strAccountId;
                mail.Account__c  = strAccountId;
                mail.Type__c = 'CSR Email';
                mail.ToAddress = toAddress;
                mail.FromAddress = fromAddress;
                mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                mail.Sent_By_Sendgrid__c = true;
                mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                mail.CcAddress = strCCAddress;
                mail.BccAddress = bccAddress;
                lstEmails.add(mail);
                system.debug('Mail obj == ' + mail);


            }//End Email Activity Creation
            if(lstEmails != null && lstEmails.size()>0 ){
                insert lstEmails;
                //update objCase;
            }
            
            
        }//End Payment details null check if

    }
    else{
        System.debug('This is CRE');
        if(objCase.Approving_User_Role__c.containsIgnoreCase('Review Team') ){
            
        String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';
        String subject, strAccountId, strDPIId, bccAddress ='', strCCAddress;           
            
        user objuser = [SELECT  Email
                        FROM User 
                        WHERE Id =: objCase.Approving_User_Id__c 
                        LIMIT 1];

        

        toAddress =Test.isRunningTest()? 'arjun.khatri@eternussolutions.com' : objuser.Email;//'arjun.khatri00231@gmail.com';//kanujulka@gmail.com'ankitashinde732@gmail.com';//objuser.Email;
        //toAddress = 'arjun.khatri@eternussolutions.com';
        System.debug('toAddress = ' + toAddress);

        fromAddress = 'caseapprovals@damacgroup.com'; //'noreply@damacgroup.com';
        bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
        contentType = 'text/html';
        strCCAddress = '';
        
        if( toAddress != '' ) {
            contentBody = 'Case '+ objCustomerDetails.strCaseNumber +' has been assigned to you, Please reveiw SR.';
            contentValue = 'Case '+ objCustomerDetails.strCaseNumber +' has been assigned to you, Please reveiw SR.';
        }
        subject = 'Case '+ objCustomerDetails.strCaseNumber +' transferred to you.';
        
            // Callout to sendgrid to send an email
            SendGridEmailService.SendGridResponse objSendGridResponse =
                SendGridEmailService.sendEmailService(
                    toAddress
                    , ''
                    , strCCAddress
                    , ''
                    , bccAddress
                    , ''
                    , subject
                    , ''
                    , fromAddress
                    , ''
                    , replyToAddress
                    , ''
                    , contentType
                    , contentValue
                    , ''
                    , lstAttach
                );

            System.debug('objSendGridResponse == ' + objSendGridResponse);
            //Create Email Activity
            String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
            if (responseStatus == 'Accepted') {
                EmailMessage mail = new EmailMessage();
                mail.Subject = subject;
                mail.MessageDate = System.Today();
                mail.Status = '3';//'Sent';
                mail.RelatedToId = strAccountId;
                mail.Account__c  = strAccountId;
                mail.Type__c = 'CSR Email';
                mail.ToAddress = toAddress;
                mail.FromAddress = fromAddress;
                mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                mail.Sent_By_Sendgrid__c = true;
                mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                mail.CcAddress = strCCAddress;
                mail.BccAddress = bccAddress;
                lstEmails.add(mail);
                system.debug('Mail obj == ' + mail);


            }//End Email Activity Creation
            if(lstEmails != null && lstEmails.size()>0 ){
                insert lstEmails;
                //update objCase;
            }   
        }           
        
    }
        
    }
    

}