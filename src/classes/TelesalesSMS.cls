public class TelesalesSMS implements Queueable,Database.AllowsCallouts {
    String body = '';
    List<String> pMobileNumberList = new List<String>();
    public TelesalesSMS(String body, List<String> pMobileNumberList){
        this.body = body;
        this.pMobileNumberList = pMobileNumberList;
    }
    public void execute(QueueableContext context) {
        try{
            if (!Test.isRunningTest())
                SecureSMSService.prepareData(body,pMobileNumberList);
            system.debug('***TelesalesSMS****SMS send********************');
        } catch(Exception ex) {
            system.debug('*****TelesalesSMS********SMS Exception occured*****************' + ex);
        }
    }
}