@isTest
public class CSR_SendNotificationToApproverTest {
    @isTest
    static void  testApproverNotification() {
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            // User Insert 
            User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABC@test.com');
            insert pcUser;
            
            // Create common test accounts
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            objAcc.Email__c ='test@test.com';
            objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
            insert objAcc ;
            
            Id recTypeCSR = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
            System.debug('recTypeCOD :'+ recTypeCSR);
            // insert CSR Case
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCSR);
            objCase.Approving_User_Id__c = pcUser.id;
            objCase.Approving_User_Role__c = 'Manager';
            objCase.status = 'Submitted';
            insert objCase;
            
            Test.startTest();
            
            List<CSR_SendNotificationToApprover.CaseDataWrap> argList = new List<CSR_SendNotificationToApprover.CaseDataWrap>();
            CSR_SendNotificationToApprover.CaseDataWrap arg1 = new CSR_SendNotificationToApprover.CaseDataWrap();
            arg1.approverEmail = 'test@test.com';
            arg1.caseId = '5001w000006Xz7E';
            arg1.caseNumber = '1018896';
            CSR_SendNotificationToApprover.CaseDataWrap arg2 = new CSR_SendNotificationToApprover.CaseDataWrap();
            arg2.approverEmail = 'userABC@test.com';
            arg2.caseId = '5001w000006Yz7E';
            arg2.caseNumber = '1018897';
            argList.add(arg2);
            
            CSR_SendNotificationToApprover.sendPushNotification(argList);
            Test.stopTest();
        } /* run as admin */
    }
}