public without sharing class CustomerCovidSurveyController {

    public  static  String  SURVEY_INITIALIZATION_ERROR = 'There was an error in initializing the survey. '
                                                            + 'Please try again later';

    public  Survey_CRM__c                   survey          {get; set;}
    public  List<SurveyQuestion>            questions       {get; set;}
    public  Survey_Taken_CRM__c             surveyResponse  {get; set;}
    public  Boolean                         isPortalSurvey  {get; set;}
    public  Boolean                         isError         {get; set;}
    public  Boolean                         surveyComplete  {get; set;}
    public  Boolean                         isUnitReadOnly  {get; set;}

    public CustomerCovidSurveyController() {
        isPortalSurvey = false;
        isError = false;
        surveyComplete = false;
        isUnitReadOnly = false;
        List<Survey_CRM__c> lstSurvey = [
            SELECT  Id
                    , Name
                    , Arabic_Name__c
                    , Description__c
                    , Arabic_Description__c
                    , Thank_You_Text__c
                    , Arabic_Thank_You_Text__c
                    , Type__c
                    , ( SELECT      Id
                                    , Question__c
                                    , Arabic_Question__c
                                    , Choices__c
                                    , OrderNumber__c
                                    , Order_Number_Displayed__c
                                    , Arabic_Order_Number_Displayed__c
                                    , Type__c
                                    , Required__c
                                    , Survey__c
                                    , Name
                                    , Choice_for_Additional_Textbox__c
                                    , Survey_Question__c
                                    , Additional_Description__c
                        FROM        SurveyQuestions__r
                        ORDER BY    OrderNumber__c
                    )
            FROM    Survey_CRM__c
            WHERE   Name = :Label.FMCustomerCovidSurveyForm
                AND Is_Active__c = TRUE
        ];
        if (lstSurvey.isEmpty()) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, SURVEY_INITIALIZATION_ERROR));
            return;
        }

        survey = lstSurvey[0];
        questions = initializeQuestions(survey.SurveyQuestions__r);
        surveyResponse = new Survey_Taken_CRM__c();
        
        //prefill BU;
        if(Apexpages.currentpage().getparameters() != null && String.isNotBlank(Apexpages.currentpage().getparameters().get('unitId'))) {

            String unitId = Apexpages.currentpage().getparameters().get('unitId');
            System.debug('unitId: '+unitId);
            List<Booking_Unit__c> objBU = [SELECT id
                                                , Unit_Name__c
                                                , Property_Name_Inventory__c 
                                           FROM Booking_Unit__c
                                           WHERE Id =:unitId
                                           ];     
            surveyResponse.Booking_Unit__c = unitId;  //a0x0Y000001kyE1QAI
            if(objBU.size() > 0) {
                surveyResponse.Booking_Unit_Name__c = objBU[0].Unit_Name__c;
                surveyResponse.Property_Name__c = objBU[0].Property_Name_Inventory__c;
                isUnitReadOnly = true;
            }
            
        }
        

    }

    

    public static List<SurveyQuestion> initializeQuestions(List<Survey_Question_CRM__c> lstQuestions) {
        System.debug('lstQuestions= '+lstQuestions);

        List<SurveyQuestion> questions = new List<SurveyQuestion>();

        List<Survey_Question_CRM__c> parentQuestions = new List<Survey_Question_CRM__c>();

        Map<Id,List<Survey_Question_CRM__c>> mapParentIdToChildQuestions = new Map<Id,List<Survey_Question_CRM__c>>();
        for (Survey_Question_CRM__c q : lstQuestions) {
            if (q.Survey_Question__c == NULL) {
                parentQuestions.add(q);
            } else {
                if (mapParentIdToChildQuestions.get(q.Survey_Question__c) == NULL) {
                    mapParentIdToChildQuestions.put(q.Survey_Question__c,new List<Survey_Question_CRM__c>());
                }
                mapParentIdToChildQuestions.get(q.Survey_Question__c).add(q);
            }
        }

        for (Survey_Question_CRM__c q : parentQuestions) {
            List<SurveyQuestion> subQuestionWrapper = new List<SurveyQuestion>();
            List<Survey_Question_CRM__c> subQuestions = mapParentIdToChildQuestions.get(q.Id);
            if (subQuestions != null && subQuestions.size() >0) {
                for (Survey_Question_CRM__c question : subQuestions) {
                    subQuestionWrapper.add(new SurveyQuestion(question,NULL));
                }
            }
            SurveyQuestion theQ = new SurveyQuestion(q,subQuestionWrapper);
            questions.add(theQ);
        }
        System.debug('questions= '+questions);
        return questions;
    }

    public void submitSurvey() {
        try {
            List <Survey_Question_Response_CRM__c> sqrList = new List<Survey_Question_Response_CRM__c>();
            List<SurveyQuestion> allCombinedQuestions = new List<SurveyQuestion>();
            allCombinedQuestions.addAll(questions);
            System.debug('questions::::'+questions);
            for (SurveyQuestion question : questions) {
               System.debug('question.isSubQuestion:::'+question.isSubQuestion);
                if (question.isSubQuestion) {
                    allCombinedQuestions.addAll(question.subQuestions);
                }
            }
            isError = false;
            System.debug('allCombinedQuestions:::'+allCombinedQuestions);
            for (SurveyQuestion q : allCombinedQuestions) {
                System.debug('q.surveyQuestion.Type__c:::'+q.surveyQuestion.Type__c);
                if (q.surveyQuestion.Type__c != 'Section') {
                    Survey_Question_Response_CRM__c sqr = new Survey_Question_Response_CRM__c();
                    if (q.choiceForAdditionalTextbox != NULL
                        && q.choiceForAdditionalTextbox != ''
                        && q.additionalResponse != ''
                    ) {
                        sqr.Additional_Response__c = q.additionalResponse;
                        sqr.Response__c = q.choices;
                        sqr.Survey_Question__c = q.Id;
                        sqrList.add(sqr);
                    }
                    else if (q.renderFreeText) {
                        sqr.Response__c = q.choices; sqr.Survey_Question__c = q.Id;sqrList.add(sqr);
                    } 
                        
                }
                    
                    

                    
            }
            
            
            
            
            surveyResponse.Survey__c = survey.Id;
            upsert surveyResponse;

            for (Survey_Question_Response_CRM__c sqr : sqrList) {
                sqr.Survey_Taker__c = surveyResponse.Id;
            }
            insert sqrList;
            System.debug('sqrList: '+sqrList);
            isError = false;
            surveyComplete=true;
        }
        catch(Exception e) {
            System.debug('Exception: ' + e.getMessage() + e.getStackTraceString() );
            isError = true;
            Apexpages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR,
                'Some error occured while saving response')
            );
        }
    }

    @RemoteAction
    public static List<Booking_Unit__c> searchBookingUnit(String searchTerm) {
        Set<String> setActiveStatuses = Booking_Unit_Active_Status__c.getAll().keySet();
        return Database.query(
            'SELECT  Id ' +
                    ', Unit_Name__c ' +
                    ', Inventory__r.Property__c ' +
                    ', Inventory__r.Property__r.Name ' +
                    ', Booking__r.Account__c ' +
                    ', Booking__r.Account__r.Name ' +
                    ', Booking__r.Account__r.IsPersonAccount ' +
                    ', Booking__r.Account__r.Email__c ' +
                    ', Booking__r.Account__r.Email__pc ' +
                    ', Booking__r.Account__r.Mobile__c ' +
                    ', Booking__r.Account__r.Mobile_Phone_Encrypt__pc ' +
            'FROM Booking_Unit__c ' +
            'WHERE Unit_Name__c LIKE \'%' + String.escapeSingleQuotes(searchTerm) + '%\' ' +
            ' AND Registration_Status__c IN :setActiveStatuses'
        );
    }

}