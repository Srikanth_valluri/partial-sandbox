/*
* Description - Test class developed for GenerateSOPService
*
* Version            Date            Author            Description
* 1.0              21/11/2017      Ashish Agarwal     Initial Draft
*/


@isTest
private class GenerateSOPServiceTest {

    static testMethod void testTotalPenaltyWaiver() {
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOPService.TotalPenaltyWaiverResponse_element>();
            GenerateSOPService.TotalPenaltyWaiverResponse_element response1 = new GenerateSOPService.TotalPenaltyWaiverResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	GenerateSOPService.PenaltyWaiverHttpSoap11Endpoint objCall = new GenerateSOPService.PenaltyWaiverHttpSoap11Endpoint();
        	objCall.TotalPenaltyWaiver('123', 'Test', 'SFDC', '123456');
        test.stopTest();
    }
    
    static testMethod void testPenaltyWaiverDetails() {
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOPService.PenaltyWaiverDetailsResponse_element>();
            GenerateSOPService.PenaltyWaiverDetailsResponse_element response1 = new GenerateSOPService.PenaltyWaiverDetailsResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	GenerateSOPService.PenaltyWaiverHttpSoap11Endpoint objCall = new GenerateSOPService.PenaltyWaiverHttpSoap11Endpoint();
        	objCall.PenaltyWaiverDetails('Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test' );
        test.stopTest(); 
    }
    
    static testMethod void testGeneratePenaltyStatment() {
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOPService.GeneratePenaltyStatmentResponse_element>();
            GenerateSOPService.GeneratePenaltyStatmentResponse_element response1 = new GenerateSOPService.GeneratePenaltyStatmentResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	GenerateSOPService.PenaltyWaiverHttpSoap11Endpoint objCall = new GenerateSOPService.PenaltyWaiverHttpSoap11Endpoint();
        	objCall.GeneratePenaltyStatment('Test', 'Test', 'Test', 'Test', 'Test', 'Test');
        test.stopTest(); 
    }
}