public without sharing class FmcCalendarController {

    @RemoteAction
    public static List<Event> fetchAllEvents() {
        System.debug('CustomerCommunityUtils.customerAccountId = ' + CustomerCommunityUtils.customerAccountId);

        Set<Id> buildingLocations = new Map<Id, Location__c>(
            FmcUtils.queryLocationsForAccount(CustomerCommunityUtils.customerAccountId)
        ).keySet();
        return convertToCalendarEvents([
            SELECT  Id
                    , Name
                    , IsAllDayEvent__c
                    , AttachmentURL__c
                    , Description__c
                    , Email__c
                    , EndDateTime__c
                    , Location__c
                    , Phone__c
                    , IsVisibleOnPortal__c
                    , StartDateTime__c
                    , Subject__c
                    , Type__c
                    , BuildingLocation__c
                    , BuildingLocation__r.Id
                    , BuildingLocation__r.Name
                    , BuildingLocation__r.Building_Name__c
                    , BuildingLocation__r.Location_ID__c
                    , BuildingLocation__r.Location_Code__c
                    , BuildingLocation__r.Location_Type__c
                    , BuildingLocation__r.RecordTypeId
                    , BuildingLocation__r.RecordType.Id
                    , BuildingLocation__r.RecordType.Name
                    , BuildingLocation__r.RecordType.DeveloperName
            FROM    FM_Event__c
            WHERE   BuildingLocation__c IN :buildingLocations
                OR  BuildingLocation__c = NULL
        ]);
    }

    private static List<Event> convertToCalendarEvents(List<FM_Event__c> lstEvent) {
        List<Event> events = new List<Event>();
        for (FM_Event__c fmEvent : lstEvent) {
            Event event = new Event();
            event.id = fmEvent.Id;
            event.title = fmEvent.Name;
            event.allDay = fmEvent.IsAllDayEvent__c;
            event.startDateTime = fmEvent.StartDateTime__c;
            event.endDateTime = event.allDay ? NULL : fmEvent.EndDateTime__c;
            event.color = '#1D89E4';
            event.url = fmEvent.AttachmentURL__c;
            event.attachmentUrl = fmEvent.AttachmentURL__c;
            event.description = fmEvent.Description__c;
            event.email = fmEvent.Email__c;
            event.location = fmEvent.Location__c;
            event.phone = fmEvent.Phone__c;
            event.subject = fmEvent.Subject__c;
            event.eventType = fmEvent.Type__c;
            if (fmEvent.BuildingLocation__c != NULL) {
                event.buildingId = fmEvent.BuildingLocation__r.Id;
                event.buildingName = String.isBlank(fmEvent.BuildingLocation__r.Building_Name__c)
                                    ? fmEvent.BuildingLocation__r.Name : fmEvent.BuildingLocation__r.Building_Name__c;
                event.buildingLocationId = fmEvent.BuildingLocation__r.Location_ID__c;
                event.buildingLocationCode = fmEvent.BuildingLocation__r.Location_Code__c;
                event.buildingLocationType = fmEvent.BuildingLocation__r.Location_Type__c;
                event.buildingRecordTypeId = fmEvent.BuildingLocation__r.RecordType.Id;
                event.buildingRecordTypeName = fmEvent.BuildingLocation__r.RecordType.Name;
                event.buildingRecordTypeDeveloperName = fmEvent.BuildingLocation__r.RecordType.DeveloperName;
            }
            events.add(event);
        }
        return events;
    }

    public class Event {
        public  String      id                              {get; set;}
        public  String      title                           {get; set;}
        public  Boolean     allDay                          {get; set;}
        public  DateTime    startDateTime                   {get; set;}
        public  DateTime    endDateTime                     {get; set;}
        public  String      url                             {get; set;}
        public  String      color                           {get; set;}
        public  String      attachmentUrl                   {get; set;}
        public  String      description                     {get; set;}
        public  String      email                           {get; set;}
        public  String      location                        {get; set;}
        public  String      phone                           {get; set;}
        public  String      subject                         {get; set;}
        public  String      eventType                       {get; set;}
        public  String      buildingId                      {get; set;}
        public  String      buildingName                    {get; set;}
        public  String      buildingLocationId              {get; set;}
        public  String      buildingLocationCode            {get; set;}
        public  String      buildingLocationType            {get; set;}
        public  String      buildingRecordTypeId            {get; set;}
        public  String      buildingRecordTypeName          {get; set;}
        public  String      buildingRecordTypeDeveloperName {get; set;}
    }

}