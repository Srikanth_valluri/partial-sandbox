@isTest
global class SOAPCalloutServiceMockHO implements WebServiceMock{
    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
           system.debug('MOCK HO request****************************'+request);
           system.debug('MOCK HO requestName****************************'+requestName);
           system.debug('MOCK HO stub****************************'+stub);
           system.debug('MOCK HO endpoint****************************'+endpoint);
           system.debug('soapAction****************************'+soapAction);
           system.debug('responseNS****************************'+responseNS);
           system.debug('responseName****************************'+responseName);
           system.debug('responseType****************************'+responseType);
        if(request instanceof DocumentationForKeyHandoverService.DoumentationForKeyHandover_element) {
            system.debug('*****HO MOCK Called*****');
            DocumentationForKeyHandoverService.DoumentationForKeyHandoverResponse_element response1 = 
            new DocumentationForKeyHandoverService.DoumentationForKeyHandoverResponse_element();
            response1.return_x = '{"allowed":"Yes","message":null,"mortgageNOCfromBank":"Not Required","ifPoaTakingHandoverColatePoaPassportResidence":"Mandatory","corporateValidTradeLicence":"Not Required","corporateArticleMemorandumOfAssociation":"Not Required","corporateBoardResolution":"Not Required","corporatePoa":"Mandatory","signedForm":"Not Required","clearAndValidPassportCopyOfOwner":"Mandatory","clearAndValidPassportCopyOfJointOwner":"Not Required","visaOrEntryStampWithUid":"Mandatory","copyofValidEmiratesId":"Not Required","copyofValidGccId":"Not Required","handoverChecklistAndLod":"Mandatory","keyReleaseForm":"Mandatory","checkOriginalSpaAndtakeCopyOfFirstFourPagesOfSpa":"Mandatory","areaVariationAddendum":"Mandatory","tempOne":"Mandatory","tempTwo":null,"tempThree":null,"handoverNoticeAllowed":null,"approvalQueueOne":null,"approvalQueueTwo":null,"approvalQueueThree":null,"eligibleforRentalPool":null}';
            //response1.put('response_x', response1);
            response.put('response_x', response1);
        }
    }
}