global class ZiwoLogs2TasksQueueable implements Database.Batchable<sObject> {

    global database.queryLocator start(Database.Batchablecontext bc){
        
        string query = 'Select id, CrtObjectId__c from Task where CrtObjectId__c != null and Recorded_Call__c = null and (Event_Type__c = \'Ziwo Inbound\' or Event_Type__c = \'Ziwo Outbound\')';
        return database.getQuerylocator(query);

        /*
        Map<string, Scheduler_Settings__c> settings = Scheduler_settings__c.getAll();
        
        Scheduler_Settings__c ziwoSettings = settings.containsKey('Logs2Tasks') ? settings.get('Logs2Tasks'): new Scheduler_Settings__c(name = 'Logs2Tasks');
        DateTime LastRunTime = (ziwoSettings.Last_Run_Time__c == null ? System.now().addMinutes(-15) : ziwoSettings.Last_Run_Time__c);
        
        string query = 'Select id, Ziwo_CallID__c, Ziwo_Activity_Subject_New__c, Ziwo_Actual_Talktime_New__c,';
        query += 'Ziwo_Task_Call_End_Time__c, Ziwo_Task_Call_Start_Time__c, Ziwo_Activity_Type__c, Ziwo_recordingFile__c ';
        query += 'from Waybeo_logs__c ';
        query += 'where Ziwo_Activity_Subject_New__c != null and Ziwo_callID__c != NULL AND LastModifiedDate >=: LastRunTime';

        ziwoSettings.Last_Run_Time__c = System.now();
        upsert ziwoSettings;
        
        */

    }
    
    global void execute(Database.Batchablecontext bc, list<Task> scope){

        set<string> callIds = new set<string>();
        for(Task tObj : scope){
            callIds.add(tObj.CrtObjectId__c);
        }
        list<Task> lstTasks2Upsert = new list<Task>();
        for(Waybeo_logs__c log : [Select id, Ziwo_CallID__c, Ziwo_Activity_Subject_New__c, Ziwo_Actual_Talktime_New__c,
                                    Ziwo_Task_Call_End_Time__c, Ziwo_Task_Call_Start_Time__c, Ziwo_Activity_Type__c, Ziwo_recordingFile__c 
                                    from Waybeo_logs__c where Ziwo_callID__c in: callIds]){
            Task tObj = new Task();
            tObj.Subject = log.Ziwo_Activity_Subject_New__c;
            tObj.Status = 'Completed';
            tObj.CallDurationInSeconds = (log.Ziwo_Actual_Talktime_New__c != null ? integer.valueOf(log.Ziwo_Actual_Talktime_New__c) : 0);
            tObj.Call_End_time__c = log.Ziwo_Task_Call_End_Time__c;
            tObj.Call_Start_Time__c = log.Ziwo_Task_Call_Start_Time__c;
            tObj.CrtObjectId__c = log.Ziwo_callID__c;
            tobj.Recorded_Call__c = log.Ziwo_recordingFile__c;
            tObj.Activity_type_3__c = log.Ziwo_Activity_Type__c;
            tObj.Recording_URL__c = log.Ziwo_recordingFile__c;
            lstTasks2Upsert.add(tObj);
        }
        if(!lstTasks2Upsert.isEmpty())
            upsert lstTasks2Upsert CrtObjectId__c;
    }
    
    global void finish(Database.Batchablecontext bc){
        database.executeBatch(new ZiwoTasks2CallLogsQueueable(), integer.valueOf(System.label.Ziwo_CallLog_Update_Batch_Count));
    }
    
}