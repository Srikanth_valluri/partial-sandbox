@isTest
private class AP_ProjectCompareControllerTest {

    @testSetup static void setupData() {
        List<AdditionalFeatures__c> features = new List<AdditionalFeatures__c>();

        AdditionalFeatures__c feature1 = new AdditionalFeatures__c();
        feature1.Name = 'Cafes';
        feature1.Label__c = 'Cafes';
        feature1.Url__c = 'AdditionalFeatures/cafe.png';
        features.add(feature1);

        AdditionalFeatures__c feature2 = new AdditionalFeatures__c();
        feature2.Name = 'Swimmimg Pool';
        feature2.Label__c = 'Swimmimg Pool';
        feature2.Url__c = 'AdditionalFeatures/Swimming_Pool.png';
        features.add(feature2);

        AdditionalFeatures__c feature3 = new AdditionalFeatures__c();
        feature3.Name = 'Spa';
        feature3.Label__c = 'Spa';
        feature3.Url__c = 'AdditionalFeatures/SPA.png';
        features.add(feature3);

        insert features;

        Marketing_Documents__c mrkDoc = new Marketing_Documents__c();
        mrkDoc.Name = 'Aurum Villas';
        mrkDoc.Has_Multiple_Property__c = true;
        mrkDoc.Marketing_Name__c = 'Aurum Villas';

        insert mrkDoc;

        Property__c firstProp  = new Property__c();
        firstProp.Name = 'COURSETIA @ AKOYA OXYGEN';
        firstProp.Property_Name__c = 'COURSETIA @ AKOYA OXYGEN';
        firstProp.Property_ID__c = 3758;
        firstProp.Currency_Of_Sale__c = 'AED';
        firstProp.Property_Code__c = 'CRT';

        insert firstProp;

        Property__c secondProp = new Property__c();
        secondProp.Name = 'JUNIPER @ AKOYA OXYGEN';
        secondProp.Property_Name__c = 'JUNIPER @ AKOYA OXYGEN';
        secondProp.Property_ID__c = 3755;
        secondProp.Currency_Of_Sale__c = 'AED';
        secondProp.Property_Code__c = 'JNP';

        insert secondProp;

        Location__c firstLoc = new Location__c();
        firstLoc.Location_Code__c = 'CRT';
        firstLoc.Name = 'CRT';
        firstLoc.Location_ID__c = '74588';
        firstLoc.Location_Type__c = 'Building';
        firstLoc.Building_Name__c = 'COURSETIA @ AKOYA OXYGEN';

        insert firstLoc;

        //insert data for first payment plan
        Payment_Plan__c objPaymentPlan = new Payment_Plan__c();
        objPaymentPlan.Building_Location__c = firstLoc.Id;
        objPaymentPlan.Effective_From__c = system.today();
        objPaymentPlan.Effective_To__c = system.today().addDays(3);
        insert objPaymentPlan;

        // insert data for first payment terms
        insertPaymentTerms(objPaymentPlan.Id);

        Location__c secondLoc = new Location__c();
        secondLoc.Location_Code__c = 'JNP';
        secondLoc.Name = 'JNP';
        secondLoc.Location_ID__c = '69148';
        secondLoc.Location_Type__c = 'Building';
        secondLoc.Building_Name__c = 'JUNIPER @ AKOYA OXYGEN';

        insert secondLoc;

        //insert data for second payment plan
        Payment_Plan__c objPaymentPlan2 = new Payment_Plan__c();
        objPaymentPlan2.Building_Location__c = secondLoc.Id;
        objPaymentPlan2.Effective_From__c = system.today();
        objPaymentPlan2.Effective_To__c = system.today().addDays(3);
        insert objPaymentPlan2;

        // insert data for second payment terms
        insertPaymentTerms(objPaymentPlan2.Id);

		Campaign__c camp = new Campaign__c();
        camp.Campaign_Name__c='Test Campaign';
        camp.start_date__c = System.today();
        camp.end_date__c = System.Today().addDays(30);
        camp.Marketing_start_date__c = System.today();
        camp.Marketing_end_date__c = System.Today().addDays(30);
        camp.Language__c = 'English';
        camp.Marketing_Active__c = true;
        camp.Credit_Control_Active__c  = true;
        camp.Sales_Admin_Active__c  = true;
        insert camp;

		Inquiry__c newInquiry = new Inquiry__c();
        newInquiry.Campaign__c = camp.Id;
        newInquiry.First_Name__c = 'Testsearch';
        newInquiry.Last_Name__c = 'Inquiry';
        newInquiry.Mobile_Phone_Encrypt__c = '05789088';
        newInquiry.Mobile_Phone__c = '05789088';
        newInquiry.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        newInquiry.Email__c = 'test.lastname@eternussolutions.com';
        newInquiry.Preferred_Language__c = 'English';
        newInquiry.Inquiry_Source__c = 'Social' ;
        newInquiry.Inquiry_Status__c = 'New' ;
        insert newInquiry;


		EOI_Process__c eOI = new EOI_Process__c();
        eOI.Inquiry__c = newInquiry.id; 
        eOI.Mode_of_Token_Payment__c = 'Cash';     
        eOI.No_of_Units__c = '1 Unit';
        insert eOI;

        Inventory__c inv1 = new Inventory__c();
        inv1.Marketing_Name_Doc__c = mrkDoc.Id;
        inv1.Marketing_Name__c = 'Aurum Villas';
        inv1.Building_Location__c = firstLoc.Id;
        inv1.Property__c = firstProp.Id;
        inv1.RecordTypeId = DamacUtility.getRecordTypeId('Inventory__c','Building');
        inv1.Status__c = 'Released';
        inv1.ACD_Date__c = '2019-03-31';
        inv1.Area_Sqft__c = '2403';
        inv1.Special_Price__c = 2016000.00;
        inv1.Property_Name__c = 'COURSETIA @ AKOYA OXYGEN';
		inv1.EOI__c = eOI.id;

        insert inv1;

        Inventory__c inv2 = new Inventory__c();
        inv2.Marketing_Name_Doc__c = mrkDoc.Id;
        inv2.Marketing_Name__c = 'Aurum Villas';
        inv2.Building_Location__c = secondLoc.Id;
        inv2.Property__c = secondProp.Id;
        inv2.RecordTypeId = DamacUtility.getRecordTypeId('Inventory__c','Building');
        inv2.Status__c = 'Released';
        inv2.ACD_Date__c = '2019-03-31';
        inv2.Area_Sqft__c = '2407';
        inv2.Special_Price__c = 201600.00;
        inv2.Property_Name__c = 'JUNIPER @ AKOYA OXYGEN';
		inv2.EOI__c = eOI.id;

        insert inv2;

        List<Project_Information__c> projectInfoList = new List<Project_Information__c>();
        Project_Information__c prjInfo1 = new Project_Information__c();
        prjInfo1.Marketing_Documents__c = mrkDoc.Id;
        prjInfo1.Features__c = 'Gallery';
        prjInfo1.Label__c = 'Project Plan';
        prjInfo1.Property__c = firstProp.Id;
        prjInfo1.Use_For_Compare__c = true;
        prjInfo1.URL__c = 'https://damacholding--devpronew--c.cs89.content.force.com/servlet/servlet.FileDownload?file=00P0E0000019Ar6';
        projectInfoList.add(prjInfo1);

        Project_Information__c prjInfo2 = new Project_Information__c();
        prjInfo2.Marketing_Documents__c = mrkDoc.Id;
        prjInfo2.Features__c = 'Unit Features';
        prjInfo2.Heading__c = 'Project Plan';
        prjInfo2.Property__c = firstProp.Id;
        prjInfo2.Detail__c = '<ul><li>All rooms feature double glazed windows</li>'+
                             '<li>Ceramic tiled floors throughout</li>'+
                             '<li>Painted plastered walls and soffit</li>'+
                             '<li>Wardrobes in Bedrooms</li></ul>';
        prjInfo2.Order__c = 1;
        projectInfoList.add(prjInfo2);

        Project_Information__c prjInfo3 = new Project_Information__c();
        prjInfo3.Marketing_Documents__c = mrkDoc.Id;
        prjInfo3.Features__c = 'Unit Features';
        prjInfo3.Heading__c = 'Kitchen';
        prjInfo3.Property__c = firstProp.Id;
        prjInfo3.Detail__c = '<ul><li>Kitchens fitted with cabinets and counter tops with space '+
                             'and hookup provisions for white goods</li>'+
                             '<li>Ceramic tiled floors</li>'+
                             '<li>Emulsion paints for walls</li>'+
                             '<li>Laminated kitchen cabinet</li>'+
                             '<li>Stone countertop</li>'+
                             '<li>Stainless Steel sink</li></ul>';
        prjInfo3.Order__c = 2;
        projectInfoList.add(prjInfo3);

        Project_Information__c prjInfo4 = new Project_Information__c();
        prjInfo4.Marketing_Documents__c = mrkDoc.Id;
        prjInfo4.Features__c = 'Unit Features';
        prjInfo4.Heading__c = 'Bathroom Features';
        prjInfo4.Property__c = firstProp.Id;
        prjInfo4.Detail__c = '<ul><li>Floor / Wall ceramic tiles</li>'+
                             '<li>Standard white sanitary ware</li>'+
                             '<li>Standard sanitary fittings and accessories</li>'+
                             '<li>Mirror</li><li>Threshold</li></ul>';
        prjInfo4.Order__c = 3;
        projectInfoList.add(prjInfo4);

        Project_Information__c prjInfo5 = new Project_Information__c();
        prjInfo5.Marketing_Documents__c = mrkDoc.Id;
        prjInfo5.Features__c = 'Project Features';
        prjInfo5.Property__c = firstProp.Id;
        prjInfo5.Project_Features__c = 'Cafes; Childrens Play Area; Community; Dining; '+
                                       'Disabled Accessibility; Entertainment; Fitness; '+
                                       'Golf; Hotel Services; Laundry Services; Nature and Parks; '+
                                       'Parking; School and Nurseries; Security; '+
                                       'Shopping; Spa; Swimmimg Pool';
        projectInfoList.add(prjInfo5);


        Project_Information__c prjInfo11 = new Project_Information__c();
        prjInfo11.Marketing_Documents__c = mrkDoc.Id;
        prjInfo11.Features__c = 'Gallery';
        prjInfo11.Label__c = 'Project Plan';
        prjInfo11.Property__c = secondProp.Id;
        prjInfo11.Use_For_Compare__c = true;
        prjInfo11.URL__c = 'https://damacholding--devpronew--c.cs89.content.force.com/servlet/servlet.FileDownload?file=00P0E0000019Ar6';
        projectInfoList.add(prjInfo11);

        Project_Information__c prjInfo12 = new Project_Information__c();
        prjInfo12.Marketing_Documents__c = mrkDoc.Id;
        prjInfo12.Features__c = 'Unit Features';
        prjInfo12.Heading__c = 'Project Plan';
        prjInfo12.Property__c = secondProp.Id;
        prjInfo12.Detail__c = '<ul><li>All rooms feature double glazed windows</li>'+
                             '<li>Ceramic tiled floors throughout</li>'+
                             '<li>Painted plastered walls and soffit</li>'+
                             '<li>Wardrobes in Bedrooms</li></ul>';
        prjInfo12.Order__c = 1;
        projectInfoList.add(prjInfo12);

        Project_Information__c prjInfo13 = new Project_Information__c();
        prjInfo13.Marketing_Documents__c = mrkDoc.Id;
        prjInfo13.Features__c = 'Unit Features';
        prjInfo13.Heading__c = 'Kitchen';
        prjInfo13.Property__c = secondProp.Id;
        prjInfo13.Detail__c = '<ul><li>Kitchens fitted with cabinets and counter tops with space '+
                             'and hookup provisions for white goods</li>'+
                             '<li>Ceramic tiled floors</li>'+
                             '<li>Emulsion paints for walls</li>'+
                             '<li>Laminated kitchen cabinet</li>'+
                             '<li>Stone countertop</li>'+
                             '<li>Stainless Steel sink</li></ul>';
        prjInfo13.Order__c = 2;
        projectInfoList.add(prjInfo13);

        Project_Information__c prjInfo14 = new Project_Information__c();
        prjInfo14.Marketing_Documents__c = mrkDoc.Id;
        prjInfo14.Features__c = 'Unit Features';
        prjInfo14.Heading__c = 'Bathroom Features';
        prjInfo14.Property__c = secondProp.Id;
        prjInfo14.Detail__c = '<ul><li>Floor / Wall ceramic tiles</li>'+
                             '<li>Standard white sanitary ware</li>'+
                             '<li>Standard sanitary fittings and accessories</li>'+
                             '<li>Mirror</li><li>Threshold</li></ul>';
        prjInfo14.Order__c = 3;
        projectInfoList.add(prjInfo14);

        Project_Information__c prjInfo15 = new Project_Information__c();
        prjInfo15.Marketing_Documents__c = mrkDoc.Id;
        prjInfo15.Features__c = 'Project Features';
        prjInfo15.Property__c = secondProp.Id;
        prjInfo15.Project_Features__c = 'Cafes; Childrens Play Area; Community; Dining; '+
                                       'Disabled Accessibility; Entertainment; Fitness; '+
                                       'Golf; Hotel Services; Laundry Services; Nature and Parks; '+
                                       'Parking; School and Nurseries; Security; '+
                                       'Shopping; Spa; Swimmimg Pool';
        projectInfoList.add(prjInfo15);

        insert projectInfoList;
    }

    //method used to insert payment terms related to payment plan
    private static List<Payment_Terms__c> insertPaymentTerms(Id paymentPlanID)
    {
        List<Payment_Terms__c> lstPaymentTerms = new List<Payment_Terms__c>();

        //insert data for payment terms
        Payment_Terms__c objPaymentTerm1 = new Payment_Terms__c();
        objPaymentTerm1.Payment_Plan__c = paymentPlanID;
        objPaymentTerm1.Installment__c = 'DP';
        objPaymentTerm1.Description__c = 'DEPOSIT';
        objPaymentTerm1.Percent_Value__c = '24';
        objPaymentTerm1.Milestone_Event__c = 'Immediate';
        objPaymentTerm1.Line_ID__c = math.random()+'1';
        lstPaymentTerms.add(objPaymentTerm1);

        Payment_Terms__c objPaymentTerm2 = new Payment_Terms__c();
        objPaymentTerm2.Payment_Plan__c = paymentPlanID;
        objPaymentTerm2.Installment__c = 'I001';
        objPaymentTerm2.Description__c = '1ST INSTALLMENT';
        objPaymentTerm2.Percent_Value__c = '50';
        objPaymentTerm2.Milestone_Event__c = 'Deposit';
        objPaymentTerm1.Line_ID__c = math.random()+'2';
        lstPaymentTerms.add(objPaymentTerm2);

        insert lstPaymentTerms;
        return lstPaymentTerms;
    }

    @isTest static void test_method_one() {

        List<Inventory__c> selInv = [SELECT
                                        id,
                                        Marketing_Name_Doc__c
                                     FROM Inventory__c
                                     WHERE Marketing_Name__c = 'Aurum Villas'];
        String selInvStr = selInv[0].Id + ','+selInv[1].Id;
		
		EOI_Process__c objEOI = [SELECT
									id,
									Mode_of_Token_Payment__c
									FROM EOI_Process__c
									WHERE Mode_of_Token_Payment__c = 'Cash'];

        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.ProjectCompare'));
        System.currentPageReference().getParameters().put('selectedInv', selInvStr);
        AP_ProjectCompareController ctrl = new AP_ProjectCompareController();
		ctrl.conversionrate1 = 1.0;
		ctrl.conversionrate2 = 1.0;
        ctrl.selProduct = 'Aurum Villas';
        ctrl.selProject ='JUNIPER @ AKOYA OXYGEN';
        ctrl.project = 'Second';
		ctrl.tocurrencyselected1 = 'AED';
		ctrl.tocurrencyselected2 = 'AED';
        ctrl.getSelProjectInfo();
		ctrl.showRelatedProducts1();
		ctrl.showRelatedProducts2();
		AP_ProjectCompareController.checkSelectedUnits(selInvStr,objEOI.id);
        Test.stopTest();
    }

    @isTest static void test_method_two() {
        List<Marketing_Documents__c> markDoc = [SELECT
                                                    id,
                                                    Name
                                                FROM Marketing_Documents__c
                                                WHERE Name = 'Aurum Villas'];

        Property__c prop = [SELECT
                                id,
                                Name
                            FROM Property__c
                            WHERE Name = 'COURSETIA @ AKOYA OXYGEN'];
        Test.startTest();
        AP_ProjectCompareController ctrl = new AP_ProjectCompareController();
        ctrl.selProduct = markDoc[0].Id;
        ctrl.selProject = prop.Id;
        ctrl.project = 'First';
		ctrl.conversionrate1 = 1.0;
		ctrl.conversionrate2 = 1.0;
		ctrl.tocurrencyselected1 = 'AED';
        ctrl.getSelProjectInfo();
		
        Test.stopTest();
    }

}