@isTest
private class HDAppGetHandoverNoticeDocs_APITest {

    @testSetup
    static void testData() {
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'Test Unit Name',
                                      Registration_ID__c = '3901');

        bookingUnit.Handover_Notice_URLs__c = '<ul> <li> <a href="https://sf.deeprootsurface.com/docs/e/60744827_HON.pdf" target="_blank">Notice of Completion and Possession for your property in PRIMROSE @AKOYA OXYGEN</a> </li> <li> <a href="https://sf.deeprootsurface.com/docs/e/60744824_89922_SOA.pdf" target="_blank">Statement of account for your property</a> </li> <li> <a href="https://sf.deeprootsurface.com/docs/e/60744825_89922_SOA.pdf" target="_blank">Statement of account for Owners Association expenses</a> </li> <li> <a href="https://sf.deeprootsurface.com/docs/i/PMR_HO_PACK.pdf" target="_blank">Handover Pack</a> </li> <li> <a href="https://sf.deeprootsurface.com/docs/e/60744826_INV.pdf" target="_blank">Service Charge Invoice</a> </li> <li> <a href="https://damacholding.my.salesforce.com/sfc/p/0Y000000ZkVQ/a/1n000000Fk8s/oA1_4xt2AyPUDIDFfm4SFXpFaVNQOVNZ4ATGx7N4vrc" target="_blank">Mortgage Info</a> </li> <a href="https://sf.deeprootsurface.com/docs/e/XDCREG_DETAILS_60744828_1.PDF" target="_blank">Handover Checklist</a> </li> </ul>';
        insert bookingUnit;
    }

    @isTest
    static void TestMethod1() {
        
        Booking_Unit__c objBU = [SELECT id, Handover_Notice_URLs__c FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getHandoverNoticeDocument';  
        req.addParameter('bookingUnitId', objBU.id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HDAppGetHandoverNoticeDocs_API.MainResponseCreator();
        Test.stopTest();
    }

    @isTest
    static void TestMethod2() {
        
        Booking_Unit__c objBU = [SELECT id, Handover_Notice_URLs__c FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getHandoverNoticeDocument';  
        //req.addParameter('bookingUnitId', objBU.id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HDAppGetHandoverNoticeDocs_API.MainResponseCreator();
        Test.stopTest();
    }
    
    @isTest
    static void TestMethod3() {
        
        Booking_Unit__c objBU = [SELECT id, Handover_Notice_URLs__c FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getHandoverNoticeDocument';  
        req.addParameter('bookingUnitId', '');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HDAppGetHandoverNoticeDocs_API.MainResponseCreator();
        Test.stopTest();
    }

    
}