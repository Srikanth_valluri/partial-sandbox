@isTest
public class API_Agent_Test {

    @isTest 
    static void doGetAgents_Test() {
        RestRequest req = new RestRequest(); 
        
        req.requestURI = '/api_contact';  
        req.httpMethod = 'GET';
        
        req.params.put('offset', '0');
        req.params.put('limit', '10');
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
        
        Test.startTest();
        
        API_Agent.doGet();
        
        Test.stopTest();
    }
    
    @isTest 
    static void doGetAgentProfile_Test() {
        RestRequest req = new RestRequest(); 
        
        req.requestURI = '/api_contact/agent_profile';  
        req.httpMethod = 'GET';
        
        req.params.put('offset', '0');
        req.params.put('limit', '10');
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
        
        Test.startTest();
        
        API_Agent.doGet();
        
        Test.stopTest();
    }
    
    @isTest 
    static void doGetAgencyContact_Test() {
        RestRequest req = new RestRequest(); 
        
        req.requestURI = '/api_contact/Agency_Contact ';  
        req.httpMethod = 'GET';
        
        req.params.put('offset', '0');
        req.params.put('limit', '10');
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
        
        Test.startTest();
        
        API_Agent.doGet();
        
        Test.stopTest();
    }
}