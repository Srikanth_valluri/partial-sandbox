/**
 *  @Description        : This is a daily batch  that will run the escalation process and send consolidated emails
 * Ver       Date            Author                 Modification
 * 1.0    9/19/2019         Arsh Dave              Initial Version                                                                       
 * 2.0    10/08/2019        Arsh Dave              Updated the fields to random fields as new fields cannot be deployet on production  
**/
global class TaskEscalationCustomerSRProcessBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful{
    
    public String query;
    public TaskOwnerIdEscalationSet.ReturnWrapper objReturnWrapperData;
    public Map<Id, Set<Task>> mapEscalatedUserIdVsTaskDetails;
    public List<Task> listTaskUpdated;

    public TaskEscalationCustomerSRProcessBatch(){
        mapEscalatedUserIdVsTaskDetails = new Map<Id, Set<Task>>();
        listTaskUpdated = new List<Task>();
    }

    public Database.Querylocator start( Database.BatchableContext bc ) {
        query = 'SELECT Id, OwnerId, Owner.name, Status, WhatId, ActivityDate, Age__c, First_Property__c, CreatedDate, Subject '+
                'FROM Task Where '+
                'Status != \'Completed\' AND Process_Name__c = \'General Inquiry\' AND First_Property__c = TRUE AND Level_Days_Left_For_Escalation__c <= 0';
        return Database.getQueryLocator(query);
    }

    public void execute( Database.BatchableContext bc, list<Task> lstTask ) {
        objReturnWrapperData = new TaskOwnerIdEscalationSet.ReturnWrapper(NULL, NULL);
        System.debug('-->> Task List Size: ' +lstTask.size() );
        for( Task objCase : lstTask ){
            System.debug('-->> objCase batch : ' + objCase );
            objReturnWrapperData = TaskOwnerIdEscalationSet.callFromBatch(objCase);
            if(objReturnWrapperData != NULL){
                listTaskUpdated.add(objReturnWrapperData.objTaskDetails);
                
                System.debug('-->> objReturnWrapperData : ' + objReturnWrapperData );
                mapfilling(objReturnWrapperData.strOwnerIdEscalation, objReturnWrapperData.objTaskDetails);
            }
        }
        System.debug('-->> listTaskUpdated batch : ' + listTaskUpdated );
    }

    public void finish( Database.BatchableContext bc ) {
        System.debug('-->> mapEscalatedUserIdVsTaskDetails batch : ' + mapEscalatedUserIdVsTaskDetails );
        TaskEscalationEmailBatch objTaskEscalationEmailBatch = new TaskEscalationEmailBatch(mapEscalatedUserIdVsTaskDetails, listTaskUpdated);
        Database.executeBatch(objTaskEscalationEmailBatch, 1);

        // if(!listTaskUpdated.isEmpty()){
        //     update listTaskUpdated;
        // }
    }

    public void mapfilling(String ownerIdEscalation, Task getTaskDetails){
        system.debug('-->>mapfilling :'+mapEscalatedUserIdVsTaskDetails);
        if(mapEscalatedUserIdVsTaskDetails.containsKey(ownerIdEscalation)) {
                    
            Set<Task> smalltempMap= new Set<Task>();

            smalltempMap = mapEscalatedUserIdVsTaskDetails.get(ownerIdEscalation);
            system.debug('-->>smalltemp get key:'+smalltempMap);
            
            smalltempMap.add(getTaskDetails);
            system.debug('-->>smalltemp after put:'+smalltempMap);
            
            mapEscalatedUserIdVsTaskDetails.put(ownerIdEscalation, smalltempMap);
            system.debug('-->>Bada map in if :'+mapEscalatedUserIdVsTaskDetails);
                
        } else{
            mapEscalatedUserIdVsTaskDetails.put(ownerIdEscalation, new Set<Task>{getTaskDetails});
        }
    }

}