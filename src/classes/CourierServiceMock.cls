@isTest
global class CourierServiceMock implements HttpCalloutMock{
	public string SAMPLE_RESPONSE;
    public String getResponseBody(HTTPRequest req){
        if(req.getEndpoint() == 'https://ws.dev.aramex.net/ShippingAPI.V2/Shipping/Service_1_0.svc/json/CreateShipments'){
                SAMPLE_RESPONSE=  '{'+
                '"Transaction": {'+
                    '"Reference1": "null",'+
                    '"Reference2": "null",'+
                    '"Reference3": "null",'+
                    '"Reference4": "null",'+
                    '"Reference5": "null"'+
                '},'+
                '"Notifications": [],'+
                '"HasErrors": false,'+
                '"Shipments": ['+
                 '{'+
                        '"ID": "44097896802",'+
                        '"Reference1": null,'+
                        '"Reference2": null,'+
                        '"Reference3": null,'+
                        '"ForeignHAWB": null,'+
                        '"HasErrors": false,'+
                        '"Notifications": [],'+
                        '"ShipmentLabel": {'+
                         '   "LabelURL": "http://www.sandbox.aramex.com/content/rpt_cache/50cf77bc973b476db72b44cff76e99bc.pdf",'+
                          '  "LabelFileContents": []'+
                        '},'+
                        '"ShipmentDetails": {'+
                         '   "Origin": "AUH",'+
                          '  "Destination": "AUH",'+
                           ' "ChargeableWeight": {'+
                            '    "Unit": "KG",'+
                            '    "Value": 0.5'+
                            '},'+
                            '"DescriptionOfGoods": "Documents",'+
                            '"GoodsOriginCountry": "AE",'+
                            '"NumberOfPieces": 1,'+
                            '"ProductGroup": "DOM",'+
                            '"ProductType": "ONP",'+
                            '"PaymentType": "P",'+
                            '"PaymentOptions": null,'+
                            '"CustomsValueAmount": null,'+
                            '"CashOnDeliveryAmount": null,'+
                            '"InsuranceAmount": null,'+
                            '"CashAdditionalAmount": null,'+
                            '"CollectAmount": null,'+
                            '"Services": null'+
                        '},'+
                        '"ShipmentAttachments": []'+
                    '}'+
                ']'+
                '}';    
        }else if(req.getEndpoint() == 'https://ws.dev.aramex.net/ShippingAPI.V2/Tracking/Service_1_0.svc/json/TrackShipments'){
           SAMPLE_RESPONSE=  '{'+
                             '"Transaction": {'+
                             '"Reference1": null,'+
                             '"Reference2": null,'+
                             '"Reference3": null,'+
                             '"Reference4": null,'+
                             '"Reference5": null'+
                             '},'+
                             '"Notifications": [],'+
                             '"HasErrors": false,'+
                             '"TrackingResults": ['+
                             '{'+
                             '"Key": "44097879825",'+
                             '"Value": ['+
                             '{'+
                             '"WaybillNumber": "44097879825",'+
                             '"UpdateCode": "SH203",'+
                             '"UpdateDescription": "Record Created",'+
                             '"UpdateDateTime": "/Date(1599121080000+0300)/",'+
                             '"UpdateLocation": "Dubai, United Arab Emirates",'+
                             '"Comments": "",'+
                             '"ProblemCode": "",'+
                             '"GrossWeight": "0.5",'+
                             '"ChargeableWeight": "0.5",'+
                             '"WeightUnit": "KG"'+
                             '},'+
                             '{'+
                             '"WaybillNumber": "44097879825",'+
                             '"UpdateCode": "SH278",'+
                             '"UpdateDescription": "Data received.",'+
                             '"UpdateDateTime": "/Date(1599121080000+0300)/",'+
                             '"UpdateLocation": "Dubai, United Arab Emirates",'+
                             '"Comments": " ",'+
                             '"ProblemCode": "",'+
                             '"GrossWeight": "0.5",'+
                             '"ChargeableWeight": "0.5",'+
                             '"WeightUnit": "KG"'+
                             '}'+
                             ']'+
                             '}'+
                             '],'+
                             '"NonExistingWaybills": []'+
                            '}';
        }else if(req.getEndpoint().endsWith('.pdf')){
        	SAMPLE_RESPONSE = 'Test Data';    
        }else if(req.getEndpoint() == 'https://ontrack.firstflightme.com/FFCService.svc/CreateAirwayBill'){
        	SAMPLE_RESPONSE = '{ "AirwayBillNumber": "103000215", "Code": 1, "Description": "Success" }';    
        }if(req.getEndpoint().endsWith('content')) {
             SAMPLE_RESPONSE = '{'+
                         '"@microsoft.graph.downloadUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/download",'+
                         '"id": "123456789A",'+
                         '"webUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/Documents/test.pdf"'+
                         '}';
         }if(req.getEndpoint().endsWith('token')) {
             SAMPLE_RESPONSE = '{'+
                        '"token_type": "Bearer",'+  
                        '"expires_in": "3600",'+
                        '"ext_expires_in": "3600",'+
                        '"expires_on": "1570628201",'+
                        '"not_before": "1570624301",'+
                        '"resource": "https://graph.microsoft.com",'+
                        '"access_token": "eyJ0eXAiOiJKV1QiLCJub25jZSI6IkN3MVpSbEFnNTZKaUt4Mkd2Tl"'+
                    '}';
         }
        
        return SAMPLE_RESPONSE;
    }
    
    global HTTPResponse respond(HTTPRequest req) {
        
        req.setMethod('POST');
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(getResponseBody(req));
        res.setStatusCode(200);
        return res;
    }
}