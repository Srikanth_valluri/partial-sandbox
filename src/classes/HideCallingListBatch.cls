public class HideCallingListBatch implements Database.Batchable<sObject>{

String query;


/*Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
Id DPcollectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('DP Calling List').RecordTypeId;
Id FMcallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
Id AgingcallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Aging Calling List').RecordTypeId;
//RecordTypeIdList.add(collectioncallingRecordTypeId);*/



public Database.QueryLocator start(Database.BatchableContext BC){
    /*List<Id>RecordTypeIdList = new List<Id>();
    RecordTypeIdList.add(collectioncallingRecordTypeId);
    RecordTypeIdList.add(DPcollectioncallingRecordTypeId);
    RecordTypeIdList.add(FMcallingRecordTypeId);
    RecordTypeIdList.add(AgingcallingRecordTypeId);*/
    
    Set<Id> recTypeIdSet=new Set<Id>();
    for(RecordType rec :[SELECT DeveloperName,Id,Name 
                           FROM RecordType 
                           WHERE SobjectType = 'Calling_List__c' 
                           And (Name='Collections Calling List' )]){
                           //OR Name='DP Calling List' 
                           //OR Name= 'FM Collections'
                            //OR Name= 'Aging Calling List'  )  ]){
        recTypeIdSet.add(rec.Id);
    }
    
    query = 'SELECT Id,IsHideFromUI__c,IsHideFalse_Relationship__c FROM Calling_List__c WHERE RecordTypeId IN :recTypeIdSet';
    //query = 'SELECT Id,IsHideFromUI__c FROM Calling_List__c WHERE RecordType.Name IN :(\'Collections Calling List\',\'DP Calling List\',\'FM Collections\',\'Aging Calling List\')';
    //query= 'Select Id,IsHideFalse_Relationship__c,IsHideFromUI__c from Calling_List__c Where IsHideFalse_Relationship__c != null AND RecordTypeId IN :recTypeIdSet AND IsHideFromUI__c = true';
  
    return Database.getQueryLocator(query);
}
public void execute(Database.BatchableContext BC, List<Calling_List__c> callingLst){
    System.debug('callingLst size:::'+callingLst.size());
    List<Calling_List__c>callingLstToHide = new List<Calling_List__c>();
    for(Calling_List__c callingObj : callingLst){
        if(callingObj.IsHideFromUI__c != true){ 
            callingObj.IsHideFromUI__c = true;
             callingObj.IsHideFalse_Relationship__c = null;
            callingLstToHide.add(callingObj);
        }
    }
    System.debug('callingLstToHide::'+callingLstToHide.size());
    update callingLstToHide;
}
public void finish(Database.BatchableContext BC){
}
}