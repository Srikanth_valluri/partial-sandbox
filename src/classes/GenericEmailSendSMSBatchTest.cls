@isTest
private class GenericEmailSendSMSBatchTest {

    static testMethod void myUnitTestOne() {

        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;

        SMS_History__c objSMS = new SMS_History__c();
        objSMS.Message__c = 'Test';
        objSMS.Is_SMS_Sent__c = false ;
        objSMS.Customer__c  = acctIns.Id;
        objSMS.Phone_Number__c = '0097154856556';
        insert objSMS ;
        List<SMS_History__c> lstSMSHistory = new List<SMS_History__c>();
        lstSMSHistory.add(objSMS);        

        test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpAgentOTPController()); 
            Database.executeBatch( new GenericEmailSendSMSBatch(lstSMSHistory) );
        test.stopTest();

    }

    static testMethod void myUnitTestTwo() {
        SMS_History__c objSMS = new SMS_History__c();
        objSMS.Message__c = 'Test';
        objSMS.Is_SMS_Sent__c = false ;
        objSMS.Phone_Number__c = '097154856556';
        insert objSMS ;
        List<SMS_History__c> lstSMSHistory = new List<SMS_History__c>();
        lstSMSHistory.add(objSMS);

        test.startTest();
            Database.executeBatch( new GenericEmailSendSMSBatch(lstSMSHistory) );
        test.stopTest();

    }
}