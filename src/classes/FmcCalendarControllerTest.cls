@isTest
private class FmcCalendarControllerTest {

    @isTest
    static void testPositive() {

        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC'
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        insert new FM_Event__c(
            Name = 'Test Building Event',
            BuildingLocation__c = location.Id
        );

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        Test.startTest();
            System.runAs(portalUser) {
                List<FmcCalendarController.Event> lstEvent = FmcCalendarController.fetchAllEvents();
                System.assertEquals(1, lstEvent.size());
            }
        Test.stopTest();
    }

}