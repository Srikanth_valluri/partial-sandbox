/*--------------------------------------------------------------------------------------------
Description: Controller class for Violation notice
=============================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------
1.0     |            | Dipika Rajput   | 1. Initial draft
----------------------------------------------------------------------------------------------
==============================================================================================
*/ 
global without sharing class FM_ViolationNoticeController {

    // Added on 19/8/2019
    public String violationNoticingDate{get;set;}

    public String selectedCategory{get;set;}
    //private Boolean isMasterCommunityRole;
    public String strDocName{get;set;}
    public String strDocBody{get;set;}
    public list<SR_Attachments__c> uploadedDocuments{get;set;}
    public list<selectOption> lstCategoryOptions{get;set;}
    public List<FM_Documents__mdt> lstDocuments{get;set;}
    public String selectedViolation{get;set;}
    public String docIdToDelete{get;set;}
    public list<selectOption> lstViolationOptions{get;set;}
    private map<String, List<Violation_Rule__c>> mapCat_rules {get;set;}
    private Map<String, Violation_Rule__c> mapId_rules {get;set;}
    public FM_Case__c objFMCase {get;set;}
    public Violation_Rule__c objVR {get;set;}
    private Boolean isTenant {get;set;}
    public Boolean isError {get;set;}
    private Location__c objLoc;
    private String locationId;
    private task objTask;
    private task PMTask;
    private Set<Id> UserIds;
    private Set<Id> FMUserIds;
    public string strAccountId;
    public string strSelectedUnit;
    public FM_ViolationNoticeController() {
        
       // isMasterCommunityRole = false;
        objVR = new Violation_Rule__c();
        isError = false;
        lstDocuments = new List<FM_Documents__mdt>();
        uploadedDocuments = new list<SR_Attachments__c>();
        UserIds = new Set<Id>();
        FMUserIds = new Set<Id>();
        lstCategoryOptions = new list<selectOption>();
        isTenant = false;
        mapCat_rules = new map<String, List<Violation_Rule__c>>();
        objFMCase = new FM_Case__c();
        Id recordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Violation Notice').getRecordTypeId();
        objFMCase.Origin__c = 'Walk-In';
        objFMCase.RecordTypeId = recordTypeId;
        objTask = new Task();
        PMTask = new Task();
        Account objAcc = new Account();
        selectedCategory = selectedViolation = 'None';
        String strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
        String strSRType = ApexPages.currentPage().getParameters().get('SRType');
        String strUnitId = ApexPages.currentPage().getParameters().get('UnitId');
        if(String.isNotBlank(strAccountId)) {
            List<Account> lstAcc = new List<Account>();
            lstAcc = [SELECT ID,Name,Email__pc from Account where Id=: strAccountId];
            if(lstAcc.size() > 0) {
                objAcc = lstAcc[0];
            }
        }
        if(String.isNotBlank(strUnitId)) {
            List<Booking_Unit__c> lstBU = new List<Booking_Unit__c>();
            lstBU = [SELECT Id, Unit_Name__c,Owner__c,Account_Id__c,Property_City__c,
                            Booking__r.Account__r.Email__pc,Booking__r.Account__c,Tenant__r.Email__pc,Tenant__c
                    FROM Booking_Unit__c
                    WHERE Id =: strUnitId];
            if(lstBU.size() > 0 && String.isNotBlank(lstBU[0].Unit_Name__c)) {
                
                // Check if SR is for Tenant/Owner
                // isTenant = lstBU[0].Owner__c == objAcc.Id ? false : true;
                isTenant = lstBU[0].Booking__r.Account__c == objAcc.Id ? false : true;
                
                //Get documents
                getDocumentsList(lstBU[0].Property_City__c);
                    objFMCase.Tenant__c =  lstBU[0].Tenant__c;
                    objFMCase.Tenant_Email__c = lstBU[0].Tenant__r.Email__pc; 
                    // objFMCase.Email__c = lstBU[0].Owner__r.Email__pc;
                    objFMCase.Email__c = lstBU[0].Booking__r.Account__r.Email__pc;
                    // objFMCase.Account__c = lstBU[0].Owner__c;
                    objFMCase.Account__c = lstBU[0].Booking__r.Account__c;
               

                
                // Get Location
                objFMCase.Booking_Unit__c = lstBU[0].Id;
                objFMCase.Request_Type_DeveloperName__c = strSRType;
                String buildingCode = lstBU[0].Unit_Name__c.substringBefore('/');
                List<Location__c> lstLocations = new List<Location__c>();
                System.debug('==buildingCode==='+buildingCode);
                lstLocations = [SELECT Name, ID,
                                        (SELECT FM_User__c,FM_User__r.Name,FM_User__r.email,
                                                FM_Role__c,
                                                Day_Shift_Contact_No__c FROM FM_Users__r
                                  WHERE (FM_Role__c = 'Property Manager'
                                        OR FM_Role__c = 'FM Manager' 
                                        OR FM_Role__c = 'FM Admin'
                                        OR FM_Role__c = 'Master Community Property Manager'
                                        OR FM_Role__c = 'Master Community FM Manager'
                                        OR FM_Role__c = 'Master Community Admin'
                                        OR FM_Role__c = 'Property Director')),
                                        (SELECT Category__c,Payble_fine__c,
                                                Remedial_Period__c,Violation_Of_Rule__c
                                        FROM Violation_Rules__r)
                                FROM Location__c
                                WHERE RecordType.Name = 'Building'
                                AND Name =: buildingCode LIMIT 1];
                
                if(lstLocations.size() > 0) {
                  for(FM_User__c obj : lstLocations[0].FM_Users__r) {
                        
                        // Only FM users part of that unit are eligible to raise this SR
                        UserIds.add(obj.FM_User__c);
                    
                        // Populate details of Property Manager
                        if(obj.FM_Role__c == 'Property Manager' ) {
                                objFMCase.Property_Manager_Name__c = obj.FM_User__r.Name;
                                objFMCase.Property_Manager_Number__c = obj.Day_Shift_Contact_No__c;
                                objFMCase.Property_Manager__c = obj.FM_User__r.Id;
                                objFMCase.Property_Manager_Email__c = obj.FM_User__r.email;
                        }
                        else if(obj.FM_Role__c == 'Master Community Property Manager' ){
                                objFMCase.Property_Manager_Name__c = obj.FM_User__r.Name;
                                objFMCase.Property_Manager_Number__c = obj.Day_Shift_Contact_No__c;
                                objFMCase.Property_Manager__c = obj.FM_User__r.Id;
                                objFMCase.Property_Manager_Email__c = obj.FM_User__r.email;
                        }
                        
                        // Populate Details of FM Admin
                        if(obj.FM_Role__c == 'FM Admin') {
                            objFMCase.Admin__c =  obj.FM_User__r.Id;
                            objFMCase.FM_Admin_Email__c = obj.FM_User__r.email;
                        }
                        else if(obj.FM_Role__c == 'Master Community Admin'){
                            objFMCase.Admin__c =  obj.FM_User__r.Id;
                            objFMCase.FM_Admin_Email__c = obj.FM_User__r.email;
                        }
                        
                        // Populate Details of Property Director
                        if(obj.FM_Role__c == 'Property Director') {
                        objFMCase.Property_Director__c =  obj.FM_User__r.Id;
                        objFMCase.Property_Director_Email__c = obj.FM_User__r.email;
                        }

                    }//for

                   
                    //Get Violation names
                    if(lstLocations.size() > 0 && lstLocations[0].Violation_Rules__r.size() > 0) {
                        getCategories(lstLocations[0].Violation_Rules__r);
                        locationId = lstLocations[0].Id;
                    }
                } 

                // Removed this check while deploying to prod ths check is used to give access to raise SR to specific users
                 System.debug('==UserIds==='+UserIds);
                /*if(! UserIds.contains(UserInfo.getUserId())){ 
                    isError = true;
                    ApexPages.addmessage(new ApexPages.message
                    (ApexPages.severity.Error, 'You are not eligible to send violation notice'));
                }*/
            }
        }
    }//constructor
    public void getDocumentsList(String propertycity) {
        try {
            lstDocuments = new list<FM_Documents__mdt>();
            String query = 'SELECT DeveloperName,Document_Name__c,MasterLabel,Mandatory__c,Type__c FROM FM_Documents__mdt ';
            query += 'WHERE Process__r.MasterLabel = \'Violation Notice\' ';
            Set<String> cityValues = new Set<String>();
            cityValues.add('All');
            cityValues.add(propertycity);
            query += 'AND Is_Active__c = true AND City__c IN : cityValues ';
            query += 'AND Submitted_By__c = ';
            query += isTenant ? '\'Tenant\'' : '\'Owner\'';
            System.debug('===Database.query(query)==' +Database.query(query));
            for(FM_Documents__mdt mdt : Database.query(query)) {
                lstDocuments.add(mdt);
            }//for
            System.debug('===lstDocuments==' +lstDocuments);
        }
        catch(System.Exception excp) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
    }
    public void deleteDocument() {
        try{
            //docIdToDelete contains Document id to delete and its name resp separated by ','
            List<String> tempLst = docIdToDelete.split(',');
            System.debug('tempLst===' + tempLst);
            delete [SELECT Id from SR_Attachments__c where Id=:tempLst[0]];
            uploadedDocuments = [SELECT Type__c,Name, View__c,Attachment_URL__c
                                FROM SR_Attachments__c
                                WHERE FM_Case__c =:objFMCase.Id];
          FM_Documents__mdt objMdt = [SELECT Id, MasterLabel, Is_Active__c, Mandatory__c,Document_Name__c,
                                    Type__c,DeveloperName
                                from FM_Documents__mdt
                                WHERE Document_Name__c =: tempLst[1] LIMIT 1];
            if(objMdt != NULL) {
              lstDocuments.add(objMdt);
            }

        }
        catch(System.Exception excp) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
    }
    
    //Multiple docs upload start
    public transient String strDocumentBody { get; set; }
    public transient String strDocumentName { get; set; }
    public  Integer             attchmentIndex      {get; set;}
    public  Integer             attachCounter = 0;

    public void removeDocument() {
        System.debug('===attchmentIndex======' + attchmentIndex);
        if(attchmentIndex != NULL) {
            lstDocuments.remove(attchmentIndex);
        }
    }

    public void addDocument() {
        attachCounter++;
        String attName = 'Attachment '+ attachCounter;
        lstDocuments.add(new FM_Documents__mdt(Document_Name__c=attName, Mandatory__c=false));
        // showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
    }
    public pagereference uploadDocument() {
        //system.debug('== strDocumentBody =='+strDocumentBody);
        system.debug('== strDocumentName =='+strDocumentName);
        //initializeFMCase();
        System.debug('objFMCase.id======'+objFMCase.id);
        System.debug('objFMCase======'+objFMCase);
        String fileName = Apexpages.currentPage().getParameters().get('fileName');
        System.debug('fileName======'+fileName);
        strAccountId= ApexPages.currentPage().getParameters().get('AccountId');
        strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
        System.debug('strSelectedUnit======'+strSelectedUnit);
        booking_unit__c objUnit = [select id,Registration_Id__c from booking_unit__c  where id =: strSelectedUnit ];
        System.debug('objUnit======'+objUnit);
        
        List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
        lstFMCases = [SELECT Name,Booking_Unit__c,Tenant__c,Tenant__r.party_ID__C
                      FROM FM_Case__c where Id=:objFMCase.Id];
        System.debug('====lstFMCases==' + lstFMCases);
        try {
            if(lstFMCases.size() >0 &&
              String.isNotBlank(strDocumentBody) && String.isNotBlank(strDocumentName)) {
                List<UploadMultipleDocController.MultipleDocRequest> lstReq = new List<UploadMultipleDocController.MultipleDocRequest>();
                List<SR_Attachments__c> lstAttachments = new List<SR_Attachments__c>();

                SR_Attachments__c objAtt = new SR_Attachments__c();
                objAtt.Name = fileName;
                objAtt.FM_Case__c = objFMCase.Id;
                objAtt.FM_Account__c = lstFMCases[0].Tenant__c;
                objAtt.Booking_Unit__c = lstFMCases[0].Booking_Unit__c;
                objAtt.Document_Type__c  = 'Violation Notice';
                lstAttachments.add(objAtt);
                UploadMultipleDocController.MultipleDocRequest reqObj = new UploadMultipleDocController.MultipleDocRequest();
                reqObj.category = 'Document';
                reqObj.entityName = 'Damac Service Requests';
                transient blob objBlob = FM_Utility.extractBody(strDocumentBody);
                if(objBlob != null){
                    reqObj.base64Binary =  EncodingUtil.base64Encode(objBlob);
                }
                reqObj.fileDescription = fileName;
                reqObj.fileId = lstFMCases[0].Name + '-'+ String.valueOf(System.currentTimeMillis()) +'.'
                + strDocumentName.substringAfterLast('.');
                reqObj.fileName = lstFMCases[0].Name + '-'
                            + String.valueOf(System.currentTimeMillis())  +'.'+ strDocumentName.substringAfterLast('.');
                reqObj.registrationId = lstFMCases[0].Name;
                reqObj.sourceFileName = 'IPMS-'+lstFMCases[0].Tenant__r.party_ID__C+'-'+FM_Utility.extractName(strDocumentName);
                reqObj.sourceId = 'IPMS-'+lstFMCases[0].Tenant__r.party_ID__C+'-'+FM_Utility.extractName(strDocumentName);
                lstReq.add(reqObj);
                System.debug('===lstReq==' + lstReq);
                if(lstReq.size() > 0) {
                    UploadMultipleDocController.data respObj = new UploadMultipleDocController.data();
                    respObj= UploadMultipleDocController.getMultipleDocUrl(lstReq);
                    System.debug('===respObj==' + respObj);
                    if(respObj != NULL && lstReq.size() > 0) {
                        if(respObj.status == 'Exception'){
                            ApexPages.addmessage(new ApexPages.message(
                                ApexPages.severity.Error,respObj.message));
                            // errorLogger(respObj.message, '', '');
                            return null;
                       }
                       if(respObj.Data == null || respObj.Data.size()==0){
                           ApexPages.addmessage(new ApexPages.message(
                           ApexPages.severity.Error,'Problems while getting response from document upload'));
                           // errorLogger('Problems while getting response from document upload', '', '');
                           return null;
                       }
                       System.debug('===respObj.data==' + respObj.data);
                       System.debug('===lstAttachments==' + lstAttachments.size());
                       Set<SR_Attachments__c> setSRAttachments_Valid = new Set<SR_Attachments__c>();
                        for(SR_Attachments__c att : lstAttachments) {
                            for(UploadMultipleDocController.MultipleDocResponse objData : respObj.data) {
                                System.debug('===objData.PARAM_ID==' + objData.PARAM_ID);
                                System.debug('===lstAttachments==' + 'IPMS-'+lstFMCases[0].Tenant__r.party_ID__C+'-'+att.name);
                                System.debug('===objData.PARAM_ID==' + lstFMCases[0].Tenant__r.party_ID__C+'-'+FM_Utility.extractName(strDocumentName));
                                if('IPMS-'+lstFMCases[0].Tenant__r.party_ID__C+'-'+FM_Utility.extractName(strDocumentName) == objData.PARAM_ID){
                                   if(objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)){
                                       att.Attachment_URL__c = objData.url;
                                       att.FM_Case__c = objFMCase.Id;
                                       objFMCase.Violation_Notice_Attachment_URL__c = objData.url;
                                       setSRAttachments_Valid.add(att);
                                   }
                                   else{
                                       ApexPages.addmessage(new ApexPages.message(
                                       ApexPages.severity.Error,'Problems while getting response from document '+objData.PARAM_ID));
                                       // errorLogger('Problems while getting response from document '+objData.PARAM_ID, '', '');
                                       return null;
                                   }
                               }
                            }
                        }//outer for
                        System.debug('===setSRAttachments_Valid=='+setSRAttachments_Valid);
                        List<SR_Attachments__c> lstAtt = new List<SR_Attachments__c>();

                        lstAtt.addAll(setSRAttachments_Valid);
                        for(SR_Attachments__c att : lstAtt) {
                            att.FM_Case__c = objFMCase.Id;
                        }
                        upsert lstAtt;
                        System.debug('===lstAtt=='+lstAtt);
                        uploadedDocuments = [SELECT Type__c,Name, View__c,Attachment_URL__c
                                            from SR_Attachments__c
                                            WHERE FM_Case__c =:objFMCase.Id];
                        System.debug('===uploadedDocuments=='+uploadedDocuments);
                        for(Integer i=0; i<lstDocuments.size(); i++) {
                            if(lstDocuments[i].Document_Name__c == fileName) {
                                lstDocuments.remove(i);
                                break;
                            }
                        }
                        // lstDocuments.remove(lstDocuments.indexOf(fileName));//removing uploaded doc placeholder
                        System.debug('===lstDocuments=='+lstDocuments);
                    }

                }
            }
        }
        catch(System.Exception excp) {
            System.debug('====excp==' + excp);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
        return null;
    }
    //Multiple docs upload end
    
    private void getCategories(List<Violation_Rule__c> lstViolations) {
        lstCategoryOptions = new list<selectOption>();
        lstCategoryOptions.add(new selectOption('None','None'));
        for(Violation_Rule__c obj : lstViolations) {
            if(mapCat_rules.containsKey(obj.Category__c)) {
                mapCat_rules.get(obj.Category__c).add(obj);
            }
            else {
                mapCat_rules.put(obj.Category__c, new Violation_Rule__c[]{obj});
                lstCategoryOptions.add(new selectOption(obj.Category__c, obj.Category__c));
            }
        }

    }

    public void getViolations() {
        lstViolationOptions = new list<selectOption>();
        lstViolationOptions.add(new selectOption('None','None'));
        mapId_rules = new Map<String, Violation_Rule__c>();
        if(mapCat_rules.containsKey(selectedCategory)) {
            for(Violation_Rule__c obj : mapCat_rules.get(selectedCategory)) {
                lstViolationOptions.add(new SelectOption(obj.Id, obj.Violation_Of_Rule__c));
                mapId_rules.put(obj.Id,obj);
            }
        }
    }

    public void getDetails() {
        if(mapId_rules.containsKey(selectedViolation)) {
            objVR = mapId_rules.get(selectedViolation);
        }
        upsert objFMCase;
    }

    public PageReference submitSR() {
        try {
            
            // This Section added on 19/8/2019 for formating date taken from UI and store it into respective field.
            if(String.isNotBlank(violationNoticingDate)  ){
                system.debug('violationNoticingDate====='+violationNoticingDate);
                List<String> lstviolationNoticingDate = violationNoticingDate.split('/');
                objFMCase.Date_of_Noticing_Violation__c = 
                Date.newinstance(
                    Integer.valueOf(lstviolationNoticingDate[2]),
                    Integer.valueOf(lstviolationNoticingDate[1]),
                    Integer.valueOf(lstviolationNoticingDate[0])
                );
            }

            objFMCase.Status__c = 'Submitted';
            
            //Populate the Approving Authorities for Violation Case.
          //  objFMCase.Approving_Authorities__c = '';
           System.debug('objFMCase.Request_Type_DeveloperName__c::::'+objFMCase.Request_Type_DeveloperName__c);
            if( String.isNotBlank( objFMCase.Request_Type_DeveloperName__c ) ) {
                System.debug('inside objFMCase.Request_Type_DeveloperName__c::::'+objFMCase.Request_Type_DeveloperName__c);
           
                objFMCase.Approving_Authorities__c = populateApprovingAuthotities(
                    objFMCase.Request_Type_DeveloperName__c
                );
            
            }//if

            objFMCase.Violation_rule_name__c = objVR.Violation_Of_Rule__c;
            objFMCase.Payble_fine__c = objVR.Payble_fine__c;
            objFMCase.Remedial_Period__c = objVR.Remedial_Period__c;
            if(String.isNotBlank(objFMCase.Remedial_Period__c) && objFMCase.Remedial_Period__c != 'Immediate') {
                
                //Get no of days to pay fine
                Integer noOfDays = Integer.valueOf(objFMCase.Remedial_Period__c.substringBefore(' '));
                objFMCase.Payment_Date__c = System.today().addDays(noOfDays);
            }
            else if(objFMCase.Remedial_Period__c == 'Immediate') {
                objFMCase.Payment_Date__c = System.today().addDays(1);
            }
            objFMCase.Violation_Rule__c = objVR.Id;
            objFMCase.Request_Type_DeveloperName__c = isTenant ? 'Violation_Notice_T' : 'Violation_Notice';
            
            System.debug('before objFMCase::upsert ::::'+objFMCase);
            upsert objFMCase;
            System.debug('after objFMCase::::::'+objFMCase);
           
           
            
            return new PageReference('/' +objFMCase.Id);
        }
        catch(System.Exception excp) {
            ApexPages.addmessage(new ApexPages.message
            (ApexPages.severity.Error,excp.getMessage() +': ' + excp.getLineNumber()));
            Error_log__c objError = new Error_log__c();
            objError.Process_Name__c= 'Violation Notice';
            if(objFMCase.Id != NULL) {
                objError.FM_Case__c = objFMCase.Id;
            }
            objError.Location__c = locationId;
            objError.Error_Details__c = excp.getMessage() +', Line no:' + excp.getLineNumber();
            insert objError;
        }
        return NULL;
    }

    // Populate approving authorities
    public static String populateApprovingAuthotities(
        String requestType) {
            String approvingAuthorities = '';
           System.debug('--------Inside populateApprovingAuthotities--------requestType--'+requestType);
            for( FM_Approver__mdt objApproMeta : [ SELECT Id
                                                        , Role__c
                                                        , Level__c
                                                     FROM FM_Approver__mdt
                                                    WHERE FM_Process__r.DeveloperName = :requestType
                                                      AND Is_Active__c = true
                                                 //ORDER BY Level__c 
                                                 ] ) {
                System.debug('---objApproMeta:'+objApproMeta);
                approvingAuthorities = objApproMeta.Role__c ;                                        
               /* if(objApproMeta != NULL){
                    if( maplevel_Roles.containskey( objApproMeta.Level__c ) ) {
                        maplevel_Roles.get( objApproMeta.Level__c).add( objApproMeta.Role__c );
                    }
                    else {
                        maplevel_Roles.put( objApproMeta.Level__c, new list<String> { objApproMeta.Role__c } );
                    }
                }*/

            }


            //if( !maplevel_Roles.isEmpty() ) {
                //for( list<String> lstRoles : maplevel_Roles.values() ){
                  //  approvingAuthorities += String.join(lstRoles,'__') + ',' ;
               // }
            //}
            //approvingAuthorities = approvingAuthorities.removeEnd(',');
            System.debug('approvingAuthorities:::::::'+approvingAuthorities);
            return approvingAuthorities;
    }
}