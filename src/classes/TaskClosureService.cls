/*
 * Description - Called when task is closed in IPMS and relevant task is closed in SF
 *
 * Version        Date            Author            Description
 * 1.0            18/12/17        Vivek Shinde      Added logic for task closure for title deed
 */
public without sharing class TaskClosureService {
    @InvocableMethod 
    public static void verifyCaseAttachment( list<Task> lstTask ) {
        system.debug('Task Closure Service Called .......');
        system.debug('TlstTask d .......'+lstTask );
        set<Id> setCaseId = new set<Id>();
        set<String> setDocURL = new set<String>();
        Set<Id> setSRAttachId = new Set<Id>();
        
        for( Task objTask : lstTask ) {
            if( String.isNotBlank( objTask.Status ) && (objTask.Status.equalsIgnoreCase('Closed') || 
                objTask.Status.equalsIgnoreCase('Completed')) && 
                String.isNotBlank( objTask.Assigned_User__c ) && 
                ( objTask.Assigned_User__c.equalsIgnoreCase('Legal') || 
                objTask.Assigned_User__c.equalsIgnoreCase('CRE') || 
                objTask.Assigned_User__c.equalsIgnoreCase('CDC')) && 
                String.isNotBlank( objTask.Document_URL__c ) && String.valueOf( objTask.whatId ).startsWith('500') && 
                String.isNotBlank(objTask.SR_Attachment_Id__c)) {
                    
                setCaseId.add( objTask.whatId );
                setDocURL.add( objTask.Document_URL__c );
                setSRAttachId.add(objTask.SR_Attachment_Id__c);
            }
            /*else if(String.isNotBlank( objTask.Status ) && (objTask.Status.equalsIgnoreCase('Closed') || 
                objTask.Status.equalsIgnoreCase('Completed')) && 
                String.isNotBlank( objTask.Assigned_User__c ) && 
                objTask.Assigned_User__c.equalsIgnoreCase('Business Development') && 
                String.valueOf( objTask.whatId ).startsWith('500') &&
                objTask.Subject.equals('Submit details in DSR and generate NOC')) {
                
                setCaseId.add( objTask.whatId );
            }*/
        }
        
        if( !setCaseId.isEmpty() && !setDocURL.isEmpty() && !setSRAttachId.isEmpty()) {
            list<SR_Attachments__c> lstCustomAttach = [ SELECT Id
                                                        , isValid__c
                                                        , Attachment_URL__c
                                                        , Case__c
                                                     FROM SR_Attachments__c
                                                    WHERE Case__c IN :setCaseId
                                                      AND Attachment_URL__c IN :setDocURL 
                                                      AND Id IN: setSRAttachId] ;
            if( lstCustomAttach != null && !lstCustomAttach.isEmpty() ) {
                for( SR_Attachments__c objAtt : lstCustomAttach ) {
                    objAtt.isValid__c = true ;
                }
                 system.debug('lstCustomAttach  .......'+lstCustomAttach  );
                update lstCustomAttach ;
            }
        }
        
        /*else if(!setCaseId.isEmpty()) {
            List<Task> lstTaskToInsert = new List<Task>();
            List<User> lstUser = [Select Id From User Where UserRole.Name = 'SVP Operations'];
            
            if(lstUser != null && !lstUser.isEmpty()) {
                for(Id idCase: setCaseId) {
                    Case objCase = new Case(Id = idCase);
                    Task objTask = TaskUtility.getTask((SObject)objCase, 'Verify and approve NOC', 'SVP Operations', 
                        'Title Deed', system.today().addDays(1));
                    objTask.OwnerId = lstUser[0].Id;
                    lstTaskToInsert.add(objTask);
                }
                
                insert lstTaskToInsert;
            }
        }*/
    }
}