public without sharing class FmcReferAFriendController {

    public Inquiry__c   inquiry     {get; set;}
    public Boolean      savedLead   {get; set;}

    public FmcReferAFriendController() {
        if (FmcUtils.isCurrentView('ReferFriend')) {
            inquiry = new Inquiry__c();
            savedLead = false;
        }
    }

    public void save() {
        savedLead = false;
        try {
            Id inquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
            List<Campaign__c> lstCampaign = [
                SELECT  Id, Campaign_Name__c
                FROM    Campaign__c
                WHERE   Campaign_Name__c = 'Be My Neighbour'
                LIMIT   1
            ];
            inquiry.Mobile_Phone_Encrypt__c = inquiry.Mobile_Phone__c;
            inquiry.RecordTypeId = inquiryRecordTypeId;
            inquiry.Primary_Contacts__c = 'Mobile Phone';
            inquiry.Preferred_Language__c = 'English';
            inquiry.Inquiry_Source__c = Label.Be_my_Colleague_Inquiry_Source;
            inquiry.Referred_Customer__c = CustomerCommunityUtils.customerAccountId;
            if (!lstCampaign.isEmpty()) {
                inquiry.Campaign__c = lstCampaign[0].Id;
            }
            System.debug('inquiry = ' + JSON.serialize(inquiry));
            if (inquiry != null) {
                insert inquiry;
                savedLead = true;
                inquiry = new Inquiry__c();
            }
        } catch(Exception e) {
            System.debug('Exception: ' + e.getMessage() + e.getStackTraceString() );
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Some error occured while saving response'));
        }
    }

}