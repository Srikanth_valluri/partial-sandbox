/*
Created Date: 15/06/2020
Batch Class: Damac_CardExpiryNotificationBatch_Acc
Test Class: Damac_CardExpiryNotificationBatchTest
Description: In this while creating object for batch class parameters are given, depending on this execution is done
*/
global class Damac_CardExpiryNotificationSch_Acc implements Schedulable {
    global void execute(SchedulableContext sc) {
        
        Damac_CardExpiryNotificationBatch_Acc cen = new Damac_CardExpiryNotificationBatch_Acc();
        Database.executeBatch(cen, 1);
    }
}