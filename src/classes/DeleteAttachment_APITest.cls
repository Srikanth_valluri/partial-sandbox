@isTest
public class DeleteAttachment_APITest {
    @isTest
    static void testDeleteAttach(){
        SR_Attachments__c objAttach = new SR_Attachments__c();
        insert objAttach;
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/deleteDocFromSF';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        DeleteAttachment_API.deleteAttachment();
        Test.stopTest();
    }
    
    @isTest
    static void testDeleteAttachId(){
        SR_Attachments__c objAttach = new SR_Attachments__c();
        insert objAttach;
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/deleteDocFromSF';  
        req.addParameter('attachmentId', '');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        DeleteAttachment_API.deleteAttachment();
        //DeleteAttachment_API.cls_data();
       // DeleteAttachment_API.cls_meta_data();
        Test.stopTest();
    }
    
    @isTest
    static void testDeleteAttachId1(){
        SR_Attachments__c objAttach = new SR_Attachments__c();
        insert objAttach;
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/deleteDocFromSF';  
        req.addParameter('attachmentId', objAttach.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        DeleteAttachment_API.deleteAttachment();
        //DeleteAttachment_API.cls_data();
       // DeleteAttachment_API.cls_meta_data();
        Test.stopTest();
    }
}