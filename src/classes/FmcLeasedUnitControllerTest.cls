@isTest
private class FmcLeasedUnitControllerTest {

    @isTest
    static void testController() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Building_Name__c = 'Test Building',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_Name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'RequestLeasedUnit');
        Test.setCurrentPage(currentPage);

        Test.startTest();
            System.runAs(portalUser) {
                FmcLeasedUnitController controller = new FmcLeasedUnitController();
                controller.req.Booking_Unit__c = lstBookingUnit[0].Id;
                controller.submitRequest();
            }
        Test.stopTest();
    }

}