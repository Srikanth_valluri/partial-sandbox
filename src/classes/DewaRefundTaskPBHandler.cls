public with sharing class DewaRefundTaskPBHandler {
    @InvocableMethod
    public static void updateApprovaingAuthorities(List<Task> lstSR) {
       List<Id> caseIds = new List<Id>();
       List<Case> caseList = new list<Case>();
       
       for(Task objTask : lstSR) {
        caseIds.add(objTask.WhatId);
       }
    if(caseIds != null && caseIds.size() > 0) {
        List<Case> objCaseLst = [ Select 
                              Id
                              ,OwnerId
                              ,Parking_Details_JSON__c 
                              ,Booking_unit__c
                              ,Booking_unit__r.No_of_parking__c 
                              ,Booking_unit__r.Parking_Bay_No__c 
                              ,Submit_for_Approval__c 
                              FROM
                              Case
                              Where Id =: caseIds
                            ];
                            System.debug('objCaseLst>>>'+objCaseLst);
        for(Case objCase : objCaseLst) {
           
            String approver = '';
            String strApprovalAuth = SfRuleEngine.filterSfRuleEngine('DEWA Refund', objCase.Id);
            System.debug('strApprovalAuth>>>'+strApprovalAuth);
            if (strApprovalAuth != null && !String.isBlank(strApprovalAuth) ){

                    approver = strApprovalAuth;
                    system.debug('approver'+approver);
         
            }
             if (!string.isBlank(approver)) {
                objCase.Roles_from_Rule_Engine__c = approver;
                objCase.Approving_Authorities__c = approver;
                objCase.Submit_for_Approval__c = true;
             }
             caseList.add(objCase);
        }
        
        if(caseList != null && caseList.size() > 0) {
            update caseList;
        }
        System.debug('After Update objCaseLst>>>'+caseList);
    }

    
        
    }
}