public class TaskAddErrorhandler{
    public static void validationBeforeUpdate(Map<Id,Task> mapNew, Map<Id,Task> mapOld){
        Map<Id,Task> mapFMCaseIdTaskId = new Map<Id,task>();
        for( Task taskIns : mapNew.values() ) {
            if( String.isNotBlank(taskIns.subject) &&
                ( taskIns.Subject=='Generate Work Permit' || taskIns.Subject == Label.NOC_FM_Admin_Task) &&
                (taskIns.status=='Closed' || taskIns.status=='Completed') &&
                !CheckRecursive.SetOfIDs.contains(taskIns.id) && (mapOld.get(taskIns.id).status != taskIns.status) ) {
               mapFMCaseIdTaskId.put(taskIns.whatId,taskIns);
               CheckRecursive.SetOfIDs.add(taskIns.id);
            }
        }

        List<SR_Attachments__c> lstDocs = [select Attachment_URL__c
                                                , isValid__c
                                                , fm_case__r.id
                                             from SR_Attachments__c
                                            where name IN ('Work Permit', 'NOC')
                                              AND FM_Case__r.id IN :mapFMCaseIdTaskId.keyset()
                                              AND Attachment_URL__c = ''];
        //System.debug('lstDocs-------'+lstDocs);
        if( !lstDocs.isEmpty() ){
            for( SR_Attachments__c insSRAttachment :lstDocs ){
            Task actualRecord=mapFMCaseIdTaskId.get(insSRAttachment .fm_case__r.id);
            actualRecord.addError('Please generate the document first and attach it to the Case.');
            }
        }
    }

    public static void validationBeforeClosingTask(Map<Id,Task> mapNew, Map<Id,Task> mapOld){
        Map<Id,Task> mapFMCaseIdTaskId = new Map<Id,task>();
        for( Task taskIns : mapNew.values() ) {
            if( String.isNotBlank(taskIns.subject) &&
                 taskIns.Subject==label.FM_Access_Card_Issue_task_label  && 
                (taskIns.status=='Closed' || taskIns.status=='Completed') && taskIns.process_name__c == 'Request For Access Card' &&
                !CheckRecursive.SetOfIDs.contains(taskIns.id) && (mapOld.get(taskIns.id).status != taskIns.status) ) {
               mapFMCaseIdTaskId.put(taskIns.whatId,taskIns);
               CheckRecursive.SetOfIDs.add(taskIns.id);
            }
        }
        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Request For Access Card').getRecordTypeId();
        List<FM_Case__c> lstFMCase = [select Access_card_number__c
                                            ,Returned_Disabled_Access_Card_No__c
                                            ,Type_of_Access_Card__c
                                        from FM_Case__c
                                        where recordtypeid =:devRecordTypeId
                                        AND ID IN :mapFMCaseIdTaskId.keyset()];

         if(!lstFMCase.isEmpty()){
             for(FM_Case__c insFMCase : lstFMCase){
                 Task actualRecord =new Task();
                 if(String.isEmpty(insFMCase.Access_card_number__c)){
                     actualRecord=mapFMCaseIdTaskId.get(insFMCase.id);
                     actualRecord.addError('Please fill Access Card Number field on FM Case');
                 }
                 if(String.isEmpty(insFMCase.Returned_Disabled_Access_Card_No__c) && insFMCase.Type_of_Access_Card__c == 'Replacement'){
                     actualRecord=mapFMCaseIdTaskId.get(insFMCase.id);
                     actualRecord.addError('Please fill Returned/Disabled Access Card No. field on FM Case');
                 }
             }

         }
    }
}