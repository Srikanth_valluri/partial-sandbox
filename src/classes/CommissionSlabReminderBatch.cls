/**************************************************************************************************
* Name         : CommissionSlabReminderBatch
* Test Class  : 
* Description: Batch class to send email notification to Account owner related to Commissions Slab
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR      DATE
* 1.0         QBurst      17/11/2020
**************************************************************************************************/
public class CommissionSlabReminderBatch implements Database.Batchable<sObject>{

    /*********************************************************************************************
    * @Description: Implementing the start method of batch interface, contains query.
    * @Params      : Database.BatchableContext
    * @Return      : Database.QueryLocator
    *********************************************************************************************/  
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id, Name, Account_Owner__c, Owner.Email
                                         FROM Account 
                                         WHERE RecordType.Name = 'Corporate Agency'
                                         AND Agency_Type__c = 'Corporate']); 
    }

    /*********************************************************************************************
    * @Description: Implementing the execute method of the batch interface, contains the criteria.
    * @Params      : Database.BatchableContext, List<sObject>
    * @Return: void
    *********************************************************************************************/
    public void execute(Database.BatchableContext BC, List<Account> scope){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Id templateId =  [select id, name from EmailTemplate where developername = 'Current_Commission_Slab_Reminder_BD_Team'].id; 
        Email_Notification_To_CC_List__c toCCList = Email_Notification_To_CC_List__c.getValues('Commission Slab Update');
        Map<Id, Account> accMap = new Map<Id, Account>();
        Map<Id, Account> accUpdMap = new Map<Id, Account>();
        system.debug('Scope Size: ' + scope.size());
        for(Account acc: scope){
            accMap.put(acc.Id, acc);
        }
        system.debug('accMap Size: ' + accMap.size());
        for(New_Partnership_Program__c prg: [SELECT Id, Account__c, Current_commission_slab__c
                                                 FROM New_Partnership_Program__c 
                                                 WHERE Account__c IN: accMap.keyset()
                                                 AND Current_commission_slab__c != NULL
                                                 AND Current_commission_slab__c != '' ]){
            accUpdMap.put(prg.Account__c, accMap.get(prg.Account__c));
        }
        system.debug('accUpdMap Size: ' + accUpdMap.size());
        for(Account acc: accUpdMap.values()){
            List<String> toAddresses = new List<String>();
            List<String> ccAddresses = new List<String>();
            List<String> bccAddresses = new List<String>();
            
            if(toCCList != null){
                if(toCCList.To_Addresses__c != null){
                    for(String email: toCCList.To_Addresses__c.split(',')){
                        toAddresses.add(email.trim());
                    }
                }
                if(toCCList.CC_Addresses__c != null){
                    for(String email: toCCList.CC_Addresses__c.split(',')){
                        ccAddresses.add(email.trim());
                    }
                }
                if(toCCList.BCC_Addresses__c != null){
                    for(String email: toCCList.BCC_Addresses__c.split(',')){
                        bccAddresses.add(email.trim());
                    }
                }
            }
            Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(templateId, acc.ownerId, acc.Id);
            mail.setToAddresses(toAddresses);
            mail.setCCAddresses(ccAddresses);
            mail.setBCCAddresses(bccAddresses);
            mail.setTargetObjectId(acc.ownerId);
            mail.setSenderDisplayName('Damac Property ');
            mail.setReplyTo('noreply@Damacgroup.com');
            mail.setTemplateId(templateId);
            mail.setSaveAsActivity(false);
            mails.add(mail);  
            system.debug('mail: ' + mail);
        }
        Messaging.sendEmail(mails);
    }

    /*********************************************************************************************
    * @Description: Implementing the Finish method, to send an email after job completion.
    * @Params      : Database.BatchableContext
    * @Return: void
    *********************************************************************************************/
    public void finish(Database.BatchableContext BC){
    }
}// End of class.