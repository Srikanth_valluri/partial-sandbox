/**************************************************************************************************
* Name               : Damac_UpdateBuyers_Test
=* Description       : Test Class for Damac_UpdateBuyers
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0                         28/06/2018      Created
* 1.1         QBurst          08/04/2020      SOAP to REST Changes
**************************************************************************************************/
@istest
public class Damac_UpdateBuyers_Test {
    private static List<Account> accountList = new List<Account>();
    private static Inquiry__c inquiry = new Inquiry__c();
    private static void init(){
        accountList = TestDataFactory.createAccountRecords(new List<Account>{new Account()});
        Inquiry_Conversion_Mapping__c icmObject = new Inquiry_Conversion_Mapping__c();
        icmObject.Name = 'First_Name__c';
        icmObject.Business_Account_Field_Name__c = 'Name';
        icmObject.Business_Contact_Field_Name__c = 'FirstName';
        icmObject.Buyer__c = 'First_Name__c';
        icmObject.Order__c = 1;
        icmObject.Person_Account_Field_Name__c = '';
        insert icmObject;
        inquiry = new Inquiry__c(
            First_Name__c = 'Test',
            Last_Name__c = 'Last Test',
            Inquiry_Source__c = 'Call Center',
            Inquiry_Status__c = 'Active',
            Email__c = 'test@gmail.com',
            OwnerId = UserInfo.getuserid()
        );
        insert inquiry;
        Account acc = new Account(Id = accountList[0].Id);
        acc.Inquiry__c = inquiry.Id;
        update acc;
    }

    @isTest 
    static void Test1() {
        init();
        List<NSIBPM__SR_Template__c> SRTemplateList 
                  = InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
        Id RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        Account a = new Account();
        a.recordtypeid = RecordTypeIdAGENT;
        a.Name = 'Test Account';
        a.Agency_Short_Name__c = 'testShrName';
        insert a;  

        Id RecordTypeIdContact = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c serviceReq = new NSIBPM__Service_Request__c();
        serviceReq.recordtypeid = RecordTypeIdContact;
        serviceReq.NSIBPM__SR_Template__c = SRTemplateList[0].Id;
        serviceReq.Agency__c = a.Id;
        serviceReq.ID_Type__c = 'Passport';
        serviceReq.country_of_sale__c = 'UAE';
        insert serviceReq;

        NSIBPM__Step__c stp = new NSIBPM__Step__c();
        stp.NSIBPM__SR__c = serviceReq.Id;
        insert stp;

        NSIBPM__Document_Master__c docMaster = new NSIBPM__Document_Master__c();
        docMaster.name = 'test';
        docMaster.Promotion_Letter__c = true;
        insert docMaster;

        Promotion__c promo = new Promotion__c();
        promo.Promotion_Title__c = 'test';
        insert promo;
        Booking__c bk = new Booking__c();
        bk.Deal_SR__c = serviceReq.Id;
        bk.Booking_channel__c = 'Office';
        insert bk;

        Location__c loc = new Location__c();
        loc.Location_ID__c = '123';
        insert loc;
        Inventory__c inv = new Inventory__c();
        inv.Unit_Location__c = loc.Id;
        insert inv;

        Booking_Unit__c bookUnit = new Booking_Unit__c ();
        bookUnit.Booking__c = bk.Id;
        bookUnit.Inventory__c = inv.Id;
        bookUnit.Related_Promotion__c = promo.Id;
        insert bookUnit;

        Buyer__c primaryBuyer = new buyer__c();
        primaryBuyer.Buyer_Type__c = 'Individual';
        primaryBuyer.Address_Line_1__c = 'Ad1';
        primaryBuyer.Country__c = 'United Arab Emirates';
        primaryBuyer.City__c = 'Dubai';
        primaryBuyer.Date_of_Birth__c = string.valueof(system.today().addyears(-30)) ;
        primaryBuyer.Email__c = 'test@test.com';
        primaryBuyer.First_Name__c = 'firstname' ;
        primaryBuyer.Last_Name__c = 'lastname';
        primaryBuyer.Nationality__c = 'Indian' ;
        primaryBuyer.Passport_Expiry_Date__c = '25/06/2019';//string.valueof(system.today().addyears(20)) ;
        primaryBuyer.Passport_Expiry__c = system.today().addyears(20);
        primaryBuyer.DOB__c = system.today().addyears(-30);
        primaryBuyer.CR_Registration_Expiry_Date__c = '25/06/2019';//string.valueof(system.today().addyears(21));
        primaryBuyer.Passport_Number__c = 'J0565556' ;
        primaryBuyer.Phone__c = '569098767' ;
        primaryBuyer.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        primaryBuyer.Place_of_Issue__c =  'India';
        primaryBuyer.Title__c = 'Mr';
        primaryBuyer.booking__c = bk.id;
        primaryBuyer.Primary_Buyer__c = true;
        primaryBuyer.Party_Id__c = '12131231';
        primaryBuyer.Address_Changed__c = true;
        primaryBuyer.Account__c = accountList[0].Id;
        primaryBuyer.Inquiry__c = inquiry.Id;
        insert primaryBuyer;

        Buyer__c jointBuyer = new Buyer__c();
        jointBuyer.Buyer_Type__c = 'Individual';
        jointBuyer.Address_Line_1__c = 'Ad1';
        jointBuyer.Country__c = 'United Arab Emirates';
        jointBuyer.City__c = 'Dubai';
        jointBuyer.Date_of_Birth__c = string.valueof(system.today().addyears(-30));
        jointBuyer.Email__c = 'test@test.com';
        jointBuyer.First_Name__c = 'firstname';
        jointBuyer.Last_Name__c = 'lastname';
        jointBuyer.Nationality__c = 'Indian';
        jointBuyer.Passport_Expiry_Date__c = '25/06/2019';//string.valueof(system.today().addyears(20));
        jointBuyer.CR_Registration_Expiry_Date__c = '25/06/2019';//string.valueof(system.today().addyears(21));
        jointBuyer.Passport_Number__c = 'J0565556' ;
        jointBuyer.Phone__c = '569098767' ;
        jointBuyer.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        jointBuyer.Place_of_Issue__c = 'India';
        jointBuyer.Title__c = 'Mr';
        jointBuyer.booking__c = bk.Id;
        jointBuyer.Primary_Buyer__c = false;
        jointBuyer.status__c = 'New';
        jointBuyer.Account__c = accountList[0].Id;
        jointBuyer.Inquiry__c = inquiry.Id;
        insert jointBuyer;

        Damac_UpdateBuyers.updateToIPMS (primaryBuyer.Id);
        Damac_UpdateBuyers.updateArabicDetailsToIPMS (primaryBuyer.Id);
    }
}