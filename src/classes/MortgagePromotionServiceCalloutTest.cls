@isTest
public class MortgagePromotionServiceCalloutTest {
    @testSetup public static void setup(){
        Credentials_Details__c objCredDetails = new Credentials_Details__c();
        objCredDetails.User_Name__c = 'testUser';
        objCredDetails.Name = 'Promotion Name';
        objCredDetails.Password__c = 'testPsw';
        objCredDetails.Endpoint__c = 'http://83.111.194.181:8033/webservices/rest/XXDC_PROCESS_SERVICE_WS/process/';
        Insert objCredDetails;
    }
    public static Integer RESPONSE_CODE = 200;
    public static String RESPONSE_HEADER_KEY = 'Content-Type';
    public static String RESPONSE_HEADER_VALUE = 'application/json';
    public static String RESPONSE_BODY = '{"body": "Test"}';
    
    @isTest
    public static void testGetPromotionNames() {
        Test.startTest();
        // Setting up the mock response
        Test.setMock(HTTPCalloutMock.class, new GetPromotionNameMock());
        // Performing the API callout
        MortgagePromotionService.GetPromotionName('111480');
        // Verifying the response information
        //System.assertNotEquals(Null, SPONSE_CODE, response);
        Test.stopTest();
    }
}