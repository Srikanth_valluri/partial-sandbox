/*
 * Description: Class used to generate Payment Plan and Payment Terms for Booking Units
 */

public class UpdatePaymentPlanPageExtension {
    public Booking_Unit__c objBookingUnit;
    public List<Payment_Terms__c> lstPaymentTerms;
    public Payment_Plan__c objPaymentPlan;
    
    public UpdatePaymentPlanPageExtension(ApexPages.StandardController controller) {
        lstPaymentTerms = new List<Payment_Terms__c>();
        objPaymentPlan = new Payment_Plan__c();
        objBookingUnit = (Booking_Unit__c)controller.getrecord();
    }

    /**
     * Method used to create the Payment Plan and Payment Terms
     */
    public PageReference generatePaymentPlanAndTerms() {
        
        objBookingUnit = [SELECT Id, Registration_ID__c, Unit_Selling_Price_AED__c FROM Booking_Unit__c WHERE Id =: objBookingUnit.Id];

        // Check if the Registration Id is not null
        if (objBookingUnit.Registration_ID__c != null) {
            MileStonePaymentDetailsWrapper.MileStonePaymentDetails objMileStonePaymentDetails = new MileStonePaymentDetailsWrapper.MileStonePaymentDetails();
            
            // Callout is done to IPMS to fetch the new Payment terms
            objMileStonePaymentDetails = AOPTMQService.getMilestonePaymentDetails(objBookingUnit.Registration_ID__c);

            // Iterate over the response recieved from callout and create the Payment Plan and Payment Term records
            if (objMileStonePaymentDetails != null) {
                if(String.isNotBlank(objMileStonePaymentDetails.customErrorMsg)) {
                    Error_Log__c objErrorLog = new Error_Log__c(Booking_Unit__c = objBookingUnit.Id, Error_Details__c='Error - Fetching New Payment Terms: '+objMileStonePaymentDetails.customErrorMsg);
                    insert objErrorLog;
                    
                    ApexPages.addmessage(new ApexPages.message(
                        ApexPages.severity.Error,'An Exception occured during execution. Please contact System Administrator for furthur details'));
                    return null;
                }
                else if(String.isNotBlank(objMileStonePaymentDetails.status) && objMileStonePaymentDetails.status.equalsIgnoreCase('e')) {
                    Error_Log__c objErrorLog = new Error_Log__c(Booking_Unit__c = objBookingUnit.Id, Error_Details__c='Error - Response For New Payment Terms: '+objMileStonePaymentDetails.message);
                    insert objErrorLog;
                    
                    ApexPages.addmessage(new ApexPages.message(
                        ApexPages.severity.Error,'An Exception occured during execution. Please contact System Administrator for furthur details'));
                    return null;
                }
            
                if (objMileStonePaymentDetails.REG_TERM_PYMNT_TABLE != null) {
                    System.debug('1.objMileStonePaymentDetails.REG_TERM_PYMNT_TABLE:------- '+objMileStonePaymentDetails.REG_TERM_PYMNT_TABLE);
                    for (MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE objPaymentTermsWrapper : objMileStonePaymentDetails.REG_TERM_PYMNT_TABLE) {
                        System.debug('2.objPaymentTermsWrapper:-- '+objPaymentTermsWrapper);
                        Payment_Terms__c objPaymentTerm = new Payment_Terms__c();
                        objPaymentTerm.Registration_Id__c = objPaymentTermsWrapper.REGISTRATION_ID;
                        if (String.isNotBlank(objPaymentTermsWrapper.DUE_DATE)) {
                            objPaymentTerm.Expected_Date__c = AOPTUtility.convertDateFormat(objPaymentTermsWrapper.DUE_DATE);
                        }
                        objPaymentTerm.Percent_Value__c = objPaymentTermsWrapper.MILESTEON_PERCENT_VALUE;
                        if (objBookingUnit.Unit_Selling_Price_AED__c != null && String.isNotBlank(objPaymentTermsWrapper.MILESTEON_PERCENT_VALUE) ) {
                            objPaymentTerm.Payment_Amount__c = String.valueOf((objBookingUnit.Unit_Selling_Price_AED__c * Decimal.valueOf(objPaymentTermsWrapper.MILESTEON_PERCENT_VALUE))/100);
                        }
                        objPaymentTerm.Milestone_Event_Arabic__c = objPaymentTermsWrapper.MILESTONE_EVENT_AR;
                        objPaymentTerm.Description__c = objPaymentTermsWrapper.DESCRIPTION;
                        objPaymentTerm.Milestone_Event__c = objPaymentTermsWrapper.MILESTONE_EVENT;
                        objPaymentTerm.Installment__c = objPaymentTermsWrapper.INSTALLMENT;
                        // objPaymentTerm.TERM_ID__c = objPaymentTermsWrapper.TERM_ID;
                        lstPaymentTerms.add(objPaymentTerm);
                    }
                    System.debug('3.lstPaymentTerms :---------- '+lstPaymentTerms);
                    
                    // Creation of Payment Plan & Payment Terms records
                    if (lstPaymentTerms.size() > 0) {
                        objPaymentPlan.Effective_From__c = System.Today();
                        objPaymentPlan.Booking_Unit__c = objBookingUnit.Id;
                        objPaymentPlan.Status__c = 'Active';
                        List<Payment_Plan__c> lstPaymentPlans = [SELECT Id, Status__c FROM Payment_Plan__c WHERE Booking_Unit__c =: objBookingUnit.Id];
                        if (lstPaymentPlans.size() > 0) {
                            for (Payment_Plan__c objOldPaymentPlan : lstPaymentPlans) {
                                objOldPaymentPlan.Status__c = 'InActive';
                                objOldPaymentPlan.Effective_To__c = System.Today();
                            }
                            update lstPaymentPlans;
                        }
                        insert objPaymentPlan;
                        for (Payment_Terms__c objPaymentTerms: lstPaymentTerms) {
                            objPaymentTerms.Payment_Plan__c = objPaymentPlan.Id;
                            System.debug('4.objPaymentTerms:---------- '+objPaymentTerms);
                        }
                        System.debug('5.lstPaymentTerms :---------- '+lstPaymentTerms);
                        insert lstPaymentTerms;
                    }
                }
            } else {
                Error_Log__c objErrorLog = new Error_Log__c(Booking_Unit__c = objBookingUnit.Id, Error_Details__c='Error - Fetching New Payment Terms');
                insert objErrorLog;
                
                ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Error,'An Exception occured during execution. Please contact System Administrator for furthur details'));
                return null;
            }
        }
        
        // Redirect to Payment Plan detail page
        PageReference paymentPlanDetailPage = new PageReference ('/'+objBookingUnit.Id);
        paymentPlanDetailPage.setRedirect(true);
        return paymentPlanDetailPage;
    }
}