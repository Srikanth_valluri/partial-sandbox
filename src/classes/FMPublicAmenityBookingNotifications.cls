/*-------------------------------------------------------------------------------------------------
Description: to send email to Amenity Admin and Amenity Manager for public amenity booking
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 10-03-2019       | Lochana Rajput   | 1. Added logic to send email
=============================================================================================================================
*/
public without sharing class FMPublicAmenityBookingNotifications {
	//Invoked from 'FM Public Amenity Booking' process builder
	@InvocableMethod
	public static void sendNotificationEmails(List<Id> lstFMCaseIds) {
		List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
		lstFMCases = [SELECT Id,Resource__r.Amenity_Admin__r.Email,
							Resource__r.Amenity_Manager__r.Email,
							Amenity_Booking_Status__c,
							Resource__c
						FROM FM_Case__c
						WHERE ID IN: lstFMCaseIds];
		List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
	    Map<String, Id> mapStatus_Id = new Map<String, Id>();
	    for(EmailTemplate et : [SELECT Id,DeveloperName FROM EmailTemplate
	            WHERE DeveloperName IN: Label.FM_AmenityComonTemplateNames.split(',')]) {
	      	if(et.DeveloperName == 'FM_Amenity_Rejected_Common_Template') {
	        	mapStatus_Id.put('Rejected',et.id);
	      	}
	      	else if(et.DeveloperName == 'FM_Amenity_Cancelled_Common_Template') {
	        	mapStatus_Id.put('Cancelled',et.id);
	      	}
	      	else if(et.DeveloperName == 'FM_Amenity_Confirmation_Common_Template') {
	        	mapStatus_Id.put('Booking Confirmed',et.id);
	      	}
	  	}//for
	    Contact objCon = [SELECT Id FROm Contact LIMIT 1];
		for(FM_Case__c objFMCase : lstFMCases) {
			Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
			email.setToAddresses(new List<String>{objFMCase.Resource__r.Amenity_Admin__r.Email,
											objFMCase.Resource__r.Amenity_Manager__r.Email});
	        if(objFMCase.Amenity_Booking_Status__c == 'Booking Confirmed') {
	          if(mapStatus_Id.containsKey(objFMCase.Amenity_Booking_Status__c)) {
	            email.setTemplateId(mapStatus_Id.get(objFMCase.Amenity_Booking_Status__c));
	          }
	        }
	        else if(objFMCase.Amenity_Booking_Status__c == 'Cancelled') {
	          if(mapStatus_Id.containsKey(objFMCase.Amenity_Booking_Status__c)) {
	            email.setTemplateId(mapStatus_Id.get(objFMCase.Amenity_Booking_Status__c));
	          }
	        }
	        else if(objFMCase.Amenity_Booking_Status__c == 'Rejected') {
	          if(mapStatus_Id.containsKey(objFMCase.Amenity_Booking_Status__c)) {
	            email.setTemplateId(mapStatus_Id.get(objFMCase.Amenity_Booking_Status__c));
	          }
	        }
	        email.setWhatId(objFMCase.Id);
	        email.setTargetObjectId(objCon.Id);
	        email.setTreatTargetObjectAsRecipient(false);
	        email.setSaveAsActivity(false);
	        emailList.add(email);
		}//for
		//Send email
     Messaging.SendEmailResult[] emailResults= Messaging.sendEmail(emailList);
     List<Error_Log__c> lstErrorLogs = new List<Error_Log__c>();
     for(Messaging.SendEmailResult er : emailResults) {
       for(Messaging.SendEmailError error : er.getErrors()) {
         lstErrorLogs.add(new Error_Log__c(Process_Name__c = 'Amenity Booking',
         Error_Details__c = error.getMessage()));
       }
     }//for
     insert lstErrorLogs;
	}//method
}