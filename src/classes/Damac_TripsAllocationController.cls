/*************************************************************
    * Class Name    : Damac_TripsAllocationController
    * Written By    : Srikanth V
    * Methods       : init
                    : assignDriverToTrip
                    : assignCarToTrip
    * Description   : To allocate Driver and Car to the Trip
*************************************************************/
Public class Damac_TripsAllocationController{

    public List<Trip__c> myTrips { get; set; }
    public List<Trip__c> myPendingTrips { get; set; }
    public List<Driver__c> availableDrivers { get; set; }
    public List <Driver__c> allocatedDrivers { get; set; }
    
    public Trip__c trip { get; set; }
    public Trip__c rescheduleTrip { get; set; }
    public List<Trip__c> allocatedTrips { get; set; }
    
    public List<Trip__c> inprogressTrips { get; set; }
    public List<Trip__c> completedTrips { get; set; }
    public List<Trip__c> cancelledTrips { get; set; }
    public Map<Id, String> tripStartTime { get; set; }
    public Map<Id, String> tripCreatedTime { get; set; }
    public Map<id, String> tripCancelledTime { get; set; }
    public String allTrips { get; set; }
    public Map <ID, Integer> tripsAllocatedToDriver { get; set; }
    
    
    public List <SelectOption> getAllTripsOptions () {
        List <SelectOption> options = new List <SelectOption> ();
        options.add (new SelectOption ('Past', 'Past 30 Trips'));
        options.add (new SelectOption ('Upcoming', 'Upcoming 30 Trips'));
        return options;    
    }
    
    public Damac_TripsAllocationController(){
        cancelledTrips = new List<Trip__c>();
        inprogressTrips = new List<Trip__c>();
        completedTrips = new List<Trip__c>();
        allocatedTrips = new List<Trip__c>();
        rescheduleTrip = new Trip__c();
        tripStartTime = new Map<Id, String> ();
        tripCreatedTime = new Map <Id, String> ();
        tripsAllocatedToDriver = new Map <ID, Integer> ();
        trip = new Trip__c();
        trip.Trip_Date__c = Date.Today();
        allTrips = 'Upcoming';
        init();
    }

    /*
        * Method Name: init
        * Return Type: Void
        * Description: To display the not assigned trips, available trips and drivers
    */
    public void init(){
        
        cancelledTrips = new List<Trip__c>();
        inprogressTrips = new List<Trip__c>();
        completedTrips = new List<Trip__c>();
        allocatedTrips = new List<Trip__c>();
        myTrips = new List<Trip__c>();
        myPendingTrips = new List <Trip__c>();
        availableDrivers = new List<Driver__c>();
        tripStartTime = new Map<Id, String> ();
        tripCreatedTime = new Map <Id, String> ();
        tripsAllocatedToDriver = new Map <ID, Integer> ();
        
        /* 
            Trips, that are not assigend to any Driver or Car 
            and whos start time is greater that or equal to current time
        */
        System.Debug(trip.Trip_Date__c);
        myTrips = [SELECT Trip_Type__c, Customer_Name__c,Inquiry_Details_for_Coordinator__c,Pick_Up_and_Drop_off_Emirate__c,
                    Trip_Requester_Comments__c, Name,
                    Start_time__c, End_time__c, Status__c,
                    Inquiry__r.First_Name__c,
                    Inquiry__r.Last_Name__c,
                    Inquiry__r.inquiry_source__c,
                    Inquiry__r.Name,
                    Pick_Up_Address__c,
                    Drop_Address__c, CreatedDate,
                    Driver__c, Car__c, Car__r.model_Logo__c
                    FROM Trip__c
                    WHERE 
                    (Driver__c = NULL OR Car__c = NULL)
                    AND Status__c = 'Pending Allocation' 
                    AND Trip_Date__c =: trip.Trip_Date__c
                    Order By Start_Time__c ASC]; 
        tripStartTime = new Map<Id, String> ();
        tripCreatedTime = new Map <ID, String> ();
        for (Trip__c trip :myTrips) {
            if (trip.Start_time__c != NULL)
                tripStartTime.put(trip.Id, trip.Start_Time__c.format());
            else
                tripStartTime.put(trip.Id, '');
                
            tripCreatedTime.put (trip.Id, trip.CreatedDate.format ());
        }
        
        String query = 'SELECT Inquiry__r.Inquiry_source__c, Inquiry__r.Name, Inquiry__c, Start_Time__c, Trip_Requester_Comments__c,Inquiry_Details_for_Coordinator__c,Pick_Up_and_Drop_off_Emirate__c, '
                        +' Drop_Address__c, Pick_Up_Address__c, trip_type__c, Customer_Name__c, '
                        +' Driver__c, Car__c, CreatedDate FROM Trip__c WHERE Status__c = \'Pending Allocation\'';
        if (allTrips == 'Past') {
            query += ' AND Trip_Date__c < today ';
        }
        if (allTrips == 'Upcoming') {
            query += ' AND Trip_Date__c > today ';
        }

        query += ' Order By Start_Time__c ASC LIMIT 30';
        System.Debug(query);
        for (Trip__c trip : Database.query (query)) {
            myPendingTrips.add (trip);
            tripCreatedTime.put (trip.Id, trip.CreatedDate.format ());
            if (trip.Start_time__c != NULL)
                tripStartTime.put(trip.Id, trip.Start_Time__c.format());
            else
                tripStartTime.put(trip.Id, '');
        }
        
        tripRecords();
    }
    
    /*
        * Method Name: assignDriverToTrip
        * Return Type: Void
        * Description: To assign the Selected Driver to the trip
    */
    public void tripRecords(){
        cancelledTrips = new List<Trip__c>();
        inprogressTrips = new List<Trip__c>();
        completedTrips = new List<Trip__c>();
        allocatedTrips = new List<Trip__c>();
        System.Debug(trip.Trip_Date__c);
        
        
        allocatedTrips = [SELECT Trip_Type__c, Customer_Name__c,Inquiry_Details_for_Coordinator__c,Pick_Up_and_Drop_off_Emirate__c,
                    Trip_Requester_Comments__c, Name,
                    Start_time__c, End_time__c, Status__c,
                    Inquiry__r.First_Name__c,
                    Inquiry__r.Last_Name__c,
                    Pick_Up_Address__c, Car__r.model_Logo__c,
                    Drop_Address__c, Driver__r.Mobile__c, CreatedDate,
                    Driver__c, Car__c, Car__r.Name, Driver__r.Name
                    FROM Trip__c
                    WHERE 
                    Status__c = 'Allocated' AND
                    Trip_Date__c =: trip.Trip_Date__c];
        
        Set <ID> driverIds = new Set <ID> ();
        for (Trip__c trip :[SELECT Trip_Type__c, Customer_Name__c,Inquiry_Details_for_Coordinator__c,Pick_Up_and_Drop_off_Emirate__c,
                    Trip_Requester_Comments__c, Name,
                    Start_time__c, End_time__c, Status__c,
                    Inquiry__r.First_Name__c,
                    Inquiry__r.Last_Name__c, CreatedDate,
                    Pick_Up_Address__c, Car__r.model_Logo__c,
                    Drop_Address__c, Driver__r.Mobile__c, 
                    Driver__c, Car__c, Car__r.Name, Driver__r.Name
                    FROM Trip__c
                    WHERE 
                    ( Status__c = 'Allocated' or Status__c = 'In Progress' or Status__c = 'Completed')  AND
                    Trip_Date__c =: trip.Trip_Date__c
                    ]) {
            
            driverIds.add (trip.Driver__c);
            if (tripsAllocatedToDriver.containsKey (trip.Driver__c))
                tripsAllocatedToDriver.put (trip.Driver__c, tripsAllocatedToDriver.get (trip.Driver__c)+1);
            else
                tripsAllocatedToDriver.put (trip.Driver__c, 1);
            
        }
        
        allocatedDrivers = [SELECT User__c, User__r.Name, Mobile__c, Assigned_Car__c, CreatedDate,
                                                                    Assigned_Car__r.Name, assigned_Car__r.model_Logo__c 
                                                                    FROM Driver__c 
                                                                    WHERE User__r.isActive =TRUE AND
                                                                    In_Active__c = FALSE AND ID IN: driverIds];
        
        availableDrivers = new List<Driver__c>([SELECT User__c, CreatedDate, User__r.Name, Mobile__c, Assigned_Car__c, 
                                            Assigned_Car__r.Name, assigned_Car__r.model_Logo__c 
                                            FROM Driver__c 
                                            WHERE User__r.isActive =TRUE AND
                                            In_Active__c = FALSE 
                                            AND ID NOT IN: driverIds]);
    
        for (Driver__c d :availableDrivers) {
            tripsAllocatedToDriver.put (d.ID, 0);
        }
        
        tripCancelledTime = new Map <ID, String> ();
        cancelledTrips = [SELECT Trip_Type__c, Customer_Name__c,Inquiry_Details_for_Coordinator__c,Pick_Up_and_Drop_off_Emirate__c,
                    Trip_Requester_Comments__c, Name,Cancelled_Time__c,
                    Start_time__c, End_time__c, Status__c,
                    Inquiry__r.First_Name__c, Car__r.model_Logo__c, 
                    Inquiry__r.Last_Name__c,
                    Pick_Up_Address__c,CreatedDate,
                    Drop_Address__c, Driver__r.Mobile__c, 
                    Driver__c, Car__c, Car__r.Name, Driver__r.Name,
                    Coordinator_Cancel_Reason__c,Cancel_Reason__c,
                    Cancel_Comments__c,Is_Cancelled__c
                    FROM Trip__c
                    WHERE 
                    Status__c = 'Cancelled' AND
                    Trip_Date__c =: trip.Trip_Date__c];
                    
        inprogressTrips = [SELECT Trip_Type__c, Customer_Name__c,Inquiry_Details_for_Coordinator__c,Pick_Up_and_Drop_off_Emirate__c,
                    Trip_Requester_Comments__c, Name,
                    Start_time__c, End_time__c, Status__c,
                    Inquiry__r.First_Name__c, car__r.model_Logo__c,
                    Inquiry__r.Last_Name__c, Check_In_Reason__c,
                    Pick_Up_Address__c, CreatedDate,
                    Drop_Address__c, Driver__r.Mobile__c, 
                    Driver__c, Car__c, Car__r.Name, Driver__r.Name
                    FROM Trip__c
                    WHERE 
                    Status__c = 'In Progress' AND
                    Trip_Date__c =:trip.Trip_Date__c];
                    
                    
        completedTrips = [SELECT Trip_Type__c, Customer_Name__c,Inquiry_Details_for_Coordinator__c,Pick_Up_and_Drop_off_Emirate__c,
                    Trip_Requester_Comments__c, Name,
                    Start_time__c, End_time__c, Status__c,
                    Inquiry__r.First_Name__c, Car__r.model_Logo__c,
                    Inquiry__r.Last_Name__c,
                    Pick_Up_Address__c, CreatedDate,
                    Drop_Address__c, Driver__r.Mobile__c, Driver_Feedback__c,
                    Driver__c, Car__c, Car__r.Name, Driver__r.Name
                    FROM Trip__c
                    WHERE 
                    Status__c = 'Completed' AND 
                    Trip_Date__c =:trip.Trip_Date__c];
        
        for (Trip__c trip :allocatedTrips ) { 
            tripCreatedTime.put (trip.Id, trip.CreatedDate.format ());           
            if (trip.Start_time__c != NULL){
                tripStartTime.put(trip.Id, trip.Start_Time__c.format());
            }
            else{
                tripStartTime.put(trip.Id, '');
            }
        }
        
        for (Trip__c trip :cancelledTrips) { 
            tripCreatedTime.put (trip.Id, trip.CreatedDate.format ()); 
            if (trip.Cancelled_Time__c != NULL)
                tripCancelledTime.put (trip.Id, trip.Cancelled_Time__c.format ());           
            else
                tripCancelledTime.put (trip.Id, '');           
            if (trip.Start_time__c != NULL){
                tripStartTime.put(trip.Id, trip.Start_Time__c.format());
            }
            else{
                tripStartTime.put(trip.Id, '');
            }
        }
        
        for (Trip__c trip :inprogressTrips  ) {   
            tripCreatedTime.put (trip.Id, trip.CreatedDate.format ());         
            if (trip.Start_time__c != NULL){
                tripStartTime.put(trip.Id, trip.Start_Time__c.format());
            }
            else{
                tripStartTime.put(trip.Id, '');
            }
        }
                    
        for (Trip__c trip :completedTrips ) {  
            tripCreatedTime.put (trip.Id, trip.CreatedDate.format ());          
            if (trip.Start_time__c != NULL){
                tripStartTime.put(trip.Id, trip.Start_Time__c.format());
            }
            else{
                tripStartTime.put(trip.Id, '');
            }
        }
        
    }
    
    public static void postToChatter(String driverID) {
        System.Debug(driverID);
        
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
        mentionSegmentInput.id = driverID;
        messageBodyInput.messageSegments.add(mentionSegmentInput);
        String codeSnippet = '\n\t\tA new Trip has been allocated for you. ';
        textSegmentInput.text = codeSnippet;
        messageBodyInput.messageSegments.add(textSegmentInput);
        
        feedItemInput.body = messageBodyInput;
        
        ConnectApi.LinkCapabilityInput linkInput = new ConnectApi.LinkCapabilityInput();
        linkInput.url = '/apex/myTrips';
        linkInput.urlName = 'My Trips';
        
        ConnectApi.FeedElementCapabilitiesInput feedElementCapabilitiesInput = new ConnectApi.FeedElementCapabilitiesInput();
        feedElementCapabilitiesInput.link = linkInput;
        feedItemInput.capabilities = feedElementCapabilitiesInput;
        
        
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = driverId;
        
        ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput, null);
    }
    
    public void reschduleTripTime() {
        ID tripId = apexpages.currentpage().getparameters().get('tripId');
        String startTime = apexpages.currentpage().getparameters().get('startTime');
        System.Debug(rescheduleTrip.Start_Time__c);
        
        if (rescheduleTrip.Start_Time__c > DateTime.Now()) {

            
            rescheduleTrip.Id = tripId;
            rescheduleTrip.Trip_Date__c = rescheduleTrip.Start_Time__c.Date();
            update rescheduleTrip;
            System.debug([SELECT Start_Time__c FROM Trip__c WHERE Id =: tripId]);
            rescheduleTrip = new Trip__c();
        }
        
    }
    
    /*
        * Method Name: assignDriverToTrip
        * Return Type: Void
        * Description: To assign the Selected Driver to the trip
    */
    @remoteAction
    public static String assignDriverToTrip(ID driverId, ID tripId){
        Trip__c trip = new Trip__c();
        trip = [SELECT Start_Time__c, Car__c, Driver__c, Status__c FROM Trip__c WHERE ID =: tripId];
        
        DateTime currentTime = DateTime.Now();
        if (trip.Start_Time__c >= currentTime) {
            Driver__c driver = [SELECT User__c, Assigned_Car__c, assigned_car__r.Vehicle_Tracking_URL__c FROM Driver__c WHERE id =: driverID LIMIT 1];
            try {
                postToChatter(driver.User__c);
            } catch (Exception e){}
            trip.Status__c = 'Allocated';
            trip.Driver__c = driverId;
            trip.Vehicle_Tracking_URL__c = driver.assigned_car__r.Vehicle_Tracking_URL__c;
            trip.Car__c = driver.Assigned_Car__c;
            update trip;
           
            return 'Driver added to the Trip.';
        } else {
            return 'You can not allocate the driver to past trip. Please reschedule the trip to allocate the driver.';
        }
    }
    
    
    public void cancelTrip(){
        ID tripId = apexpages.currentpage().getparameters().get('tripId');
        String cancelReasonComments = apexpages.currentpage().getparameters().get('comments');
        String cancelReason = apexpages.currentpage().getparameters().get('reason');
        Trip__c trip = new Trip__c ();
        trip.Id = tripId;
        trip.Cancel_Comments__c = cancelReasonComments;
        trip.Coordinator_Cancel_Reason__c = cancelReason;
        trip.Is_Cancelled__c = true;
        trip.Status__c = 'Cancelled';
        trip.Cancelled_Time__c = System.Now ();
        update trip;    
    }
    
    public void reassignTrip () {
        ID tripId = apexpages.currentpage().getparameters().get('tripId');
        Trip__c trip = new Trip__c ();
        trip.id = tripId;
        trip.Status__c = 'Pending Allocation';
        trip.Driver__c = NULL;
        trip.Car__c = NULL;
        update trip;
        clearSharing(tripId);
    }
    
    // Delete Any Custom sharing on Reassigning a Trip
    public void clearSharing(string tripId){
        list<trip__share> TripshareTable = new list<trip__share>();
        TripshareTable = [select id from trip__share where rowcause='Apex_Sharing__c' and parentid=:tripId];
        if(TripshareTable.size() > 0){
            delete TripshareTable;
        }
        
    }
}