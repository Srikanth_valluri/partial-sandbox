global  class UpdateDpAgingCLBatch implements Database.Batchable <sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
    
        Id collectionQueueId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName( 'Collection_Queue','Queue' );
        
        String query = 'Select Id,OwnerId,RecordTypeId,Account__c,Calling_List_Type__c'+
                       ' from Calling_List__c Where Account__c != null AND IsHideFromUI__c = false AND Calling_List_Status__c != \'Closed\''+ 
                       ' AND OwnerId =: collectionQueueId AND RecordTypeId =: devRecordTypeId AND ( Calling_List_Type__c = \'DP Calling\' OR  Calling_List_Type__c = \'Aging\' ) ';
                       
        system.debug( 'query  : '+ query );
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Calling_List__c> lstCallingList) {
        set<Id> setAccId = new set<Id>();
        system.debug( ' lstCallingList :'+lstCallingList );
        Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        Map<Id,List<Calling_List__c>> mapAccIdToobjCalling_List  = new Map<Id,List<Calling_List__c>>();
        if (lstCallingList != null && lstCallingList.size() > 0) {
            for( Calling_List__c objCalling_List : lstCallingList) {
                if( objCalling_List.RecordTypeId == devRecordTypeId 
                 && objCalling_List.Account__c != null 
                 && ( objCalling_List.Calling_List_Type__c == 'DP Calling' || objCalling_List.Calling_List_Type__c ==  'Aging') 
                 && !string.valueOf(objCalling_List.OwnerId).startsWith('005') ) {
                    setAccId.add( objCalling_List.Account__c );
                }
            } //End of for
            system.debug( ' setAccId :'+setAccId );
            if (setAccId != null && setAccId.size() > 0) {
                List<Calling_List__c> lstCLToUpdate = new List<Calling_List__c>();
                for( Calling_List__c objCalling_List : [ Select Id
                                                              , RecordTypeId
                                                              , Account__c
                                                              , OwnerId
                                                           From Calling_List__c
                                                          WHERE RecordTypeId =: devRecordTypeId 
                                                            AND Account__c IN: setAccId
                                                            AND IsHideFromUI__c = false
                                                            AND Customer_Flag__c = true
                                                            AND Calling_List_Status__c != 'Closed'] ) {
                    system.debug( ' objCalling_List :'+objCalling_List );
                    if( string.valueOf(objCalling_List.OwnerId).startsWith('005') ) {
                        if( mapAccIdToobjCalling_List.containsKey(objCalling_List.Account__c) && mapAccIdToobjCalling_List.get( objCalling_List.Account__c ) != null ) {
                            List<Calling_List__c> lstCL = mapAccIdToobjCalling_List.get( objCalling_List.Account__c );
                            lstCL.add(objCalling_List);
                            mapAccIdToobjCalling_List.put( objCalling_List.Account__c,lstCL );
                        } else {
                            mapAccIdToobjCalling_List.put( objCalling_List.Account__c,new List<Calling_List__c>{objCalling_List});
                        }
                    }
                }
                system.debug( ' mapAccIdToobjCalling_List :'+mapAccIdToobjCalling_List );
                //Id collectionQueueId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName( 'Collection_Queue','Queue' );
                for( Calling_List__c objCalling_List : lstCallingList) {
                    if( objCalling_List.RecordTypeId == devRecordTypeId 
                     && objCalling_List.Account__c != null 
                     && ( objCalling_List.Calling_List_Type__c == 'DP Calling' || objCalling_List.Calling_List_Type__c ==  'Aging') 
                     && !string.valueOf(objCalling_List.OwnerId).startsWith('005') ) {
                        if( mapAccIdToobjCalling_List.containsKey(objCalling_List.Account__c) ) {
                            objCalling_List.OwnerId =  mapAccIdToobjCalling_List.get(objCalling_List.Account__c)[0].OwnerId;
                        }/*else {
                            objCalling_List.OwnerId =  collectionQueueId;
                        }*/
                    }
                    lstCLToUpdate.add(objCalling_List);
                    system.debug( ' objCalling_List.OwnerId :'+objCalling_List.OwnerId );
                }
                if( lstCLToUpdate != null && lstCLToUpdate.size() > 0 ) {
                    update lstCLToUpdate;
                }
            }
        } //End of main if
    }
    global void finish(Database.BatchableContext BC){}
}