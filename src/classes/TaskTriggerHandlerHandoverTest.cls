/******************************************************************************
* Description - Test class developed for TaskTriggerHandlerHandover
*
* Version            Date            Author                    Description
* 1.0                05/02/18       Lochan Karle              Initial Draft
********************************************************************************/

@isTest 
public class TaskTriggerHandlerHandoverTest{


 @isTest static void getTest(){
       Skip_Task_Trigger_for_Informatica__c objCS =new Skip_Task_Trigger_for_Informatica__c();
        objCS.SetupOwnerId=Userinfo.getUserId();
        objCS.Enable__c = true;
        insert objCS;
         Id LocRecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
     
        /*Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Type = 'NOCVisa';
        insert Cas;
        System.assert(Cas != null);*/
        
        Location__c objLocation = new Location__c();
        objLocation.Name = 'ABC';
        objLocation.Location_ID__c = '12345';
        objLocation.RecordTypeId = LocRecordTypeId;
        insert objLocation;
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id; 
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
        
        Task tsk = new Task();
        tsk.WhatId = objLocation.Id;
        tsk.ActivityDate = System.today()+2;
        tsk.Subject = 'Subject';
        tsk.Status = 'Not Started';
        tsk.Assigned_User__c = 'Finance'; 
        tsk.Status = 'Not Started';
        tsk.Process_Name__c = 'Handover';
        insert tsk ;  
        
        list<Task> lstTask =  new list<Task>();
        lstTask.add(tsk);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();
        TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response1 = new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
        response1.return_x = '{'+
    ''+
    '"message":"message",'+
    '"status":"E"'+
    '}';
       SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
       Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
       new TaskTriggerHandlerHandover().taskForExternalUser(lstTask);     
       Test.stopTest();
    
 }
 
 @isTest static void getCaseTest(){
       Skip_Task_Trigger_for_Informatica__c objCS =new Skip_Task_Trigger_for_Informatica__c();
        objCS.SetupOwnerId=Userinfo.getUserId();
        objCS.Enable__c = true;
        insert objCS;
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
     
        /*Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Type = 'NOCVisa';
        insert Cas;
        System.assert(Cas != null);*/
        
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id; 
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
        
        Case objC = new Case();
        objC.Booking_Unit__c = BU.Id;
        objC.AccountId = Acc.Id;
        objC.RecordTypeId = RecordTypeId;
        objC.Status = 'New';
        objC.Origin = 'Portal';
        insert objC;
        
        Task tsk = new Task();
        tsk.WhatId = objC.Id;
        tsk.ActivityDate = System.today()+2;
        tsk.Subject = 'Subject';
        tsk.Status = 'Not Started';
        tsk.Assigned_User__c = 'Finance'; 
        tsk.Status = 'Not Started';
        tsk.Process_Name__c = 'Handover';
        insert tsk ;  
        
        list<Task> lstTask =  new list<Task>();
        lstTask.add(tsk);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();
        TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response1 = new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
        response1.return_x = '{'+
    ''+
    '"message":"message",'+
    '"status":"E"'+
    '}';
       SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
       Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
       new TaskTriggerHandlerHandover().taskForExternalUser(lstTask);     
       Test.stopTest();
    
 }
 
 @isTest static void checkdocs() {
     Skip_Task_Trigger_for_Informatica__c objCS =new Skip_Task_Trigger_for_Informatica__c();
        objCS.SetupOwnerId=Userinfo.getUserId();
        objCS.Enable__c = true;
        insert objCS;
     Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id; 
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
        
        Case objC = new Case();
        objC.Booking_Unit__c = BU.Id;
        objC.AccountId = Acc.Id;
        objC.RecordTypeId = RecordTypeId;
        objC.Status = 'New';
        objC.Origin = 'Portal';
        insert objC;
        
        SR_Attachments__c objDoc = new SR_Attachments__c();
        objDoc.Name = 'Test 1';
        objDoc.Case__c = objC.Id;
        insert objDoc;
        
        Task tsk = new Task();
        tsk.WhatId = objC.Id;
        tsk.ActivityDate = System.today()+2;
        tsk.Subject = 'Upload All Handover Documents';
        tsk.Status = 'Not Started';
        tsk.Assigned_User__c = 'CDC'; 
        tsk.Status = 'Not Started';
        //tsk.Process_Name__c = 'Handover';
        insert tsk ; 
        
        test.startTest();        
            Task objTask = new Task(Id = tsk.Id);
            try {
                objTask.status = 'Completed';
                update objTask;
            } catch (Exception e) {
                Boolean expectedExceptionThrown =  e.getMessage().contains('My Error Message') ? true : false;
                System.AssertEquals(expectedExceptionThrown, false);

            } 
        Test.stopTest(); 
     }

     @isTest static void eSignatureProcess(){
        Skip_Task_Trigger_for_Informatica__c objCS =new Skip_Task_Trigger_for_Informatica__c();
         objCS.SetupOwnerId=Userinfo.getUserId();
         objCS.Enable__c = true;
         insert objCS;
         Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
         
         NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
         insert Sr;
         System.assert(Sr != null);
         
         Account   Acc = TestDataFactory_CRM.createPersonAccount();
         Acc.Nationality__c = 'UAE';
         insert Acc;
         System.assert(Acc != null);
      
         Booking__c Booking =  new Booking__c();
         Booking.AWB_Number__c = 'Test AWB';
         Booking.Account__c = Acc.Id; 
         Booking.Deal_SR__c = Sr.Id;
         insert Booking;
         System.assert(Booking != null);
         
         Booking_Unit__c BU =  new Booking_Unit__c();
         BU.Unit_Name__c = 'Test Units';  
         BU.Registration_Status__c  = 'Active';  
         BU.Unit_Selling_Price_AED__c  = 100;  
         BU.Booking__c = Booking.Id;  
         insert BU;
         System.assert(BU != null);  
         
         Case objC = new Case();
         objC.Booking_Unit__c = BU.Id;
         objC.AccountId = Acc.Id;
         objC.RecordTypeId = RecordTypeId;
         objC.Status = 'New';
         objC.Origin = 'Portal';
         objC.E_Signature__c = 'E-Signature Verified';
         insert objC;
         
         Case objC1 = new Case();
         objC1.Booking_Unit__c = BU.Id;
         objC1.AccountId = Acc.Id;
         objC1.RecordTypeId = RecordTypeId;
         objC1.Status = 'New';
         objC1.Origin = 'Portal';
         objC1.E_Signature__c = 'Sent to Customer';
         insert objC1;
         
         Task tsk1 = new Task();
         tsk1.WhatId = objC.Id;
         tsk1.ActivityDate = System.today();
         tsk1.Subject = 'Get Key Release Form generated from the Director';
         tsk1.Status = 'Completed';
         insert tsk1 ;  
         
         Task tsk2 = new Task();
         tsk2.WhatId = objC1.Id;
         tsk2.ActivityDate = System.today();
         tsk2.Subject = 'Get Key Release Form generated from the Director';
         tsk2.Status = 'Completed';
         //insert tsk2 ; 
         
         list<Task> lstTask =  new list<Task>();
         lstTask.add(tsk1);
         //lstTask.add(tsk2);
         
         Test.startTest();
         new TaskTriggerHandlerHandover().checkESignatureVerification(lstTask);     
        Test.stopTest();
     
  }
}