/***************************************************************************************************
* Description - The apex class is written to call approval process automatically
*
* Version   Date            Author            Description
* 1.0       26/03/18        Monali            Initial Draft
***************************************************************************************************/


public class LeadMangementApproval {
    //public static Approval.ProcessResult result ;
    public void SendForApproval(List<Lead_Management__c> lstLeadMgmt){
        Set<Id> leadMgtIds = new Set<Id>();
        System.debug('=======LeadMangementApproval=SendForApproval====') ;
        for(Lead_Management__c leadManObj : lstLeadMgmt)   {
            if(leadManObj.Status__c == 'Submitted'){
                /*Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setComments('Submitted for approval. Please approve.');
                req.setObjectId(leadManObj.Id);
                Approval.ProcessResult result = Approval.process(req); */
                leadMgtIds.add(leadManObj.Id);
            }  
        }         

        if(leadMgtIds.size() > 0) 
            CallApprovalProcess(leadMgtIds);
    }

    @Future(callout=true)
    public static void CallApprovalProcess(Set<Id> leadMgtIds){
        System.debug('==LeadMangementApproval=====CallApprovalProcess=====') ;
        for(Id leadId: leadMgtIds) {
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Submitted for approval. Please approve.');
            req.setObjectId(leadId);
            Approval.ProcessResult result = Approval.process(req);
        }
    }
}