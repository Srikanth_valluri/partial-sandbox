/**
 * @File Name          : UploadMultipleDocController.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/26/2019, 5:38:36 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    6/26/2019, 5:37:58 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public without sharing class UploadMultipleDocController {
    @testVisible private static final String PUBLIC_SITE_URL    = String.isNotBlank(System.Label.OfficePreviewUrl)?System.Label.OfficePreviewUrl:'';
    public static String strLabelValue = System.Label.Use_Office_365 ;
    
    public static data getMultipleDocUrl(List<MultipleDocRequest> lstWrap, String str) {
        return processData(lstWrap, str); 
    }

    public static data getMultipleDocUrl(List<MultipleDocRequest> lstWrap ) {
        return processData(lstWrap, 'Blank');
    }

    public static data processData(List<MultipleDocRequest> lstWrap, String str){
        system.debug('-->>lstWrap: '+lstWrap);   
        if ( String.isNotBlank(strLabelValue) 
          && strLabelValue.equalsIgnoreCase('Y')  ) {
            data objdata = new data();
            objdata.status='S';
            for( MultipleDocRequest objWrap : lstWrap ) {
                system.debug('My test  debug request:  '+JSON.serializePretty(objWrap) );
                Blob fileToUpload = String.isNotBlank(objWrap.base64Binary) ? EncodingUtil.base64Decode(objWrap.base64Binary) : Blob.valueOf('');
                String fileName = String.isNotBlank(objWrap.fileName) ? objWrap.fileName : '';
                Office365RestService.ResponseWrapper responseWrap = new Office365RestService.ResponseWrapper();
                responseWrap = Office365RestService.uploadToOffice365Result(fileToUpload, fileName, '', '', '', '');
                MultipleDocResponse MultipleDocResponseObj = new MultipleDocResponse();
                if( responseWrap != null  && String.isNotBlank(responseWrap.id)){
                    MultipleDocResponseObj.PROC_STATUS ='S';
                    MultipleDocResponseObj.url = PUBLIC_SITE_URL + responseWrap.id;
                    MultipleDocResponseObj.PARAM_ID = objWrap.sourceId;
                    MultipleDocResponseObj.PROC_MESSAGE='[SUCCESS]Files : '+ objWrap.fileId;
                    system.debug('PROC_MESSAGE:   '+MultipleDocResponseObj.PROC_MESSAGE); 
                }else if(responseWrap != null  && !responseWrap.Error_log_lst.isEmpty()){
                    MultipleDocResponseObj.PROC_STATUS ='N';
                    objdata.status='N';
                }
                else{
                     objdata.status='N';
                }
                objdata.data.add(MultipleDocResponseObj);
                
            }
            system.debug('My test  debug request:  '+JSON.serializePretty(objdata) );
            return objdata;
            
        }
        else if ( String.isNotBlank(strLabelValue) 
          && strLabelValue.equalsIgnoreCase('N')  ) {
              system.debug('My test  debug request:  '+JSON.serializePretty(lstWrap) );
            MultipleDocUploadService.AOPTHttpSoap11Endpoint  calloutObj = 
            new MultipleDocUploadService.AOPTHttpSoap11Endpoint ();
            calloutObj.timeout_x = 120000;
            //List<MultipleDocResponse> resObj = new List<MultipleDocResponse>();
            data objdata = new data();

            try{
                List<beanComXsdMultipleDocUpload.DocUploadDTO> lstregTerms = new List<beanComXsdMultipleDocUpload.DocUploadDTO>();
                for( MultipleDocRequest objWrap : lstWrap ) {

                    soapencodingTypesDatabindingAxis2ApaMult.Base64Binary objfileBinary = new soapencodingTypesDatabindingAxis2ApaMult.Base64Binary();
                    objfileBinary.base64Binary = objWrap.base64Binary;

                    beanComXsdMultipleDocUpload.DocUploadDTO regTerms = new beanComXsdMultipleDocUpload.DocUploadDTO();
                    regTerms.category = objWrap.category;
                    regTerms.entityName = objWrap.entityName;
                    regTerms.fileBinary = objfileBinary;
                    regTerms.fileDescription = objWrap.fileDescription;
                    regTerms.fileId = objWrap.fileId;
                    regTerms.fileName = objWrap.fileName;
                    if(String.isNotBlank(str) && str.equalsIgnoreCase('FM')){
                        regTerms.registrationId = objWrap.registrationId;
                    }else{
                        regTerms.registrationId = '2-'+objWrap.registrationId;
                    }
                    regTerms.sourceFileName = objWrap.sourceFileName;
                    regTerms.sourceId = objWrap.sourceId;
                    lstregTerms.add(regTerms);


                }

                String reqNumber = '2-' + system.currentTimeMillis();
                string strJsonReq = calloutObj.DocumentAttachmentMultiple( reqNumber , 'ATTACH_DOC_IN_EBS','SFDC' ,  lstregTerms );

                system.debug('== Multiple Document Response strJsonReq =='+strJsonReq );

                objdata = (data)JSON.deserialize(strJsonReq, data.class);

            
            } catch (Exception e ){
                objdata.status = 'Exception';
                objdata.message = String.valueOf( e ) ;

            }

          system.debug(' my test response ==objdata= '+ objdata);
        return objdata;

        }
        else{
            return null;
        }
    }

    public class MultipleDocRequest{
        public String category {get;set;}
        public String entityName {get;set;}
        public String base64Binary {get;set;}
        public String fileDescription {get;set;}
        public String fileId {get;set;}
        public String fileName {get;set;}
        public String registrationId {get;set;}
        public String sourceFileName {get;set;} // same as file Name
        public String sourceId {get;set;} // SR attachment Id
    }


    public class data {
        public List<MultipleDocResponse> data{get;set;}
        public String message {get;set;}
        public String status {get;set;}
        public data(){
            this.data = new List<MultipleDocResponse>();
        }
    }

    public class MultipleDocResponse{
        public String PROC_STATUS {get;set;}
        public String PROC_MESSAGE {get;set;}
        public String PARAM_ID {get;set;}
        public String url {get;set;}
        public String FTP_Response {get;set;}
    }
}

/*

Attachment  atchObj = [SELECT
                            id,
                            name,
                            body 
                        FROM
                            Attachment Where Id = '00P25000005h2aA' Limit 1];
UploadMultipleDocController.MultipleDocRequest objData = new UploadMultipleDocController.MultipleDocRequest();
objData.base64Binary = EncodingUtil.base64Encode(atchObj.body);
objData.fileName = atchObj.name;
list<UploadMultipleDocController.MultipleDocRequest> lstWpr = new list<UploadMultipleDocController.MultipleDocRequest>();
lstWpr.add(objData);
system.debug('data------>'+UploadMultipleDocController.getMultipleDocUrl( lstWpr ));
*/