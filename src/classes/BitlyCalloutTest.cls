/****************************************************************************************************
* Name          : BitlyCalloutTest                                                                  *
* Description   : Test class for BitlyCallout Class t                                               *
* Created Date  : 01/05/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    01/05/2018      Initial Draft.                                          *
****************************************************************************************************/
@isTest
public class BitlyCalloutTest {
    
    @isTest static void test_method_one() {

        String userId = UserInfo.getUserId();
        String shareType = 'email';
        String shareType1 = 'text';
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new BitlyCalloutMockResponse ());
        Map<String, Object>  testMap = BitlyCallout.getShortenedURL(userId, shareType, '');
        Map<String, Object>  testMap2 = BitlyCallout.getShortenedURL(userId, shareType1, 'isPortal');
        Test.stopTest();
    }
}