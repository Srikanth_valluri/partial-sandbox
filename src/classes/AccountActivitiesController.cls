public with sharing class AccountActivitiesController {

    public Set<String> contactIdsSet = new Set<String>();
    public List<Contact> contactsList;
    public List<ContactsWrapper> contactsWrapperList1       {get; set;}
    public List<ContactsWrapper> contactsWrapperList2       {get; set;}
    public User userObj                                     {get; set;}
    public Account accountObj                               {get; set;}
    public Task completedtaskObj                            {get; set;}
    public Task scheduledtaskObj                            {get; set;}
    public Task editedTask                                  {get; set;}
    public String relatedToName                             {get; set;}
    public String msgType                                   {get; set;}
    public String isDisplay                                 {get; set;}
    public String message                                   {get; set;}
    public String completedTaskOwner                        {get; set;}
    public String scheduledTaskOwner                        {get; set;}
    public Boolean displayPopUp                             {get; set;}
    public Boolean isFollowUp                               {get; set;}
    public Boolean isEditTask                               {get; set;}
    public Boolean isAgentsTeam                             {get; set;}



    /*********************************************************************************************************
    * @Description : Constructor Initialize the Task, Get logged in UserId, Get Account in current context  *
    *                Get Contacts related logged in to UserId                                               *
    * @Params      : Standard controller instance                                                           *
    * @Return      :                                                                                        *
    *********************************************************************************************************/
    public AccountActivitiesController(ApexPages.StandardController controller) {
        isAgentsTeam = false;
        isFollowUp = false;
        isEditTask = false;
        isDisplay = '';
        if (message == '' || message == null) {
            isDisplay = 'hide';
        }
        displayPopUp = false;
        msgType = '';
        message = '';
        userObj = new User();
        String loggedinUserId = UserInfo.getUserId();
        String taskId = ApexPages.currentPage().getParameters().get('taskId');
        String accId = ApexPages.currentPage().getParameters().get('accId');
        if (accId == null) {
            accId = ApexPages.currentPage().getParameters().get('Id');
        }
        String accountTaskRTId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Account Task').getRecordTypeId();
        system.debug('accountTaskRTId >>>  ' + accountTaskRTId);
        // To be used on Page
        userObj = [SELECT Id, Name FROM User WHERE Id = :UserInfo.getUserId()];
        accountObj = [SELECT Id, Name, OwnerId, IsPersonAccount, Vendor_ID__c FROM Account WHERE Id = :accId];
        Date todaysDate = System.today();
        String userProfileId = UserInfo.getProfileId();
        completedtaskObj = new Task();
        scheduledtaskObj = new Task();
        completedTaskOwner = userObj.Id;
        scheduledTaskOwner = userObj.Id;

        Set<Id> agentsTeamProfileIdSet = new Set<Id>();
        for(Profile profileObj : [Select Id
                                    FROM Profile
                                   WHERE Name = :Label.Agent_Executive_Manager_Profile
                                      OR Name = :Label.Agent_Executive_Team_Profile
                                      OR Name = :Label.Agent_Admin_Team_Profile
                                      OR Name = :Label.Agent_Admin_Manager_Profile
                                      OR Name = :Label.System_Admin_Profile
        ]) {
            agentsTeamProfileIdSet.add(profileObj.Id);
        }
        system.debug('agentsTeamProfileIdSet >>>>>>  ' + agentsTeamProfileIdSet);

        contactsList = new List<Contact>();
        if (loggedinUserId == accountObj.OwnerId
            || agentsTeamProfileIdSet.contains(userProfileId)) {
            contactsList = [SELECT Id, Name, Account.Name
                              FROM CONTACT
                             WHERE AccountId = :accountObj.Id
                               AND Status__c != 'Cancelled'
            ];
            isAgentsTeam = true;
        } else {
            contactsList = [SELECT Id, Name, Account.Name
                              FROM CONTACT
                             WHERE AccountId = :accountObj.Id
                               AND OwnerId = :UserInfo.getUserId()
                               AND Status__c != 'Cancelled'
            ];
            isAgentsTeam = false;
        }
        system.debug('contactsList >>>>>>  ' + contactsList);

        if (String.isBlank(taskId)) {
            isEditTask = false;
            if (String.isNotBlank(accountTaskRTId)) {
                completedtaskObj.RecordTypeId = accountTaskRTId;
            }
            if (!isAgentsTeam) {
                completedtaskObj.OwnerId = userObj.Id;
            }
            completedtaskObj.WhatId = accountObj.Id;
            completedtaskObj.ActivityDate = todaysDate;
            completedtaskObj.Status = 'Completed';
            completedtaskObj.Priority = 'Normal';

            if (String.isNotBlank(accountTaskRTId)) {
                scheduledtaskObj.RecordTypeId = accountTaskRTId;
            }
            if (!isAgentsTeam) {
                scheduledtaskObj.OwnerId = userObj.Id;
            }
            scheduledtaskObj.ActivityDate = todaysDate;
            scheduledtaskObj.WhatId = accountObj.Id;

            populateContactsForActivity(null);

        } else if (String.isNotBlank(taskId)) {
            editedTask = [SELECT Id, Subject, ActivityDate, WhatId, OwnerId, Status,
                                 Agency_Contacts_Involved__c, Activity_Members__c,
                                 Start_Date__c, End_Date__c, Description, Priority,
                                 Activity_Sub_Type__c, Activity_Type__c, WhoId,
                                 Agency_Contacts_Ids__c
                            FROM Task WHERE Id = :taskId];
            system.debug('editedTask >>>>>>  ' + editedTask);
            
            if (editedTask != null) {
                completedtaskObj = editedTask;
                userObj = [SELECT Id, Name FROM User WHERE Id = :completedtaskObj.OwnerId];
                isEditTask = true;
                ID taskIdNew = Id.valueOf(taskId);
                for( Contact_Activity__c contActObj : [SELECT
                                                            Contact__c
                                                       FROM 
                                                           Contact_Activity__c
                                                       WHERE
                                                            TaskId__c =:taskIdNew]){
                     contactIdsSet.add(contActObj.Contact__c); 
                }
                system.debug('....contactIdsSet'+contactIdsSet);
                if(!contactIdsSet.isEmpty()){
                     populateContactsForActivity(contactIdsSet);
                } else{
                    populateContactsForActivity(null);
                }
               /* if (String.isNotBlank(completedtaskObj.Agency_Contacts_Ids__c)) {
                    List<String> editedTaskContactIds = completedtaskObj.Agency_Contacts_Ids__c.split(',');
                    if (!editedTaskContactIds.isEmpty()) {
                        contactIdsSet = new Set<String>(editedTaskContactIds);
                        populateContactsForActivity(contactIdsSet);
                    } else {
                        populateContactsForActivity(null);
                    }
                } else {
                    populateContactsForActivity(null);
                }*/
            }
        }
    }

    public void populateContactsForActivity(Set<String> pContactIdsSet) {

        contactsWrapperList1 = new List<ContactsWrapper>();
        contactsWrapperList2 = new List<ContactsWrapper>();
        ContactsWrapper contactsWrapperObj1;
        ContactsWrapper contactsWrapperObj2;
        for (Contact contactObj : contactsList) {
            System.debug('contactObj>>>>  '+ contactObj);
            if (pContactIdsSet != null && !pContactIdsSet.isEmpty()) {
                if (pContactIdsSet.contains(contactObj.Id)) {
                    contactsWrapperObj1 = new ContactsWrapper(contactObj.Id, contactObj.Name, true);
                } else {
                    contactsWrapperObj1 = new ContactsWrapper(contactObj.Id, contactObj.Name, false);
                }
                contactsWrapperList1.add(contactsWrapperObj1);
            } else {
                contactsWrapperObj1 = new ContactsWrapper(contactObj.Id, contactObj.Name, false);
                contactsWrapperObj2 = new ContactsWrapper(contactObj.Id, contactObj.Name, false);
                contactsWrapperList1.add(contactsWrapperObj1);
                contactsWrapperList2.add(contactsWrapperObj2);
            }
        }

    }

    /***************************************************************************************************
    * @Description : Method to Create new Activity record                                              *
    * @Params      :                                                                                   *
    * @Return      :                                                                                   *
    ****************************************************************************************************/
    public void saveActivity() {
        isDisplay = 'hide';
        System.debug('saveActivity>>>>  '+ isFollowUp);
        System.debug('completedTaskOwner>>>>  '+ completedTaskOwner );
        System.debug('scheduledTaskOwner>>>>  '+ scheduledTaskOwner );

        List<Contact_Activity__c> contactActivityInsertList1 = new List<Contact_Activity__c>();
        List<Contact_Activity__c> contactActivityInsertList2 = new List<Contact_Activity__c>();
        Boolean isValidCompletedTask = validateActivity(completedtaskObj);
        if (!isFollowUp) {
            if (isValidCompletedTask) {
                completedtaskObj.Subject = completedtaskObj.Activity_Type__c;
                completedtaskObj.OwnerId = UserInfo.getUserId();
                System.debug('completedtaskObjBEFROE INSERT>>>>  '+ completedtaskObj );
                insert completedtaskObj;
                contactActivityInsertList1 = createContactActivityRecords(completedtaskObj, contactsWrapperList1);
                if (!contactActivityInsertList1.isEmpty()) {
                    insert contactActivityInsertList1;
                    if (isAgentsTeam) {
                        completedtaskObj.OwnerId = completedTaskOwner;
                    }
                    update completedtaskObj;
                }
                messageBlock('show', 'success', 'Activity Logged Successfully');
            } else {
                messageBlock('show', 'error', message);
            }
        } else if (isFollowUp) {
            System.debug('isFollowUp>>>>  ' +isFollowUp);
            Boolean isValidScheduledTask = validateActivity(scheduledtaskObj);
            if (isValidScheduledTask && isValidCompletedTask) {
                completedtaskObj.Subject = completedtaskObj.Activity_Type__c;
                scheduledtaskObj.Subject = scheduledtaskObj.Activity_Type__c;
                insert completedtaskObj;
                insert scheduledtaskObj;
                contactActivityInsertList1 = createContactActivityRecords(completedtaskObj, contactsWrapperList1);
                contactActivityInsertList2 = createContactActivityRecords(scheduledtaskObj, contactsWrapperList2);
                contactActivityInsertList1.addAll(contactActivityInsertList2);
                if (!contactActivityInsertList1.isEmpty()) {
                    insert contactActivityInsertList1;
                    if (isAgentsTeam) {
                        completedtaskObj.OwnerId = completedTaskOwner;
                        scheduledtaskObj.OwnerId = scheduledTaskOwner;
                    }
                    update completedtaskObj;
                    update scheduledtaskObj;
                }
                messageBlock('show', 'success', 'Activity Logged Successfully and Created a Follow-up Task ');
            } else {
                messageBlock('show', 'error', message);
            }
        }
    }

    /***************************************************************************************************
    * @Description : Method to Create new Activity record                                              *
    * @Params      :                                                                                   *
    * @Return      :                                                                                   *
    ****************************************************************************************************/
    public void updateActivity() {
        isDisplay = 'hide';
        System.debug('updateActivity>>>>  ');
        List<Contact_Activity__c> contactActivityInsertList = new List<Contact_Activity__c>();
        Boolean isValidCompletedTask = validateActivity(completedtaskObj);
        if (isValidCompletedTask) {
            completedtaskObj.Subject = completedtaskObj.Activity_Type__c;
            update completedtaskObj;
            contactActivityInsertList = createContactActivityRecords(completedtaskObj, contactsWrapperList1);
            if (!contactActivityInsertList.isEmpty()) {
                deleteContactActivityRecords(completedtaskObj);
                insert contactActivityInsertList;
                update completedtaskObj;
            }
            messageBlock('show', 'success', 'Activity Logged Successfully');
        } else {
            messageBlock('show', 'error', message);
        }
    }

    /***************************************************************************************************
    * @Description : Method to Create new Contact Activity records(Junction Object)                    *
    * @Params      :                                                                                   *
    * @Return      : List<Contact_Activity__c>                                                         *
    ****************************************************************************************************/
    public List<Contact_Activity__c> createContactActivityRecords(Task pTaskObj, List<ContactsWrapper> pWrapperList) {

        String activityContactNames = '';
        String activityContactIds = '';
        Integer totalActivityContacts = 0;
        List<Contact_Activity__c> contactActivityList = new List<Contact_Activity__c>();

        System.debug('pWrapperList>>>>  '+ pWrapperList);
        for (ContactsWrapper wrapContact : pWrapperList) {
            if (wrapContact.isSelected == true) {
                Contact_Activity__c contactActivityObj = new Contact_Activity__c();
                contactActivityObj.Name = accountObj.Vendor_ID__c;
                contactActivityObj.Account_Name__c = accountObj.Name;
                contactActivityObj.Contact__c = wrapContact.wrapContactId;
                contactActivityObj.TaskId__c = pTaskObj.Id;
                contactActivityObj.Activity_Type__c = pTaskObj.Activity_Type__c;
                contactActivityObj.Activity_Sub_Type__c = pTaskObj.Activity_Sub_Type__c;
                contactActivityObj.Activity_Status__c = pTaskObj.Status;
                contactActivityObj.Activity_Start_Date__c = pTaskObj.Start_Date__c;
                contactActivityObj.Activity_End_Date__c = pTaskObj.End_Date__c;
                contactActivityObj.Activity_Subject__c = pTaskObj.Activity_Type__c;
                contactActivityObj.Activity_Description__c = pTaskObj.Description;
                contactActivityObj.Activity_Priority__c = pTaskObj.Priority;
                activityContactNames += wrapContact.wrapContactName + ',';
                activityContactIds += wrapContact.wrapContactId + ',';
                totalActivityContacts++;
                contactActivityList.add(contactActivityObj);
            }
        }
        if (!contactActivityList.isEmpty()) {
            pTaskObj.Activity_Members__c = totalActivityContacts;
            //pTaskObj.Agency_Contacts_Involved__c = activityContactNames.removeEnd(',');
            //pTaskObj.Agency_Contacts_Ids__c = activityContactIds.removeEnd(',');
        } else {
            Contact_Activity__c contactActivityObj = new Contact_Activity__c();
            contactActivityObj.Name = accountObj.Vendor_ID__c;
            contactActivityObj.Account_Name__c = accountObj.Name;
            contactActivityObj.TaskId__c = pTaskObj.Id;
            contactActivityObj.Activity_Type__c = pTaskObj.Activity_Type__c;
            contactActivityObj.Activity_Sub_Type__c = pTaskObj.Activity_Sub_Type__c;
            contactActivityObj.Activity_Status__c = pTaskObj.Status;
            contactActivityObj.Activity_Start_Date__c = pTaskObj.Start_Date__c;
            contactActivityObj.Activity_End_Date__c = pTaskObj.End_Date__c;
            contactActivityObj.Activity_Subject__c = pTaskObj.Activity_Type__c;
            contactActivityObj.Activity_Description__c = pTaskObj.Description;
            contactActivityObj.Activity_Priority__c = pTaskObj.Priority;
            contactActivityList.add(contactActivityObj);
        }
        return contactActivityList;

    }

    /***************************************************************************************************
    * @Description : Method to Delete Contact Activity records(Junction Object)                    *
    * @Params      :                                                                                   *
    * @Return      : List<Contact_Activity__c>                                                         *
    ****************************************************************************************************/
    public void deleteContactActivityRecords(Task pTaskObj) {

        List<Contact_Activity__c> contactActivityList = new List<Contact_Activity__c>();

        System.debug('pTaskObj DELETE>>>>  '+ pTaskObj);
        if (pTaskObj != null) {
            if (String.isNotBlank(pTaskObj.Agency_Contacts_Ids__c)) {
                List<String> contactIdsList = pTaskObj.Agency_Contacts_Ids__c.split(',');
                //if (contactIdsList != null && !contactIdsList.isEmpty()) {
                    //System.debug('contactIdsList>>>>  '+ contactIdsList);
                    contactActivityList = [ SELECT Id, TaskId__c
                                              FROM Contact_Activity__c
                                             WHERE TaskId__c = :pTaskObj.Id
                    ];
                    if (!contactActivityList.isEmpty()) {
                        delete contactActivityList;
                    }
                //}
            }
        }

    }

    /***************************************************************************************************
    * @Description : Method to validate the task record                                                *
    * @Params      : pTaskObj > Task object to be validated                                            *
    * @Return      : Boolean                                                                           *
    ****************************************************************************************************/
    public Boolean validateActivity(Task pTaskObj) {

        Boolean isValid = true;
        message = '';
        if (pTaskObj != null) {
            if (pTaskObj.Start_Date__c == null) {
                isValid = false;
                message += ' Please Enter Start Date for Activity <br/>';
            }
            if(pTaskObj.End_Date__c == null) {
                isValid = false;
                message += ' Please Enter End Date for Activity <br/>';
            } else {
                DateTime endDateTime = pTaskObj.End_Date__c;
                Date dueDate = Date.NewInstance(
                    endDateTime.year(), endDateTime.month(), endDateTime.day()
                );
                pTaskObj.ActivityDate = dueDate;
            }
            if (pTaskObj.Status == null ) {
                isValid = false;
                message += ' Please Enter Status for Activity <br/>';
            }
            if (pTaskObj.Activity_Type__c == null) {
                isValid = false;
                message += ' Please Enter Activity Type for Activity <br/>';
            }

            if (pTaskObj.Activity_Type__c != null
                && pTaskObj.Activity_Sub_Type__c == null
                && (pTaskObj.Activity_Type__c.equals('Queries')
                    || pTaskObj.Activity_Type__c.equals('Complaints')
                )
            ) {
                isValid = false;
                message += 'Please Enter Activity Sub Type for Activity <br/>';
            }
            return isValid;
        } else {
            return false;
        }

    }

    /************************************************************************************************
    * @Description : Method to Display the success/Error messages                                   *
    *                depending on their visiblity, Value and Type                                   *
    * @Params      : pDisplay > show/hide, pType > success/error, pMessage > custom message         *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void messageBlock(String pDisplay, String pType, String pMessage) {
        isDisplay = pDisplay;
        msgType = pType;
        message = pMessage ;
    }

    /************************************************************************************************
    * @Description : Wrapper Class to display the Contacts with a check box on the page             *
    * @Params      :                                                                                *
    * @Return      :                                                                                *
    *************************************************************************************************/
    public class ContactsWrapper {
        public Id wrapContactId             {get; set;}
        public String wrapContactName         {get; set;}
        public Boolean isSelected           {get; set;}

        public ContactsWrapper (Id pContactId, String pContactName, Boolean pSelected) {
            wrapContactId = pContactId;
            wrapContactName = pContactName;
            isSelected = pSelected;
        }
    }

}