/*
* Description - Class used for canceling the service request.
*
* Version            Date            Author            Description
* 1.0              28/02/2018        Ashish           Initial Draft
*/

public without sharing class CancelServiceRequest {
	
	@testVisible 
	private Id caseId ;
	
    public CancelServiceRequest( ApexPages.StandardController sc ) {
    	caseId = sc.getId();
    }
    
    public void cancelServiceRequest() {
    	if( caseId != null ) {
    		Case objCase = RentalPoolTerminationUtility.getCaseDetails( caseId );
    		if( objCase.RecordTypeId == Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() ) {
    			objCase.Status = 'Cancelled';
    			objCase.Rental_Pool_Termination_Status__c = 'Cancelled';
    			try {
    				update objCase ;
    			}
    			catch( Exception e ) {
    				ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, e.getMessage() ) );
    			}
    		}
    	}
    	else {
    		ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR,'Case not found.') );
    	}
    }
    
}