global without sharing class Damac_NewTripController {

    public Trip__c trip { get; set; }
    public String result { get; set; }
    public String inqId { get; set; }
    public Sales_Tours__c tour { get; set; }
    public Integer noOfCars { get; set; }
    
    public Damac_NewTripController () {
        trip = new Trip__c ();
        result = '';
        inqId = apexpages.currentpage().getparameters().get('id');
        
        tour = new Sales_Tours__c ();
        tour.Tour_Type__c = 'Book A Car';
        tour.No_Of_Cars__c = 1;
        noOfCars = 1;
        
    }
    
    public void save () {
        result = '';
        tour.No_Of_Cars__c = noOfCars;
        if (trip.Start_Time__c != NULL) {        
            try {
                if (trip.start_Time__c >= DateTime.Now()) {
        
                    Inquiry__c inq = new Inquiry__c ();
                    
                    if (inqId != '' && inqId != NULL) {
                        if (inqId.startsWith ('IQ-')) {
                            inq = [SELECT Name, ownerId, Mobile_Phone__c FROM Inquiry__c WHERE Name =: inqId];
                            inqId = inq.Id;
                        } 
                        else {
                            inqId = apexpages.currentpage().getparameters().get('id');
                            inq = [SELECT Name, ownerId, Mobile_Phone__c FROM Inquiry__c WHERE Id =: inqId];
                            inqId = inq.ID;
                        }
                    }
                    try {
                        Sales_Tours__c existingtour = [SELECT OwnerId FROM Sales_Tours__c where Inquiry__c =: inqId Order By LastModifiedDate DESC LIMIT 1];
                        tour.Id = existingtour.Id;
                        tour.OwnerId = existingtour.OwnerId;
                    } catch (Exception e) {}
                    
                    Profile profileDetails = [SELECT Name FROM Profile WHERE Id =: UserInfo.getProfileId()];
                    if (profileDetails.name == 'Receptionist' && tour.Id == null) {
                        result = 'There is no Sales Tour associated. Please ask RM to create a Sales Tour.';
                    }
                    
                    if (inqId != '' && inqId != null && result == '') {
                        trip.Inquiry__c = inqId;
                        
                        trip.Trip_Date__c = Date.valueOf(trip.Start_Time__c);
    
                        String profileIds = Label.Profile_For_Trip_Owner_Change;
                        if (profileIds.contains (UserInfo.getProfileId())) {
                            trip.OwnerId = inq.OwnerId;
                            if (tour.OwnerId == null)
                                tour.OwnerId = inq.OwnerId;
                        } else {
                            
                            if (tour.OwnerId == null)
                                tour.ownerId = UserInfo.getUserId ();
                            trip.OwnerId = UserInfo.getUserId();
                        }
                        
                        tour.Inquiry__c = inqId;
                        tour.Client_Contact_Number__c = inq.Mobile_Phone__c;
                        /*
                        tour.Meeting_Location_Sales_office__c = trip.Visiting_Sales_Office__c ;
                        tour.Check_In_Date__c = trip.Start_Time__c;
                        tour.Check_Out_Date__c = trip.End_Time__c ;
                        tour.Pickup_Location_Fleet__c = trip.Pick_Up_Address__c ;
                        tour.Drop_off_Location__c = trip.Drop_Address__c;
                        tour.Comments__c = trip.Trip_Requester_Comments__c;
                        */
                        if (tour.Id == null) {
                            upsert tour;
                        }
                        
                        
                        trip.Sales_tours__c = tour.Id;
                        Integer count = 1;
                        List<Trip__c> newTripsList = new List<Trip__c>();
                        while (count <= tour.No_Of_Cars__c) {
                            Trip__c newTrip = new Trip__c ();
                            newtrip.Inquiry__c = trip.Inquiry__c;
                            newtrip.Start_Time__c = trip.Start_Time__c ;
                            newTrip.End_Time__c = trip.End_Time__c ;
                            newtrip.Trip_Type__c = trip.Trip_Type__c;
                            newtrip.Visiting_Sales_Office__c = trip.Visiting_Sales_Office__c ;
                            newtrip.Trip_Requester_Comments__c = trip.Trip_Requester_Comments__c ;
                            newtrip.Pick_Up_Address__c = trip.Pick_Up_Address__c ;
                            newtrip.Drop_Address__c = trip.Drop_Address__c ;
                            newTrip.Sales_tours__c = trip.Sales_tours__c ;
                            newTrip.Trip_Date__c = Trip.Trip_Date__c;
                            newTrip.OwnerId = trip.OwnerId;
                            newTripsList.add(newTrip);
                            count++;
                        }
                        if(!newTripsList.isEmpty()){
                            insert newTripsList;
                        }
                                           
                        result = 'Trip created successfully';
                    } else {
                        if (inqId == '')
                            result = 'Please choose an Inquiry to create a Trip.';
                    }
                } else {
                    result = 'Start Date should be greater than current time.';
                }
            } catch (Exception e) {
                result = e.getMessage ()+' at :'+e.getLineNumber ();            
            }
        } else {
            result = 'Pickup/Drop Address/Start time is missing';
        }
    }
    
    @remoteAction
    global Static List<String> searchInquiries (String value) {
        List <String> inquiryNames = new List <String> ();
        value = '%'+value+'%';
        String query = 'SELECT Name FROM Inquiry__c WHERE Name LIKE : value LIMIT 20';
        for (Inquiry__c inq : Database.Query (query)) {
            inquiryNames.add (inq.Name);
        }
        return inquiryNames;
    }
}