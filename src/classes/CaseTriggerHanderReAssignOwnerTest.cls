@isTest
private class CaseTriggerHanderReAssignOwnerTest {
    @isTest
    static void testTaskOwnerAssignmentFromQueueToUser() {

        Id queueId = [
            SELECT      Id, Queue.Name, QueueId FROM QueueSobject
            WHERE       SobjectType = 'Case'
                    AND Queue.Name = 'Non Elite Arabs Queue'
        ][0].QueueId;


        Account account = TestDataFactory_CRM.createPersonAccount();
        insert account;

        Id codRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();

        Case instCase = TestDataFactory_CRM.createCase(account.Id, codRecordTypeId);
        instCase.Status = 'New';
        instCase.Type = 'Change of Contact Details';
        instCase.Origin = 'Portal';
        instCase.IsPOA__c = true;
        instCase.POA_Task_Created__c = false;
        instCase.OQOOD_Verification_Task_Created__c = false;
        instCase.OwnerId = queueId;
        insert instCase;

        Task task = new Task(
            Status = 'Not Started',
            Subject = 'Test Subject',
            OwnerId = Label.DefaultCaseOwnerId,
            ActivityDate = Date.today(),
            Task_Due_Date__c = Date.today(),
            Priority = 'Normal',
            WhatId = instCase.Id
        );
        insert task;


        instCase.OwnerId = UserInfo.getUserId();

        Test.startTest();

        update instCase;

        Test.stopTest();

        task = [SELECT Id, OwnerId FROM Task WHERE Id = :task.Id];

        System.assertEquals(UserInfo.getUserId(), task.OwnerId);

    }
}