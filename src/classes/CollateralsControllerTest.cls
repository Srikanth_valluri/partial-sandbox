@isTest
public class CollateralsControllerTest {
   
    static testMethod void testMethodOne(){
        Account accObj = new Account();
        accObj.Name='acc1';
        insert accObj;

        Contact cont = new Contact();
        cont.FirstName='Test';
        cont.LastName='Test';
        cont.Accountid = accObj.id;
        insert cont;
        
        ContentVersion contentVersionInsert = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = Blob.valueOf('Test Content Data'),
                IsMajorVersion = true
            );
             insert contentVersionInsert;

            Announcement__c anncObj = new Announcement__c();
            anncObj.Start_Date__c = system.today().adddays(-3);
            anncObj.End_Date__c = system.today().adddays(3);
            anncObj.Title__c = 'Test';
           // anncObj.Account__c = accObj.id;
            anncObj.Active__c = true;
            anncObj.Description__c = 'Test';
            anncObj.Agency_Type__c = 'Corporate';
            insert anncObj;
            
             ContentVersion contentVersionSelect = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionInsert.Id LIMIT 1];
             List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

            ContentDocumentLink contentlink=new ContentDocumentLink();
            contentlink.LinkedEntityId=anncObj.id;
            contentlink.ShareType= 'I';
            contentlink.ContentDocumentId=documents[0].Id;
            contentlink.Visibility = 'AllUsers'; 
            insert contentlink;
            
            //List<ContentDocument> contentList = createContentDocument(anncObj.id);
            CollateralsController controller = new CollateralsController();
        
    }   
}