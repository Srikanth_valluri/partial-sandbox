/**
 * @File Name          : GenerateSOAForPCCTest.cls
 * @Description        : Test class for GenerateSOAForPCC
 * @Author             : Dipika Rajput
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 6/25/2019, 12:52:20 PM
 * @Modification Log   : 
 *==============================================================================================
 * Ver         Date                     Author                    Modification
 * 
 *==============================================================================================
 * 1.0    6/25/2019                   Dipika Rajput           Initial Version
**/
@isTest
private class GenerateSOAForPCCTest{
	@isTest
	static void itShould(){
		List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

		//create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;
        
        DP_Invoices__c obj=new DP_Invoices__c();
	    obj.Accounts__c = objAccount.id;
		obj.BookingUnits__c = bookingUnitList[0].id;
        insert obj;

		Test.startTest();
            Test.setMock(HttpCalloutMock.class, new DPSoaHttpMock(1));
			GenerateSOAForPCC controller = new GenerateSOAForPCC();
			GenerateSOAForPCC.generateSOA(bookingUnitList[0].Registration_Id__c,obj.id);
		Test.stopTest();
	}
}