@isTest
public class ParentCaseSearchCtrlTest {
    @isTest
    static void testParentCase(){
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
        
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
        System.debug('recTypeId :'+ recTypeId);
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeId );
        //objCase.CaseNumber = 12345;
        insert objCase;
        
        Test.startTest();
        ParentCaseSearchCtrl obj = new ParentCaseSearchCtrl();
        ParentCaseSearchCtrl.listParentCases = new list<case>();
        ParentCaseSearchCtrl.parentSearchKey = objCase.CaseNumber;
        ParentCaseSearchCtrl.parentCaseId = objCase.Id;
        //ParentCaseSearchCtrl.parentId = objCase.Id;
        ParentCaseSearchCtrl.searchParentCase();
        Test.stopTest();
    }
}