public class NEXMO_OB_REQUEST_GEN_NO_MTM{
    public cls_from WA_from;
    public cls_to WA_to;
    public cls_message message;
    public class cls_from {
        public String type; //whatsapp
        public String WA_number;   //447418342136
    }
    public class cls_to {
        public String type; //whatsapp
        public String WA_number;   //919652300133
    }
    public class cls_message {
        public cls_content content;
    }
    public class cls_content {
        public String type; //text
        public String text; //
    }
    public static NEXMO_OB_REQUEST_GEN_NO_MTM parse(String json){
        return (NEXMO_OB_REQUEST_GEN_NO_MTM) System.JSON.deserialize(json, NEXMO_OB_REQUEST_GEN_NO_MTM.class);
    }

    
}