/*--------------------------------------------------------------------------------------------------------------------_
Description: Generic class to generate document from drawloop
=======================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By   | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     | 27-02-2020       | Aishwarya Todkar   | 1. Initial Draft
=======================================================================================================================
*/
public with sharing class DrawloopDocGen {
    public static String SERVER_URL;
    public static String SESSION_ID;
  

/**********************************************************************************************************************
 * Description: method to generate document from drawloop
 * Pararmeter(s): DDP Id, Delivery Option Id, Record Id
 * return: None
 **********************************************************************************************************************/
   
    public static void generateDoc( String strDDPTemplateId, String strTemplateDeliveryId, Id recordId ) {
       
         system.debug('strDDPTemplateId:'+strDDPTemplateId+'strTemplateDeliveryId='+strTemplateDeliveryId+'recordId:'+recordId);
       login();
        if(SESSION_ID != null && SESSION_ID != '') {
            Loop.loopMessage lm = new Loop.loopMessage();
            lm.sessionId = SESSION_ID;
            Map<string, string> variables = new map<String,String>();
                variables.put('deploy', strTemplateDeliveryId);
                lm.requests.add(
                    new Loop.loopMessage.loopMessageRequest(
                            recordId
                            , strDDPTemplateId
                            , variables )
                );
            String response = lm.sendAllRequests();
            system.debug('== response ==' +response );
        }
    }

/**********************************************************************************************************************
 * Description: Method used for login purpose to get the session id
 * Pararmeter(s): None
 * return: None
 **********************************************************************************************************************/
    public static void login(){
        Admin_Login_for_Drawloop__c mc = Admin_Login_for_Drawloop__c.getOrgDefaults();
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://' + mc.Domain__c + '.salesforce.com/services/Soap/u/28.0');
        system.debug('mc.Domain__c'+mc.Domain__c);
        system.debug('mc.Username__c'+mc.Username__c);
        system.debug('mc.Password__c'+mc.Password__c);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', '""');
        request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + mc.Username__c + '</username><password>' + mc.Password__c + '</password></login></Body></Envelope>');

        HttpResponse response = h.send(request);
        system.debug('----Response Body-----'+response.getBody());

        Dom.XmlNode resultElmt = response.getBodyDocument().getRootElement()
        .getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/')
        .getChildElement('loginResponse','urn:partner.soap.sforce.com')
        .getChildElement('result','urn:partner.soap.sforce.com');

        SERVER_URL = resultElmt.getChildElement('serverUrl','urn:partner.soap.sforce.com').getText().split('/services')[0];
        SESSION_ID = resultElmt.getChildElement('sessionId','urn:partner.soap.sforce.com').getText();

        system.debug('--SERVER_URL---'+SERVER_URL);
        system.debug('--SESSION_ID---'+SESSION_ID);
        if(Test.isRunningTest()){
            SESSION_ID = 'none';
        }
    }
}