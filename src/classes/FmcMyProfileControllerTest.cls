@isTest
private class FmcMyProfileControllerTest {

    @isTest
    static void testController() {
        Account account = new Account(Name = 'Test Account');
        insert account;

        CustomerCommunityUtils.customerAccountId = account.Id;

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'Profile');
        Test.setCurrentPage(currentPage);

        Test.startTest();
            FmcMyProfileController controller = new FmcMyProfileController();
            FmcMyProfileController.fetchDueInvoiceData();
        Test.stopTest();
    }

}