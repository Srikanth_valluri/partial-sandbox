/************************************************************************************************
 * @Name              : VirtualHandoverTeamsMeetingController
 * @Test Class Name   : VirtualHandoverTeamsMeetingCtrlTest
 * @Description       : Controller class for Virtual Handover Teams meeting VF page.
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         Rohit         10/08/2020       Created
***********************************************************************************************/

public with sharing class VirtualHandoverTeamsMeetingController {
    public string callingListId {get;set;}
    public String emailList {get; set;}
    public Calling_List__c callinglist {get; set;}
    public String message {get; set;}
    public String subject {get; set;}
    public String messageBody {get; set;}
    public Boolean success;
    public Boolean isUpdate {get; set;}
    public String meetingId;
    public String endPoint{get; set;}
    public String token{get; set;}
    public String body{get; set;}
    public String buttonLabel {get; set;}
    public String pageTitle {get; set;}
    public Boolean updateSuccess{get; set;}
    public String updateResponse{get; set;}
    public API_Integration_Value__c apiValue ;
    private static final String PAGE_TITLE = 'Create/Update Teams Meeting';
    private static final String BUTTON_LABEL = 'Create/Update Meeting';
    private static final String MEETING_CREATION_SUCCESS = 'Meeting Created Successfully';
    private static final String MEETING_CREATION_FAILURE = 'Error while creating Teams Meeting';
    private static final String MEETING_UPDATED_SUCCESS = 'Meeting Updated Successfully';
    private static final String MEETING_UPDATED_FAILURE = 'Meeting Updation Failed';
    ID recordId;
    
    public VirtualHandoverTeamsMeetingController(ApexPages.StandardController controller) {
        recordId = controller.getId();

        emailList = '';
        message = '';
        success = false;
        isUpdate = false;
        updateSuccess = false;
        updateResponse = '';
        buttonLabel = '';
        pageTitle = '';
        messageBody = '';
        
        callingListId = recordId;
        buttonLabel = BUTTON_LABEL;
        pageTitle = PAGE_TITLE;
        
        apiValue = [SELECT Id, Refresh_Token__c, Client_Id__c, Client_Secret__c,
                                                            Endpoint_URL__c, Redirect_URI__c, Scope__c, Event_Creation_URL__c,
                                                            Tenant_Id__c, Username__c, Password__c, Meeting_EndPoint_URL__c
                                                            FROM API_Integration_Value__c WHERE name = 'Virtual Handover Graph API'
                                                            AND  Username__c = 'virtual.handover@damacproperties.com'  LIMIT 1];

        if(recordId != null){
            callinglist = [SELECT Id, Name, Appointment_Start_DateTime__c, Appointment_End_DateTime__c,  
                            Account__r.Name, Booking_Unit__r.Unit_Name__c, Virtual_Meeting_Link__c, Remarks_Pratik__c, 
                            Booking_Unit__r.SnagValue__c, Remarks_Vivek__c, Account__r.Person_Business_Email__c
                           FROM Calling_List__c WHERE Id =: recordId];

            subject = 'Virtual Unit Viewing ' + callinglist.Name + ' : '+ callingList.Id + ' : ' + callingList.Booking_Unit__r.SnagValue__c + '\n\n'
            + ' for your unit ' + callingList.Booking_Unit__r.Unit_Name__c + '\n\n' 
            + ' on ' + callinglist.Appointment_Start_DateTime__c.format('dd/MM/YYYY');
            if(!String.isBlank(callingList.Remarks_Pratik__c)){
                isUpdate = true;
            }
            
            String timeZone = UserInfo.getTimezone().getDisplayName().subStringBefore(' ');
            if(messageBody == ''){
                messageBody = 'Virtual Unit Viewing ' + callinglist.Name + ' : '+ callingList.Id + ' : ' + callingList.Booking_Unit__r.SnagValue__c + '\n'
                + ' for your unit ' + callingList.Booking_Unit__r.Unit_Name__c + '\n' 
                + ' on ' + callinglist.Appointment_Start_DateTime__c.format('dd/MM/YYYY') + ' ' + timeZone ;
            }
        } else{
            message = 'callinglist Id is missing';
        }
    }

    /************************************************************************************************
    * @Description : Invokes createTeamsMeeting() in DAMAC_MSTeamsIntegration with the required
    *                    inputs from the VF Page
    * @Params      : None
    * @Return      : None 
    ************************************************************************************************/
    public void createMeeting(){
        
        if(isUpdate){
                if(!String.isBlank(callingList.Remarks_Vivek__c)){
                    for(String emailAdr: callingList.Remarks_Vivek__c.split(',')){
                        if(emailList != ''){
                            emailList += ',';
                            emailList += emailAdr;
                        }
                    }
                }
                updateMeeting(callinglist.Appointment_Start_DateTime__c, callinglist.Appointment_End_DateTime__c, 
                                                                            emailList, subject, messageBody,  callinglist);
        } else{
                
                String meetingURL;
                meetingURL = VirtualHandover_MSTeamsIntegration.createTeamsMeeting(callinglist.Appointment_Start_DateTime__c, callinglist.Appointment_End_DateTime__c,
                                                                     emailList, subject, messageBody,  callinglist);
                
                    if(!String.isBlank(meetingURL)){
                    message = MEETING_CREATION_SUCCESS; 
                    success = true;
                    callingList.Virtual_Meeting_Link__c = meetingURL;
                    if(!String.isBlank(callinglist.Account__r.Person_Business_Email__c)){
                        emailList += ',';
                        emailList += callinglist.Account__r.Person_Business_Email__c;
                    }
                    callingList.Remarks_Vivek__c = emailList;
                    try{
                        update callingList;
                    }catch(Exception ex){
                        System.debug('Meeting Generation Failed-->' +ex);
                    }
                } else{
                    message = MEETING_CREATION_FAILURE;
                    success = false;
                }
            }
    
        if(!isUpdate || message != ''){
            if(success){
                ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.CONFIRM, message));
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, message));
            }
        }
    }

    /************************************************************************************************
    * @Description : Creates body for sending Update Request to Graph API
    * @Params      : 1. DateTime startTime - Updated startTime of Calendar Event
                     2. DateTime endTime - Updated endTime of Calendar Event
                     3. String emailList - Updated List of Calendar Event Attendess
                     4. String subject - Updated subject of Calendar Event
                     5. String messageBody - Updated Message content for Calendar Event Invite
                     6. Calling_List__c - Related Calling List record
    * @Return      : None
    ************************************************************************************************/   
    public void updateMeeting(DateTime startTime, DateTime endTime, String emailList, String subject, String messageBody, Calling_List__c callingList){
        
        token = VirtualHandover_MSTeamsIntegration.fetchAccessToken();
        String msg = '';
        String stTime = startTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        String enTime = endTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        String name = '';
        String attendees = '';
        for(String emailAdr: emailList.split(',')){
            if(attendees != ''){
                attendees += ',';
            }
            attendees += '{"emailAddress": {"address":"' + emailAdr.trim() + '","name": ""},"type": "required"}';
        }
        body = '{"subject": "' + subject + '", "body": { '
              + '"contentType": "HTML", "content": "' + messageBody + '<br/><br/> Meeting Link: ' + callingList.Virtual_Meeting_Link__c + '<br/><br/>" },'
              + '"start": {"dateTime": "' + stTime + '","timeZone": "Greenwich Standard Time"},'
              + '"end": {"dateTime": "' + enTime + '","timeZone": "Greenwich Standard Time"},'
              + '"attendees": [' + attendees + ']}';  
        endPoint = 'https://graph.microsoft.com/v1.0/me/calendar/events/' + callingList.Remarks_Pratik__c;
    }

    public void updateEvent(){
        callingList.Remarks_Vivek__c = emailList;
        try{
            update callingList;
        }catch(Exception ex){
            System.debug('Meeting Generation Failed-->' +ex);
        }

        if(updateSuccess){
            message = MEETING_UPDATED_SUCCESS;
            ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.CONFIRM, message));
        } else{
            system.debug('Error thrown: ' + updateResponse);
            message = MEETING_UPDATED_FAILURE;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, message));
        }
    }
}