@isTest
public class Damac_UKURRF_WithSolicitorCntlr_Test{
    testMethod static void FormRequestWithSolicitor(){
        Account acc = new Account();
        acc.Name = 'Testing Account';
        acc.Agency_Email__c = 'acc@email.com';
        insert acc;
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c = 'First';
        inquiry.Last_Name__c = 'Last';
        inquiry.Inquiry_Status__c = 'Active';
        inquiry.Email__c = 'test@email.com';
        insert inquiry;
        Form_Request__c formRequest = new Form_Request__c();
        formRequest.Corporate__c = acc.Id;
        formRequest.Inquiry__c = inquiry.Id;
        formRequest.Status__c = 'Sign Now';
        insert formRequest;
        Inventory__c invent = new Inventory__c();
        invent.CM_Price_Per_Sqft__c = 20;
        invent.is_mixed_use__c = '0';
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1';
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Status__c = 'Released';
        invent.special_price__c = 1000000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234';
        invent.Floor_Package_ID__c = '';
        insert invent;

        Form_request_Inventories__c formInv = new Form_request_Inventories__c ();
        formInv.Inventory__c = invent.id;
        formInv.Form_request__c = formRequest.Id;
        
        insert formInv;
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(formRequest);
            
            PageReference pageRef = page.Damac_UKURRF_WithSolicitor;
            pageRef.getParameters().put('recid', String.valueOf(formRequest.Id));
            
            Test.setCurrentPage(pageRef);
            Damac_UKURRF_WithSolicitorCntlr frwithSoli = new Damac_UKURRF_WithSolicitorCntlr();
        Test.stopTest();
    }    
}