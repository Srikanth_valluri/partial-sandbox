@RestResource(urlMapping='/DAMACTrainingServices/*')
global class DAMAC_TrainingServices {
    
    @HttpPost
    global static void doPost(){
        
        RestContextHandler handler = new RestContextHandler(true);
        RestRequest req = handler.restRequest;
        string action = '';
        list<Log__c> lstLogs = new list<Log__c>();
        Map<String, string> params = new map<string, string>(); 
        
        try{
            System.debug('>>>>>requestURI>>>>>>>'+req.requestURI);
            action = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
            System.debug('>>>>>action>>>>>>>'+action);
            params = handler.getRequestParams();
            System.debug('>>>>>params>>>>>>>'+params); 
            //List of agencies
            if(action.toLowerCase() == 'agencies')
                handler = DAMACTrainingUtils.getAgencies();

            //list of trainings
            if(action.toLowerCase() == 'trainings')
                handler = DAMACTrainingUtils.getTrainings();
            
            if(action.toLowerCase() == 'attendee')
                handler = DAMACTrainingUtils.createAttendees(params.get('data'));

            lstLogs.add(new Log__c(Description__c = (params != null && !params.isEmpty() ? string.valueOf(params): ''), Type__c = action, 
                                Response__c = string.valueOf(handler.response.data), Status__c = 'SUCCESS'));    
            handler.finalize();
        }
        catch (Exception exc) {
            system.debug('>>>>>>>>>>ERROR>>>>>>>>>>>' +exc.getmessage()+'>>>>>>LineNO>>>>>'+exc.getlinenumber());
            system.debug('>>>>>>>>>>ERROR>>>>>>>>>>>'+params);
            handler.response.success = false;
            handler.response.statusCode = WebServiceResponse.STATUS_CODE_SYSTEM_ERROR;
            handler.response.message = exc.getMessage()+'-'+exc.getLineNumber()+params.isEmpty();
            lstLogs.add(new Log__c(Description__c =  (params != null && !params.isEmpty() ? string.valueOf(params): ''),Type__c = (action == '' ? req.requestURI : action), 
                                        Response__c = null, Status__c = 'ERROR'));
            handler.finalize(exc);
        }
        if(!lstLogs.isEmpty() && System.label.Log_Training_Services == 'true')
            insert lstLogs;
    }
}