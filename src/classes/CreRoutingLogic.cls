/**
 * Logic class for CreRoutingWebservice
 */
public class CreRoutingLogic {
    public static String EXTENSION_FOR_UNREGISTERED_NUMBER = '6000';
    public static String EXTENSION_FOR_UNREGISTERED_NUMBER_QATAR = '6000';//AK updated on 23/may/2020:6040
    public static String EXTENSION_FOR_UNREGISTERED_NUMBER_FM = '6136';
    public static String EXTENSION_FOR_FAM_CBR = '2025';
    Public static string strLabel = String.isNotBlank(System.Label.Run_New_CTI_Logic) ? System.Label.Run_New_CTI_Logic : '';
    public static Account registeredAccount;
    public Static String IVRCheckTemp = '';

    /**
     * Method to process request and send the response
     */
    public static void processCreRoutingRequest() {
        CreRoutingResponseBody response = new CreRoutingResponseBody();

        // Get request body
        String requestBody = RestContext.request.requestBody.toString();
        System.debug('===== requestBody : ' + requestBody);

        try {
            // Parse request body
            CreRoutingRequestBody request = (CreRoutingRequestBody) JSON.deserializeStrict(
                requestBody,
                CreRoutingRequestBody.Class
            );
            System.debug('===== request : ' + request);

            if (Request != null && String.isNotBlank(request.callingNumber)) {   
               
                Map<String, FM_CRE_CTI_Routing_Setting__c> mapFMCRECTIRouting = FM_CRE_CTI_Routing_Setting__c.getAll();
                
                system.debug('<<<<request.mapFMCRECTIRouting :  '+ mapFMCRECTIRouting);
                system.debug('<<<<mapFMCRECTIRouting.containsKey(request.calledNumber) :  '+ mapFMCRECTIRouting.containsKey(request.calledNumber));
                 //Added New IVR Logic: 01dec2020
                String IVRCheck = Test.isRunningTest()? IVRCheckTemp : Label.NEW_IVR_Check ;
                //if( IVRCheck.ContainsIgnoreCase('Y'))
                    //{
                 if(request.calledNumber=='5156111' || request.calledNumber=='7049077')
                     {
                     RestContext.response.statusCode = 200;response.extension = '9500';
                     }
                     else if(request.calledNumber=='5156120')
                     {
                     RestContext.response.statusCode = 200;response.extension = '9503';
                     }
                     else if(request.calledNumber=='5156121'){
                      RestContext.response.statusCode = 200;response.extension='6121';
                     }
                     else if(request.calledNumber=='5156122'){
                     RestContext.response.statusCode = 200;response.extension='6122';
                     }
                     else if(request.calledNumber=='5156123'){
                     RestContext.response.statusCode = 200;response.extension='9504';
                     
                     system.debug('<<<<request.calledNumber :  '+ request.calledNumber);
                }      
                else if( String.isNotBlank(request.calledNumber) 
                 && String.isNotBlank(System.Label.Run_LOAMS_New_IVR_Logic)
                 && System.Label.Run_LOAMS_New_IVR_Logic.equalsIgnoreCase('Y') 
                 && mapFMCRECTIRouting != null && mapFMCRECTIRouting.size() > 0
                 && mapFMCRECTIRouting.containsKey(request.calledNumber) 
                 && mapFMCRECTIRouting.get(request.calledNumber) != null ) {
                    system.debug('<<<<in contains :  '+mapFMCRECTIRouting.get(request.calledNumber).Extensions__c);
                    RestContext.response.statusCode = 200;
                    response.extension = mapFMCRECTIRouting.get(request.calledNumber).Extensions__c;
                    system.debug('<<<<in response.extension :  '+response.extension);
                } else if( mapFMCRECTIRouting != null 
                        && mapFMCRECTIRouting.size() > 0 
                        && !mapFMCRECTIRouting.containsKey(request.calledNumber) ) {
                 
                    List<Account> registeredAccounts = CtiTntegrationUtility.getRegisteredAccount(request.callingNumber);
                    System.debug('======== registeredAccounts : ' + registeredAccounts);
                    
                    // 1. Check if the calling number is for a registered account
                    if (registeredAccounts.size() != 0) {

                        // TODO: Considering phone fields will be unique for a account
                        registeredAccount = registeredAccounts[0];
                        System.debug('======== registeredAccount : ' + registeredAccount);
                                               
                        //Added FAM Logic
                        
                        if( Label.FAM_Check.ContainsIgnoreCase('Y') && String.isNotBlank(registeredAccount.FAM_CAUTION__c) && Label.FAMFieldValue.containsIgnoreCase(registeredAccount.FAM_CAUTION__c) ){
                            String availableCre = getAvailabeCresForAccount(registeredAccount,request.onlineExtensions,request.callingNumber);
                            System.debug('======== availableCre : ' + availableCre);
                            if(String.isNotBlank(availableCre)) {RestContext.response.statusCode = 200;response.extension = availableCre;  
                            }else{ RestContext.response.statusCode = 200;response.extension = EXTENSION_FOR_FAM_CBR;
                            }
                            
                        }else if( String.isNotBlank(strLabel) && strLabel.equalsIgnoreCase('Y') ) {
                            String availableCre = getAvailabeCresForAccount(registeredAccount,request.onlineExtensions,request.callingNumber
                            );
                            System.debug('======== availableCre : ' + availableCre);
                            
                            


                            if ( 
                               String.isNotBlank( System.Label.Qatar_CTI_Called_Number ) && !System.Label.Qatar_CTI_Called_Number.contains(request.calledNumber) && String.isNotBlank( System.Label.FM_CTI_Called_Number )
                              && !System.Label.FM_CTI_Called_Number.contains(request.calledNumber) && String.isNotEmpty(availableCre) && String.isNotBlank(request.calledNumber)) {response.extension = availableCre;  }
                            /* else {

                                // If no cre available, send the extension of CRE Pool with the customers primary language
                                String language =
                                    String.isNotEmpty(registeredAccount.Primary_Language__c) ? registeredAccount.Primary_Language__c : 'English';
                                String crePoolExtension = getCREPoolExtensionForLanguage(language, request.callingNumber, request.calledNumber);
                                System.debug('======== crePoolExtension : ' + crePoolExtension);
                                if (String.isNotEmpty(crePoolExtension)) {
                                    response.extension = crePoolExtension;
                                } else {
                                    response.errorCode = '400';
                                    response.errorMessage = 'No CRE pool Found';
                                }
                              */
                              else if (String.isNotBlank(request.calledNumber) && String.isNotBlank(System.Label.Qatar_CTI_Called_Number)  && System.Label.Qatar_CTI_Called_Number.contains(request.calledNumber)
                                && String.isNotBlank(System.Label.Run_CTI_For_Qatar)  && System.Label.Run_CTI_For_Qatar.equalsIgnoreCase( 'true' )     ) {
                                RestContext.response.statusCode = 200;  response.extension = EXTENSION_FOR_UNREGISTERED_NUMBER_QATAR;  
                              } else if ( String.isNotBlank(request.calledNumber)  && String.isNotBlank(System.Label.FM_CTI_Called_Number) && System.Label.FM_CTI_Called_Number.contains(request.calledNumber) 
                                && String.isNotBlank(System.Label.Run_CTI_For_FM)&& System.Label.Run_CTI_For_FM.equalsIgnoreCase('true')) 
                                {RestContext.response.statusCode = 200; response.extension = EXTENSION_FOR_UNREGISTERED_NUMBER_FM; 
                              }else {
                                RestContext.response.statusCode = 200;
                                response.extension = EXTENSION_FOR_UNREGISTERED_NUMBER; 
                              }              
                            //}
                        }
                        else if( String.isNotBlank(strLabel) & strLabel.equalsIgnoreCase('N') ) {//AKISHOR: 09feb2020 reverted to CC format
                        
                           if (String.isNotBlank(request.calledNumber) && String.isNotBlank(System.Label.Qatar_CTI_Called_Number)&& System.Label.Qatar_CTI_Called_Number.contains(request.calledNumber)
                            && String.isNotBlank(System.Label.Run_CTI_For_Qatar) && System.Label.Run_CTI_For_Qatar.equalsIgnoreCase( 'true' )
                        ) { 
                        RestContext.response.statusCode = 200;  response.extension = EXTENSION_FOR_UNREGISTERED_NUMBER_QATAR;  
                        } 
                        else if ( String.isNotBlank(request.calledNumber) && String.isNotBlank(System.Label.FM_CTI_Called_Number) && System.Label.FM_CTI_Called_Number.contains(request.calledNumber) 
                          && String.isNotBlank(System.Label.Run_CTI_For_FM)  && System.Label.Run_CTI_For_FM.equalsIgnoreCase('true')) {
                            RestContext.response.statusCode = 200; response.extension = EXTENSION_FOR_UNREGISTERED_NUMBER_FM; 
                        }else {
                            RestContext.response.statusCode = 200;
                            response.extension = EXTENSION_FOR_UNREGISTERED_NUMBER; 
                        }
                        }
                    } else {
                        // If number is not registered, the extension number of the welcome greeting will be returned (6000)
                        
                        // If called number is for Qatar then return 6040 else 6000
                        if (String.isNotBlank(request.calledNumber) 
                            && String.isNotBlank(System.Label.Qatar_CTI_Called_Number)&& System.Label.Qatar_CTI_Called_Number.contains(request.calledNumber)
                            && String.isNotBlank(System.Label.Run_CTI_For_Qatar) && System.Label.Run_CTI_For_Qatar.equalsIgnoreCase( 'true' )
                        ) 
                        { 
                        RestContext.response.statusCode = 200;  response.extension = EXTENSION_FOR_UNREGISTERED_NUMBER_QATAR;  
                        } 
                        else if ( String.isNotBlank(request.calledNumber) 
                          && String.isNotBlank(System.Label.FM_CTI_Called_Number) && System.Label.FM_CTI_Called_Number.contains(request.calledNumber) 
                          && String.isNotBlank(System.Label.Run_CTI_For_FM)  && System.Label.Run_CTI_For_FM.equalsIgnoreCase('true')) 
                          {
                            RestContext.response.statusCode = 200; response.extension = EXTENSION_FOR_UNREGISTERED_NUMBER_FM; 
                        }else 
                        {
                            RestContext.response.statusCode = 200;
                            response.extension = EXTENSION_FOR_UNREGISTERED_NUMBER; 
                        }
                    }
                }
                

            } 
            else {
                RestContext.response.statusCode = 400;
                response.errorCode = '400';
                response.errorMessage = 'Calling Number not found';
            }
        } catch (Exception ex) { response.errorMessage = ex.getMessage(); RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response)); Restcontext.response.statusCode = 400;            System.debug('============= response : ' + JSON.serialize(response));   return;
        }
        System.debug('============= response : ' + JSON.serialize(response));
        // Send response
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
    }

    /**
     * Method to get account for calling number
     *
    public static List<Account> getRegisteredAccount(String callingNumber) {
        List<Account> accounts = new List<Account>();
        accounts =
            [
                SELECT
                    Id,
                    Name,
                    AnnualRevenue,
                    Primary_Language__c,
                    Primary_CRE__c,
                    Secondary_CRE__c,
                    FAM_CAUTION__c ,
                    Tertiary_CRE__c
                FROM
                    Account
                WHERE
                    Mobile_Phone_Encrypt__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_2__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_3__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_4__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_5__pc =: callingNumber
            ];
        System.debug('========== accounts: ' + accounts);

        if (accounts.size() == 0) {
            List<Contact> contacts =
                [
                    SELECT
                        Id,
                        Name,
                        Account.AnnualRevenue,
                        Account.Primary_Language__c,
                        Account.Primary_CRE__c,
                        Account.Secondary_CRE__c,
                        Account.Tertiary_CRE__c
                    FROM
                        Contact
                    WHERE
                        Mobile_Phone_Encrypt__c =: callingNumber
                        OR Mobile_Phone_Encrypt_2__c =: callingNumber
                        OR Mobile_Phone_Encrypt_3__c =: callingNumber
                        OR Mobile_Phone_Encrypt_4__c =: callingNumber
                        OR Mobile_Phone_Encrypt_5__c =: callingNumber
                ];
            if (contacts.size() > 0) {
                System.debug('=========== contacts : ' + contacts);
                if (contacts[0].Account != null) {
                    System.debug('=========== contacts[0].Account : ' + contacts[0].Account);
                    accounts.add(contacts[0].Account);
                }
            }
        }
        return accounts;
    }
*/
    /**
     * Method to check if a account is vip account or not
     */
    /*public static Boolean isVipAccount(Account account) {
        List <CRE_Allocation_Customer_Type_Setting__mdt> lstCustomerTypesSetting = getCustomerTypesSetting();

        // Get customer type of account
        String customerType = getCustomerType(
            account.AnnualRevenue,
            lstCustomerTypesSetting
        );

        if (String.isNotBlank(customerType) && customerType != 'Others') {
            return true;
        } else {
            return false;
        }
    }*/
    /**
     * Method to check if a account is Elite/Top Broker account or not
     */
    public static Boolean CheckEliteCustomer(Account account) {
        System.debug('check Elite account = ' + account);
        if ( account.Customer_Category__c.equalsIgnoreCase('Elite') || account.Customer_Category__c.equalsIgnoreCase('Top Broker') ) {   return true;   } else {   return false;  }
    }

    /*public static List <CRE_Allocation_Customer_Type_Setting__mdt> getCustomerTypesSetting() {
        return new List <CRE_Allocation_Customer_Type_Setting__mdt>(
            [ SELECT Id
                   , MasterLabel
                   , Maximum_Value__c
                   , Minimum_Value__c
                   , DeveloperName
                   , Is_Max__c
                FROM CRE_Allocation_Customer_Type_Setting__mdt ] ) ;
    }

    public static String getCustomerType(
        Decimal decAmount,
        List <CRE_Allocation_Customer_Type_Setting__mdt> lstCustomerTypesSetting
    ) {
        for( CRE_Allocation_Customer_Type_Setting__mdt objType : lstCustomerTypesSetting ) {
            if( objType.Minimum_Value__c <= decAmount && ( ( objType.Maximum_Value__c != null && objType.Maximum_Value__c > decAmount ) || objType.Is_Max__c )  ) {
                return objType.MasterLabel;
            }
        }
        return '';
    }*/

    /**
     * Method is get prefered cre extensions of account form list of online extenions
     */
    public static String getAvailabeCresForAccount(
        Account registeredAccount,
        List<String> onlineExtensions,
        String callingNumber
    ) {
        // Get accounts prefered CRE's
        Set<Id> creUserIds = new Set<Id>();
        if (registeredAccount.Primary_CRE__c != null) { creUserIds.add(registeredAccount.Primary_CRE__c);
        }
        if (registeredAccount.Secondary_CRE__c != null) {  creUserIds.add(registeredAccount.Secondary_CRE__c);
        }
        if (registeredAccount.Tertiary_CRE__c != null) {   creUserIds.add(registeredAccount.Tertiary_CRE__c);
        }

        System.debug('>>>>>>>>>>> creUserIds : ' + creUserIds);
        List<String> onlineExtensionsList = new List<String>();
        
        /*
        // Have to split the extenions string into list of extensions. 3cx will be sending extensions
        // in ["6502,6503,6504"] format
        if (onlineExtensions.size() != 0) {
            System.debug('>>>>>>>>>>> onlineExtensions[0] : ' + onlineExtensions[0]);
            onlineExtensionsList = onlineExtensions[0].split(',');
        }
        System.debug('>>>>>>>>>>> onlineExtensionsList : ' + onlineExtensionsList);
        */

        

    
  // Get online CRE's user records
    Map<Id, User> creUsersMap = new Map<Id, User>(
      [
        SELECT
          Id,
          Extension
        FROM
          User
        WHERE
          Extension IN :onlineExtensions //onlineExtensionsList
        AND
          Id IN :creUserIds
      ]
    );
        
        System.debug('>>>>>>>>>>> creUsersMap : ' + creUsersMap);

        // Get last interacted CRE
        User lastInteractedCRE = getLastInteractedCRE(callingNumber);
        System.debug('>>>>>>>>>> lastInteractedCRE : ' + lastInteractedCRE);

        // If last interacted CRE is primary, sceondry or tertiary, send last interacted CRE extension
        if (lastInteractedCRE != null && creUsersMap.containsKey(lastInteractedCRE.Id)) {  return lastInteractedCRE.Extension;
        }

        User primaryUser;
        User secondaryUser;
        User tertiaryUser;

        // Sort available extensions according to user preferences
        for (User creUser : creUsersMap.values()) {
            if (creUser.Id == registeredAccount.Primary_CRE__c) { primaryUser = creUser;
            } else if (creUser.Id == registeredAccount.Secondary_CRE__c) {   secondaryUser = creUser;
            } else if (creUser.Id == registeredAccount.Tertiary_CRE__c) {   tertiaryUser = creUser;
            }
        }

        // Send available extension according to user PageReference
        if (primaryUser != null) {      return primaryUser.Extension;
        } else if (secondaryUser != null) {  return secondaryUser.Extension;
        } else if (tertiaryUser != null) {     return tertiaryUser.Extension;
        }

        return '';
    }

    /**
     * Method to get last interacted CRE
     */
    public static User getLastInteractedCRE(String callingNumber) {
        List<Call_Log__c> callLogs =
            [
                SELECT
                    Id,
                    CRE__r.Id,
                    CRE__r.Extension
                FROM
                    Call_Log__c
                WHERE
                    Calling_Number__c =:callingNumber
                Order By
                    CreatedDate
            ];
        if (callLogs.size() > 0) {   return callLogs[0].CRE__r;
        }
        return null;
    }

    /**
     *  Method to get CRE Pool Extension for prefered language
     */
    public static String getCREPoolExtensionForLanguage(String language, String callingNumber,  String calledNumber) {
        if ( String.isNotBlank(calledNumber) 
          && String.isNotBlank(System.Label.FM_CTI_Called_Number) 
          && System.Label.FM_CTI_Called_Number.contains(calledNumber) //calledNumber == System.Label.FM_CTI_Called_Number 
          && String.isNotBlank(System.Label.Run_CTI_For_FM) 
          && System.Label.Run_CTI_For_FM.equalsIgnoreCase('true')) {
            List<FM_CRE_Pool_Detail__c> crePoolDetails = FM_CRE_Pool_Detail__c.getall().values();
            for (FM_CRE_Pool_Detail__c crePoolDetail : crePoolDetails) {
                if (crePoolDetail.Language__c == language) {    return crePoolDetail.Extension__c;    }
            }
        } else if ( String.isNotBlank(calledNumber) 
          && String.isNotBlank( System.Label.Qatar_CTI_Called_Number ) 
          && System.Label.Qatar_CTI_Called_Number.contains(calledNumber) // calledNumber == System.Label.Qatar_CTI_Called_Number 
          && String.isNotBlank( System.Label.Run_CTI_For_Qatar ) 
          && System.Label.Run_CTI_For_Qatar.equalsIgnoreCase( 'true' ) ) {
            List<CRE_Qatar_Pool_Detail__c> crePoolDetails = CRE_Qatar_Pool_Detail__c.getall().values();
            for (CRE_Qatar_Pool_Detail__c crePoolDetail : crePoolDetails) {
                if (crePoolDetail.Language__c == language) {    return crePoolDetail.Extension__c;      }
            }
        } else {
            List<CRE_Pool_Detail__c> crePoolDetails = CRE_Pool_Detail__c.getall().values();
            for (CRE_Pool_Detail__c crePoolDetail : crePoolDetails) {
                if (crePoolDetail.Language__c == language) {     return crePoolDetail.Extension__c;    }
            }
        }
        return '';
    }

    /**
     *  Method to get CRE Pool Extension for prefered language
     */
    public static String getIVRExtensionForLanguage(String language, String callingNumber,  String calledNumber) {
        
        if ( String.isNotBlank(calledNumber) 
          && String.isNotBlank(System.Label.FM_CTI_Called_Number) 
          && System.Label.FM_CTI_Called_Number.contains(calledNumber)
          && String.isNotBlank(System.Label.Run_CTI_For_FM) 
          && System.Label.Run_CTI_For_FM.equalsIgnoreCase('true')) {
            List<FM_Non_Vip_IVR_Queue_Extensions__c> ivrDetails = FM_Non_Vip_IVR_Queue_Extensions__c.getall().values();
            for (FM_Non_Vip_IVR_Queue_Extensions__c ivrDetail : ivrDetails) {
                if (ivrDetail.Language__c == language) {      return ivrDetail.Extension__c;     }
            }           
        } else if ( String.isNotBlank(calledNumber) 
          && String.isNotBlank( System.Label.Qatar_CTI_Called_Number ) 
          && System.Label.Qatar_CTI_Called_Number.contains(calledNumber) // calledNumber == System.Label.Qatar_CTI_Called_Number 
          && String.isNotBlank( System.Label.Run_CTI_For_Qatar ) 
          && System.Label.Run_CTI_For_Qatar.equalsIgnoreCase( 'true' ) ) {
            List<Qatar_Non_Vip_IVR_Queue_Extensions__c> ivrDetails = Qatar_Non_Vip_IVR_Queue_Extensions__c.getall().values();
            for (Qatar_Non_Vip_IVR_Queue_Extensions__c ivrDetail : ivrDetails) {
                if (ivrDetail.Language__c == language) {     return ivrDetail.Extension__c;      }
            }           
        } else {
            List<Non_Vip_IVR_Queue_Extensions__c> ivrDetails = Non_Vip_IVR_Queue_Extensions__c.getall().values();
            for (Non_Vip_IVR_Queue_Extensions__c ivrDetail : ivrDetails) {
                if (ivrDetail.Language__c == language) {    return ivrDetail.Extension__c;   }
            }           
        }
        return '';
    }

    /**
     * Wrapper class for CreRoutingWebService request body
     */
    public class CreRoutingRequestBody {
        public String callingNumber {get; set;}
        public String calledNumber {get; set;}
        public List<String> onlineExtensions {get; set;}

        public CreRoutingRequestBody() {
            this.callingNumber = '';
            this.calledNumber = '';
            this.onlineExtensions = new List<String>();
        }
    }

    /**
     * Wrapper class for CreRoutingWebService response body
     */
    public class CreRoutingResponseBody {
        public String extension {get; set;}
        public String errorCode {get; set;}
        public String errorStatus {get; set;}
        public String errorMessage {get; set;}

        public CreRoutingResponseBody() {
            this.extension = '';
            this.errorCode = '';
            this.errorStatus = '';
            this.errorMessage = '';
        }
    }
}