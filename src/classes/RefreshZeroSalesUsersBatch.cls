/****************************************************************************************************
* Name          : RefreshZeroSalesUsersBatch                                                        *
* Description   : Batch class to refresh Zero Sales User records                                    *
* Created Date  : 08/04/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE            COMMENTS                                                *
* 1.0   Craig Lobo          08/04/2018      Initial Draft.                                          *
****************************************************************************************************/

global class RefreshZeroSalesUsersBatch implements Database.Batchable<sObject> {

    global String CURRENT_MONTH_NAME = Datetime.now().format('MMMMM');
    global String CURRENT_YEAR       = String.valueOf(Datetime.now().year());

    /**
     * Constructor
     */
    global RefreshZeroSalesUsersBatch() {
        
    }

    /** 
     * Start Block
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Integer monthNumber = System.today().Month();
        String zeroSalesUserQuery   = ' SELECT Id, Name, Closed__c, Month_Number__c, '
                                    + ' Current_Month_Target__c, Employee__c '
                                    + ' FROM Zero_Sales_User__c '
                                    + ' WHERE Closed__c = false '
                                    + ' AND Month_Number__c = :monthNumber ';
        system.debug('>>>zeroSalesUserQuery>>>>>'+zeroSalesUserQuery);
        return Database.getQueryLocator(zeroSalesUserQuery);
    }

    /**
     * Execute Block
     */
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        system.debug('>>>scope>>>>>  ' + scope);

        List<Zero_Sales_User__c> zsUserProcessingList = new List<Zero_Sales_User__c>();
        for(Zero_Sales_User__c zsRecord : (List<Zero_Sales_User__c>) scope) {
            system.debug('>>>zsRecord.Current_Month_Target__c>>>>>  ' + zsRecord.Current_Month_Target__c);
            if (String.isBlank(zsRecord.Current_Month_Target__c)) {
                zsUserProcessingList.add(zsRecord);
            }
        }

        if (!zsUserProcessingList.isEmpty()) {

            Map<String, Zero_Sales_User__c> userIdZeroSalesMap =  new Map<String, Zero_Sales_User__c> ();
            system.debug('>>>CURRENT_YEAR>>>>>  ' + CURRENT_YEAR);
            system.debug('>>>CURRENT_MONTH_NAME>>>>>  ' + CURRENT_MONTH_NAME);

            for (Zero_Sales_User__c zsUserRec : zsUserProcessingList) {
                userIdZeroSalesMap.put(zsUserRec.Employee__c, zsUserRec);
            }

            for(Target__c targetRec : [SELECT Target__c
                                            , User__c
                                            , Month__c
                                            , Year__c 
                                         FROM Target__c 
                                        WHERE User__c IN :userIdZeroSalesMap.keySet()
                                          AND Year__c = :CURRENT_YEAR
                                          AND Month__c = :CURRENT_MONTH_NAME
            ]) {
                if (targetRec.User__c != null
                    && userIdZeroSalesMap.containsKey(targetRec.User__c)
                ) {
                    userIdZeroSalesMap.get(targetRec.User__c).Current_Month_Target__c 
                        = targetRec.Month__c  + ' - AED ' + targetRec.Target__c;
                }
            }

            // DML
            update userIdZeroSalesMap.values();
        }

    }

    /**
     * Finish Block
     */
    global void finish(Database.BatchableContext BC) {

        // Call gthe method to refresh all the Action Plan values
        UpdateUserActionPlan userAPInstance = new UpdateUserActionPlan();
        userAPInstance.refreshUserActionPlanRecords('All', null);

    }

}