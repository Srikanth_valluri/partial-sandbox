/***********************************************************************************
* Description - Test class developed for ComplaintProcessEscalationHandler
*
* Version            Date            Author            Description
* 1.0                12/12/17        Naresh (accely)           Initial Draft
**************************************************************************************/
@isTest(SeeAllData=false)
private class ComplaintProcessEscalationHandlerTest
{
   
    // Test Method: 
    public static testmethod void Test_sendNotificationToDirector(){
       test.startTest();
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Complaint').getRecordTypeId();
        
        Account acc = TestDataFactory_CRM.createPersonAccount();
        insert acc;
        System.assert(acc != null); 
      
        Case newCase = new Case(); 
        newCase.AccountId = acc.Id ;
        newCase.RecordTypeId  = devRecordTypeId;
        newCase.Credit_Note_Amount__c  = 5000;
        newCase.Customer_Connected_Through_CTI__c  = false;
        newCase.Status = 'CRE Notified' ;
        insert newCase; 
        System.assert(newCase !=null);
        System.assertEquals(newCase.Status, 'CRE Notified');
        
        newCase = [Select Id, CaseNumber, OwnerId, Customer_Connected_Through_CTI__c, Status From Case Where Id =: newCase.Id];
        List<Case> lstsr = new List<Case>();
        lstsr.add(newCase);
        
        ComplaintProcessEscalationHandler.sendNotificationToDirector(lstsr);
        //ComplaintProcessEscalationHandler.sendNotificationToSVP(lstsr);
        ComplaintProcessEscalationHandler.HandleEscalationProcess(lstsr);
       
        newCase.Status = 'Escalated to Director' ; 
        update newCase ;
        System.assertEquals(newCase.Status, 'Escalated to Director');
        List<Case> lstsrUpdate = new List<Case>();
        lstsrUpdate.add(newCase);
        ComplaintProcessEscalationHandler.HandleEscalationProcess(lstsrUpdate);
        test.StopTest();
    }
    
}