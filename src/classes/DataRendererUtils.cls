public class DataRendererUtils {

    public static final String FIELDLIST_STRATEGY   = 'fieldlist';
    public static final String FIELDSET_STRATEGY    = 'fieldset';
    public static final String CLASS_STRATEGY       = 'class';

    public static final String FIELD_NAME           = 'field';
    public static final String FIELD_LABEL          = 'title';
    public static final String REFERENCED_FIELDS    = 'referencedFields';
    public static final String ON_CLICK             = 'onClick';
    public static final String H_REF                = 'href';

    public class GenericDataRendererException extends Exception {}

    public static DataDisplayConfig loadData(DataDisplayConfig config){
        //FIELDLIST_STRATEGY FIELDSET_STRATEGY
        if( CLASS_STRATEGY.equalsIgnoreCase( config.strategy )  ){

            if( String.isBlank( config.dataProviderClass ) ){
                throw new GenericDataRendererException('Data Provider Class value should not be blank. ');
            }

            Type classType = Type.forName( config.dataProviderClass );
            DataProvider dataProvider = (DataProvider)classType.newInstance();

            return dataProvider.getData(config);

        } else {


        }

        return config;
    }

    public static DataDisplayConfig extractConfig(DataDisplayConfig pConfig){
        System.debug('pConfig = ' + JSON.serializePretty(pConfig));
        Document configDocument = [
            SELECT Id
                 , Body
              FROM Document
             WHERE Name = :pConfig.configJSON
             LIMIT 1
        ];

        String jsonString = configDocument.Body == null ? null : configDocument.Body.toString() ;

        if( null == jsonString ){
            throw new GenericDataRendererException('Configuration file is empty.');
        }

        System.debug('jsonString = ' + jsonString);
        DataDisplayConfig config = (DataDisplayConfig) JSON.deserialize(jsonString, DataDisplayConfig.class);
        config.recordId = pConfig.recordId;
        config.filter = pConfig.filter;
        config.recordLimit = pConfig.recordLimit == NULL ? config.recordLimit : pConfig.recordLimit;
        return config;
    }

    public static List<Map<String,Object>> wrapData(DataDisplayConfig displayConfig, List<SObject> dataList){

        List<Map<String,Object>> dataWrappedList = new List<Map<String,Object>>();

        if( displayConfig.fieldlist == null || displayConfig.fieldlist.isEmpty() ){
            throw new GenericDataRendererException('Field list not configured correctly.');
        }

        Set<String> setFields = new Set<String>();
        Schema.SObjectType objectToken = Schema.getGlobalDescribe().get(displayConfig.objectName);
        if (objectToken != NULL) {
            setFields = objectToken.getDescribe().fields.getMap().keySet();
        }

        for(Sobject recordInstance : dataList){

            Map<String,Object> recordMap = new Map<String,Object>();

            recordMap.put( 'Id' ,  recordInstance.Id);

            if (displayConfig.fieldlist != NULL) {
                for( Map<String,String> fieldConfig : displayConfig.fieldlist){
                    String fieldAPIName = fieldConfig.get(FIELD_NAME);
                    if ( String.isBlank(fieldAPIName) ) {
                        continue;
                    }
                    recordMap.put( fieldAPIName ,  getFieldValue(fieldAPIName, recordInstance));

                    String href = fieldConfig.get(H_REF);
                    if (String.isNotBlank(href)) {
                        href = resolveMergeFields(href, recordInstance);
                        recordMap.put(H_REF, href);
                    }

                    String onClick = fieldConfig.get(ON_CLICK);
                    if (String.isBlank(onClick)) {
                        continue;
                    }
                    recordMap.put( fieldAPIName + ON_CLICK , resolveMergeFields(onClick, recordInstance) );
                }
            }

            if (displayConfig.detailFieldlist != NULL) {
                for( Map<String,String> fieldConfig : displayConfig.detailFieldlist){
                    String fieldAPIName = fieldConfig.get(FIELD_NAME);
                    if ( String.isBlank(fieldAPIName) ) {
                        continue;
                    }
                    recordMap.put( fieldAPIName ,  getFieldValue(fieldAPIName, recordInstance));

                    String href = fieldConfig.get(H_REF);
                    if (String.isNotBlank(href)) {
                        href = resolveMergeFields(href, recordInstance);
                        recordMap.put(H_REF, href);
                    }

                    String onClick = fieldConfig.get(ON_CLICK);
                    if (String.isBlank(onClick)) {
                        continue;
                    }
                    recordMap.put( fieldAPIName + ON_CLICK , resolveMergeFields(onClick, recordInstance) );
                }
            }

            dataWrappedList.add(recordMap);

        }

        return dataWrappedList;
    }

    private static Object getFieldValue(String fieldAPIName, Sobject recordInstance) {
        List<String> fieldNameParts = fieldAPIName.split('\\.');
        Sobject record = recordInstance;
        for (Integer i = 0; i < fieldNameParts.size() - 1; i++) {
            record = record.getSobject(fieldNameParts[i]);
        }
        Object fieldRawValue;
        try {
            fieldRawValue = (record == NULL) ? NULL : record.get(fieldNameParts[fieldNameParts.size()-1]);
        } catch(Exception excp) {
            return fieldAPIName;
        }
        return (fieldRawValue == null)
                ? ''
                : (fieldRawValue instanceof Date
                    ? ((Date) fieldRawValue)
                : (fieldRawValue instanceof DateTime
                        ? ((DateTime) fieldRawValue)
                        : fieldRawValue));
    }

    private static String resolveMergeFields(String text, Sobject record) {
        while (text.indexOf('{!') < text.indexOf('}')) {
            String mergeField = text.substringAfterLast('{!');
            mergeField = mergeField.substringBeforeLast('}');
            mergeField = String.isNotBlank(mergeField) ? mergeField.toLowerCase() : mergeField;

            String fieldValue = String.valueOf(getFieldValue(mergeField, record));
            String tempText = text;
            String prefix = tempText.substringBeforeLast('{!');
            String suffix = tempText.substringAfterLast('}');
            text = prefix + fieldValue + suffix;
        }
        return text;
    }
}