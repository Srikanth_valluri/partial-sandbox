/************************************************************************************************
Description :                                                                                   *
------------------------------------------------------------------------------------------------*
Version     Date        Author              Description                                         *
1.0                                         Initial Draft                                       *
1.1         30/7/19     Arjun Khatri        Added error message for Blank Attachment Url        *
************************************************************************************************/
public without sharing class GenerateLienLetter{
    public String caseId;
    public Case objC;
    public GenerateLienLetter(ApexPages.StandardController stdCon){
        caseId = stdCon.getId();
        
        objC = [Select Id
                     , Booking_Unit__c 
                     , Booking_Unit__r.Unit_Type__c                    
                     , Registration_ID__c
                from Case 
                where Id =: caseId];
    }
    
    public void callDrawloop(){
        String docsErrorMsg = 'Please populate Attachment URL for following docs: </br> <ul>';
        Boolean blankUrl = false;
        for( SR_Attachments__c objSRDoc: [SELECT
                                        Id
                                        , NAME
                                        , Attachment_URL__c
                                    FROM
                                        SR_Attachments__c
                                    WHERE
                                        NAME In : new Set<String>{'Final Offer letter'
                                                                , 'Mortgage request form'
                                                                , 'Cheque copy / Letter of guarantee/ Finance notice'} 
                                    AND
                                        Case__c =: caseId
                                ]) {
             
             System.debug('objSRDoc =' + objSRDoc);                       
            if(String.isBlank(String.valueOf(objSRDoc.Attachment_URL__c ))) {
                blankUrl = true;
                docsErrorMsg += '<li>' + objSRDoc.NAME + '</li>';
            }
        }//End For
        docsErrorMsg += '</ul>';
        if(blankUrl) {
        System.debug('== blankUrl error ==');
            ApexPages.addMessage(
                new ApexPages.Message(
                    ApexPages.Severity.ERROR
                    , docsErrorMsg
                )
            );
        }
        else if(objC != null){
            System.debug('== Else blankUrl ==');
            executeBatch();
        }
    }
    
    public pageReference returnToCase(){
        PageReference Pg = new PageReference('/'+caseId);
        return pg;
    }
    
    public void executeBatch(){
            GenerateDrawloopDocumentBatch objInstance;
            objInstance = new GenerateDrawloopDocumentBatch(caseId
                     , System.Label.Lein_Letter_DDP_Id, System.Label.Lein_Letter_Template_Id);
            Id batchId = Database.ExecuteBatch(objInstance);
            if(String.valueOf(batchId) != '000000000000000'){
                SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                 objCaseAttachment.Case__c = caseId ;
                 objCaseAttachment.Name = 'Lein Letter';
                 objCaseAttachment.Booking_Unit__c = objC.Booking_Unit__c;
                 insert objCaseAttachment;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Your request for Lein Letter was successfully submitted. Please check the documents section for the document in a while.');
                ApexPages.addMessage(myMsg);
            }else{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Your request for Lein Letter could not be completed. Please try again later.');
                ApexPages.addMessage(myMsg);
            }
        
    }
} // end of class