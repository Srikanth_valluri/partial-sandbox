@isTest
public with sharing class AdditionalParkingDetailWrapperTest 
{
    static testMethod void init_test_scenario1()
    {
        AdditionalParkingDetailWrapper.AdditionalParkingDetails objDetails = new AdditionalParkingDetailWrapper.AdditionalParkingDetails();
        
        AdditionalParkingDetailWrapper.Data objData = new AdditionalParkingDetailWrapper.Data();
        objData.ORG_ID = 123;
        objData.STATUS = 'A';
        objData.DETAIL_ID = 123;
        objData.PRICE = 500;
        objData.PROPERTY_ID = 13;
        objData.PARKING_TYPE_DESC = 'handicapped';
        objData.STATUS_CODE = 'A';
        objData.PARKING_TYPE = 'Visitor';
        objData.BUILDING_ID = 123;
        objData.PROPERTY_NAME = 'ABC';
        objData.BUILDING_NAME = 'ABC';
        objData.PARKING_BAY_NUMBER = 'PB-123';
        objData.blnIsSelected = true;

        objDetails.message = 'success';
        objDetails.status = 'success';
        objDetails.customErrorMsg = 'error';
        objDetails.data = new List<AdditionalParkingDetailWrapper.Data>{objData};

        AdditionalParkingDetailWrapper.parse('{"ORG_ID":81,"STATUS":"Sold","DETAIL_ID":88,"PRICE":8000,"PROPERTY_ID":3491,"PARKING_TYPE_DESC":"Visitor","STATUS_CODE":"S","PARKING_TYPE":"VISITOR","BUILDING_ID":64547,"PROPERTY_NAME":"DAMAC HILLS - ARTESIA","BUILDING_NAME":"ARTESIA - A","PARKING_BAY_NUMBER":"P3-0121","blnIsSelected":true}');

        parkingPaymentTermsXxdcParkingInvW.APPSXXDC_PARKING_INVX1844909X3X2 objParkingTerms = new parkingPaymentTermsXxdcParkingInvW.APPSXXDC_PARKING_INVX1844909X3X2();
        objParkingTerms.AMOUNT = 500.00;
        objParkingTerms.INSTALLMENT = 'I-001';
        objParkingTerms.PAYMENT_DATE = System.today();
        objParkingTerms.PDC_NUMBER = 'PDC-123';
        objParkingTerms.REMARKS = 'Test';
        objParkingTerms.SR_NUMBER = '12345';
        objParkingTerms.UNIT_NAME = 'test Name';
    }
}