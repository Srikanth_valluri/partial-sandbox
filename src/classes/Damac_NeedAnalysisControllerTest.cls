@isTest
public class Damac_NeedAnalysisControllerTest{
    static testMethod void needAnalysis(){
        Account acc = new Account ();
        acc.Name = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '969696';
        acc.RecordTypeId = DamacUtility.getRecordTypeId('Account', 'Corporate Agency');
        insert acc;
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName= 'Test';
        con.Email = 'test@test.com';
        con.AccountId = acc.id;
        con.Agent_Representative__c = true;
        insert con;
        Inquiry__c inq = new Inquiry__c ();
        inq.Activity_Counter__c = 101;
        inq.Inquiry_Status__c = 'Active';
        inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        inq.Telesales_Executive__c = UserInfo.getUserId();
        inq.Promoter_Name__c = 'Test';
        inq.Last_Name__c = 'Test';
        insert inq;
        Test.startTest();
        PageReference pageRef = Page.Damac_needAnalysis;
        pageRef.getParameters().put('inqId', String.valueOf(inq.Id));
        
        Test.setCurrentPage(pageRef);
            Damac_NeedAnalysisController needAnalysis = new Damac_NeedAnalysisController();
            Damac_NeedAnalysisController.searchInquiryNameForNeedAnalysis('Test');
            needAnalysis.inqNumber = inq.id; 
            needAnalysis.searchInquiry();
            needAnalysis.getBHKOptions();
            Damac_NeedAnalysisController.getAgencyDetails('test');
            Damac_NeedAnalysisController.getCorporateAgents(acc.id);
            needAnalysis.addInquiryFamily();
            
            needAnalysis.UpdateEndTime();
            
        Test.stopTest();
    }
}