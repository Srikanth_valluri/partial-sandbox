/**************************************************************************************************
 * @Name              : DAMAC_IPMS_INVENTORY_UPDATE
 * @Test Class Name   : InventoryHelperTest
 * @Description       : Websevice Call to update Inventory Details 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst          25/08/2020       Created
**************************************************************************************************/

public class DAMAC_IPMS_INVENTORY_UPDATE{
    public static boolean firstRun = true;
     public static List<String> invProcessedList;

    /*********************************************************************************************
    * @Description : Method to find Inventories for Updating Status to IPMS
    * @Params      : list<Inventory__c> 
    (                map<Id, Inventory__c>
    * @Return      : void
    *********************************************************************************************/ 
    public static void updateInvStatusToIPMS(list<Inventory__c> invList, map<Id,Inventory__c> mapOldRecords){ 
        
        if(invProcessedList == null){
            invProcessedList = new List<String>();
        }
        List<Inventory__c> statusUpdInvList = new List<Inventory__c>();
        Map<Id, String > invOldStatusMap = new Map<Id, String >();
        for(Inventory__c inv: invList){
            system.debug('inv.status__c: ' +  inv.status__c);
            system.debug('mapOldRecords.get(inv.Id).status__c ' +  mapOldRecords.get(inv.Id).status__c);
            if(inv.status__c != mapOldRecords.get(inv.Id).status__c && !invProcessedList.contains(inv.Id)){
                statusUpdInvList.add(inv);
                invOldStatusMap.put(inv.Id, mapOldRecords.get(inv.Id).status__c);
                invProcessedList.add(inv.Id);
            }
        }  
        system.debug('statusUpdInvList: ' +statusUpdInvList);
        system.debug('invOldStatusMap: ' +invOldStatusMap);
        if(statusUpdInvList.size() > 0){
            system.debug('Limits.getQueueableJobs(): ' +Limits.getQueueableJobs());
            if(Limits.getQueueableJobs() == 1 || Test.isRunningTest()){
                system.debug('Executing Scheduler ' );
                Integer hourInt = Datetime.now().hour();
                Integer minInt = Datetime.now().minute() ;
                Integer secInt = Datetime.now().second() + 5;
                if(secInt >= 60){
                    secInt = secInt - 60;
                    minInt++;
                    if(minInt >= 60){
                        minInt = minInt - 60;
                        hourInt++;
                    }                    
                }
                String hour = String.valueOf(hourInt);
                String min = String.valueOf(minInt);
                String ss = String.valueOf(secInt);
                //parse to cron expression
                String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
                System.schedule('ScheduledJob ' + String.valueOf(Math.random()), nextFireTime, 
                                new InventoryUpdateSchedulable(statusUpdInvList, invOldStatusMap));
            }else{
                 system.debug('Executing Queueable ' );
                System.enqueueJob(new InventoryUpdateIPMSQueueable(statusUpdInvList, invOldStatusMap));
            }
            
        } 
        
    }

    
}