/**
* Email services are automated processes that use Apex classes
* to process the contents, headers, and attachments of inbound
* email.
*/
global class ES_StepApproval implements Messaging.InboundEmailHandler {
    
    public Integer iListSize {get; set;}
    public List<TransitionWrapper> lstTrnsWrap {get; set;}
    public Map<Id, NSIBPM__Step_Transition__c> mapStepTransition;
    public String SRID {get; set;}
    public NSIBPM__Step__c step {get; set;}
    public NSIBPM__Service_Request__c objSR {get; set;}
    public string selTransition{get;set;}
    public string RejReason{get;set;}
    public string StepNotes{get;set;}
    
    /*********************************************************************************************
    * @Description : Prepares all the valid transitions from a given step                        *
    * @Params      : Email, Envelope                                                             *
    * @Return      : List<TransitionWrapper>                                                     *
    *********************************************************************************************/
    
    public List<TransitionWrapper> Prepare_Transitions(){
        set<id> setValidSteps = new set<id>();
        mapStepTransition = new map<id,NSIBPM__Step_Transition__c>();
        for(NSIBPM__Step_Transition__c trans : [SELECT NSIBPM__From__c, NSIBPM__To__c, NSIBPM__Transition__c, NSIBPM__Transition__r.NSIBPM__To__c, 
                                                NSIBPM__SR_Step__c, NSIBPM__SR_Status_External__c, NSIBPM__SR_Status_Internal__c 
                                                FROM NSIBPM__Step_Transition__c 
                                                WHERE NSIBPM__Transition__c != null AND 
                                                NSIBPM__From__c =: step.NSIBPM__Status__r.Name AND 
                                                NSIBPM__SR_Step__c=:step.NSIBPM__SR_Step__c AND 
                                                IsDeleted = false])
        {
            setValidSteps.add(trans.NSIBPM__Transition__r.NSIBPM__To__c);
            mapStepTransition.put(trans.NSIBPM__Transition__r.NSIBPM__To__c,trans);
            
            if(setValidSteps!=null && setValidSteps.size()>0){
                TransitionWrapper objWrap;
                for(NSIBPM__Status__c objstat : [SELECT Id, Name, NSIBPM__Type__c, NSIBPM__Rejection__c, NSIBPM__SR_Closed_Status__c, NSIBPM__Code__c 
                                                 FROM NSIBPM__Status__c 
                                                 WHERE Id != null AND 
                                                 Id IN: setValidSteps AND 
                                                 IsDeleted = false])
                {
                    system.debug(objstat);
                    objWrap = new TransitionWrapper();
                    objWrap.objStatus = objstat;
                    objWrap.objSRStepTrans = new NSIBPM__Step_Transition__c();
                    if(mapStepTransition.get(objstat.id)!=null){
                        objWrap.objSRStepTrans = mapStepTransition.get(objstat.id);
                    }
                    lstTrnsWrap.add(objWrap);
                }
                iListSize = lstTrnsWrap.size();
            }

        }
        return lstTrnsWrap;
    }
    
    /*********************************************************************************************
    * @Description : Handles the inbound Email and applies the reply value onto the Step         *
    * @Params      : Email, Envelope                                                             *
    * @Return      :  Messaging.InboundEmailResult                                               *
    *********************************************************************************************/
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        
        String DPPayment = '';
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        string strEmailBody = email.plainTextBody;
        string emailFromAddress = email.fromAddress;
        string strEmailSubject = email.subject;
        system.debug('strEmailSubject===>'+strEmailSubject);
        system.debug('strEmailBody===>'+strEmailBody);
        system.debug(email.fromAddress);
        
        string StepName;
        User replyUser = new User();
        
        
        for(User u : [SELECT Id, FirstName, LastName, Email, LastModifiedDate FROM User WHERE Email =: emailFromAddress ORDER BY LastModifiedDate DESC LIMIT 1]){
            replyUser = u;
        }
        if(strEmailSubject!=null && strEmailSubject!=''){
            list<string> lstSubject = new list<string>();
            if(strEmailSubject.indexOf('[')>-1){
                string strTemp = strEmailSubject.split('\\[')[1];
                string strFinalString = strTemp.replace(']','');
                strFinalString = strFinalString.replaceAll( '\\s+','');
                StepName = strFinalString;
            }
        }
        
        system.debug(StepName);
        step = new NSIBPM__Step__c();
        for(NSIBPM__Step__c stp:[SELECT Id, Name, NSIBPM__Summary__c, RecordTypeId, NSIBPM__Step_Status__c,  
	                                     RecordType.DeveloperName, OwnerId, Owner.Name, RecordType.Name, NSIBPM__Parent_Step__c, 
	                                     NSIBPM__SR__c, NSIBPM__SR__r.NSIBPM__SR_Template__c, NSIBPM__Status__c, NSIBPM__SR_Step__c, 
	                                     NSIBPM__Rejection_Reason__c, NSIBPM__Status__r.Name ,NSIBPM__SR_Step__r.NSIBPM__Step_Template_Code__c
                                     FROM NSIBPM__Step__c 
                                     WHERE Id != null AND 
                                     	   Name =: StepName AND 
                                     	   NSIBPM__SR__c != null AND 
                                     	   NSIBPM__SR__r.NSIBPM__SR_Template__c != null AND 
                                     	   IsDeleted = false])
        {
            step = stp;
        }
        system.debug(step);
        if(step != null){
        	objSR = SRUtility.getSRDetails(step.NSIBPM__SR__c);
        	SRID = objSR.Id;
        	lstTrnsWrap = new List<TransitionWrapper>();
        	lstTrnsWrap = Prepare_Transitions();
            system.debug(objSR);
        }
        system.debug(lstTrnsWrap);
        string FinalBody = strEmailBody;
        List<Email_Service_Body_Split__c> emailCustomSettingList = Email_Service_Body_Split__c.getAll().values();
        for(Email_Service_Body_Split__c emailText : emailCustomSettingList){
            system.debug(emailText);
            if(FinalBody!=null && FinalBody!='' && emailText.Active__c && FinalBody.indexOf(emailText.Split_By_Value__c)>-1){
                
                FinalBody = FinalBody.split(emailText.Split_By_Value__c)[0];
            }
        
        }

        string EmailReply = '';
        system.debug(FinalBody);
        String comments;
        if(String.isNotBlank(FinalBody)){
            if(FinalBody.indexOf('[') > -1){
                comments = FinalBody.split('\\[')[1];
                comments = comments.substringBefore(']');
                StepNotes = comments;
            }
            String lowerCaseStatus = '';
            for(TransitionWrapper twObj : lstTrnsWrap){
                FinalBody = FinalBody.toLowerCase();
                system.debug(twObj);
                lowerCaseStatus = (twObj.objStatus.NSIBPM__Code__c).toLowerCase();
                system.debug(lowerCaseStatus);
                system.debug((twObj.objStatus.NSIBPM__Code__c).containsIgnoreCase('approve'));
                if(
                    (((FinalBody.indexOf('yes') > -1) || (FinalBody.indexOf('approve') > -1) || (FinalBody.indexOf('approved') > -1)) && (twObj.objStatus.NSIBPM__Code__c).containsIgnoreCase('approve'))
                	|| (((FinalBody.indexOf('no') > -1) || (FinalBody.indexOf('reject') > -1) || (FinalBody.indexOf('rejected') > -1)) && (twObj.objStatus.NSIBPM__Code__c).containsIgnoreCase('reject'))
                    || ((FinalBody.indexOf('info') > -1) && (twObj.objStatus.NSIBPM__Code__c).containsIgnoreCase('info'))
                ){
                    EmailReply = twObj.objStatus.NSIBPM__Code__c;
                    selTransition = twObj.objStatus.Id;
                    if(twObj.objStatus.NSIBPM__Rejection__c && (twObj.objStatus.NSIBPM__Code__c).containsIgnoreCase('reject')){
                        RejReason = comments;
                        StepNotes = '';
                    }
                    break;
                }
            }
        }
        system.debug(EmailReply);
        system.debug(selTransition);
        system.debug(StepNotes);
        system.debug(RejReason);
        
        if(StepName!=null && StepName!='' && EmailReply!=null && EmailReply!='' && step != null){
            /*NSIBPM__Step__c stp = step;
            for(NSIBPM__Status__c stat : [Select Id,NSIBPM__Code__c from NSIBPM__Status__c where NSIBPM__Code__c= :EmailReply]){
                system.debug(stat);
                stp.NSIBPM__Status__c = stat.id;
            }*/
            if(EmailReply.containsIgnoreCase('reject') && (RejReason == null || RejReason == '')){
                
            }else{
                SaveChanges(replyUser);
            }
        }

        return result;
    }

    public void SaveChanges(User replyUser){

        if(selTransition != null && mapStepTransition.get(selTransition) != null){
            /* Savepoint can only be set after making the callout */
            Savepoint Stat_svpoint = Database.setSavepoint();
            try{
                if(mapStepTransition.get(selTransition).NSIBPM__SR_Status_Internal__c != null && mapStepTransition.get(selTransition).NSIBPM__SR_Status_External__c != null){
                    NSIBPM__Service_Request__c objSRloc = new NSIBPM__Service_Request__c(Id = SRID);
                    NSIBPM__Service_Request__c DealSr = new NSIBPM__Service_Request__c(Id = SRID);
                    DealSr = [SELECT Id, Recordtype.Name FROM NSIBPM__Service_Request__c WHERE Id =: SRID];
                    if(DealSr.Recordtype.Name == 'Deal'){// Added on May 15 2017 - For jacob
                        // v1.1 : Added the below method to make the sr docs optional for rejection step.
                        // v1.2 : Added below step template check
                        // v1.3 : Added the code to uncheck the required docs not uploaded flag on service request inside the below check
                        if(step != null && step.NSIBPM__SR_Step__c != null && step.NSIBPM__SR_Step__r.NSIBPM__Step_Template_Code__c != null && 
                           (step.NSIBPM__SR_Step__r.NSIBPM__Step_Template_Code__c.equalsIgnoreCase('DEAL_AUTO_REJECTION') || 
                            step.NSIBPM__SR_Step__r.NSIBPM__Step_Template_Code__c.equalsIgnoreCase('DEAL_REJECTION'))){
							system.debug('#### invoking the make sr docs optional code.');
							DealSr.NSIBPM__Required_Docs_not_Uploaded__c = false;
							update DealSr;
							/* Calling method to make SR Docs optional for rejection step. */
							makeSrDocsOptional(SRID); 	
                        }
                    }
                    if(mapStepTransition.get(selTransition).NSIBPM__SR_Status_Internal__c != null){
                        objSRloc.NSIBPM__Internal_SR_Status__c = mapStepTransition.get(selTransition).NSIBPM__SR_Status_Internal__c;
                    }
                    if(mapStepTransition.get(selTransition).NSIBPM__SR_Status_External__c != null){
                        objSRloc.NSIBPM__External_SR_Status__c = mapStepTransition.get(selTransition).NSIBPM__SR_Status_External__c;
                    }
                    update objSRloc;
                }
                step.NSIBPM__Status__c = mapStepTransition.get(selTransition).NSIBPM__Transition__r.NSIBPM__To__c;
                step.NSIBPM__Step_Notes__c = StepNotes;
                step.NSIBPM__Rejection_Reason__c = RejReason;
                step.Last_Modified_By_Email__c = replyUser.id;
                
                update step;
                
            }catch(DMLException e){
                string DMLError = e.getdmlMessage(0)+'';
                if(DMLError==null){
                    DMLError = e.getMessage() +' ';
                }
                selTransition = null;
                Database.rollback(Stat_svpoint);
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLError));
            }
        }else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please select the status to proceed.'));
        }
    }
    
    /*********************************************************************************************
    * @Description : #v1.2 : Making all SR Docs Optional on Deal rejection.                      *
    * @Params      : Id                                                                          *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void makeSrDocsOptional(Id srid){
        List<NSIBPM__SR_Doc__c> srdocs = new List<NSIBPM__SR_Doc__c>();
        srdocs = [SELECT Id, NSIBPM__Is_Not_Required__c 
        		  FROM NSIBPM__SR_Doc__c 
        		  WHERE NSIBPM__Service_Request__c =: srid AND 
        		  		NSIBPM__Is_Not_Required__c = false 
        		  LIMIT:LIMITS.getLimitQueryRows()];
        if(srdocs.size() > 0){
            for(NSIBPM__SR_Doc__c doc : srdocs){
            	doc.NSIBPM__Is_Not_Required__c = true;
            }   
            update srdocs;
        }	
    }
    
    public class TransitionWrapper{
        public NSIBPM__Status__c objStatus{get;set;}
        public NSIBPM__Step_Transition__c objSRStepTrans{get;set;}
    }
}