/**
 * Name          : CreateZeroSalesUserHandler
 * Description   : Create Zero Sales User & User Action Plan Record if the user is created after 1st
 *                 of the month
 * Created Date  : 18/06/2018
 * Created By    : ESPL
 * --------------------------------------------------------------------------------------------------
 * VER  AUTHOR              DATE            COMMENTS
 * 1.0  Twinkle P           18/06/2018      Initial Draft
 * 1.1  Shashank Maind      27/09/2018      Removed future annotation from createZeroSalesRecordForUsers method
 *                                          and added future annotation to createZeroSalesAndUserPlanAction method
 */
public with sharing class CreateZeroSalesUserHandler {
    public static final String PROPERTY_PROFILE_NAME = 'Property Consultant';
    public static final String NOTES                 = ' <html> <body> <div style="color: red;"> <b> '
                                                       + ' Split Deals and UK Sales are not included '
                                                       + ' </b> </div> </body> </html> ';
    public static final String PROPERTY_CONSULTANT   = 'Property Consultant';
    public static final String DIRECTOR_OF_SALES     = 'Director of Sales';
    public static final String HEAD_OF_SALES         = 'Head of Sales';
    public static final String APEX_CLASS_NAME       = 'CreateActionPlanBatch';

    /**
     * Method to create the Zero Sales User if the user's profile name is 'Property Consultant'
     * and not created on First of the month
     *
     * @param: userIds> : User Ids
     *
     * @return: NA
     */
     public static void createZeroSalesRecordForUsers(Set<Id> userIds) {
             Id apexClassId = [SELECT Id, Name
                           FROM ApexClass
                           WHERE Name =: APEX_CLASS_NAME
                           LIMIT 1].Id;
         List<AsyncApexJob> asynJobs = [SELECT Id, Status, ApexClassID
                                        FROM AsyncApexJob
                                        WHERE ApexClassID =: apexClassId
                                        AND Status = 'Processing' ];
         if(asynJobs.isEmpty()){
             Set<Id> users = new Set<Id> ();
             List<User> insertedUsers = [SELECT Id, Name, ManagerId, Manager.Profile.Name ,
                                                isActive,Profile.Name, Manager.Email,
                                                Manager.Manager.Email
                                         FROM User
                                           WHERE Id =: userIds
                                           AND Profile.Name =: PROPERTY_PROFILE_NAME
                                           AND isActive = true] ;
                 for(User userObj : insertedUsers) {
                     if( userObj.ManagerId != null &&
                        ( userObj.Manager.Profile.Name.equalsIgnoreCase (DIRECTOR_OF_SALES)
                            ||
                          userObj.Manager.Profile.Name.equalsIgnoreCase (HEAD_OF_SALES)
                        )
                     ) {
                         users.add(userObj.Id);
                     }
            }
              if(!users.isEmpty()) {
                 createZeroSalesAndUserPlanAction(users);
             }
         }
     }

    /**
     * Method to create Zero Sales Record for the user for the Current Month
     *
     * @param: Set of Users
     *
     * @return: NA
     */
    @future
    private static void createZeroSalesAndUserPlanAction(Set<Id> userIds ) {
        List<User> users = [
            SELECT  Id, Name, ManagerId, Manager.Profile.Name, IsActive, Profile.Name, Manager.Email
                    , Manager.Manager.Email
            FROM    User
            WHERE   Id = :userIds
                    AND Profile.Name =: PROPERTY_PROFILE_NAME
                    AND IsActive = TRUE
        ];
        List<Zero_Sales_User__c>  zeroSalesUsers  = new List<Zero_Sales_User__c>();
        List<User_Action_Plan__c> userActionPlans = new List<User_Action_Plan__c> ();
        Set<Id> userIdForWhichZeroSalesExist      = new Set<Id> ();
        Map<Id,String> employeeIdAndUserName      = new Map<Id,String> ();
        //Get Last Day of previous month
        Date todaysDate = System.today();
        List<Zero_Sales_User__c>  existingZeroSalesUsers  = [SELECT Id, CreatedDate, Month_Number__c ,Employee__c
                                                              FROM Zero_Sales_User__c
                                                              WHERE Month_Number__c =:  todaysDate.month()];
        for (Zero_Sales_User__c existZeroSalesObj  : existingZeroSalesUsers) {
            Date createdDate = Date.valueOf(existZeroSalesObj.CreatedDate);
            if(createdDate.year() == todaysDate.year() ) {
                userIdForWhichZeroSalesExist.add(existZeroSalesObj.Employee__c);
            }
        }

        for(User userObj : users) {
            if(!userIdForWhichZeroSalesExist.contains(userObj.Id)) {
                employeeIdAndUserName.put(userObj.Id,userObj.Name);
                Zero_Sales_User__c zsUser = new Zero_Sales_User__c();
                    zsUser.Name = userObj.Name
                                + '\'s Action Plan for '
                                + todaysDate.month()
                                + '/'
                                + todaysDate.year();
                    zsUser.Employee__c = userObj.Id;
                    zsUser.HOS_Descision__c = 'Pending';
                    zsUser.Note__c = NOTES;
                    zsUser.OwnerId = userObj.ManagerId;
              zsUser.DOS_Email_Id__c =  userObj.Manager.Email;
                system.debug('>>>userObj.Manager.Email>>'+userObj.Manager.Email);
              zsUser.Send_Email_For_DOS__c = true;
              zsUser.OwnerId = userObj.ManagerId;
              if(userObj.Manager.ManagerId != null) {
                  zsUser.HOS_Email_Id__c =  userObj.Manager.Manager.Email;
                  zsUser.Send_Email__c = true;
              }

                zeroSalesUsers.add(zsUser);
            }
        }
        if(!zeroSalesUsers.isEmpty()) {
             insert zeroSalesUsers;
        }

        // Create Action Plan for the Zero Sales Users
        for (Zero_Sales_User__c zsUserRec : zeroSalesUsers) {
            User_Action_Plan__c actionPlan = new User_Action_Plan__c();
            actionPlan.Name = employeeIdAndUserName.get(zsUserRec.Employee__c)
                            + '\'s Action Plan ';
            actionPlan.Zero_Sales_User__c = zsUserRec.Id;
            actionPlan.PC__c = zsUserRec.Employee__c;
            actionPlan.Action_Plan__c = '1st to 25th';
            userActionPlans.add(actionPlan);
        }

        // DML
        if(!userActionPlans.isEmpty()) {
            insert userActionPlans;
        }

     }

     /**
      * Method to update the Zero Sales User records email field once the User
      * Manager or Manager's Manager is updated
      *
      * @param: newRecordsMap : Map of User with new values
      * @param: oldRecordsMap : Map of User with Old values
      *
      * @return: NA
      */
      public static void updateZeroSalesUserRecords (Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap) {
          Map< Id, User> userIdsAndUserObj = new Map<Id,User>();
          set<Id> userIds = new set<Id>();
          for(Id userId : newRecordsMap.keySet()) {
             User userObjNew = (User) newRecordsMap.get(userId);
             User userObjOld = (User) oldRecordsMap.get(userId);
             if( userObjNew.ManagerId != userObjOld.ManagerId) {
                userIdsAndUserObj.put(userObjNew.Id, userObjNew);
                userIds.add(userObjNew.Id);
             }
          }
          System.debug('ssssssssssss'+userIdsAndUserObj);
          if(userIds.size() > 0) {

            updateTheEmailForZeroSales(userIds);
          }
      }

      /**
       * Method to Update the Zero Sales User Records
       *
       * @param: newRecordsMap : Map of User with new values
       *
       * @return: NA
       */
       @Future
       private static void updateTheEmailForZeroSales (set<Id> userIdsAndUserObj) {
          Map<Id, User> userIdsAndUserObjWithEmail = new Map<Id, User> ([SELECT Id, Name, ManagerId, Manager.Profile.Name ,
                                                                                isActive,Profile.Name, Manager.Email,
                                                                                Manager.Manager.Email
                                                                         FROM User
                                                                         WHERE Id in: userIdsAndUserObj]);
          List<Zero_Sales_User__c>  existingZeroSalesUsers  = [SELECT Id, HOS_Email_Id__c, DOS_Email_Id__c ,Employee__c,Send_Email_For_DOS__c,Send_Email__c
                                                               FROM Zero_Sales_User__c
                                                               WHERE Employee__c =:  userIdsAndUserObj];
          System.debug('existingZeroSalesUsersexistingZeroSalesUsers'+existingZeroSalesUsers);
          List<Zero_Sales_User__c> updatedZeroList = new List<Zero_Sales_User__c>();
          for(Zero_Sales_User__c zeroSalesObj : existingZeroSalesUsers) {
              String newDOSEmailId = userIdsAndUserObjWithEmail.get(zeroSalesObj.Employee__c).Manager.Email;
              // Added if condition to check DOS Email ID
              if (zeroSalesObj.DOS_Email_Id__c != NULL) {
                  if (zeroSalesObj.DOS_Email_Id__c != NULL) {
                      if(!(zeroSalesObj.DOS_Email_Id__c.equalsIgnoreCase(newDOSEmailId))) {
                        zeroSalesObj.DOS_Email_Id__c =  userIdsAndUserObjWithEmail.get(zeroSalesObj.Employee__c).Manager.Email;
                        zeroSalesObj.Send_Email_For_DOS__c = true;
                        zeroSalesObj.OwnerId = userIdsAndUserObjWithEmail.get(zeroSalesObj.Employee__c).ManagerId;
                      }
                  }
              }

              if(userIdsAndUserObjWithEmail.get(zeroSalesObj.Employee__c).Manager.ManagerId != null) {
                  String newHOSEmailId = userIdsAndUserObjWithEmail.get(zeroSalesObj.Employee__c).Manager.Manager.Email;
                  if (zeroSalesObj.HOS_Email_Id__c != NULL) {
                      if(!(zeroSalesObj.HOS_Email_Id__c.equalsIgnoreCase(newHOSEmailId))) {
                        zeroSalesObj.HOS_Email_Id__c =  userIdsAndUserObjWithEmail.get(zeroSalesObj.Employee__c).Manager.Manager.Email;
                        zeroSalesObj.Send_Email__c = true;
                      }
                  }
              } else {
                  zeroSalesObj.HOS_Email_Id__c = '';
              }

              updatedZeroList.add(zeroSalesObj);
          }
          List<Zero_Sales_User__c>  existZeroSalesUsersManager  = [SELECT Id, HOS_Email_Id__c, OwnerId ,Send_Email__c
                                                                   FROM Zero_Sales_User__c
                                                                   WHERE OwnerId in:  userIdsAndUserObj];
          for(Zero_Sales_User__c zeroSalesObj : existZeroSalesUsersManager) {
              if(userIdsAndUserObjWithEmail.get(zeroSalesObj.OwnerId).ManagerId != null) {
                  String newHOSEmailId = userIdsAndUserObjWithEmail.get(zeroSalesObj.OwnerId).Manager.Email;
                  if (zeroSalesObj.HOS_Email_Id__c != NULL) {
                      if(!(zeroSalesObj.HOS_Email_Id__c.equalsIgnoreCase(newHOSEmailId))) {
                        zeroSalesObj.HOS_Email_Id__c =  userIdsAndUserObjWithEmail.get(zeroSalesObj.OwnerId).Manager.Email;
                        zeroSalesObj.Send_Email__c = true;
                      }
                  }
              } else {
                  zeroSalesObj.HOS_Email_Id__c = '';
              }
              updatedZeroList.add(zeroSalesObj);
          }
          if(!updatedZeroList.isEmpty()) {
             update updatedZeroList;
          }

       }


}