/***************************************************************************************************************
Description: Test class for RoasteringApiBatch.
________________________________________________________________________________________________________________
Version Date(DD-MM-YYYY)    Author              Description
----------------------------------------------------------------------------------------------------------------
1.0     12-07-2020          Komal Shitole       Initial Draft
****************************************************************************************************************/
@isTest
public with sharing class RoasteringApiBatchTest {
    @testSetup static void setup() {
        Credentials_details__c creds = new Credentials_details__c();
        creds.Endpoint__c = 'https://inout.damacgroup.com/cosec/api.svc/v2/attendance-daily?action=get;format=json;field-name=userid,WorkingShift,firsthalf,secondhalf,UserName,ProcessDate,Punch1,Punch2,WorkingShift,LateIn,EarlyOut,Overtime,WorkTime;range=user;id=';
        creds.Name ='Roastering Api';
        creds.Password__c = 'Sale$Inter123';
        creds.User_Name__c = 'salesforce';
        insert creds;
        Credentials_details__c creds1 = new Credentials_details__c();
        creds1.Endpoint__c = 'https://inout.damacgroup.com/cosec/api.svc/v2/shift-details?action=get;format=json;shift-id=';
        creds1.Name ='Roastering Shift Api';
        creds1.Password__c = 'Sale$Inter123';
        creds1.User_Name__c = 'salesforce';
        insert creds1;
        Credentials_details__c creds2 = new Credentials_details__c();
        creds2.Endpoint__c = 'https://inout.damacgroup.com/cosec/api.svc/v2/holiday?action=get;format=json;year=2020;list-type=2;userId=';
        creds2.Name ='Roastering Holiday Api';
        creds2.Password__c = 'Sale$Inter123';
        creds2.User_Name__c = 'salesforce';
        insert creds2;
        User userObj = [SELECT  Id,
                                                     Name,
                                                     isActive,
                                                     HR_Employee_ID__c,
                                                     Manager.LastName
                                             FROM User 
                                             WHERE (Department = 'CRM' OR Department = 'Collection' OR Department = 'Handover')
                                             AND isActive = true
                                             AND HR_Employee_ID__c != Null
                                             AND IsPortalEnabled = false
                                             LIMIT 1
                                         ];
        Attendance__c attendanceObj = new Attendance__c();
        attendanceObj.User_Id__c  = userObj.HR_Employee_ID__c;
        attendanceObj.Date__c  = Date.parse('01/07/2020');
        insert attendanceObj;
    }
    @IsTest
    static void positiveTest1(){
    
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new RoasteringServiceMockHttp());
            Database.executeBatch(new RoasteringApiBatch(),1);
        Test.stopTest();
        
    }
     @IsTest
    static void negativeTest1(){
    
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new RoasteringServiceMockHttpNegative());
            Database.executeBatch(new RoasteringApiBatch(),1);
        Test.stopTest();
        
    }
    
    
   
}