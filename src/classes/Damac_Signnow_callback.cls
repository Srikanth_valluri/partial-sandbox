@RestResource(urlMapping='/signnowcallback/*')
global without sharing class Damac_Signnow_callback {
    
    @HttpPOST
    global static void callback () {
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        String requestBody = request.requestbody.tostring();
        System.debug(requestBody);
        
        Damac_Signnow_callback obj = parse (requestBody);
        String eventType = obj.meta.event;
        String documentId = obj.content.document_id;
        Damac_SignNowAPI.getDocumentDetails (documentId);
    }
    
    public cls_meta meta;
    public cls_content content;
    public class cls_meta {
        public Integer timestamp;   //1588129888
        public String event;    //document.open
    }
    public class cls_content {
        public String document_id;  //0af8fedd19ac38e4403d1fe6a2fe7a18a36e0703
        public String document_name;    //Key Release Authorization Form
        public String user_id;  //f8a6e77562454658be504edc226849fd0f03a0fd
        public String viewer_user_unique_id;    //27b0d2d47d864f43baf5d5d19b2e8108a7c3e408
    }
    public static Damac_Signnow_callback parse(String json){
        return (Damac_Signnow_callback) System.JSON.deserialize(json, Damac_Signnow_callback.class);
    }
}