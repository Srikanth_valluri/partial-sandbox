public without sharing class AP_AgentPortalHeader {
    /**************************************************************************************************
            Variables used in the class
    **************************************************************************************************/
    public List<Notification__c> notificationList{set;get;}
    public List<Notification__c> notificationList2{set;get;}
    public Integer unreadNotificationCount{set;get;}
    public Integer notificationListSize{set;get;}
    public List<Announcement__c> announcementList{get;set;}

    public Announcement__c latestAnnouncement{set;get;}
    public boolean noAnnounements{set;get;}
    public Integer announcementNo{set;get;}
    public List <String> recentTitles { get; set; }
    public Assigned_Agent__c latestCampaign{set;get;}

    public boolean isAuthorisedToUpload{set;get;} // isLoggedin user authorised to upload attahcments in announcements
    public AssignedAgentWrapper assignedAgents{set;get;}
    public String campaignstartDate{set;get;}
    public String campaignStartOrEndString{set;get;}
    public String UserName{get;set;}
    public String UserProfile{get;set;}
    public String UserTier{get;set;}
    public String strLanguageCode{get;set;}
    public String strSelectedLanguage{get;set;}
    public list<languageCodeWrapper> languageWrapperlist {get; set;}

    
    

    public boolean readAnnouncements { get; set; }
    // Change Password variables
    public static String oldPassword                        {get;set;}
    public static String newPassword                        {get;set;}
    public static String confirmNewPassword                 {get;set;}
    public static String message                            {get;set;}
    public String show                                      {get;set;}
    public static final String success                      = 'Update Success!!';
    public static final String error                        = 'Update Failed!!';
    public User objUser {get;set;}
    public List<String> listLanguages {set; get;}


    /**************************************************************************************************
    Method:         AgentPortalSidebar
    Description:    Constructor executing model of the class 
    **************************************************************************************************/
    public AP_AgentPortalHeader () {
        readAnnouncements = false;
        recentTitles = new List <String> ();
        getLatestAnnouncments ();
        objUser = [ SELECT FullPhotoUrl,
                                ContactId,LanguageLocaleKey,
                                contact.Announcement_Status__c,
                                Account.OwnerId,
                                Account.Secondary_Owner__c,
                                Account.Owner.Name,
                                Account.Secondary_Owner__r.Name
                           FROM User
                          WHERE Id =: userInfo.getUserId()
        ];
        listLanguages = new List<String>();
        readAnnouncements = objUser.contact.Announcement_Status__c == 'Read' ? true : false;
        System.debug('objUser>>>>> --' + objUser.ContactId);
        System.debug('objUser>>>>> --' + objUser.LanguageLocaleKey);
        if(objUser.LanguageLocaleKey != null){
            strSelectedLanguage = objUser.LanguageLocaleKey;
        }
        System.debug('strSelectedLanguage69>>>>> --' + strSelectedLanguage);
        //get all notification based on role.
        //if user is admin then show account level notifications as well
        message = '';
        Contact contactInfo = AgentPortalUtilityQueryManager.getContactInformation();
        notificationListSize = 0;
        notificationList = new List<Notification__c>();
         notificationList2 = new List<Notification__c>();
        if(null != contactInfo && (contactInfo.Portal_Administrator__c ||
        contactInfo.Authorised_Signatory__c || contactInfo.Owner__c)){
          List<User> uRec = new List<User>();
          uRec = [SELECT id ,
                        accountId
                        FROM User 
                        WHERE Id=: userInfo.getuserId()
                        Limit 1];
          List<Account> acc = new List<Account>();
            acc= [SELECT id,           
                        name,
                        Agency_Tier__c
                   FROM Account
                   WHERE id=:uRec[0].AccountId
                   Limit 1];
            System.debug('....uRec[0].AccountId...'+uRec[0].AccountId+'...acc....'+acc);       
            UserTier = acc[0].Agency_Tier__c;
            System.debug('...UserTierUserTier..'+UserTier);
            String condition1 = '(Contact__r.id =\''+contactInfo.Id+'\' OR Account__r.id =\''+contactInfo.accountID+
                                '\') AND Active__c  =true '+
                                ' ORDER BY Read__c,CreatedDate DESC';
            system.debug(condition1);
            notificationList = AgentPortalUtilityQueryManager.getNotifications(condition1);
        }
        else if(null != contactInfo){
          if(contactInfo.Id != null){
            String condition = 'Contact__r.id =\''+contactInfo.Id+'\' AND Active__c  =true'+ 
                                ' ORDER BY Read__c,CreatedDate DESC';
            system.debug(condition);
            notificationList = AgentPortalUtilityQueryManager.getNotifications(condition);
            if(!notificationList.isEmpty()){
            notificationList2 =notificationList;
            }
          } 
        }
        notificationListSize = notificationList.size();
        system.debug('***Notification List'+notificationList);
        unreadNotificationCount = AgentPortalUtilityQueryManager.unreadNotificationCount;
        
        if(null != contactInfo){
            Account account = AgentPortalUtilityQueryManager.getAccountInformation(contactInfo.accountID);
           
            //Announcement to be shown based on the tier, ongoing/future and agency type is corporate
            if(null != account){
                //latestAnnouncement = AgentPortalUtilityQueryManager.getLatestAnnouncement(account);
                //noAnnounements = AgentPortalUtilityQueryManager.noAnnounements;
                announcementList = AgentPortalUtilityQueryManager.getAllAnnouncements(account);
                announcementNo = announcementList.size();
                if(announcementList.size()>0){
                    latestAnnouncement = new Announcement__c();
                    latestAnnouncement = announcementList[0];
                }
            }
        }

        latestCampaign = new Assigned_Agent__c();
        latestCampaign = AgentPortalUtilityQueryManager.getLatestCampaign();
        system.debug('**sidebar latestCampaign'+latestCampaign);

        assignedAgents = new AssignedAgentWrapper(latestCampaign.Campaign_Name__c,latestCampaign.Start_Date__c,latestCampaign.End_Date__c);

        if(null != latestCampaign.Start_Date__c && latestCampaign.Start_Date__c>=System.now().Date()){
            DateTime dt = Datetime.newInstance( latestCampaign.Start_Date__c.year(),  latestCampaign.Start_Date__c.month(), latestCampaign.Start_Date__c.day()); 
            campaignstartDate = dt.format('MM/dd/yyyy kk:mm ');
            campaignStartOrEndString = 'Starts at :';
        }
        else if(null != latestCampaign.End_Date__c){
            DateTime dt = Datetime.newInstance( latestCampaign.End_Date__c.year(),  latestCampaign.End_Date__c.month(), latestCampaign.End_Date__c.day()); 
            campaignstartDate = dt.format('MM/dd/yyyy kk:mm ');
            campaignStartOrEndString = 'Ends at :';
        }
        UserName = userInfo.getName();
        UserProfile = userInfo.getProfileId();
        system.debug('***'+campaignstartDate);
        languageWrapperlist = new list<languageCodeWrapper>();
        for(AP_Languages__mdt objLanguage : [SELECT DeveloperName,
                                                    MasterLabel
                                            FROM AP_Languages__mdt])
        {
            languageWrapperlist.add(new languageCodeWrapper(objLanguage.MasterLabel,objLanguage.DeveloperName));
            listLanguages.add(objLanguage.MasterLabel);           
        }
    }
    public void changeUserLang(){
        system.debug('** objUser.strLanguageCode>>> '+ strSelectedLanguage);
        objUser.LanguageLocaleKey = strSelectedLanguage;
        system.debug('** objUser.LanguageLocaleKey>>> '+ objUser.LanguageLocaleKey);
        update objUser;
    }

    /**************************************************************************************************
    Method:         clearFields
    Description:    Method to clear the password and message fields
    **************************************************************************************************/
    @RemoteAction
    public static void clearFields() {
        confirmNewPassword = '';
        newPassword = '';
        oldPassword = '';
        message = '';
    }

    /**************************************************************************************************
    Method:         updatePassword
    Description:    Method to update the the password
    **************************************************************************************************/
    @RemoteAction
    public static String updatePassword(String pNewPassword, String pConfirmNewPassword, String pOldPassword) {
        message = '';

        newPassword = pNewPassword;
        confirmNewPassword = pConfirmNewPassword;
        oldPassword = pOldPassword;
        system.debug('**sidebar confirmNewPassword >>> '+confirmNewPassword);
        system.debug('**sidebar newPassword >>> '+newPassword);
        system.debug('**sidebar oldPassword >>> '+oldPassword);
        if (String.isNotBlank(confirmNewPassword) 
            && String.isNotBlank(newPassword) 
            && String.isNotBlank(oldPassword)
        ) {
            if (newPassword == confirmNewPassword) {
                if (!Test.isRunningTest()) {
                    PageReference pageRef = Site.changePassword(newPassword, confirmNewPassword, oldPassword); 
                }
                List<Apexpages.Message> msgs = ApexPages.getMessages();
                for(Apexpages.Message msg : msgs){
                    String severity = String.valueOf(msg.getSeverity());
                    system.debug('**sidebar severity >>> '+severity);
                    if (severity == 'ERROR') {
                        message = msg.getDetail();
                    }
                }
                if (message == '' ) {
                    message = success; 
                }
            } else {
                message = ' Error: Please enter same value for new password and confirm password ';
            }
        } else {
            message = ' Error: Please enter value for the below fields ';
        }
        return message;

    }

    public class AssignedAgentWrapper{
        public String campaignName{set;get;}
        public Date campaignStartDate{set;get;}
        public Date campaignEndDate{set;get;}

        public AssignedAgentWrapper(String campaignName, Date campaignStartDate, Date campaignEndDate){
            this.campaignName = campaignName;
            this.campaignStartDate = campaignStartDate;
            this.campaignEndDate = campaignEndDate;
        }
    }
    public class languageCodeWrapper {
        public String langMasterlabel{get; set;}
        public String langCode{get; set;}  
        public languageCodeWrapper(String langMasterlabel,String langCode){
            this.langMasterlabel=langMasterlabel;
            this.langCode=langCode;
        }
    }
    
    
    public void getLatestAnnouncments () {
        AP_CollateralsController obj = new AP_CollateralsController ();
        obj.fetchAnnoncements ();
        List<AP_CollateralsController.WrapAnnouncementAndFiles> records = new List<AP_CollateralsController.WrapAnnouncementAndFiles> ();
        recentTitles = new List <String> ();
        records = obj.announcemetIdWrapperList;
        for (AP_CollateralsController.WrapAnnouncementAndFiles record: records) {
            if (recentTitles.size () < 3)
                recentTitles.add (record.objAcc.Title__c);
            else
                break;
        }
    }
}