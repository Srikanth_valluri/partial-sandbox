/*
*    Description: The Apex Class is developed to control the behavior of ProjectCollaterals page
*                 which is used to diaplay the various parameters of a selected inventory
*
*    Version            Date            Author                Description
*    1.0                30 Aug 2017     Siddharth (Eternus)   Initial Draft
*/
public without sharing class ProjectCollateralsController{

    //Final Variables
    private static final string STR_ID = 'id';

    //Private Variables
    private Inventory__c objInventory = new Inventory__c();
    @testvisible private String inventoryId;
    
    //Properties
    public string newintID{get;set;}
    public string strMdUrl {get; set;}
    public string strProjectUrl {get; set;}
    public string strBuildingUrl {get; set;}
    public string strFeaturesUrl {get; set;}    
    public string strbrochureUrl {get; set;}
    public string strwalkthroughUrl {get; set;}
    public string strfactsheetUrl {get; set;}  
    public Marketing_Documents__c objMktDocs {get; set;}

    //Default Constructor
    public ProjectCollateralsController(){
        if(!Test.isRunningTest()){
        inventoryId = ApexPages.currentPage().getParameters().get(STR_ID);
        }

        objMktDocs = new Marketing_Documents__c();
    }
    
    //Get Inventory Details
    public void getCollateralDetails(){
        System.debug('...inventoryId....'+inventoryId); 
           if(Test.isRunningTest()){
            inventoryId = newintID;
            }
        if(!String.isBlank(inventoryId)){
            objInventory = [select Id, 
                                   Marketing_Name_Doc__c 
                            from Inventory__c 
                            where Id =: inventoryId][0];
                        
            if(objInventory != NULL && objInventory.Marketing_Name_Doc__c != NULL){
            
                //Retrieve Martketing Documents Information
                objMktDocs = [select id,
                                     MD_URL__c,
                                     Project_URL__c,
                                     Building_URL__c,
                                     Features_Amenities_URL__c,
                                     Brochure_URL__c,
                                     Factsheet_URL__c,
                                     Walkthrough_URL__c
                              from Marketing_Documents__c 
                              where Id =: objInventory.Marketing_Name_Doc__c];
                System.debug('...objMktDocs...'+objMktDocs);
                //Assigning URL fields to local string variables
                this.strMdUrl = objMktDocs.MD_URL__c;
                this.strProjectUrl = objMktDocs.Project_URL__c;
                this.strBuildingUrl = objMktDocs.Building_URL__c;
                this.strFeaturesUrl = objMktDocs.Features_Amenities_URL__c;
                this.strbrochureUrl = objMktDocs.Brochure_URL__c;
                this.strwalkthroughUrl = objMktDocs.Walkthrough_URL__c;
                this.strfactsheetUrl = objMktDocs.Factsheet_URL__c;
            }
        }
    }
}