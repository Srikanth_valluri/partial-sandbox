@isTest(SeeAlldata=false)
public class RemovePCFromCampaignControllerTest {

    public static testmethod void RemovePCFromCampaignControllerTest1() {

        // Insert User record
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('PC1111@test.com');
        insert pcUser;
        System.assertNotEquals(null, pcUser.Id);

        // Insert Campaign__c record
        Campaign__c camp = InitialiseTestData.createCampaign();
        insert camp;
        System.assertNotEquals(null, camp.Id);


        Assigned_PC__c assignedPC = InitialiseTestData.assignPCToCampaign(pcUser.Id, camp.Id);
        insert assignedPC;
        System.assertNotEquals(null, assignedPC.Id);

        // Check if Assigned_PC__c records are linked to Campaign__c
        List<Assigned_PC__c> assignedPcBeforeList = [SELECT Id FROM Assigned_PC__c WHERE Campaign__c = :camp.Id];
        System.assert(assignedPcBeforeList.size() > 0);

        // Test the Remove PC button functionality
        System.test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(camp);
        RemovePCFromCampaignController instance = new RemovePCFromCampaignController(sc);
        PageReference pageRef = instance.removePC();
        System.test.stopTest();

        // Verify if Assigned_PC__c records are cleared form Campaign__c 
        List<Assigned_PC__c> assignedPcAfterList = [SELECT Id FROM Assigned_PC__c WHERE Campaign__c = :camp.Id];

        //Asserts
        System.assert(assignedPcAfterList.size() == 0);
        System.assertNotEquals(assignedPcBeforeList, assignedPcAfterList);

    }
}