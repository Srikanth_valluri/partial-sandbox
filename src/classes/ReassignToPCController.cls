global class ReassignToPCController {

    public ReassignToPCController (Apexpages.standardController stdController) {
    
    }
    
    @remoteAction
    global static String reassigntoPC (ID UAID) {
        try {
            Unit_Assignment__c UA = new Unit_Assignment__c ();
            UA = [ SELECT Status__c, PC_Approval_Status__c, PC__c, OwnerID FROM Unit_Assignment__c WHERE ID =: UAID ];
            ID reassignerID = NULL;
            if (UA.PC__c != NULL)
                reassignerID = UA.PC__c;
            else
                reassignerID = UA.OwnerID;
            String val = autoApprove (UAID, reassignerID);
            if (val == 'success') {
                
                UA.ID = UAID;
                UA.Status__c = 'Vikram Approved';
                UA.PC_Approval_Status__c = 'Reassign';
                Update UA;
                
                  
            }
            
            return val;
        }
        catch (Exception e) {
            return e.getMessage ();
        }
    }
    public static String autoApprove (ID recId, ID PCId){
        try {
            List <ProcessInstanceWorkItem> workItemList = new List <ProcessInstanceWorkItem> ();
            workItemList = [SELECT p.ProcessInstance.Status, p.ProcessInstance.TargetObjectId,
                                    p.ProcessInstanceId, p.OriginalActorId, p.Id, p.ActorId
                            FROM ProcessInstanceWorkitem p 
                            WHERE p.ProcessInstance.TargetObjectId = : recId];
            if (!Test.isRunningTest()) {
                workItemList.get(0).ActorId  = pcId;
                update workItemList;
            }
            return 'success';
        }
        catch (Exception e) {
            return e.getMessage ();
        }
    }
}