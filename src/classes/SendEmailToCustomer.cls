public class SendEmailToCustomer {
    public Id callingListId;
    
    public Static String drawloopDocName = '';
    public Static String MORTAGE_COMM_NR_SALARIED_CLINETS = 'Non Resident - Salaried Clients';
    public Static String MORTAGE_COMM_SELF_EMPLOYED_CLINETS = 'Self Employed Clients';
    public Static String MORTAGE_COMM_SALARIED_CLINETS = 'Salaried Clients';
    public Static String MORTAGE_COMM_NR_SELF_EMPLOYED = 'Non Resident - Self Employed';
    
    public SendEmailToCustomer(ApexPages.StandardController stdController){
        callingListId = stdController.getId();
        
       // sendEmailSendGridCL(callingListId);
    }
    
    public void sendEmailSendGridCL() {
        Calling_List__c objCL = [SELECT Id
                                      , Account__c
                                      , Mortgage_Offer__c
                                      , Mortgage_Offer__r.Type_Of_Client__c
                                      , Account__r.Person_Business_Email__c
                                      , Account__r.Name 
                                   FROM Calling_List__c 
                                  WHERE Id =:callingListId 
                                  Limit 1];
        System.debug('objCL ------->' +objCL);
        System.debug('objCL.Mortgage_Offer__c------->' +objCL.Mortgage_Offer__c);
        System.debug('objCL.Mortgage_Offer__r.Type_Of_Client__c------->' +objCL.Mortgage_Offer__r.Type_Of_Client__c);
        System.debug('Account__r.Person_Business_Email__c------->' +objCL.Account__r.Person_Business_Email__c);
        System.debug('Account__r.Name------->' +objCL.Account__r.Name);
        if( objCL != null ) {
        
            if( objCL.Mortgage_Offer__c != null && String.isNotBlank( objCL.Mortgage_Offer__r.Type_Of_Client__c ) ) {
                if( objCL.Mortgage_Offer__r.Type_Of_Client__c.containsIgnoreCase( MORTAGE_COMM_NR_SALARIED_CLINETS ) ) {
                    drawloopDocName = MORTAGE_COMM_NR_SALARIED_CLINETS;
                } else if( objCL.Mortgage_Offer__r.Type_Of_Client__c.containsIgnoreCase( MORTAGE_COMM_SELF_EMPLOYED_CLINETS ) ) {
                    drawloopDocName = MORTAGE_COMM_SELF_EMPLOYED_CLINETS;
                } else if( objCL.Mortgage_Offer__r.Type_Of_Client__c.containsIgnoreCase( MORTAGE_COMM_SALARIED_CLINETS ) ) {
                    drawloopDocName = MORTAGE_COMM_SALARIED_CLINETS;
                } else if( objCL.Mortgage_Offer__r.Type_Of_Client__c.containsIgnoreCase( MORTAGE_COMM_NR_SELF_EMPLOYED ) ) {
                    drawloopDocName = MORTAGE_COMM_NR_SELF_EMPLOYED;
                }
            } else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'There is no mortgage offer associated with Calling list ');
                ApexPages.addMessage(myMsg);
            }
            
            if( !String.isBlank( drawloopDocName ) ) {
                drawloopDocName = drawloopDocName;
            }
            //IMP debug to verify the Drawloop Name Generated.
            System.debug( 'drawloopDocName ==='+drawloopDocName );
            
            Riyadh_Rotana_Drawloop_Doc_Mapping__c drawloopCS
                = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( drawloopDocName );
                
            //IMP debug to verify the Custom Setting Name.
            System.debug( 'Custom Setting Name based ob Drawloop name ==='+drawloopCS );
            
            if( drawloopCS != Null
            && String.isNotBlank( drawloopCS.Delivery_Option_Id__c )
            && String.isNotBlank( drawloopCS.Drawloop_Document_Package_Id__c ) ) {
                if( !Test.isRunningTest() ) {
                    DrawloopDocGen.generateDoc( drawloopCS.Drawloop_Document_Package_Id__c
                                              , drawloopCS.Delivery_Option_Id__c
                                              , objCL.Id );
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Request has been sent for generating Document - '+drawloopDocName+'. Please go back to Calling List record and check the generated document in the Notes & Attachment section of the related list.');
                    ApexPages.addMessage(myMsg);
                }
            }
        }
        
     }
     
     
    @future( callout = true )
    public static void sendMortgageOfferToCx( Id attachementId ) {

        List<Attachment> lstAttach = new List<Attachment>();
        List<EmailMessage> lstEmails = new List<EmailMessage>();
        
        
        EmailTemplate emailTemplateObj = [SELECT Id
                                          , Subject
                                          , Body
                                          , Name
                                          , HtmlValue
                                          , TemplateType
                                          , BrandTemplateId
                                          FROM EmailTemplate 
                                          WHERE Name = 'Mortgage Communication Email' LIMIT 1 ];
        
        if(emailTemplateObj != null){
            
            if( attachementId != null ) {
                // TODO Added to have attachment related to email template
                Attachment objAtt = [ SELECT Id
                                        , ContentType
                                        , Name
                                        , Body 
                                        , ParentId
                                     FROM Attachment 
                                    WHERE Id =: attachementId 
                                    LIMIT 1 ];
                 if( objAtt != null && objAtt.ParentId != null 
                    && String.ValueOf( objAtt.ParentId ).startsWith( AttachmentHandlerUploadDocument.keyPrefix( 'Calling_List__c' ))) {
                    
                    List<Calling_List__c> lstCL = [ SELECT  Id
                                                          , Account__c
                                                          , Mortgage_Offer__c
                                                          , Mortgage_Offer__r.Type_Of_Client__c
                                                          , Account__r.Person_Business_Email__c
                                                          , Account__r.Name 
                                                FROM
                                                    Calling_List__c
                                                WHERE
                                                    Id =: objAtt.ParentId
                                                AND
                                                    Account__c != null
                                                AND
                                                    Account__r.Person_Business_Email__c != null ];
                    
                    if( lstCL != null && lstCL.size() > 0 ) {
                        if( lstCL[0].Mortgage_Offer__c != null ) {
                            String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';
                            String subject, strAccountId, bccAddress ='', strCCAddress;
                            
                            strAccountId = lstCL[0].Account__c;
                            
                            toAddress = lstCL[0].Account__r.Person_Business_Email__c;
                            System.debug('toAddress = ' + toAddress);
                            
                            fromAddress = 'no-replysf@damacgroup.com';
                            bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
                            contentType = 'text/html';
                            strCCAddress = '';
                            system.debug(' emailTemplateObj : '+ emailTemplateObj );
                            if( toAddress != '' && emailTemplateObj != null ) {
                                
                                if(emailTemplateObj.body != NULL){
                                    contentBody =  emailTemplateObj.body;
                                }
                                if(emailTemplateObj.htmlValue != NULL){
                                    contentValue = emailTemplateObj.htmlValue;
                                }
                                if(string.isblank(contentValue)){
                                    contentValue='No HTML Body';
                                }
                                
                                // replace {!ClientIssue}
                                contentValue = String.isNotBlank(lstCL[0].Account__r.Name)?contentValue.replace('{!CxName}',
                                                                                                             lstCL[0].Account__r.Name):contentValue.replace('{!CxName}',''); 
                                contentBody = String.isNotBlank(lstCL[0].Account__r.Name)?contentBody.replace('{!CxName}', 
                                                                                                           lstCL[0].Account__r.Name ):contentBody.replace('{!CxName}',''); 
                                
                                subject = emailTemplateObj.Subject != NULL ? emailTemplateObj.Subject : 'Send Email Test';
                            }
                            
                            // Callout to sendgrid to send an email
                            SendGridEmailService.SendGridResponse objSendGridResponse =
                                SendGridEmailService.sendEmailService(
                                    toAddress
                                    , ''
                                    , strCCAddress
                                    , ''
                                    , bccAddress
                                    , ''
                                    , subject
                                    , ''
                                    , fromAddress
                                    , ''
                                    , replyToAddress
                                    , ''
                                    , contentType
                                    , contentValue
                                    , ''
                                    , new List<Attachment> { objAtt }
                                );
                            
                            System.debug('objSendGridResponse == ' + objSendGridResponse);
                            //Create Email Activity
                            String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                            if (responseStatus == 'Accepted') {
                                EmailMessage mail = new EmailMessage();
                                mail.Subject = subject;
                                mail.MessageDate = System.Today();
                                mail.Status = '3';//'Sent';
                                mail.RelatedToId = lstCL[0].Id;
                                mail.Account__c  = strAccountId;
                                mail.Type__c = 'CSR Email';
                                mail.ToAddress = toAddress;
                                mail.FromAddress = fromAddress;
                                mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                                mail.Sent_By_Sendgrid__c = true;
                                mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                                mail.CcAddress = strCCAddress;
                                mail.BccAddress = bccAddress;
                                lstEmails.add(mail);
                                system.debug('Mail obj == ' + mail);
                                
                                
                            }//End Email Activity Creation
                            if(lstEmails != null && lstEmails.size()>0 ){
                                insert lstEmails;
                                //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Email has been sent to cx: '+ toAddress);
                                //ApexPages.addMessage(myMsg);
                            }
                        }
                    }
                }
            }
        }
    }
     
         /**
     * Method used to redirect to Booking Unit detail page
     */    
    public PageReference redirectToDetailPage() {

        //Redirect to Booking Unit detail page
        PageReference callingListPage = new PageReference ('/'+callingListId);
        return callingListPage;
    }
}