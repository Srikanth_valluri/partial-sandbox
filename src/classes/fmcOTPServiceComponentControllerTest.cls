@isTest
private class fmcOTPServiceComponentControllerTest {

	@isTest
	static void TestMethod1() {
		Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
		Account account = new Account( LastName = 'Test Account',
		                               Party_ID__c = '63062',
		                               RecordtypeId = rtId,
		                               Email__pc = 'test@mailinator.com',
		                               Mobile_Phone_Encrypt__pc = '00911234567890');
		insert account;
		CustomerCommunityUtils.customerAccountId = account.Id;

		Test.startTest();
		fmcOTPServiceComponentController obj = new fmcOTPServiceComponentController();
		obj.sendVerificationCodes();
		obj.verifyOTP();
		Test.stopTest();
	}
}