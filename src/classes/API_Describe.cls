@RestResource(urlMapping='/api_describe/*')
global class API_Describe {
    
    @HttpGet
    global static void doGet() {
        RestRequest request = RestContext.request;
        
        String requestType = request.requestURI.substring(request.requestURI.lastIndexOf('/' ) + 1);
        
        String objectName = RestContext.request.params.get('object');
        String fieldName = RestContext.request.params.get('field');
        
        String responseBody = '';
        Integer statusCode = 200;
        try {
            if (requestType.equalsIgnoreCase('object')) {
                
                if(String.isBlank(objectName)){
                    statusCode = 500;
                    responseBody = 'Object Name parameter is required';
                }
                else{
                    responseBody = describeObject(objectName);
                }
                
            } else if (requestType.equalsIgnoreCase('objectFields')) {
                
                if (String.isBlank(objectName)) {
                    statusCode = 500;
                    responseBody = 'Object Name parameter is required';
                } else {
                    responseBody = describeObjectFields(objectName);
                }
                
            } else if (requestType.equalsIgnoreCase('field')) {
                
                if (String.isBlank(objectName) || String.isBlank(fieldName)) {
                    statusCode = 500;
                    responseBody = 'Object Name and Field Name parameters are required';
                } else {
                    System.debug('>>>objectName>>>>>>>'+objectName);
                    System.debug('>>>fieldName>>>>>>>'+fieldName);
                    if(objectName == 'Buyer__c' && fieldName == 'City__c'){
                        Schema.DescribeFieldResult F1 = Buyer__c.City__c.getDescribe();
                        Schema.sObjectField T1 = F1.getSObjectField();
                        
                        Schema.DescribeFieldResult F2 = Buyer__c.Country__c.getDescribe();
                        Schema.sObjectField T2 = F2.getSObjectField();
                        
                        Map<String, List<String>> mpvalues = DependentPickListFields.getDependentOptionsImpl(t1, t2);
                        list<Dependency> data = new list<Dependency>();
                        for(string country: mpValues.Keyset()){
                            Dependency wrap = new Dependency();
                            
                            wrap.label = country;
                            wrap.value = country;
                            wrap.active = true;
                            list<PicklistValue> cityValues = new list<PicklistValue>();
                            for(string city: mpValues.get(country)){
                                PicklistValue cityData = new PicklistValue();
                                cityData.label = city;
                                cityData.value = city;
                                cityData.active = true;
                                cityValues.add(cityData);
                            }
                            
                            wrap.cities = cityValues;
                            data.add(wrap); 
                        }
                        FinalData fd = new FinalData();
                        fd.data = data;
                        responseBody = JSON.serialize(fd);
                    }
                    else
                        responseBody = describeField(objectName, fieldName);
                }
                
            } else {
                statusCode = 400;
                responseBody = 'Method not found';
            }
        } catch (Exception ex)         {
            statusCode = 500;
            responseBody = ex.getMessage();
        }
        RestResponse response = RestContext.response;
        response.statusCode = statusCode;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(responseBody);
    }
    
    global static String describeObject(String objectName) {
        Schema.DescribeSObjectResult describeSObjectResult = Schema.getGlobalDescribe().get(objectName).getDescribe();
        return JSON.serialize(describeSObjectResult);
    }
    
    global static String describeObjectFields(String objectName) {
        Map<String, Schema.SObjectField> SObjectFieldMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        
        Map<String, Schema.DescribeFieldResult> describeFieldResultMap = new Map<String, Schema.DescribeFieldResult>();
        
        for (String sObjectFieldKey : SObjectFieldMap.keySet()) {
            Schema.DescribeFieldResult describeFieldResult = SObjectFieldMap.get(sObjectFieldKey).getDescribe();
            describeFieldResultMap.put(sObjectFieldKey, describeFieldResult);
        }
        
        return JSON.serialize(describeFieldResultMap);
    }
    
    global static String describeField(String objectName, String fieldName) {
        Map<String, Schema.SObjectField> SObjectFieldMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        Schema.DescribeFieldResult describeFieldResult = SObjectFieldMap.get(fieldName).getDescribe();
        //system.debug(sObjectField);
        return JSON.serialize(describeFieldResult);
    }
    global class FinalData{
        global list<Dependency> data;
    }
    global class Dependency{
        global string label;
        global string value;
        global boolean active;
        global list<PicklistValue> cities;
    }
    global class PicklistValue{
        global string label;
        global string value;
        global boolean active;
    }
}