@isTest
public class createLHOCallinglistTest{
    
    @isTest 
     static void getTest(){
     
      Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        list<Id> lstCaseId = new list<Id>();
        Case Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Type = 'Lease_Handover';
        Cas.Booking_Unit__c = BU.Id;
        insert Cas;
        System.assert(Cas != null);
        
        list<SR_Attachments__c> lstAttach = new list<SR_Attachments__c>();
        SR_Attachments__c objAttach = new SR_Attachments__c ();
        objAttach.Name = 'Doc 1';
        objAttach.Attachment_URL__c = 'www.google.com';
        objAttach.Case__c = Cas.Id;
        lstAttach.add(objAttach);
        
        SR_Attachments__c objAttach2 = new SR_Attachments__c ();
        objAttach2.Name = 'Doc 2';
        objAttach2.Attachment_URL__c = 'www.google.com';
        objAttach2.Case__c = Cas.Id;
        lstAttach.add(objAttach2);
        
        insert lstAttach;
        //Cas.Early_Handover_Status__c = 'Agreement Generated';
        //Cas.Agreement_Executed__c = false;
        
        //lstCaseId.add(Cas.Id);
        
        /*Task tsk = new Task();
        tsk.WhatId = Cas.Id;
        tsk.ActivityDate = System.today()+2;
        tsk.Subject = 'Subject';
        tsk.Status = 'Not Started';
        tsk.Assigned_User__c = 'Finance'; 
        tsk.Status = 'Not Started';
        tsk.Process_Name__c = 'Lease Handover';
        insert tsk ;  
        List<Task> lstTask =  new List<Task>();
        lstTask.add(tsk);
        */
         
        
        test.startTest();
            
            lstCaseId.add(Cas.Id);
            createLHOCallinglist.invokeApex(lstCaseId);
        test.stopTest();
    }
}