/******************************************************************************
* Description - Test class developed for CustomerNotificationBatchUtility
*
* Version            Date            Author                    Description
* 1.0                11/12/17        Naresh Kaneriya (Accely)   Initial Draft
********************************************************************************/

@isTest
public class CustomerNotificationBatchUtilityTest{
    
    
    public static testmethod void CustomerNotificationBatchUtility(){
         Set<String> lstEmailTemplateAPINames  = new Set<String>();
         CustomerNotificationBatchUtility.getListOfNotificationSettings();
         
         EmailTemplate ET = new EmailTemplate();
         ET.isActive = true;
         ET.Name = 'name';
         ET.DeveloperName = 'unique_name_addSomethingSpecialHere';
         ET.TemplateType = 'text';
         ET.FolderId = UserInfo.getUserId();
         insert ET ;
         
         System.assert(ET != null);
         System.assertEquals(ET.DeveloperName, 'unique_name_addSomethingSpecialHere');
         
         String  Tem = [select id , DeveloperName  from EmailTemplate where DeveloperName = 'unique_name_addSomethingSpecialHere'].DeveloperName;
         System.assert(Tem != null);
         lstEmailTemplateAPINames.add(Tem);
         System.assert(lstEmailTemplateAPINames != null);
         CustomerNotificationBatchUtility.getEmailTemplates(lstEmailTemplateAPINames);
    }
}