global  class ExecuteSendEmailsBatch {
    webservice static string executeBatch (ID recordId, String query) {
        Email_Request__c req = new Email_request__c ();
        req = [ SELECT Job_Status__C FROM Email_request__c WHERE ID =: recordId ];
        if (req.job_Status__c != 'Processing' && req.job_Status__c != 'Started') {
            Database.executeBatch(new DAMAC_GenerateEmailmetrics (recordId, query), 1);
            return 'Email job started.';
        }
        else {
            return 'Email job is already running.';
        }        
    }
}