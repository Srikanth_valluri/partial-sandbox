// CreatedBy :  Naresh Kaneriya
public class AgentCommissionPoliciesController{
    
    public Boolean isTier{get;set;}
    public Id AccID;
    public Account Acc;
    
    // Constructor
    public AgentCommissionPoliciesController(){
        Acc=new  Account();
        isTier=false;
        getComissionDetails();
    }
    
    // Method to Check Eligible for Tier or Not
    public void getComissionDetails(){
        
        try{
            
            AccID=AgentPortalUtilityQueryManager.getAccountId();
            System.debug('Login User Account Id--- '+AccID);    
            Acc=[SELECT id,Eligible_For_Tier_Program__c FROM Account WHERE Id=:AccID Limit 1];
            
            System.debug('Account Details---- '+Acc);
            if(Acc.Eligible_For_Tier_Program__c==true){
                isTier=true;
            }
             System.debug('isTier--isTier-- '+isTier);
            
            
        }
        catch(Exception Ex){
            System.debug('Excpetion in AgentCommissionPoliciesController Class--- Message '+Ex.getMessage());
            System.debug('Excpetion in AgentCommissionPoliciesController Class--- Line '+Ex.getLineNumber());
        }
        
    
    }
}