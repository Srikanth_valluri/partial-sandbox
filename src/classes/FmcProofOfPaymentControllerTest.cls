@isTest
private class FmcProofOfPaymentControllerTest {
    static Account a = new Account();
    static Account a1 = new Account();
    static Booking__c  bk = new  Booking__c();
    static NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
    static Booking_Unit__c bu = new Booking_Unit__c();
    static Booking_Unit__c bu1 = new Booking_Unit__c();
    static Location__c locObj = new Location__c();
    static FM_Case__c fmObj = new FM_Case__c();
    static SR_Booking_Unit__c srBu = new SR_Booking_Unit__c();

    static void setUp() {
        a.Name = 'Test Account';
        insert a;
        a1.Name = 'Test Account';
        insert a1;


        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        sr.NSIBPM__Customer__c = a.id;
        sr.RecordTypeId = RecType;
        insert sr;

        bk.Account__c = a.id;
        bk.Deal_SR__c = sr.id;
        insert bk;

        bu.Booking__c = bk.id;
        bu.Unit_Name__c = 'LSB/10/B1001';
        bu.Property_City__c = 'Dubai';
        insert bu;
        bu1.Booking__c = bk.id;
        bu1.Unit_Name__c = 'LSB/10/B1001';
        bu1.Property_City__c = 'Dubai';
        insert bu1;

        locObj.Name  = 'LSB';
        locObj.Location_ID__c = '83488';
        insert locObj;

        fmObj.Booking_Unit__c = bu.id;
        fmObj.Expected_move_out_date__c = Date.Today();
        fmObj.Tenant__c = a.id;
        fmObj.Account__c = a.id;
        insert fmObj;

        srBu.Booking_Unit__c = bu.id;
        srBu.FM_Case__c = fmObj.id;
        insert srBu;

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'ProofOfPayment');
        Test.setCurrentPage(currentPage);

    }
    static testMethod void testMethodSaveAsDraft() {
        setUp();
        PageReference myVfPage = Page.ProofOfPaymentProcessPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',a.id);
        ApexPages.currentPage().getParameters().put('SRType','Proof_of_Payment');
        ApexPages.currentPage().getParameters().put('id',String.valueOf(fmObj.id));
        ApexPages.currentPage().getParameters().put('view', 'ProofOfPayment');
        FmcProofOfPaymentController popObj = new FmcProofOfPaymentController();
        popObj.processUnitsWrapper();
        popObj.saveAsDraft();
        popObj.insertCase();
        popObj.submitSr();
    }
    static testMethod void testMethodSubmitSr() {
        setUp();
        FmcProofOfPaymentController popObj = new FmcProofOfPaymentController();
        popObj.submitSr();
        popObj.insertCase();
    }
    static testMethod void testMethodInsertCase() {
        setUp();
        FmcProofOfPaymentController popObj = new FmcProofOfPaymentController();
        popObj.insertCase();
    }
    static testMethod void testMethodSaveAsDraftId() {
        setUp();
        PageReference myVfPage = Page.ProofOfPaymentProcessPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',a.id);
        ApexPages.currentPage().getParameters().put('SRType','Proof_of_Payment');
        ApexPages.currentPage().getParameters().put('view', 'ProofOfPayment');
        FmcProofOfPaymentController popObj = new FmcProofOfPaymentController();
        popObj.processUnitsWrapper();
        popObj.saveAsDraft();
        popObj.insertCase();
        popObj.submitSr();

    }

    static testMethod void testMethodDeleteDocument() {
        SR_Attachments__c srObj = new SR_Attachments__c();
        srObj.Name = 'Test';
        insert srObj;
        FmcProofOfPaymentController popObj = new FmcProofOfPaymentController();
        popObj.processUnitsWrapper();
        popObj.deleteAttRecId = String.valueOf(srObj.id);
        popObj.deleteAttachment();
    }
}