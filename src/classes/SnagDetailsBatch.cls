/*********************************************************************************
* Description - Batch get snag defects of Booking Unit                           *
*                                                                                *
* Version   Date            Author              Description                      *
* 1.0                                           Initial Draft                    *
* 1.2       14/04/2019      Aishwarya Todkar    Moved logic from Case to BU      *
**********************************************************************************/
global class SnagDetailsBatch implements Database.Batchable<sObject>
                                        , Database.Stateful
                                        , Database.AllowsCallouts {
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('***SnagDetailsBatch start****');
        /*String strQuery = 'select Id, AccountId, Account.Mobile_Phone_Encrypt__pc, Account.Name'
                        + ', Account.Email__pc, Booking_Unit__r.Property_Name_Inventory__c '                             
                        + ', Booking_Unit__c, SNAGs_Completed__c, CaseNumber'
                        + ', Booking_Unit__r.Unit_Name__c ,Booking_Unit__r.inventory__r.Building_Location__r.Is_New_Snagger__c from case'
                        + ' Where Booking_Unit__c != Null AND '
                        + '(recordtype.Name =\'Early Handover\' OR recordtype.Name = \'Handover\')'
                        + 'AND SNAGs_Completed__c = false ';*/
         
        String strQuery = 'SELECT Id , Booking__r.Account__c, Booking__r.Account__r.Mobile_Phone_Encrypt__pc ' +
                        ' , Booking__r.Account__r.Name, Booking__r.Account__r.Email__pc, Property_Name_Inventory__c ' +
                        ' , SNAGs_Completed__c, Unit_Name__c, inventory__r.Building_Location__r.Is_New_Snagger__c ' +
                        ' , (select Id,Snags_Reported_at_first__c,SNAGs_Completed__c FROM Cases__r ' +
                        ' Where (recordtype.Name =\'Early Handover\' OR recordtype.Name = \'Handover\') ' +
                        ' AND SNAGs_Completed__c = false LImit 1) ' +
                        ' FROM Booking_Unit__c ' +
                        ' WHERE Unit_Name__c != NULL AND Booking__r.Account__c != NULL AND ' +
                        ' SNAGs_Completed__c = false AND Unit_Active__c = \'Active\'';
        
        System.debug('snag batch strQuery :: ' + strQuery);
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> listBUs){ 
        System.debug('***SnagDetailsBatch execute****');
        UpdateSnagInfoonCase.getSnagInfo(listBUs);    
    }

    global void finish(Database.BatchableContext BC){
        System.debug('***SnagDetailsBatch finish****');
    }
}