/************************************************************************************************
 * @Name              : DAMAC_Agents_UpdateDeviceTokenIdTest
 * @Description       : Test Class for DAMAC_Agents_UpdateDeviceTokenId_API
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         11/08/2020       Created
***********************************************************************************************/

@isTest
public class DAMAC_Agents_UpdateDeviceTokenIdTest{

    @isTest
    static void testAPI(){
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        
     	User usr = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jasonstatham.liveston@asdf.com',
                           Username = 'jasonstatham.liveston@asdf.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.params.put ('userId', usr.Id);        
        request.params.put ('deviceTokenId', 'testTokenId');
        request.params.put ('unRegister', 'false');
        request.requestUri ='/updateDeviceToken';
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_UpdateDeviceTokenId_API.updateDeviceTokenId();
        
        request.params.put ('userId', usr.Id+'123456');
        request.params.put ('deviceTokenId', 'testTokenId');
        request.params.put ('unRegister', 'true');
        request.requestUri ='/updateDeviceToken';
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_UpdateDeviceTokenId_API.updateDeviceTokenId();
        
        request.params.put ('userId', '');
        request.params.put ('deviceTokenId', '');
        request.params.put ('unRegister', '');
        request.requestUri ='/updateDeviceToken';
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_UpdateDeviceTokenId_API.updateDeviceTokenId();
        
        request.params.put ('userId', 'r');
        request.params.put ('deviceTokenId', 'r');
        request.params.put ('unRegister', '');
        request.requestUri ='/updateDeviceTokens';
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_UpdateDeviceTokenId_API.updateDeviceTokenId();
        
        request.params.put ('userId', usr.Id);
        request.params.put ('deviceTokenId', '');
        request.params.put ('unRegister', '');
        request.requestUri ='/updateDeviceTokens';
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_UpdateDeviceTokenId_API.updateDeviceTokenId();
        
        request.params.put ('userId', usr.Id);
        request.params.put ('deviceTokenId', '');
        request.params.put ('unRegister', 'true');
        request.requestUri ='/updateDeviceTokens';
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_UpdateDeviceTokenId_API.updateDeviceTokenId();
        
        request.params.put ('userId', usr.Id);
        request.params.put ('deviceTokenId', 'test1');
        request.params.put ('unRegister', 'true');
        request.requestUri ='/updateDeviceTokens';
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_UpdateDeviceTokenId_API.updateDeviceTokenId();
        
        Test.stopTest();
    }

}