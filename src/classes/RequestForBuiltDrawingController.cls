global virtual without sharing class RequestForBuiltDrawingController {
    public list<SelectOption> lstUnits { get; set; }
    
    //public map<String, list<FM_Additional_Detail__c>> mapAdditionalDetails { get; set; }    
    
    public Booking_Unit__c objUnit { get; set; }
    public Boolean isEdittable{get;set;}
    public Boolean isMandatory{get;set;}
    public FM_Case__c objFMCase { get; set; }
    public string strFMCaseId{get;set;}
    public String strSRType { get; set; }
    public String strSelectedUnit { get; set; }
    public String strAccountId ;
    public String[] valuesOfDrawings { get; set; }
    public SelectOption[] valueListOfDrawings { get; set; }
    public string feeAsBuiltDrawing{get; set;}
    
    public transient String strDocumentBody { get; set; }
    public transient String strDocumentName { get; set; }   
    public transient String deleteAttRecId { get; set; }
    
    public list<FM_Documents__mdt> lstDocuments { get; set; } 
    public list<SR_Attachments__c> lstRecentAttachment { get; set; }
    public list<SR_Attachments__c> lstUploadedDocs { get; set; }
    @testVisible private map<String, FM_Documents__mdt> mapProcessDocuments ;
    @testVisible private map<String, SR_Attachments__c> mapUploadedDocs ;
    
    protected RequestForBuiltDrawingController(Boolean shouldCall) {}
    
    public RequestForBuiltDrawingController() {
        isEdittable=true;
        valueListOfDrawings=new List<SelectOption>();
        valuesOfDrawings=new List<String>();
        isMandatory=false;
        strFMCaseId = ApexPages.currentPage().getParameters().get('Id');
        if(String.isBlank( strFMCaseId )){
            strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
            strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
            strSRType = ApexPages.currentPage().getParameters().get('SRType');
        }
        system.debug('== strAccountId =='+strAccountId );
        if(String.isNotBlank(strSelectedUnit)
           || String.isNotBlank(strFMCaseId)){
               init();
           }
    }
    
    public virtual void init() {
        initializeFMCase();
        System.debug('objFMCase.Submitted__c======'+objFMCase.Submitted__c);
        if(objFMCase.Submitted__c==true){
            isEdittable=false;
        }
        System.debug('isEdittable======'+isEdittable);
        if(strFMCaseId == null){
            //Initialize the Additional details map.
            //mapAdditionalDetails = new map<String, list<FM_Additional_Detail__c>>();
            //FM_Additional_Detail__c objDetail = new FM_Additional_Detail__c();
            //objDetail.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
            //objDetail.Emergency_Contact_Case__c = objFMCase.Id ;
            //mapAdditionalDetails.put( 'Emergency Contact' , new list<FM_Additional_Detail__c> { objDetail } ) ;
        }else{
            system.debug('*****entered else*****');
            strSelectedUnit = objFMCase.Booking_Unit__c;
            System.debug('strSelectedUnit======'+strSelectedUnit);
        }
        objUnit = getUnitDetails(strSelectedUnit) ;
        System.debug('objUnit.Inventory__r.Building_Location__c'+objUnit.Inventory__r.Building_Location__c);
        List<Location__c> objList=new List<Location__c>();
        objList=[select As_Built_Drawing_Fee__c from Location__c where id=:objUnit.Inventory__r.Building_Location__c];
        if(!objList.isEmpty()){
            System.debug('obj=-========'+objList);
            System.debug('obj.As_Built_Drawing_Fee__c=-========'+objList[0].As_Built_Drawing_Fee__c);
            feeAsBuiltDrawing = objList[0].As_Built_Drawing_Fee__c != NULL ? String.valueof(objList[0].As_Built_Drawing_Fee__c) : '';
            System.debug('feeAsBuiltDrawing-------'+feeAsBuiltDrawing);
        }
        
        if( String.isNotBlank( strSRType ) && objUnit != NULL ) {
            processDocuments();
        }
        getfieldValues();
        System.debug('-----objFMCase.id-----'+objFMCase.id);
        System.debug('=====lstDocuments===='+lstDocuments);
        
    }
    
    public void getfieldValues(){
        Schema.DescribeFieldResult fieldResult = FM_Case__c.Type_of_Drawings__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        System.debug('ple===='+ple);
       for( Schema.PicklistEntry f : ple)
       {
           System.debug('f-------'+f);
           System.debug('f.get'+f.getLabeL());
           valueListOfDrawings.add(new SelectOption(f.getLabel(),f.getLabel()));
       }
       valueListOfDrawings.sort();
    }
    
    public static Booking_Unit__c getUnitDetails( String strUnitId ) {
        if (String.isBlank(strUnitId)) {
            return NULL;
        }
        return  FM_Utility.getUnitDetails(strUnitId);
    }
    public void initializeFMCase() {
        system.debug('strFMCaseId*****'+strFMCaseId);
        if(strFMCaseId == null){
            objFMCase = new FM_Case__c();
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Request For Built-in Drawings').getRecordTypeId();
            objFMCase.Account__c = strAccountId ;
            objFMCase.Booking_Unit__c = strSelectedUnit ;
            objFMCase.Request_Type_DeveloperName__c = strSRType ;
            objFMCase.Request_Type__c = 'Request For Built-in Drawings';    
        }else{
            system.debug('Inside existing case*****');
            objFMCase = FM_Utility.getCaseDetails(strFMCaseId);
            if(!String.isEmpty(objFMCase.Type_of_Drawings__c)){
                String[] fetchedValuesAfterSaveAsDraft=objFMCase.Type_of_Drawings__c.split(';');
                valuesOfDrawings=fetchedValuesAfterSaveAsDraft;
            }
            System.debug('objFMCase======'+objFMCase);
            strAccountId = objFMCase.Account__c ;
            strSelectedUnit = objFMCase.Booking_Unit__c ;
            strSRType = objFMCase.Request_Type_DeveloperName__c ;
        }
    }
    
    /*public PageReference submitRequestForWorkPermit() {
if( objFMCase != NULL ) {
System.debug('objFMCase--------'+objFMCase);
objFMCase.Status__c='Submitted';
objFMCase.Submitted__c=true;
objFMCase.Origin__c='Walk-In';
if(objFMCase.Submitted__c==true){
System.debug('objFMCase===='+objFMCase);
upsert objFMCase ;
}
PageReference objPage = new PageReference('/' + objFMCase.Id );
return objPage ;
}
return NULL ;
}*/
    
    public virtual PageReference createRequestForWorkPermit() {
        if( objFMCase != NULL ) {
            System.debug('objFMCase--------'+objFMCase);
            createMethod();
            PageReference objPage = new PageReference('/' + objFMCase.Id );
            return objPage ;
        }
        return NULL ;
    }
    
    public void createCaseShowUploadDoc(){
        createMethod();
        System.debug('objFMCase-----'+objFMCase);
        System.debug('oBjFMCase.id------'+oBjFMCase.id);
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            processDocuments();
        }
        strFMCaseId=objFMCase.id;
        System.debug('strFMCaseId----'+strFMCaseId);
        
    }
    public virtual void createMethod(){
        objFMCase.Origin__c='Walk-In';
        objFMCase.Type_of_Drawings__c=String.join(valuesOfDrawings ,';');
        if(!String.isBlank(objFMCase.Email__c) || !String.isBlank(objFMCase.Email_2__c)){
            objFMCase.Contact_Email__c = !String.isBlank(objFMCase.Email__c) ? objFMCase.Email__c : objFMCase.Email_2__c ;
        }
        objFMCase.As_Built_Fee__c= !String.isBlank(feeAsBuiltDrawing) ? Decimal.valueOf(feeAsBuiltDrawing) : 0;
        upsert objFMCase ;
    }
    
    public virtual PageReference submitRequestForWorkPermit() {
        if( objFMCase != NULL ) {
            System.debug('objFMCase--------'+objFMCase);
            if(objFMCase.id==null){
                insert objFMCase;    
            }
            if(!String.isBlank(objFMCase.Email__c) || !String.isBlank(objFMCase.Email_2__c)){
                objFMCase.Contact_Email__c = !String.isBlank(objFMCase.Email__c) ? objFMCase.Email__c : objFMCase.Email_2__c ;
            }
            objFMCase.As_Built_Fee__c= !String.isBlank(feeAsBuiltDrawing) ? Decimal.valueOf(feeAsBuiltDrawing) : 0;
            objFMCase.Type_of_Drawings__c=String.join(valuesOfDrawings ,';');
            objFMCase.Status__c='Submitted';
            objFMCase.Submitted__c=true;
            System.debug('objFMCase.submitted__c====='+objFMCase.Submitted__c);
            if(objFMCase.Submitted__c==true){
                System.debug('objFMCase.submitted__c=====1'+objFMCase.Submitted__c);
                
                System.debug('objFMCase.Unit_Name__c-----'+objUnit.Unit_Name__c);
                if( String.isNotBlank( objUnit.Unit_Name__c ) ) {
                    list<FM_User__c> lstUsers = FM_Utility.getApprovingUsers( new set<String> { objUnit.Unit_Name__c.split('/')[0] } );
                    System.debug('lstUsers ======'+lstUsers);
                    for( FM_User__c objUser : lstUsers ) {
                        if( String.isNotBlank( objUser.FM_Role__c ) ) {
                            if( objUser.FM_Role__c.containsIgnoreCase( 'Manager') ) {
                                objFMCase.FM_Manager_Email__c = objUser.FM_User__r.Email;
                            }
                            else if( objUser.FM_Role__c.containsIgnoreCase( 'Admin') && 
                                    objFMCase.Admin__c == NULL ) {
                                        objFMCase.Admin__c = objUser.FM_User__c;
                                    }
                        }
                    }
                } 
                String strApprovingUsers='';
                System.debug('objFMCase.Request_Type_DeveloperName__c===='+objFMCase.Request_Type_DeveloperName__c);
                System.debug('objUnit.Property_City__c'+objUnit.Property_City__c);
                List<FM_Approver__mdt> listApproverUser=FM_Utility.fetchApprovers(objFMCase.Request_Type_DeveloperName__c,objUnit.Property_City__c);
                System.debug('listApproverUser====='+listApproverUser);
                if(!listApproverUser.isEmpty()){
                    System.debug('----1----');
                    for(FM_Approver__mdt fmApproverInstance:listApproverUser){
                        System.debug('----2----');
                        strApprovingUsers=strApprovingUsers+fmApproverInstance.Role__c+',';
                    }
                    System.debug('strApprovingUsers==='+strApprovingUsers);
                    strApprovingUsers=strApprovingUsers.removeEnd(',');
                    objFMCase.Approving_Authorities__c=strApprovingUsers;
                    objFMCase.Approval_Status__c='Pending';
                    //objFMCase.Submit_for_Approval__c=true;
                    update objFMCase;
                }
            }
            PageReference objPage = new PageReference('/' + objFMCase.Id );
            return objPage ;
        }
        return NULL ;
    }
    
    /*public PageReference returnBackToCasePage() {
System.debug('objFMCase.id-------3---------'+objFMCase.id);
if( objFMCase != NULL ) {
PageReference objPage = new PageReference('/' + objFMCase.Id );
return objPage ;
}
return NULL;
}*/
    
    
    
    
    @testVisible 
    public void processDocuments() {
        lstUploadedDocs = NULL ;
        lstDocuments = NULL ;
        Set<FM_Documents__mdt> listSpecificDocs=new Set<FM_Documents__mdt>();
        List<String> lstOfString=new List<String>();
        system.debug('objFMCase : '+objFMCase);
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            mapUploadedDocs = new map<String, SR_Attachments__c>();
            for( SR_Attachments__c objAttach : fetchUploadedDocs() ) {
                mapUploadedDocs.put( objAttach.Name, objAttach );
            }
            if( mapUploadedDocs != NULL && !mapUploadedDocs.isEMpty() ) {
                lstUploadedDocs = new list<SR_Attachments__c>();
                lstUploadedDocs.addAll( mapUploadedDocs.values() );
            }
        }
        
        mapProcessDocuments = new map< String, FM_Documents__mdt >();
        lstDocuments = new list<FM_Documents__mdt>();
        system.debug('objUnit : '+objUnit);
        system.debug('strSRType : '+strSRType);
        system.debug('objUnit.Property_City__c : '+objUnit.Property_City__c);
        for( FM_Documents__mdt objDocMeta : FM_Utility.getDocumentsList( strSRType, objUnit.Property_City__c ) ) {
            System.debug('objDocMeta===='+objDocMeta);
            mapProcessDocuments.put( objDocMeta.DeveloperName, objDocMeta );
            System.debug('mapProcessDocuments===='+mapProcessDocuments);
            System.debug('mapUploadedDocs===='+mapUploadedDocs);
            if( mapUploadedDocs != NULL && !mapUploadedDocs.containsKey( objDocMeta.MasterLabel ) ) {
                lstDocuments.add( objDocMeta );
            }
            System.debug('lstDocuments----------------->>>>'+lstDocuments);
            if(objDocMeta.Mandatory__c==true){
                lstOfString.add('true');
            }
        }
        if(lstOfString.size()>0){
            isMandatory=true;
        }
        for(FM_Documents__mdt ins:lstDocuments){
            if(!mapUploadedDocs.keyset().contains(ins.Document_Name__c)){
                if(isBlank(objFMCase.Person_To_Collect__c)=='Consultant' && ins.is_Consultant__c==true){
                    listSpecificDocs.add(ins);
                }
                if(isBlank(objFMCase.Person_To_Collect__c)=='Contractor' && ins.is_Contractor__c==true){
                    listSpecificDocs.add(ins);
                }
                if(isBlank(ins.Document_Name__c)=='Proof of Payment'){
                    listSpecificDocs.add(ins);
                }
                if(isBlank(objFMCase.Person_To_Collect__c)=='Tenant' && isBlank(ins.Submitted_By__c)=='Tenant'){
                    listSpecificDocs.add(ins);
                }
            }
        }
        lstDocuments.clear();
        lstDocuments.addAll(listSpecificDocs);
        
    }
    
    public string isBlank(String str){
        return str == NULL ? '' : str;
    }
    
    
    public void deleteAttachment() {
        if( String.isNotBlank( deleteAttRecId ) ) {
            delete new SR_Attachments__c( Id = deleteAttRecId );
            processDocuments();
        }
    }
    
    @testVisible
    private void insertCustomAttachment( Id fmCaseId, UploadMultipleDocController.data objResponse ) {
        list<SR_Attachments__c> lstCustomAttachments = new list<SR_Attachments__c>();
        
        strDocumentName = strDocumentName.substring( 0, strDocumentName.lastIndexOf('.') );
        
        FM_Documents__mdt objDocMeta = mapProcessDocuments != null && mapProcessDocuments.containsKey( strDocumentName ) ?
            mapProcessDocuments.get( strDocumentName ) :
        NULL ;
        
        for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            SR_Attachments__c objCustAttach = new SR_Attachments__c();
            objCustAttach.Account__c = strAccountId ;
            objCustAttach.Attachment_URL__c = objFile.url;
            objCustAttach.Booking_Unit__c = strSelectedUnit ;
            objCustAttach.FM_Case__c = fmCaseId ;
            objCustAttach.Name = objDocMeta != NULL ? objDocMeta.MasterLabel : '' ;
            objCustAttach.Type__c = objDocMeta != NULL ? objDocMeta.Type__c : '' ;
            objCustAttach.isValid__c = true ;
            lstCustomAttachments.add( objCustAttach );
            strDocumentName = '';
            strDocumentBody = '';
        }
        if( !lstCustomAttachments.isEmpty() ) {
            insert lstCustomAttachments ;
            system.debug('==lstCustomAttachments=='+lstCustomAttachments);
            
            processDocuments() ;
            system.debug('==lstUploadedDocs=='+lstUploadedDocs);
            system.debug('==lstDocuments=='+lstDocuments);
        }
    }
    
    public list<SR_Attachments__c> fetchUploadedDocs() {
        if( String.isNotBlank( objFMCase.Id ) ) {
            return [ SELECT Id
                    , Type__c
                    , isValid__c
                    , Attachment_URL__c
                    , Name
                    FROM SR_Attachments__c
                    WHERE FM_Case__c =: objFMCase.Id ];
        }
        return new list<SR_Attachments__c>();
    }
    
    public void uploadDocument() {
        system.debug('== strDocumentBody =='+strDocumentBody);
        system.debug('== strDocumentName =='+strDocumentName);
        //initializeFMCase();
        System.debug('objFMCase.id======'+objFMCase.id);
        System.debug('objFMCase======'+objFMCase);
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            System.debug('=========='+objFMCase != NULL && objFMCase.Id != NULL);
            UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
            list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
            if( String.isNotBlank( strDocumentName ) && String.isNotBlank( strDocumentBody ) ) {
                lstWrapper.add( PenaltyWaiverHelper.makeWrapperObject( EncodingUtil.Base64Encode( RentalPoolTerminationHelper.extractBody( strDocumentBody ) ) ,
                                                                      strDocumentName ,
                                                                      strDocumentName ,
                                                                      objUnit.Registration_Id__c, objFMCase.Id, '1' ) );
            }
            if( !lstWrapper.isEmpty() ) {
                objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper );
                system.debug('== objResponse document upload =='+objResponse);
                if( objResponse != NULL && objResponse.data != NULL ) {
                    insertCustomAttachment( objFMCase.Id, objResponse );
                }
            }
        }
    }
}