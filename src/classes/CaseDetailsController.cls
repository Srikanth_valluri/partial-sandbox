public without sharing class CaseDetailsController {

    public String caseId {get; set;}

    public CaseDetailsController() {
        PageReference currentPage = ApexPages.currentPage();
        if (currentPage != NULL) {
            caseId = ApexPages.currentPage().getParameters().get('id');
        }
    }

    @RemoteAction
    public static List<CaseComment> getCaseComments(String caseId) {
        return [SELECT      ParentId,
                            CommentBody,
                            CreatedById,
                            CreatedBy.Name,
                            CreatedDate
                FROM        CaseComment
                WHERE       ParentId = :caseId
                ORDER BY    CreatedDate
                LIMIT       5000];
    }

    @RemoteAction
    public static CaseComment createCaseComment(String caseId, String commentMessage){
        CaseComment caseComment = new CaseComment();
        caseComment.ParentId = caseId;
        caseComment.CommentBody = commentMessage;
        insert caseComment;
        return caseComment;
    }

   /* @RemoteAction
    public static ConnectApi.FeedElementPage getFeedElementsFromFeed(String subjectId) {
        return ChatterCC.getFeedElementsFromRecordFeed(subjectId);
    }

    @RemoteAction
    public static ConnectApi.FeedElement postTextFeedItem(String subjectId, String feedText) {
        return ChatterCC.postTextFeedItem(subjectId, feedText);
    }*/

    @RemoteAction
    public static Case getCaseDetails(String caseId) {
        return [SELECT      Id,
                            CaseNumber,
                            CreatedDate,
                            Type,
                            Booking_Unit__r.name
                FROM        Case
                WHERE       Id =:caseId ];

    }

}