/**************************************************************************************************
* Name               : CC_OktoCall
* Test Class         : DealCustomCodesTest
* Description        : This is the custom code class for invoking webservice for ok to generate call
* Created Date       : 13/04/2017
* Created By         : NSI - Kaavya Raghuram
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE         Comments
* 1.0         NSI - Kaavya    13/04/2017   Created
* 1.1         QBurst          08/04/2020   SOAP to REST changes
**************************************************************************************************/
global without sharing class CC_OktoCall implements NSIBPM.CustomCodeExecutable {
    public String EvaluateCustomCode(NSIBPM__Service_Request__c sr, NSIBPM__Step__c step) {
        String retStr = 'Success';
        List<Id> bookingIds = new List<Id>();
        try{
            for(Booking__c bookings :[SELECT Id FROM Booking__c WHERE Deal_SR__c =: step.NSIBPM__SR__c]){
                bookingIds.add(bookings.id);
            }
            if(bookingIds.size() > 0){
                system.debug('#### invoking REST Call for ');
                //system.enqueueJob(new AsyncReceiptWebservice (bookingIds, 'OktoCall'));
                DAMAC_IPMS_PARTY_CREATION.updateBookingUnitToIPMS(bookingIds, 'GENERATE_CALL', 'Y'); // 1.1
            }
        } catch (Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
        }
        return retStr;
    }
}