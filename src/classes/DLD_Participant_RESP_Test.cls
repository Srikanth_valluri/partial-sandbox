@IsTest
public class DLD_Participant_RESP_Test {
    
    static testMethod void testParse() {
        String json = '{\"authorityDetails\":{\"activity\":\"\",\"issuingAuthority\":\"\",\"licenseExpiryDate\":\"0\",\"licenseIssueDate\":\"0\",\"licenseNumber\":\"\"},\"creatorMsp\":\"DRBOrgAMSP\",\"docType\":\"temp\",\"email\":\"test@test.com\",\"ownerMSP\":\"DRBOrgBMSP\",\"participantDocuments\":[{\"docName\":\"Copy of a valid passport\",\"docType\":\"37102613\",\"docs\":[{\"docId\":\"b043a4e0-cd24-11e8-93bf-75f086ece67f\",\"hash\":\"4999935eea345177cc87618a8dfe4276\"}],\"isRequired\":true,\"maxPages\":10,\"maxSize\":3000}],\"participantID\":161878259,\"participantNameAr\":\"شيبينغ ياو\",\"participantNameEn\":\"SHIPING YAO\",\"participantNumber\":\"8f0656db-1364-4f98-a02f-b3b3e3d364eb\",\"participantStatus\":0,\"participantType\":1,\"partkey\":\"\",\"personDetails\":{\"dob\":\"340056000000\",\"emiratesId\":\"123455\",\"familyName\":\"ABC\",\"middleName\":\"Middle\",\"nationality\":\"33\",\"passport\":\"12345\",\"uaeIdNumber\":\"12345\"},\"phone\":\"55555555\"}';
        DLD_Participant_RESP obj = DLD_Participant_RESP.parse(json);
        System.assert(obj != null);
    }
}