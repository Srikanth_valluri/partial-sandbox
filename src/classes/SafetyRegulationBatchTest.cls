@isTest
private class SafetyRegulationBatchTest
{
	@isTest
	static void executebatchTRTest()
	{
			Account objAccount = TestDataFactoryFM.createAccount();
			insert objAccount;

			NSIBPM__Service_Request__c objSr = TestDataFactoryFM.createServiceRequest(objAccount);
			insert objSr;

			Booking__c  objBooking = TestDataFactoryFM.createBooking(objAccount, objSr);
      insert objBooking;

      Booking_Unit__c objBookingUnit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
      insert objBookingUnit;

			FM_Case__c objFMCase = new FM_Case__c();
			objFMCase.Account__c = objAccount.id;
			objFMCase.Origin__c = 'Portal';
			objFMCase.Request_Type_DeveloperName__c = 'Tenant_Registration';
			objFMCase.First_Name__c = 'Test';
			objFMCase.Last_Name__c = 'Test';
			objFMCase.Booking_Unit__c = objBookingUnit.id;
			objFMCase.Status__c = 'Submitted';
			insert objFMCase;

			Test.startTest();
			Database.executebatch(new SafetyRegulationBatch());
			Test.stopTest();
	}

	@isTest
	static void executebatchMoveInTest() {
		Account objAccount = TestDataFactoryFM.createAccount();
		insert objAccount;

		NSIBPM__Service_Request__c objSr = TestDataFactoryFM.createServiceRequest(objAccount);
		insert objSr;

		Booking__c  objBooking = TestDataFactoryFM.createBooking(objAccount, objSr);
		insert objBooking;

		Booking_Unit__c objBookingUnit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
		insert objBookingUnit;

		FM_Case__c objFMCase = new FM_Case__c();
		objFMCase.Account__c = objAccount.id;
		objFMCase.Origin__c = 'Portal';
		objFMCase.Request_Type_DeveloperName__c = 'Move_in_Request';
		objFMCase.First_Name__c = 'Test';
		objFMCase.Last_Name__c = 'Test';
		objFMCase.Booking_Unit__c = objBookingUnit.id;
		objFMCase.Status__c = 'Submitted';
		insert objFMCase;

		Test.startTest();
		Database.executebatch(new SafetyRegulationBatch());
		Test.stopTest();
	}

	@isTest
	static void executebatchMoveInTenantTest() {
		Account objAccount = TestDataFactoryFM.createAccount();
		insert objAccount;

		NSIBPM__Service_Request__c objSr = TestDataFactoryFM.createServiceRequest(objAccount);
		insert objSr;

		Booking__c  objBooking = TestDataFactoryFM.createBooking(objAccount, objSr);
		insert objBooking;

		Booking_Unit__c objBookingUnit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
		insert objBookingUnit;

		FM_Case__c objFMCase = new FM_Case__c();
		objFMCase.Tenant__c = objAccount.id;
		objFMCase.Origin__c = 'Portal';
		objFMCase.Request_Type_DeveloperName__c = 'Move_in_Request_T';
		objFMCase.First_Name__c = 'Test';
		objFMCase.Last_Name__c = 'Test';
		objFMCase.Booking_Unit__c = objBookingUnit.id;
		objFMCase.Status__c = 'Submitted';
		insert objFMCase;

		Test.startTest();
		SafetyRegulationBatch objSafetyRegulationBatch = new SafetyRegulationBatch();
		Database.executebatch(objSafetyRegulationBatch);

		SchedulableContext objSchedulableContext = NULL;
		objSafetyRegulationBatch.execute(objSchedulableContext);
		Test.stopTest();
	}
}