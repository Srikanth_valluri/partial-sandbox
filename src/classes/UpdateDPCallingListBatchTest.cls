/***************************************************************************************************
 * @Description : Test class for UpdateDPCallingListBatch                                          *
 *                                                                                                 *
 *                                                                                                 *
 *  Version     Author           Date         Description                                          *
 *  1.0                          27/12/2018   Initial development                                  *
***************************************************************************************************/
@isTest
public class UpdateDPCallingListBatchTest {
  @isTest
    public static void testDPCalling1() {
         //Activate trigger
        activateTrigger();
        //Create account
        Account objAccount = createAccount('United Arab Emirates');

        //Create Service Request
        NSIBPM__Service_Request__c objDealSr = createDealSR();

        //Create Booking
        Booking__c objBooking = createBooking(objAccount.Id, objDealSr.Id);

        //Create Booking Unit
        Booking_Unit__c objBU = createBookingUnit(objBooking.Id,'test Hod');

        //create calling list
        Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        Id  OwnerId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName('Collection_New_Q','Queue');
        System.debug('devRecordTypeId ===' + devRecordTypeId);
        Calling_List__c objCL = createCallingList(objAccount.Id, objBU.Id);
        objCL.Nationality__c = 'Indian';
        objCL.RecordTypeId = devRecordTypeId;
        objCL.OwnerId = OwnerId;
        //objCL.For_Welcome_Call__c = true;
        //objCL.DP_Due_Date__c = system.today().addDays(-25);
        //objCL.Registration_Date__c = system.today().addDays(-32);
        User u = createUser();
        System.runAs(u){
            insert objCL;
            system.debug('objCL test 1:: ' + [select recordType.name, DP_Assignment_Date_CC__c,For_Welcome_Call__c, Calling_List_Type__c,Calling_List_Status__c,IsHideFromUI__c,owner.Name,owner.profile.Name from Calling_List__c]);
            system.debug('objCL test 1:: ' + objCL);
            Test.startTest();
            Database.executeBatch(new UpdateDPCallingListBatch());
            Test.stopTest();
        }
    }
    
    @isTest
    public static void testDPCalling2() {
         //Activate trigger
        activateTrigger();
        //Create account
        Account objAccount = createAccount('India');

        //Create Service Request
        NSIBPM__Service_Request__c objDealSr = createDealSR();

        //Create Booking
        Booking__c objBooking = createBooking(objAccount.Id, objDealSr.Id);

        //Create Booking Unit
        Booking_Unit__c objBU = createBookingUnit(objBooking.Id, 'test hod');

        //create calling list
        Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        Id  OwnerId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName('Collection_New_Q','Queue');
        System.debug('devRecordTypeId ===' + devRecordTypeId);
        Calling_List__c objCL = createCallingList(objAccount.Id, objBU.Id);
        objCL.Nationality__c = 'Indian';
        objCL.RecordTypeId = devRecordTypeId;
        objCL.OwnerId = OwnerId;
        //objCL.For_Welcome_Call__c = true;
        User u = createUser();
        System.runAs(u){
            insert objCL;
            system.debug('objCL test 2:: ' + [select recordType.name,DP_Assignment_Date_CC__c, For_Welcome_Call__c, Calling_List_Type__c,Calling_List_Status__c,IsHideFromUI__c,owner.Name,owner.profile.Name from Calling_List__c]);
            system.debug('objCL test 2:: ' + objCL);
            Test.startTest();
            Database.executeBatch(new UpdateDPCallingListBatch());
            Test.stopTest();
        }
    }
    
    @isTest
    public static void testDPCalling3() {
         //Activate trigger
        activateTrigger();
        //Create account
        Account objAccount = createAccount('Haiti');

        //Create Service Request
        NSIBPM__Service_Request__c objDealSr = createDealSR();

        //Create Booking
        Booking__c objBooking = createBooking(objAccount.Id, objDealSr.Id);

        //Create Booking Unit
        Booking_Unit__c objBU = createBookingUnit(objBooking.Id, 'Rami Tabbara');
        objBU.DP_OK__c = true;
        //create calling list
        Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        Id  OwnerId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName('Collection_New_Q','Queue');
        System.debug('devRecordTypeId ===' + devRecordTypeId);
        Calling_List__c objCL = createCallingList(objAccount.Id, objBU.Id);
        objCL.Nationality__c = 'Indian';
        objCL.RecordTypeId = devRecordTypeId;
        objCL.OwnerId = OwnerId;
        //objCL.For_Welcome_Call__c = true;
        objCL.DP_OK__c = true;
        User u = createUser();
        System.runAs(u){
            insert objCL;
            system.debug('objCL test 3:: ' + [select recordType.name, DP_Assignment_Date_CC__c,For_Welcome_Call__c, Calling_List_Type__c,Calling_List_Status__c,IsHideFromUI__c,owner.Name,owner.profile.Name from Calling_List__c]);
            system.debug('objCL test 3:: ' + objCL);
            Test.startTest();
            Database.executeBatch(new UpdateDPCallingListBatch());
            Test.stopTest();
        }
    }
    
    @isTest
    public static void testDPCalling4() {
         //Activate trigger
        activateTrigger();
        //Create account
        Account objAccount = createAccount('Haiti');

        //Create Service Request
        NSIBPM__Service_Request__c objDealSr = createDealSR();

        //Create Booking
        Booking__c objBooking = createBooking(objAccount.Id, objDealSr.Id);

        //Create Booking Unit
        Booking_Unit__c objBU = createBookingUnit(objBooking.Id,'test Hod');

        //create calling list
        Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        Id  OwnerId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName('Collection_New_Q','Queue');
        System.debug('devRecordTypeId ===' + devRecordTypeId);
        Calling_List__c objCL = createCallingList(objAccount.Id, objBU.Id);
        objCL.Nationality__c = 'Indian';
        objCL.RecordTypeId = devRecordTypeId;
        objCL.OwnerId = OwnerId;
        //objCL.For_Welcome_Call__c = true;
        //objCL.DP_Due_Date__c = system.today().addDays(-25);
        //objCL.Registration_Date__c = system.today().addDays(-32);
        User u = createUser();
        System.runAs(u){
            insert objCL;
            system.debug('objCL test 4:: ' + [select recordType.name,DP_Assignment_Date_CC__c, For_Welcome_Call__c, Calling_List_Type__c,Calling_List_Status__c,IsHideFromUI__c,owner.Name,owner.profile.Name from Calling_List__c]);
            system.debug('objCL test 4:: ' + objCL);
            Test.startTest();
            Database.executeBatch(new UpdateDPCallingListBatch());
            Test.stopTest();
        }
        
    }
    static Account createAccount(String country) {
        Account accObj = new Account(FirstName = 'TestFirst', LastName = 'TestLast',Country__pc = country);
        insert accObj;
        return accObj;
    }

    Static NSIBPM__Service_Request__c createDealSR() {
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
        return objSR;
    }

    Static Booking__c createBooking(Id accId, Id SRId) {
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = accId;
        objBooking.Deal_SR__c = SRId;
        insert objBooking;
        return objBooking;

    }

    static Booking_Unit__c createBookingUnit(Id bookingId, String HodName) {
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = bookingId;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Hod_name__c = HodName;
        objBookingUnit.Booking_Type__c = 'NW';
        objBookingUnit.Registration_Status_Code__c = 'XT';
        insert objBookingUnit;
        System.debug('test objBookingUnit = ' + objBookingUnit);
        return objBookingUnit;
    }

    static Calling_List__c createCallingList(Id accId, Id BUId) {

        Id collectioncallingRecordTypeId =
            Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get(
            'Collections Calling List').RecordTypeId;
        System.debug('test FMCollectionRecordTypeId = ' + collectioncallingRecordTypeId);
        Calling_List__c callListObj = new Calling_List__c(Account__c = accId,
                                                        Booking_Unit__c = BUId,
                                                        recordTypeId = collectioncallingRecordTypeId,
                                                        Calling_List_Type__c = 'DP Calling',
                                                         DP_Due_Date__c = system.today().addDays(-30),
                                                         Registration_Date__c = system.today().addDays(-35),
                                                         Calling_List_Status__c = 'New'
                                                         );
        //insert callListObj;
        return callListObj;
    }
  Static void activateTrigger() {
        TriggerOnOffCustomSetting__c triggerSetting =
            new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                            OnOffCheck__c = true);
        insert triggerSetting;
    }
    
    static user createUser() {
      Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standardusertest@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusertest@testorg.com'); 
        insert u;
        return u;
    }
}