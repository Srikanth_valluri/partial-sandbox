public class AgencyContactListViewController {
    public transient List<Contact> agencyContacts {get; set;}
    public transient List<IndividualWrapper> individualAgencyContacts {get; set;}
    public Set<Id> myteamUsers = new Set<Id>();
    public List<SelectOption> agencyType {get; set;}
    public String selAgencyType {get; set;}
    private Map<String,IndividualWrapper> IndividualAgencies = new Map<String,IndividualWrapper>();

    public AgencyContactListViewController(ApexPages.StandardSetController controller) {
        agencyContacts = new List<Contact>();
        individualAgencyContacts = new List<IndividualWrapper>();
        agencyType = new List<SelectOption>();
        selAgencyType = Label.Agency_Type_Corporate;
        init();
    }

    public void init() {
        agencyContacts = new List<Contact>();
        individualAgencyContacts = new List<IndividualWrapper>();
        agencyType.add(new SelectOption(Label.Agency_Type_Corporate,Label.Agency_Type_Corporate));
        agencyType.add(new SelectOption(Label.Agency_Type_Individual,Label.Agency_Type_Individual));
        if(String.isNotBlank(selAgencyType)) {
            getAgencyContacts();
        }
    }

    public void getAgencyContacts() {
        if(selAgencyType.equals(Label.Agency_Type_Individual)) {
            getIndividualAgencyContacts();
        }
        else {
            getCorporateAgencyContacts();
        }
    }

    public void getCorporateAgencyContacts() {
        agencyContacts.clear();
        myteamUsers.add(UserInfo.getUserId());
        getChildUsers(new Set<Id>{UserInfo.getUserId()});
        if(!myteamUsers.isEmpty()) {
            for(Contact con : [SELECT
                                    Name,
                                    FirstName,
                                    LastName,
                                    Owner.Name,
                                    Owner__c,
                                    Authorised_Signatory__c,
                                    Portal_Administrator__c,
                                    Agent_Representative__c,
                                    Account.Name,
                                    Account.Agency_Type__c,
                                    Account.IsPersonAccount
                                FROM
                                    Contact
                                WHERE
                                    OwnerId IN : myteamUsers
                                    AND
                                    Account.Agency_Type__c = :selAgencyType
                                LIMIT 50000]) {
                agencyContacts.add(con);
            }
        }
    }

    public void getIndividualAgencyContacts() {
        individualAgencyContacts.clear();
        myteamUsers.add(UserInfo.getUserId());
        getChildUsers(new Set<Id>{UserInfo.getUserId()});
        if(!myteamUsers.isEmpty()) {
            for(Agency_PC__c pc : [SELECT
                                        id,
                                        Name,
                                        Agency__c,
                                        Contact__c,
                                        User__c,
                                        Contact__r.Name,
                                        Contact__r.OwnerId,
                                        Agency__r.Account_Owner__c,
                                        Agency__r.Agency_Type__c,
                                        Agency__r.Name
                                    FROM
                                        Agency_PC__c
                                    WHERE
                                        User__c IN :myteamUsers
                                        AND
                                        Agency__r.Agency_Type__c = :Label.Agency_Type_Individual
                                    LIMIT 40000]) {
                IndividualWrapper wrp = new IndividualWrapper();
                wrp.owner = pc.Name;
                wrp.contactName = pc.Agency__r.Name;
                wrp.agency = pc.Agency__r.Name;
                wrp.contactId = pc.Agency__c;
                individualAgencies.put(pc.Contact__c +'_'+pc.User__c,wrp);
                //individualAgencyContacts.add(wrp);
            }
            for(Contact con : [SELECT
                                    Name,
                                    FirstName,
                                    LastName,
                                    OwnerId,
                                    Owner.Name,
                                    Account.IsPersonAccount,
                                    Account.Name
                                FROM
                                    Contact
                                WHERE
                                    OwnerId IN : myteamUsers
                                    AND
                                    Account.Agency_Type__c = :Label.Agency_Type_Individual
                                LIMIT 10000]) {
                IndividualWrapper wrp = new IndividualWrapper();
                wrp.owner = con.Owner.Name;
                wrp.contactName = con.Name;
                wrp.agency = con.Account.Name;
                wrp.contactId = con.Id;
                //individualAgencyContacts.add(wrp);
                individualAgencies.put(con.Id +'_' + con.OwnerId,wrp);
            }
            individualAgencyContacts.addAll(individualAgencies.values());
        }
    }

    public void getChildUsers(Set<Id> teamUsers) {
        Set<Id> userIdSet = new Set<Id>();
        if (teamUsers.size() > 0) {
            for(User us : [SELECT Id, Name, ManagerId
                             FROM User
                            WHERE ManagerId IN :teamUsers]) {
                userIdSet.add(us.Id);
            }
            if(!userIdSet.isEmpty()) {
                myteamUsers.addAll(userIdSet);
                getChildUsers(userIdSet);
            }
        }
    }

    public class IndividualWrapper {
        public String owner {get; set;}
        public String contactName {get; set;}
        public String agency {get; set;}
        public String contactId {get; set;}

        public IndividualWrapper() {}
    }
}