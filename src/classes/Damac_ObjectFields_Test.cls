@isTest
public class Damac_ObjectFields_Test {
	static testmethod void myUnitTest() {
        Email_Request__c req = new Email_Request__c ();
        req.Template_for__c = 'Sales Offer Inquiry';
        req.Email_template__c = 'b70c2c19-b7d8-4eb4-acd1-e3f0883f438b';
        req.Substitution_tags__c = '-Name-';
        req.Related_Object_API__c = 'Inquiry__c';
        insert req;
        
        Email_Request_Rule_Criteria__c rule = new Email_Request_Rule_Criteria__c ();
        rule.Email_Request__c = req.id;
        insert rule;
        ApexPages.StandardController sc = new ApexPages.StandardController (rule);
        Damac_ObjectFieldsController obj = new Damac_ObjectFieldsController (sc);
        obj.updateRule ();
        obj.getFieldAPIs ();
        
    }
    static testmethod void myUnitTest2 () {
        Email_Request__c req = new Email_Request__c ();
        req.Template_for__c = 'Sales Offer Inquiry';
        req.Email_template__c = 'b70c2c19-b7d8-4eb4-acd1-e3f0883f438b';
        req.Substitution_tags__c = '-Name-';
        req.Related_Object_API__c = 'Inquiry__c';
        insert req;
        Email_Request_Rule_Criteria__c rule = new Email_Request_Rule_Criteria__c ();
       	apexpages.currentpage().getparameters().put('emailReqId', req.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController (rule);
        Damac_ObjectFieldsController obj = new Damac_ObjectFieldsController (sc);
        obj.updateRule ();
        obj.getFieldAPIs ();
        
    }
}