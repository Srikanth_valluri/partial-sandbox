public without sharing class AssignmentEndpoints {
    public static string fetchAssignmentDocs(Booking_Unit__c objUnit, Case objCase, String strProcess,
                                             Buyer__c objBuyer, Integer intValue){
        String strNocIssued = 'No';
        if(objUnit.NOC_Issued_Date__c != null
        && objUnit.NOC_Issued_Date__c > date.today().addDays(-30)){
            strNocIssued = 'Yes';
        }
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint objClass = new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint();
        objClass.timeout_x = 120000;
        system.debug('objUnit.Registration_ID__c=='+objUnit.Registration_ID__c);
        system.debug('objCase.Buyer_Type__c=='+objCase.Buyer_Type__c);
        system.debug('City=='+objUnit.Inventory__r.Property_City__c);
        system.debug('Prop Name=='+objUnit.Inventory__r.Property__r.Name);
        system.debug('bldg code=='+objUnit.Inventory__r.Building_Code__c);
        system.debug('objUnit.Permitted_Use__c=='+objUnit.Permitted_Use__c);
        system.debug('objUnit.Inventory__r.Bedroom_Type__c=='+objUnit.Inventory__r.Bedroom_Type__c);
        system.debug('objUnit.Unit_Type__c=='+objUnit.Unit_Type__c);
        system.debug('objCase.Buyer_Type__c=='+objCase.Buyer_Type__c);
        system.debug('objUnit.Inventory__r.Property_Status__c=='+objUnit.Inventory__r.Property_Status__c);
        String strSubProcess = '';
        String strApplicableNationality = '';
        String strAge = '';
        set<String> setNationalities = new set<String>();
        setNationalities.addAll(Label.GCCNationalities.split(','));
        if(objBuyer != null){
            system.debug('objBuyer.Date_of_Birth__c***************'+objBuyer.Date_of_Birth__c);
            if(String.isNotBlank(objBuyer.Date_of_Birth__c)){
                list<String> arr = objBuyer.Date_of_Birth__c.split('/');
                system.debug('***0**'+arr[0]);
                system.debug('***1**'+arr[1]);
                system.debug('***2**'+arr[2]);
                Date newdt = date.newinstance(integer.valueOf(arr[2]), integer.valueOf(arr[1]), integer.valueOf(arr[0]));
                system.debug('newdt*******'+newdt);
                strAge = checkBirthdate(newdt);
                system.debug('strAge*****'+strAge);
            }
        }

        system.debug('intValue *******'+intValue);
        if(intValue == 0){
            strSubProcess = objCase.Case_Type__c.equalsIgnoreCase('Succession') ? 'SUCCESSIONFLAGS' : 'NORMALFLAGS';
            strApplicableNationality = '';
        }else{
            strSubProcess = objCase.Buyer_Type__c;
            if(objBuyer.Nationality__c == 'UAE' && objBuyer.Country__c == 'United Arab Emirates'){
                strApplicableNationality = 'UAENATIONALUAERESIDENT';
            }
            else if(setNationalities.contains(objBuyer.Nationality__c) && objBuyer.Country__c == 'United Arab Emirates'){
                strApplicableNationality = 'GCCNATIONALUAERESIDENT';
            }
            else if(!setNationalities.contains(objBuyer.Nationality__c) && objBuyer.Country__c == 'United Arab Emirates'){
                strApplicableNationality = 'NONGCCNATIONALUAERESIDENT';
            }
            else if(objBuyer.Nationality__c == 'UAE' && objBuyer.Country__c != 'United Arab Emirates'){
                strApplicableNationality = 'UAENATIONALOUTSIDEUAE';
            }
            else if(setNationalities.contains(objBuyer.Nationality__c) && objBuyer.Country__c != 'United Arab Emirates'){
                strApplicableNationality = 'GCCNATIONALOUTSIDEUAE';
            }
            else if(!setNationalities.contains(objBuyer.Nationality__c) && objBuyer.Country__c != 'United Arab Emirates'){
                strApplicableNationality = 'NONGCCNATIONALOUTSIDEUAE';
            }
        }

        system.debug('strSubProcess**********'+strSubProcess);
        system.debug('strApplicableNationality**********'+strApplicableNationality);
        system.debug('strAge**********'+strAge);
        String strDocResponse = objClass.AssignmentDocument(objUnit.Registration_ID__c
                                                            ,''
                                                            ,strProcess
                                                            ,strSubProcess
                                                            ,objUnit.Inventory__r.Property_City__c
                                                            ,objUnit.Inventory__r.Property__r.Name
                                                            ,objUnit.Inventory__r.Building_Code__c
                                                            ,objUnit.Permitted_Use__c
                                                            ,objUnit.Inventory__r.Bedroom_Type__c
                                                            ,''
                                                            ,objUnit.Unit_Type__c
                                                            ,objCase.Buyer_Type__c
                                                            ,strAge
                                                            ,'No'
                                                            ,objUnit.Inventory__r.Property_Status__c
                                                            ,strApplicableNationality
                                                            ,objCase.Buyer_POA__c == true? 'Yes' : 'No'
                                                            ,objCase.Seller_POA__c == true? 'Yes' : 'No'
                                                            ,strNocIssued);
        system.debug('strDocResponse*****************'+strDocResponse);
        return strDocResponse;
    }

    public static string checkBirthdate(Date dtBirthdate)
    {
        String stAge = '';
        if(date.today().year() - dtBirthdate.year() > 21)
        {
            stAge = string.valueOf(date.today().year() - dtBirthdate.year());
        }
        else if(date.today().year() - dtBirthdate.year() == 21)
        {
            if(date.today().month() - dtBirthdate.month() < 0)
            {
                stAge = '20';
            }
            else if(date.today().month() - dtBirthdate.month() == 0)
            {
                if(date.today().day() - dtBirthdate.day() < 0)
                {
                    stAge = '20';
                }
                else
                {
                    stAge = '21';
                }
            }
            else if(date.today().month() - dtBirthdate.month() > 0)
            {
                stAge = '21';
            }
            else
            {
                stAge = '20';
            }
        }
        else
        {
            stAge = string.valueOf(date.today().year() - dtBirthdate.year());
        }
        return stAge;
    }//end of

    public static string fetchAssignmentFees(Booking_Unit__c objUnit, Case objCase, String strProcess){
        String strNocIssued = 'No';
        String strPOA = 'No';
        if(objUnit.NOC_Issued_Date__c != null
        && objUnit.NOC_Issued_Date__c > date.today().addDays(-30)){
            strNocIssued = 'Yes';
        }
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint objClass = new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint();
        objClass.timeout_x = 120000;
        system.debug('objUnit****'+objUnit);
        system.debug('objCase****'+objCase);
        system.debug('strPOA****'+strPOA);
        system.debug('strNocIssued****'+strNocIssued);
        String strFeeResponse = objClass.AssignmentFee(objUnit.Registration_ID__c
                                                       ,objCase.Origin
                                                       ,strProcess
                                                       ,objCase.Case_Type__c
                                                       ,objUnit.Inventory__r.Property_City__c
                                                       ,objUnit.Inventory__r.Property__r.Name
                                                       ,objUnit.Inventory__r.Building_Code__c
                                                       ,objUnit.Permitted_Use__c
                                                       ,objUnit.Inventory__r.Bedroom_Type__c
                                                       ,''
                                                       ,objUnit.Unit_Type__c
                                                       ,objCase.Buyer_Type__c
                                                       ,''
                                                       ,''
                                                       ,objUnit.Inventory__r.Property_Status__c
                                                       ,''
                                                       ,strPOA
                                                       ,strNocIssued);
        system.debug('strFeeResponse*****************'+strFeeResponse);
        return strFeeResponse;
    }

    public static string fetchAssignmentApprovers(Booking_Unit__c objUnit, Case objCase, String strProcess){
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint objClass = new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint();
        objClass.timeout_x = 120000;
        String strAppResponse = objClass.AssignmentApproval(objUnit.Registration_ID__c
                                                            ,''
                                                            ,strProcess
                                                            ,objCase.Case_Type__c+'-'+objCase.Relationship_with_Seller__c
                                                            ,objUnit.Inventory__r.Property_City__c
                                                            ,objUnit.Inventory__r.Property__r.Name
                                                            ,objUnit.Inventory__r.Building_Code__c
                                                            ,objUnit.Permitted_Use__c
                                                            ,objUnit.Inventory__r.Bedroom_Type__c
                                                            ,''
                                                            ,objUnit.Unit_Type__c
                                                            ,objCase.Buyer_Type__c
                                                            ,''
                                                            ,''
                                                            ,''
                                                            ,objUnit.Inventory__r.Property_Status__c
                                                            ,''
                                                            ,'');
        system.debug('strAppResponse*****************'+strAppResponse);
        return strAppResponse;
    }

    public static string fetchAssignmentDues(Booking_Unit__c objUnit) {
        return fetchAssignmentDues(objUnit.Registration_ID__c);
    }

    public static string fetchAssignmentDues(String registrationId) {
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint objClass = new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint();
        objClass.timeout_x = 120000;
        String strDueResponse = objClass.getPendingDues(registrationId,'');
        system.debug('strDueResponse*****************'+strDueResponse);
        return strDueResponse;
    }

    public static string fetchPDCDetails(Booking_Unit__c objUnit){
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint objClass = new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint();
        objClass.timeout_x = 120000;
        String strPDCResponse = objClass.getPDCDetails(String.valueOf(System.currentTimeMillis()), 'GET_PDC_DETAILS', 'SFDC', objUnit.Registration_ID__c);
        system.debug('strPDCResponse*****************'+strPDCResponse);
        return strPDCResponse;
    }

    public static string updateRegStatus(Booking_Unit__c objUnit, String regStatus){
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint objClass = new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint();
        objClass.timeout_x = 120000;
        String strRegStatusResponse = objClass.UpdateRegistrationStatus(String.valueOf(System.currentTimeMillis()), 'UPDATE_REG_STATUS', 'SFDC', objUnit.Registration_ID__c, regStatus);
        system.debug('strPDCResponse*****************'+strRegStatusResponse);
        return strRegStatusResponse;
    }
}