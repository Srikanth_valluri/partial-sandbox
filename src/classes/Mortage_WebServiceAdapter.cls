public class Mortage_WebServiceAdapter extends GenericWebServiceAdapter implements WebServiceAdapter{

    public String url; 
    
    public override WebServiceAdapter call(){
        
        this.url = Label.Mortage_Document_URL;
        return this;
    }
    
    public override String getResponse(){
        return this.url;
    }
}