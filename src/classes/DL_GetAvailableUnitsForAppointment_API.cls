/**********************************************************************************************************************
Description: This API is used for getting the list of booking units available for appointment booking for various purposes.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   24-11-2020      | Subin Antony		  | Created initial draft
***********************************************************************************************************************/

@RestResource(urlMapping='/appointment/fetchAvailableUnits')
global class DL_GetAvailableUnitsForAppointment_API {
    public static Map<Integer, String> statusCodeMap;
    static {
        statusCodeMap = new Map<Integer, String>{
            1 => 'Success',
            2 => 'Failure',
            3 => 'Bad Request',
            6 => 'Something went wrong',
            7 => 'Info'
        };
    }
	
    @HttpGet
    global static FinalReturnWrapper fetchAvailableUnitsForAppointment() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg, exceptionMsg, successMsg;
        
        String accountId = req.params.containskey('account_id') ? req.params.get('account_id') : '';
        if(String.isBlank(accountId)){
            errorMsg = 'Please provide account_id.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        Account accountRecord;
        try{
            accountRecord = [SELECT Id, Name, isPersonAccount, Email__pc, Email__c, 
                             Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c, Primary_Language__c 
                             FROM Account WHERE Id = :accountId];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
            accountRecord = NULL;
        }
        if(NULL == accountRecord){
            errorMsg = 'The account_id provided does not match any records.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        FinalReturnWrapper responseWrapper = new FinalReturnWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        
        List<Booking_Unit__c> lstBookingUnit;
        try{
        	lstBookingUnit = [Select Id, Name, Property_Name__c, Unit_Name__c, Registration_ID__c, Booking__c, Booking__r.Account__c, 
                          Inventory__r.Building_Location__r.Property_Name__r.Name, Inventory__r.Building_Location__r.Building_Name__c 
                          FROM Booking_Unit__c WHERE Booking__r.Account__c = :accountId 
                          AND Registration_ID__c != NULL AND Unit_Name__c != NULL];
        }
        catch(Exception ex) {
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
            lstBookingUnit = NULL;
        }
        if(NULL != lstBookingUnit && lstBookingUnit.size() > 0) {
            cls_data[] availableUnits = new List<cls_data>();
            for(Booking_Unit__c bu : lstBookingUnit) {
                cls_data unit = new cls_data();
                unit.unit_id = bu.id;
                unit.unique_unit_code = bu.Unit_Name__c;
                unit.reg_id = bu.Registration_ID__c;
                unit.building_name = bu.Inventory__r.Building_Location__r.Property_Name__r.Name;
                
                availableUnits.add(unit);
            }
            
            responseMetaData.status_code = 1;
            responseMetaData.title = statusCodeMap.get(1);
            responseMetaData.message = 'Successfully fetched booking units available for appointment booking';
            
        	responseWrapper.data = availableUnits;
        }
        else {
            responseMetaData.status_code = 2;
            responseMetaData.title = statusCodeMap.get(2);
            responseMetaData.message = 'Could not fetch any unit eligible for performing appointment booking';
            responseMetaData.developer_message =  exceptionMsg;
        }
        
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
        return responseWrapper;
    }
    
    private static FinalReturnWrapper getErrorResponse(Integer statusCode, String title, 
    String responseMessage, String devMessage) {
        FinalReturnWrapper responseWrapper = new FinalReturnWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = statusCode;
        responseMetaData.title = title;
        responseMetaData.message = responseMessage;
        responseMetaData.developer_message = devMessage;
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
        return responseWrapper;
    }
    
    global class FinalReturnWrapper {
        public cls_meta_data meta_data;
        public cls_data[] data;
    }
    
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }
    
    public class cls_data {
        public String unit_id;
        public String unique_unit_code;
        public String reg_id;
        public String building_name;
    }
}