/************************************************************************************************
 * @Name              : Damac_DailySalesReview_Test
 * @Description       : Test Class for Damac_DailySalesReview
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         QBurst         30/04/2020       Created
***********************************************************************************************/
@isTest
public class Damac_DailySalesReview_Test {

    @isTest
    static void test_method_1() {
        User dosUser;
        User hosUser;
        String rmId = '';
        for(User usr: [SELECT Id, Name, Manager.Id FROM User WHERE Profile.Name = 'Director of Sales' AND IsActive = TRUE LIMIT 5]){
            List<User> rmList = new List<User>();
            for(User rm: [SELECT Id FROM User WHERE IsActive = TRUE AND Manager.Id =: usr.Id]){
                rmList.add(rm);
                rmId = rm.Id;
            }
            if(rmList.size() > 0){
                dosUser = usr;
                break;
            }
        }
        dosUser.New_Team__c = 'Agents Team';
        update dosUser;
        hosUser = [SELECT Id FROM User WHERE Id =: dosUser.Manager.Id];
        Daily_Sales_Performance__c dsp = new Daily_Sales_Performance__c();
        dsp.User__c = rmId; 
        dsp.Count_of_Units_Pending_DP__c = 10;
        dsp.Report_Date__c = system.today();
        dsp.Units_Under_Reservation__c = 5;
        dsp.Value_of_Units_Pending_DP__c = 100.00;
        dsp.Value_Under_Reservation__c = 2000;  
        insert dsp;
        
        System.runAs(dosUser) {
            Test.startTest();
            PageReference pageRef = Page.Damac_DailySalesReview;
            Test.setCurrentPageReference(pageRef);
            System.currentPageReference().getParameters().put('newRecName', 'IQ-000111,IQ-000112,IQ-0001113');
            Test.setCurrentPage(pageRef);
            Damac_DailySalesReview ctlr = new Damac_DailySalesReview();
            ctlr.fetchDSR();
            ctlr.fetchDSRDetails();
            ctlr.createDSR();
            ctlr.fetchDSR();
            ctlr.fetchDSRDetails();
            String inpBoolean = '3_2_' + rmId + '_toggle_true;3_1_' + rmId + '+_toggle_true;';
            System.currentPageReference().getParameters().put('inputId', '3_4_' + rmId);
            System.currentPageReference().getParameters().put('boolUpdateSubmit', inpBoolean);
            System.currentPageReference().getParameters().put('boolUpdateSave', inpBoolean);
            System.currentPageReference().getParameters().put('boolUpdateClose', inpBoolean);
            System.currentPageReference().getParameters().put('boolUpdateSaveHOS', inpBoolean);
            System.currentPageReference().getParameters().put('recRemoveDetails', '3_4_' + rmId + '_IQ-000111');
            String remvString = 'removethis';
            List<String> stringList = new List<String>();
            stringList.add('one');
            stringList.add('two');
            stringList.add(remvString);
            ctlr.removeFromString(stringList, remvString);
            ctlr.updateRecord();
            ctlr.removeRecord();
            System.currentPageReference().getParameters().put('inputId', '3_4_HOS'); 
            System.currentPageReference().getParameters().put('recRemoveDetails', '8_3_HOS_IQ-000111');
            ctlr.updateRecord();
            ctlr.removeRecord();
            ctlr.saveDSR();
            ctlr.submitDSR();
            ctlr.saveHOSDSR();
            ctlr.closeDSR();
            ctlr.closeDSRByDOS();
            ctlr.saveDSRByHOD();
            ctlr.closeDSRByHOD();
            ctlr.fixRM();
            Damac_DailySalesReview.getRecordDetails('IQ-3', 'Inquiry__c');
            Test.stopTest();
        }
        System.runAs(hosUser) {
            Daily_Sales_Review__c dsr = [SELECT Id FROM Daily_Sales_Review__c LIMIT 1]; 
            PageReference pageRef = Page.Damac_DailySalesReview;
            Test.setCurrentPageReference(pageRef);
            System.currentPageReference().getParameters().put('id', dsr.Id);
            Damac_DailySalesReview ctlr = new Damac_DailySalesReview();
        }
    }
}