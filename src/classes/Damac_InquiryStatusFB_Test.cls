@IsTest
public class Damac_InquiryStatusFB_Test {
    static testMethod void method1 () {
        Inquiry__c newInquiry = new Inquiry__c();
        newInquiry = InitialiseTestData.getInquiryDetails(DAMAC_Constants.INQUIRY_RT,127);
        newInquiry.First_name__c = 'Test';
        newInquiry.Last_name__c = 'Lead';
        newInquiry.inquiry_source__c = 'Call Center';
        newInquiry.email__c = 'test@test.com';
        newInquiry.Inquiry_status__c = 'New';
        insert newInquiry ;
        
        newInquiry.Inquiry_status__c = 'Active';
        Update newInquiry;
        List<Inquiry__history> history = [SELECT ParentID FROM Inquiry__history 
                                            WHERE Field = 'Inquiry_Status__c' 
                                        ];
        System.debug(history );
        
        Damac_InquiryStatusWithFBSchedular obj1 = new Damac_InquiryStatusWithFBSchedular ();
        obj1.execute (null);
        
        Database.executeBatch (new Damac_InquiryStatusWithFB (), 1);
        
        Damac_Facebook_API__mdt details = new Damac_Facebook_API__mdt();
        Map <String, String> inquiryStatusesMap = new Map <String, String> ();
        for (Damac_Facebook_API__mdt detail : [SELECT Label, DeveloperName, FB_Lead_Status__c, Access_Token__c, API_URL__c, Offline_Event_Set_ID__c 
                                                    FROM Damac_Facebook_API__mdt 
                                                    WHERE DeveloperName != NULL]) {
            if (detail.developerName == 'Status_API')
                details = detail;
                
            inquiryStatusesMap.put (detail.Label, detail.FB_Lead_Status__c);
        }
        
        Damac_InquiryStatusWithFB obj = new Damac_InquiryStatusWithFB ();
        
        
        obj.sendRequest (inquiryStatusesMap, details, newInquiry, Integer.valueOf(String.valueOf(DateTime.Now().getTime()).subString (0, 10)));
        Damac_FBJSON jsonVal = Damac_FBJSON.parse ('{"test" :"test"}');
    }
}