/********************************************************************************************
*   Description : Callout to IPMS to get dues to update Outstanding amount on BU.           *
*-------------------------------------------------------------------------------------------*
*   Revision History:                                                                       *
*   Version Author              Date            Description                                 *
*   1.0     Aishwarya Todkar    03/03/2019      Initial Draft                               *
*   1.1     Arjun Khatri        07/03/2019      1.Changed callout method                    *
*                                               "GET_INV_DUE_LIST" to "GET_INV_INFO_LIST"   *
*   1.2     Arjun Khatri        13/03/2019      1.Removed filter criteria                   *
*********************************************************************************************/
global class FMServiceChargesCalloutBatch implements Database.Batchable<sObject>
                                                    , Database.Stateful
                                                    , Database.AllowsCallouts {

    global Database.QueryLocator start(Database.BatchableContext BC){
        String strQuery = 'SELECT Id, Registration_ID__c FROM Booking_Unit__c ';
                strQuery = strQuery + ' WHERE Registration_ID__c != NULL AND Unit_Active__c = \'Active\' ';
                            
                    if( Label.FM_Outstanding_Amount_Filter_On_Off.equalsIgnoreCase('On') ) {
                        strQuery = strQuery + 'AND Handover_Flag__c = \'Y\' ';
                        strQuery = strQuery + 'AND SC_From_RentalIncome__c = False AND CM_Units__c = NULL ';
                    }
                    strQuery = strQuery + ' ORDER BY Registration_ID__c ';
        System.debug('-->> strQuery == ' + strQuery);
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<Booking_Unit__c > listBU){
        System.debug('-->> FMServiceChargesCalloutBatch listBU === ' +  listBU);
        List<Booking_Unit__c> lstBuToBeIpdate = new List<Booking_Unit__c>();
        List<Error_log__c> lstErrorLogs = new  List<Error_log__c>();
        for(Booking_Unit__c objBu : listBU) {
            Decimal decDPSOABalance = 0.0;
            Decimal decFMSOABalance = 0.0;
            if(String.isNotBlank(objBu.Id)) {
                String strDueResponse = assignmentEndpoints.fetchAssignmentDues(objBu);
                system.debug('-->> strDueResponse==='+strDueResponse);
                if(String.isNotBlank(strDueResponse)){
                    map<String,Object> mapDeserializeDue = (map<String,Object>)JSON.deserializeUntyped(strDueResponse);
                    system.debug('-->> mapDeserializeDue==='+mapDeserializeDue);
                    if(mapDeserializeDue.get('status') == 'S'){
                        strDueResponse = strDueResponse.remove('{');
                        strDueResponse = strDueResponse.remove('}');
                        strDueResponse = strDueResponse.remove('"');
                        system.debug('-->> after all replacements******'+strDueResponse);
                        for(String st : strDueResponse.split(',')){
                            String strKey = st.substringBefore(':').trim();
                            system.debug('-->> *****strKey*****'+strKey);
                            if(!strKey.equalsIgnoreCase('Status')) {
                                system.debug('-->> st*****'+st);
                                Decimal calAmount = 0.0;

                                // Added on 18th sept 2018 : to store DP soa Balance and FM SOA balance
                                    Decimal dpSOABalance = 0.0;
                                    Decimal fmSOABalance = 0.0;
                                //----------------------------------------------------------------------

                                if(Decimal.valueOf(st.subStringAfter(':').trim()) != null) {
                                    system.debug('*****INSIDE*****');
                                    calAmount = Decimal.valueOf(st.subStringAfter(':').trim());

                                    // Added on 18th sept 2018 : to store DP soa Balance and FM SOA balance
                                    if( strKey.equalsIgnoreCase('Balance as per SOA') ) {
                                        dpSOABalance = Decimal.valueOf(st.subStringAfter(':').trim());
                                    }
                                    if( strKey.equalsIgnoreCase('FM Balance as per SOA') ) {
                                        fmSOABalance = Decimal.valueOf(st.subStringAfter(':').trim());
                                    }
                                    //----------------------------------------------------------------------
                                    system.debug('-->> fmSOABalance==='+fmSOABalance);
                                }
                                else {
                                    calAmount = 0.0;

                                    // Added on 18th sept 2018 : to store DP soa Balance and FM SOA balance
                                    dpSOABalance = 0.0;
                                    fmSOABalance = 0.0;
                                    //----------------------------------------------------------------------
                                }

                                // Added on 18th sept 2018 : to store DP soa Balance and FM SOA balance
                                decDPSOABalance = decDPSOABalance + dpSOABalance;
                                decFMSOABalance = decFMSOABalance + fmSOABalance;
                                system.debug('-->> decFMSOABalance==='+decFMSOABalance);

                            }
                        }

                    }
                    else if(mapDeserializeDue.get('status') == 'E' && mapDeserializeDue.containsKey('message')){
                        Error_Log__c objErr = new Error_Log__c();
                        objErr.Booking_Unit__c = objBu.Id;
                        objErr.Process_Name__c = 'Assignment';
                        objErr.Error_Details__c = 'Error : '+mapDeserializeDue.get('message');
                        insert objErr;
                    }
                } else {
                    Error_Log__c objErr = new Error_Log__c();
                    objErr.Booking_Unit__c = objBu.Id;
                    objErr.Error_Details__c = 'Error : No Response from IPMS for Payment Dues';
                    insert objErr;
                }
                /*FmIpmsRestServices.DueInvoicesResult dues;
                try {
                    dues = FmIpmsRestServices.getDueInvoices(objBu.Registration_ID__c,'', '');
                    System.debug('dues=' + dues);
                } catch(Exception excp) {
                    System.debug('excp = ' + excp);

                    //Create error log
                    lstErrorLogs.add(new Error_log__c(Error_Details__c = excp.getMessage(), Booking_Unit__c = objBu.Id));
                }

                if(dues != null ) {
                    if( String.isNotBlank(dues.totalDueAmount) ) {
                        objBu.FM_Outstanding_Amount__c = dues.totalDueAmount;
                    }else {
                        objBu.FM_Outstanding_Amount__c = '0';
                    }
                    lstBuToBeIpdate.add(objBu);
                }*/

                if(decFMSOABalance != null ) {
                    objBu.FM_Outstanding_Amount__c = decFMSOABalance.toPlainString();
                    objBu.Total_Payment_Amount__c = decFMSOABalance.toPlainString();
                    system.debug('-->> objBu==='+objBu);
                    lstBuToBeIpdate.add(objBu);
                }
                else {
                    //Create error log
                    lstErrorLogs.add(new Error_log__c(Error_Details__c = 'Dues Not Recieved.', Booking_Unit__c = objBu.Id));
                }
            }
        }

        System.debug('== lstBuToBeIpdate == ' + lstBuToBeIpdate);

        if(lstBuToBeIpdate.size() > 0 ) {
            update lstBuToBeIpdate;
        }
        if(lstErrorLogs.size() > 0) {
            insert lstErrorLogs;
        }

    }

    global void finish(Database.BatchableContext BC) {
    System.debug('== Finish == ' );

    }
}