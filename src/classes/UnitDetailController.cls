/**
 * @File Name          : UnitDetailController.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 7/17/2019, 2:48:59 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    7/17/2019, 2:43:29 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
global without sharing class UnitDetailController {

    private static String customerAccountId = CustomerCommunityUtils.customerAccountId;
    private static String SITE_PATH_PREFIX =  Site.getPathPrefix();
    private static final String PORTAL_DOCUMENT_TYPE = 'Document not required';
    private static final String SPA_DOCUMENT = 'SPA';
    private static final String AGRMNT1_DOCUMENT = 'AGRMNT 1';
    private static final String AGRMNT2_DOCUMENT = 'AGRMNT 2';
    public list<PaymentGateway__c> lstCustmSettngs {get; set;}
    public list<Case> caseLst{get; set;}
    public List<SR_Attachments__c> lstDoc {get; set;}
    public list<BookingUnitDocs>  listWrapper{get;set;}
    public Booking_Unit__c                              unit                    {get; set;}
    public Boolean                                      nocIssuedInLast15Days   {get; set;}
    public String                                       buyersFilter            {get; set;}
    public String                                       srFilter                {get; set;}
    public String                                       soaUrl                  {get; set;}
    public String                                       unitId                  {get; set;}
    public static String paymentCharges;
    public List<Payment_Charges__mdt>                   lstCharges              {get; set;}

    public UnitDetailController() {
        if (CustomerCommunityUtils.isCurrentView('unitdetail')) {
            lstCustmSettngs = new list<PaymentGateway__c>();
            lstCustmSettngs = [SELECT AccessCode__c, EncryptionKey__c, MerchantId__c,Url__c
                                  FROM PaymentGateway__c
                                  WHERE Name = 'Customer Portal'];
            lstCharges = new List<Payment_Charges__mdt>();
            initializeVariables();
        }
    }

    public void initializeVariables() {
        PageReference currentPage = ApexPages.currentpage();
        System.debug('currentPage = ' + currentPage);
        if (currentPage != NULL) {
            unitId = currentPage.getParameters().get('id');
        }
        System.debug('unitId = ' + unitId);
        listWrapper = new List<BookingUnitDocs>();
        nocIssuedInLast15Days = false;
        srFilter = ' WHERE Booking_Unit__c = ' + (unitId == NULL ? ' NULL ' : '\'' + unitId + '\'') + ' ';
        List<Booking_Unit__c> lstUnit = [
            SELECT  Id,
                    Registration_ID__c,
                    Name,
                    Unit_Name__c,Inventory__c,
                    Inventory__r.Building_Location__c,
                    Registration_Status__c,
                    Booking__c,
                    Area_Varied__c,
                    NOC_Issued_Date__c,
                    CurrencyIsoCode,
                    Bedroom_Type__c,
                    Unit_Type__c,
                    Permitted_Use__c,
                    Total_Collections__c,
                    Total_due_Overdue__c,
                    Agreement_Date__c,
                    Unit_Plan__c,
                    Floor_Plan__c,
                    Area_sft__c,
                    Requested_Price__c,
                    Construction_Status__c,
                    Anticipated_Completion_Date__c,
                    Handover_Flag__c,
                    Early_Handover__c,
                    (Select Id, Name, Attachment_URL__c,Portal_Document_Type__c
                    From Case_Attachments__r
                    Where Attachment_URL__c != NULL
                    AND Portal_Document_Type__c != :PORTAL_DOCUMENT_TYPE)
            FROM    Booking_Unit__c
            WHERE   Id = :unitId
        ];
        if (!lstUnit.isEmpty()) {
            unit = lstUnit[0];
            caseLst = new List<Case>();
            caseLst = [Select Status, Id
                       from Case
                       where Booking_Unit__c =:unitId
                       AND RecordType.Name IN ('Handover Parent', 'Handover')
                       AND (Status = 'New' OR Status = 'Submitted' OR Status = 'Draft Request')];
            system.debug('>>>caseLst '+caseLst);
            buyersFilter = ' WHERE Booking__c = '
                            + (unit.Booking__c == NULL ? ' NULL ' : '\'' + unit.Booking__c + '\'')
                            + ' AND Primary_Buyer__c = FALSE '
                            + ' AND Account__c '
                            + (String.isBlank(customerAccountId) ? ' = NULL' : (' != \'' + customerAccountId + '\''));
            if (unit.NOC_Issued_Date__c != NULL && unit.NOC_Issued_Date__c > date.today().addDays(-30)) {
                nocIssuedInLast15Days = true;
            }

            if(!lstUnit[0].Case_Attachments__r.isEmpty()){
                lstDoc = lstUnit[0].Case_Attachments__r;
            }
            if(lstDoc != NULL){
                BookingUnitDocs wrapperObj = new BookingUnitDocs();
                for(SR_Attachments__c docs : lstDoc){
                    if(docs.Portal_Document_Type__c == SPA_DOCUMENT){
                        wrapperObj.SPA = docs.Attachment_URL__c;
                    }
                    else if(docs.Portal_Document_Type__c == AGRMNT1_DOCUMENT){
                        wrapperObj.AGRMNT1 = docs.Attachment_URL__c;
                    }
                    else if(docs.Portal_Document_Type__c == AGRMNT2_DOCUMENT){
                        wrapperObj.AGRMNT2 = docs.Attachment_URL__c;
                    }

                    listWrapper.add(wrapperObj);
                }
            }
            system.debug('>>>>listWrapper'+listWrapper);
        }

        lstCharges = [SELECT id
                             , Order__c
						     , Maximum_Value__c
						     , Minimum_Value__c
						     , Charges__c
					  FROM Payment_Charges__mdt
					  ORDER BY Order__c
                    ];
        System.debug('>>>---lstCharges--- : ' + lstCharges);
    }

    @RemoteAction
    public static String chargesDetails(String amountToPay) {
        System.debug('---Get Charges---');
        System.debug('>>>----amountToPay---- : '+amountToPay);
        Decimal amount = Decimal.valueOf(amountToPay);
        List<Payment_Charges__mdt> lstPaymentCharges = new List<Payment_Charges__mdt>();
        lstPaymentCharges = [ SELECT id
		                          , Maximum_Value__c
		                          , Minimum_Value__c
		                          , Charges__c
		                   FROM Payment_Charges__mdt
		                ];
	    System.debug('>>>---lstPaymentCharges--- :'+lstPaymentCharges);
	    for(Payment_Charges__mdt obj : lstPaymentCharges) {
	        if(Decimal.valueOf(obj.Maximum_Value__c.replace(',','')) >= amount && Decimal.valueOf(obj.Minimum_Value__c.replace(',','')) <= amount ) {
	            paymentCharges = obj.Charges__c.replace(',','');
	        }
	    }
        System.debug('>>>---paymentCharges--- : '+paymentCharges);
        return paymentCharges;
    }

    @RemoteAction
    public static UnitDetailsService.BookinUnitDetailsWrapper getUnitDetailsFromIPMS(String registrationId) {
        UnitDetailsService.BookinUnitDetailsWrapper wrapper = UnitDetailsService.getBookingUnitDetails(registrationId);
        System.debug('wrapper = ' + JSON.serialize(wrapper));
        return wrapper;
    }

    @RemoteAction
    public static List<String> getStatementOfAccount(String registrationId, String soaType) {
        if (String.isBlank(registrationId) || String.isBlank(soaType)) {
            return null;
        }
        //FmIpmsRestCoffeeServices.getAllBulkSoaInLanguageUrl()
        List<String> returnResult = new List<String>();
        String responseString;
        if ('SOA'.equalsIgnoreCase(soaType)) {
            try {
                List<String> listsoaUrl_Temp = new List<String>();
                listsoaUrl_Temp = FmIpmsRestCoffeeServices.getAllUnitSoaInLanguageUrl(registrationId);
                for(String objUrl: listsoaUrl_Temp){
                    System.debug('-->> objUrl : ' + objUrl);
                    String strLangCode = FmIpmsRestCoffeeServices.urlLanguageCodeMap.get(objUrl);
                    System.debug('-->> strLangCode : ' + strLangCode);
                    String strLangName = FmIpmsRestCoffeeServices.languageCodeMap.get(strLangCode);
                    System.debug('-->> strLangName : ' + strLangName);
                    returnResult.add(objUrl + '-'+ strLangName);
                }    
                return returnResult;
            } catch(Exception e) {
                System.debug('-->> Error in try block: ' + e.getMessage());
                return null;
            }
            //return GenerateSOAController.getSOADocument(registrationId).url;
        } else if ('FM'.equalsIgnoreCase(soaType)) {
            responseString = GenerateDGMSOAController.getSOADocument(registrationId).url;
            returnResult.add(responseString);
            return returnResult;
            //return GenerateDGMSOAController.getSOADocument(registrationId).url;
        } else if ('Penalty'.equalsIgnoreCase(soaType)) {
            responseString = PenaltyWaiverService.getSOPDocument(registrationId).url;
            returnResult.add(responseString);
            return returnResult;
            //return PenaltyWaiverService.getSOPDocument(registrationId).url;
        } else {
            return null;
        }
    }

    webservice static string doPay(ID buID, Decimal amt, String url, Decimal adminFee, Decimal outstanding){
        //ID accntID,
        if(buID != null && amt != null){
            /*for(Receipt__c r : [select id,name from Receipt__c where Booking_Unit__c =: buID]){
                return 'Receipt already generated';
            }*/
            string message = initiateTransaction(buID, amt, url, adminFee, outstanding);
            System.debug('Final URL = '
                + 'https://uat.timesofmoney.com/direcpay/secure/PaymentTransactionServlet?requestParameter='
                + message
            );
            return message;
        }
        return null;
    }

    webservice static String Paymentformsubmit(string buID, Decimal amt, String url, Decimal adminFee) {
        try {

            Booking_Unit__c unit = [
                SELECT Id, Name, Unit_Name__c, Booking__c, Booking__r.Deal_SR__c
                FROM Booking_Unit__c
                WHERE Id = :buID
            ];

            //Create a receipt record with values entered by the customer
            Receipt__c R = new Receipt__c();
            //R.Customer__c=accId;
            R.Amount__c=amt;
            R.Receipt_Type__c='Card';
            R.Transaction_Date__c=system.now();
            R.Booking_Unit__c = buID != null ? buID : null;
            Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Card').getRecordTypeId();
            R.RecordTypeId=rt;
            R.Administration_Fee__c = adminFee;
            insert R;

            Receipt__c newR;

            //Query the newly created receipt record
            newR =[select id,name,Payer__c,Amount__c,Booking_Unit__c,Booking_Unit__r.Booking__c from Receipt__c where id=:R.id];
            system.debug('--check->'+newR.Payer__c+'---->'+newR.Booking_Unit__r.Booking__c+'-----'+newR.Booking_Unit__c);
            system.debug('--Name->'+newR.name);
            String payertype='Self';
            if(newR.Payer__c=='Self')
                payertype='Self';
            else
                payertype='Third Party';

            //get the Payer details
            //Buyer__c Payer =[select id,
            //                        First_Name__c,
            //                        Last_Name__c,
            //                        Email__c,
            //                        Address_Line_1__c,
            //                        City__c,
            //                        Country__c,
            //                        Booking__r.Deal_SR__c
            //                from    Buyer__c
            //                where   Payer__c= :payertype
            //                    and Booking__c= :newR.Booking_Unit__r.Booking__c
            //                LIMIT   1];
            Account Payer = [SELECT Id,
                                    Name,
                                    FirstName,
                                    LastName,
                                    Email__pc,
                                    Address_Line_1__pc,
                                    City__pc,
                                    Country__pc
                            FROM    Account
                            WHERE   Id = :CustomerCommunityUtils.customerAccountId
                            LIMIT   1];

            System.debug('Payer = ' + JSON.serializePretty(Payer));

            //Getting the country code
            String payercountry=DamacUtility.getCountryCode(Payer.Country__pc);

            //Setting the default state and PO values
            Map<String,String> StateMap = new Map<String,String>();
            Map<String,String> POMap = new Map<String,String>();
            for(NI_State_PO_values__mdt StatePOval :[select id,DeveloperName,State__c,PO__c from NI_State_PO_values__mdt]){
                StateMap.put(StatePOval.DeveloperName,StatePOval.State__c);
                POMap.put(StatePOval.DeveloperName,StatePOval.PO__c);
            }

            string cc,state,PO='';
            if(payercountry=='US'||payercountry=='CA'){
                cc=payercountry;
            } else {
                cc='Others';
            }

            state=StateMap.get(cc);
            PO = POMap.get(cc);

            if(payertype=='Third Party'){
                NSIBPM__Document_Master__c DM = [select id,NSIBPM__Code__c from NSIBPM__Document_Master__c where NSIBPM__Code__c ='Third party consent'];

                NSIBPM__SR_Doc__c SRDoc = new NSIBPM__SR_Doc__c();
                SRDoc.NSIBPM__Service_Request__c = unit.Booking__r.Deal_SR__c;
                SRDoc.Booking_Unit__c=newR.Booking_Unit__c;
                SRDoc.NSIBPM__Generate_Document__c=true;
                SRDoc.NSIBPM__Document_Master__c=DM.id;
                SRDoc.name='Third Party Consent';
                SRDoc.NSIBPM__Status__c='Generated';
                insert SRDoc;

            }

            /********** Create a pipe separated string of all fields to be sent in the request  **********************/
            //String message=newR.name+'|AED|'+amt+'|'+Label.Site_Domain+'/PaymentStatusUpdate|'+Label.Site_Domain+'/PaymentStatusUpdate|01|INTERNET|||||||Soloman|Vandy|Abhinav Nagar|Borivali|New York|NY|100255|US|merchant@test.com|34|344|34355344|3453453523|||John|Lenon|Phoenix Mill|Lower Parel|Mumbai|NY|123123|US|45|455|4534534|1321223122||test||1|Book|FALSE|aaa|bbb|ccc|ddd|eee|';
            //String message=newR.name+'|AED|'+newR.Amount__c+'|'+Label.Site_Domain+'/PaymentStatusUpdate|'+Label.Site_Domain+'/PaymentStatusUpdate|01|INTERNET|||||||||||||||||||||||||||||||||||||||||||||';

            //System.debug('Site.getBaseSecureUrl() = ' + Site.getBaseSecureUrl());

            Id ntwrkId = Network.getNetworkId();
            //System.debug('ntwrkId = ' + ntwrkId);

            String message = newR.name
                        + '|'   // MerchantOrderNumber
                        + 'AED'
                        + '|'   //Currency
                        + (newR.Amount__c == NULL ? 0 : newR.Amount__c)
                        + '|'   //Amount
                        + url + '/CustomerPaymentStatusUpdate'
                        + '|'   //SuccessURL
                        + url + '/CustomerPaymentStatusUpdate'
                        + '|'   // FailureURL
                        + '01'
                        + '|'   //TransactionType
                        + 'INTERNET'
                        + '|'   //TransactionMode
                        + '|'   //PayModeType
                        + '|'   //CreditCardNumber
                        + '|'   //CVV
                        + '|'   //ExpiryMonth
                        + '|'   //ExpiryYear
                        + '|'   //CardType
                        + (String.isBlank(Payer.FirstName) ? '' : Payer.FirstName)
                        + '|'   //BillToFirstName
                        + (String.isBlank(Payer.LastName) ? '' : Payer.LastName)
                        + '|'   //BillToLastName
                        + (String.isBlank(Payer.Address_Line_1__pc) ? '' : Payer.Address_Line_1__pc)
                        + '|'   //BillToStreet1
                        + '|'   //BillToStreet2
                        + (String.isBlank(Payer.City__pc) ? '' : Payer.City__pc)
                        + '|'   //BillToCity
                        + (String.isBlank(state) ? '' : state)
                        + '|'   //BillToState
                        + (String.isBlank(PO) ? '' : PO)
                        + '|'   //BillToPostalCode
                        + (String.isBlank(payercountry) ? '' : payercountry)
                        + '|'   //BillToCountry
                        + (String.isBlank(Payer.Email__pc) ? '' : Payer.Email__pc)
                        + '|'   //BillToEmail
                        + '|'   //BillToPhoneNumber1
                        + '|'   //BillToPhoneNumber2
                        + '|'   //BillToPhoneNumber3
                        + '|'   //BillToMobileNumber
                        + '|'   //GatewayID
                        + '|'   //CustomerID
                        + '|'   //ShipToFirstName
                        + '|'   //ShipToLastName
                        + '|'   //ShipToStreet1
                        + '|'   //ShipToStreet2
                        + '|'   //ShipToCity
                        + '|'   //ShipToState
                        + '|'   //ShipToPostalCode
                        + '|'   //ShipToCountry
                        + '|'   //ShipToPhoneNumber1
                        + '|'   //ShipToPhoneNumber2
                        + '|'   //ShipToPhoneNumber3
                        + '|'   //ShipToMobileNumber
                        + '|'   //TransactionSource
                        + unit.Unit_Name__c
                        + '|'   //ProductInfo
                        + '|'   //IsUserLoggedIn
                        + '|'   //ItemTotal
                        + '|'   //ItemCategory
                        + '|'   //IgnoreValidationResult
                        + 'Overdue Payment'
                        + '|'   //udf1
                        + '|'   //Udf2
                        + '|'   //Udf3
                        + '|'   //Udf4
                        + '|'   //Udf5
                        ;

            system.debug('RRReqqMessage===>'+message);

            //Get the encryption key from custom setting
            String keyval= Label.Encryption_key;
            Blob cryptoKey = EncodingUtil.base64Decode(keyval);

            //Get the initialization vector from custom setting
            Blob iv = Blob.valueof('0123456789abcdef');

            //Convert the request string to Blob
            Blob data = Blob.valueOf(message);

            //Encrypt the data using Salesforce Crypto class
            Blob encryptedData = Crypto.encrypt('AES256', cryptoKey,iv, data);
            system.debug('EEEE=>'+encryptedData) ;

            // Convert the encrypted data to string
            String encryptedMessage  = EncodingUtil.base64Encode(encryptedData);
            System.debug('EEEESSSS=>'+encryptedMessage);

            string decryptedMerchantID =  test.isrunningTest()==false ? Label.Merchant_ID :'123456'; // V1.0


            //Append the encrypted string to the MerchantID
            String req= decryptedMerchantID + '|' + encryptedMessage + '|';
            system.debug('RRRRR===>'+req);
            return req;
        }
        catch(Exception e){
            System.debug('>>>>>>>>>>>>>>'+e.getLineNumber() +'--->'+e.getmessage());
            return e.getMessage()+'-'+e.getLineNumber();
        }
    }

    webservice static String initiateTransaction(string buID, Decimal amt, String url, Decimal adminFee, Decimal outstanding) {
        try {
            Booking_Unit__c unit = [
                SELECT Id, Name, Unit_Name__c, Booking__c, Booking__r.Deal_SR__c
                FROM Booking_Unit__c
                WHERE Id = :buID
            ];
            Receipt__c R = new Receipt__c();
            R.Amount__c=amt;
            R.Receipt_Type__c='Card';
            R.Transaction_Date__c=system.now();
            R.Booking_Unit__c = buID != null ? buID : null;
            Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Card').getRecordTypeId();
            R.RecordTypeId=rt;
            R.Customer__c = CustomerCommunityUtils.customerAccountId;
            R.Origin__c = 'Customer Portal';
            R.Administration_Fee__c = adminFee;
            R.Outstanding_Amount__c = outstanding;
            insert R;

            Receipt__c newR;

            //Query the newly created receipt record
            newR =[select id,name,Payer__c,Amount__c,Booking_Unit__c,Booking_Unit__r.Booking__c from Receipt__c where id=:R.id];
            system.debug('--check->'+newR.Payer__c+'---->'+newR.Booking_Unit__r.Booking__c+'-----'+newR.Booking_Unit__c);
            system.debug('--Name->'+newR.name);
            String payertype='Self';
            if(newR.Payer__c=='Self')
                payertype='Self';
            else
                payertype='Third Party';
            Account Payer = [SELECT Id,
                                    Name,
                                    FirstName,
                                    LastName,
                                    Email__pc,
                                    Address_Line_1__pc,
                                    City__pc,
                                    Country__pc
                            FROM    Account
                            WHERE   Id = :CustomerCommunityUtils.customerAccountId
                            LIMIT   1];

            System.debug('Payer = ' + JSON.serializePretty(Payer));

            //Getting the country code
            String payercountry=DamacUtility.getCountryCode(Payer.Country__pc);

            //Setting the default state and PO values
            Map<String,String> StateMap = new Map<String,String>();
            Map<String,String> POMap = new Map<String,String>();
            for(NI_State_PO_values__mdt StatePOval :[select id,DeveloperName,State__c,PO__c from NI_State_PO_values__mdt]){
                StateMap.put(StatePOval.DeveloperName,StatePOval.State__c);
                POMap.put(StatePOval.DeveloperName,StatePOval.PO__c);
            }

            string cc,state,PO='';
            if(payercountry=='US'||payercountry=='CA'){
                cc=payercountry;
            } else {
                cc='Others';
            }

            state=StateMap.get(cc);
            PO = POMap.get(cc);
            if(payertype=='Third Party'){
                NSIBPM__Document_Master__c DM = [select id,NSIBPM__Code__c from NSIBPM__Document_Master__c where NSIBPM__Code__c ='Third party consent'];

                NSIBPM__SR_Doc__c SRDoc = new NSIBPM__SR_Doc__c();
                SRDoc.NSIBPM__Service_Request__c = unit.Booking__r.Deal_SR__c;
                SRDoc.Booking_Unit__c=newR.Booking_Unit__c;
                SRDoc.NSIBPM__Generate_Document__c=true;
                SRDoc.NSIBPM__Document_Master__c=DM.id;
                SRDoc.name='Third Party Consent';
                SRDoc.NSIBPM__Status__c='Generated';
                insert SRDoc;
            }
            Id ntwrkId = Network.getNetworkId();
            list<PaymentGateway__c> lstCustmSettng = new list<PaymentGateway__c>();
            lstCustmSettng = [SELECT AccessCode__c, EncryptionKey__c, MerchantId__c,Url__c
                              FROM PaymentGateway__c
                              WHERE Name = 'Customer Portal'];
            system.debug('>>>>lstCustmSettng>>>>'+lstCustmSettng);
            String encRequest = CCAvenuePaymentGateway.getInstance('Customer Portal').initiateTransaction(
                newR.name + '/' + unit.Unit_Name__c ,
                (newR.Amount__c == NULL ? 0 : newR.Amount__c),
                url + '/' + 'CustomerPaymentStatusUpdate',
               (String.isBlank(Payer.FirstName) ? '' : Payer.FirstName),
               (String.isBlank(Payer.Address_Line_1__pc) ? '' : Payer.Address_Line_1__pc),
               (String.isBlank(Payer.City__pc) ? '' : Payer.City__pc),
               (String.isBlank(state) ? '' : state),
               (String.isBlank(PO) ? '' : PO),
               (String.isBlank(payercountry) ? '' : payercountry)
            );
            return encRequest;
        }
        catch(Exception e){
            System.debug('>>>>>>>>>>>>>>'+e.getLineNumber() +'--->'+e.getmessage());
            return e.getMessage()+'-'+e.getLineNumber();
        }
    }
    public class BookingUnitDocs{
        public String SPA {get;set;}
        public String AGRMNT1 {get;set;}
        public String AGRMNT2 {get;set;}
    }
}