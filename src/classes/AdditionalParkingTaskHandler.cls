public with sharing class AdditionalParkingTaskHandler 
{
  
  public static void createTaskForArchival(Task taskInst)
  { 
    Set<Id> taskIdSet = new Set<Id>();
    Set<Id> caseIdSet = new Set<Id>();
    if(taskInst != null )
    { 
      taskIdSet.add(taskInst.Id);
      caseIdSet.add(taskInst.WhatId);
      if(!taskIdSet.isEmpty() && !caseIdSet.isEmpty())
      { 
        system.debug('createTaskForArchival called');
        createTaskOnIPMS(taskIdSet,caseIdSet);
      }
    }
  }

  @future(callout=true)
  public static void createTaskOnIPMS(set<Id> taskIdSet, set<Id> caseIdSet) {
      list<Error_Log__c> listErrorLog = new list<Error_Log__c>();
      map<String, Object> mapDeserializeTask = new map<String, Object>(); 

      system.debug('Task cfreation called********************');
      map<Id, Case> mapCaseDetails = new map<Id, Case>([select Id, Status, Buyer__r.Party_ID__c,
                          Booking_Unit__r.Registration_ID__c, CaseNumber, Relationship_with_Seller__c,
                          Case_Type__c, Buyer_Type__c, Booking_Unit__r.Unit_Details__c,
                          Booking_Unit__r.Inventory__r.Property_City__c, Buyer__r.First_Name__c,
                          Booking_Unit__r.Inventory__r.Property_Status__c, Buyer__r.Last_Name__c,
                          Booking_Unit__r.Inventory__r.Property_Country__c, Buyer__r.Nationality__c,
                          Booking_Unit__r.Inventory__r.Property__r.DIFC__c, Buyer__r.Passport_Number__c,
                          Buyer__r.Passport_Expiry_Date__c, Buyer__r.IPMS_Buyer_ID__c, Seller__r.Party_ID__c,
                          Seller__r.FirstName, Seller__r.LastName, Seller__r.Nationality__pc,
                          AccountId, Account.Party_ID__c, Booking_Unit__c, Seller__c,
                          Purpose_of_POA__c, Purpose_of_POA_Seller__c
                          ,Parking_Type_Of_Payment__c
                          ,Parking_Payment_Mode__c
                          ,Pending_Amount__c 
                          ,Parking_Details_JSON__c
                          ,Parking_Total_Payment_Terms__c
                          from Case where Id IN: caseIdSet]);
      //return mapCaseDetails;
      
      list<Task> lsTaskToUpdate = new list<Task>();
      for(Task objTask : [Select t.WhoId
                               , t.WhatId
                               , t.Type
                               , t.Status
                               , t.OwnerId
                               , t.Id
                               , t.IPMS_Role__c
                               , t.Subject
                               , t.CreatedDate
                               , t.Description
                               , t.Assigned_User__c
                               , t.ActivityDate
                               , t.Owner.Name
                               , t.Task_Error_Details__c
                               , t.Pushed_to_IPMS__c
                               , t.Document_URL__c
                               From Task t
                               where t.Id IN: taskIdSet]){

          list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5> listObjBeans = 
          new list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5>();
          
          TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objHeaderBean = 
          new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
          String reqNo = '2-'+string.valueOf(System.currentTimeMillis());
          objHeaderBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
          system.debug('param id*****'+objHeaderBean.PARAM_ID);
          objHeaderBean.ATTRIBUTE1 = 'HEADER';
          objHeaderBean.ATTRIBUTE2 = 'Additional Parking';
          if(objTask.Status != 'Cancelled'){
              objHeaderBean.ATTRIBUTE3 = 'Open';
          }else{
              objHeaderBean.ATTRIBUTE3 = 'Closed';
          }
          system.debug('objTask.Status*****'+objTask.Status);
          system.debug('party ID '+mapCaseDetails.get(objTask.WhatId).Account.Party_ID__c);
          objHeaderBean.ATTRIBUTE4 = objTask.Owner.Name;//'CRM';
          objHeaderBean.ATTRIBUTE5 = String.isNotBlank(mapCaseDetails.get(objTask.WhatId).Account.Party_ID__c) == true ? mapCaseDetails.get(objTask.WhatId).Account.Party_ID__c : '';
          objHeaderBean.ATTRIBUTE6 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c;
          system.debug('regid*****'+mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c);
          objHeaderBean.ATTRIBUTE7 = String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
          system.debug('date*****'+String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()));
          objHeaderBean.ATTRIBUTE8 = '';
          objHeaderBean.ATTRIBUTE9 = '';
          objHeaderBean.ATTRIBUTE10 = String.isNotBlank(mapCaseDetails.get(objTask.WhatId).Account.Party_ID__c) == true ? mapCaseDetails.get(objTask.WhatId).Account.Party_ID__c : '';
          objHeaderBean.ATTRIBUTE11 = objTask.WhatId;
          objHeaderBean.ATTRIBUTE12 = '';
          objHeaderBean.ATTRIBUTE13 = '';
          objHeaderBean.ATTRIBUTE14 = '';
          objHeaderBean.ATTRIBUTE15 = '';
          objHeaderBean.ATTRIBUTE16 = '';
          objHeaderBean.ATTRIBUTE17 = '';
          objHeaderBean.ATTRIBUTE18 = '';
          objHeaderBean.ATTRIBUTE19 = '';
          objHeaderBean.ATTRIBUTE20 = '';
          objHeaderBean.ATTRIBUTE21 = '';
          objHeaderBean.ATTRIBUTE22 = '';
          objHeaderBean.ATTRIBUTE23 = '';
          objHeaderBean.ATTRIBUTE24 = '';
          objHeaderBean.ATTRIBUTE25 = '';
          objHeaderBean.ATTRIBUTE26 = '';
          objHeaderBean.ATTRIBUTE27 = '';
          objHeaderBean.ATTRIBUTE28 = '';
          objHeaderBean.ATTRIBUTE29 = '';
          objHeaderBean.ATTRIBUTE30 = '';
          objHeaderBean.ATTRIBUTE31 = '';
          objHeaderBean.ATTRIBUTE32 = '';
          objHeaderBean.ATTRIBUTE33 = '';
          objHeaderBean.ATTRIBUTE34 = '';
          objHeaderBean.ATTRIBUTE35 = '';
          objHeaderBean.ATTRIBUTE36 = '';
          objHeaderBean.ATTRIBUTE37 = '';
          objHeaderBean.ATTRIBUTE38 = '';
          objHeaderBean.ATTRIBUTE39 = '';
          //objHeaderBean.ATTRIBUTE40 = '';
          objHeaderBean.ATTRIBUTE41 = '';
          objHeaderBean.ATTRIBUTE42 = '';
          objHeaderBean.ATTRIBUTE43 = '';
          objHeaderBean.ATTRIBUTE44 = '';
          objHeaderBean.ATTRIBUTE45 = '';
          objHeaderBean.ATTRIBUTE46 = '';
          objHeaderBean.ATTRIBUTE47 = '';
          objHeaderBean.ATTRIBUTE48 = '';
          objHeaderBean.ATTRIBUTE49 = '';
          objHeaderBean.ATTRIBUTE50 = '';
          listObjBeans.add(objHeaderBean);
          
          // create TASK bean
          TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objTaskBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
          objTaskBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
          objTaskBean.ATTRIBUTE1 = 'TASK';
          objTaskBean.ATTRIBUTE2 = objTask.Subject;
          system.debug('subject*****'+objTask.Subject);
          if(objTask.Status != 'Cancelled'){
              objTaskBean.ATTRIBUTE3 = 'Open';
          }else{
              objTaskBean.ATTRIBUTE3 = 'Closed';
          }
          //objTaskBean.ATTRIBUTE3 = objTask.Status;
          system.debug('status*****'+objTask.Status);
          objTaskBean.ATTRIBUTE4 = objTask.IPMS_Role__c;
          objTaskBean.ATTRIBUTE5 = '';
          objTaskBean.ATTRIBUTE6 = '';
          objTaskBean.ATTRIBUTE7 = String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
          system.debug('created date*****'+String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()));
          objTaskBean.ATTRIBUTE8 = objTask.Id;
          system.debug('task id*****'+objTask.Id);
          Datetime dt = objTask.ActivityDate;
          objTaskBean.ATTRIBUTE9 = String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase());
          system.debug('due date*****'+String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase()));
          objTaskBean.ATTRIBUTE10 = '';
          objTaskBean.ATTRIBUTE11 = objTask.WhatId;
          objTaskBean.ATTRIBUTE12 = objTask.Subject;
          objTaskBean.ATTRIBUTE13 = objTask.Status;
          objTaskBean.ATTRIBUTE14 = objTask.Type;
          objTaskBean.ATTRIBUTE15 = objTask.Description;
          objTaskBean.ATTRIBUTE16 = objTask.Assigned_User__c;
          objTaskBean.ATTRIBUTE17 = mapCaseDetails.get(objTask.WhatId).Relationship_with_Seller__c;
          objTaskBean.ATTRIBUTE18 = mapCaseDetails.get(objTask.WhatId).Case_Type__c;
          objTaskBean.ATTRIBUTE19 = mapCaseDetails.get(objTask.WhatId).Buyer_Type__c;
          objTaskBean.ATTRIBUTE20 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Unit_Details__c;
          objTaskBean.ATTRIBUTE21 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Inventory__r.Property_City__c;
          objTaskBean.ATTRIBUTE22 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Inventory__r.Property_Status__c;
          objTaskBean.ATTRIBUTE23 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Inventory__r.Property_Country__c;
          objTaskBean.ATTRIBUTE24 = '';
          objTaskBean.ATTRIBUTE25 = mapCaseDetails.get(objTask.WhatId).Buyer__r.First_Name__c;
          objTaskBean.ATTRIBUTE26 = mapCaseDetails.get(objTask.WhatId).Buyer__r.Last_Name__c;
          objTaskBean.ATTRIBUTE27 = mapCaseDetails.get(objTask.WhatId).Buyer__r.Nationality__c;
          objTaskBean.ATTRIBUTE28 = mapCaseDetails.get(objTask.WhatId).Buyer__r.Passport_Number__c;
          objTaskBean.ATTRIBUTE29 = mapCaseDetails.get(objTask.WhatId).Buyer__r.Passport_Expiry_Date__c;
          objTaskBean.ATTRIBUTE30 = mapCaseDetails.get(objTask.WhatId).Buyer__r.IPMS_Buyer_ID__c;
          //set type of payment selected for parking
          objTaskBean.ATTRIBUTE31 = mapCaseDetails.get(objTask.WhatId).Parking_Type_Of_Payment__c != null ? mapCaseDetails.get(objTask.WhatId).Parking_Type_Of_Payment__c : '';
          if(mapCaseDetails.get(objTask.WhatId).Parking_Type_Of_Payment__c != null && mapCaseDetails.get(objTask.WhatId).Parking_Type_Of_Payment__c == 'Full Payment')
          {
            objTaskBean.ATTRIBUTE32 = mapCaseDetails.get(objTask.WhatId).Parking_Payment_Mode__c != null ? mapCaseDetails.get(objTask.WhatId).Parking_Payment_Mode__c : '';
            objTaskBean.ATTRIBUTE33 = '';
          }
          else if( mapCaseDetails.get(objTask.WhatId).Parking_Type_Of_Payment__c != null 
                   && mapCaseDetails.get(objTask.WhatId).Parking_Type_Of_Payment__c == 'Installment Based Payment'
                 )
          {
            objTaskBean.ATTRIBUTE32 =  '';
            objTaskBean.ATTRIBUTE33 = mapCaseDetails.get(objTask.WhatId).Parking_Total_Payment_Terms__c != null ? String.valueOf(mapCaseDetails.get(objTask.WhatId).Parking_Total_Payment_Terms__c) : '';
          }
          
          objTaskBean.ATTRIBUTE34 = '';
          
          objTaskBean.ATTRIBUTE35 = '';
          objTaskBean.ATTRIBUTE36 = '';
          
          objTaskBean.ATTRIBUTE37 = '';
          objTaskBean.ATTRIBUTE38 = '';
          objTaskBean.ATTRIBUTE39 = '';
          objTaskBean.ATTRIBUTE41 = '';
          objTaskBean.ATTRIBUTE42 = '';
          objTaskBean.ATTRIBUTE43 = '';
          objTaskBean.ATTRIBUTE44 = '';
          objTaskBean.ATTRIBUTE45 = '';
          objTaskBean.ATTRIBUTE46 = '';
          objTaskBean.ATTRIBUTE47 = '';
          objTaskBean.ATTRIBUTE48 = '';
          objTaskBean.ATTRIBUTE49 = '';
          objTaskBean.ATTRIBUTE50 = '';
          listObjBeans.add(objTaskBean);
          
          // create UNIT bean
          TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objUnitBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
          objUnitBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
          objUnitBean.ATTRIBUTE1 = 'UNITS';
          objUnitBean.ATTRIBUTE2 = 'AdditionalParking';
          objUnitBean.ATTRIBUTE3 = '';
          objUnitBean.ATTRIBUTE4 = '';
          objUnitBean.ATTRIBUTE5 = '';
          objUnitBean.ATTRIBUTE6 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c;
          system.debug('reg*****'+mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c);
          objUnitBean.ATTRIBUTE7 = '';
          objUnitBean.ATTRIBUTE8 = '';
          objUnitBean.ATTRIBUTE9 = '';
          objUnitBean.ATTRIBUTE10 = '';
          objUnitBean.ATTRIBUTE11 = '';
          objUnitBean.ATTRIBUTE12 = '';
          objUnitBean.ATTRIBUTE13 = '';
          objUnitBean.ATTRIBUTE14 = '';
          objUnitBean.ATTRIBUTE15 = '';
          objUnitBean.ATTRIBUTE16 = '';
          objUnitBean.ATTRIBUTE17 = '';
          objUnitBean.ATTRIBUTE18 = '';
          objUnitBean.ATTRIBUTE19 = '';
          objUnitBean.ATTRIBUTE20 = '';
          objUnitBean.ATTRIBUTE21 = '';
          objUnitBean.ATTRIBUTE22 = '';
          objUnitBean.ATTRIBUTE23 = '';
          objUnitBean.ATTRIBUTE24 = '';
          objUnitBean.ATTRIBUTE25 = '';
          objUnitBean.ATTRIBUTE26 = '';
          objUnitBean.ATTRIBUTE27 = '';
          objUnitBean.ATTRIBUTE28 = '';
          objUnitBean.ATTRIBUTE29 = '';
          objUnitBean.ATTRIBUTE30 = '';
          objUnitBean.ATTRIBUTE31 = '';
          objUnitBean.ATTRIBUTE32 = '';
          objUnitBean.ATTRIBUTE33 = '';
          objUnitBean.ATTRIBUTE34 = '';
          objUnitBean.ATTRIBUTE35 = '';
          objUnitBean.ATTRIBUTE36 = '';
          objUnitBean.ATTRIBUTE37 = '';
          objUnitBean.ATTRIBUTE38 = '';
          objUnitBean.ATTRIBUTE39 = '';
          objUnitBean.ATTRIBUTE41 = '';
          objUnitBean.ATTRIBUTE42 = '';
          objUnitBean.ATTRIBUTE43 = '';
          objUnitBean.ATTRIBUTE44 = '';
          objUnitBean.ATTRIBUTE45 = '';
          objUnitBean.ATTRIBUTE46 = '';
          objUnitBean.ATTRIBUTE47 = '';
          objUnitBean.ATTRIBUTE48 = '';
          objUnitBean.ATTRIBUTE49 = '';
          objUnitBean.ATTRIBUTE50 = '';
          listObjBeans.add(objUnitBean);
          
          TaskCreationWSDL.TaskHttpSoap11Endpoint objClass = new TaskCreationWSDL.TaskHttpSoap11Endpoint();
          String response = objClass.SRDataToIPMSMultiple(reqNo, 'CREATE_SR', 'SFDC', listObjBeans);
          system.debug('resp*****'+response);
          if(String.isNotBlank(response)) {
              innerClass IC = (innerClass)JSON.deserialize(response, innerClass.class);
              system.debug('IC*****'+IC);
              if(IC.status.EqualsIgnoreCase('E')){
                  objTask.Pushed_to_IPMS__c = false;
                  objTask.Task_Error_Details__c = IC.status;
                  system.debug('objTask=='+objTask);
                  Error_Log__c objErr = createErrorLogRecord(mapCaseDetails.get(objTask.WhatId).Seller__c, mapCaseDetails.get(objTask.WhatId).Booking_Unit__c, objTask.WhatId);
                  objErr.Error_Details__c = IC.message;
                  listErrorLog.add(objErr);
              }else{
                  objTask.Pushed_to_IPMS__c = true;
              }
          }
          else {
              Error_Log__c objErr = createErrorLogRecord(mapCaseDetails.get(objTask.WhatId).Seller__c, mapCaseDetails.get(objTask.WhatId).Booking_Unit__c, objTask.WhatId);
              objErr.Error_Details__c = 'Error : No Response from IPMS for Task Creation';
              listErrorLog.add(objErr);
          }
          /*if(String.isNotBlank(response)) {
              mapDeserializeTask = (map<String, Object>)JSON.deserializeUntyped(response);
              system.debug('mapDeserializeTask*****'+mapDeserializeTask);
              if(mapDeserializeTask.get('status') == 'E') {
                  objTask.Pushed_to_IPMS__c = false;
                  objTask.Task_Error_Details__c = 'E';

                  Error_Log__c objErr = createErrorLogRecord(mapCaseDetails.get(objTask.WhatId).Seller__c, mapCaseDetails.get(objTask.WhatId).Booking_Unit__c, objTask.WhatId);
                  objErr.Error_Details__c = (String)mapDeserializeTask.get('message');
                  listErrorLog.add(objErr);
              }
              else {
                  objTask.Pushed_to_IPMS__c = true;
              }
          }
          else {
              Error_Log__c objErr = createErrorLogRecord(mapCaseDetails.get(objTask.WhatId).Seller__c, mapCaseDetails.get(objTask.WhatId).Booking_Unit__c, objTask.WhatId);
              objErr.Error_Details__c = 'Error : No Response from IPMS for Task Creation';
              listErrorLog.add(objErr);
          }*/

          /*innerClass IC = (innerClass)JSON.deserialize(response, innerClass.class);
          system.debug('IC*****'+IC);
          if(IC.status.EqualsIgnoreCase('E')){
              objTask.Pushed_to_IPMS__c = false;
              objTask.Task_Error_Details__c = IC.status;
          }else{
              objTask.Pushed_to_IPMS__c = true;
          }*/
          
          lsTaskToUpdate.add(objTask);
      } // end of for loop
      if(!lsTaskToUpdate.isEmpty()){
          update lsTaskToUpdate;
      }
      if(!listErrorLog.isEmpty()) {
          insertErrorLog(listErrorLog);
      }
  }

  public static Error_Log__c createErrorLogRecord(Id accId, Id bookingUnitId, Id caseId){
        Error_Log__c objErr = new Error_Log__c();
        objErr.Account__c = accId;
        objErr.Booking_Unit__c = bookingUnitId;
        objErr.Case__c = caseId;
        return objErr;
    }

    public static void insertErrorLog(list<Error_Log__c> listObjErr)
    {
        try{
            insert listObjErr;
        }catch(Exception ex){
            //errorMessage = 'Error : '+ ex.getMessage();
            system.debug('Error Log ex*****'+ex);
        }
    }

    public class innerClass
    {
        public string message;
        public string status;
        
        public innerClass(){
            
        }
    }
}