// Class to raise an appointment from Account details page
public virtual without sharing class RaiseAnAppointmentAccountCntrl {

    public RaiseAnAppointmentAccountCntrl(){}
    
    public list<AppointmentWrapper> lstAppointmentWrapper {get;set;}
    public String strSelectedDate { get; set; }
    public String strPurpose { get; set; }
    public String strAccId ;
    public Account objAccount {get;set;}
    private Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
    private Id handoverCLRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Handover Calling List').getRecordTypeId();
    private Id earlyHandoverCLRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Early Handover Calling List').getRecordTypeId();
    public Boolean processSelected {get; set;}
    public Boolean subProcessSelected {get; set;}
    public string strSelectedProcess {get;set;}
    public String strSelectedSubProcess {get;set;}
    public List<SelectOption> lstSRs {get;set;}
    public List<SelectOption> lstSubProcessOptions {get;set;}
    public List<SelectOption> lstBookingUnitOptions {get;set;}
    public map<String,booking_unit__c> mapBookingUnit {get; set;}
    public String strSelectedBookingUnits {get;set;}
    public Boolean unitSelected {get; set;}
    private Id buildingId ;
    private String hoTeamEmail;
    public Boolean blnIsBookingUnit {get;set;}
    public String strHolidayMsg     {get;set;}
    public Boolean isEnableBtn {get;set;}
    public Boolean virtualViewing {get;set;}

    public RaiseAnAppointmentAccountCntrl( ApexPages.standardController controller ){

        strAccId = ApexPages.currentPage().getParameters().get('id');
        if( String.isNotBlank( strAccId ) ) {
            strHolidayMsg = '';
            //strSelectedDate = String.valueOf( system.today() );
            blnIsBookingUnit = false;
            init();
        }
    } //End constructor

    public pagereference init(){
        isEnableBtn = false;


        lstBookingUnitOptions = new List<SelectOption>();
        mapBookingUnit = new map<String,booking_unit__c>();
        system.debug(' strAccId : '+strAccId);
        if( String.isNotBlank(strAccId) ) {
            objAccount = [  SELECT Id
                                , Name
                                , Primary_CRE__c
                                , Secondary_CRE__c
                                , Tertiary_CRE__c
                                , Primary_Language__c
                                , Email__pc
                                , Email__c,isPersonAccount
                             FROM Account
                            WHERE Id = :strAccId ];
        }

        List<Booking_Unit__c> lstBookingUnit = new List<Booking_Unit__c>();
        lstBookingUnit = [Select Id,name,Property_Name__c,Inventory__c,Booking__c,Manager_Name__c, HOS_Name__c,Okay_to_release_keys__c,Handover_Notice_Sent__c ,
                          Property_Consultant__c,Unit_Name__c,CurrencyIsoCode,Dispute_Flag__c,Handover_Notice_Sent_Date__c,EHO_Notice_Sent__c,
                          Registration_ID__c,Property_City__c,District__c,Construction_Status__c,Property_Country__c,Handover_Flag__c
                          from Booking_Unit__c where Booking__r.Account__c =:
                          strAccId AND Registration_ID__c != null
                          AND Unit_Name__c != null
                          ];

        if(lstBookingUnit.Size()>0){
            lstBookingUnitOptions.add(new SelectOption('','--None--' ));
            for(Booking_Unit__c objBookingUnit : lstBookingUnit){
                lstBookingUnitOptions.add(new SelectOption
                                         (objBookingUnit.id,objBookingUnit.Unit_Name__c ));
                mapBookingUnit.put(objBookingUnit.id,objBookingUnit);
            }
        }
        System.debug('-----mapBookingUnit---'+mapBookingUnit);

        map<Id, AppointmentWrapper> mapAppIdAppWrapper = new map<Id, AppointmentWrapper>();
        lstSRs = new List<SelectOption>();
        lstSRs.add(new selectOption('', '--None--'));

        List<Active_Process_on_Portal_Meta__mdt> lstActiveProcess = new List<Active_Process_on_Portal_Meta__mdt>();
        lstActiveProcess = [Select id,MasterLabel,CRE_MasterLabel__c,DeveloperName from Active_Process_on_Portal_Meta__mdt ];

        for(Active_Process_on_Portal_Meta__mdt obj : lstActiveProcess){
            if( String.isNotBlank( obj.CRE_MasterLabel__c ) ) {
                lstSRs.add(new selectoption(obj.CRE_MasterLabel__c,obj.CRE_MasterLabel__c));
            }
        }

        /*if(  String.isNotBlank(strSelectedProcess)
         && strSelectedProcess == 'Handover'
         && String.isNotBlank( strSelectedBookingUnits ) ) {
            String strProjectName = strSelectedBookingUnits.substringBefore('/');
            if( String.isNotBlank( strProjectName ) ) {
                Location__c objLoc = [ SELECT Id
                                            , Name
                                            , Handover_Team_notification_Email__c
                                         FROM Location__c
                                        WHERE Name = :strProjectName ];
                if( objLoc != null ) {
                    buildingId = objLoc.Id ;
                    hoTeamEmail = objLoc.Handover_Team_notification_Email__c;
                    for( Appointment__c objApp : getAppointments() ) {
                        if( String.isNotBlank( objApp.Sub_Process_Name__c ) && objApp.Appointment_Date__c != null ) {
                            if ( objApp.Sub_Process_Name__c == 'Unit Viewing') {
                                Date newDate = System.today().addDays( Integer.valueOf( Label.Unit_Viewing_Days ) );
                                if ( objApp.Appointment_Date__c > newDate ) {
                                    mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                                }
                            }
                            else if( objApp.Sub_Process_Name__c == 'Key Handover' ) {
                                Date newDate = System.today().addDays( Integer.valueOf( Label.Key_Handover_Days ) );
                                if ( objApp.Appointment_Date__c > newDate ) {
                                    mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                                }
                            }
                            else {
                               mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                            }
                        }
                    }

                }
            }
        } else if ( String.isNotBlank(strSelectedProcess)
                 && strSelectedProcess != 'Handover' ) {
            for( Appointment__c objApp : getAppointments() ) {
                if( String.isNotBlank( objApp.Sub_Process_Name__c ) && objApp.Appointment_Date__c != null ) {
                    if ( objApp.Sub_Process_Name__c == 'Unit Viewing') {
                        Date newDate = System.today().addDays( Integer.valueOf( Label.Unit_Viewing_Days ) );
                        if ( objApp.Appointment_Date__c > newDate ) {
                            mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                        }
                    }
                    else if( objApp.Sub_Process_Name__c == 'Key Handover' ) {
                        Date newDate = System.today().addDays( Integer.valueOf( Label.Key_Handover_Days ) );
                        if ( objApp.Appointment_Date__c > newDate ) {
                            mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                        }
                    }
                    else {
                       mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                    }
                }
            }// End of For
        }


        if( mapAppIdAppWrapper != null && !mapAppIdAppWrapper.isEmpty() ) {
            lstAppointmentWrapper = new list<AppointmentWrapper>();
            for( Calling_List__c objCalling : getAppointmentScheduling( mapAppIdAppWrapper.keySet() ) ) {
                if( mapAppIdAppWrapper.containsKey( objCalling.Appointment__c ) ) {
                    mapAppIdAppWrapper.get( objCalling.Appointment__c ).objCL = objCalling;
                }
            }
            lstAppointmentWrapper.addAll( mapAppIdAppWrapper.values() );
        }

        lstSRs.sort();*/
        return null ;

    } //End Of Init

    public pageReference subProcess() {
        isEnableBtn = true;
        system.debug( 'strSelectedProcess : ' + strSelectedProcess );
        system.debug( 'strSelectedDate : ' + strSelectedDate );

        strSelectedSubProcess = '';
        if( blnIsBookingUnit == false ) {
            strSelectedBookingUnits = '';
        }

        if(String.isNotBlank(strSelectedProcess)) {
            processSelected = String.isNotBlank(strSelectedProcess);
            lstSubProcessOptions = new List<SelectOption>();
            lstSubProcessOptions.add(new selectOption('', '--None--'));
            List<Sub_Process_for_Portal__mdt> lstSubProcess = new List<Sub_Process_for_Portal__mdt>();
            lstSubProcess = [Select id, MasterLabel, DeveloperName,Active_Process_on_Portal__c
                            from Sub_Process_for_Portal__mdt
                            where Active_Process_on_Portal__r.CRE_MasterLabel__c  =: strSelectedProcess];
            system.debug( 'lstSubProcess : ' + lstSubProcess );
            if(lstSubProcess.Size()>0){
                for(Sub_Process_for_Portal__mdt objSubProcess : lstSubProcess ){
                    lstSubProcessOptions.add(new selectoption(objSubProcess.MasterLabel,objSubProcess.MasterLabel ));
                }
            }
            system.debug( 'lstSubProcessOptions : ' + lstSubProcessOptions );
            getAvailableAppointments();
        }else {
            lstSubProcessOptions = new List<SelectOption>();
            lstSubProcessOptions.add(new selectOption('', '--None--'));
        }
        return null;
    } // End of subProcess

    public pageReference createAppointment() {
        System.debug('Inside createAppt lstAppointmentWrapper-->' +lstAppointmentWrapper);
        if( String.isNotBlank(strSelectedProcess) ) {
            if (Label.Appointment_New_Logic == 'Yes'){
                list<AppointmentSelectionHandler.AppointmentWrapper> lstwrappers = new
                list<AppointmentSelectionHandler.AppointmentWrapper>();

                for (AppointmentWrapper objWrap : lstAppointmentWrapper) {
                    //System.debug('Owner Id in lstAppointmentWrapper in createAppt' +objWrap.objApp.OwnerId);
                    if (objWrap.isSelected == true) {
                    AppointmentSelectionHandler.AppointmentWrapper newWrap = new
                        AppointmentSelectionHandler.AppointmentWrapper(objWrap.objApp, objWrap.objCL, '', true);
                    lstwrappers.add(newWrap);
                    }
                }
                if( String.isNotBlank(strSelectedDate) && !virtualViewing) {
                    AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
                    handler.createAppointments(lstwrappers, strPurpose, strSelectedBookingUnits,
                        objAccount, strSelectedSubProcess, strSelectedProcess, strSelectedDate);

                    if( blnIsBookingUnit == false ) {
                        PageReference nextPage = new PageReference('/' + strAccId);
                        return nextPage;
                    } else {
                        PageReference nextPage = new PageReference('/' + strSelectedBookingUnits);
                        return nextPage;
                    }
                }else if( String.isNotBlank(strSelectedDate) && virtualViewing) {
                    System.debug('lstwrappers--->' +lstwrappers);
                    AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
                    handler.createAppointments(lstwrappers, strPurpose, strSelectedBookingUnits,
                        objAccount, strSelectedSubProcess, strSelectedProcess, strSelectedDate, virtualViewing);

                    if( blnIsBookingUnit == false ) {
                        PageReference nextPage = new PageReference('/' + strAccId);
                        return nextPage;
                    } else {
                        PageReference nextPage = new PageReference('/' + strSelectedBookingUnits);
                        return nextPage;
                    }
                } 
                else {
                    ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Error,'Please select Appointment Date'));
                }
            } 
        } else {
            ApexPages.addmessage(new ApexPages.message(
            ApexPages.severity.Error,'Please select Process Name'));

        }
        return null;
    } //End of createAppointment

    // method used to format date to yyyy-MM-dd
    public static String formatDate( String dt ) {
        if ( String.isNotBlank( dt ) ) {
            list<String> lstDateComponents;
            system.debug('== Date1 =='+dt );
            if( dt.contains('/') ) {
                lstDateComponents = dt.split('/');
            }
            else if( dt.contains('-') ) {
                lstDateComponents = dt.split('-');
            }

            Date objDateToFormat = date.newinstance( Integer.valueOf( lstDateComponents[2] ), Integer.valueOf( lstDateComponents[0] ), Integer.valueOf( lstDateComponents[1] ));
            system.debug('== Date2 =='+dt );
            return DateTime.newInstance(objDateToFormat.year(),objDateToFormat.month(),objDateToFormat.day()).format('yyyy-MM-dd');
        }
        return null;
    } //End of formatDate

    public pageReference getAvailableAppointments() {

        //virtualViewing = false;
        lstAppointmentWrapper = new list<AppointmentWrapper>();


        map<Id, AppointmentWrapper> mapAppIdAppWrapper = new map<Id, AppointmentWrapper>();


        subProcessSelected = String.isNotBlank(strSelectedSubProcess);
        unitSelected = String.isNotBlank(strSelectedBookingUnits);

        /*if (unitSelected) {
            mapBookingUnit.put(strSelectedBookingUnits, new Booking_Unit__c());
        }*/

        strSelectedDate = String.valueOf( formatDate( strSelectedDate ) );
        system.debug('== Date3 =='+strSelectedDate );

        if( String.isNotBlank( strSelectedDate ) ) {
            system.debug( 'strSelectedProcess : '+ strSelectedProcess );
            system.debug( 'strSelectedSubProcess : '+ strSelectedSubProcess );
            system.debug( 'strSelectedBookingUnits : '+ strSelectedBookingUnits );
            /*for( DAMACHolidaySettings__c objDAMACHolidaySettings : DAMACHolidaySettings__c.getall().values()){
                //system.assert( false,'objDAMACHolidaySettings : ' +objDAMACHolidaySettings.Name );
                if ( Date.valueOf( strSelectedDate ) >= objDAMACHolidaySettings.From__c &&  Date.valueOf( strSelectedDate ) <= objDAMACHolidaySettings.To__c  ) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Damac service teams are not working from '+ objDAMACHolidaySettings.From__c.format() + ' to ' + objDAMACHolidaySettings.To__c.format() + '. Kindly choose a different date.'));
                    return null;
                }
            }

            if( strSelectedSubProcess == 'Key Handover'
             && String.isNotBlank( strSelectedBookingUnits )
             && mapBookingUnit.containsKey( strSelectedBookingUnits )
             && !mapBookingUnit.get(strSelectedBookingUnits).Okay_to_release_keys__c ) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,system.label.UV_and_KH_Appointments_Restriction));
                return null;
            }*/


            if(  String.isNotBlank(strSelectedProcess)
             && strSelectedProcess == 'Handover'
             && String.isNotBlank(strSelectedSubProcess)
             && ( strSelectedSubProcess == 'Unit Viewing' || strSelectedSubProcess == 'Unit Viewing or Key Handover' ||
                strSelectedSubProcess == 'Key Handover' )
             && String.isNotBlank( strSelectedBookingUnits ) ) {

                /*if( ( mapBookingUnit.get(strSelectedBookingUnits).Dispute_Flag__c == 'D'
                   || mapBookingUnit.get(strSelectedBookingUnits).Dispute_Flag__c == 'L'
                   || mapBookingUnit.get(strSelectedBookingUnits).Dispute_Flag__c == 'E'
                   || mapBookingUnit.get(strSelectedBookingUnits).Dispute_Flag__c == 'C'  ) && 
           mapBookingUnit.get(strSelectedBookingUnits).Handover_Flag__c  == 'Y' &&
                   !( mapBookingUnit.get(strSelectedBookingUnits).Handover_Notice_Sent__c
                   || mapBookingUnit.get(strSelectedBookingUnits).EHO_Notice_Sent__c ) ) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,system.label.UV_and_KH_Appointments_Restriction));
          You can not create appointment for this unit..
                    return null;
                } else {*/
                    String strProjectName = mapBookingUnit.get(strSelectedBookingUnits).Unit_Name__c.substringBefore('/');
                    system.debug( 'strProjectName : '+ strProjectName );
                    if( String.isNotBlank( strProjectName ) ) {
                        Location__c objLoc = [ SELECT Id
                                                    , Name
                                                    , Handover_Team_notification_Email__c
                                                 FROM Location__c
                                                WHERE Name = :strProjectName ];
                        if( objLoc != null ) {
                            buildingId = objLoc.Id ;
                            hoTeamEmail = objLoc.Handover_Team_notification_Email__c;
                            if (Label.Appointment_New_Logic == 'Yes'){
                                AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
                                for (AppointmentSelectionHandler.AppointmentWrapper objWrap :
                                    handler.availableSlots(objAccount.Id,
                                    strSelectedBookingUnits, strSelectedProcess, strSelectedSubProcess, 'CRE',
                                    strSelectedDate, false)) {
                                    system.debug('!!!!!!objWrap'+objWrap);
                                    if (!string.isBlank(objWrap.ErrorMessage) ){
                                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,
                                        objWrap.ErrorMessage));
                                        return null;
                                    } else {
                                        AppointmentWrapper objSelectedWrap =
                                        new AppointmentWrapper(objWrap.objApp, objWrap.objCL);
                                        lstAppointmentWrapper.add(objSelectedWrap);
                                    }
                                 }
                            } /*else {
                            for( Appointment__c objApp : getAppointments() ) {

                                    if( String.isNotBlank( objApp.Sub_Process_Name__c ) && objApp.Appointment_Date__c != null ) {
                                        if ( ( objApp.Sub_Process_Name__c == 'Unit Viewing or Key Handover' || objApp.Sub_Process_Name__c == 'Unit Viewing' )
                                            && strSelectedSubProcess == 'Unit Viewing') {
                                            Date newDate = System.today().addDays( Integer.valueOf( Label.Unit_Viewing_Days ) );
                                            if ( objApp.Appointment_Date__c > newDate ) {
                                                mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                                            }
                                        }
                                        else if( (objApp.Sub_Process_Name__c == 'Unit Viewing or Key Handover' || objApp.Sub_Process_Name__c == 'Key Handover' )
                                                && strSelectedSubProcess == 'Key Handover' ) {
                                            Date newDate = System.today().addDays( Integer.valueOf( Label.Key_Handover_Days ) );
                                            if ( objApp.Appointment_Date__c > newDate ) {
                                                mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                                            }
                                        }//else if( objApp.Sub_Process_Name__c == 'Unit Viewing or Key Handover') {
                                           // Date newDate = System.today().addDays( Integer.valueOf( Label.Unit_Viewing_Days ) );

                                           // if ( objApp.Appointment_Date__c > newDate ) {
                                            //    mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                                            //}
                                        //}

                                        else {
                                           mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                                        }
                                    }
                                }
                            }*/
                        }
                    }
                //}
            } else if ( String.isNotBlank(strSelectedProcess)
                     //&& strSelectedProcess != 'Handover'
                     && strSelectedSubProcess != 'Unit Viewing'
                     && strSelectedSubProcess != 'Key Handover'
                     && strSelectedSubProcess != 'Unit Viewing or Key Handover')  {
                     buildingId = null;
                if (Label.Appointment_New_Logic == 'Yes'){
                    AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
                    for (AppointmentSelectionHandler.AppointmentWrapper objWrap :
                        handler.availableSlots(objAccount.Id, strSelectedBookingUnits,
                        strSelectedProcess, strSelectedSubProcess, 'CRE', strSelectedDate, false)) {
                        system.debug('!!!!!!objWrap'+objWrap);
                        if (!string.isBlank(objWrap.ErrorMessage) ){
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,
                            objWrap.ErrorMessage));
                            return null;
                        } else {
                            AppointmentWrapper objSelectedWrap =
                            new AppointmentWrapper(objWrap.objApp, objWrap.objCL);
                            lstAppointmentWrapper.add(objSelectedWrap);
                         }
                    }
                } /*else {
                    for( Appointment__c objApp : getAppointments() ) {
                        if( String.isNotBlank( objApp.Sub_Process_Name__c ) && objApp.Appointment_Date__c != null ) {
                            if ( ( objApp.Sub_Process_Name__c == 'Unit Viewing or Key Handover' || objApp.Sub_Process_Name__c == 'Unit Viewing' )
                                && strSelectedSubProcess == 'Unit Viewing') {
                                Date newDate = System.today().addDays( Integer.valueOf( Label.Unit_Viewing_Days ) );
                                if ( objApp.Appointment_Date__c > newDate ) {
                                    mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                                }
                            }
                            else if( (objApp.Sub_Process_Name__c == 'Unit Viewing or Key Handover' || objApp.Sub_Process_Name__c == 'Key Handover' )
                                    && strSelectedSubProcess == 'Key Handover' ) {
                                Date newDate = System.today().addDays( Integer.valueOf( Label.Key_Handover_Days ) );
                                if ( objApp.Appointment_Date__c > newDate ) {
                                    mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                                }
                            }//else if( objApp.Sub_Process_Name__c == 'Unit Viewing or Key Handover') {
                                //Date newDate = System.today().addDays( Integer.valueOf( Label.Unit_Viewing_Days ) );

                                //if ( objApp.Appointment_Date__c > newDate ) {
                                 //   mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                                //}
                            //}

                            else {
                               mapAppIdAppWrapper.put( objApp.Id, new AppointmentWrapper( objApp, null ) );
                            }
                        }
                    }// End of For
                }*/
            }
        }
        /*if (Label.Appointment_New_Logic != 'Yes'){
            if( mapAppIdAppWrapper != null && !mapAppIdAppWrapper.isEmpty() ) {
                for( Calling_List__c objCalling : getAppointmentScheduling( mapAppIdAppWrapper.keySet() ) ) {
                    if( mapAppIdAppWrapper.containsKey( objCalling.Appointment__c ) ) {
                        mapAppIdAppWrapper.get( objCalling.Appointment__c ).objCL = objCalling;
                    }
                }
                lstAppointmentWrapper.addAll( mapAppIdAppWrapper.values() );
                system.debug( '== lstAppointmentWrapper ==' +lstAppointmentWrapper );
            }
        }*/
        return null;
    } //End of getAvailableAppointments

    /*public list<Calling_List__c> getAppointmentScheduling( set<Id> setAppointmentIds ) {
        return [ SELECT Id
                      , Name
                      , Booking_Unit_Name__c
                      , RecordTypeId
                      , Account__c
                      , Appointment_Date__c
                      , Appointment_Status__c
                      , Appointment__c
                      , Customer_Name__c
                      , Booking_Unit__c
                      , Booking_Unit__r.Registration_ID__c
                      , Account__r.Email__pc
                      , Account__r.Email__c
                      , Account__r.isPersonAccount
                      , ownerId
                      , owner.Email
                      , owner.Name
                      , Assigned_CRE__r.Name
                      , Purpose_of_your_visit__c
                      , Remarks__c
                   FROM Calling_List__c
                  WHERE RecordTypeId = :devRecordTypeId
                    AND Appointment_Status__c NOT IN ( 'Rejected', 'Cancelled' )
                    AND Appointment__c IN :setAppointmentIds ] ;
    }*/ //End of getAppointmentScheduling

    /*public list<Appointment__c> getAppointments() {
            system.debug( 'strSelectedProcess getAppointments : '+ strSelectedProcess );
            system.debug( 'strSelectedSubProcess getAppointments : '+ strSelectedSubProcess );
            system.debug( 'strSelectedBookingUnits getAppointments : '+ strSelectedBookingUnits );
        system.debug('strSelectedDate '+strSelectedDate);
        Date objDate = Date.valueOf( strSelectedDate );
        system.debug('objDate '+objDate);

        String query = 'Select Id , Name ,Appointment_Date__c ,Building__c,Process_Name__c,';
            query += 'Slots__c,Sub_Process_Name__c,Time_Slot_Sub__c FROM Appointment__c ';
            if( buildingId != null ) {
                query += 'WHERE Appointment_Date__c =: objDate AND Process_Name__c = \'Handover\' AND Building__c  =: buildingId';
            }else {
                if( strSelectedProcess == 'Cheque Collection' ) {
                    query += 'WHERE Appointment_Date__c =: objDate AND Process_Name__c = \'Collections\'';
                    //AND Sub_Process_Name__c = \'Collection\'';
                }else if( strSelectedProcess == 'Mortgage' ) {
                    query += 'WHERE Appointment_Date__c =: objDate AND Process_Name__c = \'Mortgage\'';
                    //AND Sub_Process_Name__c = \'Mortgage\' ';
                }else if( strSelectedProcess == 'Rental Pool' ) {
                    query += 'WHERE Appointment_Date__c =: objDate AND Process_Name__c = \'Rental Pool\'';
                    //AND Sub_Process_Name__c = \'Rental Pool\' ';
                }else {
                    query += 'WHERE Appointment_Date__c =: objDate AND Process_Name__c = \'Generic\' AND Sub_Process_Name__c = \'Head Office Appointment\' ';
                }
            }
        system.debug( ' query : ' + query);
        List<Appointment__c> recordList = Database.query(query);
        system.debug('!!!!!recordList'+recordList);
        
        return recordList;
    }*/ //End of getAppointments

    public class AppointmentWrapper {
        public Appointment__c objApp {get;set;}
        public Calling_List__c objCL {get;set;}
        public Boolean isSelected{get;set;}

        public AppointmentWrapper() {

        }

        public AppointmentWrapper( Appointment__c tempObjApp, Calling_List__c tempObjCL ) {
            objApp = tempObjApp ;
            objCL = tempObjCL ;
        }
    } //End of AppointmentWrapper

}