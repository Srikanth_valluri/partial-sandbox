public without sharing class AP_AddressInformationController {
  public String BASE_URL = Site.getBaseSecureUrl();  
  public Id arRTId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get(
        'Agent Registration').getRecordTypeId();
  public NSIBPM__Service_Request__c objSR{get;set;}
  public String strSRID{get;set;}
  public Map<string,string> mapParameters;
  public Boolean isCommunity{get;set;}
  public Boolean isGuestUser {get;set;}  
    public String strRecordType{get;set;}
    public String pgParamAccid {get;set;}
    public String pageMessage {get;set;}
    public String redirectToPageName {get; set;} 
    /*public List<SelectOption> CountriesLst {
        get {
            if (CountriesLst == null) {
                CountriesLst = getCountriesLst();
            }
            return CountriesLst;
        }
        private set;
    }
    public List<SelectOption> CitiesLst {
        get {
            if (CitiesLst == null) {
                CitiesLst = getCitiesLst();
            }
            return CitiesLst;
        }
        private set;
    }*/
  
  public AP_AddressInformationController() {
    strSRID = '';
    strRecordType = '';    
    isGuestUser = false;
    objSR = new NSIBPM__Service_Request__c();
    User objUser = new User();
    mapParameters = new map<string,string>();
    for(User objUsr : [ SELECT Id, Profile.UserLicense.Name, ContactId, Contact.Email, Contact.AccountId,
                                      Contact.FirstName, Contact.LastName, Contact.Account.Website, Contact.Account.Agency_Type__c
                            FROM User
                            WHERE Id =: UserInfo.getUserId()]){
            objUser = objUsr;
            if(objUsr.Profile.UserLicense.name == 'Guest User License'){
                isGuestUser = true;
            }else{
                if(objUsr.ContactId != null){
                    isCommunity = true;
                }
            }
        }
    system.debug('objUser'+objUser);
    if(apexpages.currentPage().getParameters() != null && apexpages.currentPage().getParameters().size() > 0){
            mapParameters = apexpages.currentPage().getParameters();
    }
        if(apexpages.currentPage().getParameters().get('Id') != null) {
            strSRID = apexpages.currentPage().getParameters().get('Id');
        }        
        if(strSRID!=null && strSRID!=''){
                String strQuery = '';
                strQuery += UtilityQueryManager.getAllFields(NSIBPM__Service_Request__c.getsObjecttype().getDescribe()) ;
                System.debug('===strQuery==='+strQuery);
                strQuery += '  WHERE Id =:strSRID ';
                for(NSIBPM__Service_Request__c SR:database.query(strQuery)){
                    objSR = SR;
                }        
       }
       if(objUser != null && objUser.ContactId != null && objUser.Contact.AccountId != null){
            objSR.NSIBPM__Customer__c = objUser.Contact.AccountId;
            System.debug('------------------->'+ objUser.Contact.Account.Agency_Type__c);
            if(objUser.Contact.Account.Agency_Type__c != null && objUser.Contact.Account.Agency_Type__c.equalsIgnoreCase('Individual')){
                System.debug('level 1 cleared');
                System.debug('objUser.Contact.FirstName--------------->' + objUser.Contact.FirstName);
                 if(objUser.Contact.FirstName != null){
                     System.debug('level 2 cleared');
                    objSR.First_Name__c = objUser.Contact.FirstName;
                 }
                 System.debug('objUser.Contact.LastName--------------->' + objUser.Contact.LastName);
                 if(objUser.Contact.LastName != null){
                     System.debug('level 3 cleared');
                    objSR.Last_Name__c = objUser.Contact.LastName;
                 }
            }
        }
        objSR.Is_Flow_Request__c = true;             
        system.debug('objSR>>>> ' + objSR);
        system.debug('isCommunity>>>> ' + isCommunity);
  }

    public PageReference upsertRequest(){
        try{
            system.debug('==objSR=='+objSR);
            if(!(objSR.filled_page_ids__c).contains('Address-03') ){
                    objSR.filled_page_ids__c += ',Address-03';
            }
            
            
            upsert objSR;
            PageReference pg;
             if(String.isBlank(redirectToPageName)){
                 if (objSR.Agent_Registration_Type__c == 'LOI') {
                    pg =  Page.AP_DAMAC_CompanyPersonnel;
                } else {
                    pg = Page.AP_BankInformation;
                }
             }else{
                 pg = new PageReference(BASE_URL +'/'+redirectToPageName);
             }
            
            //PageReference pg = new PageReference(Label.CommunityRedirectURL+'AP_UploadDocuments');
            
            pg.getParameters().put('id',objSR.id);
            return pg;

        }
        catch(Exception e){
            pageMessage = e.getMessage();
            system.debug('Exception==> '+e);
            return null;
        }
    }

    /*private List<SelectOption> getCountriesLst() {
        List<SelectOption> optionList = new List<SelectOption>();
        
        try {
            Map<String, Schema.Sobjectfield> srFields = Schema.SObjectType.NSIBPM__Service_Request__c.fields.getMap();
            Schema.DescribeFieldResult describe = srFields.get('Country__c').getDescribe();
            System.debug('==describe=='+describe);
            List<Schema.PicklistEntry> options = describe.getPicklistValues();
            System.debug('==options=='+options);
      optionList.add(new selectOption('', 'Choose Country'));
            for (Schema.PicklistEntry option : options) {
                optionList.add(new selectOption(option.getValue(), option.getLabel()));
            }           
                      
        }
        catch(Exception ex) {
            System.debug('Unable to get field details');
            //throw ex;
        }
        return optionList;
    }*/

    /*private List<SelectOption> getCitiesLst() {
        List<SelectOption> optionList = new List<SelectOption>();
        
        try {
            Map<String, Schema.Sobjectfield> srFields = Schema.SObjectType.NSIBPM__Service_Request__c.fields.getMap();
            Schema.DescribeFieldResult describe = srFields.get('City__c').getDescribe();
            System.debug('==describe=='+describe);
            List<Schema.PicklistEntry> options = describe.getPicklistValues();
            System.debug('==options=='+options);
      optionList.add(new selectOption('', 'Choose City'));
            for (Schema.PicklistEntry option : options) {
                optionList.add(new selectOption(option.getValue(), option.getLabel()));
            }           
                      
        }
        catch(Exception ex) {
            System.debug('Unable to get field details');
        }
        return optionList;
    }*/
}