/**
* Email services are automated processes that use Apex classes
* to process the contents, headers, and attachments of inbound
* email.
*/
global class NewStepApproval implements Messaging.InboundEmailHandler {
    
    public Map<String, Step_Status_Transition__c> mapStepTransition;
    public String SRID {get; set;}
    public New_Step__c step {get; set;}
    public NSIBPM__Service_Request__c objSR {get; set;}
    public string selTransition{get;set;}
    public string RejReason{get;set;}
    public string StepNotes{get;set;}
    
    /*********************************************************************************************
* @Description : Prepares all the valid transitions from a given step                        *
* @Params      : Email, Envelope
* @Return      :  void
*********************************************************************************************/
    
    public void Prepare_Transitions(){
        set<id> setValidSteps = new set<id>();
        mapStepTransition = new map<String, Step_Status_Transition__c>();
        for(Step_Status_Transition__c trans : [SELECT From_Step_Status__c, To_Step_Status__c, Is_Closed__c, Service_Request_Type__c,
                                               SR_External_Status__c, SR_Internal_Status__c, Step_No__c, Step_Type__c
                                               FROM Step_Status_Transition__c 
                                               WHERE From_Step_Status__c =: step.Step_Status__c 
                                               AND Step_Type__c =: step.Step_Type__c
                                               AND Step_No__c =: step.Step_No__c])
        {
            mapStepTransition.put(trans.Id, trans);
        }
        system.debug('mapStepTransition ' + mapStepTransition);
    }
    
    /*********************************************************************************************
* @Description : Handles the inbound Email and applies the reply value onto the Step         *
* @Params      : Email, Envelope                                                             *
* @Return      :  Messaging.InboundEmailResult                                               *
*********************************************************************************************/
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        system.debug(email);
        String DPPayment = '';
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        string strEmailBody = email.plainTextBody;
        string emailFromAddress = email.fromAddress;
        string strEmailSubject = email.subject;
        system.debug('strEmailSubject: ' + strEmailSubject);
        system.debug('strEmailBody: ' + strEmailBody);
        system.debug('email.fromAddress: ' + email.fromAddress);
        
        string StepName;
        User replyUser = new User();
        
        for(User u : [SELECT Id, FirstName, LastName, Email, LastModifiedDate FROM User WHERE Email =: emailFromAddress ORDER BY LastModifiedDate DESC LIMIT 1]){
            replyUser = u;
        }
        
        if(strEmailSubject != null && strEmailSubject != ''){
            list<string> lstSubject = new list<string>();
            if(strEmailSubject.indexOf('[')>-1){
                string strTemp = strEmailSubject.split('\\[')[1];
                string strFinalString = strTemp.replace(']','');
                strFinalString = strFinalString.replaceAll( '\\s+','');
                StepName = strFinalString;
            }
        }
        
        system.debug('StepName ' + StepName);
        step = new New_Step__c();
        for(New_Step__c stp :[SELECT Name, Comments__c, Step_No__c, Step_Status__c, Step_Type__c, 
                              Service_Request__c, Rejection_Reason__c
                              FROM New_Step__c 
                              WHERE Name =: StepName])
        {
            step = stp;
        }
        system.debug('step: ' + step);
        if(step != null){
            objSR = SRUtility.getSRDetails(step.Service_Request__c);
            SRID = objSR.Id;
            Prepare_Transitions();
            system.debug('objSR: ' + objSR);
        }
        
        string FinalBody = strEmailBody;
        List<Email_Service_Body_Split__c> emailCustomSettingList = Email_Service_Body_Split__c.getAll().values();
        for(Email_Service_Body_Split__c emailText : emailCustomSettingList){
            system.debug('emailText: ' + emailText);
            if(FinalBody != null && FinalBody != '' && emailText.Active__c && FinalBody.indexOf(emailText.Split_By_Value__c) > -1){
                FinalBody = FinalBody.split(emailText.Split_By_Value__c)[0];
            }
        }
        
        string EmailReply = '';
        system.debug('FinalBody: ' + FinalBody);
        String comments;
        if(String.isNotBlank(FinalBody)){
            if(FinalBody.indexOf('[') > -1){
                comments = FinalBody.split('\\[')[1];
                comments = comments.substringBefore(']');
                StepNotes = comments;
            }
            String lowerCaseStatus = '';
            for(Step_Status_Transition__c stepTrans : mapStepTransition.values()){
                FinalBody = FinalBody.toLowerCase();
                system.debug('stepTrans: ' + stepTrans);
                lowerCaseStatus = (stepTrans.To_Step_Status__c).toLowerCase();
                system.debug('lowerCaseStatus: ' + lowerCaseStatus);
                system.debug('contains approve: '+ (stepTrans.To_Step_Status__c).containsIgnoreCase('approve'));
                if(
                    (((FinalBody.indexOf('yes') > -1) || (FinalBody.indexOf('approve') > -1) || (FinalBody.indexOf('approved') > -1)) && (stepTrans.To_Step_Status__c).containsIgnoreCase('approve'))
                    || (((FinalBody.indexOf('no') > -1) || (FinalBody.indexOf('reject') > -1) || (FinalBody.indexOf('rejected') > -1)) && (stepTrans.To_Step_Status__c).containsIgnoreCase('reject'))
                    || ((FinalBody.indexOf('info') > -1) && (stepTrans.To_Step_Status__c).containsIgnoreCase('info'))
                ){
                    EmailReply = stepTrans.To_Step_Status__c;
                    selTransition = stepTrans.Id;
                    if((stepTrans.To_Step_Status__c).containsIgnoreCase('reject')){
                        RejReason = comments;
                        StepNotes = '';
                    }
                    break;
                }
            }
        }
        system.debug('EmailReply: ' + EmailReply);
        system.debug('selTransition: ' + selTransition);
        system.debug('StepNotes: ' + StepNotes);
        system.debug('RejReason: ' + RejReason);
        
        if(StepName != null && StepName != '' && EmailReply != null && EmailReply != '' && step != null){
            if(EmailReply.containsIgnoreCase('reject') && (RejReason == null || RejReason == '')){
                
            }else{
                SaveChanges(replyUser);
            }
        }
        
        return result;
    }
    
    public void SaveChanges(User replyUser){
        if(selTransition != null && mapStepTransition.get(selTransition) != null){
            //Savepoint can only be set after making the callout 
            Savepoint Stat_svpoint = Database.setSavepoint();
            try{
                if(mapStepTransition.get(selTransition).SR_Internal_Status__c != null && mapStepTransition.get(selTransition).SR_External_Status__c != null){
                    
                    NSIBPM__Service_Request__c selectedSR = new NSIBPM__Service_Request__c(Id = SRID);
                    selectedSR = [SELECT Id, Recordtype.Name FROM NSIBPM__Service_Request__c WHERE Id =: SRID];
                    if(selectedSR.Recordtype.Name == 'Deal'){
                        if(step != null && 
                           (step.Step_Type__c.equalsIgnoreCase('DEAL_AUTO_REJECTION') || 
                            step.Step_Type__c.equalsIgnoreCase('Deal Rejection'))){
                                system.debug('invoking the make sr docs optional code.');
                                selectedSR.NSIBPM__Required_Docs_not_Uploaded__c = false;
                                update selectedSR;
                                //Calling method to make SR Docs optional for rejection step. 
                                makeSrDocsOptional(SRID);   
                            }
                    }
                    if(mapStepTransition.get(selTransition).SR_Internal_Status__c != null){
                        selectedSR.Internal_Status__c = mapStepTransition.get(selTransition).SR_Internal_Status__c;
                    }
                    if(mapStepTransition.get(selTransition).SR_External_Status__c != null){
                        selectedSR.External_Status__c = mapStepTransition.get(selTransition).SR_External_Status__c;
                    }
                    update selectedSR;
                }
                step.Step_Status__c = mapStepTransition.get(selTransition).To_Step_Status__c;
                step.Comments__c = StepNotes;
                step.Rejection_Reason__c = RejReason;
                step.Is_Closed__c = mapStepTransition.get(selTransition).Is_Closed__c;
                //step.Last_Modified_By_Email__c = replyUser.id;
                update step;
            }catch(DMLException ex){
                System.debug('exception line: ' + ex.getLineNumber());
                System.debug('exception message: ' + ex.getMessage());
                string DMLError = ex.getdmlMessage(0)+'';
                if(DMLError == null){
                    DMLError = ex.getMessage() +' ';
                }
                selTransition = null;
                Database.rollback(Stat_svpoint);
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLError));
            }
        }else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please select the status to proceed.'));
        }
        
    }
    
    /*********************************************************************************************
* @Description : #v1.2 : Making all Unit Docs Optional on Deal rejection.                      *
* @Params      : Id                                                                          *
* @Return      : void                                                                        *
*********************************************************************************************/
    public void makeSrDocsOptional(Id srid){
        List<Unit_Documents__c> srdocs = new List<Unit_Documents__c>();
        srdocs = [SELECT Is_Required__c, Optional__c
                  FROM Unit_Documents__c 
                  WHERE Service_Request__c =: srid AND 
                  (Optional__c = false OR Is_Required__c = True)
                  LIMIT: LIMITS.getLimitQueryRows()];
        if(srdocs.size() > 0){
            for(Unit_Documents__c doc : srdocs){
                doc.Optional__c = true;
                doc.Is_Required__c = false;
            }
            update srdocs;
        }
    }
    
}