public class AP_CreateProofOfPaymentController  {
     public Proof_of_Payment__c insertPOP{get;set;}
    Public List<SelectOption> relatedBookingUnitList  {get; set;}
    public attachment objAttachment{get;set;}
    public string strCaseID ;
    //public String UserName{get;set;}
    public NSIBPM__Service_Request__c objSR  {get; set;}
    //public static id parentId;
    public  String parentId;
   // public  String bookingUnitId;
    public Case objOldCase = new Case();
    public Case objcase = new Case();
    public list<Booking_Unit__c> lstSRRelatedBookingUnits{get;set;}
    public boolean isPageError {get; set;}
    //public Boolean displayInputputText{get;set;}
    //public Boolean chkBx{get;set;}
    //public String input{get;set;}
    public string SRName {get;set;}
    public string selectedBookingUnit {get;set;}
    public String bookingUnitId {get;set;}
    public String buyerName {get;set;}
    public string statusDisplayname{get;set;}
    public string selectedBookingUnitStatus {get;set;}
    public transient string stringifyinqobj {get;set;}
    public integer intCounter = 0;
    public string buyerAccountId ;
    public String strSelectedLanguage {get; set;}
    public List<SR_Attachments__c> srAttachList = new  List<SR_Attachments__c>();
    public string BUSelected {get;set;}
    List<String> bookingUnitStatusList = new List<String>();
    public AP_CreateProofOfPaymentController() {
        parentId = ApexPages.currentPage().getParameters().get('id');
                strSelectedLanguage = apexpages.currentPage().getParameters().get('langCode');
        objSR = new NSIBPM__Service_Request__c();
         Booking__c bookingObj = new Booking__c();
         List<Buyer__c> buyerList = new List<Buyer__c>();
        insertPOP = new Proof_of_Payment__c ();
        strCaseID = '';
        isPageError = false;
        selectedBookingUnitStatus = 'Active';
        relatedBookingUnitList = new List<SelectOption>();
        lstSRRelatedBookingUnits = new list<Booking_Unit__c>();
        bookingUnitId = ApexPages.currentPage().getParameters().get('bookingUnitId');
        if(String.isNotBlank(bookingUnitId)) {
            getTheParameterValues();
        }else{
            for(Booking_Unit_Active_Status__c status : Booking_Unit_Active_Status__c.getall().values()){            
                bookingUnitStatusList.add(status.Status_Value__c);
            }
            system.debug('bookingUnitStatusList---'+bookingUnitStatusList);
        
            lstSRRelatedBookingUnits = [Select id ,Total_Collections__c,Unit_Location__c,Unit_Selling_Price__c,Requested_Token_Amount__c,
                                        Unit_Name__c, Price_per_sq_ft__c,Amount_Paid__c,Credit_Amount_in_Words__c,Total_due_Overdue__c, name,
                                         Booking__c, Booking__r.Id, Booking__r.Deal_SR__c,Registration_Status__c 
                                         From Booking_Unit__c Where Booking__r.Deal_SR__c =: parentId 
                                         AND Registration_Status__c IN: bookingUnitStatusList];
            relatedBookingUnitList.add(new SelectOption('None','None'));
            system.debug('parentId---'+parentId);
            system.debug('lstSRRelatedBookingUnits---'+lstSRRelatedBookingUnits);
            if(lstSRRelatedBookingUnits.isEmpty()){
                selectedBookingUnitStatus = 'Inactive';
                getTheParameterValues();
            }else{
                for(Booking_Unit__c buObj :lstSRRelatedBookingUnits){
                    if(buObj.id != null && buObj.Unit_Location__c!= null
                    && !relatedBookingUnitList.contains(new SelectOption(buObj.id,buObj.Unit_Location__c))){
                        relatedBookingUnitList.add(new SelectOption(buObj.id,buObj.Unit_Location__c));
                    }
                }
            }
        }
        
        
        //
       // SelectedBookingUnits();
        
        
         
        if(String.isNotBlank(parentId)){
            insertPOP.Deal_SR__c = parentId;
             objSR = [Select id,name,NSIBPM__External_Status_Code__c from NSIBPM__Service_Request__c where id =: parentId];
              SRName = objSR.name;
              bookingObj = [Select id,Account__c,Account__r.Name from Booking__c where Deal_SR__r.id =: parentId];
              if(String.isNotBlank(bookingObj.id)){
                  buyerList =  [select id,name,First_Name__c,Last_Name__c,Account__c,Account__r.id,Account__r.Name from Buyer__c where Booking__r.id=:bookingObj.id
                                            AND Primary_Buyer__c = true];
              }
              //system.debug('buyerList---------'+buyerList[0]);
              if(buyerList.size() != 0 && !buyerList.isEmpty() && buyerList != null){
                  buyerName = buyerList[0].First_Name__c+' '+ buyerList[0].Last_Name__c;
                  buyerAccountId = buyerList[0].Account__c;
              }
               
        }
        if(String.isNotBlank(bookingUnitId)){
            List<Booking_Unit__c> bookingUnitList =  [select id,Name,Booking__r.id from Booking_Unit__c where id=:bookingUnitId];
            system.debug('bookingUnitId---------'+bookingUnitId); 
            system.debug('bookingListpop---------'+bookingUnitList);
            system.debug('bookingListpop---------'+bookingUnitList[0].Booking__r.id);
            bookingObj = [Select id,Account__c,Account__r.Name from Booking__c where id =: bookingUnitList[0].Booking__r.id];
            if(String.isNotBlank(bookingObj.id)){
                  buyerList =  [select id,name,Account__c,First_Name__c,Last_Name__c,Account__r.id,Account__r.Name from Buyer__c where Booking__r.id=:bookingObj.id
                                            AND Primary_Buyer__c = true];
              }
            //system.debug('buyerList---------'+buyerList[0]);
           // system.debug('buyerList[0].Account__r.Name---------'+buyerList[0].Account__r.Name); 
            if(buyerList.size() != 0 && !buyerList.isEmpty() && buyerList != null){
                 buyerName = buyerList[0].First_Name__c+' '+ buyerList[0].Last_Name__c;
                  buyerAccountId = buyerList[0].Account__c;
              }
        }
        system.debug('buyerName---------'+buyerName);
        //statusDisplayname = AP_BookingProgressController.SRStatusMapping1(objSR.NSIBPM__External_Status_Code__c);
        string statusName;
        statusName = AP_BookingProgressController.SRStatusMapping1(objSR.NSIBPM__External_Status_Code__c);

        if(String.isBlank(statusName)) {
            statusDisplayname = 'Awaiting Token Verification';
        } else {
            statusDisplayname = statusName;
        }
        system.debug('statusDispalyname--'+statusDisplayname);


        objAttachment = new Attachment();
        system.debug('@@ insertPOP@@ id 222' + insertPOP);
        //
       /* lstSRRelatedBookingUnits = [Select id ,Total_Collections__c,Unit_Location__c,Unit_Selling_Price__c,Requested_Token_Amount__c, Unit_Name__c, Price_per_sq_ft__c,Amount_Paid__c,Credit_Amount_in_Words__c,Total_due_Overdue__c, name, Booking__c, Booking__r.Id, Booking__r.Deal_SR__c From Booking_Unit__c Where Booking__r.Deal_SR__c =: parentId];
        system.debug('lstSRRelatedBookingUnits ----'+lstSRRelatedBookingUnits);
        for(Booking_Unit__c buObj : lstSRRelatedBookingUnits){


            if(buObj.id != null && !relatedBookingUnitList.contains(new SelectOption(buObj.id,buObj.Unit_Name__c))){
                relatedBookingUnitList.add(new SelectOption(buObj.id,buObj.Unit_Name__c));
            }
        }*/
        //displayInputputText = false;
    }
    public static List<selectOption> getbookingUnitStatus() {
        List<selectOption> options = new List<selectOption>();
        options.add(new SelectOption('Active','Active'));
        options.add( new SelectOption('Inactive','Inactive'));
        return options;
    }

   public static List<String> getPaymentMode()  {
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Proof_of_Payment__c.Payment_Mode__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            String valueOfPicklist = f.getValue();
            options.add( f.getLabel());
        }
        return options;
    }
    /**
     * Method to fetch the details as per Parameters passed
     */
    public void  getTheParameterValues() {

        system.debug('>>>selectedBookingUnitStatus>>>'+selectedBookingUnitStatus);
        bookingUnitId = checkStringValueAndAssign (Apexpages.currentpage().getparameters().get('bookingUnitId'));
        parentId = checkStringValueAndAssign(ApexPages.currentPage().getParameters().get('id'));
         system.debug('>>>parentId86>>>'+parentId);
          relatedBookingUnitList = new List<SelectOption>();
        //lstSRRelatedBookingUnits = [Select id ,Total_Collections__c,Unit_Location__c,Unit_Selling_Price__c,Requested_Token_Amount__c, Unit_Name__c, Price_per_sq_ft__c,Amount_Paid__c,Credit_Amount_in_Words__c,Total_due_Overdue__c, name, Booking__c, Booking__r.Id, Booking__r.Deal_SR__c From Booking_Unit__c Where Booking__r.Deal_SR__c =: parentId];
        if(String.isNotBlank(bookingUnitId)) {
            lstSRRelatedBookingUnits = [Select
                                            id ,Total_Collections__c,Unit_Location__c,
                                            Unit_Selling_Price__c,Requested_Token_Amount__c, Unit_Name__c, Price_per_sq_ft__c,Amount_Paid__c,
                                            Credit_Amount_in_Words__c,Total_due_Overdue__c, name, Booking__c, Booking__r.Id, Booking__r.Deal_SR__c
                                            From Booking_Unit__c Where id =: bookingUnitId];
            //parentId = [SELECT Id,Booking__r.Deal_SR__r.Id FROM Booking_Unit__c  WHERE Id=:bookingUnitId LIMIT 1].Booking__r.Deal_SR__r.Id;
            system.debug('>>>lstSRRelatedBookingUnits'+lstSRRelatedBookingUnits);
            for(Booking_Unit__c buObj :lstSRRelatedBookingUnits){
                system.debug('>>>bookingUnitId'+bookingUnitId);
                system.debug('>>>buObj.id'+buObj.id);
                if(bookingUnitId.equals(string.valueOf(buObj.id))){
                    selectedBookingUnit = buObj.id;
                }
                system.debug('>>insertPOP'+insertPOP);
                system.debug('>>insertPOP.Deal_SR__c'+insertPOP.Deal_SR__c);
                insertPOP.Deal_SR__c = buObj.Booking__r.Deal_SR__c;
                system.debug('>>>relatedBookingUnitList'+relatedBookingUnitList);
                //relatedBookingUnitList.add(new SelectOption(buObj.id,buObj.Unit_Name__c));
                if(buObj.id != null && buObj.Unit_Location__c!= null && !relatedBookingUnitList.contains(new SelectOption(buObj.id,buObj.Unit_Location__c))){
                    relatedBookingUnitList.add(new SelectOption(buObj.id,buObj.Unit_Location__c));
                    system.debug('>>>relatedBookingUnitList'+relatedBookingUnitList);
                }
            }

        } else {
            bookingUnitStatusList = new List<String>();
            system.debug('>>>selectedBookingUnitStatus>>>'+selectedBookingUnitStatus);
            if(selectedBookingUnitStatus == 'Active'){
               
                for(Booking_Unit_Active_Status__c status : Booking_Unit_Active_Status__c.getall().values()){            
                    bookingUnitStatusList.add(status.Status_Value__c);
                }
                system.debug('bookingUnitStatusList---'+bookingUnitStatusList);
            }else{

                for(Booking_Unit_Inactive_Status__c status : Booking_Unit_Inactive_Status__c.getall().values()){            
                    bookingUnitStatusList.add(status.Status__c);
                }
                system.debug('bookingUnitStatusList---'+bookingUnitStatusList);
            }
            lstSRRelatedBookingUnits = [Select id ,Total_Collections__c,Unit_Location__c,Unit_Selling_Price__c,Requested_Token_Amount__c,
                                        Unit_Name__c, Price_per_sq_ft__c,Amount_Paid__c,Credit_Amount_in_Words__c,Total_due_Overdue__c, name,
                                         Booking__c, Booking__r.Id, Booking__r.Deal_SR__c,Registration_Status__c 
                                         From Booking_Unit__c Where Booking__r.Deal_SR__c =: parentId 
                                         AND Registration_Status__c IN: bookingUnitStatusList];
            relatedBookingUnitList.add(new SelectOption('None','None'));
            system.debug('parentId---'+parentId);
            system.debug('lstSRRelatedBookingUnits---'+lstSRRelatedBookingUnits);
            //insertPOP.Deal_SR__c = parentId;
            for(Booking_Unit__c buObj :lstSRRelatedBookingUnits){
                if(buObj.id != null && buObj.Unit_Location__c!= null
                && !relatedBookingUnitList.contains(new SelectOption(buObj.id,buObj.Unit_Location__c))){
                    relatedBookingUnitList.add(new SelectOption(buObj.id,buObj.Unit_Location__c));
                }
            }
            system.debug('relatedBookingUnitList137---'+relatedBookingUnitList);
            
            system.debug('selectedBookingUnitStatus---'+selectedBookingUnitStatus);
        }
        String tokenAmt = checkStringValueAndAssign (Apexpages.currentpage().getparameters().get('tokenAmt'));
        if(String.isNotBlank(tokenAmt)) {
            insertPOP.Payment_Amount__c = Decimal.valueOf(tokenAmt);
        }

        
    }
   /**
     * Method to Check if the String is Valid and assign the values accordingly
     *
     * @param: String Value
     *
     * @return: String Value as per Conditions
     */
    public String checkStringValueAndAssign(String paramVal) {
        if(String.isNotBlank(paramVal)) {
            paramVal = paramVal;

        } else {
            paramVal = '';
        }
        return paramVal;
    }
    public class POPDetails {
        public String dealSR1;
        public String bu;
        public String curr;
        public String pa;
        public String pd;
        public String pm;
        public String pr;
        public String senderName;
        public String chequeName;
        public String swiftCode;
        public String fileBody;
        public String fileName;
        public String blobfileThirdParty;
        public String filenameThirdParty;
        //public String fs;
        //public String fr;
        //public String status;
        //public String impsName;
        //public String impsStatus;

    }

    /*public static List<SelectOption> getBookingUnits() {
        system.debug('parentId----'+parentId);
        list<Booking_Unit__c> lstBookingUnits = new list<Booking_Unit__c>();
        lstBookingUnits = [Select id,Unit_Location__c,Total_Collections__c, name, Booking__c, Booking__r.Id, Booking__r.Deal_SR__c From Booking_Unit__c Where Booking__r.Deal_SR__c =: parentId];
        system.debug('lstBookingUnits----'+lstBookingUnits);

        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('None', '---None---'));
        if(!lstBookingUnits.isEmpty()) {
            for(Booking_Unit__c objBU : lstBookingUnits)
            {
                options.add(new SelectOption(objBU.Id,objBU.name));
            }
        }
        system.debug('options--------'+options);
        return options;
    }*/

       public PageReference submitSR(){
        try{
            if(String.isBlank(strCaseID)){
                system.debug('BUSelected---'+BUSelected);
                system.debug('selectedBookingUnit---'+selectedBookingUnit);
                
                Id POPcase = Schema.SObjectType.case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
                case objcase = new case();
                //objcase.AccountID = loggedinuser.accountid;
                objcase.RecordtypeID = POPcase;
                objcase.SR_Type__c = 'Proof of Payment SR';
                objcase.Type = 'Proof of Payment SR';
                objcase.Booking_Unit__c = BUSelected;
                objcase.New_Booking_Unit__c = BUSelected;
                
                insert objcase;
                strCaseID = objcase.id;
                 System.debug('....strCaseID...'+strCaseID);
                 system.debug('stringifyinqobj224---------'+stringifyinqobj);
                objOldCase = [Select id,CaseNumber from Case where id =: strCaseID];
            }
        }
        catch(Exception e){
            System.debug('....selected booking unit error...'+e.getLineNumber()+e);
        }
        return null;
    }
    public PageReference Save() {
        try{
            case objcaseNew = new case();
            system.debug('stringifyinqobj---------'+stringifyinqobj);
            system.debug('parentId----'+parentId);
            User loggedinuser = [Select accountid,contactid,id from User where id = :Userinfo.getuserid()];
            POPDetails pop = (POPDetails)JSON.deserialize(stringifyinqobj,POPDetails.class);
            system.debug('pop---------'+pop);
            
           
            
            
            system.debug('pop.pd------'+pop.pd);
            String[] formatdate = pop.pd.split('/');
            Date formatissueDate = Date.newInstance(Integer.valueof(formatdate[2].trim()),
                Integer.valueof(formatdate[0].trim()),Integer.valueof(formatdate[1].trim()));
            Id POPcase = Schema.SObjectType.case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
            system.debug('loggedinuser.accountid-----'+loggedinuser.accountid);
            
            //objcase.AccountID = loggedinuser.accountid;
            objcaseNew.RecordtypeID = POPcase;
            if(String.isBlank(buyerAccountId)){
                objcaseNew.AccountId = null;
            }else{
                 objcaseNew.AccountId = buyerAccountId;
            }
            List<Contact> contactIDList = [SELECT id,AccountId FROM Contact WHERE  AccountID =: buyerAccountId limit 1];
                    //objcase.Agency__c = loggedinuser.accountid;
            
            objcaseNew.Agency__c = loggedinuser.accountid;
            if(contactIDList != null && contactIDList.size() != 0){
                objcaseNew.ContactId = contactIDList[0].id;
            }else{
                objcaseNew.ContactId = null;
            }
            system.debug('loggedinuser.pop.pm-----'+pop.pm);
            objcaseNew.SR_Type__c = 'Proof of Payment SR';
            objcaseNew.Type = 'Proof of Payment SR';
            objcaseNew.Total_Amount__c =  decimal.valueOf(pop.pa);
            objcaseNew.Payment_Allocation_Details__c = pop.pr;
            objcaseNew.Payment_Mode__c = pop.pm;
            objcaseNew.Payment_Date__c =formatissueDate;
            objcaseNew.Payment_Currency__c =  pop.curr;
            objcaseNew.Status = 'Submitted';
            objcaseNew.Origin = 'Agent Portal';
            objcaseNew.Sender_Name__c = pop.senderName;
            objcaseNew.Cheque_Bank_Name__c = pop.chequeName;
            objcaseNew.Swift_Code__c = pop.swiftCode;
           
            system.debug('>>>objcase>>>>>'+strCaseID);
            system.debug('>>>fileBody>>>>>'+pop.fileBody);
            system.debug('>>>fileName>>>>>'+pop.fileName);
            system.debug('>>>blobfileThirdParty>>>>>'+pop.blobfileThirdParty);
            system.debug('>>>filenameThirdParty>>>>>'+pop.filenameThirdParty);
            if(String.isNotBlank(pop.blobfileThirdParty) && String.isNotBlank(pop.filenameThirdParty) ){
                uploadSRAttachment(pop.blobfileThirdParty,pop.filenameThirdParty);
            }
            if(String.isNotBlank(pop.fileBody) && String.isNotBlank(pop.fileName) ){
                uploadSRAttachment(pop.fileBody,pop.fileName);
                insert srAttachList;
                    if(String.isNotBlank(pop.bu) && pop.bu != 'None'){
                            list<SR_Booking_Unit__c> selectedBooking = new list<SR_Booking_Unit__c>();
                            List<String> bookingUnitIdList = pop.bu.split(',');
                            for(String bookingUnitId : bookingUnitIdList){
                                SR_Booking_Unit__c varSRBooking = new SR_Booking_Unit__c();
                                varSRBooking.Booking_Unit__c = bookingUnitId;
                                varSRBooking.Case__c = strCaseID;
                                selectedBooking.add(varSRBooking);
                            }
                            insert selectedBooking;
                    }
                    
                    objcaseNew.id = strCaseID;
                    objcaseNew = setCaseOwner(objcaseNew);
                    update objcaseNew;
                    List <SR_Attachments__c> objSRattList = [SELECT
                                                                id,case__c,Attachment__c,Attachment_URL__c
                                                             FROM
                                                                SR_Attachments__c where case__c =: strCaseID];
                    //List<Task> taskObjlist = new List<Task>();
                        String attachmentUrl='' ;
                        Task objTask = new Task();
                        objTask.WhatId = strCaseID;
                        objTask.Subject = 'Verify Proof of Payment Details in IPMS';
                        objTask.ActivityDate = system.today().addDays(1);
                        objTask.Assigned_User__c = 'Finance';
                        for(SR_Attachments__c srattachObj : objSRattList){
                            attachmentUrl = attachmentUrl+srattachObj.Attachment_URL__c +',';
                           
                        }
                         objTask.Document_URL__c = attachmentUrl.removeEnd(',');
                        objTask.Process_Name__c ='POP';
                        objTask.Priority = 'High';
                        objTask.Status = 'Not Started';
                        system.debug('--objTask--Proof Of Payment'+objTask);
                        insert objTask;
                    
                    
                    //insert taskObjlist;
                
            }else{
                    isPageError = true;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select POP doc before submitting SR'));
                }
            return null;

        }catch(Exception ex){
            System.debug('...Exception is....'+ex+'....Line Number'+ex.getLineNumber());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
            return null;
        }
       

       /* insertPOP = new Proof_of_Payment__c();
        insertPOP.Deal_SR__c = pop.dealSR1;
        insertPOP.Currency__c = pop.curr;
        insertPOP.Payment_Amount__c = decimal.valueOf(pop.pa);
        if(String.isNotBlank(pop.bu) && !pop.bu.equals('None')){
            insertPOP.Booking_Unit__c = pop.bu;
        }
        insertPOP.Payment_Date__c= formatissueDate;
        insertPOP.Payment_Mode__c= pop.pm;
        insertPOP.Payment_Remarks__c= pop.pr;
        insertPOP.Status__c= 'SR Submitted to Finance';
        system.debug('@@ insertPOP@@ idb4 ' + insertPOP);
        insert insertPOP;
        system.debug('@@ insertPOP@@ id ' + insertPOP);*/

        
    }
    public void uploadSRAttachment(String fileBody,String fileName){
        system.debug('>>>fileBody>>>>>'+fileBody);
        system.debug('>>>fileName>>>>>'+fileName);
        if(String.isNotBlank(strCaseID)){
            UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();
            List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest = new  List<UploadMultipleDocController.MultipleDocRequest>();
                
                lstMultipleDocRequest = docRepositoryUpload(fileName,fileBody, 'POP',objOldCase.CaseNumber);
                if(lstMultipleDocRequest.Size()>0){
                MultipleDocData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocRequest);
                //System.debug('--MultipleDocData--'+MultipleDocData);
                if(MultipleDocData != null && MultipleDocData.status == 'Exception'){
                    ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Error,MultipleDocData.message));
                    //return null;
                }
                if(MultipleDocData != null && (MultipleDocData.Data == null || MultipleDocData.Data.Size() == 0)){
                    ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Error,MultipleDocData.message));
                   // return null;
                }
            }
            SR_Attachments__c objSRatt = new SR_Attachments__c ();
            objSRatt.case__c = strCaseID;
            objSRatt.Type__C = 'POP';
            objSRatt.Name = extractName(fileName);
            if(MultipleDocData != null && MultipleDocData.Data.Size()>0){
                UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[0];
                if(objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)){
                    objSRatt.Attachment_URL__c = objData.url;
                }
                else{
                    isPageError = true;
                    ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Error,'An exception has occurred while processing the request, document-'+objData.PARAM_ID+', PROC_STATUS-'+objData.PROC_STATUS+' URL-'+objData.url));
                   
                }
            }
            else{
                    isPageError = true;
                    ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Error,'An exception has occurred while processing the request of document upload, got null value, please contact the support team.'));
                    
            }
            lstMultipleDocRequest = null;
            MultipleDocData = null;
            fileBody = '';
            fileName = '';
            srAttachList.add(objSRatt);
            //insert objSRatt;
            system.debug('>>>objSRatt>>>>>'+objSRatt.id);
        }  
    }
    private static Case setCaseOwner(Case srPop) {
        List<QueueSobject> lstQueue = [
            SELECT  Id, Queue.Name, QueueId FROM QueueSobject
            WHERE   SobjectType = 'Case' AND Queue.Name = 'Collection Queue'
            LIMIT   1
        ];
        if (!lstQueue.isEmpty()) {
            srPop.OwnerId = lstQueue[0].QueueId;
        }
        return srPop;
    }
    public String extractName( String strName )
    {
        return strName.substring( strName.lastIndexOf('\\')+1 ) ;
    }
    public  List<UploadMultipleDocController.MultipleDocRequest> docRepositoryUpload(String AttachmentName, String AttachmentBody, String DocType, String CaseNumber){
        intCounter++;
        List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequestValues = new List<UploadMultipleDocController.MultipleDocRequest>();
        UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
        objDocRequest.category =  'Document';
        objDocRequest.entityName = 'Damac Service Requests';
        objDocRequest.fileDescription  =  DocType;
        system.debug('--AttachmentName--'+AttachmentName);
        objDocRequest.fileId  =  CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intCounter)+'.'+AttachmentName.substringAfterLast('.');
        objDocRequest.fileName  = CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intCounter)+'.'+AttachmentName.substringAfterLast('.');
        if(objOldCase != null){
            objDocRequest.registrationId =  CaseNumber;
        }
        objDocRequest.sourceFileName  = CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intCounter)+'.'+AttachmentName.substringAfterLast('.');
        objDocRequest.sourceId  =  CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intCounter)+'.'+AttachmentName.substringAfterLast('.');
        blob objBlob = extractBody(AttachmentBody);
        if(objBlob != null){
            objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
        }
        objBlob = null;
        /*if(DocType == 'Other'){
            lstMultipleDocRequestAtt.add(objDocRequest);
        }
        else{
            lstMultipleDocRequest.add(objDocRequest);
        }*/
        lstMultipleDocRequestValues.add(objDocRequest);

        AttachmentName = '';
        AttachmentBody = '';

        return lstMultipleDocRequestValues ;
    }
    public Blob extractBody( String strBody )
    {
        strBody = EncodingUtil.base64Decode( strBody ).toString();
        return EncodingUtil.base64Decode( strBody.substring( strBody.lastIndexOf(',')+1 ) );
    }
    /*public PageReference SelectedBookingUnits(){
        try{
            if(String.isBlank(strCaseID)){
                Id POPcase = Schema.SObjectType.case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
                case objcase = new case();
                //objcase.AccountID = loggedinuser.accountid;
                objcase.RecordtypeID = POPcase;
                objcase.SR_Type__c = 'Proof of Payment SR';
                objcase.Type = 'Proof of Payment SR';
                insert objcase;
                strCaseID = objcase.id;
                 System.debug('....strCaseID...'+strCaseID);
                objOldCase = [Select id,CaseNumber from Case where id =: strCaseID];
            }
        }
        catch(Exception e){
            System.debug('....selected booking unit error...'+e.getLineNumber()+e);
        }
        return null;

    }*/
}